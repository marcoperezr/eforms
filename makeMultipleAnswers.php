<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$surveyCollection = BITAMSurveyCollection::NewInstance($theRepository, null, false);
foreach ($surveyCollection->Collection as $survey) 
{
	$anArrayTypes = array();
	$anArrayTypes[0] = 3;

	$questionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($theRepository, $survey->SurveyID);
	foreach ($questionCollection->Collection as $question) 
	{
		if($question->QTypeID == 3) 
		{
			//sacar las opciones seleccionadas de la tabla paralela
			//$sql = "SELECT FactKey, dim_".$question->QuestionID." FROM ".$survey->SurveyTable." WHERE dim_".$question->QuestionID." <> ".$theRepository->DataADOConnection->Quote("");
			$sql = "SELECT FactKey, dim_".$question->QuestionID." FROM ".$survey->SurveyTable." ORDER BY FactKey";
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			
			if(!is_null($aRS) && $aRS)
			{
				while(!$aRS->EOF)
				{
					$seleccionados = array();
					$seleccionados = explode(";",$aRS->fields["dim_".$question->QuestionID]);
					$factkey = (int) $aRS->fields["factkey"];
					$seleccionadosFlip = array();
	            	$seleccionadosFlip = array_flip($seleccionados);
	    	        //$notSelectValue = 1;
	    	        $notSelectValue = 3;
		            if(count($seleccionados)>0)
	        	    {
	            		$notSelectValue = 3;
	            	}
					$selectOptions = $question->SelectOptions;
					$selectValues = array();
	            	foreach ($selectOptions as $optionKey=>$optionValue)
	            	{
		            	if(isset($seleccionadosFlip[$optionValue]))
	                	{
		                	$selectValues[$optionKey] = 2;
	                	}
	                	else
	                	{
		                	$selectValues[$optionKey] = $notSelectValue;
	                	}
	            	}
	            	foreach($question->QIndDimIds as $optionKey=>$IndDimID) {
		            	$sql = "UPDATE RIFACT_".$survey->ModelID." SET RIDIM_".$IndDimID."KEY = ".$selectValues[$optionKey]." WHERE FactKey = ".$factkey;
		            	echo('UPDATE: '.$sql."<br>");
						$update = $theRepository->DataADOConnection->Execute($sql);
	            	}
	            	$aRS->MoveNext();
				}
			}
		}
	}
}
?>