<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
$goto = "";

global $strWebEntryURL;
$strWebEntryURL = '';

if (array_key_exists('params', $_GET))
{
	require_once("initialize.php");
	require_once("utils.inc.php");
	require_once("config.php");
	require_once("appuser.inc.php");
	
	//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
	$strWebEntryURL = @curPageURL();
	//@JAPR
	
	$anArrayOfParams = explode("\r\n", BITAMDecode($_GET['params']));
	
	/*
	Ejemplos de los parámetros desencriptados:
	$anArrayOfParams[0]=>"FBM_DWH_0081_0014" 
	$anArrayOfParams[1]=> "bitam" 
	$anArrayOfParams[2]=>generateSurvey.php?RepositoryName=fbm_bmd_0081&externalPage=1&surveyID=361&SchedulerID=352&CaptureVia=1&CaptureEmail=rvega@bitam.com&EmailKey=0"
	*/


	if (count($anArrayOfParams) > 2)
	{
		$UserName = strtoupper(trim($anArrayOfParams[0]));
		$Password = trim($anArrayOfParams[1]);
		$goto = trim($anArrayOfParams[2]);
		$url_parts = parse_url($goto);
		$query_string = array_key_exists("query", $url_parts) ? $url_parts["query"] : "";
		$query_array = array();
		parse_str($query_string, $query_array);

		global $BITAMRepositories;		
		$theRepositoryName = $query_array["RepositoryName"];
				
		if (!array_key_exists($theRepositoryName, $BITAMRepositories))
		{
			header("Location: index.php?error=1");
			exit();
		}
		
		$theRepository = $BITAMRepositories[$theRepositoryName];
		if (!$theRepository->open())
		{
			header("Location: index.php?error=2");
			exit();
		}
		
		//03Mayo2013: En capturas por liga de correo se utiliza el usuario maestro
		if(isset($query_array["CaptureVia"]) && $query_array["CaptureVia"]==1)
		{
			$sql = "SELECT CLA_USUARIO, NOM_CORTO, PASSWORD FROM SI_USUARIO WHERE NOM_CORTO = ".$theRepository->ADOConnection->Quote($theRepository->DataADODBDatabase);
			$aRS = $theRepository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die("(indexHTML.inc.php)".translate("Error accessing")." "."SI_USUARIO"." ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
						
			if(!$aRS->EOF)
			{
				$UserName = $aRS->fields["nom_corto"];
				$Password = BITAMDecryptPassword($aRS->fields["password"]);
			}
		}

		$theUserName = $UserName;
		
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
		if (is_null($theUser))
		{
			header("Location: index.php?error=3");
			exit();
		}
		
		$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
		if (is_null($theUser))
		{
			header("Location: index.php?error=4");
			exit();
		}

		if (!is_null($theUser))
		{
			//Establecer el lenguaje en una variable de sesión
			//0=DEFAULT, 1=Spanish, 2=English
			if($theUser->UserLanguageID<=0 || $theUser->UserLanguageID>8)
			{
				$userLanguageID = 2;
				$userLanguage = 'EN';
			}
			else
			{
				$userLanguageID = $theUser->UserLanguageID;
				$userLanguage = $theUser->UserLanguageTerminator;
			}
			
			//Verificar si se encuentra en modo FBM
			$sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";
			
			$aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);
			
			$valueFBMMode = 0;
			
			if ($aRSFBM && !$aRSFBM->EOF)
			{
				if($aRSFBM->fields["ref_configura"]!==null && trim($aRSFBM->fields["ref_configura"])!=="")
				{
					$valueFBMMode = (int)trim($aRSFBM->fields["ref_configura"]);
				}
			}

			//@JAPR 2009-02-10: Corregido el login desde enlaces extrnos para usuarios Bitam, falta integrar el cambio para usuarios LDAP pero desconozco como llegan los
			//parámetros para esos casos
			if (BITAMDecryptPassword($theUser->Password) != $Password)
			{
				setcookie("PABITAM_RepositoryName", $theRepositoryName);
			    setcookie("PABITAM_UserName", $theUserName);
				header("Location: index.php?error=3");
			}
			else
			{
				global $generateLog;

				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				/*session_register("PABITAM_RepositoryName");
				session_register("PABITAM_UserName");
				session_register("PABITAM_UserRole");
				session_register("PABITAM_Password");
				session_register("PABITAM_UserID");
				session_register("PAtheUserID");
				session_register("PAtheAppUserID");
				session_register("PAgenerateLog");
				session_register("SV_ADMIN");
				session_register("SV_USER");
				session_register("PAuserLanguageID");
				session_register("PAuserLanguage");
				session_register("PAchangeLanguage");
				session_register("PAArtusWebVersion");
				session_register("PAFBM_Mode");*/
				
				$_SESSION["PABITAM_RepositoryName"] = $theRepositoryName;
				$_SESSION["PABITAM_UserName"] = $theUserName;
				$_SESSION["PABITAM_UserRole"] = $theAppUser->Role;
				$_SESSION["PABITAM_Password"] = $theUser->Password;
				$_SESSION["PABITAM_UserID"] = $theUser->UserID;
				$_SESSION["PAtheUserID"] = $theUser->UserID;
				$_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
				$_SESSION["PAgenerateLog"] = $generateLog;
				$_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
				$_SESSION["SV_USER"] = $theAppUser->SV_User;
				$_SESSION["PAuserLanguageID"] = $userLanguageID;
				$_SESSION["PAuserLanguage"] = $userLanguage;
				$_SESSION["PAchangeLanguage"] = false;
				$_SESSION["PAArtusWebVersion"] = 6.0;
				$_SESSION["PAFBM_Mode"] = $valueFBMMode;

				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("PAstrGoTo");
				//session_register("PAdisplayAllFrames");
				//session_register("PABITAM_QueryString");
				$_SESSION["PAstrGoTo"]=$goto;
				$_SESSION["PAdisplayAllFrames"]=1;
				$_SESSION["PABITAM_QueryString"] = $query_string;

				if(isset($query_array["DisplayAllFrames"]))
				{
					if(((int)$query_array["DisplayAllFrames"])==0)
					{
						$_SESSION["PAdisplayAllFrames"]=0;
					}
				}
			}
		}
	}
}

//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
@setAppVersion();
//@JAPR

$pageName="generateSurvey.php";
if(isset($query_array["externalPage"]) && $query_array["externalPage"]==1)
{
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("SVExternalPage");
	$_SESSION["SVExternalPage"] = 1;
}

require_once("survey.inc.php");

//Generamos la pagina php a la cual vamos a acceder
$applicatorID = $theUser->UserID;
$surveyID = $query_array["surveyID"];
$schedulerID = $query_array["SchedulerID"];
//@JAPR 2014-11-05: Modificado para que permita la entrada directa a una encuesta sin tener que estar ligada a un Scheduler tipo EMail
$captureVia = (int) @$query_array["CaptureVia"];
$captureEmail = (string) @$query_array["CaptureEmail"];
$emailKey = (int) @$query_array["EmailKey"];
//@JAPR

//Verificamos antes si existe el Scheduler y el Survey
$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($theRepository, $schedulerID);

//@JAPR 2014-11-05: Modificado para que permita la entrada directa a una encuesta sin tener que estar ligada a un Scheduler tipo EMail
//Sólo validará las cuestiones relacionadas con el EMail si se recibió el parámetro para dicho tipo de Schedulers
if ($captureVia) {
	if(is_null($schedulerInstance))
	{
		$strError = "The scheduler of the survey does not exist";
		$pageName = "genericErrorPage.php?StrError=".urlencode($strError);
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$pageName."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	else 
	{
		$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $schedulerInstance->SurveyID);
		
		if(is_null($surveyInstance))
		{
			$strError = "The survey does not exist";
			$pageName = "genericErrorPage.php?StrError=".urlencode($strError);
			exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$pageName."\">\n</head>\n<body>\n</body>\n</html>\n");
		}
		else 
		{
			//Verificamos todos los casos posibles con Scheduler que No usan Catalogo
			//o bien lo usan y tienen Email Filter definido
			if($schedulerInstance->UseCatalog == 0)
			{
				//Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
				$arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
			}
			else 
			{
				switch ($schedulerInstance->EmailFilter)
				{
					case 0:
						//Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
						$arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
						break;
					case 1:
						//@JAPR 2015-02-09: Agregado soporte para php 5.6
						/*session_register("SVExternalEmailFilter");
						session_register("SVExternalSchedulerID");
						session_register("SVExternalCatalogID");
						session_register("SVExternalEmail");
						session_register("SVExternalEmailKey");*/
						$_SESSION["SVExternalEmailFilter"] = $schedulerInstance->EmailFilter;
						$_SESSION["SVExternalSchedulerID"] = $schedulerID;
						$_SESSION["SVExternalCatalogID"] = $schedulerInstance->CatalogID;
						$_SESSION["SVExternalEmail"] = $captureEmail;
						$_SESSION["SVExternalEmailKey"] = $emailKey;
						
						//Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
						$arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmailKey($theRepository, $schedulerID, $captureEmail, $emailKey);
						break;
					case 2:
						//@JAPR 2015-02-09: Agregado soporte para php 5.6
						/*session_register("SVExternalEmailFilter");
						session_register("SVExternalSchedulerID");
						session_register("SVExternalCatalogID");
						session_register("SVExternalEmail");
						session_register("SVExternalEmailKey");*/
						$_SESSION["SVExternalEmailFilter"] = $schedulerInstance->EmailFilter;
						$_SESSION["SVExternalSchedulerID"] = $schedulerID;
						$_SESSION["SVExternalCatalogID"] = $schedulerInstance->CatalogID;
						$_SESSION["SVExternalEmail"] = $captureEmail;
						$_SESSION["SVExternalEmailKey"] = 0;
						
						//Verificamos si existe informacion en la encuesta para el email y scheduler que se indica
						$arrayData = BITAMSurvey::hasSurveyDataInThisSchedEmail($theRepository, $schedulerID, $captureEmail);
						break;
				}
			}
			
		    if(isset($arrayData["EntryID"]) && $arrayData["EntryID"]>0)
		    {
				$entryID = $arrayData["EntryID"];
		    	$arrayDateAndUser = BITAMSurvey::getDateAndUserFromEntryID($theRepository, $surveyInstance, $entryID);
			    $dateID = (string) @$arrayDateAndUser["DateID"];
			    $endDateID = (string) @$arrayDateAndUser["EndDateID"];
			    if (trim($endDateID) == '') {
			    	$endDateID = $dateID;
			    }
			    $hourID = (string) @$arrayDateAndUser["HourID"];
			    $userID = (string) @$arrayDateAndUser["UserID"];
			    $endHourID = (string) @$arrayDateAndUser["EndTime"];
			    $latitude = (string) @$arrayDateAndUser["Latitude"];
			    $longitude = (string) @$arrayDateAndUser["Longitude"];
				//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			    $accuracy = (string) @$arrayDateAndUser["Accuracy"];
		    }
			
			if(is_null($arrayData))
			{
				//@JAPR 2014-11-05: Modificado para que permita la entrada directa a una encuesta sin tener que estar ligada a un Scheduler tipo EMail
				//Nunca se cargaba la instancia de la forma así que esta línea generaba error
				$strError = "You can not access this survey";
				//@JAPR
				$pageName = "genericErrorPage.php?StrError=".urlencode($strError);
				exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$pageName."\">\n</head>\n<body>\n</body>\n</html>\n");
			}
		}
	}
}
//@JAPR
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>KPIOnline Forms</title>
</head>
<body style="background-color:#f5f5f5">
<form id="frmLoadEntry" name="frmLoadEntry" method="POST" action="generateSurvey.php?surveyID=<?=$surveyID?>&CaptureVia=1&CaptureEmail=<?=$captureEmail?>&SchedulerID=<?=$schedulerID?>">
<?
	if(count($arrayData)>0)
	{
?>
	<input type="hidden" id="DateID" name="DateID" value="<?=$arrayData["DateID"]?>"/>
	<input type="hidden" id="HourID" name="HourID" value="<?=$arrayData["HourID"]?>"/>
	<input type="hidden" id="ApplicatorID" name="ApplicatorID" value="<?=$applicatorID?>"/>
	<input type="hidden" id="EntryID" name="EntryID" value="<?=$arrayData["EntryID"]?>"/>
    <input type="hidden" id="EndDateID" name="DateID" value="<?= $endDateID ?>"/>
    <input type="hidden" id="EndHourID" name="EndHourID" value="<?= $endHourID ?>"/>
    <input type="hidden" id="Latitude" name="Latitude" value="<?= $latitude ?>"/>
    <input type="hidden" id="Longitude" name="Longitude" value="<?= $longitude ?>"/>
    <input type="hidden" id="Accuracy" name="Accuracy" value="<?= $accuracy ?>"/>
<?
	}
?>
    <input type="hidden" id="WebEntryURL" name="WebEntryURL" value="<?=$strWebEntryURL?>"/>
</form>
<script language="JavaScript">
	frmLoadEntry.submit();
</script>
</body>
</html>