<?php

include_once "surveyAgendaScheduler.inc.php";

class BITAMSurveyAgendaSchedulerExtCollection extends BITAMSurveyAgendaSchedulerCollection
{
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$res['collection'] = [];
		
		if( $action = getParamValue('action', 'both', '(string)', true) )
		{
			if( $action == 'getUserList' )
			{
				$rs = $aRepository->ADOConnection->Execute("SELECT cla_usuario, cuenta_correo FROM SI_USUARIO WHERE cla_usuario > 0");

				$res = [];

				while( $rs && !$rs->EOF )
				{
					$res[] = ["value" => $rs->fields["cla_usuario"], "text" => $rs->fields["cuenta_correo"]];
					$rs->MoveNext();
				}
			} else if( $action == "getCollection" )
			{
				$filter = [ 'UserID' => null, 'SearchString' => null, 'anArrayOfAgendaIDs' => null, 'CheckPointID' => null ];

				if( $arrFilter = getParamValue('filter', 'both', '(array)', true) )
				{
					$filter = array_merge( $filter, $arrFilter );
				}

				if( getParamValue('arrAgendaSchedIDs', 'both', '(array)', true) )
				{
					$filter['anArrayOfAgendaIDs'] = getParamValue('arrAgendaSchedIDs', 'both', '(array)', true);
				}

				$searchKey = array();
				$arrAttrsKey = [];

				if( @$filter['CheckPointID'] && count( $filter['CheckPointID'] ) == 1 ) {

					$anCheckPoint = BITAMCheckpoint::NewInstanceWithId( $aRepository, $filter['CheckPointID'][0] );

					$arrKeys = $anCheckPoint->getAttrsKey();

					
					$arrAttrsDataSource = [];
					$arrFilters = [];
					$eachKeys = [];

					foreach ($arrKeys as $each)
					{
						$arrAttrsKey[] = $each['id'];

						if( @$_REQUEST['filter'][$each['id']] ) {
							$eachKeys[] = $_REQUEST['filter'][$each['id']];
						} else {
							$eachKeys[] = '%';
						}
					}

					$searchKey[] = $eachKeys;

					$arrAttr = BITAMDataSourceMemberCollection::NewInstance($aRepository, $anCheckPoint->catalog_id, $arrAttrsKey);	
				}

				$res['orderFilter'] = ['CheckPointID', 'UserID'];

				$res['orderFilter'] = array_merge($res['orderFilter'], $arrAttrsKey);

				//var_dump($searchKey); exit();

				$aCollection = BITAMSurveyAgendaSchedulerExtCollection::NewInstance($aRepository, $filter['anArrayOfAgendaIDs'], $filter['UserID'], null, null, null, null, 100, false, null, $filter['SearchString'], null, $filter['CheckPointID']);

				$defFilters = [
					[ 
						"key" => "CheckPointID"
						, "description" => translate('Check point')
						, "propKey" => "CheckPointID"
						, "propDesc" => "CheckPointName"
					 ],
					[ 
						"key" => "UserID"
						, "description" => translate('User')
						, "propKey" => "UserID"
						, "propDesc" => "UserName"
					 ]
				];

				foreach ($aCollection->Collection as $each)
				{
					$filterAttrValue = explode("|", $each->FilterText);
					$filterAttrIds = array_flip( explode("|", $each->FilterTextAttr) );

					$lastClickValue = '';

					foreach ($filterAttrIds as $key => $value) {
						if( isset( $_REQUEST['filter'][$key] ) ) {
							foreach ($_REQUEST['filter'][$key] as $eachValue) {
								if( $filterAttrValue[$value] != $eachValue ) {
									if( $key == @$_REQUEST['lastClick'] ) {
										if( !isset( $res['filter'][ $key ]['elements'][ $filterAttrValue[$value] ] ) )
											$res['filter'][ $key ]['elements'][ $filterAttrValue[$value] ] = [ 'id' => $filterAttrValue[$value], 'num' => 1 ];
										else
											$res['filter'][ $key ]['elements'][ $filterAttrValue[$value] ]['num'] += 1;
									}
									continue 3;
								}
							}
						}
					}

					$res['collection'][] = [ 
						'AgendaSchedName' => $each->AgendaSchedName
						, 'UserName' => $each->UserName
						, 'CheckpointName' => $each->CheckPointName
						, 'FilterText' => $each->FilterText
						, 'AgendaSchedID' => $each->AgendaSchedID
						, 'canRemove' => $each->canRemove( $aUser )
						, 'canEdit' => $each->canEdit( $aUser )
						, 'CaptureStartTime_Hours' => $each->CaptureStartTime_Hours
						, 'CaptureStartTime_Minutes' => $each->CaptureStartTime_Minutes
						, 'GenerateAgendasNextDayTime_Hours' => $each->GenerateAgendasNextDayTime_Hours
						, 'GenerateAgendasNextDayTime_Minutes' => $each->GenerateAgendasNextDayTime_Minutes
					];
					
					foreach ($defFilters as $eachFilter)
					{
						if( !isset( $res['filter'][ $eachFilter['key'] ] ) )
						{
							$res['filter'][ $eachFilter['key'] ] = [
								'description' => $eachFilter['description'],
								'elements' => []
							];
						}

						if( trim($each->$eachFilter['propDesc']) != '' )
						{
							if( !isset( $res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ] ) )
								$res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ] = [ 'id' => $each->$eachFilter['propDesc'], 'num' => 1 ];
							else
								$res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ]['num'] += 1;
						}
					}

					if( @$_REQUEST['filter'] && @$_REQUEST['filter']['CheckPointID'] ) {
						foreach ($arrAttr->Collection as $key => $eachAttr) {
							$keyAttrFilter = $eachAttr->MemberID;
							$valueAttrFilter = $filterAttrValue[ $filterAttrIds[ $eachAttr->MemberID ] ];
						
							if( !isset( $res['filter'][ $eachAttr->MemberName ]['description'] ) ) {
								$res['filter'][ $keyAttrFilter ]['description'] = $eachAttr->MemberName;

								if( !isset( $res['filter'][ $keyAttrFilter ]['elements'] ) ) $res['filter'][ $keyAttrFilter ]['elements'] = [];
							}

							if( $valueAttrFilter  != '' ) {
								if( !isset( $res['filter'][ $keyAttrFilter ]['elements'][ $valueAttrFilter ] ) )
									$res['filter'][ $keyAttrFilter ]['elements'][ $valueAttrFilter ] = [ 'id' => $valueAttrFilter, 'num' => 1 ];
								else
									$res['filter'][ $keyAttrFilter ]['elements'][ $valueAttrFilter ]['num'] += 1;
							}
						}
					}
				}

			} else if ( $action == "changeUser")
			{
				$arrSelected = getParamValue('selected', 'both', '(string)', true);

				if( is_string( $arrSelected ) )
				{
					$arrSelected = explode(',', $arrSelected);
				}

				$aCollection = BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, $arrSelected);

				foreach ($aCollection->Collection as $each)
				{
					$each->UserID = getParamValue('id', 'both', '(int)', true);

					$each->save( true );

					$res['collection'][] = [ 
						'AgendaSchedName' => $each->AgendaSchedName
						, 'UserName' => $each->UserName
						, 'CheckpointName' => $each->CheckPointName
						, 'FilterText' => $each->FilterText
						, 'AgendaSchedID' => $each->AgendaSchedID
						, 'canRemove' => $each->canRemove( $aUser )
						, 'canEdit' => $each->canEdit( $aUser )
					];
				}
			} else if( $action == "save" )
			{

				$arrSelected = getParamValue('id', 'both', '(string)', true);

				if( trim( $arrSelected ) == '' ) $arrSelected = [];
				else $arrSelected = explode(',', $arrSelected);				

				$dataUpdate = array_merge($aHTTPRequest->GET, $aHTTPRequest->POST);

				if( is_string( $dataUpdate['SurveyIDs'] ) ) $dataUpdate['SurveyIDs'] = [$dataUpdate['SurveyIDs']];

				if( !count( $arrSelected ) )
				{
					$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);

					$anInstance->updateFromArray( $dataUpdate );
				
					$anInstance->save();

					$res['collection'][] = [ 
						'AgendaSchedName' => $anInstance->AgendaSchedName
						, 'UserName' => $anInstance->UserName
						, 'CheckpointName' => $anInstance->CheckPointName
						, 'FilterText' => $anInstance->FilterText
						, 'AgendaSchedID' => $anInstance->AgendaSchedID
						, 'canRemove' => $anInstance->canRemove( $aUser )
						, 'canEdit' => $anInstance->canEdit( $aUser )
					];

				} else 
				{
					$aCollection = BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, $arrSelected);

					foreach ($aCollection->Collection as $each)
					{
						$each->updateFromArray( $dataUpdate );

						$each->save();

						$res['collection'][] = [ 
							'AgendaSchedName' => $each->AgendaSchedName
							, 'UserName' => $each->UserName
							, 'CheckpointName' => $each->CheckPointName
							, 'FilterText' => $each->FilterText
							, 'AgendaSchedID' => $each->AgendaSchedID
							, 'canRemove' => $each->canRemove( $aUser )
							, 'canEdit' => $each->canEdit( $aUser )
						];
					}
				
				}
			} else if( $action == 'edit' )
			{
				$anAgendaID = getParamValue('selected', 'both', '(int)', true);
				
				$anInstance = BITAMSurveyAgendaScheduler::NewInstanceWithID( $aRepository, $anAgendaID );
				
				$res = $anInstance->getJSonDefinition();
				
				//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
				$res = array_merge( $res, [
					"AgendaSchedName" => $anInstance->AgendaSchedName
					,"UserID" => $anInstance->UserID
					,"CheckPointID" => $anInstance->CheckPointID
					,'SurveyIDs' => $anInstance->SurveyIDs
					,'FilterText' => explode('|', $anInstance->FilterText)
					,'FilterTextAttr' => explode('|', $anInstance->FilterTextAttr)
					,'CaptureStartTime_Hours' => ( $anInstance->CaptureStartTime_Hours < 10 ? '0' . $anInstance->CaptureStartTime_Hours : $anInstance->CaptureStartTime_Hours )
					,'CaptureStartTime_Minutes' => ( $anInstance->CaptureStartTime_Minutes < 10 ? '0' . $anInstance->CaptureStartTime_Minutes : $anInstance->CaptureStartTime_Minutes )
					,'GenerateAgendasNextDayTime_Hours' => ( $anInstance->GenerateAgendasNextDayTime_Hours < 10 ? '0' . $anInstance->GenerateAgendasNextDayTime_Hours : $anInstance->GenerateAgendasNextDayTime_Hours )
					,'GenerateAgendasNextDayTime_Minutes' => ( $anInstance->GenerateAgendasNextDayTime_Minutes < 10 ? '0' . $anInstance->GenerateAgendasNextDayTime_Minutes : $anInstance->GenerateAgendasNextDayTime_Minutes )
					,'RecurPatternType' => $anInstance->RecurPatternType
					, 'canRemove' => $anInstance->canRemove( $aUser )
					, 'canEdit' => $anInstance->canEdit( $aUser )
					, 'FirstDayOfWeek' => $anInstance->FirstDayOfWeek
				]);
				
				foreach ($res['FilterTextAttr'] as $key => $value)
				{
					$res['FilterTextValue'][ $value ] = $res['FilterText'][$key];
				}
				
				//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
				//Agrega la versión de la metadata para poder condicionar los campos a mostrar en el Wizard
				$res['mdVersion'] = getMDVersion();
				$res['versions'] = array();
				$res['versions']['esvAgendaFirsDayWeek'] = esvAgendaFirsDayWeek;
				//@JAPR
			}

		}
		
		header("Content-Type:application/json; charset=UTF-8");
		
		echo json_encode( $res );
		
		exit();
	}
}

?>