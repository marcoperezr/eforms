<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalogmember.inc.php");
require_once("catalogfilter.inc.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("dataSource.inc.php");
require_once("dataSourceMember.inc.php");

//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
require_once("customClasses.inc.php");
//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
require_once("eBavelCatalog.inc.php");
require_once("object.trait.php");

class BITAMCatalog extends BITAMObject
{
	public $ParentID;
	public $CatalogID;
	public $CatalogName;
	public $TableName;
	public $ModelID;
	//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	public $VersionFieldName;		//Nombre del campo en la tabla del catálogo que corresponde al indicador con la versión de cada registro
	public $MaxVersionNum;			//Máximo número de versión grabado en el campo Versión de la tabla de catálogo (se debe incrementar manualmente una vez se actualice el catálogo, o bien volver a invocar a la función getCatalogVersion)
	public $VersionIndID;			//ID del indicador que contiene la versión de cada registro del catálogo
	//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo
	public $VersionNum;				//Número de versión del catálogo, ya sea porque se editó su estructura o se cambiaron sus datos (originalmente se tenía que pedir la versión máxima de los registros pero resultó muy lento)
	//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
	public $DataVersionDate;			//Fecha de última modificación de los datos del catálogo de tipo eBavel o cualquiera externo a eForms donde no se tiene control de cuándo cambian los datos. Corresponde a un campo DateTime (YYYY-MM-DD HH:MM:SS)
	public $DataVersionNum;				//Número de versión del catálogo (es lo mismo que DataVersionDate pero ajustado a valor numérico en formato YYYYMMDDHHMMSS)
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Esto NO será una opción configurable, sólo nuevos catálogos y nuevas encuestas nacerán con esta opción activada, solamente los catálogos preexistentes se actualizarán al ser usados por una encuesta nueva
	public $DissociateCatDimens;	//Indica si se manejarán dimensiones separadas para los atributos de los catálogos en esta encuesta
	public $HierarchyID;			//Indica el ID de la jerarquía de Artus que contendrá a las dimensiones del catálogo, de tal forma que se pueda reconstruir cuando se modifique el catálogo (esta jerarquía no debería ser modificada por ningún otro producto pues se regenera automáticamente)
	//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
	public $eBavelAppID;			//Id de la aplicación de eBavel asociada a este catálogo
    public $eBavelFormType; //@AAL 19/03/2015: Agregado para definir si se utilizará una forma (1), una vista (2) de eBavel o Ninguno (0)
	public $eBavelFormID;			//Id de la forma de eBavel asociado a este catálogo
	//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	public $DataSourceID;				//ID del DataSource (catálogo de v6) al que se asocia este catálogo
	//@JAPR
	//OMMC 2019-03-25: Pregunta AR #4HNJD9
	public $IsModel;					//Agregado para saber si el catálogo es un modelo para pregunta AR
	//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
	public $CatModelMemberID;			//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa el nombre del modelo en el catálogo
	public $CatPlatformMemberID;		//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la plataforma para la que aplica el modelo en el catálogo
	public $CatVersionMemberID;			//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la versión del modelo en el catálogo
	public $CatZipSourceMemberID;		//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la ruta para descarga con el modelo en formato .zip en el catálogo
	public $CatTitleMemberID; 			//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa el nombre lógico que se le presenta al usuario en el catálogo
	public $DataSourceModelMemberID;	//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa el nombre del modelo en el DataSource
	public $DataSourcePlatformMemberID;	//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la plataforma para la que aplica el modelo en el DataSource
	public $DataSourceVersionMemberID;	//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la versión del modelo en el DataSource
	public $DataSourceZipSourceMemberID;//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa la ruta para descarga con el modelo en formato .zip en el DataSource
	public $DataSourceTitleMemberID; 	//Atributo exclusivo para que los catálogos marcados como Modelo regresen al App y facilitar la descarga de los mismos. Representa el nombre lógico que se le presenta al usuario en el DataSource
	
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->CatalogID = -1;
		$this->CatalogName= "";
		$this->TableName = "";
		$this->ModelID = -1;
		$this->ParentID = -1;
		//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
		$this->VersionFieldName = '';
		$this->MaxVersionNum = -1;
		$this->VersionIndID = 0;
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo
		$this->VersionNum = 0;
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		$this->DataVersionDate = null;
		$this->DataVersionNum = -1;
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->DissociateCatDimens = 1;
		$this->HierarchyID = 0;
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$this->eBavelAppID = 0;
        //@AAL 19/03/2015: Agregado para definir si se utilizará una forma o una vista de eBavel o ninguna
        $this->eBavelFormType = ebftForms;
		$this->eBavelFormID = 0;
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
		$this->DataSourceID = 0;
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		$this->IsModel = 0;
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		$this->CatModelMemberID = 0;
		$this->CatPlatformMemberID = 0;
		$this->CatVersionMemberID = 0;
		$this->CatZipSourceMemberID = 0;
		$this->CatTitleMemberID = 0;
		$this->DataSourceModelMemberID = 0;
		$this->DataSourcePlatformMemberID = 0;
		$this->DataSourceVersionMemberID = 0;
		$this->DataSourceZipSourceMemberID = 0;
		$this->DataSourceTitleMemberID = 0;
	}
	
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMCatalogClass($aRepository, 0, $strCalledClass);
		return eval('return new '.$aClassName.'($aRepository);');
		//return new BITAMCatalog($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aCatalogID)
	{
		$anInstance = null;
		
		if (((int)  $aCatalogID) < 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetCatalogInstanceWithID($aCatalogID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', A.VersionNum ';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', A.DissociateCatDimens, A.HierarchyID';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', A.eBavelAppID, A.eBavelFormID';
		}
		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$strAdditionalFields .= ', A.eBavelFormType';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.DataSourceID, A.CreationVersion, A.LastVersion';
		}
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if (getMDVersion() >= esvDataSourceDataVersion) {
			$strAdditionalFields .= ', A.DataVersionDate';
		}
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		//Agregado el JOIN con las preguntas para obtener la referencia a los atributos marcados en cada una y dejarlos asociados directamente al catálogo
		$strAdditionalJoins = '';
		if (getMDVersion() >= esvQuestionAR) {
			$strAdditionalFields .= ", B.IsModel, C.CatMemberID AS QCatMemberID, C.DataSourceMemberID AS QDataSourceMemberID,
				C.CatMemberVersionID AS QCatMemberVersionID, C.DataMemberVersionID AS QDataMemberVersionID,
				C.CatMemberZipFileID AS QCatMemberZipFileID, C.DataMemberZipFileID AS QDataMemberZipFileID,
				C.CatMemberPlatformID AS QCatMemberPlatformID, C.DataMemberPlatformID AS QDataMemberPlatformID, 
				C.CatMemberTitleID AS QCatMemberTitleID, C.DataMemberTitleID AS QDataMemberTitleID";
			$strAdditionalJoins .= "LEFT JOIN SI_SV_Question C ON A.CatalogID = C.CatalogID ";
		}
		
		//JAPR 2018-09-10: Agregado el soporte de catálogos offline (#IAJE6Q)
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		$sql = "SELECT A.ModelID, A.ParentID, A.CatalogID, A.CatalogName, B.TableName, A.VersionIndID $strAdditionalFields 
			FROM SI_SV_Catalog A 
				INNER JOIN SI_SV_DataSource B ON A.DataSourceID = B.DataSourceID {$strAdditionalJoins}
			WHERE A.CatalogID = ".((int) $aCatalogID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF) {
			//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
	//Obtiene la instancia del catálogo a partir de la dimensión base del mismo. Sólo debe haber una dimensión base por catálogo
	static function NewInstanceWithParentID($aRepository, $aDimensionClaDescripID)
	{
		$anInstance = null;
		
		if (((int) $aDimensionClaDescripID) < 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetCatalogInstanceWithParentID($aDimensionClaDescripID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', A.VersionNum ';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', A.DissociateCatDimens, A.HierarchyID';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', A.eBavelAppID, A.eBavelFormID';
		}
		//@JAPR
		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$strAdditionalFields .= ', A.eBavelFormType';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.DataSourceID, A.CreationVersion, A.LastVersion';
		}
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if (getMDVersion() >= esvDataSourceDataVersion) {
			$strAdditionalFields .= ', A.DataVersionDate';
		}
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		//Agregado el JOIN con las preguntas para obtener la referencia a los atributos marcados en cada una y dejarlos asociados directamente al catálogo
		$strAdditionalJoins = '';
		if (getMDVersion() >= esvQuestionAR) {
			$strAdditionalFields .= ", B.IsModel, C.CatMemberID AS QCatMemberID, C.DataSourceMemberID AS QDataSourceMemberID,
				C.CatMemberVersionID AS QCatMemberVersionID, C.DataMemberVersionID AS QDataMemberVersionID,
				C.CatMemberZipFileID AS QCatMemberZipFileID, C.DataMemberZipFileID AS QDataMemberZipFileID,
				C.CatMemberPlatformID AS QCatMemberPlatformID, C.DataMemberPlatformID AS QDataMemberPlatformID,
				C.CatMemberTitleID AS QCatMemberTitleID, C.DataMemberTitleID AS QDataMemberTitleID";
			$strAdditionalJoins .= "LEFT JOIN SI_SV_Question C ON A.CatalogID = C.CatalogID ";
		}

		//JAPR 2018-09-10: Agregado el soporte de catálogos offline (#IAJE6Q)
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		$sql = "SELECT A.ModelID, A.ParentID, A.CatalogID, A.CatalogName, B.TableName, A.VersionIndID $strAdditionalFields 
			FROM SI_SV_Catalog A 
				INNER JOIN SI_SV_DataSource B ON A.DataSourceID = B.DataSourceID {$strAdditionalJoins}
			WHERE A.ParentID = ".((int) $aDimensionClaDescripID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF) {
			//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	//@JAPR
	
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$inteBavelAppID = 0;
		$inteBavelFormID = 0;
        $eBavelFormType = ebftForms;
		if (getMDVersion() >= esveBavelCatalogs) {
			$inteBavelAppID = (int) @$aRS->fields["ebavelappid"];
			$inteBavelFormID = (int) @$aRS->fields["ebavelformid"];
		}

		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$eBavelFormType = (int) @$aRS->fields["ebavelformtype"];
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCatalogClass = GetBITAMCatalogClass($aRepository, $inteBavelFormID, $strCalledClass);
		//Instancía la clase dinámicamente según si se trata o no de un catálogo de eBavel
		eval("\$anInstance = {$strCatalogClass}::NewInstance(\$aRepository);");
		//$anInstance = BITAMCatalog::NewInstance($aRepository);
		//@JAPR
		$anInstance->updateFromRS($aRS);
		
		return $anInstance;
	}
	
	//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
	/* Dada una definición de datasource existente ($aDataSourceID) y una lista ordenada de atributos a utilizar ($aDataSourceMemberIDsColl), con la posibilidad de especificar un catálogo
	existente para su actualización ($aCatalogID), determina si el catálogo existía o no, si no existía entonces crea una nueva instancia basado en el datasource proporcionado, incluyendo
	como primeros atributos los de la lista de datasourcemembers indicada, pero incluyendo también el resto de datasourcemembers al final en forma desordenada, en caso de ya haber existido
	el catálogo, procederá a actualizar su definición basado en el datasource, cambiando los atributos existentes de posición para reflejar el nuevo orden indicado, agregando aquellos
	atributos que requiera y eliminando los que ya no existan (elimina los que no existan y que no hubieran sido reutilizados como algún atributo nuevo)
	Para que este método funcione, los catálogos indicados deberán ser catálogos de la nueva versión v6 o posteriores, los cuales están preparados para integrarse con datasources en el
	sentido que no crean un modelo de Artus ni dependen de él o de eBavel para ser considerados válidos/obtener sus datos, simplemente son un cascarón para soportar el funcionamiento original
	del App de eForms, y lo único que harán será pedir los datos directamente a los datasources, tal como las formas de la nueva versión, los catálogos configurados de esta manera se
	identificarán además de por el dataSourceID, por un CreationAdminVersion (campo CreationVersion) >= esveFormsv6
	Si se debe crear un catálogo y un atributo, se regresarán como un array de resultado para que sean actualizados por quien invocó al método, un false significaría un error
	El parámetro $arrMapIDs sirve para indicar una serie de valores de DataSourceMemberIDs que se deben mapear a su correspondiente CatMemberID, utilizado para permitir en este proceso
	mapear atributos adicionales como los de Latitude y Longitude. El parámetro es un Array donde el índice es cualquier cosa que quien invoca requiere (originalmente el nombre de propiedad
	del objeto que invoca) y cada valor es un array de Pares "DataSourceMemberID, CatMemberID" donde se indica el mapeo
	*/
	static function CreateUpdateCatalogFromDataSource($aRepository, $aDataSourceID, $aDataSourceMemberIDsColl = null, $aCatalogID = null, $arrMapIDs = null) {
		$blnReturn = false;
		$intLastCatMemberID = 0;
		$intLastDataSourceMemberID = 0;
		$intDataMemberLatitudeID = 0;
		$intDataMemberLongitudeID = 0;
		//Configura los mapeos de atributos adicionales
		if (is_null($arrMapIDs) || !is_array($arrMapIDs)) {
			$arrMapIDs = array();
		}
		
		//Esta variable indicará si se está cambiando o no el DataSource asociado al catálogo, si lo hace entonces debe remapear los atributos existentes a los DataSourceMembers nuevos
		//además de cambiar el apuntador del catálogo hacia el nuevo DataSource
		$blnChangeDataSource = false;
		$arrMapMembersByDataID = array();
		foreach ($arrMapIDs as $strIndex => $arrPairs) {
			$intDataSourceMemberID = 0;
			if (!is_null($arrPairs) && is_array($arrPairs)) {
				$intDataSourceMemberID = (int) @$arrPairs['DataSourceMemberID'];
				if ($intDataSourceMemberID > 0) {
					if (!isset($arrMapMembersByDataID[$intDataSourceMemberID])) {
						$arrMapMembersByDataID[$intDataSourceMemberID] = array();
					}
					
					//Modificado para que sea un array en caso de que se estuvieran mapeando por error varias configuraciones al mismo DataSourceMember, probablemente por cambios
					//en los atributos usados que temporalmente permiten repetirlo
					$intIndex = count($arrMapMembersByDataID[$intDataSourceMemberID]);
					$arrMapMembersByDataID[$intDataSourceMemberID][$intIndex] = array();
					$arrMapMembersByDataID[$intDataSourceMemberID][$intIndex]['Index'] = $strIndex;
					$arrMapMembersByDataID[$intDataSourceMemberID][$intIndex]['MemberID'] = 0;
				}
			}
		}
		
		try {
			$aDataSourceID = (int) $aDataSourceID;
			$aCatalogID = (int) $aCatalogID;
			if ($aDataSourceID <= 0) {
				return $blnReturn;
			}
			$objDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $aDataSourceID);
			if (is_null($objDataSource)) {
				return $blnReturn;
			}
			$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aDataSourceID);
			if (is_null($objDataSourceMembersColl)) {
				return $blnReturn;
			}
			
			//Si no se recibió la lista de DataSourceMembers, se asume que se requieren todos en el mismo orden que están definidos en el DataSource
			$blnAddDataSourceMember = false;
			if (is_null($aDataSourceMemberIDsColl) || !is_array($aDataSourceMemberIDsColl) || count($aDataSourceMemberIDsColl) == 0) {
				$aDataSourceMemberIDsColl = array();
				$blnAddDataSourceMember = true;
			}
			else {
				//@JAPR 2015-07-28: Corregido un bug, el último atributo en este punto no aplica correctamente si la pregunta no era tipo menu y por tanto no se recibió el parámetro
				//con la lista de atributos, se movió el código al punto inicial donde se procesa este parámetro
				//En este caso si se recibió la lista, debe ser una pregunta tipo Menú y si es posible utilizar el último atributo de dicha lista
				//$intLastDataSourceMemberID = (int) @$aDataSourceMemberIDsColl[count($aDataSourceMemberIDsColl) -1];
			}
			
			$arrDataSourceMembersByID = array();
			foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
				$arrDataSourceMembersByID[$objDataSourceMember->MemberID] = $objDataSourceMember;
				if ($blnAddDataSourceMember) {
					$aDataSourceMemberIDsColl[] = $objDataSourceMember->MemberID;
					if ($intLastDataSourceMemberID <= 0) {
						//@JAPR 2015-07-28: Corregido un bug, el último atributo en este punto no aplica correctamente si la pregunta no era tipo menu y por tanto no se recibió el parámetro
						//con la lista de atributos, se movió el código al punto inicial donde se procesa este parámetro
						//En este caso si no se recibió la lista, debe ser una pregunta que no es Menú y se debe usar el primer atributo como el propio de la pregunta
						//$intLastDataSourceMemberID = $objDataSourceMember->MemberID;
					}
				}
			}
			
			$arrDataSourceMemberIDs = array_flip($aDataSourceMemberIDsColl);
			$arrOrderedDataSourceMemberIDs = array();
			$intOrder = 1;
			foreach ($aDataSourceMemberIDsColl as $intDataSourceMemberID) {
				if (!isset($arrOrderedDataSourceMemberIDs[$intDataSourceMemberID])) {
					$arrOrderedDataSourceMemberIDs[$intDataSourceMemberID] = $intOrder++;
				}
			}
			
			//Obtiene la referencia al catálogo a actualizar si es que se especificó alguno
			$objCatalog = null;
			$blnNewCatalog = ($aCatalogID <= 0);
			if ($aCatalogID > 0) {
				$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
				if (is_null($objCatalog)) {
					return $blnReturn;
				}
				
				//Verifica si el DataSource ha cambiado para preparar el remapeo de Atributos, sicambió también debe actualizar el catálogo cargado
				$blnChangeDataSource = ($objCatalog->DataSourceID != $aDataSourceID);
				if ($blnChangeDataSource) {
					$objCatalog = $objDataSource->createUpdateCatalog(0, $objCatalog);
				}
			}
			else {
				//En este caso tiene que crear el catálogo replicando los atributos en el orden solicitado, como será un catálogo nuevo, simplemente no se llenan los arrays que contienen
				//las referencias a sus atributos para que el proceso de creación/actualización de los mismos determine que simplemente los debe agregar
				$objCatalog = $objDataSource->createUpdateCatalog();
				if (is_null($objCatalog)) {
					return $blnReturn;
				}
				
				//En este punto ya se generó el catálogo vacio, así que ahora se puede hacer referencia a él
				$aCatalogID = $objCatalog->CatalogID;
			}
			//Actualiza el catálogo que acaba de ser creado (si ya existía, al salir de la función no será actualizado en quien invocara a este proceso)
			$blnReturn = array();
			$blnReturn["CatalogID"] = $aCatalogID;
			
			//Obtiene la referencia de los atributos del catálogo indicado para identificar cuales debe agregar, actualizar o eliminar
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
			if (is_null($objCatalogMembersColl)) {
				return $blnReturn;
			}
			
			$arrAllCatMembersIDs = array();
			$arrUsedCatMembersIDs = array();
			$arrUsedDataSourceMembersIDs = array();
			$arrMappedCatMembersIDs = array();
			$arrAvailableCatMembersColl = array();
			
			//********************************************************************************************************************************************************
			//Inicia el proceso para generar los atributos faltantes o reordenar los que sean necesarios
			//Primero recorre todos los DataSourceMembers existentes para agregarlos al array de $arrOrderedDataSourceMemberIDs si es que no estaban, ya que en base a ese se realizará
			//el proceso de creación de atributos, así que se optimiza el proceso unificando tanto los ordenados como los desordenados
			$intOrder = count($arrOrderedDataSourceMemberIDs);
			if ($intOrder > 0) {
				//Asigna el último DataSourceMember solicitado porque ese se usará para obtener el CatMemberID del atributo correspondiente si es que se debe crear
				$arrOrderedDataSourceMemberIDsFlip = array_flip($arrOrderedDataSourceMemberIDs);
				$arrOrderedDataSourceMemberIDsFlip = array_values($arrOrderedDataSourceMemberIDsFlip);
				//@JAPR 2015-07-28: Corregido un bug, el último atributo en este punto no aplica correctamente si la pregunta no era tipo menu y por tanto no se recibió el parámetro
				//con la lista de atributos, se movió el código al punto inicial donde se procesa este parámetro
				//$intLastDataSourceMemberID = (int) @$arrOrderedDataSourceMemberIDsFlip[$intOrder -1];
			}
			foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
				$intDataSourceMemberID = $objDataSourceMember->MemberID;
				//DataSourceMembers ya procesados son ignorados
				if (isset($arrOrderedDataSourceMemberIDs[$intDataSourceMemberID])) {
					continue;
				}
				
				$arrOrderedDataSourceMemberIDs[$intDataSourceMemberID] = $intOrder++;
			}
			
			//Basado en el array total de DataSourceMembers del nuevo DataSource (si es que estuviera cambiando), determina cuales de los atributos del catálogo actual (si es que hay uno)
			//No tiene un mapeo para considerarlos como disponibles, ya que de otra forma se hubieran eliminado. Al ser disponibles serán reutilizados para generar los atributos del nuevo
			//DataSource
			//Si se estuviera cambiando el DataSource ($blnChangeDataSource) entonces este array nunca hará match con los DataSourceMemberIDs del nuevo DataSource, por lo que se considerarán
			//nuevos pero se deben reutilizar, la colección ya viene ordenada por posición, así que se puede tomar cada atributo previo en esa secuencia
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				$arrAllCatMembersIDs[$objCatalogMember->MemberID] = $objCatalogMember->MemberID;
				if ($objCatalogMember->DataSourceMemberID > 0) {
					$arrMappedCatMembersIDs[$objCatalogMember->DataSourceMemberID] = $objCatalogMember;
				}
				
				//Si este atributo no estuviera mapeado (lo cual sería un error) o su mapeo no aplicara con el nuevo DataSource, entonces lo marca como disponibles siempre y cuando se
				//esté cambiando el DataSource ($blnChangeDataSource), aunque si  no se estuviera cambiando, no debería entrar a esta condición de todas maneras
				if ($blnChangeDataSource && ($objCatalogMember->DataSourceMemberID <= 0 || !isset($arrOrderedDataSourceMemberIDs[$objCatalogMember->DataSourceMemberID]))) {
					$arrAvailableCatMembersColl[] = $objCatalogMember;
				}
			}
			
			//Recorre todos los DataSourceMembers ordenados que se recibieron, agregará los faltantes y reordenará los existentes según la posición indicada
			$intMaxOrder = 1;
			//$arrMapKeys = array_keys($arrMappedCatMembersIDs);
			//@JAPR 2015-10-13: Corregido un bug, al cambiar la configuración de la pregunta no estaba actualizando la versión del catálogo aunque si cambiaba la secuencia de
			//sus atributos, por tanto al refrescar definiciones no hubiera tomado el cambio (#YGVVQT)
			$blnUpdateCatalogVersion = false;
			//@JAPR
			foreach ($arrOrderedDataSourceMemberIDs as $intDataSourceMemberID => $intOrder) {
				//Verifica si el DataSourceMember ya existe como atributo
				$objCatalogMember = @$arrMappedCatMembersIDs[$intDataSourceMemberID];
				if (is_null($objCatalogMember)) {
					//Si no existiera este atributo, entonces lo tiene que crear a partir del DataSourceMember indicado, pero antes de crearlo, verifica si se está cambiando el DataSource
					//y si aún existe algún atributo del viejo catálogo que se pueda reutilizar
					$objDataSourceMember = @$arrDataSourceMembersByID[$intDataSourceMemberID];
					if (is_null($objDataSourceMember)) {
						continue;
					}
					
					if ($blnChangeDataSource) {
						if (count($arrAvailableCatMembersColl) > 0) {
							$objCatalogMember = array_shift($arrAvailableCatMembersColl);
						}
					}
					
					//Como se está creando, ya se asigna el orden adecuado, así que no requerirá un UPDATE posterior, si $objCatalogMember se hubiera asignado, se actualizaría en
					//lugar de crear un nuevo atributo
					$objCatalogMember = $objDataSourceMember->createUpdateCatalogMember($aCatalogID, 0, $objCatalogMember, $intMaxOrder);
					if (is_null($objCatalogMember)) {
						continue;
					}
					
					//@JAPR 2015-10-13: Corregido un bug, al cambiar la configuración de la pregunta no estaba actualizando la versión del catálogo aunque si cambiaba la secuencia de
					//sus atributos, por tanto al refrescar definiciones no hubiera tomado el cambio (#YGVVQT)
					$blnUpdateCatalogVersion = true;
					//@JAPR
				}
				else {
					//En este caso simplemente verifica si es necesario actualizar el atributo comparando su orden
					//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
					//Verifica si el dato del atributo de eBavel es diferente, ya que si es así, debe actualizarlo para que sean consistentes ambas definiciones de catálogo
					$blnDifferentData = false;
					$objDataSourceMember = @$arrDataSourceMembersByID[$intDataSourceMemberID];
					if (!is_null($objDataSourceMember)) {
						//@JAPR 2015-12-15: Corregido un bug, al crear el Catalog a partir del DataSource, no estaba heredando la propiedad de si era o no atributo imagen (#SS2EKF)
						if ($objCatalogMember->eBavelFieldID != $objDataSourceMember->eBavelFieldID || $objCatalogMember->eBavelFieldDataID != $objDataSourceMember->eBavelFieldDataID || $objCatalogMember->IsImage != $objDataSourceMember->IsImage) {
							$blnDifferentData = true;
						}
					}
					
					if ($objCatalogMember->MemberOrder != $intMaxOrder || $blnDifferentData) {
						//@JAPR 2019-06-18: Corregido un bug, esta asignación se estaba haciendo después de haber igualado el orden del atributo, así que debido al fix del issue indicado
						//abajo que agregó el IF, ya no se estaba considerando modificado el catálogo, se moverá justo antes de la igualación del orden del atributo (#W2AL5H)
						//@JAPR 2015-10-13: Corregido un bug, al cambiar la configuración de la pregunta no estaba actualizando la versión del catálogo aunque si cambiaba la secuencia de
						//sus atributos, por tanto al refrescar definiciones no hubiera tomado el cambio (#YGVVQT)
						//Esta asignación sólo es necesaria si realmente está cambiando el orden del atributo, o algún dato reelevante, pero no por cambios al mapeo de eBavel
						if ($objCatalogMember->MemberOrder != $intMaxOrder) {
							$blnUpdateCatalogVersion = true;
						}
						//@JAPR
						
						$objCatalogMember->MemberOrder = $intMaxOrder;
						//@JAPR 2015-12-07: Corregido un bug, al crear el Catálogo a partir de un DataSource, no estaba asignando el mapeo hacia eBavel (#ASIPRG)
						if (!is_null($objDataSourceMember)) {
							$objCatalogMember->eBavelFieldID = $objDataSourceMember->eBavelFieldID;
							$objCatalogMember->eBavelFieldDataID = $objDataSourceMember->eBavelFieldDataID;
							//@JAPR 2015-12-15: Corregido un bug, al crear el Catalog a partir del DataSource, no estaba heredando la propiedad de si era o no atributo imagen (#SS2EKF)
							$objCatalogMember->IsImage = $objDataSourceMember->IsImage;
						}
						
						$objCatalogMember->save();
					}
				}
				
				//Si se trata del DataSourceMember seleccionado como último en el orden, actualiza el array de respuesta con el ID de atributo correspondiente
				if ($intDataSourceMemberID == $intLastDataSourceMemberID) {
					//$blnReturn["CatMemberID"] = $objCatalogMember->MemberID;
				}
				
				//Asigna el valor del mapeo hacia el CatMemberID correspondiente
				if (isset($arrMapMembersByDataID[$intDataSourceMemberID])) {
					//Modificado para que sea un array en caso de que se estuvieran mapeando por error varias configuraciones al mismo DataSourceMember, probablemente por cambios
					//en los atributos usados que temporalmente permiten repetirlo
					foreach ($arrMapMembersByDataID[$intDataSourceMemberID] as $intIndex => $arrPairs) {
						$arrMapMembersByDataID[$intDataSourceMemberID][$intIndex]['MemberID'] = $objCatalogMember->MemberID;
					}
				}
				
				//Se marcan el atributo y datasourcemember como usados para no removerlo y no duplicarlos
				$arrUsedCatMembersIDs[$objCatalogMember->MemberID] = 1;
				$arrUsedDataSourceMembersIDs[$intDataSourceMemberID] = 1;
				$intMaxOrder++;
			}
			
			//********************************************************************************************************************************************************
			//Finalmente remueve los atributos no usados del catálogo
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				$intCatMemberID = $objCatalogMember->MemberID;
				if (!isset($arrUsedCatMembersIDs[$intCatMemberID])) {
					$objCatalogMember->remove();
					//@JAPR 2015-10-13: Corregido un bug, al cambiar la configuración de la pregunta no estaba actualizando la versión del catálogo aunque si cambiaba la secuencia de
					//sus atributos, por tanto al refrescar definiciones no hubiera tomado el cambio (#YGVVQT)
					$blnUpdateCatalogVersion = true;
					//@JAPR
				}
			}
			
			//Devuelve los valores mapeados en caso de haber existido alguno
			//Modificado para que sea un array en caso de que se estuvieran mapeando por error varias configuraciones al mismo DataSourceMember, probablemente por cambios
			//en los atributos usados que temporalmente permiten repetirlo
			foreach ($arrMapMembersByDataID as $intDataSourceMemberID => $arrPairs) {
				foreach ($arrPairs as $intIndex => $arrPair) {
					$strIndex = (string) @$arrPair['Index'];
					$intCatMemberID = (int) @$arrPair['MemberID'];
					if ($intCatMemberID > 0 && $strIndex != '') {
						$blnReturn[$strIndex] = $intCatMemberID;
					}
				}
			}
			
			//@JAPR 2015-10-13: Corregido un bug, al cambiar la configuración de la pregunta no estaba actualizando la versión del catálogo aunque si cambiaba la secuencia de
			//sus atributos, por tanto al refrescar definiciones no hubiera tomado el cambio (#YGVVQT)
			//Si el catálogo cambió, debe actualizar su versión para que se refresquen las definiciones en los dispositivos
			if ($blnUpdateCatalogVersion) {
				BITAMCatalog::updateLastUserAndDateInCatalog($aRepository, $aCatalogID);
			}
			//@JAPR
			
			//$blnReturn = true;
		} catch (Exception $e) {
			die ("(".__METHOD__.") ".translate("Error").": ".$e->getMessage());
		}
		
		return $blnReturn;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (array_key_exists("CatalogID", $aHTTPRequest->POST))
		{
			$aCatalogID = $aHTTPRequest->POST["CatalogID"];
			
			if (is_array($aCatalogID))
			{
				
				$aCollection = BITAMCatalogCollection::NewInstance($aRepository, $aCatalogID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				if ($aCatalogID > 0) {
				$strCalledClass = static::class;
				//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
				$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, $aCatalogID);
				$anInstance->generateDesignWindow();
				die();
				}
			}
			return parent::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);
		}
		$anInstance = null;
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = $aHTTPRequest->GET["CatalogID"];

			$anInstance = $strCalledClass::NewInstanceWithID($aRepository,(int) $aCatalogID);
			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
			
			//@JAPR 2013-05-27: Agregado el proceso de exportación de catálogos
			$blnExportTemplate = getParamValue('ExportXLS', 'both', '(int)');
			if ($blnExportTemplate) {
				@$anInstance->exportXLSTemplate();
			}
			//@JAPR
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		return $anInstance;
	}
	
	//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
	/* Dado un Catálogo específico y una pregunta, reemplaza todas las variables de dicha pregunta que hacen referencia a los atributos del catálogo indicado, por su correspondiente
	propiedad de template de componente DHTMLX (o equivalente, donde sea que se utilizó esa sintaxis) basándose en el hecho que sólo se soportará el formato extendido de variables con
	referencias vía attrdid (o posterior, en cuyo caso se debe integrar conforme se van modificando los formatos).
	Esta función se debería utilizar exclusivamente al momento de descarga la definición de una expresión que se acaba de obtener de la metadata y se mandará al App
	*/
	static function ReplaceCatalogMemberVarsByTemplateProp($aRepository, $aCatalogID, $aQuestionID, $sText) {
		$strText = $sText;
		
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Validado para salir si no hay patrones a reemplazar
		if ($aCatalogID <= 0 || $aQuestionID <= 0 || stripos($sText, '.attrdid(') === false || stripos($sText, '{$Q') === false) {
			return $strText;
		}
		//@JAPR
		
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		$strVariable = '{$Q'.$aQuestionID.'.attrdid(#memberid#)}';
		if (!is_null($objCatalogMembersColl)) {
			//Esta colección ya debe venir ordenada, así que se puede usar tal cual pues es la secuencia correcta para generar las variables que se reemplazarán
			$intMemberOrder = 1;
			foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
				$strTemplateProp = "#attrib{$intMemberOrder}#";
				$strText = str_ireplace(str_ireplace("#memberid#", $objCatalogMember->DataSourceMemberID, $strVariable), $strTemplateProp, $strText);
				$intMemberOrder++;
			}
		}
		
		return $strText;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("ParentID", $anArray))
		{
			$this->ParentID = (int) $anArray["ParentID"];
		}
		
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = (int) $anArray["CatalogID"];
		}
		
	 	if (array_key_exists("CatalogName", $anArray))
		{
			$this->CatalogName = rtrim($anArray["CatalogName"]);
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
	 	if (array_key_exists("eBavelAppID", $anArray))
		{
			$this->eBavelAppID = rtrim($anArray["eBavelAppID"]);
		}
		
	 	if (array_key_exists("eBavelFormID", $anArray))
		{
			$this->eBavelFormID = rtrim($anArray["eBavelFormID"]);
		}
		//@JAPR

		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (array_key_exists("eBavelFormType", $anArray))
		{
			$this->eBavelFormType = rtrim($anArray["eBavelFormType"]);
		}
		
		return $this;
	}
	
	//@LROUX 2012-11-23: Agregado para ser procesado a una subclase customizada (PCP API).
	function updateFromRS($aRS)
	{
		$this->ParentID = (int) $aRS->fields["parentid"];
		$this->CatalogID = (int) $aRS->fields["catalogid"];
		$this->CatalogName= $aRS->fields["catalogname"];
		$this->ModelID = (int) $aRS->fields["modelid"];
		$this->VersionIndID = (int) $aRS->fields["versionindid"];
		$this->TableName= $aRS->fields["tablename"];
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$this->VersionNum = (int) @$aRS->fields["versionnum"];
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		$this->DataVersionDate = (string) @$aRS->fields["dataversiondate"];
		if ( $this->DataVersionDate ) {
			$this->DataVersionNum = (int) implode(privateUnformatDateTime($this->DataVersionDate, ""), "");
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$this->DissociateCatDimens = (int) @$aRS->fields["dissociatecatdimens"];
			$this->HierarchyID = (int) @$aRS->fields["hierarchyid"];
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$this->eBavelAppID = (int) @$aRS->fields["ebavelappid"];
		$this->eBavelFormID = (int) @$aRS->fields["ebavelformid"];
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$this->DataSourceID = (int) @$aRS->fields["datasourceid"];
		$this->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		$this->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR
		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$this->eBavelFormType = (int) @$aRS->fields["ebavelformtype"];
		}
		if (getMDVersion() >= esvQuestionAR) {
			$this->IsModel = (int) @$aRS->fields["ismodel"];
			$this->CatModelMemberID = (int) @$aRS->fields["qcatmemberid"];
			$this->CatPlatformMemberID = (int) @$aRS->fields["qcatmemberplatformid"];
			$this->CatVersionMemberID = (int) @$aRS->fields["qcatmemberversionid"];
			$this->CatZipSourceMemberID = (int) @$aRS->fields["qcatmemberzipfileid"];
			$this->DataSourceModelMemberID = (int) @$aRS->fields["qdatasourcememberid"];
			$this->DataSourcePlatformMemberID = (int) @$aRS->fields["qdatamemberplatformid"];
			$this->DataSourceVersionMemberID = (int) @$aRS->fields["qdatamemberversionid"];
			$this->DataSourceZipSourceMemberID = (int) @$aRS->fields["qdatamemberzipfileid"];
		}
		
		BITAMGlobalFormsInstance::AddCatalogInstanceWithID($this->CatalogID, $this);
		//@JAPR 2012-06-20: Agregado el enlace al catálogo desde los valores de la dimensión
		BITAMGlobalFormsInstance::AddCatalogInstanceWithParentID($this->ParentID, $this);
		//@JAPR
		
		return $this;
	}
	
	/* Generaba hasta v5 el modelo con los datos de los catálogos manuels de eForms. A partir de v6 queda obsoleta, si se llega a invocar no generará el modelo
	//@JAPRDescontinuada en v6
	*/
	function saveModel() {
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		//@JAPR
		
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return -1;
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		$anInstanceModel = BITAMModel::NewModel($this->Repository);
		$anInstanceModel->ModelName = $this->CatalogName;
		$anInstanceModel->tableCATALOG = $this->TableName;
		$anInstanceModel->ModelID = $this->ModelID;
		//@JAPR 2013-01-28: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Con esta propiedad se impedirá que el Model Manager manipule al modelo creado para eForms
		if ($this->DissociateCatDimens) {
			@$anInstanceModel->ri_definition = CREATED_BY_EFORMS;
		}
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceModel->save();
		chdir($strOriginalWD);
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError != '') {
			//die("(".__METHOD__.") ".translate("Error adding catalog's model").": ".$strMMError.". ");
			$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding catalog's model").": ".$strMMError.". ";
			return 0;
		}
		//@JAPR
		
		return $anInstanceModel->ModelID;
	}
	
	/* Generaba hasta v5 el modelo con los datos de los catálogos manuels de eForms. A partir de v6 queda obsoleta, si se llega a invocar no generará el modelo
	//@JAPRDescontinuada en v6
	*/
	function saveIndicator() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		require_once("../model_manager/indicator.inc.php");
		
		$anInstanceIndicator = BITAMIndicator::NewIndicator($this->Repository);
		$anInstanceIndicator->CubeID = $this->ModelID;
		$anInstanceIndicator->IndicatorName = 'COUNT';
		$anInstanceIndicator->formula_bd = "COUNT(*)";
		$anInstanceIndicator->formato = "#,##0";
		$anInstanceIndicator->is_custom = 1;
		$anInstanceIndicator->no_ejecutivo = 0;
		$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		return $anInstanceIndicator->IndicatorID;
	}
	
	//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	//Este es un indicador especial del modelo, el cual debe existir en todo momento ya que de él depende que se pueda lograr la descarga
	//inteligente de catálogos, por lo tanto se verifica si el campo correspondiente existe y de no ser así se crea nuevamente (no se creará
	//un indicador diferente por las implicaciones que eso lleva, simplemente se volverá a crear la columna invocando a los métodos
	//correspondientes del model manager)
	/* Generaba hasta v5 el modelo con los datos de los catálogos manuels de eForms. A partir de v6 queda obsoleta, si se llega a invocar no generará el modelo
	//@JAPRDescontinuada en v6
	*/
	function saveVersionIndicator() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		//@JAPR 2012-03-08: Se hace esto aquí porque al invocar desde eSurveyService.php no se habían asignado estos datos, aunque desde otros puntos
		//que invocan a esta misma función (por ejemplo desde una instancia de BITAMDimension) generalmente ya estaban asignados
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		//@JAPR
		
		$anInstanceIndicator = null;
		$blnUpdateVersionIndID = true;
		if ($this->VersionIndID > 0)
		{
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->VersionIndID);
			$blnUpdateVersionIndID = is_null($anInstanceIndicator);
		}
		
		if (!is_null($anInstanceIndicator))
		{
			//Obtiene el NOM_FISICO ya que la carga del indicador no lo trae por default (por lo menos al 2012-03-06)
			$sql = 'select NOM_FISICO from SI_CPTO_ATRIB where CLA_CONCEPTO = '.$anInstanceIndicator->ModelID.' and CONSECUTIVO = '.$anInstanceIndicator->id_cpto_atrib;
			$rst = $this->Repository->ADOConnection->Execute($sql);
			if ($rst)
			{
				if (!$rst->EOF)
				{
					$anInstanceIndicator->field_name = $rst->fields['nom_fisico'];
				}
			}
			
			//Simplemente verifica la existencia del campo aplicando una condición vacia, si el RecordSet falla se tendrá que crear de nuevo
			$sql =  "SELECT ".$anInstanceIndicator->field_name.
				" FROM RIDIM_".$this->ParentID.
				" WHERE 1 = 2";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				//En este caso no se encontró la columna así que se intenta crear nuevamente, actualizando la definición del propio indicador base
				$datatype = 'float';
				if ($this->oModel->DBID == MYSQL)
					$datatype = 'double';
				
				//Estos métodos agregan la columna a las tablas donde sea necesario
				ModelAddIndicator($this->Repository->DataADOConnection, $anInstanceIndicator->oModel->nom_tabla, $anInstanceIndicator->field_name, $datatype);
				ModelAgregationAddIndicator($this->Repository->ADOConnection, $this->Repository->DataADOConnection, $anInstanceIndicator->ModelID, $anInstanceIndicator->field_name, $datatype);
				
				//Realmente no cambió nada en la definición del indicador, por lo que no es necesario grabarlo ya que esto lanzaría procesos
				//de reexportación y actualización de fórmulas que resultan innecesarios
				//$anInstanceIndicator->save();
			}
		}
		
		if (!is_null($anInstanceIndicator))
		{
			return $anInstanceIndicator->IndicatorID;
		}
		
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
		$anInstanceIndicator->ModelID = $this->ModelID;
		$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
		$anInstanceIndicator->IndicatorName = 'Version';
		$anInstanceIndicator->formato = "#,##0";
		$anInstanceIndicator->agrupador = "MAX";
		$anInstanceIndicator->sql_source = "MAX";
		$anInstanceIndicator->save();
		$this->VersionIndID = ($anInstanceIndicator->IndicatorID <= 0)?0:$anInstanceIndicator->IndicatorID;
		
		//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
		//Corregido un bug, no estaba registrando el indicador de versión cuando se invocaba al método
		//desde cualquier punto que no fuera la creación/edición del propio catálogo
		if (!$this->isNewObject() && $blnUpdateVersionIndID)
		{
			$sql = "UPDATE SI_SV_Catalog SET ".
						" VersionIndID = ".$this->VersionIndID.
					" WHERE CatalogID = ".$this->CatalogID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
		}
		//@JAPR
		
		chdir($strOriginalWD);
		return $this->VersionIndID;
	}
	//@JAPR
	
	/* Generaba hasta v5 el modelo con los datos de los catálogos manuels de eForms. A partir de v6 queda obsoleta, si se llega a invocar no generará el modelo
	//@JAPRDescontinuada en v6
	*/
	function removeModel() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		$strOriginalWD = getcwd();
		
		$anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
		
		if ($anInstanceModel)
		{
			$anInstanceModel->tableCATALOG = $this->TableName;
			$anInstanceModel->remove();
			
// @EA 2012-12-07 Nos aseguramos de borrar todas las referencias que existan del catalogo
			$sql = 
			'delete from SI_DESCRIP_ENC where NOM_TABLA = \''.$this->TableName.'\'';
			$this->Repository->ADOConnection->Execute($sql);
			
			$sql = 
			'delete from SI_DESCRIP_DET where CLA_PADRE = '.$this->ParentID;
			$this->Repository->ADOConnection->Execute($sql);
//			
		}
		
		chdir($strOriginalWD);
	}
	
	/* Generaba hasta v5 el modelo con los datos de los catálogos manuels de eForms. A partir de v6 queda obsoleta, si se llega a invocar no generará el modelo
	//@JAPRDescontinuada en v6
	*/
	function saveModelDimension() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return -1;
		}
		//@JAPR
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		$gblModMngrErrorMessage = '';
		//@JAPR
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Dimensión de catálogo, esta dimensión contiene dimensiones-atributo así que no se puede reutilizar
		$dimensionName = $this->CatalogName;
		$cla_descrip = $this->ParentID;
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, -1);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		//@JAPR 2013-01-28: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;

		$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceModelDim->Dimension->DimensionID = $cla_descrip;
		$anInstanceModelDim->Dimension->DimensionGroup = $dimensionName;
		$anInstanceModelDim->Dimension->IsCatalog = true;
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceModelDim->save();
		$this->TableName = $anInstanceModelDim->Dimension->TableName;
		chdir($strOriginalWD);
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError != '') {
			if ($cla_descrip > 0) {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error updating catalog's model dimension").": ".$strMMError.". ";
			}
			else {
				$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding catalog's model dimension").": ".$strMMError.". ";
			}
			return 0;
		}
		//@JAPR
		
		return $anInstanceModelDim->Dimension->DimensionClaDescrip;
	}

	function save()
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		$cla_descrip = 0;
		$cla_concepto = 0;
		$gblModMngrErrorMessage = '';
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		
	 	if ($this->isNewObject()) {
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			$this->ParentID = -1;
			$cla_descrip = $this->saveModelDimension();
			//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			if ($cla_descrip > 0 && $this->CreationAdminVersion < esveFormsv6) {
				$tmpFieldKey = $this->TableName."KEY";
				$tmpFieldDesc = "DSC_".$cla_descrip;
				
				//Insertar el primer valor de No Aplica
				$sqlIns = "INSERT INTO ".$this->TableName." (".$tmpFieldKey.", ".$tmpFieldDesc.") VALUES (1, ".$this->Repository->DataADOConnection->Quote('*NA').")";
				if ($this->Repository->DataADOConnection->Execute($sqlIns) === false) {
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." ".$this->TableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
					}
				}
			}
			//@JAPR
			
			$this->ModelID = -1;
			$cla_concepto = $this->saveModel();
			
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(CatalogID)", "0")." + 1 AS CatalogID".
						" FROM SI_SV_Catalog";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->CatalogID = (int) $aRS->fields["catalogid"];
			$this->ModelID = $cla_concepto;
			//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
			if ($cla_concepto > 0) {
				$this->saveIndicator();
				//@JAPR 2012-03-06: Agregado el indicador de versión por registro para descarga inteligente de catálogos
				$this->saveVersionIndicator();
			}
			//@JAPR
			
			$this->ParentID = $cla_descrip;
			
			//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
			$strAdditionalFields = "";
			$strAdditionalValues = "";
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalFields .= ', VersionNum';
				$strAdditionalValues .= ', 1';
			}
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if (getMDVersion() >= esvCatalogDissociation) {
				$strAdditionalFields .= ', DissociateCatDimens, HierarchyID';
				$strAdditionalValues .= ", {$this->DissociateCatDimens}, {$this->HierarchyID}";
			}
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
				$strAdditionalValues .= ", {$this->eBavelAppID}, {$this->eBavelFormID}";
			}
			//@JAPR
			//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
			if (getMDVersion() >= esvRedesign) {
				$strAdditionalFields .= ', eBavelFormType';
				$strAdditionalValues .= ", {$this->eBavelFormType}";
			}
			//@AAL
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ', DataSourceID, CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$this->DataSourceID.', '.$this->LastModAdminVersion.', '.$this->LastModAdminVersion;
			}
			//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
			if (getMDVersion() >= esvDataSourceDataVersion) {
				$strAdditionalFields .= ', DataVersionDate';
				$strAdditionalValues .= ", ".((!$this->DataVersionDate)?"NULL":$this->Repository->DataADOConnection->DBTimeStamp($this->DataVersionDate));
			}	
			//@JAPR
			$sql = "INSERT INTO SI_SV_Catalog (".
						" ModelID, ParentID, CatalogID".
			            ", CatalogName".
			            ", TableName".
			            ", VersionIndID".
			            $strAdditionalFields.
			            ") VALUES (".
			            $this->ModelID.','.$this->ParentID.','.$this->CatalogID.
						",".$this->Repository->DataADOConnection->Quote($this->CatalogName).
						",".$this->Repository->DataADOConnection->Quote($this->TableName).
						",".$this->VersionIndID.
						$strAdditionalValues.
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		else
		{
			$cla_descrip = $this->saveModelDimension();
			$cla_concepto = $this->saveModel();
			//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
			if ($cla_concepto > 0) {
				$this->saveVersionIndicator();
			}
			
			//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
			//@LROUX 2012-11-27: Si VersionNum esta inicializada en -1, significa que no nos interesa las version y no debe incrementarse.
			if ($this->VersionNum >= 0)
			{
				$this->VersionNum++;
			}
			$strAdditionalValues = "";
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalValues .= ', VersionNum = '.$this->VersionNum;
			}
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if (getMDVersion() >= esvCatalogDissociation) {
				$strAdditionalValues .= ', DissociateCatDimens = '.$this->DissociateCatDimens;
			}
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalValues .= ', eBavelAppID = '.$this->eBavelAppID.', eBavelFormID = '.$this->eBavelFormID;
			}
			//@JAPR
			//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
			if (getMDVersion() >= esvRedesign) {
				$strAdditionalValues .= ', eBavelFormType = ' . $this->eBavelFormType;
			}
			//@AAL
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalValues .= ', DataSourceID = '.$this->DataSourceID.
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
			if (getMDVersion() >= esvDataSourceDataVersion) {
				$strAdditionalValues .= ", DataVersionDate = ".((!$this->DataVersionDate)?"NULL":$this->Repository->DataADOConnection->DBTimeStamp($this->DataVersionDate));
			}
			//@JAPR
			$sql = "UPDATE SI_SV_Catalog SET ".
						" CatalogName = ".$this->Repository->DataADOConnection->Quote($this->CatalogName).
						", VersionIndID = ".$this->VersionIndID.
						$strAdditionalValues.
						" WHERE CatalogID = ".$this->CatalogID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
			//@JAPRDescontinuada en v6
			//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
			//La actualización de formas la realizará el DataSource directamente
			//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($this->Repository, $this->CatalogID);
			//@JAPR
		}
		
		//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Reconstruya la jerarquía del catálogo en caso de que hubiera sido manipulada externamente
		$this->generateCatalogHierarchy();
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (($cla_concepto <= 0 || $cla_descrip <= 0) && $gblModMngrErrorMessage != '' && $this->CreationAdminVersion < esveFormsv6) {
			if($gblShowErrorSurvey) {
				die($gblModMngrErrorMessage);
			}
			else {
				return($gblModMngrErrorMessage);
			}
		}
		//@JAPR
		
		return $this;
	}

	function remove() {
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Para asegurar la consistencia, se eliminará atributo por atributo ya que las dimensiones alternativas al ser marcadas como CREATED_BY
		//eForms, no son eliminadas automáticamente al borrar el último cubo de encuesta que las use, por lo que si no se borran al eliminar el
		//atributo entonces se quedarían indefinidamente en la metadata
		if ($this->DissociateCatDimens) {
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
			if (!is_null($objCatalogMembersColl)) {
				foreach ($objCatalogMembersColl as $objCatalogMember) {
					$objCatalogMember->remove();
				}
			}
		}
		
		//@JAPR 2013-02-13: Corregido un bug, no estaba eliminando información relacionada al catálogo cuando este era eliminado
		$sql = "DELETE FROM SI_SV_CatFilterUser WHERE FilterID IN (
			SELECT FilterID FROM SI_SV_CatFilter WHERE CatalogID = {$this->CatalogID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_CatFilterRol WHERE FilterID IN (
			SELECT FilterID FROM SI_SV_CatFilter WHERE CatalogID = {$this->CatalogID})";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatFilterUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_CatFilter WHERE CatalogID = ".$this->CatalogID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
		$sql = "DELETE FROM SI_SV_SurveyFilter WHERE CatalogID = ".$this->CatalogID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
		//Al eliminar un catálogo se deben actualizar todas las preguntas que lo utilizan, cuando se trata de la versión 6+ este método se invoca desde el 
		//DataSource y en realidad sólo debería estar asociado a una única pregunta (con preguntas tipo GetData se podría enlazar con otras preguntas adicionales)
		//pero es necesario desligarlas en la metadata
		if ($this->CreationAdminVersion >= esveFormsv6) {
			$sql = "UPDATE SI_SV_Section SET 
					CatalogID = 0 
					, CatMemberID = 0 
				WHERE CatalogID = {$this->CatalogID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2016-04-29: Corregido un bug, no estaba eliminando el campo del atributo imagen
			$strAdditionalFields = '';
			if (getMDVersion() >= esvSimpleChoiceCatOpts) {
				//@JAPR 2016-12-20: Corregido un bug, estaba reemplazando en lugar de concatenar los campos adicionales (#DBNNGH)
				$strAdditionalFields .= ', CatMemberImageID = 0';
			}
			//@JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
			if (getMDVersion() >= esvSChoiceMetro) {
				//@JAPR 2016-12-20: Corregido un bug, estaba reemplazando en lugar de concatenar los campos adicionales (#DBNNGH)
				$strAdditionalFields .= ', CatMemberOrderID = 0';
			}
			$sql = "UPDATE SI_SV_Question SET 
					CatalogID = 0 
					, CatMemberID = 0 
					, CatMemberLatitudeID = 0 
					, CatMemberLongitudeID = 0 
					{$strAdditionalFields} 
				WHERE CatalogID = {$this->CatalogID}";
			//@JAPR
			$this->Repository->DataADOConnection->Execute($sql);
		}
		//@JAPR
		
		$sql = "DELETE FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_Catalog WHERE CatalogID = ".$this->CatalogID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$this->removeModel();
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->CatalogID < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Catalog");
		}
		else
		{
			return $this->CatalogName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Catalog";
		}
		else
		{
			return "BITAM_PAGE=Catalog&CatalogID=".$this->CatalogID;
		}
	}
	
	function get_Parent()
	{
		return BITAMCatalogCollection::NewInstance($this->Repository,null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function generateBeforeFormCode($aUser)
 	{	
?>
 	<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
 			var strAlert = '';
 			
 			if(Trim(BITAMCatalog_SaveForm.CatalogName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Name"))?>';
 			}
 			var MDVersion = <?=getMDVersion()?>;
 			var esvRedesign = <?=esvRedesign?>;
 			/*@AAL 19/03/2015: Si esta dentro de la versión Correcta se debe validar */
 			if (MDVersion >= esvRedesign) {
 				/*@AAL 19/03/2015: Se Valida que si se ha seleccionado una Aplicación eBavel, 
	            también debe seleccionarse ya sea una Forma o Vista y algún elementeo de ésta.*/
	            if(BITAMCatalog_SaveForm.eBavelAppID.value != "0" &&  BITAMCatalog_SaveForm.eBavelFormID.value == "0")
	            {
	                alert("Must select a eBavel data type and some eBavel data element");
	                return false;
	            }
 			}
            //@AAL

 			//En esta parte se verifica si el AttributeName ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->CatalogName?>';
			var catalogID = <?=$this->CatalogID?>;
			var attributeName = BITAMCatalog_SaveForm.CatalogName.value;

			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}

			xmlObj.open('POST', 'verifyAttributeName.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID='+catalogID+'&AttributeName='+encodeURIComponent(attributeName)+'&AttributeNameOld='+encodeURIComponent(attributeNameOld));

	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}

	 		strResponseData = Trim(unescape(xmlObj.responseText));

	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}

	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strAlert += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
	 		}

 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				BITAMCatalog_Ok(target);
 			}
 		} 

        /*@AAL 19/03/2015: Esta función actualiza la lista que mostrará ya sea los elementos de las Formas o de las vistas*/
        function UpdateCatalogeBavel()
        {
            var ObjComb = BITAMCatalog_SaveForm.eBavelFormID;
            //Si no se ha seleccionado ninguna aplicación entonces hacemos todo a NONE
            if(BITAMCatalog_SaveForm.eBavelAppID.value == "0" || ObjComb.options.length == 0){
                BITAMCatalog_SaveForm.eBavelFormType.selectedIndex = 0;
                BITAMCatalog_SaveForm.eBavelFormID.selectedIndex = 0;
                ObjComb.options.length = 0;
                ObjComb.options.add(new Option("(<?=translate('None')?>)", "0"));
            }
            else{
                //Si hemos selecionado una aplicación y la lista de las formas o vistas contienen elementos 
                if(BITAMCatalog_SaveForm.eBavelFormIDTemp.options.length > 0 || BITAMCatalog_SaveForm.eBavelViewIDTemp.options.length > 0){
                    var eBavelFormID = null;
                    //Estamos solicitando los elementos de las formas.
                    if(BITAMCatalog_SaveForm.eBavelFormType.value == "<?=ebftForms?>")
                    	eBavelFormID = BITAMCatalog_SaveForm.eBavelFormIDTemp.cloneNode(true);
                    else  //Si hemos selecconado la opción 1 estamos indicando que queremos los elementos de las vistas y clonamos el Cbbx oculto
                        eBavelFormID = BITAMCatalog_SaveForm.eBavelViewIDTemp.cloneNode(true);
                    //Como no se mostraban visibles entonces las mostramos.
                    ObjComb.parentNode.replaceChild(eBavelFormID, ObjComb);
                    eBavelFormID.style.display = "inline";
                    eBavelFormID.setAttribute("name",  "eBavelFormID");
                    ObjComb = null;
                }
                else{//Si se llega a este caso es por que no hay elementos ya sea en las vistas o formas y clonamos el Cbbx oculto
                    ObjComb.options.length = 0;
                    ObjComb.options.add(new Option("(<?=translate('None')?>)", "0"));
                }
            }
        }
        //@AAL

		function setParentValuesIDs()
		{
			frmSetCatalogValues.submit();
		}
 	</script>
<?
?>	 
  <iframe name="frameUploadCatalogsFile" style="display:none"></iframe>
  <iframe name="frameAppendCatalogsFile" style="display:none"></iframe>
	<script language="JavaScript">
 		function downloadTemplate()
 		{
			window.open("downloadTemplate.php?t=c&name=<?php echo $this->CatalogName; ?>&key=<?php echo $this->ParentID; ?>", "downloadtemplate", "menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no", 1)
 		}
 		function exportCatalog()
 		{
			window.open("main.php?BITAM_PAGE=Catalog&CatalogID=<?=$this->CatalogID?>&ExportXLS=1", "exportCatalogTemplate", "width=500,height=100,left=350,top=350,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no", 1)
 		}
 		function uploadCatalogsFile()
 		{
 		  document.forms.frmUploadCatalogsFile.style.display = "inline"
 		}
 		function refreshpage()
 		{
 		  sURL = "main.php?BITAM_SECTION=CatalogCollection"
 		  
 		  window.location.href = sURL
 		}
 		function showLogFile(sLogFile)
 		{
 		  window.open(sLogFile, "logfile", "menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=auto", 1)
 		}
 	</script>
<?

	}
	
	function generateAfterFormCode($aUser)
	{
		global $gbIsGeoControl;
?>
	<script language="JavaScript">
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false ;
		}
		objOkSelfButton = document.getElementById("BITAMCatalog_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMCatalog_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMCatalog_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
		
 		if ($gbIsGeoControl) {
?>
 		var rowField = document.getElementById("Row_DissociateCatDimens");
 		if (rowField) {
 			rowField.style.display = 'none';
 		}
<?
 		}
 		
?>
	</script>

		<form name="frmSetCatalogValues" action="main.php?BITAM_PAGE=Dimension&CubeID=-1&consecutivo=<?=$this->ParentID?>" method="post" target="body">
		</form>
<?
	}
	
	function get_FormIDFieldName()
	{
		return 'CatalogID';
	}

	function get_FormFieldName() {
		return 'CatalogName';
	}

	function get_FormFields($aUser)
	{
		global $gbIsGeoControl;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$arrDissociateDimsOptions = array();
			$arrDissociateDimsOptions[0] = translate("No");
			$arrDissociateDimsOptions[1] = translate("Yes");
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DissociateCatDimens";
			$aField->Title = translate("Use catalog dissociation?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrDissociateDimsOptions;
			$aField->IsDisplayOnly = true;				//Esta opción NO es configurable
			//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
			//if ($gbIsGeoControl) {
			$aField->IsVisible = false;
			$aField->Title = "";
			//}
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
			$arreBavelApps = array(0 => '('.translate('None').')');
			require_once('eBavelIntegration.inc.php');
			$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true, '', 0);
			
			$aFieldApps = BITAMFormField::NewFormField();
			$aFieldApps->Name = "eBavelAppID";
			$aFieldApps->Title = translate("eBavel application");
			$aFieldApps->Type = "Object";
			$aFieldApps->VisualComponent = "Combobox";
			$aFieldApps->Options = $arreBavelApps;
			if (getMDVersion() >= esvRedesign) $aFieldApps->OnChange = "UpdateCatalogeBavel();";
			$myFields[$aFieldApps->Name] = $aFieldApps;
			
			//Obtiene la lista de formas de eBavel creadas en el repositorio
			$arreBavelForms = array('' => array('' => '('.translate('None').')'));
			$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId', 0);

			/*@AAL 19/03/2015: Modificado para soportar vistas de eBavel*/
			if (getMDVersion() >= esvRedesign) {
				//Obtiene la Vistas de eBavel asociadas a una aplicación creada en el repositorio
				$arreBavelViews = array('' => array('' => '('.translate('None').')'));
				$arreBavelViews = GetEBavelCollectionByField(@GetEBavelViews($this->Repository), 'label', 'key', true, 'appId', 0);
				
				/*@AAL 19/03/2015: Se agraga un componentes Cbbx para elegir entre una Forma o una vista*/
	            $aFieldeBavelFormType = BITAMFormField::NewFormField();
	            $aFieldeBavelFormType->Name = "eBavelFormType";
	            $aFieldeBavelFormType->Title = translate("eBavel Data Type");
	            $aFieldeBavelFormType->Type = "Object";
	            $aFieldeBavelFormType->VisualComponent = "Combobox";
	            $aFieldeBavelFormType->Options = array(ebftForms => "Forms", ebftViews => "Views");
	            $aFieldeBavelFormType->OnChange = "UpdateCatalogeBavel();";
	            $myFields[$aFieldeBavelFormType->Name] = $aFieldeBavelFormType;
	            //@AAL
	                                    
	            /*@AAL 19/03/2015: Modifiacado para mostrar la lista de elementos dependiendo si se selecciono una Forma o una Vista*/
				$aFieldForms = BITAMFormField::NewFormField();
				$aFieldForms->Name = "eBavelFormID";
				$aFieldForms->Title = translate("eBavel data");
				$aFieldForms->Type = "Object";
				$aFieldForms->VisualComponent = "Combobox";
	            if (!$this->isNewObject()) {
	            	//Si se trata de una Forma de eBavel
	                if ($this ->eBavelFormType == ebftForms) {
	                    $aFieldForms->Options = $arreBavelForms[$this->eBavelAppID];
	                    if (isset($arreBavelForms[$this->eBavelAppID][$this->eBavelFormID])) 
	                        $aFieldForms->Options[$this->eBavelFormID] = $arreBavelForms[$this->eBavelAppID][$this->eBavelFormID];
	                }
	                else { //Se trata de una Vista de eBavel
	                    $aFieldForms->Options = $arreBavelViews[$this->eBavelAppID];
	                    if (isset($arreBavelViews[$this->eBavelAppID][$this->eBavelFormID])) 
	                        $aFieldForms->Options[$this->eBavelFormID] = $arreBavelViews[$this->eBavelAppID][$this->eBavelFormID];
	                }
	            }
	            else
	                $aFieldForms->Options = array(0 => '(' . translate('None') . ')');
				$myFields[$aFieldForms->Name] = $aFieldForms;
	            
	            /*@AAL 019/03/2015: Se agraga un componentes Cbbx oculto para almacenar la lista de elementos de una Forma*/            
	            $aFieldeBavelFormIDTemp = BITAMFormField::NewFormField();
	            $aFieldeBavelFormIDTemp->Name = "eBavelFormIDTemp";
	            //$aFieldeBavelFormIDTemp->Title = "Hidden eBavel Form Id";
	            $aFieldeBavelFormIDTemp->Type = "Object";
	            $aFieldeBavelFormIDTemp->VisualComponent = "Combobox";
	            $aFieldeBavelFormIDTemp->Options = $arreBavelForms;
	            $aFieldeBavelFormIDTemp->IsVisible = false;
	            $myFields[$aFieldeBavelFormIDTemp->Name] = $aFieldeBavelFormIDTemp;
	            $aFieldeBavelFormIDTemp->Parent = $aFieldApps;
	            $aFieldApps->Children[] = $aFieldeBavelFormIDTemp;
	            
	            /*@AAL 19/03/2015: Se agraga un componentes Cbbx oculto para almacenar la lista de elementos de una Vista*/
	            $aFieldeBavelViewIDTemp = BITAMFormField::NewFormField();
	            $aFieldeBavelViewIDTemp->Name = "eBavelViewIDTemp";
	            //$aFieldeBavelViewIDTemp->Title = "Hidden eBavel View Id";
	            $aFieldeBavelViewIDTemp->Type = "Object";
	            $aFieldeBavelViewIDTemp->VisualComponent = "Combobox";
	            $aFieldeBavelViewIDTemp->Options = $arreBavelViews;
	            $aFieldeBavelViewIDTemp->IsVisible = false;
	            $myFields[$aFieldeBavelViewIDTemp->Name] = $aFieldeBavelViewIDTemp;
	            $aFieldeBavelViewIDTemp->Parent = $aFieldApps;
	            $aFieldApps->Children[] = $aFieldeBavelViewIDTemp;
	            //@AAL
				
			} else {
				$aFieldForms = BITAMFormField::NewFormField();
				$aFieldForms->Name = "eBavelFormID";
				$aFieldForms->Title = translate("eBavel data form");
				$aFieldForms->Type = "Object";
				$aFieldForms->VisualComponent = "Combobox";
				$aFieldForms->Options = $arreBavelForms;
				$myFields[$aFieldForms->Name] = $aFieldForms;
				$aFieldForms->Parent = $aFieldApps;
				$aFieldApps->Children[] = $aFieldForms;
				//@JAPR
			}
		}
				
		return $myFields;
	}
	
	//@JAPRDescontinuada en v6
	static function existThisAttributeName($aRepository, $attributeName)
	{
		//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
		global $garrReservedDimNames;
		//@JAPR
		
		$sql = "SELECT A.CLA_DESCRIP FROM SI_DESCRIP_ENC A 
				WHERE A.NOM_LOGICO = ".$aRepository->ADOConnection->Quote($attributeName);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($aRS->EOF)
		{
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			if (isset($garrReservedDimNames) && isset($garrReservedDimNames[strtolower($attributeName)])) {
				return ((int) array_search(strtolower($attributeName), array_keys($garrReservedDimNames))) +1;
			}
			else {
				return false;
			}
			//@JAPR
		}
		else 
		{
			return true;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}
	
	//@JAPR 2011-06-23: Integrada la captura de valores para el catálogo
 	function addButtons($aUser)
	{
		if (!$this->isNewObject())
		{
	?>
		<!--<a href="#" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/catalogEntry.gif" displayMe=1> <?=translate("Catalog values")?> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		<button id="btnCatalogValues" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/catalogEntry.gif" alt="<?=translate('Catalog values')?>" title="<?=translate('Catalog values')?>" displayMe="1" /> <?=translate("Catalog values")?></button>
	<?	}
	//@JAPR 2012-05-03: Corregido un bug, las opciones de importación y templates sólo deben estar visibles durante la edición
	if (!$this->isNewObject()) //($_SESSION["PA_ADMIN"] == 1)
		{
?>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadTemplate();"><img src="images/download_xls_templates.gif"> <?=translate("Download catalogs template")."   "?>  </span>-->
		<button id="btnDownloadCatTpl" class="alinkescfav" onclick="downloadTemplate();"><img src="images/download_xls_templates.gif" alt="<?=translate('Download catalogs template')?>" title="<?=translate('Download catalogs template')?>" displayMe="1" /> <?=translate("Download catalogs template")?></button>
    	<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="exportCatalog();"><img src="images/download_xls_templates.gif"> <span id="txtExportSpan"> <?=translate("Export catalog values")."   "?>  </span></span>-->
    	<button id="btnExportCatalog" class="alinkescfav" onclick="exportCatalog();"><img src="images/download_xls_templates.gif" alt="<?=translate('Export catalog values')?>" title="<?=translate('Export catalog values')?>" displayMe="1" /> <?=translate("Export catalog values")?></button>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="uploadCatalogsFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> <?=translate("Upload catalogs file")."   "?>  </span></span>-->
		<button id="btnUploadCatFile" class="alinkescfav" onclick="uploadCatalogsFile();"><img src="images/load_data_XLS.gif" alt="<?=translate('Upload catalogs file')?>" title="<?=translate('Upload catalogs file')?>" displayMe="1" /> <?=translate("Upload catalogs file")?></button>
	    <form name="frmUploadCatalogsFile" method="POST" action="uploadCatalog.php?catalogid=<?=$this->ParentID?>&dte=<?=date('YmdHis')?>" enctype="multipart/form-data" target="frameUploadCatalogsFile" style="display:none">
	      <input type="file" name="xlsfile">
	      <input type="submit" name="txtsubmit" value="<?=translate("Upload file")?>">
	      <input type="button" name="txtcancel" value="<?=translate("Cancel")?>" onclick="document.forms.frmUploadCatalogsFile.style.display='none';document.forms.frmUploadCatalogsFile.reset()">
	    </form>
<?
		}
		/*
		if (strlen($this->logfile) > 0)
		{
?>
		<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="showLogFile('<?=$this->logfile?>');"><img src="images/showMsg.gif"> <?=translate("View last upload log")."   "?>  </span>
<?		  
		}*/
	}
	//@JAPR
	
	function canRemove($aUser) {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		//En esta versión los catálogos pasan a segundo plano, así que se pueden eliminar en cualquier momento
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return true;
		}
		//@JAPR
		
		//25Ene2013: Validar que no se pueda borrar un catálogo cuando es usado en una encuesta
		$sql = "SELECT COUNT(*) AS NumRec FROM SI_SV_Survey WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			if ((int) $aRS->fields["numrec"] > 0)
			{
				return false;
			}
		}
		
		//Validar que no se pueda borrar un catálogo cuando es usado en una pregunta
		$sql = 'select count(*) as NumRec from SI_SV_Question where UseCatalog = 1 and CatalogID = '.$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			if ((int) $aRS->fields["numrec"] > 0)
			{
				return false;
			}
		}
		
		$sql ='select count(*) as NumRec from SI_CPTO_LLAVE where CLA_DESCRIP = '.$this->ParentID;
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF) {
			if ((int) $aRS->fields["numrec"] > 0) {
				return false;
			}
		}
		
		//25Ene2013: Validar que no se pueda borrar un catálogo cuando es usado en una sección
		$sql = "SELECT COUNT(*) AS NumRec FROM SI_SV_Section WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			if ((int) $aRS->fields["numrec"] > 0)
			{
				return false;
			}
		}
		
		//25Ene2013: Validar que no se pueda borrar un catálogo cuando es usado en una agenda
		$sql = "SELECT COUNT(*) AS NumRec FROM SI_SV_SurveyAgenda WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			if ((int) $aRS->fields["numrec"] > 0)
			{
				return false;
			}
		}
		
		//25Ene2013: Validar que no se pueda borrar un catálogo cuando es usado en un scheduler
		$sql = "SELECT COUNT(*) AS NumRec FROM SI_SV_SurveyScheduler WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			if ((int) $aRS->fields["numrec"] > 0)
			{
				return false;
			}
		}
		
		unset($aRS);
		
		return true;
	}

	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
			$myChildren[] = BITAMCatalogFilterCollection::NewInstance($this->Repository, $this->CatalogID);
		}
		
		return $myChildren;
	}
	
	static function getCatalogs($aRepository, $includeNone = false, $level = null, $withKeyOfAgendas = null, $aSectionID = null, $valSectionNumber=null)
	{
		if (!is_null($withKeyOfAgendas) && $withKeyOfAgendas) {
			$sql = "SELECT DISTINCT(A.CatalogID), A.CatalogName FROM SI_SV_Catalog A, si_sv_catalogmember B WHERE A.CatalogID = B.CatalogID AND B.KeyOfAgenda > 0 ORDER BY A.CatalogName";
		} else {
			if(is_null($level) || $level<1)
			{
				$sql = "SELECT CatalogID, CatalogName FROM SI_SV_Catalog";
				
				if($aSectionID!==null)
				{
					//El parámetro $sectionID sólo será diferente de null cuando 
					//los catalógos se soliciten para: Preguntas y Secciones
					//Validar a nivel pregunta que en el combo de catálogos 
					//no se desplieguen los catálogos que están siendo utilizados 
					//en otra sección dinámica o sección inlines o 
					//preguntas de sección inline o preguntas de otra sección maestro detalle
					$sql.= " WHERE CatalogID NOT IN (SELECT CatalogID FROM SI_SV_Section WHERE SectionID <> ".$aSectionID." AND SectionType IN (".sectDynamic.",".sectMasterDet.",".sectInline.")
					AND SurveyID IN (SELECT SurveyID FROM SI_SV_Section WHERE SectionID=".$aSectionID.")";
					if($valSectionNumber!==null)
					{
						$sql.= " AND SectionNumber < ".$valSectionNumber;
					}
					$sql.=")
					AND CatalogID NOT IN 
					(SELECT Q.CatalogID 
					FROM SI_SV_Question Q, SI_SV_Section S
					WHERE Q.SectionID=S.SectionID AND
					Q.SectionID <> ".$aSectionID." 
					AND S.SectionType IN (".sectDynamic.",".sectMasterDet.",".sectInline.")
					AND S.SurveyID IN (SELECT SurveyID FROM SI_SV_Section WHERE SectionID=".$aSectionID.")";
					if($valSectionNumber!==null)
					{
						$sql.= " AND S.SectionNumber < ".$valSectionNumber;
					}
					
					$sql.=")";
				}
				
				$sql.= " ORDER BY CatalogName";
			}
			else 
			{
				$sql = "SELECT COUNT(b.CatalogID), b.CatalogID, b.CatalogName FROM SI_SV_CatalogMember a, SI_SV_Catalog b
						WHERE a.CatalogID = b.CatalogID GROUP BY b.CatalogID, b.CatalogName HAVING COUNT(b.CatalogID) = ".$level;
			}
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrayCatalogs = array();
		
		if($includeNone==true)
		{
			$arrayCatalogs[0] = translate("None");
		}

		while (!$aRS->EOF)
		{
			$catalogID = $aRS->fields["catalogid"];
			$catalogName = $aRS->fields["catalogname"];
			$arrayCatalogs[$catalogID] = $catalogName;
			
			$aRS->MoveNext();
		}
		
		return $arrayCatalogs;
	}
	
	//@JAPR 2012-07-04: Corregido un bug en Agendas, no se permitía crear agendas de encuestas que no estuvieran en Schedulers porque no se
	//listaban sus catálogos
	//Obtiene la lista de catálogos que están asociados a Encuestas/Preguntas
	//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
	//@JAPR 2014-10-28: Agregado el mapeo de Scores
	//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
	//Agregado el parámetro $anArrayOfSurveyIDs para permitir filtrar para ciertas agendas
	static function GetUsedCatalogs($aRepository, $anArrayOfSurveyIDs = null, $withKeyOfAgenda = null)
	{
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND B.SurveyID = ".((int)$anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = " AND B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2015-03-19: Corregido un bug, la función no estaba considerando a los catálogos directos de secciones Inline (#V95KIU)
		if (!is_null($withKeyOfAgenda) && $withKeyOfAgenda) {
			$sql = "SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Survey B, SI_SV_CatalogMember C ".
				"WHERE A.CatalogID = B.CatalogID AND A.CatalogID = C.CatalogID AND C.KeyOfAgenda > 0 ".
					"AND B.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"UNION ".
				"SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Question B, SI_SV_Survey C, SI_SV_CatalogMember D ".
				"WHERE A.CatalogID = B.CatalogID AND A.CatalogID = D.CatalogID AND D.KeyOfAgenda > 0 ".
					"AND B.SurveyID = C.SurveyID AND C.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"UNION ".
				"SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Section B, SI_SV_Survey C, SI_SV_CatalogMember D ".
				"WHERE A.CatalogID = B.CatalogID AND A.CatalogID = D.CatalogID AND D.KeyOfAgenda > 0 ".
					"AND B.SurveyID = C.SurveyID AND C.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"ORDER BY 2";
		} else {
			$sql = "SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Survey B ".
				"WHERE A.CatalogID = B.CatalogID ".
					"AND B.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"UNION ".
				"SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Question B, SI_SV_Survey C ".
				"WHERE A.CatalogID = B.CatalogID ".
					"AND B.SurveyID = C.SurveyID AND C.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"UNION ".
				"SELECT DISTINCT A.CatalogID, A.CatalogName ".
				"FROM SI_SV_Catalog A, SI_SV_Section B, SI_SV_Survey C ".
				"WHERE A.CatalogID = B.CatalogID ".
					"AND B.SurveyID = C.SurveyID AND C.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0 {$filter} ".
				"ORDER BY 2";
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog, SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			do
			{
				$allValues[$aRS->fields["catalogid"]] = $aRS->fields["catalogname"];
  				$aRS->MoveNext();
			} while (!$aRS->EOF);
		}
		
		return $allValues;
	}
	//@JAPR
	
	//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
	//Obtiene todos los elementos de la dimensión Categoría si esta se encuentra definida
	static function GetCategories($aRepository, $includeNone = false, $bGetForService = false) {
		require_once('settingsvariable.inc.php');
		$objSettings = BITAMSettingsVariable::NewInstanceAll($aRepository);
		$categoryCatalogID = 0;
		$categoryAttributeID = 0;
		if (isset($objSettings->ArrayVariableValues['CATEGORYCATALOG']))
		{
			$categoryCatalogID = abs((int) $objSettings->ArrayVariableValues['CATEGORYCATALOG']);
		}
		if (isset($objSettings->ArrayVariableValues['CATEGORYMEMBER']))
		{
			$categoryAttributeID = abs((int) $objSettings->ArrayVariableValues['CATEGORYMEMBER']);
		}
		
		$arrCategories = array();
		if ($includeNone)
		{
			if ($bGetForService)
			{
		        $arrCategories[] = array(
		            'categoryID' => 0,
		            'categoryName' => 'None'
		        );
			}
			else 
			{
				$arrCategories[0] = translate('None');
			}
		}
		
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		//A partir de esta versión se utilizarán los DataSources
		if ($categoryCatalogID > 0 && $categoryAttributeID > 0) {
			//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$aCatalog = $strCalledClass::NewInstanceWithID($aRepository, $categoryCatalogID);
			$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $categoryAttributeID);
			if (!is_null($aCatalog) && !is_null($aCatalogMember)) {
				if ($this->CreationAdminVersion >= esveFormsv6) {
					//Los datos se obtiene a partir del DataSource correspondiente
					return $arrCategories;
				}
				
				//@JAPR 2014-05-27: Agregados los catálogos tipo eBavel
				if ($aCatalog->eBavelAppID > 0 && $aCatalog->eBavelFormID > 0) {
					//En este caso se trata de un catálogo mapeado desde eBavel, en lugar de reutilizar BITAMReportCollection como se hace en catálogos propios, se
					//utilizará la funcionalidad de catalogFilter, donde ya se había implementado el método casi idéntico a lo requerido aqui, así de esa forma no
					//se duplicará código como sucedió con BITAMReportCollection, ya que el mismo tipo de consulta en catálogos normales también estaba en catalogFilter
					$aCatalogFilter = BITAMCatalogFilter::NewInstance($aRepository, $categoryCatalogID, true);
					
					$aRS = $aCatalogFilter->getCatalogValues($aCatalogMember->MemberOrder -1, true, true);
					if ($aRS && is_object($aRS) && property_exists($aRS, 'EOF') && !$aRS->EOF) {
						$fieldKey = "id_".$aCatalogFilter->FormID;
						//Este RecordSet sólo debería traer un único campo, así que es seguro recorrer el Array y cortar al primer valor
						foreach ($aCatalogFilter->RSFields as $strAttribDesc => $strFieldName) {
							break;
						}
						
						while (!$aRS->EOF) {
							$categoryID = (int) @$aRS->fields[$fieldKey];
							$categoryName = (string) @$aRS->fields[$strFieldName];
				        	if ($bGetForService)
				        	{
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						        $arrCategories[] = array(
						            'categoryID' => $categoryID,
						            'categoryName' => $categoryName
						        );
				        	}
				        	else 
				        	{
				        		$arrCategories[$categoryID] = $categoryName;
				        	}
							$aRS->MoveNext();
						}
					}
				}
				else {
					$fieldKey = "RIDIM_".$aCatalog->ParentID."KEY";
					$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
			        $sql = "SELECT MIN(".$fieldKey.") AS CategoryID, ".$fieldName." AS CategoryName FROM RIDIM_".$aCatalog->ParentID." WHERE ".$fieldKey." > 1 GROUP BY ".$fieldName." ORDER BY 2";
			        $aRS = $aRepository->DataADOConnection->Execute($sql);
			        if ($aRS) 
			        {
				        while (!$aRS->EOF)
				        {
							$categoryID = (int) $aRS->fields["categoryid"];
							$categoryName = (string) $aRS->fields["categoryname"];
				        	if ($bGetForService)
				        	{
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						        $arrCategories[] = array(
						            'categoryID' => $categoryID,
						            'categoryName' => $categoryName
						        );
				        	}
				        	else 
				        	{
				        		$arrCategories[$categoryID] = $categoryName;
				        	}
				            $aRS->MoveNext();
				        }
			        }
				}
				//@JAPR
			}
		}
		
		return $arrCategories;
	}
	
	//Obtiene los datos del catálogo de usuarios (los valores en la dimensión creada para tal fin, no directamente de SI_USUARIO ni SI_SV_Users)
	static function GetUsers($aRepository, $includeNone = false, $bGetForService = false)
	{
		require_once('user.inc.php');
		
		$arrUsers = array();
		if ($includeNone)
		{
			if ($bGetForService)
			{
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		        $arrUsers[] = array(
		            'userID' => -1,
		            'userName' => 'None'
		        );
			}
			else 
			{
				$arrUsers[-1] = translate('None');
			}
		}
		
		$theUserName = $_SESSION["PABITAM_UserName"];		
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$theUser = BITAMeFormsUser::WithUserName($aRepository, $theUserName);
		$theUserID = -1;
		if (!is_null($theUser))
		{
			if ($bGetForService)
			{
				$theUserID = $theUser->UserID;
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		        $arrUsers[] = array(
		            'userID' => $theUser->UserID,
		            'userName' => $theUser->FullName
		        );
			}
			else 
			{
				$arrUsers[$theUser->UserID] = $theUser->FullName;
			}
		}
		
		//Verifica si ya existe la dimensión Usuario, si no es así entonces sólo regresa el usuario logeado y el None si es que se pidió
		$userDimClaDescrip = 0;
		$sql = "SELECT UserDimID FROM SI_SV_GblSurveyModel";
        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset)
        {
        	$userDimClaDescrip = abs((int) $recordset->fields["userdimid"]);
        }
        if ($userDimClaDescrip > 0)
        {
			$fieldKey = "RIDIM_".$userDimClaDescrip."KEY";
	        $sql = "SELECT DISTINCT ".$fieldKey." AS UserId, DSC_".$userDimClaDescrip." AS UserName FROM RIDIM_".$userDimClaDescrip." WHERE ".$fieldKey." > 0 ORDER BY 2";
	        $recordset = $aRepository->DataADOConnection->Execute($sql);
	        if ($recordset)
	        {
		        $arrCatalogIDs = array();
		        while (!$recordset->EOF) {
					$userID = (int) $recordset->fields["userid"];
					$userName = (string) $recordset->fields["username"];
					if ($bGetForService)
					{
						if ($userID != $theUserID)
						{
							//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					        $arrUsers[] = array(
					            'userID' => $userID,
					            'userName' => $userName
					        );
						}
					}
					else 
					{
						$arrUsers[$userID] = $userName;
					}
		            $recordset->MoveNext();
		        }
	        }
        }
		
        return $arrUsers;
	}
	//@JAPR
	
	//@JAPR 2013-05-27: Agregado el proceso de exportación de catálogos
	/* Genera el archivo XLS (realmente es un .csv) que contiene los datos de todos los atributos ordenados por la jerarquía actualmente definida
	o bien por el key dependiendo de si existen o no mas encuestas que usen el catálogo pero que aun no se encuentren migradas a dimensiones
	independientes del mismo, este archivo puede ser reimportado posteriormente para cargar nuevos valores o actualizar los existentes usando el
	mismo criterio de encuestas en el nuevo/viejo formato
	*/
	//@JAPRDescontinuada en v6
	function exportXLSTemplate() {
		$blnWriteFile = true;
		
		//Verifica si hay o no atributos en el catálogo
		$intNumAttribs = 0;
		$sql = "SELECT COUNT(*) AS NumAttribs FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS !== false && !$aRS->EOF) {
			$intNumAttribs = (int) @$aRS->fields["numattribs"];
		}
		
		if ($intNumAttribs == 0) {
			$blnWriteFile = false;
			reportError(-1, translate("You require at least one attribute to continue with this process"), false, true);
			return $blnWriteFile;
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$arrAttributesColl = $strCalledClass::GetCatalogAttributes($this->Repository, $this->CatalogID, 0);
		if (!is_array($arrAttributesColl)) {
			$arrAttributesColl = array();
		}
		
		$strFields = "";
		$strFieldsGroup = "";
		$lim = count($arrAttributesColl);
		
		$sfile = 'CatalogTemplate-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$spathfile = GeteFormsLogPath().$sfile;
		//@JAPR
		$LN = chr(13).chr(10);
		$line = "";
		//for($i=0; $i<$lim; $i++)
		foreach ($arrAttributesColl as $i => $objAttribute)
		{
			if($strFields!="")
			{
				$strFields.=",";
				$strFieldsGroup.=",";
				$line.=",";
			}
			
			$attribID = $arrAttributesColl[$i]["dimID"];
			$strFields.="DSC_".$attribID;
			$strFieldsGroup.="DSC_".$attribID;
			/*@AAL 13/01/2015 Se decodifica el Nombre de los Atributos puesto que cuando se Exportaba a Excel
		      se mostraban caractéres raros */
			$line.='"'.str_replace('"', '""', decodeUTF8((string) @$arrAttributesColl[$i]["name"])).'"';
			//@AAL
		}
		
		//Crea/Abre el archivo y genera el header con los nombres de atributos
		$fStats = fopen($spathfile, 'w+');
		if ($fStats)
		{
			if(!fwrite($fStats, $line))
			{
				$blnWriteFile = false;
				reportError(-1, translate("Unable to write file contents").": ".$spathfile, false, true);
			}
		}
		else
		{
			$blnWriteFile = false;
			reportError(-1, translate("Unable to open the file").": ".$spathfile, false, true);
		}
		
		if (!$blnWriteFile) {
			return $blnWriteFile;
		}
		
		$fieldKey = $this->TableName."KEY";
		$tableName = $this->TableName;

		//Verifica si aun hay encuestas que utilicen a este catálogo y estén en el formato sin dimensiones desasociadas
		if ($this->DissociateCatDimens == 0) {
			$strFieldsGroup = $fieldKey;
		}
		else {
			$intDissociateCatDimens = 1;
			if (getMDVersion() >= esvCatalogDissociation) {
				$sql = "SELECT DISTINCT A.DissociateCatDimens 
					FROM SI_SV_Survey A, SI_SV_Question B 
					WHERE A.SurveyID = B.SurveyID AND B.QTypeID = ".qtpSingle." AND B.CatalogID = ".$this->CatalogID;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS !== false) {
					while(!$aRS->EOF) {
						$intDissociateCatDimens = (int) @$aRS->fields["dissociatecatdimens"];
						if ($intDissociateCatDimens == 0) {
							break;
						}
						$aRS->MoveNext();
					}
				}
			}
			
			if ($intDissociateCatDimens == 0) {
				$strFieldsGroup = $fieldKey;
			}
		}
		
		$where = " WHERE ".$fieldKey." <> 1 ";
		//$where .= $filter;
		
		//@JAPR 2013-05-28: Corregido un bug, estaba haciendo un DISTINCT
		//$sql = "SELECT DISTINCT ".$strFields." FROM ".$tableName.$where." ORDER BY ".$strFieldsGroup;
		$sql = "SELECT ".$strFields." FROM ".$tableName.$where." ORDER BY ".$strFieldsGroup;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			$blnWriteFile = false;
			reportError(-1, translate("Error executing SQL statement").": ".$sql.". ".translate("Error").": ".$this->Repository->DataADOConnection->ErrorMsg(), false, true);
		}
		
		$intLineNumber = 1;
		while($blnWriteFile && !$aRS->EOF)
		{
			$strAnd = '';
			$line = $LN;
			//for($i=0; $i<$lim; $i++)
			foreach ($arrAttributesColl as $i => $objAttribute)
			{
				$field = "DSC_".$arrAttributesColl[$i]["dimID"];
				$attribVal = (string) @$aRS->fields[strtolower($field)];
				$line.= $strAnd.'"'.str_replace('"', '""', $attribVal).'"';
				$strAnd = ',';
			}
			
			if(!fwrite($fStats, $line))
			{
				$blnWriteFile = false;
				reportError(-1, translate("Unable to write file contents")." (".translate("line")."#$intLineNumber: ".$spathfile, false, true);
			}
			$intLineNumber++;
			$aRS->MoveNext();
		}
		
		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				$blnWriteFile = false;
				reportError(-1, translate("File not found").": ".$spathfile, false, true);
			}
			else
			{
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			$blnWriteFile = false;
			reportError(-1, translate("Error exporting the catalog").": ".$spathfile, false, true);
		}
		
		return $blnWriteFile;
	}
	
	//@JAPR 2014-06-11: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
	//Regresa el filtro estático de catálogo para el usuario especificado
	function generateResultFilterByUser($userID) {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			//Los datos se obtiene a partir del DataSource correspondiente
			$resultFilter = "";
			return $resultFilter;
		}
		//@JAPR
		
		$resultFilter = "";
		$arrayTmpFilters = array();	
		$arrayFiltersByUser = array();
		$arrayFiltersByRol = array();
		$arrayFilters = array();
		
		//Obtenemos todos los filtros para el usuario especificado pero primero
		//obtenemos los roles de dicho usuario
		$roles = BITAMAppUser::getArtusRolesByClaUsuario($this->Repository, $userID);
		
		$sql = "SELECT FilterID FROM SI_SV_CatFilter WHERE CatalogID = ".$this->CatalogID." ORDER BY FilterID";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		while(!$aRS->EOF)
		{
			$filterID = (int) $aRS->fields["filterid"];
			$arrayTmpFilters[$filterID] = $filterID;
			
			$aRS->MoveNext();
		}
		
		//Si no hay filtros especificados para dicho catalogo entonces se envia un filtro vacio
		if(count($arrayTmpFilters)<=0)
		{
			$resultFilter = "";
		}
		else 
		{
			//Obtenemos los filtros que si estan asociados al usuario
			$sql = "SELECT FilterID FROM SI_SV_CatFilterUser 
					WHERE UserID = ".$userID." AND FilterID IN (".implode(",", $arrayTmpFilters).")";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			while(!$aRS->EOF)
			{
				$filterID = (int) $aRS->fields["filterid"];
				$arrayFiltersByUser[$filterID] = $filterID;
				
				$aRS->MoveNext();
			}
			
			if(count($roles)>0)
			{
				//Obtenemos los filtros que si estan asociados al usuario pero por rol
				$sql = "SELECT FilterID FROM SI_SV_CatFilterRol 
						WHERE RolID IN (".implode(",", $roles).") AND FilterID IN (".implode(",", $arrayTmpFilters).")";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				while(!$aRS->EOF)
				{
					$filterID = (int) $aRS->fields["filterid"];
					$arrayFiltersByRol[$filterID] = $filterID;
					
					$aRS->MoveNext();
				}
			}
			
			//Ahora juntamos todos los elementos resultados de cada array
			foreach ($arrayFiltersByUser as $value)
			{
				$arrayFilters[] = $value;
			}
			
			foreach ($arrayFiltersByRol as $value)
			{
				$arrayFilters[] = $value;
			}
			
			//Ahora se obtienen los valores unicos y se reordenan
			$arrayFilters = array_values(array_unique($arrayFilters));
			
			//Verificamos si tiene algun filtro el usuario en caso contrario
			//de que no lo tenga entonces se devuelve un filtro vacio
			if(count($arrayFilters)<=0)
			{
				$resultFilter = "";
			}
			else 
			{
				//Se obtienen todos los filtros de la tabla SI_SV_CatFilter de acuerdo a los filtros que tenga el usuario asociado
				$sql = "SELECT FilterID, FilterText, FilterLevels FROM SI_SV_CatFilter WHERE FilterID IN (".implode(",", $arrayFilters).") ORDER BY FilterText";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
				//Durante el recorrido de los filtros verificamos si alguno de ellos se encuentra vacio
				//si es asi eso quiere decir que se va a incluir todos elementos por lo tanto 
				//se devolvera un filtro vacio
				$hasAllElements = false;
				$arrayAllFilters = array();
				$i=0;
				
				while(!$aRS->EOF)
				{
					$arrayAllFilters[$i] = array();
					
					$filtertext = $aRS->fields['filtertext'];
					$filterlevels = $aRS->fields['filterlevels'];
					
					$arrayAllFilters[$i]['FilterText'] = $filtertext;
					$arrayAllFilters[$i]['FilterLevels'] = $filterlevels;
					$arrayAllFilters[$i]['IsIncluded'] = 0;
					
					if(trim($filtertext)=="")
					{
						$hasAllElements = true;
					}
					
					$i++;
					$aRS->MoveNext();
				}
				
				//Si hay un filtro que incluya todos los elementos entonces no se deberia 
				//realizar el proceso de eliminar filtros repetitivos y dejar filtros inclusivos
				if($hasAllElements==true)
				{
					$resultFilter = "";
				}
				else 
				{
					//Obtenemos solamente los filtros inclusivos
					$arrayInclusiveFilters = $this->getAllInclusiveFilters($arrayAllFilters);
					
					//Se intenta generar el filtro con solo los filtros inclusivos
					foreach ($arrayInclusiveFilters as $aFilter)
					{
						$filtertext = $aFilter['FilterText'];
						$filterlevels = $aFilter['FilterLevels'];
						//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
						$blneBavelCatalog = false;
						$filterCollection = BITAMFilteredSVCollection::NewInstanceByStrFilter($this->Repository, $filtertext, $filterlevels, -1, $this->CatalogID);
						if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
							$blneBavelCatalog = true;
						}
						//@JAPR
						$strFilter = "";
						
						foreach ($filterCollection as $element)
						{
							$fieldName = $element->nom_fisicok;
							$fieldValue = $element->values[0];
							//@JAPR 2014-06-12: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
							if ($blneBavelCatalog) {
								$fieldName = (string) @$element->formFieldID;
								if (trim($fieldName) == '') {
									continue;
								}
							}
							//@JAPR
							
							if($strFilter!="")
							{
								$strFilter.=" AND ";
							}
							
							$strFilter.=$fieldName." = ".$this->Repository->DataADOConnection->Quote($fieldValue);
						}
						
						if($strFilter!="")
						{
							if($resultFilter!="")
							{
								$resultFilter.=" OR ";
							}
							
							$resultFilter.="(".$strFilter.")";
						}
					}
					
					if($resultFilter!="")
					{
						$resultFilter = "(".$resultFilter.")";
					}
				}
			}
		}
		
		//Verificamos si de casualidad estamos en una captura web la cual provino
		//desde el link Edit de la pantalla de Reports
		//Si lo es entonces verificamos si el usuario logueado es el propietario 
		//del repositorio, si lo llegara a ser, entonces no tendría ningún filtro
		//los catalogos desplegados en la encuesta
		if (isset($_SESSION["SVFromESurveyAdmin"]) && $_SESSION["SVFromESurveyAdmin"] == 1) {

                    if (isset($_SESSION["SVExternalEmailFilter"]) && $_SESSION["SVExternalEmailFilter"] != 0
                            && isset($_SESSION["SVExternalCatalogID"]) && $_SESSION["SVExternalCatalogID"] == $this->CatalogID) {

                        if ($_SESSION["SVExternalEmailFilter"] == 1) {
                            //Filtro por Registro
                            $fieldKey = "RIDIM_" . $this->ParentID . "KEY";
                            $fieldValue = $_SESSION["SVExternalEmailKey"];

                            $resultFilter = $fieldKey . " = " . $fieldValue;
                            $resultFilter = "(" . $resultFilter . ")";

                            return $resultFilter;
                        } else if ($_SESSION["SVExternalEmailFilter"] == 2) {
                            //Filtro por DISTINCT Atributo
                            $schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($this->Repository, $_SESSION["SVExternalSchedulerID"]);
                            $catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $schedulerInstance->AttributeID);

                            $fieldDsc = "DSC_" . $catMemberInstance->ClaDescrip;
                            $fieldValue = $this->Repository->DataADOConnection->Quote($_SESSION["SVExternalEmail"]);

                            $resultFilter = $fieldDsc . " = " . $fieldValue;
                            $resultFilter = "(" . $resultFilter . ")";

                            return $resultFilter;
                        }
                    } else {

                        if (isset($_SESSION['saasuser']) && isset($_SESSION["dbid"])) {
                            require_once("utils.inc.php");

                            $userid = $_SESSION['saasuser'];
                            $dbid = $_SESSION["dbid"];

                            //Verificamos si es un saasuser administrator
                            $isAdmin = isSAASUserAdmin($userid, $dbid);

                            if ($isAdmin) {
                                $resultFilter = "";
                                return $resultFilter;
                            }
                        }
                    }
                }

		//Revisamos si la captura web proviene desde un link de un correo electronico
		//Si lo llegara ser, entonces revisamos si hay un EmailFilter y que este 
		//establecido a un valor diferente de None
		//Si se cumple la condicion entonces tampoco se envian los filtros de catalogo
		//ya que en esta encuesta se filtrara por otra condicion, que es el EmailFilter
		if(isset($_SESSION["SVExternalPage"]) && $_SESSION["SVExternalPage"]==1)
		{
			if(isset($_SESSION["SVExternalEmailFilter"]) && $_SESSION["SVExternalEmailFilter"]!=0 
				&& isset($_SESSION["SVExternalCatalogID"]) && $_SESSION["SVExternalCatalogID"]==$this->CatalogID)
			{
				if($_SESSION["SVExternalEmailFilter"]==1)
				{
					//Filtro por Registro
					$fieldKey = "RIDIM_".$this->ParentID."KEY";
					$fieldValue = $_SESSION["SVExternalEmailKey"];
					
					$resultFilter = $fieldKey." = ".$fieldValue;
					$resultFilter = "(".$resultFilter.")";
					
					return $resultFilter;
				}
				else if ($_SESSION["SVExternalEmailFilter"]==2)
				{
					//Filtro por DISTINCT Atributo
					$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($this->Repository, $_SESSION["SVExternalSchedulerID"]);
					$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $schedulerInstance->AttributeID);
					
					$fieldDsc = "DSC_".$catMemberInstance->ClaDescrip;
					$fieldValue = $this->Repository->DataADOConnection->Quote($_SESSION["SVExternalEmail"]);

					$resultFilter = $fieldDsc." = ".$fieldValue;
					$resultFilter = "(".$resultFilter.")";
					
					return $resultFilter;
				}
			}
		}

		return $resultFilter;
	}
	
	//@JAPRDescontinuada a partir de v6
	function getAllInclusiveFilters($arrayAllFilters)
	{
		$i=0;
		$m=0;
		$arrayInclusiveFilters = array();
		$countAllFilters = count($arrayAllFilters);
		
		for($i=0; $i<$countAllFilters; $i++)
		{
			if($arrayAllFilters[$i]['IsIncluded']==1)
			{
				continue;
			}
			
			$filterText = $arrayAllFilters[$i]['FilterText'];
			$lenFilterText = strlen($filterText);
			
			for($j=$i+1; $j<$countAllFilters; $j++)
			{
				$otherFilterText = $arrayAllFilters[$j]['FilterText'];
				$partOtherFilterText = substr($otherFilterText, 0, $lenFilterText);
				
				if($filterText==$partOtherFilterText)
				{
					$arrayAllFilters[$j]['IsIncluded'] = 1;
				}				
			}
			
			$arrayAllFilters[$i]['IsIncluded'] = 1;
			
			$arrayInclusiveFilters[$m] = array();
			$arrayInclusiveFilters[$m]['FilterText'] = $arrayAllFilters[$i]['FilterText'];
			$arrayInclusiveFilters[$m]['FilterLevels'] = $arrayAllFilters[$i]['FilterLevels'];
			
			$m++;
		}
		
		return $arrayInclusiveFilters;
	}
	
	//@JAPRDescontinuada a partir de v6
	static function existAttribNameInCatalogs($aRepository, $attributeName)
	{
		$strRepositoryName = $aRepository->ADODBDatabase;
		$strDataWarehouseName = $aRepository->DataADODBDatabase;
		
		//Consulta para checar si existe ese nombre de atributo en los catalogos
		$sql = "SELECT A.ParentID FROM ".$strDataWarehouseName.".SI_SV_Catalog A INNER JOIN ".$strRepositoryName.".SI_DESCRIP_ENC B 
				ON A.ParentID = B.CLA_DESCRIP WHERE B.NOM_LOGICO = ".$aRepository->ADOConnection->Quote($attributeName);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog, SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($aRS->EOF)
		{
			//Consulta para checar si existe ese nombre de atributo en los atributos de los catalogos
			$sql = "SELECT A.ParentID FROM ".$strDataWarehouseName.".SI_SV_CatalogMember A INNER JOIN ".$strRepositoryName.".SI_DESCRIP_ENC B 
					ON A.ParentID = B.CLA_DESCRIP WHERE B.NOM_LOGICO = ".$aRepository->ADOConnection->Quote($attributeName);
			$aRS2 = $aRepository->ADOConnection->Execute($sql);
			if ($aRS2 === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember, SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if($aRS2->EOF)
			{
				return false;	
			}
			else 
			{
				return true;
			}
		}
		else 
		{
			return true;
		}
	}
	
	//@JAPR 2012-03-08: Agregado el indicador de versión por registro para descarga inteligente de catálogos
	//Si la dimensión representa un catálogo, verifica que tenga la columna para la versión de sus registros y actualiza en la instancia
	//el valor de la última versión y el nombre de campo del indicador Versión utilizado en la tabla
	//La función regresa el número máximo de versión actualmente grabado, si la dimensión no es catálogo entonces regresa -1
	//@JAPRDescontinuada en v6
	function getCatalogVersion() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return $this->MaxVersionNum;
		}
		//@JAPR
		
		$this->VersionFieldName = '';
		$this->MaxVersionNum = -1;
		$this->VersionIndID = $this->saveVersionIndicator();
		if ($this->VersionIndID > 0)
		{
			$sql = 'select A.NOM_FISICO from SI_CPTO_ATRIB A, SI_INDICADOR B where A.CLA_CONCEPTO = B.CLA_CONCEPTO AND A.CONSECUTIVO = B.ID_CPTO_ATRIB AND A.CLA_CONCEPTO = '.$this->ModelID.' and B.CLA_INDICADOR = '.$this->VersionIndID;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if($aRS!==false && !$aRS->EOF)
			{
				$this->VersionFieldName = trim((string) $aRS->fields['nom_fisico']);
				if ($this->VersionFieldName != '')
				{
					$sql = 'SELECT '.$this->Repository->DataADOConnection->IfNull('MAX('.$this->VersionFieldName.')', '0').' AS MaxVersion FROM '.$this->TableName;
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					if($aRS!==false && !$aRS->EOF)
					{
						//Cada vez que se modifica un valor se actualiza el número de versión para que al descargar la
						//aplicación se obtengan los catálogos cuando por lo menos tengan un valor de una versión posterior
						//a la del dispositivo que está sincronizando
						$this->MaxVersionNum = (int) $aRS->fields['maxversion'];
					}
				}
			}
		}
		
		return $this->MaxVersionNum;
	}

	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
	function getJSonDefinition($bGetValues = true) {
		//@JAPR 2015-11-02: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		
		$arrDef = array();
		
		$arrDef['id'] = $this->CatalogID;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['name'] = $this->CatalogName;
		//@JAPR 2014-08-26: Validado que por el momento los catálogos mapeados desde eBavel se descarguen siempre como modificados, ya que pueden
		//contener cambios que eForms no tiene forma de identificar
		$intVersionNum = $this->VersionNum;
		//@JAPR 2016-01-06: Corregido un bug, los DataSource tipo Tabla también deben regresar constantemente los datos sin mantener versión local (#012P4U)
		$blnTable = false;
		$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
		//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
		$intDataSourceType = dstyManual;
		if (!is_null($objDataSource)) {
			$intDataSourceType = $objDataSource->Type;
			if ($intDataSourceType == dstyTable) {
				$blnTable = true;
			}
		}
		//@JAPR
		if (($this->eBavelAppID > 0 && $this->eBavelFormID > 0) || $blnTable) {
		//if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
			$intVersionNum = -1;
		}
		$arrDef['versionNum'] = $intVersionNum;
		//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
		$arrDef['dataVersionNum'] = $this->DataVersionNum;
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if ($this->eBavelAppID && $this->eBavelFormID) {
			$arrDef['eBavelFormID'] = $this->eBavelFormID;
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$arrDef['dataSourceID'] = $this->DataSourceID;
		//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
		$arrDef['dataSourceType'] = $intDataSourceType;
		//@JAPR
		
		$intMaxOrder = 0;
		//Obtiene la posición máxima del atributo usado ya sea en sección (mas uno) o como pregunta, ya que se asume que el catálogo será usado
		//sólo hasta esos niveles por lo que cualquier nivel posterior sería un desperdicio de memoria y tiempo de transferencia
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
		//Dado a que en v6 los catálogos están únicamente ligados a una misma pregunta, obtiene además los DataSourceFilter aplicables para este catálogo según si es de una sección
		//o una pregunta, siempre habría sólo uno por cada caso pero aunque hubiera varios en el peor de los casos podría concatenar todos, aunque eso no sería correcto
		$strDataSourceFilter = '';
		//@JAPR 2018-07-13 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//No tiene caso ejecutar el query para obtener el máximo atributo usado ni el filtro dinámico, si finalmente no se pidieron obtener los valores
		//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
		if ($bGetValues) {
			$sql = "SELECT A.CatalogID, MAX(B.MemberOrder) +1 AS MemberOrder, A.DataSourceFilter 
				FROM SI_SV_Section A, SI_SV_CatalogMember B 
				WHERE A.CatalogID = B.CatalogID AND A.CatMemberID = B.MemberID AND A.CatalogID = ".$this->CatalogID." AND A.CatMemberID > 0 
				GROUP BY A.CatalogID, A.DataSourceFilter 
				
				UNION 
				
				SELECT A.CatalogID, MAX(B.MemberOrder) AS MemberOrder, A.DataSourceFilter 
				FROM SI_SV_Question A, SI_SV_CatalogMember B 
				WHERE A.CatalogID = B.CatalogID AND A.CatMemberID = B.MemberID AND A.CatalogID = ".$this->CatalogID." AND A.CatMemberID > 0 
				GROUP BY A.CatalogID, A.DataSourceFilter";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				while (!$aRS->EOF) {
					$intNewMaxOrder = (int) $aRS->fields["memberorder"];
					if ($intNewMaxOrder > $intMaxOrder) {
						$intMaxOrder = $intNewMaxOrder;
					}
					//Por ahora tomará el último de los filtros, pero no se debería dar una situación donde existan varios
					$strDataSourceFilter = (string) $aRS->fields["datasourcefilter"];
					$aRS->MoveNext();
				}
			}
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    $intMaxOrder = 0;
	    //@JAPR
		$arrDef['attributes'] = $strCalledClass::GetCatalogAttributes($this->Repository, $this->CatalogID, $intMaxOrder);
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		//Modificado el proceso de obtención de la definición de catálogos para que en caso de tratarse de un catálogo tipo modelo, automáticamente se filtren los valores por la plataforma
		//que realizó el request (en caso de haber alguna, de lo contrario no se agregará filtro para que continue con valores aunque no se puedan utilizar en el contexto de la pregunta),
		//para esto modificará el posible filtro que tuviera la pregunta (solo aplica para cuando pide valores dinámicos, si es un catálogo con valores estáticos entonces esta modificación
		//realmente asignará el filtro) para agregar la condición por plataforma correspondiente
		if ( $objDataSource->IsModel && $this->DataSourcePlatformMemberID > 0 ) {
			$blnHasDynamicFilter = (trim($strDataSourceFilter) != '');
			$strAnd = ($blnHasDynamicFilter)?" AND ":"";
			$strDeviceFilter = '';
			$strDeviceValue = '';
			$intDeviceID = identifyDeviceType();
			switch ($intDeviceID) {
				case dvciPod:
				case dvciPad:
				case dvciPadMini:
				case dvciPhone:
					//Se trata de IOs
					$strDeviceValue = 'ios';
					break;
				case dvcCel:
				case dvcTablet:
					//Se trata de Android
					$strDeviceValue = 'android';
					break;
				default:
					//Cualquier dispositivo no identificado no podrá aplicar el filtro, esto aplica para procesos de Browser o fuera del App de eForms (como el Administrador)
					break;
			}
			
			if ( $strDeviceValue ) {
				//Si se trata de un dispositivo válido, deberá obtener la referencia al atributo plataforma para generar un filtro con él
				$strDeviceFilter = '{@DMID'.$this->DataSourcePlatformMemberID.'} = '.$this->Repository->DataADOConnection->Quote($strDeviceValue);
			}
			
			if ( $strDeviceFilter ) {
				$strDataSourceFilter .= $strAnd.$strDeviceFilter;
			}
		}
		//@JAPR
		
		$arrDef['attributesOrder'] = array();
		foreach ($arrDef['attributes'] as $aMemberID) {
			$arrDef['attributesOrder'][] = (int) $aMemberID['id'];
		}
		//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
		//$arrDef['attributeValues'] = BITAMCatalog::GetCatalogAttributeValues($this->Repository, $this, array_values($arrDef['attributes']));
		//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
		if ($bGetValues) {
			//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
			//Dado a que en v6 los catálogos están únicamente ligados a una misma pregunta, obtiene además los DataSourceFilter aplicables para este catálogo según si es de una sección
			//o una pregunta, siempre habría sólo uno por cada caso pero aunque hubiera varios en el peor de los casos podría concatenar todos, aunque eso no sería correcto
			if ($this->DataSourceID > 0 && trim($strDataSourceFilter) != '') {
				//@JAPR 2015-09-30: Corregido un bug, esta función no consideraba DataSources de tipo eBavel, además su función está repetida en TranslateAttributesVarsToFieldNames
				//por lo que se optará por esta última y se descontinuará esta función
				//$strDataSourceFilter = BITAMDataSource::ReplaceMemberIDsByFields($this->Repository, $this->DataSourceID, $strDataSourceFilter);
				$strDataSourceFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($this->Repository, $this->DataSourceID, $strDataSourceFilter);
				//@JAPR 2015-11-02: Corregido un bug con filtros dinámicos en el preview del Admin, las variables de preguntas/secciones no se pueden resolver pero no se reemplazaban y generaban error (#X7ZU1E)
				if ($gbDesignMode) {
					//Si está en modo diseño, nadie reemplaza las variables de preguntas y secciones ya que eso es labor del App, así que las cambia a NULL
					$strDataSourceFilter = ReplaceAppVariablesForDummyValues($strDataSourceFilter);
				}
				//@JAPR
			}
			
			$arrDef['attributeValues'] = $this->getAttributeValues(array_values($arrDef['attributes']), -1, $strDataSourceFilter, null, null, false, true);
			//@JAPR
		}
		else {
			$arrDef['attributeValues'] = array();
		}
		//@JAPR
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		if (getMDVersion() >= esvQuestionAR) {
			$arrDef['isModel'] = $this->IsModel;
			if ( $this->IsModel ) {
				$arrDef['nameAttribID'] = $this->CatModelMemberID;
				$arrDef['versionAttribID'] = $this->CatVersionMemberID;
				$arrDef['zipSrcAttribID'] = $this->CatZipSourceMemberID;
				$arrDef['titleAttribID'] = $this->CatTitleMemberID;
			}
			else {
				//Se debe de mandar la propiedad como 0 en caso de que se hubiera desmarcado como modelo, de lo contrario quedaría indicada pues el $.extend no elimina propiedades del
				//objeto extendido cuando el nuevo objeto no las lleva
				$arrDef['nameAttribID'] = 0;
				$arrDef['versionAttribID'] = 0;
				$arrDef['zipSrcAttribID'] = 0;
				$arrDef['titleAttribID'] = 0;
			}
		}
		
		return $arrDef;
	}
  	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	//@JAPR 2012-08-17: Agregado para v4 el parámetro $iMaxOrder para indicar hasta que atributo se desea recuperar el catálogo, de forma que no
	//se incluyan atributos que no se piensan usar (si se omite, se usarán todos los atributos)
	//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se especifica
	//entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
	//@JAPRWarning: Tratar de estandarizar, esta función debería reutilizar BITAMCatalogMemberCollection
	static function GetCatalogAttributes($aRepository, $aCatalogID, $iMaxOrder = 0, $anArrayAttribIDs = null) {
		global $appVersion;
		
		//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se especifica
		//entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
		//Validación para asegurar que la función será compatible hacia atrás y que no genere error en caso de recibir un parámetro inválido
		if (!is_null($anArrayAttribIDs) && !is_array($anArrayAttribIDs)) {
			$anArrayAttribIDs = array();
		}
		if (count($anArrayAttribIDs) == 0) {
			$anArrayAttribIDs = null;
		}
		
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Optimizado el proceso de carga de atributos exclusivamente cuando se piden todos
		if ( is_null($anArrayAttribIDs) && $iMaxOrder <= 0 && BITAMGlobalFormsInstance::IsCatalogFullyLoadedFromDB($aCatalogID) ) {
			$arrayAttribIDs = BITAMGlobalFormsInstance::GetCatMembersDefByCatalogWithID($aCatalogID);
			return $arrayAttribIDs;
		}
		//@JAPR
		
		//Identifica los atributos que funcionan como llave y que se deben ocultar
        $arrKeyAttributes = array();
		if ($appVersion >= esvFormatMaskHideAttribs)
		{
	        $sql = "SELECT A.MemberID 
	        	FROM SI_SV_CatalogMember A 
	        	WHERE A.CatalogID = $aCatalogID AND A.DescriptorID > 0";
	        $aRS = $aRepository->DataADOConnection->Execute($sql);
	        if ($aRS) {
		        while (!$aRS->EOF) {
		            $intMemberID = (int) $aRS->fields["memberid"];
			        $arrKeyAttributes[$intMemberID] = $intMemberID;
		            $aRS->MoveNext();
		        }
			}
		}
		
		//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', A.IndDimID';
		}
		//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', A.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', A.KeyOfAgenda, A.AgendaLatitude, A.AgendaLongitude, A.AgendaDisplay';
		}
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', A.eBavelFieldDataID';
		}
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$strAdditionalFields .= ', A.IsImage';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.DataSourceMemberID';
		}
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		//Se debe realizar un join con la tabla de preguntas, y dado a que un catálogo sólo puede estar asociado a una única pregunta/sección a partir de v6, no existe posibilidad de 
		//que se pudieran generar múltiples registros por atributo con este query, en los casos donde no exista JOIN se regresará simplemente un NULL en el campo de Question indicando
		//que el atributo no funciona como Ordenamiento en ninguna pregunta, y donde si lo haga servirá para asignar la propiedad que permitirá variar el ORDER BY al obtener los valores
		$strJoinQuestion = '';
		if (getMDVersion() >= esvSChoiceMetro) {
			//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
			$strAdditionalFields .= ', B.QuestionID AS IsQuestionOrder, B.DataMemberOrderDir';
			$strJoinQuestion = "LEFT OUTER JOIN SI_SV_Question B ON A.MemberID = B.CatMemberOrderID ";
		}

	    $sql = "SELECT A.MemberID, A.ParentID, A.MemberName, A.MemberOrder {$strAdditionalFields} 
	    	FROM SI_SV_CatalogMember A {$strJoinQuestion} 
	    	WHERE A.CatalogID = " . $aCatalogID;
	    //@JAPR 2012-08-17: Agregado para v4 el parámetro $iMaxOrder para indicar hasta que atributo se desea recuperar el catálogo
		if ($iMaxOrder > 0 ) {
			$sql .= " AND A.MemberOrder <= ".$iMaxOrder;
		}
	    $sql .= " ORDER BY A.MemberOrder";
	    //@JAPR
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
		//@JAPR 2015-07-28: Validado para que aunque no tenga atributos no detenga el proceso, se debe validar desde donde se invoca este método
	    if (!$aRS) {
	        //conchita 1-nov-2011 ocultar mensajes sql
	        //die("(".__METHOD__.") ".translate("Error accessing"));
	        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
	    }
	    $arrayAttribIDs = array();
	    $i = 0;
		
		//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
		//Indica si hay o no un atributo con ordenamiento, ya que si no lo hay, se tiene que utilizar el ordenamiento propio de la pregunta (cómo cada catálogo sólo pertenece a una
		//única pregunta, esto es posible determinarlo sin conflicto)
		$blnHasSortingAttr = false;
		//@JAPR
	    while (!$aRS->EOF) {
	        $intMemberID = (int) $aRS->fields["memberid"];
			//@JAPR 2014-11-25: Agregado el parámetro $anArrayAttribIDs para especificar directamente que atributos se desean cargar. Si no se
			//especifica entonces se usarán el resto de los parámetros o bien se obtendrán todos los atributos
	        if (is_null($anArrayAttribIDs) || in_array($intMemberID, $anArrayAttribIDs)) {
		        $arrayAttribIDs[$intMemberID] = array();
		        $arrayAttribIDs[$intMemberID]["id"] = $intMemberID;
		        $arrayAttribIDs[$intMemberID]["dimID"] = (int) $aRS->fields["parentid"];
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		        $arrayAttribIDs[$intMemberID]["name"] = (string) $aRS->fields["membername"];
		        $arrayAttribIDs[$intMemberID]["order"] = (int) $aRS->fields["memberorder"];
		        $arrayAttribIDs[$intMemberID]["isKey"] = (int) isset($arrKeyAttributes[$intMemberID]);
				//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		        $arrayAttribIDs[$intMemberID]["indDimID"] = (int) @$aRS->fields["inddimid"];
				//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
				$inteBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
				$inteBavelFieldDataID = (int) @$aRS->fields["ebavelfielddataid"];
				if ($inteBavelFieldID > 0) {
		        	$arrayAttribIDs[$intMemberID]["eBavelFieldID"] = $inteBavelFieldID;
					$arrayAttribIDs[$intMemberID]["eBavelFieldDataID"] = $inteBavelFieldDataID;
				}
				if (getMDVersion() >= esvAgendaRedesign) {
					$arrayAttribIDs[$intMemberID]["KeyOfAgenda"] = (int) @$aRS->fields[strtolower("KeyOfAgenda")];
					$arrayAttribIDs[$intMemberID]["AgendaLatitude"] = (int) @$aRS->fields[strtolower("AgendaLatitude")];
					$arrayAttribIDs[$intMemberID]["AgendaLongitude"] = (int) @$aRS->fields[strtolower("AgendaLongitude")];
					$arrayAttribIDs[$intMemberID]["AgendaDisplay"] = (int) @$aRS->fields[strtolower("AgendaDisplay")];
				}
				//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
				if (getMDVersion() >= esvCatalogAttributeImage) {
					$blnIsImage = (int) @$aRS->fields["isimage"];
					$arrayAttribIDs[$intMemberID]["isImage"] = ($blnIsImage)?1:0;
				}
				//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				if (getMDVersion() >= esvSChoiceMetro) {
					$blnIsOrder = (int) @$aRS->fields["isquestionorder"];
					$arrayAttribIDs[$intMemberID]["isOrder"] = ($blnIsOrder)?1:0;
					//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
					if ($blnIsOrder) {
						$blnHasSortingAttr = true;
					}
					$intSortingDir = (int) @$aRS->fields["datamemberorderdir"];
					$arrayAttribIDs[$intMemberID]["sortDir"] = ($intSortingDir)?ordbDsc:ordbAsc;
				}
				//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
				$arrayAttribIDs[$intMemberID]["sourceMemberID"] = (int) @$aRS->fields[strtolower("DataSourceMemberID")];
		        //@JAPR
		        $i++;
	        }
	        //@JAPR
	        $aRS->MoveNext();
	    }
		
		//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
		if (!$blnHasSortingAttr) {
			//En este caso no hay un atributo orden, así que debe extraer el orden directamente de la pregunta asociada a este catálogo
			$sql = "SELECT A.DataMemberOrderDir, A.CatMemberID 
				FROM SI_SV_Question A 
				WHERE A.CatalogID = " . $aCatalogID;
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$intCatMemberID = (int) @$aRS->fields["catmemberid"];
				$intSortingDir = (int) @$aRS->fields["datamemberorderdir"];
				if ($intCatMemberID && $intSortingDir && $arrayAttribIDs[$intMemberID]) {
					$arrayAttribIDs[$intCatMemberID]["isOrder"] = 1;
					$arrayAttribIDs[$intCatMemberID]["sortDir"] = ordbDsc;
				}
			}
		}
		//@JAPR
	    return $arrayAttribIDs;
	}
	//@JAPR
	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	//@JAPRWarning: En realidad el parámetro $aSurvey no se requiere, así que en lugar de generar una nueva función que sólo reciba el catálogo, se 
	//reutilizará esta enviando $aSurvey como null puesto que el resultado en ambas funciones sería el mismo
	//@JAPR 2012-08-17: Agregados estos métodos en forma estática para ser usados desde cualquier punto. Modificado para sintaxis de v4
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Agregado el parámetro $bJustGetHistoricEntryData para cargar sólo los valores usados durante la captura especificada sin regresar también
	//los valores propios del catálogo en este momento. Esto es útil sólo para generar el array de valor de la captura ya que se usa una numeración
	//negativa y tiene que se consistente con el array usado durante la edición en el browser
	//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
	//@JAPRDescontinuada: Ya no se requiere en los procesos de eForms v6, hay que evitar usarla
	static function GetCatalogAttributeValues($aRepository, $aCatalog, $arrayAttribIDs = null, $aUserID = -1, $filter="", $entryID = null, $entrySurveyID = null, $bJustGetHistoricEntryData = false, $bGetDistinctValues = false) {
		return $aCatalog->getAttributeValues($arrayAttribIDs, $aUserID, $filter, $entryID, $entrySurveyID, $bJustGetHistoricEntryData, $bGetDistinctValues);
	}
	
	//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//Convierte un String que representa un valor de simple choice tal como lo envía el App de utilizar los CLA_DESCRIP de los atributos del
	//catálogo a utilizar los CLA_DESCRIP de las dimensiones desasociadas
	static function TranslateSCHValueToDissociatedDimensions($aRepository, $aCatalogID, $aSCHFilterStr) {
		//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		//Si no se especifica un catálogo (como en el caso de las secciones Inline con opciones fijas) simplemente regresa el string recibido
		if ($aCatalogID <= 0) {
			return $aSCHFilterStr;
		}
		//@JAPR
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$objCatalog = $strCalledClass::NewInstanceWithID($aRepository, $aCatalogID);
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if (!$objCatalog->DissociateCatDimens || $objCatalog->CreationAdminVersion >= esveFormsv6) {
			return $aSCHFilterStr;
		}
		//@JAPR
		
		$strNewSCHFilterText = $aSCHFilterStr;
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		if (is_null($objCatalogMembersColl) || count($objCatalogMembersColl->Collection) == 0) {
			return $aSCHFilterStr;
		}
		
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			$intClaDescrip = $objCatalogMember->ClaDescrip;
			$intIndDimID = $objCatalogMember->IndDimID;
			
			//Si el atributo no está convertido a dimensiones independientes, sería un error pero por ahora regresa el mismo String para él
			if ($intIndDimID > 0) {
				$strNewSCHFilterText = str_ireplace('key_'.$intClaDescrip.'_', 'key_'.$intIndDimID.'_', $strNewSCHFilterText);
			}
		}
		
		return $strNewSCHFilterText;
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Dado un catálogo, identifica si es de eForms o de eBavel y traduce las variables de cada atributo a los nombres de campo correspondientes
	//regresando el filtro listo para ejecutarse según el contexto
	static function TranslateAttributesVarsToFieldNames($aRepository, $aCatalogID, $aFilterText) {
		if (trim($aFilterText) == '') {
			return '';
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$objCatalog = $strCalledClass::NewInstanceWithID($aRepository, $aCatalogID);
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (is_null($objCatalog)) {
			return $aFilterText;
		}
		
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($objCatalog->CreationAdminVersion >= esveFormsv6) {
			if ($objCatalog->DataSourceID > 0) {
				//En este caso es un Catálogo que proviene de un DataSuource, así que delega la conversión al DataSource
				return BITAMDataSource::TranslateAttributesVarsToFieldNames($aRepository, $objCatalog->DataSourceID, $aFilterText);
			}
			else {
				return $aFilterText;
			}
		}
		//@JAPR
		
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
		if (is_null($objCatalogMembersColl) || count($objCatalogMembersColl->Collection) == 0) {
			return $aFilterText;
		}
		
		$strFilterText = $aFilterText;
		$blneBavelCatalog = false;
		$arreBavelFieldIDs = array();
		if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
			$blneBavelCatalog = true;
			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					$arreBavelFieldIDs[$intFieldID] = $arrFieldData;
				}
			}
		}
		
		//Independientemente del tipo de catálog, debe recorrer la definición de atributos ya que los nombres de variables finalmente se manejan
		//mediante el CatMemberID
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
		foreach ($objCatalogMembersColl->Collection as $intMemberOrder => $objCatalogMember) {
			if ($objCatalogMember->ClaDescrip > 0) {
				$strMemberVarName = $objCatalogMember->getAttribIDVariableName();
				$arrVariableNames[$intMemberOrder] = $strMemberVarName;
			}
		}
		
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$objCatalogMember = @$objCatalogMembersColl->Collection[$intMemberOrder];
			if (is_null($objCatalogMember)) {
				continue;
			}
			
			if ($blneBavelCatalog && $objCatalogMember->eBavelFieldID > 0) {
				//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
				//En este caso el atributo que mapea a un campo tipo Geolocalización utilizará el id del campo, sin hacer referencia a cual de
				//las partes (Latitude o Longitude) especificamente utilizará, ya que los filtros de eBavel lo resuelven unificado
				$strFieldName = (string) @$arreBavelFieldIDs[$objCatalogMember->eBavelFieldID]['id'];
			}
			else {
				$strFieldName = "DSC_".$objCatalogMember->ClaDescrip;
			}
			
			$strFilterText = str_ireplace($strMemberVarName, $strFieldName, $strFilterText);
		}
		//@JAPR
		
		return $strFilterText;
	}
	
	//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
	//Actualiza la versión del catálogo (invocada desde procesos que lo pudieran afectar sin necesidad de forzar al grabado de la instancia)
	//@JAPR 2015-07-21: A partir de v6, esta función solamente se utiliza para actualizar las instancias de catálogo que utiliza el App sin necesidad de cargar una instancia, pero ya
	//no encadenará la actualización de versiones de las formas
	static function updateLastUserAndDateInCatalog($aRepository, $aCatalogID)
	{
		$currentDate = date("Y-m-d H:i:s");
		
		$sql = "UPDATE SI_SV_Catalog SET ".//LastUserID = ".$_SESSION["PABITAM_UserID"].
				//", LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
				" VersionNum = ".$aRepository->ADOConnection->IfNull("VersionNum", "0")." +1".
				" WHERE CatalogID = ".$aCatalogID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
		//@JAPRDescontinuada en v6
		//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
		//La actualización de formas la realizará el DataSource directamente
		//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($aRepository, $aCatalogID);
		//
	}
	//@JAPR
	 
	//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Asocia las dimensiones alternativas de los atributos del catálogo hasta el ID de atributo especificado, si ya estaban se ignoran
	//Regresa el ID de la dimensión que corresponde con el atributo especificado
	//Si no se especifica el parámetro $aMemberID entonces se agregan al modelo de la encuesta todas las dimensiones
	//@JAPR 2013-06-17: Agregado el proceso de migración a catálogos independientes
	//Agregado el parámetro $bForceAssoc para que aunque no se consideren convertidas las encuestas, de todas formas asocie sus atributos
	//@JAPRDescontinuada en v6
	function addDissociatedCatalogDimensions($aSurveyID, $aMemberID = -1, $bForceAssoc = false) {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return 0;
		}
		//@JAPR
		
		//@JAPR 2013-06-05: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		if ($aSurveyID <= 0) {
			return 0;
		}
		
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $aSurveyID);
		if (is_null($aSurveyInstance) || $aSurveyInstance->ModelID <= 0) {
			return 0;
		}
		
		if ((!$aSurveyInstance->DissociateCatDimens && !$bForceAssoc) || !$this->DissociateCatDimens) {
			return 0;
		}
		
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		if (is_null($objCatalogMembersColl) || count($objCatalogMembersColl->Collection) == 0) {
			return 0;
		}
		
		//Asocia al cubo todas las dimensiones de los atributos hasta encontrar al atributo de esta pregunta
		$intMemberClaDescrip = 0;
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			//Se valida si el atributo procesado ya tiene su dimensión alternativa asociada al modelo de la encuesta, si es así se ignora, de lo
			//contrario se tiene que agregar la dimensión al modelo y actualizar sus posibles valores capturados al NA
			if ($objCatalogMember->IndDimID <= 0) {
				//Si por alguna razón no estaba asignada la dimensión alternativa, trata de crearla y continua si tiene éxito
				$objCatalogMember->Save();
				if ($objCatalogMember->IndDimID <= 0) {
					continue;
				}
			}
			$intMemberClaDescrip = $objCatalogMember->IndDimID;
			
			//Identifica si la dimensión ya existe en el modelo para determinar si debe o no asociarla
			$sql = "SELECT A.CLA_DESCRIP 
				FROM SI_CPTO_LLAVE A 
				WHERE A.CLA_CONCEPTO = ".$aSurveyInstance->ModelID." AND A.CLA_DESCRIP = ".$intMemberClaDescrip;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS)
			{
			    if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			    }
			    else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			    }
			}
			
			if(!$aRS->EOF)
			{
				//Ya existe la dimensión así que simplemente regresa el ID pues no hay que agregar nada
				//Hasta la fecha en que se programó esta funcionalidad, no se permitía tener mas de una vez el mismo atributo en la misma encuesta
				//por tanto no sería válido que 2 o mas preguntas apuntaran a esta misma dimensión
				//$intMemberClaDescrip = (int)$aRS->fields["cla_descrip"];
			}
			else 
			{
				//La dimensión no existe en el modelo de esta encuesta, por tanto se procede a agregarla
				$strOriginalWD = getcwd();
		
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				require_once("../model_manager/attribdimension.inc.php");
				
				global $generateXLS;
				$generateXLS = false;
				
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Dimensión desasociada que representa un atributo, en este caso no se está creando una dimensión sino que ya existe, sólo se asocia al cubo,
				//así que es válido "reutilizar" el ID de la dimensión
				$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $aSurveyInstance->ModelID);
				$anInstanceModelDim->Dimension->Estatus = 1;
				$anInstanceModelDim->Dimension->ParentID = -1;
				$anInstanceModelDim->Dimension->IsIncremental = 0;
				$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
				$anInstanceModelDim->Dimension->DimensionName = dsdALTERNATIVE_DIMENSION_PREFIX.$objCatalogMember->MemberName;
				$anInstanceModelDim->Dimension->UseKey = 0;
				$anInstanceModelDim->Dimension->DimensionClaDescrip = $intMemberClaDescrip;
				$anInstanceModelDim->Dimension->DimensionGroup = $this->CatalogName;
				
				$objMMAnswer = @$anInstanceModelDim->save();
				chdir($strOriginalWD);
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError == '') {
					//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
					$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
					$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
					
					$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = 1";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
					    if($gblShowErrorSurvey) {
							die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					    }
					    else {
							return("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					    }
					}
				}
				else {
				    if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error adding catalog question's global dimension").": ".$strMMError.". ");
				    }
				    else {
						return("(".__METHOD__.") ".translate("Error adding catalog question's global dimension").": ".$strMMError.". ");
				    }
				}
			}
			
			//Al llegar al atributo solicitado termina el proceso para no agregar dimensiones que no se van a usar
			if ($objCatalogMember->MemberID == $aMemberID) {
				break;
			}
		}
		
		return $intMemberClaDescrip;
	}
	//@JAPR
	
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Regresa los datos del catálogo según como se grabaron en la captura de la encuesta especificada
	function getAttributeValuesForEntryID($arrayAttribIDs = null, $entrySurveyID = 0, $entryID = 0) {
		$i=0;
		$attribValues = array();
		
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return $attribValues;
		}
		//@JAPR
		
		if ($entrySurveyID <= 0 || $entryID <= 0) {
			return $attribValues;
		}
		
		//Si se está editando una captura, debe generar como primer elemento del array de datos la combinación exacta con la que se grabó la
		//captura indicada, asignandole a cada combinación un key negativo para hacer diferencia entre los datos actuales del catálogo
		//En dado caso que todas las combinaciones grabadas si fueran exactas a registros actual del catálogo, se podría omitar esta parte y
		//simplemente regresar el catálogo tal cual, ya que el App podría identificar la combinación usada en sus datos
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $entrySurveyID);
		
		//Solo si la encuesta utiliza dimensiones independientes para su catálogos es necesario obtener las descripciones directo de dichas
		//dimensiones, puesto que en cualquier otro caso se usaba sólo el catálogo tal como existiera al momento de la edición
		if (is_null($surveyInstance) || !$surveyInstance->DissociateCatDimens) {
			return $attribValues;
		}
		
		/*Se asumirán varias cosas llegado a este punto:
			- A la fecha de esta funcionalidad, no debía haber preguntas de catálogo del mismo catálogo de la dinámica posteriores a dicha
		sección, por tanto es seguro asumir que para ese catálogo bastaría con obtener la combinación de la dinámica para generar los datos
		usados en este catálogo
			- Si hubiera preguntas simple-choice de este catálogo y no hay secciones dinámicas, la última pregunta está obligada a filtrarse
		por las anteriores, por lo tanto basta con obtener el valor de la última pregunta en el orden para obtener la combinación completa,
		sin embargo algunas preguntas pueden ser omitidas y en esos casos hubiera grabado el valor de No Aplica, así que es necesario traer
		en cada registro todas las preguntas del catálogo y comparar si alguna de ellas es diferente al No Aplica (esto mismo sucede con
		las secciones dinámicas, pudiera no haberse contestado dicha sección por lo que la combinación realmente la traería la pregunta que
		la genera y no la propia sección)
		*/
		$intFactKeyDimID = $surveyInstance->FactKeyDimID;
		
		if (is_null($arrayAttribIDs)) {
			$arrayAttribIDs = array();
		}
		
		//@JAPRWarning: Validar que todas las dimensiones se encuentren en el cubo, si no es así, recordar el array de atributos hasta el máximo
		//usado para evitar que se genere un error en el query
		//Probablemente se deba cambiar a LEFT JOIN para evitar que en caso de venir como NULL el valor de una dimensión, se terminen omitiendo
		//combinaciones que si se grabaron
		//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
		//para sólo intentar utilizar dichas dimensiones y que no genere errores el query, sin embargo se debe analizar si es o no necesario agregar
		//dummies de las dimensiones omitidas o no, ya que estas combinaciones al ser de la edición, exclusivamente tendrán los atributos que
		//finalmente se utilizaron para el grabado y que teoricamente son los únicos que usa la encuesta, por lo que en la primera fase no se
		//incluirán NAs para los atributos no usados/existentes en el cubo esperando que no afecte en nada a la edición
		$arrModelCatDimens = array();
		$arrAttribIDs = array();
		$lim = count($arrayAttribIDs);
		for($i=0; $i<$lim; $i++) {
			$attribID = $arrayAttribIDs[$i]["indDimID"];
			$arrAttribIDs[] = $attribID;
		}
		
		//Sólo agregará al array los atributos que existan como dimensión en este modelo para no intentar usar algo que generará un error
		if (count($arrAttribIDs) > 0) {
			$sql = "SELECT CLA_DESCRIP 
				FROM SI_CPTO_LLAVE 
				WHERE CLA_CONCEPTO = ".$surveyInstance->ModelID." AND CLA_DESCRIP IN (".@implode(',', $arrAttribIDs).")";
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				while(!$aRS->EOF) {
					$intClaDescrip = (int) @$aRS->fields['cla_descrip'];
					if ($intClaDescrip > 0) {
						$arrModelCatDimens[$intClaDescrip] = $intClaDescrip;
					}
					$aRS->MoveNext();
				}
			}
		}
		
		//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
		//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
		//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
		//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
		//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
		//por lo que se hará el query como originalmente se hacía
		$dynamicSectionID = (int) @BITAMSection::existDynamicSection($this->Repository, $entrySurveyID);
		$blnFilterByDynamicSection = false;
		$dynamicSectionCatID = 0;
		//Si hay sección dinámica, se obtiene el ID para buscarlo en la captura
		if ($dynamicSectionID > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($this->Repository, $dynamicSectionID);
			if (!is_null($dynamicSection)) {
				$dynamicSectionCatID = $dynamicSection->CatalogID;
			}
			
			if ($dynamicSectionCatID == $this->CatalogID) {
				$blnFilterByDynamicSection = true;
			}
		}
		
		//Si el catálogo corresponde al de la sección dinámica, verifica si hay o no algún registro de dicha sección, si lo hay entonces en cualquier
		//pregunta de otro tipo que usara este catálogo solo podría haber por fuerza valores que estaban como parte de esta dinámica (a la fecha
		//de este cambio no se soportaba que se mezclaran dinámicas y maestro-detalle que reutilizaran el mismo catálogo, ni simple choice del
		//mismo catálogo posteriores a la dinámica, así que en la dinámica terminaban los posibles valores usados del catálogo), si no hubiera
		//registros entonces se pueden tomar todos incluyendo donde las dimensiones tuvieran valores de *NA
		if ($blnFilterByDynamicSection) {
			$sql = "SELECT EntrySectionID 
				FROM {$surveyInstance->SurveyTable} 
				WHERE FactKeyDimVal = $entryID AND EntrySectionID = $dynamicSectionID";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nVerifying EntrySectionID: $sql<br>\r\n");
			}
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				return $attribValues;
			}
			if($aRS->EOF)
			{
				//Si no hubiera registros entonces significa que no capturaron la sección dinámica, por tanto no es necesario filtrar por ella
				$blnFilterByDynamicSection = false;
			}
		}
		
		$strFields = "";
		$strFieldsGroup = "";
		$arrIdxs = array();
		$arrLast = array();
		
		$lim = count($arrayAttribIDs);
		$fromCat = "";
		$joinCat = "";
		for($i=0; $i<$lim; $i++)
		{
			$attribID = $arrayAttribIDs[$i]["indDimID"];
			//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
			if (!isset($arrModelCatDimens[$attribID])) {
				//No existe dimensión para este atributo en el modelo, así que lo omite (teóricamente, no podría faltar una dimensión intermedia
				//así que a partir de este punto ya no debería agregar ningún atributo mas, pero se dejará abierta la revisión, sólo se agregará
				//el array vacio de estos atributos para que el ciclo de llenado no genere errores)
				$arrIdxs[$i] = -1;
				$arrLast[$i] = null;
				eval('$attrib'.$i.'=array();');
				continue;
			}
			
			if($strFields!="")
			{
				$strFields.=", ";
				$strFieldsGroup.=", ";
			}
			//@JAPR
			
			$selectFieldKey = "RIDIM_".$attribID."KEY";
			$tableName = "RIDIM_".$attribID;
			//@JAPR 2013-04-23: Corregido un bug durante la edición, debe excluir a los valores de *NA grabados porque al considerarlos
			//los estaba interpretando como páginas dinámicas u opciones adicionales que no se deben capturar, por lo que ahora solo considerará
			//aquellos miembros de las dimensiones cuyo key > 1
			$strFields .= "MAX(".$tableName.".".$selectFieldKey.") AS ".$selectFieldKey.", ".$tableName.".DSC_".$attribID;
			$strFieldsGroup .= $tableName.".DSC_".$attribID;
			$fromCat .= ', '.$tableName;
			//@JAPR 2013-03-05: Corregido un bug, la condición de que el KEY tiene que ser > 1 no debe aplicar realmente, ya que aunque es buena
			//idea no presentar valores de NA, como se trata de un join compuesto si no capturan alguno de los últimos atributos entonces no
			//traería ningún registro y eso no es correcto
			$joinCat .= ' AND '.$surveyInstance->FactTable.'.'.$selectFieldKey.' = '.$tableName.'.'.$selectFieldKey;
				//' AND '.$tableName.'.'.$selectFieldKey.' > 1';
			//@JAPR
			$arrIdxs[$i] = -1;
			$arrLast[$i] = null;
			
			eval('$attrib'.$i.'=array();');
		}
		
		$selectFieldKey = "RIDIM_".$intFactKeyDimID."KEY";
		$where = " WHERE ".$selectFieldKey." = ".$entryID;
		//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
		//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
		//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
		//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
		//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
		//por lo que se hará el query como originalmente se hacía
		$strFromSVSurvey = '';
		$strJoinSVSurvey = '';
		if ($blnFilterByDynamicSection) {
			$strFromSVSurvey = ', '.$surveyInstance->SurveyTable;
			$strJoinSVSurvey = " AND ".$surveyInstance->FactTable.".FactKey = ".$surveyInstance->SurveyTable.".FactKey AND ".
				$surveyInstance->SurveyTable.".EntrySectionID = ".$dynamicSectionID;
		}
		//@JAPR
		
		//@JAPR 2012-08-20: Corregido un bug si el catálogo sólo tenía un atributo, entonces debe agrupar por dicho atributo
		//@JAPR 2013-12-10: Corregido un bug, si se ordena por FactKey, entonces los catálogos que no son de las secciones dinámicas y donde hubiera
		//múltiples atributos, hubieran generado un array donde se repitieran en muchas ocasiones los atributos padre pero intercalados entre
		//diferentes valores, así que al pintar en el App las combos siempre hubiera tomado la primera ruta, por tanto no se debe ordenar por FactKey
		//sino por los propios atributos, pero esto sólo es cierto si el catálogo en cuestión no es el mismo de la sección dinámica, ya que si lo
		//fuera en ese caso si debe respetar el orden en que se insertaron los valores
		$strOrderBy = '';
		if ($dynamicSectionCatID == $this->CatalogID) {
			//En este caso se trata del catálogo dinámico, por tanto se ordena en el orden de inserción de los valores en el cubo
			$strOrderBy = "ORDER BY {$surveyInstance->FactTable}.FactKey";
		}
		else {
			//En este caso se trata de un catálogo diferente al dinámico, así que se ordena por los atributos en jerarquía
			if (trim($strFieldsGroup) != '') {
				$strOrderBy = "ORDER BY {$strFieldsGroup}";
			}
		}
		
		$sql = "SELECT DISTINCT ".$strFields." 
			FROM ".$surveyInstance->FactTable.$strFromSVSurvey.$fromCat.$where.$strJoinSVSurvey.$joinCat." 
			GROUP BY ".$strFieldsGroup." 
			{$strOrderBy}";	//.$strFields;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nCatalog combination query: $sql<br>\r\n");
		}
		//@JAPR
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nError reading catalog combination: ".@$this->Repository->DataADOConnection->ErrorMsg()."<br>\r\n");
			}
			return $attribValues;
		}
		if($aRS->EOF)
		{
			return $attribValues;
		}
		
		$valueFieldKey = -1;
		while(!$aRS->EOF)
		{
			for($i=0; $i<$lim; $i++)
			{
				//@JAPR 2013-07-19: Corregido un bug, ahora se validará correctamente las dimensiones independientes que se encuentran asociadas al modelo
				$attribID = $arrayAttribIDs[$i]["indDimID"];
				if (!isset($arrModelCatDimens[$attribID])) {
					//No existe dimensión para este atributo en el modelo, así que lo omite (teóricamente, no podría faltar una dimensión intermedia
					//así que a partir de este punto ya no debería agregar ningún atributo mas, pero se dejará abierta la revisión)
					continue;
				}
				//@JAPR
				
				$field = "DSC_".$arrayAttribIDs[$i]["indDimID"];
				$attribVal = $aRS->fields[strtolower($field)];
				//@JAPR 2013-04-23: Corregido un bug durante la edición, debe excluir a los valores de *NA grabados porque al considerarlos
				//los estaba interpretando como páginas dinámicas u opciones adicionales que no se deben capturar, por lo que ahora solo considerará
				//aquellos miembros de las dimensiones cuyo key > 1
				$selectFieldKey = "RIDIM_".$arrayAttribIDs[$i]["indDimID"]."KEY";
				$attribKey = (int) @$aRS->fields[strtolower($selectFieldKey)];
				//Si es el valor de *NA o no se pudo leer, no puede continuar
				//@JAPR 2013-04-24: Corregido un bug durante la edición, la forma de corregir esto no será validando en busca de excluir registros cuando
				//venga el valor de *NA en alguno de ellos, porque para por lo menos 2 casos de clientes (La Redonda y Rich) se usaron dichos valores
				//en secciones dinámicas por lo que excluyendolos se removían algunos checkboxes y descuadraban la captura. Ahora lo que se hará será
				//filtrar el query por la sección dinámica si es que el catálogo se usó en ella para evitar traer los *NA que sean de la simple choice,
				//de lo contrario si solo hay estándaras o maestro-detalle se asume que son simple choices y no importa si se incluyen valores con *NA
				//por lo que se hará el query como originalmente se hacía
				/*
				if ($attribKey <= 1) {
					continue;
				}
				*/
				
				//@JAPR
				if($i!=($lim-1))
				{
					if($attribVal!==$arrLast[$i])
					{
						if($arrLast[$i]!==null)
						{
							for($j=($lim-2); $j>=$i; $j--)
							{
								eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
							}
							
							for($j=$i+1;$j<$lim;$j++)
							{
								eval('$attrib'.$j.'=array();');
								$arrIdxs[$j] = -1;
								$arrLast[$j] = null;
							}
						}
						
						$arrIdxs[$i] = $arrIdxs[$i] + 1;
						eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
						//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					}
				}
				else 
				{
					$arrIdxs[$i]=$arrIdxs[$i]+1;
					eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					eval('$attrib'.$i.'[$arrIdxs[$i]]["children"]=array();');
				}
				
				$arrLast[$i] = $attribVal;
			}
			
			$aRS->MoveNext();
			$valueFieldKey--;
		}
		
		if($lim>1)
		{
			for($j=$lim-2;$j>=0;$j--)
			{
				eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
			}
		}
		
		$attribValues = $attrib0;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nCatalog values:<br>\r\n");
			PrintMultiArray($attribValues);
		}
		return $attribValues;
	}
	//@JAPR
	
	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	//@JAPR 2013-02-11: Agregada la edición de datos históricos
	//Agregado el parámetro $bJustGetHistoricEntryData para cargar sólo los valores usados durante la captura especificada sin regresar también
	//los valores propios del catálogo en este momento. Esto es útil sólo para generar el array de valor de la captura ya que se usa una numeración
	//negativa y tiene que se consistente con el array usado durante la edición en el browser
	//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
	//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//@JAPR 2015-03-11: Agregado el parámetro de retorno $bError para indicar si existió o no algún error al cargar los valores, lo que básicamente
	//significa que falló el query. A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
	//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
	//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
	//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
	//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir
	function getAttributeValues($arrayAttribIDs = null, $aUserID = -1, $filter="", $entryID = null, $entrySurveyID = null, $bJustGetHistoricEntryData = false, $bGetDistinctValues = false, &$bError = false, $bIncludeSystemFields = true) {
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		
		//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//@JAPR 2015-03-11: A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
		//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
		//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
		//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
		//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir. Los errores que
		//reportaba eBavel no incluían errores por no tener las clases necesarias, así que se modificará también la respuesta en esos casos, bajo
		//el entendido que en caso de faltar algo importante de todas maneras fallará el código y detendrá la ejecución
		global $gbProcessingAgendaCatalogs;
		if (!isset($gbProcessingAgendaCatalogs)) {
			$gbProcessingAgendaCatalogs = false;
		}
		//@JAPR
		
		$i=0;
		$attribValues = array();
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			//Los datos se obtiene a partir del DataSource correspondiente
			//Obtiene la referencia al DataSource y desde ahí obtiene los valores
			if ($this->DataSourceID > 0) {
				$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
				if (!is_null($objDataSource)) {
					//@JAPR 2015-08-23: Agregados parámetros internedios que provocaban que la colección de valores regresara vacía
					//@JRPP 2015-08-26: Agregados parámetros faltantes para compatibilidad con DataSources
					//@JAPRWarning 2015-12-22: Corregido un bug, esta función en DataSource NO estaba estandarizada, se creó completamente dispareja en cuanto a los
					//parámetros que recibe de su contraparte en BITAMCatalog, pero se seguían enviando parámetros que nadie leía, así que se hará un primer
					//intento por al menos reenviar correctamente los parámetros que si son usados, aunque eso dejará a BITAMDataSource sin la funcionalidad
					//original de BITAMCatalog, la cual básicamente era para edición de capturas. El parámetro $bError será el único que se respete porque ese
					//era independiente del proceso de edición de valores, mientras que se asumirá que $bGetDistinctValues vendría siempre en false ya que
					//BITAMDataSource lo trata de esa manera (@JAPRWarning, eso es un error, no está optimizado eBavel, de tal manera que el proceso es innecesariamente
					//lento al no agrupar los registros de la consulta con eBavel)
					//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
					$attribValues = @$objDataSource->getAttributeValues($arrayAttribIDs, $aUserID, $filter, '', false, $bIncludeSystemFields, false, ADODB_FETCH_ASSOC, true, $bIncludeSystemFields, $bError);
					if (is_null($attribValues) || !is_array($attribValues)) {
						$attribValues = array();
					}
				}
			}
			return $attribValues;
		}
		//@JAPR
		
		//@JAPR 2013-02-11: Agregada la edición de datos históricos
		if (!is_null($entryID) && !is_null($entrySurveyID)) {
			$blnEdit = true;
			$entryID = (int) $entryID;
			$entrySurveyID = (int) $entrySurveyID;
		}
		else {
			$blnEdit = (bool) getParamValue('edit', 'both', "(int)");
			$entryID = getParamValue('entryID', 'both', "(int)");
			$entrySurveyID = getParamValue('entrySurveyID', 'both', "(int)");
		}
		
		//Si se está editando una captura, debe generar como primer elemento del array de datos la combinación exacta con la que se grabó la
		//captura indicada, asignandole a cada combinación un key negativo para hacer diferencia entre los datos actuales del catálogo
		//En dado caso que todas las combinaciones grabadas si fueran exactas a registros actual del catálogo, se podría omitar esta parte y
		//simplemente regresar el catálogo tal cual, ya que el App podría identificar la combinación usada en sus datos
		$blnAddHistoricData = false;
		if ($blnEdit && $entryID > 0 && $entrySurveyID > 0) {
			$attribValues = $this->getAttributeValuesForEntryID($arrayAttribIDs, $entrySurveyID, $entryID);
			$blnAddHistoricData = count($attribValues) > 0;
		}
		
		//Si sólo se piden datos históricos, ya no se carga la información actual del catálogo
		if ($bJustGetHistoricEntryData) {
			return $attribValues;
		}
		//@JAPR
		
		if (is_null($arrayAttribIDs)) {
			$arrayAttribIDs = array();
		}
		if ($aUserID <= 0) {
			$aUserID = $_SESSION["PABITAM_UserID"];
		}
		$resultFilter = $this->generateResultFilterByUser($aUserID);
		
		$strFields = "";
		$arrIdxs = array();
		$arrLast = array();
		
		$lim = count($arrayAttribIDs);
		
		for($i=0; $i<$lim; $i++)
		{
			if($strFields!="")
			{
				$strFields.=",";
			}
			
			$attribID = $arrayAttribIDs[$i]["dimID"];
			$strFields.="DSC_".$attribID;
			//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
			//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
			$field = strtolower("DSC_".$attribID);
			$arrayAttribIDs[$i]["fieldName"] = $field;
			//@JAPR
			
			$arrIdxs[$i] = -1;
			$arrLast[$i] = null;
			
			eval('$attrib'.$i.'=array();');
		}
		
		$fieldKey = $this->TableName."KEY";
		$tableName = $this->TableName;
		
		$where = " WHERE ".$fieldKey." <> 1 ";
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Ahora se validará si el parámetro $filter contiene o no un AND, ya que con la definición de filtros dinámicos, esos se generan sin AND
		//por compatibilidad con eBavel y por tanto hay que concatenarlo si en realidad era un filtro de catálogo de eForms
		$strAnd = '';
		$filter = trim($filter);
		if ($filter != '' && substr(strtolower($filter), 0, 4) != 'and ') {
			$strAnd = 'AND ';
		}
		//@JAPR
		$where.=$strAnd.$filter;
		
		//Concatenamos la seguridad del usuario definida en el catalogo
		if($resultFilter!="")
		{
			$where.=" AND ";
		}
		
		$where.=$resultFilter;
		//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
		//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
		$streBavelPath = '';
		//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
		//Si el catálogo es mapeado desde eBavel, se invoca a la clase que carga los valores pero se configura para regresar el RecordSet de tal
		//forma que se pueda usar en este método
		if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
			//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
			//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
			require_once('settingsvariable.inc.php');
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'EBAVELURL');
			if (!is_null($objSetting)) {
				$streBavelPath = trim((string) @$objSetting->SettingValue);
			}
			
			//@JAPR 2014-06-11: Agregado el soporte para el filtro estático de usuarios de los catálogos de eBavel
			$strFilter = $filter;
			if ($filter != '' && $resultFilter != '') {
				$strFilter .= ' AND ';
			}
			$strFilter .= $resultFilter;
			
			//@JAPR 2014-05-28: Agregado el parámetro $bGetDistinctValues para que en el caso de catálogos mapeados de eBavel se obtengan los valores únicos
			//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			//@JAPR 2015-03-11: Agregado el parámetro de retorno $bError para indicar si existió o no algún error al cargar los valores, lo que básicamente
			//significa que falló el query. A la fecha de implementación, eBavel Si reportaba los errores y de hecho detenía la ejecución del producto,
			//lo cual no era deseable, mientras que los catálogos de eForms no reportaban los errores y dejaban continuar, esto tampoco es muy bueno pero
			//por lo menos no detenía al App. Se modificará para que eBavel utilice una variable global cuando se estén obteniendo los valores de los
			//catálogos para las agendas, de tal manera que no truene eBavel sino que regrese indicación de error, y con este nuevo parámetro se
			//decidirá si hay o no error para saber si se debe usar la colección de valores vacia o si simplemente no se debe incluir
			$gbProcessingAgendaCatalogs = true;
			$aRS = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository,$this->CatalogID,true,$strFilter,-1, true, true, $bGetDistinctValues);
			$gbProcessingAgendaCatalogs = false;
			if (!$aRS) {
				$bError = true;
				return $attribValues;
			}
			
			//@JAPR 2014-06-11: Corregida esta validación, se debe hacer en este punto para verificar el EOF
			//@JAPR 2014-05-20: Agregada validación para controlar cuando eBavel no pueda regresar el RecordSet
			if (!property_exists($aRS, 'EOF')) {
				//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
				$bError = true;
				//@JAPR
				return $attribValues;
			}
			
			if($aRS->EOF)
			{
				return $attribValues;
			}
			//@JAPR

			/*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
			if (getMDVersion() >= esvRedesign){
	            //Si hemos seleccionado una Forma de eBavel entonces pedimos sus atributos.
	            if($this->eBavelFormType == ebftForms){
	                $arreBavelFormColl = @GetEBavelSections($this->Repository, false, $this->eBavelAppID, array($this->eBavelFormID));
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null, false, 'Forms');
	            }        
	            else{//Si hemos seleccionado una Vista de eBavel entonces pedimos sus atributos.
	                $arreBavelFormColl = @GetEBavelViews($this->Repository, false, $this->eBavelAppID, array($this->eBavelFormID));
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null, false, 'Views');
	            }
	            //@AAL
			}
			else{
				$arreBavelFormColl = @GetEBavelSections($this->Repository, false, $this->eBavelAppID, array($this->eBavelFormID));
				$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null, false, '');
			}
                     
			//Adicionalmente hay que cambiar los nombres de los campos en el array $arrayAttribIDs, ya que el recordset viene de eBavel así que
			//no reconoce los nombres de atributos de eForms
			//Carga la definición de la forma de eBavel a utilizar
			$strFormID = '';
			if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			}
			else {
				$objForm = $arreBavelFormColl[0];
				$strFormID = $objForm['id'];
			}
			
			$arreBavelFieldIDs = array();
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					$arreBavelFieldIDs[$intFieldID] = $arrFieldData;
				}
			}
			
			for($i=0; $i<$lim; $i++) {
		        //@JAPR 2015-06-17: Corregido un bug, estos valores sólo vienen si el catálogo es de eBavel, así que se validarán para no generar error
				$inteBavelFieldID = (int) @$arrayAttribIDs[$i]["eBavelFieldID"];
				$inteBavelFieldDataID = (int) @$arrayAttribIDs[$i]["eBavelFieldDataID"];
		        if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		            echo("<br>\r\n FieldID dimID: ".@$arrayAttribIDs[$i]["eBavelFieldID"]);
		            echo("<br>\r\n FieldID fieldName: ".@$arrayAttribIDs[$i]["fieldName"]);
		            echo("<br>\r\n inteBavelFieldID: {$inteBavelFieldID}");
		            echo("<br>\r\n inteBavelFieldDataID: {$inteBavelFieldDataID}");
		        }
		        //@JAPR
				$strSuffix = '';
				if ($inteBavelFieldDataID > ebfdValue) {
					switch ($inteBavelFieldDataID) {
						case ebfdLatitude:
							$strSuffix = '_L';
							break;
						case ebfdLongitude:
							$strSuffix = '_A';
							break;
					}
				}
				if ($inteBavelFieldID > 0 && isset($arreBavelFieldIDs[$inteBavelFieldID])) {
					$field = $arreBavelFieldIDs[$inteBavelFieldID]['id'];
					$arrayAttribIDs[$i]["fieldName"] = $field . $strSuffix;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n New FieldID fieldName: ".@$arrayAttribIDs[$i]["fieldName"]);
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n inteBavelFieldID not found. eBavelFields list:");
						PrintMultiArray($arreBavelFieldIDs);
					}
				}
			}
			$fieldKey = 'id_'.$strFormID;
		}
		else {
			//@JAPR 2012-08-20: Corregido un bug si el catálogo sólo tenía un atributo, entonces debe agrupar por dicho atributo
			if ($lim == 1) {
				$sql = "SELECT DISTINCT MIN(".$fieldKey.") AS ".$fieldKey.", ".$strFields." FROM ".$tableName.$where." GROUP BY ".$strFields." ORDER BY ".$strFields;
			}
			else {
				$sql = "SELECT DISTINCT ".$fieldKey.", ".$strFields." FROM ".$tableName.$where." ORDER BY ".$strFields;
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				if ($where) {
					echo("<br>\r\nCatalog->getAttributeValues ({$this->CatalogID}) Filter:\r\n{$where}<br>\r\n");
				}
				echo("<br>\r\nCatalog->getAttributeValues ({$this->CatalogID}):\r\n{$sql}<br>\r\n");
			}
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				//@JAPR 2015-03-11: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
				$bError = true;
				//@JAPR
				return $attribValues;
			}
			if($aRS->EOF)
			{
				return $attribValues;
			}
			
			//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
			//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
			$fieldKey = strtolower($fieldKey);
		}
		
		//@JAPR 2015-03-30: Corregido un bug, si el catálogo fuera de eBavel y hubieran eliminado algún campo mapeado en eForms (o requerido),
		//por la manera en que se arma el recordset, de todas maneras vienen asignados los registros pero sin el campo, así que hay que validar
		//la existencia del campo en el recordset para que en caso de no venir simplemente se omite agregar datos al catálogo (#CNS3SK)
		$blnValidRecordSet = true;
		if ($this->eBavelAppID > 0 && $this->eBavelFormID > 0) {
			//Recorre el primer registro del recordset, si detecta por lo menos un campo no existente, ya no procesará el recordset completo
			//y regresará el catálogo como si estuviera vacío
			if (!$aRS->EOF) {
				for($i=0; $i<$lim; $i++)
				{
					//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
					//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
					$field = $arrayAttribIDs[$i]["fieldName"];
					//@JAPR 2015-03-31: Modificado por array_key_exists porque isset no considera los valores NULL
					if (!array_key_exists($field, $aRS->fields)) {
						$blnValidRecordSet = false;
						break;
					}
					
					//@JAPR 2015-03-31: Modificado por array_key_exists porque isset no considera los valores NULL
					if (!array_key_exists($fieldKey, $aRS->fields)) {
						$blnValidRecordSet = false;
					}
				}
			}
		}
		
		//@JAPR 2015-03-30: Corregido un bug, si el catálogo fuera de eBavel y hubieran eliminado algún campo mapeado en eForms (o requerido),
		//por la manera en que se arma el recordset, de todas maneras vienen asignados los registros pero sin el campo, así que hay que validar
		//la existencia del campo en el recordset para que en caso de no venir simplemente se omite agregar datos al catálogo (#CNS3SK)
		if (!$blnValidRecordSet) {
			return $attribValues;
		}
		
		while(!$aRS->EOF)
		{
			for($i=0; $i<$lim; $i++)
			{
				//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
				//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
				$field = $arrayAttribIDs[$i]["fieldName"];
				$attribVal = $aRS->fields[$field];
				
				/*@AAL 15/04/2015 Issue #QOI4L5 corregido: Cuando venia un campo como NULL desde eBavel $attribVal generaba
				 un array con key -1 y se ocasionaba un error al general el Array attributeValues*/
				if($attribVal == NULL) 
					$attribVal = "";
				//@AAL

				//$field = "DSC_".$arrayAttribIDs[$i]["dimID"];
				//$attribVal = $aRS->fields[strtolower($field)];
				//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
				//Si el atributo es imagen, entonces debe ajustar las rutas para que funcionen con dispositivos
				//@JAPR 2014-12-16: Corregido un bug, estaba mal la validación, se debe hacer contra el valor no la existencia de la propiedad
				//@JAPR 2014-12-18: Corregido un bug, para atributos de eBavel si hay que hacer un proceso extra ya que la ruta a regresar debe
				//ser absoluta, de lo contrario no encontrará la imagen relativa al servicio de eForms
				if ((int) @$arrayAttribIDs[$i]["isImage"] && $attribVal && (!$blnWebMode || ($this->eBavelAppID > 0 && $this->eBavelFormID > 0))) {
					if ($blnWebMode) {
						//En este caso es modo Web por lo que el catalogo tiene que ser de tipo eBavel, así que se convierte la URL a absoluta
						$attribVal = $streBavelPath.$attribVal;
					}
					else {
						$attribVal = "{@IMGPATH}".GetImageRelativeNameForApp($this->Repository, $attribVal);
					}
				}
				//@JAPR
				
				if($i!=($lim-1))
				{
					if($attribVal!==$arrLast[$i])
					{
						if($arrLast[$i]!==null)
						{
							for($j=($lim-2); $j>=$i; $j--)
							{
								eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
							}
							
							for($j=$i+1;$j<$lim;$j++)
							{
								eval('$attrib'.$j.'=array();');
								$arrIdxs[$j] = -1;
								$arrLast[$j] = null;
							}
						}
						
						$arrIdxs[$i] = $arrIdxs[$i] + 1;
						//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
						//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
						$valueFieldKey = $aRS->fields[$fieldKey];
						//@JAPR
						eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
						//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					}
				}
				else 
				{
					//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
					//Por compatibilidad con el soporte a eBavel, ahora el array guardará el nombre de campo a buscar en el recordset
					$valueFieldKey = $aRS->fields[$fieldKey];
					//@JAPR
	
					$arrIdxs[$i]=$arrIdxs[$i]+1;
					eval('$attrib'.$i.'[$arrIdxs[$i]]["key"]=$valueFieldKey;');
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					eval('$attrib'.$i.'[$arrIdxs[$i]]["value"]=$attribVal;');
					eval('$attrib'.$i.'[$arrIdxs[$i]]["children"]=array();');
				}
				
				$arrLast[$i] = $attribVal;
			}
			
			$aRS->MoveNext();
		}
		
		if($lim>1)
		{
			for($j=$lim-2;$j>=0;$j--)
			{
				eval('$attrib'.$j.'[$arrIdxs[$j]]["children"]=$attrib'.($j+1).';');
			}
		}
		
		//@JAPR 2013-02-11: Agregada la edición de datos históricos
		if ($blnAddHistoricData) {
			//Si se regresarán los datos históricos, primero se deben anexar al array los datos históricos y luego los calculados, pero por la
			//manera en que se genera el array dinámicamente, se hace un recorrido al final para pasar de un array al otro
			foreach ($attrib0 as $aParentValueArr) {
				$attribValues[] = $aParentValueArr;
			}
		}
		else {
			$attribValues = $attrib0;
		}
		//@JAPR
		
		return $attribValues;
	}
	
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Genera la dimensión global independiente de cada atributo del catálogo indicado. Este método asume que sólo generará las dimensiones, mas no
	//que las agregará a los modelos de las preguntas que ya estuvieran usando al catálogo, ya que no se puede compartir la funcionalidad entre
	//el modo anterior y el modo de catálogo desasociados, sin embargo una encuesta que esté creando una pregunta de este catálogo debería invocar
	//a este método justo antes de crear a la pregunta para que al grabarla ese proceso si asocie las dimensiones que se requieran
	//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
	//Agregado el parámetro $bForceCreate para que aunque ya se considere convertido el catálogo, de todas formas verifique sus atributos
	//@JAPRDescontinuada en v6
	function generateDissociatedDimensions($bForceCreate = false) {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return true;
		}
		//@JAPR
		
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//Si el catálogo ya estaba marcado para usar dimensiones independientes, quiere decir que ya debió haberlas generado previamente por lo que
		//no es necesario repetir el proceso
		if ($this->DissociateCatDimens && !$bForceCreate) {
			return false;
		}
		
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		if (!is_null($objCatalogMembersColl)) {
			//@JAPR 2013-02-05: Corregido un bug, como no se estaba marcando el catálogo como desasociado, al hacer el saveDissociateDimension de
			//cada atributo pues no estaba generando la dimensión realmente, así que se cambió de posición la asignación de la propiedad del catálogo
			$this->DissociateCatDimens = 1;
			$sql = "UPDATE SI_SV_Catalog SET 
					DissociateCatDimens = {$this->DissociateCatDimens} 
				WHERE CatalogID = ".$this->CatalogID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				return $this->Repository->DataADOConnection->ErrorMsg();
			}
			
			foreach ($objCatalogMembersColl as $objCatalogMember) {
				//@JAPR 2013-02-19: Corregido un bug, sólo estaba creando la dimensión pero no estaba asignandola a la tabla de atributos
				if ($objCatalogMember->IndDimID <= 0) {
					$intIndDimID = (int) $objCatalogMember->saveDissociatedDimension();
					if ($intIndDimID > 0) {
						$objCatalogMember->IndDimID = $intIndDimID;
						//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
						$sql = "UPDATE SI_SV_CatalogMember SET 
								IndDimID = ".$objCatalogMember->IndDimID." 
							WHERE MemberID = ".$objCatalogMember->MemberID;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							if($gblShowErrorSurvey) {
								die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							else {
								return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
						}
					}
				}
				//@JAPR
			}
			
			$this->generateCatalogHierarchy();
		}
		
		return true;
	}
	
	//@JAPR 2013-01-21: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Crea o actualiza la jerarquía asociada al catálogo con dimensiones independientes, ya que al no ser atributos de una misma dimensión no hay
	//manera de conocer las dependencias entre sus valores una vez que se empiezan a grabar en el cubo, por lo que se recurre a las jerarquías originales
	//de los cubos preexistentes al diseño de dimensiones-catálogo. Como no hay métodos en el SDK del Model Manager para este fin, se implementará
	//el grabado directo en la Metadata de Artus
	//@JAPRDescontinuada en v6
	function generateCatalogHierarchy() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		if (!$this->DissociateCatDimens || $this->ModelID <= 0) {
			return;
		}
		
		//Si es la primera vez entonces crea y asocia la jerarquía a este catálogo (se crea vacia simplemente para asociar el ID)
		if ($this->HierarchyID <= 0) {
			$sql =  "SELECT ".$this->Repository->ADOConnection->IfNull("MAX(CLA_JERARQUIA)", "0")." + 1 AS HierarchyID ".
				"FROM SI_JERARQUIA_CAT";
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				return;
			}
			$this->HierarchyID = (int) @$aRS->fields["hierarchyid"];
			
			$sql = "INSERT INTO SI_JERARQUIA_CAT (CLA_JERARQUIA, CLA_CONCEPTO, NOM_JERARQUIA) 
				VALUES ({$this->HierarchyID}, {$this->ModelID}, ".$this->Repository->ADOConnection->Quote($this->CatalogName).")";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA_CAT ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "UPDATE SI_SV_Catalog SET 
					HierarchyID = {$this->HierarchyID} 
				WHERE CatalogID = ".$this->CatalogID;
			$this->Repository->DataADOConnection->Execute($sql);
		}
		
		//Reconstruye la jerarquía, cualquier adecuación realizada por otro producto se perdería, ya que esta es una jerarquía exclusiva de
		//este catálogo, así que no se espera que sea modificada externamente
		if ($this->HierarchyID > 0) {
			//Primero elimina el contenido anterior de la jerarquía por si se han reorganizado atributos
			$sql = "DELETE FROM SI_JERARQUIA WHERE CLA_JERARQUIA = {$this->HierarchyID}";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Obtiene la lista de atributos del catálogo, con ellos se obtendrán los IDs de las dimensiones para agregar a la jerarquía
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
			$arrConsecutivesByAttributeID = array();
			$strClaDescripList = '';
			if (!is_null($objCatalogMembersColl)) {
				foreach ($objCatalogMembersColl as $objCatalogMember) {
					if ($objCatalogMember->ClaDescrip > 0) {
						$strClaDescripList .= ','.$objCatalogMember->ClaDescrip;
						$arrConsecutivesByAttributeID[$objCatalogMember->ClaDescrip] = 0;
					}
				}
				
				$intNumAttributes = count($arrConsecutivesByAttributeID);
				$strClaDescripList = trim(substr($strClaDescripList, 1));
				if ($strClaDescripList != '') {
					$sql = "SELECT A.CLA_DESCRIP, A.CONSECUTIVO 
						FROM SI_CPTO_LLAVE A 
						WHERE A.CLA_CONCEPTO = {$this->ModelID} AND A.CLA_DESCRIP IN ($strClaDescripList)";
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if (!$aRS || $aRS->EOF)
					{
						return;
					}
					
					//Recorre la lista de dimensiones para actualizar el ID de dimensión en el array y así generar la jerarquía mas adelante
					while(!$aRS->EOF)
					{
						$intClaDescrip = (int) @$aRS->fields["cla_descrip"];
						$intConsecutive = (int) @$aRS->fields["consecutivo"];
						if (isset($arrConsecutivesByAttributeID[$intClaDescrip])) {
							$arrConsecutivesByAttributeID[$intClaDescrip] = $intConsecutive;
						}
						$aRS->MoveNext();
					}
					
					//Genera la jerarquía siguiendo el orden de los atributos
					$intCont = 1;
					$intParentDim = 0;
					$intChildDim = 0;
					foreach ($objCatalogMembersColl as $objCatalogMember) {
						$intClaDescrip = $objCatalogMember->ClaDescrip;
						if ($intClaDescrip > 0) {
							$intConsecutive = $arrConsecutivesByAttributeID[$intClaDescrip];
							$intChildDim = $intConsecutive;
							
							//La primer dimensión es la padre de todas, así que esa no se inserta directa. La última dimensión se inserta con un 0
							//como hija pues es la que finaliza la jerarquía, pero no tiene caso dejar una jerarquía de una sola dimensión
							if ($intCont > 1) {
								$sql = "INSERT INTO SI_JERARQUIA (CLA_JERARQUIA, CONSECUTIVO, CLA_CONCEPTO, DIM_PADRE, DIM_HIJA) 
									VALUES ({$this->HierarchyID}, ".($intCont-1).", {$this->ModelID}, $intParentDim, $intChildDim)";
								if ($this->Repository->ADOConnection->Execute($sql) === false)
								{
									//die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
								
								if ($intCont == $intNumAttributes) {
									if ($intNumAttributes == 1) {
										break;
									}
									
									//Es la última dimensión, así que cierra la jerarquía
									$intParentDim = $intChildDim;
									$intChildDim = 0;
									$sql = "INSERT INTO SI_JERARQUIA (CLA_JERARQUIA, CONSECUTIVO, CLA_CONCEPTO, DIM_PADRE, DIM_HIJA) 
										VALUES ({$this->HierarchyID}, $intCont, {$this->ModelID}, $intParentDim, $intChildDim)";
									if ($this->Repository->ADOConnection->Execute($sql) === false)
									{
										//die("(".__METHOD__.") ".translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
									}
								}
							}
							$intParentDim = $intConsecutive;
							$intCont++;
						}
					}
				}
			}
		}
	}
	//@JAPR
}

class BITAMCatalogCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfCatalogIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$where = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE A.CatalogID = ".((int) $anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aCatalogID; 
					}
					if ($where != "")
					{
						$where = "WHERE A.CatalogID IN (".$where.")";
					}
					break;
			}
		}
		
		//@JAPR 2012-08-08: Agregada la versión de datos del catálogo directamente en el catálogo (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', A.VersionNum ';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', A.DissociateCatDimens, A.HierarchyID';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', A.eBavelAppID, A.eBavelFormID';
		}
		//@JAPR

		//@AAL 19/03/2015: Agregado para Switchear entre una forma o una vista de eBavel o ninguna
		if (getMDVersion() >= esvRedesign) {
			$strAdditionalFields .= ', A.eBavelFormType';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.DataSourceID, A.CreationVersion, A.LastVersion';
		}
		//@JAPR 2018-09-17 Agregado el soporte de catálogos offline (#)
		if (getMDVersion() >= esvDataSourceDataVersion) {
			$strAdditionalFields .= ', A.DataVersionDate';
		}
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		//Agregado el JOIN con las preguntas para obtener la referencia a los atributos marcados en cada una y dejarlos asociados directamente al catálogo
		$strAdditionalJoins = '';
		if (getMDVersion() >= esvQuestionAR) {
			$strAdditionalFields .= ", B.IsModel, C.CatMemberID AS QCatMemberID, C.DataSourceMemberID AS QDataSourceMemberID,
				C.CatMemberVersionID AS QCatMemberVersionID, C.DataMemberVersionID AS QDataMemberVersionID,
				C.CatMemberZipFileID AS QCatMemberZipFileID, C.DataMemberZipFileID AS QDataMemberZipFileID,
				C.CatMemberPlatformID AS QCatMemberPlatformID, C.DataMemberPlatformID AS QDataMemberPlatformID,
				C.CatMemberTitleID AS QCatMemberTitleID, C.DataMemberTitleID AS QDataMemberTitleID";
			$strAdditionalJoins .= "LEFT JOIN SI_SV_Question C ON A.CatalogID = C.CatalogID ";
		}
		//JAPR 2018-09-10: Agregado el soporte de catálogos offline (#IAJE6Q)
		//@JAPR 2019-03-29: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
		$sql = "SELECT A.ModelID, A.ParentID, A.CatalogID, A.CatalogName, B.TableName, A.VersionIndID $strAdditionalFields 
			FROM SI_SV_Catalog A 
				INNER JOIN SI_SV_DataSource B ON A.DataSourceID = B.DataSourceID {$strAdditionalJoins}
			".$where." 
			ORDER BY A.CatalogName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		while (!$aRS->EOF) {
			//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
			$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Obtiene la colección de catálogos indicada en el array, cargando todos los elementos que dependen de ella a todos los niveles inferiores de manera masiva para
	optimizar el proceso y posteriormente agregarlos al caché para utilizarlos en las llamadas a los métodos de carga individuales, evitando así llamadas excesivas
	en cascada especialmente durante la obtención de los getJSonDefinition que se usan al obtener las definiciones para las Apps.
	Este proceso sólo se usó originalmente durante la obtención de definiciones para tener aislado el punto de uso a dicho lugar y no afectar en absoluto al resto
	de los procesos de eForms como los del Agente o el Administrador, eventualmente se podría extender a todo eForms */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfCatalogIDs = null) {
		//La carga inicial de la colección de formas se hará mediante la función original de esta colección, la cual ya deja en caché a las formas
		$anInstance = self::NewInstance($aRepository, $anArrayOfCatalogIDs);
		
		if ( !is_null($anInstance) ) {
			//Las cargas subsecuentes de las colecciones de dependencias a todos los niveles se hará enviando la lista de formas obtenida, ya que son las formas
			//válidas que si se encontraron en la Metadata. Estas llamadas dejarán los elementos en los respectivos cachés tanto los originales ya existentes que
			//se asignan en los métodos NewInstanceFromRS, como los cachés indexados por colecciones de las llamadas a los distintos métodos de las colecciones
			//usadas en el proceso de generación de definiciones (getJSonDefinition, los cuales no se encontraban en caché)
			$arrCatalogIDs = array();
			foreach ($anInstance->Collection as $objCatalog) {
				$arrCatalogIDs[] = $objCatalog->CatalogID;
				//Marca a los catálogos como procesadas con carga masiva para que cualquier objeto que necesite cargar alguna información posteriormente pueda determinar si puede o no utilizar
				//los resultados de carga masiva correspondientes
				BITAMGlobalFormsInstance::AddCatalogFullyLoadedFromDB($objCatalog->CatalogID);
			}
			
			$ketCatalogIDsCollection = BITAMCatalogCollection::LoadKeyCatalogAttributesIDFromDBFull($aRepository, $arrCatalogIDs);
			$catalogAttributesCollection = BITAMCatalogCollection::LoadCatalogAttributesFromDBFull($aRepository, $arrCatalogIDs);
			//@JAPR
		}
		
		return $anInstance;
	}
	
	/* Obtiene la colección de atributos de catálogo marcados como clave, subproceso de GetCatalogAttributes */
	static function LoadKeyCatalogAttributesIDFromDBFull($aRepository, $anArrayOfCatalogIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrKeyAttributes = array();
		
		$filter = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND A.CatalogID = ".((int) $anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aCatalogID; 
					}
					if ($filter != "")
					{
						$filter = "AND A.CatalogID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT A.CatalogID, A.MemberID 
			FROM SI_SV_CatalogMember A 
			WHERE A.DescriptorID > 0 ".(($filter)?$filter:"")." 
			ORDER BY A.CatalogID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$intCatalogID = (int) @$aRS->fields["catalogid"];
				$intKeyMemberID = (int) @$aRS->fields["memberid"];
				if ($intCatalogID > 0 && $intKeyMemberID > 0) {
					if ( !isset($arrKeyAttributes[$intCatalogID] ) ) {
						$arrKeyAttributes[$intCatalogID] = array();
					}
					$arrKeyAttributes[$intCatalogID][$intKeyMemberID] = $intKeyMemberID;
				}
				
				$aRS->MoveNext();
			}
		}
		
		foreach ($arrKeyAttributes as $intCatalogID => $arrMemberIDs) {
			BITAMGlobalFormsInstance::AddKeyCatMembersIDsByCatalogWithID($intCatalogID, $arrKeyAttributes[$intCatalogID]);
		}
	}
	
	/* Obtiene la colección de atributos de catálogo como array (NO como instancia) masivamente, equivalente a GetCatalogAttributes */
	static function LoadCatalogAttributesFromDBFull($aRepository, $anArrayOfCatalogIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrCatalogAttribColl = array();
		
		$filter = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE A.CatalogID = ".((int) $anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aCatalogID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE A.CatalogID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', A.IndDimID';
		}
		//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', A.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', A.KeyOfAgenda, A.AgendaLatitude, A.AgendaLongitude, A.AgendaDisplay';
		}
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', A.eBavelFieldDataID';
		}
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$strAdditionalFields .= ', A.IsImage';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.DataSourceMemberID';
		}
		//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
		//Se debe realizar un join con la tabla de preguntas, y dado a que un catálogo sólo puede estar asociado a una única pregunta/sección a partir de v6, no existe posibilidad de 
		//que se pudieran generar múltiples registros por atributo con este query, en los casos donde no exista JOIN se regresará simplemente un NULL en el campo de Question indicando
		//que el atributo no funciona como Ordenamiento en ninguna pregunta, y donde si lo haga servirá para asignar la propiedad que permitirá variar el ORDER BY al obtener los valores
		$strJoinQuestion = '';
		if (getMDVersion() >= esvSChoiceMetro) {
			//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
			$strAdditionalFields .= ', B.QuestionID AS IsQuestionOrder, B.DataMemberOrderDir';
			$strJoinQuestion = "LEFT OUTER JOIN SI_SV_Question B ON A.MemberID = B.CatMemberOrderID ";
		}
	    $sql = "SELECT A.CatalogID, A.MemberID, A.ParentID, A.MemberName, A.MemberOrder $strAdditionalFields 
	    	FROM SI_SV_CatalogMember A {$strJoinQuestion} ".(($filter)?$filter:"")." 
			ORDER BY A.CatalogID, A.MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$intCatalogID = (int) @$aRS->fields["catalogid"];
				$intMemberID = (int) @$aRS->fields["memberid"];
				if ($intCatalogID > 0 && $intMemberID > 0) {
					//Obtiene la lista de IDs atributos llave del cache, el cual debe estar pre-generado antes de invocar a este método
					$arrKeyAttributes = BITAMGlobalFormsInstance::GetKeyCatMembersIDsByCatalogWithID($intCatalogID);
					if ( is_null($arrKeyAttributes) || !is_array($arrKeyAttributes) ) {
						$arrKeyAttributes = array();
					}
					if ( !isset($arrCatalogAttribColl[$intCatalogID] ) ) {
						$arrCatalogAttribColl[$intCatalogID] = array();
					}
					if ( !isset($arrCatalogAttribColl[$intCatalogID][$intMemberID] ) ) {
						$arrCatalogAttribColl[$intCatalogID][$intMemberID] = array();
					}
					
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["id"] = $intMemberID;
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["dimID"] = (int) $aRS->fields["parentid"];
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["name"] = (string) $aRS->fields["membername"];
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["order"] = (int) $aRS->fields["memberorder"];
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["isKey"] = (int) isset($arrKeyAttributes[$intMemberID]);
					//@JAPR 2013-02-11: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["indDimID"] = (int) @$aRS->fields["inddimid"];
					//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
					$inteBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
					$inteBavelFieldDataID = (int) @$aRS->fields["ebavelfielddataid"];
					if ($inteBavelFieldID > 0) {
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["eBavelFieldID"] = $inteBavelFieldID;
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["eBavelFieldDataID"] = $inteBavelFieldDataID;
					}
					if (getMDVersion() >= esvAgendaRedesign) {
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["KeyOfAgenda"] = (int) @$aRS->fields[strtolower("KeyOfAgenda")];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["AgendaLatitude"] = (int) @$aRS->fields[strtolower("AgendaLatitude")];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["AgendaLongitude"] = (int) @$aRS->fields[strtolower("AgendaLongitude")];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["AgendaDisplay"] = (int) @$aRS->fields[strtolower("AgendaDisplay")];
					}
					//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
					if (getMDVersion() >= esvCatalogAttributeImage) {
						$blnIsImage = (int) @$aRS->fields["isimage"];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["isImage"] = ($blnIsImage)?1:0;
					}
					//@JAPR 2016-05-06: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					if (getMDVersion() >= esvSChoiceMetro) {
						$blnIsOrder = (int) @$aRS->fields["isquestionorder"];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["isOrder"] = ($blnIsOrder)?1:0;
						//@JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
						if ($blnIsOrder) {
							$blnHasSortingAttr = true;
						}
						$intSortingDir = (int) @$aRS->fields["datamemberorderdir"];
						$arrCatalogAttribColl[$intCatalogID][$intMemberID]["sortDir"] = ($intSortingDir)?ordbDsc:ordbAsc;
					}
					//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
					$arrCatalogAttribColl[$intCatalogID][$intMemberID]["sourceMemberID"] = (int) @$aRS->fields[strtolower("DataSourceMemberID")];
				}
				
				$aRS->MoveNext();
			}
		}
		
		foreach ($arrCatalogAttribColl as $intCatalogID => $arrCatalogAttributes) {
			BITAMGlobalFormsInstance::AddCatMembersDefByCatalogWithID($intCatalogID, $arrCatalogAttribColl[$intCatalogID]);
		}
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-04-25: Rediseñado el Admin con DHTMLX
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Catalogs");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=CatalogCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=Catalog";
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Catalog";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'CatalogID';
	}

	function get_FormFieldName() {
		return 'CatalogName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;	
	}
	
	function addButtons($aUser)
	{
	}

	function generateBeforeFormCode($aUser)
 	{
	}

	function generateAfterFormCode($aUser)
	{
	}
}
?>