<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Crear dimension FactKey
	if(is_null($surveyInstance->FactKeyDimID) || $surveyInstance->FactKeyDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Nunca se comparte
		$dimensionName = $surveyInstance->SurveyName." ID";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_RAPID_WEB;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;

		$anInstanceModelDim->save();
		chdir($strOriginalWD);
		
		//Insertamos el valor de No Aplica en el catalogo de correo electronico
		BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim, -1, true);
		
		//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
		$aFactKeyDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
		
		$sql = "UPDATE SI_SV_Survey SET FactKeyDimID = ".$aFactKeyDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
}
?>