<?
//@JAPR 2019-07-25: Compactados los archivos js mas pesados para reducir el tiempo de carga, y limpieza general del código para remover archivos ya no utilizados (#2UJ6SK)
global $blnMinified;
global $strMinifiedFiles;
$blnMinified = (bool) @$_REQUEST["minified"];
$strMinifiedFiles = '';
//El comportamiento default será cargar la versión minificada
if ( $blnMinified || !isset($_REQUEST["minified"]) ) {
	$strMinifiedFiles = '.min';
}
//@JAPR
global $strAppUserPwd;
global $strAppUserName;
global $strAppSurveyID;
global $strAppEntryID;
global $intApplicatorID;
global $strApplicationDate;
global $strApplicationTime;
global $strApplicationEndDate;
global $strApplicationEndTime;
global $strApplicationLatitude;
global $strApplicationLongitude;
global $strApplicationWebEntryURL;
//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
global $strApplicationAccuracy;
//@JAPR 2014-06-25: Agregada la posibilidad de hacer login directamente a la captura Web sin pasar por el login de eForms
global $captureVia;
//@JAPR 2014-06-30: Agregado el soporte para multiples idiomas por usuario
global $strAppUserLang;
//@JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
global $strApplicationDefaultSurveyID;
//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
global $strApplicationDataDraftContent;

if (!isset($strAppUserPwd) || is_null($strAppUserPwd)) {
	$strAppUserPwd = '';
}
if (!isset($strAppUserName) || is_null($strAppUserName)) {
	$strAppUserName = '';
}
if (!isset($strAppSurveyID) || is_null($strAppSurveyID)) {
	$strAppSurveyID = '0';
}
if (!isset($strAppEntryID) || is_null($strAppEntryID)) {
	$strAppEntryID = '0';
}
if (!isset($intApplicatorID) || is_null($intApplicatorID)) {
	$intApplicatorID = '';
}
if (!isset($strApplicationDate) || is_null($strApplicationDate)) {
	$strApplicationDate = '';
}
if (!isset($strApplicationTime) || is_null($strApplicationTime)) {
	$strApplicationTime = '';
}
if (!isset($strApplicationEndDate) || is_null($strApplicationEndDate)) {
	$strApplicationEndDate = '';
}
if (!isset($strApplicationEndTime) || is_null($strApplicationEndTime)) {
	$strApplicationEndTime = '';
}
if (!isset($strApplicationLatitude) || is_null($strApplicationLatitude)) {
	$strApplicationLatitude = '';
}
if (!isset($strApplicationLongitude) || is_null($strApplicationLongitude)) {
	$strApplicationLongitude = '';
}
//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
if (!isset($strApplicationAccuracy) || is_null($strApplicationAccuracy)) {
	$strApplicationAccuracy = '';
}
//@JAPR 2014-06-30: Agregado el soporte para multiples idiomas por usuario
if (!isset($strAppUserLang) || is_null($strAppUserLang)) {
	$strAppUserLang = '';
}
//@JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
if (!isset($strApplicationDefaultSurveyID) || is_null($strApplicationDefaultSurveyID)) {
	$strApplicationDefaultSurveyID = '';
}
//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
if (!isset($strApplicationDataDraftContent) || is_null($strApplicationDataDraftContent)) {
	$strApplicationDataDraftContent = '';
}

//@JAPR 2014-06-25: Agregada la posibilidad de hacer login directamente a la captura Web sin pasar por el login de eForms
$intManualLogin = 0;
if (isset($_REQUEST['login'])) {
	$intManualLogin = 1;
}

//@JAPR 2014-06-25: Agregada la posibilidad de hacer login directamente a la captura Web sin pasar por el login de eForms
if ($intManualLogin) {
	$masterUser = '';
}
else {
	//02Mayo2013: En capturas por liga de correo se utiliza el usuario maestro
	$masterUser = 1;
	$sql = "SELECT CLA_USUARIO FROM SI_USUARIO WHERE NOM_CORTO = ".$theRepository->ADOConnection->Quote($theRepository->DataADODBDatabase);
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die("(indexHTML.inc.php)".translate("Error accessing")." "."SI_USUARIO"." ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	if(!$aRS->EOF)
	{
		$masterUser = (int) $aRS->fields["cla_usuario"];
	}
}

//@JAPR 2014-06-30: Agregado el soporte para multiples idiomas por usuario
//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
//Si se recibió un ApplicatorID, quiere decir que se deberá impersonar al usuario según este parámetro, así que no se usa directamente la instancia de usuario que corresponde al
//usuario actualmente logeado al Administrador
//Finalmente el idioma no tiene mucha reelevancia puesto que al iniciar la carga de la captura web se intentará el login con la cuenta indicada y eso asignará el idioma real del usuario
if (isset($theUser) && (int) @$intApplicatorID <= 0) {
	$strAppUserLang = strtolower(trim((string) @$theUser->UserLanguageTerminator));
}
else if (!$intManualLogin || (int) @$intApplicatorID > 0) {
	//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
	//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
	//Ahora se considerará el caso donde se recibió un ApplicatorID para que se obtenga el idioma del usuario a impersonar. Se corrigió un bug, no estaba incluyendo en el FROM la tabla
	//de idioma así que el query siempre fallaba
	$sql = "SELECT A.WHTMLANG, B.TERMINADOR 
		FROM SI_USUARIO A, SI_IDIOMA B 
		WHERE A.WHTMLANG = B.CLA_IDIOMA AND A.CUENTA_CORREO = ".$theRepository->ADOConnection->Quote($strAppUserName);
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die("(indexHTML.inc.php)".translate("Error accessing")." "."SI_USUARIO"." ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	if(!$aRS->EOF)
	{
		$strAppUserLang = strtolower((string) @$aRS->fields["terminador"]);
		if ($strAppUserLang == '@@') {
			$strAppUserLang = '';
		}
	}
}
//@JAPR

//09Mayo2013
$catalogID = 0;
$emailFilter = -1;
$catalogFilter = "";
if(isset($_SESSION["SVExternalEmailKey"]))
{
	$emailKey = $_SESSION["SVExternalEmailKey"];
}

if($captureVia && $captureEmail!="")
{	//Si es vía correo
	if($schedulerInstance->UseCatalog && $schedulerInstance->CatalogID>0 && $schedulerInstance->AttributeID>=0)
	{
		$catalogID = $schedulerInstance->CatalogID;
		$emailFilter = $schedulerInstance->EmailFilter;
		$eMailAttributeID = $schedulerInstance->AttributeID;
		
		if($emailFilter>0)
		{
			//Es necesario aplicar filtro por correo en las preguntas simple choice
			//Obtener el parámetro catalogFilter
			$intCatalogID = $catalogID;
			$catalogInstance = BITAMCatalog::NewInstanceWithID($theRepository, $intCatalogID);
			
			$arrCatFilter = array();
			$arrCatalogMembers = null;
			$arrCatalogMembers = BITAMCatalogMemberCollection::NewInstance($theRepository, $intCatalogID);
			
			if (is_null($arrCatalogMembers))
			{
				$arrCatalogMembers = array();
			}
			
			//Obtiene el valor de todos los miembros del catálogo hasta el atributo de email, para generar el 
			//Array completo del filtro para ese catálogo
			if (!is_null($arrCatalogMembers) && count($arrCatalogMembers->Collection) > 0)
			{
				$tableName = $catalogInstance->TableName;
				$fieldKey = "RIDIM_".$catalogInstance->ParentID."KEY";
				
				
				if($emailFilter==1)
				{
					$strFields = "";
					foreach ($arrCatalogMembers as $aCatalogMember)
					{
						$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
						if($strFields!="")
						{
							$strFields.=", ";
						}
						$strFields .= $fieldName;
					}
					//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
					$sql = "SELECT ".$strFields." FROM ".$tableName." WHERE ".$fieldKey." = ".$emailKey." ORDER BY ".$fieldKey;
				}
				else
				{
					//Cuando $emailFilter=2
					$sql = "SELECT ";
					foreach ($arrCatalogMembers as $aCatalogMember)
					{
						$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
						if ($aCatalogMember->MemberID == $eMailAttributeID)
						{
							//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
							$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$theRepository->DataADOConnection->Quote($captureEmail)." ORDER BY ".$fieldKey;
							break;
						}
						else 
						{
							//Es un padre, lo agrega directamente al query
							$sql .= $fieldName.', ';
						}
					}
				}
				$aRSCat = $theRepository->DataADOConnection->Execute($sql);
				if ($aRSCat)
				{
					if (!$aRSCat->EOF)
					{
						$arrCatFilter = array();
						
						if($emailFilter==1)
						{
							foreach ($arrCatalogMembers as $aCatalogMember)
							{
								$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
								$arrCatFilter[] = (string) $aRSCat->fields[$fieldName];
							}
						}
						else
						{
							foreach ($arrCatalogMembers as $aCatalogMember)
							{
								$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
								$arrCatFilter[] = (string) $aRSCat->fields[$fieldName];
								if ($aCatalogMember->MemberID == $eMailAttributeID)
								{
									break;
								}
							}
						}
						$catalogFilter = implode($arrCatFilter, "_SEPEF_");
					}
				}
			}
		}
	}
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="chrome=1">
		<title>KPIOnline Forms v6</title>

	<!--[if IE]> <![if !IE]> <![endif]-->
		<link type="text/css" rel="stylesheet" href="jquery.mobile-1.4.5/jquery.mobile-1.4.5<?=$strMinifiedFiles?>.css"/>
		<link type="text/css" rel="stylesheet" href="jquery.mobile-forms.css"/>
		<link rel="stylesheet" href="jquery_signaturepad/jquery.signaturepad.css"/>
      	<link rel="stylesheet" href="spinningwheel/spinningwheel.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="css/fileuploader.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="js/jquery/demos.css" type="text/css"/>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<!-- solo se cambio de ui a jquery-ui-1.11.4 -->
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.autocomplete.css">
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.theme.css">
		<link rel="stylesheet" href="js/chosen/chosen.css" />
		<link type="text/css" rel="stylesheet" href="css/inline.css"/>
		<link type="text/css" rel="stylesheet" href="css/table.css"/>
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx<?=$strMinifiedFiles?>.css"/>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX<?=$strMinifiedFiles?>.css"/>
		<link href="fonts/fontawesome/css/fontawesome.css" rel="stylesheet">
		<link href="fonts/fontawesome/css/brands.css" rel="stylesheet">
		<link href="fonts/fontawesome/css/solid.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/customOpenLayers.css">
		<link rel="stylesheet" type="text/css" href="css/defaultStyles.css"/>
		<!--JAPR 2019-02-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)-->
		<link rel="stylesheet" href="css/openlayers/ol.css" type="text/css">
		<style type="text/css" media="screen">
			/* Conchita 2014-10-29 agregada esta linea para q los dialogos no tengan btn de close */
			//AAL 18/05/2015 issue AJSLOR: No permitía mostrar el botón empleado para cerrar el componente comboBox, por lo tanto se comenta.
			//.ui-dialog  .ui-header .ui-btn-icon-notext  { display:none;} 
			//AAL
		   	.ui-page {
				-webkit-touch-callout: none;
				-webkit-user-select: none;
		   	}
			
		   	.ui-footer.ui-bar {
				padding-left: 0px;
				padding-right: 0px;
		   	}
			
		    h3 {
				color: white;
			}
			
			.mdRecSelected {
				border: 1px solid #155678;
				background: #4596ce;
				font-weight: bold;
				color: #fff !important;
				cursor: pointer;
				text-shadow: 0 -1px 1px #145072;
				text-decoration: none;
				background-image: -webkit-gradient(linear, left top, left bottom, from( #85bae4 ), to( #5393c5 ));
				background-image: -webkit-linear-gradient(#85bae4 , #5393c5 );
				background-image: -moz-linear-gradient(#85bae4 , #5393c5 );
				background-image: -ms-linear-gradient(#85bae4 , #5393c5 );
				background-image: -o-linear-gradient(#85bae4 , #5393c5 );
				background-image: linear-gradient(#85bae4 , #5393c5 );
				font-family: Helvetica, Arial, sans-serif;
			}			
			@font-face { font-family: Roboto; src: url('css/font/Roboto-Regular.ttf'); } 	
		</style>
		
		<!-- Sólo para el App -->
		<!-- <script type="text/javascript" src="cordova.js"></script> -->
		<!-- <script type="text/javascript" src="cordova_plugins.js"></script> -->
		<!-- <script type="text/javascript" src="GCMPlugin.js"></script> --> <!-- Conchita, agregado GCM Cordova plugin -->
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<script type="text/javascript" src="js/jquery-1.11.3<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/jquery/jquery-ui-1.11.4/jquery-ui<?=$strMinifiedFiles?>.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<script type="text/javascript" src="jquery.mobile-1.4.5/jquery.mobile-1.4.5<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="jquery_signaturepad/flashcanvas.js"></script>
		<script type="text/javascript" charset="utf-8" src="js/jpeg_encoder_basic.js"></script>
		<!--1-Dic-2014 se cambio x el sin minificar para modificar el comportamiento del mismo -->
		<!--OMMC 2015-12-04: Agregado librería de moment para las validaciones de fecha de las preguntas OCR-->
		<script type="text/javascript" src="js/moment-with-locales<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="jquery_signaturepad/jquery.signaturepad<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="spinningwheel/spinningwheel.js"></script>
		<script type="text/javascript" src="js/FileSaver/FileSaver.js"></script>
		<script type="text/javascript" src="js/BlobBuilder/BlobBuilder.js"></script>
		<script type="text/javascript" src="js/fileuploader<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/dateshtm.js"></script>
		<!-- JAPR 2019-03-25: Agregado el nuevo tipo de pregunta AR (#4HNJD9) -->
		<script type="text/javascript" src="js/underscore-min.js"></script>
		<script>
		$(document).bind("mobileinit", function(){
			$.mobile.defaultTransition = 'none';
		});
		</script>
        <script type="text/javascript" src="BITAMXMLObject.js"></script>
		<script type="text/javascript" src="js/utils<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/filemanager<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/gpsmanager<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/mobileDetection.js"></script>
		<script type="text/javascript" src="js/appConstants.js"></script>
		<script type="text/javascript" src="js/openLayersSketch<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="eSurveyServiceMod<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="eSurveyServiceModFns<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/chosen/chosen.jquery<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/defaultLanguage.js"></script>
		<script type="text/javascript" src="js/masonry/masonry.pkgd.min.js"></script>
		<script src="js/codebase/dhtmlxFmtd<?=$strMinifiedFiles?>.js"></script>
        <script type="text/javascript" charset="utf-8" src="PushNotification.js"></script>
        <link type="text/css" rel="stylesheet" href="css/stylepanel.css"/>
        <script type="text/javascript" src="js/jquery.jpanelmenu.js"></script>
        <script type="text/javascript" src="jquery.touchSwipe.js"></script> <!--touch events supported -->
		<!--<script type="text/javascript" src='js/jsPDF/libs/require/require.js'></script>-->
		<!--<script type="text/javascript" src='js/jsPDF/jspdf.js'></script>
		<script type="text/javascript" src="js/jsPDF/libs/html2pdf.js"></script> 
		<script type="text/javascript" src="js/jsPDF/plugins/canvas.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/context2d.js"></script>
		<script type="text/javascript" src="js/jsPDF/libs/html2canvas/dist/html2canvas.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/standard_fonts_metrics.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/split_text_to_size.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/png_support.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/annotations.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/addimage.js"></script>
		<script type="text/javascript" src="js/jsPDF/libs/png_support/png.js"></script> 
		<script type="text/javascript" src="js/jsPDF/libs/png_support/zlib.js"></script> 
		<script type="text/javascript" src="js/jsPDF/libs/css_colors.js"></script>
		<script type="text/javascript" src="js/jsPDF/libs/deflate.js"></script>
		<script type="text/javascript" src="js/jsPDF/libs/polyfill.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/acroform.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/addhtml.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/addimage.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/autoprint.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/cell.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/from_html.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/javascript.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/outline.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/prevent_scale_to_fit.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/svg.js"></script>
		<script type="text/javascript" src="js/jsPDF/plugins/total_pages.js"></script>-->
		<!--JAPR 2019-02-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)-->
		<script type="text/javascript" src="js/openlayers/ol.js"></script>

		<script language="JavaScript">
			var jPM;
			var fmenu = true;
			function OnBodyLoad()
			{
				activateMenu();
				if (startUpApp && Function == startUpApp.constructor) {
					startUpApp();
				}
				else {
					alert('Failed to load KPIOnline Forms library');
				}
			}
			
			function OnBodyUnload() {
				alert('xit');
				if (shutDownApp && Function == shutDownApp.constructor) {
					shutDownApp();
				}
			}
		</script>
	<!--[if IE]> <![endif]> <![endif]-->
	
	<!--[if IE]>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>
		<style>
		/* 
		CSS rules to use for styling the overlay:
		  .chromeFrameOverlayContent
		  .chromeFrameOverlayContent iframe
		  .chromeFrameOverlayCloseBar
		  .chromeFrameOverlayUnderlay
		*/
		</style>
		
		<script language="JavaScript">
			function checkChromeFrame() {
				if (CFInstall.isAvailable && CFInstall.isAvailable()) {
					window.location.href = "index.html";
				}
				else {
					CFInstall.check({
						mode: "overlay",
						destination: "<?=@$strApplicationWebEntryURL?>"
					});
				}
			}
		</script>
		
		<script language="JavaScript">
			function OnBodyLoad()
			{
				if (typeof checkChromeFrame == 'function') {
					checkChromeFrame();
				}
			}
		</script>
	<![endif]-->
	
	</head>
	
	<body onload="OnBodyLoad()">
		<div data-role="page" data-url="emptyPage" id="emptyPage">
		</div>
		<!--INICIA CODIGO MENU -->
		<header class="main">
		<!--JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)-->
        <ul id="menu" class="jpanelstyle">
			<li formsLogo id="formslogo" style="display:block;"><a style="font-family:Roboto;font-size:16px; " href=""><img style="position:center;width:120px;" id="logoKpi" src="customAppIcons/logoMenu.png"/></a></li>
            <li myschedulemenu id="opc1" style="font-family:Roboto;display:block;"><a  style="font-family:Roboto;font-size:16px; " myschedulemenuaction href="javascript: jPM.close();loadAgendaList();">My Schedule</a></li>
            <li formsmenu id="opc2" style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " formsmenuaction href="javascript: jPM.close(); objApp.loadPage('mainpage');">Forms</a></li>
            <li draftsmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " draftsmenuaction href="javascript: jPM.close(); objApp.loadPage('draftgrouppage');">Drafts</a></li>
            <li outboxmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " outboxmenuaction href="javascript: jPM.close(); objApp.loadPage('outboxgrouppage');">Outbox</a></li>
			<li syncmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " syncmenuaction href="javascript: jPM.close(); syncToServer();">Update Definitions</a></li>
			<!--<li updefmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " updefaction href="javascript: jPM.close(); refreshFromServer();">Refresh from server</a></li>-->
		    <li advancedmenu style="font-family:Roboto;display:block;" id="ccc" name="ccc"><a style="font-family:Roboto;font-size:16px; " advancedmenuaction href="javascript: jPM.close(); objApp.loadPage('advancedSettings');">Advanced Settings</a></li>
			<li loginmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " loginmenuaction href="javascript:jPM.close(); objApp.loadPage('loginpage')">Login Page</a></li>
			<li savedraftmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " savedraftmenuaction href="javascript:jPM.close(); selSurvey.save(1)">Save Draft</a></li>
			<li aboutmenu style="font-family:Roboto;display:block;"><a style="font-family:Roboto;font-size:16px; " aboutmenuaction href="javascript: jPM.close(); objApp.loadPage('aboutESurvey')">About</a></li>
			<li nothing class="emptyLi" style="height:100px;"></li>
			<!--<li socialNetBar id="socialNetBar" style="display:-webkit-box; /*position:absolute;bottom:105px;*/border-top:0px;border-bottom:0px;">
				<a style="border-top:0px;border-bottom:0px;" target="_blank" href="https://www.facebook.com/kpiforms?fref=ts"><img src="images/facebook.png"/></a><a style="border-top:0px;border-bottom:0px;" target="_blank" href="https://twitter.com/KpiForms"><img src="images/twitter.png"/></a>
				<a style="border-top:0px;border-bottom:0px;" target="_blank" href="https://www.linkedin.com/company/kpiforms"><img src="images/linkedin.png"/></a><a style="border-top:0px;border-bottom:0px;" target="_blank" href="https://plus.google.com/105171256077728872475/posts/p/pub"><img src="images/googleplus.png"/></a>
				<a style="font-family:Roboto;font-size:16px;border-top:0px;border-bottom:0px;" target="_blank" href="http://kpionline.bitam.com/chat/chat.php"><img src="images/support.png"/></a>
			</li>-->
            <!--<li leyenda id="leyenda" align="center" style="font-family:Roboto;font-size:16px;display:-webkit-box; /*position:absolute;bottom:48px; */text-align:center;align:center;"><div style="align:center;text-align:center;"><label align="center" style="align:center;text-align:center;">www.kpiforms.com Evoluciona tu forma de colectar datos</label></div></li>-->
        </ul>
		</header>
<!-- FIN CODIGO MENU -->
		<div data-role="page" data-url="loginData" id="loginData" style="display:none">
			<input type="text" id="loginDataUser" name="loginDataUser" value="<?=$strAppUserName?>">
			<input type="password" id="loginDataPwd" name="loginDataPwd" value="<?=$strAppUserPwd?>">
			<input type="text" id="loginDataLang" name="loginDataLang" value="<?=$strAppUserLang?>">
			<input type="text" id="loginDataSurveyID" name="loginDataSurveyID" value="<?=$strAppSurveyID?>">
			<input type="text" id="loginDataEntryID" name="loginDataEntryID" value="<?=$strAppEntryID?>">
			<input type="text" id="loginDataApplicatorID" name="loginDataApplicatorID" value="<?=$intApplicatorID?>">
			<input type="text" id="loginDataDateID" name="loginDataDateID" value="<?=$strApplicationDate?>">
			<input type="text" id="loginDataHourID" name="loginDataHourID" value="<?=$strApplicationTime?>">
			<input type="text" id="loginDataEndDateID" name="loginDataEndDateID" value="<?=$strApplicationEndDate?>">
			<input type="text" id="loginDataEndHourID" name="loginDataEndHourID" value="<?=$strApplicationEndTime?>">
			<input type="text" id="loginDataLatitude" name="loginDataLatitude" value="<?=$strApplicationLatitude?>">
			<input type="text" id="loginDataLongitude" name="loginDataLongitude" value="<?=$strApplicationLongitude?>">
			<input type="text" id="loginDataAccuracy" name="loginDataAccuracy" value="<?=$strApplicationAccuracy?>">
			<input type="text" id="CaptureVia" name="CaptureVia" value="<?=$captureVia?>">
			<input type="text" id="CaptureEmail" name="CaptureEmail" value="<?=$captureEmail?>">
			<input type="text" id="SchedulerID" name="SchedulerID" value="<?=$schedulerID?>">
			<input type="text" id="masterUser" name="masterUser" value="<?=$masterUser?>">
			<input type="text" id="loginDataCatalogID" name="loginDataCatalogID" value="<?=$catalogID?>">
			<input type="text" id="loginDataEmailFilter" name="loginDataEmailFilter" value="<?=$emailFilter?>">
			<input type="text" id="loginDataCatalogFilter" name="loginDataCatalogFilter" value="<?=$catalogFilter?>">
			<?if ($intManualLogin) {?>
			<input type="text" id="loginDataManual" name="loginDataManual" value="<?=$intManualLogin?>">
			<?}?>
			<?if ($strApplicationDefaultSurveyID >= '') {?>
			<input type="text" id="loginDefaultSurveyID" name="loginDefaultSurveyID" value="<?=$strApplicationDefaultSurveyID?>">
			<?}?>
			<?if ($strApplicationDataDraftContent >= '') {
			//@JAPR 2017-01-10: Agregada la posibilidad de editar capturas grabadas desde el Admin en la captura en línea, utilizando un draft pre-cargado como parámetro para simular
			//que se estuviera cargando un draft de archivo pero en browser (#J4HPY4)
			?>
			<input type="text" id="loginDataDraftContent" name="loginDataDraftContent" value="<?=$strApplicationDataDraftContent?>">
			<?}?>
		</div>
	</body>
</html>