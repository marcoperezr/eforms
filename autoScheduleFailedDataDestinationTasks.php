<?php
//Archivo que se manda llamar para reprocesar los destinos que fallaron de manera automátizada (originalmente solo por repositorio) (#UWINQC)
session_start();

//@JAPR 2018-02-09: Agregado el proceso (que debe ser invocado desde una tarea de windows con parámetros) para automáticamente recalendarizar todas las tareas de
//destinos de datos que hubieran generado algún error, de tal manera que el usuario no tenga que ir manualmente a la bitácora de captura a reprocesarlas. Originalmente
//utilizado para Idealease, ya que sus destinos de datos marcaban un connection reset (HTTP Error 0) y no se determinó por qué, pero además no querían entrar a la
//bitacora de destinos manualmente a reprocesar todos los destinos que hubieran fallado, en ese caso reintentar el envío del destino era suficiente para que se
//corrigiera el error en la mayoría de los casos, así que esta alternativa funcionaba adecuadamente en su caso (otros destinos pudieran fallar por cambios en las
//credenciales de autenticación, en esas situaciones reprocesarlos constantemente cada que fallen será contraproducente porque necesitan interacción del usuario para
//corregirlos y si nunca los ve en la bitácora por haberlos cambiado a estado de reprocesamiento, nunca tomará una acción al respecto para corregirlos)
global $blnSaveLogStringFile;
$blnSaveLogStringFile = true;

//Esta implementación por tratarse de un tipo de Agente que corre en una tarea de windows, usará checkCurrentSession.inc.php para crear la conexión al repositorio
//basandose en los parámetros recibidos, con los cuales sobreescribirá las variables de sesión que generalmente debería haber estado asignadas, por tanto si no se
//especificaron parámetros no se podrá iniciar el proceso. Adicionalmente generará un log de todas las tareas que sean reprocesadas
$strAgentRepositoryName = '';
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Punto de entrada de un agente, deberá asignar el AgentName así como enviarlo a la función GeteFormsLogPath para que se registre en las llamadas subsecuentes
global $strAgentName;
//@JAPR
$strAgentName = '';

if (isset($argv) && is_array($argv) && count($argv) >= 2) {
	//Configuraciones por línea de comando
	foreach ($argv as $intParamNum => $strParamVal) {
		switch ($intParamNum) {
			case 0:		//Script name
				//Es irrelevante, es el nombre del archivo php del Agente
				break;
			case 1:		//Repository
				$strAgentRepositoryName = strtolower(trim((string) $strParamVal));
				break;
			//@JAPR 2015-10-14: Corregido un bug, el identificador único sólo contenía el día y la hora incluyendo segundos, pero en un caso dos agentes si lograron entrar al mismos segundo así que
			//se llegó a duplicar. En circunstancias normales eso no habría importado porque generalmente nunca dos agentes apuntarán al mismo repositorio, sin embargo hubo un error (ya corregido)
			//donde el repositorio no se tomaba en cuenta para un SELECT, sino sólo el AgentID, así que se confundieron y dos agentes procesaron la misma tarea, con este uniqid + un prefijo enviado
			//como parámetro en la URL de la tarea de Windows, se evitará repetir nuevamente este caso incluso si se deciden activar múltiples agentes para el mismo repositorio (en ese caso, el
			//agentName sería obligatorio para asegurar que nunca se repetirá el ID, en cualqueir otro caso es opcional) (#NQ995V)
			//@JAPR 2018-02-06: Faltaba agregar el parámetro vía tarea de windows desde el command prompt, así que se integró la posibilidad de variar
			//el nombre del archivo también desde línea de comando
			case 2:
				$strAgentName = (string) $strParamVal;
				break;
			//@JAPR
		}
	}
}
else {
	//Verifica si hay configuraciones por medio de un request
	$strAgentRepositoryName = strtolower(trim((string) @$_REQUEST['Repository']));
	$strAgentName = trim((string) @$_REQUEST['AgentName']);
}

//Archivo que se manda llamar para reprocesar los destinos que fallaron
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("report.inc.php");
require_once("surveytask.inc.php");
require_once("eSurveyServiceMod.inc.php");

//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//Integradas las funciones comunes para almacenamiento de log de eForms
$strLogPath = GeteFormsLogPath(null, $strAgentName);
//@JAPR

$strDefaultLogStringFile = "eFormsAgentDestRep_{$strAgentRepositoryName}_{$strAgentName}".date("Ymd").'.log';
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//$sFilePath = "log/";
$sFilePath = $strLogPath;
//$sFilePath = substr(ADODB_DIR, 0, strlen(ADODB_DIR)-5).'log/';
//@JAPR

//Si no se recibieron los parámetros necesarios para la conexión, no puede continuar con este proceso
if ( $strAgentRepositoryName == "" ) {
	echo("\r\n".date("Y-m-d H:i:s").'- '."Unable to initiate the eForms data destination rescheduler agent because the repository to process was not specified");
	@error_log("\r\n".date("Y-m-d H:i:s").'- '."Unable to initiate the eForms data destination rescheduler agent because the repository to process was not specified", 3, $sFilePath.$strDefaultLogStringFile);
	die();
}

echo("\r\n".str_repeat('*', 80));
@error_log("\r\n".str_repeat('*', 80), 3, $sFilePath.$strDefaultLogStringFile);
echo("\r\n".date("Y-m-d H:i:s").'- '."Starting eForms data destination rescheduler agent");
@error_log("\r\n".date("Y-m-d H:i:s").'- '."Starting eForms data destination rescheduler agent", 3, $sFilePath.$strDefaultLogStringFile);

//Este agente iniciará siempre como el usuario administrador de la cuenta, lo que en KPIOnline significa que el nombre corto es lo mismo que el repositorio pero con DWH
$strUserName = str_ireplace("_BMD_", "_DWH_", $strAgentRepositoryName);

//Asigna las variables de sesión para que checkCurrentSession.inc.php pueda realizar la conexión a la BD de repositorio y cargar los objetos iniciales
$_SESSION["PABITAM_RepositoryName"] = $strAgentRepositoryName;
$_SESSION["PABITAM_UserName"] = $strUserName;
//@JAPR

$aBITAMConnection = connectToKPIRepository();
$aRepository = $theRepository;

//Se necesita asignar esta variable global para que BITAMSurveyTask haga referencia a las tareas en la bitácora de capturas en lugar de a las tareas aún no procesadas
global $typeID;
$typeID = rptDataDest;

//Ejecuta la consulta al repositorio de KPIOnline para determinar si existen o no destinos de datos con error sin reprocesar
$arrDestinationsByTask = array();
$sql = "SELECT DISTINCT B.SurveyTaskLogID AS SurveyTaskID, B.DestinationID, B.SurveyDestinationLogID 
	FROM SI_SV_SurveyTasksLog A 
	INNER JOIN SI_SV_SurveyDestinationLog B ON (A.SurveyTaskID = B.SurveyTaskLogID) 
	WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($strAgentRepositoryName)." AND 
		B.Status = 1 AND (B.ProcessStatus IS NULL OR B.ProcessStatus = 0) 
	ORDER BY A.CreationDateID";		//AND A.SurveyID = 145 /*Sólo para depuración*/
$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
if ($aRS) {
	while (!$aRS->EOF) {
		$surveyTaskID = (int) @$aRS->fields["surveytaskid"];
		$destinationID = (int) @$aRS->fields["destinationid"];
		$surveyDestinationLogID = (int) @$aRS->fields["surveydestinationlogid"];
		if ( $surveyTaskID > 0 && $destinationID > 0 ) {
			if ( !isset($arrDestinationsByTask[$surveyTaskID]) ) {
				$arrDestinationsByTask[$surveyTaskID] = array();
			}
			//Se requiere como índice $surveyDestinationLogID para que este proceso sea idéntico al del archivo processDataDestination.php
			$arrDestinationsByTask[$surveyTaskID][$surveyDestinationLogID] = $destinationID;
		}
		$aRS->MoveNext();
	}
}

//Verifica si hay destinos que procesar, en caso de no existir ninguno ya no tiene caso que continue
if ( count($arrDestinationsByTask) == 0 ) {
	logString('No failed data destinations were found');
	logString('Finishing eForms data destination rescheduler agent');
	die();
}

//A partir de este punto generará las tareas de reprocesamiento de los destinos de datos que hubieran fallado
foreach ($arrDestinationsByTask as $surveyTaskID => $arrDestIDs) {
	if ( count($arrDestIDs) ) {
		$strDataDestinations = implode(",", $arrDestIDs);
		logString("surveyTaskID={$surveyTaskID}, strDataDestinations=".$strDataDestinations);
		//A diferencia de como se hace en processDataDestination.php, no se necesita esta carga porque ya se tiene toda la información que se obtendría con estos
		//objetos directamente de la consulta de arriba, así que simplemente carga la tarea correspondiente y asigna los destinos identificados para reprocesamiento
		/*//Valores default para los parámetros al obtener la colección
		$searchString="";$startDate="";$endDate="";$GroupID="";$UserID="";$aSurveyID="";$FilterDate="";$ipp=100;
		//IDs de destinos seleccionados por el usuario para el procesamiento
		$surveyDestinationLogIDs=$strDataDestinations;
		$fromProcessDataDest=true;
		$dataDestinations = BITAMReportCollection::NewInstanceToFormsLog($aRepository, $searchString, $startDate, $endDate, $GroupID, $UserID, $aSurveyID, $FilterDate, $ipp,$surveyDestinationLogIDs, $fromProcessDataDest);*/
		
		$objSurveyTask = BITAMSurveyTask::NewInstanceWithID($aRepository, $surveyTaskID);
		if ( !$objSurveyTask ) {
			continue;
		}
		$objSurveyTask->Reprocess = true;
		$objSurveyTask->DestinationIDs = array_values($arrDestIDs);
		logString('SurveyTaskID: '.$surveyTaskID);
		logString('CreationDateID: '.$objSurveyTask->CreationDateID);
		logString('SurveyID: '.$objSurveyTask->SurveyID);
		logString('SurveyDate: '.$objSurveyTask->SurveyDate);
		logString('User: '.$objSurveyTask->User);
		//@JAPR 2018-02-08: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		logString('SurveyKey: '.$objSurveyTask->SurveyKey);
		//@JAPR
		logString('ForceNew: '.$objSurveyTask->ForceNew);
		logString('***count(DestinationIDs): '.count($objSurveyTask->DestinationIDs));
		foreach($objSurveyTask->DestinationIDs as $aD)
		{
			logString(': '.$aD);
		}
		logString('--------------------------'.$strDataDestinations);
		
		//Reprograma la tarea para una fecha posterior de tal manera que el agente de eForms vuelva a tomar los destinos de datos y los intente procesar nuevamente
		$resultReschedule = $objSurveyTask->reschedule($aBITAMConnection);
		
		if($resultReschedule)
		{
			logString('Tarea agregada para reprocesar destinos');
			//Cambiar el estado de los registros de destinos para marcar que ya se mandaron a reprocesar,
			//para que ya no sea desplegado en la pantalla de bitácoras de destinos
			updateDesinationReprocess($aBITAMConnection, array_keys($arrDestIDs));
			
			//Este proceso no es neceario, en processDataDestination.php era solo para remover las rows de destinos fallidos que habían sido enviadas a reprocesar
			/*//Para cada destinationLogID que fue procesado se agrega al arreglo $rowsPro la clave de la fila que tiene en el grid
			foreach($arrSurveyDestLogID[$capID] as $aDL)
			{
				$rowsPro["rows"][] = $rowIDsByDestID[$aDL];
				error_log("\r\n rowsPro[rows][]=".$rowIDsByDestID[$aDL],3,"./log/testDestinos.log");
			}*/
		}
		else
		{
			logString('La tarea no pudo ser agregada para reprocesar destinos');
		}
		logString('');
	}
}

logString('Finishing eForms data destination rescheduler agent');

//Cambiar el estado de los registros de destinos para marcar que ya se mandaron a reprocesar,
//para que ya no sea desplegado en la pantalla de bitácoras de destinos
function updateDesinationReprocess($aBITAMConnection, $surveyDestinationLogIDs)
{
	if(!is_array($surveyDestinationLogIDs) || count($surveyDestinationLogIDs)<=0)
	{
		return;
	}
	$strSurveyDestLogIDs = implode(",",$surveyDestinationLogIDs);
	$sql = "UPDATE SI_SV_SurveyDestinationLog SET ProcessStatus = 1 WHERE SurveyDestinationLogID IN (".$strSurveyDestLogIDs.")";
	$aBITAMConnection->ADOConnection->Execute($sql);
	
	logString('Actualizar estado de destino a reprocesado:'.$sql);
}
?>