<?php
//@JAPR 2014-06-05: A partir de esta fecha, se reimplementó esta clase para filtros dinámicos de catálogos a nivel de encuesta
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("filtersv.inc.php");
require_once("dataSource.inc.php");

class BITAMSurveyFilter extends BITAMObject
{
	public $FilterID;						//ID único del filtro
	public $SurveyID;						//ID de encuesta para la que aplica
	public $FilterName;
	public $FilterText;						//Texto SQL del filtro, a diferencia del mismo campo en SI_SV_CatFilter, este se generará como un filtro SQL tradicional, con variables para representar los campos de los catálogos (ej. @CMID15) y con posibilidad de usar variables del App
	public $FilterTextUsr;					//Text SQL del filtro, a diferencia de FilterText, este contiene nombres de variables entendibles por el usuario (ej. @AttrZone) en lugar de las variables internas con IDs (ej. @CMID15, donde 15 es el CatMemberID)
	public $FilterLevels;					//No aplica, dejado por compatibilidad con la tabla, aunque internamente contendrá los IDs de atributos usados como variables @AttrName en el FilterText
	public $CatalogID;						//ID del catálogo (que debe estar asociado a la encuesta) para el que aplica
	//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
	public $DataSourceID;					//ID del DataSource correspondiente al CatlogID de este filtro
	
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $AttribPositions;
	public $AttribMemberIDs;
	public $AttribMemberVarNames;
	public $NumAttribFields;
	//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	
	function __construct($aRepository, $aSurveyID)
	{
		BITAMObject::__construct($aRepository);
		$this->FilterID = -1;
		$this->SurveyID = $aSurveyID;
		$this->FilterName = "";
		$this->FilterText = "";
		$this->FilterTextUsr = "";
		$this->FilterLevels = "";
		$this->CatalogID = 0;
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
		$this->DataSourceID = 0;
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->AttribIDs = array();
		$this->AttribPositions = array();
		$this->AttribMemberIDs = array();
		$this->AttribMemberVarNames = array();
		$this->NumAttribFields = 0;
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
	}
	
	static function NewInstance($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSurveyID);
	}
	
	static function NewInstanceWithID($aRepository, $aFilterID)
	{
		$anInstance = null;
		
		if (((int) $aFilterID) <= 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvPartialIDsReplaceFix) {
			$strAdditionalFields .= ', A.CreationUserID, A.CreationDateID, A.CreationVersion, A.LastUserID, A.LastDateID, A.LastVersion ';
		}
		//@JAPR
		$sql = "SELECT A.SurveyID, A.FilterID, A.FilterName, A.FilterText, A.FilterLevels, A.CatalogID, B.DataSourceID $strAdditionalFields 
			FROM SI_SV_SurveyFilter A 
				LEFT OUTER JOIN SI_SV_Catalog B ON A.CatalogID = B.CatalogID 
			WHERE A.FilterID = ".$aFilterID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aSurveyID = (int) @$aRS->fields["surveyid"];
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
		//@JAPR
		$anInstance->FilterID = (int) @$aRS->fields["filterid"];
		$anInstance->CatalogID = (int) @$aRS->fields["catalogid"];
		$anInstance->FilterName = (string) @$aRS->fields["filtername"];
		$anInstance->FilterText = (string) @$aRS->fields["filtertext"];
		$anInstance->FilterLevels = (string) @$aRS->fields["filterlevels"];
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
		$anInstance->DataSourceID = (int) @$aRS->fields["datasourceid"];
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		//@JAPR
		
		$anInstance->addAttributeFields();
		return $anInstance;
	}
	
	function addAttributeFields($bNullFilters = false) {
		if ($this->CatalogID <= 0) {
			return;
		}
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
		$this->FilterTextUsr = $this->FilterText;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++)
		{
			$objCatMember = $attributes->Collection[$i];
			$fieldName = 'DSC_'.$objCatMember->ClaDescrip;
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = 'DSC_'.$objCatMember->ClaDescrip;
			$this->AttribIDs[$i] = $objCatMember->ClaDescrip;
			$this->AttribNames[$i] = $objCatMember->MemberName;
			$this->AttribMemberIDs[$fieldName] = $objCatMember->MemberID;
			$this->AttribPositions[$fieldName] = $i;
			//@JAPR 2014-06-06: Agregado el refresh de catálogos dinámico
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
			$this->AttribMemberVarNames[$objCatMember->MemberID] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = '@CMID'.$objCatMember->MemberID;
			//$this->FilterTextUsr = str_ireplace('@CMID'.$objCatMember->MemberID, $strCatMemberVarName, $this->FilterTextUsr);
			//@JAPR
			
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				$this->$fieldName = "sv_ALL_sv";
				$this->$fieldNameSTR = "All";
			}
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$strCatMemberVarName = (string) @$arrUsedVarNamesByPos[$intMemberOrder];
			$this->FilterTextUsr = str_ireplace($strMemberVarName, $strCatMemberVarName, $this->FilterTextUsr);
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->FilterTextUsr = $this->TranslateVariableQuestionIDsByNumbers($this->FilterTextUsr);
	    //@JAPR
		
		$this->NumAttribFields = $numFields;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;
		}
		
		if (array_key_exists("FilterID", $aHTTPRequest->POST))
		{
			$aFilterID = $aHTTPRequest->POST["FilterID"];
			
			if (is_array($aFilterID))
			{
				$aCollection = BITAMSurveyFilterCollection::NewInstance($aRepository, $aSurveyID, $aFilterID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMSurveyFilter::NewInstance($aRepository, $aSurveyID);
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aFilterID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("FilterID", $aHTTPRequest->GET))
		{
			$aFilterID = $aHTTPRequest->GET["FilterID"];
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aFilterID);
			
			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
			}
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("FilterID", $anArray))
		{
			$this->FilterID = (int)$anArray["FilterID"];
		}
		
		if (array_key_exists("FilterName", $anArray))
		{
			$this->FilterName = rtrim($anArray["FilterName"]);
		}
		
		if (array_key_exists("FilterTextUsr", $anArray))
		{
			$this->FilterTextUsr = rtrim($anArray["FilterTextUsr"]);
		}

		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = rtrim($anArray["CatalogID"]);
		}
		
		return $this;
	}
	
	//Toma el valor del FilterTextUsr y cambia las variables de usuario por las variables internas con las que se debe grabar
	function encodeFilterText($aFilterTextUsr) {
		if ($this->CatalogID <= 0) {
			return '';
		}
		$strFilterText = $this->FilterTextUsr;
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++) {
			$objCatMember = $attributes->Collection[$i];
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = '@CMID'.$objCatMember->MemberID;
			//$strFilterText = str_ireplace($strCatMemberVarName, '@CMID'.$objCatMember->MemberID, $strFilterText);
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $intMemberOrder => $strCatMemberVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$intMemberOrder];
			$strFilterText = str_ireplace($strCatMemberVarName, $strMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, (($this->LastModAdminVersion > 0)?$this->LastModAdminVersion:esvExtendedModifInfo), otySurveyFilter, $this->FilterID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, (($this->LastModAdminVersion > 0)?$this->LastModAdminVersion:esvExtendedModifInfo), $bServerVariables);
	}
	//@JAPR
	
	function save()
	{
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$currentDate = date("Y-m-d H:i:s");
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		$intCreationUserID = $_SESSION["PABITAM_UserID"];
		$intLastUserID = $_SESSION["PABITAM_UserID"];
		$strCreationDate = $currentDate;
		$strLastDate = $currentDate;
		$dblCreationVersion = $this->LastModAdminVersion;
		$dblLastVersion = $this->LastModAdminVersion;
		//@JAPR
		
	 	if ($this->isNewObject())
		{
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterID)", "0")." + 1 AS FilterID".
						" FROM SI_SV_SurveyFilter";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->FilterID = (int)$aRS->fields["filterid"];
			//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvPartialIDsReplaceFix) {
				$strAdditionalFields .= ', CreationUserID, LastUserID, CreationDateID, LastDateID, CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$intCreationUserID.', '.$intLastUserID.
					', '.$this->Repository->DataADOConnection->DBTimeStamp($strCreationDate).
					', '.$this->Repository->DataADOConnection->DBTimeStamp($strLastDate).
					', '.$dblCreationVersion.', '.$dblLastVersion;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_SurveyFilter (".
						"SurveyID".
						",FilterID".
						",FilterName".
						",FilterText".
						",FilterLevels".
			            ",CatalogID".
			            ",CatDimID".
						$strAdditionalFields.
			            ") VALUES (".
						$this->SurveyID.
						",".$this->FilterID.
						",".$this->Repository->DataADOConnection->Quote($this->FilterName).
						",".$this->Repository->DataADOConnection->Quote($this->FilterText).
						",".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
						",".$this->CatalogID.
						",".'0'.
						$strAdditionalValues.
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
			//Por ahora en esta versión v6 no se soportarían los filtros dinámicos directos, así que cuando tiene que grabar este filtro es porque ya está ajustado como debe grabarse
			//$this->FilterText = $this->encodeFilterText($this->FilterTextUsr);
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs (por ahora ya deberá venir siempre traducido)
			//$this->FilterText = $this->TranslateVariableQuestionNumbersByIDs($this->FilterText, optyFilterText);
			//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
			$strAdditionalValues = '';
			if (getMDVersion() >= esvPartialIDsReplaceFix) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			$sql = "UPDATE SI_SV_SurveyFilter SET ".
					" FilterName = ".$this->Repository->DataADOConnection->Quote($this->FilterName).
					", FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					", FilterLevels = ".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
					$strAdditionalValues.
				" WHERE FilterID = ".$this->FilterID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
        //@JAPR 2014-10-03: Corregido un bug, no estaba actualizando la versión de la definición al grabar
    	BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
        //@JAPR
		return $this;
	}
	
	function remove()
	{		
		$sql = "DELETE FROM SI_SV_SurveyFilter WHERE FilterID = ".$this->FilterID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->FilterID <= 0);
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Filter");
		}
		else
		{
			return $this->FilterName;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyFilter&SurveyID=".$this->SurveyID;
		}
		else
		{
			return "BITAM_PAGE=SurveyFilter&SurveyID=".$this->SurveyID."&FilterID=".$this->FilterID;
		}
	}
	
	function get_Parent()
	{
		return BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return "FilterID";
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//Cuando se está creando el filtro se permite definir el catálogo que va a usar, o bien si el catálogo es vacio
		if ($this->isNewObject() || $this->CatalogID == 0) {
			$arrSurveyCatalogs = array(0 => "(".translate("None").")");
			//@JAPR 2015-04-16: Modificado para no permitir duplicar filtros del mismo catálogo en la misma forma
		    $sql = "SELECT DISTINCT A.CatalogID, B.CatalogName 
		        	FROM SI_SV_Question A, SI_SV_Catalog B 
		        	WHERE A.CatalogID = B.CatalogID AND A.SurveyID = {$this->SurveyID} AND A.CatalogID > 0 
						AND A.CatalogID NOT IN ( 
							SELECT CatalogID 
							FROM SI_SV_SurveyFilter 
							WHERE SurveyID = {$this->SurveyID}) 
		    	UNION 
		    		SELECT DISTINCT A.CatalogID, B.CatalogName 
		    		FROM SI_SV_Section A, SI_SV_Catalog B 
		    		WHERE A.CatalogID = B.CatalogID AND A.SurveyID = {$this->SurveyID} AND A.CatalogID > 0 
						AND A.CatalogID NOT IN ( 
							SELECT CatalogID 
							FROM SI_SV_SurveyFilter 
							WHERE SurveyID = {$this->SurveyID})";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				while(!$aRS->EOF) {
					$intCatalogID = (int) @$aRS->fields["catalogid"];
					$strCatalogName = (string) $aRS->fields["catalogname"];
					if ($intCatalogID > 0) {
						$arrSurveyCatalogs[$intCatalogID] = $strCatalogName;
					}
					$aRS->MoveNext();
				}
			}
		}
		else {
			$arrSurveyCatalogs = array(0 => "(".translate("None").")");
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
			if (!is_null($objCatalog)) {
				$arrSurveyCatalogs = array($objCatalog->CatalogID => $objCatalog->CatalogName);
			}
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogID";
		$aField->Title = translate("Catalog");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrSurveyCatalogs;
		if ($this->CatalogID > 0) {
			$aField->IsDisplayOnly = true;
		}
		$myFields[$aField->Name] = $aField;
		
		//Campos para apoyo en la edición del FilterSQL
		if ($this->CatalogID > 0 && $this->NumAttribFields > 0) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "FilterTextUsr";
			$aField->Title = translate("Filter");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			$myFields[$aField->Name] = $aField;
			
			//Genera la combo de atributos, agrega dinámicamente la propiedad con un valor de 0 para que no seleccione a alguno en particular
			$this->CatMemberID = 0;
			$objCatMember = BITAMCatalogMember::NewInstance($this->Repository, $this->CatalogID);
			$arrCatMembers = $objCatMember->getRemainingCatalogAttributes(true);
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "CatMemberID";
			$aField->Title = translate("Attributes");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrCatMembers;
			$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
				"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
				"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
				"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
				"onclick=\"this.style.color='#d5d5bc';addAttrName();\">";
			$myFields[$aField->Name] = $aField;
			
			//Genera las combos con los valores de cada atributo
			$arrDefaultValue = array("sv_ALL_sv" => "All");
			for ($i=0; $i < $this->NumAttribFields; $i++)
			{
				$fieldName = $this->AttribFields[$i];
				$labelText = $this->AttribNames[$i];
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = $fieldName;
				$aField->Title = $labelText;
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrDefaultValue;
				$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
					"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
					"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
					"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
					"onclick=\"this.style.color='#d5d5bc';addAttrValue('{$fieldName}');\">";
				$myFields[$aField->Name] = $aField;
			}
		}
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
	{
		if ($this->CatalogID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMembersPos = new Array;
		var filterAttribMembersVars = new Array;
		var filterAttribMemberFields = new Array;
		var filterAttribValGot = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName];
			$attribVarName = $this->AttribMemberVarNames[$attribMemberID];
?>
		filterAttribIDs[<?=$position?>]=<?=$attribID?>;
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMembersPos['<?=$position?>']=<?=$attribMemberID?>;
		filterAttribMembersVars[<?=$attribMemberID?>]='<?=$attribVarName?>';
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
		filterAttribValGot[<?=$position?>]=0;
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			var changedMemberID = filterAttribMembersPos[thisPosition -1];
			if (changedMemberID === undefined) {
				changedMemberID = -1;
			}
			
			if (filterAttribValGot[thisPosition]) {
				return;
			}
			
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
		 	xmlObj.open("POST", "getAttribValues.php?noFilters=1&CatalogID=<?=$this->CatalogID?>&CatMemberID="+filterAttribMembers[attribName]+"&ChangedMemberID="+changedMemberID, false);
		 	xmlObj.send(null);
		 	strResult = Trim(unescape(xmlObj.responseText));
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	filterAttribValGot[thisPosition] = 1;
		}
		
		function addAttrValue(attribName) {
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			
			var objSelect = document.getElementsByName(attribName);
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			if (!filterAttribValGot[thisPosition]) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == 'sv_ALL_sv') {
				return;
			}
			strSelectedVal = "'" + strSelectedVal + "'";

			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strSelectedVal;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelEnd);
			}
		}
		
		function addAttrName() {
			var objSelect = document.getElementsByName("CatMemberID");
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == '0') {
				return;
			}
			var strAttribVar = filterAttribMembersVars[strSelectedVal];
			if (!strAttribVar) {
				return;
			}
			
			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strAttribVar;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelEnd);
			}
		}
<?
		}
?>
	</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
		if ($this->CatalogID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			//document.getElementsByName('<?=$attribName?>')[0].addEventListener("click", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("focus", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    //document.getElementsByName('<?=$attribName?>')[0].attachEvent("onclick", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onfocus", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Obtiene la definición en JSon de este objeto y sus dependencias
	function getJSonDefinition() {
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;

		$arrDef = array();
		$arrDef['surveyID'] = $this->SurveyID;
		$arrDef['catalogID'] = $this->CatalogID;
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strFilterText = $this->TranslateVariableQuestionIDsByNumbers($this->FilterText);
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
		//Por ahora en esta versión v6 se asumirán que todos los filtros son de DataSource
		$strFilterText = BITAMDataSource::ReplaceMemberNamesByIDs($this->Repository, $this->DataSourceID, $strFilterText);
		$arrDef['dataSourceID'] = $this->DataSourceID;
		//@JAPR
		$arrDef['filterText'] = $strFilterText;
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		if($gbDesignMode==true)
		{
			$arrDef['filterID'] = $this->FilterID;
			$arrDef['filterName'] = $this->FilterName;
		}
		return $arrDef;
	}
	//@JAPR
}

class BITAMSurveyFilterCollection extends BITAMCollection
{
	public $SurveyID;
	
	function __construct($aRepository, $aSurveyID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->SurveyID = $aSurveyID;
	}
	
	static function NewInstance($aRepository, $aSurveyID, $anArrayOfFilterIDs = null)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = "";
		if (!is_null($anArrayOfFilterIDs))
		{
			switch (count($anArrayOfFilterIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND FilterID = ".((int)$anArrayOfFilterIDs[0]);
					break;
				default:
					foreach ($anArrayOfFilterIDs as $aFilterID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aFilterID; 
					}
					
					if ($filter != "")
					{
						$filter = " AND FilterID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2014-09-30: Corregido un bug, no estaba cargando el SurveyID
		//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData, se requiere un Join con el DataSource
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvPartialIDsReplaceFix) {
			$strAdditionalFields .= ', A.CreationUserID, A.CreationDateID, A.CreationVersion, A.LastUserID, A.LastDateID, A.LastVersion ';
		}
		//@JAPR
		$sql = "SELECT A.SurveyID, A.FilterID, A.FilterName, A.FilterText, A.FilterLevels, A.CatalogID, B.DataSourceID $strAdditionalFields 
			FROM SI_SV_SurveyFilter A 
				LEFT OUTER JOIN SI_SV_Catalog B ON A.CatalogID = B.CatalogID 
			WHERE A.SurveyID = ".$aSurveyID.$filter." 
			ORDER BY A.FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;
		}
		
		return BITAMSurveyFilterCollection::NewInstance($aRepository, $aSurveyID);
	}
	
	function get_Parent()
	{
		return BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
	}
	
	function get_Title()
	{	
		return translate("Filters");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=Survey&SurveyID=".$this->SurveyID;
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyFilter&SurveyID=".$this->SurveyID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FilterID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$arrSurveyCatalogs = array(0 => "(".translate("None").")");
	    $sql = "SELECT DISTINCT A.CatalogID, B.CatalogName 
	        	FROM SI_SV_Question A, SI_SV_Catalog B 
	        	WHERE A.CatalogID = B.CatalogID AND A.SurveyID = {$this->SurveyID} AND A.CatalogID > 0 
	    	UNION 
	    		SELECT DISTINCT A.CatalogID, B.CatalogName 
	    		FROM SI_SV_Section A, SI_SV_Catalog B 
	    		WHERE A.CatalogID = B.CatalogID AND A.SurveyID = {$this->SurveyID} AND A.CatalogID > 0";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while(!$aRS->EOF) {
				$intCatalogID = (int) @$aRS->fields["catalogid"];
				$strCatalogName = (string) $aRS->fields["catalogname"];
				if ($intCatalogID > 0) {
					$arrSurveyCatalogs[$intCatalogID] = $strCatalogName;
				}
				$aRS->MoveNext();
			}
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogID";
		$aField->Title = translate("Catalog");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrSurveyCatalogs;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>