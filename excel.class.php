<?php

class Excel
{
	public $path = '';
	public $conn = null;
	
	public $sheets = null;
	public $columns = null;
	
	function __construct($Path)
	{
		$this->path = $Path;
	}
	
	function open()
	{
		//$ext = substr($this->path,-4);
		//@JAPR 2017-01-17: (ARamirez) Corregido un bug, se debe hacer conversión a minúsculas para que pase la validación correctamente
		$ext = strtolower(pathinfo($this->path, PATHINFO_EXTENSION));
		//@JAPR
		//var_dump($this->path);
		
		if ($ext == "xls")
			$connStr = 'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='. $this->path .';Extended Properties="Excel 8.0;HDR=YES;IMEX=1;";';
		elseif ($ext == "xlsx")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0;HDR=YES;IMEX=1;";';
		elseif ($ext = "xlsb")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0;HDR=YES;IMEX=1;";';
		elseif ($ext == "xlsm")
			$connStr = 'Provider=Microsoft.ACE.OLEDB.12.0;Data Source='. $this->path .';Extended Properties="Excel 12.0 Macro;HDR=YES;IMEX=1;";';
			
		//echo $connStr;
		
		try
		{
			$conn = new COM("ADODB.Connection") or die("Cannot start ADO");
			$conn->Open($connStr);		
		}
		catch(Exception  $e)
		{
		}
		
		$this->conn = $conn;
		
		return $conn;
	}
	
	function close()
	{
		$this->conn->Close();
		$this->conn = null;	
	}
	
	function getSheets(&$error)
	{
		$rsSheets = null;
		
		if ($this->sheets == null)
		{			
			try
			{
				$rsSheets = $this->conn->OpenSchema(20);
				$this->sheets = $rsSheets;
			}
			catch(Exception  $e)
			{
				$this->sheets = null;
				$error = $e;
				return false;
			}
		}
		else 
		{
			$rsSheets = $this->sheets;
		}
		
		$tables = array();
		while (!$rsSheets->EOF)
		{
			$tables[] = str_replace("'", "", $rsSheets->Fields('TABLE_NAME')->value);
			$rsSheets->MoveNext();
		}
		
		return $tables;
	}

	function getColumns($sheet, &$error)
	{
		$rsColumns = null;
		$columns = array();
		
		$ext = pathinfo($this->path, PATHINFO_EXTENSION);
		
		//var_dump($this->path);
		
		if ($this->columns == null)
		{
			if ($ext == "xlsx")
			{
				$columnsFn = array();
				for($i=1; $i<=255; $i++)
				{
					$columnsFn[] = 'F'.$i;
				}
				
				
				$aQuery = "SELECT  * FROM [%sA:IU]";
				$rs = $this->execute($aQuery, $sheet, $error);
				if (trim($error))
				{
					echo $error;
          			return false;
					//exit();
				}
				$num_columns = $rs->Fields->Count();
				for ($i=0; $i<$num_columns; $i++)
				{
					if (!in_array($rs->Fields($i)->name, $columnsFn))
						$columns[] = $rs->Fields($i)->name;
				}
				
				$this->columns = $columns;
			}
			else 
			{
				try
				{
          $rsColumns = $this->conn->OpenSchema(4);
				}
				catch(Exception  $e)
				{
					//GCRUZ 2013-04-03. Agregada validación para intentar obtener las columnas por medio de un Query de no funcionar el OpenSchema
					$columnsFn = array();
					for($i=1; $i<=255; $i++)
					{
						$columnsFn[] = 'F'.$i;
					}
					
					
					$aQuery = "SELECT  * FROM [%sA:IU]";
					$rs = $this->execute($aQuery, $sheet, $error);
					if (trim($error))
					{
						echo $error;
						exit();
					}
					$num_columns = $rs->Fields->Count();
					for ($i=0; $i<$num_columns; $i++)
					{
						if (!in_array($rs->Fields($i)->name, $columnsFn))
							$columns[] = $rs->Fields($i)->name;
					}
					
					$this->columns = $columns;
					return $this->columns;
					
					//GCRUZ 2013-04-03. Comentado siguiente código para ignorar el error del OpenSchema
//					$this->columns = null;
//					$error = $e;
//					return false;
				}
	
				
				while (!$rsColumns->EOF)
				{
					$table_name = str_replace("'", "", $rsColumns->Fields('TABLE_NAME')->value);
					
					if ($table_name == $sheet)
					{
						$columns[] = $rsColumns->Fields('COLUMN_NAME')->value;
					}
					$rsColumns->MoveNext();
				}
	
				$this->columns = $columns;
			}
		}
		
		return $this->columns;
		
		
		/*	
		if ($this->columns == null)
		{			
			try
			{
				$rsColumns = $this->conn->OpenSchema(4);
			}
			catch(Exception  $e)
			{
				$this->columns = null;
				$error = $e;
				return false;
			}

			
			while (!$rsColumns->EOF)
			{
				$table_name = str_replace("'", "", $rsColumns->Fields('TABLE_NAME')->value);
				
				if ($table_name == $sheet)
				{
					$columns[] = $rsColumns->Fields('COLUMN_NAME')->value;
				}
				$rsColumns->MoveNext();
			}

			$this->columns = $columns;
			
		}
		else 
		{
			$columns = $this->columns;
		}
		
		return $columns;*/
	}
	
	// busca en todas las hojas de excel el query valido. Regresa el RS del primero que encuentre.
	
	function execute($aSQL, $sheet, &$error)
	{
		$rs = null;
		
		$table = $sheet;
		//echo '<br>'.'$table= "' . $table.'"'.'<br>';
		//$aSQL_tmp = sprintf($aSQL, $table);
		$aSQL_tmp = str_replace("[%s", "[".$table."", $aSQL);
				
		try{
			$rs = $this->conn->Execute($aSQL_tmp);    // Recordset
			$found = true;
		}
		catch (Exception $e)
		{
			$found = false;
			$error = $e;
			$error .= '<br><br><strong>Query:</strong> ' . $aSQL_tmp;
		}
		
		return $rs;
	}
	
	function execute_s($aSQL, &$error)
	{
		$rsSheets = null;
				
		if ($this->sheets == null)
		{			
			try
			{
				$rsSheets = $this->conn->OpenSchema(20);
				$this->sheets = $rsSheets;
			}
			catch(Exception  $e)
			{
				$this->sheets = null;
				$error = $e;
				return false;
			}
		}
		else 
		{
			$rsSheets = $this->sheets;
		}
		
				
		$found = false;
		
		while (!$rsSheets->EOF)
		{
			$field = $rsSheets->Fields('TABLE_NAME')->value;
			//echo '$field= "' . $field.'"';
			if (!$found)
			{
				if (substr($field, -1) == '$' || substr($field, -2) == "$'")
				{
					$table = $field;
					//echo '<br>'.'$table= "' . $table.'"'.'<br>';
					//$aSQL_tmp = sprintf($aSQL, $table);
					$aSQL_tmp = str_replace("[%s", "[".$table."", $aSQL);
					
					try{
						$rs = $this->conn->Execute($aSQL_tmp);    // Recordset
						$found = true;
					}
					catch (Exception $e)
					{
						$found = false;
						$error = $e;
						$error .= '<br><br><strong>Query:</strong> ' . $aSQL_tmp;
					}
				}
			}
			$rsSheets->MoveNext();
		}
		
		if ($found)
		{
			return $rs;
		}
		else
		{ 
			return false;
		}
		
	}
}


?>