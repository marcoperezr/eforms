<?php

function BITAMEncode($inString)
{

	if (is_null($inString) || strlen($inString) == 0)
	{
		return $inString;
	}

	$encryptTable = 'S7H25sy8XFqjJIwxZNp1CvdEe_9u/PiW63TtOkGQMlgRbVhnmzf4oaBLDUYA0cKr';
	$inPos = 0;
	$outPos = 0;
	$inLen = strlen($inString);
	$outLen = floor(($inLen + 2) / 3) * 4;
	$outArray = array();
	array_pad($outArray, $outLen, '');

	for ($i = 0; $i < floor($inLen / 3); $i++)
	{
		$c1 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$c2 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$c3 = ord($inString[$inPos]) & 0xFF;
		$inPos++;
		$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
		$outPos++;
		$outArray[$outPos] = $encryptTable[(($c1 & 0x03) << 4) | (($c2 & 0xF0) >> 4)];
		$outPos++;
		$outArray[$outPos] = $encryptTable[(($c2 & 0x0F) << 2) | (($c3 & 0xC0) >> 6)];
		$outPos++;
		$outArray[$outPos] = $encryptTable[$c3 & 0x3F];
		$outPos++;
	}
	switch($inLen % 3)
	{	case 1:
			$c1 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
			$outPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0x03) << 4];
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			break;
		case 2:
			$c1 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$c2 = ord($inString[$inPos]) & 0xFF;
			$inPos++;
			$outArray[$outPos] = $encryptTable[($c1 & 0xFC) >> 2];
			$outPos++;
			$outArray[$outPos] = $encryptTable[(($c1 & 0x03) << 4) | (($c2 & 0xF0) >> 4)];
			$outPos++;
			$outArray[$outPos] = $encryptTable[($c2 & 0x0F) << 2];
			$outPos++;
			$outArray[$outPos] = '|';
			$outPos++;
			break;
	}
	$outString = '';
	for ($i = 0; $i < count($outArray); $i++)
	{
		$outString .= $outArray[$i];
	}
	return $outString;
}

function BITAMDecode($inString)
{
	$decryptTable = "\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\034".
					"\074\023\003\041\063\004\040\001\007\032\000\000\000\000\000\000".
					"\000\073\066\024\070\027\011\046\002\015\014\076\067\050\021\044".
					"\035\047\053\000\042\071\055\037\010\072\020\000\000\000\000\031".
					"\000\065\054\075\026\030\062\052\056\036\013\045\051\060\057\064".
					"\022\012\077\005\043\033\025\016\017\006\061\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000".
					"\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000";				
	$inPos  = 0;
	$outPos = 0;
	$inLen = strlen($inString);
	$outLen = floor($inLen / 4) * 3;
	$outArray = array();
	array_pad($outArray, $outLen, 'X');

	for ($i = 0; $i < floor($inLen / 4); $i++) 
	{
		$c1 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c2 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c3 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;
		$c4 = ord($decryptTable[ord($inString[$inPos])]);
		$inPos++;

		$outArray[$outPos] = chr((($c1 << 2) & 0xFC) | (($c2 >> 4) & 0x03));
		$outPos++;
		$outArray[$outPos] = chr((($c2 << 4) & 0xF0) | (($c3 >> 2) & 0x0F));
		$outPos++;
		$outArray[$outPos] = chr((($c3 << 6) & 0xC0) | ($c4 & 0x3F)); 
		$outPos++;
	}

	$outString = '';
	for ($i = 0; $i < count($outArray); $i++)
	{
		if (ord($outArray[$i]) !== 000)
		{
			$outString .= $outArray[$i];
		}
	}

	return $outString;
}

if (!function_exists('BITAMHasInvalidChar'))
{
	function BITAMHasInvalidChar($aString)
	{
		$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$n = strlen($aString);
		for ($i = 0; $i < $n; $i++)
		{
			if (strpos($STRVALID, $aString[$i]) === false)
			{
				return $i;
			}
		}
		return false;
	}
}

if (!function_exists('LiberaInValidChars'))
{
	function LiberaInValidChars($aString, &$aPassPos, &$aPassChar)
	{
	    while (BITAMHasInvalidChar($aString) !==false)
	    {
	        $iPos  = BITAMHasInvalidChar($aString);
	        $sChar = substr($aString, $iPos, 1);

	        $aPassPos[]   = $iPos;
	        $aPassChar[]  = $sChar;

	        $sS1     = BITAMGetSiguiente($aString, $sChar);
	        $aString = $sS1.$aString;
	    }
	    return($aString);
	}
}

if (!function_exists('ReturnInValidChars'))
{
	function ReturnInValidChars($aString, $aPassPos, $aPassChar)
	{
	    $j = count($aPassPos);
	    for($i=0; $i<$j; $i++)
	    {
	        $iPos  = $aPassPos[$i] + $i;
	        $sChar = $aPassChar[$i];

	        $sS1  = substr($aString, 0, $iPos);
	        $sNew = $sS1.$sChar.substr($aString, $iPos);
	        $aString = $sNew;
	    }
	    return($aString);
	}
}

if (!function_exists('BITAMEncryptPassword'))
{
	function BITAMEncryptPassword($inString)
	{
		$inString = utf8_decode($inString);
		$STRVALID     = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$aPassPos     = array();
		$aPassChar    = array();
		$inString0rig = $inString;
		$inString     = trim($inString);
		$inString = str_replace(' ','_#ArtusSpace#_', $inString );
	    if ($inString == "")
	    {
	        return "";
	    }
		elseif (BITAMHasInvalidChar($inString)!==false)
		{   //EOAC 16Abr09 Soportar caracteres invalidos
			$inString=LiberaInValidChars($inString, $aPassPos, $aPassChar);
			if($inString=='') return($inString0rig); //Passwords con puros caracteres invalidos se regresaran =
			//return "";
		}

	    $movPos = rand(1, strlen($STRVALID));

		$outString = "";
		$n = strlen($inString);
		for ($i = 0; $i < $n; $i++)
		{
	    	$currentChar = $inString[$i];
	    	$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
	        If ($newPos > strlen($STRVALID))
	        {
	            $newPos = $newPos - strlen($STRVALID);
	        }
	        $outString .= $STRVALID[$newPos - 1];
		}
	    if(strlen($inString)>0) $outString .= $inString[0];
		if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
		{
			$outString=ReturnInValidChars($outString, $aPassPos, $aPassChar);
		}
		if(strlen($inString0rig)>2 && strpos(strtoupper($outString),  strtoupper($inString0rig))!==false)
		{
			$outString=BITAMEncryptPassword($inString0rig);
		}
	    return ($outString);
	}
}

if (!function_exists('BITAMDecryptPassword'))
{
	function BITAMDecryptPassword($inString)
	{
		$inString = utf8_decode($inString);
		$STRVALID = utf8_decode("!¡#$%&'()*+,-./0123456789:;<=>?¿@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]_abcdefghijklmnopqrstuvwxyz");
		$aPassPos = array();
		$aPassChar= array();
		$inString = trim($inString);
	    if ($inString == "")
	    {
	        return "";
	    }
		elseif (BITAMHasInvalidChar($inString)!==false)
		{   //EOAC 16Abr09 Soportar caracteres invalidos
			$inString2=LiberaInValidChars($inString, $aPassPos, $aPassChar);
			if($inString2=='') return $inString;
			$inString=$inString2;
		}

		$firstChar = $inString[0];
		$lastChar = $inString[strlen($inString) - 1];
	    $firstPos = strpos($STRVALID, $firstChar) + 1;
	    $lastPos = strpos($STRVALID, $lastChar) + 1;
	    $movPos = $lastPos - $firstPos;

		$outString = "";
		$n = strlen($inString) - 1;
	    for ($i = 0; $i < $n; $i++)
	    {
	    	$currentChar = $inString[$i];
	    	$newPos = strpos($STRVALID, $currentChar) + 1 + $movPos;
	        if ($newPos <= 0)
	        {
	            $newPos = strlen($STRVALID) - abs($newPos);
			}
	        If ($newPos > strlen($STRVALID))
	        {
	            $newPos = $newPos - strlen($STRVALID);
	        }

			$outString .= $STRVALID[$newPos - 1];
		}
		if(count($aPassChar)>0) //EOAC 16Abr09 Soportar caracteres invalidos
		{
			$outString=ReturnInValidChars($outString, $aPassPos, $aPassChar);
		}
		$outString = str_replace('_#ArtusSpace#_',' ', $outString );
		return ($outString);
	}
}


if (!function_exists('write_ini_file'))
{	
	function write_ini_file($path, $assoc_array) 
	{
		$content = '';
		$sections = '';
		
		foreach ($assoc_array as $key => $item) 
		{
			if (is_array($item)) 
			{
				$sections .= ($sections == '' ? "" : "\r\n")."[{$key}]\r\n";
				foreach ($item as $key2 => $item2) 
				{
					if (is_array($item2))
					{
						$sections .= "{$key2} = {";
						$first = true;
						foreach ($item2 as $item3)
						{
							if ($first)
							{
								$first = false;
							}
							else
							{
								$sections .= ',';
							}
								
							//if (is_numeric($item3) || is_bool($item3))
							//{
								$sections .= "{$item3}";
							//}
							//else
							//{
							//	$sections .= "\"{$item3}\"";
							//}
						}
						$sections .= "}\r\n";
					}
					else
					{
						if (is_numeric($item2) || is_bool($item2))
						{
							$sections .= "{$key2} = {$item2}\r\n";
						}
						else
						{
							$sections .= "{$key2} = \"{$item2}\"\r\n";
						}
					}
				}      
			} 
			else 
			{
				if (is_numeric($item) || is_bool($item))
				{
					$content .= "{$key} = {$item}\r\n";
				}
				else
				{
					$content .= "{$key} = \"{$item}\"\r\n";
				}
			}
		}
		
		$content .= $sections;
					
		if (!$handle = fopen($path, 'w')) 
		{
			return false;
		}
		
		if (!fwrite($handle, $content)) 
		{
			return false;
		}
		
		fclose($handle);
		
		return true;
	} 
}

function read_ini_file($path)
{
	$assoc_array = array();
	$lines = @file($path);
	$numlines = count($lines);
	for ($x = 0; $x < $numlines; )
	{
		if (preg_match('/^\[(.*?)\]/', trim($lines[$x]), $match))
		{
			$section = trim($match[1]);
			$x++;
			while ($x < $numlines && !preg_match('/^\[(.*?)\]/', trim($lines[$x])))
			{
				$thisline = trim($lines[$x]);
				
				if (strlen($thisline) > 0)
				{
					//@JAPR 2015-02-06: Agregado soporte para php 5.6
					list($key, $value) = explode('=', $thisline, 2);
					$key = trim($key);
					$value = trim($value);
					if (strlen($value) > 0 && ($value[0] == '"' && $value[strlen($value) - 1] == '"'))
					{
						$value = substr($value, 1, strlen($value) - 2);
					}
					if (strlen($value) > 0 && ($value[0] == '{' && $value[strlen($value) - 1] == '}'))
					{
						$value = explode(',', substr($value, 1, strlen($value) - 2));
					}
					$assoc_array[$section][$key] = $value;
				}
				$x++;
			}
		}
		else
		{
			$x++;
		}
	}
	return $assoc_array;
}

function asHTMLColor($anInteger)
{
	$aString = sprintf("%06x", $anInteger);
	return substr($aString, 4, 2).substr($aString, 2, 2).substr($aString, 0, 2);
}

global $LOCALE_ARRAY;
$LOCALE_ARRAY = Array();

$LOCALE_ARRAY[0] = Array('en_US', 'english');
$LOCALE_ARRAY[1] =  Array('es_MX', 'Spanish_Mexico', 'spanish');
$LOCALE_ARRAY[2] =  Array('en_US', 'english');
$LOCALE_ARRAY[3] =  Array('it_IT', 'italian');
$LOCALE_ARRAY[4] =  Array('pt_PT', 'Portuguese_Portugal', 'portuguese');
$LOCALE_ARRAY[5] =  Array('ca_ES', 'catalan');
$LOCALE_ARRAY[6] =  Array('pt_BR', 'Portuguese_Brazil', 'portuguese');
$LOCALE_ARRAY[7] =  Array('es_ES', 'spanish');
$LOCALE_ARRAY[1024] = Array('en_US', 'english');
$LOCALE_ARRAY[1078] = Array('af_ZA', 'afrikaans');
$LOCALE_ARRAY[1052] = Array('sq_AL', 'albanian');
$LOCALE_ARRAY[1025] = Array('ar_SA', 'Arabic_Saudi Arabia', 'arabic');
$LOCALE_ARRAY[2049] = Array('ar_IQ', 'Arabic_Iraq', 'arabic');
$LOCALE_ARRAY[3073] = Array('ar_EG', 'Arabic_Egypt', 'arabic');
$LOCALE_ARRAY[4097] = Array('ar_LY', 'Arabic_Libya', 'arabic');
$LOCALE_ARRAY[5121] =  Array('ar_DZ', 'Arabic_Algeria', 'arabic');
$LOCALE_ARRAY[6145] =  Array('ar_MA', 'Arabic_Morocco', 'arabic');
$LOCALE_ARRAY[7169] =  Array('ar_TN', 'Arabic_Tunisia', 'arabic');
$LOCALE_ARRAY[8193] =  Array('ar_OM', 'Arabic_Oman', 'arabic');
$LOCALE_ARRAY[9217] =  Array('ar_YE', 'Arabic_Yemen', 'arabic');
$LOCALE_ARRAY[10241] =  Array('ar_SY', 'Arabic_Syria', 'arabic');
$LOCALE_ARRAY[11265] =  Array('ar_JO', 'Arabic_Jordan', 'arabic');
$LOCALE_ARRAY[12289] =  Array('ar_LB', 'Arabic_Lebanon', 'arabic');
$LOCALE_ARRAY[13313] =  Array('ar_KW', 'Arabic_Kuwait', 'arabic');
$LOCALE_ARRAY[14337] =  Array('ar_AE', 'Arabic_U.A.E.', 'arabic');
$LOCALE_ARRAY[15361] =  Array('ar_BH', 'Arabic_Bahrain', 'arabic');
$LOCALE_ARRAY[16385] =  Array('ar_QA', 'Arabic_Qatar', 'arabic');
$LOCALE_ARRAY[1067] =  Array('hy_AM', 'armenian');
$LOCALE_ARRAY[1101] =  Array('as_IN', 'assamese');
$LOCALE_ARRAY[1068] =  Array('az_AZ@latin', 'azeri@latin', 'az_AZ', 'azeri');
$LOCALE_ARRAY[2092] =  Array('az_AZ@cyrillic', 'azeri@cyrillic', 'az_AZ', 'azeri');
$LOCALE_ARRAY[1069] =  Array('eu_ES', 'basque');
$LOCALE_ARRAY[1059] =  Array('be_BY', 'byelorussian');
$LOCALE_ARRAY[1093] =  Array('bn_BD', 'bengali');
$LOCALE_ARRAY[1026] =  Array('bg_BG', 'bulgarian');
$LOCALE_ARRAY[1109] =  Array('my_BD', 'burmese');
$LOCALE_ARRAY[1027] =  Array('ca_ES', 'catalan');
$LOCALE_ARRAY[1028] =  Array('zh_TW', 'Chinese_Taiwan', 'chinese');
$LOCALE_ARRAY[2052] =  Array('zh_CN', 'Chinese_PRC', 'chinese');
$LOCALE_ARRAY[3076] =  Array('zh_HK', 'Chinese_Hong Kong', 'chinese');
$LOCALE_ARRAY[4100] =  Array('zh_SG', 'Chinese_Singapore', 'chinese');
$LOCALE_ARRAY[5124] =  Array('zh_MO', 'Chinese_Macau', 'chinese');
$LOCALE_ARRAY[1050] =  Array('hr_HR', 'croatian');
$LOCALE_ARRAY[1029] =  Array('cs_CZ', 'czech');
$LOCALE_ARRAY[1030] =  Array('da_DK', 'danish');
$LOCALE_ARRAY[1043] =  Array('nl_NL', 'Dutch_Netherlands', 'dutch');
$LOCALE_ARRAY[2067] =  Array('nl_BE', 'Dutch_Belgium', 'dutch');
$LOCALE_ARRAY[1033] =  Array('en_US', 'english');
$LOCALE_ARRAY[2057] =  Array('en_GB', 'en_UK', 'English_United Kingdom', 'english');
$LOCALE_ARRAY[3081] =  Array('en_AU', 'English_Australia', 'english');
$LOCALE_ARRAY[4105] =  Array('en_CA', 'English_Canada', 'english');
$LOCALE_ARRAY[5129] =  Array('en_NZ', 'English_New Zealand', 'english');
$LOCALE_ARRAY[6153] =  Array('en_IE', 'English_Ireland', 'english');
$LOCALE_ARRAY[7177] =  Array('en_ZA', 'English_South Africa', 'english');
$LOCALE_ARRAY[8201] =  Array('en_JM', 'English_Jamaica', 'english');
$LOCALE_ARRAY[9225] =  Array('English_Caribbean', 'en_US', 'english');
$LOCALE_ARRAY[10249] =  Array('en_BZ', 'English_Belize', 'english');
$LOCALE_ARRAY[11273] =  Array('en_TT', 'English_Trinidad And Tobago', 'english');
$LOCALE_ARRAY[12297] =  Array('en_ZW', 'English_Zimbabwe', 'english');
$LOCALE_ARRAY[13321] =  Array('en_PH', 'English_Philippines', 'english');
$LOCALE_ARRAY[1061] =  Array('et_EE', 'estonian');
$LOCALE_ARRAY[1080] =  Array('fo_FO', 'faeroese');
$LOCALE_ARRAY[1065] =  Array('fa_IR', 'farsi');
$LOCALE_ARRAY[1035] =  Array('fi_FI', 'finnish');
$LOCALE_ARRAY[1036] =  Array('fr_FR', 'french');
$LOCALE_ARRAY[2060] =  Array('fr_BE', 'French_Belgium', 'french');
$LOCALE_ARRAY[3084] =  Array('fr_CA', 'French_Canada', 'french');
$LOCALE_ARRAY[4108] =  Array('fr_CH', 'French_Switzerland', 'french');
$LOCALE_ARRAY[5132] =  Array('fr_LU', 'French_Luxembourg', 'french');
$LOCALE_ARRAY[6156] =  Array('fr_MC', 'French_Monaco', 'french');
$LOCALE_ARRAY[1079] =  Array('ka_GE', 'georgian');
$LOCALE_ARRAY[1031] =  Array('de_DE', 'german');
$LOCALE_ARRAY[2055] =  Array('de_CH', 'German_Switzerland', 'german');
$LOCALE_ARRAY[3079] =  Array('de_AT', 'German_Austria', 'german');
$LOCALE_ARRAY[4103] =  Array('de_LU', 'German_Luxembourg', 'german');
$LOCALE_ARRAY[5127] =  Array('de_LI', 'German_Liechtenstein', 'german');
$LOCALE_ARRAY[1032] =  Array('el_GR', 'greek');
$LOCALE_ARRAY[1095] =  Array('gu_IN', 'gujarati');
$LOCALE_ARRAY[1037] =  Array('he_IL', 'hebrew');
$LOCALE_ARRAY[1081] =  Array('hi_IN', 'hindi');
$LOCALE_ARRAY[1038] =  Array('hu_HU', 'hungarian');
$LOCALE_ARRAY[1039] =  Array('is_IS', 'icelandic');
$LOCALE_ARRAY[1057] =  Array('id_ID', 'indonesian');
$LOCALE_ARRAY[1040] =  Array('it_IT', 'italian');
$LOCALE_ARRAY[2064] =  Array('it_CH', 'Italian_Switzerland', 'italian');
$LOCALE_ARRAY[1041] =  Array('ja_JP', 'japanese');
$LOCALE_ARRAY[1099] =  Array('kn_IN', 'kannada');
$LOCALE_ARRAY[2144] =  Array('ks_IN', 'kashmiri');
$LOCALE_ARRAY[1087] =  Array('kk_KZ', 'kazakh');
$LOCALE_ARRAY[1111] =  Array('konkani');
$LOCALE_ARRAY[1042] =  Array('ko_KR', 'korean');
$LOCALE_ARRAY[2066] =  Array('korean (johab)', 'ko_KR', 'korean');
$LOCALE_ARRAY[1062] =  Array('lv_LV', 'latvian');
$LOCALE_ARRAY[1063] =  Array('lt_LT', 'lithuanian');
$LOCALE_ARRAY[2087] =  Array('lithuanian (classic)', 'lt_LT', 'lithuanian');
$LOCALE_ARRAY[1071] =  Array('mk_MK', 'macedonian');
$LOCALE_ARRAY[1086] =  Array('ms_MY', 'malay');
$LOCALE_ARRAY[2110] =  Array('ms_BN', 'Malay_Brunei Darussalam', 'ms_MY', 'malay');
$LOCALE_ARRAY[1100] =  Array('ml_IN', 'malayalam');
$LOCALE_ARRAY[1112] =  Array('manipuri');
$LOCALE_ARRAY[1102] =  Array('mr_IN', 'marathi');
$LOCALE_ARRAY[2145] =  Array('ne_NP', 'nepali');
$LOCALE_ARRAY[1044] =  Array('bokmal', 'no_NO', 'norwegian');
$LOCALE_ARRAY[2068] =  Array('nynorsk', 'no_NO', 'norwegian');
$LOCALE_ARRAY[1096] =  Array('or_IN', 'oriya');
$LOCALE_ARRAY[1045] =  Array('pl_PL', 'polish');
$LOCALE_ARRAY[1046] =  Array('pt_BR', 'Portuguese_Brazil', 'portuguese');
$LOCALE_ARRAY[2070] =  Array('pt_PT', 'Portuguese_Portugal', 'portuguese');
$LOCALE_ARRAY[1094] =  Array('pa_IN', 'punjabi');
$LOCALE_ARRAY[1048] =  Array('ro_RO', 'romanian');
$LOCALE_ARRAY[1049] =  Array('ru_RU', 'russian');
$LOCALE_ARRAY[1103] =  Array('sa_IN', 'sanskrit');
$LOCALE_ARRAY[3098] =  Array('sr_YU@cyrillic', 'serbian@cyrillic', 'sr_YU', 'serbian');
$LOCALE_ARRAY[2074] =  Array('sr_YU@latin', 'serbian@latin', 'sr_YU', 'serbian');
$LOCALE_ARRAY[1113] =  Array('sd_PK', 'sindhi');
$LOCALE_ARRAY[1051] =  Array('sk_SK', 'slovak');
$LOCALE_ARRAY[1060] =  Array('sl_SI', 'slovenian');
$LOCALE_ARRAY[1034] =  Array('es_ES', 'spanish');
$LOCALE_ARRAY[2058] =  Array('es_MX', 'Spanish_Mexico', 'spanish');
$LOCALE_ARRAY[3082] =  Array('Spanish  (Modern Sort)', 'es_ES', 'spanish');
$LOCALE_ARRAY[4106] =  Array('es_GT', 'Spanish_Guatemala', 'spanish');
$LOCALE_ARRAY[5130] =  Array('es_CR', 'Spanish_Costa Rica', 'spanish');
$LOCALE_ARRAY[6154] =  Array('es_PA', 'Spanish_Panama', 'spanish');
$LOCALE_ARRAY[7178] =  Array('es_DO', 'Spanish_Dominican Republic', 'spanish');
$LOCALE_ARRAY[8202] =  Array('es_VE', 'Spanish_Venezuela', 'spanish');
$LOCALE_ARRAY[9226] =  Array('es_CO', 'Spanish_Colombia', 'spanish');
$LOCALE_ARRAY[10250] =  Array('es_PE', 'Spanish_Peru', 'spanish');
$LOCALE_ARRAY[11274] =  Array('es_AR', 'Spanish_Argentina', 'spanish');
$LOCALE_ARRAY[12298] =  Array('es_EC', 'Spanish_Ecuador', 'spanish');
$LOCALE_ARRAY[13322] =  Array('es_CL', 'Spanish_Chile', 'spanish');
$LOCALE_ARRAY[14346] =  Array('es_UY', 'Spanish_Uruguay', 'spanish');
$LOCALE_ARRAY[15370] =  Array('es_PY', 'Spanish_Paraguay', 'spanish');
$LOCALE_ARRAY[16394] =  Array('es_BO', 'Spanish_Bolivia', 'spanish');
$LOCALE_ARRAY[17418] =  Array('es_CV', 'Spanish_El Salvador', 'spanish');
$LOCALE_ARRAY[18442] =  Array('es_HN', 'Spanish_Honduras', 'spanish');
$LOCALE_ARRAY[19466] =  Array('es_NI', 'Spanish_Nicaragua', 'spanish');
$LOCALE_ARRAY[20490] =  Array('es_PR', 'Spanish_Puerto Rico', 'spanish');
$LOCALE_ARRAY[1072] =  Array('sx', 'sutu');
$LOCALE_ARRAY[1089] =  Array('sw_KE', 'swahili');
$LOCALE_ARRAY[1053] =  Array('sv_SE', 'swedish');
$LOCALE_ARRAY[2077] =  Array('sv_FI', 'Swedish_Finland', 'swedish');
$LOCALE_ARRAY[1097] =  Array('ta_IN', 'tamil');
$LOCALE_ARRAY[1092] =  Array('tt_RU', 'tatar');
$LOCALE_ARRAY[1098] =  Array('te_IN', 'telugu');
$LOCALE_ARRAY[1054] =  Array('th_TH', 'thai');
$LOCALE_ARRAY[1055] =  Array('tr_TR', 'turkish');
$LOCALE_ARRAY[1058] =  Array('uk_UA', 'ukrainian');
$LOCALE_ARRAY[1056] =  Array('ur_PK', 'Urdu_Pakistan', 'urdu');
$LOCALE_ARRAY[2080] =  Array('ur_IN', 'Urdu_India', 'urdu');
$LOCALE_ARRAY[1091] =  Array('uz_UZ@latin', 'uzbek@latin', 'uz_UZ', 'uzbek');
$LOCALE_ARRAY[2115] =  Array('uz_UZ@cyrillic', 'uzbek@cyrillic', 'uz_UZ', 'uzbek');
$LOCALE_ARRAY[1066] =  Array('vi_VN', 'vietnamese');

global $LOCALE_ID;
$LOCALE_ID = null;

global $LOCALE_CACHE;
$LOCALE_CACHE = Array();

function InitializeLocale($aLocaleID)
{
	global $LOCALE_ARRAY, $LOCALE_ID;

	if (!is_null($LOCALE_ID))
	{
		if ($LOCALE_ID == $aLocaleID)
		{
			return;
		}
	}
	if (!array_key_exists($aLocaleID, $LOCALE_ARRAY))
	{
		$aLocaleID = 0;
	}

	global $LOCALE_CACHE;
	if (array_key_exists($aLocaleID, $LOCALE_CACHE))
	{
		global $LOCALE_ID;
		$LOCALE_ID = $LOCALE_CACHE[$aLocaleID]['LOCALE_ID'];

		global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
		$DECIMAL_SYMBOL = $LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'];
		$DIGIT_GROUPING_SYMBOL = $LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'];

		global $MONTH_NAMES, $MONTH_ABBR;
		$MONTH_NAMES = $LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'];
		$MONTH_ABBR = $LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'];

		global $DAY_NAMES, $DAY_ABBR;
		$DAY_NAMES = $LOCALE_CACHE[$aLocaleID]['DAY_NAMES'];
		$DAY_ABBR = $LOCALE_CACHE[$aLocaleID]['DAY_ABBR'];

		global $MONTH_REGEXP;
		$MONTH_REGEXP = $LOCALE_CACHE[$aLocaleID]['MONTH_REGEXP'];

		return;
	}

	$aLocaleArray = $LOCALE_ARRAY[$aLocaleID];
	if (setlocale(LC_ALL, $aLocaleArray) === false)
	{
		$aLocaleID = 0;
		setlocale(LC_ALL, 'en_US', 'english');

		if (array_key_exists($aLocaleID, $LOCALE_CACHE))
		{
			global $LOCALE_ID;
			$LOCALE_ID = $LOCALE_CACHE[$aLocaleID]['LOCALE_ID'];

			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = $LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'];
			$DIGIT_GROUPING_SYMBOL = $LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'];

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = $LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'];
			$MONTH_ABBR = $LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'];

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = $LOCALE_CACHE[$aLocaleID]['DAY_NAMES'];
			$DAY_ABBR = $LOCALE_CACHE[$aLocaleID]['DAY_ABBR'];

			global $MONTH_REGEXP;
			$MONTH_REGEXP = $LOCALE_CACHE[$aLocaleID]['MONTH_REGEXP'];

			return;
		}
	}

	$LOCALE_CACHE[$aLocaleID] = Array();

	$LOCALE_ID = $aLocaleID;
	$LOCALE_CACHE[$aLocaleID]['LOCALE_ID'] = $LOCALE_ID;

	switch ($aLocaleID)
	{
		case 0:
		case 2:
		case 1024:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
			$MONTH_ABBR = array(1=>'Jan',2=>'Feb',3=>'Mar',4=>'Apr',5=>'May',6=>'Jun',7=>'Jul',8=>'Aug',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dec');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
			$DAY_ABBR = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 1:
		case 2058:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Enero',2=>'Febrero',3=>'Marzo',4=>'Abril',5=>'Mayo',6=>'Junio',7=>'Julio',8=>'Agosto',9=>'Septiembre',10=>'Octubre',11=>'Noviembre',12=>'Diciembre');
			$MONTH_ABBR = array(1=>'Ene',2=>'Feb',3=>'Mar',4=>'Abr',5=>'May',6=>'Jun',7=>'Jul',8=>'Ago',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dic');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
			$DAY_ABBR = array('Dom','Lun','Mar','Mie','Jue','Vie','Sab');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 3:
		case 1040:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Gennaio',2=>'Febbraio',3=>'Marzo',4=>'Aprile',5=>'Maggio',6=>'Giugno',7=>'Luglio',8=>'Agosto',9=>'Settembre',10=>'Ottobre',11=>'Novembre',12=>'Dicembre');
			$MONTH_ABBR = array(1=>'Gen',2=>'Feb',3=>'Mar',4=>'Apr',5=>'Mag',6=>'Giu',7=>'Lug',8=>'Ago',9=>'Set',10=>'Ott',11=>'Nov',12=>'Dic');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Domenica','Lunedì','Martedì','Mercoledì','Giovedì','Venerdì','Sabato');
			$DAY_ABBR = array('Dom','Lun','Mar','Mer','Gio','Ven','Sab');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 4:
		case 2070:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Janeiro',2=>'Fevereiro',3=>'Março',4=>'Abril',5=>'Maio',6=>'Junho',7=>'Julho',8=>'Agosto',9=>'Setembro',10=>'Outubro',11=>'Novembro',12=>'Dezembro');
			$MONTH_ABBR = array(1=>'Jan',2=>'Fev',3=>'Mar',4=>'Abr',5=>'Mai',6=>'Jun',7=>'Jul',8=>'Ago',9=>'Set',10=>'Out',11=>'Nov',12=>'Dez');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
			$DAY_ABBR = array('Dom','Seg','Ter','Qua','Qui','Sex','Sáb');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 5:
		case 1027:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Gener',2=>'Febrer',3=>'Març',4=>'Abril',5=>'Maig',6=>'Juny',7=>'Juliol',8=>'Agost',9=>'Setembre',10=>'Octubre',11=>'Novembre',12=>'Desembre');
			$MONTH_ABBR = array(1=>'Gen',2=>'Feb',3=>'Març',4=>'Abr',5=>'Maig',6=>'Juny',7=>'Jul',8=>'Ag',9=>'Set',10=>'Oct',11=>'Nov',12=>'Des');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Diumenge','Dilluns','Dimarts','Dimecres','Dijous','Divendres','Dissabte');
			$DAY_ABBR = array('Dg.','Dl.','Dt.','Dc.','Dj.','Dv.','Ds.');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 6:
		case 1046:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Janeiro',2=>'Fevereiro',3=>'Março',4=>'Abril',5=>'Maio',6=>'Junho',7=>'Julho',8=>'Agosto',9=>'Setembro',10=>'Outubro',11=>'Novembro',12=>'Dezembro');
			$MONTH_ABBR = array(1=>'Jan',2=>'Fev',3=>'Mar',4=>'Abr',5=>'Mai',6=>'Jun',7=>'Jul',8=>'Ago',9=>'Set',10=>'Out',11=>'Nov',12=>'Dez');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Domingo','Segunda-feira','Terça-feira','Quarta-feira','Quinta-feira','Sexta-feira','Sábado');
			$DAY_ABBR = array('Dom','Seg','Ter','Qua','Qui','Sex','Sáb');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		case 7:
		case 1034:
		case 3082:
			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = ',';
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = '.';
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = array(1=>'Enero',2=>'Febrero',3=>'Marzo',4=>'Abril',5=>'Mayo',6=>'Junio',7=>'Julio',8=>'Agosto',9=>'Septiembre',10=>'Octubre',11=>'Noviembre',12=>'Diciembre');
			$MONTH_ABBR = array(1=>'Ene',2=>'Feb',3=>'Mar',4=>'Abr',5=>'May',6=>'Jun',7=>'Jul',8=>'Ago',9=>'Sep',10=>'Oct',11=>'Nov',12=>'Dic');
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
			$DAY_ABBR = array('Dom','Lun','Mar','Mie','Jue','Vie','Sab');
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
		default:
			$aLocalConv = localeconv();

			global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
			$DECIMAL_SYMBOL = $aLocalConv['mon_decimal_point'];
			$LOCALE_CACHE[$aLocaleID]['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
			$DIGIT_GROUPING_SYMBOL = $aLocalConv['mon_thousands_sep'];
			$LOCALE_CACHE[$aLocaleID]['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

			global $MONTH_NAMES, $MONTH_ABBR;
			$MONTH_NAMES = Array();
			$MONTH_ABBR = Array();
			for ($i = 1; $i <= 12; $i++)
			{
				$aTime = strtotime('2001-'.$i.'-01');
			    $MONTH_NAMES[$i] = strftime('%B', $aTime);
			    $MONTH_ABBR[$i] = strftime('%b', $aTime);
			}
			$LOCALE_CACHE[$aLocaleID]['MONTH_NAMES'] = $MONTH_NAMES;
			$LOCALE_CACHE[$aLocaleID]['MONTH_ABBR'] = $MONTH_ABBR;

			global $DAY_NAMES, $DAY_ABBR;
			$DAY_NAMES = Array();
			$DAY_ABBR = Array();
			for ($i = 0, $offset = 0 - strftime('%w'); $i < 7; $i++, $offset++)
			{
				$aTime = strtotime($offset.' day');
			    $DAY_NAMES[$i] = strftime('%A', $aTime);
			    $DAY_ABBR[$i] = strftime('%a', $aTime);
			}
			$LOCALE_CACHE[$aLocaleID]['DAY_NAMES'] = $DAY_NAMES;
			$LOCALE_CACHE[$aLocaleID]['DAY_ABBR'] = $DAY_ABBR;
			break;
	}

	if ($aLocaleID != 0)
	{
		setlocale(LC_ALL, 'en_US', 'english');
	}

	global $MONTH_REGEXP;
	$MONTH_REGEXP = '(';
	foreach (array_reverse($MONTH_NAMES, true) as $key => $value)
	{
		if ($MONTH_REGEXP != '(')
		{
			$MONTH_REGEXP .= '|';
		}
		$MONTH_REGEXP .= $value;
		$MONTH_REGEXP .= ('|'.$MONTH_ABBR[$key]);
		$MONTH_REGEXP .= ('|'.sprintf("%02d", $key));
	}
	$MONTH_REGEXP .= ')?';
	$LOCALE_CACHE[$aLocaleID]['MONTH_REGEXP'] = $MONTH_REGEXP;
}


function SaveLocale()
{
	$anArray = Array();

	global $LOCALE_ID;
	$anArray['LOCALE_ID'] = $LOCALE_ID;

	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
	$anArray['DECIMAL_SYMBOL'] = $DECIMAL_SYMBOL;
	$anArray['DIGIT_GROUPING_SYMBOL'] = $DIGIT_GROUPING_SYMBOL;

	global $MONTH_NAMES, $MONTH_ABBR;
	$anArray['MONTH_NAMES'] = $MONTH_NAMES;
	$anArray['MONTH_ABBR'] = $MONTH_ABBR;

	global $DAY_NAMES, $DAY_ABBR;
	$anArray['DAY_NAMES'] = $DAY_NAMES;
	$anArray['DAY_ABBR'] = $DAY_ABBR;

	global $MONTH_REGEXP;
	$anArray['MONTH_REGEXP'] = $MONTH_REGEXP;

	global $LOCALE_STACK;
	array_push($LOCALE_STACK, $anArray);
}

function RestoreLocale()
{
	global $LOCALE_STACK;
	$anArray = array_pop($LOCALE_STACK);
	if (!is_null($anArray))
	{
		global $LOCALE_ID;
		$LOCALE_ID = $anArray['LOCALE_ID'];

		global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;
		$DECIMAL_SYMBOL = $anArray['DECIMAL_SYMBOL'];
		$DIGIT_GROUPING_SYMBOL = $anArray['DIGIT_GROUPING_SYMBOL'];

		global $MONTH_NAMES, $MONTH_ABBR;
		$MONTH_NAMES = $anArray['MONTH_NAMES'];
		$MONTH_ABBR = $anArray['MONTH_ABBR'];

		global $DAY_NAMES, $DAY_ABBR;
		$DAY_NAMES = $anArray['DAY_NAMES'];
		$DAY_ABBR = $anArray['DAY_ABBR'];

		global $MONTH_REGEXP;
		$MONTH_REGEXP = $anArray['MONTH_REGEXP'];
	}
}

global $LOCALE_STACK;
$LOCALE_STACK = Array();

InitializeLocale(0);


function unformatStringRegExpStr($aFormat)
{
 	$regExpStr = '';
 	$n = strlen($aFormat);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aFormat[$i];
		if ($ch == '\\')
		{
			$i++;
			if ($i < $n)
			{
				$ch = $aFormat[$i];
				switch ($ch)
				{
					case '\\':
						$regExpStr .= '(\\\\?)';
						break;
					case '$':
						$regExpStr .= '(\\'.$ch.'?)';
					    break;
					case '^':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					case '.':
						$regExpStr .= '(\\'.$ch.'?)';
					    break;
					default:
						$regExpStr .= '('.$ch.'?)';
					    break;
				}
			}
		}
		else
		{
			switch ($ch)
			{
				case '$':
					$regExpStr .= '(\\'.$ch.'?)';
				    break;
				case '^':
				case '*':
				case '+':
				case '?':
				case '{':
				case '}':
				case '!':
				case '(':
				case ')':
				case '[':
				case ']':
				case '.':
					$regExpStr .= '(\\'.$ch.'?)';
				    break;
				case '#':
				    $regExpStr .= '([0-9]*)';
					break;
				case '0':
					$regExpStr .= '([0-9]?)';
					break;
				case 'A':
				    $regExpStr .= '([A-Z]*)';
					break;
				case 'a':
					$regExpStr .= '([A-Z]?)';
					break;
				case 'N':
				    $regExpStr .= '([0-9A-Z]*)';
					break;
				case 'n':
				    $regExpStr .= '([0-9A-Z]?)';
					break;
				case 'W':
				    $regExpStr .= '([0-9A-Z_]*)';
					break;
				case 'w':
					$regExpStr .= '([0-9A-Z_]?)';
					break;
				default:
					$regExpStr .= '('.$ch.'?)';
				    break;
			}
		}
	}
	return $regExpStr;
}

function formatStringRegExpStr($aFormat)
{
 	$regExpStr = '';
 	$inWord = false;
 	$n = strlen($aFormat);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aFormat[$i];
		if ($ch == '\\')
		{
			$i++;
			if ($i < $n)
			{
				$ch = $aFormat[$i];
				switch ($ch)
				{
					case '\\':
						if (!$inWord)
						{
							$regExpStr .= '(?:';
							$inWord = true;
						}
						$regExpStr .= '\\\\';
						break;
					case '$':
					case '^':
					case '*':
					case '+':
					case '?':
					case '{':
					case '}':
					case '!':
					case '(':
					case ')':
					case '[':
					case ']':
					case '.':
					    if (!$inWord)
						{
							$regExpStr .= '(?:';
							$inWord = true;
						}
						$regExpStr .= '\\'.$ch;
						break;
					default:
						if (!$inWord)
						{
							$regExpStr .= '(?:';
							$inWord = true;
						}
						$regExpStr .= $ch;
						break;
				}
			}
		}
		else
		{
			switch ($ch)
			{
				case '$':
				case '^':
				case '*':
				case '+':
				case '?':
				case '{':
				case '}':
				case '!':
				case '(':
				case ')':
				case '[':
				case ']':
				case '.':
					if (!$inWord)
					{
						$regExpStr .= '(?:';
						$inWord = true;
					}
					$regExpStr .= '\\'.$ch;
					break;
				case '#':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9]*)';
					break;
				case '0':
					if ($inWord)
					{
					    $regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9]?)';
					break;
				case 'A':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([A-Z]*)';
					break;
				case 'a':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([A-Z]?)';
					break;
				case 'N':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9A-Z]*)';
					break;
				case 'n':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9A-Z]?)';
					break;
				case 'W':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9A-Z_]*)';
					break;
				case 'w':
					if ($inWord)
					{
						$regExpStr .= ')?';
					    $inWord = false;
					}
					$regExpStr .= '([0-9A-Z_]?)';
					break;
				default:
					if (!$inWord)
					{
						$regExpStr .= '(?:';
						$inWord = true;
					}
					$regExpStr .= $ch;
					break;
			}
		}
	}
	if ($inWord)
	{
		$regExpStr .= ')?';
		$inWord = false;
	}
	return $regExpStr;
}

function unformatString($aString, $aFormat)
{
	$aString = (string) $aString;
	if ($aFormat == '')
	{
		return $aString;
	}
	$regExpStr = unformatStringRegExpStr($aFormat);
	$matchArray = array();
	if (!preg_match('/'.$regExpStr.'/i', $aString, $matchArray))
 	{
 	    return '';
 	}
 	if (strlen($matchArray[0]) != strlen($aString))
 	{
 		return '';
 	}
 	$unformatedString = '';
 	$n = count($matchArray);
	for ($i = 1; $i < $n; $i++)
	{
 	    $unformatedString .= $matchArray[$i];
 	}
 	return unformatedString;
}

function formatString($aString, $aFormat)
{
	$aString = (string) $aString;
	if ($aFormat == '')
	{
		return $aString;
	}
 	$regExpStr = formatStringRegExpStr($aFormat);
	$matchArray = array();
	if (!preg_match('/'.$regExpStr.'/i', $aString, $matchArray))
 	{
 	    return '';
 	}
 	if (strlen($matchArray[0]) != strlen($aString))
 	{
 		return '';
 	}
 	$formatedString = '';
 	$matchPos = 1;
 	$matchLen = count($matchArray);
 	while ($matchLen > 1 && ($matchArray[$matchLen - 1] == ''))
 	{
 	    $matchLen--;
 	}
 	$n = strlen($aFormat);
	for ($i = 0; $i < $n && $matchPos < $matchLen; $i++)
	{
		$ch = $aFormat[$i];
		if ($ch == '\\')
		{
			$i++;
			if ($i < $n)
			{
				$ch = $aFormat[$i];
				$formatedString .= $ch;
			}
		}
		else
		{
			switch ($ch)
			{
				case '#':
				case '0':
				case 'A':
				case 'a':
				case 'N':
				case 'n':
				case 'W':
				case 'w':
					$formatedString .= $matchArray[$matchPos];
					$matchPos++;
					break;
				default:
						$formatedString .= $ch;
					    break;
			}
		}
	}
 	return $formatedString;
}

function indexOfChar($aString, $aChar)
{
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aString);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aString[$i];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if (!$inQuotes)
					{
						if ($ch == '\\')
						{
							$i++;
						}
						else
						{
							if ($ch == $aChar)
							{
								return $i;
							}
						}
					}
				}
			}
		}
	}
	return -1;
}

function formatPositions($aString)
{
	$formatPositions = array(-1, -1, -1, '', 0, 0, 0, 0, 0);
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aString);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aString[$i];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
			else
			{
				if ($formatPositions[2] == -1)
				{
					$formatPositions[3] .= $ch;
				}
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						if ($formatPositions[2] == -1)
						{
							$formatPositions[3] .= $ch;
						}
					}
					else
					{
						if ($ch == '\\')
						{
							if (($i + 1) < $n)
							{
								$i++;
								$ch = $aString[$i];
							}
							if ($formatPositions[2] == -1)
							{
								$formatPositions[3] .= $ch;
							}
						}
						else
						{
							if ($formatPositions[0] == -1 && $ch == '.')
							{
								$formatPositions[0] = $i;
							}
							if ($formatPositions[1] == -1 && $ch == ',')
							{
								$formatPositions[1] = $i;
							}
							if ($formatPositions[2] == -1)
							{
								if ($ch == '#' || $ch == '0')
								{
									$formatPositions[2] = $i;
								}
								else
								{
									$formatPositions[3] .= $ch;
								}
							}
							if ($formatPositions[0] == -1)
							{
								if ($ch == ',')
								{
									$formatPositions[4]++;
								}
								else
								{
									$formatPositions[4] = 0;
								}
							}
							if ($ch == '%')
							{
								$formatPositions[5]++;
							}
							if ($formatPositions[0] == -1)
							{
								if ($ch == '0')
								{
									$formatPositions[6]++;
								}
								if ($ch == '#' && $formatPositions[6] > 0)
								{
									$formatPositions[6]++;
								}
							}
							else
							{
								if ($ch == '0')
								{
									$formatPositions[7]++;
									$formatPositions[8] = $formatPositions[7];
								}
								else
								{
									if ($ch == '#')
									{
					                	$formatPositions[7]++;
					            	}
								}
							}
						}
					}
				}
			}
		}
	}
	if ($formatPositions[2] == -1)
	{
		$formatPositions[2] = 0;
	}
	return $formatPositions;
}

function removeChar($aString, $aChar)
{
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aString) - 1;
	for ($i = $n; $i >= 0; $i--)
	{
		$ch = $aString[$i];
		if ($inBrackets)
		{
			if ($ch == '[')
			{
				$inBrackets = false;
			}
		}
		else
		{
			if ($ch == ']' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if (!$inQuotes)
					{
						if ($i > 0 && $aString[$i - 1] == '\\')
						{
							$i--;
						}
						else
						{
							if ($ch == $aChar)
							{
								$aString = substr($aString, 0, $i).substr($aString, $i + 1);
								$n--;
							}
						}
					}
				}
			}
		}
	}
	return $aString;
}

function unformatNumberRegExpStr($aFormat, &$anInfoArray)
{
	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;

	$regExpStr = '(\-)?';
	$digitMatchCapture = false;
	$decimalPointMatchCapture = false;
	$colons = 0;
	$percentSigns = 0;
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aFormat);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aFormat[$i];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
			else
			{
				$regExpStr .= '\\'.$ch.'?';
				$digitMatchCapture = false;
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						$regExpStr .= '\\'.$ch.'?';
						$digitMatchCapture = false;
					}
					else
					{
						if ($ch == '\\')
						{
							$digitMatchCapture = false;
							$i++;
							if ($i < $n)
							{
								$ch = $aFormat[$i];
								switch ($ch)
								{
									case '\\':
										$regExpStr .= '\\\\?';
										break;
									case '^':
									case '$':
									case '*':
									case '+':
									case '?':
									case '{':
									case '}':
									case '!':
									case '(':
									case ')':
									case '[':
									case ']':
									    $regExpStr .= '\\'.$ch.'?';
										break;
									default:
										if (($ch >= '0' && $ch <= '9') || $ch == '-')
										{
											$regExpStr .= $ch;
										}
										else
										{
										 	$regExpStr .= $ch.'?';
										}
										break;
								}
							}
						}
						else
						{
							switch ($ch)
							{
								case '^':
								case '$':
								case '*':
								case '+':
								case '?':
								case '{':
								case '}':
								case '!':
								case '(':
								case ')':
								case '[':
								case ']':
									$regExpStr .= '\\'.$ch.'?';
									$digitMatchCapture = false;
									break;
								case ',':
									if (!$decimalPointMatchCapture)
									{
									    $colons++;
									}
									break;
								case '#':
								case '0':
									if (!$decimalPointMatchCapture)
									{
										if (!$digitMatchCapture)
									    {
									    	$regExpStr .= '([0-9'.$DIGIT_GROUPING_SYMBOL.']*)';
									    }
										$colons = 0;
									}
									else
									{
									    if (!$digitMatchCapture)
									    {
									    	$regExpStr .= '([0-9]*)';
									    }
									}
									$digitMatchCapture = true;
									break;
								case '.':
									if (!$decimalPointMatchCapture)
									{
									    $regExpStr .= '(\\'.$DECIMAL_SYMBOL.'?)';
									}
									else
									{
										$regExpStr .= '\\'.$DECIMAL_SYMBOL.'?';
									}
									$digitMatchCapture = false;
									$decimalPointMatchCapture = true;
									break;
								case '%':
									$percentSigns++;
								default:
									if (($ch >= '0' && $ch <= '9') || $ch == '-')
									{
										$regExpStr .= $ch;
									}
									else
									{
										$regExpStr .= $ch.'?';
									}
									$digitMatchCapture = false;
									break;
							}
						}
					}
				}
			}
		}
	}
	if (!is_null($anInfoArray))
	{
		$anInfoArray[0] = $colons;
		$anInfoArray[1] = $percentSigns;
	}
	return $regExpStr;
}

function unformatNumber($aNumber, $aFormat, $aLocaleID = null, $aSingleFormat = false)
{
	if (!is_string($aNumber))
	{
		$unformatedNumber = sprintf("%F", $aNumber);
		return $unformatedNumber;
	}

	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;

	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}

	if ($aFormat == '')
	{
		$unformatedNumber = preg_replace('/([^0-9'.$DECIMAL_SYMBOL.'\-])/', '', $aNumber);
		$unformatedNumber = preg_replace('/(['.$DECIMAL_SYMBOL.'])/', '.', $unformatedNumber);

		if ($unformatedNumber == '')
		{
			$unformatedNumber = (double) 0;
		}
		else
		{
			$unformatedNumber = (double) $unformatedNumber;
			if (is_nan($unformatedNumber))
			{
				$unformatedNumber = (double) 0;
			}
		}
		if ($restoreLocale)
		{
			RestoreLocale();
		}
		return $unformatedNumber;
	}
	if (!$aSingleFormat)
	{
		$semicolonPos = indexOfChar($aFormat, ';');
		if ($semicolonPos >= 0)
		{
			$positiveFormat = substr($aFormat, 0, $semicolonPos);
			$negativeFormat = substr($aFormat, $semicolonPos + 1);
			$zeroFormat = null;
			$semicolonPos = indexOfChar($negativeFormat, ';');
			if ($semicolonPos >= 0)
			{
				$zeroFormat = substr($negativeFormat, $semicolonPos + 1);
				$negativeFormat = substr($negativeFormat, 0, $semicolonPos);
			}
			$anArray = array(null, null, null);
			$anArray[0] = privateUnformatNumber($aNumber, $positiveFormat);
			$anArray[1] = privateUnformatNumber($aNumber, $negativeFormat);
			if (!is_null($anArray[1]))
			{
				$anArray[1][0] = -1 * $anArray[1][0];
			}
			$anArray[2] = null;
			if (!is_null($zeroFormat))
			{
				$anArray[2] = privateUnformatNumber($aNumber, $zeroFormat);
				if (!is_null($anArray[2]))
				{
					$anArray[2][0] = 0;
				}
			}
			$max_i = -1;
			$max_len = -1;
			$n = count($anArray);
			for ($i = 0; $i < $n; $i++)
			{
				if (!is_null($anArray[$i]))
				{
					if ($anArray[$i][1] > $max_len)
					{
						$max_i = i;
						$max_len = $anArray[$i][1];
					}
				}
			}
			if ($restoreLocale)
			{
				RestoreLocale();
			}
			return ($max_i < 0 ? 0 : $anArray[$max_i][0]);
		}
	}
 	$anArray = privateUnformatNumber($aNumber, $aFormat);
	if ($restoreLocale)
	{
		RestoreLocale();
	}
	return (is_null($anArray) ? 0 : $anArray[0]);
}

function privateUnformatNumber($aNumber, $aFormat)
{
	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;

	$infoArray = array();
	$regExpStr = unformatNumberRegExpStr($aFormat, $infoArray);
	$colons = $infoArray[0];
	$percentSigns = $infoArray[1];
	$matchArray = array();
	if (!preg_match('/'.$regExpStr.'/i', $aNumber, $matchArray))
 	{
 	    return null;
 	}
 	if (strlen($matchArray[0]) != strlen($aNumber))
 	{
 		return null;
 	}
 	$unformatedNumber = '';
 	$n = count($matchArray);
 	for ($i = 1; $i < $n; $i++)
 	{
 	    $unformatedNumber .= $matchArray[$i];
 	}

 	$unformatedNumber = preg_replace('/(['.$DIGIT_GROUPING_SYMBOL.'])/', '', $unformatedNumber);
 	$unformatedNumber = preg_replace('/(['.$DECIMAL_SYMBOL.'])/', '.', $unformatedNumber);
	if (is_nan($unformatedNumber))
	{
		$unformatedNumber = '0';
	}
	$unformatedNumber = $unformatedNumber *  bcdiv(pow(1000, $colons), pow(100, $percentSigns));
	//$unformatedNumber = $unformatedNumber * ($colons / $percentSigns);
 	return array($unformatedNumber, strlen($matchArray[0]));
}

global $NUMBER_FORMATS;
$NUMBER_FORMATS = Array();

$NUMBER_FORMATS['#,##0.0000'] = Array(4, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '', '', 0, '');
$NUMBER_FORMATS['#,##0.000'] = Array(3, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '', '', 0, '');
$NUMBER_FORMATS['#,##0.00'] = Array(2, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '', '', 0, '');
$NUMBER_FORMATS['#,##0.0'] = Array(1, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '', '', 0, '');
$NUMBER_FORMATS['#,##0'] = Array(0, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '', '', 0, '');
$NUMBER_FORMATS['$#,##0.0000'] = Array(4, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '$', '', 0, '');
$NUMBER_FORMATS['$#,##0.000'] = Array(3, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '$', '', 0, '');
$NUMBER_FORMATS['$#,##0.00'] = Array(2, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '$', '', 0, '');
$NUMBER_FORMATS['$#,##0.0'] = Array(1, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '$', '', 0, '');
$NUMBER_FORMATS['$#,##0'] = Array(0, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '$', '', 0, '');
$NUMBER_FORMATS['€#,##0.0000'] = Array(4, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '€', '', 0, '');
$NUMBER_FORMATS['€#,##0.000'] = Array(3, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '€', '', 0, '');
$NUMBER_FORMATS['€#,##0.00'] = Array(2, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '€', '', 0, '');
$NUMBER_FORMATS['€#,##0.0'] = Array(1, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '€', '', 0, '');
$NUMBER_FORMATS['€#,##0'] = Array(0, $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL, 1, '€', '', 0, '');
$NUMBER_FORMATS['0.0000'] = Array(4, $DECIMAL_SYMBOL, '', 1, '', '', 0, '');
$NUMBER_FORMATS['0.000'] = Array(3, $DECIMAL_SYMBOL, '', 1, '', '', 0, '');
$NUMBER_FORMATS['0.00'] = Array(2, $DECIMAL_SYMBOL, '', 1, '', '', 0, '');
$NUMBER_FORMATS['0.0'] = Array(1, $DECIMAL_SYMBOL, '', 1, '', '', 0, '');
$NUMBER_FORMATS['0'] = Array(0, $DECIMAL_SYMBOL, '', 1, '', '', 0, '');
$NUMBER_FORMATS['00'] = Array(0, $DECIMAL_SYMBOL, '', 1, '', '', 2, '00');
$NUMBER_FORMATS['000'] = Array(0, $DECIMAL_SYMBOL, '', 1, '', '', 3, '000');
$NUMBER_FORMATS['0000'] = Array(0, $DECIMAL_SYMBOL, '', 1, '', '', 4, '0000');
$NUMBER_FORMATS['00000'] = Array(0, $DECIMAL_SYMBOL, '', 1, '', '', 5, '00000');
$NUMBER_FORMATS['#.0000%'] = Array(4, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['#.000%'] = Array(3, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['#.00%'] = Array(2, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['#.0%'] = Array(1, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['#%'] = Array(0, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['0.0000%'] = Array(4, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['0.000%'] = Array(3, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['0.00%'] = Array(2, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['0.0%'] = Array(1, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');
$NUMBER_FORMATS['0%'] = Array(0, $DECIMAL_SYMBOL, '', 100, '', '%', 0, '');


function formatNumber($aNumber, $aFormat, $aLocaleID = null, $aSingleFormat = false)
{
	//GCRUZ 2015-11-02. Validar con vacíos para que no marque warnigns subsecuentes. Issue:WWZIDH
	if (trim($aNumber) == '')
		$aNumber = 0;
	if ($aFormat != '' && (strpos($aFormat, '0') === false && strpos($aFormat, '#') === false))
	{
		return formatDateTime($aNumber, $aFormat, $aLocaleID);
	}
	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}

	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;

	if ($aFormat == '')
	{
		$formatedNumber = sprintf("%F", $aNumber);
 		$formatedNumber = preg_replace('/([.])/', $DECIMAL_SYMBOL, $formatedNumber);

		if ($restoreLocale)
		{
			RestoreLocale();
		}
		//return sprintf("%F", $formatedNumber);
		return $formatedNumber;
	}
	global $NUMBER_FORMATS;
	if (array_key_exists($aFormat, $NUMBER_FORMATS))
	{
		$formatedNumber = number_format(($NUMBER_FORMATS[$aFormat][3] == 1 ? $aNumber : $aNumber * $NUMBER_FORMATS[$aFormat][3]), $NUMBER_FORMATS[$aFormat][0], ($NUMBER_FORMATS[$aFormat][1] == '' ? '' : $DECIMAL_SYMBOL), ($NUMBER_FORMATS[$aFormat][2] == '' ? '' : $DIGIT_GROUPING_SYMBOL));
		if ($aFormat[0] == '#' && $aFormat[1] == $NUMBER_FORMATS[$aFormat][1])
		{
			if ($formatedNumber[0] == '0' && $formatedNumber[1] == $NUMBER_FORMATS[$aFormat][1])
			{
				$formatedNumber = substr($formatedNumber, 1);
			}
		}
		if ($NUMBER_FORMATS[$aFormat][6] > 0)
		{
			if (strlen($formatedNumber) < $NUMBER_FORMATS[$aFormat][6])
			{
				$formatedNumber = substr($NUMBER_FORMATS[$aFormat][7].$formatedNumber, -1 * $NUMBER_FORMATS[$aFormat][6], $NUMBER_FORMATS[$aFormat][6]);
			}
		}
		$formatedNumber = $NUMBER_FORMATS[$aFormat][4].$formatedNumber.$NUMBER_FORMATS[$aFormat][5];
	
		if ($restoreLocale)
		{
			RestoreLocale();
		}
		//return sprintf("%F", $formatedNumber);
		return $formatedNumber;
	}
	if (!$aSingleFormat)
	{
		$semicolonPos = indexOfChar($aFormat, ';');
		if ($semicolonPos >= 0)
		{
			$positiveFormat = substr($aFormat, 0, $semicolonPos);
			$negativeFormat = substr($aFormat, $semicolonPos + 1);
			$zeroFormat = null;
			$semicolonPos = indexOfChar($negativeFormat, ';');
			if ($semicolonPos >= 0)
			{
				$zeroFormat = substr($negativeFormat, $semicolonPos + 1);
				$negativeFormat = substr($negativeFormat, 0, $semicolonPos);
			}
			if ($aNumber < 0)
			{
				$formatedNumber = formatNumber(-1 * $aNumber, $negativeFormat, $aLocaleID, true);
				if ($restoreLocale)
				{
					RestoreLocale();
				}
				return $formatedNumber;
			}
			else
			{
	        	if ($aNumber > 0 || is_null($zeroFormat))
				{
					$formatedNumber = formatNumber($aNumber, $positiveFormat, $aLocaleID, true);
					if ($restoreLocale)
					{
						RestoreLocale();
					}
					return $formatedNumber;
				}
				else
				{
					$formatedNumber = formatNumber($aNumber, $zeroFormat, $aLocaleID, true);
					if ($restoreLocale)
					{
						RestoreLocale();
					}
					return $formatedNumber;
				}
			}
		}
	}
	$formatPositions = formatPositions($aFormat);

  	$firstPart = $formatPositions[3];
	$secondPart = substr($aFormat, $formatPositions[2]);
	if ($secondPart == '')
	{
		$formatedNumber = sprintf("%F", $aNumber);
 		$formatedNumber = preg_replace('/([.])/', $DECIMAL_SYMBOL, $formatedNumber);
		if ($firstPart != '')
		{
			if ($formatedNumber[0] == '-')
			{
				$formatedNumber = $formatedNumber[0].$firstPart.substr($formatedNumber, 1);
			}
			else
			{
				$formatedNumber = $firstPart.$formatedNumber;
			}
		}

		if ($restoreLocale)
		{
			RestoreLocale();
		}
		//return sprintf("%F", $formatedNumber);
		return $formatedNumber;
	}
	if (array_key_exists($secondPart, $NUMBER_FORMATS))
	{
		$formatedNumber = number_format(($NUMBER_FORMATS[$secondPart][3] == 1 ? $aNumber : $aNumber * $NUMBER_FORMATS[$secondPart][3]), $NUMBER_FORMATS[$secondPart][0], ($NUMBER_FORMATS[$secondPart][1] == '' ? '' : $DECIMAL_SYMBOL), ($NUMBER_FORMATS[$secondPart][2] == '' ? '' : $DIGIT_GROUPING_SYMBOL));
		if ($secondPart[0] == '#' && $secondPart[1] == $NUMBER_FORMATS[$secondPart][1])
		{
			if ($formatedNumber[0] == '0' && $formatedNumber[1] == $NUMBER_FORMATS[$secondPart][1])
			{
				$formatedNumber = substr($formatedNumber, 1);
			}
		}
		if ($firstPart != '')
		{
			if ($formatedNumber[0] == '-')
			{
				$formatedNumber = $formatedNumber[0].$firstPart.$NUMBER_FORMATS[$secondPart][4].substr($formatedNumber, 1).$NUMBER_FORMATS[$secondPart][5];
			}
			else
			{
				$formatedNumber = $firstPart.$NUMBER_FORMATS[$secondPart][4].$formatedNumber.$NUMBER_FORMATS[$secondPart][5];
			}
		}
		else
		{
			$formatedNumber = $NUMBER_FORMATS[$secondPart][4].$formatedNumber.$NUMBER_FORMATS[$secondPart][5];
		}
		if ($restoreLocale)
		{
			RestoreLocale();
		}
		//return sprintf("%F", $formatedNumber);
		return $formatedNumber;
	}
	$leftPart = '';
	$rightPart = '';
	$decimalPos = $formatPositions[0];
	if ($decimalPos >= 0)
	{
  		$leftPart = substr($aFormat, 0, $decimalPos);
  		$rightPart = substr($aFormat, $decimalPos + 1);
  		$hasDecimalPoint = true;
	}
	else
  	{
		$leftPart = $aFormat;
		$rightPart = '';
		$hasDecimalPoint = false;
  	}
 	$leftPart = substr($leftPart, $formatPositions[2]);
  	$thousandDivisor = $formatPositions[4];
  	$hasThousandSeparator = ($formatPositions[1] != -1);
  	if ($hasThousandSeparator)
  	{
		$leftPart = removeChar($leftPart, ',');
  	}
	$thousandDivisor = pow(1000, $thousandDivisor);
  	$percentageMultiplier = $formatPositions[5];
  	if ($percentageMultiplier == 0)
  	{
  		$percentageMultiplier = 1;
  	}
  	else
  	{
		$percentageMultiplier = pow(100, $percentageMultiplier);
  	}
  	$leftZeroDigits = $formatPositions[6];
	$rightDigits = $formatPositions[7];
	$rightZeroDigits = $formatPositions[8];

	$formatedNumber = sprintf("%F", $aNumber);
	$formatedNumber = $formatedNumber * ($percentageMultiplier / $thousandDivisor);
	$formatedNumber = round($formatedNumber, $rightDigits);
	$isNegative = false;
	if ($formatedNumber < 0)
	{
		$isNegative = true;
		$formatedNumber = ($formatedNumber < 0)  ? -1 * $formatedNumber : $formatedNumber;
	}
	$divisor = pow(10, $rightDigits);
	$formatedNumber = $formatedNumber * $divisor;
	$formatedNumber = sprintf("%F", $formatedNumber);
	$formatedNumber = explode('.', $formatedNumber);
	$formatedNumber = $formatedNumber[0];
	$fraction = bcmod($formatedNumber, $divisor);
	$formatedNumber = bcdiv($formatedNumber, $divisor);
	//$formatedNumber = sprintf("%F", $formatedNumber);
	$formatedNumber = explode('.', $formatedNumber);
	$formatedNumber = $formatedNumber[0];
	$rightZeros = '';
	for ($i = 0; $i < ($rightDigits - strlen($fraction)); $i++)
	{
		$rightZeros = $rightZeros.'0';
	}
	$fraction = $rightZeros.$fraction;
	$fraction = substr($fraction, strlen($fraction) - $rightDigits, $rightDigits);
	$i = strlen($fraction);
	while ($i > $rightZeroDigits && $fraction[$i - 1] == '0')
	{
		$i--;
	}
	$fraction = substr($fraction, 0, $i);
 	$leftZeros = '';
	for ($i = 0; $i < $leftZeroDigits - strlen($formatedNumber); $i++)
	{
		$leftZeros = $leftZeros.'0';
	}
	$formatedNumber = $leftZeros.$formatedNumber;
 	$i = strlen($formatedNumber);
	while ($i > $leftZeroDigits && $formatedNumber[strlen($formatedNumber) - $i] == '0')
	{
		$i--;
	}
	$formatedNumber = substr($formatedNumber, strlen($formatedNumber) - $i);
	$digits = 0;
	$formatPos = strlen($leftPart) - 1;
	$numberPos = strlen($formatedNumber) - 1;
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($leftPart);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $leftPart[$formatPos];
		if ($inBrackets)
		{
			if ($ch == '[')
			{
				$inBrackets = false;
			}
			else
			{
				$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$ch.substr($formatedNumber, $numberPos + 1);
			}
		}
		else
		{
			if ($ch == ']' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$ch.substr($formatedNumber, $numberPos + 1);
					}
					else
					{
						if ($formatPos > 0 && $leftPart[$formatPos - 1] == '\\')
						{
							$ch = $leftPart[$formatPos];
							$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$ch.substr($formatedNumber, $numberPos + 1);
							$formatPos--;
						}
						else
						{
							if ($ch == '#' || $ch == '0')
							{
								if ($digits == 3)
								{
									if ($hasThousandSeparator && $numberPos >= 0)
									{
										$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$DIGIT_GROUPING_SYMBOL.substr($formatedNumber, $numberPos + 1);
									}
									$digits = 0;
								}
								$digits++;
								$numberPos--;
							}
							else
							{
								$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$ch.substr($formatedNumber, $numberPos + 1);
							}
						}
					}
				}
			}
		}
		$formatPos--;
	}
	if ($hasThousandSeparator)
	{
		while ($numberPos >= 0)
		{
			if ($digits == 3)
			{
				$formatedNumber = substr($formatedNumber, 0, $numberPos + 1).$DIGIT_GROUPING_SYMBOL.substr($formatedNumber, $numberPos + 1);
				$digits = 0;
			}
			$digits++;
			$numberPos--;
		}
	}
 	$formatPos = 0;
	$numberPos = 0;
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($rightPart);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $rightPart[$formatPos];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
			else
			{
				$fraction = substr($fraction, 0, $numberPos).$ch.substr($fraction, $numberPos);
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						$fraction = substr($fraction, 0, $numberPos).$ch.substr($fraction, $numberPos);
					}
					else
					{
						if ($ch == '\\')
						{
							if (($formatPos + 1) < $n)
							{
								$formatPos++;
								$ch = $rightPart[$formatPos];
							}
							$fraction = substr($fraction, 0, numberPos).$ch.substr($fraction, $numberPos);
						}
						else
						{
							if ($ch != '#' && $ch != '0')
							{
								$fraction = substr($fraction, 0, $numberPos).$ch.substr($fraction, $numberPos);
							}
						}
					}
				}
			}
		}
		$formatPos++;
		$numberPos++;
	}
	if ($hasDecimalPoint)
  	{
  		$formatedNumber = ((($isNegative) ? '-' : '').$firstPart.$formatedNumber.$DECIMAL_SYMBOL.$fraction);
  		if ($restoreLocale)
		{
			RestoreLocale();
		}
		return $formatedNumber;

	}
	else
	{
		$formatedNumber = ((($isNegative) ? '-' : '').$firstPart.$formatedNumber);
  		if ($restoreLocale)
		{
			RestoreLocale();
		}
		return $formatedNumber;
	}
}

function asNumber($aNumber, $aLocaleID = null)
{
	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}

	global $DECIMAL_SYMBOL, $DIGIT_GROUPING_SYMBOL;

	if (!is_string($aNumber))
	{
		$unformatedNumber = sprintf("%F", $aNumber);
	}
	else
	{
		$unformatedNumber = preg_replace('/([^0-9'.$DECIMAL_SYMBOL.'\-])/', '', $aNumber);
		$unformatedNumber = preg_replace('/(['.$DECIMAL_SYMBOL.'])/', '.', $unformatedNumber);

		if ($unformatedNumber == '')
		{
			$unformatedNumber = (double) 0;
		}
		else
		{
			$unformatedNumber = (double) $unformatedNumber;
			if (is_nan($unformatedNumber))
			{
				$unformatedNumber = (double) 0;
			}
		}
	}
	if ($restoreLocale)
	{
		RestoreLocale();
	}
	return $unformatedNumber;
}

function monthFromName($aString)
{
	global $MONTH_NAMES;
	foreach ($MONTH_NAMES as $key => $value)
	{
		if (strtolower($value) == strtolower($aString))
		{
			return ((string) $key);
		}
	}
	return $aString;
}

function monthFromAbbr($aString)
{
    global $MONTH_ABBR;
	foreach ($MONTH_ABBR as $key => $value)
	{
		if (strtolower($value) == strtolower($aString))
		{
			return ((string) $key);
		}
	}
	return $aString;
}

function unformatDateTimeRegExpStrPart(&$aFormatArray, &$aTypeArray)
{
    global $MONTH_REGEXP;
	$regExpStr = '';
 	if ($aFormatArray[0] == 4)
	{
		$aTypeArray[] = 'yyyy';
		$regExpStr = '([0-9]{0,4})';
	}
	if ($aFormatArray[0] == 3)
	{
		$aTypeArray[] = 'yy';
		$aTypeArray[] = 'y';
		$regExpStr = '([0-9]{0,4})([0-9]{0,4})';
	}
	if ($aFormatArray[0] == 2)
	{
		$aTypeArray[] = 'yy';
		$regExpStr = '([0-9]{0,4})';
	}
	if ($aFormatArray[0] == 1)
	{
		$aTypeArray[] = 'y';
		$regExpStr = '([0-9]{0,4})';
	}
	if ($aFormatArray[1] == 4)
	{
		$aTypeArray[] = 'MMMM';
		$regExpStr = $MONTH_REGEXP;
	}
	if ($aFormatArray[1] == 3)
	{
		$aTypeArray[] = 'MMM';
		$regExpStr = $MONTH_REGEXP;
	}
	if ($aFormatArray[1] == 2)
	{
		$aTypeArray[] = 'MM';
		$regExpStr = $MONTH_REGEXP;
	}
	if ($aFormatArray[1] == 1)
	{
		$aTypeArray[] = 'M';
		$regExpStr = $MONTH_REGEXP;
	}
	if ($aFormatArray[2] == 2)
	{
		$aTypeArray[] = 'dd';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[2] == 1)
	{
		$aTypeArray[] = 'd';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[3] == 2)
	{
		$aTypeArray[] = 'hh';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[3] == 1)
	{
		$aTypeArray[] = 'h';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[4] == 2)
	{
		$aTypeArray[] = 'mm';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[4] == 1)
	{
		$aTypeArray[] = 'm';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[5] == 2)
	{
		$aTypeArray[] = 'ss';
		$regExpStr = '([0-9]{0,2})';
	}
	if ($aFormatArray[5] == 1)
	{
		$aTypeArray[] = 's';
		$regExpStr = '([0-9]{0,2})';
	}
	for ($i = 0; $i < 6; $i++)
	{
		$aFormatArray[$i] = 0;
	}
	return $regExpStr;
}

function unformatDateTimeRegExpStr($aFormat, &$aTypeArray)
{
	if (is_null($aTypeArray))
	{
		$aTypeArray = array();
	}
 	$regExpStr = '';
 	$formatArray = array(null, null, null, null, null, null);
 	for ($i = 0; $i < 6; $i++)
	{
		$formatArray[$i] = 0;
	}
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aFormat);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aFormat[$i];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
			else
			{
				$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
				$regExpStr .= $ch.'?';
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
						$regExpStr .= $ch.'?';
					}
					else
					{
						if ($ch == '\\')
						{
							$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
							$i++;
							if ($i < $n)
							{
								$ch = $aFormat[$i];
								switch ($ch)
								{
									case '\\':
										$regExpStr .= '\\\\?';
										break;
									case '^':
									case '$':
									case '*':
									case '+':
									case '?':
									case '{':
									case '}':
									case '!':
									case '(':
									case ')':
									case '[':
									case ']':
									case '.':
									    $regExpStr .= '\\'.$ch.'?';
										break;
									default:
										if (($ch >= '0' && $ch <= '9'))
										{
										    $regExpStr .= $ch;
										}
										else
										{
										    $regExpStr .= $ch.'?';
										}
										break;
								}
							}
						}
						else
						{
							switch ($ch)
							{
								case '^':
								case '$':
								case '*':
								case '+':
								case '?':
								case '{':
								case '}':
								case '!':
								case '(':
								case ')':
								case '[':
								case ']':
								case '.':
									$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									$regExpStr .= '\\'.$ch.'?';
									break;
								case 'y':
									if ($formatArray[0] == 4)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[0]++;
									break;
								case 'M':
									if ($formatArray[1] == 4)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[1]++;
									break;
								case 'd':
									if ($formatArray[2] == 2)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[2]++;
									break;
								case 'h':
									if ($formatArray[3] == 2)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[3]++;
									break;
								case 'm':
									if ($formatArray[4] == 2)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[4]++;
									break;
								case 's':
									if ($formatArray[5] == 2)
									{
										$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									}
									$formatArray[5]++;
									break;
								default:
									$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
									if (($ch >= '0' && $ch <= '9'))
									{
									    $regExpStr .= $ch;
									}
									else
									{
									    if($ch=="/")
									    {
									    	$ch="\/";
									    }
										$regExpStr .= $ch.'?';
									}
									break;
							}
						}
					}
				}
			}
		}
	}
	$regExpStr .= unformatDateTimeRegExpStrPart($formatArray, $aTypeArray);
	return $regExpStr;
}

function privateUnformatDateTime($aDateTime, $aFormat)
{
	if ($aFormat == '')
	{
		$aFormat = 'yyyy-MM-dd hh:mm:ss';
	}
	$aDateTime = (string) $aDateTime;
 	$typeArray = array();
 	$regExpStr = unformatDateTimeRegExpStr($aFormat, $typeArray);
	$matchArray = array();
	if (!preg_match('/'.$regExpStr.'/i', $aDateTime, $matchArray))
 	{
	 	return array('0000', '00', '00', '00', '00', '00');
 	}
 	if (strlen($matchArray[0]) == 0)
 	{
	 	return array('0000', '00', '00', '00', '00', '00');
 	}
	$year = null;
	$month = null;
	$day = null;
	$hours = null;
	$minutes = null;
	$seconds = null;
	$n = count($matchArray);
	for ($i = 1; $i < $n; $i++)
	{
		switch ($typeArray[$i - 1])
 	    {
 	    	case 'yyyy':
			case 'yy':
			case 'y':
 	    	    if (is_null($year))
 	    		{
					$year = $matchArray[$i];
					if ($year == '' || is_nan((double) $year))
					{
						$year = null;
					}
					else
					{
						if ($year < 1)
						{
							$year = null;
						}
						else
						{
							if ($year < 100)
							{
								$today = getdate();
								$year = floor(($today['year'] / 100) * 100 + ((int) $year));
							}
						}
					}
				}
				break;
			case 'MMMM':
			case 'MMM':
 	    	case 'MM':
 	    	case 'M':
 	    	   	if (is_null($month))
 	    		{
					$month = $matchArray[$i];
					$month = monthFromName($month);
					$month = monthFromAbbr($month);
					if ($month == '' || is_nan((double) $month))
					{
						$month = null;
					}
					else
					{
						if ($month < 1 || $month > 12)
						{
							$month = null;
						}
					}
				}
				break;
 	    	case 'hh':
 	    	case 'h':
 	    	    if (is_null($hours))
 	    		{
					$hours = $matchArray[$i];
					if ($hours == '' || is_nan((double) $hours))
					{
						$hours = null;
					}
					else
					{
						if ($hours > 23)
						{
							$hours = null;
						}
					}
				}
				break;
 	    	case 'mm':
			case 'm':
 	    	    if (is_null($minutes))
 	    		{
					$minutes = $matchArray[$i];
					if ($minutes == '' || is_nan((double) $minutes))
					{
						$minutes = null;
					}
					else
					{
						if ($minutes > 59)
						{
							$minutes = null;
						}
					}
				}
				break;
 	    	case 'ss':
 	    	case 's':
 	    	    if (is_null($seconds))
 	    		{
					$seconds = $matchArray[$i];
					if ($seconds == '' || is_nan((double) $seconds))
					{
						$seconds = null;
					}
					else
					{
						if ($seconds > 59)
						{
							$seconds = null;
						}
					}
				}
				break;
			default:
				break;
		}
	}
	$n = count($matchArray);
 	for ($i = 1; $i < $n; $i++)
 	{
 	    if ($typeArray[$i - 1] == 'dd' || $typeArray[$i - 1] == 'd')
 	    {
 	    	if (is_null($day))
 	    	{
				$day = $matchArray[$i];
				if ($day == '' || is_nan($day))
				{
					$day = null;
				}
				else
	  			{
	 				if ($day < 1)
	 				{
	 					$day = null;
	 				}
	 				else
	 				{
	 					if (is_null($month))
	 					{
	 						if ($day > 31)
	 						{
	 							$day = null;
	 						}
	 					}
		 				else
		 				{
							if ($month == 2)
							{
								if ((($year % 4 == 0) && ($year % 100 != 0)) || ($year % 400 == 0))
								{
									if ($day > 29)
									{
										$day = 29;
									}
								}
								else
								{
									if ($day > 28)
									{
										$day = 28;
									}
								}
							}
							else
							{
								if (($month == 4) || ($month == 6) ||
									($month == 9) || ($month == 11))
								{
									if ($day > 30)
									{
										$day = 30;
									}
								}
								else
								{
									if ($day > 31)
									{
										$day = 31;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	if (is_null($year))
	{
		$year = '0000';
		$month = '00';
		$day = '00';
	}
	else
	{
		$year = '0000'.((int) $year);
		$year = substr($year, strlen($year) - 4);
		if (is_null($month))
		{
			$month = '01';
			$day = '01';
		}
		else
		{
			if ($month < 10)
			{
				$month = '0'.((int) $month);
			}
			if (is_null($day))
			{
				$day = '01';
			}
			else
			{
				if ($day < 10)
				{
					$day = '0'.((int) $day);
				}
			}
		}
	}
	if (is_null($hours))
	{
		$hours = '00';
		$minutes = '00';
		$seconds = '00';
	}
	else
	{
		if ($hours < 10)
		{
			$hours = '0'.((int) $hours);
		}
		if (is_null($minutes))
		{
			$minutes = '00';
			$seconds = '00';
		}
		else
		{
			if ($minutes < 10)
			{
				$minutes = '0'.((int) $minutes);
			}
			if (is_null($seconds))
			{
				$seconds = '00';
			}
			else
			{
				if ($seconds < 10)
				{
					$seconds = '0'.((int) $seconds);
				}
			}
		}
	}
 	return array($year, $month, $day, $hours, $minutes, $seconds);
}

function unformatDateTime($aDateTime, $aFormat, $aLocaleID = null)
{
	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}
	$dateTimeArray = privateUnformatDateTime($aDateTime, $aFormat);
	if ($restoreLocale)
	{
		RestoreLocale();
	}
	return $dateTimeArray[0].'-'.$dateTimeArray[1].'-'.$dateTimeArray[2].' '.$dateTimeArray[3].':'.$dateTimeArray[4].':'.$dateTimeArray[5];
}

function unformatDate($aDateTime, $aFormat, $aLocaleID = null)
{
	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}

	$dateTimeArray = privateUnformatDateTime($aDateTime, $aFormat);

	if ($restoreLocale)
	{
		RestoreLocale();
	}
	return $dateTimeArray[0].'-'.$dateTimeArray[1].'-'.$dateTimeArray[2];
}

function formatDateTimePart($dateTimeArray, &$formatArray)
{
	global $MONTH_NAMES, $MONTH_ABBR;
	global $DAY_NAMES, $DAY_ABBR;
	$formatedDateTimePart = '';
 	if ($formatArray[0] == 4)
	{
		$formatedDateTimePart = $dateTimeArray[0];
	}
	if ($formatArray[0] == 3)
	{
		$formatedDateTimePart = substr($dateTimeArray[0], 2).$dateTimeArray[0];
	}
	if ($formatArray[0] == 2)
	{
		$formatedDateTimePart = substr($dateTimeArray[0], 2);
	}
	if ($formatArray[0] == 1)
	{
		$aDate = make_date($dateTimeArray[0], $dateTimeArray[1], $dateTimeArray[2], 0, 0, 0);
		//@JAPR 2008-11-20: Corregido el formato "y", le faltaba sumar un día para que fuera idéntico al comportamiento en VB
		$formatedDateTimePart = (string) (yday($aDate) + 1);
	}
	if ($formatArray[1] == 4)
	{
		if ($dateTimeArray[1] > 0)
		{
			$formatedDateTimePart = $MONTH_NAMES[((int) $dateTimeArray[1])];
		}
		else
		{
			$formatedDateTimePart = $dateTimeArray[1];
		}
	}
	if ($formatArray[1] == 3)
	{
		if ($dateTimeArray[1] > 0)
		{
			$formatedDateTimePart = $MONTH_ABBR[((int) $dateTimeArray[1])];
		}
		else
		{
			$formatedDateTimePart = $dateTimeArray[1];
		}
	}
	if ($formatArray[1] == 2)
	{
		$formatedDateTimePart = $dateTimeArray[1];
	}
	if ($formatArray[1] == 1)
	{
		$formatedDateTimePart = (string) ((int) $dateTimeArray[1]);
	}
	if ($formatArray[2] == 4)
	{
		if ($dateTimeArray[1] > 0 && $dateTimeArray[2] > 0)
		{
			$aDate = make_date($dateTimeArray[0], $dateTimeArray[1], $dateTimeArray[2], 0, 0, 0);
			$formatedDateTimePart = $DAY_NAMES[wday($aDate)];
		}
		else
		{
			$formatedDateTimePart = $dateTimeArray[2];
		}
	}
	if ($formatArray[2] == 3)
	{
		if ($dateTimeArray[1] > 0 && $dateTimeArray[2] > 0)
		{
			$aDate = make_date($dateTimeArray[0], $dateTimeArray[1], $dateTimeArray[2], 0, 0, 0);
			$formatedDateTimePart = $DAY_ABBR[wday($aDate)];
		}
		else
		{
			$formatedDateTimePart = $dateTimeArray[1];
		}
	}
	if ($formatArray[2] == 2)
	{
		$formatedDateTimePart = $dateTimeArray[2];
	}
	if ($formatArray[2] == 1)
	{
		$formatedDateTimePart = (string) ((int) $dateTimeArray[2]);
	}
	if ($formatArray[3] == 2)
	{
		$formatedDateTimePart = $dateTimeArray[3];
	}
	if ($formatArray[3] == 1)
	{
		$formatedDateTimePart = (string) ((int) $dateTimeArray[3]);
	}
	if ($formatArray[4] == 2)
	{
		$formatedDateTimePart = $dateTimeArray[4];
	}
	if ($formatArray[4] == 1)
	{
		$formatedDateTimePart = (string) ((int) $dateTimeArray[4]);
	}
	if ($formatArray[5] == 2)
	{
		$formatedDateTimePart = $dateTimeArray[5];
	}
	if ($formatArray[5] == 1)
	{
		$formatedDateTimePart = (string) ((int) $dateTimeArray[5]);
	}
	if ($formatArray[6] == 1)
	{
		$formatedDateTimePart = (string) (((int) (($dateTimeArray[1] - 1) / 3)) + 1);
	}
	if ($formatArray[7] == 2)
	{
		$aFirstDate = make_date($dateTimeArray[0], 1, 1, 0, 0, 0);
		$aFirstDate = days_add($aFirstDate, 7 - wday($aFirstDate));
		$aDate = make_date($dateTimeArray[0], $dateTimeArray[1], $dateTimeArray[2], 0, 0, 0);
		$days = days_diff($aFirstDate, $aDate);
		if ($days < 0)
		{
			$weeks = 1;
		}
		else
		{
			$weeks = (int) ($days / 7) + 2;
		}
		$formatedDateTimePart = (string) $weeks;
	}
	if ($formatArray[7] == 1)
	{
		$aDate = make_date($dateTimeArray[0], $dateTimeArray[1], $dateTimeArray[2], 0, 0, 0);
		$formatedDateTimePart = (string) (wday($aDate) + 1);
	}
	for ($i = 0; $i < 8; $i++)
	{
		$formatArray[$i] = 0;
	}
	return $formatedDateTimePart;
}

function formatDateTime($aDateTime, $aFormat, $aLocaleID = null)
{
	$restoreLocale = false;
	if (!is_null($aLocaleID))
	{
		global $LOCALE_ID;
		if (is_null($LOCALE_ID) || $LOCALE_ID != $aLocaleID)
		{
			SaveLocale();
			InitializeLocale($aLocaleID);
			$restoreLocale = true;
		}
	}

	$dateTimeArray = privateUnformatDateTime($aDateTime, '');

  	if ($aFormat == '')
	{
		if ($restoreLocale)
		{
			RestoreLocale();
		}
		return $dateTimeArray[0].'-'.$dateTimeArray[1].'-'.$dateTimeArray[2].' '.$dateTimeArray[3].':'.$dateTimeArray[4].':'.$dateTimeArray[5];
	}
	$formatedDateTime = '';
  	$formatArray = array(null, null, null, null, null, null, null, null);
 	for ($i = 0; $i < 8; $i++)
	{
		$formatArray[$i] = 0;
	}
	$inDate = true;
	$lastCh = '';
	$inQuotes = false;
	$inBrackets = false;
	$n = strlen($aFormat);
	for ($i = 0; $i < $n; $i++)
	{
		$ch = $aFormat[$i];
		if ($inBrackets)
		{
			if ($ch == ']')
			{
				$inBrackets = false;
			}
			else
			{
				$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
				$formatedDateTime .= $ch;
			}
		}
		else
		{
			if ($ch == '[' && !$inQuotes)
			{
				$inBrackets = true;
			}
			else
			{
				if ($ch == '"')
				{
					$inQuotes = $inQuotes ? false : true;
				}
				else
				{
					if ($inQuotes)
					{
						$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
						$formatedDateTime .= $ch;
					}
					else
					{
						if ($ch == '\\')
						{
							$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
							$i++;
							if ($i < $n)
							{
								$ch = $aFormat[i];
								$formatedDateTime .= $ch;
							}
						}
						else
						{
							switch ($ch)
							{
								case 'y':
								case 'Y':
									$inDate = true;
									if (($lastCh != '' && $lastCh != 'y' && $lastCh != 'Y') || $formatArray[0] == 4)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[0]++;
									break;
								case 'm':
								case 'M':
									if ($inDate)
									{
										if (($lastCh != '' && $lastCh != 'm' && $lastCh != 'M') || $formatArray[1] == 4)
										{
											$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
										}
										$formatArray[1]++;
									}
									else
									{
										if (($lastCh != '' && $lastCh != 'm' && $lastCh != 'M') || $formatArray[4] == 2)
										{
											$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
										}
										$formatArray[4]++;
									}
									break;
								case 'd':
								case 'D':
									$inDate = true;
									if (($lastCh != '' && $lastCh != 'd' && $lastCh != 'D') || $formatArray[2] == 4)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[2]++;
									break;
								case 'h':
								case 'H':
									$inDate = false;
									if (($lastCh != '' && $lastCh != 'h' && $lastCh != 'H') || $formatArray[3] == 2)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[3]++;
									break;
								case 's':
								case 'S':
									$inDate = false;
									if (($lastCh != '' && $lastCh != 's' && $lastCh != 'S') || $formatArray[5] == 2)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[5]++;
									break;
								case 'q':
								case 'Q':
									$inDate = true;
									if (($lastCh != '' && $lastCh != 'q' && $lastCh != 'Q') || $formatArray[6] == 1)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[6]++;
									break;
								case 'w':
								case 'W':
									$inDate = true;
									if (($lastCh != '' && $lastCh != 'w' && $lastCh != 'W') || $formatArray[7] == 2)
									{
										$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									}
									$formatArray[7]++;
									break;
								default:
									$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
									$formatedDateTime .= $ch;
									break;
							}
						}
					}
				}
			}
		}
		$lastCh = $ch;
	}
	$formatedDateTime .= formatDateTimePart($dateTimeArray, $formatArray);
	if ($restoreLocale)
	{
		RestoreLocale();
	}
	return $formatedDateTime;
}

function today()
{
	return date("Y-m-d H:i:s");
}

function year($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["year"];
}

function mon($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["mon"];
}

function month($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["month"];
}

function yday($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["yday"];
}

function mday($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["mday"];
}

function wday($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["wday"];
}

function weekday($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["weekday"];
}

function hours($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["hours"];
}

function minutes($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["minutes"];
}

function seconds($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return $anArray["seconds"];
}

function secondsSinceMidnight($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return ($anArray["hours"] * 60 + $anArray["minutes"]) * 60 + $anArray["seconds"];
}

function make_date($year, $month, $day, $hour, $minute, $second)
{
	$aDate = mktime($hour, $minute, $second, $month, $day, $year);
	return date("Y-m-d H:i:s", $aDate);
}

function getStrDateByInt($aDateInt)
{
	$anArray = getdate($aDateInt);
	
	if((int)$anArray["mon"]<10)
	{
		$month='0'.$anArray["mon"];
	}
	else
	{
		$month=$anArray["mon"];
	}
	
	if((int)$anArray["mday"]<10)
	{
		$day='0'.$anArray["mday"];
	}
	else
	{
		$day=$anArray["mday"];
	}

	return $anArray["year"]."-".$month."-".$day." 00:00:00";
}

function date_part($aString)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	return date("Y-m-d H:i:s", mktime(0, 0, 0, $anArray["mon"], $anArray["mday"], $anArray["year"]));
}

function days_add($aString, $aNumber)
{
	$oldDate = strtotime($aString);
	$newDate = $oldDate + $aNumber * (24 * 60 * 60);
	$newDate += (date('I', $oldDate) - date('I', $newDate)) * 60 * 60;
	return date("Y-m-d H:i:s", $newDate);
}

function days_diff($aString1, $aString2)
{
	$aDate1 = strtotime($aString1);
	$aDate2 = strtotime($aString2);
	$aDate2 += (date('I', $aDate2) - date('I', $aDate1)) * 60 * 60;
	return ($aDate2 - $aDate1) / (24 * 60 * 60);
}

function months_add($aString, $aNumber)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	$totalMonths = $anArray["year"] * 12 + ($anArray["mon"] - 1) + $aNumber;
	$newYear = (int) ($totalMonths / 12);
	$newMonth = $totalMonths - ($newYear * 12) + 1;
	$newDate = mktime($anArray["hours"], $anArray["minutes"], $anArray["seconds"], $newMonth, $anArray["mday"], $newYear);
	return date("Y-m-d H:i:s", $newDate);
}

function years_add($aString, $aNumber)
{
	$aDate = strtotime($aString);
	$anArray = getdate($aDate);
	$newDate = mktime($anArray["hours"], $anArray["minutes"], $anArray["seconds"], $anArray["mon"], $anArray["mday"], $anArray["year"] + $aNumber);
	return date("Y-m-d H:i:s", $newDate);
}

function weeks_add($aString, $aNumber)
{
	$oldDate = strtotime($aString);
	$newDate = $oldDate + ($aNumber * 7) * (24 * 60 * 60);
	$newDate += (date('I', $oldDate) - date('I', $newDate)) * 60 * 60;
	return date("Y-m-d H:i:s", $newDate);
}

function numDaysOfMonth($year, $month)
{
	$numDays=0;
	
	$monthsWith30=array(4,6,9,11);
	$monthsWith31=array(1,3,5,7,8,10,12);
	
	if(in_array($month,$monthsWith30))
	{
		$numDays=30;	
	}
	else
	{
		if(in_array($month,$monthsWith31))
		{
			$numDays=31;	
		}
		else
		{
			if($month==2)
			{
				$leapYear=((($year % 4) == 0) && ((($year % 100) != 0) || (($year % 400) == 0)));
				if($leapYear)
				{
					$numDays=29;
				}
				else
				{
					$numDays=28;
				}
			}
		}
	}
	
	return $numDays;
}

function getDateByWeekAndDay($intYear, $intMonth, $intWeek, $intWeekDay)
{
	$firstDay=$intYear.'-'.$intMonth.'-01';
	$aDate = strtotime($firstDay);
	$anArray = getdate($aDate);
	if($anArray["wday"]<=$intWeekDay)
	{
		$daysDiff = $intWeekDay-$anArray["wday"];
		$date=days_add($firstDay, $daysDiff);
	}
	else
	{
		$auxNumDays= 6 - $anArray["wday"];
		$daysDiff = $intWeekDay + $auxNumDays + 1;
		$date=days_add($firstDay, $daysDiff);
	}
	
	if($intWeek <= 1)
	{
		return $date;
	}
	else
	{	//$intWeek = 5 (opción: última semana)
		if($intWeek==5)
		{
			//$month: mes del que se quiere obtener la última semana
			$month=mon($date);
			$auxMonth=$month;
			$lastWeek = false;
			$auxDate = $date;
			while(!$lastWeek)
			{
				$auxDate=days_add($auxDate, 7);
				$auxMonth=mon($auxDate);
				if($month==$auxMonth)
				{
					$date = $auxDate;
				}
				else
				{
					$lastWeek = true;
				}
			}
		}
		else
		{
			//Se recorre la fecha hasta el número de semana solicitado
			for($i=1; $i<$intWeek; $i++)		
			{
				$date=days_add($date, 7);
			}
		}
	}
	
	return $date;
}

//Regresa un arreglo de fechas que correspondan al año, mes y día de la semana
// $intWeekDay= 0 (Domingo), 1 (Lunes) ... 6 (Sábado)
function getDatesByYearMonthDay($intYear, $intMonth, $intWeekDay)
{
	$resultDates = array();
	
	$date=$intYear.'-'.(($intMonth<10)?('0'.$intMonth):$intMonth).'-01';
	
	$numDaysOfMonth = numDaysOfMonth($intYear, $intMonth);
	
	for($i=0; $i<$numDaysOfMonth; $i++)
	{
		$intDate = strtotime($date);
		$anArray = getdate($intDate);
		if($anArray["wday"]==$intWeekDay)
		{
			$resultDates[]=$date;
		}
		$date=days_add($date, 1);
	}
	
	return $resultDates;
}

function getDateIdxWithYMD($arrayDates,$year=null,$month=null,$day=null)
{
	$arrayIdx=array();
	
	if($arrayDates==null || count($arrayDates)==0)
	{
		return $arrayIdx;
	}
	
	$regularExpresion="/";
	if($year!=null)
	{
		$regularExpresion.=$year."\-";	
	}
	else
	{
		$regularExpresion.="\d\d\d\d"."\-";
	}
	if($month!=null)
	{
		$regularExpresion.=$month."\-";	
	}
	else
	{
		$regularExpresion.="\d\d\-";
	}
	if($day!=null)
	{
		$regularExpresion.=$day."/";	
	}
	else
	{
		$regularExpresion.="\d\d/";
	}
	
	$numDates=count($arrayDates);
	for($i=0;$i<$numDates;$i++)
	{
		$elements=array();
		$numElements=preg_match_all($regularExpresion,$arrayDates[$i],$elements); 
		if($numElements>0)
		{
			$arrayIdx[]=$i;
		}
	}
	
	return $arrayIdx;
}

function GetNewArray($numRows, $numColumns, $initialValue)
{	
	$newArray=null;
	$newArray=BITAMGlobalInstance::GetNewEmptyArray($numRows,$numColumns,$initialValue);
	if(!is_null($newArray))
	{
		return $newArray;
	}
	$newArray=array();
	
	if($numColumns>0)
	{
		for ($i=0;$i<$numRows;$i++)
		{
			for ($j=0;$j<$numColumns;$j++)
			{
				$newArray[$i][$j]=$initialValue;
			}	
		}
	}
	else
	{
		for ($i=0;$i<$numRows;$i++)
		{
			$newArray[$i]=$initialValue;
				
		}
	}
	BITAMGlobalInstance::AddNewEmptyArray($numRows,$numColumns,$initialValue,$newArray);
	return $newArray;
}

function updateQueriesLogFile($queriesLogFile,$aLogString)
{           
	global $generateLog;
	
	if($generateLog===true || $_SESSION["PAgenerateLog"]==true)
	{
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log($aLogString, 3, GeteFormsLogPath().$queriesLogFile);
		//@JAPR
	}
}

//@JAPR 2013-06-19: Agregado el agente de eForms
//Si se activa el parámetro $bAddCaller, generalmente significaría que se trata de una llamada de error ya que requiere identificar exactamente
//desde donde se originó, así que en ese caso sobreescribe a la variable que forza a grabar el archivo
function logString($aString, $sFileName = "", $bAddDate = true, $bAddCaller = false, $bAddEcho = true) {
	global $strDefaultLogStringFile;
	global $blnSaveLogStringFile;
	
	$blnLocalSaveLogStringFile = false;
	if (isset($blnSaveLogStringFile)) {
		$blnLocalSaveLogStringFile = (bool) @$blnSaveLogStringFile;
	}
	
	if (trim($sFileName) == '') {
		if (((string) @$strDefaultLogStringFile) != '') {
			$sFileName = $strDefaultLogStringFile;
		}
	}
	
	//Si no hay nombre de archivo entonces utiliza el default. Si hay nombre pero no contiene un path entonces usa el path default
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$sFilePath = GeteFormsLogPath();
	//@JAPR
	if (trim($sFileName) == '') {
		$sFileName = $sFilePath."eFormsAgent.log";
	}
	elseif (strpos($sFileName, "\\") === false) {
		$sFileName = $sFilePath.$sFileName;
	}
	
	$strCaller = '';
	if ($bAddCaller) {
		$trace = @debug_backtrace();
		$caller = @$trace[1];
		if (!is_null($caller)) {
			$strClass = (string) @$caller['class'];
			if (isset($caller['function'])) {
				$strCaller = '('.(($strClass != '')?"$strClass".(string) @$caller['type']:'').$caller['function'].') ';
			}
		}
		
		$blnLocalSaveLogStringFile = true;
	}
	
	if ($bAddDate) {
		$strText = date("Y-m-d H:i:s").'- ';
	}
	else {
		$strText = '';
	}
	
	$strText .= $strCaller.$aString;
	if ($bAddEcho) {
		echo("<br>\r\n{$strText}");
	}
	
	if ($blnLocalSaveLogStringFile) {
		@error_log("\r\n".str_ireplace("<br>", '', $strText), 3, $sFileName);
	}
	
	if (getParamValue('DebugLog', 'both', '(string)', true)) {
		//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//Integradas las funciones comunes para almacenamiento de log de eForms
		//Este archivo se debe estandarizar a la ruta del path como todos los demás
		$sFileName = $sFilePath.getParamValue('DebugLog', 'both', '(string)', true);
		//@JAPR
		@error_log("\r\n".str_ireplace("<br>", '', $strText), 3, $sFileName);
	}
}
//@JAPR

function reportError($aErrCode, $aErrDesc, $bAddErrNum=true, $bCloseWindow=false, $sPostCode='', $bDieOnError=true, $bShowAlert=true)
{
	global $aMSExcelApplication;
	global $aMSExcelFileGlob;
	
	$strFullErrDesc = $aErrDesc;
	if ($bAddErrNum)
	{
		$strFullErrDesc = translate('Error').' #'.$aErrCode.': '.$aErrDesc;
	}
	$strFullErrDesc = str_ireplace('</b>', ' ', $strFullErrDesc);
	$strFullErrDesc = str_ireplace('<b>', '', $strFullErrDesc);
	$strFullErrDesc = str_ireplace(array('<br>', '<br />', '<br/>'), "\r\n", $strFullErrDesc);
	
	if ($bShowAlert)
	{
	?>
	<SCRIPT language="JavaScript">
	alert ('<?=addcslashes($strFullErrDesc, "\0..\37\\\"\'\177..\377")?>');
	</SCRIPT>
	<?
	}
	
	if (isset($aMSExcelApplication) && !is_null($aMSExcelApplication))
	{
		$aMSExcelApplication->Quit;
		$aMSExcelApplication = null;
		unlink($aMSExcelFileGlob);
	}
	
	if ($bCloseWindow)
	{
		//if (!$bDieOnError)
		//{
	?>
	<SCRIPT language="JavaScript">
	returnValue = '<error>';
	top.close();
	</SCRIPT>
	<?
		//}
	}
	else
	{
		echo $sPostCode;
	}
	
	if ($bDieOnError)
	{
		die();
	}
}

function getMultiples($number,$percentage)
{
	$arrayResult=array();
	
	$result=$number/(100/$percentage);
	$key=$result;
	$percTemp=0;
	$count=1;
	do
	{	
		$percTemp=$count*$percentage;
		$arrayResult[$key]=$percTemp;
		$key=$key+$result;
		$count++;
	}while ($key<=$number);
	
	return ($arrayResult);
}

/*======================================================= EAT */

function BITAMGetSiguiente(&$sCadena, $Token = ',')
{
	$npos = 0;
	$npos = strpos($sCadena, $Token);
	if ($npos !== false)
	{
		$stmp = substr($sCadena, 0, $npos);
		$sCadena = substr($sCadena, $npos + strlen($Token));
		return $stmp;
	}
	else
	{
        if (strlen(trim($sCadena)) > 0)
        {
        	$tmpCadena = $sCadena;
        	$sCadena = '';
            return $tmpCadena;
        }
		else
		{
            return '';
		}
	}
}

function BITAMReemplazaComillas($sCadena,
                                $bPonComillas = false)
{
	$npos = 0;
	$sTmp = '';
	do
	{
    	$npos = strpos($sCadena, "'");
    	if ($npos !== false ) 
    	{
            $sTmp = $sTmp.substr($sCadena, 0, $npos + 1)."'";
            $sCadena = substr($sCadena, $npos + 1);
    	}
    	else
    	{
            $sTmp = $sTmp.$sCadena;
            break;
    	}
	} while (true);
// @EA 2005-04-21 Agregamos el parametro opcional bPonComillas
    return $bPonComillas ? "'".$sTmp."'" : $sTmp;
}

function BITAMPrintRst(&$pRst, &$pCubeRolap, $nPosDate)
{
	if (!is_null($pRst))
	{
		$nfieldscount = 0;
		if (!$pRst->EOF)
			$nfieldscount = count($pRst->fields);
		while (!$pRst->EOF)
		{
			if (0 == $nPosDate)
			{
				if (array_key_exists($pRst->fields[0], $pCubeRolap->periods_date))
				{
					print "->'".$pCubeRolap->periods_date[$pRst->fields[0]]."'";							
				}
				else
					print '->'.$pRst->fields[0];
			}
			else
				print '<BR>'.$pRst->fields[0];				
			for ( $nfield = 1; $nfield < $nfieldscount; $nfield++ )
			{
				if ($nfield == $nPosDate)
				{
					if (array_key_exists($pRst->fields[$nfield], $pCubeRolap->periods_date))	
					{
						print "->'".$pCubeRolap->periods_date[$pRst->fields[$nfield]]."'";				
					}
					else
						print '->'.$pRst->fields[$nfield];				
					
				}
				else
					print '->'.$pRst->fields[$nfield];				
			}
			$pRst->MoveNext();
		}
	}
}

function BITAMGetDateRelativeToPeriod($pComPeriods, $OrigPeriod, $OrigDate )
{
	$intRelativeToPerioEntry = -1;
	$dtRelativePerInitialDate = '';
    
	$intRelativeToPerioEntry = $pComPeriods[$OrigPeriod]->relative_to;
    
	if ($intRelativeToPerioEntry == -1) 
	    return $OrigDate;

// Determinar la fecha de inicio del período relativo, según la fecha original
    $dtRelativePerInitialDate = $pComPeriods[$intRelativeToPerioEntry]->GetInitialDate($OrigDate);
    
    return $pComPeriods[$OrigPeriod]->GetInitialDate($dtRelativePerInitialDate);
    
}

function BITAMArrayPrint( &$aRst )
{
	$nrows = 0;
	$nrow = 0;
	$ncols = 0;
	$ncol = 0;
	
	$nrows = count($aRst);
	$ncols = count($aRst[0]);
	for ($nrow = 0; $nrow < $nrows; $nrow++)
	{
		print '<BR>';
		for ($ncol = 0; $ncol < $ncols; $ncol++)
		{
			print $aRst[$nrow][$ncol].'|';
		}
	}
}
	
/*Controla los errores producidos en el eval de las fórmulas (tanto de FormulaResult como de
FormulaEvaluator) para imprimir información adicional como el nombre del indicador Calculado
que se está resolviendo
*/
function formulaEvaluatorErrorHandler($num_err, $cadena_err, $archivo_err, $linea_err)
{
	global $aCurrentDimValue;
	global $aCurrentDate;
	global $aCurrentCalcIndName;
	
	switch ($num_err)
 	{
		default:
		//case E_ERROR:			//Error que debe detener la ejecución
		//case E_WARNING:		//Advertencia de Php, puede continuar
		//case E_NOTICE:		//Advertencia de Php, puede continuar
		//case E_USER_ERROR:	//Errores de usuario (no usados en Ektos), puede continuar
		//case E_USER_WARNING:	//Errores de usuario (no usados en Ektos), puede continuar
		//case E_USER_NOTICE:	//Errores de usuario (no usados en Ektos), puede continuar
			if (isset($aCurrentDate) && !is_null($aCurrentDate))
			{
				echo '<b>'.translate('Date').'</b>: '.$aCurrentDate.str_repeat(" ",4096)."\n<br>";
			}
			if (isset($aCurrentCalcIndName) && !is_null($aCurrentCalcIndName))
			{
				echo '<b>'.translate('Calculated indicator').'</b>: '.$aCurrentCalcIndName.str_repeat(" ",4096)."\n<br>";
			}
			echo "<b>Error #$num_err- $cadena_err in $archivo_err on line $linea_err".str_repeat(" ",4096)."</b>\n\n<br><br>";
			
			if ($num_err == E_ERROR)
			{
				die();
			}
			break;
	}
}

function GetADPwd($sUserName)
{
	$dDate = date('Ymd');
	$sPassword = formatDateTime($dDate, "YYYY").$sUserName.formatDateTime($dDate, "MMDD")."CKPWD";
	$sPassword =BITAMEncryptPassword($sPassword);
	$sPassword =BITAMEncode($sPassword);
	return $sPassword;
}

//@JAPR 2008-09-22: Agregado el método estándar para leer las configuraciones sobre el tamaño máximo de archivo a subir con PhP
/*Agregado este método para convertir las notaciones referentes a Bytes del PhP.ini (hasta la versión 5.1.0) a un valor numérico en bytes
El orden del switch no debe ser alterado o de lo contrario los resultados obtenidos no serán los correctos (está así que para que las entradas
superiores multipliquen incluyendo a las inferiores, de forma que el resultado finalmente es correcto)
*/
function convertShortHandBytesNotation($val)
{
    $val = trim($val);
    $last = strtolower($val{strlen($val)-1});
    switch($last) {
        // The 'G' modifier is available since PHP 5.1.0
        case 'g':
            $val *= 1024;
        case 'm':
            $val *= 1024;
        case 'k':
            $val *= 1024;
    }
	
    return $val;
}

/*Verifica si el PhP.ini tiene la configuración del upload_max_filesize y la regresa como bytes, si no la tiene utiliza entonces un default fijo
*/
function getMaxUploadFileSize()
{
	global $ekt_config;
	
	$strMaxFileSize = ini_get("upload_max_filesize");
	if (trim($strMaxFileSize) != '')
	{
		return convertShortHandBytesNotation($strMaxFileSize);
	}
	else 
	{
		return $ekt_config['upload_maxsize'];
	}
}

//@JAPR 2008-11-24: Validados los tipos de dato de dimensiones
//Aplica al valor especificado el formato requerido para la utilizarse como parte de un estatuto SQL
//El parámetro $bToRepository en True indica que se debe utilizar ADOConnection, de lo contrario será DataADOConnection
function formatValueForSQL($aRepository, $aValue, $aDataType = tINT, $bToRepository = false)
{
	switch ((int) $aDataType)
	{
		case tCHAR:
		case tVARCHAR:
			//Texto
			if ($bToRepository)
			{
				$aFormattedValue = $aRepository->ADOConnection->Quote($aValue);
			}
			else 
			{
				$aFormattedValue = $aRepository->DataADOConnection->Quote($aValue);
			}
			break;
		case tDATE:
		case tSMALLDATE:
			//Fechas
			if ($bToRepository)
			{
				$aFormattedValue = $aRepository->ADOConnection->DBTimeStamp($aValue);
			}
			else 
			{
				$aFormattedValue = $aRepository->DataADOConnection->DBTimeStamp($aValue);
			}
			break;
		default:
			//Números enteros o flotantes, o valores que se resuelvan como ellos (notaciones especiales que no requieren delimitadores)
			$aFormattedValue = $aValue;
			break;
	}
	return $aFormattedValue;
}
//@JAPR

function addMonthsToCurrentDate($currentDate, $numberMonths)
{
	if(is_null($currentDate) || trim($currentDate)=="")
	{
		$currentDate = date("Y-m-d");
	}
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $currentDate);
	
	//Year
	$yearDate = (int)$arrayDate[0];
	
	//Month
	$monthDate = (int)$arrayDate[1];

	//Day
	$dayDate = substr($arrayDate[2], 0, 2);
	
	$initialTime = "00:00:00";
	
	$amountYears = (int)($numberMonths/12);
	$newNumberMonths = (int)($numberMonths%12);
	
	$newYear = $yearDate + $amountYears;
	
	if($numberMonths>=0)
	{
		$factor = 1;
		
		if( ($monthDate + $newNumberMonths)>12 )
		{
			$newMonth = ( $monthDate + $newNumberMonths ) % 12;
			$newYear = $newYear + $factor;
		}
		else 
		{
			$newMonth = $monthDate + $newNumberMonths;
		}
	}
	else 
	{
		$factor = -1;
		
		if( ($monthDate + $newNumberMonths)<1 )
		{
			$difference = $monthDate + $newNumberMonths;
			$newMonth = 12 + $difference;
			$newYear = $newYear + $factor;
		}
		else 
		{
			$newMonth = $monthDate + $newNumberMonths;		
		}
	}
	
	$newDate = "".$newYear."-".($newMonth < 10 ? "0".$newMonth : $newMonth)."-".$dayDate." ".$initialTime;
	
	return $newDate;
}

function check_email_address($email) 
{
	// First, we check that there's one @ symbol, and that the lengths are right
	if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) 
	{
		// Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
		return false;
	}

	// Split it into sections to make life easier
	$email_array = explode("@", $email);
	$local_array = explode(".", $email_array[0]);
	for ($i = 0; $i < sizeof($local_array); $i++) 
	{
		if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) 
		{
			return false;
		}
	}  

	if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) 
	{ 
		// Check if domain is IP. If not, it should be valid domain name
		$domain_array = explode(".", $email_array[1]);
		if (sizeof($domain_array) < 2) 
		{
			return false; // Not enough parts to domain
		}
		for ($i = 0; $i < sizeof($domain_array); $i++) 
		{
			if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) 
			{
				return false;
			}
		}
	}

	return true;
}

function getWebVersion()
{
	$WebVersion = 6.00000;
	
	if (file_exists('../artus_version.php'))
	{
		$WebVersion = file_get_contents('../artus_version.php');
		$WebVersion = str_replace('<?php'.chr(13).chr(10).'echo "', '', $WebVersion);
		$WebVersion = str_replace('";'.chr(13).chr(10).'?>', '', $WebVersion);
		if (is_numeric($WebVersion)) 
		{
			$WebVersion = floatval($WebVersion);
		}
	}
	
	return $WebVersion;
}

function isEqualTheseAmounts($firstAmount, $secondAmount, $precision=6)
{
	$degreePrecision = ((float)1) / ((float)(pow(10,$precision)));
	
	$absDifference = (float)abs((float)$firstAmount - (float)$secondAmount);
	
	if((float)$absDifference < (float)$degreePrecision)
	{
		return true;
	}
	else 
	{
		return false;
	}
}

function getKPAProjectAndUser($kpiUser, $kpiPassword)
{
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	global $blnMultiPass;
	//@JAPR 2012-04-17: Agregada validación para cuando se invoca este método dentro de un require_once en otra función, en ese caso las variables
	//sólo son accesibles desde globales
	if (!isset($server))
	{
		global $server;
		global $server_user;
		global $server_pwd;
		global $masterdbname;
	}
	//@JAPR
	
	require_once('../../../fbm/conns.inc.php');

	if (strlen($kpiUser) == 0)
	{
		return 'Error, user email empty';
	}

	$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);

	if (!$mysql_Connection)
	{
		//return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
		//@JAPR 2015-02-25: No estaba estandarizado el texto de error
		return 'Error, Connection error! ';
	}

	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);

	if (!$db_selected)
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		//return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
		return 'Error, could not select '.$masterdbname;
	}

	//Obtener el UserID de la tabla de SAAS_Users
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
	$aSQL = sprintf("SELECT userID, Email, AccPassword, ActiveAcc FROM saas_users WHERE Email = '%s'", mysql_real_escape_string($kpiUser, $mysql_Connection));

	$result = mysql_query($aSQL, $mysql_Connection);

	if (!$result || mysql_num_rows($result) == 0)
	{
		//return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
		return 'Error, email or password incorrect';
	}

	//Obtener resultado de la consulta
	$row = mysql_fetch_assoc($result);
	$intUserID = $row["userID"];
	$strUserEmail = $row["Email"];
	$intUserActive = intval($row["ActiveAcc"]);
	$strEncryptedPass = $row["AccPassword"];
	
	//Verificar que la cuenta se encuentre activa
	if ($intUserActive == 2)
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		//return 'Error, the user account is not active'."\n".mysql_error();
		return 'Error, the user account is not active';
	}
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Primero obtiene la configuración del uso de múltiples passwords para saber si ya se encuentra o no habilitada
	$aSQL = "SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = -2 AND settingValue > 0";
	//Si se recibió un repositorio, primero identifica el DataBaseID correspondiente
	$strProjectName = (isset($_REQUEST["projectName"]) ? trim($_REQUEST["projectName"]) : "");
	if ($strProjectName) {
		$intDBID = 0;
		$sql = "SELECT c.DatabaseID 
			FROM saas_databases c 
			WHERE c.Repository = '{$strProjectName}'";
		$result = @mysql_query($sql, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$intDBID = (int) @$row["DatabaseID"];
		}
		
		if ($intDBID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = {$intDBID} AND settingUserID = -2 AND settingValue > 0";
		}
	}
	
	//Si se recibió el EMail (que todo request siempre lo debe recibir) primero identifica el UserID correspondiente
	if ($strUserEmail) {
		$intSAASUserID = $intUserID;	//En este caso ya se había consultado este dato arriba, así que se puede reutilizar
		/*
		$sql = "SELECT UserID 
			FROM saas_users 
			WHERE Email = '{$strUserEmail}'";
		$result = @mysql_query($sql, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$intSAASUserID = (int) @$row["UserID"];
		}
		*/
		if ($intSAASUserID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = {$intSAASUserID} AND settingValue > 0";
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Query to check Multi-Passwords enabled (getKPAProjectAndUser): {$aSQL}");
	}
	
	$result = @mysql_query($aSQL, $mysql_Connection);
	if ($result && @mysql_num_rows($result) > 0) {
		$row = @mysql_fetch_assoc($result);
		$blnMultiPass = (bool) @$row["settingValue"];
	}
	//@JAPR
	
	//Verificar que el password coincida
	$strUserPass = BITAMDecryptPassword($strEncryptedPass);
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	if ($kpiPassword !== $strUserPass && !$blnMultiPass)
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		//return 'Error, incorrect user or password'."\n".mysql_error();
		return "Error, incorrect user or password";
		//return "Error, incorrect user or password, kpiPassword == '{$kpiPassword}', strUserPass == '{$strUserPass}'";
	}
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
	//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
	//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
	$strDBIDWhere = '';
	if ($blnMultiPass) {
		//Si se recibió el AppName se le dará prioridad a ese parámetro para identificar al repositorio (Apps de versión 6.0+)
		$strAppName = (isset($_REQUEST["appName"]) ? trim($_REQUEST["appName"]) : "");
		if (strlen($strAppName) > 0)
		{
			$aSQL = <<<EOS
SELECT c.DatabaseID 
FROM saas_eforms_apps a, saas_users b, saas_databases c 
WHERE a.appname = '$strAppName' 
AND a.masterAccount = b.EMail 
AND b.userID = c.UserID
EOS;
			$restemp = mysql_query($aSQL, $mysql_Connection);
			if ($restemp && mysql_num_rows($restemp) > 0)
			{
				$rowtemp = mysql_fetch_assoc($restemp);
				$intDBID = intval($rowtemp["DatabaseID"]);
				if ($intDBID > 0)
				{
					$strDBIDWhere = " AND A.DatabaseID = $intDBID ";
				}
			}
		}
		else {
			//En caso de no recibir el AppName, se verificará si se recibió el ProjectName (Apps de 4.0+ hasta antes de 6.0, aunque 6.0+ sigue enviando ese parámetro)
			$strProjectName = (isset($_REQUEST["projectName"]) ? trim($_REQUEST["projectName"]) : "");
			if (strlen($strProjectName) > 0) {
				$strProjectName = strtolower($strProjectName);
				$aSQL = <<<EOS
SELECT c.DatabaseID 
FROM saas_databases c 
WHERE c.Repository = '$strProjectName'
EOS;
				$restemp = mysql_query($aSQL, $mysql_Connection);
				if ($restemp && mysql_num_rows($restemp) > 0)
				{
					$rowtemp = mysql_fetch_assoc($restemp);
					$intDBID = intval($rowtemp["DatabaseID"]);
					if ($intDBID > 0)
					{
						$strDBIDWhere = " AND A.DatabaseID = $intDBID ";
					}
				}
			}
		}
	}
	
	$blnValideFormsAcc = false;
	//Obtener el nombre del repositorio de KPA al cual tiene acceso
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	$intFormsDatabaseID = 0;
	$strRepository = '';
	$strServer = null;
	$aSQL = sprintf("SELECT A.DatabaseID, A.Repository, A.DBServer, A.DBServerUsr, A.DBserverPwd, A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.TemplateID, A.DataWarehouse 
					FROM saas_dbxusr B, saas_databases A 
					WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
					AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	// SBF_2016-11-14: si ya está activa la funcionalidad de contraseña por repositorio, pedir en el query la contraseña de saas_dbxusr, si es que existe
	if ($blnMultiPass)
	{
		$aSQL = sprintf("SELECT A.DatabaseID, A.Repository, A.DBServer, A.DBServerUsr, A.DBserverPwd, A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.TemplateID, A.DataWarehouse, COALESCE(B.AccPassword, C.AccPassword) AS PasswordEnc 
											FROM saas_dbxusr B, saas_databases A, saas_users C 
						WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
						AND B.UserID = C.UserID
						AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Query to get user project to process the request: {$aSQL}");
	}
	
	$result = mysql_query($aSQL, $mysql_Connection);
	if (!$result || mysql_num_rows($result) == 0)
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		return 'Error, no repository access found';
	}
	
	$isFoundRep = false;
	//if ($row = mysql_fetch_assoc($result)) 
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
	$row = mysql_fetch_assoc($result);
    do {
	    $intTemplateID = (int) @$row["TemplateID"];
		$strRepository = $row["Repository"];
		$strServer = $row["DBServer"];
		$strServer_User = $row["DBServerUsr"];
		$strServer_Pwd = $row["DBserverPwd"];
	    $strFormsPath = is_null($row["FormsPath"]) ? '' : $row["FormsPath"];
	    $strFormsVersion = is_null($row["FormsVersion"]) ? '' : $row["FormsVersion"];
		//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
		$intFormsDatabaseID = (int) @$row["DatabaseID"];
		//@JAPR
		
		if(is_null($strServer))
		{
			$strServer = $server;
		}
	    $blnValideFormsAcc = ($intTemplateID == 5 || $strFormsPath != '');
		//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
		//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
		//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
		// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
		if ($blnMultiPass)
		{
			$strUserPass = BITAMDecryptPassword($row["PasswordEnc"]);
			$blnValideFormsAcc = $blnValideFormsAcc && ($kpiPassword === $strUserPass);
		}
		$row = mysql_fetch_assoc($result);
	} while($row && !$blnValideFormsAcc);
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
	//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
	//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
	// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
	if ($blnMultiPass && !$blnValideFormsAcc)
	{
		return "Error, incorrect user or password";
	}
    //@JAPR
	
	if($strServer!=$server)
	{
		$strServer_Pwd = BITAMDecryptPassword($strServer_Pwd);
		$mysql_Connection = mysql_connect($strServer, $strServer_User, $strServer_Pwd);
	}

	if (!$mysql_Connection)
	{
		//return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
		//@JAPR 2015-02-25: No estaba estandarizado el texto de error
		return 'Error, Connection error! ';
	}

	/*
	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);

	if (!$db_selected)
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		//return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
		return 'Error, could not select '.$masterdbname;
	}
	*/

	//Verificar si el repositorio contiene tablas de eSurvey
	//Obtener el nombre corto de tabla de  usuarios de eSurvey
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
	$aSQL = sprintf("SELECT ArtusUser FROM $strRepository.SI_SV_Users WHERE Email = '%s'", mysql_real_escape_string($strUserEmail, $mysql_Connection));

	$tempResult = mysql_query($aSQL, $mysql_Connection);

	if ($tempResult && mysql_num_rows($tempResult) == 1)
	{
		$isFoundRep = true;
	}

	if($isFoundRep==true)
	{
		//Obtener el nombre corto de la cuenta de KPI
		//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
		$aSQL = sprintf("SELECT NOM_CORTO FROM $strRepository.SI_USUARIO WHERE CUENTA_CORREO = '%s'", mysql_real_escape_string($strUserEmail, $mysql_Connection));

		$result = mysql_query($aSQL, $mysql_Connection);

		if (!$result || mysql_num_rows($result) == 0)
		{
			//conchita 1-nov-2011 ocultar mensajes sql
			//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
			return translate("Either the user/password combination is incorrect or the account has been blocked");
			//@JAPR
		}
	}
	else
	{
		//conchita 1-nov-2011 ocultar mensajes sql
		//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
		return translate("Either the user/password combination is incorrect or the account has been blocked");
		//@JAPR
	}
	
	//Se asigna el nombre corto del usuario
	$row = mysql_fetch_assoc($result);
	$strUserNomCorto = $row["NOM_CORTO"];

	unset($row);
	unset($mysql_Connection);

	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
	return $strRepository.'_KPASep_'.$strUserNomCorto.'_KPASep_'.$intFormsDatabaseID.'_KPASep_'.$strServer;
}

function isSAASUserAdmin($saas_userid, $databaseid)
{
	$isAdmin = false;
	
	include('../../../fbm/conns.inc.php');

	$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);

	if (!$mysql_Connection)
	{
		return $isAdmin;
	}

	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);

	if (!$db_selected)
	{
		return $isAdmin;
	}

	//Obtener el UserID de la tabla de SAAS_Users
	$aSQL = "SELECT DatabaseID FROM saas_databases WHERE DatabaseID = ".$databaseid." AND UserID = ".$saas_userid;

	$result = mysql_query($aSQL, $mysql_Connection);

	if (!$result || mysql_num_rows($result) == 0)
	{
		return $isAdmin;
	}
	else 
	{
		unset($mysql_Connection);
		$isAdmin = true;
		return $isAdmin;
	}
}

/* Agregadas para compatibilidad con model manager */

if (!function_exists('InModeSAAS'))
{
	function InModeSAAS()
	{
		return true;
	}
}	

if (!function_exists('GetPathParentEA'))
{
	function GetPathParentEA()
{
	$dirlog = substr(ADODB_DIR, 0, strlen(ADODB_DIR)-6);
	
	for ($x=strlen($dirlog)-1; $x > 0; $x--)
	{
		if (substr($dirlog, $x, 1) == '/' || substr($dirlog, $x, 1) == '\\')
		{
			$dirlog = substr($dirlog, 0, $x+1);
			break;
		}
	}
	
//		$dirlog .= 'projects/';
	
	return $dirlog;
}
}

if (!function_exists('GetLocalURLWEBG6'))
{
function GetLocalURLWEBG6()
{
	$surl = GetPathParentEA();
	
	$surl = str_replace('\\', '/', $surl);
	
//	@savelogfile('$surl ->'.$surl);
			
	$npos = strpos($surl, 'wwwroot');
	
	$npos_separator = strpos($surl, '/', $npos);
	
	$surl = substr($surl, $npos_separator+1);
	
//	@savelogfile('$surl without wwwroot ->'.$surl);
	
	$surl = 'http://localhost/'.$surl;
	
	return $surl;
}
}

if (!function_exists('getNextTableKey'))
{
	//Regresa el siguiente índice disponible del campo/tabla especificados según los filtros opcionales aplicados
	//El parámetro $aDatabaseConn es realmente la conexión ADODB, no el objeto BITAMRepository, ya que este método se puede invocar
	//indiferentemente para la Metadata o el DWH dependiendo de las tablas especificadas como parámetro
	function getNextTableKey($aDatabaseConn, $sField, $sTableName, $sJoinsAndFilters = "")
	{
		$blnValid = false;
		
		if (is_null($aDatabaseConn))
		{
			return -1;
		}
		
		$intNewId = -1;
		$sql = "SELECT ".$aDatabaseConn->IfNull("MAX(".$sField.")", "0")." + 1 AS ".$sField." ".
			"FROM ".$sTableName;
		if (trim($sJoinsAndFilters) != '')
		{
			$sql .= " WHERE ".$sJoinsAndFilters;
		}
		
		$blnValid = @$aDatabaseConn->Execute($sql);
		if (is_null($blnValid)) $blnValid = false;
		if ($blnValid)
		{
			$aRS = $blnValid;
			$intNewId = (int)$aRS->fields[strtolower($sField)];
		}
		
		return $intNewId;
	}
}
//@JAPR

if (!function_exists('getArtusWebPath'))
{
	function getArtusWebPath()
	{
		return GetPathParentEA();
	}
}

if (!function_exists('savelogfile'))
{
	function savelogfile($sLog, $IsError = false)
	{
		//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//Integradas las funciones comunes para almacenamiento de log de eForms
		$dirlog = GeteFormsLogPath();
		//@JAPR
		$logFile = $dirlog.'Model'.session_id().'.log';
		
		$fecha = time();
		$fecha = date("h:i:s", $fecha); 
		
		@error_log("\r\n".$fecha.' '.$sLog, 3, $logFile);
		
		if ($IsError)
		{
			@session_start();
			$SAASWizard = 0;
			if (array_key_exists('SAASWizard', @$_SESSION))
				$SAASWizard = @$_SESSION['SAASWizard'];
				
			if ($SAASWizard == 1)
				setRequestError('SAASRI.800'.'.0', $sLog);
		}
	}
}

function cleanSpecialCharacters($text)
{
	$text = htmlentities($text, ENT_QUOTES, 'UTF-8');
	$text = strtolower($text);
	$patron = array (
		// Espacios, puntos y comas por guion
		'/[\., ]+/' => '-',

		// Vocales
		'/&agrave;/' => 'a',
		'/&egrave;/' => 'e',
		'/&igrave;/' => 'i',
		'/&ograve;/' => 'o',
		'/&ugrave;/' => 'u',

		'/&aacute;/' => 'a',
		'/&eacute;/' => 'e',
		'/&iacute;/' => 'i',
		'/&oacute;/' => 'o',
		'/&uacute;/' => 'u',

		'/&acirc;/' => 'a',
		'/&ecirc;/' => 'e',
		'/&icirc;/' => 'i',
		'/&ocirc;/' => 'o',
		'/&ucirc;/' => 'u',

		'/&atilde;/' => 'a',
		'/&etilde;/' => 'e',
		'/&itilde;/' => 'i',
		'/&otilde;/' => 'o',
		'/&utilde;/' => 'u',

		'/&auml;/' => 'a',
		'/&euml;/' => 'e',
		'/&iuml;/' => 'i',
		'/&ouml;/' => 'o',
		'/&uuml;/' => 'u',

		'/&auml;/' => 'a',
		'/&euml;/' => 'e',
		'/&iuml;/' => 'i',
		'/&ouml;/' => 'o',
		'/&uuml;/' => 'u',

		// Otras letras y caracteres especiales
		'/&aring;/' => 'a',
		'/&ntilde;/' => 'n',

		// Agregar aqui mas caracteres si es necesario

	);

	$text = preg_replace(array_keys($patron),array_values($patron),$text);
	return $text;
}

function PrintMultiArray(&$arrData, $aShift=0)
{
	if (is_null($arrData)) {
		return;
	}
	
	foreach ($arrData as $aKey => $aValue) 
	{
		if (is_object($aValue)) {
			$strData = str_repeat('&nbsp;',$aShift).'['.$aKey.'] => Object';
			echo "\r\n".'<br>'.$strData;
			//@JAPR 2015-08-28: Corregido un bug, no representaba a los elementos con índice numérico 0 porque se comparaba con != contra un String y resultaba true
			if ($aKey !== 'Repository') {
				PrintMultiArray($aValue, $aShift + 3);
			}
			else {
			}
		}
		else if (is_array($aValue)) {
			$strData = str_repeat('&nbsp;',$aShift).'['.$aKey.'] => Array';
			echo "\r\n".'<br>'.$strData;
			PrintMultiArray($aValue, $aShift + 3);
		}
		else {
			$strData = str_repeat('&nbsp;',$aShift).'['.$aKey.'] => '.((is_null($aValue))?'[NULL]':$aValue);
			echo "\r\n".'<br>'.$strData;
		}
	}
}

/* @JAPR 2012-03-16: Agregado un método que controla el buffer para evitar problemas de Warnings al usar la función 'date' a partir de la 
versión 5.3 de Php, ya que macaba el error:
Warning: date() [<a href='function.date'>function.date</a>]: It is not safe to rely on the system's timezone settings. You are *required* to 
use the date.timezone setting or the date_default_timezone_set() function. In case you used any of those methods and you are still getting 
this warning, you most likely misspelled the timezone identifier. We selected 'America/Chicago' for '-6.0/no DST'
*/
function dateFmtd($sFormat)
{
	ob_start();
	$strDateTime = date($sFormat);
	ob_end_clean();
	return $strDateTime;
}

//@JAPR 2012-06-05: Agregado el código para accesar a FBM000 y así obtener algunos datos que se quedarán como variables de Sesión
//Este código fué extraído de ProcessSurveyFunctions en el método que graba las estadísticas después de cada grabado de encuestas
//Originalmente usada para obtener el ID del usuario de SAAS correspondiente con la cuenta logeada desde el index.php, ya que esa información
//si está disponible cuando se logean desde un escenario de Artus Web, así que es la primer validación que se hace
function getSAASDataByEMail($aRepository)
{
	if (array_key_exists("saasuser", $_SESSION) && array_key_exists("dbid", $_SESSION))
	{
		return;
	}
	
	try
	{
	    //Para conectarse a fbm000
	    @require('../../../fbm/conns.inc.php');
	    if (!isset($server) || is_null($server) || trim($server) == '')
	    {
	    	return 'Unable to load saas repository connection data';
	    }
	    
	    $mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
		
	    if (!$mysql_Connection)
	    {
            //return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
            return 'Connection error! ';
	    }
		
	    $db_selected = mysql_select_db($masterdbname, $mysql_Connection);
	    if (!$db_selected)
	    {
            //conchita 1-nov-2011 ocultar mensajes sql
            //return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
            return 'Error, could not select '.$masterdbname;
	    }
		
		$intUserID = 0;
		if (array_key_exists("saasuser", $_SESSION))
		{
			$intUserID = (int) $_SESSION["saasuser"];
		}
		
		//Obtiene el UserId de SAAS para el usuario logeado
		if ($intUserID <= 0)
		{
		    //obtenemos el email del usuario
		    $sql = "SELECT CUENTA_CORREO FROM SI_USUARIO WHERE CLA_USUARIO = ".$_SESSION["PABITAM_UserID"];
		    $aRS = $aRepository->ADOConnection->Execute($sql);
		    if ($aRS === false)
		    {
		    	return translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		        //die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    if (!$aRS->EOF)
		    {
		        $userEmail = $aRS->fields["CUENTA_CORREO"];
		    }
		    
		    //Obtenemos los valores del userID en fbm000 de la tabla de saas_users
		    $sql = "SELECT UserID FROM SAAS_USERS WHERE Email = '".$userEmail."'";
		    $result = mysql_query($sql, $mysql_Connection);
		    if (!$result || mysql_num_rows($result) == 0)
		    {
	            //return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
	            return 'Error, email or password incorrect';
		    }
		    
		    $row = mysql_fetch_assoc($result);
		    $intUserID = (int) $row["UserID"];
		    if ($intUserID > 0)
		    {
		    	$_SESSION["saasuser"] = $intUserID;
		    }
		}
		
		$intDatabaseID = 0;
		if (array_key_exists("dbid", $_SESSION))
		{
			$intDatabaseID = (int) $_SESSION["dbid"];
		}
		
		//Obtiene el DatabaseID de SAAS para el usuario logeado (se asume que es un usuario Master)
		if ($intDatabaseID <= 0 && $intUserID > 0)
		{
		    $sql = "SELECT DatabaseID FROM SAAS_DATABASES WHERE UserID = ".$intUserID;
		    $result = mysql_query($sql, $mysql_Connection);
		    if (!$result || mysql_num_rows($result) == 0)
		    {
	            //return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
	            //return 'Error, email or password incorrect';
		    }
		    else
		    {
		    	$row = mysql_fetch_assoc($result);
			    $intDatabaseID = (int) $row["DatabaseID"];  
			    if ($intDatabaseID > 0)
			    {
			    	$_SESSION["dbid"] = $intDatabaseID;
			    }
		    }
		}
	}
	catch (Exception $e)
	{
		//Se ignoran los errores, simplemente no se graban las variables de sesión
	}
	
	return "Ok";
}

function ImprimirArregloBonito(&$arrData, $aShift=0, $inFile=0)
{
	foreach ($arrData as $aKey => $aValue) 
	{
		if($inFile==0)
		{
			if (is_array($aValue))
			{
				$strData = str_repeat('&nbsp;',$aShift).'['.$aKey.'] => Array';
				echo "\r\n".'<br>'.$strData;
				ImprimirArregloBonito($aValue, $aShift + 3, $inFile);
			}
			else 
			{
				$strData = str_repeat('&nbsp;',$aShift).'['.$aKey.'] => '.$aValue;
				echo "\r\n".'<br>'.$strData;
			}
		}
		else
		{
			if (is_array($aValue))
			{
				$strData = str_repeat(' ', $aShift).'['.$aKey.'] => Array';
				error_log("\r\n".' '.$strData, 3, ".\log\PruebaGrabado.txt");
				ImprimirArregloBonito($aValue, $aShift + 3, $inFile);
			}
			else 
			{
				$strData = str_repeat(' ', $aShift).'['.$aKey.'] => '.$aValue;
				error_log("\r\n".' '.$strData, 3, ".\log\PruebaGrabado.txt");				
			}
		}
	}
}

//@JAPR 2012-03-12: Agregado el uso del servicio como un servidor Web para cualquier tipo de aplicación
// Lee el parámetro especificado aplicando el tipo de dato indicado, ya sea del $_GET, $_POST o de ambos
/* El parámetro $bEncrypted funciona así: si se manda 'false' los parámetros se regresan tal cual, si se manda 'true' los parámetros se desencriptan antes de regresar, pero
si se manada null entonces primero se verifica si existe el parámetro 'encrypted' y dependiendo de esto activa/desactiva localmente dicha variable (los parámetros 'encrypted' y
'debug' son los únicos que jamás podrían venir encriptados) */
function getParamValue($sParamName, $sFrom = 'both', $sDataType = "(string)", $bNullIfMissing = false, $bEncrypted = null)
{
	$sValue = '';
	if (is_null($bEncrypted))
	{
		$bEncrypted = (bool) @getParamValue('encrypted', 'both','(int)', false, false);
	}
	
	if (strtolower($sFrom) == 'both')
	{
		$sValue = @getParamValue($sParamName, 'get',$sDataType, true, $bEncrypted);
		if (is_null($sValue))
		{
			$sValue = @getParamValue($sParamName, 'post',$sDataType, $bNullIfMissing, $bEncrypted);
		}
	}
	else
	{
		if (strpos(strtolower($sFrom), "get") !== false)
		{
			$sFrom = '$_GET';
		}
		else 
		{
			$sFrom = '$_POST';
		}
		
		@eval('$sValue = isset('.$sFrom.'[\''.$sParamName.'\']);');
		if (!$sValue && $bNullIfMissing)
		{
			return null;
		}
		
		if ($bEncrypted && $sParamName != 'debug' && $sParamName != 'encrypted' && $sParamName != 'encoding')
		{
			@eval('$sValue = @'.$sFrom.'[\''.$sParamName.'\'];');
			$sValue = BITAMDecode($sValue);
			@eval('$sValue = '.$sDataType.'$sValue;');
		}
		else 
		{
			@eval('$sValue = '.$sDataType.'@'.$sFrom.'[\''.$sParamName.'\'];');
		}
	}

	return $sValue;
}

//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
/* Esta función lee el parámetro appVersion (originalmente usado sólo en eSurveyVersion) y lo almacena en una variable de sesión, de tal forma
que pueda ser utilizado en cualquier parte directamente, con esto se controlará aquellos elementos de la metadata que si pueden ser libremente
consultados por el servicio/administrador.
Si la variable de sesión ya se encontraba previamente asignada entonces NO la sobreescribe
Si no se recibe el parámetro, se asume que se desea entrar con la versión actual del código
*/
function setAppVersion()
{
	$appVersion = 0;
	
	//Obtiene la versión de la sesión, si ya estaba asignada no la debe sobreescribir
	if (isset($_SESSION["PAAppVersion"]))
	{
		$appVersion = (float) @$_SESSION["PAAppVersion"];
	}
	
	//Si no estaba ya asignada la versión, la obtiene del parámetro del request
	if ($appVersion <= 0)
	{
		$appVersion = getParamValue('appVersion', 'both', '(string)', true);
		if (is_null($appVersion))
		{
			$appVersion = 0;
		}
		
		$appVersion = (float) $appVersion;
		
		//Si no se recibió la versión como parámetro, se toma directamente de la versión actual del Administrador de eForms
		//Para que este método sea útil y sin errores se debe invocar siempre después de incluir config.php
		if ($appVersion <= 0)
		{
			$appVersion = (float) @ESURVEY_SERVICE_VERSION;
			if (is_null($appVersion))
			{
				$appVersion = 0;
			}
		}
	}
	else 
	{
		//Ya existía la versión en la sesión así que no continua
		return;
	}
	
	//Escribe la variable en la sesión para que esté disponible en todos lados
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//@session_register("PAAppVersion");
	@$_SESSION["PAAppVersion"] = $appVersion;
}

/* Esta función obtiene el parámetro de la versión a utilizar directamente del $_SESSION, en caso de no estar asignado utilizará la versión de
este administrador
*/
function getAppVersion()
{
	$appVersion = 0;
	
	$appVersion = (float) @$_SESSION["PAAppVersion"];
	if ($appVersion <= 0)
	{
		$appVersion = (float) @ESURVEY_SERVICE_VERSION;
	}
	
	return $appVersion;
}
//@JAPR

/* Esta función obtiene el parámetro de la versión a utilizar directamente del $_SESSION, en caso de no estar asignado utilizará la versión de
este administrador
*/
function getMDVersion()
{
	$appVersion = 0;
	
	$appVersion = (float) @$_SESSION["PAeFormsMDVersion"];
	if ($appVersion <= 0)
	{
		$appVersion = 0.0;
	}
	
	return $appVersion;
}

//@JAPR 2012-10-23: Corregida la sincronización de encuestas, si el archivo JSon recibido del Outbox era muy grande, el json_decode no
//funcionaba así que se cambió el método de parseo pero se regresaba ahora un objeto, por lo que hubo que convertirlo a array de nuevo
function object_to_array($data) {
	if (is_array($data) || is_object($data)) {
		$result = array();
		foreach ($data as $key => $value) {
			$result[$key] = object_to_array($value);
		}         
		return $result;
	}
	return $data; 
} 

//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
//Equivalente a IfNull pero para usar internamente, ya que las de ADODB regresan el string con la referencia a la función correspondiente y no el 
//resultado de la evaluación
function StrIFNULL($aVar = null) {
	if (is_null($aVar)) {
		return 'NULL';
	}
	else {
		return $aVar;
	}
}

//@JAPR 2013-01-28: Agregado el proceso de identificación de errores del Model Manager
//Esta función validará si el objeto especificado contiene o no información de error de un proceso del Model Manager, en caso de existir se 
//regresará el texto específico del error. El parámetro $bRequired indica si en caso de no poder identificar la propiedad de error se debería marcar
//un error o no, ya que en ese caso se trata de un Model Manager que no contiene funcionalidad específica posterior a cuando se integró la propiedad
//que reporta los errores (esto debido a que Model Manager no tiene forma de ligar la funcionalidad a alguna variable de versión o cosas así), esto
//es necesario por ejemplo al invocar el método "BITAMModelDimension->remove_only_dimension" el cual se agregó junto con la propiedad Message, en
//caso de un error en ese método no se debería poder continuar con la ejecución del programa, otro tipo de errores mandarían $bRequired == false
//pues errores en ese punto no son causa de interrupción (como por ejemplo al eliminar alguna dimensión o exportar a PhP) así que en esos casos
//simplemente se regresa vacio incluso si no existe la propiedad Message
function identifyModelManagerError($aModelManagerObject, $bRequired = false) {
	$strErrorDesc = @$aModelManagerObject->Message;
	if (is_null($strErrorDesc) && $bRequired) {
		$strErrorDesc = 'The Model Manager version is older than the required version to run this program, please contact your System Administrator.';
	}
	
	return trim((string) $strErrorDesc);
}

//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
/* Identifica el tipo de nombre de archivo basado en el reconocimiento de patrones. Si el archivo no corresponde con un archivo del usuario
logeado regresará false, de lo contrario regresa un objeto con las partes identificadas del nombre de archivo
El parámetro $aUserName indica la cuenta del usuario a utilizar, si no se especifica se asumirá que se trata del usuario que hace la sincronización
*/
function unformatFileName($aFileName, $aUserName = null) {
	if (is_null($aUserName) || trim($aUserName) == '') {
		//Identifica la cuenta a partir de los datos del POST
		$aUserName = (string) @$_POST['UserID'];
	}
	//Si no se puede identificar la cuenta, no puede continuar con el proceso (se debería deshabilitar la recuperación de la captura del lado del 
	//server pues no pudo copiar los archivos a la ruta que debía para permitir esto)
	if (is_null($aUserName) || trim($aUserName) == '') {
		return false;
	}
	
	$strFileName = $aFileName;
	if (stripos($strFileName, $aUserName) === false) {
		return false;
	}
	
	//Remueve la parte del usario
	$strFileName = str_ireplace($aUserName.'_', '', $strFileName);
	$arrParts = @explode('_', $strFileName);
	//No se pudo identificar como un archivo válido de eForms, así que termina
	if (is_null($arrParts) || $arrParts === false || !is_array($arrParts) || count($arrParts) == 0) {
		return false;
	}
	
	$objFileStruct = array();
	switch ($arrParts[0]) {
		case 'surveyanswers':
			//Puede ser un Draft u Outbox, así que verifica buscando la palabra que los identifica
			$blnOutbox = false;
			foreach ($arrParts as $intIndex => $strValue) {
				$intPos = stripos($strValue, 'finished.dat');
				//@JAPR 2013-02-27: Ajustada la funcionalidad de Reupload para v4
				if ($intPos === false) {
					$intPos = stripos($strValue, 'finished_restored.dat');
				}
				//@JAPR
				if ($intPos !== false && $intPos >= 0) {
					$blnOutbox = true;
					break;
				}
			}
			
			if ($blnOutbox) {
				$objFileStruct['typeid'] = otyOutbox;
				$objFileStruct['type'] = 'Outbox';
			}
			else {
				$objFileStruct['typeid'] = otyDraft;
				$objFileStruct['type'] = 'Draft';
			}
			
			if (count($arrParts) > 1) {
				$objFileStruct['date'] = str_ireplace('.dat', '', $arrParts[1]);
				$objFileStruct['fmtdate'] = formatNumericDate($objFileStruct['date']);
			}
			if (count($arrParts) > 2) {
				$objFileStruct['hour'] = str_ireplace('.dat', '', $arrParts[2]);
				$objFileStruct['fmthour'] = formatNumericTime($objFileStruct['hour']);
			}
			if (count($arrParts) > 3) {
				$objFileStruct['surveyID'] = str_ireplace('.dat', '', $arrParts[3]);
			}
			if (count($arrParts) > 4) {
				$objFileStruct['agendaID'] = str_ireplace('.dat', '', $arrParts[4]);
			}
			break;
		
		case 'survey':
			//Definición de una encuesta
			$objFileStruct['typeid'] = otySurvey;
			$objFileStruct['type'] = 'Survey';
			if (count($arrParts) > 1) {
				$objFileStruct['surveyID'] = $arrParts[1];
			}
			break;
			
		case 'catalog':
			//Definición de un catálogo
			$objFileStruct['typeid'] = otyCatalog;
			$objFileStruct['type'] = 'Catalog';
			if (count($arrParts) > 1) {
				$objFileStruct['catalogID'] = $arrParts[1];
			}
			break;
			
		case 'photo':
			//Definición de una foto
			$objFileStruct['typeid'] = otyPhoto;
			$objFileStruct['type'] = 'Photo';
			if (count($arrParts) > 1) {
				$objFileStruct['surveyID'] = $arrParts[1];
			}
			if (count($arrParts) > 2) {
				$objFileStruct['date'] = $arrParts[2];
				$objFileStruct['fmtdate'] = formatNumericDate($objFileStruct['date']);
			}
			if (count($arrParts) > 3) {
				$objFileStruct['hour'] = $arrParts[3];
				$objFileStruct['fmthour'] = formatNumericTime($objFileStruct['hour']);
			}
			if (count($arrParts) > 4) {
				$objFileStruct['questionID'] = $arrParts[4];
			}
			if (count($arrParts) > 5) {
				$objFileStruct['recordNum'] = $arrParts[5];
			}
			break;
			
		case 'agenda':
			//Definición de una agenda, pero pude estar marcada como completada o no, así que busca la palabra clave
			$blnCompleted = false;
			foreach ($arrParts as $intIndex => $strValue) {
				$intPos = stripos($strValue, 'completed');
				if ($intPos !== false && $intPos >= 0) {
					$blnCompleted = true;
					break;
				}
			}
			
			$objFileStruct['typeid'] = otyAgenda;
			$objFileStruct['type'] = 'Agenda';
			if ($blnCompleted) {
				$objFileStruct['completed'] = true;
			}
			
			if (count($arrParts) > 1) {
				if (stripos($arrParts[1], 'completed') === false) {
					$objFileStruct['agendaID'] = $arrParts[1];
				}
			}
			break;
		
		case 'signature':
			$objFileStruct['typeid'] = otySignature;
			$objFileStruct['type'] = 'Signature';
			if (count($arrParts) > 1) {
				$objFileStruct['date'] = str_ireplace('.dat', '', $arrParts[1]);
				$objFileStruct['fmtdate'] = formatNumericDate($objFileStruct['date']);
			}
			if (count($arrParts) > 2) {
				$objFileStruct['hour'] = str_ireplace('.dat', '', $arrParts[2]);
				$objFileStruct['fmthour'] = formatNumericTime($objFileStruct['hour']);
			}
			if (count($arrParts) > 3) {
				$objFileStruct['surveyID'] = str_ireplace('.dat', '', $arrParts[3]);
			}
			break;
	}
	
	$objFileStruct['fileName'] = $aFileName;
	//@JAPR 2013-08-08: Agregado el proceso de regrabado de preguntas basado en los archivos .dat sobre capturas ya almacenadas en las tablas de datos
	$objFileStruct['userName'] = $aUserName;
	//@JAPR
	
	return $objFileStruct;
}

//@JAPR 2013-08-08: Agregado el proceso de regrabado de preguntas basado en los archivos .dat sobre capturas ya almacenadas en las tablas de datos
/* Identifica el tipo de nombre de archivo basado en el reconocimiento de patrones. 
A diferencia de unformatFileName, esta función no recibe el usuario sino que lo obtiene directamente del nombre de archivo, además opcionalmente
se verificará la existencia del usuario en la tabla de usuarios del repositorio para descartar el archivo si se trata de un usuario
que ya no se encuentre registrado
*/
function unformatFileNameWOUser($aFileName, $aRepository) {
	global $arrDataFileUserNames;
	
	$strFileName = $aFileName;
	
	//Debido a que la cuenta del usuario podría contener "_", no se puede hacer un explode directo del nombre del archivo y esperar que vengan
	//tantos componentes como se podría suponer, por tanto aunque se hará el explode, se procesarán desde el final para permitir descartar
	//divisiones provocadas por la cuenta del usuario
	//Si se habilitó la identificación mediante la tabla de usuarios, entonces primero se buscará la primera ocurrencia de una @ posterior a
	//remover el prefijo del archivo, y a partir de la @ buscará el siguiente "_" suponiendo que todo hasta ese punto es parte de una cuenta de
	//correo con la que intentará hacer match en la tabla de usuarios, apoyandose además de un array global de cuentas ya procesadas
	//$strFileName = str_ireplace($aUserName.'_', '', $strFileName);
	$arrParts = @explode('_', $strFileName);
	//No se pudo identificar como un archivo válido de eForms, así que termina
	if (is_null($arrParts) || $arrParts === false || !is_array($arrParts) || count($arrParts) == 0) {
		return false;
	}
	
	//Trata de identificar el nombre del usuario dentro del archivo (o por lo menos una porción del mismo ya que puede traer al caracter "_")
	$aUserName = '';
	$strFileNameTmp = str_ireplace($arrParts[0].'_', '', $strFileName);
	$intPos = strpos($strFileNameTmp, '@');
	if ($intPos !== false) {
		$intPos = strpos($strFileNameTmp, '_', $intPos);
		//No es posible que no exista otro separador, así que en caso de no haberlo no es un nombre válido de archivo
		if ($intPos !== false && $intPos > 0) {
			$aUserName = (string) @substr($strFileNameTmp, 0, $intPos -1);
		}
		if (trim($aUserName) == '') {
			$aUserName = '';
		}
	}
	
	//Si se va a buscar por el usuario en la metadata, entonces primero trata de identificar la mayor porción que pueda del nombre de usuario
	//que viene en el archivo, cortanto para esto el prefijo ya identificado
	if ($aRepository && trim($aUserName) != '') {
		//Primero intenta identificar al usuario de entre los EMails previamente identificados de otras llamadas a esta función en la sesión
		$blnFound = false;
		if (isset($arrDataFileUserNames) && is_array($arrDataFileUserNames)) {
			foreach ($arrDataFileUserNames as $intClaUsuario => $strEMail) {
				$intPos = stripos($strFileName, '_'.$strEMail.'_');
				if ($intPos !== false) {
					//Esta debe ser la cuenta buscada, así que la va a reutilizar
					$aUserName = $strEMail;
					$blnFound = true;
					break;
				}
			}
		}
		
		//Si no se logró identificar el usuario del array entonces si hace una consulta a la metadata
		if (!$blnFound) {
			require_once("user.inc.php");
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$arrUsers = @BITAMeFormsUser::GetUsersArrayWithEMailLike($aRepository, $aUserName);
			if (!is_null($arrUsers) && is_array($arrUsers) && count($arrUsers) > 0) {
				//Si se encontraron usuarios, es posible que pudiera haber cuentas muy similares así que recorre cada una de las identificadas hasta
				//encontrar la primera que exactamente se encuentre completa en el nombre de archivo 
				foreach ($arrUsers as $intClaUsuario => $strEMail) {
					$intPos = stripos($strFileName, '_'.$strEMail.'_');
					if ($intPos !== false) {
						//Esta debe ser la cuenta buscada, así que la va a reutilizar
						$aUserName = $strEMail;
						if (trim($aUserName) != '') {
							$arrDataFileUserNames[$intClaUsuario] = $aUserName;
						}
						break;
					}
				}
			}
			else {
				$aUserName = '';
			}
		}
	}
	
	//Si hay una cuenta válida identificada, entonces simplemente invoca al método tradicional para identificar el archivo
	if (trim($aUserName) != '') {
		return unformatFileName($aFileName, $aUserName);
	}
	
	//No es sencillo identificar el archivo si no se tiene el nombre de la cuenta, así que en este caso no se puede hacer nada
	return false;
}
//@JAPR

/* Regresa la fecha especificada (en formato yyyymmdd) a una fecha en formato estándar (yyyy-mm-dd) 
*/
function formatNumericDate($aNumericDate) {
	$aNumericDate = trim((string) $aNumericDate);
	if ($aNumericDate == '') {
		return $aNumericDate;
	}
	
	return substr($aNumericDate, 0, 4).'-'.substr($aNumericDate, 4, 2).'-'.substr($aNumericDate, 6, 2);
}

/* Regresa la hora especificada (en formato hhmmss) a una hora en formato estándar (hh:mm:ss) 
*/
function formatNumericTime($aNumericTime) {
	$aNumericTime = trim((string) $aNumericTime);
	if ($aNumericTime == '') {
		return $aNumericTime;
	}
	if (strlen($aNumericTime) < 6) {
		$aNumericTime = '0'.$aNumericTime;
	}
	
	return substr($aNumericTime, 0, 2).':'.substr($aNumericTime, 2, 2).':'.substr($aNumericTime, 4, 2);
}

///Une dos arrays multidimensionales respetando la llave tipo string o numérica que tenga cada elemento del array interno y lo regresa como un
//array multidimensional con el resultado de la unión ignorando repetidos (aunque el valor sea diferente en estos, se tomará el último en llegar)
function array_multi_merge($arr1, $arr2) {
	$arrNew = array();
	
	foreach ($arr1 as $aParentKey => $anItemArr) {
		if (!isset($arrNew[$aParentKey])) {
			$arrNew[$aParentKey] = array();
		}
		foreach ($anItemArr as $aKey => $aValue) {
			$arrNew[$aParentKey][$aKey] = $aValue;
		}
	}

	foreach ($arr2 as $aParentKey => $anItemArr) {
		if (!isset($arrNew[$aParentKey])) {
			$arrNew[$aParentKey] = array();
		}
		foreach ($anItemArr as $aKey => $aValue) {
			$arrNew[$aParentKey][$aKey] = $aValue;
		}
	}
	
	return $arrNew;
}

//@JAPR 2013-05-02: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
if (!function_exists('encodeUTF8')) {	
	function encodeUTF8($sValue) {
		if ( version_compare(phpversion(), '5.6.0', '>=') )
			return $sValue;
		
		if (@iconv('UTF-8','UTF-8',$sValue) != $sValue) {
			return utf8_encode($sValue);
		}
		return $sValue;
	}
}

if (!function_exists('decodeUTF8')) {	
	function decodeUTF8($sValue) {
		if (version_compare(phpversion(), '5.6.0', '>='))
			return $sValue;
		
		if (@iconv('UTF-8','UTF-8',$sValue) != $sValue) {
			return $sValue;
		}
		return utf8_decode($sValue);
	}
}

//Genera un tag Span para incluir en el reporte de un proceso de migración. Permite recibir parámetros para variar directamente el estilo
function sendUpgradeMessage($sErrorText, $aStyleSStr = '') {
	if ($aStyleSStr == '') {
		$aStyleSStr = 'color: red; font-weight:bold;';
	}
	$strHTML = "<br>\r\n<span style=\"$aStyleSStr\">".str_replace("\r\n", "<br>\r\n", $sErrorText)."</span>\r\n";
	
	return $strHTML;
}

//@JAPR 2013-06-27: Agregada la integración con IExplorer desde edición y captura vía EMail
//Obtiene la URL completa invocada para cargar la página actual
function curPageURL() {
	$isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
	$port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
	$port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
	$url = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
	
	return $url;
}

//@JAPR 2013-07-03: Agregado el mapeo de una forma de eBavel para acciones
/* Dado un ID de columna default de datos de eForms obtenible durante el grabado (constantes sfid y safid, aunque por compatibilidad con el grabado
de datos directo de la tabla SI_SV_SurveyFieldsMap que NO incluye IDs de preguntas, se manejarán las constantes positivas solamente
independientemente de si en la tabla correspondiente se grabaron negativas - Ver comentarios en la definición de las constantes para entender
por qué de esta diferencia), regresará el QTypeID que mejor aplique para dicha columna basado en los datos que se permiten grabar en ella 
(manualmente se pueden manejar excepciones en el código, como por ejemplo el UserID, el cual se pudiera regresar como ID o como Texto) */
function getDefaultEFormsColumnQTypeID($aDefaultEFormsColumnID) {
	$intQTypeID = qtpInvalid;
	
	if ($aDefaultEFormsColumnID < 0) {
		$aDefaultEFormsColumnID *= -1;
	}
	switch ($aDefaultEFormsColumnID) {
		case sfidStartDate:
		case sfidEndDate:
		case sfidSyncDate:
		case sfidServerStartDate:
		case sfidServerEndDate:
		case safidDueDate:
			$intQTypeID = qtpOpenDate;
			break;
		
		case sfidStartHour:
		case sfidEndHour:
		case sfidSyncHour:
		case sfidServerStartHour:
		case sfidServerEndHour:
			$intQTypeID = qtpOpenTime;
			break;
		
		case sfidLocation:
			$intQTypeID = qtpLocation;
			break;
		
		case sfidEntryID:
		case sfidUserID:
		case safidSourceID:
		case sfidFactKey:
		case safidSourceRowKey:
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		case sfidLocationAcc:
		//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
		case sfidFixedNumExpression:
		//@JAPR 2014-10-27: Agregado el mapeo de Scores
		case sfidSurveyScore:
			$intQTypeID = qtpOpenNumeric;
			break;
		
		//En este caso alguna columnas aun no son mapeables, por lo tanto se excluyen de la lista
		case sfidScheduler:
		case sfidEMail:
		case sfidDuration:
		case sfidAnsweredQuestions:
		case sfidEntrySection:
		case sfidDynamicPage:
		case sfidDynamicOption:
			$intQTypeID = qtpInvalid;
			break;
		
		//@JAPR 2014-10-27: Agregado el mapeo de atributos de catálogos
		case sfidCatalogAttribute:
			$intQTypeID = qtpAttribute;
			break;
		//@JAPR
			
		case sfidUserName:
		case sfidGPSCountry:
		case sfidGPSState:
		case sfidGPSCity:
		case sfidGPSAddress:
		case sfidGPSZipCode:
		case sfidGPSFullAddress:
		case safidDescription:
		case safidResponsible:
		case safidCategory:
		case safidStatus:
		case safidSource:
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
		case safidTitle:
		//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
		case sfidFixedStrExpression:
		//@JAPR
		default:
			//Cualquier otra columna por default se considerará tipo texto
			$intQTypeID = qtpOpenAlpha;
			break;
	}
	
	return $intQTypeID;
}

//Si se activa el parámetro $bAddCaller, generalmente significaría que se trata de una llamada de error ya que requiere identificar exactamente
//desde donde se originó, así que en ese caso sobreescribe a la variable que forza a grabar el archivo
function logeBavelString($aString, $sFileName = "", $bAddDate = true, $bAddCaller = false) {
	global $strDefaulteBavelLogStringFile;
	global $blnSaveeBavelLogStringFile;
	
	if (!isset($blnSaveeBavelLogStringFile)) {
		$blnSaveeBavelLogStringFile = false;
	}
	
	if (trim($sFileName) == '') {
		if (((string) @$strDefaulteBavelLogStringFile) != '') {
			$sFileName = $strDefaulteBavelLogStringFile;
		}
	}
	
	//Si no hay nombre de archivo entonces utiliza el default. Si hay nombre pero no contiene un path entonces usa el path default
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$sFilePath = GeteFormsLogPath();
	//@JAPR
	if (trim($sFileName) == '') {
		$sFileName = $sFilePath."eBavelActions.log";
	}
	elseif (strpos($sFileName, "\\") === false) {
		$sFileName = $sFilePath.$sFileName;
	}
	
	$strCaller = '';
	if ($bAddCaller) {
		$trace = @debug_backtrace();
		$caller = @$trace[1];
		if (!is_null($caller)) {
			$strClass = (string) @$caller['class'];
			if (isset($caller['function'])) {
				$strCaller = '('.(($strClass != '')?"$strClass".(string) @$caller['type']:'').$caller['function'].') ';
			}
		}
	}
	
	if ($bAddDate) {
		$strText = date("Y-m-d H:i:s").'- ';
	}
	else {
		$strText = '';
	}
	
	$strText .= $strCaller.$aString;
	if ($blnSaveeBavelLogStringFile) {
		@error_log("\r\n".str_ireplace("<br>", '', $strText), 3, $sFileName);
	}
	
	if (getParamValue('DebugLog', 'both', '(string)', true)) {
		//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//Integradas las funciones comunes para almacenamiento de log de eForms
		$sFileName = $sFilePath.getParamValue('DebugLog', 'both', '(string)', true);
		//@JAPR
		@error_log("\r\n".str_ireplace("<br>", '', $strText), 3, $sFileName);
	}
}

//@JAPR 2013-11-06: Agregada por compatibilidad con el Model Manager para recalculo de agregaciones
if (!function_exists('CountExecs')) {	
	function CountExecs($conn, $sql, $inputarray) {
		
	}
}

//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
//Dato un valor de GPS en formato "Latitude, Longitude (Accuracy)", lo divide en sus componentes y regresa un array con índice numérico con la
//secuencia 0=Latitude, 1=Longitude, 2=Accuracy
//En caso de no venir datos o error, regresa un array vacio
function SplitGPSData($aGPSData) {
	if (trim($aGPSData) == '') {
		return array('', '', '');
	}
	
	$arrData = explode('(', $aGPSData);
	$strAccuracy = trim(str_ireplace(')', '', (string) @$arrData[1]));
	if ($strAccuracy == '') {
		$strAccuracy = $gblEFormsNA;
	}
	$strAccuracy = $strAccuracy;
	
	$arrData = explode(',', ((string) @$arrData[0]).' ');
	$strLatitude = trim((string) @$arrData[0]);
	if ($strLatitude == '') {
		$strLatitude = $gblEFormsNA;
	}
	$strLatitude = $strLatitude;
	
	$strLongitude = trim((string) @$arrData[1]);
	if ($strLongitude == '') {
		$strLongitude = $gblEFormsNA;
	}
	$strLongitude = $strLongitude;
	
	$arrGPSData = array();
	$arrGPSData[0] = $strLatitude;
	$arrGPSData[1] = $strLongitude;
	$arrGPSData[2] = $strAccuracy;
	return $arrGPSData;
}

function AddCuentaCorreoAltToMail ($aUser, $aMail, $aVersion = null) {
	$aVersion = (is_null($aVersion)) ? getMDVersion() : $aVersion;
	if($aVersion >= esvCuentaCorreoAlt && !is_null($aUser) && !is_null($aMail)) {
		$cuentaCorreoAlt = $aUser->CuentaCorreoAlt;
		if(!is_null($cuentaCorreoAlt) && $cuentaCorreoAlt != '') {
			@$aMail->AddCC($cuentaCorreoAlt, $cuentaCorreoAlt);
		}
	}
}

function deployable ($deployableVersion) {
	$isDeployable = false;
	if(defined($deployableVersion)) {
		global $appVersion;
		$currentVersion = ($appVersion) ? $appVersion : getMDVersion();
		$isDeployable = ($currentVersion >= $deployableVersion) ? true : false;
	}
	return $isDeployable;
}

//@JAPR 2013-11-06: Agregada por compatibilidad con eBavel a partir del momento en que se hizo funcionar fuera de KPIOnline
/**
 * Revisa si eBavel esta montado en un servidor con ambiente BITAM KPI 
 * @return boolean True si estamos en un servidor KPI, false si no
 */
if (!function_exists('isKPIServer')) {
	function isKPIServer()
	{
		return true;
		/*
		$bRes = false;
		// Ruta de la carpeta FBM
		$sPath = "c:\\inetpub\\wwwroot\\fbm";
	
		if(is_file(FBM_PATH."\\ESurvey.class.php") && is_file(FBM_PATH."\\conns.inc.php"))
			$bRes = true;
	
		return $bRes;
		*/
	}
}

//@JAPR 2014-07-30: Agregado el Responsive Design
/* Dado un Id de dispositivo para Responsive Design, regresa el nombre equivalente a dicho dispositivo, el cual se usa entre otras cosas
como el nombre de propiedad cuando se quiere accesar a las propiedades basadas en Responsive Design
*/
function getDeviceNameFromID($aDeviceID) {
	$aDeviceName = '';
	switch ((int) $aDeviceID) {
		case dvciPod:
			$aDeviceName = 'iPod';
			break;
		case dvciPad:
			$aDeviceName = 'iPad';
			break;
		case dvciPadMini:
			$aDeviceName = 'iPadMini';
			break;
		case dvciPhone:
			$aDeviceName = 'iPhone';
			break;
		case dvcCel:
			$aDeviceName = 'Cel';
			break;
		case dvcTablet:
			$aDeviceName = 'Tablet';
			break;
		case dvcWeb:
		default:
			$aDeviceName = '';
			break;
	}
	
	return $aDeviceName;
}

/* Basado en los parámetros que todo dispositivo debe enviar en los requests a partir de la versión esvDeviceDataLog, regresa el ID de
dispositivo del que se trata, tomando en cuenta no sólo el nombre sino las dimensiones de la pantalla (especialmente en el caso de Android)
*/
function identifyDeviceType() {
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	//Si no viene el parámetro deviceID, querría decir que no pasó por el Agente así que debe venir el parámetro platform y por ende se procesa desde ese (si tampcoo viniera dicho
	//parámetro entonces sería un error, pues se trataría de una versión muy vieja del App). Este dato se obtiene en la funciòn identifyDeviceType
	$intDeviceID = getParamValue('deviceID', 'both', '(int)', true);
	if (!is_null($intDeviceID)) {
		//Si no es NULL entonces ya se estaba enviando el parámetro, así que no tiene necesidad de calcularlo
		return $intDeviceID;
	}
	//@JAPR
	
	$intDeviceID = dvcWeb;
	$strPlatform = strtolower(trim(getParamValue('platform')));
    
    //2015.01.23 JCEM #YAE927 Se cambia la forma de identificar los dispositivos iOS
	switch ($strPlatform) {
        case 'iphone simulator':
            $intDeviceID = dvciPhone;
			break;
		case 'ipad simulator':
			$intDeviceID = dvciPad;
			break;
       	case 'ipod':
		case 'ipad':
		case 'iphone':
            //2015.01.23 JCEM siendo dispositivos ios hay que detectar las diferencias de los diversos modelos, 
            //2015.01.23 JCEM En un futuro se puede ocupar, determina el modelo exacto del dispositivo ios function iOSDeviceModelByMachineID($sMachineID)
            $strModel =  trim(getParamValue('model'));
            //2015.01.28 JCEM si por alguna circunstancia del destino el model no llega 
            //se toma el camino anterior por platform
            if ($strModel != ''){
                $intDeviceID = iOSDeviceTypeByMachineID($strModel);
            }else{
                if ($strPlatform == 'ipod') {$intDeviceID = dvciPod;}
                if ($strPlatform == 'iphone') {$intDeviceID = dvciPhone;}
                if ($strPlatform == 'ipad') {$intDeviceID = dvciPad;}
            }
            
            break;
		case 'android':
			//En el caso de android no basta con el platform ya que todos los dispositivos dicen Android, así que se comparará el screenSize
			//para determinar la posibilidad de que se trate de un celular o una tablet, tomando como referencia la menor de las resoluciones
			//recibidas independientemente de la orientación en que fue enviada
			$strScreenSize = strtolower(trim(getParamValue('screenSize')));
			$intDeviceID = identifyAndroidDeviceByWidth($strScreenSize);
			break;
			
		//@JAPR 2015-08-14: Corregido un bug, a partir de la última actualización de Cordova (eForms v6) ya no se recibe el platform con el tipo de dispositivos de IOs correcto
		//sino que se recibe sólo "iOS", así que hay que determinar el tipo de DeviceID de otra manera
		case 'ios':
			//A partir de la actualización inicial de Cordova en v6, hay que identificar el dispositivo por el Screen Size ya que el platform llega como iOs independientemente
			//del dispositivo específico
			$strScreenSize = strtolower(trim(getParamValue('screenSize')));
			$intDeviceID = identifyIOsDeviceByWidth($strScreenSize);
			break;
			//@JAPR
			
		case 'browser':
			$intDeviceID = dvcWeb;
			break;
			
		default:
			//Por default se debería considerar Web para que mínimo regrese las propiedades originales, sin embargo se hará un último intento
			//buscando por palabras clave
			if (strpos($strPlatform, 'ipod') !== false) {
				$intDeviceID = dvciPod;
			}
			elseif (strpos($strPlatform, 'ipad') !== false) {
				$intDeviceID = dvciPad;
			}
			elseif (strpos($strPlatform, 'iphone') !== false) {
				$intDeviceID = dvciPhone;
			}
			elseif (strpos($strPlatform, 'android') !== false) {
				//En el caso de android no basta con el platform ya que todos los dispositivos dicen Android, así que se comparará el screenSize
				//para determinar la posibilidad de que se trate de un celular o una tablet, tomando como referencia la menor de las resoluciones
				//recibidas independientemente de la orientación en que fue enviada
				$strScreenSize = strtolower(trim(getParamValue('screenSize')));
				$intDeviceID = identifyAndroidDeviceByWidth($strScreenSize);
			}
			break;
	}
	
	return $intDeviceID;
}

function iOSDeviceTypeByMachineID($sMachineID){
    //por compatibilidad
    $res = dvcWeb;
    
    switch ($sMachineID){
        case "iPhone1,1": 
        case "iPhone1,2":  
        case "iPhone2,1":  
        case "iPhone3,1":  
        case "iPhone3,2":  
        case "iPhone3,3":  
        case "iPhone4,1":
        case "iPhone5,1": 
        case "iPhone5,2": 
        case "iPhone5,3": 
        case "iPhone5,4": 
        case "iPhone6,1": 
        case "iPhone6,2": 
        case "iPhone7,1": 
        case "iPhone7,2": 
            $res = dvciPhone;
            break;
        case "iPod1,1": 
        case "iPod2,1": 
        case "iPod3,1": 
        case "iPod4,1": 
        case "iPod5,1": 
            $res = dvciPod;
            break;   
        case "iPad1,1":  
        case "iPad2,1":  
        case "iPad2,2":  
        case "iPad2,3":  
        case "iPad2,4":  
        case "iPad3,1":  
        case "iPad3,2":  
        case "iPad3,3":  
        case "iPad3,4":  
        case "iPad3,5":  
        case "iPad3,6":  
        case "iPad4,1":  
        case "iPad4,2":  
        case "iPad4,3":  
            $res = dvciPad;
            break;   
        case "iPad2,5":  
        case "iPad2,6":  
        case "iPad2,7":  
        case "iPad4,4":  
        case "iPad4,5":  
        case "iPad4,6":  
            $res = dvciPadMini;
            break;
        case "x86_64":  //simulador x64
        case "i386":    //simulador x86
        default:
           $res = dvcWeb;
           break;
    }
    return $res;
}

//2015.01.23 JCEM En un futuro se puede ocupar, determina el modelo exacto del dispositivo ios
function iOSDeviceModelByMachineID($sMachineID){
            $res = "Unknow";
    
            switch ($sMachineID){
                case "iPhone1,1":
                    $res = "iPhone 1G";
                    break;
                case "iPhone1,2":    
                    $res = "iPhone 3G";
                    break;
                case "iPhone2,1":    
                    $res = "iPhone 3GS";
                    break;
                case "iPhone3,1":    
                    $res = "iPhone 4";
                    break;
                case "iPhone3,2":    
                    $res = "iPhone 4";
                    break;
                case "iPhone3,3":    
                    $res = "Verizon iPhone 4";
                    break;
                case "iPhone4,1":    
                    $res = "iPhone 4S";
                    break;
                case "iPhone5,1":    
                    $res = "iPhone 5 (GSM)";
                    break;
                case "iPhone5,2":    
                    $res = "iPhone 5 (GSM+CDMA)";
                    break;
                case "iPhone5,3":    
                    $res = "iPhone 5C (GSM)";
                    break;
                case "iPhone5,4":    
                    $res = "iPhone 5C (GSM+CDMA)";
                    break;
                case "iPhone6,1":    
                    $res = "iPhone 5S (GSM)";
                    break;
                case "iPhone6,2":    
                    $res = "iPhone 5S (GSM+CDMA)";
                    break;
                case "iPhone7,1":    
                    $res = "iPhone 6+";
                    break;
                case "iPhone7,2":    
                    $res = "iPhone 6";
                    break;
                case "iPod1,1":      
                    $res = "iPod Touch 1G";
                    break;
                case "iPod2,1":      
                    $res = "iPod Touch 2G";
                    break;
                case "iPod3,1":      
                    $res = "iPod Touch 3G";
                    break;
                case "iPod4,1":      
                    $res = "iPod Touch 4G";
                    break;
                case "iPod5,1":      
                    $res = "iPod Touch 5G";
                    break;
                case "iPad1,1":      
                    $res = "iPad";
                    break;
                case "iPad2,1":      
                    $res = "iPad 2 (WiFi)";
                    break;
                case "iPad2,2":      
                    $res = "iPad 2 (GSM)";
                    break;
                case "iPad2,3":      
                    $res = "iPad 2 (CDMA)";
                    break;
                case "iPad2,4":      
                    $res = "iPad 2 (WiFi)";
                    break;
                case "iPad2,5":      
                    $res = "iPad Mini (WiFi)";
                    break;
                case "iPad2,6":      
                    $res = "iPad Mini (GSM)";
                    break;
                case "iPad2,7":      
                    $res = "iPad Mini (GSM+CDMA)";
                    break;
                case "iPad3,1":      
                    $res = "iPad 3 (WiFi)";
                    break;
                case "iPad3,2":      
                    $res = "iPad 3 (GSM+CDMA)";
                    break;
                case "iPad3,3":      
                    $res = "iPad 3 (GSM)";
                    break;
                case "iPad3,4":      
                    $res = "iPad 4 (WiFi)";
                    break;
                case "iPad3,5":      
                    $res = "iPad 4 (GSM)";
                    break;
                case "iPad3,6":      
                    $res = "iPad 4 (GSM+CDMA)";
                    break;
                case "iPad4,1":      
                    $res = "iPad Air (WiFi)";
                    break;
                case "iPad4,2":      
                    $res = "iPad Air (Cellular)";
                    break;
                case "iPad4,3":      
                    $res = "iPad Air";
                    break;
                case "iPad4,4":      
                    $res = "iPad Mini 2G (WiFi)";
                    break;
                case "iPad4,5":      
                    $res = "iPad Mini 2G (Cellular)";
                    break;
                case "iPad4,6":      
                    $res = "iPad Mini 2G";
                    break;
                case "i386":         
                    $res = "Simulator";
                    break;
                case "x86_64":       
                    $res = "Simulator";
                    break;
            }
            return $res;
}


/* Basado en un string en formato width_height recibido desde un dispositivo previamente identificado como Android, determina la menor de las
resoluciones ya sea width o height independientemente de la orientación, para comparar contra el menos valor preestablecido que se consideraría
como un teléfono Android y determinar así si se trata de una Tablet o un teléfono
*/
function identifyAndroidDeviceByWidth($sScreenSize) {
	$intDeviceID = dvcTablet;
	
	if ($sScreenSize != '') {
		$arrSize = explode('_', $sScreenSize);
		$intWidth = (int) @$arrSize[0];
		$intHeigh = (int) @$arrSize[1];
		if ($intWidth > $intHeigh) {
			$intWidth = $intHeigh;
		}
		
		if ($intWidth < 500) {
			$intDeviceID = dvcCel;
		}
	}
	
	return $intDeviceID;
}

/* Basado en un string en formato width_height recibido desde un dispositivo previamente identificado como IOs, determina la menor de las
resoluciones ya sea width o height independientemente de la orientación, para comparar contra el menos valor preestablecido que se consideraría
como un teléfono Android y determinar así si se trata de una Tablet o un teléfono
*/
function identifyIOsDeviceByWidth($sScreenSize) {
	$intDeviceID = dvciPad;
	
	if ($sScreenSize != '') {
		$arrSize = explode('_', $sScreenSize);
		$intWidth = (int) @$arrSize[0];
		$intHeigh = (int) @$arrSize[1];
		if ($intWidth > $intHeigh) {
			$intWidth = $intHeigh;
		}
		
		if ($intWidth < 500) {
			$intDeviceID = dvciPhone;
		}
	}
	
	return $intDeviceID;
}

//@JAPR 2014-08-07: Agregado el parámetro para remover las comillas dobles de las cadenas
function getQuotedStringForExport($sText) {
	static $blnNoQuotes = null;
	static $strDobleQuote = '""';
	
	if (is_null($blnNoQuotes)) {
		$blnNoQuotes = getParamValue('noQuotes', 'both', '(bool)');
		if ($blnNoQuotes) {
			$strDobleQuote = '';
		}
	}
	
	$strText = str_replace('"', $strDobleQuote, $sText);
	return $strText;
}

//@JAPR 2014-08-19: Agregada por compatibilidad con eBavel
/**
 * 2014-07-11@ARMZ Obtiene la ultima fecha de actualizacion de la metadata
 * @return float 		timestamp  de la ultima actualizacion
 */
if (!function_exists('getMDUpdate')) {	
	function getMDUpdate() {
		$appVersion = 0;
		if (!class_exists('Session') || !class_exists('Metadata')) {
			return $appVersion;
		}
		
		$appVersion = (float) @Session::get("EBMDUpdate");
		if ($appVersion <= 0 && @Metadata::connection())
		{
			if (class_exists('Configura')) {
				$appVersion = (float) @Configura::get(80, 0, -2, Metadata::connection());
			}
			
			@Session::set("EBMDUpdate", $appVersion);
		}
		
		return $appVersion;
	}
}

//@JAPR 2014-09-03: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
/* Regresa la fecha y hora del archivo especificado (con ruta relativa o física) en el formato indicado,
1 = Formato numérico (es el usado con el proceso de sincronización)
2 = Formato string tradicional (para despliegue al usuario)
0 (default) = Formato UNIX para uso de otras funciones de PhP
Texto = Regresa la fecha aplicando el formato especificado, aunque por el momento dicho formato debe ser compatible con la función date()
*/
function GetFileLastModTimestamp($sFileName, $iFormat = 0) {
	//OMMC 2015-10-12: Se decodifica como UTF para eliminar problema de acentos.
	$sFileName = utf8_decode($sFileName); 
	if (is_null($sFileName) || trim((string) @$sFileName) == '') {
		return null;
	}
	
	clearstatcache();
	//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
	//Para impedir que se intente descargar la imagen, se marcará con un número de versión 0 si se detecta que no existe, de esta forma el App va a
	//omitir su descarga sin importar si la tiene o no previamente en el dispositivo
	if (!file_exists($sFileName)) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("GetFileLastModTimestamp file not exists: {$sFileName}, format: {$iFormat}");
		}
		return 0;
	}
	//@JAPR
	
	$varTime = filemtime($sFileName); 
	$blnIsDST = (date('I', $varTime) == 1); 
	$blnIsSystemDST = (date('I') == 1); 
	
	$intAdjustment = 0; 
	if($blnIsDST == false && $blnIsSystemDST == true) {
	  $intAdjustment = 3600; 
	}
	else if($blnIsDST == true && $blnIsSystemDST == false) {
	  $intAdjustment = -3600; 
	}
	else {
	  $intAdjustment = 0; 
	}
	
	$varDateTime = $varTime + $intAdjustment;
	if (!is_numeric($iFormat)) {
		//Si no es numérico, se debe tratar de un formato personalizado así que lo aplica directamente
		$varDateTime = @date($iFormat, $varDateTime);
	}
	else {
		switch ($iFormat) {
			case 1:		//Regresa la fecha como un string numérico (yyyymmddhhnnss)
				$varDateTime = @date("YmdHis", $varDateTime);
				break;
			case 2:		//Regresa la fecha como un string tradicional (yyyy-mm-dd hh:nn:ss)
				$varDateTime = @date("Y-m-d H:i:s", $varDateTime);
				break;
			default:	//Regresa el timeStamp UNIX de la función filemtime
				break;
		}
	}
	return ($varDateTime); 
}

/* Dada una ruta de archivo que se encuentra en el mismo server que el script ejecutado, regresa la ruta física del mismo para permitir utilizar las
funciones de archivos directamente
*/
function GetPhysicalFilePathFromURL($anURL) {
	if (is_null($anURL) || trim((string) @$anURL) == '') {
		return '';
	}
	
	$strPath = '';
	try {
		//@JAPR 2015-11-11: Corregido un bug con imagenes con caracteres raros, parse_url no entiende utf8, así que estaba regresando mal el array cuando la URL venía con esa codificación
		$arrURL = parse_url(utf8_decode($anURL));
		//$arrURL = parse_url($anURL);
		//@JAPR 2015-11-11: Corregido un bug con imagenes con caracteres raros, parse_url no entiende utf8, así que estaba regresando mal el array cuando la URL venía con esa codificación
		//Se debe volver a codificar porque json_encode solo funciona con caracteres en utf8
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@utf8_encode($arrURL['path']));
		//$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']);
		$strPath = str_replace("/", "\\", $strPath);
	} catch(Exception $e) {}
	
	return $strPath;
}

/* Dada una URL o ruta física de archivo, separa por las porciones "\" y obtiene la última, la cual debería corresponder con el nombre de archivo
(no considera URLs con parámetros ?)
*/
function GetFileNameFromFilePath($sFilePath) {
	if (is_null($sFilePath) || trim((string) @$sFilePath) == '') {
		return '';
	}
	
	$sFilePath = str_replace("/", "\\", $sFilePath);
	$arrFileParts = explode("\\", $sFilePath);
	$strFileName = (string)@$arrFileParts[count($arrFileParts) -1];
	return $strFileName;
}

/* Dada la ruta absoluta de una imagen que será enviada al App, la convierte a la ruta relativa tal como se tiene que utilizar en el dispositivo, ya
que ahí se grabará dentro de la carpeta images con una subcarpeta referente al FBM_BMD_#### de la cuenta que se trate, esto para no sobreescribir con
imagenes de otros proyectos que pudieran usarse en el mismo dispositivo o con fotos tomadas por el propio aparato.
Esta función se tiene que utilizar durante la descarga de definiciones para cambiar las referencias internas de cada imagen, ya que aunque para captura
web si se pueden quedar como absolutas, para dispositivos deben ajustarse por lo que se explicó así que se sobreescriben de la definición original
*/
function GetImageRelativeNameForApp($aRepository, $sFilePath) {
	if (is_null($aRepository) || is_null($sFilePath) || trim((string) @$sFilePath) == '') {
		return '';
	}
	
	$strFileName = GetFileNameFromFilePath($sFilePath);
	$strFileName = "images/".$aRepository->ADOConnection->databaseName."/".urldecode($strFileName);
	return $strFileName;
}

//@JAPR 2014-10-16: Corregido un bug, al grabar las configuraciones de variables utilizando IDs en lugar de números, el copiado ya no funcionaba
//porque dejaba ahora las referencias a los IDs originales las cuales no se actualizaban
/* Esta función recibe como parámetro la colección de secciones y preguntas indexadas por su ID anterior y como valor el ID nuevo (se asume que ya
está ordenada en forma descendente numérica a partir de los IDs anteriores), así como la versión de última modificación del registro y un campo a
validar por posibles variables, si el registro es posterior a la versión que introdujo el grabado con IDs, y si detecta por lo menos una variable,
recorrerá los IDs recibidos tanto para secciones como para preguntas para actualizar todas las referencias, primero reemplazando por una variable
temporal para evitar colisión de los nuevos IDs en reemplazos subsecuentes, y posteriormente ya con la variable traducida a los nuevos IDs
El valor regresado será un string si se realizó alguna actualización de IDs, o FALSE en caso de que no hubiera necesidad, así se puede controlar
desde el punto en que se invoca si se hará o no un UPDATE a ese campo específico (esta función se debería utilizar exclusivamente durante el copiado
de las encuestas, ya que está ajustada principalmente para ese proceso)
El parámetro $bServerVariables realmente sólo aplicaría si se hiciera algún cambio en el método de reemplazo, ya que las variables del server
tienen la particularidad que no se han encerrado entre { }, por lo que es mas sencillo el reemplazo ya que se puede hacer directo asumiendo que
siempre seguirían la sintaxis @Q# directamente, a diferencia de las variables del App que pueden contener espacios y aún así funcionar
adecuadamente, sin embargo para efectos de este proceso, se tomará como premisa que todas las variables siguen el patrón @Q# sin espacios, ya que
se complicaría mas hacer un parse que considerara espacios intermedios
*/
function ReplaceOldVariableIDsAfterCopy($sText, $aSectionIDsColl, $aQuestionIDsColl, $anAdminVersionNum, $bServerVariables = false) {
	//Si la versión de metadata no es la que comenzaba a grabar a partir de IDs, entonces no hay necesidad de realizar este proceso
	if (getMDVersion() < esvExtendedModifInfo) {
		return false;
	}
	
	$anAdminVersionNum = (float) @$anAdminVersionNum;
	
	//Si la versión del objeto es de una que todavía hubiera grabado con números de preguntas, entonces no hay necesidad de realizar este proceso
	if ($anAdminVersionNum < esvExtendedModifInfo) {
		return false;
	}
	
	if (trim($sText) == '') {
		return false;
	}
	
	$blnQuestionVars = false;
	$blnSectionVars = false;
	$intOffSet = 0;
	$strResult = $sText;
	
	//Verifica si existen variables tanto de la sintaxis original como de la nueva
	if ($bServerVariables) {
		//Las variables del server se caracterizan por no estar delimitadas entre { }
		$blnQuestionVars = stripos($sText, '@Q', $intOffSet);
		if ($blnQuestionVars === false) {
			$blnQuestionVars = stripos($sText, '$Q', $intOffSet);
		}
	}
	else {
		//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
		$blnQuestionVars = stripos($sText, '{@Q', $intOffSet);
		if ($blnQuestionVars === false) {
			$blnQuestionVars = stripos($sText, '{$Q', $intOffSet);
		}
	}
	
	if ($bServerVariables) {
		//Las variables del server se caracterizan por no estar delimitadas entre { }
		$blnSectionVars = stripos($sText, '@S', $intOffSet);
		if ($blnSectionVars === false) {
			$blnSectionVars = stripos($sText, '$S', $intOffSet);
		}
	}
	else {
		//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
		$blnSectionVars = stripos($sText, '{@S', $intOffSet);
		if ($blnSectionVars === false) {
			$blnSectionVars = stripos($sText, '{$S', $intOffSet);
		}
	}
	
	//Si el texto no tiene ni variables de preguntas ni de secciones, entonces no hay necesidad de realizar este proceso
	if ($blnQuestionVars === false && $blnSectionVars === false) {
		return false;
	}
	
	//En este punto ya se identifico que existen variables de secciones o preguntas, así que recorrerá los IDs recibidos para hacer el reemplazo
	//de los IDs originales por los nuevos, considerando ambas sintaxis de variables (no se aplicará parseo específico, aunque eso podría llegar
	//a optimizar el proceso)
	$arrReplacedVars = array();
	//Variables de preguntas
	//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
	if ($blnQuestionVars !== false) {
		if (is_null($aQuestionIDsColl) || !is_array($aQuestionIDsColl)) {
			$aQuestionIDsColl = array();
		}
		
		foreach ($aQuestionIDsColl as $intOldQuestionID => $intQuestionID) {
			//Formato anterior
			$strOldVar = (($bServerVariables)?'':'{')."@Q".$intOldQuestionID;
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = (($bServerVariables)?'':'{')."@QID".$intQuestionID;
				$arrReplacedVars[$strNewVar] = (($bServerVariables)?'':'{')."@Q".$intQuestionID;
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
			
			//Formato nuevo
			//@JAPR 2017-02-17: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$strOldVar = (($bServerVariables)?'':'{')."\$Q".$intOldQuestionID.(($bServerVariables)?'':'.');
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = (($bServerVariables)?'':'{')."\$QID".$intQuestionID.(($bServerVariables)?'':'.');
				$arrReplacedVars[$strNewVar] = (($bServerVariables)?'':'{')."\$Q".$intQuestionID.(($bServerVariables)?'':'.');
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
			//@JAPR
		}
	}
	
	//Variables de secciones
	//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
	if ($blnSectionVars !== false) {
		if (is_null($aSectionIDsColl) || !is_array($aSectionIDsColl)) {
			$aSectionIDsColl = array();
		}
		
		foreach ($aSectionIDsColl as $intOldSectionID => $intSectionID) {
			//Formato anterior
			$strOldVar = (($bServerVariables)?'':'{')."@S".$intOldSectionID;
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = (($bServerVariables)?'':'{')."@SID".$intSectionID;
				$arrReplacedVars[$strNewVar] = (($bServerVariables)?'':'{')."@S".$intSectionID;
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
			
			//Formato nuevo
			//@JAPR 2017-02-17: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$strOldVar = (($bServerVariables)?'':'{')."\$S".$intOldSectionID.(($bServerVariables)?'':'.');
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = (($bServerVariables)?'':'{')."\$SID".$intSectionID.(($bServerVariables)?'':'.');
				$arrReplacedVars[$strNewVar] = (($bServerVariables)?'':'{')."\$S".$intSectionID.(($bServerVariables)?'':'.');
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
			//@JAPR
		}
	}
	
	//Si realmente no se realizaron reemplazos (esto significaria que las variables usadas ya no existen actualmente como preguntas/secciones),
	//entonces no hay necesidad de realizar este proceso
	if (count($arrReplacedVars)) {
		//Antes de terminar debe remover las variables temporales
		foreach ($arrReplacedVars as $strTempVar => $strNewVar) {
			$strResult = str_ireplace($strTempVar, $strNewVar, $strResult);
		}
		
		return $strResult;
	}
	else {
		return false;
	}
}

/* Similar a ReplaceOldVariableIDsAfterCopy pero en este caso busca patrones predefinidos de Javascript que hagan referencia a los objetos de eForms dentro del texto, de la forma
selSurvey.questions[ID] o selSurvey.sections[ID] entre otras, para reemplazar los IDs originales por los nuevos y que se respeten los scripts personalizados de usuario
*/
function ReplaceJavaScriptOldVariableIDsAfterCopy($sText, $aSectionIDsColl, $aQuestionIDsColl) {
	if (trim($sText) == '') {
		return false;
	}
	
	$blnQuestionVars = false;
	$blnSectionVars = false;
	$intOffSet = 0;
	$strResult = $sText;
	
	//Acceso directo a la colección de preguntas de la forma
	$blnQuestionVars = stripos($sText, 'selSurvey.questions[', $intOffSet);
	
	//Acceso directo a la colección de secciones de la forma
	$blnSectionVars = stripos($sText, 'selSurvey.sections[', $intOffSet);
	
	//Si el texto no tiene ni referencias a preguntas ni a secciones, entonces no hay necesidad de realizar este proceso
	if ($blnQuestionVars === false && $blnSectionVars === false) {
		return false;
	}
	
	//En este punto ya se identifico que existen referencias de secciones o preguntas, así que recorrerá los IDs recibidos para hacer el reemplazo
	//de los IDs originales por los nuevos
	$arrReplacedVars = array();
	//Referencias de preguntas
	if ($blnQuestionVars !== false) {
		if (is_null($aQuestionIDsColl) || !is_array($aQuestionIDsColl)) {
			$aQuestionIDsColl = array();
		}
		
		foreach ($aQuestionIDsColl as $intOldQuestionID => $intQuestionID) {
			$strOldVar = 'selSurvey.questions['.$intOldQuestionID.']';
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = 'selSurvey.questions['.$intQuestionID.']';
				$arrReplacedVars[$strNewVar] = $strNewVar;
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
		}
	}
	
	//Referencias de secciones
	if ($blnSectionVars !== false) {
		if (is_null($aSectionIDsColl) || !is_array($aSectionIDsColl)) {
			$aSectionIDsColl = array();
		}
		
		foreach ($aSectionIDsColl as $intOldSectionID => $intSectionID) {
			$strOldVar = 'selSurvey.sections['.$intOldSectionID.']';
			$intPos = stripos($strResult, $strOldVar, $intOffSet);
			if ($intPos !== false) {
				$strNewVar = 'selSurvey.sections['.$intSectionID.']';
				$arrReplacedVars[$strNewVar] = $strNewVar;
				$strResult = str_ireplace($strOldVar, $strNewVar, $strResult);
			}
		}
	}
	
	//Si realmente no se realizaron reemplazos (esto significaria que las variables usadas ya no existen actualmente como preguntas/secciones),
	//entonces no hay necesidad de realizar este proceso
	if (count($arrReplacedVars)) {
		return $strResult;
	}
	else {
		return false;
	}
}

//@JAPR 2014-10-23: Corregido un bug, faltaba considerar al nuevo formato de preguntas
/* Dado un string que representa exactamente una variable de pregunta (originalmente, pero se puede adaptar para las de sección también) que está
definida en el nuevo formato de variables, buscará el primer punto y quitará todo lo que siga a ese dato hasta llegar al caracter de cierre si
es que había uno. Esta función originalmente se utilizó como callback de un array_walk para eliminar estos componentes al resultad de un preg_match_all
de cadenas que podían contener muchas variables, pero sólo si la expresión regular ya era de las que utilizan el nuevo formato de variables
*/
function replaceNewVariableParams(&$sText) {
	$strText = $sText;
	//El punto es el primer caracter que puede venir en el nuevo formato de variables posterior a la referencia al objeto, así que sólo si hay alguno
	//es como se debe procesar
	$intPos = strpos($strText, ".");
	if ($intPos !== false) {
		if ($intPos == 0) {
			$strText = '';
		}
		else {
			$strText = substr($strText, 0,  $intPos);
		}
	}
	
	//Al finalizar si el texto contenía el delimitador inicial, se tendría que agregar el final para completar una definición válida de variable
	$intPos = strpos($strText, '{');
	if ($intPos !== false) {
		$strText .= '}';
	}
	
	$sText = $strText;
}

//@JAPR 2014-11-19: Agregada la dimensión Agenda
/* Esta función se encarga de agregar los campos y/o tablas que son requeridos con cada actualización sin pedir validaciones de versión, dichos
cambios son por ejemplo nuevas dimensiones o tablas de registro de eventos o cualquier tipo de elemento que requiera ser utilizado por el producto
(en ocasiones si dependiendo de alguna versión, pero en su mayoría no) y que o son opcionales, por lo cual no hay un encargado de agregarlos y
por tanto se deben crear automáticamente, o bien son requeridos, por lo cual es indispensable que se existan
Los siguientes son los casos que hacen uso de esta funcionalidad, la cual se invoca desde un login normal (index.php), acceso como webServiice
(eSurveyService.php) o login externo (loginbitam.php)
- Dimensión Agenda (SI_SV_GblSurveyModel.AgendaDimID y SI_SV_Survey.AgendaDimID). Versión: esvAgendaDimID
*/
function AutoUpdateRequiredMetadata($aRepository) {
	if (is_null($aRepository)) {
		return;
	}
	
	$sql = "ALTER TABLE SI_SV_GblSurveyModel ADD AgendaDimID INT NULL";
	@$aRepository->DataADOConnection->Execute($sql);
	
	$sql = "ALTER TABLE SI_SV_Survey ADD AgendaDimID INT NULL";
	@$aRepository->DataADOConnection->Execute($sql);
}

//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
/* Esta función recibe como parámetro un valor que corresponde con una URL ya sea absoluta o relativa, la cual según el tipo ajustará a una
ruta relativa de eForms donde deberá ser guardada y la regresará en un array preparado para un posterior proceso de descarga de la imagen, o bien
simplemente como el texto ajustado que deberá ser grabado en lugar del valor original recibido (ya que eForms procesará estos valores de cierta
manera y por tanto las URLs deben estar adaptadas). Esta función se debería usar exclusivamente en el grabado de valores de catálogo cuando se
trata de un atributo imagen, o bien en la importación desde XLS en el mismo caso
Se espera que el parámetro $anImageURL venga urlencoded
Regresa null si no hay que hacer ajustes en el valor recibido
*/
function IdentifyAndAdjustAttributeImageToDownload($anImageURL) {
	$aReturnValue = null;
	$anImageURL = trim((string) $anImageURL);
	if ($anImageURL == '') {
		return $aReturnValue;
	}
	
	$strCurrentServicePath = $_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
	$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
	$strURLImage = $anImageURL;
	if (stripos($anImageURL, 'http://') === 0 || stripos($anImageURL, 'https://') === 0) {
		//En este caso se trata de una URL absoluta, por lo tanto se debe descargar la imagen siempre y cuando no sea una
		//URL propia del servicio de eForms, ya que en ese caso simplemente se tomará la ruta relativa
		$intServicePos = stripos($anImageURL, $strCurrentServicePath);
		if ($intServicePos !== false) {
			//Reemplaza de la ruta a grabar la porción correspondiente al servicio de forma que quede una URL relativa, la
			//cual correspondería a una imagen dentro de alguna subcarpeta del servicio de eForms (generalmente sólo images
			//o customerImages)
			$anImageURL = trim(substr($anImageURL, $intServicePos + strlen($strCurrentServicePath)));
			while (strpos($anImageURL, "/") == 0) {
				$anImageURL = trim(substr($anImageURL, 1));
			}
			$aReturnValue = $anImageURL;
		}
		else {
			//Si no es una URL del servicio de eForms, se verifica si es una URL del mismo server, en ese caso si se puede
			//dejar como una ruta absoluta para permitir accesarla y verificar su versión, pero tampoco hay necesidad de
			//descargarla a la carpeta de imagenes de eForms
			$intServicePos = stripos($anImageURL, "/".$_SERVER['HTTP_HOST']."/");
			if ($intServicePos !== false) {
				//Se deja la ruta tal cual
			}
			else {
				//En este caso es una URL absoluta de otro servidor, por tanto se tiene que descargar la imagen hacia la
				//carpeta de eForms del usuario, pero como sólo podemos tener una imagen con el mismo nombre, se indexa
				//precisamente por el nombre de la imagen. La URL a grabar cambiará a la ruta relativa a la carpeta
				//del usuario
				$fileName = GetFileNameFromFilePath(urldecode($strURLImage));
				$customerImageFile = "customerimages/".$theRepositoryName.'/'.$fileName;
				$aReturnValue = array();
				$aReturnValue['name'] = $fileName;
				$aReturnValue['path'] = $customerImageFile;
				$aReturnValue['url'] = $strURLImage;
				$anImageURL = $customerImageFile;
			}
		}
	}
	else {
		//En este caso se asume que es una URL relativa pero no necesariamente al servicio de eForms, ya que si inicia con 
		//una "/" entonces se refiere a una ruta relativa al servidor, de lo contrario será una ruta relativa al servicio
		//de eForms correspondiente
		//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
		if (stripos($anImageURL, '/') === false) {
			//Ahora se agregó la posibilidad de simplemente definir un nombre de imagen (es decir, NO iniciará ni contendrá "/" como parte del texto), cuando esto suceda, verificará
			//si la imagen se encuentra dentro de las 2 carpetas posibles de imágenes de eForms que son customerImages o images, si es así, se auto-ajustará el texto para reflejar la
			//ruta donde se encontró, de lo contrario se dejará el texto sin cambio alguno lo cual representará un error
			//@JAPR 2016-06-23: Por acuerdo con LRoux y CGil, no se considerará buscar la imagen dentro de la carpeta images, e independientemente de si se encontraba o no dentro de
			//customerimages, se ajustará la ruta para que se busque ahí, ya que el usuario tendrá la posibilidad de subir la imagen mas adelante y se subirá siempre a dicha ruta
			//Primero verifica contra la carpeta de customerImages
			$customerImageFile = "customerimages/".$theRepositoryName.'/'.$anImageURL;
			$aReturnValue = $customerImageFile;
			/*if (file_exists($customerImageFile)) {
				$aReturnValue = $customerImageFile;
			}
			else {
				//Ahora verifica contra la ruta de imagenes default
				$customerImageFile = "images/".$anImageURL;
				if (file_exists($customerImageFile)) {
					$aReturnValue = $customerImageFile;
				}
			}
			*/
		}
		//@JAPR
	}
	
	return $aReturnValue;
}
function SearchFormReferenceInMetadata($aRepository, $aSurveyID)
{	
	$strReturn="";
	$i=1;
	//Validar si hay algún planificador que tenga referencia a la forma
	$agendaSchedNames = array();
	$sql = "SELECT A.AgendaSchedName FROM SI_SV_SurveyAgendaScheduler A
		INNER JOIN SI_SV_SurveyAgendaSchedDet B
        ON A.AgendaSchedID=B.AgendaSchedID
        WHERE B.SurveyID = ".$aSurveyID;
	$aRS = $aRepository->DataADOConnection->Execute($sql);

	if ($aRS) {
		while(!$aRS->EOF) {		
			$agendaSchedNames[] = $i.". ".trim($aRS->fields["agendaschedname"])." (".translate("Agendas Scheduler").")";
			$i++;
			$aRS->MoveNext();
		}
	}
	
	//Validar si hay alguna agenda que tenga referencia a la forma
	$agendaNames = array();
	$sql = "SELECT DISTINCT A.AgendaName FROM SI_SV_SurveyAgenda A
		INNER JOIN SI_SV_SurveyAgendaDet B
        ON A.AgendaID=B.AgendaID
        WHERE B.SurveyID = ".$aSurveyID;
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS) {
		while(!$aRS->EOF) {		
			$agendaNames[] = $i.". ".trim($aRS->fields["agendaname"])." (".translate("Agenda").")";
			$i++;
			$aRS->MoveNext();
		}
	}
	
	if(count($agendaSchedNames)>0)
	{	//La forma está siendo usada en planificadores, se formará la cadena que indique en cuales planificadores se está utilizando
		$strReturn.="\r\n".implode("\r\n\t",$agendaSchedNames);
	}
	
	if (count($agendaNames)>0)
	{	//La forma está siendo usada en agendas, se formará la cadena que indique en cuales agendas se está utilizando
		$strReturn.="\r\n".implode("\r\n\t",$agendaNames);
	}
	
	return $strReturn;
}

//@JAPR 2015-01-20: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se utiliza como configuración directa para advertir al usuario
/* Esta función consulta la BD en busca de campos donde se utilicen los IDs del objeto indicado,
aunque si se especifica el parámetro $aFieldsColl entonces limita la búsqueda exclusivamente a esa combinación, regresando una lista de los 
nombres de objeto donde se utilizan para que se pueda advertir antes de eliminar la pregunta correspondiente
Regresa un string vacío si no hay ocurrencias de objetos (mediante el parámetro $bReturArray se puede regresar el array de ocurrencias)
El parámetro $anObjectID indica el ID de la pregunta, el cual se usará automáticamente en los patrones pre-generados
El parámetro $anObjectType indica el tipo de objeto, el cual se usará automáticamente en los patrones pre-generados
Para facilidad y compatibilidad, sólo se ejecutará este proceso si la metadata se encuentra migrada a partir de la versión donde todos los
campos y tablas ya existían por primera vez, sólo nuevos campos y tablas agregados posteriormente deberán ser validados contra la versión del
la Metadata
El parámetro $bGetIDs permitirá que la respuesta obtenida sólo regrese el ID de los objetos donde se encontraron estos patrones, sin especificar
exactamente en que propiedad se encuentran. Se debe usar en conjunto con $bReturArray = true para que sea útil
*/
function SearchObjectReferenceInMetadata($aRepository, $aSurveyID, $anObjectID, $anObjectType = otyQuestion, $aFieldsColl = null, $bReturArray = false, $bGetIDs = false) {
	$anObjectID = (int) $anObjectID;
	$anObjectType = (int) $anObjectType;
	if ($bGetIDs) {
		$bReturArray = true;
	}
	
	$arrResult = array();
	if (($anObjectID < 0 || $anObjectType < 0) || getMDVersion() < esvDeleteObjectsValidation) {
		return ($bReturArray)?$arrResult:'';
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchObjectReferenceInMetadata: Processing ObjectType == {$anObjectType}, ObjectID == {$anObjectID}");
	}
	
	//Verifica si se recibió una colección de campos/tablas
	$blnAddTablesFields = false;
	if (is_null($aFieldsColl) || !is_array($aFieldsColl) || count($aFieldsColl) == 0) {
		$blnAddTablesFields = true;
		$aFieldsColl = array();
	}
	
	$arrSurveyNames = array();
	$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);
	foreach ($surveyCollection->Collection as $objSurvey) {
		$arrSurveyNames[$objSurvey->SurveyID] = $objSurvey->SurveyName;
	}
	
	//Array exclusivo para las opciones de respuesta ya que se necesita para obtener la referencia a la pregunta a la que pertenecen
	$arrQuestionsByID = array();
	$objQuestionColl = BITAMQuestionCollection::NewInstanceIncludeTheseTypes($aRepository, $aSurveyID, array(qtpSingle, qtpCallList, qtpMulti));
	if (!is_null($objQuestionColl)) {
		foreach ($objQuestionColl->Collection as $objQuestion) {
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
		}
	}
	
	$objDataSourceMember = null;
	//@JAPR 2015-12-08: Agregada la validación de los destinos de datos (#4IA5BE)
	//Primero debe verificar si existe o no la tabla de eBavel, ya que el query de validación realiza UNIONs y no puede fallar ninguno de los elementos del mismo
	$blnHaseBavel = false;
	$streBavelSpace = '';
	$sql = "SELECT id_fieldform 
		FROM {$aRepository->ADOConnection->databaseName}.SI_FORMS_FIELDSFORMS 
		WHERE 1 = 2";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$blnHaseBavel = true;
		$streBavelSpace = $aRepository->ADOConnection->databaseName;
	}
	//@JAPR
	
	//Prepara los patrones pregenerados según el tipo de objeto a buscar
	$objPatternColl = array();
	$strRegExpMatch = '';
	switch ($anObjectType) {
		case otyQuestion:
			if ($blnAddTablesFields) {
				$aFieldsColl['SI_SV_Section'] = array();
				$aFieldsColl['SI_SV_Section']['ID'] = 'SectionID';
				$aFieldsColl['SI_SV_Section']['Type'] = otySection;
				$aFieldsColl['SI_SV_Section']['Name'] = 'SectionName';
				$aFieldsColl['SI_SV_Section']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Section']['Fields'] = array();
				$aFieldsColl['SI_SV_Section']['Fields']['SelectorQuestionID'] = array('SelectorQuestionID', translate("Selector question"));
				$aFieldsColl['SI_SV_Section']['Fields']['SourceQuestionID'] = array('SourceQuestionID', translate("Source question")." (".translate("Section").")");
				//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if ( getMDVersion() < esvSketchPlus ) {
					$aFieldsColl['SI_SV_Section']['Fields']['XPosQuestionID'] = array('XPosQuestionID', translate("X Coord"));
					$aFieldsColl['SI_SV_Section']['Fields']['YPosQuestionID'] = array('YPosQuestionID', translate("Y Coord"));
					$aFieldsColl['SI_SV_Section']['Fields']['ItemNameQuestionID'] = array('ItemNameQuestionID', translate("Name"));
				}
				//@JAPR
				$aFieldsColl['SI_SV_Question'] = array();
				$aFieldsColl['SI_SV_Question']['ID'] = 'QuestionID';
				$aFieldsColl['SI_SV_Question']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_Question']['Name'] = 'QuestionText';
				$aFieldsColl['SI_SV_Question']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Question']['Fields'] = array();
				$aFieldsColl['SI_SV_Question']['Fields']['FatherQuestionID'] = array('FatherQuestionID', translate("Source question")." (".translate("Question")." - ".translate("Parent").")");
				$aFieldsColl['SI_SV_Question']['Fields']['ChildQuestionID'] = array('ChildQuestionID', translate("Source question")." (".translate("Question")." - ".translate("Child").")");
				$aFieldsColl['SI_SV_Question']['Fields']['SourceQuestionID'] = array('SourceQuestionID', translate("Other question")." (".translate("Question").")");
				$aFieldsColl['SI_SV_Question']['Fields']['SourceSimpleQuestionID'] = array('SourceSimpleQuestionID', translate("Exclude question")." (".translate("Question").")");
				$aFieldsColl['SI_SV_Question']['Fields']['sharedQuestionID'] = array('sharedQuestionID', translate("Copy question")." (".translate("Question").")");
				//@JAPR 2015-12-08: Agregada la validación de los destinos de datos (#4IA5BE)
				if ($blnHaseBavel) {
					$aFieldsColl['SI_SV_DataDestinationsMapping'] = array();
					$aFieldsColl['SI_SV_DataDestinationsMapping']['ID'] = 'id_datadestinations';
					$aFieldsColl['SI_SV_DataDestinationsMapping']['Type'] = otyDataDestinations;
					$aFieldsColl['SI_SV_DataDestinationsMapping']['Name'] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS.label";
					$aFieldsColl['SI_SV_DataDestinationsMapping']['MasterID'] = 'id_survey';
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'] = array();
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'] = array();
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'][] = "SI_SV_DataDestinations";
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'][] = "SI_SV_DataDestinations.id = SI_SV_DataDestinationsMapping.id_datadestinations";
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'][] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS";
					$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'][] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS.id = SI_SV_DataDestinationsMapping.fieldformid";
					$aFieldsColl['SI_SV_DataDestinationsMapping']['Fields'] = array();
					$aFieldsColl['SI_SV_DataDestinationsMapping']['Fields']['id_question'] = array('Mapping', translate("Mapping eBavel"));
				}
				//@JAPR 2015-12-08: Agregada la validación de los mapeos de Modelos de Artus (#4IA5BE)
				$aFieldsColl['SI_SV_ModelArtus_Mapping'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['ID'] = 'SI_SV_ModelArtus_Mapping.id';
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['Type'] = otyArtusModels;
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['Name'] = "SI_SV_ModelArtus.name";
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['MasterID'] = 'SI_SV_ModelArtus.id_survey';
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['JoinTables'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['JoinFields'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['JoinTables'][] = "SI_SV_ModelArtus";
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['JoinFields'][] = "SI_SV_ModelArtus.id_modelArtus = SI_SV_ModelArtus_Mapping.id_modelArtus";
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['Fields'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Mapping']['Fields']['id_question'] = array('ModelMapping', translate("Model")." - ".translate("Mapping eBavel"));
				$aFieldsColl['SI_SV_ModelArtus_Questions'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Questions']['ID'] = 'SI_SV_ModelArtus_Questions.id';
				$aFieldsColl['SI_SV_ModelArtus_Questions']['Type'] = otyArtusModels;
				$aFieldsColl['SI_SV_ModelArtus_Questions']['Name'] = "SI_SV_ModelArtus.name";
				$aFieldsColl['SI_SV_ModelArtus_Questions']['MasterID'] = 'SI_SV_ModelArtus.id_survey';
				$aFieldsColl['SI_SV_ModelArtus_Questions']['JoinTables'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Questions']['JoinFields'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Questions']['JoinTables'][] = "SI_SV_ModelArtus";
				$aFieldsColl['SI_SV_ModelArtus_Questions']['JoinFields'][] = "SI_SV_ModelArtus.id_modelArtus = SI_SV_ModelArtus_Questions.id_modelArtus";
				$aFieldsColl['SI_SV_ModelArtus_Questions']['Fields'] = array();
				$aFieldsColl['SI_SV_ModelArtus_Questions']['Fields']['id_question'] = array('ModelMapping', translate("Model"));
				//@JAPR
				$aFieldsColl['SI_SV_ShowQuestions'] = array();
				$aFieldsColl['SI_SV_ShowQuestions']['ID'] = 'SI_SV_ShowQuestions.QuestionID';
				$aFieldsColl['SI_SV_ShowQuestions']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_ShowQuestions']['Name'] = 'ShowID';
				$aFieldsColl['SI_SV_ShowQuestions']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_ShowQuestions']['Parent'] = 'SI_SV_ShowQuestions.QuestionID';
				$aFieldsColl['SI_SV_ShowQuestions']['JoinTables'] = 'SI_SV_Question';
				$aFieldsColl['SI_SV_ShowQuestions']['JoinFields'] = 'SI_SV_ShowQuestions.QuestionID = SI_SV_Question.QuestionID';
				$aFieldsColl['SI_SV_ShowQuestions']['Fields'] = array();
				$aFieldsColl['SI_SV_ShowQuestions']['Fields']['ShowQuestionID'] = array('ShowQuestionID', translate("Show Questions"));
			}
			break;
		case otySection:
			if ($blnAddTablesFields) {
				$aFieldsColl['SI_SV_Section'] = array();
				$aFieldsColl['SI_SV_Section']['ID'] = 'SectionID';
				$aFieldsColl['SI_SV_Section']['Type'] = otySection;
				$aFieldsColl['SI_SV_Section']['Name'] = 'SectionName';
				$aFieldsColl['SI_SV_Section']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Section']['Fields'] = array();
				$aFieldsColl['SI_SV_Section']['Fields']['NextSectionID'] = array('NextSectionID', translate("Next section"));
				$aFieldsColl['SI_SV_Section']['Fields']['SourceSectionID'] = array('SourceSectionID', translate("Source section")." (".translate("Section").")");
				$aFieldsColl['SI_SV_Question'] = array();
				$aFieldsColl['SI_SV_Question']['ID'] = 'QuestionID';
				$aFieldsColl['SI_SV_Question']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_Question']['Name'] = 'QuestionText';
				$aFieldsColl['SI_SV_Question']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Question']['Fields'] = array();
				$aFieldsColl['SI_SV_Question']['Fields']['SourceSectionID'] = array('SourceSectionID', translate("Source section")." (".translate("Question").")");
				$aFieldsColl['SI_SV_Question']['Fields']['GoToSection'] = array('GoToSection', translate("Skip to section")." (".translate("Question").")");
				$aFieldsColl['SI_SV_QAnswers'] = array();
				$aFieldsColl['SI_SV_QAnswers']['ID'] = 'ConsecutiveID';
				$aFieldsColl['SI_SV_QAnswers']['Type'] = otyAnswer;
				$aFieldsColl['SI_SV_QAnswers']['Name'] = 'DisplayText';
				$aFieldsColl['SI_SV_QAnswers']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_QAnswers']['Parent'] = 'SI_SV_QAnswers.QuestionID';
				$aFieldsColl['SI_SV_QAnswers']['JoinTables'] = 'SI_SV_Question';
				$aFieldsColl['SI_SV_QAnswers']['JoinFields'] = 'SI_SV_QAnswers.QuestionID = SI_SV_Question.QuestionID';
				$aFieldsColl['SI_SV_QAnswers']['Fields'] = array();
				$aFieldsColl['SI_SV_QAnswers']['Fields']['NextSection'] = array('NextSection', translate("Skip to section")." (".translate("Question Option").")");
			}
			break;
		case otyDataSource:
			if ($blnAddTablesFields) {
				$aFieldsColl['SI_SV_Section'] = array();
				$aFieldsColl['SI_SV_Section']['ID'] = 'SectionID';
				$aFieldsColl['SI_SV_Section']['Type'] = otySection;
				$aFieldsColl['SI_SV_Section']['Name'] = 'SectionName';
				$aFieldsColl['SI_SV_Section']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Section']['Fields'] = array();
				$aFieldsColl['SI_SV_Section']['Fields']['DataSourceID'] = array('DataSourceID', translate("Catalog")." (".translate("Section").")");
				$aFieldsColl['SI_SV_Question'] = array();
				$aFieldsColl['SI_SV_Question']['ID'] = 'QuestionID';
				$aFieldsColl['SI_SV_Question']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_Question']['Name'] = 'QuestionText';
				$aFieldsColl['SI_SV_Question']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Question']['Fields'] = array();
				$aFieldsColl['SI_SV_Question']['Fields']['DataSourceID'] = array('DataSourceID', translate("Catalog")." (".translate("Question").")");
				//Falta aplicar a los filtros dinámicos, ya que ahora esos son por CatalogID y aquí se recibe el DataSourceID
				$aFieldsColl['SI_SV_SurveyAgendaScheduler'] = array();
				$aFieldsColl['SI_SV_SurveyAgendaScheduler']['ID'] = 'AgendaSchedID';
				$aFieldsColl['SI_SV_SurveyAgendaScheduler']['Type'] = otyAgendasScheduler;
				$aFieldsColl['SI_SV_SurveyAgendaScheduler']['Name'] = 'AgendaSchedName';
				$aFieldsColl['SI_SV_SurveyAgendaScheduler']['Fields'] = array();
				$aFieldsColl['SI_SV_SurveyAgendaScheduler']['Fields']['CatalogID'] = array('CatalogID', translate("Catalog")." (".translate("Agendas Scheduler").")");
				$aFieldsColl['si_sv_checkpoint'] = array();
				$aFieldsColl['si_sv_checkpoint']['ID'] = 'id';
				$aFieldsColl['si_sv_checkpoint']['Type'] = otyAgendasCheckpoint;
				$aFieldsColl['si_sv_checkpoint']['Name'] = 'name';
				$aFieldsColl['si_sv_checkpoint']['Fields'] = array();
				$aFieldsColl['si_sv_checkpoint']['Fields']['catalog_id'] = array('catalog_id', translate("Catalog")." (".translate("Check point").")");
			}
			break;
		case otyDataSourceMember:
			if ($blnAddTablesFields) {
				$aFieldsColl['SI_SV_Section'] = array();
				$aFieldsColl['SI_SV_Section']['ID'] = 'SectionID';
				$aFieldsColl['SI_SV_Section']['Type'] = otySection;
				$aFieldsColl['SI_SV_Section']['Name'] = 'SectionName';
				$aFieldsColl['SI_SV_Section']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Section']['Fields'] = array();
				$aFieldsColl['SI_SV_Section']['Fields']['DataSourceMemberID'] = array('DataSourceMemberID', translate("Attribute")." (".translate("Section").")");
				$aFieldsColl['SI_SV_Question'] = array();
				$aFieldsColl['SI_SV_Question']['ID'] = 'QuestionID';
				$aFieldsColl['SI_SV_Question']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_Question']['Name'] = 'QuestionText';
				$aFieldsColl['SI_SV_Question']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_Question']['Fields'] = array();
				$aFieldsColl['SI_SV_Question']['Fields']['DataSourceMemberID'] = array('DataSourceMemberID', translate("Attribute")." (".translate("Question").")");
				$aFieldsColl['SI_SV_Question']['Fields']['DataMemberLatitudeID'] = array('DataMemberLatitudeID', translate("Latitude attribute"));
				$aFieldsColl['SI_SV_Question']['Fields']['DataMemberLongitudeID'] = array('DataMemberLongitudeID', translate("Longitude attribute"));
				$aFieldsColl['si_sv_checkpointattr'] = array();
				$aFieldsColl['si_sv_checkpointattr']['ID'] = 'checkpoint_id';
				$aFieldsColl['si_sv_checkpointattr']['Type'] = otyAgendasCheckpoint;
				$aFieldsColl['si_sv_checkpointattr']['Name'] = 'checkpoint_id';
				$aFieldsColl['si_sv_checkpointattr']['MasterName'] = 'name';
				$aFieldsColl['si_sv_checkpointattr']['JoinTables'] = 'si_sv_checkpoint';
				$aFieldsColl['si_sv_checkpointattr']['JoinFields'] = 'si_sv_checkpointattr.checkpoint_id = si_sv_checkpoint.id';
				$aFieldsColl['si_sv_checkpointattr']['Fields'] = array();
				$aFieldsColl['si_sv_checkpointattr']['Fields']['attr_id'] = array('attr_id', translate("Attribute")." (".translate("Check point").")");
				/*$aFieldsColl['SI_SV_DataSourceFilter_Det'] = array();
				$aFieldsColl['SI_SV_DataSourceFilter_Det']['ID'] = 'FilterID';
				$aFieldsColl['SI_SV_DataSourceFilter_Det']['Type'] = otyAgendasCheckpoint;
				$aFieldsColl['SI_SV_DataSourceFilter_Det']['Name'] = 'FilterdetID';
				$aFieldsColl['SI_SV_DataSourceFilter_Det']['Fields'] = array();
				$aFieldsColl['SI_SV_DataSourceFilter_Det']['Fields']['DataSourceMemberID'] = array('DataSourceMemberID', translate("Attribute")." (".translate("Filter").")");*/
				$aFieldsColl['SI_SV_QuestionMembers'] = array();
				$aFieldsColl['SI_SV_QuestionMembers']['ID'] = 'SI_SV_QuestionMembers.QuestionID';
				$aFieldsColl['SI_SV_QuestionMembers']['Type'] = otyQuestion;
				$aFieldsColl['SI_SV_QuestionMembers']['Name'] = 'QuestionText';
				$aFieldsColl['SI_SV_QuestionMembers']['MasterID'] = 'SurveyID';
				$aFieldsColl['SI_SV_QuestionMembers']['JoinTables'] = 'SI_SV_Question';
				$aFieldsColl['SI_SV_QuestionMembers']['JoinFields'] = 'SI_SV_QuestionMembers.QuestionID = SI_SV_Question.QuestionID';
				$aFieldsColl['SI_SV_QuestionMembers']['Fields'] = array();
				$aFieldsColl['SI_SV_QuestionMembers']['Fields']['MemberID'] = array('MemberID', translate("Attribute")." (".translate("Question").")");
			}
			break;
	}
	
	/*if (count($objPatternColl) == 0) {
		return ($bReturArray)?$arrResult:'';
	}*/
	
	//Genera el estatuto SQL en base a las tablas y campos especificados
	//Primero obtiene el total de campos para permitir hacer el UNION de todos los casos y así procesar únicamente un recordset
	$objAllFields = array();
	$objSELECTFields = array();
	$objWHEREByTable = array();
	$strIDFieldID = 'ID';
	$strTypeFieldID = 'Type';
	$strNameFieldID = 'Name';
	$strMasterNameFieldID = 'MasterName';
	$strMasterIDFieldID = 'MasterID';
	$strParentNameFieldID = 'Parent';
	$strJoinTablesFieldID = 'JoinTables';
	$strJoinFieldsFieldID = 'JoinFields';
	$strIDField = $aRepository->DataADOConnection->Quote($strIDFieldID);
	$strTypeField = $aRepository->DataADOConnection->Quote($strTypeFieldID);
	$strNameField = $aRepository->DataADOConnection->Quote($strNameFieldID);
	$strMasterNameField = $aRepository->DataADOConnection->Quote($strMasterNameFieldID);
	$strMasterIDField = $aRepository->DataADOConnection->Quote($strMasterIDFieldID);
	$strParentField = $aRepository->DataADOConnection->Quote($strParentNameFieldID);
	$objSELECTFields[$strIDFieldID] = '0 AS '.$strIDField;
	$objSELECTFields[$strTypeFieldID] = '0 AS '.$strTypeField;
	$objSELECTFields[$strNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strNameField;
	$objSELECTFields[$strMasterNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strMasterNameField;
	$objSELECTFields[$strMasterIDFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strMasterIDField;
	$objSELECTFields[$strParentNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strParentField;
	foreach ($aFieldsColl as $strTableName => $objFieldsColl) {
		$arrWHEREFields = array();
		foreach ($objFieldsColl['Fields'] as $strFieldName => $objFieldDef) {
			$strFieldID = $strTableName.$strFieldName;
			$objAllFields[$strFieldID] = $objFieldDef;
			//En este caso no se requiere el campo de la búsqueda, sólo saber si cumple o no la condición
			//$objSELECTFields[$strFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$aRepository->DataADOConnection->Quote($strFieldID);
			/*foreach ($objPatternColl as $strPattern) {
				$arrWHEREFields[] = $strFieldName.' = '.$anObjectID;
			}*/
		}
		
		//Por cada tabla se tiene que armar un WHERE exclusivamente con sus campos
		//$objWHEREByTable[$strTableName] = implode(' OR ', $arrWHEREFields);
	}
	
	//Genera el estatuto SQL correspondiente a cada tabla, agregando los campos que no son los 
	$sql = '';
	$sqlUNIONAnd = '';
	foreach ($aFieldsColl as $strTableName => $objFieldsColl) {
		$arrSELECT = $objSELECTFields;
		$arrSELECT[$strIDFieldID] = (string) @$objFieldsColl[$strIDFieldID].' AS '.$strIDField;
		$arrSELECT[$strTypeFieldID] = (string) @$objFieldsColl[$strTypeFieldID].' AS '.$strTypeField;
		$arrSELECT[$strNameFieldID] = (string) @$objFieldsColl[$strNameFieldID].' AS '.$strNameField;
		if (isset($objFieldsColl[$strMasterNameFieldID])) {
			$arrSELECT[$strMasterNameFieldID] = $objFieldsColl[$strMasterNameFieldID].' AS '.$strMasterNameField;
		}
		if (isset($objFieldsColl[$strMasterIDFieldID])) {
			$arrSELECT[$strMasterIDFieldID] = $objFieldsColl[$strMasterIDFieldID].' AS '.$strMasterIDField;
		}
		if (isset($objFieldsColl[$strParentNameFieldID])) {
			$arrSELECT[$strParentNameFieldID] = $objFieldsColl[$strParentNameFieldID].' AS '.$strParentField;
		}
		//Si está asignado un join, lo prepara en este punto
		$strJoinTables = '';
		if (isset($objFieldsColl[$strJoinTablesFieldID]) && isset($objFieldsColl[$strJoinFieldsFieldID])) {
			//@JAPR 2015-12-08: Modificados los Joins para permitir un array
			$arrJoinTables = $objFieldsColl[$strJoinTablesFieldID];
			if (!is_array($objFieldsColl[$strJoinTablesFieldID])) {
				$arrJoinTables = array($objFieldsColl[$strJoinTablesFieldID]);
			}
			
			$arrJoinFields = $objFieldsColl[$strJoinFieldsFieldID];
			if (!is_array($objFieldsColl[$strJoinFieldsFieldID])) {
				$arrJoinFields = array($objFieldsColl[$strJoinFieldsFieldID]);
			}
			
			foreach ($arrJoinTables as $intJoinIdx => $strJoinTable) {
				$strJoinFields = (string) @$arrJoinFields[$intJoinIdx];
				$strJoinTables .= " LEFT JOIN {$strJoinTable} ON {$strJoinFields} ";
			}
			//@JAPR
		}
		
		//En este caso no se requiere el campo de la búsqueda, sólo saber si cumple o no la condición
		/*foreach ($objFieldsColl['Fields'] as $strFieldName => $objFieldDef) {
			//Por cada tabla se tiene que agregar la colección completa de campos con un valor '' por default, excepto el campo específico que se
			//está procesando en esta iteración, así que se hace un clonado del array original que compone el SELECT
			$strFieldID = $strTableName.$strFieldName;
			$arrSELECT[$strFieldID] = $strFieldName.' AS '.$aRepository->DataADOConnection->Quote($strFieldID);
		}*/
		
		foreach ($objFieldsColl['Fields'] as $strFieldName => $objFieldDef) {
			$strFieldID = $strTableName.$strFieldName;
			$sql .= $sqlUNIONAnd."
				SELECT ".implode(', ',$arrSELECT).", ".$aRepository->DataADOConnection->Quote($objFieldDef[1])." AS Label, ".
					$aRepository->DataADOConnection->Quote($strFieldID)." AS FieldName 
				FROM {$strTableName} {$strJoinTables} 
				WHERE ".$strFieldName.' = '.$anObjectID;
			$sqlUNIONAnd = ' UNION ';
		}
	}
	
	//Ejecuta el Query completo y recorre todos los campos para obtener la lista completa de los que tienen referencias
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchObjectReferenceInMetadata: Base SQL == {$sql}");
	}
	$intNumRef = 1;
	$arrGroupedResult = array();
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		while(!$aRS->EOF) {
			$strFieldLabel = (string) @$aRS->fields[strtolower("Label")];
			$intObjectID = (int) @$aRS->fields[strtolower($strIDFieldID)];
			$intObjectType = (int) @$aRS->fields[strtolower($strTypeFieldID)];
			$strObjectName = (string) @$aRS->fields[strtolower($strNameFieldID)];
			$strMasterName = (string) @$aRS->fields[strtolower($strMasterNameFieldID)];
			$intMasterID = (int) @$aRS->fields[strtolower($strMasterIDFieldID)];
			$intParentID = (int) @$aRS->fields[strtolower($strParentNameFieldID)];
			$strFieldID = (string) @$aRS->fields[strtolower("FieldName")];
			//Obtiene el nombre de la forma, generalmente el MasterName es la forma, si no estuviera asignado pero hubiera un MasterID, se asumirá que ese es el ID de la forma end
			//cuestión, y si no hay un MasterID entonces no es un elemento de la forma sino algo diferente como un Checkpoint por lo que se usará directo el MasterName
			$strFormName = $strMasterName;
			if (trim($strFormName) == '' && $intMasterID > 0) {
				$strFormName = $arrSurveyNames[$intMasterID];
			}
			
			if ($bGetIDs) {
				//En este caso se regresan los IDs agrupados por categorías de objetos donde se encontró el patrón
				$strObjectTypeName = '';
				switch ($intObjectType) {
					case otyAnswer:
						if (!isset($arrGroupedResult['Questions'])) {
							$arrGroupedResult['Questions'] = array();
						}
						$arrGroupedResult['Questions'][$intParentID] = $intParentID;
						if (!isset($arrGroupedResult['Answers'])) {
							$arrGroupedResult['Answers'] = array();
						}
						$arrGroupedResult['Answers'][$intObjectID] = $intObjectID;
						break;
						
					default:
						switch ($intObjectType) {
							case otySurvey:
								$strObjectTypeName = 'Surveys';
								break;
							case otySection:
								$strObjectTypeName = 'Sections';
								break;
							case otyQuestion:
								$strObjectTypeName = 'Questions';
								break;
							case otyAgendasScheduler:
								$strObjectTypeName = 'Agendas Scheduler';
								break;
							case otyAgendasCheckpoint:
								$strObjectTypeName = 'Check point';
								break;
						}
						
						if (!isset($arrGroupedResult[$strObjectTypeName])) {
							$arrGroupedResult[$strObjectTypeName] = array();
						}
						$arrGroupedResult[$strObjectTypeName][$intObjectID] = $intObjectID;
						break;
				}
			}
			else {
				//En este caso se regresan las propiedades específicas y los nombres de los objetos donde se encontró el patrón
				//@JAPR 2015-10-23: Modificada la manera de reportar los detalles, ahora se concatenará de la siguiente forma:
				/*
					- Si es un elemento de la forma:
					NombreForma - NombreSeccion/Pregunta/Filtro/etc [Opcional: Si es una opción de respuesta se agrega - NombrePregunta] - NombreDePropiedadDondeSeUsa
					- Si es un CheckPoint o AgendaScheduler
					NombreCheckPoint/AgendaScheduler - Propiedad
				*/
				/*
				if (!isset($arrGroupedResult[$strFieldID])) {
					//Si es la primera vez entonces agrega la descripción de la opción
					$arrGroupedResult[$strFieldID] = array("-".$strFieldLabel.":");
				}
				*/
				//Agrega el campo que aún contiene al elemento buscado
				$strObjectParentName = '';
				$objQuestion = @$arrQuestionsByID[$intParentID];
				if (!is_null($objQuestion)) {
					$strObjectParentName = (string) @$objQuestion->QuestionText;
				}
				//$arrGroupedResult[$strFieldID][] = "   ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strObjectName;
				$arrGroupedResult[] = $intNumRef."= ".$strFormName." - ".$strObjectName." - ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strFieldLabel;
				$intNumRef++;
				//@JAPR
			}
			
			$aRS->MoveNext();
		}
	}
	//@JAPR 2015-12-09: Agregado el reporte de error sólo en modo de depuración
	else {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n<br>\r\n SearchObjectReferenceInMetadata error: ".$aRepository->DataADOConnection->ErrorMsg());
		}
	}
	//@JAPR
	
	//La tabla de mapeos se debe procesar por separado, ya que existen condiciones especiales como un mapeo directo de atributos los cuales
	//también pueden estar como variables mas complejas y son varias tablas las cuales no necesariamente tienen la misma estructura
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchObjectReferenceInMetadata: Grouped matches");
		PrintMultiArray($arrGroupedResult);
		if (count($arrGroupedResult) == 0) {
			echo("<br>\r\n");
		}
	}
	
	//Finalmente si se encontraron elementos que aún contienen referencias, arma el String final de error
	if ($bGetIDs) {
		$arrResult = $arrGroupedResult;
	}
	else {
		//@JAPR 2015-10-23: Modificada la manera de reportar los detalles, ahora se concatenará de la forma descrita arriba
		$arrResult = $arrGroupedResult;
		/*
		foreach ($arrGroupedResult as $strFieldID => $arrReferences) {
			$arrResult = array_merge($arrResult, $arrGroupedResult[$strFieldID]);
		}
		*/
		//@JAPR
	}
	
	if (!$bReturArray) {
		$strReturn = implode("\r\n", $arrResult);
		$arrResult = $strReturn;
	}
	
	return $arrResult;
}

//@JAPR 2015-01-20: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
//si es utilizado y advertir al usuario (#75TPJE)
/* Esta función consulta la BD en busca de patrones específicos de preguntas campos/tablas donde se sabe que se pueden utilizar dichas variables,
aunque si se especifica el parámetro $aFieldsColl entonces limita la búsqueda exclusivamente a esa combinación, regresando una lista de los 
nombres de objeto donde se utilizan para que se pueda advertir antes de eliminar la pregunta correspondiente
Regresa un string vacío si no hay ocurrencias de objetos (mediante el parámetro $bReturArray se puede regresar el array de ocurrencias)
El parámetro $anObjectID indica el ID de la pregunta, el cual se usará automáticamente en los patrones pre-generados
El parámetro $anObjectType indica el tipo de objeto, el cual se usará automáticamente en los patrones pre-generados
Para facilidad y compatibilidad, sólo se ejecutará este proceso si la metadata se encuentra migrada a partir de la versión donde todos los
campos y tablas ya existían por primera vez, sólo nuevos campos y tablas agregados posteriormente deberán ser validados contra la versión del
la Metadata
El parámetro $bGetIDs permitirá que la respuesta obtenida sólo regrese el ID de los objetos donde se encontraron estos patrones, sin especificar
exactamente en que propiedad se encuentran. Se debe usar en conjunto con $bReturArray = true para que sea útil
*/
function SearchVariablePatternInMetadata($aRepository, $aSurveyID, $anObjectID, $anObjectType = otyQuestion, $aFieldsColl = null, $bReturArray = false, $bGetIDs = false) {
	require_once("section.inc.php");
	require_once("question.inc.php");
	
	$anObjectID = (int) $anObjectID;
	$anObjectType = (int) $anObjectType;
	if ($bGetIDs) {
		$bReturArray = true;
	}
	
	$arrResult = array();
	if (($anObjectID < 0 || $anObjectType < 0) || getMDVersion() < esvDeleteObjectsValidation) {
		return ($bReturArray)?$arrResult:'';
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchVariablePatternInMetadata: Processing ObjectType == {$anObjectType}, ObjectID == {$anObjectID}");
	}
	
	//Verifica si se recibió una colección de campos/tablas
	$blnAddTablesFields = false;
	if (is_null($aFieldsColl) || !is_array($aFieldsColl) || count($aFieldsColl) == 0) {
		$blnAddTablesFields = true;
		$aFieldsColl = array();
	}
	
	$arrSurveyNames = array();
	$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);
	foreach ($surveyCollection->Collection as $objSurvey) {
		$arrSurveyNames[$objSurvey->SurveyID] = $objSurvey->SurveyName;
	}
	
	//Array exclusivo para las opciones de respuesta ya que se necesita para obtener la referencia a la pregunta a la que pertenecen
	$arrQuestionsByID = array();
	$objQuestionColl = BITAMQuestionCollection::NewInstanceIncludeTheseTypes($aRepository, $aSurveyID, array(qtpSingle, qtpCallList, qtpMulti));
	if (!is_null($objQuestionColl)) {
		foreach ($objQuestionColl->Collection as $objQuestion) {
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
		}
	}
	
	$objDataSourceMember = null;
	
	//Prepara los patrones pregenerados según el tipo de objeto a buscar
	$objPatternColl = array();
	$strRegExpMatch = '';
	//@JAPR 2015-10-30: Validado que si se utiliza un Atributo por nombre, sólo se advierta si el objeto que lo usó es del mismo DataSource que se está validando
	$intDataSourceID = 0;
	$objSectionsCollbyID = array();
	$objQuestionsCollByID = array();
	//@JAPR
	switch ($anObjectType) {
		case otyQuestion:
			//$objPatternColl[] = '%@Q'.$anObjectID.'%';
			//@JAPR 2015-01-27: Corregido un bug, se había puesto mal el wildcart del patrón
			$objPatternColl[] = '%$Q'.$anObjectID.'.%';
			//$strRegExpMatch = "/([@]{1}[Q|q]{1}".$anObjectID."[\}\(])|([$]{1}[Q|q]{1}".$anObjectID."[\}\.])/";
			$strRegExpMatch = "/([$]{1}[Q|q]{1}".$anObjectID."[\}\.])/";
			break;
		case otySection:
			//$objPatternColl[] = '%@S'.$anObjectID.'%';
			//@JAPR 2015-01-27: Corregido un bug, se había puesto mal el wildcart del patrón
			$objPatternColl[] = '%$S'.$anObjectID.'.%';
			//$strRegExpMatch = "/([@]{1}[S|s]{1}".$anObjectID."[\}\(])|([$]{1}[S|s]{1}".$anObjectID."[\}\.])/";
			$strRegExpMatch = "/([$]{1}[S|s]{1}".$anObjectID."[\}\.])/";
			break;
		case otyDataSourceMember:
			$objPatternColl[] = '%@DMID'.$anObjectID.'%';
			$objDataSourceMember = BITAMDataSourceMember::NewInstanceWithID($aRepository, $anObjectID);
			if (!is_null($objDataSourceMember)) {
				$objPatternColl[] = '%.attr('.$objDataSourceMember->MemberName.')}%';
				$objPatternColl[] = '%.attr(\''.$objDataSourceMember->MemberName.'\')}%';
				$strRegExpMatch = "/([\{]{1}[@]{1}DMID".$anObjectID."[\}]{1})|([\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.attr(\([']?".$objDataSourceMember->MemberName."[']?\))){1}[}]{1})/i";
				//@JAPR 2015-10-30: Validado que si se utiliza un Atributo por nombre, sólo se advierta si el objeto que lo usó es del mismo DataSource que se está validando
				$intDataSourceID = $objDataSourceMember->DataSourceID;
			}
			break;
	}
	
	//@JAPR 2015-12-08: Agregada la validación de los destinos de datos (#4IA5BE)
	//Primero debe verificar si existe o no la tabla de eBavel, ya que el query de validación realiza UNIONs y no puede fallar ninguno de los elementos del mismo
	$blnHaseBavel = false;
	$streBavelSpace = '';
	$sql = "SELECT id_fieldform 
		FROM {$aRepository->ADOConnection->databaseName}.SI_FORMS_FIELDSFORMS 
		WHERE 1 = 2";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$blnHaseBavel = true;
		$streBavelSpace = $aRepository->ADOConnection->databaseName;
	}
	//@JAPR
	
	//@JAPR 2015-10-22: Ahora todos los tipos de objetos que puedan utilizarse en variables, se buscarán en todas las configuraciones que permitan variables, sin hace diferencia
	//por el tipo de objeto (independientemente de si sería o no lógico intentar usarlo en esa situación)
	if ($blnAddTablesFields) {
		$aFieldsColl['SI_SV_Survey'] = array();
		$aFieldsColl['SI_SV_Survey']['ID'] = 'SurveyID';
		$aFieldsColl['SI_SV_Survey']['Type'] = otySurvey;
		$aFieldsColl['SI_SV_Survey']['Name'] = 'SurveyName';
		$aFieldsColl['SI_SV_Survey']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_Survey']['Fields'] = array();
		$aFieldsColl['SI_SV_Survey']['Fields']['AdditionalEMails'] = array('AdditionalEMails', translate("Additional emails  for capture report delivery"));
		$aFieldsColl['SI_SV_SurveyFilter'] = array();
		$aFieldsColl['SI_SV_SurveyFilter']['ID'] = 'SurveyID';
		$aFieldsColl['SI_SV_SurveyFilter']['Type'] = otySurvey;
		$aFieldsColl['SI_SV_SurveyFilter']['Name'] = 'FilterName';
		$aFieldsColl['SI_SV_SurveyFilter']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_SurveyFilter']['Fields'] = array();
		$aFieldsColl['SI_SV_SurveyFilter']['Fields']['FilterText'] = array('FilterText', translate("Filtro")." (".translate("Survey").")");
		$aFieldsColl['SI_SV_Section'] = array();
		$aFieldsColl['SI_SV_Section']['ID'] = 'SectionID';
		$aFieldsColl['SI_SV_Section']['Type'] = otySection;
		$aFieldsColl['SI_SV_Section']['Name'] = 'SectionName';
		$aFieldsColl['SI_SV_Section']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_Section']['Fields'] = array();
		$aFieldsColl['SI_SV_Section']['Fields']['FormattedText'] = array('FormattedText', translate("Formatted text")." (".translate("Section").")");
		$aFieldsColl['SI_SV_Section']['Fields']['ShowCondition'] = array('ShowCondition', translate("Show condition"));
		$aFieldsColl['SI_SV_Section']['Fields']['HTMLFooter'] = array('HTMLFooter', translate("Customized footer"));
		$aFieldsColl['SI_SV_Section']['Fields']['HTMLHeader'] = array('HTMLHeader', translate("Customized header"));
		/*$aFieldsColl['SI_SV_SectionFilter'] = array();
		$aFieldsColl['SI_SV_SectionFilter']['ID'] = 'SectionID';
		$aFieldsColl['SI_SV_SectionFilter']['Type'] = otySection;
		$aFieldsColl['SI_SV_SectionFilter']['Name'] = 'FilterName';
		$aFieldsColl['SI_SV_SectionFilter']['Fields'] = array();
		$aFieldsColl['SI_SV_SectionFilter']['Fields']['FilterText'] = array('FilterText', translate("Filtro")." (".translate("Section").")");*/
		$aFieldsColl['SI_SV_Question'] = array();
		$aFieldsColl['SI_SV_Question']['ID'] = 'QuestionID';
		$aFieldsColl['SI_SV_Question']['Type'] = otyQuestion;
		$aFieldsColl['SI_SV_Question']['Name'] = 'QuestionText';
		$aFieldsColl['SI_SV_Question']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_Question']['Fields'] = array();
		$aFieldsColl['SI_SV_Question']['Fields']['QuestionText'] = array('QuestionText', translate("Question"));
		$aFieldsColl['SI_SV_Question']['Fields']['QuestionMessage'] = array('QuestionMessage', translate("Formatted text")." (".translate("Question").")");
		$aFieldsColl['SI_SV_Question']['Fields']['DefaultValue'] = array('DefaultValue', translate("Default value")." (".translate("Question").")");
		$aFieldsColl['SI_SV_Question']['Fields']['Formula'] = array('Formula', translate("Formula"));
		$aFieldsColl['SI_SV_Question']['Fields']['ShowCondition'] = array('ShowCondition', translate("Show condition"));
		$aFieldsColl['SI_SV_Question']['Fields']['ActionText'] = array('ActionText', translate("Action description")." (".translate("Question").")");
		$aFieldsColl['SI_SV_Question']['Fields']['ActionTitle'] = array('ActionTitle', translate("Action title")." (".translate("Question").")");
		$aFieldsColl['SI_SV_QAnswers'] = array();
		$aFieldsColl['SI_SV_QAnswers']['ID'] = 'ConsecutiveID';
		$aFieldsColl['SI_SV_QAnswers']['Type'] = otyAnswer;
		$aFieldsColl['SI_SV_QAnswers']['Name'] = 'DisplayText';
		$aFieldsColl['SI_SV_QAnswers']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_QAnswers']['Parent'] = 'SI_SV_QAnswers.QuestionID';
		$aFieldsColl['SI_SV_QAnswers']['JoinTables'] = 'SI_SV_Question';
		$aFieldsColl['SI_SV_QAnswers']['JoinFields'] = 'SI_SV_QAnswers.QuestionID = SI_SV_Question.QuestionID';
		$aFieldsColl['SI_SV_QAnswers']['Fields'] = array();
		$aFieldsColl['SI_SV_QAnswers']['Fields']['SI_SV_QAnswers.ActionText'] = array('ActionText', translate("Action description")." (".translate("Question Option").")");
		$aFieldsColl['SI_SV_QAnswers']['Fields']['SI_SV_QAnswers.ActionTitle'] = array('ActionTitle', translate("Action title")." (".translate("Question Option").")");
		$aFieldsColl['SI_SV_QAnswers']['Fields']['HTMLText'] = array('HTMLText', translate("Formatted text")." (".translate("Question Option").")");
		$aFieldsColl['SI_SV_QAnswers']['Fields']['SI_SV_QAnswers.DefaultValue'] = array('DefaultValue', translate("Default value")." (".translate("Question Option").")");
		$aFieldsColl['SI_SV_QuestionFilter'] = array();
		$aFieldsColl['SI_SV_QuestionFilter']['ID'] = 'SI_SV_QuestionFilter.QuestionID';
		$aFieldsColl['SI_SV_QuestionFilter']['Type'] = otyQuestion;
		$aFieldsColl['SI_SV_QuestionFilter']['Name'] = 'FilterName';
		$aFieldsColl['SI_SV_QuestionFilter']['MasterID'] = 'SurveyID';
		$aFieldsColl['SI_SV_QuestionFilter']['JoinTables'] = 'SI_SV_Question';
		$aFieldsColl['SI_SV_QuestionFilter']['JoinFields'] = 'SI_SV_QuestionFilter.QuestionID = SI_SV_Question.QuestionID';
		$aFieldsColl['SI_SV_QuestionFilter']['Fields'] = array();
		$aFieldsColl['SI_SV_QuestionFilter']['Fields']['FilterText'] = array('FilterText', translate("Filtro")." (".translate("Question").")");
		//@JAPR 2015-12-08: Agregada la validación de los destinos de datos (#4IA5BE)
		if ($blnHaseBavel) {
			$aFieldsColl['SI_SV_DataDestinationsMapping'] = array();
			$aFieldsColl['SI_SV_DataDestinationsMapping']['ID'] = 'id_datadestinations';
			$aFieldsColl['SI_SV_DataDestinationsMapping']['Type'] = otyDataDestinations;
			$aFieldsColl['SI_SV_DataDestinationsMapping']['Name'] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS.label";
			$aFieldsColl['SI_SV_DataDestinationsMapping']['MasterID'] = 'id_survey';
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'] = array();
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'] = array();
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'][] = "SI_SV_DataDestinations";
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'][] = "SI_SV_DataDestinations.id = SI_SV_DataDestinationsMapping.id_datadestinations";
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinTables'][] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS";
			$aFieldsColl['SI_SV_DataDestinationsMapping']['JoinFields'][] = "{$streBavelSpace}.SI_FORMS_FIELDSFORMS.id = SI_SV_DataDestinationsMapping.fieldformid";
			$aFieldsColl['SI_SV_DataDestinationsMapping']['Fields'] = array();
			$aFieldsColl['SI_SV_DataDestinationsMapping']['Fields']['formula'] = array('Formula', translate("Formula")." (".translate("Data destinations").")");
		}
		//@JAPR
	}
	
	if (count($objPatternColl) == 0) {
		return ($bReturArray)?$arrResult:'';
	}
	
	//Genera el estatuto SQL en base a las tablas y campos especificados
	//Primero obtiene el total de campos para permitir hacer el UNION de todos los casos y así procesar únicamente un recordset
	$objAllFields = array();
	$objSELECTFields = array();
	$objWHEREByTable = array();
	$strIDFieldID = 'ID';
	$strTypeFieldID = 'Type';
	$strNameFieldID = 'Name';
	$strMasterNameFieldID = 'MasterName';
	$strMasterIDFieldID = 'MasterID';
	$strParentNameFieldID = 'Parent';
	$strJoinTablesFieldID = 'JoinTables';
	$strJoinFieldsFieldID = 'JoinFields';
	$strIDField = $aRepository->DataADOConnection->Quote($strIDFieldID);
	$strTypeField = $aRepository->DataADOConnection->Quote($strTypeFieldID);
	$strNameField = $aRepository->DataADOConnection->Quote($strNameFieldID);
	$strMasterNameField = $aRepository->DataADOConnection->Quote($strMasterNameFieldID);
	$strMasterIDField = $aRepository->DataADOConnection->Quote($strMasterIDFieldID);
	$strParentField = $aRepository->DataADOConnection->Quote($strParentNameFieldID);
	$objSELECTFields[$strIDFieldID] = '0 AS '.$strIDField;
	$objSELECTFields[$strTypeFieldID] = '0 AS '.$strTypeField;
	$objSELECTFields[$strNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strNameField;
	$objSELECTFields[$strMasterNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strMasterNameField;
	$objSELECTFields[$strMasterIDFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strMasterIDField;
	$objSELECTFields[$strParentNameFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$strParentField;
	foreach ($aFieldsColl as $strTableName => $objFieldsColl) {
		$arrWHEREFields = array();
		foreach ($objFieldsColl['Fields'] as $strFieldName => $objFieldDef) {
			$strFieldID = $strTableName.$strFieldName;
			$objAllFields[$strFieldID] = $objFieldDef;
			$objSELECTFields[$strFieldID] = $aRepository->DataADOConnection->Quote('').' AS '.$aRepository->DataADOConnection->Quote($strFieldID);
			foreach ($objPatternColl as $strPattern) {
				$arrWHEREFields[] = $strFieldName.' LIKE '.$aRepository->DataADOConnection->Quote($strPattern);
			}
		}
		
		//Por cada tabla se tiene que armar un WHERE exclusivamente con sus campos
		$objWHEREByTable[$strTableName] = implode(' OR ', $arrWHEREFields);
	}
	
	//Genera el estatuto SQL correspondiente a cada tabla, agregando los campos que no son los 
	$sql = '';
	$sqlUNIONAnd = '';
	foreach ($aFieldsColl as $strTableName => $objFieldsColl) {
		$arrSELECT = $objSELECTFields;
		$arrSELECT[$strIDFieldID] = (string) @$objFieldsColl[$strIDFieldID].' AS '.$strIDField;
		$arrSELECT[$strTypeFieldID] = (string) @$objFieldsColl[$strTypeFieldID].' AS '.$strTypeField;
		$arrSELECT[$strNameFieldID] = (string) @$objFieldsColl[$strNameFieldID].' AS '.$strNameField;
		if (isset($objFieldsColl[$strMasterNameFieldID])) {
			$arrSELECT[$strMasterNameFieldID] = $objFieldsColl[$strMasterNameFieldID].' AS '.$strMasterNameField;
		}
		if (isset($objFieldsColl[$strMasterIDFieldID])) {
			$arrSELECT[$strMasterIDFieldID] = $objFieldsColl[$strMasterIDFieldID].' AS '.$strMasterIDField;
		}
		if (isset($objFieldsColl[$strParentNameFieldID])) {
			$arrSELECT[$strParentNameFieldID] = $objFieldsColl[$strParentNameFieldID].' AS '.$strParentField;
		}
		//Si está asignado un join, lo prepara en este punto
		$strJoinTables = '';
		if (isset($objFieldsColl[$strJoinTablesFieldID]) && isset($objFieldsColl[$strJoinFieldsFieldID])) {
			//@JAPR 2015-12-08: Modificados los Joins para permitir un array
			$arrJoinTables = $objFieldsColl[$strJoinTablesFieldID];
			if (!is_array($objFieldsColl[$strJoinTablesFieldID])) {
				$arrJoinTables = array($objFieldsColl[$strJoinTablesFieldID]);
			}
			
			$arrJoinFields = $objFieldsColl[$strJoinFieldsFieldID];
			if (!is_array($objFieldsColl[$strJoinFieldsFieldID])) {
				$arrJoinFields = array($objFieldsColl[$strJoinFieldsFieldID]);
			}
			
			foreach ($arrJoinTables as $intJoinIdx => $strJoinTable) {
				$strJoinFields = (string) @$arrJoinFields[$intJoinIdx];
				$strJoinTables .= " LEFT JOIN {$strJoinTable} ON {$strJoinFields} ";
			}
			//@JAPR
		}
		
		foreach ($objFieldsColl['Fields'] as $strFieldName => $objFieldDef) {
			//Por cada tabla se tiene que agregar la colección completa de campos con un valor '' por default, excepto el campo específico que se
			//está procesando en esta iteración, así que se hace un clonado del array original que compone el SELECT
			$strFieldID = $strTableName.$strFieldName;
			$arrSELECT[$strFieldID] = $strFieldName.' AS '.$aRepository->DataADOConnection->Quote($strFieldID);
		}
		
		$sql .= $sqlUNIONAnd."
			SELECT ".implode(', ',$arrSELECT)." 
			FROM {$strTableName} {$strJoinTables} 
			WHERE ".$objWHEREByTable[$strTableName];
		$sqlUNIONAnd = ' UNION ';
	}
	
	//Ejecuta el Query completo y recorre todos los campos para obtener la lista completa de los que tienen referencias
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchVariablePatternInMetadata: Base SQL == {$sql}");
	}
	$intNumRef = 1;
	$arrGroupedResult = array();
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		while(!$aRS->EOF) {
			foreach ($objAllFields as $strFieldID => $objFieldDef) {
				$strFieldName = strtolower($objFieldDef[0]);
				$strFieldLabel = $objFieldDef[1];
				$strFieldData = (string) @$aRS->fields[$strFieldID];
				$intObjectID = (int) @$aRS->fields[strtolower($strIDFieldID)];
				$intObjectType = (int) @$aRS->fields[strtolower($strTypeFieldID)];
				$strObjectName = (string) @$aRS->fields[strtolower($strNameFieldID)];
				$strMasterName = (string) @$aRS->fields[strtolower($strMasterNameFieldID)];
				$intMasterID = (int) @$aRS->fields[strtolower($strMasterIDFieldID)];
				$intParentID = (int) @$aRS->fields[strtolower($strParentNameFieldID)];
				//Obtiene el nombre de la forma, generalmente el MasterName es la forma, si no estuviera asignado pero hubiera un MasterID, se asumirá que ese es el ID de la forma end
				//cuestión, y si no hay un MasterID entonces no es un elemento de la forma sino algo diferente como un Checkpoint por lo que se usará directo el MasterName
				$strFormName = $strMasterName;
				if (trim($strFormName) == '' && $intMasterID > 0) {
					$strFormName = $arrSurveyNames[$intMasterID];
				}
				
				if ($strFieldData != '') {
					$blnValid = false;
					//Si contiene por lo menos en una ocasión alguno de los patrones válidos, agrega el elemento al array de resultados agrupados
					//@JAPR 2015-10-30: Validado que si se utiliza un Atributo por nombre, sólo se advierta si el objeto que lo usó es del mismo DataSource que se está validando
					if (preg_match($strRegExpMatch, $strFieldData)) {
						//Para el caso de los atributos, no basta si cumple o no con la variable, se debe verificar si en realidad el objeto en el que se usa utiliza el DataSource
						//del atributo que se está precesando, ya que si no simplemente es un atributo que se llama igual pero en otro DataSource y eso no debe impedir borrar este
						if ($intDataSourceID > 0 && $objDataSourceMember) {
							//Se trata de un atributo, primero verifica si se trata de una referencia de variable de atributo, si es así entonces si se debe impedir eliminar
							$strAttributeRegExpMatch = "/[\{]{1}[@]{1}DMID".$anObjectID."[\}]{1}/i";
							if (preg_match($strAttributeRegExpMatch, $strFieldData)) {
								//Se usa una variable de atributo directa, se debe marcar para no eliminarse
								$blnValid = true;
							}
							else {
								//En este caso la validación es mas compleja, deben obtenerse todas las posibles variables que usan este atributo y comprobar que el objeto de la variable
								//efectivamente utiliza el DataSource de este atributo
								$strAttributeRegExpMatch = "/[\{]{1}[$]{1}([Q|q|S|s]){1}([0-9]+)(\.attr(\([']?".$objDataSourceMember->MemberName."[']?\))){1}[}]{1}/i";
								$arrMatches = array();
								if (preg_match_all($strAttributeRegExpMatch, $strFieldData, $arrMatches)) {
									//El índice 1 son las ocurrencias del tipo de objeto (Q|S)
									//El índice 2 son los IDs del objeto al que hace referencia (QuestionID|SectionID)
									if (is_array($arrMatches) && isset($arrMatches[1]) && isset($arrMatches[2])) {
										foreach ($arrMatches[1] as $intIndex => $strMatchObjectType) {
											$intMatchObjectID = (int) @$arrMatches[2][$intIndex];
											if ($intMatchObjectID > 0) {
												$objObject = null;
												switch (strtoupper($strMatchObjectType)) {
													case "Q":
														$objObject = @$objQuestionsCollByID[$intMatchObjectID];
														if (is_null($objObject)) {
															$objObject = BITAMQuestion::NewInstanceWithID($aRepository, $intMatchObjectID);
															if (!is_null($objObject)) {
																$objQuestionsCollByID[$intMatchObjectID] = $objObject;
															}
														}
														break;
													case "S":
														$objObject = @$objSectionsCollbyID[$intMatchObjectID];
														if (is_null($objObject)) {
															$objObject = BITAMSection::NewInstanceWithID($aRepository, $intMatchObjectID);
															if (!is_null($objObject)) {
																$objSectionsCollbyID[$intMatchObjectID] = $objObject;
															}
														}
														break;
												}
												
												if ($objObject) {
													if ($objObject->DataSourceID == $intDataSourceID) {
														$blnValid = true;
														break;
													}
												}
											}
										}
									}
								}
							}
						}
						else {
							//En este caso no es un atributo, así que cualquier match debe impedir eliminar este elemento
							$blnValid = true;
						}
					}
					
					if ($blnValid) {
						if ($bGetIDs) {
							//En este caso se regresan los IDs agrupados por categorías de objetos donde se encontró el patrón
							$strObjectTypeName = '';
							switch ($intObjectType) {
								case otyAnswer:
									if (!isset($arrGroupedResult['Questions'])) {
										$arrGroupedResult['Questions'] = array();
									}
									$arrGroupedResult['Questions'][$intParentID] = $intParentID;
									if (!isset($arrGroupedResult['Answers'])) {
										$arrGroupedResult['Answers'] = array();
									}
									$arrGroupedResult['Answers'][$intObjectID] = $intObjectID;
									break;
									
								default:
									switch ($intObjectType) {
										case otySurvey:
											$strObjectTypeName = 'Surveys';
											break;
										case otySection:
											$strObjectTypeName = 'Sections';
											break;
										case otyQuestion:
											$strObjectTypeName = 'Questions';
											break;
									}
									
									if (!isset($arrGroupedResult[$strObjectTypeName])) {
										$arrGroupedResult[$strObjectTypeName] = array();
									}
									$arrGroupedResult[$strObjectTypeName][$intObjectID] = $intObjectID;
									break;
							}
						}
						else {
							//En este caso se regresan las propiedades específicas y los nombres de los objetos donde se encontró el patrón
							//@JAPR 2015-10-23: Modificada la manera de reportar los detalles, ahora se concatenará de la siguiente forma:
							/*
								- Si es un elemento de la forma:
								NombreForma - NombreSeccion/Pregunta/Filtro/etc [Opcional: Si es una opción de respuesta se agrega - NombrePregunta] - NombreDePropiedadDondeSeUsa
								- Si es un CheckPoint o AgendaScheduler
								NombreCheckPoint/AgendaScheduler - Propiedad
							*/
							/*
							if (!isset($arrGroupedResult[$strFieldID])) {
								//Si es la primera vez entonces agrega la descripción de la opción
								$arrGroupedResult[$strFieldID] = array("-".$strFieldLabel.":");
							}
							*/
							//Agrega el campo que aún contiene al elemento buscado
							$strObjectParentName = '';
							$objQuestion = @$arrQuestionsByID[$intParentID];
							if (!is_null($objQuestion)) {
								$strObjectParentName = (string) @$objQuestion->QuestionText;
							}
							//$arrGroupedResult[$strFieldID][] = "   ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strObjectName;
							$arrGroupedResult[] = $intNumRef."= ".$strFormName." - ".$strObjectName." - ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strFieldLabel;
							$intNumRef++;
							//@JAPR
						}
					}
				}
			}
			
			$aRS->MoveNext();
		}
	}
	//@JAPR 2015-12-09: Agregado el reporte de error sólo en modo de depuración
	else {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n<br>\r\n SearchObjectReferenceInMetadata error: ".$aRepository->DataADOConnection->ErrorMsg());
		}
	}
	//@JAPR
	
	//La tabla SI_SV_SurveyHTML se debe procesar por separado ya que contiene diferentes combinaciones de diferentes objetos, así que no se podría
	//incluir fácilmente con un UNION. Este proceso sólo aplicaba para variables de Preguntas o Secciones
	if ($anObjectType == otyQuestion || $anObjectType == otySection) {
		$objFieldsColl = array('FormattedText', 'HTMLHeader', 'HTMLFooter');
		$objFieldsTransCollByType = array(
			otySection => array('FormattedText' => translate("Formatted text")." (".translate("Section").")", 
				'HTMLHeader' => translate("Customized footer"), 
				'HTMLFooter' => translate("Customized header")),
			otyQuestion => array('FormattedText' => translate("Formatted text")." (".translate("Question").")", 
				'HTMLHeader' => translate(""), 
				'HTMLFooter' => translate("")),
			otyAnswer => array('FormattedText' => translate("Formatted text")." (".translate("Question Option").")", 
				'HTMLHeader' => translate(""), 
				'HTMLFooter' => translate(""))
			);
		$arrWHEREFields = array();
		foreach ($objFieldsColl as $strFieldName) {
			foreach ($objPatternColl as $strPattern) {
				$arrWHEREFields[] = 'A.'.$strFieldName.' LIKE '.$aRepository->DataADOConnection->Quote($strPattern);
			}
		}
		$strWHERESurveyHTML = implode(' OR ', $arrWHEREFields);
		
		$strNameField = $aRepository->DataADOConnection->Quote('Name');
		$strParentField = $aRepository->DataADOConnection->Quote('Parent');
		$strParentFieldID = $aRepository->DataADOConnection->Quote('ParentID');
		$sql = "SELECT A.ObjectType, A.ObjectID, A.FormattedText, A.HTMLHeader, A.HTMLFooter, B.SectionName AS {$strNameField}, '' AS {$strParentField}, 0 AS {$strParentFieldID} 
			FROM SI_SV_SurveyHTML A, SI_SV_Section B 
			WHERE A.SurveyID = B.SurveyID AND A.ObjectType = ".otySection." AND A.ObjectID = B.SectionID AND 
				A.SurveyID = ".$aSurveyID." AND (".$strWHERESurveyHTML.") 
			UNION 
			SELECT A.ObjectType, A.ObjectID, A.FormattedText, A.HTMLHeader, A.HTMLFooter, B.QuestionText AS {$strNameField}, '' AS {$strParentField}, 0 AS {$strParentFieldID} 
			FROM SI_SV_SurveyHTML A, SI_SV_Question B 
			WHERE A.SurveyID = B.SurveyID AND A.ObjectType = ".otyQuestion." AND A.ObjectID = B.QuestionID AND 
				A.SurveyID = ".$aSurveyID." AND (".$strWHERESurveyHTML.") 
			UNION 
			SELECT A.ObjectType, A.ObjectID, A.FormattedText, A.HTMLHeader, A.HTMLFooter, C.DisplayText AS {$strNameField}, B.QuestionText AS {$strParentField}, B.QuestionID AS {$strParentFieldID} 
			FROM SI_SV_SurveyHTML A, SI_SV_Question B, SI_SV_QAnswers C 
			WHERE A.SurveyID = B.SurveyID AND B.QuestionID = C.QuestionID AND A.ObjectType = ".otyAnswer." AND A.ObjectID = C.ConsecutiveID AND 
				A.SurveyID = ".$aSurveyID." AND (".$strWHERESurveyHTML.")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n<br>\r\n SearchVariablePatternInMetadata: SI_SV_SurveyHTML SQL == {$sql}");
		}
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while(!$aRS->EOF) {
				$intObjectType = (int) @$aRS->fields["objecttype"];
				$objFieldDef = @$objFieldsTransCollByType[$intObjectType];
				if ($intObjectType <= 0 || is_null($objFieldDef)) {
					continue;
				}
				
				foreach ($objFieldsColl as $strFieldName) {
					$strFieldData = (string) @$aRS->fields[$strFieldID];
					if ($strFieldData != '') {
						$strFieldID = '';
						switch ($intObjectType) {
							case otySection:
								//A la fecha de implementación, los 3 campos de esta tabla se basaban en secciones, así que se usan directo
								$strFieldID = 'SI_SV_Section'.$strFieldName;
								break;
							case otyQuestion:
								//A la fecha de implementación, sólo el QuestionMessage se mapeaba a esta tabla para las preguntas
								switch ($strFieldName) {
									case 'FormattedText':
										$strFieldID = 'SI_SV_QuestionQuestionMessage';
										break;
								}
								break;
							case otyAnswer:
								//A la fecha de implementación, sólo el HTMLText se mapeaba a esta tabla para las opciones de respuesta
								switch ($strFieldName) {
									case 'FormattedText':
										$strFieldID = 'SI_SV_QAnswersHTMLText';
										break;
								}
								break;
						}
						
						if ($strFieldID == '') {
							continue;
						}
						
						$strFieldLabel = (string) @$objFieldDef[$strFieldName];
						$strObjectName = (string) @$aRS->fields['name'];
						$intObjectID = (int) @$aRS->fields['objectid'];
						$intParentID = (int) @$aRS->fields['parentid'];
						$strObjectParentName = (string) @$aRS->fields['parent'];
						//Si contiene por lo menos en una ocasión alguno de los patrones válidos, agrega el elemento al array de resultados agrupados
						if (preg_match($strRegExpMatch, $strFieldData)) {
							if ($bGetIDs) {
								//En este caso se regresan los IDs agrupados por categorías de objetos donde se encontró el patrón
								$strObjectTypeName = '';
								switch ($intObjectType) {
									case otyAnswer:
										if (!isset($arrGroupedResult['Questions'])) {
											$arrGroupedResult['Questions'] = array();
										}
										$arrGroupedResult['Questions'][$intParentID] = $intParentID;
										if (!isset($arrGroupedResult['Answers'])) {
											$arrGroupedResult['Answers'] = array();
										}
										$arrGroupedResult['Answers'][$intObjectID] = $intObjectID;
										break;
										
									default:
										switch ($intObjectType) {
											case otySurvey:
												$strObjectTypeName = 'Surveys';
												break;
											case otySection:
												$strObjectTypeName = 'Sections';
												break;
											case otyQuestion:
												$strObjectTypeName = 'Questions';
												break;
										}
										
										if (!isset($arrGroupedResult[$strObjectTypeName])) {
											$arrGroupedResult[$strObjectTypeName] = array();
										}
										$arrGroupedResult[$strObjectTypeName][$intObjectID] = $intObjectID;
										break;
								}
							}
							else {
								//En este caso se regresan las propiedades específicas y los nombres de los objetos donde se encontró el patrón
								if (!isset($arrGroupedResult[$strFieldID])) {
									//Si es la primera vez entonces agrega la descripción de la opción
									$arrGroupedResult[$strFieldID] = array("-".$strFieldLabel.":");
								}
								//Agrega el campo que aún contiene al elemento buscado
								$arrGroupedResult[$strFieldID][] = "   ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strObjectName;
							}
						}
					}
				}
				
				$aRS->MoveNext();
			}
		}
	}
	
	//La tabla de mapeos se debe procesar por separado, ya que existen condiciones especiales como un mapeo directo de atributos los cuales
	//también pueden estar como variables mas complejas y son varias tablas las cuales no necesariamente tienen la misma estructura
	if ($anObjectType == otyQuestion || $anObjectType == otySection) {
		//Por el momento sólo la tabla de mapeos de opciones de respuesta tiene programado el soporte para expresiones, así que sólo se usa esa
		$objFieldsColl = array('Formula');
		$arrWHEREFields = array();
		foreach ($objFieldsColl as $strFieldName) {
			foreach ($objPatternColl as $strPattern) {
				$arrWHEREFields[] = 'A.'.$strFieldName.' LIKE '.$aRepository->DataADOConnection->Quote($strPattern);
			}
		}
		$strWHERESurveyHTML = implode(' OR ', $arrWHEREFields);
		
		$strObjectIDField = $aRepository->DataADOConnection->Quote('ObjectID');
		$strObjectTypeField = $aRepository->DataADOConnection->Quote('ObjectType');
		$strNameField = $aRepository->DataADOConnection->Quote('Name');
		$strParentField = $aRepository->DataADOConnection->Quote('Parent');
		$strParentFieldID = $aRepository->DataADOConnection->Quote('ParentID');
		$sql = "SELECT ".otyAnswer." AS {$strObjectTypeField}, A.QuestionOptionID AS {$strObjectIDField}, A.Formula, C.DisplayText AS {$strNameField}, B.QuestionText AS {$strParentField}, B.QuestionID AS {$strParentFieldID} 
			FROM SI_SV_SurveyAnswerActionFieldsMap A, SI_SV_Question B, SI_SV_QAnswers C 
			WHERE A.SurveyID = B.SurveyID AND B.QuestionID = C.QuestionID AND A.QuestionOptionID = C.ConsecutiveID AND 
				A.SurveyID = ".$aSurveyID." AND A.SurveyFieldID IN (".sfidFixedStrExpression.", ".sfidFixedNumExpression.") AND 
				(".$strWHERESurveyHTML.")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n<br>\r\n SearchVariablePatternInMetadata: eBavel Mapping SQL == {$sql}");
		}
		
		if ($aRS) {
			$strFieldID = "Formula";
			$strFieldName = $strFieldID;
			$strFieldLabel = translate("Map fields")." (".translate("Question Option").")";
			while(!$aRS->EOF) {
				$intObjectType = (int) @$aRS->fields["objecttype"];
				$strFieldData = (string) @$aRS->fields[$strFieldID];
				if ($strFieldData != '') {
					$strObjectName = (string) @$aRS->fields['name'];
					$intObjectID = (int) @$aRS->fields['objectid'];
					$intParentID = (int) @$aRS->fields['parentid'];
					$strObjectParentName = (string) @$aRS->fields['parent'];
					//Si contiene por lo menos en una ocasión alguno de los patrones válidos, agrega el elemento al array de resultados agrupados
					if (preg_match($strRegExpMatch, $strFieldData)) {
						if ($bGetIDs) {
							//En este caso se regresan los IDs agrupados por categorías de objetos donde se encontró el patrón
							$strObjectTypeName = '';
							switch ($intObjectType) {
								case otyAnswer:
									if (!isset($arrGroupedResult['Questions'])) {
										$arrGroupedResult['Questions'] = array();
									}
									$arrGroupedResult['Questions'][$intParentID] = $intParentID;
									if (!isset($arrGroupedResult['Answers'])) {
										$arrGroupedResult['Answers'] = array();
									}
									$arrGroupedResult['Answers'][$intObjectID] = $intObjectID;
									break;
									
								default:
									switch ($intObjectType) {
										case otySurvey:
											$strObjectTypeName = 'Surveys';
											break;
										case otySection:
											$strObjectTypeName = 'Sections';
											break;
										case otyQuestion:
											$strObjectTypeName = 'Questions';
											break;
									}
									
									if (!isset($arrGroupedResult[$strObjectTypeName])) {
										$arrGroupedResult[$strObjectTypeName] = array();
									}
									$arrGroupedResult[$strObjectTypeName][$intObjectID] = $intObjectID;
									break;
							}
						}
						else {
							//En este caso se regresan las propiedades específicas y los nombres de los objetos donde se encontró el patrón
							if (!isset($arrGroupedResult[$strFieldID])) {
								//Si es la primera vez entonces agrega la descripción de la opción
								$arrGroupedResult[$strFieldID] = array("-".$strFieldLabel.":");
							}
							//Agrega el campo que aún contiene al elemento buscado
							$arrGroupedResult[$strFieldID][] = "   ".(($strObjectParentName)?$strObjectParentName.' - ':'').$strObjectName;
						}
					}
				}
				
				$aRS->MoveNext();
			}
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n<br>\r\n SearchVariablePatternInMetadata: Grouped matches");
		PrintMultiArray($arrGroupedResult);
		if (count($arrGroupedResult) == 0) {
			echo("<br>\r\n");
		}
	}
	
	//Finalmente si se encontraron elementos que aún contienen referencias, arma el String final de error
	if ($bGetIDs) {
		$arrResult = $arrGroupedResult;
	}
	else {
		//@JAPR 2015-10-23: Modificada la manera de reportar los detalles, ahora se concatenará de la forma descrita arriba
		$arrResult = $arrGroupedResult;
		/*
		foreach ($arrGroupedResult as $strFieldID => $arrReferences) {
			$arrResult = array_merge($arrResult, $arrGroupedResult[$strFieldID]);
		}
		*/
		//@JAPR
	}
	
	if (!$bReturArray) {
		$strReturn = implode("\r\n", $arrResult);
		$arrResult = $strReturn;
	}
	
	return $arrResult;
}

//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
/* Regresa el identificador numérico del request recibido por el servicio
*/
function GeteFormsProcessID($sRequest, $bGetID = true) {
	global $arrEventParams;
	
	$intProcessID = -1;
	//@JAPR 2018-04-16: Agregados registros nuevos al Monitor de Operaciones para los procesos del Agente (#PCRUVT)
	$strProcessDesc = 'Unknown: '.((string) @$sRequest);
	//@JAPR
	$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	$blnEdit = (bool) getParamValue('edit', 'both', "(int)");
	$captureVia = getParamValue('CaptureVia', 'both', "(int)", false);
	switch (strtolower($sRequest))
    {
		//Ya no es utilizado, viene de eFormsV3
    	case 'getagendalist':
    	case 'getsurveysbyattribute':
    		break;
    	//Interno sólo para pruebas
    	case 'testsavefromfile':
    	case 'testsaveaction':
    	case 'saveanswers':
    	case 'testuploadanswers':
    		break;
    	
    	case 'getdefinitions':
    		$intProcessID = procGetDefinitions;
			$arrSurveyIDs = getParamValue('Survey', 'both', '', true);
			$arrCatalogIDs = getParamValue('Catalog', 'both', '', true);
			$arrAgendaIDs = getParamValue('Agenda', 'both', '', true);
    		$intProcessID = procGetDefinitions;
    		$strProcessDesc = "Get definitions";
    		if (!is_null($arrSurveyIDs)) {
    			$strProcessDesc .= " for survey ".(is_array($arrSurveyIDs)? implode(',', $arrSurveyIDs) : (int) $arrSurveyIDs);
    		}
    		if (!is_null($arrSurveyIDs)) {
    			$strProcessDesc .= " for catalog ".(is_array($arrCatalogIDs)? implode(',', $arrCatalogIDs) : (int) $arrCatalogIDs);
    		}
    		if (!is_null($arrAgendaIDs)) {
    			$strProcessDesc .= " for agenda ".(is_array($arrAgendaIDs)? implode(',', $arrAgendaIDs) : (int) $arrAgendaIDs);
    		}
    		break;
    	
    	case 'getdefinitionsversions':
    		$intProcessID = procGetDefinitionsVersions;
    		$strProcessDesc = "Get definition versions";
    		break;
    	
    	case 'getlanguagefile':
    		$strLanguage = getParamValue('language', 'both', "(string)", true);
    		$intProcessID = procGetLanguageFile;
    		$strProcessDesc = "Get language file for ".$strLanguage;
    		break;
    		
    	case 'uploadfile':
    		$strFileType = getParamValue('ftype', 'both', "(string)");
			$surveyID = getParamValue('surveyID', 'both', "(int)");
			$questionID = getParamValue('questionID', 'both', "(int)");
			$arrEventParams["ParentType"] = otySurvey;
			$arrEventParams["ParentID"] = $surveyID;
			$arrEventParams["ObjectID"] = $questionID;
    		switch (strtolower($strFileType)) {
    			case 'photo':
					$surveyDate = getParamValue('surveyDate', 'both', "(string)");
					$surveyHour = getParamValue('surveyHour', 'both', "(string)");
					$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
					$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
				    $path = "tmpsurveyimages/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
				    $strFileName = trim(getParamValue('filename', 'both', "(string)"));
				    $strTMPName = "photo_".$surveyID."_".$questionID."_".$strFileName;
    				$intProcessID = procUploadPhoto;
		    		$strProcessDesc = "Upload photo file for survey {$surveyID}: {$path}{$strTMPName}";
					$arrEventParams["ObjectType"] = otyPhoto;
    				break;
				case 'audio':
					$audioFileName = getParamValue('audioFileName', 'both', "(string)");
					$intProcessID = procSyncAudio;
		    		$strProcessDesc = "Synchronize audio file for survey {$surveyID}: {$audioFileName}";
					$arrEventParams["ObjectType"] = otyAudio;
					break;
				case 'video':
					$videoFileName = getParamValue('videoFileName', 'both', "(string)");
					$intProcessID = procSyncVideo;
		    		$strProcessDesc = "Synchronize video file for survey {$surveyID}: {$videoFileName}";
					$arrEventParams["ObjectType"] = otyVideo;
					break;
				case 'document':
					$surveyDate = getParamValue('surveyDate', 'both', "(string)");
					$surveyHour = getParamValue('surveyHour', 'both', "(string)");
					$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
					$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
					$path = "tmpsurveydocuments/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
					$strFileName = trim(getParamValue('filename', 'both', "(string)"));
					$strTMPName = "document_".$surveyID."_".$questionID."_".$strFileName;
					$intProcessID = procUploadDocument;
		    		$strProcessDesc = "Upload document file for survey {$surveyID}: {$path}{$strTMPName}";
					$arrEventParams["ObjectType"] = otyStatusDocument;
    				break;
    			case 'sketch':
					$surveyDate = getParamValue('surveyDate', 'both', "(string)");
					$surveyHour = getParamValue('surveyHour', 'both', "(string)");
					$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
					$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
    				$path = "tmpsurveyimages/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
    				$strFileName = trim(getParamValue('filename', 'both', "(string)"));
		    		$strProcessDesc = "Upload sketch file for survey {$surveyID}: {$path}{$strFileName}";
					$arrEventParams["ObjectType"] = otySketch;
    				break;
    		}
    		break;
    	
    	case 'uploadsyncdata':
    		$strFileType = getParamValue('ftype', 'both', "(string)");
    		switch (strtolower($strFileType)) {
    			case 'signature':
    				$surveyID = getParamValue('surveyID', 'both', "(int)");
    				$strFileName = getParamValue('filename', 'both', "(string)", true);
    				$intProcessID = procSyncSignature;
		    		$strProcessDesc = "Synchronize signature file for survey {$surveyID}: {$strFileName}";
					$arrEventParams["ParentType"] = otySurvey;
					$arrEventParams["ParentID"] = $surveyID;
		    		break;
    			case 'photo':
    				$surveyID = getParamValue('surveyID', 'both', "(int)");
					$questionID = getParamValue('questionID', 'both', "(int)");
    				$strFileName = getParamValue('filename', 'both', "(string)", true);
		    		$intProcessID = procSyncPhoto;
		    		$strProcessDesc = "Synchronize photo file for survey {$surveyID}: {$strFileName}";
					$arrEventParams["ParentType"] = otySurvey;
					$arrEventParams["ParentID"] = $surveyID;
					$arrEventParams["ObjectType"] = otyPhoto;
					$arrEventParams["ObjectID"] = $questionID;
		    		break;
    			case 'outbox':
    				$surveyID = getParamValue('surveyID', 'both', "(int)");
    				$strFileName = getParamValue('filename', 'both', "(string)", true);
    				$intProcessID = procSyncOutbox;
		    		$strProcessDesc = "Synchronize outbox file for survey {$surveyID}: {$strFileName}";
					$arrEventParams["ParentType"] = otySurvey;
					$arrEventParams["ParentID"] = $surveyID;
    				break;
    			case 'file':
    				$strFileType = getParamValue('filetype', 'both', "(string)");
    				$strFileName = getParamValue('filename', 'both', "(string)", true);
		    		$intProcessID = procUploadReportFile;
		    		$strProcessDesc = "Upload report file, type == '{$strFileType}': {$strFileName}";
		    		break;
    			case 'log':
    				$strFileName = getParamValue('filename', 'both', "(string)", true);
		    		$intProcessID = procSyncUsrLog;
		    		$strProcessDesc = "Synchronize user log file: {$strFileName}";
		    		break;
    		}
    		break;
		
    	case 'getcatalogvalues':
    		$intCatalogID = getParamValue('catalogID', 'both', "(int)");
    		$strFilter = getParamValue('filterText', 'both', "(string)");
    		$parameterAgendaID = getParamValue('agendaID', 'both', "(int)");
    		$intProcessID = procGetDynCatValues;
    		$strProcessDesc = "Get dynamic catalog values";
			$arrEventParams["ObjectType"] = otyCatalog;
			$arrEventParams["ObjectID"] = $intCatalogID;
    		break;
    		
    	case 'getsectionvalues':
    		$intSectionID = getParamValue('sectionID', 'both', "(int)");
    		$strFilter = getParamValue('filterText', 'both', "(string)");
    		$intProcessID = procGetRecapSectValues;
    		$strProcessDesc = "Get dynamic recap section values";
			$arrEventParams["ObjectType"] = otySection;
			$arrEventParams["ObjectID"] = $intSectionID;
    		break;
    		
    	case 'loadanswers':
    		$surveyID = getParamValue('surveyID', 'both', "(int)");
    		$entryID = getParamValue('entryID', 'both', "(int)");
    		$intProcessID = procEntryReportData;
    		$strProcessDesc = "Get entry data";
			$arrEventParams["ParentType"] = otySurvey;
			$arrEventParams["ParentID"] = $surveyID;
			$arrEventParams["ObjectType"] = otyOutbox;
			$arrEventParams["ObjectID"] = $entryID;
    		break;
    		
    	case 'checkoutboxfile':
    		$strFileName = getParamValue('filename', 'both', "(string)", true);
    		$intProcessID = procCheckOutboxFile;
    		$strProcessDesc = "Check outbox file";
    		break;
		
		//@JAPR 2018-04-16: Agregados registros nuevos al Monitor de Operaciones para los procesos del Agente (#PCRUVT)
		case 'syncagendas':
			$intProcessID = procProcessAgendas;
			$strProcessDesc = "Agent task: Generate agendas";
			break;
		case 'syncincdatadestinations':
			$intProcessID = procProcessIncDestinations;
			$strProcessDesc = "Agent task: Process incremental destinations";
			break;
		case 'agendacubegeneration':
			$intProcessID = procProcessAgendasCube;
			$strProcessDesc = "Agent task: Generate agendas model";
			break;
		case 'syncdatasources':
			$intProcessID = procProcessDataSources;
			$strProcessDesc = "Agent task: Synchronize external datasources";
			break;
		//@JAPR
    }
    
    return ($bGetID)?$intProcessID:$strProcessDesc;
}

/* Regresa la descripción del request recibido por el servicio, incluyendo los nombres de archivos de log reelevantes para el mismo
*/
function GeteFormsProcessDesc($sRequest) {
	return GeteFormsProcessID($sRequest, false);
}
//@JAPR

/* Manda un echo a la salida formateandolo para presentación adecuada
*/
function ECHOString($sText, $iLnBefore = 1, $iLnAfter = 0, $sStyle = "") {
	$ln = "\r\n";
	if ($iLnBefore) {
		echo(nl2br(str_repeat($ln, $iLnBefore)));
	}
	if ($sStyle) {
		echo("<span style=\"".$sStyle."\">");
	}
	echo(nl2br(date("Y-m-d H:i:s")." - ".$sText));
	if ($sStyle) {
		echo("</span>");
	}
	if ($iLnAfter) {
		echo(nl2br(str_repeat($ln, $iLnAfter)));
	}
	
	if (getParamValue('DebugLog', 'both', '(string)', true)) {
		//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		//Integradas las funciones comunes para almacenamiento de log de eForms
		$sFilePath = GeteFormsLogPath();
		$sFileName = $sFilePath.getParamValue('DebugLog', 'both', '(string)', true);
		//@JAPR 2018-05-08: Optimizada la carga de eForms (#G87C4W)
		@error_log("\r\n".str_ireplace("<br>", '', getMicroDateTime()." - ".$sText), 3, $sFileName);
		//@JAPR
	}
}

//Obtiene la ruta URL base del script ejecutado para anexar a rutas relativas y convertirlas en absolutas
//@JAPR 2016-01-11: Agregado el defaultValue para preguntas tipo Photo
/* Agregado el parámetro $bAddProtocol para indicar si se desea anexar dicha parte a la URL resultante. Al agregar este parámetro, la optimización para no
recalcular la URL en cada petición se volvió inválida, ya que ahora se podía generar con o sin protocolo, así que se removió la optimización
*/
function GetBaseURLPath($bAddSlash = true, $bAddProtocol = true) {
	/*global $gsBaseURLPath;
	if (isset($gsBaseURLPath) && $gsBaseURLPath != '') {
		return $gsBaseURLPath.(($bAddSlash)?"/":"");
	}
	*/
	
	$protocol = (isset($_SERVER["HTTPS"]) &&  $_SERVER["HTTPS"] == 'on' ? 'https' : 'http');
	//@JAPR 2016-01-11: Agregado el defaultValue para preguntas tipo Photo
	//Agregado el parámetro $bAddProtocol para indicar si se desea anexar dicha parte a la URL resultante
	$strHost = (($bAddProtocol)?($protocol."://"):'').$_SERVER["SERVER_NAME"];
	//@JAPR
	$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
	$strScriptPath = $path_parts["dirname"];
	$gsBaseURLPath = $strHost.$strScriptPath;
	
	return $gsBaseURLPath.(($bAddSlash)?"/":"");
}

//@JAPR 2015-10-16: Modificada la redirección en caso de error para que dependa del tipo de servicio donde se encuentra
/* Verifica si el request fue realizado o no por Ajax para redireccionar o mostrar un mensaje de error, adicionalmente en errores que no son por pérdida de sesión sino por algún
problema de autentificación o conexión, relaiza la misma revisión para redireccionar a la página de login correcta según el servicio de eForms, ya que estaba redireccionando siempre
a la ventana de login del producto
*/
function RedirectSessionConnectionError($errNumber = -1, $sGoto = '') {
	//@JAPR 2015-10-07: Corregido un bug, faltaba incluir este archivo para tener acceso a las funciones y constantes que verifican si es GeoControl
	require_once('config.php');
	
	global $gbIsGeoControl;
	
	$errNumber = (int) $errNumber;
	if ($errNumber <= 0) {
		$errNumber = -1;
	}
	switch ($errNumber) {
		case -1:
			//@JAPR 2015-10-06: Validado que si el request es Ajax, entonces sólo regrese un texto descriptivo de error en lugar de redireccionar
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == strtolower('XMLHttpRequest')) {
				if ($gbIsGeoControl) {
					die(translate("Los datos de su sesión se perdieron o esta sobrepasó el tiempo de inactividad. Por favor reinicie el proceso"));
				}
				else {
					die(translate("Your session data was lost or has timed out due to inactivity. Please restart the process"));
				}
			}
			else {
				header("Location: loadKPILogin.php");
			}
			break;
		
		default:
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == strtolower('XMLHttpRequest')) {
				if ($gbIsGeoControl) {
					die(translate("Los datos de su sesión se perdieron o esta sobrepasó el tiempo de inactividad. Por favor reinicie el proceso"));
				}
				else {
					die(translate("Your session data was lost or has timed out due to inactivity. Please restart the process"));
				}
			}
			else {
				header("Location: loadKPILogin.php?error=".$errNumber);
			}
			break;
	}
	
	exit();
	//@JAPR
}

//@JAPR 2015-11-02: Corregido un bug con filtros dinámicos en el preview del Admin, las variables de preguntas/secciones no se pueden resolver pero no se reemplazaban y generaban error (#X7ZU1E)
/* Esta función busca todas las variables de preguntas/secciones sin importar el dato solicitado, y simplemente las reemplaza por null ya que se utiliza para una consulta SQL así que no 
se puede quedar como definición de variable, y puede intentar usarse como String, número o fecha, así que debe poder reemplazar por un valor adecuado en cada caso
*/
function ReplaceAppVariablesForDummyValues($sText) {
	try {
		$strText = (string) $sText;
		//Buscará como patrón de captura todas las variables tanto de preguntas como de sección, sin embargo las que ya están delimitadas serán ignoradas por ya ser un string válido,
		//así que la expresión devuelve como capturable solo aquellas no delimitadas para cambiarlas a NULL
		$strRegExpMatch = "/\"(?:[^\"]|\\\")*\"|\\\"|'(?:[^']|\\')*'|\\'|([\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1})/i";
		preg_match_all($strRegExpMatch, $strText, $arrResults);
		if (is_array($arrResults) && isset($arrResults[0])) {
			foreach ($arrResults[0] as $intIndex => $strVariable) {
				//El índice 1 vendría vacío si fuera una variable ya delimitada, en ese caso no se altera
				$blnIsDelimited = trim((string) @$arrResults[1][$intIndex]) == '';
				if (!$blnIsDelimited) {
					//Si no está delimitada, entonces se debe reemeplazar por NULL para que independientemente del tipo de campo sea una condición válida
					//Si esta misma expresión se usaba en otra variable ya delimitada, el reemplazo también la afectará pero estará bien porque quedará simplemente 'NULL' (delimitado) y
					//se considerará un string, así que sigue siendo válido (si estaba mal aplicado como String para comparar una columna que no es String, eso ya generará un error de un
					//tipo diferente)
					$strText = str_ireplace($strVariable, 'NULL', $strText);
				}
			}
		}
	} catch (Exception $e) {
		return $sText;
	}
	
	return $strText;
}

//OMMC 2015-11-18: Generada para solucionar problemas relacionados con el issue #4EBDE6
/* Esta funcion remueve caracteres especiales y los reemplaza por "_" esto para evitar problemas de encoding o envío de parámetros GET. Recibe el nombre del archivo con los caracteres y el tipo de archivo, de no enviar un tipo de archivo se considera que es una foto.*/
function StripSpecialCharsFromFileNames($strName, $fType){
	try {
		if($strName == '' && !$strName){
			return;
		}
		if(!$fType){
			$fType = "photo";
		}

		//OMMC 2016-04-26: Agregado el signo de $ a la validación
		//Algunos de los carcteres que podrían causar problemas, sólo es cuestión de agregarlos al [#%]+
		//modificar la regex: [# \. - _ - ( ) \^ % $ @ , + ; ]
		$charsToRemove = "/[$#%+\'\"]+/";
		$charToReplace = "_";
		$strNameR = "";

		switch($fType){
			case "photo":
			default:
				$strNameR = preg_replace($charsToRemove, $charToReplace, $strName);
			break;
		}	
	} catch (Exception $e) {
		return $strName;
	}
	return $strNameR;	
}

//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
function RemoveUNICODEFromUtf8Str($aString) {
	return preg_replace('/[^\x{0000}-\x{ffff}]/u', '?', $aString);
}
//@JAPR

//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
/*Reemplaza las referencias de estilos css de background-image que están en formato JSON por un patrón que la función BITAMSection::getArrayImages puede identificar como imagen para que 
el proceso de descarga de imágenes hacia dispositivos pueda funcionar correctamente con los estilos CSS de los templates personalizados
*/
function ReplaceCSSStyleBackgroundImageForIMG($sText) {
	$strText = $sText;
	
	//Obtiene todas las referencias a estilos background-image":"NombreImagen"
	$count = preg_match_all('/(background-image)(\"|\'):(\"|\')([^\"\'\}]+)/i', $strText, $media);
	if($count!==false && $count>0) {
		$dataImg = preg_replace('/(background-image)(\"|\'):(\"|\')([^\"\'\}]+)/i', "$4", $media[0]);
		$theRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
		
		foreach($dataImg as $intIndex => $url) {
			$info = pathinfo($url);
			if (getParamValue('DebugBreakImg', 'both', '(int)', true)) {
				echo("<br>url from CSS == {$url}");
				PrintMultiArray($info);
			}
			if (isset($info['extension'])) {
				$info['extension'] = strtolower($info['extension']);
				if (($info['extension'] == 'jpg') || ($info['extension'] == 'jpeg') || ($info['extension'] == 'gif') || ($info['extension'] == 'png')) {
					$strURL = '<img src="'.GetBaseURLPath()."customerimages/".$theRepositoryName.'/'.$url.'" />';
					$strText = str_ireplace($media[$intIndex], $strURL, $strText);
				}
			}
		}
	}
	
	return $strText;
}
//@JAPR

//GCRUZ 2016-05-26. Función global para reemplazar variables globales: @LOGIN, @REPOSITORY, @ENCRYPTEDPASSWORD
function TranslateGlobalVariables($aRepository, $strText, $cla_usuario = -1)
{
	require_once('user.inc.php');
	$bitamUser = BITAMeFormsUser::WithUserName($aRepository, $_SESSION["PABITAM_UserName"]);
	$strText=str_ireplace("@LOGINEMAIL", $bitamUser->FullName, $strText);
	$strText=str_ireplace("@LOGIN", $_SESSION["PABITAM_UserName"], $strText);
	$strText=str_ireplace("@ENCRYPTEDPASSWORD", BITAMEncode($bitamUser->Password), $strText);
	$strText=str_ireplace("@REPOSITORY", $_SESSION["PABITAM_RepositoryName"], $strText);
	if (getMDVersion() >= esvUserParams && $cla_usuario != -1) {
		require_once('appUSer.inc.php');
		$appUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $cla_usuario);
		$strText=str_ireplace("@USERPARAM1", $appUser->UserParam1, $strText);
		$strText=str_ireplace("@USERPARAM2", $appUser->UserParam2, $strText);
		$strText=str_ireplace("@USERPARAM3", $appUser->UserParam3, $strText);
	}
	return $strText;
}

//@JAPR 2016-06-22: Agregada la posibilidad de crear atributos tipo imagen para catálogos con definición manual (#R3Q9JQ)
/* Dada una ruta de imagen como http o bien de manera relativa o simplemente como un nombre de imagen, realizará uno de 3 procesos según el caso del que se trate, para regresar como
resultado el nombre de la imagen con la ruta relativa donde se puede localizar dentro del servicio de eForms:
1- Si es una ruta http o https, se descargará la imagen de dicha URL y se grabará dentro de la ruta de customerimages correspondiente, regresando la ruta relativa donde se colocó
2- Si es una ruta que inicia con "/" o contiene alguna "/" (por tanto se asume que está dentro de una carpeta), se considerará que es una ruta relativa al server o al servicio de eForms
	y no se modificará, simplemente regresará el texto recibido
4- Si es sólo un texto, asumirá que se trata de un nombre de imagen (no validará por extensiones válidas, aunque podría introducirse mas adelante tal validación) y simplemente le agregará
	la referencia al customerimages correspondiente independientemente de si realmente existe o no en ese path, ya que el usuario podrá mas adelante subir la imagen con dicho nombre si
	así lo desea
Debido a que este proceso se puede ejecutar desde la carga de datos con MS Excel, y como dicho proceso se ejecuta dentro de la subcarpeta loadExcel, el copy realizado estaba creando
los archivo también dentro de dicha subcarpeta, así que se agregó el parámetro $sRootPath para permitir especificar una ruta inicial o bien relativa desde donde se deberán crear los
archivos, con lo cual se puede forzar a iniciar en una carpeta específica, a partir del root, o regresar "n" niveles desde donde actualmente se encuentra, o incluso usar una ruta física
completa
*/
function GetRelativeUserImgFileNameFromURL($sImageFilePathName, $sRootPath = '', &$sErrorMsg = '') {
	$strAttributeFieldValue = str_replace("\\", "/", (string) $sImageFilePathName);
	$arrURLData = @IdentifyAndAdjustAttributeImageToDownload($strAttributeFieldValue);
	if (!is_null($arrURLData)) {
		//En este caso la URL necesita algún ajuste y posiblemente se debe descargar la imagen, según el objeto regresado
		if (is_array($arrURLData)) {
			//Se debe descargar la imagen, además de actualizar el valor a grabar
			$strAttributeFieldValue = (string) @$arrURLData['path'];
			//$arrImagesToDownload[(string) @$arrURLData['name']] = $arrURLData;
			$fileName = (string) @$arrURLData['name'];
			//Antes de hacer la copia, primero crea la ruta destino
			$path = str_ireplace($fileName, '', $sRootPath.$arrURLData['path']);
			if (!file_exists($path)) {
				@mkdir($path, null, true);
			}
			
			if (!@copy($arrURLData['url'], $sRootPath.$arrURLData['path'])) {
				$errors = error_get_last();
				$sErrorMsg = "({$fileName}) ".$errors['message'];
			}
		}
		else {
			//En este caso simplemente se ajusta la URL a grabar
			$strAttributeFieldValue = $arrURLData;
		}
	}
	
	return $strAttributeFieldValue;
}

//@JAPR 2016-11-08: Corregidos los textos utilizados dentro de funciones de DHTMLX que generan JScript para escapar caracteres que podrían causar conflictos (#KPCWW3)
/* Dado un string que generalmente será utilizado como atributo de un tag HTML, parámetro de una función de JScript o el equivalente como parámetro de una función de DHTMLX, reemplaza
los caracteres que generalmente son problemáticos aplicando un addslashes (reemplaza ' " \ y caracter "null") además de los <enter>
El parámetro $oManualReplaceText permite especificar mediante un conjunto de "índiceAReemplazar" => "Reemplazo" que combinaciones reemplazar, por ejemplo:
$oManualReplaceText = array("\r\n" => "<br>")
Reemplazaría sólo los <Enter> por "<br>"
Generalmente este parámetro se debe dejar vacío, en cuyo caso se aplicará el reemplazo default de la función
Se puede modificar la función para agregar banderas al parámetro $oOptions para controlar de manera diferente que caracteres escapar, y así poder ajustar la función para diferentes
situaciones (probablemente llenando internamente el array $oManualReplaceText en dichos casos), siempre y cuando el funcionamiento default sea el explicado arriba
*/
function GetValidJScriptText($sText, $oOptions = array(), $oManualReplaceText = array()) {
	$strText = $sText;
	
	//Caracter de reemplazo del <enter>, se puede cambiar mediante el array de parámetros, el default es simplemente removerlo (vacío)
	$strEnterReplace = (string) @$oOptions["enterReplace"];
	
	//@JAPR 2016-11-28: Corregido el uso de caracteres que rompen textos de JScript como nombres de elementos de las fórmulas (#RMIHUH)
	//@JAPR 2016-11-28: Agregada la opción para reemplazar caracteres con la función htmlspecialchars de manera completa (ENT_QUOTES)
	//Originalmente usada para el issue (#RMIHUH) porque el JSON como String asignado al value de los options del editor de fórmula no estaban grabando correctamente las ', esto porque
	//internamente se convertían los caracteres como " a su versión "html entity" (&quot;) así que se decidió a codificar todas las entidades con esta función para que pudiera leer
	//correctamente el JSON
	$blnUseFullHTMLSpecialChars = (bool) @$oOptions["fullhtmlspecialchars"];
	//@JAPR 2016-12-02: Corregido el uso de caracteres que rompen textos de JScript como nombres de elementos de las fórmulas (#RMIHUH)
	//Por alguna razón las comillas doble no se debían convertir a entidad HTML, ya que al convertirlas a JSON automáticamente las escapaba correctamente como \" pero si estaban como
	//HTML entonces no hacía el escape, así que se forzará con este nuevo parámetro a no convertir esa entidad, si no se envía funcionará como se espera según htmlspecialchars
	$blnRestoreDoubleQuotesEnt = (bool) @$oOptions["restoredoublequoteent"];
	//@JAPR
	if ($blnUseFullHTMLSpecialChars) {
		$strText = htmlspecialchars($strText, ENT_QUOTES);
		//@JAPR 2016-12-02: Corregido el uso de caracteres que rompen textos de JScript como nombres de elementos de las fórmulas (#RMIHUH)
		//Por alguna razón las comillas doble no se debían convertir a entidad HTML, ya que al convertirlas a JSON automáticamente las escapaba correctamente como \" pero si estaban como
		//HTML entonces no hacía el escape, así que se forzará con este nuevo parámetro a no convertir esa entidad, si no se envía funcionará como se espera según htmlspecialchars
		if ($blnRestoreDoubleQuotesEnt) {
			$strText = str_replace("&quot;", "\"", $strText);
		}
		//@JAPR
		return $strText;
	}
	//@JAPR
	
	//Aplica el reemplazo default de la cadena, excepto en casos donde se hubiesen especificado banderas especiales o bien se hubiera especificado el array de reemplazos manual
	if (is_array($oManualReplaceText) && count($oManualReplaceText) > 0) {
		$arrSearch = array_keys($oManualReplaceText);
		$arrReplace = array_values($oManualReplaceText);
		$strText = str_ireplace($arrSearch, $arrReplace, $strText);
	}
	else {
		//Reemplazo default de los caracteres mas comunes
		$strText = str_ireplace(array("\r\n", "\r", "\n"), $strEnterReplace, addslashes($strText));
	}
	
	return $strText;
}

//@JAPR 2018-05-08: Optimizada la carga de eForms (#G87C4W)
/* Obtiene la fecha y hora hasta milisegundos basado en la fecha UNIX */
function getMicroDateTime($sMicrotime = '') {
	if ( $sMicrotime ) {
		$microTime = $sMicrotime;
	}
	else {
		$microTime = microtime();
	}
	$date_array = explode(" ",$microTime);
	$date = date("Y-m-d H:i:s",@$date_array[1]).str_replace("0.", ".", @$date_array[0]);
	return $date;
}

/* Crea la conexión al repositorio de KPIOnline para reutilizarla de manera global en cualquier llamada subsecuente. Este método NO utiliza ADODB por compatibilidad
con el código de versiones viejas que usaba directamente mysql_connect, sin embargo se debe tener cuidado de NO realizar un mysql_select_db con la instancia
devuelta por esta conexión, porque de lo contrario afectará a llamadas posteriores que pretendan reutilizar la misma instancia para evitar reconexión a la BD FBM000 */
function GetKPIRepositoryConnection() {
	global $mysql_Connection;
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Starting GetKPIRepositoryConnection");
	}
	//@JAPR 2012-04-17: Agregada validación para cuando se invoca este método dentro de un require_once en otra función, en ese caso las variables
	//sólo son accesibles desde globales
	if (!isset($server))
	{
		global $server;
		global $server_user;
		global $server_pwd;
		global $masterdbname;
	}
	//@JAPR
	
	//Reutiliza la conexión en caso de haberse creado previamente
	if ( isset($mysql_Connection) ) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Finishing GetKPIRepositoryConnection with Cache");
		}
		return $mysql_Connection;
	}
	
	//Crea la conexión la primera vez y la asigna en la variable global para ser reutilizada
	require_once('../../../fbm/conns.inc.php');
	$mysql_Connection = @mysql_connect($server, $server_user, $server_pwd);
	if (!$mysql_Connection) {
		return null;
	}
	
	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);
	if (!$db_selected) {
		return null;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Finishing connecting GetKPIRepositoryConnection");
	}
	return $mysql_Connection;
}

//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
/* Dado un array que eventualmente será convertido a JSON con json_encode, remueve todos los caracteres que no son válidos en utf8 (generados por lo descrito en el issue indicado entre
otras cosas) cambiándolos por el caracter dummy especificado para que el json_encode no produzca errores y se pueda continuar usando el array como era planeado */
function FixNonUTF8CharsInArray($anObjectArray, $sDummyChar = '?') {
	if ( !$anObjectArray || !is_array($anObjectArray) || !count($anObjectArray) ) {
		return array();
	}
	
	$some_string = serialize($anObjectArray);
	//die("\r\n The string with non UTF-8 characters: ".$some_string);
	
	//reject overly long 2 byte sequences, as well as characters above U+10000 and replace with $sDummyChar
	$some_string = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]'.
		'|[\x00-\x7F][\x80-\xBF]+'.
		'|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*'.
		'|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})'.
		'|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
		$sDummyChar, $some_string );
	
	//reject overly long 3 byte sequences and UTF-16 surrogates and replace with $sDummyChar
	$some_string = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]'.
		'|\xED[\xA0-\xBF][\x80-\xBF]/S',$sDummyChar, $some_string );
	
	return unserialize($some_string);
}

//@JAPR 2019-04-30: Corregido un bug al utilizar variables de objetos del App en filtros dinámicos, el uso de comillas sencillas dentro de strings causaba errores de queries (#PDZMWE)
/* Esta función reemplazará las variables de objetos del App exclusivamente para uso dentro de filtros de catálogos, por una versión que pueda ser utilizada de manera segura en el query,
ya que el uso de comillas sencillas como parte de la variable cuando se encerraba entre otras comillas sencillas provocabas errores de query */
function ReplaceAppObjectVariablesForFilters($sFilterText) {
	$strFilterText = $sFilterText;
	
	$strFilterText = str_replace("'{objSettings.['user']}'", "\"{objSettings.['user']}\"", $strFilterText);
	$strFilterText = str_replace("'{selSurvey.['name']}'", "\"{selSurvey.['name']}\"", $strFilterText);
	
	return $strFilterText;
}

//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
/* Genera la ruta donde se deberán colocar los archivos de log de todos los procesos, basado en las configuraciones globales y/o los parámetros según la invocación */
function GeteFormsLogPath($sRepositoryName = null, $sAgentName = null, $sAlternativeLogPath = null, $sLogExtraPath = null) {
	//Subcarpeta por Nombre de Agente (servicio/tarea de windows), todos los requests que sean invocados por un agente deberían contener este parámetro
	global $streFormsAgentName;
	if ( !is_null($sAgentName) ) {
		$streFormsAgentName = $sAgentName;
	}
	else {
		if ( !isset($streFormsAgentName) || !$streFormsAgentName ) {
			$streFormsAgentName = getParamValue('FAgentName', 'both', "(string)");
		}
	}
	
	//Subcarpeta por repositorio, los procesos/servicios específicos por repositorio siempre deberían asignarla, pero los Agentes que no están ligados a un repositorio específico no
	//podrían utilizar este valor directamente para sus logs
	global $strLogRepositoryName;
	if ( !is_null($sRepositoryName) ) {
		$strLogRepositoryName = $sRepositoryName;
	}
	else {
		if ( !isset($strLogRepositoryName) || !$strLogRepositoryName ) {
			$strLogRepositoryName = (string) $sRepositoryName;
		}
	}
	
	//Subcarpeta extra a la ruta, generalmente utilizada para establecer por fecha de procesamiento
	global $strLogExtraPath;
	if ( !is_null($sLogExtraPath) ) {
		$strLogExtraPath = $sLogExtraPath;
	}
	else {
		if ( !isset($strLogExtraPath) || !$strLogExtraPath ) {
			$strLogExtraPath = date("Ymd");
		}
	}
	
	//Path alternativo al del servicio de eForms donde se generarán los logs
	global $strAlternativeLogPath;
	if ( !is_null($sAlternativeLogPath) ) {
		$strAlternativeLogPath = $sAlternativeLogPath;
	}
	else {
		if ( !isset($strAlternativeLogPath) || !$strAlternativeLogPath ) {
			$strAlternativeLogPath = (string) $sAlternativeLogPath;
		}
	}
	
	//Genera la ruta completa de logs de eForms con las diferentes partes especificadas
	if ( $strAlternativeLogPath ) {
		$strLogPath = $strAlternativeLogPath.'/';
	}
	else {
		$strLogPath = substr(ADODB_DIR, 0, strlen(ADODB_DIR)-5).'log/';
	}
	
	if ( $streFormsAgentName ) {
		$strLogPath .= $streFormsAgentName.'/';
	}
	
	if ( $strLogExtraPath ) {
		$strLogPath .= $strLogExtraPath.'/';
	}
	
	//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Variable para indicar que el proceso se está ejecutando desde un agente de eForms en general, sea cual sea dicho agente
	global $gbIsAneFormsAgent;
	//Adicionalmente crea la ruta si no existiera
	$strLocalLogPath = $strLogPath;
	if ( $strLogRepositoryName ) {
		$strLocalLogPath .= $strLogRepositoryName.'/';
	}
	else {
		if ( !isset($gbIsAneFormsAgent) || !$gbIsAneFormsAgent ) {
			//Si no se trata de un agente, automáticamente intentará agregar el repositorio desde la variable de sesión correspondiente, asumiendo que es un request que pasa por el
			//web service exclusivamente, pero en este caso NO lo dejará grabado como parte del path de las variables globales
			$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
			if ( $strRepositoryName ) {
				$strLocalLogPath .= $strRepositoryName.'/';
			}
		}
	}
	
	if ( !file_exists($strLocalLogPath) ) {
		@mkdir($strLocalLogPath, null, true);
	}
	
	return $strLocalLogPath;
}
//@JAPR
?>