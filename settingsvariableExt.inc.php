<?php

require_once("repository.inc.php");
require_once("settingsvariable.inc.php");
//require_once("object.trait.php");

class BITAMSettingsVariableExt extends BITAMSettingsVariable
{
	public $NoHeader = 1;

	function generateBeforeFormCode($aUser) {
	}
	
	function addButtons($aUser) {
	}
	
	function get_Title() {
	}
	//Necesaria para que el grabado con processRequest no genere error
	function getJSonDefinition()
	{
		$arrDef = array();
		foreach($this->ArrayVariables as $variable)
		{
			$arrDef[$variable]=$this->$variable;
		}
		return $arrDef;
	}
	
	function generateForm($aUser) {
		$fieldTypes = array();
		$fieldTypes["MEETING"]="input";
		$fieldTypes["CATEGORYCATALOG"]="combo";
		$fieldTypes["CATEGORYMEMBER"]="combo";
		$fieldTypes["EBAVELURL"]="input";
		$fieldTypes["ALTEFORMSSERVICE"]="input";
		$fieldTypes["ALTEFORMSSERVICEVERSION"]="input";
		$fieldTypes["REQUIREDVERSIONFORALTSERVER"]="input";
		$fieldTypes["SURVEYIMAGESMIRRORFOLDER"]="input";
		$fieldTypes["EBAVELDEFAULTACTIONSTATUS"]="input";
		$fieldTypes["EBAVELDEFAULTACTIONSOURCE"]="input";
		$fieldTypes["USEINCREMENTALAGGREGATIONS"]="combo"; //Si/NO
		$fieldTypes["REGISTERGPSDATAAT"]="combo"; //Al grabar la captura/Al iniciar la captura
		$fieldTypes["CUSTOMIZEDSTORESURVEYSERVICE"]="input";
		$fieldTypes["DEFAULTSURVEY"]="combo";
		$fieldTypes["ENABLECOLORTEMPLATES"]="combo"; //Si/NO
		$fieldTypes["DEFAULTCOLORTEMPLATE"]="combo";
		$fieldTypes["BACKGROUNDIMAGE"]="image";
		$fieldTypes["MENUMARGINTOP"]="input";
		$fieldTypes["MENUMARGINLEFT"]="input";
		$fieldTypes["MENUMARGINBOTTOM"]="input";
		$fieldTypes["MENUMARGINRIGHT"]="input";
		$fieldTypes["BACKGROUNDIMAGEVERSION"]="image"; //Esta variable no la despliegan en settingsvariable.inc.php
		$fieldTypes["CUSTOMHEADER"]="input";
		$fieldTypes["USECACHE"]="combo"; //Si/NO
		$fieldTypes["CACHEREQUESTS"]="input";
		$fieldTypes["FORMSMENUPOSITION"]="input";
		$fieldTypes["AGENDARADIO"]="input";
		//OMMC 2015-12-15: Agregado para ocultar la opción de descripción de la forma
		$fieldTypes["HIDEDESCRIPTION"]="combo";
		//GCRUZ 2016-01-21. Opción para hora de ejecución de destinos incrementales
		$fieldTypes["AUTOINCREMENTALGENERATIONTIME"]="combo";
		//@JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
		$fieldTypes["DEFAULTFORMSWIDTH"]="combo";
		$fieldTypes["DEFAULTFORMSHEIGHT"]="combo";
		$fieldTypes["DEFAULTFORMSTEXTPOSITION"]="combo";
		$fieldTypes["DEFAULTFORMSTEXTSIZE"]="input";
		$fieldTypes["DEFAULTFORMSCUSTOMHTML"]="html";
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		$fieldTypes["SHOWQUESTIONNUMBERS"]="combo";
		//RV 2016-04-14: Modo de despliegue de los enlaces 
		$fieldTypes["LINKSDISPLAYMODE"]="combo";
		//@JAPR 2016-05-19: Agregada la configuración para envío de GPS constante desde el App (#WRWF3Y)
		$fieldTypes["APPGPSTRACKER"]="input";
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		$fieldTypes["ENABLENAVHISTORY"]="combo";
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		$fieldTypes["AUTOFITIMAGE"]="combo";
		//MAPR 2018-02-19: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
		$fieldTypes["ALLMOBILE"]="combo";
		//MAPR 2018-05-21: Se integra la configuración para el modo en segundo plano activado.
		$fieldTypes["BACKGROUNDMODE"]="combo";
		//@JAPR 2018-10-19: Integrado los Mapas de Apple (#64ISFM)
		$fieldTypes["MAPTYPE"]="combo";
		//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
		$fieldTypes["FORGETCREDENTIALS"]="combo";
		
		//Cgil mandó la lista de cuales son las variables que deben desplegarse, algunas se quitaron por que no tenía caso que el usuario las cambiara
		//Sólo las variables que estén en $varVisibles serán desplegadas
		$varVisibles=array();
		$varVisibles[12]="REGISTERGPSDATAAT";
		$varVisibles[14] = "DEFAULTSURVEY";
		//@JAPR 2016-03-30: Validada la versión de metadata para la opción de templates
		if (getMDVersion() >= esvTemplateStyles) {
			$varVisibles[15] = "ENABLECOLORTEMPLATES";
			$varVisibles[16] = "DEFAULTCOLORTEMPLATE";
		}
		//@JAPR
		$varVisibles[17] = "BACKGROUNDIMAGE";
		/*
		$varVisibles[18] = "MENUMARGINTOP";
		$varVisibles[19] = "MENUMARGINLEFT";
		$varVisibles[20] = "MENUMARGINBOTTOM";
		$varVisibles[21] = "MENUMARGINRIGHT";
		*/
		$varVisibles[24] = "CUSTOMHEADER";
		//OMMC 2015-12-15: Agregado para ocultar la opción de descripción de la forma
		//OMMC 2016-04-28: Agregada la configuración de control de versión
		if (getMDVersion() >= esvHideDescription) {
			$varVisibles[28] = "HIDEDESCRIPTION";
		}
		//GCRUZ 2016-01-21. Opción para hora de ejecución de destinos incrementales
		$varVisibles[31] = "AUTOINCREMENTALGENERATIONTIME";
		//@JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
		//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$varVisibles[32] = "DEFAULTFORMSWIDTH";
			$varVisibles[33] = "DEFAULTFORMSHEIGHT";
			$varVisibles[34] = "DEFAULTFORMSTEXTPOSITION";
			$varVisibles[35] = "DEFAULTFORMSTEXTSIZE";
			$varVisibles[36] = "DEFAULTFORMSCUSTOMHTML";
		}
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			$varVisibles[37] = "SHOWQUESTIONNUMBERS";
 		}
		
		//RV 2016-04-14: Modo de despliegue de los enlaces 
		if (getMDVersion() >= esvConfLinks) {
			$varVisibles[38] = "LINKSDISPLAYMODE";
 		}
		
		//@JAPR 2016-05-19: Agregada la configuración para envío de GPS constante desde el App (#WRWF3Y)
		$varVisibles[40] = "APPGPSTRACKER";
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$varVisibles[44] = "ENABLENAVHISTORY";
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$varVisibles[45] = "AUTOFITIMAGE";
		}
		//@JAPR

		//MAPR 2018-02-19: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
		if (getMDVersion() >= esvAllMobile) {
			$varVisibles[46] = "ALLMOBILE";
		}
		if ( getMDVersion() >= esvBackgroundMode ) {
			$varVisibles[47] = "BACKGROUNDMODE";
		}
		//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
		//OMMC 2019-01-19: Comentado para que no pueda moverse el setting de mapas
		// if ( getMDVersion() >= esvMapTypeSelector ) {
		// 	$fieldTypes["MAPTYPE"]="combo";
		// 	$varVisibles[48] = "MAPTYPE";
		// }
		//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
		//Esta es una configuración interna, NO debe ser visible para el usuario, sólo se puede configurar bajo demanda vía el equipo de Soporte / Provisioning
		//$fieldTypes["DISABLEPREVIEWDUMMIES"]="combo";
		//$varVisibles[56] = "DISABLEPREVIEWDUMMIES";
		//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
		if (getMDVersion() >= esvForgetCredentials) {
			$varVisibles[58] = "FORGETCREDENTIALS";
		}
		//@JAPR
		foreach($this->ArrayVariables as $iVar=>$variable) {
			if(!in_array($variable, $varVisibles)) {
				unset($this->ArrayVariables[$iVar]);
			}
		}
?>
<html>
<?
	//Contiene lo necesario para el manejo del grid
	require_once("genericgrid.inc.php");
?>
	<script>
		optionsRegisterGPS = new Array;
		optionsRegisterGPS[0]="<?=translate("When saving data entry")?>";
		optionsRegisterGPS[1]="<?=translate("When starting data entry")?>";
		
		var opDisplayModeLinks;
		opDisplayModeLinks = {
			1:"<?=translate("Current page")?>",
			2:"<?=translate("New window")?>"
		};

<?		//@JAPR 2018-10-19: Integrado los Mapas de Apple (#64ISFM) ?>
		var opMapAPI;
		opMapAPI = {
			0:"<?=translate("Google maps")?>",
			1:"<?=translate("Apple Mapkit")?>"
		};
<?		//@JAPR ?>
		
		var tabSettings = "settings";
		var tabColors = "colors";
		var tabLinks = "links";
		//GCRUZ 2016-04-18. Opciónes para menú de 3 rayas en app
		var tabAppMenu = 'appmenu';
		var objFormTabs;					//Cejillas de la opción de configuraciones
		var catalogs = new Array;
<?
		//Muestra la lista de todos los catálogos a los que tiene acceso el usuario logeado, incluyendo el previamente seleccionado
		//por si él no tiene acceso
		require_once('catalog.inc.php');
		$arrCatalogs = BITAMCatalog::getCatalogs($this->Repository, true);
		$iCat=0;
		$categoryCatalogIndex=0;
		foreach($arrCatalogs as $catID => $catName)
		{
			?>
			catalogs[<?=$iCat?>]={value: "<?=$catID?>", text: "<?=addslashes($catName)?>"};
			<?
			if($catID==$this->ArrayVariableValues["CATEGORYCATALOG"])
			{	//índice que ocupa en el arreglo de java script el elemento seleccionado
				$categoryCatalogIndex=$iCat;
			}
			$iCat++;
		}
?>
		//Obtener las formas que se desplegarán en el combo de la variable DEFAULTSURVEY
<?
		require_once('survey.inc.php');
		//$intCurrentSurveyID = (int) $variable;
		$arrSurveys = array();
		//@JAPR 2015-09-02: Corregido un bug, no había necesidad de agregar el Ninungo ya que el campo se configuró para generarlo automáticamente
		//array_push($arrSurveys, translate("None"));
		$loadedSurveys = BITAMSurvey::getSurveys($this->Repository, true);
		foreach($loadedSurveys as $skey => $asurvey) {
			$arrSurveys[$skey] = $asurvey;
		}
		/*
		if ($intCurrentSurveyID > 0)
		{
			if (!isset($arrSurveys[$intCurrentSurveyID]))
			{
				$arrSurveys[$intCurrentSurveyID] = 'Current survey';
			}
		}
		*/
		//@JAPR 2016-07-28: Corregido el ordenamiento de las formas en la combo de forma default (#RFNIWR);
?>		
		var formsArray = new Object;
		var formsOrderArray = new Array;
<?		$iForm=0;
		$defSurveyIndex=0;
		foreach($arrSurveys as $formID => $formName)
		{
?>
			formsArray[<?=$formID?>]="<?=addslashes($formName)?>";
			formsOrderArray.push(<?=$formID?>);
<?
			if($formID==$this->ArrayVariableValues["DEFAULTSURVEY"])
			{	//índice que ocupa en el arreglo de java script el elemento seleccionado
				$defSurveyIndex=$iForm;
			}
			$iForm++;
		}
		
		//Opciones para DEFAULTCOLORTEMPLATE
		//$intTemplateID = (int) $variable;
		$arrColorTemplates = array();
		//@JAPR 2015-09-02: Corregido un bug, no había necesidad de agregar el Ninungo ya que el campo se configuró para generarlo automáticamente
		//$arrColorTemplates[-1] = translate("None");
		//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$objColorTemplatesColl = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
			if (!is_null($objColorTemplatesColl)) 
			{
				foreach($objColorTemplatesColl->Collection as $objColorTemplate)
				{
					$arrColorTemplates[$objColorTemplate->TemplateID] = $objColorTemplate->TemplateName;
				}
			}
		}
		/*
		if ($intTemplateID > 0)
		{
			if (!isset($arrColorTemplates[$intTemplateID]))
			{
				$arrColorTemplates[$intTemplateID] = translate("Current template");
			}
		}
		*/	
?>
		var optionsColorTemplate = new Object();
		var optionsColorTemplateOrder = new Array();
		
<?		$iColorTem=0;
		$defColorTmpIndex=0;
		foreach($arrColorTemplates as $templateID => $templateName) {
?>
			//optionsColorTemplate[<?=$iColorTem?>]={value: "<?=$templateID?>", text: "<?=$templateName?>"};
			<?//@JAPR 2018-11-09: Corregido el escape de algunos caracteres en strings (#KGW0CU)?>
			optionsColorTemplate[<?=$templateID?>]="<?=GetValidJScriptText($templateName)?>";
			optionsColorTemplateOrder.push(<?=$templateID?>);
<?
			if ($templateID == $this->ArrayVariableValues["DEFAULTCOLORTEMPLATE"]) {
				//índice que ocupa en el arreglo de java script el elemento seleccionado
				$defColorTmpIndex = $iColorTem;
			}
			$iColorTem++;
		}
		
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
?>
		var optionsFormsSizes = new Object;
		optionsFormsSizes[100] = "<?=translate("Size")." x1"?>";
		optionsFormsSizes[200] = "<?=translate("Size")." x2"?>";
		optionsFormsSizes[300] = "<?=translate("Size")." x3"?>";

		var optionsFormsLayOutTextPos = new Array;
		optionsFormsLayOutTextPos[<?=svtxpDefault?>] = "<?=translate("Image only")?>";
		optionsFormsLayOutTextPos[<?=svtxpBottom?>] = "<?=translate("Text below image")?>";
		optionsFormsLayOutTextPos[<?=svtxpLeft?>] = "<?=translate("Text before image")?>";
		optionsFormsLayOutTextPos[<?=svtxpRight?>] = "<?=translate("Text after image")?>";
		optionsFormsLayOutTextPos[<?=svtxpTop?>] = "<?=translate("Text above image")?>";
		optionsFormsLayOutTextPos[<?=svtxOverTop?>] = "<?=translate("Text inside and above image")?>";
		optionsFormsLayOutTextPos[<?=svtxOverMiddle?>] = "<?=translate("Text inside and center image")?>";
		optionsFormsLayOutTextPos[<?=svtxOverBottom?>] = "<?=translate("Text inside and below image")?>";
		optionsFormsLayOutTextPos[<?=svtxpCustom?>] = "<?=translate("Form custom layout html")?>";

		var appMenuOptions = {};
		var appSelMenuOptions = {};
<?
		//GCRUZ 2016-04-20. Opciones para configuración de menú de 3 rayas en app
		global $gbarrAppMenuOptions;
		$arrSelAppMenuOptions = array();
		//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
		//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
		//el menú de 3 rayas del App (#EJTH2E)
		if (!isset($this->ArrayVariableValues["APPMENUOPTIONS"]) || is_null($this->ArrayVariableValues["APPMENUOPTIONS"])) {
			$arrSelAppMenuOptions = array_keys($gbarrAppMenuOptions);
		}
		elseif ($this->ArrayVariableValues["APPMENUOPTIONS"] != '') {
			$arrSelAppMenuOptions = explode(',', $this->ArrayVariableValues["APPMENUOPTIONS"]);
		}
		//@JAPR
		
		foreach ($gbarrAppMenuOptions as $key => $value)
		{
			if (array_search($key, $arrSelAppMenuOptions) !== false)
			{
?>
				appSelMenuOptions[<?=$key?>] = "<?=translate($value)?>";
<?
			}
?>
			appMenuOptions[<?=$key?>] = "<?=translate($value)?>";	
<?
		}

		//@JAPR
		
		$arrJSON = BITAMSettingsVariableExt::getJSonDefinition($this->Repository);
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		$strJSONDefinitions = json_encode($arrJSON);
?>		
		var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
		var objSetttings = objFormsDefs.data;
		
		//******************************************************************************************************
		SettingsCls.prototype = new GenericItemCls();
		SettingsCls.prototype.constructor = SettingsCls;
		function SettingsCls() {
			this.objectType = AdminObjectType.otySetting;
		}
		
		/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
		//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
		*/
		SettingsCls.prototype.getDataFields = function(oFields) {
			var blnAllFields = false;
			if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
				blnAllFields = true;
			}
			var objParams = {};
<?			
			foreach($this->ArrayVariables as $variable)
			{				
?>
			if (blnAllFields || oFields["<?=$variable?>"]) {
				objParams["<?=$variable?>"] = this.<?=$variable?>;
			}
<?
			}
?>	
			return objParams;
		}
		
		//******************************************************************************************************			
		function doOnLoad() {
			console.log('doOnLoad');
			
			addClassPrototype();
			
			//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
			objWindows = new dhtmlXWindows({
				image_path:"images/"
			});
			objWindowsFn = new dhtmlXWindows({
				image_path:"images/"
			});
			objWindowsEd = new dhtmlXWindows({
				image_path:"images/"
			});
			
			objMainLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});
			objMainLayout.cells("a").appendObject("divDesign");
			objMainLayout.cells("a").hideHeader();
			//objMainLayout.setSizes();
			objFormTabs = new dhtmlXTabBar({
				parent:"divDesignBody"
			});
			objFormTabs.addTab(tabSettings, "<span class='tabFormsOpts'><?=translate("Settings")?></span>", null, null, true);
<?
	//@JAPR 2016-03-30: Validada la versión de metadata para la opción de templates
	if (getMDVersion() >= esvTemplateStyles)
	{
?>
			objFormTabs.addTab(tabColors, "<span class='tabFormsOpts'><?=translate("Color templates")?></span>");
<?
	}
	//@JAPR
?>
<?
	if (getMDVersion() >= esvLinks)
	{
?>
			objFormTabs.addTab(tabLinks, "<span class='tabFormsOpts'><?=translate("Links")?></span>");
<?
	}
	//GCRUZ 2016-04-18. Opciónes para menú de 3 rayas en app
	if (getMDVersion() >= esvAppMenuOptions)
	{
?>
			objFormTabs.addTab(tabAppMenu, "<span class='tabFormsOpts'><?=translate("App Menu")?></span>");
<?
	}
?>
			objFormTabs.setArrowsMode("auto");
			objFormTabs.tabs(tabSettings).attachLayout({
				parent: document.body,
				pattern: "1C"
			});
			objFormsLayout = objFormTabs.tabs(tabSettings).getAttachedObject();
			//Evento del cambio de tab para permitir grabar información que hubiera cambiado
			objFormTabs.attachEvent("onTabClick", function(id, lastId) {
				console.log('objFormTabs.onTabClick ' + id + ', lastId == ' + lastId);
			});
			objFormTabs.attachEvent("onContentLoaded", function(id) {
				objFormTabs.tabs(id).progressOff();
			});
			
			//Genera la ventana de propiedades
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?>");
			objPropsLayout = objFormsLayout.cells(dhxPropsCell).attachLayout({pattern: "1C"});
			objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Properties")?>");
			objPropsLayout.cells(dhxPropsDetailCell).hideHeader();
			objFormsLayout.cells(dhxPropsCell).hideHeader();
			
			//Agrega el evento para procesos cuando termina de cargar cada frame del layout
			objPropsLayout.attachEvent("onContentLoaded", function(id) {
				console.log('objPropsLayout.onContentLoaded ' + id);
				objPropsLayout.cells(id).progressOff();
			});
			
			doShowSettingProps();
<?
	//@JAPR 2016-03-30: Validada la versión de metadata para la opción de templates
	if (getMDVersion() >= esvTemplateStyles)
	{
?>
			doDrawColors();
<?
	}
	//@JAPR
?>
			
<?
	if (getMDVersion() >= esvLinks)
	{
?>
			doDrawLinks();
<?
	}
	//GCRUZ 2016-04-18. Opciónes para menú de 3 rayas en app
	if (getMDVersion() >= esvAppMenuOptions)
	{
?>
			doDrawAppMenuOptions();
<?
	}
?>
			//Resize completo de la ventana y prepara el evento para los resize posteriores
			doOnResizeScreen();

			$(window).resize(function() {
				doOnResizeScreen();
			});

			//Marca la opción de Settings como seleccionada
			$("#divDesignHeader").find("[tabname='settings']").addClass("linktdSelected");
		}
		
		function doDrawColors()
		{
			//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
			//objFormTabs.tabs(tabColors).attachURL("main.php?BITAM_SECTION=EFormsAppCustomizationTplExtCollection", false);
			objFormTabs.tabs(tabColors).attachURL("main.php?BITAM_SECTION=EFormsAppCustomStylesTplExtCollection", false);
		}
		
<?
	if (getMDVersion() >= esvLinks)
	{
?>
		function doDrawLinks()
		{
			objFormTabs.tabs(tabLinks).attachURL("main.php?BITAM_SECTION=LinkCollection", false);
		}
<?
	}
?>
		//GCRUZ 2016-04-18. Opciónes para menú de 3 rayas en app
		function doDrawAppMenuOptions()
		{
			objFormTabs.tabs(tabAppMenu).attachURL("google.com", false);
			var objFormData = [
				{type:"fieldset", offsetLeft:20,  name:"fldsAppMenuOptions", label:"<?=translate("Check options you want to be visible on the menu")?>", width:380, className:"selection_list", list:[
					{type:"container", name:"grdAppMenuOptions", inputWidth:360, inputHeight:200, className:"selection_container"}
				]}/*,
				{type:"newcolumn", offset:20},
				{type:"block", blockOffset:0, offsetTop:200, list:[
					{type:"button", name: "btnSelMenuOption", value:"", className:"assoc_button"},
					{type:"button", name: "btnRemMenuOption", value:"", className:"remove_button"}
				]},
				{type:"newcolumn", offset:20},
				{type:"fieldset",  name:"fldsSelAppMenuOptions", label:"<?=translate("Visible Menu Options")?>", width:380, className:"selection_list", list:[
					{type:"container", name:"grdSelAppMenuOptions", inputWidth:330, inputHeight:400, className:"selection_container"}
				]}*/
			];

			//Se tuvo que invocar a la creación de la forma mediante un div, ya que de lo contrario al hacer resize a la forma, sus fieldset y blocks se movían cuando no
			//cupiedan horizontalmente, así con el div se puede poner un Scroll y tenerlos siempre en la misma posición
			objFormTabs.tabs(tabAppMenu).attachObject("divAppMenuParent");
			//objUsersForm = objFormTabs.tabs(tabUsers).attachForm(objFormData);
			objAppMenu = new dhtmlXForm("divAppMenu", objFormData);
			$(objAppMenu.cont).addClass('customGrid');

			//Contenido de la lista de usuarios disponibles
			var objAppMenuOptions = new dhtmlXGridObject(objAppMenu.getContainer("grdAppMenuOptions"));
			objAppMenuOptions.setImagePath("<?=$strScriptPath?>/images/");
			objAppMenuOptions.setHeader("<?=translate("Menu Options")?>,#cspan");
			objAppMenuOptions.setInitWidthsP("10,90");
			objAppMenuOptions.setColAlign("center,left");
			objAppMenuOptions.setColTypes("ch,ro");
			objAppMenuOptions.enableEditEvents(false);
			//objAppMenuOptions.enableDragAndDrop(true);
			objAppMenuOptions.enableDragAndDrop(false);
			objAppMenuOptions.setColSorting("str");
			objAppMenuOptions.enableMultiselect(true);
			objAppMenuOptions.init();
			objAppMenuOptions.sortRows(1,"str","asc");
			
			/*
			//Contenido de la lista de usuarios invitados
			var objSelAppMenuOptions = new dhtmlXGridObject(objAppMenu.getContainer("grdSelAppMenuOptions"));
			objSelAppMenuOptions.setImagePath("<?=$strScriptPath?>/images/");
			objSelAppMenuOptions.setHeader("<?=translate("Menu Options")?>");
			objSelAppMenuOptions.setInitWidthsP("100");
			objSelAppMenuOptions.setColAlign("left");
			objSelAppMenuOptions.setColTypes("ro");
			objSelAppMenuOptions.enableEditEvents(false);
			objSelAppMenuOptions.enableDragAndDrop(true);
			objSelAppMenuOptions.setColSorting("str");
			objSelAppMenuOptions.enableMultiselect(true);
			objSelAppMenuOptions.init();
			objSelAppMenuOptions.sortRows(0,"str","asc");
			*/

			for (var strRowId in appMenuOptions) {
				if (appSelMenuOptions[strRowId])
				{
					objAppMenuOptions.addRow(strRowId, '1,'+appMenuOptions[strRowId]);
				}
				else
				{
					objAppMenuOptions.addRow(strRowId, '0,'+appMenuOptions[strRowId]);
				}
				objAppMenuOptions.setUserData(strRowId, "id", strRowId);
			}

			objAppMenuOptions.attachEvent("onCheck", function(rId,cInd,state){
				console.log('objAppMenuOptions.onCheck');
				console.log('rId='+rId+', cInd='+cInd+', state='+state);
				var checked=objAppMenuOptions.getCheckedRows(0);
				console.log(checked);
				doUpdateAppMenuOptions(AdminObjectType.otySetting, checked);
			});
			/*
			for (var strRowId in appSelMenuOptions) {
				objSelAppMenuOptions.addRow(strRowId, appSelMenuOptions[strRowId]);
				objSelAppMenuOptions.setUserData(strRowId, "id", strRowId);
			}
			

			objAppMenuOptions.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objAppMenuOptions.onDrop Forms');
				if (sObj == tObj) {
					return;
				}
				var arrSIds = sId.split(',');
				var movingAppOptions = [];
				objSelAppMenuOptions.forEachRow(function (id){
					if (arrSIds.indexOf(id) == -1)
					{
						movingAppOptions.push(id);
					}
				});
				movingAppOptions.sort();
				var strMovingAppOptions = movingAppOptions.toString();
				doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
			});

			objAppMenuOptions.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objAppMenuOptions.onRowDblClicked Forms');
				var movingAppOptions = [];
				this.moveRow(rId, "row_sibling", undefined, objSelAppMenuOptions);
				objSelAppMenuOptions.forEachRow(function (id){
					movingAppOptions.push(id);
				});
				movingAppOptions.sort();
				var strMovingAppOptions = movingAppOptions.toString();
				doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
			});
			
			objSelAppMenuOptions.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objSelAppMenuOptions.onDrop Users');
				if (sObj == tObj) {
					return;
				}
				var arrSIds = sId.split(',');
				var movingAppOptions = [];
				objSelAppMenuOptions.forEachRow(function (id){
					movingAppOptions.push(id);
				});
				movingAppOptions.sort();
				var strMovingAppOptions = movingAppOptions.toString();
				doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
			});
			
			//JAPR 2015-07-15: Agregado el evento del doble click
			objSelAppMenuOptions.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objSelAppMenuOptions.onRowDblClicked Forms');
				var movingAppOptions = [];
				this.moveRow(rId, "row_sibling", undefined, objAppMenuOptions);
				objSelAppMenuOptions.forEachRow(function (id){
					movingAppOptions.push(id);
				});
				movingAppOptions.sort();
				var strMovingAppOptions = movingAppOptions.toString();
				doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
			});

			objAppMenu.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnSelMenuOption":
						var strSelection = objAppMenuOptions.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objAppMenuOptions.moveRow(strId, "row_sibling", undefined, objSelAppMenuOptions);
									}
								}
								
								if (blnMove) {
									var movingAppOptions = [];
									objSelAppMenuOptions.forEachRow(function (id){
										movingAppOptions.push(id);
									});
									movingAppOptions.sort();
									var strMovingAppOptions = movingAppOptions.toString();
									doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
								}
							}
						}
						break;
					
					case "btnRemMenuOption":
						var strSelection = objSelAppMenuOptions.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								var movingForms = new Object();
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objSelAppMenuOptions.moveRow(strId, "row_sibling", undefined, objAppMenuOptions);
									}
								}
								
								if (blnMove) {
									var movingAppOptions = [];
									objSelAppMenuOptions.forEachRow(function (id){
										movingAppOptions.push(id);
									});
									movingAppOptions.sort();
									var strMovingAppOptions = movingAppOptions.toString();
									doUpdateAppMenuOptions(AdminObjectType.otySetting, strMovingAppOptions);
								}
							}
						}
						break;
				}
			});
			*/

		}

		//GCRUZ 2015-08-18
		/* Realiza el request para grabar permisos sobre formas
		*/
		function doUpdateAppMenuOptions(iAssocItemType, movingAppOptions) {
			console.log("doUpdateAppMenuOptions type == " + iAssocItemType);
			//debugger;
			
			var strMovingAppOptions = movingAppOptions;
			var objObject = getObject(AdminObjectType.otySetting);
			//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
			if (objObject && objObject.send) {
				var objParams = {};
				objParams["APPMENUOPTIONS"] = strMovingAppOptions;
				objParams["Process"] = "Edit";
				objParams["ObjectType"] = iAssocItemType;
				setTimeout(objObject.send(objParams), 100);
			}
		}

		/* Obtiene la referencia al objeto indicado por el tipo y id de los parámetros. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
		los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
		respuesta fijas de preguntas/secciones)
		*/
		function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
			var objObject = undefined;
			
			if (!objSetttings) {
				return objObject;
			}

			return objSetttings;
		}

		/* Permite hacer ajustes a la pantalla donde DHTMLX no lo hace correctamente */
		function doOnResizeScreen() {
			console.log('doOnResizeScreen');
			//Para permitir que se pinten correctamente las ventanas de Invitar usuario y grupos, es necesario ocultar momentáneamente los divs que las contienen
			//ya que DHTMLX no estaba redimensionando sus componentes al maximizar la ventana
			var intTopAdjust = 120;
			//$('#divInviteUsers .selection_container div.dhxform_container').height($(window).height() - intTopAdjust - 50);
			var intHeight = $(window).height();
			$('.selection_container div.dhxform_container').height(intHeight - intTopAdjust - 50);
			$('.selection_container div.objbox').height(intHeight - intTopAdjust - 90);
			$('.selection_list fieldset.dhxform_fs').height(intHeight - intTopAdjust);
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
//				if (objMainLayout) {
//					objMainLayout.setSizes();
//				}
//				if (objFormTabs) {
//					objFormTabs.setSizes();
//				}
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
		}

		function doChangeTab(sTabId) {
			console.log('doChangeTab ' + sTabId);
			//debugger;
			var objEvent = this.event || window.event;
			if (objEvent) {
				var objTarget = objEvent.target || objEvent.srcElement;
				if (objTarget) {
					$("#divDesignHeader td").removeClass("linktdSelected");
					$(objTarget).addClass("linktdSelected");
				}
			}
			if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
				return;
			}
			if(sTabId=="colors")
			{
				var scrColors = objFormTabs.tabs(sTabId).getFrame().src;
				//Ejemplo de la URL cuando está cargada la colección de templates:
				//http://kpionline8.bitam.com/artus/gen8/eSurveyV6RV/main.php?BITAM_SECTION=EFormsAppCustomizationTplExtCollection&dhxr1440429907861
				//Ejemplo de la URL cuando está cargada la pantalla de propiedades de un template de colores
				//http://kpionline8.bitam.com/artus/gen8/eSurveyV6RV/main.php?BITAM_PAGE=EFormsAppCustomizationExt&TemplateID=4&dhxr1440430385811
				//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
				//var indexURL = scrColors.search("BITAM_SECTION=EFormsAppCustomizationTplExtCollection");
				var indexURL = scrColors.search("BITAM_SECTION=EFormsAppCustomStylesTplExtCollection");
				//@JAPR
				if(indexURL==-1)
				{	//Si indexURL==-1 quiere decir que no está cargada la colección de templates en el tab de colores
					///alert("se llama doDrawColors por que indexURL:"+indexURL);
					objFormTabs.tabs(sTabId).detachObject();
					doDrawColors();
				}
			}
			else if(sTabId=="links")
			{
<?
	if (getMDVersion() >= esvLinks)
	{
?>
				var scrLinks = objFormTabs.tabs(sTabId).getFrame().src;
				var indexURL = scrLinks.search("BITAM_SECTION=LinkCollection");
				if(indexURL==-1)
				{	//Si indexURL==-1 quiere decir que no está cargada la colección de enlazadores
					objFormTabs.tabs(sTabId).detachObject();
					doDrawLinks();
				}
<?
	}
?>
			}
			
			objFormTabs.tabs(sTabId).setActive();
		}
		
		function addClassPrototype() {
			if (!objSetttings) {
				return;
			}
			
			$.extend(objSetttings, new SettingsCls());
		}
		
		function doShowSettingProps(){
			console.log('doShowSectionProps');
			objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
			generateSettingsProps();
		}
			/* Obtiene la referencia al objeto indicado por el tipo y id de los parámetros. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objObject = objSetttings;
				
				return objObject;
			}
		
		function generateSettingsProps() 
		{
			console.log('generateSettingsProps');
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Settings")?>");
			
			//Generar todos los fields
			objTabsColl = {
				"generalTab":{
					id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true,defaultVisibility:{name:true, descrip:true}
				}
<?	
	//@JAPR 2016-06-01: Validada la versión de metadata para la opción de templates (#FSMUZC)
	if (getMDVersion() >= esvTemplateStyles)
	{
?>,
				"layoutTab":{
					id:"layoutTab", name:"<?=translate("Default forms layouts")?>", defaultVisibility:{name:true, descrip:true}
				}
<?
	}
?>
			};
			
			var objCommon = {id:1, parentType:AdminObjectType.otySetting, tab:"generalTab"};
			objTabsColl["generalTab"].fields =  new Array(
			
			<?
				$numSetVariables = count($this->ArrayVariables);
				$iSetVar = 1;
				$strAnd = "";
				foreach($this->ArrayVariables as $variable)
				{	
					$fieldType=$fieldTypes[$variable];
					switch($fieldType)
					{
						case "input":
						{
							//JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
							if($variable!="DEFAULTFORMSTEXTSIZE") {
								echo($strAnd);
								$strAnd = ", ";
			?>
							new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.alphanum, maxLength:255}))
			<?		
							}
							break;
						}
						
						case "combo":
						{
							switch($variable)
							{
								case "CATEGORYCATALOG":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
								new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:arrCatalogs}))
			<?
									break;
								}
								
								case "CATEGORYMEMBER":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, parent:"CATEGORYCATALOG", type:GFieldTypes.combo}))
			<?
									break;
								}
								
								case "USEINCREMENTALAGGREGATIONS":
								case "ENABLECOLORTEMPLATES":
								case "USECACHE":
								//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
								case "ENABLENAVHISTORY":
								//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
								case "AUTOFITIMAGE":
								//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
								case "FORGETCREDENTIALS":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
								new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:optionsYesNo}))
			<?					
									break;
								}
								
								case "ALLMOBILE":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
								new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:optionsYesNo}))
			<?					
									break;
									
								}

								//@JAPR 2018-10-19: Integrado los Mapas de Apple (#64ISFM)
								case "MAPTYPE":
									echo($strAnd);
									$strAnd = ", ";
			?>
								new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:opMapAPI, default:1}))
			<?					
									break;
								//@JAPR
								
								case "REGISTERGPSDATAAT":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>					new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:optionsRegisterGPS}))
			<?
									break;
								}
								
								case "DEFAULTSURVEY":
								{
									echo($strAnd);
									$strAnd = ", ";
									//@JAPR 2016-07-28: Corregido el ordenamiento de las formas en la combo de forma default (#RFNIWR);
			?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:formsArray, optionsOrder:formsOrderArray}))
			<?
									break;
								}
								
								case "DEFAULTCOLORTEMPLATE":
								{
									//JAPR 2015-10-06: Removido temporalmente en lo que se implementan cambios al esquema de colores
									//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
									if (getMDVersion() >= esvTemplateStyles) {
										echo($strAnd);
										$strAnd = ", ";
			?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:optionsColorTemplate, optionsOrder:optionsColorTemplateOrder}))
			<?
									}
									break;
									//@JAPR
								}
								
								//OMMC 2015-12-15: Agregado para ocultar la config. de descripción de las formas.
								case "HIDEDESCRIPTION":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:optionsYesNo}))
			<?
									break;
								}
								
								//GCRUZ 2016-01-21. Opción para hora de ejecución de destinos incrementales
								case "AUTOINCREMENTALGENERATIONTIME":
								{
									echo($strAnd);
									$strAnd = ", ";
			?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.hour, options:new Array()}))
			<?
									break;
								}

								//OMMC 2015-12-15: Agregado para ocultar la config. de descripción de las formas.
								case "SHOWQUESTIONNUMBERS":
								{
									if (getMDVersion() >= esvShowQNumbers) {
										echo($strAnd);
										$strAnd = ", ";
				?>
										new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:optionsYesNo, default:1}))
				<?
									}
									break;
								}
								
								//RV 2016-04-14: Modo de despliegue de enlaces
								case "LINKSDISPLAYMODE":
								{
									if (getMDVersion() >= esvConfLinks) {
										echo($strAnd);
										$strAnd = ", ";
				?>
										new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.combo, options:opDisplayModeLinks, default:1}))
				<?
									}
									break;
								}
							}
							break;
						}
						
						case "image":
						{
							if($variable!="BACKGROUNDIMAGEVERSION")
							{
								echo($strAnd);
								$strAnd = ", ";
			?>
							new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.uploadImg, maxLength:255}))
			<?
							}
							break;
						}
						
						default:
						{
							break;
						}
					}
					$iSetVar++;
				}
			?>
			);
			
			<?
				//@JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
				//@JAPR 2016-06-01: Validada la versión de metadata para la opción de templates (#FSMUZC)
				if (getMDVersion() >= esvTemplateStyles)
				{
			?>
			var objCommon = {id:1, parentType:AdminObjectType.otySetting, tab:"layoutTab"};
			objTabsColl["layoutTab"].fields =  new Array(
			
			<?
					$numSetVariables = count($this->ArrayVariables);
					$iSetVar = 1;
					$strAnd = "";
					foreach($this->ArrayVariables as $variable)
					{	
						$fieldType=$fieldTypes[$variable];
						switch($fieldType)
						{
							case "input":
							{
								switch($variable)
								{
									//JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
									case "DEFAULTFORMSTEXTSIZE":
									{
										echo($strAnd);
										$strAnd = ", ";
							?>
										new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.alphanum, maxLength:255}))
							<?		
									}
								}
								break;
							}
							case "combo":
							{
								switch($variable)
								{
									//JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
									case "DEFAULTFORMSWIDTH":
									case "DEFAULTFORMSHEIGHT":
									{
										echo($strAnd);
										$strAnd = ", ";
				?>
										new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:optionsFormsSizes}))
				<?
										break;
									}
									
									case "DEFAULTFORMSTEXTPOSITION":
									{
										echo($strAnd);
										$strAnd = ", ";
				?>
										new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.combo, options:optionsFormsLayOutTextPos}))
				<?
										break;
										
									}
									//@JAPR
								}
								break;
							}
							
							//JAPR 2016-02-04: Agregados los LayOuts personalizados por forma (#FSMUZC)
							case "html":
								switch($variable)
								{
									case "DEFAULTFORMSCUSTOMHTML":
									{
										echo($strAnd);
										$strAnd = ", ";
				?>
									new FieldCls($.extend(true, {}, objCommon, {name:"<?=$variable?>", label:"<?=translate($variable)?>", empty:true, type:GFieldTypes.editorHTMLDialog, size:5000}))
				<?
										break;
									}
								}
								break;
							//JAPR
							
							default:
							{
								break;
							}
						}
						$iSetVar++;
					}
			?>
			);
			<?
				}
				//@JAPR
			?>
			
			//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
			//Los detalles de la forma se muestran en el tab de propiedades
			generateFormDetail(getSettingsFieldsDefaultVisibility(objTabsColl), AdminObjectType.otySetting, 1, objPropsLayout.cells(dhxPropsDetailCell));
		}

		/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default*/
		function getSettingsFieldsDefaultVisibility(oTabsColl) {
			console.log("getSettingsFieldsDefaultVisibility");
			
			var strTabId = "generalTab";
			for (var strTabId in oTabsColl) {
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};
				<?
					foreach($this->ArrayVariables as $variable)
					{
				?>
					oTabsColl[strTabId].defaultVisibility["<?=$variable?>"] = true;
				<?
					}
				?>
				}
			}
			return oTabsColl;
		}
		
		/* Regresa la definición del campo a partir de la combinación tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
		que se está desplegando en el momento en que se solicita, ya que esos son los que están almacenados en el array
		//JAPR 2015-06-25: Agregado el parámetro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
		que se está desplegando en la celda de propiedades
		*/
		function getFieldDefinition(sTabName, sFieldName, bForForm) {
			console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					for (var intCont in objTab.fields) {
						var objFieldTmp = objTab.fields[intCont];
						if (objFieldTmp && objFieldTmp.name == sFieldName) {
							objField = objFieldTmp;
							break;
						}
					}
				}
			}
			
			return objField;
		}
		
		/* Idéntica a getFieldDefinition pero en lugar del nombre de campo el cual es útil en un grid de propiedades, se utiliza el índice del campo, el cual es útil en
		un grid de valores */
		function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
			console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					objField = objTab.fields[iFieldIdx];
				}
			}
			
			return objField;
		}
		
		function doExecuteURL(id, ev, html, sCustomURL) {
			if(!id && !sCustomURL) {
				return;
			}
			//Si hay una URL personalizada, se le da preferencia a ella
			if (sCustomURL) {
				setTimeout(function() {
					objFormTabs.tabs(id).progressOn();
					objFormTabs.tabs(id).attachURL(sCustomURL);
				}, 100);
				return;
			}
		}
	</script>
	
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<!--
		<div id="divDesign" style="height:100%;width:100%;">
		</div>
		-->
		<div id="divColors" style="display:none;height:100%;width:100%;">
		</div>
		<div id="divAppMenuParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divAppMenu" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignHeader" class="linkToolbar" style="height:30px;width:100%">
				<table style="/*width:100%;*/height:100%;">
					<tbody>
						<tr>
							<td class="linkTD" tabName="settings" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Settings")?></td>
<?
	//@JAPR 2016-03-30: Validada la versión de metadata para la opción de templates
	if (getMDVersion() >= esvTemplateStyles)
	{
?>
							<td class="linkTD" tabName="colors" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Color templates")?></td>
<?
	}
	//@JAPR
?>
<?
	if (getMDVersion() >= esvLinks)
	{
?>
							<td class="linkTD" tabName="links"  onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Links")?></td>
<?
	}
	//GCRUZ 2016-04-18. Opción de configuración de Menú de tres rayas en app
	if (getMDVersion() >= esvAppMenuOptions)
	{
?>
							<td class="linkTD" tabName="appmenu" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("App Menu")?></td>
<?
	}
?>
<!--
							<td class="linkTD" tabName="save" style="text-decoration: none !important; cursor:auto;width:150px;">
								<div id="divSaving" class="dhx_cell_progress_img" style="display:none;position:relative !important;background-size:20px 20px;">&nbsp;</div>
								<div id="divSavingError" style="display:none;position:relative !important;background-size:20px 20px;cursor:pointer;" onclick="doShowErrors()">&nbsp;</div>
							</td>
-->
						</tr>
				  </tbody>
				</table>
			</div>
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
	</body>
</html>
<?
		die();
	}
}
?>