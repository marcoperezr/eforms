<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Obtenemos los datos del cubo global de encuestas a partir de la primera encuesta de la coleccion
if(isset($surveyCollection->Collection[0]))
{
	$surveyInstance = $surveyCollection->Collection[0];
	$strOriginalWD = getcwd();
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");

	$aEMailInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EmailDimID);
	chdir($strOriginalWD);
	$aStatusInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StatusDimID);
	chdir($strOriginalWD);
	$aSchedulerInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SchedulerDimID);
	chdir($strOriginalWD);
	
	$emailDimField = $aEMailInstanceDim->Dimension->TableName."KEY";
	$statusDimField = $aStatusInstanceDim->Dimension->TableName."KEY";
	$schedulerDimField = $aSchedulerInstanceDim->Dimension->TableName."KEY";
}

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	$aModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->ModelID);
	chdir($strOriginalWD);

	//Tablas de Hechos
	$tablaHechos = $aModel->nom_tabla;

	$valueEMail = 1;
	$valueStatus = 4;
	$valueScheduler = 1;
	
	if(true)
	{
		//Primer Metodo => Actualizacion global del cubo
		$sql = "UPDATE ".$tablaHechos." SET ".$emailDimField." = ".$valueEMail.", ".$statusDimField." = ".$valueStatus.", ".$schedulerDimField." = ".$valueScheduler." 
				WHERE ".$schedulerDimField." IS NULL OR ".$schedulerDimField." = 1";
	
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
		}
		else
		{
			echo('UPDATE: '.$sql.'<br>');
		}
	}
	else 
	{
		//Segundo Metodo => UPDATE registro por registro
		$sql = "SELECT A.FactKey FROM ".$tablaHechos." A WHERE A.".$schedulerDimField." IS NULL OR A.".$schedulerDimField." = ".$valueScheduler." ORDER BY A.FactKey";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if(!$aRS)
		{
			echo('Error en consulta de datos de Survey "'.$surveyInstance->SurveyName.'"(ID='.$surveyInstance->SurveyID.'). <br>SQL = '.$sql.'<br>');
			echo(translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
			continue;
		}
	
		while(!$aRS->EOF)
		{
			$factkey = (int)$aRS->fields["factkey"];
			
			$sql = "UPDATE ".$tablaHechos." SET ".$emailDimField." = ".$valueEMail.", ".$statusDimField." = ".$valueStatus.", ".$schedulerDimField." = ".$valueScheduler." 
					WHERE FactKey = ".$factkey;
	
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				echo(translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
			}
			else
			{
				echo('UPDATE: '.$sql.'<br>');
			}
	
			$aRS->MoveNext();
		}
	}
}
?>