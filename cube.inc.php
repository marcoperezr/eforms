<?php

class BITAMCube extends BITAMObject
{
	public $cla_concepto;
	public $nom_concepto;
	public $OwnerID;
	//Nomnbre de la tabla de hechos
	public $nom_tabla;
	//Nombre del campo fecha en la tabla de hechos
	public $fecha;
	public $tipo_cubo;
	//Nombre de la tabla donde están los periodos o tabla de tiempo
	public $dim_periodo;
	//ODBC donde está el cubo físicamente (donde está la tabla de hechos)
	public $servidor;
	//usuario y password para entrar a la B.D. indicada en $servidor
	public $usuario;
	public $password;
	//formato del campo fecha de la tabla de hechos
	public $fmt_fecha;
	//Indica si usa agregados
	public $usa_agregacion;
	//Indica si usa los nuevos agregados (solo aplica si usa_agregacion = 1)
	public $ag_new;
	//Tipo de Base de datos (según SI_BD)
	public $cla_bd;
	//@JAPR 2010-03-26: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
	public $tipo_conn;
	public $service_name;
	public $database_name;
	//Tabla de agregación 0
	public $catalogo_olap;
	//Indica si las dimensiones del cubo utilizan o no Claves (cuando no lo hacen, la clave es la propia descripción y solo
	//existe un campo por dimensión en las tablas de agregados
	public $show_alias;
	//Indica las dimensiones que no se encuentran en la tabla de hechos (se debe cargar mediante un método)
	public $DimensionsNotInFactTable;
	//Datos adicionales para el clonado de cubos (generación automática al crear versiones)
	public $reg_settings;
	public $Dimensions;
	public $Attributes;
	public $Joins;
	public $IndicatorGroupID;
	
	function __construct($aRepository)
	{	
		BITAMObject::__construct($aRepository);
	
		$this->cla_concepto=-1;
		$this->nom_concepto="";
		$this->OwnerID = $_SESSION["PABITAM_UserID"];
		$this->nom_tabla="";
		$this->fecha="";
		$this->tipo_cubo=-1;
		$this->dim_periodo="";
		$this->servidor="";
		$this->usuario="";
		$this->password="";
		$this->fmt_fecha="";
		$this->usa_agregacion=0;
		$this->ag_new=0;
		$this->cla_bd=-1;
		//@JAPR 2010-03-26: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
		$this->tipo_conn = 0;
		$this->service_name = '';
		$this->database_name = '';
		//@JAPR
		$this->catalogo_olap='';
		$this->show_alias=1;
		$this->DimensionsNotInFactTable = null;
		$this->reg_settings = 1033;
		$this->Dimensions = BITAMDimensionCollection::NewInstanceEmpty($aRepository);
		$this->Attributes = BITAMAttributeCollection::NewInstanceEmpty($aRepository);
		$this->Joins = BITAM_SI_DESCRIPT_KEYSCollection::NewInstanceEmpty($aRepository);
		$this->IndicatorGroupID = -1;
	}
	
	
	static function NewInstance($aRepository)
	{
		return new BITAMCube($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aCubeID)
	{
		global $workingMode;
		global $checkDisabledCubes;
		
		$anInstance = null;
		
		$theInfoBITAMCube=null;
		$theInfoBITAMCube =& BITAMGlobalInstance::GetBITAMCube($aCubeID);
		if(!is_null($theInfoBITAMCube))
		{
			return $theInfoBITAMCube;
		}
		
		if (((int) $aCubeID) < 0)
		{
			return $anInstance;
		}
		
		$sql = 
	"SELECT CLA_CONCEPTO, NOM_CONCEPTO, NOM_TABLA, FECHA, TIPO_CUBO, DIM_PERIODO, SERVIDOR, SERVICE_NAME, DATABASE_NAME, TIPO_CONN, USUARIO, PASSWORD, FMT_FECHA, USA_AGREGACION, CLA_BD, CATALOGO_OLAP, SHOW_ALIAS, CLA_OWNER ".(($_SESSION["PAArtusMDVersion"] >= 6.0)?',AG_NEW ':'')." FROM SI_CONCEPTO 
	WHERE CLA_CONCEPTO = ".$aCubeID;
	//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
	if ($checkDisabledCubes)
	{
		$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
	}
	//@JAPR

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMCube::NewInstanceFromRS($aRepository, $aRS);
		}
		
		//@JAPR 2010-03-26: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
		//Si se está en modo de trabajo BITAM, no tenemos una conexión fija a la BD fuente sino que esta se obtiene desde la instancia del Cubo
		//que se está procesando, así que al cargar la instancia hacemos la conexión a la Fuente mediante los datos leídos del cubo
		if($workingMode == wrkmBITAM)
		{
			if ($anInstance->tipo_conn == 0)
			{
				$aRepository->DataADODBDriver = 'odbc';
				$strServer = $anInstance->servidor;
			}
			else
			{
				$aRepository->DataADODBDriver = @GetNativeDriver($anInstance->cla_bd);
				$strServer = $anInstance->service_name;
			}
			$aRepository->DataADODBServer = $strServer;
			$aRepository->DataADODBDatabase = $anInstance->database_name;
			$aRepository->DataADODBUser = $anInstance->usuario;
			$aRepository->DataADODBPassword = BITAMDecryptPassword($anInstance->password);
			//@JAPR 2010-11-23: Corregido un bug, cuando la conexión era por odbc, la función formatValueForSQL requería comprobar el 
			//DataADODBType para poder reemplazar caracteres de escape en MySQL
			$aRepository->DataADODBDBType = $anInstance->cla_bd;
			//@JAPR
			$aRepository->open(false, true);
		}
		//@JAPR
		
		return $anInstance;
	}
	
	static function NewInstanceWithCubeName($aRepository, $aCubeName)
	{
		global $checkDisabledCubes;
		
		$anInstance = null;
		
		if( trim($aCubeName) == "" )
		{
			return $anInstance;
		}

		$anInstance =& BITAMGlobalInstance::GetBITAMCubeByName($aCubeName);
		
		if(is_null($anInstance))
		{
		
			$sql = "SELECT CLA_CONCEPTO, NOM_CONCEPTO, NOM_TABLA, FECHA, TIPO_CUBO, DIM_PERIODO, SERVIDOR, SERVICE_NAME, DATABASE_NAME, TIPO_CONN, USUARIO, PASSWORD, FMT_FECHA, USA_AGREGACION, CLA_BD, CATALOGO_OLAP, SHOW_ALIAS, CLA_OWNER ".(($_SESSION["PAArtusMDVersion"] >= 6.0)?',AG_NEW ':'')."FROM SI_CONCEPTO 
					WHERE NOM_CONCEPTO = ".$aRepository->ADOConnection->Quote($aCubeName);
			//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
			if ($checkDisabledCubes)
			{
				$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
			}
			//@JAPR
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			if (!$aRS->EOF)
			{
				$anInstance = BITAMCube::NewInstanceFromRS($aRepository, $aRS);
			}
			
			BITAMGlobalInstance::AddBITAMCubeByName($aCubeName, $anInstance);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aCubeID=(int) $aRS->fields["cla_concepto"];
		$anInstance = null;
		$anInstance =& BITAMGlobalInstance::GetBITAMCube($aCubeID);
		if(is_null($anInstance))
		{
			$anInstance = BITAMCube::NewInstance($aRepository);
			$anInstance->cla_concepto =  (int) $aRS->fields["cla_concepto"];
			$anInstance->nom_concepto =  $aRS->fields["nom_concepto"];
			$anInstance->OwnerID = (int) $aRS->fields["cla_owner"];
			$anInstance->nom_tabla =   $aRS->fields["nom_tabla"];
			$anInstance->fecha =   $aRS->fields["fecha"];
			$anInstance->tipo_cubo = (int) $aRS->fields["tipo_cubo"];
			$anInstance->dim_periodo =  $aRS->fields["dim_periodo"];
			$anInstance->servidor=   $aRS->fields["servidor"];
			$anInstance->usuario =   $aRS->fields["usuario"];
			//Está encriptado
		 	$anInstance->password = $aRS->fields["password"];
		 	//$anInstance->fmt_fecha = $aRS->fields["fmt_fecha"];
		 	$anInstance->fmt_fecha = "yyyy-MM-dd";
		 	$anInstance->usa_agregacion = $aRS->fields["usa_agregacion"];
		 	$anInstance->cla_bd = $aRS->fields["cla_bd"];
			//@JAPR 2010-03-26: Agregado el modo de trabajo BITAM para reutilizar los proyectos de Artus Web G6
		 	$anInstance->tipo_conn = (int) $aRS->fields["tipo_conn"];
		 	$anInstance->service_name = $aRS->fields["service_name"];
		 	$anInstance->database_name = $aRS->fields["database_name"];
		 	//@JAPR
		 	if(array_key_exists('catalogo_olap',$aRS->fields))
		 	{
		 		$anInstance->catalogo_olap = $aRS->fields["catalogo_olap"];
		 	}
		 	if(array_key_exists('show_alias',$aRS->fields))
		 	{
		 		if (!is_null($aRS->fields["show_alias"]))
		 		{
		 			$anInstance->show_alias = $aRS->fields["show_alias"];
		 		}
		 		else 
		 		{
		 			$anInstance->show_alias = 0;
		 		}
		 	}
		 	if(array_key_exists('ag_new',$aRS->fields))
		 	{
		 		$anInstance->ag_new = $aRS->fields["ag_new"];
		 		if (is_null($anInstance->ag_new))
		 		{
		 			$anInstance->ag_new = 0;
		 		}
		 	}
			BITAMGlobalInstance::AddBITAMCube($aCubeID, $anInstance);
		}
	 	
		return $anInstance;
	}
	
	static function getCubeIDByCubeNameAndCubeType($aRepository,$cubeName,$cubeType=null)
	{
		global $checkDisabledCubes;
		
		$cubeID=null;
		
		$cubeID =& BITAMGlobalInstance::GetCubeIDByCubeNameAndCubeType($cubeName,$cubeType);
		
		if(is_null($cubeID))
		{
			$sql="SELECT CLA_CONCEPTO AS CubeID FROM SI_CONCEPTO WHERE NOM_CONCEPTO =".$aRepository->ADOConnection->Quote($cubeName);
			//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
			if ($checkDisabledCubes)
			{
				$sql .= ' AND CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
			}
			//@JAPR
			
			if($cubeType!=null)
			{
				$sql.= " AND TIPO_CUBO=".$cubeType;
			}
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if (!$aRS->EOF)
			{
				$cubeID = (int) $aRS->fields["cubeid"];
			}
			
			BITAMGlobalInstance::AddCubeIDByCubeNameAndCubeType($cubeName, $cubeType, $cubeID);
		}
	
		return $cubeID;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = null;
		if (array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aCubeID = $aHTTPRequest->GET["CubeID"];
			$anInstance = BITAMCube::NewInstanceWithID($aRepository,$aCubeID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMCube::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMCube::NewInstance($aRepository);
		}
		return $anInstance;
	}
	
	function get_FormIDFieldName()
	{
		return 'cla_concepto';
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Cube";
		}
		else
		{
			return "BITAM_PAGE=Cube&CubeID=".$this->cla_concepto;
		}
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Cube");
		}
		else
		{  
			return $this->nom_concepto;
		}
	}

	function isNewObject()
	{
		return ($this->cla_concepto < 0);
	}

	function get_Parent()
	{
		return BITAMCubeCollection::NewInstance($this->Repository,null,3);
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_concepto";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_tabla";
		$aField->Title = translate("Table");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "servidor";
		$aField->Title = translate("DSN");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;
		*/
		
		return $myFields;
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMAttributeCollection::NewInstance($this->Repository,null,$this->cla_concepto,true);
			//$myChildren[] = BITAMDimensionCollection::NewInstance($this->Repository,null,$this->cla_concepto,true,false,false);
		}
		
		return $myChildren;
	}
	
	/*Obtiene el listado de las dimensiones que no se encuentran en la tabla de hechos, entre todas las dimensiones del cubo
	y asigna el resultado en el arreglo correspondiente de esta instancia de cubo
	*/
	public function getAllDimensionsNotInFactTable()
	{
		if (!is_null($this->DimensionsNotInFactTable))
		{
			return $this->DimensionsNotInFactTable;
		}
		
		$dimensions = BITAMDimensionCollection::NewInstance($this->Repository,null,$this->cla_concepto,true,false,false);
		$descriptKeysCollection=BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($this->Repository,$this->cla_concepto);
		$this->DimensionsNotInFactTable = BITAMDimension::getDimensionsNotInFactTable($dimensions, $descriptKeysCollection);
		return $this->DimensionsNotInFactTable;
	}
	
	/*Graba la información del cubo en la metadata. El parámetro $bSaveChildren indica que se deberán de grabar los elementos hijos
	del cubo tales como Dimensiones, Joins e Indicadores, en caso de que se trate de un cubo nuevo exclusivamente (en edición, cada
	elemento deberá grabarse individualmente). Si se especifica el parámetro $bCreateTables == true además se van creando las
	tablas correspondientes con la definición del cubo (no usado aún)
	El parámetro $iSourceCubeID indica la clave del cubo del cual deberá de obtener cierta información para insertarla como parte del
	nuevo cubo creado
	*/
	public function Save($bSaveChildren = false, $bCreateTables = false, $iSourceCubeID = -1)
	{
	 	if ($this->isNewObject())
		{
			$initialKey = GetBitamInitialKey($this->Repository);

			// Agrega el nuevo Cubo
			$sql = 'SELECT '.
						$this->Repository->ADOConnection->IfNull('MAX(CLA_CONCEPTO)', "".$initialKey."").' + 1 AS CLA_CONCEPTO FROM SI_CONCEPTO ';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$temporalID = (int)$aRS->fields["cla_concepto"];
			
			if($temporalID<($initialKey+1))
			{
				$temporalID = $initialKey+1;
			}

			$this->cla_concepto = $temporalID;
			$this->OwnerID = $_SESSION['PAtheUserID'];
			$sql = 'INSERT INTO SI_CONCEPTO (CLA_CONCEPTO, ESTATUS, NOM_CONCEPTO, NOM_TABLA, 
						FECHA, CAPTURABLE, TIPO_CUBO, CLA_BD, DIM_PERIODO, SERVIDOR, USUARIO, 
						PASSWORD, FMT_FECHA, REG_SETTINGS, USA_AGREGACION, SHOW_ALIAS, CLA_OWNER,
						USE_CACHE, NO_NULLS, NO_ZEROS, DATE_KEY)
			    VALUES ('.$this->cla_concepto.', 1, '.
					$this->Repository->ADOConnection->Quote($this->nom_concepto).', '.
					$this->Repository->ADOConnection->Quote($this->nom_tabla).', '.
					$this->Repository->ADOConnection->Quote($this->fecha).', 0, 3, '.
					$this->cla_bd.', '.
					$this->Repository->ADOConnection->Quote($this->dim_periodo).', '.
					$this->Repository->ADOConnection->Quote($this->servidor).', '.
					$this->Repository->ADOConnection->Quote($this->usuario).', '.
					$this->Repository->ADOConnection->Quote($this->password).', '.
					$this->Repository->ADOConnection->Quote($this->fmt_fecha).', '.
					$this->reg_settings.', '.$this->usa_agregacion.', 1, '.
					'0, 0, 0, 0, 0)';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die(translate("Error inserting ")." SI_CONCEPTO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Agrega las definiciones de los elementos adicionales del cubo
			if ($bSaveChildren)
			{
				//Dimensiones
				foreach ($this->Dimensions as $aDimension)
				{
					$aDimension->cla_concepto = $this->cla_concepto;
					if ($aDimension->consecutivo < 10)
					{
						$strParameters = '0'.$aDimension->consecutivo;
					}
					else 
					{
						$strParameters = $aDimension->consecutivo;
					}
					
					if ($aDimension->dependsOnDimension > 0)
					{
						if ($aDimension->dependsOnDimension < 10)
						{
							$strParameters .= '|0'.$aDimension->dependsOnDimension;
						}
						else 
						{
							$strParameters .= '|'.$aDimension->dependsOnDimension;
						}
					}
					
					$sql = 'INSERT INTO SI_CPTO_LLAVE (NIVEL, CLA_CONCEPTO, CLA_DESCRIP, NOM_LOGICO, NOM_FISICO, '.
							'TIPO_DATO, NOM_DESCRIPTOR, CONSECUTIVO, LONG_DATO, PAR_DESCRIPTOR, ESTATUS, EDIT_OUTLINE, AGRUPADOR,'.
							'CLA_PERIODO, FMT_PERIODO, ORDEN_DEL_SERVER) '.
					    'VALUES ('.$aDimension->consecutivo.', '.$aDimension->cla_concepto.', '.$aDimension->cla_descrip.', '.
							$this->Repository->ADOConnection->Quote($aDimension->nom_logico).', '.
							$this->Repository->ADOConnection->Quote($aDimension->nom_fisicok_bd).', '.
							$aDimension->tipo_datok.', '.
							$this->Repository->ADOConnection->Quote($aDimension->nom_fisico_bd).', '.
							$aDimension->consecutivo.', '.$aDimension->long_datok.', '.
							$this->Repository->ADOConnection->Quote($strParameters).', '.
							'1, 0, '.
							$this->Repository->ADOConnection->Quote('').', '.
							$aDimension->cla_periodo.', '.
							$this->Repository->ADOConnection->Quote($aDimension->fmt_periodo).
							', 0)';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die(translate("Error inserting ")." SI_CPTO_LLAVE ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}

				//Grupo de Indicadores
				$anIndicatorGroupID = -1;
				if (count($this->Attributes->Collection) > 0)
				{
					$sql = 'SELECT '.
								$this->Repository->ADOConnection->IfNull('MAX(CLA_GPO_IND)', "".$initialKey."").' + 1 AS CLA_GPO_IND FROM SI_GPO_IND ';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error accessing")." SI_GPO_IND ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$temporalID = (int)$aRS->fields["cla_gpo_ind"];
					
					if($temporalID<($initialKey+1))
					{
						$temporalID = $initialKey+1;
					}
					
					$anIndicatorGroupID = $temporalID;
					
					$sql = 'INSERT INTO SI_GPO_IND (CLA_GPO_IND, NOM_GPO_IND, CLA_OWNER)'.
						' VALUES ('.$anIndicatorGroupID.', '.
						$this->Repository->ADOConnection->Quote($this->nom_concepto).', '.$_SESSION['PAtheUserID'].')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error inserting")." SI_GPO_IND ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$this->IndicatorGroupID = $anIndicatorGroupID;
				}
				
				//Atributos
				//Obtiene la lista de Indicadores para obtener los Usuarios y Grupos de Usuarios con Acceso
				$strIndicatorList = '';
				foreach ($this->Attributes as $anAttribute)
				{
					$strIndicatorList .= ','.$anAttribute->cla_indicador;
				}
				$strIndicatorList = trim(substr($strIndicatorList, 1));
				
				foreach ($this->Attributes as $anAttribute)
				{
					// Agrega el nuevo Indicador Base a la definición del Cubo
					$anAttribute->cla_concepto = $this->cla_concepto;
					$sql = 'INSERT INTO SI_CPTO_ATRIB (ORDEN, CLA_CONCEPTO, NOM_FISICO, NOM_LOGICO, TIPO_DATO, 
							ESTATUS, CREA_INDICADOR, CONSECUTIVO, LONG_DATO, AGRUPADOR) 
					    VALUES ('.$anAttribute->orden.', '.$anAttribute->cla_concepto.', '.
							$this->Repository->ADOConnection->Quote($anAttribute->nom_fisico_bd).', '.
							$this->Repository->ADOConnection->Quote($anAttribute->nom_logico).', '.
							$anAttribute->tipo_dato.', 1, '.$anAttribute->crea_indicador.', '.$anAttribute->consecutivo.', '.
							$this->Repository->ADOConnection->Quote($anAttribute->long_dato).', '.
							$this->Repository->ADOConnection->Quote($anAttribute->agrupador).')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die(translate("Error inserting ")." SI_CPTO_ATRIB ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					// Agrega el nuevo Indicador a la definición del Cubo
					$sql = 'SELECT '.
								$this->Repository->ADOConnection->IfNull('MAX(CLA_INDICADOR)', "".$initialKey."").' + 1 AS CLA_INDICADOR FROM SI_INDICADOR ';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error accessing")." SI_INDICADOR ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$temporalID = (int)$aRS->fields["cla_indicador"];
					
					if($temporalID<($initialKey+1))
					{
						$temporalID = $initialKey+1;
					}
					
					$anIndicatorID = $temporalID;
					
					$strFormat = $anAttribute->formato;
					$strUserFormula = '{'.$anAttribute->nom_logico.'}';
					$strDBFormula = '('.$anAttribute->agrupador.'( t1.'.$anAttribute->nom_fisico.'))';
					$sql = 'INSERT INTO SI_INDICADOR (CLA_INDICADOR, CLA_CONCEPTO, NOM_INDICADOR, FORMULA_USR, NOM_TABLA, 
							FORMULA_BD, LLAVES, TIPO_AGRUPACION, FORMATO, CLA_PADRE, TIPO_INDICADOR, ES_ATRIBUTO, CLA_OWNER,
							DIMENSIONES) 
						VALUES ('.$anIndicatorID.', '.$anAttribute->cla_concepto.', '.
							$this->Repository->ADOConnection->Quote($anAttribute->nom_logico).', '.
							$this->Repository->ADOConnection->Quote($strUserFormula).', '.
							$this->Repository->ADOConnection->Quote($this->nom_tabla).', '.
							$this->Repository->ADOConnection->Quote($strDBFormula).', '.
							$this->Repository->ADOConnection->Quote('').', '.
							$this->Repository->ADOConnection->Quote('NORMAL').', '.
							$this->Repository->ADOConnection->Quote($strFormat).', '.
							'-1, 1, 1, '.$_SESSION['PAtheUserID'].','.
							$this->Repository->ADOConnection->Quote($anAttribute->dimensiones).')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error inserting")." SI_INDICADOR ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					if ($anIndicatorGroupID > 0)
					{
						$sql = 'INSERT INTO SI_GPO_IND_DET (CLA_GPO_IND, CLA_INDICADOR)'.
							' VALUES ('.$anIndicatorGroupID.', '.$anIndicatorID.')';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die( translate("Error inserting")." SI_GPO_IND_DET ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
				
				//Indicadores por Usuario y Grupo de Usuario
				if ($anIndicatorGroupID > 0 && strlen($strIndicatorList) > 0)
				{
					$sql = 'INSERT INTO SI_USR_IND (CLA_USUARIO, CLA_GPO_IND)'.
						' SELECT A.CLA_USUARIO, '.$anIndicatorGroupID.
						' FROM SI_USR_IND A, SI_GPO_IND_DET B, SI_INDICADOR C'.
						' WHERE A.CLA_GPO_IND = B.CLA_GPO_IND AND B.CLA_INDICADOR = C.CLA_INDICADOR AND C.CLA_INDICADOR IN ('.$strIndicatorList.')'.
						' GROUP BY A.CLA_USUARIO';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error inserting")." SI_USR_IND ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}

					$sql = 'INSERT INTO SI_ROL_IND (CLA_ROL, CLA_GPO_IND)'.
						' SELECT A.CLA_ROL, '.$anIndicatorGroupID.
						' FROM SI_ROL_IND A, SI_GPO_IND_DET B, SI_INDICADOR C'.
						' WHERE A.CLA_GPO_IND = B.CLA_GPO_IND AND B.CLA_INDICADOR = C.CLA_INDICADOR AND C.CLA_INDICADOR IN ('.$strIndicatorList.')'.
						' GROUP BY A.CLA_ROL';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error inserting")." SI_ROL_IND ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				//Joins
				foreach ($this->Joins as $aDescriptKey)
				{
					$aDescriptKey->cla_concepto = $this->cla_concepto;
					$sql = 'INSERT INTO SI_DESCRIP_KEYS (CLA_CONCEPTO, NOM_TABLA, CONSECUTIVO, NOM_FISICOK, NOM_FISICOK_JOIN) 
						VALUES ('.$aDescriptKey->cla_concepto.', '.
							$this->Repository->ADOConnection->Quote($aDescriptKey->nom_tabla).', '.
							$aDescriptKey->consecutivo.', '.
							$this->Repository->ADOConnection->Quote($aDescriptKey->nom_fisicok).', '.
							$this->Repository->ADOConnection->Quote($aDescriptKey->nom_fisicok_join).')';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error accessing")." SI_DESCRIP_KEYS ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
				
				if ($iSourceCubeID > 0)
				{
					//Periodos por cubo
					$sql = 'INSERT INTO SI_PERIODOSxCUBO (CLA_PERIODO, CLA_CONCEPTO)'.
						' SELECT CLA_PERIODO, '.$this->cla_concepto.
						' FROM SI_PERIODOSxCUBO '.
						' WHERE CLA_CONCEPTO = '.$iSourceCubeID;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error inserting")." SI_PERIODOSxCUBO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//Jerarquias
					$arrHiearchies = array();
					//$sql = 'INSERT INTO SI_JERARQUIA_CAT (CLA_JERARQUIA, CLA_CONCEPTO, NOM_JERARQUIA)'.
					$sql = 'SELECT CLA_JERARQUIA, '.$this->cla_concepto.', NOM_JERARQUIA'.
						' FROM SI_JERARQUIA_CAT'.
						' WHERE CLA_CONCEPTO = '.$iSourceCubeID;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS !== false)
					{
						//die( translate("Error inserting")." SI_JERARQUIA_CAT ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						while (!$aRS->EOF)
						{
							$sql = 'SELECT '.
										$this->Repository->ADOConnection->IfNull('MAX(CLA_JERARQUIA)', "".$initialKey."").' + 1 AS CLA_JERARQUIA FROM SI_JERARQUIA_CAT ';
							$aRSMax = $this->Repository->ADOConnection->Execute($sql);
							if ($aRSMax === false)
							{
								die( translate("Error accessing")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							$temporalID = (int)$aRSMax->fields["cla_jerarquia"];
							
							if($temporalID<($initialKey+1))
							{
								$temporalID = $initialKey+1;
							}

							//Hay que crear una nueva jerarquia con clave única pero mapeando la clave anterior para posteriormente actualizar el detalle
							$aNewHierarchyID = $temporalID;
							$arrHiearchies[(int) $aRS->fields["cla_jerarquia"]] = $aNewHierarchyID;
							
							$sql = 'INSERT INTO SI_JERARQUIA_CAT (CLA_JERARQUIA, CLA_CONCEPTO, NOM_JERARQUIA)'.
								' VALUES ('.$aNewHierarchyID.','.$this->cla_concepto.','.
									$this->Repository->ADOConnection->Quote((string) $aRS->fields["nom_jerarquia"]).')';
							$aRSIns = $this->Repository->ADOConnection->Execute($sql);
							if ($aRSIns === false)
							{
								//die( translate("Error inserting")." SI_JERARQUIA_CAT ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							$aRS->MoveNext();
						}
					}
					
					//$sql = 'INSERT INTO SI_JERARQUIA (CLA_JERARQUIA, CONSECUTIVO, CLA_CONCEPTO, DIM_PADRE, DIM_HIJA)'.
					$sql = 'SELECT CLA_JERARQUIA, CONSECUTIVO, '.$this->cla_concepto.', DIM_PADRE, DIM_HIJA'.
						' FROM SI_JERARQUIA'.
						' WHERE CLA_CONCEPTO = '.$iSourceCubeID;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS !== false)
					{
						//die( translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						while (!$aRS->EOF)
						{
							$sql = 'INSERT INTO SI_JERARQUIA (CLA_JERARQUIA, CONSECUTIVO, CLA_CONCEPTO, DIM_PADRE, DIM_HIJA)'.
								' VALUES ('.@$arrHiearchies[$aRS->fields["cla_jerarquia"]].','.$aRS->fields["consecutivo"].','.$this->cla_concepto.','.
									$aRS->fields["dim_padre"].','.$aRS->fields["dim_hija"].')';
							$aRSIns = $this->Repository->ADOConnection->Execute($sql);
							if ($aRSIns === false)
							{
								//die( translate("Error inserting")." SI_JERARQUIA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							$aRS->MoveNext();
						}
					}
					
					//Seguridad
					//Por Grupos de Usuario
					$sql = 'INSERT INTO SI_ROL_CONFIG (CLA_ROL, PRIORIDAD, CLA_CONCEPTO, MINIMO, FILTRO, NIVELES, JERARQUIA)'.
						' SELECT CLA_ROL, PRIORIDAD, '.$this->cla_concepto.', MINIMO, FILTRO, NIVELES, JERARQUIA'.
						' FROM SI_ROL_CONFIG'.
						' WHERE CLA_CONCEPTO = '.$iSourceCubeID;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error inserting")." SI_ROL_CONFIG ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//Por Usuario
					$sql = 'INSERT INTO SI_USR_CONFIG (CLA_USUARIO, CLA_CONCEPTO, MINIMO, FILTRO, NIVELES, JERARQUIA)'.
						' SELECT CLA_USUARIO, '.$this->cla_concepto.', MINIMO, FILTRO, NIVELES, JERARQUIA'.
						' FROM SI_USR_CONFIG'.
						' WHERE CLA_CONCEPTO = '.$iSourceCubeID;
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error inserting")." SI_USR_CONFIG ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			
			// Actualiza el Cache de Artus
			$sql = 'DELETE FROM SI_CACHE_METADATA 
				WHERE CLA_CACHE IN (1, 7)';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = 'DELETE FROM SI_CACHE_METADATA 
				WHERE CLA_CACHE IN (2) AND CLA_CONCEPTO = '.$this->cla_concepto;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error deleting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strCacheDate = formatADateTime(date('Y-m-d H:i:s'),'yyyy-mm-dd hh:mm:ss');
			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (1, 0, '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (2, '.$this->cla_concepto.', '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = 'INSERT INTO SI_CACHE_METADATA (CLA_CACHE, CLA_CONCEPTO, FECHA) 
				VALUES (7, 0, '.
				$this->Repository->ADOConnection->Quote($strCacheDate).')';
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error inserting")." SI_CACHE_METADATA ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_CONCEPTO SET".
					" NOM_CONCEPTO = ".$this->Repository->ADOConnection->Quote($this->nom_concepto).
					", NOM_TABLA = ".$this->Repository->ADOConnection->Quote($this->nom_tabla).
					", FECHA = ".$this->Repository->ADOConnection->Quote($this->fecha).
					", DIM_PERIODO = ".$this->Repository->ADOConnection->Quote($this->dim_periodo).
					", SERVIDOR = ".$this->Repository->ADOConnection->Quote($this->servidor).
					", USUARIO = ".$this->Repository->ADOConnection->Quote($this->usuario).
					", PASSWORD = ".$this->Repository->ADOConnection->Quote($this->password).
					", BudgetTemplateID = ".$this->BudgetTemplateID.
					", FMT_FECHA=".$this->Repository->ADOConnection->Quote($this->fmt_fecha).
					", REG_SETTINGS = ".$this->reg_settings.
				" WHERE CLA_CONCEPTO = ".$this->cla_concepto;
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die(translate("Error inserting ")." SI_CONCEPTO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$this->Repository->addPhPCacheRequest(MDOBJECT_Cube, $this->cla_concepto, MDOBJECTDATA_All);
		return $this;
	}
	
	/*Crea una tabla basada en la de hechos pero solo con los indicadores Atributo y las dimensiones especificadas, utilizando
	como nombre de la tabla las primeras 10 letras del nombre del cubo anteponiendo el prefijo "EKTV" seguido de la clave del cubo
	y con una terminación numérica indicada como parámetro (inicialmente el número de versión que se está generando, el cuales 
	un consecutivo en SI_EKT_VERSION)
	Regresa la clave del nuevo cubo creado para esta versión a partir del original, o -1 en caso de error
	//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
	//Agregado el parámetro $calcIndicatorIDs para especificar que indicadores calculados se agregarán a la versión
	*/
	public function createVersionTable($dimensionIDs, $indicatorIDs, $iVersionID, $sFilter, $aStartDate, $aEndDate, &$sNewTableName, &$iNumRecords, $calcIndicatorIDs = null)
	{
		require_once("version.inc.php");
		
		$aRepository = $this->Repository;
		$sNewTableName = '';
		$aTableName = "EKTV_".$this->cla_concepto."_".$iVersionID;
		@addProcessLogRow(translate('Reading cube scheme').': '.$this->nom_concepto, $iVersionID, 'infoGenIndCal');
		
		$indicators = BITAMIndicatorCollection::NewInstance($aRepository,$indicatorIDs);
		$dimensions = BITAMDimensionCollection::NewInstance($aRepository,$dimensionIDs,$this->cla_concepto,true,false,false);
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		//Clona la colección de Indicadores para concatenar la de Indicadores Calculados y hacer un solo procesamiento
		$allIndicators = clone($indicators);
		if (!is_null($calcIndicatorIDs) && count($calcIndicatorIDs) > 0)
		{
			//Por el momento solo se cargan los indicadores calculados que pertenecen al mismo cubo que se está versionando
			$calcIndicators = BITAMIndicatorCollection::NewInstance($aRepository,$calcIndicatorIDs, $this->cla_concepto);
			foreach ($calcIndicators as $anIndicator)
			{
				$allIndicators->Collection[] = $anIndicator;
			}
		}
		//@JAPR
		
		//Valida que existan elementos a agregar en la tabla
		if (count($dimensions->Collection) == 0 || count($indicators->Collection) == 0)
		{
			@addProcessLogRow(translate('There were either no dimensions or indicators found in this cube'.'. '.translate('The process was aborted')), $iVersionID, 'infoGenIndCal');
			return -1;
		}
		
		//$metaColumns = $aRepository->DataADOConnection->MetaColumns($aTableID);
		$metaColumns=$aRepository->GetTableSchema($this->nom_tabla,true);
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		foreach ($allIndicators as $anIndicator)
		{
			$anIndicator->ADODataType = $metaColumns[strtoupper($anIndicator->nom_campo_bd)]->type;
			switch ($anIndicator->ADODataType)
			{
				case 'D':
				case 'T':
					$anIndicator->DataType = 12;	// Fecha
					break;
				case 'I':
					$anIndicator->DataType = 7;		// Entero
					break;
				default:
					$anIndicator->DataType = 8;		// Flotante
					break;
			}
		}
		
		$aCube = BITAMCube::NewInstanceWithID($aRepository, $this->cla_concepto);
		$aCube->getAllDimensionsNotInFactTable();
		
		//Lee todos los tipos de dato del tipo de base de datos que es el cubo
		$arrDBTypes = array();
		$sql = 'SELECT TIPO_DATO, NOM_DATO FROM SI_BD_TIPOS '.
			'WHERE CLA_BD = '.$aCube->cla_bd;
		@updateVersionLogFile($iVersionID, $sql, true);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die( translate("Error accessing")." SI_BD_TIPOS ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			@addProcessLogRow(translate('Error accesing').' SI_BD_TIPOS '.translate('table').': '.$aRepository->ADOConnection->ErrorMsg(), $iVersionID, 'infoGenIndCal');
		}
		else
		{
			while (!$aRS->EOF)
			{
				$arrDBTypes[(int)$aRS->fields["tipo_dato"]] = $aRS->fields["nom_dato"];
				$aRS->MoveNext();
			}
		}
		
		$strDateTimeType = 'DateTime';
		if (array_key_exists(12, $arrDBTypes))
		{
			$strDateTimeType = $arrDBTypes[12];
		}
		
		//El campo fecha siempre tendrá el nombre del campo tal como se usa en el Cubo/Tabla de tiempo
		@addProcessLogRow(translate('Creating cube version table').': '.$aTableName, $iVersionID, 'infoGenIndCal');
		$sql = 'CREATE TABLE '.$aTableName.' ('.$aCube->fecha.' '.$strDateTimeType.' NOT NULL';
		
		//Agrega todas las dimensiones especificadas
		foreach ($dimensions as $aDimension)
		{
			if (!array_key_exists($aDimension->consecutivo, $aCube->DimensionsNotInFactTable))
			{
				$strDataType = 'Integer';
				if (array_key_exists($aDimension->tipo_datok, $arrDBTypes))
				{
					$strDataType = $arrDBTypes[$aDimension->tipo_datok];
				}
				else
				{
					//die( translate("Dimension data type does not exists").": ".$aDimension->tipo_datok);
					@addProcessLogRow(translate('Dimension data type does not exists').': '.$aDimension->tipo_datok, $iVersionID, 'infoGenIndCal');
				}
				
				switch ($aDimension->tipo_datok)
				{
					case 1:
					case 2:
		 				$strDataType .= '('.$aDimension->long_dato.')';
		 				break;
				}
				
				$strKeyFieldDefinition = $aDimension->nom_fisicok_bd.$aDimension->consecutivo.' '.$strDataType.' NOT NULL';
				$sql .= ', '.$strKeyFieldDefinition;
			}
		}
		
		//Agrega todos los indicadores especificados
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		foreach ($allIndicators as $anIndicator)
		{
			$strDataType = 'Integer';
			if (array_key_exists($anIndicator->DataType, $arrDBTypes))
			{
				$strDataType = $arrDBTypes[$anIndicator->DataType];
			}
			else
			{
				//die( translate("Indicator data type does not exists").": ".$anIndicator->DataType);
				@addProcessLogRow(translate('Indicator data type does not exists').': '.$anIndicator->DataType, $iVersionID, 'infoGenIndCal');
			}
			
			switch ($anIndicator->DataType)
			{
				case 1:
				case 2:
	 				$strDataType .= '(30)';		// No se supone que se soporten indicadores tipo String, pero por si acaso...
	 				break;
			}
			
			$strKeyFieldDefinition = $anIndicator->nom_campo_bd.' '.$strDataType.' NULL';
			$sql .= ', '.$strKeyFieldDefinition;
		}
		$sql .= ')';

		@updateVersionLogFile($iVersionID, $sql, true);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//Si no se pudo crear la tabla, puede significar solo que ya existía, así que no debe terminar el proceso
			//die( translate("Error creating")." ".$aTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			@addProcessLogRow(translate('Error creating cube version table').': '.$aRepository->DataADOConnection->ErrorMsg(), $iVersionID, 'infoGenIndCal');
		}
		else
		{
			@addProcessLogRow(translate('Cube version table successfully created'), $iVersionID, 'infoGenIndCal');
		}
		
		$sNewTableName = $aTableName;
		$this->fillVersionTable($dimensionIDs, $indicatorIDs, $iVersionID, $sFilter, $aStartDate, $aEndDate, $iNumRecords, $calcIndicatorIDs);
		
		//@JAPR 2008-06-03: Agregada la creación del cubo para la tabla de Versión
		//Crea la estructura del cubo con sus elementos asociados en la metadata para que la tabla de versión esté disponible para consulta
		//@JAPR 2008-09-09: Agregada la generación del cubo de Versiones
		$intVersionCubeID = $aCube->mapVersionCube($iVersionID, $aTableName, $indicatorIDs, $calcIndicatorIDs);
		//@JAPR 2008-09-09
		
		/*
		$aNewCube = clone($aCube);
		//Limpia los datos que identifican al cubo como uno ya existente para que se force a un Insert
		$aNewCube->cla_concepto = -1;
		$aNewCube->nom_concepto .= ' ('.translate('Version').' '.$iVersionID.')';
		$aNewCube->nom_tabla = $aTableName;
		$aNewCube->usa_agregacion = 0;
		$aNewCube->ag_new = 0;

		//Clona los Joins del cubo
		$bTimeJoinFound = false;
		$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository,$this->cla_concepto);
		foreach ($descriptKeysCollection as $aDescriptKey)
		{
			$aNewDescripKey = clone($aDescriptKey);
			$aNewDescripKey->cla_concepto = -1;
			//Si se trata del Join de la tabla de tiempo, hay que cambiarlo para que utilice el campo Fecha exclusivamente. Solo
			//se puede tener un Join a la tabla de tiempo por cubo
			if (!$bTimeJoinFound && $aNewDescripKey->nom_tabla == $aNewCube->dim_periodo)
			{
				$aNewDescripKey->nom_fisicok = $aNewCube->fecha;
				$aNewDescripKey->nom_fisicok_join = $aNewCube->fecha;
				$bTimeJoinFound = true;
			}
			$aNewCube->Joins->Collection[] = $aNewDescripKey;
		}
		
		//Clona todas las dimensiones del cubo
		$AllCubeDimensions = BITAMDimensionCollection::NewInstance($aRepository,null,$this->cla_concepto,true,true,false);
		foreach ($AllCubeDimensions as $aDimension)
		{
			$aNewDimension = clone($aDimension);
			$aNewCube->Dimensions->Collection[] = $aNewDimension;
			//Actualizamos la info del Join de esta dimensión si es que va al cubo, ya que en las tablas de versiones lo nombres de 
			//campo para las claves de dimensión siguen una nomenclatura distinta, parecida a las tablas de agregaciones
			if ($aNewDimension->dependsOnDimension == 0 && $aNewDimension->cla_periodo < 0)
			{
				foreach ($aNewCube->Joins as $aNewDescripKey)
				{
					if ($aNewDescripKey->nom_tabla == $aNewDimension->nom_tabla)
					{
						//En este caso, la dimensión está en el cubo así que hay que verificar además que el Join sea el del mismo nombre
						//de campo de la clave, ya que existen casos donde hay joins compuestos o varias dimensiones en la misma tabla así que
						//no podemos sobreescribir el join con la primer dimensión que apunte a esta tabla (si el Join de esta tabla NO es
						//ninguna de las claves de las dimensiones que se encuentren aquí, lo que terminará pasando es que el join se quedará
						//igual pero no se podrá resolver porque solo se versionan dimensiones e indicadores, no campos adicionales). Además
						//hay que identificar la tabla de tiempo, ya que esa recibe un trato especial pues el join se cambia para que use el
						//campo Fecha
						if ($aNewDescripKey->nom_fisicok == $aNewDimension->nom_fisicok_bd)
						{
							$aNewDescripKey->nom_fisicok_join = $aNewDimension->nom_fisicok_bd.$aNewDimension->consecutivo;
						}
					}
				}
			}
		}
		
		//Clona todos los atribudos del cubo
		$intCont = 1;
		foreach ($indicators as $anIndicator)
		{
			$aNewAttribute = BITAMAttribute::NewInstance($aRepository, -1);
			$aNewAttribute->consecutivo = $intCont;
			$aNewAttribute->orden = $intCont;
			$aNewAttribute->nom_logico = $anIndicator->nom_indicador;
			$aNewAttribute->nom_fisico = $anIndicator->nom_campo_bd;
			$aNewAttribute->tipo_dato = $anIndicator->DataType;
			switch ($aIndicator->DataType)
			{
				case 1:
				case 2:
					$aNewAttribute->long_dato = 30;	// No se supone que se soporten indicadores tipo String, pero por si acaso...
					break;
				default:
					$aNewAttribute->long_dato = 0;
					break;
			}
			$aNewAttribute->agrupador = $anIndicator->getAggregateFunction($aRepository, $anIndicator->cla_indicador);
			$aNewAttribute->valor_default = null;
			$aNewAttribute->setAggregateFunctionID(); 
			$aNewAttribute->formato = $anIndicator->formato;
			$aNewAttribute->cla_indicador = $anIndicator->cla_indicador;
			$aNewAttribute->dimensiones = $anIndicator->dimensiones;
			$aNewCube->Attributes->Collection[] = $aNewAttribute;
			$intCont++;
		}
		
		//Crea el nuevo Cubo
		$aNewCube->Save(true, false, $aCube->cla_concepto);
		
		return $aNewCube->cla_concepto;
		*/
		
		return $intVersionCubeID;
	}
	
	//@JAPR 2008-09-09: Agregada la generación del cubo de Versiones por Presupuesto
	//Crea un "clon" del cubo pero apuntando a una tabla de Versión creada del cubo origen, cambiando las referencias de las claves de dimensiones
	//de acuerdo a la nueva tabla y dándole un nombre representativo. Al final regresa la clave del nuevo cubo o -1 si hubo algún problema
	//Requiere como parámetros la Versión sobre la cual se creará el cubo (si se especifica -1, asume que es la tabla de versiones por presupuesto
	//así que no es un cubo de una versión en particular sino un cubo para contener múltiples versiones), la tabla de Hechos del nuevo cubo y
	//la lista de indicadores que serán creados como primer lote para este cubo
	//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
	//Agregado el parámetro $calcIndicatorIDs para especificar que indicadores calculados se agregarán a la versión
	public function mapVersionCube($aVersionID, $aTableName, $indicatorIDs, $calcIndicatorIDs = null)
	{
		$aRepository = $this->Repository;
		$aCube = BITAMCube::NewInstanceWithID($aRepository, $this->cla_concepto);
		$aCube->getAllDimensionsNotInFactTable();
		$indicators = BITAMIndicatorCollection::NewInstance($aRepository,$indicatorIDs);
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		//Clona la colección de Indicadores para concatenar la de Indicadores Calculados y hacer un solo procesamiento
		$allIndicators = clone($indicators);
		if (!is_null($calcIndicatorIDs) && count($calcIndicatorIDs) > 0)
		{
			//Por el momento solo se cargan los indicadores calculados que pertenecen al mismo cubo que se está versionando
			$calcIndicators = BITAMIndicatorCollection::NewInstance($aRepository,$calcIndicatorIDs, $this->cla_concepto);
			foreach ($calcIndicators as $anIndicator)
			{
				$allIndicators->Collection[] = $anIndicator;
			}
		}
		//@JAPR
		
		//Crea la estructura del cubo con sus elementos asociados en la metadata para que la tabla de versión esté disponible para consulta
		$aNewCube = clone($aCube);
		//Limpia los datos que identifican al cubo como uno ya existente para que se force a un Insert
		$aNewCube->cla_concepto = -1;
		if ($aVersionID == -1)
		{
			$aNewCube->nom_concepto .= ' (V)';
		}
		else
		{
			//@JAPR 2008-10-16: Cambio solicitado para Nemak, el nombre del cubo ahora concatenará la descripción de la versión, así que además
			//de eso se cambiará la palabra Version por V simplemente
			$strVersionName = '';
			$aVersion = BITAMVersion::NewInstanceWithID($aRepository,(int) $aVersionID);
			if (!is_null($aVersion))
			{
				$strVersionName = $aVersion->Description;
			}
			$aNewCube->nom_concepto .= ' (V '.$aVersionID.'- '.$strVersionName.')';
			$aNewCube->nom_concepto = substr($aNewCube->nom_concepto, 0, 100);
			//$aNewCube->nom_concepto .= ' ('.translate('Version').' '.$aVersionID.')';
			//@JAPR
		}
		$aNewCube->nom_tabla = $aTableName;
		$aNewCube->usa_agregacion = 0;
		$aNewCube->ag_new = 0;

		//Clona los Joins del cubo
		$bTimeJoinFound = false;
		$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository,$this->cla_concepto);
		foreach ($descriptKeysCollection as $aDescriptKey)
		{
			$aNewDescripKey = clone($aDescriptKey);
			$aNewDescripKey->cla_concepto = -1;
			//Si se trata del Join de la tabla de tiempo, hay que cambiarlo para que utilice el campo Fecha exclusivamente. Solo
			//se puede tener un Join a la tabla de tiempo por cubo
			if (!$bTimeJoinFound && $aNewDescripKey->nom_tabla == $aNewCube->dim_periodo)
			{
				$aNewDescripKey->nom_fisicok = $aNewCube->fecha;
				$aNewDescripKey->nom_fisicok_join = $aNewCube->fecha;
				$bTimeJoinFound = true;
			}
			$aNewCube->Joins->Collection[] = $aNewDescripKey;
		}
		
		$AllCubeDimensions = BITAMDimensionCollection::NewInstance($aRepository,null,$this->cla_concepto,true,true,false);
		if ($aVersionID == -1)
		{
			//Antes de clonar las dimensiones, hay que identificar si existen en el repositorio las dimensiones globales que apuntan a las tablas 
			//de Ektos ya que de no ser así se tienen que crear previamente (solo para el Cubo de Versiones, los cubos por Versión global no
			//tienen necesidad de esto pues es un cubo por cada versión/cubo origen, no un cubo compartido entre diferentes versiones)
			$arrEktosGlobalDimensions = array();
			$arrEktosGlobalDimensions['SI_USUARIO'] = -1;
			$arrEktosGlobalDimensions['SI_EKT_BudgetVersion'] = -1;
			//@JAPR 2008-09-17: Esta variable fue descontinuada, aunque se dejó por si se llegase a necesitar la funcionalidad nuevamente
			//$arrEktosGlobalDimensions['SI_EKT_GlobalVersions'] = -1;
			$arrEktosGlobalDimensions['SI_EKT_BudgetPeriod'] = -1;
			$arrEktosGlobalDimensions['SI_EKT_BudgetIndicator'] = -1;
			
			//No debiera ser que este cubo pudiera contener una dimensión cuya tabla sea una de los catálogos, pero si así fuera, se reutiliza
			$intNumMappedDimensions = 0;
			$intCont = 0;
			$aVersionDimensionID = 0;
			foreach ($AllCubeDimensions as $aDimension)
			{
				if (array_key_exists($aDimension->nom_tabla, $arrEktosGlobalDimensions))
				{
					//Sí encontró una dimensión que apunta a nuestra metadata, así que reutilizamos el ID global
					$arrEktosGlobalDimensions[$aDimension->nom_tabla] = $aDimension->cla_descrip;
					$intNumMappedDimensions++;
					if ($aDimension->nom_tabla == 'SI_EKT_BudgetVersion')
					{
						$aVersionDimensionID = $aDimension->consecutivo;
					}
				}
				
				//Obtiene el último Consecutivo para utilizarlo en las dimensione de los catálogos de metadata
				if ($intCont < $aDimension->consecutivo)
				{
					$intCont = $aDimension->consecutivo;
				}
			}
			$intCont++;
			
			//Si todavía quedan dimensiones que no están ya mapeadas en este cubo, las buscamos en el catálogo global para reutilizarlas
			//(esto puede pasar cuando crean una versión para un cubo que no se había versionado antes, pero teniendo versiones de un cubo 
			//diferente previamente)
			if ($intNumMappedDimensions < count($arrEktosGlobalDimensions))
			{
				$strInTables = '';
				foreach ($arrEktosGlobalDimensions as $strTableName => $aDescriptKey)
				{
					if ($aDescriptKey == -1)
					{
						if ($strInTables == '')
						{
							$strInTables = $aRepository->ADOConnection->Quote($strTableName);
						}
						else
						{
							$strInTables .= ', '.$aRepository->ADOConnection->Quote($strTableName);
						}
					}
				}
				
				$sql = 'SELECT CLA_DESCRIP, NOM_TABLA '.
					'FROM SI_DESCRIP_ENC '.
					'WHERE NOM_TABLA IN ('.$strInTables.')';
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die( translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$arrEktosGlobalDimensions[$aRS->fields["nom_tabla"]] = (int) $aRS->fields["cla_descrip"];
					$intNumMappedDimensions++;
					$aRS->MoveNext();
				}
			}
			
			//Agregamos las dimensiones que apuntan a la metadata a la colección para que se agreguen como parte del cubo
			//Necesitamos un ID de conexión y como las dimensiones pertenecen todas a la misma BD que la tabla de hechos hasta esta fecha
			//en cubos ROLAP, podemos asumir que tomando el ID de conexión de cualquiera de las dimensiones existentes será suficiente 
			//para usar en estas nuevas dimensiones (además, debería existir una dimensión en el cubo mínimo para presupuestarse, así
			//que es válido tomar la primera de ellas que siempre debería existir)
			$aConnectionID = $AllCubeDimensions[0]->cla_conexion;
			foreach ($arrEktosGlobalDimensions as $strTableName => $aDescriptKey)
			{
				$arrJoinNames = array();
				$intDependsOnDimension = 0;
				switch (strtolower($strTableName))
				{
					case "si_usuario":
						$strNomLogico = translate("User");
						$strDescName = "NOM_LARGO";
						$intLongDatoDesc = 60;
						$strKeyName = "CLA_USUARIO";
						$strNoApplyValue = '';
						$arrJoinNames[] = array('CLA_USUARIO', 'EKTUserID');
						break;
						
					case "si_ekt_budgetindicator":
						$strNomLogico = translate("Budget");
						$strDescName = "Description";
						$intLongDatoDesc = 255;
						$strKeyName = "BudgetID";
						$strNoApplyValue = '';
						$arrJoinNames[] = array('BudgetID', 'EKTBudgetID');
						break;
						
					case "si_ekt_budgetperiod":
						$strNomLogico = translate("Budget Period");
						$strDescName = "Description";
						$intLongDatoDesc = 255;
						$strKeyName = "BudgetPeriodID";
						$strNoApplyValue = '';
						$arrJoinNames[] = array('BudgetPeriodID', 'EKTBudgetPeriodID');
						break;
						
					case "si_ekt_budgetversion":
						$strNomLogico = translate("Version");
						$strDescName = "EKTDescription";
						$intLongDatoDesc = 255;
						$strKeyName = "EKTVersionID";
						$strNoApplyValue = '';
						$arrJoinNames[] = array('EKTVersionID', 'EKTVersionID');
						//@JAPR 2008-09-17: Modificada la PRIMARY KEY para que el ID de versión sea único por repositorio, de lo contrario dos versioens con el
						//mismo ID aunque fueran de diferente usuario/persupuesto/periodo hubieran acumulado juntas al navegar por Versión en el cubo de versiones
						//$arrJoinNames[] = array('EKTBudgetPeriodID', 'EKTBudgetPeriodID');
						//$arrJoinNames[] = array('EKTBudgetID', 'EKTBudgetID');
						//$arrJoinNames[] = array('EKTUserID', 'EKTUserID');
						break;
						
					//@JAPR 2008-09-17: Esta variable fue descontinuada, aunque se dejó por si se llegase a necesitar la funcionalidad nuevamente
					case "si_ekt_globalversions":
						$strNomLogico = translate("Global Version");
						$strDescName = "EKTDescription";
						$intLongDatoDesc = 255;
						$strKeyName = "EKTGlobVersionID";
						$strNoApplyValue = '-1';
						$arrJoinNames[] = array('EKTGlobVersionID', 'EKTGlobVersionID');
						$intDependsOnDimension = $aVersionDimensionID;
						break;
				}
				
				//Si aun quedan dimensiones que no están ya mapeadas, entonces hay que crearlas como dimensiones globales utilizando los valores
				//conocidos hasta la versión actual de Ektos para las llaves y descriptores
				if ($aDescriptKey == -1)
				{
					$initialKey = GetBitamInitialKey($this->Repository);
					
					$sql = 'SELECT '.
								$this->Repository->ADOConnection->IfNull('MAX(CLA_DESCRIP)', "".$initialKey."").' + 1 AS CLA_DESCRIP FROM SI_DESCRIP_ENC';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die( translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					$temporalID = (int)$aRS->fields["cla_descrip"];
					
					if($temporalID<($initialKey+1))
					{
						$temporalID = $initialKey+1;
					}

					$intClaDescrip = $temporalID;
					
					$arrEktosGlobalDimensions[$strTableName] = $intClaDescrip;
					
					$sql = 'INSERT INTO SI_DESCRIP_ENC (CLA_DESCRIP, CLA_CONEXION, NOM_LOGICO, NOM_FISICO, SQL_SELECT, 
							TIPO_DATO, LONG_DATO, NOM_FISICOK, TIPO_DATOK, LONG_DATOK, 
							AGRUPADOR, NOM_TABLA, ORDEN_DEL_SERVER, CLA_OWNER, TIPO_DIMENSION, DEFAULT_KEY, CREATED_BY)  
					    VALUES ('.$intClaDescrip.', '.$aConnectionID.', '.
							$this->Repository->ADOConnection->Quote($strNomLogico).', '.
							$this->Repository->ADOConnection->Quote($strDescName).', '.
							$this->Repository->ADOConnection->Quote('').', '.
							'2, '.$intLongDatoDesc.', '.
							$this->Repository->ADOConnection->Quote($strKeyName).', '.
							'7, 0, '.
							$this->Repository->ADOConnection->Quote('').', '.
							$this->Repository->ADOConnection->Quote($strTableName).', '.
							'0, '.$_SESSION['PAtheUserID'].', 3, '.
							$this->Repository->ADOConnection->Quote($strNoApplyValue).
							', 1)';
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die(translate("Error inserting ")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					//@JAPR 2008-09-17: Esta variable fue descontinuada, aunque se dejó por si se llegase a necesitar la funcionalidad nuevamente
					//Para la dimensión de versiones globales necesitamos declarar la dependencia con la dimensión de versiones por presupuesto
					/*
					if ($strTableName == 'SI_EKT_GlobalVersions')
					{
						$sql = 'INSERT INTO SI_DESCRIP_DET (CLA_DESCRIP, CLA_PADRE)  
						    VALUES ('.$intClaDescrip.', '.$arrEktosGlobalDimensions['SI_EKT_BudgetVersion'].')';
						$aRS = $this->Repository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die(translate("Error inserting ")." SI_DESCRIP_DET ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
					*/
				}
				else 
				{
					$intClaDescrip = $aDescriptKey;
				}
				
				//Agregamos la dimensión a esta colección para que sea grabada como parte del cubo
				$aDimension = null;
				$aDimension = BITAMDimension::NewInstance($aRepository, -1);
				$aDimension->consecutivo = $intCont++;
				$aDimension->cla_descrip = $intClaDescrip;
				$aDimension->nom_tabla = $strTableName;
				$aDimension->nom_logico = $strNomLogico;
				$aDimension->nom_fisicok = strtolower($strKeyName);
				$aDimension->nom_fisicok_bd = $strKeyName;
				$aDimension->tipo_datok = 7;
				$aDimension->long_datok = 0;
				$aDimension->nom_fisico = strtolower($strDescName);
				$aDimension->nom_fisico_bd = $strDescName;
				$aDimension->tipo_dato = 2;
				$aDimension->long_dato = $intLongDatoDesc;
				$aDimension->cla_periodo = -1;
				$aDimension->dependsOnDimension = $intDependsOnDimension;
				$AllCubeDimensions->Collection[] = $aDimension;
				if ($strTableName == 'SI_EKT_BudgetVersion')
				{
					$aVersionDimensionID = $aDimension->consecutivo;
				}
				
				$intContJoin = 1;
				foreach ($arrJoinNames as $aJoinData)
				{
					$aNewDescripKey = BITAM_SI_DESCRIPT_KEYS::NewInstance($aRepository);
					$aNewDescripKey->consecutivo = $intContJoin++;
					$aNewDescripKey->nom_fisicok = $aJoinData[0];
					$aNewDescripKey->nom_tabla = $strTableName;
				 	$aNewDescripKey->cla_concepto = -1;
				 	$aNewDescripKey->nom_fisicok_join = $aJoinData[1];
					$aNewCube->Joins->Collection[] = $aNewDescripKey;
				}
			}
		}
		
		//Clona todas las dimensiones del cubo
		foreach ($AllCubeDimensions as $aDimension)
		{
			$aNewDimension = clone($aDimension);
			$aNewCube->Dimensions->Collection[] = $aNewDimension;
			//Actualizamos la info del Join de esta dimensión si es que va al cubo, ya que en las tablas de versiones lo nombres de 
			//campo para las claves de dimensión siguen una nomenclatura distinta, parecida a las tablas de agregaciones
			if ($aNewDimension->dependsOnDimension == 0 && $aNewDimension->cla_periodo < 0)
			{
				//@JAPR 2008-10-21: Agregada validación para que a las dimensiones fijas no se les concatene el consecutivo en los Joins
				switch (strtolower($aNewDimension->nom_tabla))
				{
					case "si_usuario":
					case "si_ekt_budgetindicator":
					case "si_ekt_budgetperiod":
					case "si_ekt_budgetversion":
					case "si_ekt_globalversions":
						//En estos casos no se debe concatenar el consecutivo pues los campos tienen nombres fijos y no se deberían de haber
						//usado como dimensiones propias de otras dimensiones, si se llega a presentar el caso se analizará como resolverlo
						//pero por lo pronto para mantener un estandar entre las diferentes tablas se dejarán fijos sin el consecutivo
						break;
					
					default:
						foreach ($aNewCube->Joins as $aNewDescripKey)
						{
							if ($aNewDescripKey->nom_tabla == $aNewDimension->nom_tabla)
							{
								/*En este caso, la dimensión está en el cubo así que hay que verificar además que el Join sea el del mismo nombre
								de campo de la clave, ya que existen casos donde hay joins compuestos o varias dimensiones en la misma tabla así que
								no podemos sobreescribir el join con la primer dimensión que apunte a esta tabla (si el Join de esta tabla NO es
								ninguna de las claves de las dimensiones que se encuentren aquí, lo que terminará pasando es que el join se quedará
								igual pero no se podrá resolver porque solo se versionan dimensiones e indicadores, no campos adicionales). Además
								hay que identificar la tabla de tiempo, ya que esa recibe un trato especial pues el join se cambia para que use el
								campo Fecha
								*/
								if ($aNewDescripKey->nom_fisicok == $aNewDimension->nom_fisicok_bd)
								{
									$aNewDescripKey->nom_fisicok_join = $aNewDimension->nom_fisicok_bd.$aNewDimension->consecutivo;
								}
							}
						}
						break;
				}
			}
		}
		
		//Clona todos los atribudos del cubo que fueron especificados
		$intCont = 1;
		foreach ($allIndicators as $anIndicator)
		{
			$aNewAttribute = BITAMAttribute::NewInstance($aRepository, -1);
			$aNewAttribute->consecutivo = $intCont;
			$aNewAttribute->orden = $intCont;
			$aNewAttribute->nom_logico = $anIndicator->nom_indicador;
			$aNewAttribute->nom_fisico = $anIndicator->nom_campo;
			$aNewAttribute->nom_fisico_bd = $anIndicator->nom_campo_bd;
			$aNewAttribute->tipo_dato = $anIndicator->DataType;
			switch ($anIndicator->DataType)
			{
				case 1:
				case 2:
					$aNewAttribute->long_dato = 30;	// No se supone que se soporten indicadores tipo String, pero por si acaso...
					break;
				default:
					$aNewAttribute->long_dato = 0;
					break;
			}
			$aNewAttribute->agrupador = $anIndicator->getAggregateFunction($aRepository, $anIndicator->cla_indicador);
			$aNewAttribute->valor_default = null;
			$aNewAttribute->setAggregateFunctionID(); 
			$aNewAttribute->formato = $anIndicator->formato;
			$aNewAttribute->cla_indicador = $anIndicator->cla_indicador;
			$aNewAttribute->dimensiones = $anIndicator->dimensiones;
			$aNewCube->Attributes->Collection[] = $aNewAttribute;
			$intCont++;
		}
		
		//Crea el nuevo Cubo
		$aNewCube->Save(true, false, $aCube->cla_concepto);
		
		//Copia la seguridad de cada usuario con acceso al cubo pero filtrando por la dimensión Usuario
		$aNewCube->copySecurityFilterFromSourceCubeForVersions($aCube->cla_concepto);
		
		return $aNewCube->cla_concepto;
	}
	//@JAPR
	
	/*Agregados los filtros por usuario default al cubo de versiones, donde cada usuario se filtra por si mismo en la dimensión Usuario
	pero hereda el filtro actual del cubo origen del que se creó el cubo de versiones. Copia el filtro a todos los usuarios que en el
	momento de invocar al método tenían acceso al cubo origen
	@JAPR 2008-09-22: Agregado el parámetro $aUserList para indicar a que usuarios se les procesará su seguridad (si se deja NULL, se obtien
	todos los usuario que tuvieran seguridad en el cubo origen)
	*/
	public function copySecurityFilterFromSourceCubeForVersions($aSourceCubeID, $aUserList = null)
	{
		$aRepository = $this->Repository;
		
		//Identifica el ID de la dimensión Usuario en el nuevo cubo
		$intUserDimensionID = 0;
		$intMaxConsecutivo = 0;
		foreach ($this->Dimensions as $aDimension)
		{
			if ($intUserDimensionID == 0 && $aDimension->nom_tabla == 'SI_USUARIO' && $aDimension->nom_fisicok_bd == 'CLA_USUARIO')
			{
				$intUserDimensionID = $aDimension->consecutivo;
				//break;
			}
			if ($intMaxConsecutivo < $aDimension->consecutivo)
			{
				$intMaxConsecutivo = $aDimension->consecutivo;
			}
		}
		
		//Si no se encontró la dimensión Usuario, ya no hace el copiado de la seguridad
		if ($intUserDimensionID <= 0)
		{
			return;
		}
		
		//Arreglo de los usuarios con acceso al cubo original y una indicación de si tienen o no filtro personalizado
		$arrUsers = array();
		if (is_null($aUserList) || trim($aUserList) == '')
		{
			//Obtiene los usuarios con acceso al cubo origen en forma directa por al menos un indicador
			$sql = 'SELECT DISTINCT CLA_USUARIO '.
				'FROM SI_USR_IND '.
				'WHERE CLA_GPO_IND IN ('.
					'SELECT CLA_GPO_IND '.
					'FROM SI_GPO_IND_DET '.
					'WHERE CLA_INDICADOR IN ('.
						'SELECT CLA_INDICADOR '.
						'FROM SI_INDICADOR '.
						'WHERE CLA_CONCEPTO = '.$aSourceCubeID.'))';
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_USR_IND, SI_GPO_IND_DET, SI_INDICADOR ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$arrUsers[(int) $aRS->fields["cla_usuario"]] = 0;
				$aRS->MoveNext();
			}
			
			//Obtiene los usuarios con acceso al cubo origen mediante roles por al menos un indicador
			$sql = 'SELECT DISTINCT CLA_USUARIO '.
				'FROM SI_ROL_USUARIO '.
				'WHERE CLA_ROL IN ('.
					'SELECT CLA_ROL '.
					'FROM SI_ROL_IND '.
					'WHERE CLA_GPO_IND IN ('.
						'SELECT CLA_GPO_IND '.
						'FROM SI_GPO_IND_DET '.
						'WHERE CLA_INDICADOR IN ('.
							'SELECT CLA_INDICADOR '.
							'FROM SI_INDICADOR '.
							'WHERE CLA_CONCEPTO = '.$aSourceCubeID.')))';
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_ROL_USUARIO, SI_ROL_IND, SI_GPO_IND_DET, SI_INDICADOR ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$arrUsers[(int) $aRS->fields["cla_usuario"]] = 0;
				$aRS->MoveNext();
			}
		}
		else 
		{
			$arrUsers = explode(',', $aUserList);
			$arrUsers = array_flip($arrUsers);
		}
		
		//Genera la cadena con los niveles de navegación para Artus
		$strDefaultMinimumNav = '';
		for ($intIdx = 1; $intIdx <= $intMaxConsecutivo; $intIdx++)
		{
			$strDefaultMinimumNav .= $intIdx.',1|';
		}
		
		//Utilizando la lista de usuarios, se obtiene el filtro actual de cada uno en el cubo origen y se agrega el filtro por la 
		//dimensión Usuario utilizandose a si mismo como único elemento
		foreach ($arrUsers as $aUserID => $aFilterSource)
		{
			if ((int) $aUserID >= 0)
			{
				//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
				$userFilter = BITAMeFormsUser::getSecurityFilterByUserAndCubeID($this->Repository, $aUserID, $aSourceCubeID);
				if (count($userFilter) >= 1)
				{
					$arrUsers[$aUserID] = @$userFilter[2];
				}
				else
				{
					//No se supone que pueda regresar un arreglo vacio, pero por si acaso ponemos los defaults
					$userFilter[0] = "";
					$userFilter[1] = "";
					$userFilter[2] = 0;
					$userFilter[3] = $strDefaultMinimumNav;
					$userFilter[4] = 0;
				}
				
				//Carga la colección del filtro de dimensiones de este usuario. Como el cubo es un clon del original en este instante, todas
				//las dimensiones deberían estar en ambos y los consecutivos conservados en forma idéntica, así que usamos la clave del nuevo cubo
				//para que de una vez se prepare correctamente para el grabado
				$filteredDimByUser = BITAMFilteredDimensionCollection::NewInstanceByStrFilter($aRepository, $userFilter[0],$userFilter[1], $this->cla_concepto);
				
				$blnUserdimFiltered = false;
				$numFilterElements = count($filteredDimByUser->Collection);
				for ($aFilterIdx = 0; $aFilterIdx < $numFilterElements; $aFilterIdx++)
				{
					if ($intUserDimensionID == $filteredDimByUser->Collection[$aFilterIdx]->consecutivo)
					{
						//Sobreescribimos cualquier filtro que pudiera traer y dejamos solo al usuario correspondiente (aunque no se supone que
						//pueda traer un filtro previo, pues la dimensión no debería existir en el cubo origen, a menos que este sea el proceso
						//de creación de una nueva versión por usuario en cuyo caso la dimensión ya estaba previamente filtrada, y nada mas nos
						//aseguramos que cada usuario solo pueda verse a si mismo)
						$filteredDimByUser->Collection[$aFilterIdx]->values = array();
						$filteredDimByUser->Collection[$aFilterIdx]->values[] = $aUserID;
						$blnUserdimFiltered = true;
						break;
					}
				}
				
				//Si no se encontraba ya filtrado el usuario, agrega el nuevo filtro
				if (!$blnUserdimFiltered)
				{
					$intIndex = count($filteredDimByUser->Collection);
					$filteredDimByUser->Collection[$intIndex] = BITAMFilteredDimension::NewInstance($aRepository);
					$filteredDimByUser->Collection[$intIndex]->consecutivo = $intUserDimensionID;
					$filteredDimByUser->Collection[$intIndex]->nom_tabla = 'SI_USUARIO';
					$filteredDimByUser->Collection[$intIndex]->nom_fisicok = 'CLA_USUARIO';
					$filteredDimByUser->Collection[$intIndex]->values = array();
					$filteredDimByUser->Collection[$intIndex]->values[] = $aUserID;
				}
				
				//Genera la nueva cadena de filtro e inserta o actualiza el filtro para este usuario
				$filteredDimByUser->GenerateStrFilterByDimensionCollection();
				$strFilter = $filteredDimByUser->Filter;
				$strLevels = $filteredDimByUser->Levels;
				//Obtiene los valores actuales de MINIMO y JERARQUIA de sus filtros, o bien pone un default
				if ($arrUsers[$aUserID] == 0)
				{
					//No hay filtro, así que pone el default
					$strMinimumNav = $strDefaultMinimumNav;
					$intHierarchy = 0;
				}
				else 
				{
					//Tiene ya sea filtro directo o por usuario (este es preferente) así que hereda la configuración actual
					$strMinimumNav = (string) @$userFilter[3];
					$intHierarchy = (int) @$userFilter[4];
				}
				
				//Primero borra cualquier filtro de este usuario para el cubo de versiones, de forma que podamos insertar el nuevo filtro
				//(el filtro por Roles no se afecta)
				$sql = 'DELETE FROM SI_USR_CONFIG '.
					'WHERE CLA_USUARIO = '.$aUserID.' AND CLA_CONCEPTO = '.$this->cla_concepto;
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die( translate("Error accessing")." SI_USR_CONFIG ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$sql = 'INSERT INTO SI_USR_CONFIG (CLA_USUARIO, CLA_CONCEPTO, MINIMO, FILTRO, NIVELES, JERARQUIA) '.
					'VALUES ('.$aUserID.
						', '.$this->cla_concepto.
						', '.$aRepository->ADOConnection->Quote($strMinimumNav).
						', '.$aRepository->ADOConnection->Quote($strFilter).
						', '.$aRepository->ADOConnection->Quote($strLevels).
						', '.$intHierarchy.')';
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die( translate("Error accessing")." SI_USR_CONFIG ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Agrega el grupo de indicadores del cubo a la seguridad directa de este usuario o de lo contrario no podrá reeditar el filtro
				//desde Artus Administrator (solo si el cubo es diferente del fuente, de lo contrario obviamente ya está asignado)
				if ($this->cla_concepto != $aSourceCubeID)
				{
					$sql = 'INSERT INTO SI_USR_IND (CLA_USUARIO, CLA_GPO_IND) '.
						'VALUES ('.$aUserID.', '.$this->IndicatorGroupID.')';
					$aRS = $aRepository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						//die( translate("Error accessing")." SI_USR_IND ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
		}
	}
	
	/*Llena la tabla de versión con los datos existentes en el cubo para las dimensiones e indicadores especificados, aplicando
	el filtro de las dimensiones seleccionadas y agrupando por todas ellas para traer el detalle completo, solo en el rango
	de fechas del periodo presupuestal actual
	//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
	//Agregado el parámetro $calcIndicatorIDs para especificar que indicadores calculados se agregarán a la versión
	*/
	public function fillVersionTable($dimensionIDs, $indicatorIDs, $iVersionID, $sFilter, $aStartDate, $aEndDate, &$iNumRecords, $calcIndicatorIDs = null)
	{
		require_once("version.inc.php");
		
		$aRepository = $this->Repository;
		@addProcessLogRow(translate('Filling cube version table'), $iVersionID, 'infoGenIndCal');
		$aCube = BITAMCube::NewInstanceWithID($aRepository, $this->cla_concepto);
		$aCube->getAllDimensionsNotInFactTable();
		$aTableName = "EKTV_".$this->cla_concepto."_".$iVersionID;
		$aPeriodID = 4;
		$aPeriodDimensionCollection = BITAMDimensionCollection::NewInstancePeriodDims($aRepository, $this->cla_concepto, false);
		if (!is_null($aPeriodDimensionCollection))
		{
			//Obtenemos un periodo existente en este cubo, de preferencia Mensual si está asociado, si no, el último encontrado
			foreach ($aPeriodDimensionCollection as $aPeriodDimension)
			{
				$aPeriodID = $aPeriodDimension->cla_periodo;
				if ($aPeriodID == 4)
				{
					break;
				}
			}
		}
		
		$arrQuery = array('FIELDS' => '', 'SELECT' => '', 'FROM' => '', 'WHERE' => '', 'GROUP' => '', 'HAVING' => '', 'ORDER' => '');
		$arrQuery['FIELDS'] = $this->fecha;
		//$thePeriods = BITAMCInfoPeriodCollection::NewInstance($aRepository);
		//@JAPR 2010-07-19: Agregado el parámetro $bCreateQuery para indicar que la instancia de rolap sea generada automáticamente
		$theCube = BITAMCInfoCube::NewInstance($aRepository, $this->cla_concepto, false, true);
		//Bloqueamos el uso de agregaciones para forzar a que el query se resuelva directamente en el cubo
		$theCube->use_agregation = false;
		
		// Estamos listos para usar la funcionalidad de ROLAP
		$theCubeRolap = $theCube->rolap;
		
		// Initialize
		$theCubeRolap->Initialize();
		
		// Filter
		$theCubeRolap->Filter($sFilter);
		
		$arrProcessedDimensions = array();
		// InsertDimension para las dimensiones solicitadas
		$numDimensions = count($dimensionIDs);
		$numDimensionsInTable = 0;
		for ($i = 0; $i < $numDimensions; $i++)
		{
			$aDimensionID = $dimensionIDs[$i];
			if (!array_key_exists($aDimensionID, $this->DimensionsNotInFactTable))
			{
				$arrQuery['FIELDS'] .= ', '.$theCube->a_dimensions[$aDimensionID]->physical_name_key.$aDimensionID;
				$theCubeRolap->InsertDimension('['.$theCube->a_dimensions[$aDimensionID]->physical_table.'].['.	$theCube->a_dimensions[$aDimensionID]->physical_name_descrip.']');
				if (!array_key_exists($aDimensionID, $arrProcessedDimensions))
				{
					$arrProcessedDimensions[$aDimensionID] = 1;
				}
				$numDimensionsInTable++;
			}
		}
		
		//Inserta los indicadores y genera el Having para que por lo menos uno de ellos contenga un valor <> de NULL
		$strHaving = '';
		$indicators = BITAMIndicatorCollection::NewInstance($aRepository,$indicatorIDs);
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		//Clona la colección de Indicadores para concatenar la de Indicadores Calculados y hacer un solo procesamiento
		$allIndicators = clone($indicators);
		if (!is_null($calcIndicatorIDs) && count($calcIndicatorIDs) > 0)
		{
			//Por el momento solo se cargan los indicadores calculados que pertenecen al mismo cubo que se está versionando
			$calcIndicators = BITAMIndicatorCollection::NewInstance($aRepository,$calcIndicatorIDs, $this->cla_concepto);
			foreach ($calcIndicators as $anIndicator)
			{
				$allIndicators->Collection[] = $anIndicator;
			}
		}
		//@JAPR
		
		$arrIndicatorFields = array();
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		foreach ($allIndicators as $anIndicator)
		{
			$arrIndicatorFields[] = $anIndicator->nom_campo_bd;
			$theCubeRolap->InsertIndicator($theCube->a_indicators[$anIndicator->cla_indicador]->formule_db,false, 0, -1, -1);
			$strHaving .= 'OR '.$anIndicator->formula_bd.' IS NOT NULL ';
		}
		$strHaving = trim(strpbrk($strHaving, ' '));
		
		$theCubeRolap->sHaving = $strHaving;

		$aux_brange_date=$theCubeRolap->brange_date;
		$theCubeRolap->brange_date=true;
		$theCubeRolap->InsertPeriod($aPeriodID, $aStartDate, $aEndDate, true, false, '', '', false, '' );
		
		// RowSet
		//print '<BR>QueryText()<BR>==================================<BR>'.$theCubeRolap->QueryText().'<BR>';
		$sql = $theCubeRolap->QueryText();
		$theCubeRolap->brange_date=$aux_brange_date;
		
		$arrQueryText = explode('<BR>', $sql);
		// SELECT, GROUP BY y ORDER BY (Tantos campos como dimensiones se pidieron agregar * 2 para excluir los padres 
		//filtrados si es que hay, + la Fecha, para el caso del SELECT se agregan además los indicadores)
		/*Se considera que los queries ROLAP siempre usan como Alias para la tabla de tiempo el identificador "t2", así que
		utilizamos el nombre del campo Fecha como se especificó en el cubo (el cual se asume que siempre existe en la tabla
		de tiempo) directamente para que se seleccione como parte del Query en la primer posición, mismo que además será
		agregado en el GROUP BY y el ORDER BY de forma que la tabla de versión se llene por la fecha exacta del registro
		*/
		$strDateField = 't2.'.$this->fecha;
		$arrQuery['SELECT'] = $strDateField;
		$arrQuery['GROUP'] = $strDateField;
		$arrQuery['ORDER'] = $strDateField;
		$elements = explode(',', strpbrk(ltrim($arrQueryText[0]), ' '));
		for ($i=0; $i < $numDimensionsInTable * 2; $i++)
		{
			if (($i % 2) == 0)
			{
				$arrQuery['SELECT'] .= ', '.$elements[$i];
				$arrQuery['GROUP'] .= ', '.$elements[$i];
				$arrQuery['ORDER'] .= ', '.$elements[$i];
			}
		}
		/*El siguiente elemento después de las dimensiones corresponde con el campo periodo, pero como ya lo agregamos se puede
		ignorar, así que solo procesamos el resto de los indicadores pero no el total de los campos pues al final vienen las
		dimensiones que forman parte del filtro que no fueron pedidas en el SELECT, las cuales pudieran no estar en la tabla
		de hechos, así que no las incluimos
		*/
		//@JAPR 2008-10-16: Agregado el versionamiento de Indicadores Calculados
		$numIndicators = count($indicatorIDs) + count($calcIndicatorIDs);
		for ($j=0; $j < $numIndicators; $j++)
		{
			$i++;
			$arrQuery['SELECT'] .= ', '.$elements[$i];
		}
		
		// FROM (Todas las tablas, incluso las de dimensiones filtradas, ya que no afectan al SELECT)
		$arrQuery['FROM'] .= strpbrk(ltrim($arrQueryText[1]), ' ');
		
		// WHERE (Todos los filtros, incluso los joins de las dimensiones filtradas, ya que no afectan al SELECT)
		if (substr(strtoupper(trim($arrQueryText[2])),0,6) == 'WHERE ')
		{
			$arrQuery['WHERE'] .= strpbrk(ltrim($arrQueryText[2]), ' ');
		}
		
		// HAVING (Todas las condiciones)
		if (substr(strtoupper(trim($arrQueryText[4])),0,7) == 'HAVING ')
		{
			$arrQuery['HAVING'] = strpbrk(ltrim($arrQueryText[4]), ' ');
		}
		
		//Genera el estatuto INSERT SELECT para llenar la tabla de versión
		$arrQuery['FIELDS'] .= ', '.implode(', ', $arrIndicatorFields);
		$sqlSELECT = 'SELECT '.$arrQuery['SELECT'].' '.
			'FROM '.$arrQuery['FROM'].' '.
			'WHERE '.$arrQuery['WHERE'].' '.
			'GROUP BY '.$arrQuery['GROUP'].' ';
		if (trim($arrQuery['HAVING']) != '')
		{
			$sqlSELECT .= 'HAVING '.$arrQuery['HAVING'].' ';
		}
		//@JAPR 2008-10-02: Se removió el ORDER BY porque en los remotos casos donde dos o mas dimensiones tuvieran el mismo campo como clave,
		//no se puede ordenar mas de una vez por él así que estaba fallando
		//$sqlSELECT .= 'ORDER BY '.$arrQuery['ORDER'];
		
		$sql = 'INSERT INTO '.$aTableName.' ('.$arrQuery['FIELDS'].') '.$sqlSELECT;
		@updateVersionLogFile($iVersionID, $sql, true);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//Si no se pudo llenar la tabla, no debería continuar con el proceso, pero también es posible que se marque solo 
			//como no procesada
			//die( translate("Error inserting")." ".$aTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			@addProcessLogRow(translate('Error while filling cube version table').': '.$aRepository->DataADOConnection->ErrorMsg(), $iVersionID, 'infoGenIndCal');
		}
		else
		{
			//Verifica cuantos registros tiene esta tabla de versión para grabarlos en la metadata
			$iNumRecords = 0;
			$sql = 'SELECT COUNT(*) AS NumRecords FROM '.$aTableName;
			@updateVersionLogFile($iVersionID, $sql, true);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false)
			{
				if (!$aRS->EOF)
				{
					$iNumRecords = (int) $aRS->fields["numrecords"];
				}
			}
			
			@addProcessLogRow(translate('Cube version table successfully filled').': '.$iNumRecords.' '.translate('records'), $iVersionID, 'infoGenIndCal');
		}
	}
	
	static function useAggregations($aRepository, $cubeID)
	{
		$useAggregations=null;
		
		$useAggregations =& BITAMGlobalInstance::GetUseAggregationByCubeID($cubeID);
		
		if(is_null($useAggregations))
		{
			$sql="SELECT USA_AGREGACION FROM SI_CONCEPTO WHERE CLA_CONCEPTO =".$cubeID;
	
			$aRS = $aRepository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if (!$aRS->EOF)
			{
				if(((int)$aRS->fields["usa_agregacion"])==1)
				{
					$useAggregations = true;
				}
				else
				{
					$useAggregations = false;
				}
			}
			
			BITAMGlobalInstance::AddUseAggregationByCubeID($cubeID, $useAggregations);
		}
	
		return $useAggregations;
	}
}

class BITAMCubeCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	
	static function NewInstance($aRepository, $anArrayOfCubeIDs = null, $aCubeType = null)
	{
		global $checkDisabledCubes;
		
		$strAnd = 'WHERE ';
		
		$anInstance = new BITAMCubeCollection($aRepository);
		
		$where = "";
		
		if (!is_null($anArrayOfCubeIDs))
		{
			switch (count($anArrayOfCubeIDs))
			{
				case 0:
					break;
				
				case 1: 
					$where = " WHERE CLA_CONCEPTO = ".((int) $anArrayOfCubeIDs[0]);
					$strAnd = 'AND ';
					break;	
					
				default:
				
					foreach ($anArrayOfCubeIDs as $aCubeID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aCubeID; 
					}
					if ($where != "")
					{
						$where = "WHERE CLA_CONCEPTO IN (".$where.")";
						$strAnd = 'AND ';
					}
					break;
			}
		}

		if (!is_null($aCubeType))
		{
			$where .= $strAnd.'TIPO_CUBO = '.$aCubeType;
			$strAnd = 'AND ';
		}
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$where .= $strAnd.' CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		//@JAPR
		
		$sql = "SELECT CLA_CONCEPTO, NOM_CONCEPTO, NOM_TABLA, TIPO_CUBO, SERVIDOR, SERVICE_NAME, DATABASE_NAME, TIPO_CONN, USUARIO, PASSWORD, DIM_PERIODO, FECHA, FMT_FECHA, USA_AGREGACION, CLA_BD, CATALOGO_OLAP, SHOW_ALIAS, CLA_OWNER ".(($_SESSION["PAArtusMDVersion"] >= 6.0)?',AG_NEW ':'')."FROM SI_CONCEPTO ".$where." ORDER BY 2";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("Error accessing SI_INDICADOR table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			die( translate("Error accessing")." SI_INDICADOR ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMCube::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewInstanceWithTable($aRepository, $aTableName = null, $aCubeType = null)
	{
		global $checkDisabledCubes;
		
		$strAnd = 'WHERE ';
		
		$anInstance = new BITAMCubeCollection($aRepository);
		
		$where = "";
		
		if (!is_null($aTableName))
		{
			$where = " WHERE NOM_TABLA = ".$aRepository->ADOConnection->Quote($aTableName);
			$strAnd = 'AND ';
			break;	
		}

		if (!is_null($aCubeType))
		{
			$where .= $strAnd.'TIPO_CUBO = '.$aCubeType;
			$strAnd = 'AND ';
		}
		//@JAPR 2011-05-02: Validados los cubos deshabilitados mediente la página de SAAS - FBM (#23934)
		if ($checkDisabledCubes)
		{
			$where .= $strAnd.' CLA_CONCEPTO NOT IN (SELECT CLA_CONCEPTO FROM SI_CONCEPTO_DESHAB)';
		}
		//@JAPR

		$sql = "SELECT CLA_CONCEPTO, NOM_CONCEPTO, NOM_TABLA, FECHA, TIPO_CUBO, DIM_PERIODO, SERVIDOR, SERVICE_NAME, DATABASE_NAME, TIPO_CONN, USUARIO, PASSWORD, FMT_FECHA, USA_AGREGACION, CLA_BD, CATALOGO_OLAP, SHOW_ALIAS, CLA_OWNER ".(($_SESSION["PAArtusMDVersion"] >= 6.0)?',AG_NEW ':'')." FROM SI_CONCEPTO ".$where." ORDER BY 2";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("Error accessing SI_INDICADOR table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			die( translate("Error accessing")." SI_CONCEPTO ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMCube::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		return BITAMCubeCollection::NewInstance($aRepository,null,3);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		$title = translate("Cube Definitions");
		
		return $title;
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=CubeCollection";
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=Cube";
	}

	function get_FormIDFieldName()
	{
		return 'cla_concepto';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_concepto";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_tabla";
		$aField->Title = translate("Table");
		$aField->Type = "String";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "servidor";
		$aField->Title = translate("DSN");
		$aField->Type = "String";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

}

?>