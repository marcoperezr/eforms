<?php

function getComponents($aConnection, $nStage, $nType = 140)
{
	$aSQL = "SELECT  A.TIPO_OBJETO, A.CONSECUTIVO, A.ARCHIVO, ".
	                "       A.NOMBRE, A.TIMING, ".
	                    "   B.REFRESCAR, B.VERSION, A.ORDEN, A.ORDEN_FILL ".
	                "FROM   SI_OBJETO A, SI_ESCENARIO B ".
	                "WHERE A.CLA_ESCENARIO = B.CLA_ESCENARIO AND ".
	                    "   B.CLA_ESCENARIO = ".$nStage ." AND TIPO_OBJETO = {$nType} ".
	                " ORDER BY A.ORDEN_FILL, A.ORDEN ";
		
	$recordSet = $aConnection->Execute($aSQL);
	$ComComponents = array();
	while (!$recordSet->EOF) 
	{
		$ptype = $recordSet->fields['0'];
		$pconsecutive = $recordSet->fields['1'];
		$sMD = $recordSet->fields['2'];
		$pName = $recordSet->fields['3'];
		
		$recordSet->MoveNext();
		
		$cComp = new BITAMComponent();
		$cComp->Restore($ptype,$pName,$pconsecutive,$sMD);
		$ComComponents[$cComp->type.'_'.$cComp->consecutive]=$cComp;
		unset($cComp);
	}
	
	$recordSet->close();
	
	return $ComComponents;
}

function getRestoreElem($aMD, $Top, $i, $default){
	if ($i<=$Top ){
		if (isset($aMD[$i])){
			return $aMD[$i];
		}
		else{
			return $default;
		}
	}
	else{
		return $default;
	}
}

function CreateNewComponent($type){
	switch ($type) {
		case 140:
			return new BITAMDimensionFilter();
			break;
		/*case FT_OPERANDO:
			return new BITAMOperandFilter();
			break;
		case FT_OPERANDO_DIVISOR:
			return new BITAMDivisorFilter();
			break;*/
		case 120:
			return new BITAMTimeFilter();
			break;
		default:
			return null;
	}
}

class BITAMComponent
{
	public $name  = '';
	public $consecutive=0;
	public $type  = 0;
	public $Comp  = null;
	
	function __destruct() {
		unset($this->Comp);
	}
	function Restore($ptype,$pname,$pconsecutive,$sMD){
		$this->name =$pname;
		$this->type =$ptype;
		$this->consecutive = $pconsecutive;
		
		$this->Comp = CreateNewComponent($this->type);
		//$this->Comp = new BITAMDimensionFilter();
		if (!is_null($this->Comp)){
			$this->Comp->Restore($sMD);
		}
	}
	
	
}

class BITAMDimensionFilter{
	public $Cube=0;
	public $Dimension=0;
	public $sCubo = '';
    public $sClaDimension = '';
    public $sNomDimension = '';
	
	function __construct() {
		
	}
	function __destruct() {
		
	}
	function Restore($CellInfo){
		
		$VersionStr = '';
    	$lngStrLen = '';
     
    	if(Mid($CellInfo, 1, strlen("VERSION=")) == "VERSION=")
    	{
    		$VersionStr = GetSiguiente($CellInfo);
    		$Version = Mid($VersionStr, strlen("VERSION=")+1);
    	}
    	else 
    	{
    		$Version = 0;
    	}
    
    	if($Version >= 3)
    	{
    		$lngStrLen = GetSiguiente($CellInfo);
    		$ComponentName = Mid($CellInfo, 1, $lngStrLen); //Devuelve una cadena que contiene un número especificado de caracteres desde el lado izquierdo de una cadena
    		$CellInfo = Mid($CellInfo, $lngStrLen + 2); // Remover también la coma
    	}
    	
		if ($Version >= 2.5)
    	{
			$bCapturable = (GetSiguiente($CellInfo)) == 1;
        	$bEditable = (GetSiguiente($CellInfo)) == 1;
        	$iNFilterChars = (GetSiguiente($CellInfo));
        
        	
        	$sDummy = '';
        	if (strpos($CellInfo, "@cboProperties@") !== false)
        	{
            	$sDummy = GetSiguiente($CellInfo, "@cboProperties@");
               	GetSiguiente($CellInfo);
            }
    	}
          	
    	//RestoreDimProperties
    	for($i=0; $i<14; $i++)
    		GetSiguiente($CellInfo);
    	
    	//@JAPR 2012-07-04: Corregido un bug, no estaban inicializadas estas variables así que generaban warnings
		$lDimInfoLen = 0;
    	//RestoreInfDimension
    	if ($Version == 0)
    	{/*
			$lDimInfoLen = strpoStrings.GetNthPos(CellInfo, "|", 2)
        If (lDimInfoLen = -1) Then
            ' No apareció el segundo Pipe
            ' En la cadena aparecen 4 items separados por coma
            lDimInfoLen = Strings.GetNthPos(CellInfo, ",", 4) - 1
        Else
            ' Es una versión en la que aparecen los Pipes
            ' Buscar la primera coma después del 2do pipe
            lDimInfoLen = Strings.GetNthPos(CellInfo, ",", 1, lDimInfoLen) - 1
        End If*/
    	}
    	else 
    	{
    		//version >= 1.0
    		$lDimInfoLen = GetSiguiente($CellInfo);
    	}
    
    
	    //Tomar la cadena con info de las dimensiones
	    $sDimInfo = Mid($CellInfo,1, $lDimInfoLen);
	    //Dejar el resto de la cadena en CellInfo
	    $CellInfo = Mid($CellInfo, $lDimInfoLen + 2); // Remover la coma
	    
	    //Ver si alguien desea proveer otra cadena de info de las dimensiones
	    // It's necesary to determine the similar src filter when going back to the default/previous scenario
	    $sArgumentFilter = $sDimInfo;
	    //Procesar sDimInfo
	    $sCuboP = GetSiguiente($sDimInfo);
	    $sClaDimensionP = GetSiguiente($sDimInfo);
	    $sNomDimensionP = GetSiguiente($sDimInfo);
    	//@JAPR 2012-07-04: Corregido un bug, no estaban inicializadas estas variables así que generaban warnings
	    $nCubo = 0;
	    $nClaDimension = 0;
	    //@JAPR
	    if (strpos($sDimInfo, "|") !== false)
	    {
	    	$sKeyValue = GetSiguiente($sDimInfo, "|");
	    	$sKeyValue = str_replace("#artuspipe#", "|", $sKeyValue);
	    	
	    	if( strpos($sDimInfo, ',') !== false )
	    	{
	    		$Paso = GetSiguiente($sDimInfo);
	    	}
	    	else 
	    	{
	    		$Paso = $sDimInfo;
	            $sDimInfo = "";
	    	}
	    	$nCubo = GetSiguiente($Paso, "|");
	    	$nClaDimension = $Paso;
	    }
	    			
		$this->Cube = $nCubo;
		$this->Dimension = $nClaDimension;
		$this->sCubo = $sCuboP;
		$this->sClaDimension = $sClaDimensionP;
		$this->sNomDimension = $sNomDimensionP;

	}
}

class BITAMTimeFilter{
	public $PeriodKey=4;
	public $bRange = false;
	public $Components = '';
	public $Font = null;
	public $BorderType = 0;
	Public $Date = 'Actual, 0';
	Public $DateR = 'Actual, 0';
	Public $InitialDate ='';
	Public $FinalDate = '';
	Public $Format = '';
	
	function __construct() {
		
	}
	
	function __destruct() {
		
	}

	function Save(){
		
	}

	function Restore($CellInfo){
		
		return ;
		
		$VersionStr = '';
    	$lngStrLen = '';
     
    	if(Mid($CellInfo, 1, strlen("VERSION=")) == "VERSION=")
    	{
    		$VersionStr = GetSiguiente($CellInfo);
    		$Version = Mid($VersionStr, strlen("VERSION=")+1);
    	}
    	else 
    	{
    		$Version = 0;
    	}
    
    	if ($Version >= 3)
    	{
    		$lngStrLen = (GetSiguiente($CellInfo));
	        $ComponentName = Mid($CellInfo, 1, lngStrLen);
	        $CellInfo = Mid($CellInfo, $lngStrLen + 2);
    	}
    	
    	$x = strpos($CellInfo, '|||');
	    
	    if($x > 0)
	    {
	    	$CellInfo1 = Mid($CellInfo, 1, $x - 1);
	        $CellInfo2 = Mid($CellInfo, $x + 3);
	    }
	    else 
	    {
	    	$CellInfo1 = $CellInfo;
	        $CellInfo2 = "";
	    }
	    
	    // 1ro restauramos las propiedades
	    //Call RestoreSpinProperties(CellInfo1)
	    for($i=0; $i<16; $i++)
    		GetSiguiente($CellInfo1);
    	
    		
    	//Periodo
	    
	    $Tag = GetSiguiente($CellInfo1);
	    $Paso = $Tag;
	    GetSiguiente($Paso, "|");
	    GetSiguiente($Paso, "|");
	    GetSiguiente($Paso, "|");
	    $FechaReal = GetSiguiente($Paso, "|");
				
		$x = strpos($CellInfo2, "|||");
	    If ($x !== false)
	    {
	    	$strRemainingInfo = Mid($CellInfo2, $x + 3);
	        $CellInfo2 = Mid($CellInfo2, 1, $x-1);
	    }
	    if (trim($CellInfo2) != "")
	    {
	    	$FechaRealR = $Tag;
	    }
	    
		$Paso = GetSiguiente($strRemainingInfo, "|||");
		var_dump($strRemainingInfo);exit();
		$SyncINPC = (GetSiguiente($Paso)) == 1;
	    $bShowYear = (GetSiguiente($Paso)) == 1;
	    
	    if (GetSiguiente($strRemainingInfo, "|") == 1 )
	    {
	    	//Call BlockComp.AddBC(Me.SpinTexto)
	    }
	    
	    $sPeriodos = GetSiguiente($strRemainingInfo, "|");
	    var_dump($sPeriodos);
	    exit();
	}
}

function Mid($str, $start, $length = null)
{
	$start = $start -1;
	if($length == null)
		return substr($str, $start);
	else
		return substr($str, $start, $length);
}

function GetSiguiente(&$sCadena, $Token = ",", $GetNToken = 1)
{
    for ($i = 1; $i<=$GetNToken; $i++)
    {
    	$npos = strpos($sCadena, $Token);
    	if ($npos >= 0)
    	{
    		$GetSiguiente = Mid($sCadena, 1, $npos);
    		$sCadena = Mid($sCadena, $npos + strlen($Token) + 1);
    	}
    	else 
    	{
    		if (strlen(trim($sCadena))>0)
    		{
    			$GetSiguiente = $sCadena;
    			$sCadena = '';
    		}
    		else 
    		{
    			$GetSiguiente = '';	
    		}
    	}
    			
    }
 
    return $GetSiguiente;
    
}

?>