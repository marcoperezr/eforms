<?php


class AdvisorHTTPRequest
{
	var $_fp; // HTTP socket
	var $_url; // full URL
	var $_host; // HTTP host
	var $_protocol; // protocol (HTTP/HTTPS)
	var $_path;
	var $_query;
	var $_port; // port
	var $_timeout = 30; // timeout used
	var $_authUsername = null;
	var $_authPassword = null;
	var $_requestHeaders = array();
	var $_usePOST = false;

	var $headers = array();
	var $body = "";
	var $statusCode = null;
	var $code = null;
	
	var $uploadFilename = "";

	// scan url
	function _scan_url($url)
	{
		$url_info = parse_url($url);

		$this->_url = $url;

		$this->_protocol = isset($url_info['scheme']) ? $url_info['scheme'] : "";
		$this->_host = isset($url_info['host']) ? $url_info['host'] : "";
		$this->_port = isset($url_info['port']) ? $url_info['port'] : 80;
		$this->_path = isset($url_info['path']) ? $url_info['path'] : "";
		$this->_query = isset($url_info['query']) ? $url_info['query'] : "";
	}

	// constructor
	function AdvisorHTTPRequest($url)
	{
		$this->_scan_url($url);
	}

	// download URL to string
	function execute()
	{
		$crlf = "\r\n";
		$response = "";

		$this->statusCode = null;
		$this->code = null;

		$errno = 0;
		$errstr = "";

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->_timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->_timeout);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_NOBODY, 0);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		
/*		$ssl_verifypeer = AdvisorAgentConfiguration::getSSL_verifypeer();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $ssl_verifypeer);
	*/	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// include authorization if required
		if ($this->_authUsername != null )
		{
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			$auth = $this->_authUsername . ":" . $this->_authPassword;
			curl_setopt($ch, CURLOPT_USERPWD, $auth);
		}

		// include request headers
		$heads = array();
		foreach ($this->_requestHeaders as $eachHeader => $eachHeaderValue)
			$heads[] = $eachHeader . ": " . $eachHeaderValue;

		// if there is a file to upload, create the contents to upload it
		$tempUploadFile = null;
		if ($this->uploadFilename != "")
		{
			// create the file
			$tempUploadFile = AdvisorUtils::createUniqueTemporaryFilename();

			// add the multipart format
			$heads[] = "Content-Type: multipart/form-data; boundary=----123412341234";

			// create the temp file
			$writeFile = fopen($tempUploadFile, "wb");
			
			// assign the separator
			fwrite($writeFile, "------123412341234");

			// the name should be change, to make BITAMArtusVigilantServer->#fileReceptionWith: work properly
			$properFilename = str_replace('/', '\\', $this->uploadFilename);
			$noExt = AdvisorUtils::fileNameLessExtension($properFilename);

			fwrite($writeFile, "\r\nContent-Disposition: form-data; name='{$noExt}'; filename='{$properFilename}'");
			fwrite($writeFile, "\r\nContent-Type: application/octect-stream");

			// upload file content length
			fwrite($writeFile, "\r\nContent-Length: " . filesize($this->uploadFilename));
			fwrite($writeFile, "\r\n");
			fwrite($writeFile, "\r\n");

			// write the real upload file
			$readFile = fopen($this->uploadFilename, "rb");
			$cs = 8192 * 1024;
			while (!feof($readFile))
			{
				fwrite($writeFile, fread($readFile, $cs));
			}
			fclose($readFile);

			// end of file			
			fwrite($writeFile, "------123412341234--");

			// close the file
			fclose($writeFile);

			// add the real content length
			$heads[] = "Content-Length: " . filesize($tempUploadFile);
		}

		if ($this->_usePOST)
		{
			curl_setopt($ch, CURLOPT_URL, AdvisorUtils::urlBasePath($this->_url));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $this->_query);
		}
		else
		{
			curl_setopt($ch, CURLOPT_URL, $this->_url);
		}

		// set all headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, $heads);

		// write the file using the curl API
		$fileHandle = null;
		if (!is_null($tempUploadFile))
		{
			$aSize = filesize($tempUploadFile);
			$fileHandle = fopen($tempUploadFile, "rb");

			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_INFILE, $fileHandle);
			curl_setopt($ch, CURLOPT_INFILESIZE, $aSize);
		}

		// perform all action
		$response = curl_exec($ch);
		
		// close temp handle
		if (!is_null($fileHandle))
			fclose($fileHandle);

		$errno = curl_errno($ch);
		$errstr = curl_error($ch);

		if ($errno != 0)
		{
			curl_close($ch);
			throw new Exception($errstr, $errno);
		}

		$infoArray = curl_getinfo($ch);
		curl_close($ch);

		// remove temp file, after curl closed
		if (!is_null($tempUploadFile))
			@unlink($tempUploadFile);

//print "<br>"; print_r($infoArray);
		$this->code = $infoArray['http_code'];
		$headerSize = $infoArray['header_size'];

		// split header and body
		//$pos = strpos($response, $crlf . $crlf);
		$pos = $headerSize;
		
		if($pos === false)
			return($response);
//print $response;
		$header = substr($response, 0, $pos);
		$this->body = substr($response, $pos);

		// parse headers
		$this->headers = array();
		$lines = explode($crlf, $header);

		foreach ($lines as $line)
		{
		 	//print $line;

			if (($pos = strpos($line, ':')) !== false)
			{
				$headerName = trim(substr($line, 0, $pos));
				$headerValue = trim(substr($line, $pos+1));

				if ($headerName == 'Set-Cookie')
				{
					if (!isset($this->headers[$headerName]))
						$this->headers[$headerName] = array();

					$this->headers[$headerName][] = $headerValue;
				}
				else
					$this->headers[$headerName] = $headerValue;
			}
			else
				if ($this->statusCode == null)
					$this->statusCode = $line;
		}

		//@JAPR 2015-02-06: Agregado soporte para php 5.6
		$array = explode(" ", $this->statusCode);
		//$this->code = @$array[1];
		$aLocation = @$this->headers['Location'];

		// redirection?
		if (!is_null($aLocation))
		{
			// on redirect, turn off posting
			$this->_usePOST = false;

			$newURL = $aLocation;
			$url_info = parse_url($newURL);

			$aProtocol = isset($url_info['scheme']) ? $url_info['scheme'] : $this->_protocol;
			$aHost = isset($url_info['host']) ? $url_info['host'] : $this->_host;
			$aPort = isset($url_info['port']) ? $url_info['port'] : $this->_port;
			$aPath = isset($url_info['path']) ? $url_info['path'] : $this->_path;
			$aQuery = isset($url_info['query']) ? $url_info['query'] : $this->_query;
			
			$newComputedURL = $aProtocol . "://" . $aHost . ($aPort != '80' ? ':' . $aPort : '');

			if (substr($aPath, 0, 1) == "/")
				$newComputedURL .= $aPath;
			else
			{
				$newComputedURL .= dirname($this->_path);
				$newComputedURL .= "/" . $aPath;
			}
	
			if ($aQuery != "")
				$newComputedURL .= "?" . $aQuery;

			// if there are cookies, keep them
			if (isset($this->headers['Set-Cookie']))
			{
				$cookieChunk = "Cookie: ";

				foreach ($this->headers['Set-Cookie'] as $eachCookie)
				{
					$pos = strpos($eachCookie, ';');
					
					if ($pos !== false)
						$cookieChunk .= substr($eachCookie, 0, $pos)  . "; ";
					else
						$cookieChunk .= $eachCookie . "; ";
				}
				
				$this->_requestHeaders["Cookie"] = $cookieChunk;
			}

			$this->_scan_url($newComputedURL);
			$this->execute();
		}
	}
}
?>