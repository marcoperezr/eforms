<?php
require_once("repository.inc.php");
require_once("initialize.php");
require_once("settingsvariable.inc.php");
require_once('eformsAppCustomization.inc.php');

class BITAMEFormsAppCustomizationTpl extends BITAMObject
{
	public $TemplateID;					//ID de definición de templates para permitir reutilizar en diferentes usuarios como si fuera un catálogo sin tener que perder antiguas definiciones
	public $TemplateName;				//Cuando se carga la instancia para grabar un template, contiene el nombre asignado al mismo
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->TemplateID = -1;
		$this->TemplateName = '';
	}
	
	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aTemplateID)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (((int)  $aTemplateID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.TemplateName 
			FROM SI_SV_AppCustomizationTpls A 
			WHERE A.TemplateID = {$aTemplateID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceWithName($aRepository, $aTemplateName)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (trim($aSettingsVariableName) == '')
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.TemplateName 
			FROM SI_SV_AppCustomizationTpls A 
			WHERE A.TemplateName = ".$aRepository->DataADOConnection->Quote($aTemplateName);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		$anInstance->TemplateName = (string) @$aRS->fields["templatename"];
		
		return $anInstance;
	}
	
	/* Dados los parámetros, obtiene la configuración de interface del App que actualmente tiene asignado el usuario logeado, si no se especifica el
	parámetro $aUserID entonces se utilizará el usuario actual de la sesión. El esquema a utilizar se obtiene como sigue:
	- Si hay configuración de Template por usuario, se utiliza primero dicha configuración
	- Si hay configuración de Template global, se utiliza dicha configuración
	- Si no hay Template configurado se utilizará la configuración de colores directa de Settings
	- En caso de no poder cargar el template indicado, se utilizarán los colores defaults del App (que es el equivalente a cuando nunca configuraron
		colores en los settings)
	*/
	static function GetCurrentColorScheme($aRepository, $aUserID = -1) {
		$strCalledClass = static::class;		
		if (is_null($aUserID) || (int) $aUserID <= 0) {
			$aUserID = $_SESSION["PABITAM_UserID"];
		}
		if ($aUserID <= 0) {
			$aUserID = -1;
		}
		
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
	    //Lee la configuración default del usuario especificado
		$intTemplateID = -1;
	    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $aUserID);
	    if (!is_null($theAppUser)) {
	    	$intTemplateID = $theAppUser->TemplateID;
	    }
		
		//Leer la configuración default por grupo
		if ($intTemplateID <= 0) {
			require_once('usergroup.inc.php');
			$groups = BITAMUserGroupCollectionLimited::NewInstance($aRepository, $aUserID);
			foreach($groups->Collection as $aGroup)
			{
				if($aGroup->TemplateID>0)
				{
					$intTemplateID = (int) @$aGroup->TemplateID;	
					break;
				}
			}
		}
		
		//Lee la configuración default global si no había una por usuario
		if ($intTemplateID <= 0) {
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTCOLORTEMPLATE');
			if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
				$intTemplateID = (int) @$objSetting->SettingValue;
			}
		}
		
		if ($intTemplateID <= 0) {
			$intTemplateID = -1;
		}
		
		//Carga la instancia del esquema de color
		//@JAPR 2014-10-29: Corregido un bug, no se estaba limpiando el ID del usuario, así que buscaba el template pero configurado por el usuario
		//específico, siendo que el usuario sólo debe servir para validar la configuración de usuario antes que la global, pero todos los templates
		//de colores se configuran en forma global realmente
		$anInstance = BITAMEFormsAppCustomization::NewInstanceAll($aRepository, -1, $intTemplateID);
		//@JAPR
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("TemplateID", $aHTTPRequest->POST))
		{
			$intTemplateID = $aHTTPRequest->POST["TemplateID"];
			
			if (is_array($intTemplateID))
			{
				$aCollection = BITAMEFormsAppCustomizationTplCollection::NewInstance($aRepository, $intTemplateID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intTemplateID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				$aResult = $anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("TemplateID", $aHTTPRequest->GET))
		{
			$intTemplateID = $aHTTPRequest->GET["TemplateID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intTemplateID);

			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("TemplateID", $anArray)) {
			$this->TemplateID = $anArray["TemplateID"];
		}
		
		if (array_key_exists("TemplateName", $anArray)) {
			$this->TemplateName = $anArray["TemplateName"];
		}
		
		return $this;
	}
	
	function save()
	{	
	 	if ($this->isNewObject()) {
			$sql =  "SELECT ".$this->Repository->DataADOConnection->IfNull("MAX(TemplateID)", "0")." + 1 AS TemplateID".
				" FROM SI_SV_AppCustomizationTpls";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->TemplateID = (int) $aRS->fields["templateid"];
			$sql = "INSERT INTO SI_SV_AppCustomizationTpls (".
						" TemplateID".
			            ", TemplateName".
			            ") VALUES (".
			            $this->TemplateID.
						",".$this->Repository->DataADOConnection->Quote($this->TemplateName).
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	 	else {
			$sql = "UPDATE SI_SV_AppCustomizationTpls SET ".
					"TemplateName = ".$this->Repository->DataADOConnection->Quote($this->TemplateName).
				" WHERE TemplateID = ".$this->TemplateID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	}
	
	function remove($bJustMobileTables = false)
	{
		$sql = "DELETE FROM SI_SV_AppCustomization WHERE TemplateID = {$this->TemplateID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$sql = "DELETE FROM SI_SV_AppCustomizationTpls WHERE TemplateID = {$this->TemplateID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->TemplateID < 0);
	}
	
	function get_Title()
	{
		return translate("Color template");
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=EFormsAppCustomizationTpl";
		}
		else
		{
			return "BITAM_PAGE=EFormsAppCustomizationTpl&TemplateID=".$this->TemplateID;
		}
	}
	
	function get_Parent()
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'TemplateID';
	}
	
	function get_FormFieldName()
	{
		return 'TemplateName';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TemplateName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	 
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}

 	function addButtons($aUser)
	{
		if ($_SESSION["PABITAM_UserRole"]==1 && !$this->isNewObject())
		{
?>
			<button id="colorsBtn" class="alinkescfav" onclick="javascript: customizeApp();"><img src="images/copy.gif" alt="<?=translate("Colors")?>" title="<?=translate("Colors")?>" displayMe="1" /> <?=translate("Colors")?></button>
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
		function customizeApp()
		{
			openWindowDialog('main.php?BITAM_PAGE=EFormsAppCustomization&TemplateID=<?=$this->TemplateID?>&UserID=-1', [window], [], 600, 600, customizeAppDone, 'customizeAppDone');
		}
		
		function customizeAppDone(sValue, sParams)
		{
			var strInfo = sValue;
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.CustomizedColors.value = strInfo;
				} catch (e) {
				}
			}
		}
	</script>
<?
 	}
}

class BITAMEFormsAppCustomizationTplCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfTemplateIDs = null)
	{
		$strCalledClass = static::class;
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance = new $strCalledClass($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfTemplateIDs))
		{
			switch (count($anArrayOfTemplateIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.TemplateID = ".((int) $anArrayOfTemplateIDs[0]);
					break;
				default:
					foreach ($anArrayOfTemplateIDs as $intTemplateID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$intTemplateID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.TemplateID IN (".$where.")";
					}
					break;
			}
		}
		
		$sql = "SELECT t1.TemplateID, t1.TemplateName 
			FROM SI_SV_AppCustomizationTpls t1 ".$where." ORDER BY t1.TemplateName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomizationTpls ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		//return BITAMSettingsVariable::NewInstance($this->Repository);
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Color templates");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=SettingsVariable";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=EFormsAppCustomizationTpl";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'TemplateID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TemplateName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_FormFieldName()
	{
		return 'TemplateName';
	}
}
?>