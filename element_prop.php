<?php
session_start();
require_once('utils.php');
require_once('cBitamFont.php');
require_once('element_prop.inc.php');
global $userlang,$html_Dims,$html_Inds,$array_formats,$array_inds_js,$array_inds_notnumeric,$array_conceptType;
global $html_Periods, $array_concept_dims_date;
?>
<html>
	<head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language='JavaScript' src='js/EditorHTML/<?=$_SESSION["PAuserLanguage"]?>.js'></script>
		<script language='JavaScript' src='js/EditorHTML/utils.js'></script>
		<script language="JavaScript" src="js/EditorHTML/dialogs.js"></script>
		<script language="JavaScript">
		var window_dialogArguments = getDialogArgument();
		document.writeln("<title>" + window_dialogArguments[0] + "</title>");
		owner = window_dialogArguments[1];

		var aIndsFormats = new Array();

		var aCubeIndJS = new Array();

		var aIndsNotNumeric = new Array();

		var aTempConceptType = new Array();

		var aConceptDimDate = new Array();

		</script>
		<script language='JavaScript' src='js/EditorHTML/element_prop.js'></script>
		<style type="text/css">@import url("css/fudgeEditorHTML.css"); </style>
	</head>
<body onload="OnStart();" class="NBody" onkeypress="check_keypress(event);">
<input type="hidden" id="Rep_type" name="Rep_type" value="">
<input type="hidden" id="Rep_name" name="Rep_name" value="">
<input type="hidden" id="Rep_id" name="Rep_id" value="">

<div id='divHS' class="HeaderStep">
	<img style="position:absolute; left:10px; top:5px; width:112px; height:49px;" src="images/lineasfondo.png">
	<table cellpadding="0" cellspacing="1" border="0" class="TBStep">
		<tr>
			<td width="20px"><img src="images/step_icon.gif"></td>
			<td width="270px" id="lbStep" class="Step"></td>
		</tr>
	</table>
</div>

<table width="100%" height="5%" cellpadding="0" cellspacing="1" border="0">
	<tr><td height="6px"></td></tr>
</table>

<!--
<table width="100%" height="5%" cellpadding="0" cellspacing="1" border="0">
	<tr>
		<td width="5%"><img src="images/step_icon.gif"></td>
		<td width="95%" id="lbStep" class="Step"></td>
	</tr>
	<tr><td height="6px"></td></tr>
</table>
-->

<div id="id_Step1" style="display:block;" class="NtabForm">
  <table cellpadding="0" cellspacing="0" width="100%" id="tbBody">
	<tr><td colspan="3" height="7px"></td></tr>
	<tr id="tr_legend">
		<td class="dataLabel" width="30%" id=legend>&nbsp;&nbsp;<script language="JavaScript">document.write(ML[591])</script></td>
		<td class="dataLabel" width="50%">
			<input id="id_caption" name="id_caption" size="35" maxlength="255" value="">
			<select id="cbDimensions" name="cbDimensions" style="display:none; width:250px;" onchange="SetTitle(this.id); checkPeriod(this);"><?php echo $html_Dims?></select>
			<select id="cbIndicators" name="cbIndicators" style="display:none; width:250px;" onchange="SetTitle(this.id);"><?php echo $html_Inds?></select>
			<select id="cbElem1" name="cbElem1" style="display:none; width:250px;" onchange="SetTitle(this.id);"></select>
			<select id="cbElem2" name="cbElem2" style="display:none; width:250px;" onchange="SetTitle(this.id);"></select>
		</td>
		<td id="col_color_sel_I1_I2" class="dataLabel" width="20%">
			<?php
				$perspective_sel = new cBitamGridSel('color_sel_I1');
				$bgcolor = 'FFFFFF';
				echo $perspective_sel->gen_selector_color(array('key' => $bgcolor, 'image' => $bgcolor, 'hint' => $bgcolor));
			?>
			<?php
				$perspective_sel = new cBitamGridSel('color_sel_I2');
				$bgcolor = 'FFFFFF';
				echo $perspective_sel->gen_selector_color(array('key' => $bgcolor, 'image' => $bgcolor, 'hint' => $bgcolor));
			?>
			<input type="checkbox" onclick="ShowAll();" id="id_viewall" name="id_viewall" value="1" style="background-color:transparent; border:none; display:none;">
		</td>
	</tr>

	<tr id="tr_period" style="display:none;">
		<td class="dataLabel" id=td_lbl_period>&nbsp;&nbsp;<script language="JavaScript">document.write(ML[83])</script></td>
		<td class="dataLabel">
			<select id="cbPeriods" name="cbPeriods" style="width:250px;" onchange="SetTitle(this.id);"><?php echo $html_Periods?></select>
		</td>
		<td>
			&nbsp;
		</td>
	</tr>
	
	 <!-- Sep-2014: Combo de secciones para el componente Botón -->
	<tr id="trButton" style="display:none;">
		<td class="dataLabel" width="35%">&nbsp;&nbsp;<script language="JavaScript">document.write(ML["Section"])</script></td>
		<td class="dataLabel" width="35%" colspan="2">
			<select id="cbButtonOptions" name="cbButtonOptions">
			</select>
		</td>
	</tr>

	<tr>
		<td id="id_labelVisible" class="dataLabel" width="35%">&nbsp;&nbsp;<script language="JavaScript">document.write(ML[268])</script></td>
		<td class="dataLabel" width="35%">
			<input type="checkbox" id="id_visible" name="id_visible" value="1" style="background-color:transparent; border:none">
		</td>
		<td class="dataLabel" width="30%">
			<?php
				if (isset($_REQUEST['elemStyle'])){
					$def_font = $_REQUEST['elemStyle'];
				}
				else {
					$def_font = 'arial_0_0_0_11_#FFFFFF_#000000';  // Name_Bold_Italic_Underline_Size_BG_FC
				}
				$style_comm = explode('_', $def_font);
				$font_sel = new cBitamFont('element');
				$font_sel->align = 'left';
				echo $font_sel->gen_control('1px', '1px', $style_comm, 'div_padre', '350px', false);
			?>
		</td>
	</tr>
	<tr id="trPivotValue" style="display:none;">
		<td class="dataLabel" width="35%">&nbsp;&nbsp;<script language="JavaScript">document.write(ML[656])</script></td>
		<td class="dataLabel" width="35%" colspan="2">
			<input type="checkbox" id="id_bPivotValue" name="id_bPivotValue" value="1" style="background-color:transparent; border:none">
		</td>
	</tr>
	<tr id="trTotalType" style="display:none;">
		<td class="dataLabel" width="35%">&nbsp;&nbsp;<script language="JavaScript">document.write(ML[106])</script></td>
		<td class="dataLabel" width="35%" colspan="2">
			<select id="cbTotalType" name="cbTotalType">
				<option value="0"><script language="JavaScript">document.write(ML[448])</script></option>
				<option value="1"><script language="JavaScript">document.write(ML[129])</script></option>
				<option value="2"><script language="JavaScript">document.write(ML[130])</script></option>
				<option value="3"><script language="JavaScript">document.write(ML[131])</script></option>
				<option value="4"><script language="JavaScript">document.write(ML[366])</script></option>
			</select>
		</td>
	</tr>
	<tr id='PGB_ShowDecimals'>
		<td class="dataLabel" width="35%">&nbsp;&nbsp;<script language="JavaScript">document.write(ML[652])</script></td>
		<td class="dataLabel" width="65%">
			<input type="checkbox" id="id_ShowDecimals" name="id_ShowDecimals" value="1" style="background-color:transparent; border:none">
		</td>
	</tr>
	<!-- POSITIONAMENT -->
	<tr id="trPositionament">
	  <td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td rowspan=2 width="5%">&nbsp;</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[478])</script>
					</td>
					<td class="dataLabel">
						<input id="id_left" name="id_left" size="4" maxlength="4" value="0" style="text-align:right;">
					</td>
					<td rowspan=2 width="5%">&nbsp;</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[592])</script>
					</td>
					<td class="dataLabel">
						<input id="id_width" name="id_width" size="4" maxlength="4" value="80" style="text-align:right;">
					</td>
				</tr>
				<tr>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[476])</script>
					</td>
					<td class="dataLabel">
						<input id="id_top" name="id_top" size="4" maxlength="4" value="10" style="text-align:right;">
					</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[587])</script>
					</td>
					<td class="dataLabel">
						<input id="id_height" name="id_height" size="4" maxlength="4" value="20" style="text-align:right;">
					</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- ALIGNMENT -->
	<tr id='id_alignment'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML[593])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<input type="radio" name="rb_align" id="rb_align_0" value="0" checked style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML[478])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_align" id="rb_align_1" value="1" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML[594])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_align" id="rb_align_2" value="2" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML[479])</script>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- Style Degradado -->
	<tr id='id_styledegradado'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML[265])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_styledegradado_Grad" value="Grad" checked style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['degradado'])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_styledegradado_CilBar" value="CilBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['cilindro'])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_styledegradado_GlassBar" value="GlassBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['vidrio'])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_styledegradado_SoftBar" value="SoftBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['suave'])</script>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- Color Degradado -->
	<tr id='id_colordegradado'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML[270])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML['inicial'])</script>
					</td>
					<td class="dataLabel">
						<?php
							$perspective_sel = new cBitamGridSel('ini_color_sel');
							$bgcolor = 'FFFFFF';
							echo $perspective_sel->gen_selector_color(array('key' => $bgcolor, 'image' => $bgcolor, 'hint' => $bgcolor));
						?>
					</td>
					<td width="15%">&nbsp;</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML['final'])</script>
					</td>
					<td class="dataLabel">
						<?php
							$perspective_sel = new cBitamGridSel('fin_color_sel');
							$bgcolor = 'FFFFFF';
							echo $perspective_sel->gen_selector_color(array('key' => $bgcolor, 'image' => $bgcolor, 'hint' => $bgcolor));
						?>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- Gradiante Horizontal/Vertical -->
	<tr id='id_gradianteHorVer'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML['degradado'])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<input type="radio" name="rb_verticaldegradado" id="rb_degradado_ver" value="Ver" checked style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML[242])</script>
					</td>
					<td width="20%">&nbsp;</td>
					<td class="dataLabel">
						<input type="radio" name="rb_verticaldegradado" id="rb_degradado_hor" value="Hor" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML[243])</script>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- Style Degradado -->
	<tr id='id_styledegradadoPGB'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML[265])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_stylePGB_CilBar" value="CilBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['cilindro'])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_stylePGB_GlassBar" value="GlassBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['vidrio'])</script>
					</td>
					<td class="dataLabel">
						<input type="radio" name="rb_styledegradado" id="rb_stylePGB_SoftBar" value="SoftBar" style="border:none; background-color:transparent;">
						&nbsp;<script language="JavaScript">document.write(ML['suave'])</script>
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- BORDER -->
	<tr id='id_border'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
	  	<legend class="dataLabel"><script language="JavaScript">document.write(ML[595])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td width="5%">&nbsp;</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[270])</script>
					</td>
					<td class="dataLabel">
						<?php
							$perspective_sel = new cBitamGridSel('border_color_sel');
							$bgcolor = 'FFFFFF';
							echo $perspective_sel->gen_selector_color(array('key' => $bgcolor, 'image' => $bgcolor, 'hint' => $bgcolor));
						?>
					</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[360])</script>
					</td>
					<td class="dataLabel">
						<?php
							$perspective_sel = new cBitamGridSel('border_type_sel');
							$perspective_sel->align = 'left';
							$perspective_sel->addItem('0', 'images/border_none.gif',  'None');
							$perspective_sel->addItem('1', 'images/border_regular.gif', 'Regular');
							$perspective_sel->addItem('2', 'images/border_raised.gif', 'Raised');
							$perspective_sel->addItem('3', 'images/border_drop.gif',  'Drop');
							echo $perspective_sel->gen_control('152px', '133px', '0');
						?>
					</td>
					<td class="dataLabel">
						<script language="JavaScript">document.write(ML[592])</script>
					</td>
					<td class="dataLabel">
						<input id="id_border_width" name="id_border_width" size="4" maxlength="4" value="1" style="text-align:right;">
					</td>
					<td width="5%">&nbsp;</td>
				</tr>
			</table>
	    </fieldset>
	</td></tr>
	<!-- IMAGE -->
	<tr id='id_imagen'><td colspan="3" class="dataLabel">
	  <fieldset style="border-style: outset">
		<legend class="dataLabel"><script language="JavaScript">document.write(ML[361])</script></legend>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" width="99.5%">
				<tr>
					<td rowspan=2 width="5%">&nbsp;</td>
					<td rowspan=2 width="15%" height="25px">
						<IMG id="idDefImg" style="WIDTH:60px; HEIGHT:50px; border: 1px solid #062040;" src = "images/transparent.gif" ></IMG>
					</td>
					<td width="80%" align="center">
						<input id=botSelImg class="Nbutton" type="button" name="botSelImg" onclick="SelImg();" style="width:auto;">
						&nbsp;&nbsp;
						<input id=botClearImg class="Nbutton" type="button" name="botClearImg" onclick="ClearImg();" style="width:auto;">
					</td>
				</tr>
				<tr>
					<td class="dataLabel" align="center">&nbsp;
						<script language="JavaScript">document.write(ML[590])</script>
						&nbsp;&nbsp;&nbsp;
						<input type="checkbox" id="id_streched" name="id_streched" value="1" style="background-color:transparent; border:none">
					</td>
				</tr>
			</table>
			<table cellpadding="0" cellspacing="0" border="0" height="5px">
				<tr><td height="5px"></td></tr>
			</table>
	    </fieldset>
	  </td></tr>
	  
	 <tr><td colspan="3" height="7px"></td></tr>
  </table>
  <br>
</div>
<!-- OK -->
<table width="100%" cellpadding="0" cellspacing="0" border="0" id="tbButtons" style="position:absolute; bottom:20px;">
	<tr>
		<td align="right">
			<input id=botOK class="Nbutton" type="button" name="OK" onclick="On_Ok();">
			<input id=botCancel class="Nbutton" type="button" name="Cancel" onclick="closeDialog()">
		</td>
	</tr>
</table>
</body>
</html>