<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("survey.inc.php");

class BITAMCatalogMember extends BITAMObject
{
	public $CatalogID;
	public $CatalogName;
	public $MemberID;
	public $MemberName;
	public $ClaDescripParent;
	public $ClaDescrip;
	public $MemberOrder;
	public $ModelID;
	//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
	public $DescriptorID;				//MemberID del mismo catálogo del que es clave este atributo
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $DissociateCatDimens;
	public $IndDimID;
	public $DissociatedDimTableName;	//Nombre de tabla de la dimensión independiente en caso de usar esa funcionalidad
	//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
	public $eBavelFieldID;
	//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
	public $eBavelFieldDataID;			//Dato específico que se solicita al campo de eBavel mapeado (a la fecha de implementación sólo aplicaba para campos tipo Geolocation)
	//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
	public $CreationAdminVersion;			//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;			//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	public $DataSourceMemberID;				//ID del DataSourceMember (atributo de un catálogo de v6) al que se asocia esta pregunta
	//@JAPR
	
	public $KeyOfAgenda;
	public $AgendaLatitude;
	public $AgendaLongitude;
	public $AgendaDisplay;
	
	public $IsImage;
	
	function __construct($aRepository, $aCatalogID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->CatalogID = $aCatalogID;
		$this->CatalogName= "";
		$this->MemberID = -1;
		$this->MemberName = "";
		$this->ModelID = -1;
		$this->ClaDescripParent = -1;
		$this->ClaDescrip = -1;
		//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
		$this->DescriptorID = 0;
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->DissociateCatDimens = 1;
		$this->IndDimID = 0;
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$this->eBavelFieldID = 0;
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		$this->eBavelFieldDataID = ebfdValue;
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
		$this->DataSourceMemberID = 0;
		//@JAPR
		
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		//Optimizado para asignarse esta información a partir del catálogo, ya que su instancia se queda en Caché a diferencia del query que originalmente se hacía
		$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
		if (!is_null($objCatalog)) {
			if ($objCatalog->CreationAdminVersion < esveFormsv6) {
				$this->ModelID = $objCatalog->ModelID;
				$this->ClaDescripParent = $objCatalog->ParentID;
				$this->CatalogName = $objCatalog->CatalogName;
				//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				$this->DissociateCatDimens = $objCatalog->DissociateCatDimens;
				//@JAPR
			}
		}
		/*
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = "";
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', DissociateCatDimens';
		}
		$sql = "SELECT ModelID, ParentID, CatalogName $strAdditionalFields 
			FROM SI_SV_Catalog 
			WHERE CatalogID = ".$this->CatalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			if (!$aRS->EOF) {
				$this->ModelID = (int) $aRS->fields['modelid'];
				$this->ClaDescripParent = (int) $aRS->fields['parentid'];
				$this->CatalogName= trim($aRS->fields['catalogname']);
				//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				$this->DissociateCatDimens = (int) @$aRS->fields['dissociatecatdimens'];
				//@JAPR
			}
		}
		*/
		
		$this->KeyOfAgenda = 0;
		$this->AgendaLatitude = 0;
		$this->AgendaLongitude = 0;
		$this->AgendaDisplay = 0;
		
		$this->IsImage = 0;
		
		unset($aRS);
	}

	static function NewInstance($aRepository, $aCatalogID)
	{
		return new BITAMCatalogMember($aRepository, $aCatalogID);
	}

	static function NewInstanceWithID($aRepository, $aMemberID)
	{
		$anInstance = null;
		
		if (((int) $aMemberID) <= 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetCatMemberInstanceWithID($aMemberID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', t1.IndDimID, t2.DissociateCatDimens';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', t1.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.KeyOfAgenda, t1.AgendaLatitude, t1.AgendaLongitude, t1.AgendaDisplay';
		}
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', t1.eBavelFieldDataID';
		}
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$strAdditionalFields .= ', t1.IsImage';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', t1.DataSourceMemberID, t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
		$sql = "SELECT t1.CatalogID, t2.CatalogName, t1.MemberID, t1.MemberName, t2.ParentID as ClaDescripParent, t1.ParentID as ClaDescrip, t1.MemberOrder, 
					t1.DescriptorID $strAdditionalFields 
				FROM SI_SV_CatalogMember t1, SI_SV_Catalog t2 
				WHERE t1.CatalogID = t2.CatalogID AND t1.MemberID = ".$aMemberID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF) {
			$anInstance = BITAMCatalogMember::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aCatalogID = (int)$aRS->fields["catalogid"];
		
		$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
		$anInstance->CatalogName = $aRS->fields["catalogname"];
		
		$anInstance->MemberID = (int)$aRS->fields["memberid"];
		$anInstance->MemberName = $aRS->fields["membername"];
		
		$anInstance->ClaDescripParent = (int) @$aRS->fields["cladescripparent"];
		$anInstance->ClaDescrip = (int) @$aRS->fields["cladescrip"];
		
		$anInstance->MemberOrder = (int) $aRS->fields["memberorder"];
		//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
		$anInstance->DescriptorID = (int) $aRS->fields["descriptorid"];
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$anInstance->IndDimID = (int) @$aRS->fields["inddimid"];
		$anInstance->DissociateCatDimens = (int) @$aRS->fields["dissociatecatdimens"];
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		$anInstance->eBavelFieldDataID = (int) @$aRS->fields["ebavelfielddataid"];
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$anInstance->DataSourceMemberID = (int) @$aRS->fields["datasourcememberid"];
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->KeyOfAgenda = (int) @$aRS->fields["keyofagenda"];
			$anInstance->AgendaLatitude = (int) @$aRS->fields["agendalatitude"];
			$anInstance->AgendaLongitude = (int) @$aRS->fields["agendalongitude"];
			$anInstance->AgendaDisplay = (int) @$aRS->fields["agendadisplay"];
		}
		//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
		$anInstance->IsImage = (int) @$aRS->fields["isimage"];
		//@JAPR
		
		BITAMGlobalFormsInstance::AddCatMemberInstanceWithID($anInstance->MemberID, $anInstance);
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = (int) $aHTTPRequest->GET["CatalogID"];
		}
		else
		{
			$aCatalogID = 0;
		}

		if (array_key_exists("MemberID", $aHTTPRequest->POST))
		{
			$aMemberID = $aHTTPRequest->POST["MemberID"];
			
			if (is_array($aMemberID))
			{
				$aCollection = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID, $aMemberID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove(true);
				}
				$aCollection = BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
				$newOrder = 1;
				foreach ($aCollection as $anInstanceMember) {
					$sql = "UPDATE SI_SV_CatalogMember SET MemberOrder = ".$newOrder." WHERE MemberID = ".$anInstanceMember->MemberID." AND CatalogID = ".$aCatalogID;
					if ($aRepository->DataADOConnection->Execute($sql) === false) {
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					$newOrder++;
				}

				$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, (int)$aMemberID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("MemberID", $aHTTPRequest->GET))
		{
			$aMemberID = $aHTTPRequest->GET["MemberID"];
			$anInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, (int)$aMemberID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
			}
		}
		else
		{
			$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("ModelID", $anArray))
		{
			$this->ModelID = (int)$anArray["ModelID"];
		}
		
		if (array_key_exists("ClaDescripParent", $anArray))
		{
			$this->ClaDescripParent = (int)$anArray["ClaDescripParent"];
		}
		
		if (array_key_exists("ClaDescrip", $anArray))
		{
			$this->ClaDescrip = (int)$anArray["ClaDescrip"];
		}
		
		if (array_key_exists("MemberID", $anArray))
		{
			$this->MemberID = (int)$anArray["MemberID"];
		}
		
		if (array_key_exists("MemberOrder", $anArray))
		{
			$this->MemberOrder = (int)$anArray["MemberOrder"];
		}
		
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = (int)$anArray["CatalogID"];
		}

 		if (array_key_exists("MemberName", $anArray))
		{
			$this->MemberName = rtrim($anArray["MemberName"]);
		}

		//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
		if (array_key_exists("DescriptorID", $anArray))
		{
			$this->DescriptorID = (int) $anArray["DescriptorID"];
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (array_key_exists("eBavelFieldID", $anArray))
		{
			$this->eBavelFieldID = (int) $anArray["eBavelFieldID"];
		}
		
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (array_key_exists("eBavelFieldDataID", $anArray))
		{
			$this->eBavelFieldDataID = (int) $anArray["eBavelFieldDataID"];
		}
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			if (array_key_exists("KeyOfAgenda", $anArray)) {
				$this->KeyOfAgenda = (int) $anArray["KeyOfAgenda"];
			}
			if (array_key_exists("AgendaLatitude", $anArray)) {
				$this->AgendaLatitude = (int) $anArray["AgendaLatitude"];
			}
			if (array_key_exists("AgendaLongitude", $anArray)) {
				$this->AgendaLongitude = (int) $anArray["AgendaLongitude"];
			}
			if (array_key_exists("AgendaDisplay", $anArray)) {
				$this->AgendaDisplay = (int) $anArray["AgendaDisplay"];
			}
		}
		
		if (array_key_exists("IsImage", $anArray)) {
			$this->IsImage = (int) $anArray["IsImage"];
		}
		
		return $this;
	}
	
	//@JAPRDescontinuada en v6
	function removeAttribDimension() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		if ($this->ClaDescrip <= 0) {
			return;
		}
		
		$strOriginalWD = getcwd();
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		$AttribName = $this->MemberName;
		
		$cla_descrip = $this->ClaDescrip;
		$cla_descrip_parent = $this->ClaDescripParent;
		$cla_concepto = $this->ModelID;
		
		$anInstanceModelDim = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
		
		$anInstanceModelDim->Dimension->isCATALOG = true;
		$anInstanceModelDim->Dimension->DimensionName = $AttribName;
		$anInstanceModelDim->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceModelDim->Dimension->FieldDescription = 'DSC_'.$cla_descrip;
		$anInstanceModelDim->Dimension->DimensionID = $cla_descrip;
		
		//Vamos a forzar que se elimine el atributo aun cuando la dimension esta presente en algun escenario
		$anInstanceModelDim->Dimension->CheckStages = false;
		
		$anInstanceModelDim->remove();
		
		chdir($strOriginalWD);
		
		//@JAPR 2013-01-21: Aunque a la fecha no hay forma de verificar si lo hizo bien o no, se actualiza el ID de dimensión para que ya no intente
		//remover nuevamente la dimensión en caso de un error mas adelante
		$sql = "UPDATE SI_SV_CatalogMember SET 
				ParentID = 0 
			WHERE MemberID = ".$this->MemberID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$this->ClaDescrip = 0;
	}
	
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Elimina la dimensión independiente del catálogo si es que estuviera asignada
	//@JAPRDescontinuada en v6
	function removeDissociatedDimension() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		if (!$this->DissociateCatDimens || $this->IndDimID <= 0) {
			return;
		}
		
		$strOriginalWD = getcwd();
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, -1, -1, $this->IndDimID);
		$strMMError = '';
        if (!is_null($anInstanceModelDim)) {
			$objMMAnswer = @$anInstanceModelDim->remove_only_dimension();
			$strMMError = identifyModelManagerError($objMMAnswer);
        }
		
		chdir($strOriginalWD);
		
		//@JAPR 2013-01-21: Aunque a la fecha no hay forma de verificar si lo hizo bien o no, se actualiza el ID de dimensión para que ya no intente
		//remover nuevamente la dimensión en caso de un error mas adelante
		if ($strMMError == '') {
			$sql = "UPDATE SI_SV_CatalogMember SET 
					IndDimID = 0 
				WHERE MemberID = ".$this->MemberID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->IndDimID = 0;
		}
	}
	//@JAPR
	
	//@JAPRDescontinuada en v6
	function saveAttribDimension() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return -1;
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		$AttribName = $this->MemberName;
		
		$cla_descrip = $this->ClaDescrip;
		$cla_descrip_parent = $this->ClaDescripParent;
		$cla_concepto = $this->ModelID;
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Dimensión atributo, no debe ser reutilizada de otro cubo
		$anInstanceModelDim = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
		$anInstanceModelDim->Dimension->isCATALOG = true;
		$anInstanceModelDim->Dimension->DimensionName = $AttribName;
		$anInstanceModelDim->Dimension->ParentDimensionID = $cla_descrip_parent;
		$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
		$anInstanceModelDim->Dimension->DimensionID = $cla_descrip;
		//@JAPR 2013-01-28: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Con esta propiedad se impedirá que el Model Manager manipule a la dimensión creada para eForms
		@$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->save();
		
		$this->TableName = $anInstanceModelDim->Dimension->TableName;
		
		chdir($strOriginalWD);
		
		return $anInstanceModelDim->Dimension->DimensionClaDescrip;
	}
	
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Agrega la dimensión independiente del catálogo si es que está configurado para soportarlas
	//@JAPRDescontinuada en v6
	function saveDissociatedDimension()
	{
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		if (!$this->DissociateCatDimens || $this->IndDimID > 0) {
			return $this->IndDimID;
		}
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Dimensión desasociada que representa un atributo, no se había programado para ser compartida, y aunque no es realmente necesario ya que es una
		//dimensión completamente independiente, no se va a reutilizar entre diferentes catálogos para mantener la consistencia con las dimensiones
		//atributos originales de los catálogos (se podría reutilizar internamente como dimensión de una pregunta, así que eso si se debería impedir)
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, -1);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		//@JAPR 2013-01-28: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Con esta propiedad se impedirá que el Model Manager manipule a la dimensión creada para eForms
		@$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = dsdALTERNATIVE_DIMENSION_PREFIX.$this->MemberName;
		$anInstanceModelDim->Dimension->UseKey = 0;
		
		//$anInstanceModelDim->Dimension->DimensionClaDescrip = $this->ParentID;
		$anInstanceModelDim->Dimension->DimensionID = -1;
		$anInstanceModelDim->Dimension->DimensionGroup = $this->CatalogName;
		$anInstanceModelDim->Dimension->IsCatalog = false;
		$objMMAnswer = @$anInstanceModelDim->save();
		$strMMError = identifyModelManagerError($objMMAnswer);
		chdir($strOriginalWD);
		
		if ($strMMError == '') {
			$this->DissociatedDimTableName = $anInstanceModelDim->Dimension->TableName;
			$intIndDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			$tmpFieldKey = $this->DissociatedDimTableName."KEY";
			$tmpFieldDesc = "DSC_".$intIndDimID;
			//Insertar el primer valor de No Aplica
			$sqlIns = "INSERT INTO ".$this->DissociatedDimTableName." (".$tmpFieldKey.", ".$tmpFieldDesc.") VALUES (1, ".$this->Repository->DataADOConnection->Quote('*NA').")";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$this->DissociatedDimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
			
			return $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		else {
			die("(".__METHOD__.") ".translate("Error creating attribute's global dimension").": ".$strMMError.". ");
		}
	}
	//@JAPR
	
	//@JAPRDescontinuada en v6
	function exportAttribDimension() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if ($this->CreationAdminVersion >= esveFormsv6) {
			return;
		}
		//@JAPR
		
		/** PHPExcel */
		require_once("../model_manager/phpexcel/Classes/PHPExcel.php");
		
		$anInstanceModelDim = BITAMAttribDimension::NewAttribDimension($this->Repository, -1, $this->ClaDescripParent);
		$anInstanceModelDim->ParentDimensionID = $this->ClaDescripParent;
		$anInstanceModelDim->CreateXLS_PHP_Attribs($this->CatalogID, $this->CatalogName);
	}
	
	function save() {
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		
		if ($this->isNewObject()) {
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			$this->ClaDescrip = -1;
			$cla_descrip = $this->saveAttribDimension();
			
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			//Se desligan los catalogos completamente de los cubos de Artus
			if ($this->CreationAdminVersion < esveFormsv6) {
				$tmpTableName = "RIDIM_".$this->ClaDescripParent;
				$tmpFieldKey = $tmpTableName."KEY";
				$tmpFieldDesc = "DSC_".$cla_descrip;
				
				//Actualizamos el campo a valor de NO Aplica
				$sql = "UPDATE ".$tmpTableName." SET ".$tmpFieldDesc." = ".$this->Repository->DataADOConnection->Quote('*NA')." 
						WHERE ".$tmpFieldKey." = 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false) {
					//die("(".__METHOD__.") ".translate("Error accessing")." ".$tmpTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			//@JAPR
			
			//Obtener el MemberID
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MemberID)", "0")." + 1 AS MemberID".
						" FROM SI_SV_CatalogMember";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->MemberID = (int) $aRS->fields["memberid"];
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(MemberOrder)", "0")." + 1 AS MemberOrder".
						" FROM SI_SV_CatalogMember where CatalogID = ".$this->CatalogID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->MemberOrder = (int) $aRS->fields["memberorder"];
			unset ($aRS);
			$this->ClaDescrip = $cla_descrip;
			$this->IndDimID = 0;
			
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvCatalogDissociation) {
				$strAdditionalFields .= ', IndDimID';
				$strAdditionalValues .= ', '.$this->IndDimID;
			}
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ', eBavelFieldID';
				$strAdditionalValues .= ', '.$this->eBavelFieldID;
			}
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', KeyOfAgenda';
				$strAdditionalValues .= ', '.$this->KeyOfAgenda;
				$strAdditionalFields .= ', AgendaLatitude';
				$strAdditionalValues .= ', '.$this->AgendaLatitude;
				$strAdditionalFields .= ', AgendaLongitude';
				$strAdditionalValues .= ', '.$this->AgendaLongitude;
				$strAdditionalFields .= ', AgendaDisplay';
				$strAdditionalValues .= ', '.$this->AgendaDisplay;
			}
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$strAdditionalFields .= ', eBavelFieldDataID';
				$strAdditionalValues .= ', '.$this->eBavelFieldDataID;
			}
			if (getMDVersion() >= esvCatalogAttributeImage) {
				$strAdditionalFields .= ', IsImage';
				$strAdditionalValues .= ', '.$this->IsImage;
			}
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ', DataSourceMemberID, CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$this->DataSourceMemberID.', '.$this->LastModAdminVersion.', '.$this->LastModAdminVersion;
			}
			//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
			$sql = "INSERT INTO SI_SV_CatalogMember (".
					"CatalogID".
					",MemberOrder".
					",ParentID".
					",MemberID".
					",MemberName".
					",DescriptorID".
					$strAdditionalFields.
					") VALUES (".
					$this->CatalogID.
					",".$this->MemberOrder.
					",".$this->ClaDescrip.
					",".$this->MemberID.
					",".$this->Repository->DataADOConnection->Quote($this->MemberName).
					",".$this->DescriptorID.
					$strAdditionalValues.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Al agregar un nuevo atributo al catálogo se debe regenerar la jerarquía
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			//Se desligan los catalogos completamente de los cubos de Artus
			if ($this->CreationAdminVersion < esveFormsv6) {
				if ($this->DissociateCatDimens) {
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
					if (!is_null($objCatalog)) {
						$objCatalog->generateCatalogHierarchy();
					}
				}
			}
			//@JAPR
		}
		else
		{
			$this->saveAttribDimension();
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
			$strAdditionalFields = '';
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ", eBavelFieldID = ".$this->eBavelFieldID;
			}
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ", KeyOfAgenda = ".$this->KeyOfAgenda;
				$strAdditionalFields .= ", AgendaLatitude = ".$this->AgendaLatitude;
				$strAdditionalFields .= ", AgendaLongitude = ".$this->AgendaLongitude;
				$strAdditionalFields .= ", AgendaDisplay = ".$this->AgendaDisplay;
			}
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$strAdditionalFields .= ", eBavelFieldDataID = ".$this->eBavelFieldDataID;
			}
			if (getMDVersion() >= esvCatalogAttributeImage) {
				$strAdditionalFields .= ", IsImage = ".$this->IsImage;
			}
			//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ", DataSourceMemberID = ".$this->DataSourceMemberID.
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
			$sql = "UPDATE SI_SV_CatalogMember SET 
					MemberName = ".$this->Repository->DataADOConnection->Quote($this->MemberName)." 
					,MemberOrder = ".$this->MemberOrder." 
					,DescriptorID = ".$this->DescriptorID.$strAdditionalFields." 
				WHERE MemberID = ".$this->MemberID." AND CatalogID = ".$this->CatalogID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
		//@JAPRDescontinuada en v6
		//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
		//La actualización de formas la realizará el DataSource directamente
		//BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($this->Repository, $this->CatalogID);
		//@JAPR
		$this->exportAttribDimension();
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$intIndDimID = 0;
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		//Se desligan los catalogos completamente de los cubos de Artus
		if (getMDVersion() >= esvCatalogDissociation && $this->CreationAdminVersion < esveFormsv6) {
			if ($this->IndDimID <= 0) {
				$intIndDimID = (int) $this->saveDissociatedDimension();
				if ($intIndDimID > 0) {
					$this->IndDimID = $intIndDimID;
					//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
					$sql = "UPDATE SI_SV_CatalogMember SET 
							IndDimID = ".$this->IndDimID." 
						WHERE MemberID = ".$this->MemberID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
		}
		//@JAPR
		
		return $this;
	}
	
	function remove ($fromCollection = null) {
		//@JAPR 2013-01-21: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Movida la eliminación de la dimensión a este punto por si ocurre un error durante el borrado, de esta forma no se quedan las dimensiones
		//flotando
		$this->removeAttribDimension();
		if (getMDVersion() >= esvCatalogDissociation) {
			$this->removeDissociatedDimension();
		}
		//@JAPR
		
		//@JAPR 2015-07-26: Agregada la eliminación de los catálogos internos de las preguntas cuando se elimina la pregunta o el DataSource
		//Al eliminar un catálogo se deben actualizar todas las preguntas que lo utilizan, cuando se trata de la versión 6+ este método se invoca desde el 
		//DataSource y en realidad sólo debería estar asociado a una única pregunta (con preguntas tipo GetData se podría enlazar con otras preguntas adicionales)
		//pero es necesario desligarlas en la metadata
		if ($this->CreationAdminVersion >= esveFormsv6) {
			$sql = "UPDATE SI_SV_Section SET 
					CatMemberID = 0 
				WHERE CatMemberID = {$this->MemberID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "UPDATE SI_SV_Question SET 
					CatMemberID = 0 
				WHERE CatMemberID = {$this->MemberID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "UPDATE SI_SV_Question SET 
					CatMemberLatitudeID = 0 
				WHERE CatMemberLatitudeID = {$this->MemberID}";
			$this->Repository->DataADOConnection->Execute($sql);
			
			$sql = "UPDATE SI_SV_Question SET 
					CatMemberLongitudeID = 0 
				WHERE CatMemberLongitudeID = {$this->MemberID}";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		//@JAPR
		
		$memberOrder = $this->MemberOrder;
		$memberCatalogID = $this->CatalogID;
		$sql = "DELETE FROM SI_SV_CatalogMember WHERE MemberID = ".$this->MemberID." AND CatalogID = ".$this->CatalogID;
		if ($this->Repository->DataADOConnection->Execute($sql)) {
			if (is_null($fromCollection)) {
				$sql = "UPDATE SI_SV_CatalogMember SET MemberOrder = MemberOrder - 1 WHERE MemberOrder > ".$memberOrder." AND CatalogID = ".$memberCatalogID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false) {
					//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		
		return $this;
	}

	function isNewObject()
	{
		return ($this->MemberID <= 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Attribute");
		}
		else
		{
			return translate("Attribute")." ".$this->MemberName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=CatalogMember&CatalogID=".$this->CatalogID;
		}
		else
		{
			return "BITAM_PAGE=CatalogMember&CatalogID=".$this->CatalogID."&MemberID=".$this->MemberID;
		}
	}

	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MemberID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return true;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MemberName";
		$aField->Title = translate("Attribute");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DescriptorID";
		$aField->Title = translate("Is key of");
		$aField->Type = "Object";
		$aField->VisualComponent = "ComboBox";
		$aField->Options = $this->getRemainingCatalogAttributes(true);
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
			
			require_once('eBavelIntegration.inc.php');

            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
            if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma entonces pedimos sus atributos
	            if($objCatalog->eBavelFormType == ebftForms)
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	            else//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');

            } //@AAL
            else
            	$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');         
            
			$arrAllEBavelFormsFields = array(0 => '('.translate("None").')');
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			$arrAllEBavelFormsFieldsData = array(0 => array(0 => '('.translate("None").')'));
			//@JAPR
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					if ($intFieldID > 0) {
						$arrAllEBavelFormsFields[$intFieldID] = $arrFieldData['label'];
						
						//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$inteBavelQTypeID = convertEBavelToEFormsFieldType($arrFieldData['tagname']);
						$arrAllEBavelFormsFieldsData[$intFieldID] = array(ebfdValue => translate("Value"));
						switch ($inteBavelQTypeID) {
							case qtpLocation:
								//En los campos tipo Geolocalización, se agregarán las opciones para utilizar el valor, la latitud o la longitud
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLatitude] = translate("Latitude");
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLongitude] = translate("Longitude");
								break;
							default:
								//El resto de los campos sólo agrega el valor para forzar a que sea la única opción posible a usar
								break;
						}
						//@JAPR
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("eBavel form field");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrAllEBavelFormsFields;
			$aField->OnChange = "showHideOptions();";
			$myFields[$aField->Name] = $aField;
			$eBavelFieldsField = $aField;
			
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			if (getMDVersion() >= esveBavelGeolocationAttribs) {
				$eBavelFieldsField->CloseCell = false;
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "eBavelFieldDataID";
				$aField->Title = "DataID";
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrAllEBavelFormsFieldsData;
				//Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
				//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
				//su tamaño provocaba que si se considerara menor que 0px debido a eso
				$aField->FixedWidth = "0";
				$myFields[$aField->Name] = $aField;
				$eBavelFieldDataIDField = $aField;
				
				$eBavelFieldDataIDField->Parent = $eBavelFieldsField;
				$eBavelFieldsField->Children[] = $eBavelFieldDataIDField;
			}
			//@JAPR
		}
		//@JAPR
		
		if(getMDVersion() >= esvAgendaRedesign) {
			$this->get_FormField_KeyOfAgenda($myFields);
			$this->get_FormField_AgendaLatitude($myFields);
			$this->get_FormField_AgendaLongitude($myFields);
			$this->get_FormField_AgendaDisplay($myFields);
		}
		
		if(getMDVersion() >= esvCatalogAttributeImage) {
			$this->get_FormField_IsImage($myFields);
		}
		
		return $myFields;
	}
	
	function get_Field ($name, $title, $type, $options = null) {
		$aField = BITAMFormField::NewFormField();
		$aField->Name = $name;
		$aField->Title = translate($title);
		switch($type) {
			case 'Checkbox':
				$aField->Type = "Boolean";
				$aField->VisualComponent = $type;
				break;
			case 'Combobox':
				$aField->Type = "Object";
				$aField->VisualComponent = $type;
				$aField->Options = ((isset($options['options']))? $options['options'] : array());
				break;
			case 'Integer':
				$aField->Type = $type;
				$aField->FormatMask = ((isset($options['formatMask']))? $options['formatMask'] : '');
				break;
			case 'String':
				$aField->Type = $type;
				$aField->Size = ((isset($options['size']))? $options['size'] : 255);
				break;
			default:
				$aField->Type = $type;
				$aField->Size = 255;
				break;
		}
		return $aField;
	}
		
	function get_FormField ($name, $title, $type, $deployableVersion) {
		$aField = null;
		if(deployable($deployableVersion)) {
			$aField = $this->get_Field($name, $title, $type);
		}
		return $aField;
	}
	
	function append_FormField (&$myFields, $aField) {
		if(!is_null($myFields)) {
			$myFields[$aField->Name] = $aField;
		}
	}
	
	function get_FormField_AgendaRedesign ($name, $title, $type, $deployableVersion, &$myFields = null) {
		$aField = $this->get_FormField($name, $title, $type, $deployableVersion);
		$aField->Options = $this->getRemainingCatalogAttributes(true);
		$this->append_FormField($myFields, $aField);
		return $aField;
	}
	
	function get_FormField_KeyOfAgenda (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("KeyOfAgenda", "Key of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaLatitude (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaLatitude", "Latitude of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaLongitude (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaLongitude", "Longitude of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_AgendaDisplay (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("AgendaDisplay", "Display of Agenda", "Checkbox" , "esvAgendaRedesign", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	function get_FormField_IsImage (&$myFields = null) {
		$aField = $this->get_FormField_AgendaRedesign("IsImage", "Is Image?", "Checkbox" , "esvCatalogAttributeImage", $myFields);
		//$aField->OnChange = "showHideChkValidation();";
		return $aField;
	}
	
	//@JAPR 2014-06-06: Agregado el refresh de catálogos dinámico
	//Regresa el nombre de variable asignado a este catálogo, el cual es un nombre entendible por el usuario basado en el nombre del atributo en sí
	//eliminando espacios en blanco, en caso de repetirse en el mismo catálogo, se agregará el número de atributo al nombre obtenido. Se usan los
	//primeros 10 caracteres o hasta encontrar el primer espacio en blanco. Estos nombres NO son con los que se graban, internamente se grabarán
	//los nombres usando el ID para permitir su reemplazo dinámicamente
	function getVariableName() {
		$strCatMemberVarName = trim($this->MemberName);
		$intPos = strpos($strCatMemberVarName, ' ');
		if ($intPos !== false) {
			//@JAPR 2014-08-28: Corregido un bug, se estaba dejando el espacio en blanco, ahora preferentemente agregará una segunda palabra cuando
			//se detecte un espacio en blanco, si es que hay un segundo espacio
			$intPos2 = strpos($strCatMemberVarName, ' ', $intPos +1);
			if ($intPos2 !== false) {
				$strCatMemberVarName = str_replace(' ', '', substr($strCatMemberVarName, 0, $intPos2));
			}
			else {
				$strCatMemberVarNamePrev = $strCatMemberVarName;
				$strCatMemberVarName = substr($strCatMemberVarName, 0, $intPos);
				//En este caso, agregará los siguientes 10 caracteres a partir del espacio
				$strCatMemberVarName .= trim(substr($strCatMemberVarNamePrev, $intPos +1, 10));
			}
			//@JAPR
		}
		else {
			$strCatMemberVarName = substr($strCatMemberVarName, 0, 10);
		}
		
		if ($strCatMemberVarName == '') {
			$strCatMemberVarName = 'Member'.$this->MemberOrder;
		}
		
		$strCatMemberVarName = "@Attr".str_replace("'", '', $strCatMemberVarName);
		
		return $strCatMemberVarName;
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Dado un ID de atributo, regresa el identificador interno con el que se graba el filterText de los filtros dinámicos por encuesta, que no es
	//el mismo nombre de variable que el usuario utilizará para definirlo
	function getAttribIDVariableName() {
		return '@CMID'.$this->MemberID;
	}
	
	//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
	//Obtiene la lista de atributos del catálogo actual excluyendo al propio atributo de esta instancia
	//@JAPR 2012-08-06: Agregada la opción de ninguno con el parámetro $includeNone
	//@JAPRDescontinuada en v6, sólo se usaba para los filtros de catálogo originales
	function getRemainingCatalogAttributes($includeNone = false)
	{
		$allValues = array();
		//@JAPR 2012-08-06: Agregada la opción de ninguno con el parámetro $includeNone
		if($includeNone==true)
		{
			$allValues[0] = translate("None");
		}
		//@JAPR
		
		$sql = "SELECT MemberID, MemberName 
			FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->CatalogID." AND MemberID <> ".$this->MemberID." 
			ORDER BY MemberOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$memberID = (int)$aRS->fields["memberid"];
			$memberName = $aRS->fields["membername"];
			$allValues[$memberID] = $memberName;			
			$aRS->MoveNext();
		}
		
		return $allValues;
	}
	//@JAPR
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
 		<script language="JavaScript">
		
		function getRemainingCatalogAtributes () {

		}		
		
		function verifyFieldsAndSave(target)
 		{
 			var strBlanksField = "";

			if(Trim(BITAMCatalogMember_SaveForm.MemberName.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Member Name"))?>';
			}
			
 			//En esta parte se verifica si el AttributeName ha sido modificado por valores validos
			var attributeNameOld = '<?=$this->MemberName?>';
			var memberID = <?=$this->MemberID?>;
			var attributeName = BITAMCatalogMember_SaveForm.MemberName.value;

			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}

			xmlObj.open('POST', 'verifyAttributeName.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID='+memberID+'&AttributeName='+encodeURIComponent(attributeName)+'&AttributeNameOld='+encodeURIComponent(attributeNameOld));

	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}

	 		strResponseData = Trim(unescape(xmlObj.responseText));

	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}

	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strBlanksField += '\n- ' + '<?=translate('Error found during data validation')?>: ' + strErrMsg;
	 		}

			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{
  				BITAMCatalogMember_Ok(target);
  			}
 		}
 		
 		function showHideOptions() {
 			var intNumOptions = 0;
			var objCmb = BITAMCatalogMember_SaveForm.eBavelFieldDataID;
			if (objCmb) {
	 			intNumOptions = parseInt(objCmb.options.length);
				objCmb.style.display = (intNumOptions && intNumOptions > 1)?((bIsIE)?"inline":"table-row"):"none";
			}
 		}
 		</script>
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMCatalogMember_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMCatalogMember_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMCatalogMember_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?
	}
	
	//@JAPRDescontinuada 2015-07-18: No se encontró ninguna referencia a esta función
	static function getCatalogIDByMemberID($aRepository, $aMemberID)
	{
		$aCatalogID = -1;
		
		$sql = "SELECT t1.CatalogID FROM SI_SV_CatalogMember t1 WHERE t1.MemberID = ".$aMemberID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$aCatalogID = (int)$aRS->fields["catalogid"];
		}
		
		return $aCatalogID;
	}
	
	//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
	//@JAPR 2014-10-28: Agregado el mapeo de Scores
	//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
	/* Regresa la colección de atributos indexada por su catálogo. Esta función se encuentra definida en múltiples clases, en el futuro se deberá
	optar por utilizar directamente esta versión ya que es lo correcto pues esta es la clase de Atributos que es lo que se está pidiendo cargar
	El parámetro $anArrayOfCatalogIDs representa la colección de catálogos que se deben filtrar, si no se recibe se asumirá que se obtendrán los atributos de
	todos los catálogos existentes
	*/
	static function GetCatMembersByCatalogID($aRepository, $anArrayOfCatalogIDs = null)
	{
		$filter = "";
		if (!is_null($anArrayOfCatalogIDs))
		{
			switch (count($anArrayOfCatalogIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " t1.CatalogID = ".((int)$anArrayOfCatalogIDs[0]);
					break;
				default:
					foreach ($anArrayOfCatalogIDs as $aCatalogID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aCatalogID; 
					}
					if ($filter != "")
					{
						$filter = " t1.CatalogID IN (".$filter.")";
					}
					break;
			}
		}
		if ($filter != '') {
			$filter = " WHERE ".$filter;
		}
		
		$sql = "SELECT t1.CatalogID, t1.MemberID, t1.MemberName, t1.MemberOrder 
			FROM SI_SV_CatalogMember t1 
			{$filter} 
			ORDER BY t1.CatalogID, t1.MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["memberid"]] = $aRS->fields["membername"];
  				$aRS->MoveNext();
			} while (!$aRS->EOF);
			$allValues[$lastID] = $aGroup;
		}
		
		return $allValues;
	}
	
	//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
	//Obtiene la posición actual del atributo de catálogo especificado
	static function getCurrentOrderPos($aRepository, $aMemberID)
	{
		$sql = "SELECT MemberOrder FROM SI_SV_CatalogMember WHERE MemberID = {$aMemberID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$orderPos = -1;
		if(!$aRS->EOF)
		{
			$orderPos = (int) @$aRS->fields["memberorder"];
		}
		
		return $orderPos;
	}
	//@JAPR
}

class BITAMCatalogMemberCollection extends BITAMCollection
{
	public $CatalogID;

	function __construct($aRepository, $aCatalogID)
	{
		BITAMCollection::__construct($aRepository);
		$this->CatalogID = $aCatalogID;
		//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
		//$this->OrderPosField = "MemberOrder";
		//@JAPR
	}
	
	static function NewInstance($aRepository, $aCatalogID, $anArrayOfMemberIDs = null)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2015-08-02: Optimizada la carga de catálogos con el cache de objetos
		if (is_null($anArrayOfMemberIDs) || count($anArrayOfMemberIDs) <= 0) {
			$anInstance =& BITAMGlobalFormsInstance::GetCatMemberCollectionByCatalogWithID($aCatalogID);
			if (!is_null($anInstance)) {
				return $anInstance;
			}
		}
		//@JAPR
		
		$anInstance = new BITAMCatalogMemberCollection($aRepository, $aCatalogID);

		$filter = "";

		if (!is_null($anArrayOfMemberIDs))
		{
			switch (count($anArrayOfMemberIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.MemberID = ".((int)$anArrayOfMemberIDs[0]);
					break;
				default:
					foreach ($anArrayOfMemberIDs as $aMemberID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aMemberID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.MemberID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = '';
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', t1.IndDimID, t2.DissociateCatDimens';
		}
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$strAdditionalFields .= ', t1.eBavelFieldID';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.KeyOfAgenda, t1.AgendaLatitude, t1.AgendaLongitude, t1.AgendaDisplay';
		}
		//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		if (getMDVersion() >= esveBavelGeolocationAttribs) {
			$strAdditionalFields .= ', t1.eBavelFieldDataID';
		}
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$strAdditionalFields .= ', t1.IsImage';
		}
		//@JAPR 2015-07-18: Agregado el mapeo de datasources a catálogos
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', t1.DataSourceMemberID, t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas (campo DescriptorID)
		$sql = "SELECT t1.CatalogID, t2.CatalogName, t1.MemberID, t1.MemberName, t2.ParentID as ClaDescripParent, t1.ParentID as ClaDescrip, t1.MemberOrder, 
					t1.DescriptorID $strAdditionalFields 
				FROM SI_SV_CatalogMember t1, SI_SV_Catalog t2 
				WHERE t1.CatalogID = t2.CatalogID AND t1.CatalogID = ".$aCatalogID.$filter." ORDER BY t1.MemberOrder";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMCatalogMember::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}
		
		//@JAPR 2015-08-02: Optimizada la carga de catálogos con el cache de objetos
		if (is_null($anArrayOfMemberIDs) || count($anArrayOfMemberIDs) <= 0) {
			BITAMGlobalFormsInstance::AddCatMemberCollectionByCatalogWithID($aCatalogID, $anInstance);
		}
		//@JAPR
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("CatalogID", $aHTTPRequest->GET))
		{
			$aCatalogID = (int) $aHTTPRequest->GET["CatalogID"];
		}
		else
		{
			$aCatalogID = 0;
		}
		
		//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
		if(array_key_exists("MemberOrder", $aHTTPRequest->POST))
		{
			$orderPos = (int)$aHTTPRequest->POST["MemberOrder"];
			$aMemberID = (int)$aHTTPRequest->POST["MemberID"];
			BITAMCatalogMemberCollection::Reorder($aRepository, $aCatalogID, $aMemberID, $orderPos);
			$anInstance = BITAMCatalogMember::NewInstance($aRepository, $aCatalogID);
			$anInstance = $anInstance->get_Parent();
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		//@JAPR
		
		return BITAMCatalogMemberCollection::NewInstance($aRepository, $aCatalogID);
	}
		
	//@JAPR 2014-04-30: Agregado el reordenamiento de atributos de los catálogos
	static function Reorder($aRepository, $aCatalogID, $aMemberID, $orderPos)
	{
 		$prevOrderPos = BITAMCatalogMember::getCurrentOrderPos($aRepository, $aMemberID);
 		if ($orderPos > $prevOrderPos)
		{
			$sql = "UPDATE SI_SV_CatalogMember SET MemberOrder = MemberOrder - 1 
					WHERE MemberOrder BETWEEN ".$prevOrderPos." AND ".$orderPos.' AND CatalogID = '.$aCatalogID;
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if ($prevOrderPos > $orderPos)
		{
			$sql = "UPDATE SI_SV_CatalogMember SET MemberOrder = MemberOrder + 1 
					WHERE MemberOrder BETWEEN ".$orderPos." AND ".$prevOrderPos.' AND CatalogID = '.$aCatalogID;
			if ($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$sql = "UPDATE SI_SV_CatalogMember SET MemberOrder = ".$orderPos." WHERE MemberID = ".$aMemberID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Realizamos actualizacion de LastDate en las encuestas relacionadas con este catalogo
		//@JAPR 2015-07-21: Sólo realiza este proceso si el catálogo no fuera de la v6 o posterior
		//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
		//BITAMCatalog::updateLastUserAndDateInCatalog($aRepository, $aCatalogID);
		
		return;
	}
	//@JAPR
	
	function get_Parent()
	{
		return BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	}

	function get_Title()
	{
		return translate("Attributes");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=CatalogMemberCollection&CatalogID=".$this->CatalogID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=CatalogMember&CatalogID=".$this->CatalogID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MemberID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MemberName";
		$aField->Title = translate("Attribute");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-11-19: Agregados campos adicionales a la lista para facilitar la identificación de elementos
		$optionsGral = array();
		$optionsGral[0] = "";
		$optionsGral[1] = translate("Yes");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "KeyOfAgenda";
		$aField->Title = translate("Key of Agenda");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AgendaDisplay";
		$aField->Title = translate("Display of Agenda");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		if (getMDVersion() >= esvCatalogAttributeImage) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "IsImage";
			$aField->Title = translate("Image");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $optionsGral;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
		if (getMDVersion() >= esveBavelCatalogs) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
			
			require_once('eBavelIntegration.inc.php');
            /*@AAL 19/03/2015: Modificado para soportar formas y vistas de eBavel */
      		if (getMDVersion() >= esvRedesign) {
	            //Si hemos seleccionado una Forma entonces pedimos sus atributos
	            if($objCatalog->eBavelFormType == ebftForms)
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Forms');        
	            else//Si hemos seleccionado una Vista entonces solicitamos sus atributos.
	                $arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, 'Views');
      		}//@AAL
      		else
      			$arrEBavelFormsFields = GetMappedEBavelFieldsForms($this->Repository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID), null, false, '');           
            
			$arrAllEBavelFormsFields = array(0 => '('.translate("None").')');
			//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
			$arrAllEBavelFormsFieldsData = array(0 => array(0 => '('.translate("None").')'));
			//@JAPR
			foreach ($arrEBavelFormsFields as $intQTypeID => $arrFieldsColl) {
				foreach ($arrFieldsColl as $intFieldID => $arrFieldData) {
					if ($intFieldID > 0) {
						$arrAllEBavelFormsFields[$intFieldID] = $arrFieldData['label'];
						
						//@JAPR 2014-11-12: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
						$inteBavelQTypeID = convertEBavelToEFormsFieldType($arrFieldData['tagname']);
						$arrAllEBavelFormsFieldsData[$intFieldID] = array(ebfdValue => translate("Value"));
						switch ($inteBavelQTypeID) {
							case qtpLocation:
								//En los campos tipo Geolocalización, se agregarán las opciones para utilizar el valor, la latitud o la longitud
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLatitude] = translate("Latitude");
								$arrAllEBavelFormsFieldsData[$intFieldID][ebfdLongitude] = translate("Longitude");
								break;
							default:
								//El resto de los campos sólo agrega el valor para forzar a que sea la única opción posible a usar
								break;
						}
						//@JAPR
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("eBavel form field");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrAllEBavelFormsFields;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->get_Parent()->canRemove($aUser);
	}
}
?>