<?php
global $ML;
$ML = Array();
$ML[0]='January';
$ML[1]='February';
$ML[2]='March';
$ML[3]='April';
$ML[4]='May';
$ML[5]='June';
$ML[6]='July';
$ML[7]='August';
$ML[8]='September';
$ML[9]='October';
$ML[10]='November';
$ML[11]='December';
$ML[12]='Jan';
$ML[13]='Feb';
$ML[14]='Mar';
$ML[15]='Apr';
$ML[16]='May';
$ML[17]='Jun';
$ML[18]='Jul';
$ML[19]='Aug';
$ML[20]='Sep';
$ML[21]='Oct';
$ML[22]='Nov';
$ML[23]='Dec';
$ML[24]='Sunday';
$ML[25]='Monday';
$ML[26]='Tuesday';
$ML[27]='Wednesday';
$ML[28]='Thursday';
$ML[29]='Friday';
$ML[30]='Saturday';
$ML[31]='Sun';
$ML[32]='Mon';
$ML[33]='Tue';
$ML[34]='Wed';
$ML[35]='Thu';
$ML[36]='Fri';
$ML[37]='Sat';
$ML[38]='All';
$ML[39]='Except';
$ML[40]='None';
$ML[41]='hundreds';
$ML[42]='hundreds of thousands';
$ML[43]='tens';
$ML[44]='tens of thousands';
$ML[45]='thousands';
$ML[46]='Millions';
$ML[120]='(Customized)';
$ML['LBL_USER']='User';
$ML['LBL_GROUP']='Group';
$ML['LBL_ADD']='Add to the List';
$ML['LBL_DEL']='Delete from the List';
$ML['Minimum']='Minimum';
$ML['Maximum']='Maximum';
$ML['Average']='Average';
$ML['Status']='Status';
$ML['Comparative']='Variation';
$ML['Previous_Period']='Previous Periods';
$ML['Previous_Year']='Previous Year';
$ML['Acumulated']='Accum';
$ML['Ranking']='Ranking';
$ML['Asc']='Asc';
$ML['Desc']='Desc';
$ML['Count']='Count';
$ML['Target']='Target';
$ML['Start']='Start';
$ML['Monthly']='Monthly';
$ML['Total']='Total';
$ML['Increase']='Increase';
$ML['Delta']='Delta';
$ML['Actual_Period']='Current Period';
$ML['KPI']='Indicators';
$ML['Value']='Value';
$ML['Default']='Default';
$ML['Personalized']='Customized';
$ML['sidebyside']='Side by Side';
$ML['stacked']='Stacked';
$ML['stacked_100']='Stacked 100%';
$ML['none']='None';
$ML['horizontal']='Horizontal';
$ML['vertical']='Vertical';
$ML['both']='Both';

$ML['line']='Line';
$ML['area']='Area';
$ML['curve']='Curve';
$ML['areacurve']='Area-Curve';
$ML['step']='Step';
$ML['Menu']='Menu';

$ML['bubble']='Bubble';
$ML['scatter']='Scatter';
$ML['bar']='Bar';
$ML['gantt']='Gantt';
$ML['cube']='Cube';

$ML['candlestick']='Candlestick';
$ML['ohlc']='Open-High-Low-Close';
$ML['hlc']='High-Low-Close';
$ML['pie']='Pie';
$ML['doughnut']='Doughnut';

$ML['pareto']='80/20 Rule';
$ML['pyramid']='Pyramid';
$ML['radar']='Radar';
$ML['cs']='Contour/Spectral';
$ML['surface']='Surface';

$ML['square']='Square';
$ML['circle']='Circle';
$ML['triangulo']='Triangle';
$ML['rombo']='Diamond';
$ML['line_hor']='Hor Line';
$ML['line_ver']='Ver Line';
$ML['both_lines']='Cross';
$ML['triangulo_inv']='Inverse Triangle';
$ML['square2']='Double Square';
$ML['shape']='Various';

$ML['font']='Font';
$ML['top']='Top';
$ML['right']='Right';
$ML['bottom']='Bottom';
$ML['left']='Left';
$ML['mult']='Mult';
$ML['suma']='Sum';

$ML['Column_Filter'] = array(0=>'None',
							 1=>'Above or equal (>=)',
							 2=>'Above (>)',
							 3=>'Equal (=)',
							 4=>'Below (<)',
							 5=>'Below or Equal (<=)',
							 6=>'Different (!=)',
							 7=>'First',
							 8=>'Between');

$ML['pareto_reverse']='80/20 Rule (reverse)';
$ML['Higher_Avg']='Higher than Average';
$ML['Lower_Avg']='Lower than Average';
$ML['First']='First';

$ML['Filter'] = 'Filter';

$ML['MinMob'] = 'Dynamic Minimum';
$ML['MaxMob'] = 'Dynamic Maximum';
$ML['AveMob'] = 'Moving Averages';

$ML['Accumulated_Period'] = 'Accumulated period';
$ML['Ac_Per_Pre_Year']='Accumulated period of previous year';

$ML['Linear_Regresion']='Linear';

$ML['trend']='Trend';
$ML['trend_qualifiers']='Trend with qualifers';
$ML['linear_ind_qualif']='Linear indicator qualifiers';
$ML['radar_ind_qualif']='Radar indicator qualifiers';

$ML['DATANOTFOUND'] = 'Data not found';
$ML['ACCOMPLISHMENT'] = 'Accomplishment';

$ML['Median']='Median';
$ML['StandardDev']='Standard Deviation';

$ML['NOT_EXIST_ESC_DEF'] = 'There is no default dashboard';
$ML['DATANOTSET'] = 'Data not set';
$ML['OK']='Ok';
$ML['CANCEL']='Cancel';
$ML['PERIOD']='Period';

$ML['CHOOSE_COLOR']='Choose Color';
$ML['DEF_CUST_COLOR'] = 'Define Custom Colors';
$ML['ADD_CUST_COLOR'] = 'Add Custom Color';
$ML['PRINTBOOK'] = 'Binder Management';

$ML['from'] = 'From';
$ML['to'] = 'To';
$ML['increase']='Increment';

$ML['others']='others';
$ML['MoveToStage']='Go to Dashboard';
$ML['ShowEach']='Show labels each';

$ML['or'] = 'or';
$ML['and'] = 'and';

$ML['AoAve'] = 'Avg';
$ML['AdvErr1'] = 'Project name _AWSREPOSITORY_ does not match ODBC name _AWSWWWROOT_';
$ML['AdvErr2'] = 'Export user\'s metadata to php';
$ML['AdvErr3'] = 'Export dashboard to php';
$ML['AdvErr4'] = 'The dashboard [#] is configured not to generate cache. Consult Artus Designer and the Web repository Configurator.';
$ML['INCR']='Increase';
$ML['DECR']='Decrease';
$ML['ALARM']='Alarms';

$ML['All Periods'] = 'All Periods';
$ML['solid']       = 'Solid line';
$ML['dot']         = 'Dotted line';
$ML['dash']        = 'Dashed Line';
$ML['dashdot']     = 'Dot-Dashed Line';
$ML['dashdotdot']  = 'Dot-Dot-Dashed Line';

$ML['bpmProjectProperties']='Project properties';
$ML['bpmTaskProperties']='Task properties';
$ML['bpmAddTasks']='Add tasks';
$ML['bpmNewProject']='New project';
$ML['bpmProjectName']='Project name';
$ML['bpmDescription']='Description';
$ML['bpmResponsible']='Responsible';
$ML['bpmSave']='Save';
$ML['bpmCancel']='Cancel';
$ML['bpmNext']='Next';
$ML['bpmFinish']='Finish';
$ML['bpmStartDate']='Start date';
$ML['bpmDueDate']='Due date';
$ML['bpmRealStartDate']='Real start date';
$ML['bpmRealFinalDate']='Real final date';
$ML['bpmProject']='Project';
$ML['bpmTaskName']='Task name';
$ML['bpmSubtaskOf']='Sub-task of';
$ML['bpmNone']='None';
$ML['bpmDaily']='Daily';
$ML['bpmWeekly']='Weekly';
$ML['bpmMonthly']='Monthly';
$ML['bpmView']='View';
$ML['bpmFrom']='From';
$ML['bpmTo']='To';
$ML['bpmRefresh']='Refresh';
$ML['bpmDelSelTasks']='Delete selected tasks';
$ML['bpmResp']='Resp';
$ML['bpmTasksStatusLegend']='Tasks status legend';
$ML['bpmDelayed']='Delayed';
$ML['bpmStartedAndOnSchedule']='Started and on schedule';
$ML['bpmOnTimeOrAheadSchedule']='Not yet started and on time';
$ML['bpmFinished']='Finished';
$ML['bpmShowCalendar']='Show calendar';
$ML['bpmProjectAssigned']='Artus: a new project has been assigned to you';
$ML['bpmTaskAssigned']='Artus: a new task has been assigned to you';
$ML['bpmSendMailToResp']='Send e-mail notification to assigned responsible';
$ML['bpmIndicator']='Indicator';
$ML['bpmGeneral']='General';
$ML['bpmUsers']='Users';
$ML['bpmRightsLegend']='Permission legend';
$ML['bpmCanSeeProject']='The user can see this project without having any responsability on it';
$ML['bpmCanSeeProjectResp']='The user can see this project because he is author and/or responsible of it or any of its tasks';
$ML['bpmCantSeeProject']='The user is not allowed to see this project';
$ML['bpmIndsState']='Indicatos state';
$ML['bpmIncludeInd']='Include the indicator in the chart';
$ML['bpmLinkedInd']='Indicator linked to the project';
$ML['bpmIndNotIncluded']='Do not include this indicator';
$ML['bpmErrorTryingToExec']='There was an error while trying to execute';
$ML['bpmErrorCode']='Error code';
$ML['bpmErrorDescription']='Error description';
$ML['bpmMakeSureProperTableStruct']='Please make sure you have the proper table structure of';
$ML['bpmNotInstalled']='Not installed';
$ML['bpmInstalled']='Installed';
$ML['bpmInstall']='Install';
$ML['bpmInstallingAddIn']='Installing Add-in';
$ML['bpmAddInNotInstalled']='Add-in not installed';
$ML['bpmInstallationComplete']='Installation complete';
$ML['bpmAddInInstallation']='Add-In installation';
$ML['bpmState']='State';
$ML['bpmNewVersionAtServer']='A new version has been found in the server';
$ML['bpmRestartWindows']='Waiting for MS Windows (R) to be restarted';
$ML['bpmOutlookAddIn']='Add-in for MS Outlook	(R)';
$ML['bpmTasksSynchronizer']='Tasks synchronization';
$ML['bpmClose']='Close';

$ML['Ranges'] = 'Ranges';
$ML['DatosExcMaxGraficar']='Data exceed Maximum to Chart';
$ML['New'] = 'New';
$ML['Open'] = 'Open';
$ML['Del'] = 'Delete';
$ML['Lower'] = 'Lower than';
$ML['Higher_Equal'] = 'Higher or equal than';
$ML['TemplateNotSupp'] = 'At the moment the analysis template is not supported';

$ML['EAS'] = 'Go to the Enterprise Analysis';
$ML['PrevStage']='Go to Dashboard - Previous';
$ML['PapRep']='Papiro Report';
$ML['Print']='Print';
$ML['NewEAS'] = 'New Enterprise Analysis';
$ML['ShowHide']='Hide/Show Component';
$ML['ColAna']='Collaborative Analysis';
$ML['Tab']='Tab';
$ML['BookPrint']='Binder Management';
$ML['ApplyFilter'] = 'Apply Filters';
$ML['ResetFilter'] = 'Reset Filters';
$ML['Logout']='Exit';
$ML['DefaultStage']='Go to Dashboard - Default';
$ML['PrevPeriod']='Previous Period';
$ML['Stages']='Dashboards';
$ML['ExecAplic']='Run application';
$ML['CapIndWithDim'] = 'Data Entry - With Dimensions';
$ML['CapIndWiOutDim'] = 'Data Entry - Without Dimensions';

$ML['Dynamic_Table'] = 'Dynamic Table';
$ML['Table'] = 'Table';
$ML['Plus_Cell'] = 'Plus Cell';
$ML['Cell'] = 'Cell';
$ML['Chart'] = 'Chart';
$ML['Progress_Bar'] = 'Progress Bar';
$ML['Thermometer'] = 'Thermometer';
$ML['Gauge'] = 'Speedometer';
$ML['Banner'] = 'Banner';
$ML['Map'] = 'Map';
$ML['Query'] = 'Query';
$ML['Data'] = 'Data';
$ML['All'] = 'All';
$ML['Stage']='Dashboard';
$ML['NODESCR'] = 'No description';
$ML['Parameter'] = 'Parameter';
$ML['Learning']='Learning';
$ML['NPC']='Authorization of the New Possible Cause';
$ML['CAUSE']='Possible Cause';
$ML['AUT_CAUSE']='Authorize Cause';
$ML['REJ_CAUSE']='Reject Cause';
$ML['AUTHORIZED']='Authorized';
$ML['REJECTED']='Rejected';
$ML['SO']='Operation completed successfully';
$ML['DataNoReceive']='No data received';
$ML['RepoNotFound']='Repository not found';
$ML['NoConnect']='Could not establish connection to database';
$ML['NOPROJ']='Web project not identified. Please verify with your Administrator.';
$ML['StartAni']='Start';
$ML['StopAni']='Stop';
$ML['PauseAni']='Pause';
$ML['ErrIniData']='The maximum ranking of consecutives of the Artus Project has been reached';

$ML['MailTest_Subject'] = 'Mail test';
$ML['MailTest_SuccessConfig'] = 'Artus mail configuration has been successful';
$ML['MailTest_MailSent'] = 'The mail has been sent. Please verify the message in the target account';
$ML['MultAna']='Multidimensional analysis';

$ML[549]='The pivot period must be the same as the table period';
$ML[550]='The operand is not applicable to these indicators';
$ML[551]='Dimension(s) not accessible';
$ML[552]='The dimension(s) is(are) already in the table';
$ML[553]='The Indicator(s) is(are) not valid';
$ML[557]='There is a period type column in the table already. It is impossible to apply All Periods.';
$ML[1000]='It is not possible to add All Periods to a Chronological Table.';
$ML[1001]='It is not possible to Duplicate neither as Variation nor Percentage in Pivot Tables.';
$ML[1002]='It is not possible to Show/Duplicate as Variation in Chronological and Pivot Tables with the Show Percentage Values option enabled.';
$ML[1003]='To apply visibility to Chronological Tables it is necessary to have the Show Alarms option enabled.';
$ML[1004]='Only the Alarm\'s Visibility may be applied to Chronological Tables.';
$ML[1005]='Impossible to add indicators in rows when the table has been defined with indicators in columns.';

$ML['Cut']='Cut';
$ML['AplicarTodos']='Apply all';
$ML['De']=' out of ';
$ML['SOURCE']='Source';
$ML['ONLINE_IND']='Online Indicators';
$ML['SECONDS']='Seconds';
$ML['COMMS']='Comments';
$ML['DE_CI']='From';
$ML['Previous']='Previous';
$ML['Next']='Next';
$ML['KPI_Value'] = 'KPI Value';
$ML['KPI_Goal'] = 'KPI Target';
$ML['KPI_Status'] = 'KPI Status';
$ML['KPI_Trend'] = 'KPI Trend';
$ML['KPI_Weight'] = 'KPI Weight';

// velocimeter props screen strings
$ML['Properties'] = 'Properties';
$ML['Maximums and minimums'] = 'Maximums and minimums';
$ML['Set first indicator as 100%'] = 'Set first indicator as 100%';
$ML['Reverse'] = 'Reverse';
$ML['Minimum value of indicator'] = 'Minimum value of indicator';
$ML['Maximum value of indicator'] = 'Maximum value of indicator';
$ML['Minimum'] = 'Minimum';
$ML['Maximum'] = 'Maximum';
$ML['Show labels'] = 'Show labels';
$ML['Tick marks'] = 'Tick marks';
$ML['Color'] = 'Color';
$ML['Alarms'] = 'Alarms';
$ML['Use alarms background color'] = 'Use alarms background color';
$ML['Use alarms font color'] = 'Use alarms font color';
$ML['Show alarms'] = 'Show alarms';
$ML['3D'] = '3D';
$ML['Size'] = 'Size';
$ML['Needle'] = 'Needle';
$ML['Width'] = 'Width';
$ML['Reset'] = 'Reset';
$ML['Speedometer'] = 'Speedometer';
$ML['Decrement percentage'] = 'Decrement percentage';
$ML['Increment percentage'] = 'Increment percentage';
$ML['Automatic scale'] = 'Automatic scale';
$ML['Round'] = 'Round';
$ML['Restore'] = 'Restore';
$ML['Stripe'] = 'Stripe';
$ML['Background color'] = 'Background color';
$ML['NotPublished'] = 'The dashboard has not been published for this user.';

$ML['ExpDash'] = 'Export Dashboard to ';
$ML['Date'] = 'Date';
$ML['Hour'] = 'Hour';
$ML['Commentary'] = 'Comment';
$ML['FormatDT'] = 'n/j/Y\|g:i:s A';
$ML['ExportPPT'] = 'Export dashboards to slide format';
$ML['CompanyName'] = 'Company Name';
$ML['RepTitle'] = 'Report Title';
$ML['PageNo'] = 'Page Number';
$ML['Apply'] = 'Apply';
$ML['Select Params'] = 'Select parameters';
$ML['INITW'] = 'Starts with';
$ML['CONTENT'] = 'Contain';

$ML['Current']='Current';
$ML['CurrentRel']='Current relative';

$ML['moldura3d']   ='3D Frame';
$ML['molduraMath'] ='Math Frame';
$ML['molduraPlana']='Flat Frame';
$ML['NoDatos']='No data available';
$ML['NoSerie']='No series to draw';

$ML['Section']='Section';
$ML['Image']='Image';
$ML['SelAll']='Select all';
$ML['SelNone']='Select none';

$ML['Operando']='Operand';
$ML['Opdivisor']='Divisor Operand';

$ML['gradiante']='Gradient';
$ML['suave']='Soft';
$ML['vidrio']='Glass';

$ML['Error'] = 'Error';
$ML['ErrConn'] = 'Connection Error';
$ML['ErrAccEKTComm'] = 'Accesing SI_ETK_Comments Table';
$ML['ErrCommContentOR'] = 'It is not possible to modify comments when there is more than one dimension value selected.';
$ML['ErrExecute'] = 'Executing';
$ML['Folder']='Folder';
$ML['SideLayout']='Side Layout';
$ML['CircularLayout']='Circular Layout';
$ML['Text']='Text';
$ML['TextLine']='Text Line';
$ML['TextBox']='Text Box';
$ML['TextBoxLine']='Text Box Line';
$ML['incremental']='Incremental';
$ML['email']='Mail';
$ML['password']='Password';
$ML['recover_pass']='Restore Password';
$ML['advance']='Progress';
$ML['data_load']='Data loading in progress, please wait';
$ML['AdminEmp']='Enterprise Manager';

$ML['DesplazaPer']='Time scroll';
$ML['RangTotal']='Total range';
$ML['RangMostIni']='Range to show initially';
$ML['DespZoom']='Zoom scroll';
$ML['ElemMos']='Elements to show in scroll';
$ML['MostEtiqDosLin']='Show labels in two rows';
$ML['Centered']='Centered';

$ML['by'] = 'by';
$ML['Button'] = 'Button';
$ML['Section'] = 'Section';
?>