<?php 

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");

class BITAMQuestionOptionCol extends BITAMObject
{
	public $QuestionID;
	public $QuestionOptionID;
	public $QuestionOptionName;
	public $QuestionOptionNameBack;
	public $SortOrder;
	public $IndDimID;
	public $IndicatorID;
	public $QTypeID;
	public $Score;
	public $ScoreOperation;

	function __construct($aRepository, $aQuestionID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
		$this->QuestionOptionID = -1;
		$this->QuestionOptionName = "";
		$this->QuestionOptionNameBack = "";
		$this->SortOrder = 0;
		$this->IndDimID = 0;
		$this->IndicatorID = 0;
		
		//Obtenemos instancia de la pregunta para determinar que tipo de pregunta es
		$mainQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $this->QuestionID);
		$this->QTypeID = $mainQuestion->QTypeID;
		$this->Score = "";
		$this->ScoreOperation = 0;
	}

	static function NewInstance($aRepository, $aQuestionID)
	{
		return new BITAMQuestionOptionCol($aRepository, $aQuestionID);
	}

	static function NewInstanceWithID($aRepository, $aQuestionOptionID)
	{
		$anInstance = null;
		
		if (((int) $aQuestionOptionID) <= 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT t1.QuestionID, t1.ConsecutiveID, t1.DisplayText, t1.SortOrder, t1.IndDimID, t1.IndicatorID, 
				t1.Score, t1.ScoreOperation, t2.QTypeID 
				FROM SI_SV_QAnswersCol t1, SI_SV_Question t2 
				WHERE t1.ConsecutiveID = ".$aQuestionOptionID." AND t1.QuestionID = t2.QuestionID";

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$anInstance = BITAMQuestionOptionCol::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aQuestionID = (int)$aRS->fields["questionid"];
		
		$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
		
		$anInstance->QuestionOptionID = (int)$aRS->fields["consecutiveid"];
		$anInstance->QuestionOptionName = $aRS->fields["displaytext"];
		$anInstance->QuestionOptionNameBack = $aRS->fields["displaytext"];
		$anInstance->SortOrder = (int)$aRS->fields["sortorder"];
		$anInstance->IndDimID = (int)$aRS->fields["inddimid"];
		$anInstance->IndicatorID = (int)$aRS->fields["indicatorid"];
		$anInstance->QTypeID = (int)$aRS->fields["qtypeid"];
		$anInstance->Score = $aRS->fields["score"];
		$anInstance->ScoreOperation = (int)$aRS->fields["scoreoperation"];
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}

		if (array_key_exists("QuestionOptionID", $aHTTPRequest->POST))
		{
			$aQuestionOptionID = $aHTTPRequest->POST["QuestionOptionID"];
			
			if (is_array($aQuestionOptionID))
			{
				$aCollection = BITAMQuestionOptionColCollection::NewInstance($aRepository, $aQuestionID, $aQuestionOptionID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}

				$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMQuestionOptionCol::NewInstanceWithID($aRepository, (int)$aQuestionOptionID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("QuestionOptionID", $aHTTPRequest->GET))
		{
			$aQuestionOptionID = $aHTTPRequest->GET["QuestionOptionID"];
			$anInstance = BITAMQuestionOptionCol::NewInstanceWithID($aRepository, (int)$aQuestionOptionID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
			}
		}
		else
		{
			$anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("QuestionOptionID", $anArray))
		{
			$this->QuestionOptionID = (int)$anArray["QuestionOptionID"];
		}

		if (array_key_exists("QuestionOptionName", $anArray))
		{
			$this->QuestionOptionName = trim($anArray["QuestionOptionName"]);
		}

		if (array_key_exists("Score", $anArray))
		{
			$this->Score = trim($anArray["Score"]);
		}

		if (array_key_exists("ScoreOperation", $anArray))
		{
			$this->ScoreOperation = trim($anArray["ScoreOperation"]);
		}

		return $this;
	}
	
	function save()
	{
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		//$gblModMngrErrorMessage = '';
		
		if($this->QTypeID==2)
		{
			$this->saveForSimpleChoice();
		}
		else if($this->QTypeID==3)
		{
			$this->saveForMultipleChoice();
		}
		
		//@JAPR 2013-03-21: Agregado el control de errores del Model Manager
		if (isset($gblModMngrErrorMessage) && !is_null($gblModMngrErrorMessage) && trim($gblModMngrErrorMessage) != '') {
			die("(".__METHOD__.") ".translate("There were some errors while saving the question option").":\r\n".$gblModMngrErrorMessage.". ");
		}
		//@JAPR
		
		return $this;
	}
	
	function saveForMultipleChoice()
	{
		$currentDate = date("Y-m-d H:i:s");

		if ($this->isNewObject())
		{
			//Generamos instancia de Question
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

			
			//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
			$arraySelectOptionsCol = array();
			
			//Validamos que las opciones sean unicas
			if(trim($mainQuestion->StrSelectOptionsCol)!="")
			{
				//Modificamos el atributo ->StrSelectOptionsCol agregandole el nuevo si realmente no existe dicha opcion
				$arraySelectOptionsCol = explode("\r\n", $mainQuestion->StrSelectOptionsCol);
			}
			
			$arraySelectOptionsColFlip = array_flip($arraySelectOptionsCol);
			
			$optionName = $this->QuestionOptionName;
			if(isset($arraySelectOptionsColFlip[$optionName]))
			{
				//Si existe dicha opcion no hay nada que hacer
				return;
			}
			
			$arraySelectOptionsCol[] = $this->QuestionOptionName;
			$LN = chr(13).chr(10);
			$mainQuestion->StrSelectOptionsCol = implode($LN, $arraySelectOptionsCol);
			
			$sql = "UPDATE SI_SV_Question SET 
					OptionsTextCol = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptionsCol)." 
					WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Y guardamos las opciones en la tabla de SI_SV_QAnswersCol y en las tablas de la HandHeld
			$mainQuestion->saveSelectOptions();
			
			//Si la pregunta es multidimension entonces se procede a crear la dimension faltante para la nueva opcion
			//La funcion createSelectOptionDims afortunadamente detecta cuales de las opciones todavia no les han creado 
			//su dimension a la opcion por lo tanto no se crearan mas dimensiones de las necesarias solo para aquellas 
			//opciones que las necesiten, y automaticamente inserta los valores de No aplica, 0 y 1 y actualiza tabla de hechos
			if($mainQuestion->IsMultiDimension==1)
			{
				$mainQuestion->createSelectOptionDims();
			}
			
			//Obtenemos el ID de la opcion con la cual se inserto para establecer la propiedad QuestionOptionID
			$sql = "SELECT ConsecutiveID, SortOrder FROM SI_SV_QAnswersCol 
					WHERE QuestionID = ".$this->QuestionID." AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
			}

			$this->QuestionOptionID = (int)$aRS->fields["consecutiveid"];
			$this->SortOrder = (int)$aRS->fields["sortorder"];
			
			//Y Actualizamos el Score y ScoreOperation
			$sql = "UPDATE SI_SV_QAnswersCol SET 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score).", 
					ScoreOperation = ".$this->ScoreOperation." 
					WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;

			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		}
		else
		{
			$sql = "UPDATE SI_SV_QAnswersCol SET 
					DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).", 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score).", 
					ScoreOperation = ".$this->ScoreOperation." 
					WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->updateStrSelectOptions();

			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

			//Despues de actualizar dicha opcion se verifica si hubo un cambio en su descripcion si es asi es necesario 
			//actualizarlo en la tabla paralela de una pregunta de seleccion multiple, que no sea de catalogo
			if($mainQuestion->QTypeID==3 && $mainQuestion->UseCatToFillMC==0)
			{
				//Se verifica si hubo cambio en la descripcion de la opcion
				if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
				{
					//Modificamos tabla paralela siempre cuando la entrada de valores sea por checkbox 
					//y no sean campos de captura numericos y string
					if($mainQuestion->MCInputType==0)
					{
						$this->updateSVSurveyTable();
					}
					
					//Si la pregunta es de tipo MultiDimension entonces debera modificarse el nombre logico de dicha dimension
					if($mainQuestion->IsMultiDimension==1)
					{
						$this->updateOptionNomLogico();
						
						//Verificamos si la pregunta es de entrada numerica y si hay indicador creado para ella 
						//para hacer 
						if($mainQuestion->MCInputType==1 && $mainQuestion->IsIndicatorMC==1)
						{
							$this->updateOptionNomLogicoInd();
						}
					}

					//Actualizar datos de modificacion de la encuesta
					BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
				}
			}
		}
	}
	
	function updateOptionNomLogico()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		$strOriginalWD = getcwd();

		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $this->IndDimID);
		$dimensionName = "Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		$cla_descrip = $this->getDimClaDescripByDimName($dimensionName);
				
		if($cla_descrip!=-1)
		{
			$dimensionName = "SV".$mainQuestion->SurveyID."Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		}
				
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->save();
		chdir($strOriginalWD);
	}
	
	function updateOptionNomLogicoInd()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");

		$anInstanceOptionInd = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
		$indicatorName = "Q".$mainQuestion->QuestionNumber."-".$mainQuestion->AttributeName."-".$this->QuestionOptionName;
		
		$anInstanceOptionInd->IndicatorName = $indicatorName;
		$anInstanceOptionInd->formato = "#,##0";
		$anInstanceOptionInd->agrupador = "SUM";
		$anInstanceOptionInd->sql_source = "SUM";
		
		$anInstanceOptionInd->save();
		chdir($strOriginalWD);
	}
	
	function getDimClaDescripByDimName($dimensionName)
	{
		$cla_descrip = -1;

		$sql = "SELECT CLA_DESCRIP FROM SI_DESCRIP_ENC WHERE NOM_LOGICO = ".$this->Repository->ADOConnection->Quote($dimensionName);
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$cla_descrip = (int)$aRS->fields["cla_descrip"];
		}
		
		return $cla_descrip;
	}
	
	function updateSVSurveyTable()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	
		$tablaParalela = $mainQuestion->SurveyTable;
		$questionField = $mainQuestion->SurveyField;
		
		//Primero le agregamos un ; al inicio y final del valor del campo
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = CONCAT(".$this->Repository->DataADOConnection->Quote(";").", ".$questionField.", ".$this->Repository->DataADOConnection->Quote(";").")";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$strNewOption = ";".$this->QuestionOptionName.";";
		$strOldOption = ";".$this->QuestionOptionNameBack.";";

		//Reemplazamos el valor anterior por el valor nuevo
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = REPLACE(".$questionField.", ".$this->Repository->DataADOConnection->Quote($strOldOption).", ".$this->Repository->DataADOConnection->Quote($strNewOption).")";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Le quitamos el ; al inicio y al final del campo para dejarlo como antes
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = SUBSTRING(".$questionField.", 2, (CHAR_LENGTH(".$questionField.")-2))";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function saveForSimpleChoice()
	{
		$currentDate = date("Y-m-d H:i:s");
		if ($this->isNewObject())
		{
			//Generamos instancia de Question
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

			//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
			$this->QTypeID = $mainQuestion->QTypeID;
			
			$arraySelectOptionsCol = array();
			
			//Validamos que las opciones sean unicas
			if(trim($mainQuestion->StrSelectOptionsCol)!="")
			{
				//Modificamos el atributo ->StrSelectOptionsCol agregandole el nuevo
				$arraySelectOptionsCol = explode("\r\n", $mainQuestion->StrSelectOptionsCol);
			}

			$arraySelectOptionsCol[] = $this->QuestionOptionName;
			$LN = chr(13).chr(10);
			$mainQuestion->StrSelectOptionsCol = implode($LN, $arraySelectOptionsCol);
			//Validamos las opciones
			$mainQuestion->validateSelectOptions();
			
			$sql = "UPDATE SI_SV_Question SET 
					OptionsTextCol = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptionsCol)." 
					WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Y guardamos las opciones en la tabla de SI_SV_QAnswersCol y en las tablas de la HandHeld
			$mainQuestion->saveSelectOptions();
			
			//Despues de grabar en la tabla SI_SV_Question y SI_SV_QAnswersCol verificamos se realiza la insercion de los valores que faltan en la dimension
			$strOriginalWD = getcwd();
			
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/modeldimension.inc.php");

			$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
			chdir($strOriginalWD);

			//Se agregan los nuevos elementos a la dimension y se eliminan aquellos que ya no estan en uso
			$mainQuestion->updateMassiveDimensionValues($anInstanceDim);
			
			//Obtenemos el ID de la opcion con la cual se inserto para establecer la propiedad QuestionOptionID
			$sql = "SELECT ConsecutiveID, SortOrder FROM SI_SV_QAnswersCol 
					WHERE QuestionID = ".$this->QuestionID." AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
			}

			$this->QuestionOptionID = (int)$aRS->fields["consecutiveid"];
			$this->SortOrder = (int)$aRS->fields["sortorder"];
			
			//Y Actualizamos el Score
			$sql = "UPDATE SI_SV_QAnswersCol SET 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score).", 
					ScoreOperation = ".$this->ScoreOperation." 
					WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion simple
			if(trim($this->Score)!="")
			{
				if($mainQuestion->QDisplayMode <> dspMatrix)
				{
					//Y verificamos si para esta pregunta de selección simple no se ha creado el indicador anteriormente
					if(is_null($mainQuestion->IndicatorID) || $mainQuestion->IndicatorID==0)
					{
						$mainQuestion->createIndicatorKPI();
					}
				}
			}

			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		}
		else
		{
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
			{
				$arraySelectOptionsCol = array();
				
				//Validamos que las opciones sean unicas
				if(trim($mainQuestion->StrSelectOptionsCol)!="")
				{
					//Obtenemos las opciones para verificar si no se quiere editar la opcion 
					//con el nombre de otra opcion ya existente
					$arraySelectOptionsCol = explode("\r\n", $mainQuestion->StrSelectOptionsCol);
				}
				$arraySelectOptionsColFlip = array_flip($arraySelectOptionsCol);
				
				$key = $this->QuestionOptionName;
				
				//Si existe el valor entonces no debera ser modificado
				if(isset($arraySelectOptionsColFlip[$key]))
				{
					$this->QuestionOptionName = $this->QuestionOptionNameBack;
					return;
				}
			}
			
			$sql = "UPDATE SI_SV_QAnswersCol SET 
					DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName).", 
					Score = ".$this->Repository->DataADOConnection->Quote($this->Score).", 
					ScoreOperation = ".$this->ScoreOperation." 
					WHERE ConsecutiveID = ".$this->QuestionOptionID." AND QuestionID = ".$this->QuestionID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->updateStrSelectOptions();

			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			//Despues de actualizar dicha opcion se verifica si hubo un cambio en su descripcion
			//si es asi es necesario actualizarlo en el catalogo y en la tabla paralela siempre y cuando se trate
			//de una pregunta de seleccion simple que no utilice catalogo
			if($mainQuestion->QTypeID==2 && $mainQuestion->UseCatalog==0)
			{
				if($this->QuestionOptionName!=$this->QuestionOptionNameBack)
				{
					$strOriginalWD = getcwd();
					
					//@JAPR 2015-02-09: Agregado soporte para php 5.6
					//session_register("BITAM_UserID");
					//session_register("BITAM_UserName");
					//session_register("BITAM_RepositoryName");
					$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
					$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
					$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
					require_once("../model_manager/model.inc.php");
					require_once("../model_manager/modeldata.inc.php");
					require_once("../model_manager/modeldimension.inc.php");
				
					$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
					chdir($strOriginalWD);
					
					$this->updateDimensionValueInQuestion($anInstanceDim);
				}
			}

			//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion simple
			if(trim($this->Score)!="")
			{
				if($mainQuestion->QDisplayMode == dspMatrix)
				{
					//Y verificamos el valor de OneChoicePer para revisar ya sea en las respuestas
					//por renglon o por columna si tienen los indicadores creados
					$blnCreateIndicators = false;
					$arrayAnswerOptions = array();
					$strQIndDimIds = "";
			
					if($mainQuestion->OneChoicePer==0)
					{
						$arrayOptions = $mainQuestion->SelectOptions;
						$theQIndicatorIds = $mainQuestion->QIndicatorIds;
					}
					else 
					{
						$arrayOptions = $mainQuestion->SelectOptionsCol;
						$theQIndicatorIds = $mainQuestion->QIndicatorIdsCol;
					}

					//Recorremos las opciones para revisar los indicadores
					foreach ($arrayOptions as $optionKey=>$optionValue)
					{
						$indicatorid = 0;
						
						if(isset($theQIndicatorIds[$optionKey]))
						{
							$indicatorid = (int)$theQIndicatorIds[$optionKey];
						}
						else 
						{
							$indicatorid = 0;
						}
			
						if(is_null($indicatorid)||$indicatorid==0)
						{
							$blnCreateIndicators = true;
							break;
						}
					}

					if($blnCreateIndicators==true)
					{
						$mainQuestion->createMatrixDataIndicators();
					}
				}
			}

			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
		}
	}
	
	function updateStrSelectOptions()
	{
		$arrayOptions = array();
		$LN = chr(13).chr(10);

		$sql = "SELECT DisplayText FROM SI_SV_QAnswersCol WHERE QuestionID = ".$this->QuestionID." ORDER BY SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$arrayOptions[] = $aRS->fields["displaytext"];

			$aRS->MoveNext();
		}
		
		$strSelectOptionsCol = implode($LN, $arrayOptions);
		
		//Actualizamos StrSelectOptionsCol
		$sql = "UPDATE SI_SV_Question SET OptionsTextCol = ".$this->Repository->DataADOConnection->Quote($strSelectOptionsCol)." 
				WHERE QuestionID = ".$this->QuestionID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function updateDimensionValueInQuestion($anInstanceModelDim)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Se actualiza el valor de la dimension en el catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//Verificamos primero si la opcion no se encuentra ya en la tabla de dimension
		//si es asi no es necesaria agregarla en el catalogo porque ya existe
		$sqlExist = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		
		$aRSExist = $this->Repository->DataADOConnection->Execute($sqlExist);
		
		if ($aRSExist === false)
		{
			die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlExist);
		}
		
		//Si no existe dicho valor en el catalogo entonces si se procede a almacenar ese valor
		if($aRSExist->EOF)
		{
			$sql = "UPDATE ".$tableName." SET ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." 
					WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionNameBack);
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Se actualiza el valor de la dimension en la tabla paralela
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
		
		$tablaParalela = $mainQuestion->SurveyTable;
		$questionField = $mainQuestion->SurveyField;
		
		$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName)." 
				WHERE ".$questionField." = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionNameBack);

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function remove()
	{
		if($this->QTypeID==2)
		{
			$this->removeForSimpleChoice();
		}
		else if($this->QTypeID==3)
		{
			$this->removeForMultipleChoice();
		}

		return $this;
	}
	
	function removeForMultipleChoice()
	{
		//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$arraySelectOptionsCol = array();
		
		//Validamos que las opciones sean unicas
		if(trim($mainQuestion->StrSelectOptionsCol)!="")
		{
			//Modificamos el atributo ->StrSelectOptionsCol haciendo nulo aquel que se va a eliminar
			$arraySelectOptionsCol = explode("\r\n", $mainQuestion->StrSelectOptionsCol);
		}
		$arraySelectOptionsColFlip = array_flip($arraySelectOptionsCol);
		unset($arraySelectOptionsColFlip[$this->QuestionOptionName]);
		$arraySelectOptionsCol = array_flip($arraySelectOptionsColFlip);
		$arraySelectOptionsCol = array_values($arraySelectOptionsCol);
		
		//Obtenemos nuevo valor de ->StrSelectOptionsCol
		$LN = chr(13).chr(10);
		$mainQuestion->StrSelectOptionsCol = implode($LN, $arraySelectOptionsCol);
		
		$sql = "UPDATE SI_SV_Question SET 
				OptionsTextCol = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptionsCol)." 
				WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Y guardamos las opciones en la tabla de SI_SV_QAnswersCol y en las tablas de la HandHeld
		$mainQuestion->saveSelectOptions();
		
		//Eliminamos la dimension si dicha opcion pertenece a una pregunta multidimension
		if($mainQuestion->IsMultiDimension==1)
		{
			$this->removeSelectOptionDim();

			//Verificamos que sea multiple opcion la pregunta, que sea multidimension y tenga marcado
			//las entradas numericas y que haya indicador definido para dicha opcion
			if($mainQuestion->MCInputType==1 && $mainQuestion->IsIndicatorMC==1)
			{
				$this->removeSelectOptionInd();
			}
		}
		
		//Ahora procedemos a actualizar la tabla de hechos para eliminar aquella opcion
		//que se elimino siempre y cuando dicha pregunta no contenga campos de captura 
		//numericos o string
		if($mainQuestion->MCInputType==0)
		{
			$tablaParalela = $mainQuestion->SurveyTable;
			$questionField = $mainQuestion->SurveyField;
			
			//Primero le agregamos un ; al inicio y final del valor del campo
			$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = CONCAT(".$this->Repository->DataADOConnection->Quote(";").", ".$questionField.", ".$this->Repository->DataADOConnection->Quote(";").")";
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strNew = ";";
			$strOld = ";".$this->QuestionOptionName.";";
	
			//Reemplazamos el valor de la opcion eliminada por el valor de ;
			$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = REPLACE(".$questionField.", ".$this->Repository->DataADOConnection->Quote($strOld).", ".$this->Repository->DataADOConnection->Quote($strNew).")";
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			//Le quitamos el ; al inicio y al final del campo para dejarlo como antes pero con la opcion eliminada
			$sql = "UPDATE ".$tablaParalela." SET ".$questionField." = SUBSTRING(".$questionField.", 2, (CHAR_LENGTH(".$questionField.")-2))";
			
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." ".$tablaParalela." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
	}

	function removeSelectOptionDim()
	{
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Verificamos que sea multiple opcion la pregunta, que sea multidimension y tenga 
		//un valor valido el atributo IndDimID
		if($this->QTypeID==3 && $mainQuestion->IsMultiDimension==1 && !is_null($this->IndDimID) && $this->IndDimID>0)
		{
			$strOriginalWD = getcwd();
			//Eliminar una dimension
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			require_once("../model_manager/modeldata.inc.php");

			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $this->IndDimID);
			$anInstanceModelDim->Dimension->remove();

			chdir($strOriginalWD);
		}
	}
	
	function removeSelectOptionInd()
	{
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);

		//Verificamos que sea multiple opcion la pregunta, que sea multidimension y tenga marcado
		//las entradas numericas y que haya indicador definidp para dicha opcion
		if($this->QTypeID==3 && $mainQuestion->IsMultiDimension==1 && $mainQuestion->MCInputType==1 && $mainQuestion->IsIndicatorMC==1 && !is_null($this->IndicatorID) && $this->IndicatorID>0)
		{
			$strOriginalWD = getcwd();
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager

			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			require_once("../model_manager/indicatorkpi.inc.php");
			require_once("../model_manager/modeldimension.inc.php");
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($this->Repository, $this->IndicatorID);
			$anInstanceIndicator->remove();

			chdir($strOriginalWD);
		}
	}

	function removeForSimpleChoice()
	{
		//Vamos a simular parte de las funciones de la clase Question para utilizar funciones de ellas
		//Generamos instancia de Question
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$arraySelectOptionsCol = array();
		
		//Validamos que las opciones sean unicas
		if(trim($mainQuestion->StrSelectOptionsCol)!="")
		{
			//Modificamos el atributo ->StrSelectOptionsCol haciendo nulo aquel que se va a eliminar
			$arraySelectOptionsCol = explode("\r\n", $mainQuestion->StrSelectOptionsCol);
		}

		$arraySelectOptionsColFlip = array_flip($arraySelectOptionsCol);
		unset($arraySelectOptionsColFlip[$this->QuestionOptionName]);
		$arraySelectOptionsCol = array_flip($arraySelectOptionsColFlip);
		$arraySelectOptionsCol = array_values($arraySelectOptionsCol);
		//Obtenemos nuevo valor de ->StrSelectOptionsCol
		$LN = chr(13).chr(10);
		$mainQuestion->StrSelectOptionsCol = implode($LN, $arraySelectOptionsCol);
		//Validamos las opciones
		$mainQuestion->validateSelectOptions();
		
		$sql = "UPDATE SI_SV_Question SET 
				OptionsTextCol = ".$this->Repository->DataADOConnection->Quote($mainQuestion->StrSelectOptionsCol)." 
				WHERE QuestionID = ".$mainQuestion->QuestionID." AND SurveyID = ".$mainQuestion->SurveyID;

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Y guardamos las opciones en la tabla de SI_SV_QAnswersCol y en las tablas de la HandHeld
		$mainQuestion->saveSelectOptions();
		
		//En la tabla de dimension se eliminara la opcion si es que esta fue eliminada de las opciones
		//de la pregunta
		$strOriginalWD = getcwd();
		
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");

		$anInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $mainQuestion->ModelID, -1, $mainQuestion->IndDimID);
		chdir($strOriginalWD);

		//Se agregan los nuevos elementos a la dimension y se eliminan aquellos que ya no estan en uso
		$mainQuestion->updateMassiveDimensionValues($anInstanceDim);

		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $mainQuestion->SurveyID);
	}

	function isNewObject()
	{
		return ($this->QuestionOptionID <= 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Question Option");
		}
		else
		{
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			
			if($mainQuestion->QTypeID==2 && $mainQuestion->QDisplayMode == dspMatrix)
			{
				return translate("If column option is")." \"".$this->QuestionOptionName."\"";
			}
			else 
			{
				return str_ireplace("{var1}", " \"".$this->QuestionOptionName."\"", translate("If response option is {var1}"));
			}
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionOptionCol&QuestionID=".$this->QuestionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionOptionCol&QuestionID=".$this->QuestionID."&QuestionOptionID=".$this->QuestionOptionID;
		}
	}

	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'QuestionOptionID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				
			//Si está en desarrollo la encuesta si se puede editar la pregunta
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		if($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType!=0)
		{
			return false;
		}
		else 
		{
			if($_SESSION["PABITAM_UserRole"]==1)
			{
				$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);

				//Si está en desarrollo la encuesta si se puede editar la pregunta
				if($aSurveyInstance->Status==0)
				{
					//Por el momento deshabilitamos el remove de las opciones
					//de las preguntas de matriz hasta q haya mas tiempo para programarlo
					if(($mainQuestion->QTypeID==2 && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix))
					{
						return false;
					}
					else 
					{
						return true;
					}
				}
				else 
				{
					return false;
				}
			}
			else 
			{
				return false;
			}
		}
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		if($this->isNewObject() && $this->QTypeID==2)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "QuestionOptionName";
			$aField->Title = translate("Option");
			$aField->Type = "String";
			$aField->Size = 255;
			if(!$this->isNewObject())
			{
				//Verificar si esta opcion existe en otra dimension compartida de otra encuesta
				$existQOption = $this->existQOptionInOtherSharedDims();
				
				if($existQOption==true)
				{
					$aField->IsDisplayOnly = true;
				}
			}
			$myFields[$aField->Name] = $aField;
		}
		
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		//if($mainQuestion->QTypeID==2 || ($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType==0))
		if($mainQuestion->QTypeID==2)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Score";
			$aField->Title = translate("Score value");
			$aField->Type = "Float";
			$aField->DefaultNumberZero = false;
			$aField->FormatMask = "#,##0.00";
			$myFields[$aField->Name] = $aField;
		}
		
		//if($mainQuestion->QTypeID==2 || ($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType==0))
		if($mainQuestion->QTypeID==2)
		{
			$arrayOperations = array();
			$arrayOperations[0] = translate("Add");
			$arrayOperations[1] = translate("Multiply");
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ScoreOperation";
			$aField->Title = translate("Select score operation");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrayOperations;
			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}
	
	function existQOptionInOtherSharedDims()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$sql = "SELECT DisplayText FROM SI_SV_QAnswersCol WHERE QuestionID IN
				(
  					SELECT QuestionID FROM SI_SV_Question WHERE IndDimID = ".$mainQuestion->IndDimID." 
  					AND QuestionID <> ".$this->QuestionID." 
				) AND DisplayText = ".$this->Repository->DataADOConnection->Quote($this->QuestionOptionName);
		
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if($aRS->EOF)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
	 	<script language="JavaScript">

		function verifyFieldsAndSave(target)
 		{
 			//alert(BITAMQuestionOptionCol_SaveForm.Score._value);
 			//alert(BITAMQuestionOptionCol_SaveForm.Score.value);
 			
 			BITAMQuestionOptionCol_Ok(target);
 		}
  		</script>
<?

	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		
	 	objOkSelfButton = document.getElementById("BITAMQuestionOptionCol_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMQuestionOptionCol_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
			objOkNewButton = document.getElementById("BITAMQuestionOptionCol_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?

	}
	
	function get_Children($aUser)
	{
		$myChildren = array();

		return $myChildren;
	}
}

class BITAMQuestionOptionColCollection extends BITAMCollection
{
	public $QuestionID;

	function __construct($aRepository, $aQuestionID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
		
		$mainQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $this->QuestionID);
		if($mainQuestion->QTypeID==2 && $mainQuestion->QDisplayMode <> dspMatrix)
		{
			$this->OrderPosField = "SortOrder";
		}
	}
	
	static function NewInstance($aRepository, $aQuestionID, $anArrayOfQuestionOptionIDs = null)
	{
		$anInstance = new BITAMQuestionOptionColCollection($aRepository, $aQuestionID);

		$filter = "";

		if (!is_null($anArrayOfQuestionOptionIDs))
		{
			switch (count($anArrayOfQuestionOptionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.ConsecutiveID = ".((int)$anArrayOfQuestionOptionIDs[0]);
					break;
				default:
					foreach ($anArrayOfQuestionOptionIDs as $aQuestionOptionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aQuestionOptionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.ConsecutiveID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT t1.QuestionID, t1.ConsecutiveID, t1.DisplayText, t1.SortOrder, t1.IndDimID, t1.IndicatorID, 
				t1.Score, t1.ScoreOperation, t2.QTypeID 
				FROM SI_SV_QAnswersCol t1, SI_SV_Question t2 
				WHERE t1.QuestionID = ".$aQuestionID.$filter." AND t1.QuestionID = t2.QuestionID 
				ORDER BY t1.SortOrder";

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMQuestionOptionCol::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}
		if (array_key_exists("SortOrder", $aHTTPRequest->POST)) {
         
            $orderPos = (int) $aHTTPRequest->POST["SortOrder"];
            $aQuestionOptionID = (int) $aHTTPRequest->POST["QuestionOptionID"];
            BITAMQuestionOptionColCollection::Reorder($aRepository, $aQuestionID, $aQuestionOptionID, $orderPos);
        }

        if (array_key_exists("SortOrder", $aHTTPRequest->POST)) {
            $anInstance = BITAMQuestionOptionCol::NewInstance($aRepository, $aQuestionID);
            $anInstance = $anInstance->get_Parent();
            $aHTTPRequest->RedirectTo = $anInstance;
            return null;
        }
		else{
			return BITAMQuestionOptionColCollection::NewInstance($aRepository, $aQuestionID);
		}
	}
	
	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}

	function get_Title()
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		if($mainQuestion->QTypeID==2 && $mainQuestion->QDisplayMode == dspMatrix)
		{
			return translate("Response items")."- ".translate("Columns");
		}
		else 
		{
			return translate("Response items");
		}
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=QuestionOptionColCollection&QuestionID=".$this->QuestionID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=QuestionOptionCol&QuestionID=".$this->QuestionID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'QuestionOptionID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionOptionName";
		if($mainQuestion->QTypeID==2 && $mainQuestion->QDisplayMode == dspMatrix)
		{
			$aField->Title = translate("Column option");
		}
		else 
		{
			$aField->Title = translate("Question Option");
		}
		
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//if($mainQuestion->QTypeID==2 || ($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType==0))
		if($mainQuestion->QTypeID==2)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Score";
			$aField->Title = translate("Score value");
			$aField->Type = "Float";
			$aField->DefaultNumberZero = false;
			$aField->FormatMask = "#,##0.00";
			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		if($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType!=0)
		{
			return false;
		}
		else 
		{
			if($_SESSION["PABITAM_UserRole"]==1)
			{
				$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				
				//Si está en desarrollo la encuesta si se puede editar la pregunta
				if($aSurveyInstance->Status==0)
				{
					//Por el momento deshabilitamos el add de las opciones
					//de las preguntas de matriz hasta q haya mas tiempo para programarlo
					if(($mainQuestion->QTypeID==2 && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix))
					{
						return false;
					}
					else 
					{
						return true;
					}
				}
				else 
				{
					return false;
				}
			}
			else 
			{
				return false;
			}
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		
		if($mainQuestion->QTypeID==3 && $mainQuestion->MCInputType!=0)
		{
			return false;
		}
		else 
		{
			if($_SESSION["PABITAM_UserRole"]==1)
			{
				$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				
				//Si está en desarrollo la encuesta si se puede editar la pregunta
				if($aSurveyInstance->Status==0)
				{
					//Por el momento deshabilitamos el remove de las opciones
					//de las preguntas de matriz hasta q haya mas tiempo para programarlo
					if(($mainQuestion->QTypeID==2 && $mainQuestion->UseCatalog==0 && $mainQuestion->QDisplayMode==dspMatrix))
					{
						return false;
					}
					else 
					{
						return true;
					}
				}
				else 
				{
					return false;
				}
			}
			else 
			{
				return false;
			}
		}
	}
	 
	static function Reorder($aRepository, $aQuestionID, $aQuestionOptionID, $orderPos) 
	{
    	$prevOrderPos = BITAMQuestionOptionColCollection::getCurrentOrderPos($aRepository, $aQuestionID, $aQuestionOptionID);
        
        if ($prevOrderPos != -1) 
        {
            if ($orderPos > $prevOrderPos) 
            {
                $prevOrderPos = $prevOrderPos +1;
                $sql = "UPDATE SI_SV_QAnswersCol SET SortOrder = SortOrder - 1 
						WHERE ( SortOrder BETWEEN " . $prevOrderPos . " AND " . $orderPos . " ) AND QuestionID = " . $aQuestionID;
                if ($aRepository->DataADOConnection->Execute($sql) === false) 
                {
                    die(translate("Error accessing") . " SI_SV_QAnswersCol " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
                }
            }

            if ($prevOrderPos > $orderPos) 
            {
                $sql = "UPDATE SI_SV_QAnswersCol SET SortOrder = SortOrder + 1 
						WHERE ( SortOrder BETWEEN " . $orderPos . " AND " . $prevOrderPos . " ) AND QuestionID = " . $aQuestionID;

                if ($aRepository->DataADOConnection->Execute($sql) === false) {
                    die(translate("Error accessing") . " SI_SV_QAnswersCol " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
                }
            }

            $sql = "UPDATE SI_SV_QAnswersCol SET SortOrder = " . $orderPos . " 
					WHERE QuestionID  = " . $aQuestionID . " AND ConsecutiveID = " . $aQuestionOptionID;

            if ($aRepository->DataADOConnection->Execute($sql) === false) 
            {
                die(translate("Error accessing") . " SI_SV_QAnswersCol " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
            }
        }
    }
    
	static function getCurrentOrderPos($aRepository, $aQuestionID, $aQuestionOptionID) 
	{
        $sql = "SELECT SortOrder FROM SI_SV_QAnswersCol WHERE QuestionID = " . $aQuestionID . " AND ConsecutiveID = " . $aQuestionOptionID;

        $aRS = $aRepository->DataADOConnection->Execute($sql);

        if (!$aRS) {
            die(translate("Error accessing") . " SI_SV_QAnswersCol " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
        }

        $orderPos = -1;

        if (!$aRS->EOF) 
        {
            $orderPos = (int) $aRS->fields["sortorder"];
        }

        return $orderPos;
    }
}
?>