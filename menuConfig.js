//var arrInd = new Array();
//var arrIndSort = new Array();
//var arrVal = new Array();
var col_active = '';
var optionsIDs = new Array();
var optionsNames = new Array();
var optionsVisible = new Array();
var optionsNoVisible = new Array();

function Start()
{
	/*
	GetElementWithId('botOK').value 		= ML[69];
	GetElementWithId('botCancel').value 	= ML[70];
	GetElementWithId('add_column').title	= ML[157];
	GetElementWithId('del_column').title	= ML[193];
	GetElementWithId('up_column').title		= ML[377];
	GetElementWithId('down_column').title	= ML[378];
	construct_arrays();
	*/
	fill_Cols();
}
/*
function construct_arrays(){
	var arrCols = new Array();
	var arrSortCols = new Array();
	var arrInfo = new Array();
	var arrAux = new Array();
	arrCols = window_dialogArguments[2];
	arrCols = arrCols.split('_AWSC_');
	arrSortCols = window_dialogArguments[3];
	arrSortCols = arrSortCols.split('_AWSC_');
	itot = arrCols.length;
	for (i=0; i<itot; i++){
		arrInfo = arrCols[i].split('_AWS_');
		arrInd.push(arrInfo[0]);	// indice
		arrAux = exist_sort_id(arrInfo[0], arrSortCols);
		if (arrAux == null)
			arrVal[arrInfo[0]] = [arrInfo[1], arrInfo[2], -1, -1];
		else{
			arrVal[arrInfo[0]] = [arrInfo[1], arrInfo[2], arrAux[1], arrAux[2]];
		}
		//alert(arrVal[arrInfo[0]]);
	}
	itot = arrSortCols.length;
	for (i=0; i<itot; i++){
		if (arrSortCols[i] != ''){
			aux_arr = arrSortCols[i].split('_AWS_');
			if (arrVal[aux_arr[0]] != undefined) {
				arrIndSort.push(aux_arr[0]);	// indice sort
			}
		}
	}
	arrCols = null;
	arrSortCols = null;
	arrInfo = null;
	arrAux = null;
}

function exist_sort_id(ColId, arrSort){
	var tam_sc = arrSort.length;
	for (j=0; j<tam_sc; j++){
		aux_arr = arrSort[j].split('_AWS_');
		if (aux_arr[0] == ColId)
			return aux_arr;
	}
	return null;
}
*/
function fill_Cols(){
	
	var tableVisible = Array();
	var tableNoVisible = Array();
	onmouse_ov_out = 'onmouseover="change_color_over(this);" onmouseout="change_color_out(this,\'transparent\');" onclick="set_color(this);" ';
	sstyle = 'style="border-bottom:1px solid #F3F3F3;"';
	itot = optionsIDs.length;
	for (i=0; i<itot; i++)
	{
		//if ((arrVal[arrInd[i]][2] == -1) && (arrVal[arrInd[i]][3] == -1))
		if(inArray(optionsVisible, optionsIDs[i]))
		{
			tableVisible.push('<tr '+onmouse_ov_out+' id="'+optionsIDs[i]+'"><td '+sstyle+'>'+optionsNames[i]+'</td></tr>');
		}
		//else
		//{
			if(inArray(optionsNoVisible, optionsIDs[i]))
			{
				tableNoVisible.push('<tr '+onmouse_ov_out+' id="'+optionsIDs[i]+'"><td '+sstyle+'>'+optionsNames[i]+'</td></tr>');
			}
		//}
	}
	
	tableVisible_HTML = '<table cellspading=0 cellspacing=0 width=100% style="font-size:11px;">' + tableVisible.join('') + '</table>';
	tableNoVisible_HTML = '<table cellspading=0 cellspacing=0 width=100% style="font-size:11px;">' + tableNoVisible.join('') + '</table>';
	document.getElementById("Columns").innerHTML = tableNoVisible_HTML;
	document.getElementById("SortColumns").innerHTML = tableVisible_HTML;
}

function on_ok()
{
	var numOptions = optionsVisible.length;
	var strOptionsVisible = '';
	for (i=0; i<numOptions; i++)
	{
		if(strOptionsVisible!='')
		{
			strOptionsVisible += ',';
		}
		strOptionsVisible += optionsVisible[i];
	}

	window.returnValue = strOptionsVisible;
	top.close();
}

function cancel()
{
	window.returnValue = null;
	top.close();
}

function change_color_over(celda){
	if (celda.id != col_active)
		celda.style.backgroundColor="#dfd2c0";
}
function change_color_out(celda, color){
	if (celda.id != col_active)
		celda.style.backgroundColor=color;
}
/*
function ED_rb(){
	if ((arrVal[col_active][2] == -1) && (arrVal[col_active][3] == -1)){	// disable all rbs
		document.getElementById('rb_asc').disabled = true;
		document.getElementById('rb_des').disabled = true;
		document.getElementById('rb_asc_key').disabled = true;
		document.getElementById('rb_des_key').disabled = true;
	}
	else if (arrVal[col_active][0] == 4){	// able all rbs if column is dimension
		document.getElementById('rb_asc').disabled = false;
		document.getElementById('rb_des').disabled = false;
		document.getElementById('rb_asc_key').disabled = false;
		document.getElementById('rb_des_key').disabled = false;
		if (arrVal[col_active][2] == 0){	//  0 order, 1 order by key
			if (arrVal[col_active][3] == 0)	// checked 0 asc, 1 desc
				document.getElementById('rb_asc').checked = true;
			else if (arrVal[col_active][3] == 1)	// checked 0 asc, 1 desc
				document.getElementById('rb_des').checked = true;
		}
		else if (arrVal[col_active][2] == 1){	//  0 order, 1 order by key
			if (arrVal[col_active][3] == 0)	// checked 0 asc, 1 desc
				document.getElementById('rb_asc_key').checked = true;
			else if (arrVal[col_active][3] == 1)	// checked 0 asc, 1 desc
				document.getElementById('rb_des_key').checked = true;
		}
		else {
			document.getElementById('rb_asc').checked = true;
		}
	}
	else {	// only able the order and disabled order by key
		document.getElementById('rb_asc').disabled = false;
		document.getElementById('rb_des').disabled = false;
		document.getElementById('rb_asc_key').disabled = true;
		document.getElementById('rb_des_key').disabled = true;
		if (arrVal[col_active][2] == 0){	//  0 order, 1 order by key
			if (arrVal[col_active][3] == 0)	// checked 0 asc, 1 desc
				document.getElementById('rb_asc').checked = true;
			else if (arrVal[col_active][3] == 1)	// checked 0 asc, 1 desc
				document.getElementById('rb_des').checked = true;
		}
		else {
			document.getElementById('rb_asc').checked = true;
		}
	}
}
*/
function set_color(celda){

	if (celda.id != col_active)
	{
		celda.style.backgroundColor="brown";
		celda.style.color="white";
		if (col_active != '')
		{
			document.getElementById(col_active).style.backgroundColor="transparent";
			document.getElementById(col_active).style.color="#000000";
		}
		col_active = celda.id;
		//ED_rb();
		//alert(arrVal[col_active]);
	}
}

function Add_to_Order()
{

	if (col_active != '')
	{
		if(inArray(optionsVisible, col_active)==false)
		{
		//if ((arrVal[col_active][2] == -1) && (arrVal[col_active][3] == -1)){	// disable all rbs
			//arrVal[col_active][2] = 0;
			//arrVal[col_active][3] = 0;
			optionsVisible.push(col_active);
			DelElemArray(0);
			fill_Cols();
			document.getElementById(col_active).style.backgroundColor="brown";
			document.getElementById(col_active).style.color="white";
			//ED_rb();
		//}
		}
	}
}

function Del_to_Order()
{	
	if (col_active != '')
	{
		if(inArray(optionsNoVisible, col_active)==false)
		{
		//if ((arrVal[col_active][2] != -1) && (arrVal[col_active][3] != -1)){
			//arrVal[col_active][2] = -1;
			//arrVal[col_active][3] = -1;
			optionsNoVisible.push(col_active);
			DelElemArray(1);
			fill_Cols();
			document.getElementById(col_active).style.backgroundColor="brown";
			document.getElementById(col_active).style.color="white";
			//ED_rb();
		//}
		}
	}
}

function DelElemArray(arrayType)
{
	if(arrayType==0)
	{
		if (col_active != '')
		{
			var tam_arr = optionsNoVisible.length;
			for (i=0; i<tam_arr; i++)
			{
				if (optionsNoVisible[i] == col_active)
				{
					for (j=i; j<tam_arr-1; j++)
					{
						optionsNoVisible[j] = optionsNoVisible[j+1];
					}
					optionsNoVisible.pop();
					break;
				}
			}
		}
	}
	else
	{
		if(arrayType==1)
		{
			if (col_active != '')
			{
				var tam_arr = optionsVisible.length;
				for (i=0; i<tam_arr; i++)
				{
					if (optionsVisible[i] == col_active)
					{
						for (j=i; j<tam_arr-1; j++)
						{
							optionsVisible[j] = optionsVisible[j+1];
						}
						optionsVisible.pop();
						break;
					}
				}
			}
		}	
	}
}

/*
function Set_Order(SbK, AscDesc){
	if (col_active != ''){
		if ((arrVal[col_active][2] != -1) && (arrVal[col_active][3] != -1)){	// disable all rbs
			arrVal[col_active][2] = SbK;
			arrVal[col_active][3] = AscDesc;
		}
	}
}
function Up() {
	if (col_active != ''){
		if ((arrVal[col_active][2] != -1) && (arrVal[col_active][3] != -1)){
			if (arrIndSort.length < 2)
				return false;
			var sel = new Array();
			for (i=0; i<arrIndSort.length; i++) {
				if (arrIndSort[i] == col_active) {
					sel[sel.length] = i;
					break;
				}
			}
			for (i in sel) {
				if (sel[i] != 0) {
					var tmp = new Array(arrIndSort[sel[i]-1]);
					arrIndSort[sel[i]-1] = arrIndSort[sel[i]];
					arrIndSort[sel[i]] = tmp[0];
				}
			}
			fill_Cols();
			document.getElementById(col_active).style.backgroundColor="brown";
			document.getElementById(col_active).style.color="white";
			ED_rb();
		}
	}
}
function Down() {
	if (col_active != ''){
		if ((arrVal[col_active][2] != -1) && (arrVal[col_active][3] != -1)){
			if (arrIndSort.length < 2)
				return false;
			var sel = new Array();
			for (i=arrIndSort.length-1; i>-1; i--) {
				if (arrIndSort[i] == col_active) {
					sel[sel.length] = i;
				}
			}
			for (i in sel) {
				if (sel[i] != arrIndSort.length-1) {
					var tmp = new Array(arrIndSort[sel[i]+1]);
					arrIndSort[sel[i]+1]= arrIndSort[sel[i]];
					arrIndSort[sel[i]] = tmp[0];
				}
			}
			fill_Cols();
			document.getElementById(col_active).style.backgroundColor="brown";
			document.getElementById(col_active).style.color="white";
			ED_rb();
		}
	}
}
*/