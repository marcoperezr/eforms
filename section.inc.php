<?php 

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("sectionFilter.inc.php");

class BITAMSection extends BITAMObject
{
	static $KeyField = "SectionKey";		//SVeForms_####_SectionStd.SectionKey o SVeForms_####_Section_xxxx.SectionKey: Key auto generado que representa el registro de la sección estándar para la captura (es el equivalente al anterior registro con EntrySectionID == 0)

	public $ForceNew;
	public $SurveyID;
	public $SurveyName;
	public $SectionID;
	public $SectionName;
	//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
	public $SectionNameHTML;
	//@JAPR 2014-09-23: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
	public $SectionDimValID;		//Contiene el ID de la dimensión para almacenar el valor de los registros de secciones Inline (en el caso de las que usan catálogos, preferentemente usar las dimensiones del catálogo pues esta es sólo una copia del atributo de la sección)
	//@JAPR
	public $SectionDimID;
	public $ModelID;
	public $SurveyTable;
	public $SectionNumber;
	public $DisplaySection;
	public $SectionType;
	public $SectionTypeOld;
	public $SurveyCatalogID;
	public $CatalogID;
	public $CatMemberID;
	//@JAPR 2015-08-20: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6
	public $CatalogIDOld;
	//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $ChildCatMemberID;
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	public $ChildCatMemberOrder;		//Sólo utilizado para almacenar la posición dentro del catálogo del ChildCatMemberID durante el procesamiento de secciones Inline en el grabado
	//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
	public $DataSourceID;					//ID del DataSource (catálogo de v6) al que se asocia esta sección
	public $DataSourceIDOld;				//ID del DataSource (catálogo de v6) al que se asocia esta sección
	public $DataSourceMemberID;				//ID del DataSourceMember (atributo de un catálogo de v6) al que se asocia esta sección
	//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
	public $DataSourceFilter;				//Filtro de catálogo personalizable a nivel de sección
	//@JAPR
	public $Styled;
	public $FormattedText;
	public $SendThumbnails;
	public $EnableHyperlinks;
	//@JAPR 2012-07-26: Agregado el salto de sección a sección
	public $NextSectionID;
	//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
	public $AutoRedraw;
	//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
	public $ShowCondition;
	//@JAPR 2014-05-19: Agregado el tipo de sección Inline
	public $SelectorQuestionID;				//ID de pregunta que debe ser una tipo Single Choice con despliegue tipo CheckBox, que sirve como switch por registro para indicar si uno se va o no a capturar
	//@JAPR 2014-05-23: Agregado el tipo de sección Inline
	public $DisplayMode;			//Modo de despliegue de la sección (a la fecha de implementación sólo aplicaba para secciones Inline)
	//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
	public $SwitchBehaviour;		//Indica si el comportamiento de las secciones Inline será que al marcar el Selector se habilitan las preguntas (0 == default) o que se ocultarán (1)
	//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
	public $HTMLFooter;				//Porción HTML formateada que sirve como pié de página personalizado de la sección (no va dentro del footer propio, va simplemente al final de la página antes del footer automático)
	public $HTMLHeader;				//Porción HTML formateada que sirve como encabezado personalizado de la sección (no va dentro del header propio, va simplemente al inicio de la página después del header automático)
	//@JAPR 2014-07-30: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
	public $FormattedTextiPod;
	public $HTMLFooteriPod;
	public $HTMLHeaderiPod;
	public $FormattedTextiPad;
	public $HTMLFooteriPad;
	public $HTMLHeaderiPad;
	public $FormattedTextiPadMini;
	public $HTMLFooteriPadMini;
	public $HTMLHeaderiPadMini;
	public $FormattedTextiPhone;
	public $HTMLFooteriPhone;
	public $HTMLHeaderiPhone;
	public $FormattedTextCel;
	public $HTMLFooterCel;
	public $HTMLHeaderCel;
	public $FormattedTextTablet;
	public $HTMLFooterTablet;
	public $HTMLHeaderTablet;
	public $ResponsiveDesignProps;	//Array indexado por el nombre de la propiedad de Responsive Design (formato NombreDeviceID) cuando dicha propiedad ya existía en la tabla, si no está asignado significa que es nueva (esto es útil durante el grabado)
	//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
	public $StrSelectOptions;		//Lista de opciones fijas para generar los registros de la sección separados por <enter>
	//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
	//Ahora el tipo de llenado del componente será un campo mas en la metadata
	public $ValuesSourceType;		//Indica el tipo de fuente para llenar los valores de esta sección Inline/Dinámica
	//@MABH 2014-06-17
	public $SectionFormID;
	//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
	public $ShowInNavMenu;
	public $SummaryInMDSection;
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	public $LastModAdminVersion;	//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
	public $ShowSelector;			//Indica si se deberá o no mostrar el CheckBox para habilitar la captura de las preguntas en cada registro (Default == true)
	//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
	public $ShowAsQuestion;			//Indica que la sección (Inline solamente) se deberá mostrar incrustada dentro de la sección Estándar o Inline previa, de tal manera que se vea como una única página
	//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
	public $SourceQuestionID;		//ID de la pregunta múltiple choice de la que obtendrá sus opciones de respuesta esta sección Inline
	public $SourceSectionID;		//ID de la sección Inline de la que obtendrá sus opciones de respuesta esta sección Inline
	
	//Sep-2014: Tipo de Editor de HTML
	public $EditorType;
    public $MinRecordsToSelect;  //2015.02.10 JCEM #9T6ASP numero minimo de registros que se deben seleccionar

	//Sep-2014: Campos para guardar lo del nuevo diseñador en formato con separador de coma
	public $FormattedTextDes;
	public $FormattedTextiPodDes;
	public $FormattedTextiPadDes;
	public $FormattedTextiPadMiniDes;
	public $FormattedTextiPhoneDes;
	public $FormattedTextCelDes;
	public $FormattedTextTabletDes;
	//Sep-2014: Campos para guardar lo del nuevo diseñador en formato HTML
	public $FormattedTextDesHTML;
	public $FormattedTextiPodDesHTML;
	public $FormattedTextiPadDesHTML;
	public $FormattedTextiPadMiniDesHTML;
	public $FormattedTextiPhoneDesHTML;
	public $FormattedTextCelDesHTML;
	public $FormattedTextTabletDesHTML;
	
	
	public $HTMLHeaderDes;
	public $HTMLHeaderiPodDes;
	public $HTMLHeaderiPadDes;
	public $HTMLHeaderiPadMiniDes;
	public $HTMLHeaderiPhoneDes;
	public $HTMLHeaderCelDes;
	public $HTMLHeaderTabletDes;
	
	public $HTMLHeaderDesHTML;
	public $HTMLHeaderiPodDesHTML;
	public $HTMLHeaderiPadDesHTML;
	public $HTMLHeaderiPadMiniDesHTML;
	public $HTMLHeaderiPhoneDesHTML;
	public $HTMLHeaderCelDesHTML;
	public $HTMLHeaderTabletDesHTML;
	
	public $HTMLFooterDes;
	public $HTMLFooteriPodDes;
	public $HTMLFooteriPadDes;
	public $HTMLFooteriPadMiniDes;
	public $HTMLFooteriPhoneDes;
	public $HTMLFooterCelDes;
	public $HTMLFooterTabletDes;
	
	public $HTMLFooterDesHTML;
	public $HTMLFooteriPodDesHTML;
	public $HTMLFooteriPadDesHTML;
	public $HTMLFooteriPadMiniDesHTML;
	public $HTMLFooteriPhoneDesHTML;
	public $HTMLFooterCelDesHTML;
	public $HTMLFooterTabletDesHTML;
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	//Propiedades con nombres de campos y/o tablas que apuntan a la nueva estructura de grabado de datos de las formas
	public $DescKeyField;			//SVeForms_####_Section_xxxx.SectionDescKey: Key de la descripción que se encuentra en la tabla de catálogo de la sección (SVeForms_####_Section_xxxx_Desc) según el tipo de sección Dinámica o Inline solamente
	public $DescField;				//SVeForms_####_Section_xxxx_Desc.SectionDesc: Si la sección es Inline con opciones fijas o basada en una pregunta u otra sección Inline de opciones fijas, sólo se requiere una única descripción la cual se almacenará en este campo
	public $SectionTable;			//SVeForms_####_SectionStd o SVeForms_####_Section_xxxx: Nombre de la tabla que contiene los registros de la sección
	public $SectionDescTable;		//SVeForms_####_Section_xxxx_Desc: Nombre de la tabla que almacena las descripciones de opciones fijas o catálogos si la sección es tipo Dinámica o Inline
	//@JAPR 2015-08-13: Agregado el borrado de reportes a partir del ID de la captura
	public $SectionMultTable;		//Sólo usado durante el proceso de borrado de reportes. Contiene el nombre de la tabla si es que esta fuera multi-registro, se maneja independiente porque se deben borrar datos considerando la posibilidad que en algún momento esta sección fue multi-registro
	//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
	//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
	public $Repetitions;			//Indica el tipo de sección basado en el número de registros u origen de los mismos (sólo lectura, se auto-asigna al cargar la instancia)
	//@JAPR
	//@RTORRES 2015-08-28: Agregado ocultar boton next,back
	public $HideNavButtons;
	//OMMC 2016-01-19: Agregados para el header y footer de sección
	public $HTMLHEditorState;
	public $HTMLFEditorState;
	public $CopiedSectionID;
	public $CreateCat;				//Es utilizada para condicionar el código de creación de catálogo, en la copia de formas se hace un segundo save para ajustar IDs de la
									//sección en este caso no es necesario que se ejecute todo el código de creación de catálogos por que este se hizo en el primer save al copiar
									//la sección
	//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	public $SourceID;				//ID del objeto original al que apunta esta instancia para mantener la sincronización entre formas de desarrollo/producción
	//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
	public $UseDescriptorRow;		//Indica si se utilizará una row con la descripción de cada registro de secciones tabla (true), o si se continuará mostrando como una columna mas (false - default)
	//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	public $XPosQuestionID;			//ID de pregunta que debe ser una tipo Numérica, que sirve para almacenar la posición X por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	public $YPosQuestionID;			//ID de pregunta que debe ser una tipo Numérica, que sirve para almacenar la posición Y por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	public $ItemNameQuestionID;		//ID de pregunta que debe ser una tipo Alfanumérica, que sirve para almacenar el nombre de la opción de respuesta seleccionada por registro de la sección Maestro-detalle que representa a un elemento colocado en una pregunta Sketch+
	
	function __construct($aRepository, $aSurveyID)
	{
		BITAMObject::__construct($aRepository);
		$this->ForceNew = false;
		$this->SurveyID = $aSurveyID;
		$this->SurveyName= "";
		$this->SectionID = -1;
		$this->SectionName = "";
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		$this->SectionNameHTML = "";
		//@JAPR
		$this->SectionDimValID = -1;
		$this->SectionDimID = 0;
		$this->ModelID = -1;
		$this->SurveyTable = "";
		$this->SectionNumber = 0;
		$this->DisplaySection = "";
		$this->SectionType = sectNormal;
		$this->SectionTypeOld = sectNormal;
		$this->CatalogID = 0;
		//@JAPR 2015-08-20: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6
		$this->CatalogIDOld = 0;
		//@JAPR
		$this->CatMemberID = 0;
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		$this->DataSourceID = 0;
		$this->DataSourceIDOld = 0;
		$this->DataSourceMemberID = 0;
		//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
		$this->DataSourceFilter = '';
		//@JAPR
		$this->FormattedText = "";
		$this->SendThumbnails = 0;
		$this->EnableHyperlinks = 0;
		//@JAPR 2012-07-26: Agregado el salto de sección a sección
		$this->NextSectionID = 0;
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->ChildCatMemberID = 0;
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		$this->ChildCatMemberOrder = 0;
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		$this->AutoRedraw = 0;
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		$this->ShowCondition = '';
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		$this->SelectorQuestionID = 0;
		//@JAPR 2014-05-23: Agregado el tipo de sección Inline
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento
		$this->DisplayMode = sdspDefault;
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		$this->SwitchBehaviour = 0;
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		$this->HTMLFooter = '';
		$this->HTMLHeader = '';
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		$this->StrSelectOptions = '';
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		$this->ValuesSourceType = tofFixedAnswers;
		//@MABH 2014-06-17
		$this->SectionFormID = 0;
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		$this->ShowInNavMenu = 1;
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		$this->ShowSelector = 0;
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		$this->ShowAsQuestion = 0;
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		$this->SourceQuestionID = 0;
		$this->SourceSectionID = 0;
		//@JAPR 2014-07-30: Agregado el Responsive Design (Variables para configurar propiedades por dispositivo usando el Framework)
		$this->FormattedTextiPod = '';
		$this->HTMLFooteriPod = '';
		$this->HTMLHeaderiPod = '';
		$this->FormattedTextiPad = '';
		$this->HTMLFooteriPad = '';
		$this->HTMLHeaderiPad = '';
		$this->FormattedTextiPadMini = '';
		$this->HTMLFooteriPadMini = '';
		$this->HTMLHeaderiPadMini = '';
		$this->FormattedTextiPhone = '';
		$this->HTMLFooteriPhone = '';
		$this->HTMLHeaderiPhone = '';
		$this->FormattedTextCel = '';
		$this->HTMLFooterCel = '';
		$this->HTMLHeaderCel = '';
		$this->FormattedTextTablet = '';
		$this->HTMLFooterTablet = '';
		$this->HTMLHeaderTablet = '';
		$this->ResponsiveDesignProps = array();
		$this->SummaryInMDSection = 1; //Agregada la propiedad para las MD con summary en la misma seccion
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$this->CreationAdminVersion = 0;
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = 0;
		//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
		//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
		$this->Repetitions = srptSingle;
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		$this->HideNavButtons = 0;
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		$this->UseDescriptorRow = 0;
		//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
		//$u_agent = $_SERVER['HTTP_USER_AGENT'];
		//if(preg_match('/MSIE/i',$u_agent)) 
		if (is_IE())
    	{ 
	    	$this->Styled = 62; 
    	} else {
    		$this->Styled = 100;
    	}

		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//Actualizar el ModelID
			$this->getModel();
		}
		//@JAPR
		
		//Sep-2014: Opciones - [0] = "Tinymce", 1 = "Designer";
		$this->EditorType = 1;
        $this->MinRecordsToSelect = 0;
		$this->FormattedTextDes = '';
		$this->FormattedTextiPodDes = '';
		$this->FormattedTextiPadDes = '';
		$this->FormattedTextiPadMiniDes = '';
		$this->FormattedTextiPhoneDes = '';
		$this->FormattedTextCelDes = '';
		$this->FormattedTextTabletDes = '';
		$this->FormattedTextDesHTML = '';
		$this->FormattedTextiPodDesHTML = '';
		$this->FormattedTextiPadDesHTML = '';
		$this->FormattedTextiPadMiniDesHTML = '';
		$this->FormattedTextiPhoneDesHTML = '';
		$this->FormattedTextCelDesHTML = '';
		$this->FormattedTextTabletDesHTML = '';
		
		
		$this->HTMLHeaderDes = '';
		$this->HTMLHeaderiPodDes = '';
		$this->HTMLHeaderiPadDes = '';
		$this->HTMLHeaderiPadMiniDes = '';
		$this->HTMLHeaderiPhoneDes = '';
		$this->HTMLHeaderCelDes = '';
		$this->HTMLHeaderTabletDes = '';
		$this->HTMLHeaderDesHTML = '';
		$this->HTMLHeaderiPodDesHTML = '';
		$this->HTMLHeaderiPadDesHTML = '';
		$this->HTMLHeaderiPadMiniDesHTML = '';
		$this->HTMLHeaderiPhoneDesHTML = '';
		$this->HTMLHeaderCelDesHTML = '';
		$this->HTMLHeaderTabletDesHTML = '';
		
		$this->HTMLFooterDes = '';
		$this->HTMLFooteriPodDes = '';
		$this->HTMLFooteriPadDes = '';
		$this->HTMLFooteriPadMiniDes = '';
		$this->HTMLFooteriPhoneDes = '';
		$this->HTMLFooterCelDes = '';
		$this->HTMLFooterTabletDes = '';
		$this->HTMLFooterDesHTML = '';
		$this->HTMLFooteriPodDesHTML = '';
		$this->HTMLFooteriPadDesHTML = '';
		$this->HTMLFooteriPadMiniDesHTML = '';
		$this->HTMLFooteriPhoneDesHTML = '';
		$this->HTMLFooterCelDesHTML = '';
		$this->HTMLFooterTabletDesHTML = '';

		$this->HTMLHEditorState = edstGraphic;
		$this->HTMLFEditorState = edstGraphic;
		$this->CopiedSectionID = 0;
		$this->CreateCat = true;
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$this->SourceID = 0;
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$this->XPosQuestionID = 0;
		$this->YPosQuestionID = 0;
		$this->ItemNameQuestionID = 0;
	}

	static function NewInstance($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSurveyID);
	}

	static function NewInstanceWithID($aRepository, $aSectionID)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance = null;
		
		if (((int) $aSectionID) <= 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetSectionInstanceWithID($aSectionID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
		$strAdditionalFields = "";
		if (getMDVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.NextSectionID ';
		}
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$strAdditionalFields .= ', t1.AutoRedraw ';
		}
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		if (getMDVersion() >= esvShowSectionCondition) {
			$strAdditionalFields .= ', t1.ShowCondition ';
		}
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		if (getMDVersion() >= esvInlineSection) {
			$strAdditionalFields .= ', t1.SelectorQuestionID, t1.DisplayMode, t1.SwitchBehaviour ';
		}
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$strAdditionalFields .= ', t1.HTMLFooter, t1.HTMLHeader ';
		}
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		if (getMDVersion() >= esvFixedInlineSections) {
			$strAdditionalFields .= ', t1.OptionsText ';
		}
		//@MABH 2014-06-17
		if (getMDVersion() >= esvSectionRecap) {
			$strAdditionalFields .= ', t1.SectionFormID ';
		}
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.ShowInNavMenu ';
		}
		//Conchita Agregado el summary en las paginas de secciones maestro detalle
		if (getMDVersion() >= esvShowInOnePageMD) {
			$strAdditionalFields .= ', t1.SummaryInMDSection ';
		}
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion ';
		}
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (getMDVersion() >= esvInlineMerge) {
			$strAdditionalFields .= ', t1.ShowSelector, t1.ShowAsQuestion ';
		}
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$strAdditionalFields .= ', t1.SourceQuestionID, t1.SourceSectionID ';
		}
		//Sep-2014
		if (getMDVersion() >= esvEditorHTML)
		{
			$strAdditionalFields .= ', t1.EditorType ';
			$strAdditionalFields .= ', t1.FormattedTextDes, t1.HTMLHeaderDes, t1.HTMLFooterDes ';
		}		
        //2015.02.10 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
			$strAdditionalFields .= ', t1.MinRecordsToSelect';
		}
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.SectionNameHTML, t1.ValuesSourceType';
        }
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		if (getMDVersion() >= esveFormsv6) {
			//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			$strAdditionalFields .= ', t1.DataSourceID, t1.DataSourceMemberID, t1.DataSourceFilter';
        }
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			$strAdditionalFields .= ', t1.HideNavButtons ';
		}
		//OMMC 2016-01-19: Agregado para el estado del editor HTML de header y footer
		if (getMDVersion() >= esvHTMLEditorState){
			$strAdditionalFields .= ', t1.HTMLHEditorState, t1.HTMLFEditorState';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID';
		}
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		if (getMDVersion() >= esvTableSectDescRow) {
			$strAdditionalFields .= ', t1.UseDescriptorRow';
		}
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if (getMDVersion() >= esvSketchPlus) {
			$strAdditionalFields .= ', t1.XPosQuestionID, t1.YPosQuestionID, t1.ItemNameQuestionID';
		}
		//@JAPR
		$sql = "SELECT t1.SurveyID, t2.SurveyName, t1.SectionID, t1.SectionName, t1.SectionNumber,t1.SectionDimValID, 
				t1.SectionType, t1.CatalogID, t1.CatMemberID, t1.FormattedText, t1.SendThumbnails, t1.EnableHyperlinks $strAdditionalFields 
			FROM SI_SV_Section t1, SI_SV_Survey t2 
			WHERE t1.SurveyID = t2.SurveyID AND t1.SectionID = ".$aSectionID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		
		return $anInstance;
	}
	
	//@JAPR 2014-07-30: Agregado el Responsive Design
	/* Carga las propiedades específicas del Responsive Design basado en los parámetros, agregándolas a las instancias especificadas en la
	colección o array proporcionado, de tal forma que este método se puede invocar bajo demanda sólo en los casos donde realmente se necesiten
	estas propiedades sin tener que forzar a que el NewInstanceFromRS se sature con queries y asignaciones que no van a ser utilizadas
	El parámetro $anArrayOfObjects se asumirá que es un array ya indexado por el SectionID, sin embargo si se trata de una colección
	de Secciones, entonces primero se procede a crear el array interno para facilitar el acceso a las instancias
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	static function GetResponsiveDesignProps($aRepository, $anArrayOfObjects, $aSurveyID = null, $anArrayOfObjectIDs = null, $anObjectType = null) {
		return true;
		global $gblShowErrorSurvey;
		
		$aSurveyID = (int) $aSurveyID;
		if (is_null($anArrayOfObjectIDs) || !is_array($anArrayOfObjectIDs)) {
			$anArrayOfObjectIDs = array();
		}
		if (is_null($anObjectType)) {
			$anObjectType = -1;
		}
		
		//Asigna el array de secciones indexado por el ID
		$arrObjects = $anArrayOfObjects;
		if (!is_array($anArrayOfObjects)) {
			$arrObjects = array();
			foreach ($anArrayOfObjects->Collection as $objObject) {
				$arrObjects[$objObject->SectionID] = $objObject;
			}
		}
		
		//Si no se hubiera especificado ningún filtro, utiliza el array de instancias para obtener las llaves y cargar sólo los objetos que
		//fueron recibidos, de esta forma se puede sólo recibir el array de instancias sin especificar mas parámetros y funcionará bien
		if ($aSurveyID <= 0 && count($anArrayOfObjectIDs) == 0 && $anObjectType <= -1) {
			$anArrayOfObjectIDs = array_keys($arrObjects);
		}
		
		//Genera el filtro basado en los parámetros especificados
		$where = "";
		if ($aSurveyID > 0) {
			$where .= " AND A.SurveyID = {$aSurveyID}";
		}
		
		if ($anObjectType >= sectNormal) {
			$where .= " AND B.SectionType = {$anObjectType}";
		}
		
		$filter = '';
		if (!is_null($anArrayOfObjectIDs))
		{
			switch (count($anArrayOfObjectIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.ObjectID = ".((int)$anArrayOfObjectIDs[0]);
					break;
				default:
					foreach ($anArrayOfObjectIDs as $anObjectID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anObjectID; 
					}
					if ($filter != "")
					{
						$filter = " AND A.ObjectID IN (".$filter.")";
					}
					break;
			}
		}
		$where .= $filter;
		
		//Sep-20014
		$strSelect = "";
		if (getMDVersion() >= esvEditorHTML)
		{
			$strSelect = ", A.FormattedTextDes, A.HTMLHeaderDes, A.HTMLFooterDes";
		}
		//2015.02.11 JCEM #9T6ASP
        if(getMDVersion() >= esvMinRecordsToSelect){
            $strSelect = ", B.MinRecordsToSelect";
        }
		
		$sql = "SELECT B.SurveyID, B.SectionID, A.DeviceID, A.FormattedText, A.HTMLHeader, A.HTMLFooter ".$strSelect."
			FROM SI_SV_SurveyHTML A 
				INNER JOIN SI_SV_Section B ON A.ObjectID = B.SectionID 
			WHERE A.ObjectType = ".otySection.$where;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML, SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$intObjectID = (int) @$aRS->fields["sectionid"];
			$objObject = @$arrObjects[$intObjectID];
			if (!is_null($objObject)) {
				$intDeviceID = (int) @$aRS->fields["deviceid"];
				$objObject->ResponsiveDesignProps[$intDeviceID] = 1;
				
				$strProperty = "FormattedText";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				$strProperty = "HTMLHeader";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				$strProperty = "HTMLFooter";
				$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
				$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
				
				//Sep-2014: Campo para el nuevo Editor
				if (getMDVersion() >= esvEditorHTML)
				{
					$strProperty = "FormattedText";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1);

					$strProperty = "FormattedText";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1, 1);
					
					$strProperty = "HTMLHeader";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1);

					$strProperty = "HTMLHeader";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1, 1);
					
					$strProperty = "HTMLFooter";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)."des"];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1);

					$strProperty = "HTMLFooter";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue, 1, 1);
				}
				//2015.02.11 JCEM #9T6ASP
                if(getMDVersion() >= esvMinRecordsToSelect){
                    $strProperty = "MinRecordsToSelect";
					$strPropValue = (string) @$aRS->fields[strtolower($strProperty)];
					$objObject->setResponsiveDesignProperty($strProperty, $intDeviceID, $strPropValue);
                }
			}
			
			$aRS->MoveNext();
		}
	}
	
	/* Asigna dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	//Sep-2014: Se agregó el nuevo parámetro $editor para indicar a cual editor pertenece la propiedad, 0: TinyMCE y  1: El nuevo editor
	//Ya que se guardan en difetentes propiedades, las propiedades del nuevo editor teminan con "Des"
	//Octubre 2014: Se agregó el campo $html que es utilizado cuando $editor=1 (nuevo editor) y es para especificar
	//si el valor de la propiedad es en formato $html
	public function setResponsiveDesignProperty($aPropertyName, $aDeviceName, $aValue, $editor=0, $html=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$strPropName = trim($aPropertyName.$aDeviceName);
		
		if ($editor==1)
		{
			$strPropName = $strPropName."Des";
			
			if($html==1)
			{
				$strPropName = $strPropName."HTML";
			}
		}
		
		if ($strPropName != '') {
			@$this->$strPropName = $aValue;
		}
	}
	
	/* Obtiene dinámicamente el valor de la propiedad especificada ajustada al tipo de dispositivo indicado en el parámetro $aDeviceName, 
	si este es numérico entonces se convierte a su equivalente en String para completar el nombre de propiedad
	*/
	public function getResponsiveDesignProperty($aPropertyName, $aDeviceName, $bDefaultIfEmpty = false, $editor=0) {
		if (is_numeric($aDeviceName)) {
			$aDeviceName = getDeviceNameFromID($aDeviceName);
		}
		
		$aValue = null;
		$strPropName = trim($aPropertyName.$aDeviceName);
		if ($strPropName != '') {
			
			if ($editor==1)
			{
				$strPropName = $strPropName."Des";		
			}			
			$aValue = @$this->$strPropName;
			
			//Si no se encuentra configurado nada específico para el tipo de dispositivo y no se trata del dispositivo default, entonces
			//extrae el valor default directo 
			if (trim($aValue) == '' && $aDeviceName != '' && $bDefaultIfEmpty) {
				$aPropertyName = trim($aPropertyName);
				
				if ($editor==1)
				{
					$aPropertyName = $aPropertyName."Des";
				}
				
				$aValue = @$this->$aPropertyName;
			}
		}
		
		return $aValue;
	}
	//@JAPR
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aSurveyID = (int)$aRS->fields["surveyid"];
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
		//@JAPR
		$anInstance->SurveyName = $aRS->fields["surveyname"];
		
		$anInstance->SectionID = (int)$aRS->fields["sectionid"];
		$anInstance->SectionName = $aRS->fields["sectionname"];
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$anInstance->SectionNameHTML = (string) @$aRS->fields["sectionnamehtml"];
			//Si el texto formateado no viene asignado, se usará por default el texto normal (compatibilidad hacia atrás)
			if (trim($anInstance->SectionNameHTML) == '') {
				$anInstance->SectionNameHTML = $anInstance->SectionName;
			}
		}
		//@JAPR
		$anInstance->SectionNumber = (int)$aRS->fields["sectionnumber"];
		
		$anInstance->SectionDimValID = (int)@$aRS->fields["sectiondimvalid"];
		$anInstance->DisplaySection = $anInstance->SectionNumber.". ".$anInstance->SectionName;
		
		$anInstance->SectionType = (int)$aRS->fields["sectiontype"];
		$anInstance->SectionTypeOld = $anInstance->SectionType;
		$anInstance->CatalogID = (int)$aRS->fields["catalogid"];
		//@JAPR 2015-08-20: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6
		$anInstance->CatalogIDOld = $anInstance->CatalogID;
		//@JAPR
		$anInstance->CatMemberID = (int)$aRS->fields["catmemberid"];
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		$anInstance->DataSourceID = (int)$aRS->fields["datasourceid"];
		$anInstance->DataSourceIDOld = $anInstance->DataSourceID;
		$anInstance->DataSourceMemberID = (int)$aRS->fields["datasourcememberid"];
		//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
		$anInstance->DataSourceFilter = (string) @$aRS->fields["datasourcefilter"];
		//@JAPR
		$anInstance->FormattedText = $aRS->fields["formattedtext"];
		$anInstance->SendThumbnails = (int)$aRS->fields["sendthumbnails"];
		$anInstance->EnableHyperlinks = (int)$aRS->fields["enablehyperlinks"];
		//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
		if (getMDVersion() >= esvMultiDynamicAndStandarization) {
			$anInstance->NextSectionID = (int) @$aRS->fields["nextsectionid"];
		}
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		$anInstance->AutoRedraw = (int) @$aRS->fields["autoredraw"];
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		$anInstance->ShowCondition = (string) @$aRS->fields["showcondition"];
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		$anInstance->SelectorQuestionID = (int) @$aRS->fields["selectorquestionid"];
		//@JAPR 2014-05-23: Agregado el tipo de sección Inline
		$anInstance->DisplayMode = (int) @$aRS->fields["displaymode"];
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		$anInstance->SwitchBehaviour = (int) @$aRS->fields["switchbehaviour"];
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		$anInstance->HTMLFooter = (string) @$aRS->fields["htmlfooter"];
		$anInstance->HTMLHeader = (string) @$aRS->fields["htmlheader"];
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		$anInstance->StrSelectOptions = (string) @$aRS->fields["optionstext"];
		//@MABH 2014-06-17
		$anInstance->SectionFormID = (int) @$aRS->fields["sectionformid"];
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		$anInstance->ShowInNavMenu = (int) @$aRS->fields["showinnavmenu"];
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		$anInstance->ShowSelector = (int) @$aRS->fields["showselector"];
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		//El requerimiento original cambió durante la implementación y finalmente ya no se utilizará una configuración a nivel de sección, sino que
		//se basará en la existencia de preguntas Inline, se dejará el campo por si se desea aplicarlo de alguna otra forma mas adelante, pero por
		//ahora se va a asignar automáticamente en la definición descargada
		//$anInstance->ShowAsQuestion = (int) @$aRS->fields["showasquestion"];
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		$anInstance->SourceQuestionID = (int) @$aRS->fields["sourcequestionid"];
		$anInstance->SourceSectionID = (int) @$aRS->fields["sourcesectionid"];
		
		//Agregado el summary en cada pagina en la seccion de maestro detalle
		$anInstance->SummaryInMDSection = (int) @$aRS->fields["summaryinmdsection"];
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$anInstance->ValuesSourceType = @$aRS->fields["ValuesSourceType"];
			//Si no está asignada la propiedad, se asume que es una versión vieja de la definición así que se ajusta dependiendo de otras configuraciones
			if (is_null($anInstance->ValuesSourceType)) {
				if ($anInstance->SectionType == sectInline) {
					$anInstance->ValuesSourceType = $anInstance->typeOfFilling(true);
				}
				else {
					$anInstance->ValuesSourceType = tofFixedAnswers;
				}
			}
			
			//Dependiendo del tipo de llenado, se desactivan algunas otras configuraciones
			switch ($anInstance->ValuesSourceType) {
				case tofFixedAnswers:
					break;
				case tofMCHQuestion:
					break;
				case tofSharedAnswers:
					break;
				default:
				//case tofFixedAnswers:
					break;
			}
		}
		//@JAPR
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//@JAPR 2013-02-19: Corregido un bug, durante la creación de una pregunta multiple choice en sección dinámica, se cargaba la instancia
		//de section antes de crear la pregunta, así que cuando la cargaba de nuevo para validar si había o no multiple choice la instancia no se
		//leía de la metadata y quedaba con la propiedad sin asignar, provocando que no se agregara la dimensión correspondiente, ahora mejor se
		//separará el método para identificar las multiple choice dentro de la sección
		$anInstance->getChildCatMemberID();
		//@JAPR
		
		//Sep-2014		
		$anInstance->EditorType = (int) @$aRS->fields["editortype"];
		//2015.02.11 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
            $anInstance->MinRecordsToSelect = (int) @$aRS->fields["MinRecordsToSelect"];
		}
		$anInstance->FormattedTextDes = trim(@$aRS->fields["formattedtextdes"]);
		$anInstance->FormattedTextDesHTML = trim(@$aRS->fields["formattedtext"]);
		$anInstance->HTMLHeaderDes = trim(@$aRS->fields["htmlheaderdes"]);
		$anInstance->HTMLHeaderDesHTML = trim(@$aRS->fields["htmlheader"]);
		$anInstance->HTMLFooterDes = trim(@$aRS->fields["htmlfooterdes"]);
		$anInstance->HTMLFooterDesHTML = trim(@$aRS->fields["htmlfooter"]);
		
		//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
		//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
		//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
		$anInstance->setRepetitions();
		
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$anInstance->setTableAndFieldsProperties();
		//@JAPR
		
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			$anInstance->HideNavButtons = (int) @$aRS->fields["hidenavbuttons"];
		}
		//OMMC 2016-01-19: Agregado para guardar el estado del editor HTML para header y footer
		if(getMDVersion() >= esvHTMLEditorState){
			$anInstance->HTMLHEditorState = (int) @$aRS->fields["HTMLHEditorState"];
			$anInstance->HTMLFEditorState = (int) @$aRS->fields["HTMLFEditorState"];
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$anInstance->SourceID = (int) @$aRS->fields["sourceid"];
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		$anInstance->UseDescriptorRow = (int) @$aRS->fields["usedescriptorrow"];
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$anInstance->XPosQuestionID = (int) @$aRS->fields["xposquestionid"];
		$anInstance->YPosQuestionID = (int) @$aRS->fields["yposquestionid"];
		$anInstance->ItemNameQuestionID = (int) @$aRS->fields["itemnamequestionid"];
		//@JAPR
		
		BITAMGlobalFormsInstance::AddSectionInstanceWithID($anInstance->SectionID, $anInstance);
		
		return $anInstance;
	}
	
	//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
	//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
	/* Asigna el valor del campo Repetitions basado en el tipo de sección del que se trata */
	//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
	function setRepetitions() {
		$intRepetitions = srptSingle;
		switch ($this->SectionType) {
			case sectMasterDet:
				$intRepetitions = srptMulti;
				break;
			case sectNormal:
			case sectFormatted:
				$intRepetitions = srptSingle;
				break;
			//JAPR 2015-08-20: Se determinó que las repeticiones seguirán fijas a 1 y múltiples, luego se subclasificaría las múltiples, pero ya no dependerá sólo de este campo
			//para saber el tipo de sección
			case sectInline:
			default:
				$intRepetitions = srptMulti;
				break;
		}
		
		$this->Repetitions = $intRepetitions;
	}
	
	/* Asigna el valor del tipo de sección y otras propiedades en base al valor de repeticiones
	Esta función se debe usar preferentemente en UpdateFromArray o en algún proceso que modifica la definición de la sección antes de invocar al save
	//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
	A partir de esta actualización el concepto de Repetitions cambia, ahora cuando es una sola repetición se convertirá en sección estándar automáticamente, pero cuando sean múltiples,
	debe considerar el SectionType actual para ver si era o no estándar, si lo era entonces automáticamente asigna Maestro-detalle, de lo contrario no tendría caso cambiar el tipo de 
	sección ni asignar el newTypeOfFilling porque todos los demás tipos también son multi-registro
	//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
	*/
	function setSectionTypeByRepetitions($iRepetitions) {
		$this->Repetitions = $iRepetitions;
		
		$blnUpdateTypeOfFilling = false;
		switch ($iRepetitions) {
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			//Este tipo ya no aplica
			/*
			case srptCatalog:
				//Será una sección Inline de catálogo, pero aún no tenemos el catálogo indicado
				$this->SectionType = sectInline;
				break;
			*/
			
			case srptSingle:
				//Será una sección estándar, no requiero datos extras
				$this->SectionType = sectNormal;
				$blnUpdateTypeOfFilling = true;
				break;
			
			case srptMulti:
			default:
				//Será una sección Maestro-Detalle, no requiere catálogo ni tipo de llenado
				//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
				//A partir de esta actualización hay que comprobar el tipo que tenía anteriormente la sección para determinar si debe o no hacer algún ajuste, sólo si viniera de una
				//sección estándar se asumiría automáticamente que se quiere crear una sección maestro-detalle
				if ($this->SectionTypeOld == sectNormal || $this->SectionTypeOld == sectFormatted) {
					$this->SectionType = sectMasterDet;
					$blnUpdateTypeOfFilling = true;
				}
				else {
					//En este caso no hay elementos para cambiar este valor, ya que todas las demás secciones son múltiples por naturaleza, puede estar cambiando entre maestro-detalle
					//e Inline o dinámica (en v6 la dinámica es una Inline en despliegue de paginación)
				}
				break;
		}
		
		//Sobreescribe newTypeOfFilling para que al momento de invocar al save se limpien varias propiedades que aplicarían sólo en ciertos tipos de secciones
		if ($blnUpdateTypeOfFilling) {
			$this->newTypeOfFilling = tofFixedAnswers;
		}
	}
	//@JAPR
	
	//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
	/* Graba el filtro dinámico de la forma para el catálogo de esta sección
	*/
	function saveDynamicFilter() {
		if ($this->ValuesSourceType != tofCatalog || $this->CatalogID <= 0 || $this->SectionType != sectInline) {
			return;
		}
		
		if (trim($this->DataSourceFilter) == '') {
			$sql = "DELETE FROM SI_SV_SurveyFilter WHERE CatalogID = ".$this->CatalogID;
			$this->Repository->DataADOConnection->Execute($sql);
			return;
		}
		
		//Obtiene la referencia al filtro dinámico actual para actualizar si es que hay uno
		$intFilterID = 0;
		$sql = "SELECT FilterID 
			FROM SI_SV_SurveyFilter 
			WHERE SurveyID = {$this->SurveyID} AND CatalogID = {$this->CatalogID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intFilterID = (int) @$aRS->fields["filterid"];
		}
		
		$objSurveyFilter = null;
		if ($intFilterID > 0) {
			$objSurveyFilter = BITAMSurveyFilter::NewInstanceWithID($this->Repository, $intFilterID);
		}
		if (is_null($objSurveyFilter)) {
			$objSurveyFilter = BITAMSurveyFilter::NewInstance($this->Repository, $this->SurveyID);
		}
		
		if (is_null($objSurveyFilter)) {
			return;
		}
		
		$objSurveyFilter->FilterName = $this->SectionName;
		$objSurveyFilter->CatalogID = $this->CatalogID;
		$objSurveyFilter->FilterText = $this->DataSourceFilter;
		$objSurveyFilter->save();
	}
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	/* Asigna las propiedades que contienen los nombres de tablas y campos a utilizar durante el proceso de generación de las tablas de datos o de grabado
	de las capturas realizadas para las formas de v6. Si la forma fuera previa a v4 y la propiedad hubiera existido previamente, entonces le deja el mismo
	nombre que originalmente se utilizaba */
	function setTableAndFieldsProperties() {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			//$this->KeyField = "SectionKey";
			$this->DescKeyField = "SectionDescKey";
			$this->DescField = "SectionDesc";
			if ($this->SectionType == sectNormal) {
				$this->SectionTable = "SVeForms_{$this->SurveyID}_SectionStd";
			}
			else {
				$this->SectionTable = "SVeForms_{$this->SurveyID}_Section_{$this->SectionID}";
			}
			$this->SectionMultTable = "SVeForms_{$this->SurveyID}_Section_{$this->SectionID}";
			$this->SectionDescTable = "SVeForms_{$this->SurveyID}_Section_{$this->SectionID}_Desc";
		}
	}
	
	//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
	//Regresa el tipo de llenado de la sección (tofFixedAnswers, tofMCHQuestion, tofInlineSection)
	//La validación da prioridad a las opciones de Shared o Source Question, y si no hay nada entonces asume que es de tipo fija (este tipo de
	//preguntas a la fecha de implementación, explícitamente no podían ser de tipo catálogo)
	function typeOfFilling($bAutoDetectFromOldVersion = false) {
		$intTypeOfFilling = tofFixedAnswers;
		
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		//En versión v6 ahora depende del tipo de captura configurado así que es directo
		if (getMDVersion() >= esveFormsv6 && !$bAutoDetectFromOldVersion) {
			return $this->ValuesSourceType;
		}
		//@JAPR
		
		if ($this->CatalogID > 0 && $this->CatMemberID > 0) {
			$intTypeOfFilling = tofCatalog;
		}
		elseif ($this->SourceQuestionID > 0) {
			$intTypeOfFilling = tofMCHQuestion;
		}
		elseif ($this->SourceSectionID > 0) {
			$intTypeOfFilling = tofInlineSection;
		}
		
		return $intTypeOfFilling;
	}
	
	//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//@JAPR 2013-02-19: Corregido un bug, durante la creación de una pregunta multiple choice en sección dinámica, se cargaba la instancia
	//de section antes de crear la pregunta, así que cuando la cargaba de nuevo para validar si había o no multiple choice la instancia no se
	//leía de la metadata y quedaba con la propiedad sin asignar, provocando que no se agregara la dimensión correspondiente, ahora mejor se
	//separará el método para identificar las multiple choice dentro de la sección
	function getChildCatMemberID() {
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		if ($this->SectionType != sectDynamic && $this->SectionType != sectInline) {
			return 0;
		}
		
		switch ($this->SectionType) {
			case sectDynamic:
				//Si es una sección dinámica, verifica si tiene o no preguntas multiple choice para asignar el atributo que deben usar
				$dynamicSectionWithMultiChoice = false;
				$sql = "SELECT A.QuestionID 
					FROM SI_SV_Question A, SI_SV_Section B 
					WHERE A.SectionID = B.SectionID AND A.SurveyID = {$this->SurveyID} AND B.SectionID = {$this->SectionID} 
						AND A.QTypeID = ".qtpMulti;
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS !== false)
				{
				    if (!$aRS->EOF) {
				    	//Si hay preguntas multiples en la dinámica, por tanto se obtiene el siguiente atributo del catálogo
				    	$dynamicSectionWithMultiChoice = true;
				    }
				}
				
				if ($dynamicSectionWithMultiChoice) {
					//Como hay preguntas múltiple choice en la sección dinámica, obtenemos el ID de atributo siguiente de esta sección
					$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
					if (!is_null($objCatalogMembersColl)) {
			    		$blnOnlyNextAttr = false;
						foreach ($objCatalogMembersColl as $objCatalogMember) {
							if ($blnOnlyNextAttr) {
								$this->ChildCatMemberID = $objCatalogMember->MemberID;
								break;
							}
							
							if ($objCatalogMember->MemberID == $this->CatMemberID) {
								//Sólo se debe procesar hasta el atributo siguiente al de la sección dinámica según el orden definido
								$blnOnlyNextAttr = true;
							}
						}
					}
				}
				break;
				
			//@JAPR 2014-05-29: Agregado el tipo de sección Inline
			case sectInline:
				//Si es una sección Inline, verifica si hay alguna pregunta de catálogo del mismo de la propia sección dentro de ella, si la hay
				//obtiene la última y el atributo usado por ella es el Child
				$intCatMemberID = 0;
				$sql = "SELECT A.QuestionID, A.QuestionNumber, A.CatMemberID, C.MemberOrder 
					FROM SI_SV_Question A, SI_SV_Section B, SI_SV_CatalogMember C 
					WHERE A.SectionID = B.SectionID AND A.SurveyID = {$this->SurveyID} AND B.SectionID = {$this->SectionID} 
						AND A.CatMemberID = C.MemberID AND A.QTypeID = ".qtpSingle." AND A.CatalogID = B.CatalogID 
					ORDER BY  A.QuestionNumber DESC";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS !== false)
				{
				    if (!$aRS->EOF) {
				    	$this->ChildCatMemberID = (int) @$aRS->fields["catmemberid"];
				    	$this->ChildCatMemberOrder = (int) @$aRS->fields["memberorder"];
				    }
				}
				break;
		}
		
		return $this->ChildCatMemberID;
	}
	//@JAPR
	
	//Extrae los datos del Modelo en donde existe esta sección
	function getModel()
	{
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		$sql = "SELECT t1.ModelID, t1.SectionDimID, t1.CatalogID FROM SI_SV_Survey t1 WHERE t1.SurveyID = ".$this->SurveyID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$this->ModelID = (int)$aRS->fields["modelid"];
			$this->SurveyTable = "SVSurvey_".$this->ModelID;
			$this->SectionDimID = (int)$aRS->fields["sectiondimid"];
		}
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		//@JAPR 2015-06-18: Agregada la edición de datos desde la ventana de rediseño
		//Se leerá directo rel request, en caso de no venir se leerá del objeto enviado como parámetro para procesar el request
		$aSurveyID = getParamValue('SurveyID', 'both', '(int)', true);
		if (is_null($aSurveyID)) {
			if (array_key_exists("SurveyID", $aHTTPRequest->GET))
			{
				$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
			}
			else
			{
				$aSurveyID = 0;
			}
		}
		//@JAPR
		
		if (array_key_exists("SectionID", $aHTTPRequest->POST))
		{
			$aSectionID = $aHTTPRequest->POST["SectionID"];
			
			if (is_array($aSectionID))
			{
				$aCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID, $aSectionID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMSection::NewInstance($aRepository, $aSurveyID);
				
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aSectionID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
				}
				//@JAPR 2014-07-30: Agregado el Responsive Design
				else {
					//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
					//Estas propiedades están descontinuadas a partir de eForms v6
					if (getMDVersion() < esveFormsv6 ) {
						if (getMDVersion() >= esvResponsiveDesign) {
							//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
							$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->SectionID => $anInstance));
						}
					}
					//@JAPR
				}
				//@JAPR
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("SectionID", $aHTTPRequest->GET))
		{
			$aSectionID = $aHTTPRequest->GET["SectionID"];
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aSectionID);
			
			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
			}
			//@JAPR 2014-07-30: Agregado el Responsive Design
			else {
				//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
				//Estas propiedades están descontinuadas a partir de eForms v6
				if (getMDVersion() < esveFormsv6 ) {
					if (getMDVersion() >= esvResponsiveDesign) {
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$strCalledClass::GetResponsiveDesignProps($aRepository, array($anInstance->SectionID => $anInstance));
					}
				}
				//@JAPR
			}
			//@JAPR
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aSurveyID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		global $garrUpdatedPropsWithVars;
		if (!isset($garrUpdatedPropsWithVars)) {
			$garrUpdatedPropsWithVars = array();
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"])) {
			$garrUpdatedPropsWithVars["Sections"] = array();
		}
		//@JAPR
		
		if (array_key_exists("SectionID", $anArray))
		{
			$this->SectionID = (int)$anArray["SectionID"];
		}

		if (array_key_exists("SurveyID", $anArray))
		{
			$this->SurveyID = (int)$anArray["SurveyID"];
		}

 		if (array_key_exists("SectionName", $anArray))
		{
			$this->SectionName = rtrim($anArray["SectionName"]);
		}
		
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			if (array_key_exists("SectionNameHTML", $anArray)) {
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->SectionNameHTML = rtrim($anArray["SectionNameHTML"]);
				//strip_tags(html_entity_decode($text))	<< Utilizar html_entity_decode si hay problema con escapes de caracteres HTML
				$this->SectionName = strip_tags($this->SectionNameHTML);
			}
			else {
				//@JAPR 2015-08-03: Removido el nombre con HTML de las secciones, si no se especificó se tomará el nombre normal
				$this->SectionNameHTML = $this->SectionName;
			}
		}
		//@JAPR
		
		//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
		//Ahora hay un campo para el CatalogID directamente
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = (int) @$anArray["CatalogID"];
		}

		if (array_key_exists("CatMemberID", $anArray))
		{
			$this->CatMemberID = (int) @$anArray["CatMemberID"];
		}
		
		if (array_key_exists("SectionType", $anArray))
		{
			$this->SectionType = (int)$anArray["SectionType"];
			//@JAPR 2014-05-19: Agregado el tipo de sección Inline
			if ($this->SectionType != sectDynamic && $this->SectionType != sectInline) {
				$this->CatalogID = 0;
				$this->CatMemberID = 0;
			}
			$this->setRepetitions();
		}
		//@JAPR
		
 		if (array_key_exists("FormattedText", $anArray))
		{
			$this->FormattedText = $anArray["FormattedText"];
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->FormattedText = $this->TranslateVariableQuestionNumbersByIDs($this->FormattedText, optyFormattedText);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Sections"]["FormattedText"] = 1;
			//@JAPR
		}
		
		if(array_key_exists("SendThumbnails",$anArray))
		{
			$this->SendThumbnails = (int)$anArray["SendThumbnails"];
		}
		else 
		{
			$this->SendThumbnails = 0;
		}

		if(array_key_exists("EnableHyperlinks",$anArray))
		{
			$this->EnableHyperlinks = (int)$anArray["EnableHyperlinks"];
		}
		else 
		{
			$this->EnableHyperlinks = 0;
		}
		
		//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
		if(array_key_exists("NextSectionID",$anArray))
		{
			$this->NextSectionID = (int)$anArray["NextSectionID"];
		}
		else 
		{
			$this->NextSectionID = 0;
		}
		
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if(array_key_exists("AutoRedraw",$anArray)) {
			$this->AutoRedraw = (int) $anArray["AutoRedraw"];
		}
		else {
			$this->AutoRedraw = 0;
		}

		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
 		if (array_key_exists("ShowCondition", $anArray))
		{
			$this->ShowCondition = $anArray["ShowCondition"];
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->ShowCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ShowCondition, optyShowCondition);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Sections"]["ShowCondition"] = 1;
			//@JAPR
		}
		
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if(array_key_exists("SelectorQuestionID",$anArray)) {
			$this->SelectorQuestionID = (int) $anArray["SelectorQuestionID"];
		}
		else {
			if (getMDVersion() < esveFormsv6) {
				$this->SelectorQuestionID = 0;
			}
		}
		
		//@JAPR 2014-05-23: Agregado el tipo de sección Inline
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento
		//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
		if (array_key_exists("DisplayMode",$anArray)) {
			$this->DisplayMode = (int) $anArray["DisplayMode"];
			if (getMDVersion() >= esveFormsv6) {
				//En esta versión al cambiar el modo de despliegue de una sección se está cambiando entre los tipos de sección Maestro-Detalle o inline, en el caso de esa última además
				//se cambia el uso o no de un catálogo
				switch ($this->DisplayMode) {
					case sdspDefault:
						$this->SectionType = sectMasterDet;
						break;
					default:
						//Si llega a este punto, a la fecha de implementación sólo pudieron haber sido secciones Inline, aunque se consideraban visualmente como Inline y dinámica pero
						//internamente eran lo mismo
						$this->SectionType = sectInline;
						break;
				}
				//$this->newTypeOfFilling = tofFixedAnswers;
			}
		}
		/*
		else {
			$this->DisplayMode = sdspDefault;
		}
		*/
		
		//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que se usará una propiedad exclusiva de desarrollo para controlar
		//el SectionType real, ya no deberían utilizarse las otras configuraciones mientras se tenga planeado este nuevo esquema de asignación del tipo
		if (array_key_exists("SectionTypeDes",$anArray)) {
			$intSectionTypeDes = (int) $anArray["SectionTypeDes"];
			//En esta versión al cambiar el modo de despliegue de una sección se está cambiando entre los tipos de sección Maestro-Detalle o inline, en el caso de esa última además
			//se cambia el uso o no de un catálogo
			//Esta configuración utiliza los mismos tipos ya definidos, realmente el único ajuste es la sección Dinámica, ya que en v6 no existe y se simula con una Inline en paginación
			switch ($intSectionTypeDes) {
				case sectDynamic:
					//La sección Dinámica es realmente una Inline en modo paginación
					$this->SectionType = sectInline;
					$this->DisplayMode = sdspPages;
					break;
				case sectMasterDet:
					$this->SectionType = $intSectionTypeDes;
					//@JAPR 2015-11-13: Corregido un bug, al seleccionar este tipo de sección, se deben resetear propiedades que no aplican con ella (#VQ92PE)
					$this->ValuesSourceType = tofFixedAnswers;
					$this->newTypeOfFilling = $this->ValuesSourceType;
					break;
				case sectInline:
					$this->SectionType = $intSectionTypeDes;
					$this->DisplayMode = sdspInline;
					break;
				case sectNormal:
				default:
					//Cualquier otra sección se considerará como estándar (ya no existe la sección formateada en v6, es una estándar con una pregunta tipo Mensaje, pero debe ser configurada
					//de tal manera por parte del usuario)
					$this->SectionType = $intSectionTypeDes;
					//@JAPR 2015-11-13: Corregido un bug, al seleccionar este tipo de sección, se deben resetear propiedades que no aplican con ella (#VQ92PE)
					$this->ValuesSourceType = tofFixedAnswers;
					$this->newTypeOfFilling = $this->ValuesSourceType;
					break;
			}
			
			$this->setRepetitions();
		}
		//@JAPR
		
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		if(array_key_exists("SwitchBehaviour",$anArray)) {
			$this->SwitchBehaviour = (int) $anArray["SwitchBehaviour"];
		}
		else {
			$this->SwitchBehaviour = 0;
		}
		
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
 		if (array_key_exists("HTMLFooter", $anArray))
		{
			$this->HTMLFooter = $anArray["HTMLFooter"];
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->HTMLFooter = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLFooter, optyHTMLFooter);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Sections"]["HTMLFooter"] = 1;
			//@JAPR
		}
		
 		if (array_key_exists("HTMLHeader", $anArray))
		{
			$this->HTMLHeader = $anArray["HTMLHeader"];
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->HTMLHeader = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLHeader, optyHTMLHeader);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Sections"]["HTMLHeader"] = 1;
			//@JAPR
		}
		
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
 		if (array_key_exists("StrSelectOptions", $anArray))
		{
			$this->StrSelectOptions = $anArray["StrSelectOptions"];
		}
		
		//@MABH 2014-06-17
		if (array_key_exists("SectionFormID", $anArray))
		{
			$this->SectionFormID = $anArray["SectionFormID"];
		}
		
		//Agregado el summary en cada pagina de la maestro detalle
		if (array_key_exists("SummaryInMDSection", $anArray))
		{
			$this->SummaryInMDSection = $anArray["SummaryInMDSection"];
		}

		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if (array_key_exists("ShowInNavMenu", $anArray)) {
			$this->ShowInNavMenu = $anArray["ShowInNavMenu"];
		}
		
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		if (array_key_exists("ShowSelector", $anArray)) {
			$this->ShowSelector = $anArray["ShowSelector"];
		}
		
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (array_key_exists("ShowAsQuestion", $anArray)) {
			$this->ShowAsQuestion = $anArray["ShowAsQuestion"];
		}
		
		//@JAPR 2014-07-30: Agregado el Responsive Design
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				
				$strProperty = "FormattedText";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strValue, optyFormattedText);
					//@JAPR
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
				
				$strProperty = "HTMLHeader";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strValue, optyHTMLHeader);
					//@JAPR
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
				
				$strProperty = "HTMLFooter";
				$strFieldName = $strProperty.$strDeviceName;
				$strValue = '';
		 		if (array_key_exists($strFieldName, $anArray))
				{
					$strValue = $anArray[$strFieldName];
					//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
					//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
					$strValue = $this->TranslateVariableQuestionNumbersByIDs($strValue, optyHTMLFooter);
					//@JAPR
					$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue);
				}
			}
		}
		
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if ($this->SectionType == sectInline) {
			if (array_key_exists("typeOfFilling", $anArray)) {
				$this->newTypeOfFilling = (int) @$anArray["typeOfFilling"];
			}
			
			if (array_key_exists("SourceQuestionID", $anArray)) {
				$this->SourceQuestionID = (int) @$anArray["SourceQuestionID"];
			}
			
			if (array_key_exists("SourceSectionID", $anArray)) {
				$this->SourceSectionID = (int) @$anArray["SourceSectionID"];
			}
		}
		//@JAPR
		
		//Sep-2014
		if (getMDVersion() >= esvEditorHTML)
		{
	 		$editor = 1;
	 		
			if (array_key_exists("EditorType", $anArray))
			{
				$this->EditorType = (int) $anArray["EditorType"];
			}
			
	 		if (array_key_exists("FormattedTextDes", $anArray))
			{
				$this->FormattedTextDes = $anArray["FormattedTextDes"];
			}
			
	 		if (array_key_exists("FormattedTextDesHTML", $anArray))
			{
				$this->FormattedTextDesHTML = $anArray["FormattedTextDesHTML"];
			}
			
	 		if (array_key_exists("HTMLHeaderDes", $anArray))
			{
				$this->HTMLHeaderDes = $anArray["HTMLHeaderDes"];
			}
			
	 		if (array_key_exists("HTMLHeaderDesHTML", $anArray))
			{
				$this->HTMLHeaderDesHTML = $anArray["HTMLHeaderDesHTML"];
			}
			
	 		if (array_key_exists("HTMLFooterDes", $anArray))
			{
				$this->HTMLFooterDes = $anArray["HTMLFooterDes"];
			}
			
	 		if (array_key_exists("HTMLFooterDesHTML", $anArray))
			{
				$this->HTMLFooterDesHTML = $anArray["HTMLFooterDesHTML"];
			}
		
			if (getMDVersion() >= esvResponsiveDesign) {
				for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					
					$strProperty = "FormattedText";
					//Campo correspondiente al nuevo editor por eso termina en "Des"
					$strFieldName = $strProperty.$strDeviceName."Des";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor);
					}
					
					$strFieldName = $strProperty.$strDeviceName."Des"."HTML";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor, 1);
					}
					
					$strProperty = "HTMLHeader";
					//Campo correspondiente al nuevo editor por eso termina en "Des"
					$strFieldName = $strProperty.$strDeviceName."Des";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor);
					}
					
					$strFieldName = $strProperty.$strDeviceName."Des"."HTML";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor, 1);
					}
					
					$strProperty = "HTMLFooter";
					//Campo correspondiente al nuevo editor por eso termina en "Des"
					$strFieldName = $strProperty.$strDeviceName."Des";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor);
					}
					
					$strFieldName = $strProperty.$strDeviceName."Des"."HTML";
					$strValue = '';
					
			 		if (array_key_exists($strFieldName, $anArray))
					{
						$strValue = $anArray[$strFieldName];
						$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $strValue, $editor, 1);
					}
				}
			}
		
		}
		
		if (getMDVersion() >= esvMinRecordsToSelect){
			 //2015.02.10 JCEM #9T6ASP 
			if(array_key_exists("MinRecordsToSelect", $anArray))
			{
				$this->MinRecordsToSelect = (int) $anArray["MinRecordsToSelect"];
			}
		}
		
		//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
		//Ahora el tipo de llenado del componente será un campo mas en la metadata
		if (getMDVersion() >= esvAdminWYSIWYG) {
			if (array_key_exists("ValuesSourceType",$anArray)) {
				$this->ValuesSourceType = (int) $anArray["ValuesSourceType"];
				//Dependiendo del tipo de llenado, se desactivan algunas otras configuraciones
				//En este caso el proceso de grabado ya valida algunas cosas según el valor de la propiedad newTypeOfFilling, así que se asigna en este punto
				$this->newTypeOfFilling = $this->ValuesSourceType;
			}
			
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			if (array_key_exists("DataSourceID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceID = (int) $anArray["DataSourceID"];
			}
			
			if (array_key_exists("DataSourceMemberID",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceMemberID = (int) $anArray["DataSourceMemberID"];
				if ($this->DataSourceMemberID > 0) {
				}
				else {
					//@JAPR 2015-08-21: Validado que si se limpia el DataSourceMemberID, se debe limpiar el CatMemberID también para mantener consistencia
					$this->DataSourceMemberID = 0;
					$this->CatMemberID = 0;
				}
			}
			
			//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			if (array_key_exists("DataSourceFilter",$anArray) && $this->ValuesSourceType == tofCatalog) {
				$this->DataSourceFilter = (string) $anArray["DataSourceFilter"];
				//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
				$this->DataSourceFilter = $this->TranslateVariableQuestionNumbersByIDs($this->DataSourceFilter, optyFormula);
				if ($this->SectionType == sectInline && $this->DataSourceID > 0) {
					$this->DataSourceFilter = BITAMDataSource::ReplaceMemberNamesByIDs($this->Repository, $this->DataSourceID, $this->DataSourceFilter);
				}
				//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				$garrUpdatedPropsWithVars["Sections"]["DataSourceFilter"] = 1;
				//@JAPR
			}
			//@JAPR
		}
		
		//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
		//Al final se procesa el número de repeticiones, ya que eso controla varias configuraciones, en primer instancia el SectionType, pero a su vez al cambiar ese dato se deben
		//actualizar CatalogID, CatMemberID, ValuesSourceType y varias mas, así que sobreescribe internamente newTypeOfFilling para limpiar todas y sólo alterar el tipo de sección
		//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
		if (getMDVersion() >= esvAdminWYSIWYG) {
			if (array_key_exists("Repetitions",$anArray)) {
				$this->Repetitions = (int) $anArray["Repetitions"];
				$this->setSectionTypeByRepetitions($this->Repetitions);
			}
		}
		//@JAPR
		//@RTORRES 2015-08-28: Agregado ocultar botones next y back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			if(array_key_exists("HideNavButtons",$anArray))
			{
				$this->HideNavButtons = (int)$anArray["HideNavButtons"];
			}
			else 
			{
				$this->HideNavButtons = 0;
			}
		}
		//OMMC 2016-01-19: Agregado para guardar el estado del editor HTML para header y footer
		if (getMDVersion() >= esvHTMLEditorState){
			if(array_key_exists("HTMLHEditorState", $anArray)){
				$this->HTMLHEditorState = (int)$anArray["HTMLHEditorState"];
			}
			if(array_key_exists("HTMLFEditorState", $anArray)){
				$this->HTMLFEditorState = (int)$anArray["HTMLFEditorState"];	
			}
		}
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		if (getMDVersion() >= esvTableSectDescRow){
			if(array_key_exists("UseDescriptorRow", $anArray)){
				$this->UseDescriptorRow = (int)$anArray["UseDescriptorRow"];
			}
		}
		//@JAPR
		
		return $this;
	}
	
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, otySection, $this->SectionID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, $bServerVariables);
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	/* Esta función actualizará las propiedades que pudieran tener variables en su interior y que no fueron asignadas por la función UpdateFromArray, forzando a traducirlas primero 
	a números de pregunta para que en caso de que el objeto no hubiera sido grabado aún con la versión que ya corregía los IDs se puedan recuperar correctamente, posteriormente las
	volverá a traducir a IDs pero ahora aplicando ya la corrección para no realizar reemplazos parciales. Esta función debe utilizarse siempre antesde un Save del objeto para que 
	durante el primer grabado sea cual fuera la razón para hacerlo, se puedan traducir correctamente las propiedades al momento de asignar el numero de versión de este servicio que ya
	asumirá que graba correctamente este tipo de propiededes, por tanto su uso es:
	1- Esperar a un UpdateFromArray (opcional, si no se invocó, simplemente sobreescribirá todas las propiedades, como por ejemplo en el proceso de copiado)
	2- Ejecutar esta función (si hubo un UpdateFromArray, algunas propiedades podrían ya estar asignadas en el array de reemplazos, así que esas se ignoran)
	3- Ejecutar al Save (esto sobreescribirá al objeto y sus propiedades que utilizan variables
	*/
	function fixAllPropertiesWithVarsNotSet() {
		global $garrUpdatedPropsWithVars;
		
		if (!isset($garrUpdatedPropsWithVars) || !is_array($garrUpdatedPropsWithVars) || !isset($garrUpdatedPropsWithVars["Sections"])) {
			$garrUpdatedPropsWithVars = array();
			$garrUpdatedPropsWithVars["Sections"] = array();
		}
		
		//Si ya se había aplicado un grabado a este objeto por lo menos con la versión de este fix, entonces ya no tiene sentido continuar con el proceso pues no habra diferencia
		if ($this->LastModAdminVersion >= esvPartialIDsReplaceFix) {
			return;
		}
		
		//Actualiza las propiedades no asignadas en el array de propiedades modificadas
		//Para que el proceso funcione correctamente, es indispensable que se engañe a la instancia sobreescribiendo temporalmente LastModAdminVersion a la versión actual pero sólo
		//cuando ya se va a reemplazar por IDs, cuando se va a reemplazar por números si puede dejar la versión actual para que auto-corrija el error con el que había grabado usando IDs
		//previamente
		$dblLastModAdminVersion = $this->LastModAdminVersion;
		$blnUpdatedProperties = false;
		//Primero traducirá de IDs a números todas las propiedades no grabadas manualmente para corregir los IDs mal grabados
		if (!isset($garrUpdatedPropsWithVars["Sections"]["FormattedText"])) {
			$this->FormattedText = $this->TranslateVariableQuestionIDsByNumbers($this->FormattedText);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["ShowCondition"])) {
			$this->ShowCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["HTMLFooter"])) {
			$this->HTMLFooter = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLFooter);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["HTMLHeader"])) {
			$this->HTMLHeader = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLHeader);
			$blnUpdatedProperties = true;
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["DataSourceFilter"])) {
			$this->DataSourceFilter = $this->TranslateVariableQuestionIDsByNumbers($this->DataSourceFilter);
			$blnUpdatedProperties = true;
		}
		
		if (!$blnUpdatedProperties) {
			return;
		}
		
		//Finalmente actualizará las propiedades no grabadas manualmente a IDs ya aplicando la corrección sobreescribiendo la versión del objeto
		//Como esta función se supone que se usará justo antes de un save, es correcto dejar la propiedad LastModAdminVersion actualizada, ya que de todas formas se modificaría en el save
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		if (!isset($garrUpdatedPropsWithVars["Sections"]["FormattedText"])) {
			$this->FormattedText = $this->TranslateVariableQuestionNumbersByIDs($this->FormattedText, optyFormattedText);
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["ShowCondition"])) {
			$this->ShowCondition = $this->TranslateVariableQuestionNumbersByIDs($this->ShowCondition, optyShowCondition);
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["HTMLFooter"])) {
			$this->HTMLFooter = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLFooter, optyHTMLFooter);
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["HTMLHeader"])) {
			$this->HTMLHeader = $this->TranslateVariableQuestionNumbersByIDs($this->HTMLHeader, optyHTMLHeader);
		}
		if (!isset($garrUpdatedPropsWithVars["Sections"]["DataSourceFilter"])) {
			$this->DataSourceFilter = $this->TranslateVariableQuestionNumbersByIDs($this->DataSourceFilter, optyFormula);
		}
	}
	//@JAPR
	
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	function save($bJustMobileTables = false, $bCopy=false) {
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		global $form;
		
		$currentDate = date("Y-m-d H:i:s");
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Al realizar el grabado del objeto (pero no copiado) se deben actualizar a la par todas las configuraciones que pudieran tener variables para arreglar los IDs mal grabados
		//Se debe realizar antes de la asignación de LastModAdminVersion o de lo contrario ya no tendría efecto pues ya consideraría que está grabado con la versión corregida
		if (!$bCopy) {
			$this->fixAllPropertiesWithVarsNotSet();
		}
		
		$isNewObj = $this->isNewObject();
		if ($this->SectionType != sectInline) {
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			//En versión v6 la referencia al selector no se remueve aunque quede inconsistente, esto para evitar que el proceso del cambio de tipo de sección la vuelva a crear
			if (getMDVersion() < esveFormsv6) {
				$this->SelectorQuestionID = 0;
			}
			
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento, así que si no es sección inline, tiene
			//que tratarse de una sección Maestro-detalle (aunque aún así requiere que SectionType == sectMasterDet)
			//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
			$this->DisplayMode = sdspDefault;
			$this->SwitchBehaviour = 0;
			//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
			//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
			$this->ShowSelector = 0;
			$this->ShowAsQuestion = 0;
			//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
			$this->SourceQuestionID = 0;
			$this->SourceSectionID = 0;
		}
		//@JAPR 2014-09-26: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		else {
			//Si es una sección Inline pero se va a mostrar fusionada a otra sección, entonces debe deshabilitar algunas configuraciones que no
			//tiene caso que aplique
			if ($this->ShowAsQuestion) {
				$this->ShowInNavMenu = 0;
				//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
				//Ahora esta propiedad puede configurar el tipo de despliegue de Maestro-detalle. sdspDefault correspondería a dicho comportamiento. Si se mostrará como pregunta, sólo
				//puede tratarse de una sección Inline
				$this->DisplayMode = sdspInline;
			}
			//Si la sección no habilitará a la pregunta selectora, entonces deshabilita configuraciones que no tiene caso que aplique
			if (!$this->ShowSelector) {
				$this->SwitchBehaviour = 0;
			}
			
			//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
			if (isset($this->newTypeOfFilling)) {
				//Si viene asignado el tipo de llenado y se trata de una sección tipo Inline, se tiene que evaluar el tipo
				//para reemplazar otras configuraciones que ya no aplicarían
				switch ($this->newTypeOfFilling) {
					case tofCatalog:
						if ($this->CatalogID > 0 && $this->CatMemberID > 0) {
							$this->StrSelectOptions = '';
							//@JAPR 2015-05-05: Corregido un bug, no podía cambiar de tipo de llenado correctamente (#Q3RYRG)
							$this->SourceSectionID = 0;
							$this->SourceQuestionID = 0;
							//@JAPR
						}
						break;
					
					case tofMCHQuestion:
						$this->SourceSectionID = 0;
						$this->CatalogID = 0;
						$this->CatMemberID = 0;
						$this->StrSelectOptions = '';
						break;
					
					case tofInlineSection:
						$this->SourceQuestionID = 0;
						$this->CatalogID = 0;
						$this->CatMemberID = 0;
						$this->StrSelectOptions = '';
						break;
					
					case tofFixedAnswers:
					default:
						$this->CatalogID = 0;
						$this->CatMemberID = 0;
						//@JAPR 2015-05-05: Corregido un bug, no podía cambiar de tipo de llenado correctamente (#Q3RYRG)
						$this->SourceSectionID = 0;
						$this->SourceQuestionID = 0;
						//@JAPR
						break;
				}
				
				//@JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
				///Si se cambió el tipo de llenado a algo que no es un catálogo, a partir de ahora se limpiará el filtro dinámico de la pregunta y la lista de atributos que hubiera tenido
				//asociada así como el DataSource (requiere una contraparte en surveyExt.inc.php para limpiar localmente en memoria dichas referencias)
				$this->DataSourceFilter = '';
				$this->DataSourceID = 0;
				$this->DataSourceMemberID = 0;
				//@JAPR
			}
			//@JAPR
		}
		
		//@JAPR 2014-07-28: Corregido un bug, se validó que las secciones que no son tipo Recap no se queden
		//ligadas a una SectionFormID, lo cual en el código las convertiría automáticamente en secciones tipo Recap
		if ($this->SectionType != sectRecap) {
			$this->SectionFormID = 0;
		}
		//@JAPR
		
		/*if($this->SectionType != sectMasterDet){
			$this->SummaryInMDSection = 0;
		}*/
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		
		if (getMDVersion() >= esvEditorHTML && $this->EditorType == 1)
		{
			//Los campos HTML del editor anterior serán sustituidos con el código HTML generado con el nuevo editor
			if($this->FormattedTextDes!="" || $this->FormattedTextiPodDes !="" || $this->FormattedTextiPadDes !=""
			|| $this->FormattedTextiPadMiniDes!="" || $this->FormattedTextiPhoneDes!="" || $this->FormattedTextCelDes!=""
			|| $this->FormattedTextTabletDes!=""
			|| $this->HTMLHeaderDes!="" || $this->HTMLHeaderiPodDes !="" || $this->HTMLHeaderiPadDes !=""
			|| $this->HTMLHeaderiPadMiniDes!="" || $this->HTMLHeaderiPhoneDes!="" || $this->HTMLHeaderCelDes!=""
			|| $this->HTMLHeaderTabletDes!=""
			|| $this->HTMLFooterDes!="" || $this->HTMLFooteriPodDes !="" || $this->HTMLFooteriPadDes !=""
			|| $this->HTMLFooteriPadMiniDes!="" || $this->HTMLFooteriPhoneDes!="" || $this->HTMLFooterCelDes!=""
			|| $this->HTMLFooterTabletDes!="")
			{
				$this->FormattedText = $this->FormattedTextDesHTML;
				$this->FormattedTextiPod = $this->FormattedTextiPodDesHTML;
				$this->FormattedTextiPad = $this->FormattedTextiPadDesHTML;
				$this->FormattedTextiPadMini = $this->FormattedTextiPadMiniDesHTML;
				$this->FormattedTextiPhone = $this->FormattedTextiPhoneDesHTML;
				$this->FormattedTextCel = $this->FormattedTextCelDesHTML;
				$this->FormattedTextTablet = $this->FormattedTextTabletDesHTML;
				
				$this->HTMLHeader = $this->HTMLHeaderDesHTML;
				$this->HTMLHeaderiPod = $this->HTMLHeaderiPodDesHTML;
				$this->HTMLHeaderiPad = $this->HTMLHeaderiPadDesHTML;
				$this->HTMLHeaderiPadMini = $this->HTMLHeaderiPadMiniDesHTML;
				$this->HTMLHeaderiPhone = $this->HTMLHeaderiPhoneDesHTML;
				$this->HTMLHeaderCel = $this->HTMLHeaderCelDesHTML;
				$this->HTMLHeaderTablet = $this->HTMLHeaderTabletDesHTML;
				
				$this->HTMLFooter = $this->HTMLFooterDesHTML;
				$this->HTMLFooteriPod = $this->HTMLFooteriPodDesHTML;
				$this->HTMLFooteriPad = $this->HTMLFooteriPadDesHTML;
				$this->HTMLFooteriPadMini = $this->HTMLFooteriPadMiniDesHTML;
				$this->HTMLFooteriPhone = $this->HTMLFooteriPhoneDesHTML;
				$this->HTMLFooterCel = $this->HTMLFooterCelDesHTML;
				$this->HTMLFooterTablet = $this->HTMLFooterTabletDesHTML;
			}
		}
		
		//@JAPR 2015-07-11: Rediseñado el Admin con DHTMLX
		//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
		$this->setRepetitions();
		//@JAPR
		
		if ($this->isNewObject()) {
			$this->CreationAdminVersion = $this->LastModAdminVersion;
			
			//Obtener el SectionID
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(SectionID)", "0")." + 1 AS SectionID".
						" FROM SI_SV_Section";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->SectionID = (int)$aRS->fields["sectionid"];
			
			//Obtener el SectionNumber
			$sqlNumber =  "SELECT ".
							$this->Repository->DataADOConnection->IfNull("MAX(SectionNumber)", "0")." + 1 AS SectionNumber".
							" FROM SI_SV_Section WHERE SurveyID = ".$this->SurveyID;
			$aRS = $this->Repository->DataADOConnection->Execute($sqlNumber);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
			}
			
			$this->SectionNumber = (int)$aRS->fields["sectionnumber"];
			//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
			$strAdditionalFields = "";
			$strAdditionalValues = "";
			if (getMDVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalFields .= ',NextSectionID';
				$strAdditionalValues .= ','.$this->NextSectionID;
			}
			//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
			if (getMDVersion() >= esvAutoRedrawFmtd) {
				$strAdditionalFields .= ',AutoRedraw';
				$strAdditionalValues .= ','.$this->AutoRedraw;
			}
			//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
			if (getMDVersion() >= esvShowSectionCondition) {
				$strAdditionalFields .= ',ShowCondition';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->ShowCondition);
			}
			//@JAPR 2014-05-19: Agregado el tipo de sección Inline
			//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
			if (getMDVersion() >= esvInlineSection) {
				$strAdditionalFields .= ',SelectorQuestionID,DisplayMode,SwitchBehaviour';
				$strAdditionalValues .= ','.$this->SelectorQuestionID.','.$this->DisplayMode.','.$this->SwitchBehaviour;
			}
			//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
			if (getMDVersion() >= esvSectionFmtdHeadFoot) {
				$strAdditionalFields .= ',HTMLFooter,HTMLHeader';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->HTMLFooter).
					','.$this->Repository->DataADOConnection->Quote($this->HTMLHeader);
			}
			//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			if (getMDVersion() >= esvFixedInlineSections) {
				$strAdditionalFields .= ',OptionsText';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->StrSelectOptions);
			}
			//@MABH 2014-06-17
			if (getMDVersion() >= esvSectionRecap) {
				$strAdditionalFields .= ',SectionFormID';
				$strAdditionalValues .= ','.$this->SectionFormID;
			}

			//Agregado el SummaryInMDSection
			if (getMDVersion() >= esvShowInOnePageMD) {
				$strAdditionalFields .= ',SummaryInMDSection';
				$strAdditionalValues .= ','.$this->SummaryInMDSection;
			}

			//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
			if (getMDVersion() >= esvSurveySectDisableOpts) {
				$strAdditionalFields .= ',ShowInNavMenu';
				$strAdditionalValues .= ','.$this->ShowInNavMenu;
			}
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalFields .= ',CreationUserID,LastUserID,CreationDateID,LastDateID,CreationVersion,LastVersion';
				$strAdditionalValues .= ','.$_SESSION["PABITAM_UserID"].','.$_SESSION["PABITAM_UserID"].
					','.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					','.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					','.$this->LastModAdminVersion.','.$this->LastModAdminVersion;
			}
			//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
			//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
			if (getMDVersion() >= esvInlineMerge) {
				$strAdditionalFields .= ',ShowSelector,ShowAsQuestion';
				$strAdditionalValues .= ','.$this->ShowSelector.','.$this->ShowAsQuestion;
			}
			//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
			if (getMDVersion() >= esvInlineTypeOfFilling) {
				$strAdditionalFields .= ',SourceQuestionID,SourceSectionID';
				$strAdditionalValues .= ','.$this->SourceQuestionID.','.$this->SourceSectionID;
			}
			//@JAPR
			//Sep-2014: Agregado el nuevo campo para definir que editor se utiliza
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalFields .= ', EditorType, FormattedTextDes, HTMLHeaderDes, HTMLFooterDes';
				$strAdditionalValues .= ', '.$this->EditorType.", ".$this->Repository->DataADOConnection->Quote($this->FormattedTextDes).", ".$this->Repository->DataADOConnection->Quote($this->HTMLHeaderDes).", ".$this->Repository->DataADOConnection->Quote($this->HTMLFooterDes);
			}
			//2015.02.10 JCEM #9T6ASP
			if (getMDVersion() >= esvMinRecordsToSelect) {
				$strAdditionalFields .= ', MinRecordsToSelect';
				$strAdditionalValues .= ', '.$this->MinRecordsToSelect;
			}
			//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
			if (getMDVersion() >= esvAdminWYSIWYG) {
				$strAdditionalFields .= ', SectionNameHTML, ValuesSourceType';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->SectionNameHTML).', '.
					$this->ValuesSourceType;
			}
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ', DataSourceID, DataSourceMemberID, DataSourceFilter';
				$strAdditionalValues .= ', '.$this->DataSourceID.', '.$this->DataSourceMemberID.', '.
					$this->Repository->DataADOConnection->Quote($this->DataSourceFilter);
			}
			//@RTORRES 2015-08-28: Agregado ocultar boton next,back
			if (getMDVersion() >= esveHideNavButtonsSection) {
				$strAdditionalFields .= ', HideNavButtons';
				$strAdditionalValues .= ', '.$this->HideNavButtons;
			}
			//OMMC 2016-01-19: Agregado para guardar el estado del editor HTML para header y footer.
			if(getMDVersion() >= esvHTMLEditorState){
				$strAdditionalFields .= ', HTMLHEditorState, HTMLFEditorState';
				$strAdditionalValues .= ', '.$this->HTMLHEditorState.','.$this->HTMLFEditorState;
			}
			
			//RV Feb2016: Agregar el campo que indica cual es el ID de la sección a partir de la cual se copio esta sección (cuando sea el caso)
			//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			//$form sólo estará asignada si se trata de un proceso de generación de forma de desarrollo iniciado por processCopyForm.php
			if (getMDVersion() >= esvCopyForms) {
				$strAdditionalFields .= ', SourceID';
				if (isset($form) && $form->ActionType == devAction) {
					$strAdditionalValues .= ', '.$this->CopiedSectionID;
				}
				else {
					$strAdditionalValues .= ', 0';
				}
			}
			//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
			if (getMDVersion() >= esvTableSectDescRow) {
				$strAdditionalFields .= ', UseDescriptorRow';
				$strAdditionalValues .= ', '.$this->UseDescriptorRow;
			}
			//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			if (getMDVersion() >= esvSketchPlus) {
				$strAdditionalFields .= ', XPosQuestionID, YPosQuestionID, ItemNameQuestionID';
				$strAdditionalValues .= ', '.$this->XPosQuestionID.', '.$this->YPosQuestionID.', '.$this->ItemNameQuestionID;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_Section (".
					"SurveyID".
					",SectionID".
					",SectionName".
					",SectionNumber".
					",SectionType".
					",CatalogID".
					",CatMemberID".
					",FormattedText".
					",SendThumbnails".
					",EnableHyperlinks".
					$strAdditionalFields.
					") VALUES (".
					$this->SurveyID.
					",".$this->SectionID.
					",".$this->Repository->DataADOConnection->Quote($this->SectionName).
					",".$this->SectionNumber.
					",".$this->SectionType.
					",".$this->CatalogID.
					",".$this->CatMemberID.
					",".$this->Repository->DataADOConnection->Quote($this->FormattedText).
					",".$this->SendThumbnails.
					",".$this->EnableHyperlinks.
					$strAdditionalValues.
					")";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Inserting section: {$sql}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
		}
		else {
			//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			if ($this->SectionType == sectInline && $this->CatalogID > 0 && $this->CatMemberID > 0) {
				$this->StrSelectOptions = '';
			}
			
			//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
			$strAdditionalValues = "";
			if (getMDVersion() >= esvMultiDynamicAndStandarization) {
				$strAdditionalValues .= ', NextSectionID = '.$this->NextSectionID;
			}
			//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
			if (getMDVersion() >= esvAutoRedrawFmtd) {
				$strAdditionalValues .= ', AutoRedraw = '.$this->AutoRedraw;
			}
			//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
			if (getMDVersion() >= esvShowSectionCondition) {
				$strAdditionalValues .= ', ShowCondition = '.$this->Repository->DataADOConnection->Quote($this->ShowCondition);
			}
			//@JAPR 2014-05-19: Agregado el tipo de sección Inline
			//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
			if (getMDVersion() >= esvInlineSection) {
				$strAdditionalValues .= ', SelectorQuestionID = '.$this->SelectorQuestionID.
					', DisplayMode = '.$this->DisplayMode.
					', SwitchBehaviour = '.$this->SwitchBehaviour;
			}
			//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
			if (getMDVersion() >= esvSectionFmtdHeadFoot) {
				$strAdditionalValues .= ', HTMLFooter = '.$this->Repository->DataADOConnection->Quote($this->HTMLFooter).
					', HTMLHeader = '.$this->Repository->DataADOConnection->Quote($this->HTMLHeader);
			}
			//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			if (getMDVersion() >= esvFixedInlineSections) {
				$strAdditionalValues .= ', OptionsText = '.$this->Repository->DataADOConnection->Quote($this->StrSelectOptions);
			}
			//@MABH 2014-06-17
			if (getMDVersion() >= esvSectionRecap) {
				$strAdditionalValues .= ', SectionFormID = '.$this->SectionFormID;
			}
			//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
			if (getMDVersion() >= esvSurveySectDisableOpts) {
				$strAdditionalValues .= ', ShowInNavMenu = '.$this->ShowInNavMenu;
			}
			//Agregado el SummaryInMDSection para q se muestre el summary en las secciones maestro detalle
			if (getMDVersion() >= esvShowInOnePageMD) {
				$strAdditionalValues .= ', SummaryInMDSection = '.$this->SummaryInMDSection;
			}
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
			//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
			if (getMDVersion() >= esvInlineMerge) {
				$strAdditionalValues .= ', ShowSelector = '.$this->ShowSelector.
					', ShowAsQuestion = '.$this->ShowAsQuestion;
			}
			//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
			if (getMDVersion() >= esvInlineTypeOfFilling) {
				$strAdditionalValues .= ', SourceQuestionID = '.$this->SourceQuestionID.
					', SourceSectionID = '.$this->SourceSectionID;
			}
			//@JAPR
			//Sep-2014: Agregado el nuevo campo para definir que editor se utiliza
			if (getMDVersion() >= esvEditorHTML)
			{
				$strAdditionalValues .= ', EditorType = '.$this->EditorType
				.', FormattedTextDes = '.$this->Repository->DataADOConnection->Quote($this->FormattedTextDes)
				.', HTMLHeaderDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLHeaderDes)
				.', HTMLFooterDes = '.$this->Repository->DataADOConnection->Quote($this->HTMLFooterDes);
			}
			//2015.02.10 JCEM #9T6ASP
			if (getMDVersion() >= esvMinRecordsToSelect) {
				$strAdditionalValues .= ', MinRecordsToSelect = '.$this->MinRecordsToSelect;
			}
			//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
			if (getMDVersion() >= esvAdminWYSIWYG) {
				$strAdditionalValues .= ', SectionNameHTML = '.$this->Repository->DataADOConnection->Quote($this->SectionNameHTML).
					', ValuesSourceType = '.$this->ValuesSourceType;
			}
			//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalValues .= ', DataSourceID = '.$this->DataSourceID.', DataSourceMemberID = '.$this->DataSourceMemberID.
					', DataSourceFilter = '.$this->Repository->DataADOConnection->Quote($this->DataSourceFilter);
			}
			//@RTORRES 2015-08-28: Agregado ocultar boton next,back
			if (getMDVersion() >= esveHideNavButtonsSection) {
				$strAdditionalValues .= ', HideNavButtons = '.$this->HideNavButtons;
			}
			//OMMC 2016-01-19: Agregado para guardar el estado del editor HTML para header y footer.
			if(getMDVersion() >= esvHTMLEditorState){
				$strAdditionalValues .= ', HTMLHEditorState= '.$this->HTMLHEditorState.', HTMLFEditorState='.$this->HTMLFEditorState;
			}
			//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
			if (getMDVersion() >= esvTableSectDescRow) {
				$strAdditionalValues .= ', UseDescriptorRow = '.$this->UseDescriptorRow;
			}
			//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			if (getMDVersion() >= esvSketchPlus) {
				$strAdditionalValues .= ', XPosQuestionID = '.$this->XPosQuestionID.
					', YPosQuestionID = '.$this->YPosQuestionID.', ItemNameQuestionID = '.$this->ItemNameQuestionID;
			}
			//@JAPR
			$sql = "UPDATE SI_SV_Section SET 
					SectionName = ".$this->Repository->DataADOConnection->Quote($this->SectionName)." 
					, SectionType = ".$this->SectionType." 
					, CatalogID = ".$this->CatalogID." 
					, CatMemberID = ".$this->CatMemberID." 
					, FormattedText = ".$this->Repository->DataADOConnection->Quote($this->FormattedText)." 
					, SendThumbnails = ".$this->SendThumbnails." 
					, EnableHyperlinks = ".$this->EnableHyperlinks.
					$strAdditionalValues." 
				WHERE SectionID = ".$this->SectionID." AND SurveyID = ".$this->SurveyID;
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Updating section: {$sql}", 2, 0, "color:blue;");
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
			
			//@JAPR 2015-08-20: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6
			if ($this->CreationAdminVersion >= esveFormsv6) {
				//Al editar si se eliminó la referencia al catálogo, debe eliminar la instancia en la metadata
				if ($this->CatalogID <= 0 && $this->CatalogIDOld > 0) {
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogIDOld);
					if (!is_null($objCatalog)) {
						$objCatalog->remove();
					}
				}
			}
			//@JAPR
		}
		if(!$bCopy) //Inicio de if temporal de !bCopy
		{
			//@AAL 27/05/2015: En versiones de EformsV6 las Dimensión ya no aplican
			if($this->CreationAdminVersion < esveFormsv6){
				//@JAPR 2013-02-01: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				//Si la encuesta utilizará catálogos con dimensiones independientes, invoca al nuevo método que asocia a dichas dimensiones al modelo
				$this->createDissociatedCatalogDimension();
			}
			$this->createSelectorQuestion();
			
			//@AAL 27/05/2015: En versiones de EformsV6 las Dimensión ya no aplican
			if($this->CreationAdminVersion < esveFormsv6){
				//@JAPR 2014-09-23: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
				$this->createSectionDimension();
			}
			
			//@JAPR 2014-07-30: Agregado el Responsive Design
			//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			if (getMDVersion() < esveFormsv6 ) {
				if (getMDVersion() >= esvResponsiveDesign) {
					$this->saveResponsiveDesignProps();
				}
			}
			
			//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			$this->createSketchDataQuestions();
			//@JAPR
		}
		//@JAPR 2015-08-21: Modificado para utilizar DataSources en lugar de catálogos originales
		//Si está configurada como sección de catálogo y hay un DataSource asignado, debe crear el catálogo correspondiente al mismo
		if ($this->ValuesSourceType == tofCatalog && $this->DataSourceID > 0 && $this->DataSourceMemberID > 0 && $this->CreateCat) {
			//Si el DataSource utilizado ha cambiado, se deben resetear todas las configuraciones que utilizaban el DataSource en esta sección porque quedarían inconsistentes (quien
			//ha invocado al grabado debe hacer también una actualización de la definición)
			$blnChangeDataSource = false;
			if($bCopy)
			{	//RV Sep2016: Cuando se está copiando una forma se inicializan estos valores para crear un catálogo nuevo para la sección
				$this->CatalogID = 0;
				$this->CatMemberID = 0;
				$this->CatalogIDOld = 0;
			}
			//@JAPR 2015-07-31: Corregido un bug, toda esta validación es correcta excepto si apenas se está definiendo el catálogo, porque en ese caso
			//removía las configuraciones de definidas durante la creación
			if ($this->DataSourceID != $this->DataSourceIDOld && (!$isNewObj)) {
				$blnChangeDataSource = true;
				$this->DataSourceMemberID = 0;
				$this->CatMemberID = 0;
			}
			
			$intCatalogID = $this->CatalogID;
			$intCatMemberID = $this->DataSourceMemberID;
			$arrMappedAttribs = array();
			//@JAPR 2015-07-29: Corregido un bug, al corregir el problema de remapeo de atributos, había faltado entrar a esta condición si hay un atributo seleccionado
			if ($intCatMemberID > 0) {
				$arrMappedAttribs['DataMemberID'] = array('DataSourceMemberID' => $intCatMemberID);
			}
			
			$arrDataSourceMembersIDs = array();
			if ($this->DataSourceMemberID > 0) {
				$arrDataSourceMembersIDs[] = $this->DataSourceMemberID;
			}
			
			$arrReturn = BITAMCatalog::CreateUpdateCatalogFromDataSource($this->Repository, $this->DataSourceID, $arrDataSourceMembersIDs, $intCatalogID, $arrMappedAttribs);
			$intCatMemberID = 0;
			if ($arrReturn !== false && is_array($arrReturn)) {
				$intCatalogID = (int) @$arrReturn["CatalogID"];
				$intCatMemberID = (int) @$arrReturn["DataMemberID"];
			}
			//@JAPR 2015-07-29: Removida la validación de IDs positivos, ya que se puede limpiar la configuración y debe hacer el Update (en caso donde cambie el DataSource por ejemplo)
			if ($blnChangeDataSource || $intCatalogID != $this->CatalogID || $intCatMemberID != $this->CatMemberID) {
				$strAnd = "";
				$strAdditionalValues = "";
				if ($intCatalogID != $this->CatalogID) {
					$strAdditionalValues .= $strAnd."CatalogID = {$intCatalogID}";
					$strAnd = ", ";
				}
				if ($blnChangeDataSource) {
					//Si el DataSource fue modificado, entonces es irrelevante lo que se había preparado para actualizar, ya que todos los IDs de configuraciones de atributos se resetean
					//a 0 exceptuando el catálogo, el cual se debe ajustar al nuevo DataSource
					$strAdditionalValues .= $strAnd."CatMemberID = 0, DataSourceMemberID = 0";
				}
				else {
					if ($intCatMemberID != $this->CatMemberID) {
						$strAdditionalValues .= $strAnd."CatMemberID = {$intCatMemberID}";
						$strAnd = ", ";
					}
				}
				
				if ($strAdditionalValues) {
					//En este caso si se logró crear un catálogo a partir del DataSource, así que actualiza la información
					$sql = "UPDATE SI_SV_Section SET {$strAdditionalValues} 
						WHERE SectionID = {$this->SectionID}";
					$this->Repository->DataADOConnection->Execute($sql);
				}
			}
		}
		//@JAPR
		
		//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, Se crea la Tabla paralela correspondientes a las Secciones
		$this->createDataTablesAndFields($isNewObj);
		//@JAPR 2015-08-25: Corregido un bug, había faltado grabar el filtro dinámico
		if(!$bCopy)
		{
			$this->saveDynamicFilter();
		}
		//@JAPR
		
		return $this;
	}
	
	//@AAL 23/06/2015 Agregado para EformsV6 y siguientes, por cada sección se crea la tabla SVeFormsSection_XXX
	//Para todas las secciones que tienen más de una respuesta o registro.
	//El parámetro $bIsNew nos indicará si se estaba creando o no el objeto, ya que siempre que se invoca ya tiene activado un SectionID, así que no hay forma de identificar si se estaba
	//creando o no realmente con la propiedad isNewObject en este punto
	function createDataTablesAndFields($bIsNew = false) {
		if ($this->CreationAdminVersion < esveFormsv6) {
			return;
		}
		
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$this->setTableAndFieldsProperties();
		//@JAPR
		
    	/*Se crea la tabla SVeForms_####_Section_xxxx correspondientes a las secciones que no sean Estándar/Formateadas */
    	if ($this->SectionType != sectNormal && $this->SectionType != sectFormatted) {
    		$strTableName = $this->SectionTable;
			$strSectionDescKeyField = "";
			$sql = "CREATE TABLE {$strTableName} (
				SurveyKey INTEGER NOT NULL, 
				".BITAMSection::$KeyField." INTEGER NOT NULL AUTO_INCREMENT, 
				PRIMARY KEY (`".BITAMSection::$KeyField."`)
			)";
			$this->Repository->DataADOConnection->Execute($sql);
    	}
		
		//@JAPR 2015-07-13: Corregido un bug, se había introducido bien esta validación, pero estaba en el if previo que impedía que al cambiar a sección estándar se aplicara (#X244PW)
		//Si la sección cambió de tipo de un estándar a uno múltiple o visceversa, debe volver a generar las columnas de las preguntas en la tabla de sección que le corresponda
		//a su nuevo tipo. Si cambió de un múltiple a otro múltiple no hay problema, porque en ese caso ya tiene una tabla independiente propia y entre todas las secciones múltiples
		//tiene la misma estructura (No se consideran las secciones formateadas porque van a desaparecer, ahora serán igual que una sección estándar pero con header/footer personalizado
		//y sin preguntas
		if ($this->SectionType != $this->SectionTypeOld && ($this->SectionType == sectNormal || $this->SectionTypeOld == sectNormal)) {
			$aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
			foreach ($aQuestionCollection->Collection as $ObjQuestion) {
				//@JAPR 2015-07-14: Corregido un bug, al cambiar el tipo de sección se debe forzar a que se considere nueva la pregunta para que agregue sus columnas en la nueva tabla
				//de sección, ya que de lo contrario sólo lo hace al considerarlo necesario según su definición, y esa no ha cambiado al modificar la sección (#NO9YDB)
				$ObjQuestion->createDataTablesAndFields(true);
				//@JAPR
			}
		}
		
		if ($this->SectionType == sectInline) {
			//@JAPR 2015-07-11: Dada la limitante de tablas en un join en MySQL, las tablas de sección ya no tendrán una subtabla con sus descripciones, ahora grabarán los campos
			//que requieran en la misma tabla
			/*
			//Se crean las tablas correspondientes a la Descripción de las secciones con formato SVeForms_####_Section_xxxx_Desc
			$strTableName = $this->SectionDescTable;
			$sql = "CREATE TABLE {$strTableName} (
				{$this->DescKeyField} INTEGER NOT NULL AUTO_INCREMENT, 
				{$this->DescField} VARCHAR(255), 
				PRIMARY KEY (`{$this->DescKeyField}`)
			)";
			$this->Repository->DataADOConnection->Execute($sql);
			*/
			
			//Si la sección es Dinámica o Inline basada en catálogo ences se crea la columna Attributos_XXXX
			//@JAPRWarning: Faltan crear los atributos del catálogo de la sección
			/*
			if (($this->SectionType == sectDynamic || $this->SectionType == sectInline) && $this->ValuesSourceType == 1){
				$sql = "ALTER TABLE " . $strTableName . " ADD Attribute_" . $this->CatMemberID;
				$this->Repository->DataADOConnection->Execute($sql);	
			}
			*/
			
			//@JAPR 2015-08-26: Agregadas las columnas para las secciones Inline, ya que debido a que finalmente no se normalizaron todas las tablas como estaba pensado, se había
			//olvidado hacer este paso y no se estaban grabando los datos de las secciones
			//Agrega el campo con la descripción del atributo específico usado en la sección y es el valor para cuando NO es de catálogo, en esos casos siempre es un único valor
			//pues no hay mas "atributos" que desplegar como con los catálogos
			$sql = "ALTER TABLE {$strTableName} ADD {$this->DescField} VARCHAR(255)";
			$this->Repository->DataADOConnection->Execute($sql);
			
			if ($this->ValuesSourceType == tofCatalog) {
				if ($this->DataSourceID > 0) {
					$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $this->DataSourceID);
					if (!is_null($objDataSource)) {
						$objDataSource->regenerateMemberColumns($strTableName);
					}
				}
			}
			//@JAPR
		}
	}
	
	//@JAPR 2014-07-30: Agregado el Responsive Design
	/* Graba las propiedades que corresponden al Responsive Design de forma especial, ya que se graban en la misma tabla pero variando el ID
	en base al tipo de dispositivo del que se trate
	//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	function saveResponsiveDesignProps() {
		return true;
		//Se debe verificar si este objeto ya tenía grabada su propiedad de Responsive Design para cierto tipo de dispositivo, de tal forma
		//que se pueda armar un INSERT o un UPDATE según el caso (No es necesario grabar la propiedad Default pues esa está directamente
		//en la tabla del objeto)
		for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
			$strFormattedText = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("FormattedText", $intDeviceID));
			$strHTMLHeader = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLHeader", $intDeviceID));
			$strHTMLFooter = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLFooter", $intDeviceID));
			$strEmpty = $this->Repository->DataADOConnection->Quote("");
			
			//Sep-2014
			$strUpdate = "";
			$strInsertFields = "";
			$strInsertValues = "";
			if (getMDVersion() >= esvEditorHTML)
			{
				$strFormattedTextDes = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("FormattedText", $intDeviceID, false, 1));
				$strHTMLHeaderDes = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLHeader", $intDeviceID, false, 1));
				$strHTMLFooterDes = $this->Repository->DataADOConnection->Quote($this->getResponsiveDesignProperty("HTMLFooter", $intDeviceID, false, 1));
				$strUpdate = ", FormattedTextDes = ". $strFormattedTextDes.", HTMLHeaderDes = ". $strHTMLHeaderDes.", HTMLFooterDes = ". $strHTMLFooterDes;
				$strInsertFields = ", FormattedTextDes, HTMLHeaderDes, HTMLFooterDes";
				$strInsertValues = " ,".$strFormattedTextDes." ,".$strHTMLHeaderDes." ,".$strHTMLFooterDes;
			}
			
			if (isset($this->ResponsiveDesignProps[$intDeviceID])) {
				$sql = "UPDATE SI_SV_SurveyHTML SET 
						FormattedText = {$strFormattedText} 
						, HTMLHeader = {$strHTMLHeader} 
						, HTMLFooter = {$strHTMLFooter} 
						, ImageText = {$strEmpty} 
						{$strUpdate}
					WHERE ObjectType = ".otySection." AND DeviceID = {$intDeviceID} AND ObjectID = {$this->SectionID}";
			}
			else {
				$sql = "INSERT INTO SI_SV_SurveyHTML (SurveyID, ObjectType, ObjectID, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText {$strInsertFields} ) 
					VALUES ({$this->SurveyID}, ".otySection.", {$this->SectionID}, {$intDeviceID}, 
						{$strFormattedText}, {$strHTMLHeader}, {$strHTMLFooter}, {$strEmpty} {$strInsertValues})";
			}
			@$this->Repository->DataADOConnection->Execute($sql);
		}
	}
	
	//@JAPR 2014-05-19: Agregado el tipo de sección Inline
	//Si la sección es en línea, verifica si ya está asignada y/o creada la pregunta selector de la sección, si no es así o ya no se encuentra, 
	//entonces crea una nueva pregunta con el mismo formato de Simple Choice con sólo las opciones de Si/No y la asigna a la sección
	function createSelectorQuestion() {
		if ($this->SectionType != sectInline) {
			return;
		}
		
		//Verifica que la pregunta realmente siga existiendo
		$blnCreateSelectorQuestion = true;
		if ($this->SelectorQuestionID > 0) {
			$objQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->SelectorQuestionID);
			if (!is_null($objQuestion)) {
				$blnCreateSelectorQuestion = false;
			}
		}
		
		//En este caso no hay necesidad de crear la pregunta selector pues ya existe
		if (!$blnCreateSelectorQuestion) {
			return;
		}
		
		//Si llega hasta aquí, entonces se debe crear la nueva pregunta selector de esta sección
		$objQuestion = BITAMQuestion::NewInstance($this->Repository, $this->SectionID);
		$objQuestion->QTypeID = qtpSingle;
		$objQuestion->QuestionText = translate("Selector");
		$objQuestion->StrSelectOptions = translate("Yes")."\r\n".translate("No");
		$objQuestion->QDisplayMode = dspVertical;	//Realmente no aplica, se presentará como CheckBox
		$objQuestion->IsMultiDimension = 0;
		$strAttributeName = "S".$this->SectionID."-Selector";
		$intNumAttempt = 0;
		
		//Validación de nombres duplicados de atributo, incrementará el contador cada vez que se identifique como ya usado
		do {
			//@JAPR 2014-09-23: Corregido un bug, estaba dejando el nombre como un número en lugar de concatenar un consecutivo para nombres repetidos
			$objQuestion->AttributeName = $strAttributeName.(($intNumAttempt)?$intNumAttempt:'');
			//@JAPR
			//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
			//ya dado de alto como nom_logico en la tabla si_descrip_enc
			$exist = BITAMSurvey::existAttribNameInThisSurvey($this->Repository, $this->SurveyID, $objQuestion->AttributeName);
			$existCatName = BITAMCatalog::existAttribNameInCatalogs($this->Repository, $objQuestion->AttributeName);
			$blnRepeatedName = $exist || $existCatName;
			$intNumAttempt++;
		} while($blnRepeatedName);
		$objQuestion->save();
		
		//Asigna la pregunta como selector de la sección
		if ($objQuestion->QuestionID > 0) {
			$sql = "UPDATE SI_SV_Section SET SelectorQuestionID = {$objQuestion->QuestionID} 
				WHERE SectionID = {$this->SectionID}";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->SelectorQuestionID = $objQuestion->QuestionID;
			$objQuestion->IsSelector = true;
		}
	}
	
	//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	//Si la sección es Maestro-detalle, verifica si ya están asignadas y/o creadas las preguntas para almacenar la posición X,Y de cada registro siempre y cuando la sección sea utilizada, 
	//como parte de una opción de respuesta de pregunta Sketch+
	function createSketchDataQuestions() {
		if (getMDVersion() < esvSketchPlus || $this->SectionType != sectMasterDet) {
			return;
		}
		
		//Verifica que la sección realmente esté siendo utilizada para una pregunta Sketch+ como una sección de captura de sus elementos colocados
		//@JAPRWarning: Esta parte se podría optimizar probablemente, sin embargo como solo es un dato que se necesita al momento de grabar la sección, no es algo que se vaya a repetir
		//constantemente durante la carga así que por ahora se dejará sin optimización de llave, índice o caché hasta que se compruebe que representa un problema
		$sql = "SELECT A.QuestionID 
			FROM SI_SV_Question A 
			WHERE A.SurveyID = {$this->SurveyID} AND A.QTypeID = ".qtpSketchPlus." AND A.QuestionID IN (
				SELECT QuestionID 
				FROM SI_SV_Qanswers 
				WHERE NextSection = {$this->SectionID} 
				GROUP BY QuestionID
			)";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF) {
			//En caso de fallo o no existir alguna pregunta donde esta sección se utilice como sección de captura para Sketch+, no tiene caso intentar crear de nuevo las preguntas posición
			return;
		}
		
		//Verifica que las preguntas realmente sigan existiendo
		$blnCreateXQuestion = true;
		$blnCreateYQuestion = true;
		$blnCreateItemQuestion = true;
		if ($this->XPosQuestionID > 0) {
			$objQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->XPosQuestionID);
			if (!is_null($objQuestion)) {
				$blnCreateXQuestion = false;
			}
		}
		if ($this->YPosQuestionID > 0) {
			$objQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->YPosQuestionID);
			if (!is_null($objQuestion)) {
				$blnCreateYQuestion = false;
			}
		}
		if ($this->ItemNameQuestionID > 0) {
			$objQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->ItemNameQuestionID);
			if (!is_null($objQuestion)) {
				$blnCreateItemQuestion = false;
			}
		}
		
		//En este caso no hay necesidad de crear la pregunta selector pues ya existe
		if (!$blnCreateXQuestion && !$blnCreateYQuestion && !$blnCreateItemQuestion) {
			return;
		}
		
		//Si llega hasta aquí, entonces se debe crear la nueva pregunta posición de esta sección
		if ( $blnCreateXQuestion ) {
			$objQuestion = BITAMQuestion::NewInstance($this->Repository, $this->SectionID);
			$objQuestion->QTypeID = qtpOpenNumeric;
			$objQuestion->QuestionText = translate("X");
			$objQuestion->IsMultiDimension = 0;
			$objQuestion->ReadOnly = 1;
			$objQuestion->IsInvisible = 1;
			$strAttributeName = $this->SectionID."-CoordX";
			$objQuestion->AttributeName = $strAttributeName;
			//No se realizará este proceso para este tipo de pregunta, si bien para la selector se llevaba a cabo, esa pregunta venía desde versiones previas ligadas a modelos, mientras que
			//esta otra no tendría ese tipo de problemas
			/*$intNumAttempt = 0;
			
			//Validación de nombres duplicados de atributo, incrementará el contador cada vez que se identifique como ya usado
			do {
				//@JAPR 2014-09-23: Corregido un bug, estaba dejando el nombre como un número en lugar de concatenar un consecutivo para nombres repetidos
				$objQuestion->AttributeName = $strAttributeName.(($intNumAttempt)?$intNumAttempt:'');
				//@JAPR
				//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
				//ya dado de alto como nom_logico en la tabla si_descrip_enc
				$exist = BITAMSurvey::existAttribNameInThisSurvey($this->Repository, $this->SurveyID, $objQuestion->AttributeName);
				$existCatName = BITAMCatalog::existAttribNameInCatalogs($this->Repository, $objQuestion->AttributeName);
				$blnRepeatedName = $exist || $existCatName;
				$intNumAttempt++;
			} while($blnRepeatedName);*/
			$objQuestion->save();
			
			//Asigna la pregunta como selector de la sección
			if ($objQuestion->QuestionID > 0) {
				$sql = "UPDATE SI_SV_Section SET XPosQuestionID = {$objQuestion->QuestionID} 
					WHERE SectionID = {$this->SectionID}";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$this->XPosQuestionID = $objQuestion->QuestionID;
				$objQuestion->IsCoordX = true;
			}
		}
		
		if ( $blnCreateYQuestion ) {
			$objQuestion = BITAMQuestion::NewInstance($this->Repository, $this->SectionID);
			$objQuestion->QTypeID = qtpOpenNumeric;
			$objQuestion->QuestionText = translate("Y");
			$objQuestion->IsMultiDimension = 0;
			$objQuestion->ReadOnly = 1;
			$objQuestion->IsInvisible = 1;
			$strAttributeName = $this->SectionID."-CoordY";
			$objQuestion->AttributeName = $strAttributeName;
			//No se realizará este proceso para este tipo de pregunta, si bien para la selector se llevaba a cabo, esa pregunta venía desde versiones previas ligadas a modelos, mientras que
			//esta otra no tendría ese tipo de problemas
			/*$intNumAttempt = 0;
			
			//Validación de nombres duplicados de atributo, incrementará el contador cada vez que se identifique como ya usado
			do {
				//@JAPR 2014-09-23: Corregido un bug, estaba dejando el nombre como un número en lugar de concatenar un consecutivo para nombres repetidos
				$objQuestion->AttributeName = $strAttributeName.(($intNumAttempt)?$intNumAttempt:'');
				//@JAPR
				//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
				//ya dado de alto como nom_logico en la tabla si_descrip_enc
				$exist = BITAMSurvey::existAttribNameInThisSurvey($this->Repository, $this->SurveyID, $objQuestion->AttributeName);
				$existCatName = BITAMCatalog::existAttribNameInCatalogs($this->Repository, $objQuestion->AttributeName);
				$blnRepeatedName = $exist || $existCatName;
				$intNumAttempt++;
			} while($blnRepeatedName);*/
			$objQuestion->save();
			
			//Asigna la pregunta como selector de la sección
			if ($objQuestion->QuestionID > 0) {
				$sql = "UPDATE SI_SV_Section SET YPosQuestionID = {$objQuestion->QuestionID} 
					WHERE SectionID = {$this->SectionID}";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$this->YPosQuestionID = $objQuestion->QuestionID;
				$objQuestion->IsCoordY = true;
			}
		}
		
		if ( $blnCreateItemQuestion ) {
			$objQuestion = BITAMQuestion::NewInstance($this->Repository, $this->SectionID);
			$objQuestion->QTypeID = qtpOpenAlpha;
			$objQuestion->QuestionText = translate("Name");
			$objQuestion->IsMultiDimension = 0;
			$objQuestion->ReadOnly = 1;
			$objQuestion->IsInvisible = 1;
			$strAttributeName = $this->SectionID."-Name";
			$objQuestion->AttributeName = $strAttributeName;
			//No se realizará este proceso para este tipo de pregunta, si bien para la selector se llevaba a cabo, esa pregunta venía desde versiones previas ligadas a modelos, mientras que
			//esta otra no tendría ese tipo de problemas
			/*$intNumAttempt = 0;
			
			//Validación de nombres duplicados de atributo, incrementará el contador cada vez que se identifique como ya usado
			do {
				//@JAPR 2014-09-23: Corregido un bug, estaba dejando el nombre como un número en lugar de concatenar un consecutivo para nombres repetidos
				$objQuestion->AttributeName = $strAttributeName.(($intNumAttempt)?$intNumAttempt:'');
				//@JAPR
				//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
				//ya dado de alto como nom_logico en la tabla si_descrip_enc
				$exist = BITAMSurvey::existAttribNameInThisSurvey($this->Repository, $this->SurveyID, $objQuestion->AttributeName);
				$existCatName = BITAMCatalog::existAttribNameInCatalogs($this->Repository, $objQuestion->AttributeName);
				$blnRepeatedName = $exist || $existCatName;
				$intNumAttempt++;
			} while($blnRepeatedName);*/
			$objQuestion->save();
			
			//Asigna la pregunta como selector de la sección
			if ($objQuestion->QuestionID > 0) {
				$sql = "UPDATE SI_SV_Section SET ItemNameQuestionID = {$objQuestion->QuestionID} 
					WHERE SectionID = {$this->SectionID}";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$this->ItemNameQuestionID = $objQuestion->QuestionID;
				$objQuestion->IsItemName = true;
			}
		}
	}
	
	//@JAPR 2013-02-01: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Si la sección es dinámica y se usan catálogos desasociados, agrega las dimensiones independientes al modelo hasta esta el atributo
	//usado por esta sección dinámica, o si ya tiene preguntas multiple choice hasta el atributo usado por estas
	function createDissociatedCatalogDimension() {
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if ($this->SectionType != sectDynamic && $this->SectionType != sectInline) {
			return;
		}
		
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		if (is_null($aSurveyInstance) || !$aSurveyInstance->DissociateCatDimens) {
			return;
		}
		
		//Primero obtiene la instancia del catálogo junto a sus atributos para asegurarse que está ya migrado al formato de dimensiones
		//independientes, puesto que sin eso no se puede continuar con el proceso
		$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if (is_null($objCatalog)) {
			return;
		}
		
		$objCatalog->generateDissociatedDimensions();
		if (!$objCatalog->DissociateCatDimens) {
			die("(".__METHOD__.") ".translate("The selected catalog is not properly configured to support dissociated dimensions, please select another catalog"));
		}
		
		$intCatMemberID = $this->CatMemberID;
		if ($intCatMemberID > 0) {
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Como hay preguntas múltiple choice en la sección dinámica, obtenemos el ID de atributo siguiente de esta sección
			//@JAPR 2014-05-29: Agregado el tipo de sección Inline
			//En este caso las secciones Inline no necesitan agregar automáticamente el atributo usado por sus preguntas, ya que al crearse ellas
			//mismas lo hacen
			if ($this->SectionType == sectDynamic && $this->ChildCatMemberID > 0) {
				$intCatMemberID = $this->ChildCatMemberID;
			}
			//@JAPR
			
			//Asocia al cubo todas las dimensiones de los atributos hasta encontrar al atributo usado en esta sección o sus multiple-choice
			$objCatalog->addDissociatedCatalogDimensions($this->SurveyID, $intCatMemberID);
		}
	}
	//@JAPR
	
	//@JAPR 2014-09-23: Agregada la dimensión de la sección para las que están definidas como Inline y que no requieren de catálogo
	/* Esta función crerará la dimensión que contiene el valor del registro de las secciones Inline cuando tienen opciones fijas. En el caso de ser
	una sección que utiliza un catálogo, contendría sólo el valor específico del atributo que se indicó en la sección, mas no el valor de todos los
	atributos padre, aunque en realidad en ese caso es innecesario porque para eso existen las dimensiones propias del catálogo asociadas a la encuesta
	cuando se ligan en la sección, ahí se almacenan todos los atributos hasta el indicado en la sección
	*/
	function createSectionDimension() {
		global $gblEFormsNA;
		global $gblModMngrErrorMessage;
		global $generateXLS;
		$generateXLS = false;
		
		//Valida que sólo aplique para secciones Inline y sólo si es necesario, como no se puede cambiar el tipo de sección una vez que ya está
		//construida, no debería ser un problema
		if ($this->SectionType != sectInline || $this->SectionDimValID > 0) {
			return;
		}
		
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión no se programó para compartirse, pero no hay una razón específica para no hacerlo
		$dimensionName = $this->SectionName;
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($this->Repository, $this->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_EFORMS;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		
		//@JAPR 2013-03-20: Agregado el control de errores del Model Manager
		$objMMAnswer = @$anInstanceModelDim->save();
		chdir($strOriginalWD);
		
		$strMMError = identifyModelManagerError($objMMAnswer);
		if ($strMMError == '') {
			//Insertamos el valor de No Aplica en la dimensión
			$objQuestion = @BITAMQuestion::NewInstance($this->Repository, $this->SectionID);
			if (!is_null($objQuestion) && !is_null($anInstanceModelDim)) {
				$objQuestion->insertNoApplyValue($anInstanceModelDim);
			}
			
			//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
			$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
			$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
			
			$dimTableName = $anInstanceModelDim->Dimension->TableName;
			$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
			
			//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
			$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$this->Repository->DataADOConnection->Quote($gblEFormsNA);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
			
			$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
			//Como a partir de la identificación de errores el Model Manager hace RollBack de lo que llevaba antes del error, ya no regresaría
			//un cla_descrip por lo que no tiene caso hacer esta asignación a menos que no hubiera error
			$this->SectionDimValID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		else {
			$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error creating section's dimension").": ".$strMMError.". ";
		}
		
		$sql = "UPDATE SI_SV_Section SET SectionDimValID = ".$this->SectionDimValID." WHERE SectionID = ".$this->SectionID." AND SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function updateSectionDimensionValue($anInstanceModelDim)
	{
		//Procedemos a almacenar el elemento Section en la tabla del catalogo siempre y cuando no este en dicho catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		$dimensionValue = $this->SectionName;
		
		//Se verifica si el valor realmente existe
		$sql = "SELECT ".$fieldKey." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($dimensionValue);
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Si el valor no existe entonces se procede a insertarlo
		if($aRS->EOF)
		{
			$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$this->Repository->DataADOConnection->Quote($dimensionValue).")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
			$sql = "SELECT ".$fieldKey." FROM ".$tableName." WHERE ".$fieldDescription." = ".$this->Repository->DataADOConnection->Quote($dimensionValue);
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			$valueKey =  $aRS->fields[strtolower($fieldKey)];

		}
		else 
		{
			//Ya existe ese valor en la tabla de la dimension, se retorna su clave
			$valueKey = $aRS->fields[strtolower($fieldKey)];
		}
		
		return $valueKey;
	}

	function remove() {
		$aCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
		
		foreach ($aCollection->Collection as $anInstanceToRemove)
		{
			$anInstanceToRemove->remove();
		}
		
		//Antes de eliminar la seccion actualizamos los SectionNumbers
		//Ya no obtenemos el SectionNumber desde la instancia ya que puede venir desde una colección y si hay
		//un recorrido de la coleccion para eliminar cada uno de los elementos dichos elementos no estaran actualizados
		//con su SectionNumber actual ya que en esta funcion remove se hace la actualización de este campo y mas sin embargo
		//los elementos de la colección se quedaron desactualizados
		//$sectionNumber = $this->SectionNumber + 1;
		
	    $sqlNumber =  "SELECT SectionNumber FROM SI_SV_Section WHERE SurveyID = ".$this->SurveyID." AND SectionID = ".$this->SectionID;
		$aRS = $this->Repository->DataADOConnection->Execute($sqlNumber);
		if (!$aRS || $aRS->EOF)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlNumber);
		}
		
		$sectionNumber = (int)$aRS->fields["sectionnumber"] + 1;
		$sqlUpd = "UPDATE SI_SV_Section SET SectionNumber = SectionNumber - 1 WHERE SurveyID = ".$this->SurveyID." AND SectionNumber >= ".$sectionNumber;
		if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
		}
		
		//Antes de eliminar esta seccion se deben eliminar sus referencias en los saltos a secciones de la tabla SI_SV_QAnswers
		$sqlUpd = "UPDATE SI_SV_QAnswers SET NextSection = 0 WHERE NextSection = ".$this->SectionID;
		if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
		}
		
		//Antes de eliminar esta seccion se deben eliminar sus referencias en los saltos a secciones de la tabla SI_SV_Question
		$sqlUpd = "UPDATE SI_SV_Question SET GoToSection = 0 WHERE SurveyID = ".$this->SurveyID." AND GoToSection = ".$this->SectionID;
		if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
		}
		
		//@JAPR 2015-01-19: Corregido un bug, al eliminar una sección no eliminaba los brincos de sección a sección
		$sqlUpd = "UPDATE SI_SV_Section SET NextSectionID = 0 WHERE SurveyID = ".$this->SurveyID." AND NextSectionID = ".$this->SectionID;
		if ($this->Repository->DataADOConnection->Execute($sqlUpd) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@AAL 27/05/2015 solo se elimina la tabla SVeFormsSection correspondiente a la Sección
		if ($this->CreationAdminVersion >= esveFormsv6 && $this->SectionType != sectNormal && $this->SectionType != sectFormatted) {
		 	//Ahora borramos la tabla paralela  SVeForms_####_Section_XXXX_Desc correspondiente a la sección con Descripción
		 	$sql = "DROP TABLE {$this->SectionDescTable}";
		 	$this->Repository->DataADOConnection->Execute($sql);
			
			//Ahora borramos la tabla paralela SVeForms_####_Section_XXXX correspondiente a la sección
		 	$sql = "DROP TABLE {$this->SectionTable}";
		 	$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			//Elimina los filtros dinámicos creados a partir de las preguntas que son tipo GetData, como ahora los catálogos son únicos por pregunta, es seguro hacer este proceso
			$sql = "DELETE FROM SI_SV_SurveyFilter WHERE CatalogID = ".$this->CatalogID;
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2015-08-20: Validado que si se limpia el catálogo se debe remover la instancia, ya que no habrá mas elementos que lo utilicen en v6
			if ($this->ValuesSourceType == tofCatalog && $this->CatalogID > 0) {
				$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
				if (!is_null($objCatalog)) {
					$objCatalog->remove();
				}
			}
			//@JAPR
		}
		
		$sql = "DELETE FROM SI_SV_Section WHERE SectionID = ".$this->SectionID." AND SurveyID = ".$this->SurveyID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Deleting section: {$sql}", 2, 0, "color:blue;");
		}
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Actualizar datos de modificacion de la encuesta
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
		
		return $this;
	}

	function isNewObject()
	{
		return ($this->SectionID <= 0 || $this->ForceNew);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Section");
		}
		else
		{
			return translate("Section")." ".$this->SectionName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Section&SurveyID=".$this->SurveyID;
		}
		else
		{
			return "BITAM_PAGE=Section&SurveyID=".$this->SurveyID."&SectionID=".$this->SectionID;
		}
	}

	function get_Parent()
	{
		return BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SectionID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			//Si está en desarrollo la encuesta si se puede editar la pregunta
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-01-22: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
		//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
		//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
		//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
		//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
		if (!$this->enableRemove) {
			return false;
		}
		
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}
	
	function getSurveyMapFields() {
		global $gblShowErrorSurvey;
		
		$arreBavelForms = array();
		//@JAPR 2014-11-10: Validado que en caso de no existir la tabla de eBavel en esta cuenta, simplemente regrese un array vacío
		$sql = "SELECT COUNT(*) 
			FROM {$this->Repository->ADOConnection->databaseName}.SI_FORMS_FIELDSFORMS t2 
				JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_SECTIONS t3 ON t2.section_id = t3.id_section";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			return $arreBavelForms;
		}
		
		//@JAPR 2014-08-14: Corregido un bug, había una referencia a eBavel que se estaba pidiendo al DWH, funcionaba sólo si tenían repositorio de desarrollo pero eso es incorrecto
		//incluso en esos casos pues pudiera haber diferencias
		$sql = "SELECT t3.id_section, t3.sectionName 
			FROM SI_SV_SurveyAnswerActionFieldsMap t1 
				JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_FIELDSFORMS t2 ON t1.EBavelFieldID = t2.id_fieldform 
				JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_SECTIONS t3 ON t2.section_id = t3.id_section 
			WHERE t1.SurveyID = $this->SurveyID 
			GROUP BY t3.id_section";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap, SI_FORMS_FIELDSFORMS, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap, SI_FORMS_FIELDSFORMS, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if(!$aRS->EOF) {
			$sectionID = (int) @$aRS->fields["id_section"];
			$sectionName = (string) @$aRS->fields["sectionname"];
			$arreBavelForms[$sectionID] = $sectionName;
		}
		
		//@JAPR 2014-08-19: Agregadas las formas que están mapeadas a preguntas sin necesidad de que realmente tengan mapeo de campos, ya
		//que las secciones Recap podrán capturar valores que no necesariamente son contestados o asignados con acciones
		$sql = "SELECT DISTINCT B.id_section, B.sectionName 
			FROM SI_SV_Question A 
				INNER JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_SECTIONS B ON A.eBavelActionFormID = B.id_section 
			WHERE A.SurveyID = {$this->SurveyID} AND A.eBavelActionFormID > 0";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if(!$aRS->EOF) {
			$sectionID = (int) @$aRS->fields["id_section"];
			$sectionName = (string) @$aRS->fields["sectionname"];
			$arreBavelForms[$sectionID] = $sectionName;
		}

		//@JAPR 2014-08-19: Agregadas las formas que están mapeadas a preguntas sin necesidad de que realmente tengan mapeo de campos, ya
		//que las secciones Recap podrán capturar valores que no necesariamente son contestados o asignados con acciones
		$sql = "SELECT DISTINCT B.id_section, B.sectionName 
			FROM SI_SV_QAnswers A 
				INNER JOIN SI_SV_Question A1 ON A.QuestionID = A1.QuestionID 
				INNER JOIN {$this->Repository->ADOConnection->databaseName}.SI_FORMS_SECTIONS B ON A.eBavelActionFormID = B.id_section 
			WHERE A1.SurveyID = {$this->SurveyID} AND A.eBavelActionFormID > 0";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers, SI_SV_Question, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers, SI_SV_Question, SI_FORMS_SECTIONS ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if(!$aRS->EOF) {
			$sectionID = (int) @$aRS->fields["id_section"];
			$sectionName = (string) @$aRS->fields["sectionname"];
			$arreBavelForms[$sectionID] = $sectionName;
		}
		
		return $arreBavelForms;
	}
	
	function get_FormFields($aUser)
	{
		$surveyMapFields = (getMDVersion() >= esvSectionRecap) ? $this->getSurveyMapFields() : array();
		$surveyHasMapFields = (count($surveyMapFields) > 0) ? true : false;
		
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$isNewObj = $this->isNewObject();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SectionName";
		$aField->Title = translate("Section");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->FormattedText = $this->TranslateVariableQuestionIDsByNumbers($this->FormattedText);
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$this->HTMLHeader = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLHeader);
			$this->HTMLFooter = $this->TranslateVariableQuestionIDsByNumbers($this->HTMLFooter);
		}
		if (getMDVersion() >= esvResponsiveDesign) {
			for ($intDeviceID = 1; $intDeviceID <= dvcTablet; $intDeviceID++) {
				$strDeviceName = getDeviceNameFromID($intDeviceID);
				$strProperty = "FormattedText";
				$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
				$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
				$strProperty = "HTMLHeader";
				$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
				$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
				$strProperty = "HTMLFooter";
				$strValue = $this->getResponsiveDesignProperty($strProperty, $strDeviceName);
				$this->setResponsiveDesignProperty($strProperty, $strDeviceName, $this->TranslateVariableQuestionIDsByNumbers($strValue));
			}
		}
		
		//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		$arrTypeOfFilling = array();
		$arrTypeOfFilling[tofFixedAnswers] = translate("Fixed answers");
		$arrTypeOfFilling[tofCatalog] = translate("Catalog");
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$arrTypeOfFilling[tofMCHQuestion] = translate("From another multiple choice's answers");
			$arrTypeOfFilling[tofInlineSection] = translate("From the responses of an Inline section");
		}
		
		//@JAPR 2012-05-31: Agregada una validación cuando la sección se está editando, NO debe permitir el cambio de tipo de sección
		if (!$isNewObj)
		{
			$options = array();
			switch ($this->SectionType)
			{
				case sectDynamic:
					$options[sectDynamic] = translate('List-based');
					break;
				case sectFormatted:
					$options[sectFormatted] = translate('Formatted text');
					break;
				case sectMasterDet:
					$options[sectMasterDet] = translate('Master-Detail');
					break;
				//@JAPR 2014-05-19: Agregado el tipo de sección Inline
				case sectInline:
					$options[sectInline] = translate('Inline');
					break;
				//@MABH 2014-06-17
				case sectRecap:
					$options[sectRecap] = translate('Recap');
					break;
				case sectNormal:
				default:
					$options[sectNormal] = translate('Standard question & answer');
					break;
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SectionType";
			$aField->Title = translate("Type");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $options;
			$aField->OnChange = "showHideCatMembers();";
			$myFields[$aField->Name] = $aField;
			
			//13Marzo2013: Que se despliegue como sólo lectura el atributo seleccionado
			//en la sección dinámica
			//@JAPR 2014-05-19: Agregado el tipo de sección Inline
			if($this->SectionType==sectDynamic || $this->SectionType==sectInline)
			{
				//Realizamos instancia del catalogo para obtener el nombre de dicho catalogo
				//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
				//@JAPR 2014-05-19: Agregado el tipo de sección Inline
				//Para secciones dinámicas no se puede cambiar esta información durante la edición, pero para secciones en línea se puede cambiar
				//siempre y cuando la encuesta esté aún marcada como edición (se asumirá que si está marcada como producción, ya hay capturas y no
				//sería entonces posible cambiar este valor ya que dañaría lo previamente grabado)
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
				$selectedCatalog = array();
				$selectedCatMember = array();
				$blnIsDisplayOnly = true;
				if ($this->SectionType==sectDynamic || $aSurveyInstance->Status!=0) {
					//Como es edición, se puede tomar directamente la propiedad CatalogID, ya que no sería dinámica si no la tuviera asignada
					//Obtener el nombre del atributo seleccionado
					$sql = "SELECT A.CatalogName, B.MemberName 
						FROM SI_SV_Catalog A, SI_SV_CatalogMember B 
						WHERE A.CatalogID = B.CatalogID AND B.CatalogID = ". $this->CatalogID ." AND B.MemberID = ".$this->CatMemberID;
					//@JAPR
					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					
					if(!$aRS->EOF)
					{
						$selectedCatalog[$this->CatalogID] = $aRS->fields["catalogname"];
						//@JAPR 2014-05-27: Corregido un bug, debe ser un array de arrays
						$selectedCatMember[$this->CatalogID] = array($this->CatMemberID => $aRS->fields["membername"]);
					}
				}
				else {
					//En este caso es una sección en línea que tiene libertad de cambiar su definición, por tanto se pueden agregar todos los
					//catálogos y atributos que no hubiesen sido ya utilizados dentro de secciones dinámica, en línea o maestro detalle, ya que
					//en ese caso no se puede hacer una sincronización de dichas preguntas inter secciones con varios registros
					$arrInvalidCatalogs = array();
					$sql = "SELECT DISTINCT A.CatalogID, B.CatalogName 
						FROM SI_SV_Question A, SI_SV_Catalog B, SI_SV_Section C 
						WHERE A.CatalogID = B.CatalogID AND A.SectionID = C.SectionID AND C.SectionType IN (".sectDynamic.",".sectInline.",".sectMasterDet.") AND 
							A.SurveyID = {$this->SurveyID} AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1
						UNION SELECT D.CatalogID, E.CatalogName 
						FROM SI_SV_Section D, SI_SV_Catalog E 
						WHERE D.CatalogID=E.CatalogID AND D.SurveyID = {$this->SurveyID} AND D.SectionType = ".sectDynamic;

					$aRS = $this->Repository->DataADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					while(!$aRS->EOF) {
						$intCatalogID = (int) @$aRS->fields["catalogid"];
						if ($intCatalogID > 0) {
							$arrInvalidCatalogs[$intCatalogID] = (string) @$aRS->fields["catalogname"];
						}
						$aRS->MoveNext();
					}
					
					//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
					//Ahora las secciones Inline pueden utilizar opciones fijas, para ello se agrega el catálogID == 0, por lo que teoricamente
					//se puede cambiar la definición de la sección sin problema aunque obviamente no es recomendable
					$selectedCatalog[0] = translate("None");
					$objCatalogColl = BITAMCatalogCollection::NewInstance($this->Repository);
					foreach ($objCatalogColl as $objCatalog) {
						//Sólo se agrega la lista de catálogos que aún no se han utilizado para secciones múltiples, o bien el catálogo que ya
						//estuviera asignado si es que dichas secciones se agregaron posteriormente (así por lo menos se puede ver el error y
						//modificarlo, no dar la impresión de que nunca estuvo asignado dicho catálogo)
						if (!isset($arrInvalidCatalogs[$objCatalog->CatalogID]) || $objCatalog->CatalogID == $this->CatalogID) {
							$selectedCatalog[$objCatalog->CatalogID] = $objCatalog->CatalogName;
						}
					}
					
					$selectedCatMember = $this->getUnusedCatMembers(array_keys($selectedCatalog));
					//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
					if (!isset($selectedCatMember[0])) {
						$selectedCatMember[0] = array(0 => translate("None"));
					}
					//@JAPR
					$blnIsDisplayOnly = false;
				}
				
				//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "typeOfFilling";
				$aField->Title = translate("Type of filling");
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrTypeOfFilling;
				$aField->OnChange = "showFillingFields();";
				$myFields[$aField->Name] = $aField;
				//@JAPR
				
				$catalogField = BITAMFormField::NewFormField();
				$catalogField->Name = "CatalogID";
				$catalogField->Title = translate("Select Catalog");
				$catalogField->Type = "Object";
				$catalogField->VisualComponent = "Combobox";
				$catalogField->Options = $selectedCatalog;
				//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
				//$catalogField->OnChange = "showHideFixedOptions();";
				$catalogField->IsDisplayOnly = $blnIsDisplayOnly;
				$myFields[$catalogField->Name] = $catalogField;
				
				$catmemberField = BITAMFormField::NewFormField();
				$catmemberField->Name = "CatMemberID";
				$catmemberField->Title = translate("Select Attribute");
				$catmemberField->Type = "Object";
				$catmemberField->VisualComponent = "Combobox";
				$catmemberField->Options = $selectedCatMember;
				$catmemberField->IsDisplayOnly = $blnIsDisplayOnly;
				$myFields[$catmemberField->Name] = $catmemberField;
				$catmemberField->Parent = $catalogField;
				$catalogField->Children[] = $catmemberField;
			}
			
			//@JAPR 2012-07-26: Agregado el salto de sección a sección
			$isLastSection = ($this->SectionID == BITAMSurvey::GetLastSectionID($this->Repository, $this->SurveyID));
			if (!$isLastSection)
			{
				$arrayNextSections = BITAMSection::GetSectionsFromNumber($this->Repository, $this->SurveyID, $this->SectionNumber +1);
				if(count($arrayNextSections) > 1)
				{
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "NextSectionID";
					$aField->Title = translate("After response, skip to section");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrayNextSections;
					$myFields[$aField->Name] = $aField;
				}
			}
			//@JAPR
		}
		else 
		{
			//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
			//En el caso de una nueva sección, hay que agregar ahora una configuración para el CatalogID ya que no existe un catálogo a nivel
			//de la encuesta, sólo que los catálogos disponibles serán exclusivamente aquellos que tienen al menos una pregunta simple choice
			//previa a la sección, si no hay entonces no se puede crear la sección dinámica
			//Como en este punto es una sección nueva y se creará al final, no hay necesidad de validar sólo las preguntas previas, todas lo son
			//03-Feb-2015: Agregar validación para que en los catálogos disponibles para una sección dinámica no se consideren áquellos
			//que están siendo utilizados en una sección dinámica, in line o maestro detalle.
			$optionsCatalog = array();
			$sql = "SELECT DISTINCT A.CatalogID, B.CatalogName 
				FROM SI_SV_Question A, SI_SV_Catalog B, SI_SV_Section C 
				WHERE A.CatalogID = B.CatalogID AND A.SectionID = C.SectionID AND C.SectionType = ".sectNormal." AND 
					A.SurveyID = {$this->SurveyID} AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1
					AND A.CatalogID NOT IN
					(SELECT DISTINCT A.CatalogID
						FROM SI_SV_Question A, SI_SV_Catalog B, SI_SV_Section C 
						WHERE A.CatalogID = B.CatalogID AND A.SectionID = C.SectionID AND C.SectionType IN (".sectDynamic.",".sectInline.",".sectMasterDet.") AND 
							A.SurveyID = {$this->SurveyID} AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1
						UNION SELECT D.CatalogID 
						FROM SI_SV_Section D, SI_SV_Catalog E 
						WHERE D.CatalogID=E.CatalogID AND D.SurveyID = {$this->SurveyID} AND D.SectionType IN ( ".sectDynamic.",".sectInline."))";
			
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while(!$aRS->EOF) {
				$intCatalogID = (int) @$aRS->fields["catalogid"];
				if ($intCatalogID > 0) {
					$optionsCatalog[$intCatalogID] = (string) @$aRS->fields["catalogname"];
				}
				$aRS->MoveNext();
			}
			
			//@JAPR 2014-05-19: Agregado el tipo de sección Inline
			//Si es sección dinámica entonces tiene que existir previamente alguna sección estándar previa con por lo menos algún catálogo usado
			//en una pregunta simple choice, ya que se requiere algo que genere a la dinámica, si por el contrario es una sección inline entonces
			//se puede crear sin prerequisitos, excepto claro que exista por lo menos un catálogo con atributos, sin embargo como apenas estamos
			//creando la sección, no tenemos forma de validar que se pretente hacer con ella ni que tipo será, así que se tendrá que agregar primero
			//la sección y posteriormente si es inline definir sus atributos durante la edición de la misma
			if (count($optionsCatalog) > 0)
			{
				$showFieldsAboutDynamicSection = true;
				$tmpSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
				$showFieldsAboutDynamicSection = ($tmpSectionID>0)?(false):(true);
				
				//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
				//Ahora las secciones Inline pueden utilizar opciones fijas, para ello se agrega el catálogID == 0, por lo que teoricamente
				//se puede cambiar la definición de la sección sin problema aunque obviamente no es recomendable
				$optionsCatalog[0] = translate("None");
				
				//@JAPR 2014-02-18: Removidas validaciones innecesarias, si llega aquí ya es una sección nueva, además debe haber alguna sección
				//previamente definida o si no sencillamente no habría preguntas de catálogo disponibles en sección estándar ($optionsCatalog)
				if($showFieldsAboutDynamicSection==true)
				{
					$options = array();
					$options[sectNormal] = translate('Standard question & answer');
					$options[sectDynamic] = translate('List-based');
					$options[sectFormatted] = translate('Formatted text');
					$options[sectMasterDet] = translate('Master-Detail');
					//@JAPR 2014-05-19: Agregado el tipo de sección Inline
					$options[sectInline] = translate('Inline');
					//@JAPR 2014-08-19: Corregido un bug, faltaba integrar las Recap cuando existían catálogos
					if ($surveyHasMapFields) {
						$options[sectRecap] = translate('Recap');
					}
					//@JAPR
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "SectionType";
					$aField->Title = translate("Type");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $options;
					$aField->OnChange = "showHideCatMembers();";
					$myFields[$aField->Name] = $aField;
					
					//Si en la encuesta hay un catalogo definido entonces si podremos definir
					//secciones dinamicas y que no haya sido definido antes otro catalogo dinamico
					//adicionalmente tenemos q verificar si hay atributos para asignarle uno a este catalogo
					//Queda la duda si debe haber mas de un atributo para seleccionar SectionType = 1
					$arrCatalogMembers = $this->getUnusedCatMembers(array_keys($optionsCatalog));
					
					$optionsCatMember = array();
					//Suprimimos el ultimo valor para que no aparezca en el combo
					if(count($arrCatalogMembers)>0)
					{
						foreach ($arrCatalogMembers as $catalogID => $arrAttributes) {
							$countOptions = count($arrAttributes);
							$optionsCatMember[$catalogID] = array();
							$countMembers = 0;
							foreach ($arrAttributes as $optionIdx=>$optionName)
							{
								$countMembers++;
								
								if($countMembers == $countOptions)
								{
									unset($arrAttributes[$optionIdx]);
								}
							}
							$optionsCatMember[$catalogID] = $arrAttributes;
						}
					}
					
					//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
					//Ahora las secciones Inline pueden utilizar opciones fijas, para ello se agrega el catálogID == 0, por lo que teoricamente
					//se puede cambiar la definición de la sección sin problema aunque obviamente no es recomendable
					if (!isset($optionsCatMember[0])) {
						$optionsCatMember[0] = array(0 => translate("None"));
					}
					
					//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "typeOfFilling";
					$aField->Title = translate("Type of filling");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrTypeOfFilling;
					$aField->OnChange = "showFillingFields();";
					$myFields[$aField->Name] = $aField;
					//@JAPR
					
					$catalogField = BITAMFormField::NewFormField();
					$catalogField->Name = "CatalogID";
					$catalogField->Title = translate("Select Catalog");
					$catalogField->Type = "Object";
					$catalogField->VisualComponent = "Combobox";
					//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
					$catalogField->OnChange="updateFields();";	//showHideFixedOptions();
					$catalogField->Options = $optionsCatalog;
					$myFields[$catalogField->Name] = $catalogField;
					
					$catmemberField = BITAMFormField::NewFormField();
					$catmemberField->Name = "CatMemberID";
					$catmemberField->Title = translate("Select Attribute");
					$catmemberField->Type = "Object";
					$catmemberField->VisualComponent = "Combobox";
					$catmemberField->Options = $optionsCatMember;
					$myFields[$catmemberField->Name] = $catmemberField;
					$catmemberField->Parent = $catalogField;
					$catalogField->Children[] = $catmemberField;
				}
				else 
				{
					$options = array();
					$options[sectNormal] = translate('Standard question & answer');
					$options[sectFormatted] = translate('Formatted text');
					$options[sectMasterDet] = translate('Master-Detail');
					//@JAPR 2014-05-19: Agregado el tipo de sección Inline
					$options[sectInline] = translate('Inline');
					//@JAPR 2014-08-19: Corregido un bug, faltaba integrar las Recap cuando existían catálogos
					if ($surveyHasMapFields) {
						$options[sectRecap] = translate('Recap');
					}
					//@JAPR
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "SectionType";
					$aField->Title = translate("Type");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $options;
					$aField->OnChange = "showHideCatMembers();";
					$myFields[$aField->Name] = $aField;
					
					//@JAPR 2014-11-11: Corregido un bug, en este caso no se estaban considerando todas las posibles opciones de secciones Inline
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "typeOfFilling";
					$aField->Title = translate("Type of filling");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrTypeOfFilling;
					$aField->OnChange = "showFillingFields();";
					$myFields[$aField->Name] = $aField;
					
					$catalogField = BITAMFormField::NewFormField();
					$catalogField->Name = "CatalogID";
					$catalogField->Title = translate("Select Catalog");
					$catalogField->Type = "Object";
					$catalogField->VisualComponent = "Combobox";
					//$catalogField->OnChange="updateFields();";	//showHideFixedOptions();
					$catalogField->Options = array(0 => translate("None"));
					$myFields[$catalogField->Name] = $catalogField;
					
					$catmemberField = BITAMFormField::NewFormField();
					$catmemberField->Name = "CatMemberID";
					$catmemberField->Title = translate("Select Attribute");
					$catmemberField->Type = "Object";
					$catmemberField->VisualComponent = "Combobox";
					$catmemberField->Options = array(0 => array(0 => translate("None")));
					$myFields[$catmemberField->Name] = $catmemberField;
					$catmemberField->Parent = $catalogField;
					$catalogField->Children[] = $catmemberField;
					//@JAPR
				}
			}
			else
			{
				$options = array();
				$options[sectNormal] = translate('Standard question & answer');
				$options[sectFormatted] = translate('Formatted text');
				$options[sectMasterDet] = translate('Master-Detail');
				//@JAPR 2014-05-19: Agregado el tipo de sección Inline
				$options[sectInline] = translate('Inline');
				//@MABH 2014-06-17
				if ($surveyHasMapFields) {
					$options[sectRecap] = translate('Recap');
				}
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "SectionType";
				$aField->Title = translate("Type");
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $options;
				$aField->OnChange = "showHideCatMembers();";
				$myFields[$aField->Name] = $aField;
				
				//@JAPR 2014-11-11: Corregido un bug, en este caso no se estaban considerando todas las posibles opciones de secciones Inline
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "typeOfFilling";
				$aField->Title = translate("Type of filling");
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrTypeOfFilling;
				$aField->OnChange = "showFillingFields();";
				$myFields[$aField->Name] = $aField;
				
				$catalogField = BITAMFormField::NewFormField();
				$catalogField->Name = "CatalogID";
				$catalogField->Title = translate("Select Catalog");
				$catalogField->Type = "Object";
				$catalogField->VisualComponent = "Combobox";
				//$catalogField->OnChange="updateFields();";	//showHideFixedOptions();
				$catalogField->Options = array(0 => translate("None"));
				$myFields[$catalogField->Name] = $catalogField;
				
				$catmemberField = BITAMFormField::NewFormField();
				$catmemberField->Name = "CatMemberID";
				$catmemberField->Title = translate("Select Attribute");
				$catmemberField->Type = "Object";
				$catmemberField->VisualComponent = "Combobox";
				$catmemberField->Options = array(0 => array(0 => translate("None")));
				$myFields[$catmemberField->Name] = $catmemberField;
				$catmemberField->Parent = $catalogField;
				$catalogField->Children[] = $catmemberField;
				//@JAPR
			}
		}
		
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		if (getMDVersion() >= esvFixedInlineSections) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "StrSelectOptions";
			$aField->Title = translate("Response items");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$arrInlineSections = array(0 => "(".translate("None").")");
			$objSectionCollection = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
			if (!is_null($objSectionCollection)) {
				foreach ($objSectionCollection->Collection as $objSection) {
					if ($this->SectionNumber && $objSection->SectionNumber >= $this->SectionNumber) {
						break;
					}
					
					if ($objSection->SectionType == sectInline) {
						$arrInlineSections[$objSection->SectionID] = $objSection->SectionNumber.". ".$objSection->SectionName;
					}
				}
			}
			
			$arrMChoiceQuestions = array(0 => "(".translate("None").")");
			$objQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
			if (!is_null($objQuestionCollection)) {
				foreach ($objQuestionCollection->Collection as $objQuestion) {
					if ($objQuestion->SectionNumber == $this->SectionNumber) {
						break;
					}
					
					if (($objQuestion->QTypeID == qtpMulti || $objQuestion->QTypeID == qtpSection) && $objQuestion->SectionType == sectNormal) {
						$arrMChoiceQuestions[$objQuestion->QuestionID] = $objQuestion->QuestionNumber.". ".$objQuestion->QuestionText;
					}
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SourceSectionID";
			$aField->Title = translate("Obtain response items from the following Inline section");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrInlineSections;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SourceQuestionID";
			$aField->Title = translate("Obtain response items from the following question");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrMChoiceQuestions;
			$myFields[$aField->Name] = $aField;
		}
		
		//@MABH 2014-06-17
		if(/*$this->SectionType == sectRecap &&*/ $surveyHasMapFields) {
			$arrMapForm = array();
			if(!$isNewObj && $this->SectionFormID > 0 && !array_key_exists($this->SectionFormID, $surveyMapFields)) {
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
				$sectionForm = @GetEBavelSections($this->Repository, false, $aSurveyInstance->eBavelAppID, array($this->SectionFormID));
				$arrMapForm[$sectionForm['id_section']] = $sectionForm['sectionName'];
			}
			
			$arrForms = $arrMapForm + $surveyMapFields;
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SectionFormID";
			$aField->Title = translate("Form");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrForms;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		if (getMDVersion() >= esvShowSectionCondition) {
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->ShowCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
			//@JAPR
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ShowCondition";
			$aField->Title = translate("Show condition");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin: 5px 535px;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'ShowCondition\');">';
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "AutoRedraw";
			$aField->Title = translate("Auto redraw");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			if (!$isNewObj && $this->SectionType != sectFormatted) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
		}
		//Conchita agregado el mostrar en la maestro detalle el summary
		if (getMDVersion() >= esvShowInOnePageMD) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SummaryInMDSection";
			$aField->Title = translate("Display summary in MD Section");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			//$aField->IsVisible = false;
			
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-09-26: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (getMDVersion() >= esvInlineMerge) {
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			//El requerimiento original cambió durante la implementación y finalmente ya no se utilizará una configuración a nivel de sección, sino que
			//se basará en la existencia de preguntas Inline, se dejará el campo por si se desea aplicarlo de alguna otra forma mas adelante, pero por
			//ahora se va a asignar automáticamente en la definición descargada
			/*
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ShowAsQuestion";
			$aField->Title = translate("Show with previous section?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			$aField->OnChange = "showHideInlineOptions();";
			$myFields[$aField->Name] = $aField;
			*/
		}
		
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ShowInNavMenu";
			$aField->Title = translate("Show in navigation menu?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if (getMDVersion() >= esvInlineSection) {
			$arrDisplayModes = array();
			$arrDisplayModes[sdspDefault] = translate("Default");
			$arrDisplayModes[sdspInline] = translate("Inline");
			$arrDisplayModes[sdspPages] = translate("Pages");
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DisplayMode";
			$aField->Title = translate("Entry display mode");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrDisplayModes;
			if (!$isNewObj && $this->SectionType != sectInline) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-09-26: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		if (getMDVersion() >= esvInlineMerge) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ShowSelector";
			$aField->Title = translate("Show selector question?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = array(0 => translate('No'), 1 => translate('Yes'));
			$aField->OnChange = "showHideInlineOptions();";
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-19: Agregado el tipo de sección Inline
		if (getMDVersion() >= esvInlineSection) {
			if (!$isNewObj && $this->SectionType == sectInline) {
				$anArrayTypes = array(qtpSingle);
				$questionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
				$arrSCHQuestionIDs = array();
				$arrSCHQuestionIDs[0] = translate("None");
				//Todas las preguntas simple choice de la sección son potencialmente válidas, aunque en realidad esta configuración no se debería poder
				//cambiar, a menos que por algún error no exista asignada una pregunta como selector de la sección, en ese caso se habilita para permitir
				//elegir una
				foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
					if ($aQuestion->QTypeID != qtpSingle || $aQuestion->UseCatalog || $aQuestion->QDisplayMode==dspMatrix) {
						continue;
					}
					
					$arrSCHQuestionIDs[$aQuestion->QuestionID] = $aQuestion->QuestionText;
				}
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "SelectorQuestionID";
				$aField->Title = translate("Selector question");
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrSCHQuestionIDs;
				if ($this->SelectorQuestionID > 0 && isset($arrSCHQuestionIDs[$this->SelectorQuestionID])) {
					$aField->IsDisplayOnly = true;
				}
				$myFields[$aField->Name] = $aField;
			}
			
			$optionsGral = array();
			$optionsGral[0] = "No";
			$optionsGral[1] = "Yes";
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SwitchBehaviour";
			$aField->Title = translate("Switch default behaviour when selecting");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $optionsGral;
			if (!$isNewObj && $this->SectionType != sectInline) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
		}
		//2015.02.10 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
		
            $aField = BITAMFormField::NewFormField();
			$aField->Name = "MinRecordsToSelect";
			$aField->Title = translate("Minimum number of records to select");
			$aField->Type = "Integer";
			$myFields[$aField->Name] = $aField;
		}
		//Sep-2014
		if (getMDVersion() >= esvEditorHTML)
		{
			if($isNewObj)
			{
				$arrEditorType[0] = "Tinymce";
				$arrEditorType[1] = translate("Image Editor");
			}
			else
			{
				if($this->EditorType==0)
				{
					$arrEditorType[0] = "Tinymce";
				}
				else
				{
					$arrEditorType[1] = translate("Image Editor");
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "EditorType";
			$aField->Title = translate("Editor");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrEditorType;
			if($isNewObj) $aField->OnChange = "onChangeEditor(true)";
			else $aField->OnChange = "onChangeEditor()";
			$myFields[$aField->Name] = $aField;
		}
		
		
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			//@JAPR 2014-08-04: Agregado el Responsive Design
			$arrDevices = array();
			if (getMDVersion() >= esvResponsiveDesign) {
				$arrDevices[] = getDeviceNameFromID(dvciPod);
				$arrDevices[] = getDeviceNameFromID(dvciPad);
				$arrDevices[] = getDeviceNameFromID(dvciPadMini);
				$arrDevices[] = getDeviceNameFromID(dvciPhone);
				$arrDevices[] = getDeviceNameFromID(dvcCel);
				$arrDevices[] = getDeviceNameFromID(dvcTablet);
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "HTMLHeader";
			$aField->Title = translate("Customized header");
			$aField->Type = "LargeStringHTML";
			$aField->HTMLFormatButtons = "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image";
			//@JAPR 2014-08-04: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				$aField->MultiDeviceNames = $arrDevices;
			}
			//@JAPR
			$aField->Size = 5000;
			$aField->UseHTMLDes = 1;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "HTMLFooter";
			$aField->Title = translate("Customized footer");
			$aField->Type = "LargeStringHTML";
			$aField->HTMLFormatButtons = "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image";
			//@JAPR 2014-08-04: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				$aField->MultiDeviceNames = $arrDevices;
			}
			//@JAPR
			$aField->Size = 5000;
			$aField->UseHTMLDes = 1;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		return $myFields;
	}
	
	//14Marzo2013: Revisa si en la encuesta $aSurveyID existen preguntas simple choice 
	//que utilicen el catálogo $aCatalogID y que pertenezcan a una sección estándar.
	//Retorna la cantidad de preguntas
	//@JAPRDescontinuada, esta función ya no es necesaria, era para una validación que ya resulta evidente dentro de get_formFields
	function checkSimpleChoiceCatalog($aRepository, $aSurveyID, $aCatalogID)
	{
		$numQuestions = 0;
		
		$sql="SELECT COUNT(*) AS NumQuestions
		FROM SI_SV_QUESTION t1, SI_SV_Section t2
		WHERE t1.SectionID = t2.SectionID AND t2.SectionType=".sectNormal.
		" AND t1.SurveyID=".$aSurveyID." AND t1.QTypeID = ".qtpSingle." AND t1.UseCatalog = 1 
		AND t1.CatalogID = ".$aCatalogID;

		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QUESTION ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$numQuestions = (int) $aRS->fields["numquestions"];
		}
		
		return $numQuestions;
	}
	
	function generateInsideFormCode($aUser)
	{
		//Obtener secciones de la encuesta
		$sql = "SELECT t1.SectionID, t1.SectionName
				FROM SI_SV_Section t1
				WHERE t1.SurveyID = ".$this->SurveyID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$aSections = array();
		
?>
		<script language="JavaScript">
		var htmlOptionsBtn = '';
		aButtonElements[0]= "Exit";
<?			
		
		while (!$aRS->EOF)
		{
			$aSections[(int)$aRS->fields["sectionid"]]=$aRS->fields["sectionname"];
?>
			aButtonElements[<?=(int)$aRS->fields["sectionid"]?>] = '<?=str_replace("'", "\'", $aRS->fields["sectionname"])?>';
			htmlOptionsBtn += '<option value="<?=(int)$aRS->fields["sectionid"]?>"><?=str_replace("'", "\'", $aRS->fields["sectionname"])?></option>';
<?			
			$aRS->MoveNext();
		}
?>
		</script>
<?		
		include("formattedText.php");
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
 		<script language="JavaScript">
 		var myServerPath='<?=(((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";?>';
 		function updateFields()
		{
			if (cancel==true)
			{
				<?=$myFormName?>_CatMemberID_Update();
				
				var catMember = <?=$myFormName?>_SaveForm.CatMemberID;
				
				if (initialSelectionCatMember!=null)
				{
					for(i=0;i<catMember.options.length;i++)
					{
						if (catMember.options[i].value==initialSelectionCatMember)
						{	
							catMember.selectedIndex=i;
							break;
						}
					}
				}
			}

			cancel=false;
		}

		initialSelectionCatMember = null;

		function setInitialValues()
		{
	       	var catMember = <?=$myFormName?>_SaveForm.CatMemberID;
	        initialSelectionCatMember = null;
	        if (catMember && catMember.selectedIndex >= 0)
	        {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
	        }
		}

		/*@AAL 25/02/2015: Abre la ventana de dialogo donde se mostrará el editor de fórmulas, recibe como 
		parámetro el nombre del elemento HTML (NameElement) de donde se tomará y escribirá la fórmula.*/
		function ShowWindowsDialogEditor(NameElement)
		{
			var SurveyID = '<?= $this->SurveyID ?>';
			var SectionID = '<?= $this->SectionID ?>';
			var aText = document.getElementsByName(NameElement);
			var text = "";
			if ((NameElement.indexOf('HTMLHeader') != -1 || NameElement.indexOf('HTMLFooter') != -1) && aText[0].style.display == "none")
				text = tinyMCE.activeEditor.selection.getContent({format: 'text'});
			else
				text = aText[0].value;

			var Parameters = 'SurveyID=' + SurveyID + '&SectionID=' + SectionID + '&NameElement=' + NameElement + '&Type=S&aTextEditor=' + encodeURIComponent(text);
			openWindowDialog('formulaEditor.php?' + Parameters, [window], [NameElement], 620, 460, ResultEditFormulas, 'ResultEditFormulas');
		}

		/*@AAL 25/02/2015: Esta función recibe como parámetro sValue que corresponde al texto devuelto del editor de fórmulas 
		y sParams corresponde al nombre del elemento HTML donde se asignará la nueva fórmula o texto*/
		function ResultEditFormulas(sValue, sParams)
		{
			var aText = document.getElementsByName(sParams[0]);
			if ((sParams[0].indexOf('HTMLHeader') != -1 || sParams[0].indexOf('HTMLFooter') != -1) && aText[0].style.display == "none")
				tinyMCE.activeEditor.selection.setContent(sValue + " ");
			else
				aText[0].value = sValue;
		}
		//@AAL

		function verifyFieldsAndSave(target)
 		{
 			strBlanksField = "";

			if(Trim(<?=$myFormName?>_SaveForm.SectionName.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Section Name"))?>';
			}
<?
        //2015.02.10 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
?>
            var nMrts = <?=$myFormName?>_SaveForm.MinRecordsToSelect.value;
            if (!nMrts || nMrts ==""){
                nMrts = 0;
            }else if(isNaN(nMrts) || parseInt(nMrts) < 0){
                strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be a valid and positive number"),translate("Minimum number of records to select"))?>';
            }else{
                var NumAnswers = <?=$myFormName?>_SaveForm.StrSelectOptions.value.split('\n');
				if (<?=$myFormName?>_SaveForm.typeOfFilling) {
					if(parseInt(<?=$myFormName?>_SaveForm.typeOfFilling.value) == 0 && nMrts > NumAnswers.length){
						strBlanksField += '\n- ' + '<?=sprintf(translate("%s should not be greater than the number of %s"),translate("Minimum number of records to select"),translate("Response items"))?>';
					}
				}
            }
            
			
			
 <?
        }
 ?>
			var objCmb = <?=$myFormName?>_SaveForm.SectionType;
			var sectionType = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			var rowCatalogID = document.getElementById("Row_CatalogID");
 			if (rowCatalogID && sectionType == 1 && <?=$myFormName?>_SaveForm && <?=$myFormName?>_SaveForm.CatalogID) {
				catalogID = parseInt(<?=$myFormName?>_SaveForm.CatalogID.value);
				if (catalogID <= 0) {
					strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Catalog"))?>';
				}
 			}
			
			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{	//debugger;
  				//Llenar los campos en los que se guardarán los componentes del diseñador
  				var newEditor = false;
  				for(var divName in aHTMLDivObjects)
  				{
  					var textAreaName = divName.replace("HDiv", "");
  					textAreaName = textAreaName.replace("Default", "");
  					textAreaNameObj = document.getElementById(textAreaName);
  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.save(true,',');
  					if(textAreaNameObj.value!='')
  					{
  						newEditor = true;
  					}
  				}
  				//Si ya se utilizó por lo menos una vez el nuevo editor en la pantalla de sección
  				//entonces los campos HTML que se manejaban anteriormente serán reemplazados con el HTML
  				//generado a partir del nuevo editor, esto se hará para todos los dispositivos
  				if(newEditor==true)
  				{
	  				for(var divName in aHTMLDivObjects)
	  				{
	  					var textAreaName = divName.replace("HDiv", "");
	  					textAreaName = textAreaName.replace("Default", "");
						textAreaName = textAreaName+'HTML';
	  					textAreaNameObj = document.getElementById(textAreaName);
	  					textAreaNameObj.value=aHTMLDivObjects[divName].oPart.saveHTML();
  						//alert(textAreaName+' - saveHTML(): '+aHTMLDivObjects[divName].oPart.saveHTML());
  						//alert(textAreaName+' - textAreaNameObj.value: '+textAreaNameObj.value);
  						//alert(textAreaName+' - textAreaNameObj.innerHTML: '+textAreaNameObj.innerHTML);

	  				}
  				}
 <?
        //2015.02.10 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
?>
                var rowMinRecordsToSelect = document.getElementById("Row_MinRecordsToSelect");
		 		if (rowMinRecordsToSelect && rowMinRecordsToSelect.style.display == 'none') {
                    <?=$myFormName?>_SaveForm.MinRecordsToSelect.value = 0;
		 		}
 <?
        }
 ?>
  				<?=$myFormName?>_Ok(target);
  			}
 		}
<? 		
		//@JAPR 2015-01-22: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
?>		
 		function verifyObjectAndRemove() {
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}
			
			xmlObj.open('POST', 'verifyUsedObjectToRemove.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID=<?=$this->SectionID?>&ObjectType=<?=otySection?>&ValidationType=0');
	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}
			
	 		strResponseData = Trim(unescape(xmlObj.responseText));
	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}
	 		
	 		var strErrMsg = '';
	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strErrMsg = '<?=translate("This section cannot be removed because the following configurations contain references to it")?>: ' + "\r\n" + strErrMsg.replace(/\r/gi, "");
  				alert(strErrMsg);
  				return;
	 		}
	 		
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert('<?=translate('XML Object not supported')?>');
				return(new Array);
			}
			
			xmlObj.open('POST', 'verifyUsedObjectToRemove.php', false);
			xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8'); 
	 		xmlObj.send('ObjectID=<?=$this->SectionID?>&ObjectType=<?=otySection?>&ValidationType=1');
	 		if(xmlObj.status == 404)
			{
				alert('<?=translate('Unable to launch validation process')?>');
				return false;
			}
			
	 		strResponseData = Trim(unescape(xmlObj.responseText));
	 		arrResponseData = strResponseData.split('<SVSEP>');
	 		if(arrResponseData.length == 0)
	 		{
	 			alert('<?=translate('Error found during data validation')?>');
				return false;
	 		}
	 		
	 		var strErrMsg = '';
	 		if(arrResponseData[0]!="OK")
	 		{
				strErrMsg = arrResponseData[0];
				if(arrResponseData.length > 1)
				{
					strErrMsg = arrResponseData[1];
				}
				
  				strErrMsg = '<?=translate("This section cannot be removed because the following questions contains references in other configurations")?>: ' + "\r\n" + strErrMsg.replace(/\r/gi, "");
  				alert(strErrMsg);
  				return;
	 		}
 			
	 		<?=$myFormName?>_Remove();
 		}
<?
		//@JAPR
?> 		
 		
 		function showHideCatMembers()
 		{
 			var objCmb = <?=$myFormName?>_SaveForm.SectionType;
 			var sectionType = parseInt(objCmb.options[objCmb.selectedIndex].value);
            
            var style = "";
            if (sectionType == 4){
                style = "table-row";
            }else{
                style = "none";
            }
            
            var rowShowSelector = document.getElementById("Row_ShowSelector");
            if (rowShowSelector){
                rowShowSelector.style.display = style;
            }
            
 			var rowDisplayMode = document.getElementById("Row_DisplayMode");
 			if (rowDisplayMode) {
				rowDisplayMode.style.display = 'none';
 			}
 			var rowSelectorQuestionID = document.getElementById("Row_SelectorQuestionID");
 			if (rowSelectorQuestionID) {
 				rowSelectorQuestionID.style.display = 'none';
 			}
 			var rowSwitchBehaviour = document.getElementById("Row_SwitchBehaviour");
 			if (rowSwitchBehaviour) {
 				rowSwitchBehaviour.style.display = 'none';
 			}
 			var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
 			if (rowStrSelectOptions) {
 				rowStrSelectOptions.style.display = 'none';
 			}
 			
 			var rowTypeOfFilling = document.getElementById("Row_typeOfFilling");
 			if (rowTypeOfFilling) {
 				rowTypeOfFilling.style.display = 'none';
 			}
 			
 			var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
 			if (rowSourceQuestionID) {
 				rowSourceQuestionID.style.display = 'none';
 			}
 			
 			var rowSourceSectionID = document.getElementById("Row_SourceSectionID");
 			if (rowSourceSectionID) {
 				rowSourceSectionID.style.display = 'none';
 			}
 			
			var rowSectionFormID = document.getElementById("Row_SectionFormID");
			if (rowSectionFormID) {
				rowSectionFormID.style.display = "none";
				if (sectionType == 5) {
					rowSectionFormID.style.display = (bIsIE)?"inline":"table-row";
				}
			}
 			//Ocultar siempre el summaryinmdsection
			var rowSummaryInMDSection = document.getElementById("Row_SummaryInMDSection");
			if (rowSummaryInMDSection) {
				rowSummaryInMDSection.style.display = 'none';
				
				//mostrar el summaryINMDsection en las maestro detalle
				if(sectionType==3){
					rowSummaryInMDSection.style.display = (bIsIE)?"inline":"table-row";
				}
			}
 			
            var rowMinRecordsToSelect = document.getElementById("Row_MinRecordsToSelect");
 			
			//SectionType = 1
	 		if(sectionType==1 || sectionType==4)
	 		{
	 			var rowCatalogID = document.getElementById("Row_CatalogID");
	 			var rowCatMemberID = document.getElementById("Row_CatMemberID");
	 			var divFormattedText = document.getElementById("formattedTextArea");
	 			var rowAutoRedraw = document.getElementById("Row_AutoRedraw");
	 			
	 			if (rowCatalogID) {
	 				rowCatalogID.style.display = (bIsIE)?"inline":"table-row";
	 			}
				if(rowCatMemberID!=undefined && rowCatMemberID!=null)
				{
					rowCatMemberID.style.display = (bIsIE)?"inline":"table-row";
				}
	 			
	 			divFormattedText.style.display = 'none';
	 			if (rowAutoRedraw) {
	 				rowAutoRedraw.style.display = 'none';
	 			}
	 			
	 			if (sectionType == 4) {
	 				if (rowDisplayMode) {
	 					rowDisplayMode.style.display = (bIsIE)?"inline":"table-row";
	 				}
	 				if (rowSelectorQuestionID) {
	 					rowSelectorQuestionID.style.display = (bIsIE)?"inline":"table-row";
	 				}
	 				if (rowSwitchBehaviour) {
	 					rowSwitchBehaviour.style.display = (bIsIE)?"inline":"table-row";
	 				}
		 			if (rowTypeOfFilling) {
		 				rowTypeOfFilling.style.display = (bIsIE)?"inline":"table-row";
		 			}
	 				
	 				showFillingFields();
	 				/*
	 				var objCatalogID = <?=$myFormName?>_SaveForm.CatalogID;
	 				if (rowStrSelectOptions && objCatalogID && objCatalogID.options && objCatalogID.options.length) {
	 					var intCatalogID = objCatalogID.options[objCatalogID.selectedIndex].value;
	 					if (intCatalogID <= 0) {
		 					rowStrSelectOptions.style.display = (bIsIE)?"inline":"table-row";
	 					}
	 				}
	 				*/
                   //2015.02.10 
                   if(rowMinRecordsToSelect){
                       rowMinRecordsToSelect.style.display = (bIsIE)?"inline":"table-row";
                   }
	 			}
	 		}
	 		else
	 		{
	 			var rowCatalogID = document.getElementById("Row_CatalogID");
	 			var rowCatMemberID = document.getElementById("Row_CatMemberID");
	 			var divFormattedText = document.getElementById("formattedTextArea");
	 			var rowAutoRedraw = document.getElementById("Row_AutoRedraw");
				
				if(sectionType==2)
				{
					divFormattedText.style.display = 'block';
		 			if (rowAutoRedraw) {
		 				rowAutoRedraw.style.display = ((bIsIE)?"inline":"table-row");
		 			}
				}
				else
				{
					divFormattedText.style.display = 'none';
		 			if (rowAutoRedraw) {
		 				rowAutoRedraw.style.display = 'none';
		 			}
				}
				
				if (rowCatalogID) {
					rowCatalogID.style.display = 'none';
				}
				
				if(rowCatMemberID!=undefined && rowCatMemberID!=null)
				{
					rowCatMemberID.style.display = 'none';
				}
                
                //2015.02.10 
                if(rowMinRecordsToSelect){
                    rowMinRecordsToSelect.style.display = 'none';
                }
	 		}
 		}
 		
 		function showFillingFields() {
			var objTypeOfFilling = <?=$myFormName?>_SaveForm.typeOfFilling;
			if (!objTypeOfFilling || !objTypeOfFilling.options || !objTypeOfFilling.options.length || objTypeOfFilling.selectedIndex == -1) {
				return;
			}
 			
 			var objCmb = <?=$myFormName?>_SaveForm.SectionType;
 			var sectionType = parseInt(objCmb.options[objCmb.selectedIndex].value);
			var intTypeOfFilling = parseInt(objTypeOfFilling.options[objTypeOfFilling.selectedIndex].value);
 			var rowCatalogID = document.getElementById("Row_CatalogID");
 			var rowCatMemberID = document.getElementById("Row_CatMemberID");
 			var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
 			var rowSourceQuestionID = document.getElementById("Row_SourceQuestionID");
 			var rowSourceSectionID = document.getElementById("Row_SourceSectionID");
 			if (rowStrSelectOptions) {
 				rowStrSelectOptions.style.display = 'none';
 			}
 			if (rowCatalogID) {
 				rowCatalogID.style.display = 'none';
 			}
 			if (rowCatMemberID) {
 				rowCatMemberID.style.display = 'none';
 			}
 			if (rowSourceQuestionID) {
 				rowSourceQuestionID.style.display = 'none';
 			}
 			if (rowSourceSectionID) {
 				rowSourceSectionID.style.display = 'none';
 			}
			switch (intTypeOfFilling) {
				case 1:
					if (rowCatalogID) {
						rowCatalogID.style.display = (bIsIE)?"inline":"table-row";
					}
					if (rowCatMemberID) {
						rowCatMemberID.style.display = (bIsIE)?"inline":"table-row";
					}
					break;
				case 2:
					if (rowSourceQuestionID) {
						rowSourceQuestionID.style.display = (bIsIE)?"inline":"table-row";
					}
					break;
				case 4:
					if (rowSourceSectionID) {
						rowSourceSectionID.style.display = (bIsIE)?"inline":"table-row";
					}
					break;
				case 0:
				default:
					if (rowStrSelectOptions && sectionType == 4) {
						rowStrSelectOptions.style.display = (bIsIE)?"inline":"table-row";
					}
					break;
			}
 		}
 		
 		function showHideFixedOptions() {
 			var objCmb = <?=$myFormName?>_SaveForm.SectionType;
 			var sectionType = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			
	 		if (sectionType==4) {
	 			var rowCatalogID = document.getElementById("Row_CatalogID");
	 			var rowStrSelectOptions = document.getElementById("Row_StrSelectOptions");
	 			if (rowCatalogID && rowStrSelectOptions && <?=$myFormName?>_SaveForm && <?=$myFormName?>_SaveForm.CatalogID) {
					catalogID = parseInt(<?=$myFormName?>_SaveForm.CatalogID.value);
					if (catalogID > 0) {
						rowStrSelectOptions.style.display = 'none';
					}
					else {
	 					rowStrSelectOptions.style.display = (bIsIE)?"inline":"table-row";
					}
	 			}
	 		}
 		}
 		
 		function showHideInlineOptions() {
 			var objCmb = <?=$myFormName?>_SaveForm.SectionType;
 			var sectionType = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			
	 		if (sectionType==4) {
	 			var objCmb = <?=$myFormName?>_SaveForm.ShowAsQuestion;
	 			if (objCmb) {
		 			var intShowAsQuestion = parseInt(objCmb.options[objCmb.selectedIndex].value);
		 			var rowShowInNavMenu = document.getElementById("Row_ShowInNavMenu");
		 			if (rowShowInNavMenu) {
		 				rowShowInNavMenu.style.display = (intShowAsQuestion)?'none':((bIsIE)?"inline":"table-row");
		 			}
		 			
		 			var rowDisplayMode = document.getElementById("Row_DisplayMode");
		 			if (rowDisplayMode) {
		 				rowDisplayMode.style.display = (intShowAsQuestion)?'none':((bIsIE)?"inline":"table-row");
		 			}
	 			}
	 			
	 			var objCmb = <?=$myFormName?>_SaveForm.ShowSelector;
	 			if (objCmb) {
		 			var intShowSelector = parseInt(objCmb.options[objCmb.selectedIndex].value);
		 			var rowSwitchBehaviour = document.getElementById("Row_SwitchBehaviour");
		 			if (rowSwitchBehaviour) {
		 				rowSwitchBehaviour.style.display = (intShowSelector)?((bIsIE)?"inline":"table-row"):'none';
		 			}
		 			var rowSelectorQuestionID = document.getElementById("Row_SelectorQuestionID");
		 			if (rowSelectorQuestionID) {
		 				rowSelectorQuestionID.style.display = (intShowSelector)?((bIsIE)?"inline":"table-row"):'none';
		 			}
<?
        //2015.02.10 JCEM #9T6ASP
        if (getMDVersion() >= esvMinRecordsToSelect) {
?>
                    var rowMinRecordsToSelect = document.getElementById("Row_MinRecordsToSelect");
		 			if (rowMinRecordsToSelect) {
		 				rowMinRecordsToSelect.style.display = (intShowSelector)?((bIsIE)?"inline":"table-row"):'none';
		 			}
 <?
        }
 ?>
	 			}
	 		}
 		}
 		/*@AAL 01/04/2015: Validación modificada para mostrar u ocultar ya sea Image Editor o tinyMCE cuado se quiera editar */
 		var IsEdit = false;
 		function onChangeEditor(IsNew)
 		{
 			var objCmbEditor = <?=$myFormName?>_SaveForm.EditorType;
 			var editorTypeValue = parseInt(objCmbEditor.options[objCmbEditor.selectedIndex].value);

 			if(IsNew)
 			{
 				if(editorTypeValue == 1)
	 			{
	 				document.getElementById("FormattedTextSpanDesigner").style.display="inline";
	 				document.getElementById("HTMLHeaderSpanDesigner").style.display="inline";
	 				document.getElementById("HTMLFooterSpanDesigner").style.display="inline";
	 				
	 			}
	 			else
	 			{
	 				document.getElementById("FormattedTextSpanDesigner").style.display="none";
	 				document.getElementById("HTMLHeaderSpanDesigner").style.display="none";
	 				document.getElementById("HTMLFooterSpanDesigner").style.display="none";
	 			}
 			}
 			else
 			{//Si se va a editar entonces se verifica el tipo de editor para mostrarlo al usuario
 				if (IsEdit) {
 					if(editorTypeValue == 1)
		 			{
		 				document.getElementById("FormattedTextSpanDesigner").style.display="inline";
		 				document.getElementById("HTMLHeaderSpanDesigner").style.display="inline";
		 				document.getElementById("HTMLFooterSpanDesigner").style.display="inline";
		 			}
		 			else
		 			{
		 				document.getElementById("HTMLHeaderTabs").style.display="inline";
		 				document.getElementById("HTMLFooterTabs").style.display="inline";
		 				document.getElementById("FormattedTextTabs").style.display="inline";
		 			}
		 			IsEdit = false;
 				}
 				else{
 					document.getElementById("FormattedTextSpanDesigner").style.display="none";
	 				document.getElementById("HTMLHeaderSpanDesigner").style.display="none";
	 				document.getElementById("HTMLFooterSpanDesigner").style.display="none";
 					document.getElementById("HTMLHeaderTabs").style.display="none";
 					document.getElementById("HTMLFooterTabs").style.display="none";
 					document.getElementById("FormattedTextTabs").style.display="none";
 					IsEdit = true;
 				}
 			}
 			
			//Cargar el div de la pestaña default
 			var elemDefault = document.getElementById("defaultFormattedText");
			changeTab(elemDefault,'FormattedText');
			var elemDefault = document.getElementById("defaultHTMLHeader");
			changeTab(elemDefault,'HTMLHeader');
			var elemDefault = document.getElementById("defaultHTMLFooter");
			changeTab(elemDefault,'HTMLFooter');
 		}
 		</script>
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
	 	<script language="JavaScript">
	 	setInitialValues();
	 	
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");

		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");		
		if(objOkParentButton)
		{
			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		}
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("<?=$myFormName?>_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
		//@JAPR 2015-01-22: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
		else {
			//Se lanza a la función que validará si puede o no remover el objeto, para lo cual va al server a realizar una consulta personalizada
			//según el tipo de objeto
?>
			objRemoveButton = document.getElementById("<?=$myFormName?>_RemoveButton");
			if (objRemoveButton) {
				objRemoveButton.onclick=new Function("verifyObjectAndRemove();");
			}
<?
		}
?>
		</script>
<?
	}
	
	static function existDynamicSection($aRepository, $surveyID)
	{
		//@JAPR 2013-06-05: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance =& BITAMGlobalFormsInstance::GetDynSectionOfSurveyWithID($surveyID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$sectionID = 0;
		$sql = "SELECT SectionID FROM SI_SV_Section WHERE SectionType = ".sectDynamic." AND SurveyID = ".$surveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
		    if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		}
		
		if (!$aRS->EOF)
		{
			$sectionID = (int)$aRS->fields["sectionid"];
		}
		
		BITAMGlobalFormsInstance::AddDynSectionOfSurveyWithID($surveyID, $sectionID);
		
		return $sectionID;
	}
	
	//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
	//Verifica si la encuesta indicada contiene secciones Maestro-Detalle regresando según el parámetro $bGetSectionIDs, un Boolean simplemente o bien
	//el Array de todas las secciones Maestro-Detalle que se definieron para la encuesta
	static function existMasterDetSections($aRepository, $surveyID, $bGetSectionIDs = false)
	{
		$hasMasterDetSections = false;
		
		$sql = "SELECT SectionID FROM SI_SV_Section WHERE SurveyID = ".$surveyID." AND SectionType = ".sectMasterDet;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			if ($bGetSectionIDs)
			{
				$hasMasterDetSections = array();
				while(!$aRS->EOF)
				{
					$aSectionID = (int) $aRS->fields["sectionid"];
					if ($aSectionID > 0)
					{
						$hasMasterDetSections[] = $aSectionID;
					}
					$aRS->MoveNext();
				}
			}
			else
			{
				$hasMasterDetSections = true;
			}
		}
	
		return $hasMasterDetSections;
	}

	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	//Verifica si la encuesta indicada contiene secciones Inline regresando según el parámetro $bGetSectionIDs, un Boolean simplemente o bien
	//el Array de todas las secciones Inline que se definieron para la encuesta
	static function existInlineSections($aRepository, $surveyID, $bGetSectionIDs = false)
	{
		$hasInlineSections = false;
		
		$sql = "SELECT SectionID FROM SI_SV_Section WHERE SurveyID = ".$surveyID." AND SectionType = ".sectInline;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			if ($bGetSectionIDs)
			{
				$hasInlineSections = array();
				while(!$aRS->EOF)
				{
					$aSectionID = (int) $aRS->fields["sectionid"];
					if ($aSectionID > 0)
					{
						$hasInlineSections[] = $aSectionID;
					}
					$aRS->MoveNext();
				}
			}
			else
			{
				$hasInlineSections = true;
			}
		}
	
		return $hasInlineSections;
	}
	
	//@JAPR 2014-02-18: Removida la configuración del catálogo de la encuesta
	function getUnusedCatMembers($aCatalogIDs = null)
	{	
		//Ahora generará un array indexado por catálogo, con la información del atributo anterior usado. Sólo considera los catálogos que ya
		//están siendo usados en preguntas simple choice dentro de secciones estándar
		//Este array tiene un elemento: contiene el último atributo usado antes de la posición de esta pregunta
		$arrCatMembersByCatalog = array();
		$countQuestion = 0;
		$memberOrder = 0;
		
		//Primero obtiene todos los catálogos utilizados en la encuesta
		if (is_null($aCatalogIDs) || !is_array($aCatalogIDs)) {
			$aCatalogIDs = array();
			$sql = "SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Question A, SI_SV_Catalog B, SI_SV_Section C 
				WHERE A.CatalogID = B.CatalogID AND A.SectionID = C.SectionID AND C.SectionType = ".sectNormal." AND 
					A.SurveyID = {$this->SurveyID} AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Catalog, SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF) {
				$catalogID = (int) @$aRS->fields["catalogid"];
				if ($catalogID > 0) {
					$aCatalogIDs[] = $catalogID;
				}
				$aRS->MoveNext();
			}
		}
		
		//Recorre todos los catálogos involucrados en la encuesta para obtener los atributos disponibles
		foreach ($aCatalogIDs as $catalogID)
		{
			//Ya no hay necesidad de este query porque todos los catálogos que entren aquí obviamente tienen al menos una pregunta
			$countQuestion = 1;
			if ($countQuestion>0)
			{
				//Obtenemos el MemberOrder
				$sql = "SELECT A.CatMemberID, C.MemberOrder 
					FROM SI_SV_Question A, SI_SV_Section B, SI_SV_CatalogMember C 
					WHERE A.SurveyID = ".$this->SurveyID." AND A.QTypeID = ".qtpSingle." AND A.UseCatalog = 1 AND 
						A.CatalogID = ".$catalogID." AND A.SectionID = B.SectionID AND B.SectionType = ".sectNormal." AND 
						A.CatMemberID = C.MemberID 
					ORDER BY A.QuestionNumber DESC";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question, SI_SV_Section, SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$memberOrder = 0;
				if (!$aRS->EOF)
				{
					//Obtenemos el MemberOrder
					$memberOrder = (int) @$aRS->fields["memberorder"];
				}
				$arrCatMembersByCatalog[$catalogID] = $memberOrder;
			}
		}
		
		//Obtiene todos los atributos que están disponibles a partir del anterior usado a la posición donde se está creando/editando
		//la sección (siempre debería haber al menos un atributo previo, y deberían existir atributos posteriores, pero si no los hay, simplemente
		//el array regresará vacío para este catálogo)
		$strIDs = implode(',', $aCatalogIDs);
		if (trim($strIDs) == '') {
			return array();
		}
		
		$sql = "SELECT CatalogID, MemberID, MemberName, MemberOrder 
			FROM SI_SV_CatalogMember 
			WHERE CatalogID IN ({$strIDs}) 
			ORDER BY CatalogID, MemberOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				
				$memberID = (int)$aRS->fields["memberid"];
				$memberName = $aRS->fields["membername"];
				$tmpOrder = (int)$aRS->fields["memberorder"];
				
				//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
				//Ahora se preguntará por cualquier catálogo usado en la encuesta, ya que no existe mas el catálogo de la encuesta
				//if($lastID==$catalogID)
				$memberOrder = (int) @$arrCatMembersByCatalog[$lastID];
				
				//Si el orden es mayor al obtenido de las preguntas de catalogo
				//entonces si se agrega el atributo en caso contrario lo ignora
				if($tmpOrder>$memberOrder)
				{
					$aGroup[$memberID] = $memberName;
				}
				
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}
		
		return $allValues;
	}
	
	//@JAPR 2015-01-22: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
	//si es utilizado y advertir al usuario (#75TPJE)
	/* Regresa un array con los nombres (o IDs) de las preguntas de esta sección que contienen alguna referencia a ellas mediante variables
	dentro de alguna otra sección que no sea esta misma. Utilizada especialmente para validación de borrado de secciones
	*/
	function getQuestionsWithVarReferences($bGetIDs = false) {
		//@JAPR 2015-10-25: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
		//Ahora el array se regresará como un texto directamente con los casos encontrados
		if (!$bGetIDs) {
			$arrUsedQuestions = '';
		}
		else {
			$arrUsedQuestions = array();
		}
		//@JAPR
		
		$arrQuestionsByID = array();
		$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
		if (is_null($objQuestionColl)) {
			return $arrUsedQuestions;
		}
		
		foreach ($objQuestionColl->Collection as $objQuestion) {
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
		}
		
		//Recorre todas las preguntas de esta sección para validar una por una si se utilizan en alguna otra configuración
		$strAnd = '';
		foreach ($objQuestionColl->Collection as $objQuestion) {
			$inQuestionID = $objQuestion->QuestionID;
			//En este caso no basta con saber si se usa en otras configuraciones y en cuales, hay que saber específicamente si
			//se utiliza dentro de alguna otra sección, por tanto se pide obtener sólo los IDs de los objetos donde se utiliza
			//@JAPR 2015-10-25: Agregada la validación de uso de objetos en propiedades que se pueden limpiar o que cuya referencia puede quedarse sin afectar al producto
			//Ahora el array se regresará como un texto directamente con los casos encontrados
			$arrObjects = SearchVariablePatternInMetadata($this->Repository, $this->SurveyID, $inQuestionID, otyQuestion, null, false, $bGetIDs);
			if (!$bGetIDs) {
				if (trim($arrObjects) != '') {
					$arrUsedQuestions .= $strAnd.$arrObjects;
					$strAnd = "\n";
				}
			}
			else {
				if (!is_null($arrObjects) && is_array($arrObjects)) {
					$blnUsed = false;
					foreach ($arrObjects as $strObjectTypeName => $arrObjectIDs) {
						switch ($strObjectTypeName) {
							case 'Surveys':
								//Si se usa a nivel de sección, es un ShowStopper inmediato
								$blnUsed = true;
								break;
							case 'Sections':
								//Si se usa en secciones, sólo cuenta si no es la propia sección que se está validando, ya que si lo
								//fuera entonces de todas maneras se borrarán las referencias donde se usa
								foreach ($arrObjectIDs as $intUsedObjectID) {
									if ($intUsedObjectID != $this->SectionID) {
										$blnUsed = true;
										break;
									}
								}
								break;
							case 'Questions':
								//Si se usa dentro de preguntas, sólo cuenta si la pregunta no pertenece a la propia sección que se
								//está validando
								foreach ($arrObjectIDs as $intUsedObjectID) {
									if (!isset($arrQuestionsByID[$intUsedObjectID])) {
										$blnUsed = true;
										break;
									}
								}
								break;
							case 'Answers':
							default:
								//Si se usa dentro de opciones de respuesta, entonces es el mismo caso que con preguntas, pero ya se
								//habría validado en el case de Questions así que este array y cualquier otro son ignorados
								break;
						}
						
						if ($blnUsed) {
							break;
						}
					}
					
					if ($blnUsed) {
						if ($bGetIDs) {
							$arrUsedQuestions[] = $objQuestion->QuestionID;
						}
						else {
							$arrUsedQuestions[] = $objQuestion->QuestionText;
						}
					}
				}
			}
			//@JAPR
		}
		
		return $arrUsedQuestions;
	}
	//@JAPR
	
	function generateResultFilter()
	{
		$resultFilter = "";
		
		//Realizamos instancia del catalogo para obtener el filtro por catalogo
		$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		
		$resultFilter = $aCatalog->generateResultFilterByUser($_SESSION["PABITAM_UserID"]);
		
		return $resultFilter;
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			if($this->SectionType!=sectFormatted)
			{
				$myChildren[] = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
				
				//@JAPR 2014-08-07: Agregados los datos heredados desde eBavel para secciones Recap
				if (getMDVersion() >= esvRecapDynamicData) {
					if ($this->SectionType == sectRecap) {
						//Si la sección es tipo Recap, se deben agregar filtros dinámicos basados en los campos de eBavel para permitir obtener
						//datos que se heredarán durante la captura de la sección
						$myChildren[] = BITAMSectionFilterCollection::NewInstance($this->Repository, $this->SectionID);
					}
				}
				//@JAPR
			}		
			else
			{
				//15Feb2013: Si es sección formateada, las preguntas se mostrarán sólo si la sección tiene preguntas.
				//Una sección formateada puede tener preguntas sólo si en versiones anteriores a una pregunta le cambiaron
				//el campo Change Section y eligieron una sección formateada, entonces se habilitan las preguntas
				//para que puedan cambiarse de sección o eliminarse ya que no se soporta una sección formateada con preguntas.
				//También ya se agregó el cambio para que una pregunta no pueda moverse a una sección formateada.
				$countQuestion = BITAMSection::getQuestionCountBySectionID($this->Repository, $this->SectionID);
				if($countQuestion>0)
				{
					$myChildren[] = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
				}
			}
		}
		
		return $myChildren;
	}
	
	static function getSurveyIDBySectionID($aRepository, $aSectionID)
	{
		$aSurveyID = -1;
		
		$sql = "SELECT t1.SurveyID FROM SI_SV_Section t1 WHERE t1.SectionID = ".$aSectionID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$aSurveyID = (int)$aRS->fields["surveyid"];
		}
		
		return $aSurveyID;
	}
	
	static function getCurrentOrderPos($aRepository, $aSurveyID, $aSectionID)
	{
		$sql = "SELECT SectionNumber FROM SI_SV_Section WHERE SurveyID = ".$aSurveyID." AND SectionID = ".$aSectionID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$orderPos = -1;

		if(!$aRS->EOF)
		{
			$orderPos = (int)$aRS->fields["sectionnumber"];
		}
		
		return $orderPos;
	}
	
	//@JAPR 2014-08-20: Agregada una función genérica (no basada en un catálogo) para extraer valores de una forma de eBavel según los parámetros
	/* Basado en la definición de mapeo de esta sección (la cual debe ser exclusivamente tipo Recap), obtiene los valores de la forma
	correspondiente de eBavel y los formatea para utilizarse en una respuesta de definición de un App de eForms
	El parámetro $aFieldsArray permitiría teóricamente recuperar valores diferentes a los de la definición de la forma, pero no se permitirá
	consultar valores de una forma diferente a la mapeada, generalmente no se recibiría este parámetro para forzar a obtener todos los campos
	que si están mapeados como preguntas
	El parámetro $aFieldsFilters permite entre especificar filtros para campos específicos si se trata de un array, donde cada elemento se
	indexa por el ID numérico del campo, o bien especifica un filtro directo al WHERE si lo que se recibe es un String
	*/
	function GetEBavelFieldValues($aFieldsArray = null, $aFieldsFilters = null, $iReturnType = ebdtJSonArrayForApps) {
		if ($this->SectionType != sectRecap || $this->SectionFormID <= 0) {
			return null;
		}
		
		//Genera el array de campos que están mapeados en esta definición de encuesta
		if (is_null($aFieldsArray) || !is_array($aFieldsArray)) {
			$aFieldsArray = array();
			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
			foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
				if ($aQuestion->eBavelFieldID > 0) {
					$aFieldsArray[] = $aQuestion->eBavelFieldID;
				}
			}
		}
		
		if (is_null($aFieldsFilters) || !(is_string($aFieldsFilters) || is_array($aFieldsFilters))) {
			$aFieldsFilters = array();
		}
		
		$arrayValues = GetEBavelFieldValues($this->Repository, $this->SectionFormID, $aFieldsArray, $aFieldsFilters, $iReturnType);
		return $arrayValues;
	}
	
	//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
	//Dada una sección, traduce las variables de cada campo de eBavel a los nombres de campo correspondientes
	//regresando el filtro listo para ejecutarse según el contexto
	static function TranslateFieldVarsToFieldNames($aRepository, $aSectionID, $aFilterText) {
		if (trim($aFilterText) == '') {
			return '';
		}
		
		$objSection = BITAMSection::NewInstanceWithID($aRepository, $aSectionID);
		if (is_null($objSection) || $objSection->SectionFormID <= 0) {
			return $aFilterText;
		}
		
		$arreBavelFormColl = @GetEBavelSections($aRepository, false, null, array($objSection->SectionFormID));
		if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			return $aFilterText;
		}
		$objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
		
		$arreBavelFields = @GetEBavelFieldsForms($aRepository, true, null, array($aFormID));
		if (is_null($arreBavelFields) || !is_array($arreBavelFields) || count($arreBavelFields) == 0) {
			return $aFilterText;
		}
		
		$questionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $aSectionID);
		if (is_null($questionCollection) || count($questionCollection->Collection) == 0) {
			return $aFilterText;
		}
		
		$strFilterText = $aFilterText;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
		foreach ($questionCollection->Collection as $intMemberOrder => $objQuestion) {
			if ($objQuestion->eBavelFieldID <= 0) {
				continue;
			}
			
			$objeBavelField = @$arreBavelFields[$objQuestion->eBavelFieldID];
			if (is_null($objeBavelField)) {
				continue;
			}
			
			$strMemberVarName = GetEBavelFieldVariable($objeBavelField);
			$arrVariableNames[$intMemberOrder] = $strMemberVarName;
		}
		
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$objQuestion = @$questionCollection->Collection[$intMemberOrder];
			if (is_null($objQuestion)) {
				continue;
			}
			
			$objeBavelField = @$arreBavelFields[$objQuestion->eBavelFieldID];
			if (is_null($objeBavelField)) {
				continue;
			}
			
			$strFieldName = (string) @$objeBavelField['id'];
			$strFilterText = str_ireplace($strMemberVarName, $strFieldName, $strFilterText);
		}
		//@JAPR
		
		return $strFilterText;
	}
	//@JAPR
	
	//14Feb2013: Se agregró el cuarto parámetro para que no incluya las secciones formateadas
	//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
	//Modificado el parámetro $includeSecFormated por $arrExcepSectionTypes para permitir especificar que tipos de sección no se deben regresar
	static function getSectionsFromSurvey($aRepository, $aSurveyID, $arrayExcept=array(), $arrExcepSectionTypes=array())
	{
		if (!is_array($arrayExcept))
		{
			$arrayExcept = array($arrayExcept);
		}

		$arraySections = array();
		
		$filter = "";
		switch (count($arrayExcept))
		{
			case 0:
				break;
			case 1:
				$filter = " AND SectionID <> ".((int)$arrayExcept[0]);
				break;
			default:
				foreach ($arrayExcept as $aSectionID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int)$aSectionID; 
				}
				
				if ($filter != "")
				{
					//@JAPR 2014-10-03: Corregido un bug, se había dejado un Alias siendo que el query no los maneja
					$filter = " AND SectionID NOT IN (".$filter.")";
				}
				break;
		}
		
		//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
		//Modificado el parámetro $includeSecFormated por $arrExcepSectionTypes para permitir especificar que tipos de sección no se deben regresar
		if (!is_array($arrExcepSectionTypes)) {
			$arrExcepSectionTypes = array($arrExcepSectionTypes);
		}
		$filterSectionType = "";
		switch (count($arrExcepSectionTypes))
		{
			case 0:
				break;
			case 1:
				$filterSectionType = " AND SectionType <> ".((int)$arrExcepSectionTypes[0]);
				break;
			default:
				foreach ($arrExcepSectionTypes as $aSectionType)
				{
					if ($filterSectionType != "")
					{
						$filterSectionType .= ", ";
					}
					$filterSectionType .= (int)$aSectionType; 
				}
				
				if ($filterSectionType != "")
				{
					$filterSectionType = " AND SectionType NOT IN (".$filterSectionType.")";
				}
				break;
		}
		$filter .= $filterSectionType;
		//@JAPR
		
		$sql = "SELECT SectionID, SectionNumber, SectionName FROM SI_SV_Section WHERE SurveyID = ".$aSurveyID." ".$filter;
		//14Feb2013: Se agregró el cuarto parámetro para que no incluya las secciones formateadas
		//@JAPR 2015-01-13: Corregido un bug, las preguntas tipo sección no se deben poder mover dentro de otra sección múltiple (#XWKDHA)
		//Modificado el parámetro $includeSecFormated por $arrExcepSectionTypes para permitir especificar que tipos de sección no se deben regresar
		/*
		if($includeSecFormated==false)
		{
			$sql .= " AND SectionType <> ".sectFormatted;
		}
		*/
		//@JAPR
		
		$sql .= " ORDER BY SectionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$tmpSectionID = (int)$aRS->fields["sectionid"];
			$tmpSectionNumber = (int)$aRS->fields["sectionnumber"];
			$tmpSectionName = $aRS->fields["sectionname"];
			
			$arraySections[$tmpSectionID] = $tmpSectionNumber.". ".$tmpSectionName;

			$aRS->MoveNext();
		}
		
		return $arraySections;
	}

	//@JAPR 2012-07-26: Agregado el salto de sección a sección
	static function GetSectionsFromNumber($aRepository, $aSurveyID, $aSectionNumber = 0)
	{
		$arrayNextSections = array();
		$arrayNextSections[0] = "None";
		
		$sql = "SELECT SectionID, SectionNumber, SectionName 
			FROM SI_SV_Section 
			WHERE SurveyID = ".$aSurveyID." AND SectionNumber >= ".$aSectionNumber." 
			ORDER BY SectionNumber";		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$sectionid = (int)$aRS->fields["sectionid"];
			$sectionname = $aRS->fields["sectionname"];
			$arrayNextSections[$sectionid] = $sectionname;
			
			$aRS->MoveNext();
		}
		
		return $arrayNextSections;
	}
	
	//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
	/* Regresa un array con las secciones que se encuentran mapeadas (es decir, a las cuales apunta hacia ellas una pregunta tipo qtpSection) y que
	básicamente necesitan un trato especial, ya que estas secciones no pueden ser utilizadas como un salto desde otro punto ni deberían aparecer
	en el selector de secciones entre muchas validaciones mas
	Este método se debe utilizar en diferentes puntos donde se configuren este tipo de cosas, además de los métodos que preparan las definiciones
	que se envían a los dispositivos para validar que no se utilicen en ninguno de esos puntos
	El array multi-dimensional regresará datos importantes, pero con el parámetro $bGetExtendedData activado, de lo contrario simplemente regresa una
	lista de las secciones clasificadas de esta manera
	Dado a que esta función se podría llamar con frecuencia, los datos obtenidos se guardarán en el cache para optimizar las llamadas posteriores
	*/
	static function GetMappedSections($aRepository, $aSurveyID, $bGetExtendedData = true) {
		$arrayMappedSections =& BITAMGlobalFormsInstance::GetMappedSectionsWithID($aSurveyID);
		if (is_null($arrayMappedSections) || !is_array($arrayMappedSections)) {
			$arrayMappedSections = array();
			$sql = "SELECT B.SectionID, B.SectionName, B.SectionType, B.SectionNumber 
				FROM SI_SV_Question A 
					INNER JOIN SI_SV_Section B ON (A.SourceSectionID = B.SectionID) 
				WHERE A.SurveyID = {$aSurveyID} AND A.QTypeID = ".qtpSection." 
				ORDER BY B.SectionNumber";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS) {
				while (!$aRS->EOF) {
					$arrData = array();
					$sectionid = (int) @$aRS->fields["sectionid"];
					$arrData['id'] = $sectionid;
					$arrData['name'] = (string) @$aRS->fields["sectionname"];
					$arrData['number'] = (int) @$aRS->fields["sectionnumber"];
					$arrData['type'] = (int) @$aRS->fields["sectiontype"];
					$arrayMappedSections[$sectionid] = $arrData;
					$aRS->MoveNext();
				}
			}
			
			//El array se guarda con la información completa
		 	BITAMGlobalFormsInstance::AddMappedSectionsWithID($aSurveyID, $arrayMappedSections);
		}
		
		$arrReturn = $arrayMappedSections;
		if (!$bGetExtendedData) {
			//Si no se pidieron datos extendidos, se regresa la colección simple sólo con el nombre
			$arrReturn = array();
			foreach ($arrayMappedSections as $intSectionID => $arrData) {
				$arrReturn[$intSectionID] = $arrData['name'];
			}
		}
		
		return $arrReturn;
	}
	//@JAPR
	
	static function getSectionNumber($aRepository, $aSectionID)
	{
		$sectionNumber = 0;

		$sql = "SELECT t1.SectionNumber FROM SI_SV_Section t1 WHERE t1.SectionID = ".$aSectionID;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$sectionNumber = (int)$aRS->fields["sectionnumber"];
		}

		return $sectionNumber;
	}
	
	static function removeHyperlinks($htmlText)
	{
		$patterns = array();
		$patterns[0] = '/<a ([^>]*)>/i';
		$patterns[1] = '/(<\/a>)/i';

		$replacements = array();
		$replacements[0] = '';
		$replacements[1] = '';

		$newHtmlText = preg_replace($patterns, $replacements, $htmlText);
		
		return $newHtmlText;
	}
	
	static function convertImagesToLabels($htmlText)
	{
		//Obtenemos el arreglo de tags de imagenes
		$count = preg_match_all('/<img([^>]*)>/i', $htmlText, $arrayTemp);
		
		if($count!==false && $count>0)
		{
			$arrayTags = $arrayTemp[0];
		
			foreach($arrayTags as $tag)
			{
				$countAlt = preg_match_all('/alt\=(\"|\')([^\"\']*)(\"|\')/i', $tag, $results, PREG_SET_ORDER);
				
				if($countAlt!==false && $countAlt>0)
				{
					$label = '<span>'.$results[0][2].'</span>';
				}
				else 
				{
					$label = '<span></span>';
				}
		
				$htmlText = str_replace($tag, $label, $htmlText);
			}
		}

		return $htmlText;
	}
	
	static function convertImagesToBase64($htmlText)
	{
		//Obtenemos todas las imagenes de las etiquetas IMG contenidas en el $htmlText
		$images = BITAMSection::getArrayImages($htmlText);
		
		//Ahora obtenemos los base64encode las imagenes obtenidas anteriormente
		//Y lo sustituimos sobre el codigo $htmlText
		foreach ($images as $urlimg)
		{
			$htmlText = str_replace($urlimg, BITAMSection::base64_encode_image($urlimg), $htmlText);
		}
		
		return $htmlText;
	}
	
	//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
	/* Dado un string, parsea el mismo en busca de archivos de imagenes y cuando los encuentra los transforma a una ruta relativa tal como se usará
	en el App de eForms
	//@JAPR 2015-07-19: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
	Agregado el parámetro $bSingleImage para indicar que el texto HTML realmente es sólo la ruta a una imagen, así que no debe buscar una envoltura <img> sino directamente tratarla
	como una imagen, sin embargo dada la manera en que funcionaban las Apps, si se requiere que se regrese como un tag image así que en este caso se envuelve en esta función directamente
	//JAPR 2015-08-06: Modificada para utilizar DHTMLX
	//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>. Originalmente todas las propiedades se grababan con dicho tag y se mandaban tal
	cual al App, sin embargo a partir de que se empezó a usar la version v6, las propiedades ya no se graban como tag sino sólo como ruta, así que hubo que ajustar estas funciones para
	agregar el tag, pero luego se implementó DHTMLX en el app y se tuvieron que usar rutas relativas locales así que para ciertas propiedades NO se debe agregar el tag, este parámetro
	nos indicará para cuales NO se desea agregar (originalmente sólo la propiedad displayImage del Survey se mandaba como relativa). Sólo aplica si $bSingleImage == true
	*/
	static function convertImagesToRelativePath($aRepository, $htmlText, $bSingleImage = false, $bUseImgTag = true) {
		global $appVersion;
		
		//@JAPR 2015-07-19: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
		//Si no se lograron identificar tag image en el texto recibido pero si hay un contenido en dicho texto, además que se indicó que era sólo una ruta de imagen, entonces si procesa
		//el contenido agregando un propio tag Image
		if ($bSingleImage && trim($htmlText) != '') {
			//@JAPRWarning: Podría faltar concatenar la ruta de este servicio
			//JAPR 2015-08-06: Modificada para utilizar DHTMLX
			//Si el App que sincroniza es >= esveFormsDHTMLX, entonces ya está preparada para usar las imagenes en formato relativo y sin tag <Img>, así que no es necesario procesar el
			//resto de esta función puesto que esa regresará todo como si fuera un tag <Img> y se usa sólo si no es una propiedad sencilla de imagen (un texto formateado por ejemplo)
			if ($appVersion >= esveFormsDHTMLX && !$bUseImgTag) {
				return "{@IMGPATH}".GetImageRelativeNameForApp($aRepository, $htmlText);
			}
			else {
				$htmlText = '<img src="'.GetBaseURLPath().$htmlText.'" />';
			}
		}
		//@JAPR
		
		//Obtenemos todas las imagenes de las etiquetas IMG contenidas en el $htmlText
		$images = BITAMSection::getArrayImages($htmlText, $bDie);
		
		//Ahora obtenemos los base64encode las imagenes obtenidas anteriormente
		//Y lo sustituimos sobre el codigo $htmlText
		//Debido a que para IOs es diferente la ruta de archivos que para Android, y que las referencias a imagenes tienen que incluir esta diferencia
		//cuando se utilizan localmente en un tag <IMG src=""> o URL(), se tiene que agregar una constante de ruta de imagen que el propio server tendrá
		//que incluir en el Path cuando se descargan definiciones para dispositivos, ya que el server no puede anticipar correctamente la ruta que se 
		//utilizará pues depende del dispositivo, así que esta variable especial funcionará para ajustarla dinámicamente {@IMGPATH}
		foreach ($images as $urlimg)
		{
			$htmlText = str_replace($urlimg, "{@IMGPATH}".GetImageRelativeNameForApp($aRepository, $urlimg), $htmlText);
		}
		
		return $htmlText;
	}
	//@JAPR
	
	static function getArrayImages($htmlText, $bDie = null)
	{
		$images = array();
		$count = preg_match_all('/(img|src)\=(\"|\')[^\"\'\>]+/i', $htmlText, $media);
		if (($bDie && false) || getParamValue('DebugBreakImg', 'both', '(int)', true)) {
			echo("<br>htmlTxtMatch: {$htmlText}, count == {$count}");
			PrintMultiArray($media);
		}
		if($count!==false && $count>0)
		{
			$dataImg = preg_replace('/(img|src)(\"|\'|\=\"|\=\')(.*)/i', "$3", $media[0]);
			if (($bDie && false) || getParamValue('DebugBreakImg', 'both', '(int)', true)) {
				echo("<br>dataImg");
				PrintMultiArray($dataImg);
			}
	
			foreach($dataImg as $url)
			{
				$info = pathinfo($url);
				if (($bDie && false) || getParamValue('DebugBreakImg', 'both', '(int)', true)) {
					echo("<br>url == {$url}");
					PrintMultiArray($info);
				}
				if (isset($info['extension']))
				{
					$info['extension'] = strtolower($info['extension']);
					if (($info['extension'] == 'jpg') || ($info['extension'] == 'jpeg') || ($info['extension'] == 'gif') || ($info['extension'] == 'png'))
					{
						array_push($images, $url);
					}
				}
			}
		}
		
		return (array_values(array_unique($images)));
	}

	static function base64_encode_image($filename) 
	{
		require_once("thumbnail.inc.php");

		$thumbnail = new Thumbnail($filename);
		//@JAPR 2014-04-15: Modificado el límite de tamaño de 200 a 600
		$thumbnail->max_size_auto(640);
		//@JAPR
		
		//Almacenamos dicho thumbnail a archivo,  pero primero generamos nombre de archivo temporal
		$tempDir = "tmp";
	
		do
		{
			$newfilename = $tempDir."/"."thumb".sprintf("%04x", rand(0, 32768)).".{$thumbnail->img["format"]}";
		}
		while (file_exists($newfilename));
	
		$thumbnail->save($newfilename);
		
		//@JAPR: Corrección de MBarragan integrada porque se necesitaba actualizar este archivo en los servidores
		$img64 = '';
		if(!$thumbnail->isCorrupted()) {
			if (fopen($newfilename, "rb"))
			{
				$imgbinary = stream_get_contents(fopen($newfilename, "rb"));
				$img64 = 'data:image/'.$thumbnail->img["format"].';base64,'.base64_encode($imgbinary);
			}
			else 
			{
				$img64 = '';
			}
			
			//Eliminamos la imagen temporal para que no queden residuos en la carpeta tmp
			unlink($newfilename);
		}
		//@JAPR
		
		return $img64;
	}
	
	//@JAPR 2014-10-21: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
	//Obtiene la lista de secciones Inline cuyas opciones de registros deberán llenarse a partir de esta sección
	function getSectionsToFill() {
		$arrSectionsToFill = array();
		
		$sql = "SELECT SectionID 
			FROM SI_SV_Section 
			WHERE SurveyID = {$this->SurveyID} AND SourceSectionID = {$this->SectionID} 
				UNION 
			SELECT SectionID 
			FROM SI_SV_Section 
			WHERE SurveyID = {$this->SurveyID} AND SourceQuestionID IN (
					SELECT QuestionID 
					FROM SI_SV_Question 
					WHERE SurveyID = {$this->SurveyID} AND QTypeID = ".qtpSection." AND SourceSectionID = {$this->SectionID} 
				)";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF) {
			$arrSectionsToFill[] = (int)$aRS->fields["sectionid"];
			$aRS->MoveNext();
		}
		
		return $arrSectionsToFill;
	}
	//@JAPR
	
	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		//@JAPR 2013-03-12: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		global $appVersion;
		//@JAPR 2015-06-12: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		
		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$arrInvalidSections = array();
		if (getMDVersion() >= esvSectionQuestions) {
			$arrInvalidSections = @BITAMSection::GetMappedSections($this->Repository, $this->SurveyID);
		}
		//@JAPR
		
		$arrDef = array();
		
		$arrDef['id'] = $this->SectionID;
		$arrDef['surveyID'] = $this->SurveyID;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['name'] = $this->SectionName;
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strData = $this->SectionNameHTML;
			//@JAPR 2015-08-03: Removido el nombre con HTML de las secciones, si no se especificó se tomará el nombre normal
			if (trim($strData) == '') {
				$strData = $this->SectionName;
			}
			$arrDef['nameHTML'] = $strData;
			//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
			//Temporalmente se sobreescribirá el nombre del objeto con la versión formateada, pero el App debería decidir utilizar cada uno según el contexto
			if (trim($strData) != '') {
				//Para el modo de diseño, se mandará la propiedad especial nameRaw para poder presentar el nombre sin formato alguno y sin afectar al App
				$arrDef['nameRaw'] = $arrDef['name'];
				$arrDef['name'] = $strData;
			}
			
			//@JAPR 2015-06-27: Rediseñado el Admin con DHTMLX
			//Ahora el tipo de llenado del componente será un campo mas en la metadata
			$arrDef['valuesSourceType'] = $this->ValuesSourceType;
		}
		//@JAPR
		$arrDef['number'] = $this->SectionNumber;
		$arrDef['type'] = $this->SectionType;
		$arrDef['catalogID'] = $this->CatalogID;
		$arrDef['catMemberID'] = $this->CatMemberID;
		//@JAPR 2015-08-21: Validado para que en caso de que no se tenga un dataSource configurado o no sea de dicho tipo, no se mande la info del catálogo
		if (getMDVersion() >= esveFormsv6) {
			//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType
			if ($gbDesignMode) {
				$arrDef['sectionType'] = $this->SectionType;
				//La única diferencia es si el SectionType es Inline, ya que hay que considerar el tipo de despliegue para regresarlo como dinámica
				if ($this->DisplayMode == sdspPages) {
					$arrDef['sectionType'] = sectDynamic;
				}
			}
			//@JAPR
			
			if ($this->DataSourceID <= 0 || $this->ValuesSourceType != tofCatalog) {
				$arrDef['catalogID'] = 0;
				$arrDef['catMemberID'] = 0;
			}
		}
		
		//@JAPR 2013-03-12: Corregido un bug, no estaba incrustando las imagenes de secciones formateadas (#28818)
		$formattedText = $this->FormattedText;
		//@JAPR 2014-07-30: Agregado el Responsive Design
		if (getMDVersion() >= esvResponsiveDesign && $appVersion >= esvDeviceDataLog) {
			$intDeviceID = identifyDeviceType();
			$formattedText = $this->getResponsiveDesignProperty("FormattedText", $intDeviceID, true);
		}
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$formattedText = $this->TranslateVariableQuestionIDsByNumbers($formattedText);
		//@JAPR
		
		if (!$blnWebMode) {
	        //Si esta habilitado el envio de imagenes entonces vamos a pasarlas a codigo base64
	        //en caso contrario se convierten en etiquetas con la descripcion definida en Image Description
	        if ($this->SendThumbnails == 1) {
				//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
				if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
	            	$formattedText = BITAMSection::convertImagesToBase64($formattedText);
				}
	            elseif (!$blnWebMode) {
	            	$formattedText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $formattedText);
	            }
	            //@JAPR
	        } else {
	            $formattedText = BITAMSection::convertImagesToLabels($formattedText);
	        }
			
	        //Si se deshabilito los hipervinculos entonces se eliminan dichos anchors
	        if ($this->EnableHyperlinks == 0) {
	            $formattedText = BITAMSection::removeHyperlinks($formattedText);
	        }
		}
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['formattedText'] = $formattedText;
		//@JAPR
		$arrDef['sendThumbnails'] = $this->SendThumbnails;
		$arrDef['enableHyperlinks'] = $this->EnableHyperlinks;
		$arrDef['nextSectionID'] = $this->NextSectionID;
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$arrDef['autoRedraw'] = $this->AutoRedraw;
		}
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		if (getMDVersion() >= esvShowSectionCondition) {
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$strShowCondition = $this->TranslateVariableQuestionIDsByNumbers($this->ShowCondition);
			//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrDef['showCondition'] = $strShowCondition;
			//@JAPR
		}
		//@JAPR 2014-05-21: Agregado el tipo de sección Inline
		if ($this->SectionType == sectInline) {
			$arrDef['selectorQuestionID'] = $this->SelectorQuestionID;
			$arrDef['displayMode'] = $this->DisplayMode;
			//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			//Si la sección es utilizada por una pregunta tipo sección, su tipo de despliegue debe descargarse como Inline
			if (isset($arrInvalidSections[$this->SectionID])) {
				$arrDef['displayMode'] = sdspInline;
			}
			//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
			$arrDef['switchBehaviour'] = $this->SwitchBehaviour;
		//OMMC 2015-11-26: Agregado el selectorQuestionID para modo de diseño para evitar mover la pregunta selector de sección.
		}else{
			if($gbDesignMode) {
				$arrDef['selectorQuestionID'] = $this->SelectorQuestionID;
			}
		}
		
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if (getMDVersion() >= esvSketchPlus) {
			if ($this->SectionType == sectMasterDet) {
				$arrDef['xPosQuestionID'] = $this->XPosQuestionID;
				$arrDef['yPosQuestionID'] = $this->YPosQuestionID;
				$arrDef['itemNameQuestionID'] = $this->ItemNameQuestionID;
			}
		}
		//@JAPR
		
        //2015.02.10 JCEM #9T6ASP        
        if (getMDVersion() >= esvMinRecordsToSelect){
            if ($this->SectionType == sectInline || $this->SectionType == sectNormal) {
               $arrDef['MinRecordsToSelect'] = $this->MinRecordsToSelect;
            }
		}
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$formattedText = $this->HTMLFooter;
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign && $appVersion >= esvDeviceDataLog) {
				$intDeviceID = identifyDeviceType();
				$formattedText = $this->getResponsiveDesignProperty("HTMLFooter", $intDeviceID, true);
			}
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$formattedText = $this->TranslateVariableQuestionIDsByNumbers($formattedText);
			//@JAPR 2015-09-08: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
			$formattedTextDes = $formattedText;
			
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
	            $formattedText = BITAMSection::convertImagesToBase64($formattedText);
			}
			elseif (!$blnWebMode) {
				$formattedText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $formattedText);
			}
			//@JAPR
			//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrDef['htmlFooter'] = $formattedText;
			//@JAPR 2015-09-08: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
			if ($gbDesignMode) {
				$arrDef['htmlFooterDes'] = $formattedTextDes;
			}
			//@JAPR
			
			$formattedText = $this->HTMLHeader;
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign && $appVersion >= esvDeviceDataLog) {
				$intDeviceID = identifyDeviceType();
				$formattedText = $this->getResponsiveDesignProperty("HTMLHeader", $intDeviceID, true);
			}
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$formattedText = $this->TranslateVariableQuestionIDsByNumbers($formattedText);
			//@JAPR 2015-09-08: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
			$formattedTextDes = $formattedText;
			
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
	            $formattedText = BITAMSection::convertImagesToBase64($formattedText);
			}
			elseif (!$blnWebMode) {
				$formattedText = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $formattedText);
			}
			//@JAPR
			//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrDef['htmlHeader'] = $formattedText;
			//@JAPR 2015-09-08: Corregido un bug, en modo diseño debe reemplazar la variable de rutas relativas
			if ($gbDesignMode) {
				$arrDef['htmlHeaderDes'] = $formattedTextDes;
			}
			//@JAPR
		}
		
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		if (getMDVersion() >= esvFixedInlineSections) {
			if (trim($this->StrSelectOptions)) {
				$arrDef['options'] = array();
				$arrDef['optionsOrder'] = array();
				$arrOptions = explode("\r\n", $this->StrSelectOptions);
				foreach ($arrOptions as $optionValue) {
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			    	$strOptionValueUTF = $optionValue;
			    	$arrDef['options'][$strOptionValueUTF] = array();
			    	$arrDef['options'][$strOptionValueUTF]['name'] = $strOptionValueUTF;
			    	$arrDef['optionsOrder'][] = $strOptionValueUTF;
				}
			}
		}
		//@JAPR
		
		if(getMDVersion() >= esvSectionRecap) {
			$arrDef['SectionFormID'] = $this->SectionFormID;
		}
		
		//Conchita 2014-08-11 Agregado el summaryinmdsection
		if(getMDVersion() >= esvShowInOnePageMD) {
			$arrDef['summaryInMDSection'] = $this->SummaryInMDSection;
		}

		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if(getMDVersion() >= esvSurveySectDisableOpts) {
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			//@JAPR 2014-10-07: Corregido un bug, se debe buscar por la llave y no por el contenido
			if (isset($arrInvalidSections[$this->SectionID])) {
				//Si la opción se encuentra mapeada en alguna pregunta, entonces no debe aparecer en el selector de secciones
				$arrDef['hideFromMenu'] = 1;
			}
			else {
				$arrDef['hideFromMenu'] = ($this->ShowInNavMenu)?0:1;
			}
		}
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if(getMDVersion() >= esvInlineMerge) {
			$arrDef['hideSelector'] = ($this->ShowSelector)?0:1;
			//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
			//El requerimiento original cambió durante la implementación y finalmente ya no se utilizará una configuración a nivel de sección, sino que
			//se basará en la existencia de preguntas Inline, se dejará el campo por si se desea aplicarlo de alguna otra forma mas adelante, pero por
			//ahora se va a asignar automáticamente en la definición descargada
			//$arrDef['showAsQuestion'] = ($this->ShowAsQuestion)?1:0;
			//@JAPR 2014-10-07: Corregido un bug, se debe buscar por la llave y no por el contenido
			$arrDef['showAsQuestion'] = (isset($arrInvalidSections[$this->SectionID]))?1:0;
		}

		//@JAPR 2015-06-11: Rediseñado el Admin con DHTMLX
		if ($gbDesignMode) {
			$arrDef['objectType'] = otySection;
			//Si se trata del modo de diseño, se debe devolver la configuración de la sección en lugar del valor mejor aplicado para efectos de captura, por lo
			//tanto se agregara una configuración adicional
			$arrDef['showInNavMenu'] = $this->ShowInNavMenu;
			$arrDef['repetitions'] = $this->Repetitions;
			$arrDef['dataSourceID'] = $this->DataSourceID;
			$arrDef['dataSourceMemberID'] = $this->DataSourceMemberID;
			//El displayMode es parte indispensable del diseño, así que ahora se envía independientemente de si es o no una sección Inline, ya que para las maestro-detalle cuenta en diseño
			//@JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
			$arrDef['displayMode'] = $this->DisplayMode;
			$arrDef['strSelectOptions'] = $this->StrSelectOptions;
			//@JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
			$strData = $this->TranslateVariableQuestionIDsByNumbers($this->DataSourceFilter);
			if ($this->SectionType == sectInline && $this->DataSourceID > 0) {
				$strData = BITAMDataSource::ReplaceMemberIDsByNames($this->Repository, $this->DataSourceID, $strData);
			}
			$arrDef['dataSourceFilter'] = $strData;
			//JAPR 2015-08-24: Corregido un bug, no estaba aplicando la propiedad por usar un nombre equivocado
			$arrDef['showSelector'] = ($this->ShowSelector)?1:0;
			//@JAPR
		}
		//@JAPR
		
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if(getMDVersion() >= esvInlineTypeOfFilling && $this->SectionType == sectInline && $appVersion >= esvInlineTypeOfFilling) {
			//@JAPR 2015-08-21: Modificado para que en v6 se regresen siempre las propiedades aunque sea con 0
			if (getMDVersion() >= esveFormsv6) {
				$arrDef['sourceQuestionID'] = 0;
				$arrDef['sourceSectionID'] = 0;
			}
			//@JAPR
			
			switch ($this->typeOfFilling()) {
				case tofMCHQuestion:
					$arrDef['sourceQuestionID'] = $this->SourceQuestionID;
					break;
				case tofInlineSection:
					$arrDef['sourceSectionID'] = $this->SourceSectionID;
					break;
			}
			
			//Sólo se asigna la colección de secciones a llenar, si esta pregunta es tipo múltiple choice y hay alguna sección configurada para llenarse
			//a partir de ella, o bien si esta pregunta es simple choice selector de otra sección Inline y hay alguna sección configurada para llenarse
			//a partir de su sección Inline. Respecto al segundo caso, también aplica si esta pregunta es tipo Sección - Inline y la sección a la que
			//mapea se configuró para ser quien llena a otras secciones Inline
			$arrDef['fillTargetSectionChildren'] = $this->getSectionsToFill();
		}
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			$arrDef['hideNavButtons'] = $this->HideNavButtons;
		}
		//@JAPR
		//OMMC 2016-01-19: Agregado para guardar el estado del editor HTML para el header y el footer
		if(getMDVersion() >= esvHTMLEditorState){
			$arrDef['HTMLHEditorState'] = $this->HTMLHEditorState;
			$arrDef['HTMLFEditorState'] = $this->HTMLFEditorState;
		}
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		if (getMDVersion() >= esvTableSectDescRow) {
			$arrDef['useDescriptorRow'] = $this->UseDescriptorRow;
		}
		//@JAPR
		
        //Obtenemos coleccion de preguntas
        $arrDef['questions'] = array();
        $arrDef['questionsOrder'] = array();
        $questionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->SectionID);
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-08-01: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				//Carga las propiedades dinámicas equivalentes al responsive Design pero sólo si se recibió el tipo de dispositivo que está
				//haciendo la petición
				$intDeviceID = identifyDeviceType();
				if ($intDeviceID > dvcWeb) {
					BITAMQuestion::GetResponsiveDesignProps($this->Repository, $questionCollection, null, null, $this->SectionID);
				}
			}
		}
		//@JAPR
		
        foreach ($questionCollection->Collection as $questionElement) {
        	$arrDef['questions'][$questionElement->QuestionID] = $questionElement->getJSonDefinition();
	        $arrDef['questionsOrder'][] = $questionElement->QuestionID;
        }
		
		return $arrDef;
	}
	
	//15Feb2013: Retorna cuantas preguntas tiene una sección
	function getQuestionCountBySectionID($aRepository, $aSectionID)
	{
		$count = 0;
		
		$sql = "SELECT COUNT(*) AS QuestionCount FROM SI_SV_Section WHERE SectionID = ".$aSectionID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$count = (int)$aRS->fields["questioncount"];
		}
		
		return $count;
	}
	
	//15Feb2013: Retorna tipo de sección
	function getSectionType($aRepository, $aSectionID)
	{
		$sectype = 0;
		
		$sql = "SELECT SectionType FROM SI_SV_Section WHERE SectionID = ".$aSectionID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$sectype = (int)$aRS->fields["sectiontype"];
		}
		
		return $sectype;
	}
}

class BITAMSectionCollection extends BITAMCollection
{
	public $SurveyID;
	
	function __construct($aRepository, $aSurveyID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->SurveyID = $aSurveyID;
		$this->OrderPosField = "SectionNumber";
		$this->MsgValidateReorder = translate("A dynamic section can not go before the section that contains the simple choice question using the form\'s catalog");
	}
	
	static function NewInstanceEmpty($aRepository, $aSurveyID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aSurveyID);
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
	//@JAPR 2017-02-21: Agregado el método para caché de secciones, parámetro $bAddCache. Se hizo de esta manera porque no hubo tiempo de verificar si agregar cache siempre habría o no
	//afectado algún proceso, por tanto sólo se usará bajo demanda específicamente en el proceso que lo va a utilizar
	static function NewInstance($aRepository, $aSurveyID, $anArrayOfSectionIDs = null, $sectionType = null, $bAddCache = false)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
		//@JAPR 2017-02-21: Agregado el método para caché de secciones
		if($bAddCache && (is_null($anArrayOfSectionIDs) || count($anArrayOfSectionIDs)<=0)&&(is_null($sectionType)))
		{
			$anInstance =& BITAMGlobalFormsInstance::GetSectionCollectionBySurveyWithID($aSurveyID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aSurveyID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = "";
		
		if (!is_null($anArrayOfSectionIDs))
		{
			switch (count($anArrayOfSectionIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.SectionID = ".((int)$anArrayOfSectionIDs[0]);
					break;
				default:
					foreach ($anArrayOfSectionIDs as $aSectionID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aSectionID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.SectionID IN (".$filter.")";
					}
					break;
			}
		}
		
		$filterSectionType="";
		if($sectionType!==null)
		{
			$filterSectionType = " AND t1.SectionType = ".$sectionType." ";
		}
		
		//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
		$strAdditionalFields = "";
		if (getMDVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.NextSectionID ';
		}
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$strAdditionalFields .= ', t1.AutoRedraw ';
		}
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		if (getMDVersion() >= esvShowSectionCondition) {
			$strAdditionalFields .= ', t1.ShowCondition ';
		}
		//@JAPR 2014-05-22: Agregado el tipo de sección Inline
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		if (getMDVersion() >= esvInlineSection) {
			$strAdditionalFields .= ', t1.SelectorQuestionID, t1.DisplayMode, t1.SwitchBehaviour ';
		}
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$strAdditionalFields .= ', t1.HTMLFooter, t1.HTMLHeader ';
		}
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		if (getMDVersion() >= esvFixedInlineSections) {
			$strAdditionalFields .= ', t1.OptionsText ';
		}
		//@MABH 2014-06-17
		if (getMDVersion() >= esvSectionRecap) {
			$strAdditionalFields .= ', t1.SectionFormID ';
		}
		//Conchita Agregado el summary en las paginas de secciones maestro detalle
		if (getMDVersion() >= esvShowInOnePageMD) {
			$strAdditionalFields .= ', t1.SummaryInMDSection ';
		}
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.ShowInNavMenu ';
		}
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion ';
		}
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (getMDVersion() >= esvInlineMerge) {
			$strAdditionalFields .= ', t1.ShowSelector, t1.ShowAsQuestion ';
		}
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$strAdditionalFields .= ', t1.SourceQuestionID, t1.SourceSectionID ';
		}
        //2015.02.11 JCEM #9T6ASP
        if(getMDVersion() >= esvMinRecordsToSelect){
            $strAdditionalFields .= ', t1.MinRecordsToSelect';
        }
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
            $strAdditionalFields .= ', t1.SectionNameHTML, t1.ValuesSourceType';
        }
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		if (getMDVersion() >= esveFormsv6) {
            $strAdditionalFields .= ', t1.DataSourceID, t1.DataSourceMemberID, t1.DataSourceFilter';
        }
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			$strAdditionalFields .= ', t1.HideNavButtons ';
		}
		//OMMC 2016-01-19: Agregado para el estado del editor HTML de header y footer
		if (getMDVersion() >= esvHTMLEditorState){
			$strAdditionalFields .= ', t1.HTMLHEditorState, t1.HTMLFEditorState';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID';
		}
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		if (getMDVersion() >= esvTableSectDescRow) {
			$strAdditionalFields .= ', t1.UseDescriptorRow';
		}
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if (getMDVersion() >= esvSketchPlus) {
			$strAdditionalFields .= ', t1.XPosQuestionID, t1.YPosQuestionID, t1.ItemNameQuestionID';
		}
		//@JAPR
		$sql = "SELECT t1.SurveyID, t2.SurveyName, t1.SectionID, t1.SectionName, t1.SectionNumber, t1.SectionDimValID, t1.SectionType, t1.CatalogID, t1.CatMemberID, 
				t1.FormattedText, t1.SendThumbnails, t1.EnableHyperlinks $strAdditionalFields 
			FROM SI_SV_Section t1, SI_SV_Survey t2 
			WHERE t1.SurveyID = t2.SurveyID AND t1.SurveyID = ".$aSurveyID.$filterSectionType.$filter." ORDER BY t1.SectionNumber";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			
			$aRS->MoveNext();
		}
		
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
		//@JAPR 2017-02-21: Agregado el método para caché de secciones
		if ($bAddCache && (is_null($anArrayOfSectionIDs) || count($anArrayOfSectionIDs)<=0)&&(is_null($sectionType)))
		{
			BITAMGlobalFormsInstance::AddSectionCollectionBySurveyWithID($aSurveyID, $anInstance);
		}
		//@JAPR
		
		return $anInstance;
	}

	//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Obtiene la colección de secciones pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección para 
	cada forma además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance pero con todos
	los parámetros en null excepto el SurveyID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrSectionsBySurveyID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2012-07-26: Agregado el salto de sección a sección (Campo NextSectionID)
		$strAdditionalFields = "";
		if (getMDVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.NextSectionID ';
		}
		//@JAPR 2013-11-19: Agregada la opción para redibujar las secciones formateadas automáticamente
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$strAdditionalFields .= ', t1.AutoRedraw ';
		}
		//@JAPR 2013-12-09: Agregada la opción para determinar si una sección se muestra o no durante la captura en base a una condición
		if (getMDVersion() >= esvShowSectionCondition) {
			$strAdditionalFields .= ', t1.ShowCondition ';
		}
		//@JAPR 2014-05-22: Agregado el tipo de sección Inline
		//@JAPR 2014-05-26: Agregado el switch de comportamiento de las secciones Inline
		if (getMDVersion() >= esvInlineSection) {
			$strAdditionalFields .= ', t1.SelectorQuestionID, t1.DisplayMode, t1.SwitchBehaviour ';
		}
		//@JAPR 2014-06-04: Agregado el header y footer formateado de las secciones
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$strAdditionalFields .= ', t1.HTMLFooter, t1.HTMLHeader ';
		}
		//@JAPR 2014-06-20: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
		if (getMDVersion() >= esvFixedInlineSections) {
			$strAdditionalFields .= ', t1.OptionsText ';
		}
		//@MABH 2014-06-17
		if (getMDVersion() >= esvSectionRecap) {
			$strAdditionalFields .= ', t1.SectionFormID ';
		}
		//Conchita Agregado el summary en las paginas de secciones maestro detalle
		if (getMDVersion() >= esvShowInOnePageMD) {
			$strAdditionalFields .= ', t1.SummaryInMDSection ';
		}
		//@JAPR 2014-08-18: Agregada la configuración para ocultar secciones del menú de navegación
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.ShowInNavMenu ';
		}
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion ';
		}
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (getMDVersion() >= esvInlineMerge) {
			$strAdditionalFields .= ', t1.ShowSelector, t1.ShowAsQuestion ';
		}
		//@JAPR 2014-10-20: Agregada la opción para que las secciones Inline obtengan sus opciones de respuesta a partir de otras fuentes en la captura
		if (getMDVersion() >= esvInlineTypeOfFilling) {
			$strAdditionalFields .= ', t1.SourceQuestionID, t1.SourceSectionID ';
		}
		//2015.02.11 JCEM #9T6ASP
		if(getMDVersion() >= esvMinRecordsToSelect){
			$strAdditionalFields .= ', t1.MinRecordsToSelect';
		}
		//@JAPR 2015-06-26: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.SectionNameHTML, t1.ValuesSourceType';
		}
		//@JAPR 2015-08-20: Agregado el soporte para secciones Inline y dinámicas en v6
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', t1.DataSourceID, t1.DataSourceMemberID, t1.DataSourceFilter';
		}
		//@RTORRES 2015-08-28: Agregado ocultar boton next,back
		if (getMDVersion() >= esveHideNavButtonsSection) {
			$strAdditionalFields .= ', t1.HideNavButtons ';
		}
		//OMMC 2016-01-19: Agregado para el estado del editor HTML de header y footer
		if (getMDVersion() >= esvHTMLEditorState){
			$strAdditionalFields .= ', t1.HTMLHEditorState, t1.HTMLFEditorState';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID';
		}
		//@JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
		if (getMDVersion() >= esvTableSectDescRow) {
			$strAdditionalFields .= ', t1.UseDescriptorRow';
		}
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if (getMDVersion() >= esvSketchPlus) {
			$strAdditionalFields .= ', t1.XPosQuestionID, t1.YPosQuestionID, t1.ItemNameQuestionID';
		}
		//@JAPR
		$sql = "SELECT t1.SurveyID, t2.SurveyName, t1.SectionID, t1.SectionName, t1.SectionNumber, t1.SectionDimValID, t1.SectionType, t1.CatalogID, t1.CatMemberID, 
				t1.FormattedText, t1.SendThumbnails, t1.EnableHyperlinks $strAdditionalFields 
			FROM SI_SV_Section t1, SI_SV_Survey t2 
			WHERE t1.SurveyID = t2.SurveyID {$filter} 
			ORDER BY t1.SurveyID, t1.SectionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$objSection = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
			$anInstance->Collection[] = $objSection;
			
			//Agrega el objeto a la colección indexada por los padres
			$intSurveyID = $objSection->SurveyID;
			if ( !isset($arrSectionsBySurveyID[$intSurveyID]) ) {
				$arrSectionsBySurveyID[$intSurveyID] = array();
			}
			$arrSectionsBySurveyID[$intSurveyID][] = $objSection;
			
			$aRS->MoveNext();
		}

		//Finalmente procesa el array de formas para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché (si no había
		//ninguna forma especificada, entonces se ejecutará un query para cargar todas las formas del repositorio, lo óptimo es filtrar la función por formas)
		if (isset(BITAMGlobalFormsInstance::$arrAllSurveyInstances)) {
			$arrAllSectionCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllSurveyInstances as $objSurvey) {
				$intSurveyID = $objSurvey->SurveyID;
				//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
				$arrAllSectionCollection[$intSurveyID] = new $strCalledClassColl($aRepository, $intSurveyID);
				//$objSectionCollection = new $strCalledClassColl($aRepository, $intSurveyID);
				if ( isset($arrSectionsBySurveyID[$intSurveyID]) ) {
					$arrAllSectionCollection[$intSurveyID]->Collection = $arrSectionsBySurveyID[$intSurveyID];
				}
				BITAMGlobalFormsInstance::AddSectionCollectionBySurveyWithID($intSurveyID, $arrAllSectionCollection[$intSurveyID]);
			}
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		global $gbDesignMode;
		
		//@JAPR 2015-06-22: Agregado el reordenamiento de objetos
		//Se leerá directo del request, en caso de no venir se leerá del objeto enviado como parámetro para procesar el request
		$aSurveyID = getParamValue('SurveyID', 'both', '(int)', true);
		if (is_null($aSurveyID)) {
			if (array_key_exists("SurveyID", $aHTTPRequest->GET))
			{
				$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
			}
			else
			{
				$aSurveyID = 0;
			}
		}
		
		if(array_key_exists("SectionNumber", $aHTTPRequest->POST))
		{
			$orderPos = (int)$aHTTPRequest->POST["SectionNumber"];
			$aSectionID = (int)$aHTTPRequest->POST["SectionID"];
			BITAMSectionCollection::Reorder($aRepository, $aSurveyID, $aSectionID, $orderPos);
			//@JAPR 2015-06-22: Agregado el reordenamiento de objetos
			if ($gbDesignMode) {
				return null;
			}
			//@JAPR
		}
		
		if(array_key_exists("SectionNumber", $aHTTPRequest->POST))
		{
			$anInstance = BITAMSection::NewInstance($aRepository, $aSurveyID);
			$anInstance = $anInstance->get_Parent();
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else 
		{
			return BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
		}
	}
	
	static function Reorder($aRepository, $aSurveyID, $aSectionID, $orderPos)
	{
		$prevOrderPos = BITAMSection::getCurrentOrderPos($aRepository, $aSurveyID, $aSectionID);
		
		if($prevOrderPos>0) {
			$nexQuestionsToDelete = array();
			$questionsToDelNextQuestion = array();
			
			if($orderPos > $prevOrderPos)
			{
				//Obtenemos el numero de preguntas que tiene la seccion afectada
				$sqlCount = "SELECT COUNT(*) AS CountQuestions FROM SI_SV_Question WHERE SectionID = ".$aSectionID;
				$aRSCount = $aRepository->DataADOConnection->Execute($sqlCount);
				if($aRSCount===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlCount);
				}
				
				$countQuestions = (int)$aRSCount->fields["countquestions"];
				
				//Obtenemos los IDs de las secciones que se van a mover excepto la seccion afectada directamente
				$sqlAllReg = "SELECT SectionID, SectionNumber FROM SI_SV_Section WHERE ( SectionNumber BETWEEN ".$prevOrderPos." AND ".$orderPos." ) 
							AND SurveyID = ".$aSurveyID." AND SectionID <> ".$aSectionID." ORDER BY SectionNumber";
				$aRSAllReg = $aRepository->DataADOConnection->Execute($sqlAllReg);
				if($aRSAllReg===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlAllReg);
				}
				
				$allSections = array();
				
				while(!$aRSAllReg->EOF)
				{
					$tempSectionNumber = $aRSAllReg->fields["sectionnumber"];
					$tempSectionID = $aRSAllReg->fields["sectionid"];
					$allSections[$tempSectionNumber] = $tempSectionID;
					
					$aRSAllReg->MoveNext();
				}
				
				//Obtenemos el numero de preguntas de las demas secciones que se movieron excepto la afectada
				$sqlAllCount = "SELECT COUNT(*) AS CountAllQuestions FROM SI_SV_Question WHERE SectionID IN (".implode(",", $allSections).")";
				$aRSAllCount = $aRepository->DataADOConnection->Execute($sqlAllCount);
				if($aRSAllCount===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlAllCount);
				}
				
				$countAllQuestions = (int)$aRSAllCount->fields["countallquestions"];
				
				//Obtenemos los QuestionID y NextSection que se tendran q actualizar en NextQuestion porque haran referencias a secciones previas
				$sql = "SELECT A.QuestionID, A.NextSection FROM SI_SV_QAnswers A, SI_SV_Question B WHERE A.QuestionID = B.QuestionID 
						AND B.QTypeID = 2 AND B.UseCatalog = 0 AND B.SectionID = ".$aSectionID." AND A.NextSection IN 
						(
  							SELECT SectionID FROM SI_SV_Section WHERE SectionID IN (".implode(",", $allSections).") 
						) GROUP BY A.QuestionID, A.NextSection ORDER BY B.QuestionNumber";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if($aRS===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while(!$aRS->EOF)
				{
					$tempQuestionID = (int)$aRS->fields["questionid"];
					$tempNextQuestion = (int)$aRS->fields["nextsection"];
					$questionsToDelNextQuestion[] = $tempQuestionID;
					$nexQuestionsToDelete[] = $tempNextQuestion;

					$aRS->MoveNext();
				}
				
				$questionsToDelNextQuestion = array_values(array_unique($questionsToDelNextQuestion));
				$nexQuestionsToDelete = array_values(array_unique($nexQuestionsToDelete));

				//Procedemos a actualizar los QuestionNumber de SI_SV_Question de las secciones que se movieron excepto la afectada
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber - ".$countQuestions."	WHERE SectionID IN (".implode(",", $allSections).") AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Procedemos a actualizar el QuestionNumber de SI_SV_Question de las preguntas de la seccion afectada
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber + ".$countAllQuestions." WHERE SectionID = ".$aSectionID." AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Actualizar el SectionNumber en todas las secciones
				$sql = "UPDATE SI_SV_Section SET SectionNumber = SectionNumber - 1 
						WHERE ( SectionNumber BETWEEN ".$prevOrderPos." AND ".$orderPos." ) AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				//Actualizar el SectionNumber de la seccion afectada
				$sql = "UPDATE SI_SV_Section SET SectionNumber = ".$orderPos." 
						WHERE SurveyID = ".$aSurveyID." AND SectionID = ".$aSectionID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Se deberan eliminar la referencia a los NextSections si es que aplica
				if(count($nexQuestionsToDelete)>0 && count($questionsToDelNextQuestion)>0)
				{
					$sql = "UPDATE SI_SV_QAnswers SET NextSection = 0 
							WHERE QuestionID IN (".implode(",", $questionsToDelNextQuestion).") AND NextSection IN (".implode(",", $nexQuestionsToDelete).")";
					if($aRepository->DataADOConnection->Execute($sql)===false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			
			if ($prevOrderPos > $orderPos)
			{
				//Obtenemos el numero de preguntas que tiene la seccion afectada
				$sqlCount = "SELECT COUNT(*) AS CountQuestions FROM SI_SV_Question WHERE SectionID = ".$aSectionID;
				$aRSCount = $aRepository->DataADOConnection->Execute($sqlCount);
				if($aRSCount===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlCount);
				}
				
				$countQuestions = (int)$aRSCount->fields["countquestions"];

				//Obtenemos los IDs de las secciones que se van a mover excepto la seccion afectada directamente
				$sqlAllReg = "SELECT SectionID, SectionNumber FROM SI_SV_Section WHERE ( SectionNumber BETWEEN ".$orderPos." AND ".$prevOrderPos." ) 
							AND SurveyID = ".$aSurveyID." AND SectionID <> ".$aSectionID." ORDER BY SectionNumber";
				$aRSAllReg = $aRepository->DataADOConnection->Execute($sqlAllReg);
				if($aRSAllReg===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlAllReg);
				}
				
				$allSections = array();
				
				while(!$aRSAllReg->EOF)
				{
					$tempSectionNumber = $aRSAllReg->fields["sectionnumber"];
					$tempSectionID = $aRSAllReg->fields["sectionid"];
					$allSections[$tempSectionNumber] = $tempSectionID;
					
					$aRSAllReg->MoveNext();
				}

				//Obtenemos el numero de preguntas de las demas secciones que se movieron excepto la afectada
				$sqlAllCount = "SELECT COUNT(*) AS CountAllQuestions FROM SI_SV_Question WHERE SectionID IN (".implode(",", $allSections).")";
				$aRSAllCount = $aRepository->DataADOConnection->Execute($sqlAllCount);
				if($aRSAllCount===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlAllCount);
				}
				
				$countAllQuestions = (int)$aRSAllCount->fields["countallquestions"];
				
				//Obtenemos los QuestionID y NextSection que se tendran q actualizar en NextQuestion porque haran referencias a secciones previas
				$sql = "SELECT A.QuestionID, A.NextSection FROM SI_SV_QAnswers A, SI_SV_Question B WHERE A.QuestionID = B.QuestionID 
						AND B.QTypeID = 2 AND B.UseCatalog = 0 AND B.SectionID IN (".implode(",", $allSections).") AND A.NextSection IN 
						(
  							SELECT SectionID FROM SI_SV_Section WHERE SectionID = ".$aSectionID." 
						) GROUP BY A.QuestionID, A.NextSection ORDER BY B.QuestionNumber";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if($aRS===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while(!$aRS->EOF)
				{
					$tempQuestionID = (int)$aRS->fields["questionid"];
					$tempNextQuestion = (int)$aRS->fields["nextsection"];
					$questionsToDelNextQuestion[] = $tempQuestionID;
					$nexQuestionsToDelete[] = $tempNextQuestion;

					$aRS->MoveNext();
				}
				
				$questionsToDelNextQuestion = array_values(array_unique($questionsToDelNextQuestion));
				$nexQuestionsToDelete = array_values(array_unique($nexQuestionsToDelete));
				
				//Procedemos a actualizar los QuestionNumber de SI_SV_Question de las secciones que se movieron excepto la afectada
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber + ".$countQuestions."	WHERE SectionID IN (".implode(",", $allSections).") AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				//Procedemos a actualizar el QuestionNumber de SI_SV_Question de las preguntas de la seccion afectada
				$sql = "UPDATE SI_SV_Question SET QuestionNumber = QuestionNumber - ".$countAllQuestions." WHERE SectionID = ".$aSectionID." AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				//Actualizamos todas las secciones
				$sql = "UPDATE SI_SV_Section SET SectionNumber = SectionNumber + 1 
						WHERE ( SectionNumber BETWEEN ".$orderPos." AND ".$prevOrderPos." ) AND SurveyID = ".$aSurveyID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Actualizamos el SectionNumber de la seccion afectada directamente
				$sql = "UPDATE SI_SV_Section SET SectionNumber = ".$orderPos." 
						WHERE SurveyID = ".$aSurveyID." AND SectionID = ".$aSectionID;
				if($aRepository->DataADOConnection->Execute($sql)===false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//Eliminamos referencias de NextSection que apunten a las preguntas de la seccion afectada
				if(count($nexQuestionsToDelete)>0 && count($questionsToDelNextQuestion)>0)
				{
					$sql = "UPDATE SI_SV_QAnswers SET NextSection = 0 
							WHERE QuestionID IN (".implode(",", $questionsToDelNextQuestion).") AND NextSection IN (".implode(",", $nexQuestionsToDelete).")";
					if($aRepository->DataADOConnection->Execute($sql)===false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			
			//Actualizar datos de modificacion de la encuesta
			BITAMSurvey::updateLastUserAndDateInSurvey($aRepository, $aSurveyID);
		}
	}

	function get_Parent()
	{
		return BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
	}

	function get_Title()
	{
		return translate("Sections");
	}

	function get_QueryString()
	{
		//return "BITAM_PAGE=Survey&SurveyID=".$this->SurveyID;
		return "BITAM_SECTION=SectionCollection&SurveyID=".$this->SurveyID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Section&SurveyID=".$this->SurveyID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SectionID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DisplaySection";
		$aField->Title = translate("Section");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2015-01-13: Agregada información adicional a la forma
		$options = array();
		$options[sectNormal] = translate('Standard question & answer');
		$options[sectDynamic] = translate('List-based');
		$options[sectFormatted] = translate('Formatted text');
		$options[sectMasterDet] = translate('Master-Detail');
		$options[sectInline] = translate('Inline');
		$options[sectRecap] = translate('Recap');
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SectionType";
		$aField->Title = translate("Type");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $options;
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status!=2)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
			
			if($aSurveyInstance->Status==0)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-01-22: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
	//si es utilizado y advertir al usuario (#75TPJE)
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
		foreach ($this->Collection as $aSection)
		{
			//Verifica si esta sección tiene o no referencias, si es así entonces la marca como no eliminable para que el canRemove falle
			//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
			//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
			//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
			//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
			$strVarsUsed = SearchVariablePatternInMetadata($this->Repository, $aSection->SurveyID, $aSection->SectionID, otySection);
			if ($strVarsUsed != '') {
				$aSection->enableRemove = false;
			}
			
			if ($aSection->enableRemove) {
				$arrUsedQuestions = $aSection->getQuestionsWithVarReferences();
				if (!is_null($arrUsedQuestions) && is_array($arrUsedQuestions)) {
					if (count($arrUsedQuestions) > 0) {
						$aSection->enableRemove = false;
					}
				}
			}
			//@JAPR
		}
	}
	
	//15Marzo2013:
	//Que no puedas mover la sección dinámica a una posición que esté antes que la sección 
	//que contiene la pregunta simple choice del mismo catálogo que la dinámica (catálogo de la encuesta)
	function validateReorder()
	{
		$dynamicSectionID = BITAMSection::existDynamicSection($this->Repository, $this->SurveyID);
		if($dynamicSectionID>0)
		{
			//Obtener cual es la posición a partir de la cual debe estar la dinámica
			//Si se mueve antes de esa posición no debe permitirse

			$sql = "SELECT MAX(t2.SectionNumber) AS SectionNumber
					FROM SI_SV_Question t1, SI_SV_Section t2, SI_SV_Survey t3
					WHERE t1.SectionID = t2.SectionID AND
					t2.SurveyID = t3.SurveyID AND
					t1.SurveyID=".$this->SurveyID." AND 
					t1.QTypeID=".qtpSingle." AND 
					t1.UseCatalog = 1 AND 
					t1.CatalogID = t3.CatalogID";

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
			if($aRS===false)
			{
				die("(".__METHOD__.")".translate("Error accessing")." SI_SV_Question t1, SI_SV_Section t2, SI_SV_Survey t3 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$position = 0;
			
			if(!$aRS->EOF)
			{
				$position = (int) $aRS->fields["sectionnumber"];
			}
?>			
			//Sí hay dinámca
			//Si se está moviendo la sección dinámica
			if(objectID==<?=$dynamicSectionID?>)
			{
				if(orderPos <= <?=$position?>)
				{
					canReorder = false;
				}
			}
<?			
			//En caso de que se mueva la sección que contiene una simple choice con catálogo de la encuesta
			//tiene que verificarse que esa sección no se mueva abajo de la sección dinámica
			$sql = "SELECT t1.SectionID
					FROM SI_SV_Question t1, SI_SV_Section t2, SI_SV_Survey t3
					WHERE t1.SectionID = t2.SectionID AND
					t2.SurveyID = t3.SurveyID AND
					t1.SurveyID=".$this->SurveyID." AND 
					t1.QTypeID=".qtpSingle." AND 
					t1.UseCatalog = 1 AND 
					t1.CatalogID = t3.CatalogID";
	
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
				
			if($aRS===false)
			{
				die("(".__METHOD__.")".translate("Error accessing")." SI_SV_Question t1, SI_SV_Section t2, SI_SV_Survey t3 ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Secciones que contienen pregunta simple choice que utilizan el catálogo de la encuesta
			$sectionIDs = array();
			
			while(!$aRS->EOF)
			{
				$sectionIDs[] = (int) $aRS->fields["sectionid"];
				$aRS->MoveNext();
			}
			
			if(count($sectionIDs)>0)
			{
?>
			//Secciones que contienen pregunta simple choice que utilizan el catálogo de la encuesta
			var sectionIDs = new Array();
<?
			foreach ($sectionIDs as $i => $sectionID)
			{
?>		
				sectionIDs[<?=$i?>] = <?=$sectionID?>;
<?
			}
?>			
			if(inArray(sectionIDs, objectID))
			{
				//Es una sección que contiene una pregunte simple choice que utiliza catálogo de la encuesta
				//debe validarse que no se mueva después de la sección dinámica
<?
				$dynamicPos = BITAMSection::getCurrentOrderPos($this->Repository, $this->SurveyID, $dynamicSectionID);
?>			
				var dynamicPos = <?=$dynamicPos?>;
				if(orderPos>=dynamicPos)
				{
					canReorder = false;
				}
			}
<?
			}
		}//Fin del if($dynamicSectionID>0)	
	}
}
?>