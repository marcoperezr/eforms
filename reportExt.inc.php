<?php
//Esta clase es una copia de report.inc.php generada exclusivamente con la intención de ser usada durante la edición de una captura a partir de v4
//NO se debe usar para administrar o exportar un reporte, a menos que sea con adecuaciones específicas de v4 o posterior, debido a esto mismo
//los métodos que están registrados aquí y que venían originalmente de la clase report, no necesariamente están actualizados, si se pretende
//substituir por completo a report con esta clase, entonces se deben mantener sincronizados, pero se dejó por compatiblidad con v3 ya que se 
//programó en paralelo

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("paginator.inc.php");

$svStartDate=null;
$svEndDate=null;
$svUserID=null;
$svIpp=null;
$hasPic=array();
$hasDocument=array();
$hasComment=array();

class BITAMReportExt extends BITAMObject
{
	public $ReportID;
	public $FolioID;
	public $SurveyID;
	public $UserID;
	public $UserName;
	public $DateID;
	public $QuestionFields;
	public $NumQuestionFields;
	public $Questions;
	public $QuestionText;
	public $EmailID;
	public $EmailDesc;
	public $SurveyName;

	public $Survey;
	public $FactKeyDimVal;
	public $HasDynamicSection;
	public $DynamicSectionID;
	public $DynamicSection;
	public $DynQuestionCollection;
	public $Catalog;
	public $ChildDynamicAttribID;
	public $ArrayDynSectionValues;
	//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
	public $HasMasterDetSection;
	public $MasterDetSectionIDs;
	public $MasterDetQuestionCollection;
	public $ArrayMasterDetSectionValues;

	function __construct($aRepository, $aSurveyID)
	{
		BITAMObject::__construct($aRepository);
		$this->ReportID = -1;
		$this->FolioID = -1;
		$this->SurveyID = $aSurveyID;
		$this->Survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$this->UserID = -1;
		$this->UserName = "";
		$this->DateID = "";
		$this->FactKeyDimVal = -1;
		$this->SurveyName = $this->Survey->SurveyName;
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		$this->Survey->getSpecialFieldsStatus();
		//@JAPR

		$this->QuestionFields = array();
		$this->FieldTypes = array();
		$this->QuestionText = array();
		
		$this->EmailID = 1;
		$this->EmailDesc = "NA";

		$this->HasDynamicSection = false;
		$this->DynamicSectionID = -1;
		$this->DynamicSection = null;
		$this->DynQuestionCollection = null;
		$this->Catalog = null;
		$this->ChildDynamicAttribID = -1;
		$this->ArrayDynSectionValues = array();
		//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
		$this->HasMasterDetSection = false;
		$this->MasterDetSectionIDs = array();
		$this->MasterDetQuestionCollection = array();
		$this->ArrayMasterDetSectionValues = array();
		
		$this->HasPath = false;
		$this->MsgConfirmRemove = "Do you wish to delete this entry?";
		
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
		
		if($dynamicSectionID>0)
		{
			$this->HasDynamicSection = true;
			$this->DynamicSectionID = $dynamicSectionID;
			$this->DynamicSection = BITAMSection::NewInstanceWithID($aRepository, $this->DynamicSectionID);
			
			//Obtenemos el ChildDynamicAttribID
			$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, $this->DynamicSection->CatMemberID);
			
			//Obtener el siguiente CatMemberID
			$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->DynamicSection->CatalogID." AND MemberOrder > ".$catMemberInstance->MemberOrder." ORDER BY MemberOrder ASC";
			
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			if(!$aRS->EOF)
			{
				//Asignamos el atributo hijo
				$this->ChildDynamicAttribID = (int)$aRS->fields["parentid"];
				
				//Obtenemos coleccion de las preguntas de la seccion dinamica
				$this->DynQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->DynamicSectionID);
				
				//Obtenemos el catalogo
				$this->Catalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DynamicSection->CatalogID);
			}
			else 
			{
				//Se vuelven a quedar los valores como si no hubiera seccion dinamica
				$this->HasDynamicSection = false;
				$this->DynamicSectionID = -1;
				$this->DynamicSection = null;
			}
		}

		//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
		$arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $aSurveyID, true);
		if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
		{
			$this->HasMasterDetSection = true;
			$this->MasterDetSectionIDs = $arrMasterDetSections;
			
			//Obtiene la colección de preguntas para indexarlas por cada Id de sección Maestro - Detalle
			foreach ($this->MasterDetSectionIDs as $aSectionID)
			{
				$aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $aSectionID);
				if (!is_null($aQuestionCollection))
				{
					$this->MasterDetQuestionCollection[$aSectionID] = $aQuestionCollection;
				}
				else 
				{
					$this->MasterDetQuestionCollection[$aSectionID] = BITAMQuestionCollection::NewInstanceEmpty($this->Repository, $this->SurveyID);
				}
			}
		}
		//@JAPR
		
		$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, null, null);
		$this->Questions = $questions;
		$numFields = count($questions->Collection);

		for($i=0; $i<$numFields; $i++)
		{
			$fieldName = $questions->Collection[$i]->SurveyField;
			$fieldType = $questions->Collection[$i]->QTypeID;

			if($fieldType==1)
			{
				$this->$fieldName = 0;
			}
			else
			{
				$this->$fieldName = "";
			}

			$this->QuestionFields[$i] = $fieldName;
			$this->FieldTypes[$i] = $fieldType;
			$this->QuestionText[$i] = $questions->Collection[$i]->QuestionText;
		}

		$this->NumQuestionFields = $numFields;
	}

	static function NewInstance($aRepository, $aSurveyID)
	{
		return new BITAMReportExt($aRepository, $aSurveyID);
	}

	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
	//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
	static function NewInstanceWithID($aRepository, $aReportID, $aSurveyID, $bGetValues = true)
	{
		$anInstance = null;

		if (((int)$aReportID) < 0 || ((int)$aSurveyID) < 0 )
		{
			return $anInstance;
		}

		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		$anInstance = BITAMReportExt::NewInstance($aRepository, $aSurveyID);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si se usarán registros únicos, lo que se necesita es el FactKeyDimVal y no el FactKey
		$intFactKeyDimVal = 0;
		if ($anInstance->Survey->UseStdSectionSingleRec) {
			$sql = "SELECT FactKeyDimVal FROM ".$aSurvey->SurveyTable." WHERE FactKey = ".$aReportID;
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			if (!$aRS->EOF)
			{
				$intFactKeyDimVal = (int) @$aRS->fields["factkeydimval"];
			}
		}
		//@JAPR
		
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;
		
		$sql = "SELECT t1.FolioID, t1.FactKey, t1.UserID, t1.DateID, t1.StartTime, t1.EndTime, 
			C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, t1.FactKeyDimVal ";
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
						break;
						
					default:
						$sql.=", t1.".$anInstance->QuestionFields[$i];
						break;
				}
				//@JAPR
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY"." AS ".$attribField."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", t1.".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
				else {
					$catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					$attribFieldKey = "RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField.", ".$catTable.".".$attribFieldKey." AS ".$attribField."KEY";
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en elFROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=" AND t1.".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable." t1 ".$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		
		//Ligamos las Tablas para lo de Email
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		$sql .= " WHERE t1";
		if ($anInstance->Survey->UseStdSectionSingleRec && $intFactKeyDimVal > 0) {
			//El registro EntrySectionID == 0 es el que contiene los datos de las preguntas estándar, y para haber podido configurar que usara
			//registro único quiere decir que debe existir esta columna también
			$sql .= ".FactKeyDimVal = ".$intFactKeyDimVal." AND t1.EntrySectionID = 0 ";
		}
		else {
			$sql .= ".FactKey = ".$aReportID;
		}
		$sql .= " ".$joinCat." AND t1.FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey;
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nMain query: $sql");
		}
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
			//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
			$anInstance = BITAMReportExt::NewInstanceFromRS($aRepository, $aSurveyID, $aRS, $bGetValues);
			//@JAPR
		}

		return $anInstance;
	}

	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
	//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
	static function NewInstanceFromRS($aRepository, $aSurveyID, $aRS, $bGetValues = true)
	{
		$anInstance = BITAMReportExt::NewInstance($aRepository, $aSurveyID);

		$anInstance->ReportID = $aRS->fields["factkey"];
		$anInstance->FolioID = (int) $aRS->fields["folioid"];
		$anInstance->UserID = (int) $aRS->fields["userid"];
		$anInstance->EmailID = (int) $aRS->fields["emailkey"];
		$anInstance->EmailDesc = $aRS->fields["emaildesc"];
		$anInstance->FactKeyDimVal = (int)$aRS->fields["factkeydimval"];
		
		if($anInstance->UserID!=-1)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance->UserName = BITAMeFormsUser::GetUserName($aRepository, $anInstance->UserID);
		}
		else 
		{
			$anInstance->UserName = "NA";
		}
		
		$strHour = $aRS->fields["starttime"];
		$anInstance->DateID = substr($aRS->fields["dateid"], 0, 10)." ".substr($strHour, 0, 8);
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		if (!$bGetValues) {
			return $anInstance;
		}
		//@JAPR
		
		//Obtenemos el arreglo $this->ArrayDynSectionValues si es que existe seccion dinamica
		if($anInstance->HasDynamicSection==true)
		{
			$anInstance->getDynamicSectionValues();
		}

		//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection)
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
			$anInstance->getMasterDetSectionValues();
		}
		//@JAPR
		for ($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
			//que tengan seccion dinamica
			if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
			{
				$fieldName =$anInstance->QuestionFields[$i];
				$strValue = "";
				//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayDynSectionValues
				//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
				$arrValues = array();		//Indexado por la descripción del elemento, si se repite se sobreescribirá
				//Obtenemos el valor de esta pregunta a partir del arreglo $this->ArrayDynSectionValues
				//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
				//porque todavia no se sabe exactamente como se va a comportar
				if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
				{
					//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
					if ($anInstance->Survey->UseStdSectionSingleRec) {
						//En este caso se sabe anticipadamente que el array contiene un campo especial para dinámicas, así que exclusivamente
						//obtenemos los valores del registro único (se generan mas elementos porque en caso de haber multiple-choice, cada elemento
						//representa a una de las opciones, si no las hubiera entonces hay justo la cantidad de elementos == registros de la sección)
						foreach ($anInstance->ArrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							//@JAPR 2013-08-15: Corregido un bug, si no estaba asignada la respuesta generaba un error de script que impedía la
							//edición
							if ($anInstance->Questions[$i]->FormatMask != '' && is_numeric(@$element[$idxName])) {
								$strFieldValue = formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
							}
							else {
								$strFieldValue = (string) @$element[$idxName];
							}
							
							$strValue.=$element["desc"]."=".$strFieldValue;
						}
					}
					else {
						//En este caso el ciclo recorre todos los posibles registros, los cuales vienen mezclados entre maestro-detalle y dinámicas,
						//y no teníamos forma de identificar a cual sección pertenecía el registro
						foreach ($anInstance->ArrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
							if (!(trim($element[$idxName]) === '' || (((int) $element[$idxName])==0 && is_numeric($element[$idxName]))))
							{
								@$arrValues[$element["parentdesc"]] = $element["parentdesc"]."=".$element[$idxName];
							}
							//@$arrValues[$element["desc"]] = $element[$idxName];
							$strValue.=$element["desc"]."=".$element[$idxName];
							//@JAPR 2012-06-07: Las preguntas que no son multiple choice y que no vienen de catálogo (no están soportadas las single
							//choice de catálogo dentro de las dinámicas, así que entran en esta categoría) realmente repiten su valor en todos los
							//registros de la misma subsección dinámica, por lo que no tiene caso que se repita varias veces en el valor mostrado,
							//sin embargo como puede haber secciones Maestro - Detalle y esas se graban primero, el primer valor siempre vendría
							//repetido "n" veces, así que salimos hasta que encontramos por lo menos 2 valores o que ya en definitiva terminó de
							//recorrer todos los registros
							
							if ($anInstance->Questions[$i]->CatalogID <= 0)
							{
								if (count($arrValues) > 1)
								{
									$strValKey ='';
									foreach ($arrValues as $strValKeyTemp => $aValue)
									{
										$strValKey = $strValKeyTemp;
									}
									
									//unset($arrValues[$strValKey]);
									//NO funcionará, porque si hay varias subsecciones dinámicas, esto sólo traería el valor de la primera, así que
									//mejor se dejará comentado esta validación hasta pensar mejor como resolverlo
									//break;
								}
							}
							
							//@JAPR
						}
					}
					//@JAPR
				}
				else 
				{
					//Si es entrada de tipo Checkbox, el valor seran
					//todo los valores seleccionados concatenados con ;
					if($anInstance->Questions[$i]->MCInputType==0)
					{
						foreach ($anInstance->ArrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$valueInteger = (int)$element[$idxName];
							
							if($valueInteger!=0)
							{
								if($strValue!="")
								{
									$strValue.=";";
								}
								
								//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
								@$arrValues[$element["desc"]] = $element["desc"];
								$strValue.=$element["desc"];
							}
						}
					}
					else 
					{
						foreach ($anInstance->ArrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
							@$arrValues[$element["desc"]] = $element["desc"]."=".$element[$idxName];
							$strValue.=$element["desc"]."=".$element[$idxName];
						}
					}
				}
				
				/*
				//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
				//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
				if (!$anInstance->Survey->UseStdSectionSingleRec) {
					//En caso de usar un registro para preguntas estándar, ya no aplica la validación pues ya no existirían duplicados
				*/
					$strValue = @implode(';',$arrValues);
				//}
				//@JAPR
				$anInstance->$fieldName = $strValue;
			}
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//Preguntas de categoría de dimensión
			else if($anInstance->Questions[$i]->QTypeID==qtpMulti && $anInstance->Questions[$i]->QDisplayMode==dspVertical && 
					$anInstance->Questions[$i]->MCInputType==1 && $anInstance->Questions[$i]->IsMultiDimension==0 && 
					$anInstance->Questions[$i]->UseCategoryDimChoice!=0) {
				$fieldName =$anInstance->QuestionFields[$i];
				$fieldType =$anInstance->FieldTypes[$i];
				//@JAPR 2012-11-20: Validado el caso de categoría de dimensión junto a otras secciones de multiples registros
				if ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection) {
					//Si hay secciones maestro-detalle o dinámicas, a la fecha de hoy no se podían combinar con categoría de dimensión, así que
					//esta pregunta internamente se grababa como una multiple-choice IsMultuDimension==0 MCInputType==1 aunque quedara asociada
					//a una categoría de dimensión
					//@JAPR 2013-01-16: Removido un trim que afectaba el match al editar capturas
					$anInstance->$fieldName = $aRS->fields[strtolower($fieldName)];
					//@JAPR
				}
				else {
					//En el caso de las preguntas con dimension categoria vamos a obtener la informacion desde una funcion q realice 
					//en una cadena con enters las respuestas q fueron capturadas
					$LN = chr(13).chr(10);
					$strCatDimValAnswers = BITAMReportExtCollection::getStrCatDimValAnswers($aRepository, $anInstance->Survey, $anInstance->Questions[$i], $anInstance->FactKeyDimVal, $LN, false);
					$anInstance->$fieldName = $strCatDimValAnswers;
				}
				//@JAPR
				
				if(trim($anInstance->$fieldName)=="")
				{
					//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
					//En este caso se dejará vacio para que no afecte a la edición
					//$anInstance->$fieldName = "NA";
				}
			}
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//******************************************************************************************************************************
			//Preguntas de Maestro-detalle
			else if (isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
			{
				//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
				//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
				$fieldName =$anInstance->QuestionFields[$i];
				$strValue = "";
				//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayMasterDetSectionValues
				//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
				//porque todavia no se sabe exactamente como se va a comportar
				$intIndex = 1;
				//@JAPR 2013-07-02: Validado que si existe la colección antes de usarla
				if (isset($anInstance->ArrayMasterDetSectionValues[$anInstance->Questions[$i]->SectionID])) {
					foreach ($anInstance->ArrayMasterDetSectionValues[$anInstance->Questions[$i]->SectionID] as $element)
					{
						$idxName = $anInstance->QuestionFields[$i];
						//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones dinámicas
						$strTempVal = $element[$idxName];
						if ($anInstance->HasDynamicSection)
						{
							//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
							if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
							{
								//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
								$strTempVal = false;
							}
						}
						
						if ($strTempVal !== false)
						{
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							$strValue .= "<".$intIndex."=".$strTempVal.">";
							//$strValue .= "<".$element[$idxName].">";
						}
						$intIndex++;
						//@JAPR
					}
				}
				//@JAPR
				
				$anInstance->$fieldName = $strValue;
			}
			else 
			{
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//Preguntas simple-choice de catálogo
				if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
				{
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldNameKey =$anInstance->QuestionFields[$i].'KEY';
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($anInstance->Survey->DissociateCatDimens) {
						$memberParentID = $objQuestion->CatIndDimID;
						$attribField = "DSC_".$memberParentID;
						$attribFieldKey = "DSC_".$memberParentID."KEY";
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
							$anInstance->$fieldNameKey = (int) @$aRS->fields[strtolower($attribFieldKey)];
							
							//Obtiene la jerarquía del catálogo pero utilizando los datos grabados en la tabla paralela
							$strCatDimValAnswers = trim(@$aRS->fields[strtolower($objQuestion->SurveyCatField)]);
							$arrayCatValues = array();
							if ($strCatDimValAnswers != '') {
								$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
								$arrayCatClaDescrip = array();
								$arrayTemp = array();
								$arrayInfo = array();
								foreach($arrayCatPairValues as $element)
								{
									$arrayInfo = explode('_SVSep_', $element);
									//Obtener el cla_descrip y almacenar (key_1178)
									$arrayTemp = explode('_', $arrayInfo[0]);
									$arrayCatClaDescrip[] = $arrayTemp[1];
									//Obtener el valor del catalogo
									$arrayCatValues[] = $arrayInfo[1];
								}
							}
							//@JAPR 2013-02-11: Agregada la edición de datos históricos
							$anInstance->{$objQuestion->SurveyCatField} = $strCatDimValAnswers;
							
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$fieldNameHierarchy = $fieldName."_Hierarchy";
							$anInstance->$fieldNameHierarchy = implode(', ', $arrayCatValues);
						}
						
						if(trim($anInstance->$fieldName)=="")
						{
							//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
							//En este caso se dejará vacio para que no afecte a la edición
							//$anInstance->$fieldName = "NA";
							$anInstance->$fieldNameKey = 0;
						}
					}
					else {
						$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID, $anInstance->Questions[$i]->CatMemberID);
						$attribField = "DSC_".$memberParentID;
						$attribFieldKey = "DSC_".$memberParentID."KEY";
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							//@JAPR 2013-01-16: Removido un trim que afectaba el match al editar capturas
							$anInstance->$fieldName = $aRS->fields[strtolower($attribField)];
							//@JAPR
							$anInstance->$fieldNameKey = (int) @$aRS->fields[strtolower($attribFieldKey)];
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
							if(array_key_exists(strtolower("RIDIM_".$catParentID."KEY"), $aRS->fields))
							{
								$fieldNameHierarchy = $fieldName."_Hierarchy";
								$anInstance->$fieldNameHierarchy = BITAMReportExt::getCatHierarchyByMemberValue($aRepository, $anInstance->Questions[$i]->CatalogID, $anInstance->Questions[$i]->CatMemberID, $attribField, $aRS->fields[strtolower("RIDIM_".$catParentID."KEY")], $aRS->fields[strtolower($attribField)]);
							}
							if(trim($anInstance->$fieldName)=="")
							{
								//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
								//En este caso se dejará vacio para que no afecte a la edición
								//$anInstance->$fieldName = "NA";
								$anInstance->$fieldNameKey = 0;
							}
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
								//En este caso se dejará vacio para que no afecte a la edición
								//$anInstance->$fieldName = "NA";
								$anInstance->$fieldNameKey = 0;
							}
						}
					}
					//@JAPR
				}
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//Preguntas simple-choice de matriz
				else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && 
						$anInstance->Questions[$i]->QDisplayMode==dspMatrix) {
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];

					//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
					//en una cadena con enters las respuestas q fueron capturadas
					$strAnswersSCMatrix = $anInstance->getStrAnswersSCMatrix($anInstance->Questions[$i]);
					
					$anInstance->$fieldName = $strAnswersSCMatrix;
					
					if(trim($anInstance->$fieldName)=="")
					{
						//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
						//En este caso se dejará vacio para que no afecte a la edición
						//$anInstance->$fieldName = "NA";
					}
				}
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//******************************************************************************************************************************
				//Cualquier otro tipo de pregunta
				else {
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];
	
					if(array_key_exists(strtolower($fieldName),$aRS->fields))
					{
						if($fieldType == 1)
						{
							//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
							//En este caso se dejará vacio para que no afecte a la edición
							if (trim(@$aRS->fields[strtolower($fieldName)]) != '') {
								$anInstance->$fieldName = (float) $aRS->fields[strtolower($fieldName)];
							}
							else {
								$anInstance->$fieldName = '';
							}
						}
						else
						{
							if($fieldType == qtpMulti)
							{
								//@JAPR 2013-01-16: Removido un trim que afectaba el match al editar capturas
								$anInstance->$fieldName = $aRS->fields[strtolower($fieldName)];
								//@JAPR
								
								if(trim($anInstance->$fieldName)=="")
								{
									//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
									//En este caso se dejará vacio para que no afecte a la edición
									//$anInstance->$fieldName = "NA";
								}
							}
							else 
							{
								//@JAPR 2013-01-16: Removido un trim que afectaba el match al editar capturas
								$anInstance->$fieldName = $aRS->fields[strtolower($fieldName)];
								//@JAPR
	
								if(trim($anInstance->$fieldName)=="")
								{
									//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
									//En este caso se dejará vacio para que no afecte a la edición
									//$anInstance->$fieldName = "NA";
								}
							}
						}
					}
					else
					{
						if($fieldType == 1)
						{
							//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
							//En este caso se dejará vacio para que no afecte a la edición
							$anInstance->$fieldName = '';	// 0;
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
								//En este caso se dejará vacio para que no afecte a la edición
								$anInstance->$fieldName = '';	//"NA";
							}
						}
					}
				}
			}
		}

		return $anInstance;
	}
	
	static function getCatHierarchyByMemberValue($aRepository, $catalogID, $catMemberID, $attribField, $id, $value)
	{
		$hierarchyValue="";
		
		$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID=".$catalogID." AND MemberOrder<(SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID = ".$catalogID." AND MemberID = ".$catMemberID.") ORDER BY MemberOrder";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$fieldNames = array();
		
		while(!$aRS->EOF)
		{
			$fieldNames[] = "DSC_".$aRS->fields["parentid"];
			
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-09-21: Corrección de RVega al reporte
 		if(count($fieldNames)==0)
		{
			$hierarchyValue = trim($value);
			return $hierarchyValue;	
		}
		//@JAPR		
		
		$sql = "SELECT TableName, ParentID FROM SI_SV_Catalog WHERE CatalogID =".$catalogID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$tableName = $aRS->fields["tablename"];
			$catParentID = $aRS->fields["parentid"];
		}
		
		$sql = "SELECT ".implode(",", $fieldNames)." FROM ".$tableName." WHERE "."RIDIM_".$catParentID."KEY"." = ".$id;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			foreach ($fieldNames as $fieldName)
			{
				if($hierarchyValue!="")
				{
					$hierarchyValue.=", ";
				}
				$hierarchyValue.= $aRS->fields[strtolower($fieldName)];
			}
		}
		
		if($hierarchyValue!="")
		{
			$hierarchyValue.=", ";			
		}
		
		$hierarchyValue.= $value;
		
		return $hierarchyValue;
	}
	
	function getStrAnswersSCMatrix($questionInstance)
	{
		$strAnswersSCMatrix = "";
		$LN = chr(13).chr(10);
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT A.DisplayText, b.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$this->ReportID." AND B.FactKeyDimVal = ".$this->FactKeyDimVal." 
				ORDER BY A.SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strDisplayText." = ".$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	function getDynamicSectionValues()
	{
		//Obtenemos coleccion de las preguntas de la seccion dinamica
		$dynQuestionCollection = $this->DynQuestionCollection;
		//Obtenemos la instancia de la seccion dinamica
		$dynSectionInstance = $this->DynamicSection;
		//Obtenemos la instancia de Survey		
		$aSurvey = $this->Survey;
		//Obtenemos la instancia del catalogo
		$catalogInstance = $this->Catalog;
		//Obtenemos la instancia del atributo del catálogo que genera las secciones dinámicas
		$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $this->DynamicSection->CatMemberID);
		
		//Obtener la primera pregunta de seleccion simple de catalogo para obtener los valores del hijo del atributo 
		//de la seccion dinamica
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
				AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$dynSectionInstance->CatalogID." 
				ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		elseif ($aRS->EOF) {
			//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
			die("(".__METHOD__.") ".translate("There is an error in the form, a catalog simple choice question is needed to generate the dynamic section"));
		}
		
		//El key subrrogado del catálogo es irrelevante cuando se usa DissociateCatDimens, sin embargo se dejará sólo como referencia para no cambiar
		//la manera en como se generaba el Array y por compatibilidad con encuestas que aun no han sido migradas a esta característica
		$questionID = (int)$aRS->fields["questionid"];
		$dimCatFieldID = "dim_".$questionID;
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		if ($aSurvey->DissociateCatDimens) {
			//En este caso son las dimensiones usadas como atributo en la sección dinámica y la del siguiente atributo (en caso de tener preguntas
			//multiple choice, de lo contrario, se repite la propia dimensión de la dinámica) las que hacen join, ya que la dimensión del catálogo
			//se separó de la tabla paralela aunque internamente sigue una referencia a sus keys subrrogados
			if ($dynSectionInstance->ChildCatMemberID > 0) {
				$childCatMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $dynSectionInstance->ChildCatMemberID);
			}
			else {
				$childCatMemberInstance = $catMemberInstance;
			}
			
			//Atributo de la sección dinámica
			$catParentID = $catMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			$fromCat.=", ".$catTable;
			$joinField = "RIDIM_".$catParentID."KEY";
			$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
			$dimCatParentDsc = $catTable.".DSC_".$catMemberInstance->IndDimID;
			
			//Atributo de las preguntas múltiple choice (si existen, si no entonces es el mismo de arriba)
			$catParentID = $childCatMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			if(!isset($arrayTables[$catTable])) {
				$fromCat.=", ".$catTable;
				$joinField = "RIDIM_".$catParentID."KEY";
				$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			}
			
			$dimCatFieldDsc = $catTable.".DSC_".$childCatMemberInstance->IndDimID;
		}
		else {
			$dimCatFieldDsc = "B.DSC_".$this->ChildDynamicAttribID;
			$dimCatFieldJoin = "RIDIM_".$catalogInstance->ParentID."KEY";
			$dimCatTable = "RIDIM_".$catalogInstance->ParentID;
			$dimCatParentDsc = "B.DSC_".$catMemberInstance->ClaDescrip;
		}
		
		$sql = "SELECT A.FactKey, A.EntrySectionID, A.".$dimCatFieldID." AS id, ".$dimCatFieldDsc." AS description, ".$dimCatParentDsc." AS parentdesc";
		//@JAPR 2012-11-21: Corregido un bug, las preguntas tipo foto y firma realmente no deben tener campo en la paralela, y si se genera
		//el query sin preguntas no se debe ejecutar para que no falle
		$blnHasFields = false;
		foreach($dynQuestionCollection as $aQuestion)
		{
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
					break;
				
				//@JAPR 2013-12-10: Corregido un bug, las preguntas simple choice de catálogo no estaban cargandose correctamente al editar cuando
				//se encontraban dentro de secciones dinámicas
				case qtpSingle:
					//En caso de ser una pregunta Simple Choice de catálogo y usar catálogos desasociados, se agrega el campo cat_
					if ($aSurvey->DissociateCatDimens && $aQuestion->UseCatalog && $aQuestion->CatalogID > 0 && $aQuestion->CatIndDimID) {
						$sql .= ", {$aQuestion->SurveyCatField}";
					}
					
					//Debe cotinuar para que inserte el campo dim_
				//@JAPR
					
				default:
					$sql .= ", dim_".$aQuestion->QuestionID;
					$blnHasFields = true;
					break;
			}
		}
		
		if (!$blnHasFields) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\ngetDynamicSectionValues ({$dynSectionInstance->SectionID}): There're no fields in this section");
			}
			return;
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseDynamicPageField) {
			$sql .= ", A.DynamicPageDSC";
		}
		//@JAPR
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if ($aSurvey->DissociateCatDimens) {
			$sql .= ", A.DynamicOptionDSC, A.DynamicValue";
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$aSurvey->FactTable." B ".$fromCat;
			$sql .= " WHERE A.FactKey = B.FactKey ".$joinCat." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		else {
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$dimCatTable." B ";
			$sql .= " WHERE A.".$dimCatFieldID." = B.".$dimCatFieldJoin." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseStdSectionSingleRec) {
			$sql .= " AND A.EntrySectionID = ".$dynSectionInstance->SectionID;
		}
		//@JAPR
		$sql .= " ORDER BY A.FactKey ";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\ngetDynamicSectionValues ({$dynSectionInstance->SectionID}): $sql");
		}
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
		if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$blnUseSingleRecord = $this->Survey->UseStdSectionSingleRec;
		$aSectionID = $dynSectionInstance->SectionID;
		$count = 0;
		$this->ArrayDynSectionValues[$aSectionID] = array();
		while(!$aRS->EOF)
		{
			$this->ArrayDynSectionValues[$aSectionID][$count] = array();
			$this->ArrayDynSectionValues[$aSectionID][$count]["id"] = (int)$aRS->fields["id"];
			$this->ArrayDynSectionValues[$aSectionID][$count]["desc"] = $aRS->fields["description"];
			$this->ArrayDynSectionValues[$aSectionID][$count]["parentdesc"] = $aRS->fields["parentdesc"];
			//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
			$intEntrySectionID = @$aRS->fields["entrysectionid"];
			$blnIsEmpty = ($intEntrySectionID != $aSectionID);
			$blnCheckRow = is_null($intEntrySectionID) || $intEntrySectionID == -1;
			//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
			$aDynamicPageDSC = (string) @$aRS->fields["dynamicpagedsc"];
			foreach($dynQuestionCollection as $aQuestion)
			{
				$fieldName = "dim_".$aQuestion->QuestionID;
				$aFieldValue = @$aRS->fields[$fieldName];
				//Si es un registro de cuando aun no se grababan los EntryIDs, se debe verificar que no esté vacio pues de lo contrario
				//es muy probable que no sea un registro de esta sección
				if ($blnCheckRow) {
					if (!is_null($aFieldValue) && trim($aFieldValue) <> '') {
						$blnIsEmpty = false;
					}
				}
				
				if($aQuestion->QTypeID != qtpMulti || $aQuestion->MCInputType != mpcNumeric)
				{
					//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único, por el contrario si
					//usa un registro único entonces sólo se procesa se trata precisamente de él
					if (!$blnUseSingleRecord || $aDynamicPageDSC != '' || $aQuestion->QTypeID == qtpMulti) {
						//Si no se usa el registro único, cada registro debe venir con las descripciones de preguntas no repetibles
						$this->ArrayDynSectionValues[$aSectionID][$count][$fieldName] = $aFieldValue;
						
						//@JAPR 2013-12-10: Corregido un bug, las preguntas simple choice de catálogo no estaban cargandose correctamente al editar cuando
						//se encontraban dentro de secciones dinámicas
						if ($aQuestion->QTypeID == qtpSingle && $aSurvey->DissociateCatDimens && $aQuestion->UseCatalog && $aQuestion->CatalogID > 0 && $aQuestion->CatIndDimID) {
							//Si es una simple choice de catálogo que se encuentra dentro de la dinámica, agrega además el valor del campo cat_
							$this->ArrayDynSectionValues[$aSectionID][$count][$aQuestion->SurveyCatField] = (string) @$aRS->fields[strtolower($aQuestion->SurveyCatField)];
						}
					}
					//@JAPR
				}
				else 
				{
					//Si es de tipo Multiple Choice Numerica le realizamos un float
					$this->ArrayDynSectionValues[$aSectionID][$count][$fieldName] = (float)$aFieldValue;
				}
			}
			
			//Sólo agrega el registro si no estaba vacio o si pertenece a la sección Dinámica analizada 
			if (!$blnIsEmpty) {
				$this->ArrayDynSectionValues[$aSectionID][$count]["FactKey"] = (int) @$aRS->fields["factkey"];
				//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
				//Si se usa el registro único, entonces agrega el nuevo campo para indicar que si este es el registro a usar por cada página pues
				//sería el que contiene a las preguntas no repetibles
				if ($this->Survey->UseDynamicPageField) {
					$this->ArrayDynSectionValues[$aSectionID][$count]["DynamicPageDSC"] = $aDynamicPageDSC;
				}
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					$aDynamicValue = (string) @$aRS->fields["dynamicvalue"];
					//@JAPR 2013-04-24: Corregido un bug, estaba agregando este elemento al nivel raiz de los datos en lugar de a nivel de la
					//sección por lo que en lugar de ser un atributo mas de cada registro, era un registro mas y eso generaba que la edición no
					//funcionara correctamente
					$this->ArrayDynSectionValues[$aSectionID][$count]["DynamicValue"] = $aDynamicValue;
				}
				//@JAPR
				
				$count++;
			}
			else {
				unset ($this->ArrayDynSectionValues[$aSectionID][$count]);
			}
			$aRS->MoveNext();
		}
	}

	//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
	//Obtiene los valores de todas las secciones Maestro - Detalle que incluye la encuesta y llena los campos correspondientes como un Array
	//indexado por cada registro de los conjuntos de respuestas
	function getMasterDetSectionValues()
	{
		foreach ($this->MasterDetSectionIDs as $aSectionID)
		{
			//Obtenemos coleccion de las preguntas de la seccion Maestro - Detalle
			$aQuestionCollection = @$this->MasterDetQuestionCollection[$aSectionID];
			if (is_null($aQuestionCollection))
			{
				continue;
			}
			
			//Obtenemos la instancia de Survey		
			$aSurvey = $this->Survey;
			//Obtenemos la instancia del catalogo
			$catalogInstance = $this->Catalog;
			
			$sql = "SELECT A.FactKey, A.EntrySectionID";
			$blnFirst = false;
			//@JAPR 2012-11-21: Corregido un bug, las preguntas tipo foto y firma realmente no deben tener campo en la paralela, y si se genera
			//el query sin preguntas no se debe ejecutar para que no falle
			$blnHasFields = false;
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$fromCat = "";
			$joinCat = "";
			$arrayTables = array();
			//@JAPR
			foreach($aQuestionCollection as $aQuestion)
			{
				switch ($aQuestion->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
						break;
					
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					case qtpSingle:
						//Si la pregunta es simple choice de catálogo y usa catálogos con dimensiones independientes, entonces se puede pedir
						//directamente el valor a la dimensión en lugar de mostrar el key subrrogado que anteriormente mostraba
						if ($aSurvey->DissociateCatDimens && $aQuestion->UseCatalog && $aQuestion->CatalogID > 0 && $aQuestion->CatIndDimID) {
							$catParentID = $aQuestion->CatIndDimID;
							$memberParentID = $catParentID;
							$catTable = "RIDIM_".$catParentID;
							$attribField = "DSC_".$memberParentID;
							$sql.=(($blnFirst)?'':',').$catTable.".".$attribField." AS dim_".$aQuestion->QuestionID;
							$sql.=", A.".$aQuestion->SurveyCatField;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$blnFirst = false;
							//@JAPR 2013-08-13: Corregido un bug, si la sección sólo hubiera contenido este tipo de preguntas, no estaba registrando
							//que había campos así que no ejecutaba el query
							$blnHasFields = true;
							break;
						}
					//@JAPR
					
					default:
						$sql .= (($blnFirst)?'':',')." dim_".$aQuestion->QuestionID;
						$blnFirst = false;
						$blnHasFields = true;
						break;
				}
			}
			
			$this->ArrayMasterDetSectionValues[$aSectionID] = array();
			if (!$blnHasFields) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\ngetMasterDetSectionValues ({$aSectionID}): There're no fields in this section");
				}
				return;
			}
			
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$aSurvey->FactTable." B ".$fromCat;
			$sql .= " WHERE A.FactKey = B.FactKey ".$joinCat." AND A.FactKeyDimVal = ".$this->FactKeyDimVal." AND 
				(A.EntrySectionID = ".$aSectionID." OR A.EntrySectionID = -1 OR A.EntrySectionID IS NULL)";
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			if ($this->Survey->UseStdSectionSingleRec) {
				$sql .= " AND A.EntrySectionID = ".$aSectionID;
			}
			//@JAPR
			$sql .= " ORDER BY A.FactKey ";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\ngetMasterDetSectionValues ($aSectionID): $sql");
			}
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			//@JAPR 2012-11-20: Un EOF no debería marcar un error sino continuar sin realizar acción alguna
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$count = 0;
			while(!$aRS->EOF)
			{
				$this->ArrayMasterDetSectionValues[$aSectionID][$count] = array();
				//$this->ArrayMasterDetSectionValues[$aSectionID][$count]["id"] = (int)$aRS->fields["id"];
				//$this->ArrayMasterDetSectionValues[$aSectionID][$count]["desc"] = $aRS->fields["description"];
				//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
				$intEntrySectionID = @$aRS->fields["entrysectionid"];
				$blnIsEmpty = ($intEntrySectionID != $aSectionID);
				$blnCheckRow = is_null($intEntrySectionID) || $intEntrySectionID == -1;
				foreach($aQuestionCollection as $aQuestion)
				{
					$fieldName = "dim_".$aQuestion->QuestionID;
					$aFieldValue = @$aRS->fields[$fieldName];
					//Si es un registro de cuando aun no se grababan los EntryIDs, se debe verificar que no esté vacio pues de lo contrario
					//es muy probable que no sea un registro de esta sección
					if ($blnCheckRow) {
						if (!is_null($aFieldValue) && trim($aFieldValue) <> '') {
							//@JAPR 2012-11-26: Corregido un bug, no estaba considerando en estos casos a las preguntas numéricas, las cuales
							//por su naturaleza tienen que grabar por fuerza un 0, el cual se considera como NO capturado
							switch ($aQuestion->QTypeID) {
								//@JAPR 2012-11-28: Validado el caso de preguntas de catálogo en registros realmente vacios dentro de
								//maestro-detalle, en este caso si viene el valor de NA no se debe considerar contestada la pregunta
								case qtpSingle:
									if ($aQuestion->UseCatalog && $aQuestion->CatalogID > 0) {
										if (((int) $aFieldValue) > 1) {
											$blnIsEmpty = false;
										}
										
										//@JAPR 2013-02-11: Agregada la edición de datos históricos
										//@JAPR 2013-03-21: Corregido un bug, estaba usando una referencia al objeto $objQuestion
										$strCatDimValAnswers = trim(@$aRS->fields[strtolower($aQuestion->SurveyCatField)]);
										$this->ArrayMasterDetSectionValues[$aSectionID][$count][$aQuestion->SurveyCatField] = $strCatDimValAnswers;
										//@JAPR
									}
									break;
									
								case qtpOpenNumeric:
								case qtpCalc:
									//En estos casos si se resuelve a 0, entonces se considera vacio
									if (((int) $aFieldValue) != 0) {
										$blnIsEmpty = false;
									}
									break;
								
								default:
									$blnIsEmpty = false;
									break;
							}
						}
					}
					//@JAPR 2013-05-17: Corregido un bug, si ya se usaba el nuevo formato, no estaba agregando la columna "cat_" al array de datos
					else {
						switch ($aQuestion->QTypeID) {
							//@JAPR 2012-11-28: Validado el caso de preguntas de catálogo en registros realmente vacios dentro de
							//maestro-detalle, en este caso si viene el valor de NA no se debe considerar contestada la pregunta
							case qtpSingle:
								if ($aQuestion->UseCatalog && $aQuestion->CatalogID > 0) {
									$strCatDimValAnswers = trim(@$aRS->fields[strtolower($aQuestion->SurveyCatField)]);
									$this->ArrayMasterDetSectionValues[$aSectionID][$count][$aQuestion->SurveyCatField] = $strCatDimValAnswers;
								}
						}
					}
					//@JAPR
					//$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldName] = "<".$aRS->fields[$fieldName].">";
					$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldName] = $aFieldValue;
				}
				
				//Sólo agrega el registro si no estaba vacio o si pertenece a la sección Maestro-Detalle analizada 
				if (!$blnIsEmpty) {
					$this->ArrayMasterDetSectionValues[$aSectionID][$count]["FactKey"] = (int) @$aRS->fields["factkey"];
					$count++;
				}
				else {
					unset ($this->ArrayMasterDetSectionValues[$aSectionID][$count]);
				}
				$aRS->MoveNext();
			}
		}
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;
		}

		global $svStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$svStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $svEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$svEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $svUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$svUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $svIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$svIpp = $aHTTPRequest->GET["ipp"];
		}

		if (array_key_exists("ReportID", $aHTTPRequest->POST))
		{
			$aReportID = $aHTTPRequest->POST["ReportID"];

			if (is_array($aReportID))
			{
				if (array_key_exists("SurveyID", $aHTTPRequest->POST))
				{
					$aSurveyID = (int) $aHTTPRequest->POST["SurveyID"];
				}
				
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$svStartDate = $aHTTPRequest->POST["startDate"];
				}
		
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$svEndDate = $aHTTPRequest->POST["endDate"];
				}
		
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$svUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$svIpp = $aHTTPRequest->POST["ipp"];
				}

				$aCollection = BITAMReportExtCollection::NewInstanceForRemove($aRepository, $aSurveyID, $aReportID, $svStartDate, $svEndDate, $svUserID, $svIpp);

				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMReportExt::NewInstanceWithID($aRepository, (int) $aReportID, $aSurveyID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMReportExt::NewInstance($aRepository, $aSurveyID);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMReportVariable::NewInstance($aRepository, $aSurveyID);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("ReportID", $aHTTPRequest->GET))
		{
			$aReportID = $aHTTPRequest->GET["ReportID"];
			
			$callExportPDF = false;
			if(isset($aHTTPRequest->GET["ReportAction"]))
			{
				$actionValue = $aHTTPRequest->GET["ReportAction"];
	
				if($actionValue=="ExportPDF")
				{
					$callExportPDF = true;
				}
			}

			$anInstance = BITAMReportExt::NewInstanceWithID($aRepository,(int)$aReportID, $aSurveyID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMReportExt::NewInstance($aRepository, $aSurveyID);
			}
			else 
			{
				if($callExportPDF)
				{
					$anInstance->exportToPDF();
				}
			}
		}
		else
		{
			$anInstance = BITAMReportExt::NewInstance($aRepository, $aSurveyID);
		}

		return $anInstance;
	}
	
	function exportToPDF()
	{
		set_time_limit(0);
		require_once("exportsurvey.inc.php");
		$docPDF = BITAMExportSurvey::NewInstanceWithID($this->Repository, $this->SurveyID, $this->ReportID);
		$docPDF->exportSurveyToPDF(false);
	}

	function updateFromArray($anArray)
	{
	}

	function save()
	{
	}

	//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
	//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
	//editando o agregando otros
	//El parámetro $anArrayFactKeys controla que FactKeys se desean eliminar de esta captura, si no se especifica entonces elimina toda la captura
	//completa, aunque en si el registro de la captura depende de si se manda el $bDeleteFactKeyDimVal == true
	function remove($bDeleteFactKeyDimVal = true, $anArrayFactKeys = null)
	{
		//A partir del Encuesta ID se van a eliminar los registros
		$factTable = "RIFACT_".$this->Survey->ModelID;
		$factKeyDimValTable = "RIDIM_".$this->Survey->FactKeyDimID;
		$factKeyDimValField = "RIDIM_".$this->Survey->FactKeyDimID."KEY";
		$blnUseFactKeysFilter = false;
		//@JAPR 2012-11-16: Agregado el borrado selectivo de registros de una captura
		$arrayFactKeys = array();
		if (is_null($anArrayFactKeys)) {
			$sql = "SELECT FactKey FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			//@JAPR 2012-11-20: Un EOF no debería marcar un error sino continuar sin realizar acción alguna
			if (!$aRS)	// || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$arrayFactKeys = array();
			
			while (!$aRS->EOF) 
			{
				$arrayFactKeys[] = (int)$aRS->fields["factkey"];
				
				$aRS->MoveNext();
			}
		}
		else {
			$arrayFactKeys = $anArrayFactKeys;
			$blnUseFactKeysFilter = true;
		}
		
		if (is_null($arrayFactKeys) || !is_array($arrayFactKeys)) {
			$arrayFactKeys = array();
		}
		//@JAPR
		
		//Si no hay elementos FactKey por eliminar entonces no se realiza 
		//el proceso de eliminado
		if(count($arrayFactKeys)<=0)
		{
			return;
		}
		
		$strFactKeys = implode(",", $arrayFactKeys);
		
		//Eliminamos las firmas
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta
		//La firma sólo se elimina si se está borrando la captura completa
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveysignature";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			
			while (!$aRS->EOF)
			{
				$pathImage = $aRS->fields["pathimage"];
				@unlink($RepositoryDir."/".$pathImage);
				
				$aRS->MoveNext();
			}
			
			$sql = "DELETE FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		//Eliminamos los comentarios
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND MainFactKey IN ('.$strFactKeys.') ';
		}
		$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las acciones por pregunta que ya no se ocupa por el momento
		$sql = "DELETE FROM SI_SV_SurveyQuestionActions WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las acciones por opcion de pregunta
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$this->SurveyID." AND SurveyDimVal = ".$this->FactKeyDimVal;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las fotos
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
		//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
		//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
		$arrayImgDimIDs = array();
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveyimages";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			
			while (!$aRS->EOF)
			{
				$pathImage = $aRS->fields["pathimage"];
				@unlink($RepositoryDir."/".$pathImage);
				
				$aRS->MoveNext();
			}
			
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$this->FactKeyDimVal." )".$strFactKeysFilter;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "SELECT ImgDimID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND HasReqPhoto > 0 AND ImgDimID IS NOT NULL ORDER BY ImgDimID";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strSelectImgDim = "";
			
			while(!$aRS->EOF)
			{
				$imgdimid = (int)$aRS->fields["imgdimid"];
				//@JAPR 2012-10-22: Corregido un bug, si no está asignada esta dimensión no se debe intentar usar
				if ($imgdimid > 0) {
					$arrayImgDimIDs[] = $imgdimid;
					
					if($strSelectImgDim!="")
					{
						$strSelectImgDim.=", ";
					}
					
					$strSelectImgDim.="RIDIM_".$imgdimid."KEY";
				}
				//@JAPR
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND FactKey IN ('.$strFactKeys.') ';
		}
		if(count($arrayImgDimIDs)>0)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "SELECT ".$strSelectImgDim." FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
	
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				foreach ($arrayImgDimIDs as $imgdimid)
				{
					$tmpTable = "RIDIM_".$imgdimid;
					$tmpField = "RIDIM_".$imgdimid."KEY";
					$tmpValue = (int)$aRS->fields[strtolower($tmpField)];
					
					//Eliminamos cualquier valor excepto el de no Aplica
					//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
					//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
					//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
					if($tmpValue!=1 && $bDeleteFactKeyDimVal)
					{
						$sql = "DELETE FROM ".$tmpTable." WHERE ".$tmpField." = ".$tmpValue;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tmpTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}

				$aRS->MoveNext();
			}
		}

		//Eliminamos los documentos
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
		//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
		//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
		$arrayDocDimIDs = array();
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathDocument FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveydocuments";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			
			while (!$aRS->EOF)
			{
				$pathDocument = $aRS->fields["pathdocument"];
				@unlink($RepositoryDir."/".$pathDocument);
				
				$aRS->MoveNext();
			}
			
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "DELETE FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$this->FactKeyDimVal." )".$strFactKeysFilter;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "SELECT DocDimID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND HasReqDocument > 0 AND DocDimID IS NOT NULL ORDER BY DocDimID";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strSelectDocDim = "";
			
			while(!$aRS->EOF)
			{
				$docdimid = (int)$aRS->fields["docdimid"];
				//@JAPR 2012-10-22: Corregido un bug, si no está asignada esta dimensión no se debe intentar usar
				if ($docdimid > 0) {
					$arrayDocDimIDs[] = $docdimid;
					
					if($strSelectDocDim!="")
					{
						$strSelectDocDim.=", ";
					}
					
					$strSelectDocDim.="RIDIM_".$docdimid."KEY";
				}
				//@JAPR
				$aRS->MoveNext();
			}
		}
				
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND FactKey IN ('.$strFactKeys.') ';
		}
		if(count($arrayDocDimIDs)>0)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "SELECT ".$strSelectDocDim." FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
	
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				foreach ($arrayDocDimIDs as $docdimid)
				{
					$tmpTable = "RIDIM_".$docdimid;
					$tmpField = "RIDIM_".$docdimid."KEY";
					$tmpValue = (int)$aRS->fields[strtolower($tmpField)];
					
					//Eliminamos cualquier valor excepto el de no Aplica
					//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
					//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
					//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
					if($tmpValue!=1 && $bDeleteFactKeyDimVal)
					{
						$sql = "DELETE FROM ".$tmpTable." WHERE ".$tmpField." = ".$tmpValue;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tmpTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}

				$aRS->MoveNext();
			}
		}

		//Eliminamos registros de la tabla matrix
		if ($bDeleteFactKeyDimVal) {
			$tableMatrix = "SVSurveyMatrixData_".$this->Survey->ModelID;
			
			$sql = "DELETE FROM ".$tableMatrix." WHERE FactKeyDimVal = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableMatrix." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			//Eliminamos registros de la tabla de categorydimension
			$tableCatDimVal = "SVSurveyCatDimVal_".$this->Survey->ModelID;
			
			$sql = "DELETE FROM ".$tableCatDimVal." WHERE FactKeyDimVal = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDimVal." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		//Eliminamos registros de la tabla paralela
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$sql = "DELETE FROM ".$this->Survey->SurveyTable." WHERE FactKeyDimVal = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->Survey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos registros de la tabla de hechos
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$sql = "DELETE FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
		//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
		//editando o agregando otros
		//No se tiene hasta el momento una manera de identificar en la tabla global de encuestas cuales son los registros a eliminar
		$this->removeDataInGlobalSurveyModel($this->FactKeyDimVal, $arrayFactKeys, $bDeleteFactKeyDimVal);
                
		if ($bDeleteFactKeyDimVal) {
			//Eliminamos el registro de la dimension Encuesta ID
			$sql = "DELETE FROM ".$factKeyDimValTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factKeyDimValTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		//@JAPR
	}
        
	//Elimina del cubo global de capturas de encuestas, todos los registros especificados en $arrayFactKeys de la captura registrada en el key
	//$factKeyDimVal, sin embargo tiene un error, ya que asume que el $arrayFactKeys es la captura completa representada por $factKeyDimVal,
	//por lo que inmediatamente después borra en si el registro de la captura, con lo que dañaría a registros que aun existieran, por lo tanto
	//si se ha usado, ha sido sólo para eliminar reportes completos
	//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
	//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
	//editando o agregando otros
    function removeDataInGlobalSurveyModel($factKeyDimVal, $arrayFactKeys, $bDeleteFactKeyDimVal = true) {
        $strOriginalWD = getcwd();

        //Obtener el DateKey
        require_once("cubeClasses.php");
        require_once("dimension.inc.php");

		//@JAPR 2015-02-09: Agregado soporte para php 5.6
        //session_register("BITAM_UserID");
        //session_register("BITAM_UserName");
        //session_register("BITAM_RepositoryName");
        $_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
        $_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
        $_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

        require_once("../model_manager/model.inc.php");
        require_once("../model_manager/modeldata.inc.php");
        require_once("../model_manager/modeldimension.inc.php");
        require_once("../model_manager/indicatorkpi.inc.php");

        $anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->Survey->GblSurveyModelID);
        chdir($strOriginalWD);
        
        //Obtenemos el SurveyKey
        $aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveyDimID);
        $surveyKey = $this->getSurveyDimKey($aSurveyInstanceDim);
        
        //Obtenemos las dims SurveyGlobalDimID y SurveySingleRecordDimID
        $aSurveyGlobalInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveyGlobalDimID);
        $aSurveySingleRecordInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveySingleRecordDimID);
        
        //campos surveyglobalid y surveysinglerecord
        $surveyGlobalDimTable = $aSurveyGlobalInstanceDim->Dimension->TableName;
        $surveyGlobalDimField = $aSurveyGlobalInstanceDim->Dimension->TableName."KEY";
        $surveySingleRecordDimTable = $aSurveySingleRecordInstanceDim->Dimension->TableName;
        $surveySingleRecordDimField = $aSurveySingleRecordInstanceDim->Dimension->TableName."KEY";

        $factTable = $anInstanceModel->nom_tabla;
		$searchSurveyGlobalValue = $surveyKey."-".$factKeyDimVal;
		//@JAPR: Ejemplo: $searchSurveyGlobalValue == KeySubrrogadoDelSurveyID-FactKeyDimVal
		foreach ($arrayFactKeys as $factKey)
		{
			$searchSurveySingleRecordValue = $searchSurveyGlobalValue."-".$factKey;
			//@JAPR: Ejemplo: $searchSurveySingleRecordValue == KeySubrrogadoDelSurveyID-FactKeyDimVal-FactKey
	        //obtenemos el surveysinglerecordkey
	        $surveySingleRecordDesc = "DSC_".$this->Survey->GblSurveySingleRecordDimID;
	        $sql = "SELECT ".$surveySingleRecordDimField." FROM ".$surveySingleRecordDimTable." WHERE ".$surveySingleRecordDesc. " = ".$this->Repository->DataADOConnection->Quote($searchSurveySingleRecordValue);
	        $aRS = $this->Repository->DataADOConnection->Execute($sql);
	        if(!$aRS)
	        {
	            echo(translate("Error accessing")." ".$surveySingleRecordDimField." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
	        }
	        
	        if (!$aRS->EOF)
	        {
	            $foundSurveySingleRecordKey = (int) $aRS->fields[strtolower($surveySingleRecordDimField)];
				//@JAPR: Borra registro de la dimensión de captura - registro: DELETE FROM RIDIM_10017 WHERE RIDIM_10017KEY = ###
		        $sql = "DELETE FROM ".$surveySingleRecordDimTable." WHERE ".$surveySingleRecordDimField. " = ".$foundSurveySingleRecordKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo(translate("Error accessing")." ".$surveySingleRecordDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
		        
		        //se elimina el registro de la tabla global
		        //@JAPR: Borra registro de la tabla de hechos de capturas global: DELETE FROM RIFACT_10007 WHERE RIDIM_10017KEY = ###
		        $sql = "DELETE FROM ".$factTable." WHERE ".$surveySingleRecordDimField." = ".$foundSurveySingleRecordKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo(translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
	        }
		}
        
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
		//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
		//editando o agregando otros
		if ($bDeleteFactKeyDimVal) {
	        //obtenemos el surveyglobalkey
	        $surveyGlobalDesc = "DSC_".$this->Survey->GblSurveyGlobalDimID;
	        $sql = "SELECT ".$surveyGlobalDimField." FROM ".$surveyGlobalDimTable." WHERE ".$surveyGlobalDesc." = ".$this->Repository->DataADOConnection->Quote($searchSurveyGlobalValue);
	        $aRS = $this->Repository->DataADOConnection->Execute($sql);
	        if(!$aRS)
	        {
	            echo(translate("Error accessing")." ".$surveyGlobalDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
	        }
	        
	        if (!$aRS->EOF)
	        {
	            $foundSurveyGlobalKey = (int) $aRS->fields[strtolower($surveyGlobalDimField)];
	
		        //se elimina el elemento de la tabla d edimension
		        $sql = "DELETE FROM ".$surveyGlobalDimTable." WHERE ".$surveyGlobalDimField. " = ".$foundSurveyGlobalKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo(translate("Error accessing")." ".$surveyGlobalDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
	        }
		}
		//@JAPR
    }
    
	function getSurveyDimKey($aSurveyInstanceDim)
	{
		$valueKey = -1;
		$tableName = $aSurveyInstanceDim->Dimension->TableName;
		$fieldSurrogateKey = $tableName."KEY";
		$fieldKey = $aSurveyInstanceDim->Dimension->FieldKey;
		
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$this->SurveyID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Considerando que es un solo valor el que se esta procesando
		if (!$aRS->EOF)
		{	
			$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
		}
		
		return $valueKey;
	}

	function isNewObject()
	{
		return ($this->ReportID < 0);
	}

	function get_Title()
	{
		return translate("Entry Detail");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}

		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}
		
		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Report&SurveyID=".$this->SurveyID.$strParameters;;
		}
		else
		{
			return "BITAM_PAGE=Report&SurveyID=".$this->SurveyID."&ReportID=".$this->ReportID.$strParameters;
		}
	}

	function get_Parent()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		return BITAMReportExtCollection::NewInstance($this->Repository, $this->SurveyID, null, $svStartDate, $svEndDate, $svUserID, $svIpp);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ReportID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		$this->HasSignature = $this->Survey->HasSignature;
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailDesc";
		$aField->Title = translate("EMail");
		$aField->ToolTip = translate("EMail");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		//@JAPR 2012-06-06: Corregido un bug, en las secciones Maestro - Detalle los valores de las preguntas numéricas salían mal porque
		//les aplicaba una máscara, siendo que vienen como String combinando todos los registros capturados de dicha sección separado por ","
        $arrMasterDetSections = BITAMSection::existMasterDetSections($this->Repository, $this->SurveyID, true);
        if (!is_array($arrMasterDetSections))
        {
        	$arrMasterDetSections = array();
        }
        $arrMasterDetSections = array_flip($arrMasterDetSections);
		//@JAPR
		
		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			$fieldType = $this->FieldTypes[$i];
			$toolTip = $this->QuestionText[$i];
			$insideDyamicSection = ($this->Questions[$i]->SectionID==$this->DynamicSectionID);

			if(trim($this->Questions[$i]->AttributeName)!="")
			{
				$questionText = $this->Questions[$i]->AttributeName;
			}
			else
			{
				$questionText = $this->QuestionText[$i];
			}

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $questionText;
			$aField->ToolTip = $toolTip;

			if($fieldType==1)
			{
				//@JAPR 2012-06-06: Corregido un bug, en las secciones Maestro - Detalle los valores de las preguntas numéricas salían mal porque
				//les aplicaba una máscara, siendo que vienen como String combinando todos los registros capturados de dicha sección separado por ","
				if ($this->DynamicSectionID == $this->Questions[$i]->SectionID || isset($arrMasterDetSections[$this->Questions[$i]->SectionID]))
				{
					$aField->Type = "String";
					$aField->Size = 255;
				}
				else 
				{
					$aField->Type = "Float";
					$aField->FormatMask = "#,##0.00";
				}
				//@JAPR
			}
			else
			{
				if($fieldType==3) 
				{
					$aField->Type = "LargeString";
					$aField->Size = 5000;
				}
				else
				{
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					//Cambiado a un switch para mayor facilidad
					$blnValidQuestion = true;
					switch ($this->Questions[$i]->QTypeID) {
						case qtpPhoto:
						case qtpSignature:
						case qtpSkipSection:
						case qtpMessage:
						case qtpSync:
						case qtpPassword:
						case qtpAudio:
						case qtpVideo:
						case qtpSection:
							$blnValidQuestion = false;
							break;
					}
					//@JAPR
					
					if(!$insideDyamicSection)
					{
						if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==0 && $this->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$aField->Type = "LargeString";
							$aField->Size = 5000;
						}
						else if($this->Questions[$i]->QTypeID==qtpMulti && $this->Questions[$i]->QDisplayMode==dspVertical && $this->Questions[$i]->MCInputType==1 && $this->Questions[$i]->IsMultiDimension==0 && $this->Questions[$i]->UseCategoryDimChoice!=0)
						{
							$aField->Type = "LargeString";
							$aField->Size = 5000;
						}
                        //@MABH 2012-05-22: Si es tipo foto solo debe mostrarse el icono
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						//Cambiado a un switch para mayor facilidad
                        else if(!$blnValidQuestion) {
                        
                        }
						else 
						{
							$aField->Type = "String";
							$aField->Size = 255;
						}
					}
					else 
					{
						$aField->Type = "LargeString";
						$aField->Size = 5000;
					}
				}
			}

			if($this->DynamicSectionID!=$this->Questions[$i]->SectionID)
			{
				$aField->AfterMessage = $this->getAfterMessage($this->Questions[$i]);
			}
			$myFields[$aField->Name] = $aField;
		}
		if ($this->HasSignature == 1) {//Preguntamos si el survey tiene firma
            $aField = BITAMFormField::NewFormField();
            $aField->Name = "AltSignature";
            $aField->Title = translate("Signature");
            $aField->Type = "String";
            $aField->ToolTip = translate("Signature");
            $aField->IsLink = false;
            $aField->Size = 255;
            $aField->IsDisplayOnly = true;
            $aField->AfterMessage = $this->getAfterImageSignature();
            $myFields[$aField->Name] = $aField;
        }

		return $myFields;
	}

	function getAfterMessage($aQuestion)
	{
		$htmlString = "";

		//Verificamos si dicha pregunta tiene imagen a mostrar
		if ($aQuestion->HasReqPhoto==1 || $aQuestion->HasReqPhoto==2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				if($aQuestion->QTypeID == qtpPhoto) {                
                    $strIcon="images/camara.gif";
                    $strTitle="View photo";
                } else {
					if($aQuestion->QTypeID == qtpSignature) {
	                    $strIcon="images/firma.gif";
	                    $strTitle="View signature";
	                }
					else {
						//28Ene2013: En las preguntas con foto opcional o requerida que se despliegue el ícono de la cámara
						//y no el ícono de la firma
                		$strIcon="images/camara.gif";
                    	$strTitle="View photo";
					}
                }
                $renglon = 0;
                $columna = 0;
                $htmlString.="<img width='16px' height='16px' title='".$strTitle."' src='".$strIcon."' style='cursor:hand' onclick='javascript:openImg(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
			}
            
            if($aQuestion->QTypeID == qtpPhoto || $aQuestion->QTypeID == qtpSignature) {
                if (!($aRS !== false && !$aRS->EOF)) {
                    if($aQuestion->QTypeID == qtpPhoto) {
                        $htmlString .= "No photo taken";
                    } else {
                        $htmlString .= "No signature captured";
                    }
                }
            }
		}

		//Verificamos si dicha pregunta tiene imagen a mostrar
		if ($aQuestion->HasReqDocument==1 || $aQuestion->HasReqDocument==2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				if($aQuestion->QTypeID == qtpDocument) {                
                    $strIcon="images/document.gif";
                    $strTitle="View document";
                }
				if($aQuestion->QTypeID == qtpAudio) {                
                    $strIcon="images/audio.gif";
                    $strTitle="Listen audio";
                }
				if($aQuestion->QTypeID == qtpVideo) {                
                    $strIcon="images/video.gif";
                    $strTitle="View video";
                }
                $renglon = 0;
                $columna = 0;
                $htmlString.="<img width='16px' height='16px' title='".$strTitle."' src='".$strIcon."' style='cursor:hand' onclick='javascript:openDoc(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
			}
            
            if($aQuestion->QTypeID == qtpDocument || $aQuestion->QTypeID == qtpAudio || $aQuestion->QTypeID == qtpVideo) {
                if (!($aRS !== false && !$aRS->EOF)) {
                    //if($aQuestion->QTypeID == qtpDocument) {
                        $htmlString .= "No document attached";
                    //}
                }
            }
		}

		if ($aQuestion->HasReqComment==1 || $aQuestion->HasReqComment==2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID, StrComment FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				$strComment = trim($aRS->fields["strcomment"]);
				if($strComment!="")
				{
					$strIcon="images/comment.gif";
					$renglon = 0;
					$columna = 0;
					$htmlString.="<img width='16px' height='16px' title='View comment' src='".$strIcon."' style='cursor:hand' onclick='javascript:openComment(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
				}
			}
		}
		
		return $htmlString;
	}
	function getAfterImageSignature() {
        $htmlString = "";

        //Verificamos si el reporte tiene alguna firma capturada
        $sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = " . $this->SurveyID . " AND FactKey = " . $this->ReportID;

        $aRS = $this->Repository->DataADOConnection->Execute($sql);

        if (!$aRS || $aRS->EOF) { //$aRS !== false && !$aRS->EOF
            $htmlString.="";
        } else {
            $strIcon = "images/firma.gif";
            $htmlString.= "<img width='16px'  height='16px' title='View Signature' src='" . $strIcon . "' style='cursor:hand' onclick='javascript:showSignature(" . $this->SurveyID . "," . $this->ReportID . ")' displayMe='1'>"; //.ENTER;
          
        }
        return $htmlString;
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideBackButton($aUser)
	{
		return true;
	}

	function addButtons($aUser)
	{
		/*
?>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="googleMap(<?=$this->SurveyID?>, '<?=$this->UserID?>', '<?=$this->DateID?>', <?=$this->ReportID?>);"><img src="images/map.gif" displayMe="1"> <?=translate("Show Data in Google Maps")."   "?>  </span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="exportPDF();"><img src="images/exportpdf.gif"  displayMe="1"> <?=translate("Export To PDF")."   "?>  </span>
<?
		*/
	}

	function getImages($questionID)
	{
		global $hasPic;
		global $hasDocument;
		global $hasComment;

		$htmlString="";

		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		if(isset($hasPic[$this->FactKeyDimVal]))
		{
			if(isset($hasPic[$this->FactKeyDimVal][$questionID]))
			{
				if($hasPic[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/camara.gif";
					$renglon = 0;
					$hasPic[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasPic[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View photo' src='".$strIcon."' style='cursor:hand' onclick='javascript:openImg(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}

		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		if(isset($hasDocument[$this->FactKeyDimVal]))
		{
			if(isset($hasDocument[$this->FactKeyDimVal][$questionID]))
			{
				if($hasDocument[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/document.gif";
					$renglon = 0;
					$hasDocument[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasDocument[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View document' src='".$strIcon."' style='cursor:hand' onclick='javascript:openDoc(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}

		if(isset($hasComment[$this->FactKeyDimVal]))
		{
			if(isset($hasComment[$this->FactKeyDimVal][$questionID]))
			{
				if($hasComment[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/comment.gif";
					$renglon = 0;
					$hasComment[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasComment[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View comment' src='".$strIcon."' style='cursor:hand' onclick='javascript:openComment(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}
		//@JAPR

		return $htmlString;
	}

	function generateBeforeFormCode($aUser)
	{
		require_once('settingsvariable.inc.php');
		$strAltEformsService = '';
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ALTEFORMSSERVICE');
		if (!is_null($objSetting) && trim($objSetting->SettingValue != ''))
		{
			$strAltEformsService = $objSetting->SettingValue;
			if (substr($strAltEformsService, -1) != "/") {
				$strAltEformsService .= "/";
			}
			
			if (strtolower(substr($strAltEformsService, 0, 4)) != 'http') {
				$strAltEformsService = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$strAltEformsService;
			}
		}
		
?>		<script language="JavaScript">

		function openImg(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyImage.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openDoc(surveyID, questionID, folioID, renglon, columna)
		{
			window.open('getSurveyDocument.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openComment(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'commentWin', 'width=400, height=150, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}
		
		function showSignature(surveyID, reportID)
        {
			var aWindow = window.open('getSurveySignature.php?surveyID='+surveyID+'&reportID='+reportID, 'signatureWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
        }

		function openGoogleMap(surveyID, userID, startDate, endDate, reportID)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
			var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&userID='+userID+'&startDate='+startDate+'&endDate='+endDate+'&reportID='+reportID, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}

		//Exportar los datos del Reporte actual en GoogleMaps
 		function googleMap(surveyID, userID, dateID, reportID)
 		{
 	 		openGoogleMap(surveyID, userID, dateID, dateID, reportID);
 		}
 		
  		function exportPDF()
 		{
			frmExportPDF.submit();
 		}
 
 		function editEntry(entryID)
 		{
 			var surveyID = <?=$this->SurveyID?>;

 			openWindow('<?=$strAltEformsService?>loadEntry.php?surveyID=' + surveyID + '&extp=1&EntryID=' + entryID);
 		}
		</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
?>
	<form id="frmExportPDF" name="frmExportPDF" action="main.php" method="GET" target="_blank">
		<input type="hidden" name="BITAM_PAGE" value="Report">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportID" id="ReportID" value="<?=$this->ReportID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportPDF">
	</form>
<?
	}

	function Signature()
	{
		$htmlString = "";

		//Verificamos si el reporte tiene alguna firma capturada
		$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->ReportID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if(!$aRS || $aRS->EOF)
		{
			$htmlString = "";
		}
		else
		{
			$htmlString="<a href=javascript:showSignature(".$this->SurveyID.",".$this->ReportID.")><img src='images/firma.gif' title='".translate("View Signature")."'></a>";
		}

		return $htmlString;
	}

	function AltSignature() 
	{
        $htmlString = "";

        //Verificamos si el reporte tiene alguna firma capturada
        $sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = " . $this->SurveyID . " AND FactKey = " . $this->ReportID;

        $aRS = $this->Repository->DataADOConnection->Execute($sql);

        if (!$aRS || $aRS->EOF) {
            $htmlString = "Not captured";
        } else {
            $htmlString = "Captured";
        }

        return $htmlString;
    }
	
	function Task()
	{
		$htmlString="<a id=\"linkView\" name=\"linkView\" href=\"main.php?".$this->get_QueryString()."\">View</a>&nbsp;&nbsp;&nbsp;";
		$htmlString.="<a id=\"linkEdit\" name=\"linkEdit\" style=\"display:none\" href=\"javascript:editEntry(".$this->FactKeyDimVal.");\">Edit</a>";

		return $htmlString;
	}
	
	function insertPreviewRows($aUser)
	{
?>
		<tr id="Row_Buttons">
			<td class="object_field_value" title="Form" style="padding-top:1px;">
				<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:document.location.href='<?='main.php?'.$this->get_Parent()->get_QueryString()?>';"><img src="images/home.png" displayMe="1">&nbsp;<?=translate("Back")?></span>
				<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editEntry(<?=$this->FactKeyDimVal?>);"><img src="images/edit.png" displayMe="1">&nbsp;<?=translate("Edit entry")?></span>
				<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:BITAMReport_Remove();"><img src="images/delete.png" displayMe="1">&nbsp;<?=translate("Delete entry")?></span>
				<br><br>
			</td>
                                <td class="object_field_value_3" colspan="3" align="right" style="padding-top:1px;padding-right:200px;">
				<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')"><img src="images/edit.png" displayMe="1">&nbsp;<?=translate("Audit trail")?></span>
                                
                                <span style="float:right;" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="exportPDF();"><img src="images/exportpdf.gif"  displayMe="1">&nbsp;<?=translate("Export To PDF")?></span>
                                <span style="float:right;" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="googleMap(<?=$this->SurveyID?>, '<?=$this->UserID?>', '<?=$this->DateID?>', <?=$this->ReportID?>);"><img src="images/map.gif" displayMe="1">&nbsp;<?=translate("Map data")?></span>
                                
				<br><br>
			</td>
                        
                        
		</tr>
		
		<tr id="Row_SurveyName">
			<td class="object_field_value" title="Form"><b><?=translate("Form")?></b></td>
			<td class="object_field_value_3" colspan="3"><?=$this->SurveyName?></td>
		</tr>
<?	
	}
}

class BITAMReportExtCollection extends BITAMCollection
{
	public $SurveyID;
	public $QuestionFields;
	public $FieldTypes;
	public $QuestionText;
	public $NumQuestionFields;
	public $FirstCatQuestion; //Instancia de la primera pregunta que usa catálogo en sus respuestas
	public $CatTableName; //Nombre del catálogo usado por la pregunta $FirstCatQuestion
	public $FilterAttributes; //Infomación de los atributos que están en el catálogo $CatTableName que serán utilizados como combos de filtros en la colección.
	public $NumFilterAttributes;
	public $AttribSelected;
	public $CatParentID;
	public $CatalogID;
	public $Questions;
	public $StartDate;
	public $EndDate;
	public $UserID;
	public $ipp;
	public $HasSignature;
	public $ShowAllQuestions;
	public $Pages;
	
	public $Survey;
	public $HasDynamicSection;
	public $DynamicSectionID;
	public $DynamicSection;
	public $DynQuestionCollection;
	public $Catalog;
	public $ChildDynamicAttribID;
	//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
	public $HasMasterDetSection;
	public $MasterDetSectionIDs;
	public $MasterDetQuestionCollection;
	public $ArrayMasterDetSectionValues;
	//@JAPR
	
	function __construct($aRepository, $aSurveyID, $ShowAllQuestions=true)
	{
		BITAMCollection::__construct($aRepository);

		$this->SurveyID = $aSurveyID;
		$this->QuestionFields = array();
		$this->FieldTypes = array();
		$this->QuestionText = array();
		$this->NumQuestionFields = 0;
		$this->ShowAllQuestions = $ShowAllQuestions;
		$this->HasSignature = 0;
		$this->MsgConfirmRemove = "Do you wish to delete the selected entries?";
		$this->MsgNoSelectedRemove = "Please select the entries you wish to delete.";
        
        $this->MsgConfirmSendPDF = "Do you wish to Send PDF of the selected entries?";
		$this->MsgNoSelectedSendPDF = "Please select the entries you wish to Send PDF.";
        $this->MsgCancelSendPDF = "Send PDF of selected entries cancelled.";
		
		$this->FirstCatQuestion = null;
		$this->CatTableName = "";
		$this->FilterAttributes = array();
		$this->CatParentID = null;
		$this->NumFilterAttributes = 0;
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->ipp = 100;
		
		$this->Pages = null;
		
		$this->HasDynamicSection = false;
		$this->DynamicSectionID = -1;
		$this->DynamicSection = null;
		$this->DynQuestionCollection = null;
		$this->Catalog = null;
		$this->ChildDynamicAttribID = -1;
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$this->HasMasterDetSection = false;
		$this->MasterDetSectionIDs = array();
		$this->MasterDetQuestionCollection = array();
		$this->ArrayMasterDetSectionValues = array();
		//@JAPR
		
		if($aSurveyID!=0)
		{
			$this->Survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

			$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
			
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			$this->Survey->getSpecialFieldsStatus();
			//@JAPR
			
			if($dynamicSectionID>0)
			{
				$this->HasDynamicSection = true;
				$this->DynamicSectionID = $dynamicSectionID;
				$this->DynamicSection = BITAMSection::NewInstanceWithID($aRepository, $this->DynamicSectionID);

				//Obtenemos el ChildDynamicAttribID
				$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, $this->DynamicSection->CatMemberID);
				
				//Obtener el siguiente CatMemberID
				$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->DynamicSection->CatalogID." AND MemberOrder > ".$catMemberInstance->MemberOrder." ORDER BY MemberOrder ASC";
				
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRS->EOF)
				{
					//Asignamos el atributo hijo
					$this->ChildDynamicAttribID = (int)$aRS->fields["parentid"];
					
					//Obtenemos coleccion de las preguntas de la seccion dinamica
					$this->DynQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->DynamicSectionID);
					
					//Obtenemos el catalogo
					$this->Catalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DynamicSection->CatalogID);
				}
				else 
				{
					//Se vuelven a quedar los valores como si no hubiera seccion dinamica
					$this->HasDynamicSection = false;
					$this->DynamicSectionID = -1;
					$this->DynamicSection = null;
				}
			}
			
			$aSurvey = $this->Survey;
			$this->HasSignature = $aSurvey->HasSignature;

			if($this->ShowAllQuestions)
			{
				$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			}
			else
			{
				$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, null, 1);
			}
	
			$this->Questions = $questions;
			$numFields = count($questions->Collection);
	
			for($i=0; $i<$numFields; $i++)
			{
				$this->QuestionFields[$i] = $questions->Collection[$i]->SurveyField;
				$this->FieldTypes[$i] = $questions->Collection[$i]->QTypeID;
				$this->QuestionText[$i] = $questions->Collection[$i]->QuestionText;
			}
	
			$this->NumQuestionFields = $numFields;
	
			$questionID = BITAMReportExtCollection::getFirtsCatalogQuestion($this->Repository, $this->SurveyID);
	
			if($questionID!==null)
			{
				$this->FirstCatQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $questionID);
				$this->CatTableName = BITAMReportExtCollection::getCatTableName($this->Repository, $this->FirstCatQuestion->CatalogID);
				$this->CatParentID = BITAMReportExtCollection::getCatParentID($this->Repository, $this->FirstCatQuestion->CatalogID);
				$this->FilterAttributes = BITAMReportExtCollection::getAttributes($this->Repository, $this->FirstCatQuestion->CatalogID, $this->FirstCatQuestion->CatMemberID);
				$this->NumFilterAttributes = count($this->FilterAttributes);
				$this->CatalogID = $this->FirstCatQuestion->CatalogID;
				foreach ($this->FilterAttributes as $memberID => $memberInfo)
				{
					$this->AttribSelected[$memberID]= "";
				}
			}
			
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $aSurveyID, true);
			if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
			{
				$this->HasMasterDetSection = true;
				$this->MasterDetSectionIDs = $arrMasterDetSections;
				
				//Obtiene la colección de preguntas para indexarlas por cada Id de sección Maestro - Detalle
				foreach ($this->MasterDetSectionIDs as $aSectionID)
				{
					$aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $aSectionID);
					if (!is_null($aQuestionCollection))
					{
						$this->MasterDetQuestionCollection[$aSectionID] = $aQuestionCollection;
					}
					else 
					{
						$this->MasterDetQuestionCollection[$aSectionID] = BITAMQuestionCollection::NewInstanceEmpty($this->Repository, $this->SurveyID);
					}
				}
			}
			//@JAPR
		}
	}

	static function NewInstance($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2, $ipp=100)
	{
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, false);
		
		//Si es igual a cero el SurveyID no deberia hacer algo
		if($aSurveyID==0)
		{
			return $anInstance;
		}

		$aSurvey = $anInstance->Survey;

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		//Obtener los datos de la dimension Email
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;

		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";

		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd." B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en elFROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.= $strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
			}
		}

		$where = $joinCat;
		$filterJoin = "";
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;

			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}

			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$filterJoin .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$filterJoin .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					if($where!="")
					{
						$where.=" AND ";
					}
	
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}

		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Se movió el armado del FROM a esta parte porque se necesita agregar las tablas de las dimensiones filtradas si es que se aplicó filtros
		//por atributos y se usan dimensiones independientes
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		//@JAPR
		
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"").$aSurvey->SurveyTable.".FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey.$filterJoin;
		
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}

		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Total de Registros
		$numRows = $aRS->RecordCount();
		
		//Verificar si existen los indices ipp y page en el GET, sino es asi, asignarle valores por default
		/*
		if(!isset($_GET["ipp"]))
		{
			$_GET["ipp"] = 100;
		}
		*/
		
		$_GET["ipp"] = $ipp;
		
		if(!isset($_GET["page"]))
		{
			$_GET["page"] = 1;
		}
		
		if($numRows>0)
		{
			//Creamos el objeto de paginacion
			$anInstance->Pages = new Paginator();
			$anInstance->Pages->items_total = $numRows;
			//$anInstance->Pages->items_per_page = 100;
			//$anInstance->Pages->items_per_page = $_GET["ipp"];
			$anInstance->Pages->items_per_page = $ipp;
			$anInstance->Pages->mid_range = 9;
			$anInstance->Pages->paginate();
			
			//Agregamos el elemento LIMIT al SELECT para obtener los registros que se requieren
			$sql .= $anInstance->Pages->limit;
	
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
	
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$anInstance->Pages->current_num_items = $aRS->RecordCount();
			}
		}
		
		$arrFolios=array();
		$arrFolios2=array();
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMReportExt::NewInstanceFromRS($anInstance->Repository, $aSurveyID, $aRS);
			//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
			//Para los comentarios
			$arrFolios[(int)$aRS->fields["factkey"]]=(int)$aRS->fields["factkey"];
			//Para las imagenes
			$arrFolios2[(int)$aRS->fields["factkeydimval"]]=(int)$aRS->fields["factkeydimval"];
			//@JAPR
			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}

		global $hasPic;
		$hasPic = $aSurvey->getAnswerWithPictures($arrFolios2);
		global $hasDocument;
		$hasDocument = $aSurvey->getAnswerWithDocuments($arrFolios2);
		global $hasComment;
		$hasComment = $aSurvey->getAnswerWithComments($arrFolios2);

		return $anInstance;
	}

	//@JAPR 2012-01-10: Agregado el método que genera el archivo .zip que contiene las imagenes del Reporte especificado
	static function NewInstanceToCompress($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, true);

		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();

		//Insertamos el encabezado del array
		$line = '"User Name","Date"';

		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			if(trim($anInstance->Questions[$i]->AttributeName)!="")
			{
				$strText = $anInstance->Questions[$i]->AttributeName;
			}
			else
			{
				$strText = $anInstance->Questions[$i]->QuestionText;
			}

			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';

		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;

			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}

			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where = " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where = " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey

		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
		$strZipFileName = "Survey".$anInstance->SurveyID."Photos.zip";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullZipFileName = $strLogPath.$strZipFileName;
		//@JAPR
		$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		$strZipPath = "survey_".$anInstance->SurveyID;
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		if ($aZipObj->open($file, ZIPARCHIVE::OVERWRITE)!==TRUE)
		{
			$intFailures = 1;
		    echo("Cannot create zip file <$strFullZipFileName>");
		}
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->AttributeName;
					if (trim($strQuestionText) == '')
					{
						$strQuestionText = $arrQuestionsColl[$intQuestionID]->QuestionText;
					}
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			echo("No photo file was compressed for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			/*
			//$strZipData = $aZipObj->file();
			header("Content-type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			//header("Content-length: ".strlen($strZipData));
			header("Content-length: ".filesize($file));
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
			*/
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
			header("Content-Length: ".filesize($file));

			readfile($file);
		}
		exit();
	}
	//@JAPR

	//@ 2012-04-12: Agregado el método que genera el archivo .pptx que contiene las imagenes del Reporte especificado
	static function NewInstanceToPPTX($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, true);

		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
		$sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();

		//Insertamos el encabezado del array
		$line = '"User Name","Date"';

		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			if(trim($anInstance->Questions[$i]->AttributeName)!="")
			{
				$strText = $anInstance->Questions[$i]->AttributeName;
			}
			else
			{
				$strText = $anInstance->Questions[$i]->QuestionText;
			}

			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';

		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;

			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}

			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where = " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where = " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey

		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToPPTX: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		// pptx stuff
	    set_include_path(get_include_path() . PATH_SEPARATOR . './phppowerpoint/Classes/');
	    require_once("PHPPowerpoint.php");
	    require_once('PHPPowerPoint/IOFactory.php');
	    
	    $slideWidth = 1024;
	    $slideHeight = 768;
	    $titleHeight = 38;
	    $fontSize = 16;
	    
	    $powerpoint = new PHPPowerPoint();
	    
	    //$powerpoint->removeSlideByIndex(0);
	
	    // Create slide
	    //$currentSlide = $powerpoint->createSlide();
	    
	    // first slide
	    $currentSlide = $powerpoint->getActiveSlide();
	
	    $powerpoint->setSlideWidth(round($slideWidth/1.333333333));
	    $powerpoint->setSlideHeight(round($slideHeight/1.333333333));
			
	    // add the date
	    $shape = $currentSlide->createRichTextShape();
	    $shape->setWidth($slideWidth);
	    $shape->setHeight($titleHeight);
	    // create the text run
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($aSurvey->SurveyName);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(32);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FF000000'));
	    $shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
	    //$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_BOTTOM);
	    $shape->setOffsetY(300);
	    $shape->createBreak();
	    $shape->createBreak();
	
	    $strDatesRange = "";
	    if (strlen($startDate) > 0 && strlen($endDate) > 0)
	    {
	      $strDatesRange = date("F d, Y", strtotime($startDate))."   -    ".date("F d, Y", strtotime($endDate));
	    }
	    elseif (strlen($startDate) > 0)
	    {
	      $strDatesRange = "From: ".date("F d, Y", strtotime($startDate));
	    }
	    elseif (strlen($endDate) > 0)
	    {
	      $strDatesRange = "To: ".date("F d, Y", strtotime($endDate));
	    }
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($strDatesRange);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(26);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
	
	    //Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("./log", "pptx");
		$strPPTXFileName = "Survey".$anInstance->SurveyID."Photos.pptx";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullPPTXFileName = $strLogPath.$strPPTXFileName;
		//@JAPR
		$strCurrPath = "./surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';

		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];

			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->AttributeName;
					if (trim($strQuestionText) == '')
					{
						$strQuestionText = $arrQuestionsColl[$intQuestionID]->QuestionText;
					}
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
        		$strNewFileName .= $strQuestionText;
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);

				// sbf: agregar imagen
				if (file_exists($strCurrPath.$strPhotoName))
				{
					// add a new slide
					$currentSlide = $powerpoint->createSlide();
					$powerpoint->setSlideWidth(round($slideWidth/1.333333333));
					$powerpoint->setSlideHeight(round($slideHeight/1.333333333));
					
					// create the drawing shape
					$shape = $currentSlide->createDrawingShape();
					$shape->setName($strPhotoName);
					$shape->setDescription($strNewFileName);
					$sPathPict = $strCurrPath.$strPhotoName;
					$shape->setPath($sPathPict);
					$shape->setOffsetY($titleHeight);
					
					// check image size
					$maxwidth = $slideWidth;
					$maxheight = $slideHeight - $titleHeight * 2;
					if (($img_info = @getimagesize($sPathPict)) !== false) 
					{
						//@JAPR 2015-02-06: Agregado soporte para php 5.6
						$aImg = explode(' ',$img_info[3]);
						$wImg = str_replace("width=",'',$aImg[0]);
						$wImg = (float)str_replace('"','',$wImg);
						$hImg = str_replace("height=",'',$aImg[1]);
						$hImg = (float)str_replace('"','',$hImg);
						if ($wImg > $maxwidth || $hImg > $maxheight)
						{
							$shape->setResizeProportional(true);
							$shape->setWidthAndHeight($maxwidth, $maxheight);
							$wImg = $shape->getWidth(); //$maxwidth;
							$hImg = $shape->getHeight(); //$maxheight;
						}
						// center image
						$shape->setOffsetX(($maxwidth - $wImg) / 2);
						$shape->setOffsetY($titleHeight + ($maxheight - $hImg) / 2);
						//$shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
						//$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_CENTER);
					}
					
					// create the text shape
					$shape = $currentSlide->createRichTextShape();
					$shape->setWidth($slideWidth);
					$shape->setHeight($titleHeight);
					// background-color
					//          $shape->getFill()->setFillType(PHPPowerPoint_Style_Fill::FILL_SOLID);
					//          $shape->getFill()->getStartColor()->setRGB('4F81BD');
					// create the text run
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$textRun = $shape->createTextRun($strNewFileName);
					$textRun->getFont()->setName('Calibri');
					$textRun->getFont()->setSize($fontSize);
					$textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
				}
				else 
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = "File not exists";
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		
		if($intFailures == $intCount)
		{
			echo("No photo file was included for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe pptx creation process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			// save pptx
			$objWriter = PHPPowerPoint_IOFactory::createWriter($powerpoint, 'PowerPoint2007');
			$objWriter->save($file);
			@rename($file, $file .".pptx");
			$file .= ".pptx";
			$powerpoint = null;
		  
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strPPTXFileName."\";");
			header("Content-Length: ".filesize($file));

			readfile($file);
		}
		exit();
	}
	//@SBF

	static function NewInstanceToExportDesg($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
        
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, true);

		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();

		//Insertamos el encabezado del array
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		$line = '"User Name","Date","Latitude","Longitude"';
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$line .= ', Accuracy';
		}

		//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
		//Carga las definiciones de catálogos utilizadas y dependiendo de las preguntas va asignando que atributos se deberían agregar a cada una,
		//ya que múltiples preguntas de catálogo en la misma encuesta sólo podrían usar atributos posteriores a los previamente usados, por lo 
		//tanto dichos atributos ni los padres no se deben mostrar en preguntas posteriores, sólo aquellos que aun no se estén desplegando
		$arrCatalogs = array();
		$arrCatalogMembers = array();
		$arrLastShownAttributeIdx = array();
		$arrAttributesByQuestion = array();
		//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
		$arrQuestionsByID = array();
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
			$objQuestion = $anInstance->Questions[$i];
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
			//@JAPR
			
			if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode != dspMatrix)
			{
				$intCatalogID = $anInstance->Questions[$i]->CatalogID;
				$intCatMemberID = $anInstance->Questions[$i]->CatMemberID;
				if ($intCatalogID > 0 && $intCatMemberID > 0)
				{
					if (isset($arrCatalogs[$intCatalogID]))
					{
						$aCatalog = $arrCatalogs[$intCatalogID];
						$aCatalogMembersColl = $arrCatalogMembers[$intCatalogID];
						$intLastShownAttribute = $arrLastShownAttributeIdx[$intCatalogID];
					}
					else 
					{
						$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
						$aCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
						$aCatalogMembersColl = $aCatalogMembersColl->Collection;
						$intLastShownAttribute = -1;
						$arrCatalogs[$intCatalogID] = $aCatalog;
						$arrCatalogMembers[$intCatalogID] = $aCatalogMembersColl;
						$arrLastShownAttributeIdx[$intCatalogID] = $intLastShownAttribute;
					}
					
					//Primero verifica si el Atributo de esta pregunta ya había sido desplegado, si lo fué entonces se trata de un error y por
					//tanto simplemente se agregará a este atributo, de lo contrario se incluirían otros posteriores lo cual estaría incorrecto
					$blnAttributeUsed = true;
					$intNumAttributes = count($aCatalogMembersColl);
					for ($intIdx = $intLastShownAttribute + 1; $intIdx < $intNumAttributes; $intIdx++)
					{
						if ($aCatalogMembersColl[$intIdx]->MemberID == $intCatMemberID)
						{
							$blnAttributeUsed = false;
							break;
						}
					}
					
					$arrAttributesColl = array();
					//Si el atributo ya estaba usado, simplemente brincará este If para sólo agregar el atributo en cuestión, de lo contrario
					//agrega todos los padres hasta llegar al atributo de la pregunta
					if (!$blnAttributeUsed)
					{
						//Crea la entrada para esta pregunta usando su índice, el contenido será el Array de todos los Atributos que debe imprimir
						//los cuales consisten en los Atributos Padres del asociado a la pregunta, incluyendo al propio atributo de la pregunta, siemre
						//y cuando NO se hubieran desplegado ya en otra pregunta previa
						for($intLastShownAttribute = $intLastShownAttribute +1; $intLastShownAttribute < $intNumAttributes; $intLastShownAttribute++)
						{
							if ($aCatalogMembersColl[$intLastShownAttribute]->MemberID == $intCatMemberID)
							{
								break;
							}
							
							$arrAttributesColl[] = $aCatalogMembersColl[$intLastShownAttribute];
						}
					}

					//En realidad no se necesita agregar el Atributo de la pregunta, ya que el código mas abajo simplemente usará el nombre de la
					//pregunta, sólo se necesitan los atributos padre pues esos no se conocían y hay que anteponerlos a la pregunta en cuestión
					//$arrAttributesColl[] = null;	// NO es necesario activar esta línea, si el count de este array es 0 aun así funciona abajo
					
					//Agrega el atributo de la pregunta como el texto de la misma directamente
					$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID] = $arrAttributesColl;
				}
			}
		}
		//@JAPR
		
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
                if(trim($anInstance->Questions[$i]->AttributeName)!="")
                {
                    $strText = $anInstance->Questions[$i]->AttributeName;
                }
                else
                {
                    $strText = $anInstance->Questions[$i]->QuestionText;
                }

                if ($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID)
                {
                    foreach ($anInstance->Questions[$i]->SelectOptions as $displayText)
                    {
                        $line.=',"'.str_replace('"', '""', $strText.'-'.$displayText).'"';
                    }

                    //@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
                    //Se genera un nuevo Array vacio según el tipo de respuestas múltiples para ser reutilizado al recorrer las encuestas capturadas
                    if($anInstance->Questions[$i]->QTypeID == qtpMulti && count($anInstance->Questions[$i]->SelectOptions) > 0)
                    {
                        if($anInstance->Questions[$i]->MCInputType==0)
                        {
                            //En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se toma tal cual
                            //y sólo se rellena con cadenas vacias en su valores
                            $arrAnswers = array_flip(array_values($anInstance->Questions[$i]->SelectOptions));
                        }
                        else 
                        {
                            //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                            //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                            //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                            $arrAnswers = array_values($anInstance->Questions[$i]->SelectOptions);
                        }

                        $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                        foreach ($arrAnswers as $intID => $strValue)
                        {
                            $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                        }
                    }
                    //@JAPR
                }
                else if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode == dspMatrix)
                {
                    if($anInstance->Questions[$i]->OneChoicePer==0)
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptions;
                    }
                    else 
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptionsCol;
                    }

                    foreach ($theOptions as $displayText)
                    {
                        $line.=',"'.str_replace('"', '""', $strText.'-'.$displayText).'"';
                    }

                    //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                    //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                    //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                    $arrAnswers = array_values($theOptions);

                    $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                    foreach ($arrAnswers as $intID => $strValue)
                    {
                        $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                    }
                }
                else 
                {
                    //@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
                    //Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
                    //de catálogo por lo que deben ser selección sencilla exclusivamente
                    if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
                    {
                        $arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
                        if (!is_null($arrAttributesColl))
                        {
                            foreach ($arrAttributesColl as $aCatMember)
                            {
                                $line.=',"'.str_replace('"', '""', $aCatMember->MemberName).'"';
                            }
                        }
                    }
                    //@JAPR
                    $line.=',"'.str_replace('"', '""', $strText).'"';
                }
            }
            
			//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
			if ($objQuestion->HasReqComment) {
                if(trim($objQuestion->AttributeName)!="")
                {
                    $strText = $objQuestion->AttributeName;
                }
                else
                {
                    $strText = $objQuestion->QuestionText;
                }
                $strText .= ' '.translate('Comment');
                $line.=',"'.str_replace('"', '""', $strText).'"';
			}
			//@JAPR
		}

		//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection) 
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
		}
		//@JAPR
		
		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;

		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
		$fieldAttrLatitude = '0';
		$fieldAttrLongitude = '0';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = $catTable."."."DSC_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = $catTable."."."DSC_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = $catTable."."."DSC_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		$sql.=", ".$fieldAttrLatitude.", ".$fieldAttrLongitude.$fieldAttrAccuracy;
		
		//Se modifica para usarse al leer el recordset
		$fieldAttrLatitude = '';
		$fieldAttrLongitude = '';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = "dsc_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = "dsc_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$fieldAttrAccuracy = "dsc_".$aSurvey->AccuracyAttribID;
			}
			//@JAPR
		}
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		//Se forzará a utilizar la tabla de hechos para extraer la información adicional
		if ($aSurvey->FactKeyDimID > 0) {
			$fromCat.=", ".$aSurvey->FactTable." B";
			$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
			$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
			$strAnd = " AND ";
			
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			$joinField = "RIDIM_".$aSurvey->FactKeyDimID."KEY";
			$fromCat.=", ".$catTable;
			$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
		}
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//Modificada la condición para omitir el ciclo en lugar de preguntar si debe entrar o no a procesar algo
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($objQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$objQuestion->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = $aCatMember->IndDimID;
								$catTable = "RIDIM_".$memberParentID;
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$memberParentID."KEY";
									$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
									$strAnd = " AND ";
								}
							}
						}
					}
					//@JAPR
					
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
							}
						}
					}
					//@JAPR
					
					$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
	
					$sql.=", ".$catTable.".".$attribField;
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en elFROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
				//@JAPR
			}
		}

		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;

			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}

			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					if($where!="")
					{
						$where.=" AND ";
					}
	
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
		//El filtro completo se usará tal cual entre las tablas paralela, hechos y catálogos, pero filtros adicionales ya no aplican para extraer
		//los comentarios así que esos se ignorarán
		$strJoins = $where;
		if ($strJoins != '') {
			$strJoins = "AND ".$strJoins;
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$blnMultiRecordSection = ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection);
		if ($aSurvey->UseStdSectionSingleRec) {
			//Si se usa el registro único, entonces se trae exclusivamente los datos de dichos registros, de lo contrario puede traer cualquier
			//registro ya que antes de este cambio en todos se grababa lo mismo para las preguntas estándar
			if ($where != "") {
				$where .= " AND ";
			}
			$where .= $aSurvey->SurveyTable.".EntrySectionID = 0";
		}
		//@JAPR
		
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}

		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
		$arrAllComments = array();
		$sqlComments = "";
		$sqlComments = "SELECT tComm.FactKey as FactKeyDimVal, tComm.MainFactKey as FactKey, tComm.SurveyID, tComm.QuestionID, tComm.StrComment 
			FROM SI_SV_SurveyAnswerComment tComm, $aSurvey->SurveyTable $fromCat 
			WHERE tComm.SurveyID = $aSurveyID AND tComm.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
			ORDER BY tComm.FactKey, tComm.MainFactKey";
		$aRSComm = $anInstance->Repository->DataADOConnection->Execute($sqlComments);
		if ($aRSComm) {
			while (!$aRSComm->EOF) {
				$intQuestionID = (int) $aRSComm->fields["questionid"];
				$objQuestion = @$arrQuestionsByID[$intQuestionID];
				if (!is_null($objQuestion) && $objQuestion->HasReqComment) {
					$factKey = (int) @$aRSComm->fields["factkey"];
					$factKeyDimVal = (int) @$aRSComm->fields["factkeydimval"];
					$strComment = (string) @$aRSComm->fields["strcomment"];
						if (!isset($arrAllComments[$factKeyDimVal])) {
						$arrAllComments[$factKeyDimVal] = array();
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					elseif (!isset($arrAllComments[$factKeyDimVal][$intQuestionID])) {
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					
					//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
					//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
					//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
					if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
						//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
						//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					else {
						//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
						if (count($arrAllComments[$factKeyDimVal][$intQuestionID]) == 0) {
							$arrAllComments[$factKeyDimVal][$intQuestionID][0] = $strComment;
						}
					}
				}
				$aRSComm->MoveNext();
			}
		}
		//@JAPR
		
		$objReportData = BITAMReport::NewInstance($aRepository, $aSurveyID);
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$factKey = (int)$aRS->fields["factkey"];
			//Se modificó la lógica para cargar una instancia de report mejor, ya que eso incluiría a la sección Maestro-detalle o cualquier nueva
			//funcionalidad que se agregue en el futuro en lugar de tener una función independiente como getDynamicSectionValuesByFactKeyDimVal
			//para cargar por separado cada cosa, al final la instancia de report ya incluía el mismo array que genera la función mencionada así
			//que se tenía código duplicado realmente
			if ($blnMultiRecordSection) {
				require_once('reportExt.inc.php');
				$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $factKey, $aSurveyID);
				$arrayDynSectionValues = @$objReportData->ArrayDynSectionValues[$anInstance->DynamicSectionID];
				if (is_null($arrayDynSectionValues) || !is_array($arrayDynSectionValues)) {
					$arrayDynSectionValues = array();
				}
			}
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = str_replace('"', '""', BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = str_replace('"', '""', $aRS->fields["dateid"]);
			//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
			$latitude = (float) @$aRS->fields[$fieldAttrLatitude];
			$longitude = (float) @$aRS->fields[$fieldAttrLongitude];
			$line='"'.$userName.'","'.$dateValue.'","'.$latitude.'","'.$longitude.'"';
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$accuracy = (float) @$aRS->fields[$fieldAttrAccuracy];
				$line .= ', "'.$accuracy.'"';
			}
			//@JAPR
			
			$line='"'.$userName.'","'.$dateValue.'"';
			$validateValue = '';
			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{
				//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
				$objQuestion = $anInstance->Questions[$i];
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
	            if ($blnValidQuestion) {
	                $strCatParentAttibuteValues = '';
					//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
					//que tengan seccion dinamica
					if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						//NO se soportan preguntas de selección sencilla que usen catálogo dentro de secciones dinámicas, por lo tanto no es necesario
						//programar esta parte, si se hubiesen creado algunas encuestas con este caso simplemente tronarían desde la App así que no
						//habría necesidad de llegar al reporte (aunque el código de Web generalmente soporta mas cosas, no es algo permitido en realidad)
						//@JAPR
						
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
						{
							foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
							{
								$idxName = $anInstance->QuestionFields[$i];
								
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								$strValue.=$element["desc"]."=".(string) @$element[$idxName];
							}
						}
						else 
						{
							//Si es entrada de tipo Checkbox, el valor seran
							//todo los valores seleccionados concatenados con ;
							if($anInstance->Questions[$i]->MCInputType==0)
							{
								foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									$valueInteger = (int) @$element[$idxName];
									
									if($valueInteger!=0)
									{
										if($strValue!="")
										{
											$strValue.=";";
										}
										
										$strValue.=$element["desc"];
									}
								}
							}
							else 
							{
								foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue.=$element["desc"]."=".(string) @$element[$idxName];
								}
							}
						}
						
						$validateValue = $strValue;
						//$line.=',"'.str_replace('"', '""', $strValue).'"';
					}
					//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
					{
						//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
						//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayMasterDetSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
						if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
							$arrMasterDetSectionValues = array();
						}
						
						$intIndex = 1;
						foreach ($arrMasterDetSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$strTempVal = (string) @$element[$idxName];
							//Si tiene registro único activado, ya no hay forma en que este valor no corresponda con la sección maestro-detalle
							if (!$aSurvey->UseStdSectionSingleRec && $anInstance->HasDynamicSection)
							{
								//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
								//Conchita, antes estaba asi 2012-12-04: if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
								//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
								//entonces no se muestre, porque probablemente es un registro de otra sección
								if (trim($strTempVal) === '' || ($objQuestion->QTypeID == qtpOpenNumeric && trim($strTempVal) == '0' && is_numeric($strTempVal)))
								{
									//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
									$strTempVal = false;
								}
							}
							
							if ($strTempVal !== false)
							{
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								//if($anInstance->Questions[$i]->FormatMask!='') {
								//	$strTempVal = formatNumber($strTempVal,$anInstance->Questions[$i]->FormatMask);
								//}
								
								$strValue .= "<".$intIndex."=".$strTempVal.">";
								//$strValue .= "<".$element[$idxName].">";
							}
							$intIndex++;
							//@JAPR
						}
						
						$validateValue = $strValue;
					}
					//@JAPR
					else 
					{
						if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
						{
							//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
							//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
							//de catálogo por lo que deben ser selección sencilla exclusivamente
							if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
							{
								$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
								if (!is_null($arrAttributesColl))
								{
									foreach ($arrAttributesColl as $aCatMember)
									{
										//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
										if ($aSurvey->DissociateCatDimens) {
											$memberParentID = $aCatMember->IndDimID;
											$attribField = "DSC_".$memberParentID;
										}
										else {
											$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
											$attribField = "DSC_".$memberParentID;
										}
										$lowerFieldName = strtolower($attribField);
										$strCatParentAttibuteValues .= ',"'.$aRS->fields[$lowerFieldName].'"';
										//@JAPR
									}
									//$strCatParentAttibuteValues = substr($strCatParentAttibuteValues, 1);
								}
							}
							//@JAPR
							
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
								$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
							$attribField = "DSC_".$memberParentID;
							$lowerFieldName = strtolower($attribField);
							if($anInstance->FieldTypes[$i]==1)
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=','.$aRS->fields[$lowerFieldName];
							}
							else
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
							}
						}
						else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$factKey = (int)$aRS->fields["factkey"];
							$separator = "_SVSep_";
							//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
							//en una cadena con enters las respuestas q fueron capturadas
							$strAnswersSCMatrix = $anInstance->getOnlyAnswersSCMatrix($anInstance->Questions[$i], $factKeyDimVal, $factKey, $separator);
							
							$validateValue = $strAnswersSCMatrix;
						}
						else 
						{
							$lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
							
							if($anInstance->FieldTypes[$i]==1)
							{
								//$line.=','.$aRS->fields[$lowerFieldName];
								$validateValue = (float) $aRS->fields[$lowerFieldName];
								//$line.=','.$validateValue;
							}
							else
							{
								if($anInstance->FieldTypes[$i]==3)
								{
									//$line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
									$newValidateValue = '';
									$validateValue = $aRS->fields[$lowerFieldName];
									
									if(is_null($validateValue))
									{
										$validateValue = "";
									}
									
									if($anInstance->Questions[$i]->MCInputType==0)
									{
										$newValidateValue = $validateValue;
									}
									else
									{
										$arrayTemporal = explode(';', $validateValue);
										
										foreach($arrayTemporal as $elementTemp)
										{
											if($elementTemp=='')
											{
												$elementTemp = 'NA';
											}
											
											if($newValidateValue!='')
											{
												$newValidateValue.=';';
											}
											
											$newValidateValue.=$elementTemp;
										}
									}
									$validateValue = $newValidateValue;
									//$line.=',"'.str_replace('"', '""', $newValidateValue).'"';
								}
								else
								{
	                                $validateValue = $aRS->fields[$lowerFieldName];
									
	                                if(is_null($validateValue) || $validateValue=="")
	                                {
	                                    $validateValue = "NA";
	                                }
	                                //$line.=',"'.str_replace('"', '""', $validateValue).'"';
								}
							}
						}
					}
					
					//@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
					if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
					{
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						
						if(trim($validateValue)!="")
						{
							$arrayTemporal = explode(';', $validateValue);
						}
						else 
						{
							$arrayTemporal = array();
						}
						
						if($anInstance->Questions[$i]->MCInputType==0)
						{
							//En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se reemplaza
							//tal cual
							foreach($arrayTemporal as $elementTemp)
							{
								@$arrAnswers[$elementTemp] = $elementTemp;
							}
						}
						else 
						{
							//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
							//posibles respuestas no es asociativo en este caso)
							$intNumAnsw = count($arrayTemporal);
							for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
							{
								$elementTemp = $arrayTemporal[$intIdx];
								if (!is_null($elementTemp) && trim($elementTemp) != '')
								{
									@$arrAnswers[$intIdx] = $elementTemp;
								}
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						foreach ($arrAnswers as $elementTemp)
						{
							$line.=',"'.str_replace('"', '""', $elementTemp).'"';
						}
					}
					else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
					{
						$separator = "_SVSep_";
						
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						$arrayTemporal = explode($separator, $validateValue);
						//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
						//posibles respuestas no es asociativo en este caso)
						$intNumAnsw = count($arrayTemporal);
						for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
						{
							$elementTemp = $arrayTemporal[$intIdx];
							if (!is_null($elementTemp) && trim($elementTemp) != '')
							{
								@$arrAnswers[$intIdx] = $elementTemp;
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						foreach ($arrAnswers as $elementTemp)
						{
							$line.=',"'.str_replace('"', '""', $elementTemp).'"';
						}
					}
					else 
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						$line.= $strCatParentAttibuteValues.',"'.str_replace('"', '""', $validateValue).'"';
					}
				}
				
				//@JAPR 2013-03-06: Agregados los catálogos al reporte (#28011)
				if ($objQuestion->HasReqComment) {
					//El array de comentarios viene ordenado por factKey dentro de la pregunta, esto es los comentarios por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrComments = @$arrAllComments[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrComments) || !is_array($arrComments)) {
						$arrComments = array();
					}
					
					$strValue = '';
					if (count($arrComments) > 0) {
						//$strComment = implode('; ')
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrComments as $intCommFactKey => $strComment) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $factKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan comentarios por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strComment) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strComment;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strComment = (string) @$arrComments[$intFactKey];
								if (trim($strComment) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strComment.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un comentario
							$strValue = (string) @$arrComments[0];
						}
					}
					$line.=',"'.str_replace('"', '""', $strValue).'"';
				}
				//@JAPR
			}
			
			array_push($afile, $line);
			
			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;
		
		$fStats = fopen($spathfile, 'w+');
		
		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}

		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo "Error file path:{$spathfile}";
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo "Error file path:{$spathfile}";
			exit();
		}
	}
	
	static function NewInstanceToExport($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
        
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, true);

		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();

		//Insertamos el encabezado del array
		$line = '"User Name","Date"';

		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
                if(trim($anInstance->Questions[$i]->AttributeName)!="")
                {
                    $strText = $anInstance->Questions[$i]->AttributeName;
                }
                else
                {
                    $strText = $anInstance->Questions[$i]->QuestionText;
                }

                $line.=',"'.str_replace('"', '""', $strText).'"';
            }
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;

		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";

		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
				//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
				if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
                {
					//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
					switch ($anInstance->Questions[$i]->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
						case qtpSignature:
						case qtpShowValue:
						case qtpMessage:
						case qtpSkipSection:
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
							break;
							
						default:
    	                	$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
    	                	break;
					}
                }
                else
                {
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						//Si se vuelve a repetir el catalogo, con este arreglo
						//se evita que se vuelva a dar de alta en el FROM dicha tabla
						if(!isset($arrayTables[$aSurvey->FactTable]))
						{
							$fromCat.=", ".$aSurvey->FactTable." B";
							$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
							$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
							$strAnd = " AND ";
						}
						
						//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
						$catParentID = $objQuestion->CatIndDimID;
						$memberParentID = $catParentID;
						$catTable = "RIDIM_".$catParentID;
						$attribField = "DSC_".$memberParentID;
						$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
						$sql.=", ".$catTable.".".$attribField;
						$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
						
						//Si se vuelve a repetir el catalogo, con este arreglo
						//se evita que se vuelva a dar de alta en el FROM dicha tabla
						if(!isset($arrayTables[$catTable]))
						{
							$fromCat.=", ".$catTable;
							$joinField = "RIDIM_".$catParentID."KEY";
							$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
							$arrayTables[$catTable] = $catTable;
							$strAnd = " AND ";
						}
					}
					else {
	                    $catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
	                    $catTable = "RIDIM_".$catParentID;
	                    $memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
	                    $attribField = "DSC_".$memberParentID;
	
	                    $sql.=", ".$catTable.".".$attribField;
	
	                    //Si se vuelve a repetir el catalogo, con este arreglo
	                    //se evita que se vuelva a dar de alta en elFROM dicha tabla
	                    if(!isset($arrayTables[$catTable]))
	                    {
	                        $fromCat.=", ".$catTable;
	                    }
	
	                    $arrayTables[$catTable] = $catTable;
	
	                    if($joinCat!="")
	                    {
	                        $joinCat.=" AND ";
	                    }
	                    $joinField = "RIDIM_".$catParentID."KEY";
	                    $joinCat.=$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
	                    $strAnd = " AND ";
					}
					//@JAPR
                }
            }
		}

		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;

			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}

			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					if($where!="")
					{
						$where.=" AND ";
					}
	
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}

		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}

		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];

			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = str_replace('"', '""', BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = str_replace('"', '""', $aRS->fields["dateid"]);

			$line='"'.$userName.'","'.$dateValue.'"';

			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
                if ($blnValidQuestion) {
                    //El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
                    //que tengan seccion dinamica
                    if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
                    {
                        $fieldName =$anInstance->QuestionFields[$i];
                        $strValue = "";
                        //Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
                        //Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
                        //porque todavia no se sabe exactamente como se va a comportar
                        if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
                        {
                            foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
                            {
                                $idxName = $anInstance->QuestionFields[$i];

                                if($strValue!="")
                                {
                                    $strValue.="; ";
                                }

                                $strValue.=$element["desc"]."=".$element[$idxName];
                            }
                        }
                        else 
                        {
                            //Si es entrada de tipo Checkbox, el valor seran
                            //todo los valores seleccionados concatenados con ;
                            if($anInstance->Questions[$i]->MCInputType==0)
                            {
                                foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
                                {
                                    $idxName = $anInstance->QuestionFields[$i];
                                    $valueInteger = (int)$element[$idxName];

                                    if($valueInteger!=0)
                                    {
                                        if($strValue!="")
                                        {
                                            $strValue.=";";
                                        }

                                        $strValue.=$element["desc"];
                                    }
                                }
                            }
                            else 
                            {
                                foreach ($arrayDynSectionValues[$anInstance->DynamicSectionID] as $element)
                                {
                                    $idxName = $anInstance->QuestionFields[$i];

                                    if($strValue!="")
                                    {
                                        $strValue.="; ";
                                    }

                                    $strValue.=$element["desc"]."=".$element[$idxName];
                                }
                            }
                        }

                        $line.=',"'.str_replace('"', '""', $strValue).'"';
                    }
                    else 
                    {
						//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
						if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
                        {
                            $lowerFieldName = strtolower($anInstance->QuestionFields[$i]);

                            if($anInstance->FieldTypes[$i]==1)
                            {
                                //$line.=','.$aRS->fields[$lowerFieldName];
                                $validateValue = (float) $aRS->fields[$lowerFieldName];
                                $line.=','.$validateValue;
                            }
                            else
                            {
                                if($anInstance->FieldTypes[$i]==3)
                                {
                                    //$line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
                                    $newValidateValue = '';
                                    $validateValue = $aRS->fields[$lowerFieldName];

                                    if(is_null($validateValue) || $validateValue=="")
                                    {
                                        $newValidateValue = "NA";
                                    }
                                    else
                                    {
                                        $arrayTemporal = explode(';', $validateValue);

                                        foreach($arrayTemporal as $elementTemp)
                                        {
                                            if($elementTemp=='')
                                            {
                                                $elementTemp = 'NA';
                                            }

                                            if($newValidateValue!='')
                                            {
                                                $newValidateValue.=';';
                                            }

                                            $newValidateValue.=$elementTemp;
                                        }
                                    }
                                    $line.=',"'.str_replace('"', '""', $newValidateValue).'"';
                                }
                                else
                                {
                                    $validateValue = $aRS->fields[$lowerFieldName];

                                    if(is_null($validateValue) || $validateValue=="")
                                    {
                                        $validateValue = "NA";
                                    }
                                    $line.=',"'.str_replace('"', '""', $validateValue).'"';
                                }
                            }
                        }
                        else
                        {
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
	                            $memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
                            $attribField = "DSC_".$memberParentID;
                            $lowerFieldName = strtolower($attribField);
                            if($anInstance->FieldTypes[$i]==1)
                            {
                                $line.=','.$aRS->fields[$lowerFieldName];
                            }
                            else
                            {
                                $line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
                            }
                        }
                    }
                }
			}

			array_push($afile, $line);

			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;

		$fStats = fopen($spathfile, 'w+');

		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}

		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo "Error file path:{$spathfile}";
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				readfile($spathfile);
				exit();
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo "Error file path:{$spathfile}";
			exit();
		}
	}
	
	static function NewInstanceForRemove($aRepository, $aSurveyID, $anArrayOfReportIDs=null, $startDate="", $endDate="", $userID=-2, $ipp=100)
	{
		$anInstance = new BITAMReportExtCollection($aRepository, $aSurveyID, false);

		//Si es igual a cero el SurveyID no deberia hacer algo
		if($aSurveyID==0)
		{
			return $anInstance;
		}

		$aSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		$whereFilter = "";
		if (!is_null($anArrayOfReportIDs))
		{
			switch (count($anArrayOfReportIDs))
			{
				case 0:
					break;
				case 1:
					$whereFilter = " AND B.FactKey = ".((int)$anArrayOfReportIDs[0]);
					break;
				default:
					foreach ($anArrayOfReportIDs as $aReportID)
					{
						if ($whereFilter != "")
						{
							$whereFilter .= ", ";
						}

						$whereFilter .= (int)$aReportID; 
					}

					if ($whereFilter != "")
					{
						$whereFilter = " AND B.FactKey IN (".$whereFilter.")";
					}
					break;
			}
		}

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;

		//Obtener los datos de la dimension Email
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;

		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";

		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportExtCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportExtCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
	
					$sql.=", ".$catTable.".".$attribField;
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en elFROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
	
					$arrayTables[$catTable] = $catTable;
	
					if($joinCat!="")
					{
						$joinCat.=" AND ";
					}
	
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}

		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		$where = $joinCat;
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"").$aSurvey->SurveyTable.".FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey;
		$sql .= " WHERE ".$where.$whereFilter;
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMReportExt::NewInstanceFromRS($anInstance->Repository, $aSurveyID, $aRS);
			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}

		return $anInstance;
	}
	
	function getStrAnswersSCMatrix($questionInstance, $factkeydimval, $factkey, $separator="; ")
	{
		$strAnswersSCMatrix = "";
		$LN = $separator;
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT A.DisplayText, B.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$factkey." AND B.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strDisplayText." = ".$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	function getOnlyAnswersSCMatrix($questionInstance, $factkeydimval, $factkey, $separator=";")
	{
		$strAnswersSCMatrix = "";
		$LN = $separator;
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT B.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$factkey." AND B.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	static function getStrCatDimValAnswers($aRepository, $surveyInstance, $questionInstance, $factkeydimval, $separator="; ", $onlyAnswer=false)
	{
		$strCatDimValAnswers = "";
		$LN = $separator;
		
		$tableCatDimVal = "SVSurveyCatDimVal_".$surveyInstance->ModelID;
		$tableDimension = "RIDIM_".$questionInstance->ElementDimID;
		$tableName = "SI_SV_QAnswers";

		$sql = "SELECT A.DisplayText, C.ResponseValue FROM (".$tableName." A INNER JOIN ".$tableDimension." B 
				ON A.QuestionID = ".$questionInstance->QuestionID." AND A.DisplayText = B.DSC_".$questionInstance->ElementDimID.") 
				LEFT JOIN ".$tableCatDimVal." C ON B.RIDIM_".$questionInstance->ElementDimID."KEY = C.ElemDimVal 
				AND A.QuestionID = C.QuestionID AND C.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";

		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableDimension.", ".$tableCatDimVal." ".translate("tables").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["responsevalue"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strCatDimValAnswers!="")
			{
				$strCatDimValAnswers.=$LN;
			}
			
			if(!$onlyAnswer)
			{
				$strCatDimValAnswers.=$strDisplayText." = ".$strAnswerText;
			}
			else 
			{
				$strCatDimValAnswers.=$strAnswerText;
			}

			$aRS->MoveNext();
		}
		
		return $strCatDimValAnswers;
	}
	
	//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
	//@JAPRWarning: Descontinuada
	/* Si se requería usar esta función, mejor se debería cargar una instancia de record pues dicha instancia además de contener ya el propio
	resultado de esta función, contendría todas las nuevas funcionalidades que se agregan y evitaría tener que generar funciones como esta para
	cargar por separado cada una de ellas (por ejemplo las propias secciones maestro-detalle)
	*/
	function getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal)
	{
		//Obtenemos coleccion de las preguntas de la seccion dinamica
		$dynQuestionCollection = $this->DynQuestionCollection;
		//Obtenemos la instancia de la seccion dinamica
		$dynSectionInstance = $this->DynamicSection;
		//Obtenemos la instancia de Survey		
		$aSurvey = $this->Survey;
		//Obtenemos la instancia del catalogo
		$catalogInstance = $this->Catalog;
		
		//Obtener la primera pregunta de seleccion simple de catalogo para obtener los valores del hijo del atributo 
		//de la seccion dinamica
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
				AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$dynSectionInstance->CatalogID." 
				ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		elseif ($aRS->EOF) {
			//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
			die("(".__METHOD__.") ".translate("There is an error in the form, a catalog simple choice question is needed to generate the dynamic section"));
		}
		
		$questionID = (int)$aRS->fields["questionid"];
		$factTable = "";
		$factTableJoin = "";
		if ($aSurvey->DissociateCatDimens) {
			$intCatMemberID = $dynSectionInstance->CatMemberID;
			if ($dynSectionInstance->ChildCatMemberID > 0) {
				$intCatMemberID = $dynSectionInstance->ChildCatMemberID;
			}
			
			$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $intCatMemberID);
			$factTable = $aSurvey->FactTable." C, ";
			$factTableJoin = "A.FactKey = C.FactKey AND ";
			$dimCatFieldID = "C.RIDIM_".$catMemberInstance->IndDimID."KEY";
			$dimCatFieldDsc = "B.DSC_".$catMemberInstance->IndDimID;
			$dimCatFieldJoin = "RIDIM_".$catMemberInstance->IndDimID."KEY";
			$dimCatTable = "RIDIM_".$catMemberInstance->IndDimID;
		}
		else {
			$dimCatFieldID = "A.dim_".$questionID;
			$dimCatFieldDsc = "B.DSC_".$this->ChildDynamicAttribID;
			$dimCatFieldJoin = "RIDIM_".$catalogInstance->ParentID."KEY";
			$dimCatTable = "RIDIM_".$catalogInstance->ParentID;
		}

		$sql = "SELECT A.FactKey, A.EntrySectionID, ".$dimCatFieldID." AS id, ".$dimCatFieldDsc." AS description ";
		foreach($dynQuestionCollection as $aQuestion)
		{
			//@JAPR 2013-01-07: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
					break;
				
				default:
					$sql .= ", dim_".$aQuestion->QuestionID;
					break;
			}
			//@JAPR
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseDynamicPageField) {
			$sql .= ", A.DynamicPageDSC";
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$factTable.$dimCatTable." B ";
		$sql .= " WHERE ".$factTableJoin.$dimCatFieldID." = B.".$dimCatFieldJoin." AND A.FactKeyDimVal = ".$factKeyDimVal;
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseStdSectionSingleRec) {
			$sql .= " AND A.EntrySectionID = ".$dynSectionInstance->SectionID;
		}
		//@JAPR
		$sql .= " ORDER BY A.FactKey ";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
		if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$aSectionID = $dynSectionInstance->SectionID;
		$count = 0;
		$arrayDynSectionValues = array();
		$arrayDynSectionValues[$aSectionID] = array();
		while(!$aRS->EOF)
		{
			$arrayDynSectionValues[$aSectionID][$count] = array();
			$arrayDynSectionValues[$aSectionID][$count]["id"] = (int)$aRS->fields["id"];
			$arrayDynSectionValues[$aSectionID][$count]["desc"] = $aRS->fields["description"];
			//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
			$intEntrySectionID = @$aRS->fields["entrysectionid"];
			$blnIsEmpty = ($intEntrySectionID != $aSectionID);
			$blnCheckRow = is_null($intEntrySectionID) || $intEntrySectionID == -1;
			
			foreach($dynQuestionCollection as $aQuestion)
			{
				$fieldName = "dim_".$aQuestion->QuestionID;
				$aFieldValue = @$aRS->fields[$fieldName];
				//Si es un registro de cuando aun no se grababan los EntryIDs, se debe verificar que no esté vacio pues de lo contrario
				//es muy probable que no sea un registro de esta sección
				if ($blnCheckRow) {
					if (!is_null($aFieldValue) && trim($aFieldValue) <> '') {
						$blnIsEmpty = false;
					}
				}
				
				if($aQuestion->QTypeID != qtpMulti || $aQuestion->MCInputType != mpcNumeric)
				{
					$arrayDynSectionValues[$aSectionID][$count][$fieldName] = $aFieldValue;
				}
				else 
				{
					//Si es de tipo Multiple Choice Numerica le realizamos un float
					$arrayDynSectionValues[$aSectionID][$count][$fieldName] = (float)$aFieldValue;
				}
			}
			
			//Sólo agrega el registro si no estaba vacio o si pertenece a la sección Maestro-Detalle analizada 
			if (!$blnIsEmpty) {
				$arrayDynSectionValues[$aSectionID][$count]["FactKey"] = (int) @$aRS->fields["factkey"];
				//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
				//Si se usa el registro único, entonces agrega el nuevo campo para indicar que si este es el registro a usar por cada página pues
				//sería el que contiene a las preguntas no repetibles
				$aDynamicPageDSC = (string) @$aRS->fields["dynamicpagedsc"];
				if ($this->Survey->UseDynamicPageField) {
					//@JAPR 2013-03-06: Corregido un bug, se estaba usando una propiedad de la instancia pero en esta función es un array local
					$arrayDynSectionValues[$count]["DynamicPageDSC"] = $aDynamicPageDSC;
				}
				//@JAPR
				
				$count++;
			}
			else {
				unset ($arrayDynSectionValues[$aSectionID][$count]);
			}
			$aRS->MoveNext();
		}
		
		return $arrayDynSectionValues;
	}
	
	function getCatalogKey()
	{
		$key=null;

		$fieldKey="RIDIM_".$this->CatParentID."KEY";

		$sql= "SELECT ".$fieldKey. " FROM ".$this->CatTableName;

		$where="";

		foreach ($this->FilterAttributes as $memberID=>$memberInfo)
		{
			if($this->AttribSelected[$memberID]!="sv_ALL_sv")
			{
				if($where!="")
				{
					$where.= " AND ";
				}
				$where.= " DSC_".$memberInfo["ParentID"]." = ".$this->Repository->DataADOConnection->Quote($this->AttribSelected[$memberID]);
			}
		}

		if($where!="")
		{
			$where=" WHERE ".$where;
		}

		$sql.= $where;

		$aRS = $this->Repository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->CatTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$key=array();

			while (!$aRS->EOF)
			{
				$key[] = $aRS->fields[strtolower($fieldKey)];
				$aRS->MoveNext();
			}
		}

		return $key;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;

			$sql = "SELECT SurveyID, SurveyName FROM SI_SV_Survey Order By SurveyName";

			$aRS = $aRepository->DataADOConnection->Execute($sql);

			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			if (!$aRS->EOF)
			{
				$aSurveyID = $aRS->fields["surveyid"];
			}
		}

		$attribSelected=null;
		
		if($aSurveyID!=0)
		{
			$questionID = BITAMReportExtCollection::getFirtsCatalogQuestion($aRepository, $aSurveyID);
	
			if($questionID!==null)
			{
				$firstCatQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $questionID);
				$catTableName = BITAMReportExtCollection::getCatTableName($aRepository, $firstCatQuestion->CatalogID);
				$filterAttributes = BITAMReportExtCollection::getAttributes($aRepository, $firstCatQuestion->CatalogID, $firstCatQuestion->CatMemberID);
				foreach ($filterAttributes as $memberID => $memberInfo)
				{
					$attribSelected[$memberID]= "";
	
					if (array_key_exists("AttribID_".$memberID, $aHTTPRequest->GET))
					{
						$attribSelected[$memberID] = $aHTTPRequest->GET["AttribID_".$memberID];
					}
					else
					{
						$attribSelected[$memberID]= "sv_ALL_sv";
					}
				}
			}
		}

    $startDate="";
		if (array_key_exists("startDate", $aHTTPRequest->GET))
		{
			if(trim($aHTTPRequest->GET["startDate"])!="" && substr($aHTTPRequest->GET["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->GET["startDate"]);
			}
		}

		$endDate="";
		if (array_key_exists("endDate", $aHTTPRequest->GET))
		{
			if(trim($aHTTPRequest->GET["endDate"])!="" && substr($aHTTPRequest->GET["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->GET["endDate"]);
			}
		}
		
		$userID=-2;
		if (array_key_exists("userID", $aHTTPRequest->GET))
		{
			$userID = (int)$aHTTPRequest->GET["userID"];
		}
		
		$ipp=100;
		if (array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$ipp = (int)$aHTTPRequest->GET["ipp"];
		}


		$callExportProcess = false;
		$actionValue = '';
		//Verificamos sino viene de una accion de exportacion
		if (array_key_exists("ReportAction", $aHTTPRequest->GET))
		{
			$actionValue = $aHTTPRequest->GET["ReportAction"];

			if($actionValue=="ExportCVS")
			{
				$callExportProcess = true;
			}
		}

		if(!$callExportProcess)
		{
			if($actionValue=="ZipReport")
			{
				return BITAMReportExtCollection::NewInstanceToCompress($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID);
			}
			elseif($actionValue=="PPTXReport")
			{
				return BITAMReportExtCollection::NewInstanceToPPTX($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID);
			}
			else
			{
				return BITAMReportExtCollection::NewInstance($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID, $ipp);
			}
		}
		else
		{
			//@JAPR 2012-01-11: Modificado el método de exportación para que cada columna de selección múltiple se divida en tantas columnas como
			//posibles respuestas existan
			return BITAMReportExtCollection::NewInstanceToExportDesg($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID);
			//return BITAMReportExtCollection::NewInstanceToExport($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID);
		}
	}

	function get_Parent()
	{
		return $this->Repository;
	}

	function get_Title()
	{
		return "Log: Data Captures";
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_SECTION=ReportCollection&SurveyID=".$this->SurveyID.$strParameters;
	}

	function get_ChildQueryString()
	{
		return "BITAM_SECTION=Report";
	}

	function get_AddRemoveQueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_PAGE=Report".$strParameters;
	}

	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ReportID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Task";
		$aField->Title = translate("Task");
		$aField->Type = "String";
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FactKeyDimVal";
		$aField->Title = translate("Entry Id");
		$aField->ToolTip = translate("Entry Id");
		$aField->Type = "Integer";
		$aField->FormatMask = "####";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailDesc";
		$aField->Title = translate("EMail");
		$aField->ToolTip = translate("EMail");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			$fieldType = $this->FieldTypes[$i];
			$toolTip = $this->QuestionText[$i];

			if(trim($this->Questions[$i]->AttributeName)!="")
			{
				$questionText = $this->Questions[$i]->AttributeName;
			}
			else
			{
				$questionText = $this->QuestionText[$i];
			}

			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $questionText;
			$aField->ToolTip = $toolTip;
			$aField->BeforeMessage = "getImages(".$this->Questions[$i]->QuestionID.")";

			if($fieldType==1)
			{
				$aField->Type = "Float";
				$aField->FormatMask = "#,##0.00";
			}
			else
			{
				$aField->Type = "String";
				$aField->Size = 255;
			}

			$myFields[$aField->Name] = $aField;
		}

		if($this->HasSignature==1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Signature";
			$aField->Title = translate("Signature");
			$aField->Type = "String";
			$aField->IsLink = false;
			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return true;
	}

	static function getCatTableName($aRepository, $aCatalogID)
	{
		$tableName = "";

		$sql= "SELECT TableName FROM SI_SV_Catalog WHERE CatalogID = ".$aCatalogID;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$tableName = trim($aRS->fields["tablename"]);
		}

		return $tableName;
	}

	static function getCatParentID($aRepository, $aCatalogID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetClaDescripWithCatalogID($aCatalogID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$parentID = "";

		$sql= "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$aCatalogID;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$parentID = (int) $aRS->fields["parentid"];
		}
		
		BITAMGlobalFormsInstance::AddClaDescripWithCatalogID($aCatalogID, $parentID);

		return $parentID;
	}

	static function getParentIDFromByCatMember($aRepository, $aCatalogID, $aMemberID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetClaDescripWithCatMemberID($aCatalogID, $aMemberID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$parentID = "";

		$sql= "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$aCatalogID. " AND MemberID = ".$aMemberID;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$parentID = (int) $aRS->fields["parentid"];
		}
		
		BITAMGlobalFormsInstance::AddClaDescripWithCatMemberID($aCatalogID, $aMemberID, $parentID);

		return $parentID;
	}

	static function getAttribValues($aRepository, $tableName, $fieldName, $filter="")
	{
		$values = array();

		$sql= "SELECT DISTINCT ".$fieldName." FROM ".$tableName.
		" WHERE ".$fieldName." <> ".$aRepository->DataADOConnection->Quote("*NA");

		if($filter!="")
		{
			$sql.=" ".$filter;
		}

		$sql.=" ORDER BY ".$fieldName;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$value = trim($aRS->fields[strtolower($fieldName)]);
			$values[$value]	= $value;
			$aRS->MoveNext();
		}

		return $values;
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	//La función no estaba definida como static pero siempre se utilizaba de esa manera
	static function getAttributes($aRepository, $aCatalogID, $aCatMemberID)
	{
		$attributes = array();

		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = "";
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', IndDimID';
		}
		//@JAPR
		$sql= "SELECT MemberID, MemberName, ParentID, MemberOrder $strAdditionalFields FROM SI_SV_CatalogMember WHERE CatalogID=".$aCatalogID.
		" AND MemberOrder <= (SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID=".$aCatalogID." AND MemberID=". $aCatMemberID.")".
		" ORDER BY MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$memberID = $aRS->fields["memberid"];
			$attributes[$memberID]["MemberID"] = $memberID;
			$attributes[$memberID]["MemberName"] = $aRS->fields["membername"];
			$attributes[$memberID]["ParentID"] = $aRS->fields["parentid"];
			$attributes[$memberID]["MemberOrder"] = $aRS->fields["memberorder"];
			$attributes[$memberID]["FieldName"] = "DSC_".$aRS->fields["parentid"];
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$attributes[$memberID]["IndDimID"] = (int) @$aRS->fields["inddimid"];
			//@JAPR
			
			$aRS->MoveNext();
		}

		return $attributes;
	}

	static function getFirtsCatalogQuestion($aRepository, $aSurveyID)
	{
		$questionID=null;
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_QUESTION WHERE UseCatalog=1 AND SurveyID = ". $aSurveyID. " AND QTypeID <> ".qtpOpenNumeric." ORDER BY QuestionNumber";

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QUESTION ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{	//Se considera sólo la primera pregunta encontrada que usa catálogo
			$questionID = (int) $aRS->fields["questionid"];
		}

		return $questionID;
	}

	function generateBeforeFormCode($aUser)
	{
		require_once('settingsvariable.inc.php');
		$strAltEformsService = '';
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ALTEFORMSSERVICE');
		if (!is_null($objSetting) && trim($objSetting->SettingValue != ''))
		{
			$strAltEformsService = $objSetting->SettingValue;
			if (substr($strAltEformsService, -1) != "/") {
				$strAltEformsService .= "/";
			}
			
			if (strtolower(substr($strAltEformsService, 0, 4)) != 'http') {
				$strAltEformsService = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$strAltEformsService;
			}
		}
		
?>		<script language="JavaScript">
 		
        function reportcollectionSendPDF() {
            var selected = 0;
            var toignore = 0;
            var elements = BITAMReportCollection_SelectForm.elements;
            for (var i = 0; i < elements.length; i++)
            {
                if (elements[i].name == "ReportID[]")
                {
                    if (elements[i].checked)
                    {
                        selected++;
                        if (elements[i].id == "0")
                        {
                            toignore++;
                        }
                    }
                }
            }
            if (selected == 0)
            {
            <?
                if(trim($this->MsgNoSelectedSendPDF)!="")
                {
?>
                alert('<?= str_replace("'", "\'", translate($this->MsgNoSelectedSendPDF)) ?>');
<?
                }
                else 
                {
?>
                alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to Send PDF"), "BITAM Report Send PDF")) ?>');
<?
                }
            ?>				

                return;
            }
            <?
                if(trim($this->MsgCancelSendPDF)!="")
                {
                    $strAllDelete = $this->MsgCancelSendPDF;
                }
                else 
                {
                    $strAllDelete = translate("No selected %s can be send");
                }			
            ?>
            if (selected == toignore)
            {
                alert('<?= str_replace("'", "\'", sprintf($strAllDelete, "BITAM Report Send PDF")) ?>');
                return;
            }
            <?
                if(trim($this->MsgCancelSendPDF)!="")
                {
                    $strSomeDelete = translate("Some %s can not be send")." ".translate("because")." ".$this->MsgCancelSendPDF.", ".translate("they will be deselected");
                }
                else 
                {
                    $strSomeDelete = translate("Some %s can not be sended, they will be deselected");
                }			
            ?>				
            if (toignore != 0)
            {
                alert('<?=str_replace("'", "\'", sprintf($strSomeDelete, "BITAM Report Send PDF")) ?>');
                var elements = BITAMReportCollection_SelectForm.elements;
                for (var i = 0; i < elements.length; i++)
                {
                    if (elements[i].name == "ReportID[]")
                    {
                        if (elements[i].id == "0")
                        {
                            elements[i].checked = false;
                        }
                    }
                }
            }

            <?
                if(trim($this->MsgConfirmRemove)!="")
                {
?>
            var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmSendPDF)) ?>');
<?
                }
                else 
                {
?>
            var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to Send PDF of all selected %s?"), "BITAM Report Send PDF")) ?>');
<?
                }
            ?>

            if (answer)
            {
                var elements = BITAMReportCollection_SelectForm.elements;
                var elementsSendPDF = new Array();
                var s = 0;
                for (var i = 0; i < elements.length; i++)
                {
                    if (elements[i].name == "ReportID[]")
                    {
                        //elements[i].disabled = false;
                        if (elements[i].checked)
                        {
                            selected++;
                            if (elements[i].id != "0")
                            {
                                elementsSendPDF[s] = elements[i].value;
                                s++;
                            }
                        }
                    }
                }
                var strReportIDs = elementsSendPDF.join();
                var inputReportIDs = document.getElementById("ReportSendPDFIDs");
                
                var objUserID = document.getElementById("UserID");
                frmSendPDF.userID.value = objUserID.value;
                
                inputReportIDs.value = strReportIDs;
                frmSendPDF.submit();
                //BITAMReportCollection_SelectForm.action = '<?= 'main.php?'.$this->get_AddRemoveQueryString() ?>';
                //BITAMReportCollection_SelectForm.submit();
            }
        }
        
        function editEntry(entryID)
 		{
 			var surveyID = <?=$this->SurveyID?>;

 			openWindow('<?=$strAltEformsService?>loadEntry.php?surveyID=' + surveyID + '&extp=1&EntryID=' + entryID);
 		}

		function openImg(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyImage.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openComment(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'commentWin', 'width=400, height=150, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function showSignature(surveyID, reportID)
		{
			var aWindow = window.open('getSurveySignature.php?surveyID='+surveyID+'&reportID='+reportID, 'signatureWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openGoogleMap(surveyID, startDate, endDate, userID, AttribIDs, AttribVals)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
			var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&startDate='+startDate+'&endDate='+endDate+'&userID='+userID+'&AttribIDs='+AttribIDs+'&AttribVals='+AttribVals, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}

		function applyFilterSurvey()
		{
			var objSurvey = document.getElementById("SurveyID");
			frmChangeFilter.SurveyID.value=objSurvey.value;
			frmChangeFilter.submit();
		}

		function applyFilterAttrib()
		{
<?
			foreach($this->FilterAttributes as $memberID => $memberInfo)
			{
?>
			var obj_<?=$memberID?> = document.getElementById("AttribID_<?=$memberID?>");
			frmAttribFilter.AttribID_<?=$memberID?>.value = obj_<?=$memberID?>.value;
<?
			}
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = objStartDate.value;

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = objEndDate.value;
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = objUserID.value;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = objIpp.value;

			frmAttribFilter.submit();
		}
		
		function clearFilter()
		{
<?
			$hasAttribFilter = false;
			//Revisamos todos los filtros de los catalogos si los hay
			if(!is_null($this->AttribSelected))
			{
				foreach ($this->AttribSelected as $idx=>$value)
				{
					if($value!="sv_ALL_sv")
					{
						$hasAttribFilter = true;
						break;
					}
				}
				
			}
			
			if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100 || $hasAttribFilter==true)
			{
				foreach($this->FilterAttributes as $memberID => $memberInfo)
				{
?>
			var obj_<?=$memberID?> = document.getElementById("AttribID_<?=$memberID?>");
			frmAttribFilter.AttribID_<?=$memberID?>.value = "sv_ALL_sv";
<?
				}
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
			else 
			{
?>
<?
			}
?>
		}
		
		var ctrlEditAudit;
		ctrlEditAudit = -1;
		
		function editAudit()
		{
			ctrlEditAudit = ctrlEditAudit*(-1);
			var obDivTitle = document.getElementById("collection_title_div");
			var objBtnBack = document.getElementById("btnBack");
			var objBtnEditLog = document.getElementById("btnEditLog");
			var objBtnAuditTrail = document.getElementById("btnAuditTrail");
			var objBtnDelEntries = document.getElementById("btnDelEntries");
            var objBtnSendPDFEntries = document.getElementById("btnSendPDFEntries");
			var objBtnMapData = document.getElementById("btnMapData");
			var arrayLinkEdit = document.getElementsByName("linkEdit");
			var arrayTrCollection = document.getElementsByName("tr_collection_checkbox");
			var i;
			
			if(ctrlEditAudit==1)
			{
				objBtnBack.style.display = 'inline';
				objBtnEditLog.style.display = 'none';
				objBtnMapData.style.display = 'none';
				objBtnAuditTrail.style.display = 'inline';
                objBtnDelEntries.style.display = 'inline';
                objBtnSendPDFEntries.style.display = 'inline';
				obDivTitle.innerHTML = 'Edit Log<hr>';
				
				if(arrayLinkEdit!=undefined && arrayLinkEdit!=null)
				{
    				for(i=0; i<arrayLinkEdit.length; i++)
    				{
    					objLinkEdit = arrayLinkEdit[i];
    					objLinkEdit.style.display = "inline";
    				}
				}

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
      				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = ((bIsIE)?"inline":"table-cell");
    				}
    			}
			}
			else
			{
				objBtnBack.style.display = 'none';
				objBtnEditLog.style.display = 'inline';
				objBtnMapData.style.display = 'inline';
				objBtnAuditTrail.style.display = 'none';
				objBtnDelEntries.style.display = 'none';
                bjBtnSendPDFEntries.style.display = 'none';
				obDivTitle.innerHTML = 'Log: Data Captures<hr>';

				if(arrayLinkEdit!=undefined && arrayLinkEdit!=null)
				{
    				for(i=0; i<arrayLinkEdit.length; i++)
    				{
    					objLinkEdit = arrayLinkEdit[i];
    					objLinkEdit.style.display = "none";
    				}
				}

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
    				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = 'none';
    				}
    			}		
			}
		}
		
		function extractNumber(obj)
		{
			//Obtener el valor del input text
			var temp = obj.value;

			//Crear nuestra expresion regular con digitos
			var reg0Str = '[0-9]*';

			//No se manejara signo negativo 
			//reg0Str = '^-?' + reg0Str;
			reg0Str = '^' + reg0Str;

			//Indicamos que nuestra expresion regular solo
			//aceptara digitos al final de la cadena
			reg0Str = reg0Str + '$';

			//Hacemos la instancia RegExp para que se cree
			//un objeto tipo expresion regular
			var reg0 = new RegExp(reg0Str);

			//Evaluamos nuestra cadena o valor que se
			//encuentra en nuestro input text
			//Si regresa true esto quiere decir
			//que hasta el momento si se ha cumplido
			//con la expresión regular
			if (reg0.test(temp)) return true;

			//Creamos una cadena con todos los caracteres permitidos
			//en nuestra entrada de texto, esto es digitos y el signo
			//negativo, el [^.....] indica que se hara una busqueda
			//para todos aquellos caracteres que no esten entre los
			//corchetes
			var reg1Str = '[^0-9' + ']';

			//Creamos nuestra expresión regular, pero se
			//le indica ahora, que es una expresión regular
			//con búsqueda globalpara todas las entradas de
			//texto que no cumplen con lo especificado en reg1Str
			var reg1 = new RegExp(reg1Str, 'g');
			//Se reemplaza dichos caracteres con vacío
			temp = temp.replace(reg1, '');

			//Se asigna nuevamente el valor a obj.value
			obj.value = temp;
		}
		
		function checkLimitNumber(obj)
		{
			// value is present
			var tval=Trim(obj.value);
			if (tval=='')
			{
				return true;
			}
			
			reg=/^0*/;
			tval=tval.replace(reg,'')

			if (tval!='')
			{
				val=parseInt(tval);
			}
			else
			{
				val=0;
			}

			var min=1;
			var max=1000;
			
			var msg="";

			if(min!='' && max!='')
			{
				msg='Max records should be in range of ' + min + ' to ' + max + '.' ;
			}
			else
			{
  				if(min!='')
  				{
  					msg='Input value should be greater than or equal to ' + min +'.';
  				}
				else
				{
    				if(max!='')
    				{
						msg='Input value should be less than or equal to ' + max + '.';
    				}
				}
			}

			if(min!='')
			{
				if (min>val)
				{
					alert(msg);
    				obj.value = 100;
				}
			}

			if (max!='')
			{
				if (val>max) 
				{
					alert(msg);
    				obj.value = 100;
				}
			}
		}

		function zipReport()
		{
			frmZipReport.submit();
		}
		
		function exportPPTX()
		{
			frmPPTXReport.submit();
		}
		
 		function exportReport()
 		{
			frmExportReport.submit();
 		}

 		//Exportar los datos del Reporte actual en GoogleMaps
 		function googleMap(surveyID)
 		{
 			var AttribIDs  = '';
 			var AttribVals = '';
<?
			foreach($this->FilterAttributes as $memberID => $memberInfo)
			{
?>
				var obj_<?=$memberID?> = document.getElementById("AttribID_<?=$memberID?>");
				AttribIDs +=<?=$memberID?>+'_AWSep_';
				AttribVals+=obj_<?=$memberID?>.value+'_AWSep_';
<?
			}
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			var objEndDate = document.getElementById("CaptureEndDate");
			var objUserID = document.getElementById("UserID");
 			openGoogleMap(surveyID, objStartDate.value, objEndDate.value, objUserID.value, AttribIDs, AttribVals);
 		}

		var filterAttributes = new Array;
<?
		$i=0;
		foreach ($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			filterAttributes[<?=$i?>]=<?=$memberID?>;
<?			$i++;
		}
?>
		var numFilterAttributes = <?=$this->NumFilterAttributes?>;

<?
		if(count($this->FilterAttributes)>0)
		{
?>
		function refreshAttributes(objAttrib)
		{
			if(objAttrib.value=='sv_ALL_sv')
			{
				for(i=0; i<numFilterAttributes; i++)
				{
					var obj = document.getElementById("AttribID_"+filterAttributes[i]);
					if(obj.getAttribute("memberOrder") > objAttrib.getAttribute("memberOrder"))
					{
				 		obj.length = 0;

						var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
						obj.options[obj.options.length] = newOption;
					}
				}
				return;
			}

			var xmlObj = getXMLObject();

			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}

			strMemberIDs="";
			strParentIDs="";
			strValues="";

			for(i=0; i<numFilterAttributes; i++)
			{
				var obj = document.getElementById("AttribID_"+filterAttributes[i]);
				if(obj.getAttribute("memberOrder") <= objAttrib.getAttribute("memberOrder"))
				{
					if(strMemberIDs!="")
					{
						strMemberIDs+="_SVSEPSV_";
					}
					strMemberIDs+=obj.getAttribute("memberID");

					if(strParentIDs!="")
					{
						strParentIDs+="_SVSEPSV_";
					}
					strParentIDs+=obj.getAttribute("parentID");

					if(strValues!="")
					{
						strValues+="_SVSEPSV_";
					}

					strValues+=obj.value;
				}
				else
				{
					break;
				}
			}

		 	xmlObj.open("POST", "getAttribValues.php?SurveyID=<?=$this->SurveyID?>&CatalogID=<?=$this->CatalogID?>&CatMemberID=<?=$this->FirstCatQuestion->CatMemberID?>&ChangedMemberID="+objAttrib.getAttribute("memberID")+"&strMemberIDs="+strMemberIDs+"&strParentIDs="+strParentIDs+"&strValues="+strValues, false);

		 	xmlObj.send(null);

		 	strResult = Trim(unescape(xmlObj.responseText));

		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();

		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;

			 		var obj = document.getElementById("AttribID_"+membID);

			 		obj.length = 0;

					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;


			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}

			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
		 	// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
			for(i=0; i<numFilterAttributes; i++)
			{
				var obj = document.getElementById("AttribID_"+filterAttributes[i]);
				if(obj.getAttribute("memberOrder") > objAttrib.getAttribute("memberOrder") && !inArray(refreshAttrib, obj.getAttribute("memberID")))
				{
			 		obj.length = 0;

					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
				}
			}
		}
<?
		}
?>
		</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	
    <form name="frmSendPDF" action="reportSendPDF.php" method="GET" target="_blank">
		<input type="hidden" name="ReportSendPDFIDs" id="ReportSendPDFIDs" value="">
        <input type="hidden" name="surveyID" id="surveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="userID" id="userID" value="">
	</form>
        
    <form name="frmChangeFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID">
	</form>

	<form name="frmAttribFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value=<?=$this->SurveyID?>>
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="">
<?
		}
?>
	</form>

	<form id="frmZipReport" name="frmZipReport" action="main.php" method="GET" target="_blank">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ZipReport">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
	
	<form id="frmPPTXReport" name="frmPPTXReport" action="main.php" method="GET" target="_blank">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="PPTXReport">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
	
	<form id="frmExportReport" name="frmExportReport" action="main.php" method="GET" target="_blank">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportCVS">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}

	function displayCheckBox($aUser)
	{
		return false;
	}
	
	function afterTitleCode($aUser)
	{
		$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		$numSurveys = count($surveyCollection->Collection);
		
		$numFilters = 1 + count($this->FilterAttributes) + 3;
		$numRows = ceil($numFilters/3);
		$countCell = 0;
		$maxCell = 4;
		//@JAPR 2012-04-04: Modificado el combo de encuestas para que aparezca mas grande y no corte nombres de encuestas largos
		$numColSpanForm = 4; // 2;
		//@JAPR
		$totalColSpanTable = 1 + ($maxCell*2);
		
		$hasAttribFilter = false;
		//Revisamos todos los filtros de los catalogos si los hay
		if(!is_null($this->AttribSelected))
		{
			foreach ($this->AttribSelected as $idx=>$value)
			{
				if($value!="sv_ALL_sv")
				{
					$hasAttribFilter = true;
					break;
				}
			}
			
		}
		
		if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100 || $hasAttribFilter==true)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = "Reapply";
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = "Apply";
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply Filters").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">Clear</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		$btnMapData='<span id="btnMapData" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="googleMap('.$this->SurveyID.');"><img src="images/map.gif">&nbsp;'.translate("Map data").'</span>';
		$btnZipData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="zipReport();"><img src="images/zip.gif">&nbsp;'.translate("Download images").'</span>';
		$btnPPTXData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="exportPPTX();"><img src="images/pptx.gif">&nbsp;'.translate("Download images").'</span>';
		$btnExportData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="exportReport();"><img src="images/download.gif">&nbsp;'.translate("Export data").'</span>';
		$btnEditAudit='<span id="btnEditLog" style="display:inline" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:editAudit();"><img src="images/edit.png">&nbsp;'.translate("Edit log").'</span>';
		$btnEditAudit.='<span id="btnAuditTrail" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:return false;"><img src="images/edit.png">&nbsp;'.translate("Audit trail").'</span>';
		
		$arrayButtons[0]=$btnMapData;
		$arrayButtons[1]=$btnZipData;
		// mostrar botón de pptx report???
		if (true) //(isset($_SESSION["PABITAM_RepositoryName"]) && $_SESSION["PABITAM_RepositoryName"] == "fbm_bmd_0375")
		{
		  $arrayButtons[2]=$btnPPTXData;
  		$arrayButtons[3]=$btnExportData;
  		$arrayButtons[4]=$btnEditAudit;
		}
		else 
		{
  		$arrayButtons[2]=$btnExportData;
  		$arrayButtons[3]=$btnEditAudit;
		}
		
		/*
		$arrayButtons[1]=$btnExportData;
		$arrayButtons[2]=$btnEditAudit;
		*/
		$btnDelEntries = '<span id="btnDelEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:BITAMReportCollection_Remove();"><img src="images/delete.png">&nbsp;'.translate("Delete entries").'</span>';
        $btnSendPDFEntries = '<span id="btnSendPDFEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript: reportcollectionSendPDF();"><img src="images/delete.png">&nbsp;'.translate("Send PDF entries").'</span>';
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons);
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo=
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

?>
	<span id="btnBack" style="display:none" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editAudit();"><img src="images/home.png">&nbsp;<?="Back"?></span>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
			<?="Form"?>&nbsp;
			</td>
			<td colspan="<?=$numColSpanForm?>">
				<select id="SurveyID" name="SurveyID" style="width:100%;font-size:11px" onchange="javascript:applyFilterSurvey();">
<?
			global $idxSelSurveyID;
			$idxSelSurveyID = 0;
			$cmbIndex = 0;

			for ($i=0; $i<$numSurveys; $i++)
			{
				$anSurveyID = $surveyCollection->Collection[$i]->SurveyID;
				$select="";
				if($this->SurveyID == $anSurveyID)
				{
					$idxSelSurveyID = $cmbIndex;
					$select = "selected";
				}
?>
					<option value="<?=$anSurveyID?>" <?=$select?>><?=($surveyCollection->Collection[$i]->SurveyName)?></option>
<?
				$cmbIndex++;
			}
?>
				</select>
			</td>
<?
			$countCell = 1;
			
			while(($countCell%$maxCell)!=0)
			{
?>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
				$countCell++;
			}
?>
		</tr>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		$arrayUsers[-2] = translate("All");
		$arrayUsers[-1] = "NA";
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}

		$sql.=" ORDER BY NOM_LARGO ";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;

			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		
		if((count($this->FilterAttributes)+1)>$countButtons)
		{
			$maxRows = count($this->FilterAttributes)+1;
		}
		else 
		{
			$maxRows = $countButtons;
		}

		$selected = "";
		if($this->SurveyID!=0)
		{
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?="Filters"?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser=>$nameUser)
		{
			$selected="";

			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
			$countCell=1;
		}
		
		$loadCat = true;
		$strAtribFilter = "";
		if(count($this->FilterAttributes)>0)
		{
			foreach($this->FilterAttributes as $memberID => $memberInfo)
			{
				$position++;
				$select="";

				if($position==$positionDates)
				{
					if($numRows==0)
					{
?>
			<td style="text-align:right;" class="styleBorderFilterTop">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
			</td>
			<td class="styleBorderFilterTop">
				<?=$strFrom?>
			</td>
<?
					}
					else if($numRows==1)
					{
?>
			<td style="text-align:right;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
			</td>
			<td>
				<?=$strTo?>
			</td>
<?
					}
					else if($numRows==2)
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}

?>
			<td style="text-align:right;" <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
			<td <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
			
				<?=$this->Pages->display_items_per_page_report()?>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					$classBorderTop = "";
					$classBorderRightTop = "";
					if($numRows==0)
					{
						$classBorderTop = ' class="styleBorderFilterTop"';
						$classBorderRightTop = ' class="styleBorderFilterRightTop"';
					}
					else 
					{
						$classBorderTop = '';
						$classBorderRightTop = ' class="styleBorderFilterRight"';
					}
					
					if(isset($arrayFilters[$numRows]))
					{
?>
			<td <?=$classBorderTop?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightTop?>>
				<?=$arrayFilters[$numRows]?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = "";
						$classBorderRightBottom = "";
						if($maxRows == ($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
							$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderBottom = '';
							$classBorderRightBottom = ' class="styleBorderFilterRight"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					if(isset($arrayButtons[$numRows]))
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td style="padding-top:5px;padding-bottom:5px;">
				<?=$arrayButtons[$numRows]?>
			</td>
<?
					}
					else 
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
					}
					
					$countCell++;
				}
				
				if(($countCell%$maxCell)==0)
				{
					$numRows++;
					$position = 1;
?>
		</tr>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
				else 
				{
					$position++;
				}
				
				$classBorderLeft = ' class="styleBorderFilterLeft"';
				$classBorderBottom = '';
				if($maxRows == ($numRows+1))
				{
					$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
					$classBorderBottom = ' class="styleBorderFilterBottom"';
				}

?>
			<td style="text-align:right;" <?=$classBorderLeft?>>
				&nbsp;&nbsp;&nbsp;<span><?=$memberInfo["MemberName"]?></span>
			</td>
			<td <?=$classBorderBottom?>>
				<select id="AttribID_<?=$memberID?>" name="AttribID_<?=$memberID?>" memberID="<?=$memberID?>" parentID="<?=$memberInfo["ParentID"]?>" memberOrder="<?=$memberInfo["MemberOrder"]?>" onchange="javascript:refreshAttributes(this)" style="width:150px; font-size:11px">
<?
				global $idxSelSurveyID;
				$idxSelSurveyID = 0;
				$cmbIndex = 0;

				if($loadCat==true)
				{

?>
					<option value="sv_ALL_sv" ><?=translate("All")?></option>
<?
					$attribValues = BITAMReportExtCollection::getAttribValues($this->Repository, $this->CatTableName, $memberInfo["FieldName"], $strAtribFilter);
					foreach ($attribValues as $key=>$desc)
					{
						$select="";

						if($this->AttribSelected[$memberID] == $key)
						{
							$idxSelSurveyID = $cmbIndex;
							$select = "selected";
						}

?>
						<option value="<?=$key?>" <?=$select?>><?=$desc?></option>
<?
						$cmbIndex++;
					}

					if($this->AttribSelected[$memberID]!='sv_ALL_sv')
					{
						$strAtribFilter.= " AND ".$memberInfo["FieldName"]." = ".$this->Repository->DataADOConnection->Quote($this->AttribSelected[$memberID]);
					}

				}
				else
				{
?>
					<option value="sv_ALL_sv" selected><?=translate("All")?></option>
<?
				}
?>
				</select>
			</td>
<?
				if($this->AttribSelected[$memberID]=='sv_ALL_sv')
				{
					$loadCat = false;
				}
				
				$countCell++;
			}
		}
		
		if($this->SurveyID!=0)
		{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
	?>
				<td style="text-align:right;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		}
?>
	</table>
<?
	}
	
	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="SurveyID" name="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
<?
	}
}
?>