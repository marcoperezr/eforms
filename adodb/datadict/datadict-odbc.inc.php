<?php

/**
  V4.61 24 Feb 2005  (c) 2000-2005 John Lim (jlim@natsoft.com.my). All rights reserved.
  Released under both BSD license and Lesser GPL library license. 
  Whenever there is any discrepancy between the two licenses, 
  the BSD license will take precedence.
	
  Set tabs to 4 for best viewing.
 
*/

// security - hide paths
if (!defined('ADODB_DIR')) die();

class ADODB2_odbc extends ADODB_DataDict {
	
	var $databaseType = 'odbc';
	var $seqField = false;
	
	function MetaType($t,$len=-1,$fieldobj=false)
	{
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
		    substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{
			if (is_object($t))
			{
				$fieldobj = $t;
				$t = $fieldobj->type;
				$len = $fieldobj->max_length;
			}
			
			$len = -1; // mysql max_length is not accurate
			switch (strtoupper($t))
			{
				case 'R':
				case 'INT': 
				case 'INTEGER': return  'I';
				case 'BIT':
				case 'TINYINT': return  'I1';
				case 'SMALLINT': return 'I2';
				case 'BIGINT':  return  'I8';
				
				case 'REAL':
				case 'FLOAT': return 'F';
				default: return parent::MetaType($t,$len,$fieldobj);
			}
		}
		else
		{
			return parent::MetaType($t,$len,$fieldobj);
		}
	}
	
 	function ActualType($meta)
	{
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT ACCESS')) == 'MICROSOFT ACCESS')
		{
			switch (strtoupper($meta)) 
			{
				case 'C': return 'CHAR';
				case 'XL':
				case 'X': return 'TEXT'; 
				
				case 'C2': return 'CHAR'; // up to 32K
				case 'X2': return 'TEXT';
				
				case 'B': return 'BINARY';
					
				case 'D': return 'DATETIME';
				case 'T': return 'DATETIME';
				
				case 'L': return 'BYTE';
				case 'I': return 'INTEGER';
				case 'I1': return 'BYTE';
				case 'I2': return 'SMALLINT';
				case 'I4': return 'INTEGER';
				case 'I8': return 'INTEGER';
				
				case 'F': return 'FLOAT';
				case 'N': return 'NUMERIC';
				default:
					return $meta;
			}
		}
		else if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
			 substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{
			switch (strtoupper($meta))
			{
		
				case 'C': return 'VARCHAR';
				case 'XL':
				case 'X': return 'TEXT';
				
				case 'C2': return 'NVARCHAR';
				case 'X2': return 'NTEXT';
				
				case 'B': return 'IMAGE';
					
				case 'D': return 'DATETIME';
				case 'T': return 'DATETIME';
				case 'L': return 'BIT';
				
				case 'R':		
				case 'I': return 'INT'; 
				case 'I1': return 'TINYINT';
				case 'I2': return 'SMALLINT';
				case 'I4': return 'INT';
				case 'I8': return 'BIGINT';
				
				case 'F': return 'FLOAT';
				case 'N': return 'NUMERIC';
				default:
					return $meta;
			}
		}
		else
		{
			switch(strtoupper($meta))
			{
				case 'C': return 'VARCHAR';
				case 'XL':
				case 'X': return 'VARCHAR(250)';
				
				case 'C2': return 'VARCHAR';
				case 'X2': return 'VARCHAR(250)';
				
				case 'B': return 'VARCHAR';
					
				case 'D': return 'DATE';
				case 'T': return 'DATE';
				
				case 'L': return 'DECIMAL(1)';
				case 'I': return 'DECIMAL(10)';
				case 'I1': return 'DECIMAL(3)';
				case 'I2': return 'DECIMAL(5)';
				case 'I4': return 'DECIMAL(10)';
				case 'I8': return 'DECIMAL(20)';
				
				case 'F': return 'DECIMAL(32,8)';
				case 'N': return 'DECIMAL';
				default:
					return $meta;
			}
		}
	}

	function AddColumnSQL($tabname, $flds)
	{
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT ACCESS')) == 'MICROSOFT ACCESS' ||
			substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
			substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{
			$tabname = $this->TableName ($tabname);
			$sql = array();
			$f = array();
			list($lines,$pkey) = $this->_GenFields($flds);
			$s = 'ALTER TABLE ' . $tabname . $this->addCol;
			foreach($lines as $v) 
			{
				$f[] = "\n".$v;
			}
			$s .= implode(', ',$f);
			$sql[] = $s;
			return $sql;
		}
		else
		{
			return parent::AddColumnSQL($tabname, $flds);
		}
	}
	
	function AlterColumnSQL($tabname, $flds)
	{
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT ACCESS')) == 'MICROSOFT ACCESS' ||
			substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
			substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{			
			return parent::AlterColumnSQL($tabname, $flds);
		}
		else
		{
			if ($this->debug) ADOConnection::outp("AlterColumnSQL not supported");
			return array();
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function _CreateSuffix($fname,$ftype,$fnotnull,$fdefault,$fautoinc,$fconstraint,$funsigned)
	{	
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
			substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{
			$suffix = '';
			if (strlen($fdefault)) $suffix .= " DEFAULT $fdefault";
			if ($fautoinc) $suffix .= ' IDENTITY(1,1)';
			if ($fnotnull) $suffix .= ' NOT NULL';
			else if ($suffix == '') $suffix .= ' NULL';
			if ($fconstraint) $suffix .= ' '.$fconstraint;
			return $suffix;
		}
		else
		{
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			return parent::_CreateSuffix($fname,$ftype,$fnotnull,$fdefault,$fautoinc,$fconstraint,$funsigned);
		}
	}
	
	function DropColumnSQL($tabname, $flds)
	{
		if (substr($this->connection->dbmsName(), 0, strlen('MICROSOFT ACCESS')) == 'MICROSOFT ACCESS' ||
			substr($this->connection->dbmsName(), 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
			substr($this->connection->dbmsName(), 0, strlen('SQL SERVER')) == 'SQL SERVER')
		{
			$tabname = $this->TableName ($tabname);
			if (!is_array($flds))
				$flds = explode(',',$flds);
			$f = array();
			$s = 'ALTER TABLE '.$tabname."\n".$this->dropCol;
			foreach($flds as $v) {
				$f[] = "\n".$this->NameQuote($v);
			}
			$s .= implode(', ',$f);
			$sql[] = $s;
			return $sql;
		}
		else
		{
			if ($this->debug) ADOConnection::outp("DropColumnSQL not supported");
			return array();
		}
	}
	
}

/*
//db2
 	function ActualType($meta)
	{
		switch($meta) {
		case 'C': return 'VARCHAR';
		case 'X': return 'VARCHAR'; 
		
		case 'C2': return 'VARCHAR'; // up to 32K
		case 'X2': return 'VARCHAR';
		
		case 'B': return 'BLOB';
			
		case 'D': return 'DATE';
		case 'T': return 'TIMESTAMP';
		
		case 'L': return 'SMALLINT';
		case 'I': return 'INTEGER';
		case 'I1': return 'SMALLINT';
		case 'I2': return 'SMALLINT';
		case 'I4': return 'INTEGER';
		case 'I8': return 'BIGINT';
		
		case 'F': return 'DOUBLE';
		case 'N': return 'DECIMAL';
		default:
			return $meta;
		}
	}
	
// ifx
function ActualType($meta)
	{
		switch($meta) {
		case 'C': return 'VARCHAR';// 255
		case 'X': return 'TEXT'; 
		
		case 'C2': return 'NVARCHAR';
		case 'X2': return 'TEXT';
		
		case 'B': return 'BLOB';
			
		case 'D': return 'DATE';
		case 'T': return 'DATETIME';
		
		case 'L': return 'SMALLINT';
		case 'I': return 'INTEGER';
		case 'I1': return 'SMALLINT';
		case 'I2': return 'SMALLINT';
		case 'I4': return 'INTEGER';
		case 'I8': return 'DECIMAL(20)';
		
		case 'F': return 'FLOAT';
		case 'N': return 'DECIMAL';
		default:
			return $meta;
		}
	}
*/
?>