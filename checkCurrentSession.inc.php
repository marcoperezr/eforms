<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
@session_start();

set_time_limit(0);
//@JAPR 2015-02-05: Agregado soporte para php 5.6
if (isset($_SESSION["PABITAM_RepositoryName"]) && isset($_SESSION["PABITAM_UserName"]))
{
	require_once("repository.inc.php");
	require_once("config.php");
	require_once("cubeClasses.php");
	require_once("appuser.inc.php");
	
	global $BITAMRepositories;

	$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
	
	if (!array_key_exists($theRepositoryName, $BITAMRepositories))
	{
		header("Location: index.php?error=1");
		exit();
	}
	
	$theRepository = $BITAMRepositories[$theRepositoryName];
	if (!$theRepository->open())
	{
		header("Location: index.php?error=2");
		exit();
	}	
	
	$theUserName = $_SESSION["PABITAM_UserName"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
	if (is_null($theUser))
	{
		header("Location: index.php?error=3");
		exit();
	}
	
	//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
	//Carga la seguridad del usuario logeado para mantenerla en memoria por si aplica en diferentes ventanas y/o procesos
	//Por el momento dado a que esta seguridad no puede ser modificada desde la interfaz, es factible sólo cargarla una vez y dejarla en la sesión permanentemente, sólo que se indexará
	//utilizando el ID del usuario para que en caso de mezcla de sesiones que no sea por medio de un login, al no encontrar la seguridad por lo menos intentará cargarla de nuevo
	//@JAPR 2016-08-23: Corregido el registro de seguridad por usuario, no estaba considerando al repositorio así que mismos usuarios de diferentes BDs habrían tenido conflicto (#KY40NF)
	if (!isset($_SESSION["PABITAM_Security"]) || !isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]]) || !isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$theUser->UserID])) {
		if (!isset($_SESSION["PABITAM_Security"])) {
			$_SESSION["PABITAM_Security"] = array();
		}
		
		if (!isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]])) {
			$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]] = array();
		}
		
		$arrSecurity = BITAMAppUser::GetUserSecurity($theRepository, $theUser->UserID);
		if (is_array($arrSecurity)) {
			$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$theUser->UserID] = $arrSecurity;
		}
	}
	//@JAPR
}
else
{
	//@JAPR 2015-10-07: Corregido un bug, faltaba incluir este archivo para tener acceso a las funciones y constantes que verifican si es GeoControl
	require_once('config.php');
	
	//Identificar si nos encontramos en modo FBM
	$fbmMode = (int)$_COOKIE["PAFBM_Mode"];
		
	if($fbmMode!=1)
	{
		if (array_key_exists("QUERY_STRING", $_SERVER))
		{
			$goto = base64_encode($_SERVER["PHP_SELF"]."?".$_SERVER["QUERY_STRING"]);
		}
		else
		{
			$goto = base64_encode($_SERVER["PHP_SELF"]);
		}
		header("Location: index.php?error=4&goto=".$goto);
	}
	else 
	{
		//@JAPR 2015-10-16: Modificada la redirección en caso de error para que dependa del tipo de servicio donde se encuentra
		RedirectSessionConnectionError();
		//@JAPR
	}
	
	exit();
}

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}
?>