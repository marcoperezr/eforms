﻿<?php
    //@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
    require_once("checkCurrentSession.inc.php");
	require_once("pushMsg.inc.php");
	require_once("utils.inc.php");
	
	$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
	$strScriptPath = $path_parts["dirname"];
		    
    /*EARS 2016-10-24: Extraemos los parámetros que se enviaron desde el archivo principal(pushMsg.inc.php) */
	$MessageID = getParamValue("MessageID", 'both', '(int)'); 
	    
	//EARS 2016-10-24: Se obtienen los datos correspondientes del detalle del mensaje
	$pushInstance = BITAMPushMsg::NewInstanceWithID($theRepository, $MessageID);	
	
	$strTitle = (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($pushInstance->Title)); //str_replace("\"", "\\\"", $anInstance->title);
	$strPushMessage = (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($pushInstance->PushMessage)); //str_replace("\"", "\\\"", $anInstance->pushmessage);
	
	//$strTitle = translate('Title') . ': ' . (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($pushInstance->Title)); //str_replace("\"", "\\\"", $anInstance->title);
	//$strPushMessage = translate('Message') . ': ' . (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($pushInstance->PushMessage)); //str_replace("\"", "\\\"", $anInstance->pushmessage);
		
	//PrintMultiArray($pushInstance);
	//die();
	/*
	print($strTitle);
	print('<br>');
	print($strPushMessage);
	die();
	*/
?>
<!-- AAL 29/06/2015: Se inicia a crear el HTML y los elementos que mostrara la página, pero antes
    en el encabezado se definen los estilos y el código JavaScrip emplado por los elemetos HTML -->
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html" charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
        <script src="js/codebase/dhtmlxFmtd.js"></script>
        <script type="text/javascript" src="js/json2.min.js"></script>
        <script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
        <style>
            /*AAL 25/02/2015: Definición de algunos estilos empleados en algunos elementos HTML*/
           div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
                text-transform: none !important;
            }
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
        </style>
	
		<!-- AAL 29/06/2015: Se inicia el apartado de texto javascript para validar los datos a mostrar en 
        cada uno de los elementos HTML -->
        <script type="text/javascript">
			//var dhxGridDataCell = "a";
			//var objReportLayout = undefined;
			//var objReportToolbar = undefined;
			//var objGrid = undefined;
			
			
        </script>
    </head>

    <!-- AAL 29/06/2015: Se inicia el apartado del cuerpo HTML (BODY) asi como los elementos que contendra la página -->
    <body>
        <!--<div id="title" style="width:100%; margin: 5px 0px 5px; text-align: left; color: #285768; font: bold 20px arial,sans-serif;"><?=translate('Title')?></div>-->
        <!--<div id="grdDetail" style="width:624px; height: 405px; overflow:hidden"></div>-->
		<div id="grdDetail" style="width:100%; height: 100%; overflow:hidden"></div>
        <script language="JavaScript">
<?
	//if ($surveyInstance->CreationAdminVersion >= esveFormsv6) {
		//Si es una forma de V6, el reporte se debe generar agrupando las secciones multi-registro en un subgrid, las estándar y los datos base si se generan en el grid principal
?>
			
			//JAPR 2015-08-13: Agregado el LayOut para permitir la toolbar y corregido el resize de la ventana
			/*objReportLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});
			objReportLayout.cells(dhxGridDataCell).hideHeader();
			*/
			/*
			var objFormDetail = [
				{type: "label", label: "<?=$strTitle?>", labelWidth:480},				
				{type: "label", label: "<?=$strPushMessage?>", labelWidth:480}
				//{type:"container", name:"grdDetail", label:"Detalle", inputWidth:480, inputHeight:180, className:"selection_container"}
			];

			var objForm = window.parent.objDialog.attachForm(objFormDetail);
			objForm.adjustParentSize();
			var dialogDimension = window.parent.objDialog.getDimension();
			window.parent.objDialog.setDimension(dialogDimension[0]+30, dialogDimension[1]+10);
			objForm.setItemFocus("TemplateForms");
			//objForm.hideItem('DestinationGroup');
			//objForm.hideItem('blockGroups');
			//objForm.getInput('Message').style.resize = 'auto';
			window.parent.objDialog.centerOnScreen();
			
			objForm.adjustParentSize();
			var dialogDimension = window.parent.objDialog.getDimension();
			window.parent.objDialog.setDimension(dialogDimension[0]+30, dialogDimension[1]+10);			
			*/
			
            var objGrid = new dhtmlXGridObject('grdDetail');
            objGrid.setHeader(" , , ");
            objGrid.setInitWidths("200");
            objGrid.setStyle("", "font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);font-family:Arial;font-size:9pt;");
            //objGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
            //Para los scrollbars del gridbox
            objGrid.objBox.style.overflowX = "auto";  
            objGrid.objBox.style.overflowY = "auto";
			objGrid.enableMultiselect(false);
			objGrid.enableMultiline(true);
            objGrid.init();
            objGrid.addRow(1, ["<?=translate('Title') . ': '?>", "<?= $strTitle ?>"]);
            objGrid.setCellTextStyle(1, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            objGrid.addRow(2, ["<?=translate('Message') . ': '?>", "<?= $strPushMessage ?>"]);
            objGrid.setCellTextStyle(2, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
            objGrid.addRow(3, ["<?=translate('User Name')?>", "<?=translate('Status')?>","<?=translate('Error')?>"]);
			objGrid.setCellTextStyle(3, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
			objGrid.setCellTextStyle(3, 1, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
			objGrid.setCellTextStyle(3, 2, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");
						
			/*
			var objGrid = new dhtmlXGridObject("grdDetail");
			objGrid.setImagePath("<?=$strScriptPath?>/images/");
			//objGrid.setHeader("<?=translate('User Name')?>,<?=translate('Status')?>",null,["text-align:left;","text-align:left;"]);
			objGrid.setHeader("<?=translate("User Name")?>");
			objGrid.setInitWidthsP("150");
			objGrid.setColAlign("left");
			objGrid.setColTypes("ro");
			objGrid.setColSorting("str");
			//objGrid.enableResizing("false");			
			//objGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
			objGrid.init();
			//objGrid.sortRows(0,"str","asc");
			*/
			
			/*
			var objGrid = objReportLayout.cells(dhxGridDataCell).attachGrid();

			objReportLayout.cells(dhxGridDataCell).hideHeader();
			objReportLayout.cells(dhxGridDataCell).fixSize(true, true); 
			
			objGrid.setHeader("<?=translate('User Name')?>,<?=translate('Status')?>",null,["text-align:left;","text-align:left;"]);
			objGrid.setColAlign("left,left");
			objGrid.setColTypes("ro,ro");
			objGrid.setColSorting("str,str");
			objGrid.setInitWidths("150,150");
			objGrid.enableResizing("false,false");
			objGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
			objGrid.init(); 
			*/
			var nRow = 4;
<?			
			foreach ($pushInstance->MessageDetail as $objPushMsg) {							
				$nom_largo = (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes(@$objPushMsg['NOM_LARGO'])); //@$objPushMsg['NOM_LARGO'];
				$estado = (string) @$objPushMsg['State'];
				$error = (string) str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes(@$objPushMsg['ErrorDesc']));//@$objPushMsg['ErrorDesc'];
				
?>							
				var rowvaluesP = [];
				
				rowvaluesP.push("<?=$nom_largo?>");
				rowvaluesP.push("<?=$estado?>");
				rowvaluesP.push("<?=$error?>");
				objGrid.addRow(nRow, rowvaluesP);
				objGrid.cellById(nRow,0).setAttribute("estatus",rowvaluesP);
				nRow++;
				//objGrid.setCellTextStyle(nRow++, 0, "background-color:wheat;font-family:Arial;font-weight:bold;font-size:10pt;");				
<?				
			}
			
?>				
			objGrid.selectCell(0,1);
			objGrid.editCell();
			objGrid.editStop();
			objGrid.setEditable(false);
        </script>
    </body>
</html>
