<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el SurveyID
$SurveyID = getParamValue('SurveyID', 'both', '(int)', true);
if (is_null($SurveyID)) {
	$SurveyID = -1;
}

//Obtener el QuestionID
$QuestionID = getParamValue('QuestionID', 'both', '(int)', true);
if (is_null($QuestionID)) {
	$QuestionID = -1;
}

//Obtener el AttributeName
$AttributeName = getParamValue('AttributeName', 'both', '(string)');

//Obtener el AttributeNameOld
$AttributeNameOld = getParamValue('AttributeNameOld', 'both', '(string)');

//Obtener el CatalogID
$CatalogID = getParamValue('CatalogID', 'both', '(int)');

//Obtener el CatalogIDOld
$CatalogIDOld = getParamValue('CatalogIDOld', 'both', '(int)');

//Obtener el GQTypeID
$GQTypeID = getParamValue('GQTypeID', 'both', '(int)', true);
if (is_null($GQTypeID)) {
	$GQTypeID = -1;
}

require_once("survey.inc.php");

$response = "ERROR";

//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
global $garrReservedDimNames;

//Se agregará entre las palabras reservadas el nombre de la forma con terminación ID, ya que hay una dimensión automática con ese nombre
if ($SurveyID > 0) {
	$surveyInstance = @BITAMSurvey::NewInstanceWithID($theRepository, $SurveyID);
	if (!is_null($surveyInstance)) {
		$garrReservedDimNames[strtolower($surveyInstance->SurveyName." id")] = 1;
	}
}

//@JAPR 2015-05-11: Corregido un bug, se permitía reutilizar el ShortName porque sólo se validaba a nivel de dimensión, así que ahora se validará directamente
//con el shortName para no repetirlo en la misma forma, ya que se utiliza como identificador en exportaciones entre otras cosas (#WD0ZBN)
if (trim($AttributeName) != '') {
	$blnError = false;
	$sql = "SELECT A.QuestionID 
		FROM SI_SV_Question A 
		WHERE A.SurveyID = {$SurveyID} AND A.AttributeName = ".$theRepository->DataADOConnection->Quote($AttributeName);
	if ($QuestionID > 0) {
		$sql .= " AND A.QuestionID <> {$QuestionID}";
	}

	$sql .= " 
		UNION 
		SELECT A.SectionID 
		FROM SI_SV_Section A 
		WHERE A.SurveyID = {$SurveyID} AND A.SectionType = ".sectInline." 
			AND A.SectionName = ".$theRepository->DataADOConnection->Quote($AttributeName);
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		//En este caso el nombre corto especificado ya es utilizado por alguna otra pregunta de esta forma, por lo que no se puede reutilizar
		$blnError = true;
		$strErrorMsg = translate("The name {var1} is already in use in this form, use another name please");
		$response = "ERROR<SVSEP>".str_ireplace('{var1}', $AttributeName, $strErrorMsg);
	}

	if ($blnError) {
		header('Content-Type: text/plain; charset=utf-8');
		echo $response;
		die();
	}
}
//@JAPR

//Verificamos si el GQTypeID es de tipo seleccion multiple entonces no hay problema con el AttributeName 
//ya que este tipo de pregunta no se refleja en la tabla de hechos
$blnCheckImgName = false;
$blnCheckDocName = false;
switch ($GQTypeID)
{
	//@JAPR 2012-05-15: Agregados nuevos tipos de pregunta Foto y Action
	//Este tipo de preguntas no tienen campo de respuesta, así que no importa el nombre del atributo
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	//case qtpPhoto:
	//case qtpDocument:
	//case qtpAudio:
	//case qtpVideo:
	case qtpSkipSection:
	case qtpMessage:
	case qtpMapped:
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	//case qtpSketch: 
	//Conchita 2014-09-09 agregado el tipo sketch
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	//case qtpSignature:
	//@JAPR 2013-04-12: Agregados nuevos tipos de pregunta Sincronización
	case qtpSync:
	//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
	case qtpPassword:
	//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
	case qtpSection:
	//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
	case qtpExit:
	//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
	case qtpUpdateDest:
		$response = "OK";
		break;
	
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	case qtpPhoto:
	case qtpSignature:
	case qtpSketch:
		$response = "OK";
		$blnCheckImgName = true;
		break;
		
	case qtpDocument:
	case qtpAudio:
	case qtpVideo:
		$response = "OK";
		$blnCheckDocName = true;
		break;
	
	//@JAPR 2012-05-15: Agregados nuevos tipos de pregunta Foto y Action
	//Por lo pronto la pregunta tipo Action se comportará como una Open Alfanumérica en esencia, aunque graba mas elementos en realidad
	case qtpAction:
	case qtpCalc:
	case qtpOpen:
	//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
	case qtpGPS:
	//@AAL 06/05/2015: Agregado para preguntas tipo Código de Barras
	case qtpBarCode:
		//Si se trata de un nuevo objeto se debe validar los nuevos nombres de atributo
		//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
		//Se unificó el código para mayor claridad, ya que lo único que cambiaba era el mensaje si era edición o creación
		$strErrorMsg = '';
		$strCatErrorMsg = '';
		//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
		$strReservedErrorMsg = '';
		//@JAPR
		if ($QuestionID==-1) {
			$strErrorMsg = translate("Can not add this question because there is another question with same short name");
			$strCatErrorMsg = translate("Can not add this question because there is a catalog or catalog attribute with same short name");
		}
		else {
			$strErrorMsg = translate("Can not edit this question because there is another question with same short name");
			$strCatErrorMsg = translate("Can not edit this question because there is a catalog or catalog attribute with same short name");
		}
		//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
		$strReservedErrorMsg = translate("{var1} is a reserved word, use another name please");
		//@JAPR
		
		//Si es una nueva pregunta, o bien si su nombre corto ha cambiado
		if($QuestionID==-1 || $AttributeName!=$AttributeNameOld)
		{
			//Verificamos en la tabla si_descrip_enc si el AttributeName ya se encuentra dado de alta
			//como nom_logico en dicha tabla
			$exist = BITAMSurvey::existAttribNameInThisSurvey($theRepository, $SurveyID, $AttributeName);
			
			//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
			//ya dado de alto como nom_logico en la tabla si_descrip_enc
			$existCatName = BITAMCatalog::existAttribNameInCatalogs($theRepository, $AttributeName);
			
			if(!$exist && !$existCatName)
			{
				$response = "OK";
			}
			else 
			{
				if($exist)
				{
					//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
					if ($exist === true) {
						$response = "ERROR<SVSEP>".$strErrorMsg;
					}
					else {
						global $garrReservedDimNames;
						$strReservedWord = (string) @array_keys($garrReservedDimNames)[(int) $exist -1];
						$strReservedErrorMsg = str_ireplace('{var1}', $strReservedWord, $strReservedErrorMsg);
						$response = "ERROR<SVSEP>".$strReservedErrorMsg;
					}
					//@JAPR
				}
				else 
				{
					$response = "ERROR<SVSEP>".$strCatErrorMsg;
				}
			}
		}
		else 
		{
			//En este caso no hay necesidad de verificar el nombre corto
			$response = "OK";
		}
		
		//@JAPR 2015-01-23: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
		switch ($GQTypeID) {
			case qtpGPS:
				break;
			//case qtpAction:
			//case qtpCalc:
			//case qtpOpen:
			default:
				$blnCheckImgName = true;
				break;
		}
		//@JAPR
		break;
		//Conchita agregado qtpCallList 2013-05-02
	case qtpCallList:	
	case qtpSingle:
		//Si no hay catalogo entonces si hay que buscar el AttributeName en la tabla si_descrip_enc
		//en el campo nom_logico
		if($CatalogID==0)
		{
			//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
			//Se unificó el código para mayor claridad, ya que lo único que cambiaba era el mensaje si era edición o creación
			$strErrorMsg = '';
			$strCatErrorMsg = '';
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			$strReservedErrorMsg = '';
			//@JAPR
			if ($QuestionID==-1) {
				$strErrorMsg = translate("Can not add this question because there is another question with same short name");
				$strCatErrorMsg = translate("Can not add this question because there is a catalog or catalog attribute with same short name");
			}
			else {
				$strErrorMsg = translate("Can not edit this question because there is another question with same short name");
				$strCatErrorMsg = translate("Can not edit this question because there is a catalog or catalog attribute with same short name");
			}
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			$strReservedErrorMsg = translate("{var1} is a reserved word, use another name please");
			//@JAPR
			
			//Si es una nueva pregunta, o bien si su nombre corto ha cambiado
			if($QuestionID==-1 || $AttributeName!=$AttributeNameOld)
			{
				$exist = BITAMSurvey::existAttribNameInThisSurvey($theRepository, $SurveyID, $AttributeName);

				//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
				//ya dado de alto como nom_logico en la tabla si_descrip_enc
				$existCatName = BITAMCatalog::existAttribNameInCatalogs($theRepository, $AttributeName);

				if(!$exist && !$existCatName)
				{
					$response = "OK";
				}
				else 
				{
					if($exist)
					{
						//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
						if ($exist === true) {
							$response = "ERROR<SVSEP>".$strErrorMsg;
						}
						else {
							global $garrReservedDimNames;
							$strReservedWord = (string) @array_keys($garrReservedDimNames)[(int) $exist -1];
							$strReservedErrorMsg = str_ireplace('{var1}', $strReservedWord, $strReservedErrorMsg);
							$response = "ERROR<SVSEP>".$strReservedErrorMsg;
						}
						//@JAPR
					}
					else 
					{
						$response = "ERROR<SVSEP>".$strCatErrorMsg;
					}
				}
			}
			else 
			{
				//En este caso no hay necesidad de verificar el nombre corto
				$response = "OK";
			}
		}
		else 
		{
			//Si se trata de un nuevo objeto se debe validar los catalogos seleccionados
			if($QuestionID==-1)
			{
				//Realizamos instancia de Survey para verificar cual es el catalogo 
				$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $SurveyID);
				
				//Vamos a permitir agregar varias preguntas con el mismo catalogo 
				//siempre y cuando haga referencia al catalogo de la encuesta,
				//en caso contrario se seguira realizando la misma validacion al momento
				if($CatalogID!=$surveyInstance->CatalogID)
				{
					//En este caso como se accesa a un catalogo entonces se debe verificar si no hay otra pregunta 
					//catalogo en dicha encuesta que use el mismo catalogo indicado
					//@JAPR 2013-02-19: Esta validación ya no aplica, ahora la única restricción es no usar mas de una vez el mismo atributo, así
					//como que preguntas posteriores solo usen atributos posteriores a los usados previamente en la misma encuesta
					//$exist = BITAMSurvey::existCatalogQuestionInThisSurvey($theRepository, $SurveyID, $CatalogID);
					$exist = false;
					if(!$exist)
					{
						$response = "OK";
					}
					else 
					{
						$response = "ERROR<SVSEP>Can not add this question because there is another question that uses the same catalog";
					}
				}
				else 
				{
					$response = "OK";
				}
			}
			else 
			{
				//Verificamos si hubo cambio en los catalogos utilizados
				if($CatalogID!=$CatalogIDOld)
				{
					//Realizamos instancia de Survey para verificar cual es el catalogo 
					$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $SurveyID);
					
					//Vamos a permitir agregar varias preguntas con el mismo catalogo 
					//siempre y cuando haga referencia al catalogo de la encuesta,
					//en caso contrario se seguira realizando la misma validacion al momento
					if($CatalogID!=$surveyInstance->CatalogID)
					{
						//En este caso como se accesa a un catalogo entonces se debe verificar si no hay otra pregunta 
						//catalogo en dicha encuesta que use el mismo catalogo indicado
						//@JAPR 2013-02-19: Esta validación ya no aplica, ahora la única restricción es no usar mas de una vez el mismo atributo, así
						//como que preguntas posteriores solo usen atributos posteriores a los usados previamente en la misma encuesta
						//$exist = BITAMSurvey::existCatalogQuestionInThisSurvey($theRepository, $SurveyID, $CatalogID);
						$exist = false;
						if(!$exist)
						{
							$response = "OK";
						}
						else 
						{
							$response = "ERROR<SVSEP>Can not edit this question because there is another question that uses the same catalog";
						}
					}
					else 
					{
						$response = "OK";
					}
				}
				else 
				{
					$response = "OK";
				}
			}
		}
		
		//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
		$blnCheckImgName = true;
		break;
	case qtpMulti:
		$response = "OK";
		//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
		$blnCheckImgName = true;
		break;
}

//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
if ($response == "OK" && $blnCheckImgName) {
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	//Se unificó el código para mayor claridad, ya que lo único que cambiaba era el mensaje si era edición o creación
	$strErrorMsg = '';
	$strCatErrorMsg = '';
	if ($QuestionID==-1) {
		$strErrorMsg = translate("Can not add this question because there is another question with same short name");
		$strCatErrorMsg = translate("Can not add this question because there is a catalog or catalog attribute with same short name");
	}
	else {
		$strErrorMsg = translate("Can not edit this question because there is another question with same short name");
		$strCatErrorMsg = translate("Can not edit this question because there is a catalog or catalog attribute with same short name");
	}
	$strDimType = " (".translate("Image").")";
	
	//Si es una nueva pregunta, o bien si su nombre corto ha cambiado
	if($QuestionID==-1 || $AttributeName!=$AttributeNameOld)
	{
		//Verificamos en la tabla si_descrip_enc si el AttributeName ya se encuentra dado de alta
		//como nom_logico en dicha tabla
		$exist = BITAMSurvey::existAttribNameInThisSurvey($theRepository, $SurveyID, "IMG ".$AttributeName);
		
		//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
		//ya dado de alto como nom_logico en la tabla si_descrip_enc
		$existCatName = BITAMCatalog::existAttribNameInCatalogs($theRepository, "IMG ".$AttributeName);
		
		if(!$exist && !$existCatName)
		{
			$response = "OK";
		}
		else 
		{
			if($exist)
			{
				$response = "ERROR<SVSEP>".$strErrorMsg.$strDimType;
			}
			else 
			{
				$response = "ERROR<SVSEP>".$strCatErrorMsg.$strDimType;
			}
		}
	}
}

if ($response == "OK" && $blnCheckDocName) {
	//@JAPR 2015-01-26: Corregido un bug, no se estaban validando los nombres de dimensión imagen ni documento para algunas preguntas (#WD0ZBN)
	//Se unificó el código para mayor claridad, ya que lo único que cambiaba era el mensaje si era edición o creación
	$strErrorMsg = '';
	$strCatErrorMsg = '';
	if ($QuestionID==-1) {
		$strErrorMsg = translate("Can not add this question because there is another question with same short name");
		$strCatErrorMsg = translate("Can not add this question because there is a catalog or catalog attribute with same short name");
	}
	else {
		$strErrorMsg = translate("Can not edit this question because there is another question with same short name");
		$strCatErrorMsg = translate("Can not edit this question because there is a catalog or catalog attribute with same short name");
	}
	$strDimType = " (".translate("Document").")";
	
	//Si es una nueva pregunta, o bien si su nombre corto ha cambiado
	if($QuestionID==-1 || $AttributeName!=$AttributeNameOld)
	{
		//Verificamos en la tabla si_descrip_enc si el AttributeName ya se encuentra dado de alta
		//como nom_logico en dicha tabla
		$exist = BITAMSurvey::existAttribNameInThisSurvey($theRepository, $SurveyID, "DOC ".$AttributeName);
		
		//Verificamos en las tablas de catalogos y atributos si el AttributeName no se encuentra
		//ya dado de alto como nom_logico en la tabla si_descrip_enc
		$existCatName = BITAMCatalog::existAttribNameInCatalogs($theRepository, "DOC ".$AttributeName);
		
		if(!$exist && !$existCatName)
		{
			$response = "OK";
		}
		else 
		{
			if($exist)
			{
				$response = "ERROR<SVSEP>".$strErrorMsg.$strDimType;
			}
			else 
			{
				$response = "ERROR<SVSEP>".$strCatErrorMsg.$strDimType;
			}
		}
	}
}
//@JAPR

header('Content-Type: text/plain; charset=utf-8');
echo $response;
?>