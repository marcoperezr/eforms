<?php
require_once('WKPDF.class.php');

/**
    $anINstance = contiene los elementos que se vana apintar en la exportacion
*/
function exportSurveyToPDFF($anInstance,$download = false,$aRepository)
{       

  /*PrintMultiArray($anInstance);
  exit();*/
  $userName = $anInstance->UserName;
  $dateID = $anInstance->DateID;
  $questions = $anInstance->Questions;
  $formName = $anInstance->SurveyName;
  $data = "";
  //Datos por default de la Survey
  $data .= CreateTr(translate("Report id"), $anInstance->ReportID,"","");
  $data .= CreateTr(translate("User Name"),$anInstance->UserName,"","");
  $data .= CreateTr(translate("Survey"),$anInstance->SurveyName,"","");
  $data .= CreateTr(translate("Start date"),$anInstance->StartDate,"","");
  $data .= CreateTr(translate("Start time"),$anInstance->StartTime,"","");
  $data .= CreateTr(translate("End date"),$anInstance->EndDate,"","");
  $data .= CreateTr(translate("End time"),$anInstance->EndTime,"","");
  $data .= CreateTr(translate("Latitude"),$anInstance->Latitude,"","");
  $data .= CreateTr(translate("Longitude"),$anInstance->Longitude,"","");
  $data .= CreateTr(translate("Sync date"),$anInstance->SyncDate,"","");
  $data .= CreateTr(translate("Sync time"),$anInstance->SyncTime,"","");
  $data .= CreateTr(translate("Sync latitude"),$anInstance->SyncLatitude,"","");
  $data .= CreateTr(translate("Sync longitude"),$anInstance->SyncLongitude,"","");
  $data .= CreateTr(translate("GPS country"),$anInstance->Country,"","");
  $data .= CreateTr(translate("GPS state"),$anInstance->State,"","");
  $data .= CreateTr(translate("GPS city"),$anInstance->City,"","");
  $data .= CreateTr(translate("GPS zip code"),$anInstance->ZipCode,"","");
  $data .= CreateTr(translate("GPS address"),$anInstance->Address,"","");
  if ($anInstance->bHasTransferData)
  {
	  $data .= CreateTr(translate("Transfer Time (min)"),(double) sprintf('%1.2f', @$anInstance->TransferTime),"","");
	  $data .= CreateTr(translate("Transfer Distance (mts)"),(double) sprintf('%1.2f', @$anInstance->TransferDistance),"","");
  }

  foreach ($questions as $key => $value) 
  { 
	if($questions[$key]->QTypeID==qtpUpdateDest || $questions[$key]->QTypeID==qtpExit 
	|| $questions[$key]->QTypeID==qtpSection || $questions[$key]->QTypeID==qtpSync
	|| $questions[$key]->QTypeID==qtpSkipSection || $questions[$key]->QTypeID==qtpMessage || ($questions[$key]->QTypeID==qtpOCR && !$questions[$key]->SavePhoto))
	{
		continue;
	}
  
	$QuestionText = $questions[$key]->getAttributeName();
	/*@ears 2016-12-08 se usan las funciones de cadena multibyte para evitar la aparición de caracteres especiales en la exportación*/
	$ShortText = (mb_strlen($QuestionText,'UTF-8') >= 24) ? mb_substr($QuestionText, 0, 24,'UTF-8') . "..." : $QuestionText;
    $surveyField = $questions[$key]->SurveyField;   // dim_NNNN (Numero para enlazar con la pregunta)
    $value =  $anInstance->$surveyField;            //valor de captura del campo.
    $valueF = $value["value"];
    $image = $value["image"];
    $document = $value["document"];
    //$comment = $value["comment"];
    if ($questions[$key]->QTypeID != qtpBarCode) {
    $image = ResolveImage($image);
    }
    $document = ResolveDocument($document);

    if(is_array($valueF))
      continue;

    $data .= CreateTr($ShortText,$valueF,$image,$document);

  }
  /**
  Aqui empiensan a pintarse los detalles
  */
  $data2 = "";
  $dataDetail = "";

  $objSectionColl = BITAMSectionCollection::NewInstance($aRepository, $anInstance->SurveyID);
  foreach ($objSectionColl->Collection as $objSection) {
    if ($objSection->SectionType != sectMasterDet && $objSection->SectionType != sectInline) {
      continue;
    }
    $intSectionID = $objSection->SectionID;
    $objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $intSectionID);
    foreach ($objQuestionColl->Collection as $objQuestion) 
    {
      if ($objQuestion->SectionID != $intSectionID) {
        continue;
      }
    }
    $intRowCount = (int) @$anInstance->SectRowsCount[$intSectionID];
    $headerHTML = "<tr class='trDetail'>";
    $detailHTML = "<tr class='trDetail'>";
    $buildHeader = true;
    for ($intRowIndex = 0; $intRowIndex < $intRowCount; $intRowIndex++) {
      $strValues = "";
      $strAnd = "";
      foreach ($objQuestionColl->Collection as $objQuestion) {
        if ($objQuestion->SectionID != $intSectionID) {
          continue;
        }
        $intQuestionID = $objQuestion->QuestionID;
        $strPropName = $objQuestion->SurveyField;
        $QuestionText = $objQuestion->getAttributeName();
		/*@ears 2016-12-08 se usan las funciones de cadena multibyte para evitar la aparición de caracteres especiales en la exportación*/
        $ShortText = (mb_strlen($QuestionText,'UTF-8') >= 24) ? mb_substr($QuestionText, 0, 24,'UTF-8') . "..." : $QuestionText;
        $strValue = (string) @$anInstance->{$strPropName}["value"][$intRowIndex];
        $strImage = (string) @$anInstance->{$strPropName}["image"][$intRowIndex];
        $strComment = (string) @$anInstance->{$strPropName}["comment"][$intRowIndex];
        $strDocument = (string) @$anInstance->{$strPropName}["document"][$intRowIndex];
		//@ears 2016-12-19 mostraba la frase Array = al exportar la información #A9EZGY
		$strValue = str_replace("Array =","", $strValue);
        $valid = true;
		
        switch ($objQuestion->QTypeID) {
            case qtpPhoto:
            case qtpDocument:
            case qtpSignature:
            case qtpSketch:
            //@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
            case qtpSketchPlus:
            //@JAPR
            case qtpSkipSection:
            case qtpMessage:
            case qtpSync:
            case qtpPassword:
            case qtpAudio:
            case qtpVideo:
            case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
                $valid = false;
                break;
        }
        if($valid)
        {
            if ( $objSection->SectionType == sectInline && isset($anInstance->{$strPropName}["sectionDesc"]))
            {
                if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0 && isset($anInstance->{$strPropName}["sectionDescAttributesNames"]) && isset($anInstance->{$strPropName}["sectionDescAttributesValues"]))
                {
                    foreach ($anInstance->{$strPropName}["sectionDescAttributesNames"][0] as $anAttributeName)
                    {
                        if ($buildHeader) 
                        {
                            $headerHTML .= "<td id = questionText  class= questionTextDetail >{$anAttributeName}</td>";
                        }
                        //$detailHTML .= "<td>{$strValue}</td>";
                    }
                    foreach ($anInstance->{$strPropName}["sectionDescAttributesValues"][$intRowIndex] as $anAttributeValue)
                        $detailHTML .= "<td>{$anAttributeValue}</td>";
                }
                else
                {
                    $namesectiondesc = translate('SectionDesc');
                    $strValueSectionDesc = (string) @$anInstance->{$strPropName}["sectionDesc"][$intRowIndex];
                    if ($buildHeader) 
                    {
                        $headerHTML .= "<td id = questionText  class= questionTextDetail >{$namesectiondesc}</td>";
                    }
                    $detailHTML .= "<td>{$strValueSectionDesc}</td>";	
                }
            }
            if ($buildHeader) {
              $headerHTML .= "<td id = questionText  class= questionTextDetail >{$ShortText}</td>";
            }
            $detailHTML .= "<td>{$strValue}</td>";
        }
        if (($objQuestion->HasReqPhoto > 0 && $objQuestion->QTypeID != qtpBarCode) || ($objQuestion->QTypeID == qtpOCR && $objQuestion->SavePhoto > 0)){		
            $strImage = ResolveImage($strImage);
            if ($buildHeader) {				
				if ($strImage == ""){
					$headerHTML .="";						
				}
				else {
					$headerHTML .= "<td id = questionText  class= questionTextDetail >".$ShortText."_".translate("Photo")."</td>";
				}				
            }
			
			if ($strImage == ""){
				$detailHTML .= "";			
			}
			else {
				$detailHTML .= "<td>{$strImage}</td>";
			}	
        }
        if ($objQuestion->HasReqComment > 0) {
             if ($buildHeader) {
                $headerHTML .= "<td id = questionText  class= questionTextDetail >".$ShortText."_".translate("Comment")."</td>";
            }
            $detailHTML .= "<td>{$strComment}</td>";
        }
        if ($objQuestion->HasReqDocument > 0) {
            $strDocument = ResolveDocument($strDocument);
            if ($buildHeader) {
                $headerHTML .= "<td id = questionText  class= questionTextDetail >".$ShortText."_".translate("Document")."</td>";
            }
            $detailHTML .= "<td>{$strDocument}</td>";
        }
      }
      $buildHeader = false;
      $headerHTML .= "</tr>";
      $detailHTML .= "</tr>";

    }
    $headerHTML .= $detailHTML;
    $data2 = $headerHTML;
    $dataDetail .= "<h2 id = formName >".$objSection->SectionName."</h2>";
    $dataDetail .= "<table class='tableDetail'>";
    $dataDetail .= $data2;
    $dataDetail .= "</table>";

  }
  $stream = "<head>";
  $stream .= "<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>";
  $stream .= "<link type='text/css' href='".getcwd()."/css/stylePDF.css' rel='stylesheet'/>";
  $stream .= "</head>";
  $stream .= "<body>";
  $stream .= "<h2 id = formName >".$formName."</h2>";
  $stream .= "<table class='table'>";
  $stream .= $data;
  $stream .= "</table>";
  $stream .= $dataDetail;
  $stream .= "</body>";
  // se pintan los campos del survey		
  //echo $stream;
  //exit();
  //empesamos con los detalles

  //@JAPR 2019-05-27: Corregido un bug, no estaba aplicando el nombre recibido as� que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
  //servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
  //$pdfName = "KPIOnline eSurvey".date("YmdHis"); 
  $strDatabaseNumber = str_ireplace("fbm_bmd_", "", $aRepository->RepositoryName);
  $pdfName = "KPISurveyPDF_{$strDatabaseNumber}_{$anInstance->UserID}_{$anInstance->SurveyID}_".formatDateTime($dateID, "yyyymmddhhmmss");
  //@JAPR
  $pdf = new WKPDF();
  //@JAPR 2019-07-09: Corregido un bug, no estaba aplicando el nombre recibido as� que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
  //servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
   //Corregido un bug, esta funci�n deber� utilizar el mismo nombre recibido al invocar a la generaci�n, en lugar de generar un nombre aleatorio como antes
  $pdf->set_time($pdfName);
  //@JAPR
  $pdf->set_html($stream);
  $pdf->set_page_size("letter");
  $pdf->set_title($pdfName);
  $pdf->render();
  $tnombre = new DateTime();
  //@JAPR 2019-05-27: Corregido un bug, no estaba aplicando el nombre recibido as� que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
  //servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
  //$name = $pdfName.$tnombre->getTimestamp().".pdf";
  $name = $pdfName.".pdf";
  //@JAPR
  if($download)
  {
    $pdf->output(WKPDF::$PDF_DOWNLOAD,$name);
  }else
  {   
    $link = array();
    $link["file"] = $pdf->output(WKPDF::$PDF_LINK,$name);
    $link["name"] = $name;
    /*var_dump($link);
    exit();*/
    return $link;
  }
}
function CreateTr($questionText,$valueF,$image,$document)
{
  $data = "<tr>";
  $data .= "<td id = questionText >{$questionText}</td>";
  $data .= "<td id = value >{$valueF}</td>";
  $data .= "<td id = image >{$image}</td>";
  $data .= "<td id = document >{$document}</td>";
  $data .= "</tr>";

  return $data;
}
function ResolveImage($image) {
    if($image == "" || $image == "NA" || is_array($image) || is_null($image))
    {
        //@JAPR 2015-08-11: Modificado para no mostrar etiqueta en caso de no existir este valor
        $image = ""; 	//translate("No image captured");	//"No se capturó imagen";
    }else
    {
       $image =  getcwd()."/surveyimages/".$_SESSION["PABITAM_RepositoryName"]."/".$image;
        //var_dump($image);
        $image = "<img style=\"width:200px;height:200px\" src=\"$image\"/>";
    }
    return $image;
}
function ResolveDocument($document) {
    if($document== "" || $document == "NA" || is_array($document))
    {
		//@JAPR 2015-08-11: Modificado para no mostrar etiqueta en caso de no existir este valor
		$document = '';//translate("No document attached");	//"No hay documento";
    }else
    {
        $documentName = $document;

        $documentx = explode("_", $document,7);
        if(count($documentx)>2)
        {
          $documentName = $documentx[6];
        }
		//var_dump($documentName);
		$document =  getcwd()."/images/document.gif";
		$document = "<img style=\"width:20px;height:20px\" src=\"$document\"/>{$documentName}";
    }
    return $document;
}
?>