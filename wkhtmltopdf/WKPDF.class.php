<?php
global $WKGLOBALS;
//@JAPR 2015-08-12: Corregido un bug, se estaba haciendo un str_replace en lugar de str_ireplace, así que si se entraba por una ruta URL que NO era exactamente como existe en ruta
//de directorio, no hacía el reemplazo, quedaba con la ruta completa y funcionaba, cuando si se entraba como debía si reemplazaba, quedaba como ruta relativa y fallaba, así que se optó
//por corregir el bug pero terminar dejando la ruta absoluta, ya que el resto del proceso se adaptó para usarlo de esa manera
//$WKGLOBALS['WKPDF_BASE_PATH']		=	str_ireplace(str_replace('\\','/',getcwd().'/'),'',dirname(str_replace('\\','/',__FILE__))).'/';
$WKGLOBALS['WKPDF_BASE_PATH']		=	dirname(str_replace('\\','/',__FILE__)).'/';
//@JAPR 2019-09-19: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//$WKGLOBALS['WKPDF_BASE_PATH_TMP']	=	$WKGLOBALS['WKPDF_BASE_PATH'];
$WKGLOBALS['WKPDF_BASE_PATH_TMP']	=	GeteFormsLogPath()."wkhtmltopdf";
//@JAPR
$WKGLOBALS['WKPDF_BASE_SITE']		=	'http://'.$_SERVER['SERVER_NAME'].'/artus/genvi/';

class WKPDF
{
    /**
     * Private use variables.
     */
    private $urlhtml='';
    private $html='';
    private $cmd='';
    private $tmp='';
    private $pdf='';
    private $status='';
    private $orient='Portrait';
    private $size='A4';
    private $page_height = '';
    private $page_width  = '';
    private $toc=false;
    private $copies=1;
    private $grayscale=false;
    private $title='';
    private $bMargin = false;
    private $header = '';
    private $footer = '';
    private $time = '';
    private static $cpu='';
    /**
     * Advanced execution routine.
     * @param string $cmd The command to execute.
     * @param string $input Any input not in arguments.
     * @return array An array of execution data; stdout, stderr and return "error" code.
     */
    private static function _pipeExec($cmd,$input='')
    {   
//		echo "<pre>";
//		var_dump($cmd);
//		exit();
        $proc=proc_open($cmd,array(0=>array('pipe','r'),1=>array('pipe','w'),2=>array('pipe','w')),$pipes);
//        fwrite($pipes[0],$input);
//        fclose($pipes[0]);
//        $stdout=stream_get_contents($pipes[1]);
//        fclose($pipes[1]);
        $stderr=stream_get_contents($pipes[2]);
//        fclose($pipes[2]);
//        $rtn=proc_close($proc);
//	var_dump($stderr);
//        exit();
//        return array(
////            'stdout'=>$stdout,
//            'stderr'=>$stderr,
////            'return'=>$rtn
//        );
    }
    /**
     * Function that attempts to return the kind of CPU.
     * @return string CPU kind ('amd64' or 'i386').
     */
    private static function _getCPU()
    {
        if(self::$cpu==''){
            if(`grep -i amd /proc/cpuinfo`!='')                     self::$cpu='amd64';
            elseif(`grep -i intel /proc/cpuinfo`!='')       self::$cpu='i386';
            else throw new Exception('WKPDF couldn\'t determine CPU ("'.`grep -i vendor_id /proc/cpuinfo`.'").');
        }
        return self::$cpu;
    }
    /**
     * Force the client to download PDF file when finish() is called.
     */
    public static $PDF_DOWNLOAD='D';
    /**
     * Returns the PDF file as a string when finish() is called.
     */
    public static $PDF_ASSTRING='S';
    /**
     * When possible, force the client to embed PDF file when finish() is called.
     */
    public static $PDF_EMBEDDED='I';
    /**
     * PDF file is saved into the server space when finish() is called. The path is returned.
     */
    public static $PDF_SAVEFILE='F';
    /**
     * PDF retorna el link donde esta el archivo.
     */
    public static $PDF_LINK='L';
    /**
     * PDF generated as landscape (vertical).
     */
    public static $PDF_PORTRAIT='Portrait';
    /**
     * PDF generated as landscape (horizontal).
     */
    public static $PDF_LANDSCAPE='Landscape';
    /**
     * Constructor: initialize command line and reserve temporary file.
     */
    public function __construct()
    {
    	global $WKGLOBALS;
        $this->cmd = $WKGLOBALS['WKPDF_BASE_PATH'].'wkhtmltopdf';
        if (!file_exists($this->cmd.'.exe')) throw new Exception('WKPDF static executable "'.htmlspecialchars($this->cmd,ENT_QUOTES).'" was not found.');

        do {
			$this->tmp=$WKGLOBALS['WKPDF_BASE_PATH_TMP'].'tmp/'.mt_rand().'.html';
        } while(file_exists($this->tmp));
    }
    /**
     * Set orientation, use constants from this class.
     * By default orientation is portrait.
     * @param string $mode Use constants from this class.
     */
    public function set_orientation($mode){
		$this->orient=$mode;
    }
    /**
     * Set page/paper size.
     * By default page size is A4.
     * @param string $size Formal paper size (eg; A4, letter...)
     */
    public function set_page_size($size){
		$this->size=$size;
    }

    /**
     * Set page/paper size width.
     * Page width (default unit millimeter)
     * @param integer $page_width (default unit millimeter)
     */
    public function set_page_width($pw){
		$this->page_width=$pw;
    }

    /**
     * Set page/paper size height.
     * Page height (default unit millimeter)
     * @param integer $page_height (default unit millimeter)
     */
    public function set_page_height($ph){
		$this->page_height=$ph;
    }

    /**
     * Whether to automatically generate a TOC (table of contents) or not.
     * By default TOC is disabled.
     * @param boolean $enabled True use TOC, false disable TOC.
     */
    public function set_toc($enabled){
		$this->toc=$enabled;
    }
    /**
     * Set the number of copies to be printed.
     * By default it is one.
     * @param integer $count Number of page copies.
     */
    public function set_copies($count){
		$this->copies=$count;
    }
    /**
     * Whether to print in grayscale or not.
     * By default it is OFF.
     * @param boolean True to print in grayscale, false in full color.
     */
    public function set_grayscale($mode){
		$this->grayscale=$mode;
    }
    /**
     * Set PDF title. If empty, HTML <title> of first document is used.
     * By default it is empty.
     * @param string Title text.
     */
    public function set_title($text){
		$this->title=$text;
    }
    /**
     * Set html content.
     * @param string $html New html content. It *replaces* any previous content.
     */
    public function set_html($html){
		global $WKGLOBALS;
		
        $this->html=$html;
		try {
			//@JAPR 2015-08-11: Corregido un bug, faltaba validar contra directorios ya existentes
			@mkdir($WKGLOBALS['WKPDF_BASE_PATH_TMP'].'tmp/', null, true);
			file_put_contents($this->tmp,$html);
		} catch(Exception $e) {
		}
		$this->set_url_html($this->tmp);
    }
    /**
     * Set url of html content.
     * @param string $html New URL html content.
     */
    public function set_url_html($url_html){
        $this->url_html = $url_html;
    }
    /**
     * Returns WKPDF print status.
     * @return string WPDF print status.
     */
    public function get_status(){
		return $this->status;
    }
    /**
     * Attempts to return the library's full help.
     * @return string WKHTMLTOPDF HTML help.
     */
    public function get_help()
    {
        $tmp = self::_pipeExec('"'.$this->cmd.'" --extended-help');
        return $tmp['stdout'];
    }
    /**
     * Returns WKPDF print url html.
     * @return string WPDF print url html.
     */
    public function get_url_html(){
		return $this->url_html;
    }
    
    //@JAPR 2019-07-09: Corregido un bug, no estaba aplicando el nombre recibido así que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
    //servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
    //Corregido un bug, esta función deberá utilizar el mismo nombre recibido al invocar a la generación, en lugar de generar un nombre aleatorio como antes
    public function set_time($name = ''){
        return $this->time = ($name)?$name:time();
        //return $this->time = time();
    }
    
    public function set_header($header){
		$this->header=$header;
    }
    
    public function set_footer($footer){
		$this->footer=$footer;
    }
    /**
     * Convert HTML to PDF.
     */
    public function render()
    {   
        global $WKGLOBALS;
        $ruta = $WKGLOBALS['WKPDF_BASE_PATH_TMP']."tmp";
        //modificaciones para eforms
		//GCRUZ 2015-08-05. Validar si la ruta del html viene como ruta relativa o path completo
		$fullPath = str_replace("\\", '/', dirname(__DIR__));
		$htmlPath = $this->get_url_html();
		if (strpos($htmlPath, $fullPath) === false)
		{
			//ruta relativa
			$webArray = explode('/', $htmlPath);
			$htmlFile = $webArray[count($webArray)-1];
			$web = "file:///".$ruta.'/'.$htmlFile;
			$webPDF = $ruta.'/'.$this->time.'.pdf';
		}
		else
		{
			//ruta completa
			$web = "file:///".$htmlPath;
			$webPDF = $ruta.'/'.$this->time.'.pdf';
		}
/*
        $web = $this->get_url_html();
        $webArray = explode("/", $web);
        $web = dirname(__DIR__)."/".$ruta."/".$webArray[2];
        $webArray = explode("/", $web);
        array_pop ($webArray);
        $webArray[] = $this->time.'.pdf';
        $webPDF = implode("/", $webArray);

        $web = "file:///".$web;   //ruta donde busca el html
*/
        $this->cmd = dirname(__DIR__)."/wkhtmltopdf/wkhtmltopdf";
//		$web = str_replace("\\", "/", $web);
//		$webPDF = str_replace("\\", "/", $webPDF);
        $this->cmd = str_replace("\\", "/", $this->cmd);
        $this->pdf = self::_pipeExec(
                '"'.$this->cmd.'.exe"'
                .(($this->copies>1)?' --copies '.$this->copies:'')          // number of copies
                .' --orientation '.$this->orient                            // orientation
                .' --margin-bottom '."9"                            // orientation
                .' --margin-top '."28"                            // orientation
                .( ($this->page_height != '' && $this->page_width != '')
                	? ' --page-height '.$this->page_height.' --page-width '.$this->page_width
                	: ' --page-size '.$this->size                           // page size
                 )
                .($this->toc?' --toc':'')                                   // table of contents
                .($this->grayscale?' --grayscale':'')                       // grayscale
//                .(($this->title!='')?' --title "'.$this->title.'"':'')      // title
                .($this->header!=''?'  --header-html '.$this->header.' ':'')
                .($this->footer!=''?'  --footer-html '.$this->footer:'')
//                .($this->footer!=''?'  --footer-right Page [page] of [toPage] ':'')
                //.(!$this->bMargin ? ' --margin-bottom 0'.' --margin-left 0'.
                //				   ' --margin-right 0' .' --margin-top 0'
                //				 : '')
//                .' '.$web.' -'	
                //.' '.$web.' "'.$this->title.'.pdf" '
//                .' '.$web.' test.pdf'											// URL and optional to write to STDOUT
                .' '.$web.' '.$webPDF												// URL and optional to write to STDOUT
        );
/*
var_dump('"'.$this->cmd.'"'
.(($this->copies>1)?' --copies '.$this->copies:'')          // number of copies
.' --orientation '.$this->orient                            // orientation
.( ($this->page_height != '' && $this->page_width != '')
	? ' --page-height '.$this->page_height.' --page-width '.$this->page_width
	: ' --page-size '.$this->size                           // page size
 )
.($this->toc?' --toc':'')                                   // table of contents
.($this->grayscale?' --grayscale':'')                       // grayscale
.(($this->title!='')?' --title "'.$this->title.'"':'')      // title
.' '.$web.' -'												// URL and optional to write to STDOUT
);
*/
        if (isset($this->pdf['stdout']) && $this->pdf['stdout'] == '')
        {       
            var_dump("si entro");
            exit();
        	if (strpos(strtolower($this->pdf['stderr']),'error') !== false)
        	{
        		throw new Exception('WKPDF system error: <pre>'.$this->pdf['stderr'].'</pre>');
        	}
        	elseif (((int)$this->pdf['return'])>1)
	        {
	        	throw new Exception('WKPDF shell error, return code '.(int)$this->pdf['return'].'.');
	        }
	        else
	        {
	        	throw new Exception('WKPDF didn\'t return any data. <pre>'.$this->pdf['stderr'].'</pre>');
	        }
        }

//        $this->status=$this->pdf['stderr'];
//        $this->pdf=$this->pdf['stdout'];
        //@unlink($this->tmp);
    }
    /**
     * Return PDF with various options.
     * @param string $mode How two output (constants from this same class).
     * @param string $file The PDF's filename (the usage depends on $mode.
     * @return string|boolean Depending on $mode, this may be success (boolean) or PDF (string).
     */
    public function output($mode,$file)
    {
        switch($mode)
        {
            case self::$PDF_DOWNLOAD:
                if(!headers_sent())
                {   
                    global $WKGLOBALS;
                    $enlace = $WKGLOBALS['WKPDF_BASE_PATH_TMP'].$this->time.".pdf";
                    header('Content-Description: File Transfer');
                    header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
                    header('Pragma: public');
                    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                    // force download dialog
                    header('Content-Type: application/force-download');
                    header('Content-Type: application/octet-stream', false);
                    header('Content-Type: application/download', false);
                    header('Content-Type: application/pdf', false);
                    // use the Content-Disposition header to supply a recommended filename
                    header('Content-Disposition: attachment; filename="'.basename($file).'";');
                    header('Content-Transfer-Encoding: binary');
                    $enlaceDownload = $WKGLOBALS['WKPDF_BASE_PATH_TMP']."tmp/".$this->time.".pdf";
                    header('Content-Length: '.filesize($enlaceDownload));
                    readfile($enlaceDownload);
                    @unlink($enlaceDownload);
                } else {
					throw new Exception('WKPDF download headers were already sent.');
                }
                break;
            case self::$PDF_ASSTRING:
                return $this->pdf;
                break;
            case self::$PDF_EMBEDDED:
                if(!headers_sent())
                {
                    header('Content-Type: application/pdf');
                    header('Cache-Control: public, must-revalidate, max-age=0'); // HTTP/1.1
                    header('Pragma: public');
                    header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                    header('Content-Length: '.strlen($this->pdf));
                    header('Content-Disposition: inline; filename="'.basename($file).'";');
                    echo $this->pdf;
                } else {
                    throw new Exception('WKPDF embed headers were already sent.');
                }
                break;
            case self::$PDF_SAVEFILE:
                return file_put_contents($file,$this->pdf);
                break;
            case self::$PDF_LINK:
                global $WKGLOBALS;
                //@JAPR 2019-05-27: Corregido un bug, no estaba aplicando el nombre recibido así que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
                //servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
                $enlace = $WKGLOBALS['WKPDF_BASE_PATH_TMP']."tmp/".$file;
                //$enlace = $WKGLOBALS['WKPDF_BASE_PATH_TMP']."tmp/".$this->time.".pdf";
                //@JAPR
                return $enlace;
                break;
            default:
				throw new Exception('WKPDF invalid mode "'.htmlspecialchars($mode,ENT_QUOTES).'".');
        }
        return false;
    }
}
?>