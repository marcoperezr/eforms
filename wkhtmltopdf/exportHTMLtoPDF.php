<?php
global $WKGLOBALS;
require_once('WKPDF.class.php');
	$HTMLString = (isset($_POST['HTMLString']))?$_POST['HTMLString']:'';
	$pdf = new WKPDF();
	$pdf->set_time();
	$pdf->set_html($HTMLString);
	$pdf->set_page_size("letter");
	$pdfName = "KPIOnlineESurvey".date("YmdHis");
	$pdf->set_title($pdfName);
	$pdf->render();
	$tnombre = new DateTime();
	//$name = $WKGLOBALS['WKPDF_BASE_SITE'].'eSurveyV602JP/wkhtmltopdf/temp/'.$pdfName.".pdf";
	$pdf->output(WKPDF::$PDF_DOWNLOAD,$pdfName.".pdf");
	// $link = array();
	// $link["file"] = $pdf->output(WKPDF::$PDF_LINK,$pdfName);
	// $link["name"] = $pdfName;
	// return $link;
?>