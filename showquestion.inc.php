<?php 
require_once("repository.inc.php");
require_once("initialize.php");
require_once("question.inc.php");
require_once("questionoption.inc.php");

class BITAMShowQuestion extends BITAMObject
{
	public $QuestionID;
	public $ConsecutiveID;
	public $QuestionOptionName;
	public $ShowID;
	public $ShowQuestionID;
	public $ShowQuestionName;
	//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
	public $ShowQuestionSectionID;
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	public $QuestionNumber;
	public $QuestionText;
	//@JAPR
	
	//14Marzo2013: Agregar propiedad SurveyID
	public $SurveyID;		
	public $ForceNew;
			
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
	function __construct($aRepository, $aConsecutiveID, $bSkipSurveyID = false)
	{
		BITAMObject::__construct($aRepository);
		
		$this->ConsecutiveID = $aConsecutiveID;
		$this->ShowID = -1;
		$this->ShowQuestionID = 0;
		//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
		$this->ShowQuestionSectionID = 0;
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$this->QuestionNumber = 0;
		$this->QuestionText = '';
		//@JAPR
		
		$sql = 'SELECT QuestionID, DisplayText FROM SI_SV_QAnswers WHERE ConsecutiveID = '.$this->ConsecutiveID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS)
		{
			if (!$aRS->EOF)
			{
				$this->QuestionID = (int) $aRS->fields['questionid'];
				$this->QuestionOptionName = $aRS->fields['displaytext'];
			}
		}
		
		unset($aRS);
		
		//14Marzo2013: Agregar propiedad SurveyID
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
		if ( !$bSkipSurveyID ) {
			$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			$this->SurveyID = (int) @$aQuestion->SurveyID;
		}
		
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		$this->HasPath = false;
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->ForceNew = false;
	}

	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
	static function NewInstance($aRepository, $aConsecutiveID, $bSkipSurveyID = false)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
		return new $strCalledClass($aRepository, $aConsecutiveID, $bSkipSurveyID);
	}

	static function NewInstanceWithID($aRepository, $aShowID)
	{
		$anInstance = null;
		
		if (((int) $aShowID) <= 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
		$sql = "SELECT t1.ConsecutiveID, t1.ShowID, t1.ShowQuestionID, t2.QuestionNumber, t2.QuestionText, t2.SectionID 
				FROM SI_SV_ShowQuestions t1, SI_SV_Question t2 
				WHERE t1.ShowQuestionID = t2.QuestionID AND t1.ShowID = ".$aShowID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
	static function NewInstanceFromRS($aRepository, $aRS, $bSkipSurveyID = false)
	{
		$aConsecutiveID = (int)$aRS->fields["consecutiveid"];
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
		$anInstance = $strCalledClass::NewInstance($aRepository, $aConsecutiveID, $bSkipSurveyID);
		//@JAPR

		$anInstance->ShowID = (int)$aRS->fields["showid"];
		$anInstance->ShowQuestionID = (int)$aRS->fields["showquestionid"];
		$anInstance->ShowQuestionName = $aRS->fields["questionnumber"].". ".$aRS->fields["questiontext"];
		//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
		$anInstance->ShowQuestionSectionID = (int) @$aRS->fields["sectionid"];
		//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$anInstance->QuestionNumber = (int) @$aRS->fields["questionnumber"];
		$anInstance->QuestionText = (string) @$aRS->fields["questiontext"];
		//@JAPR

		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("ConsecutiveID", $aHTTPRequest->GET))
		{
			$aConsecutiveID = (int) $aHTTPRequest->GET["ConsecutiveID"];
		}
		else
		{
			$aConsecutiveID = 0;
		}

		if (array_key_exists("ShowID", $aHTTPRequest->POST))
		{
			$aShowID = $aHTTPRequest->POST["ShowID"];
			
			if (is_array($aShowID))
			{
				$aCollection = BITAMShowQuestionCollection::NewInstance($aRepository, $aConsecutiveID, $aShowID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}

				$anInstance = BITAMShowQuestion::NewInstance($aRepository, $aConsecutiveID);

				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aShowID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aConsecutiveID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();

				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}

				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aConsecutiveID);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("ShowID", $aHTTPRequest->GET))
		{
			$aShowID = $aHTTPRequest->GET["ShowID"];
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aShowID);

			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aConsecutiveID);
			}
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aConsecutiveID);
		}

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("ShowID", $anArray))
		{
			$this->ShowID = (int)$anArray["ShowID"];
		}
		
		if (array_key_exists("ConsecutiveID", $anArray))
		{
			$this->ConsecutiveID = (int)$anArray["ConsecutiveID"];
		}
		
		if (array_key_exists("ShowQuestionID", $anArray))
		{
			$this->ShowQuestionID = (int)$anArray["ShowQuestionID"];
		}

		return $this;
	}
	
	function save()
	{
		if ($this->isNewObject())
		{
			//Obtener el ShowID Maximo
	    	$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(ShowID)", "0")." + 1 AS ShowID".
						" FROM SI_SV_ShowQuestions";

			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			
			if (!$aRS || $aRS->EOF)
			{
				die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$this->ShowID = (int)$aRS->fields["showid"];
			
			$sql = "INSERT INTO SI_SV_ShowQuestions (".
					"ShowID".
					",QuestionID".
					",ConsecutiveID".
					",ShowQuestionID".
					") VALUES (".
					$this->ShowID.
					",".$this->QuestionID.
					",".$this->ConsecutiveID.
					",".$this->ShowQuestionID.
					")";

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_SV_ShowQuestions SET 
					ShowQuestionID = ".$this->ShowQuestionID." 
					WHERE ConsecutiveID = ".$this->ConsecutiveID." AND ShowID = ".$this->ShowID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//14Marzo2013: Que se modifique el VersionNum
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);		
		
		return $this;
	}
	
	function remove()
	{
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE ConsecutiveID = ".$this->ConsecutiveID." AND ShowID = ".$this->ShowID;

		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//14Marzo2013: Que se modifique el VersionNum
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);		

		return $this;
	}

	function isNewObject()
	{
		return ($this->ShowID <= 0 || $this->ForceNew);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("If options is")." \"".$this->QuestionOptionName."\"";
		}
		else
		{
			return translate("If options is")." \"".$this->QuestionOptionName."\"";
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=ShowQuestion&ConsecutiveID=".$this->ConsecutiveID;
		}
		else
		{
			return "BITAM_PAGE=ShowQuestion&ConsecutiveID=".$this->ConsecutiveID."&ShowID=".$this->ShowID;
		}
	}

	function get_Parent()
	{
		return BITAMQuestionOption::NewInstanceWithID($this->Repository, $this->ConsecutiveID);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ShowID';
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ShowQuestionSectionID";
		$aField->Title = translate("Section");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		if ($this->isNewObject())
		{
			$aField->Options = $this->getSectionsWithNonHiddenQuestions();
			if (count($aField->Options) > 0)
			{
				foreach ($aField->Options as $this->ShowQuestionSectionID => $aSectionName)
				{
					break;
				}
				$this->ShowQuestionSectionID;
			}
		}
		else 
		{
			$aField->Options = array($this->ShowQuestionSectionID => 'Section');
		}
		$myFields[$aField->Name] = $aField;
		$sectionField = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ShowQuestionID";
		$aField->Title = translate("Show Question");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		if ($this->isNewObject())
		{
			$aField->Options = $this->getNonHiddenQuestionsBySection();
		}
		else 
		{
			$aField->Options = array($this->ShowQuestionSectionID => array($this->ShowQuestionID => $this->ShowQuestionName));
		}
		$myFields[$aField->Name] = $aField;
		
		$aField->Parent = $sectionField;
		$sectionField->Children[] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
	//@JAPREliminar: Esta función ya no se utiliza, originalmente el ocultar preguntas sólo aplicaba dentro de la misma sección
	function getNextQuestions()
	{
		$arrayNextQuestions = array();
		
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$questionNumber = $aQuestion->QuestionNumber;
		$surveyID = $aQuestion->SurveyID;
		$sectionID = $aQuestion->SectionID;
		
		$sql = "SELECT QuestionID, QuestionNumber, QuestionText FROM SI_SV_Question 
				WHERE SurveyID = ".$surveyID." AND SectionID = ".$sectionID." 
				AND QuestionNumber > ".$questionNumber;
		
		if($this->isNewObject())
		{	
			$sql.=	" AND QuestionID NOT IN
					(
						SELECT ShowQuestionID FROM SI_SV_ShowQuestions WHERE ConsecutiveID = ".$this->ConsecutiveID." 
					)";
		}
		
		$sql.=	" ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while(!$aRS->EOF)
		{
			$questionid = (int)$aRS->fields["questionid"];
			$questiontext = $aRS->fields["questionnumber"].". ".$aRS->fields["questiontext"];
			$arrayNextQuestions[$questionid] = $questiontext;

			$aRS->MoveNext();
		}

		return $arrayNextQuestions;
	}

	//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
	//Obtiene la lista secciones que contienen por lo menos una pregunta que aun no se han ocultado con esta opción de respuesta
	function getSectionsWithNonHiddenQuestions()
	{
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$intSurveyID = (int) @$aQuestion->SurveyID;
		$intQuestionNumber = (int) @$aQuestion->QuestionNumber;
		//@JAPR 2013-08-23: Corregido un bug, si la pregunta es de una sección dinámica o Maestro-detalle, sólo se pueden mostrar/ocultar preguntas
		//de la propia sección, porque sería ilógico que algunos registros de secciones múltiples decidieran el mismo estado de una pregunta fija
		//de una sección estándar posterior, ya que se podrían contraponer y alguno estaría "mal"
		$strFilter = '';
		//@JAPR 2014-08-26: Corregido un bug, se estaba reasignando el valor de SectionType en lugar de compararlo
		if ($aQuestion->SectionType == sectDynamic || $aQuestion->SectionType == sectMasterDet || $aQuestion->SectionType == sectInline || $aQuestion->SectionType == sectRecap) {
			$strFilter = 'AND A.SectionID = '.$aQuestion->SectionID;
		}

		//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
		$arrInvalidSections = array();
		if (getMDVersion() >= esvSectionQuestions) {
			$arrInvalidSections = @BITAMSection::GetMappedSections($this->Repository, $intSurveyID, false);
			$arrInvalidSections = array_keys($arrInvalidSections);
		}
		//@JAPR
		
		$sql = "SELECT DISTINCT A.SectionID, A.SectionName 
			FROM SI_SV_Section A, SI_SV_Question B 
			WHERE A.SectionID = B.SectionID AND A.SurveyID = ".$intSurveyID." AND B.QuestionNumber > ".$intQuestionNumber." AND 
				B.QuestionID NOT IN (
					SELECT ShowQuestionID FROM SI_SV_ShowQuestions WHERE ConsecutiveID = ".$this->ConsecutiveID.") {$strFilter} 
			ORDER BY A.SectionNumber";
		//@JAPR
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die(translate("Error accessing")." SI_SV_Section, SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			while (!$aRS->EOF)
			{
				$intSectionID = (int) @$aRS->fields["sectionid"];
				//@JAPR 2014-10-03: Agregadas las preguntas tipo sección (Inline)
				//No debe considerar a las secciones que ya se han mapeado pues ya no funcionarán como secciones en páginas independientes
				//En este caso se va a deshabilitar que se puedan ocultar preguntas de secciones que han sido mapeadas, ya que no hay un proceso
				//de refresh programado y se pudiera dar el caso de que tanto quien oculta como quien es ocultada (mediante una pregunta tipo qtpSection)
				//se encuentren en la misma página. Se validará si esto es o no deseable para en el futuro implementarlo
				if (!in_array($intSectionID, $arrInvalidSections)) {
					$strSectionName = (string) @$aRS->fields["sectionname"];
					$allValues[$intSectionID] = $strSectionName;
				}
				//@JAPR
  				$aRS->MoveNext();
			}
		}

		return $allValues;
	}
	//@JAPR
	
	//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
	//Obtiene la lista de preguntas que aun no se han ocultado agrupadas por secciones
	function getNonHiddenQuestionsBySection()
	{		
		$aQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		$intSurveyID = (int) @$aQuestion->SurveyID;
		$intQuestionNumber = (int) @$aQuestion->QuestionNumber;
		
		$sql = "SELECT DISTINCT A.SectionID, B.QuestionID, B.QuestionNumber, B.QuestionText 
			FROM SI_SV_Section A, SI_SV_Question B 
			WHERE A.SectionID = B.SectionID AND A.SurveyID = ".$intSurveyID." AND B.QuestionNumber > ".$intQuestionNumber." AND 
				B.QuestionID NOT IN (
					SELECT ShowQuestionID FROM SI_SV_ShowQuestions WHERE ConsecutiveID = ".$this->ConsecutiveID.") 
			ORDER BY B.QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die(translate("Error accessing")." SI_SV_Section, SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["sectionid"];
			$aGroup = array();
			while (!$aRS->EOF)
			{
				if ($lastID != (int) @$aRS->fields["sectionid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = (int) @$aRS->fields["sectionid"];
					$aGroup = array();
				}
				
				$intQuestionID = (int) @$aRS->fields["questionid"];
				$strQuestiontext = (int) @$aRS->fields["questionnumber"].". ".@$aRS->fields["questiontext"];
				$aGroup[$intQuestionID] = $strQuestiontext;
  				$aRS->MoveNext();
			}
			$allValues[$lastID] = $aGroup;
		}
		
		return $allValues;
	}
	//@JAPR
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
 		<script language="JavaScript">
		function verifyFieldsAndSave(target)
 		{
 			var strBlanksField = "";
 			
 			selectedIdx = parseInt(<?=$myFormName?>_SaveForm.ShowQuestionID.selectedIndex);
					
			if(selectedIdx<0)
			{
				strBlanksField+= '\n'+'<?=translate("You must have selected a valid question.")?>';
			}

			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else
  			{
  				<?=$myFormName?>_Ok(target);
  			}
 		}
 		</script>
<?
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
 		$myFormName = get_class($this);
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		if(objOkParentButton)
		{
			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		}
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("<?=$myFormName?>_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?

	}

	//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser)
	{
		return true;
	}

	//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
	function hideRemoveButton($aUser)
	{
		return true;
	}
}

class BITAMShowQuestionCollection extends BITAMCollection
{
	public $ConsecutiveID;		// -1 Significaría que es la colección de una pregunta en lugar de una opción de pregunta
	//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
	public $QuestionID;			// Sólo si $ConsecutiveID == -1
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	//Agregado el default de $aConsecutiveID porque para V6 no es requerido
	function __construct($aRepository, $aConsecutiveID = -1, $aQuestionID = 0)
	{
		BITAMCollection::__construct($aRepository);
		$this->ConsecutiveID = $aConsecutiveID;
		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		$this->QuestionID = $aQuestionID;
	}
	
	//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
	static function NewInstanceByQuestionID($aRepository, $aQuestionID)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, -1, $aQuestionID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
		$sql = "SELECT t1.ConsecutiveID, t1.ShowID, t1.ShowQuestionID, t2.QuestionNumber, t2.QuestionText, t2.SectionID 
				FROM SI_SV_ShowQuestions t1, SI_SV_Question t2 
				WHERE t1.ShowQuestionID = t2.QuestionID AND t1.QuestionID = ".$aQuestionID." 
				ORDER BY t2.QuestionNumber";

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	//@JAPR
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	//Agregado el default de $aConsecutiveID porque para V6 no es requerido
	static function NewInstance($aRepository, $aConsecutiveID = -1, $anArrayOfShowIDs = null)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aConsecutiveID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		$filter = "";

		if (!is_null($anArrayOfShowIDs))
		{
			switch (count($anArrayOfShowIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND t1.ShowID = ".((int)$anArrayOfShowIDs[0]);
					break;
				default:
					foreach ($anArrayOfShowIDs as $aShowID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aShowID; 
					}
					if ($filter != "")
					{
						$filter = " AND t1.ShowID IN (".$filter.")";
					}
					break;
			}
		}

		//@JAPR 2012-07-26: Agregado soporte para ocultar preguntas posteriores en cualquier sección
		$sql = "SELECT t1.ConsecutiveID, t1.ShowID, t1.ShowQuestionID, t2.QuestionNumber, t2.QuestionText, t2.SectionID 
				FROM SI_SV_ShowQuestions t1, SI_SV_Question t2 
				WHERE t1.ShowQuestionID = t2.QuestionID AND t1.ConsecutiveID = ".$aConsecutiveID.$filter." 
				ORDER BY t2.QuestionNumber";

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);

			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
	/* Obtiene la colección de ShowQuestions pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección para 
	cada forma además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance pero con todos
	los parámetros en null sin un ConsecutiveID y agregando SurveyID) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrShowQuestionsByQuestionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t2.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "AND t2.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT t1.ConsecutiveID, t1.ShowID, t1.ShowQuestionID, t2.QuestionNumber, t2.QuestionText, t2.SectionID 
			FROM SI_SV_ShowQuestions t1, SI_SV_Question t2 
			WHERE t1.ShowQuestionID = t2.QuestionID ".$filter." 
			ORDER BY t2.QuestionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			//Agregado el parámetro $bSkipSurveyID para indicar que no se desea obtener el SurveyID (originalmente utilizado para la carga masiva de datos)
			$objShowQuestion = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, true);
			//@JAPR
			$anInstance->Collection[] = $objShowQuestion;
			
			//Agrega el objeto a la colección indexada por los padres
			$intQuestionID = $objShowQuestion->QuestionID;
			if ( !isset($arrShowQuestionsByQuestionID[$intQuestionID]) ) {
				$arrShowQuestionsByQuestionID[$intQuestionID] = array();
			}
			$arrShowQuestionsByQuestionID[$intQuestionID][] = $objShowQuestion;
			
			$aRS->MoveNext();
		}
		
		//Finalmente procesa el array de ShowQuestions para indexar por todos los padres agregarlos al caché
		foreach ($arrShowQuestionsByQuestionID as $intQuestionID => $arrShowQuestionsColl) {
			BITAMGlobalFormsInstance::AddShowQuestionCollectionByQuestionWithID($intQuestionID, $arrShowQuestionsByQuestionID[$intQuestionID]);
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("ConsecutiveID", $aHTTPRequest->GET))
		{
			$aConsecutiveID = (int) $aHTTPRequest->GET["ConsecutiveID"];
		}
		else
		{
			$aConsecutiveID = 0;
		}
		
		return Collection::NewInstance($aRepository, $aConsecutiveID);
	}

	function get_Parent()
	{
		return BITAMQuestionOption::NewInstanceWithID($this->Repository, $this->ConsecutiveID);
	}

	function get_Title()
	{
		return translate("Show Questions");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=QuestionOption&ConsecutiveID=".$this->ConsecutiveID;
	}

	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=ShowQuestion&ConsecutiveID=".$this->ConsecutiveID;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ShowID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ShowQuestionName";
		$aField->Title = translate("");
		$aField->Type = "String";
		$aField->Size = 512;
		$myFields[$aField->Name] = $aField;

		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
}
?>