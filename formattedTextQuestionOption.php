<?php
$myFormName = get_class($this);
?>
&nbsp;
<br>

<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
				function changeTab(oEvt, sPropName) {
					if (!oEvt || !sPropName) {
						return;
					}
					
					objEditorType=<?=$myFormName?>_SaveForm.EditorType;
					if(objEditorType)
					{
						editorTypeValue = objEditorType.options[objEditorType.selectedIndex].value;
					}
					else
					{
						editorTypeValue = 0;
					}
					
					if(editorTypeValue==0)
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
					}
					else
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'DesDiv';
						
						if (document.getElementById(strTabId))
						{
							visibleDivArray[sPropName] = sPropName+oEvt.getAttribute('name')+'DesHDiv';
						}
						else 
						{
							var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
						}
					}
					
					document.getElementById(sPropName+'DefaultDiv').style.display = 'none';
					var objDiv = document.getElementById(sPropName+'iPodDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					var objDiv = document.getElementById(sPropName+'DefaultDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPodDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					var objDiv = document.getElementById(strTabId);
					if (objDiv) {
						objDiv.style.display = 'block';
					}
					try {
						var objDiv = document.getElementById(sPropName+'Menu');
						if (objDiv) {
							var arrLi = objDiv.getElementsByTagName('li');
							if (arrLi) {
								for (var intIdx in arrLi) {
									arrLi[intIdx].className = "menuOption";
								}
							}
						}
						
						if (oEvt) {
							oEvt.className = "menuOption selectedTab"						
						}
					} catch(e) {}
				}
				
				var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "DisplayImage,ajaxfilemanager",
				      	theme : "advanced",
				      	width: 150,
        				height: 150,
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						theme_advanced_buttons1 : "image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",
   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						//oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					
					/*
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "HTMLText,ajaxfilemanager",
				      	theme : "advanced",
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",
   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					*/
					
					var arrResponsiveDesignObjectsMsg = new Array();
					var arrResponsiveDesignObjectsImg = new Array();
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					/*
					arrResponsiveDesignObjectsMsg.push('HTMLTextiPod');
					arrResponsiveDesignObjectsMsg.push('HTMLTextiPad');
					arrResponsiveDesignObjectsMsg.push('HTMLTextiPadMini');
					arrResponsiveDesignObjectsMsg.push('HTMLTextiPhone');
					arrResponsiveDesignObjectsMsg.push('HTMLTextCel');
					arrResponsiveDesignObjectsMsg.push('HTMLTextTablet');
					*/
					
					arrResponsiveDesignObjectsImg.push('DisplayImageiPod');
					arrResponsiveDesignObjectsImg.push('DisplayImageiPad');
					arrResponsiveDesignObjectsImg.push('DisplayImageiPadMini');
					arrResponsiveDesignObjectsImg.push('DisplayImageiPhone');
					arrResponsiveDesignObjectsImg.push('DisplayImageCel');
					arrResponsiveDesignObjectsImg.push('DisplayImageTablet');
<?
		}
		//@JAPR
?>
					
					for (var intIdx in arrResponsiveDesignObjectsMsg) {
						tinyMCE.init({
							// General options
							mode : "exact",
							readonly : 0,
					        elements : arrResponsiveDesignObjectsMsg[intIdx] + ",ajaxfilemanager",
					      	theme : "advanced",
							file_browser_callback : "ajaxfilemanager",
							handle_event_callback : "formattedtextchanged",
							entity_encoding : "raw",
							theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink",
							theme_advanced_buttons2: "",
							theme_advanced_buttons3: "",
							theme_advanced_buttons4: "",		
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "none",
							theme_advanced_resizing : true,
							//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
							theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	        										"Arial=arial,helvetica,sans-serif;"+
	        										"Arial Black=arial black,avant garde;"+
	        										"Book Antiqua=book antiqua,palatino;"+
									                "Comic Sans MS=comic sans ms,sans-serif;"+
									                "Courier New=courier new,courier;"+
									                "Georgia=georgia,palatino;"+
									                "Helvetica=helvetica;"+
									                "Impact=impact,chicago;"+
									                "Symbol=symbol;"+
									                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
									                "Terminal=terminal,monaco;"+
									                "Times New Roman=times new roman,times;"+
									                "Trebuchet MS=trebuchet ms,geneva;"+
									                "Verdana=verdana,geneva;"+
									                "Webdings=webdings;"+
									                "Wingdings=wingdings,zapf dingbats",
	   						relative_urls : false,
							content_css : "",
							convert_urls : false,
							// Drop lists for link/image/media/template dialogs
							external_link_list_url : "lists/link_list.js",
							save_callback : "myCustomSaveContent",
							oninit: "myInitTinyMCE",
							cleanup: false,
							verify_html : false,
							forced_root_block : false
						});
					}
					
					for (var intIdx in arrResponsiveDesignObjectsImg) {
						tinyMCE.init({
							// General options
							mode : "exact",
							readonly : 0,
					        elements : arrResponsiveDesignObjectsImg[intIdx] + ",ajaxfilemanager",
					      	theme : "advanced",
					      	width: 150,
	        				height: 150,
							file_browser_callback : "ajaxfilemanager",
							handle_event_callback : "formattedtextchanged",
							entity_encoding : "raw",
							theme_advanced_buttons1 : "image",
							theme_advanced_buttons2: "",
							theme_advanced_buttons3: "",
							theme_advanced_buttons4: "",		
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "none",
							theme_advanced_resizing : true,
							//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
							theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	        										"Arial=arial,helvetica,sans-serif;"+
	        										"Arial Black=arial black,avant garde;"+
	        										"Book Antiqua=book antiqua,palatino;"+
									                "Comic Sans MS=comic sans ms,sans-serif;"+
									                "Courier New=courier new,courier;"+
									                "Georgia=georgia,palatino;"+
									                "Helvetica=helvetica;"+
									                "Impact=impact,chicago;"+
									                "Symbol=symbol;"+
									                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
									                "Terminal=terminal,monaco;"+
									                "Times New Roman=times new roman,times;"+
									                "Trebuchet MS=trebuchet ms,geneva;"+
									                "Verdana=verdana,geneva;"+
									                "Webdings=webdings;"+
									                "Wingdings=wingdings,zapf dingbats",
	   						relative_urls : false,
							content_css : "",
							convert_urls : false,
							// Drop lists for link/image/media/template dialogs
							external_link_list_url : "lists/link_list.js",
							save_callback : "myCustomSaveContent",
							//oninit: "myInitTinyMCE",
							cleanup: false,
							verify_html : false,
							forced_root_block : false
						});
					}
					
					function myInitTinyMCE() {
						/*
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["HTMLText"]) {
							var htmlSrc = tinyMCE.editors["HTMLText"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								setTimeout(function () {toogleEditorMode('HTMLText');}, 300);
							}
						}
						*/
					}
					
					function myCustomSaveContent(element_id, html, body) {
					        // Do some custom HTML cleanup
					        if (html) {
					        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
					        }
					
					        return html;
					}
					
					function formattedtextchanged(event) {
						
						var htmlSrc = tinyMCE.activeEditor.getContent();
			            
						if(event.type == "keydown") {
							
						}
						
						/*
						var rexp = [a-zA-z0-9\t\n ./<>?;:"'`!@#$%^&*()[]{}_+=-|\\];
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      		*/
			         
					}

					function ajaxfilemanager(field_name, url, type, win) 
					{
						var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
						var view = 'detail';
						switch (type) 
						{
							case "image":
								view = 'thumbnail';
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
						}
    					
						tinyMCE.activeEditor.windowManager.open({
							url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
							width: 900,
        					height: 600,
							inline : "yes",
							close_previous : "no"
						},{
							window : win,
							input : field_name
						}); 
					}
					
					//Inicio conchita 16-nov-2011 toggle tinymce
			        function toogleEditorMode(sEditorID) { 
				        var x;
				        var tmp;
			        
			            try { 
			                if(tinyMCEmode) { 
			                	//x=removeHTML();
			               		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
			                   	//document.getElementById(sEditorID).value=x; 
			                    tinyMCEmode = false; 
			                } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
			                    //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
			                    tinyMCEmode = true; 
			                } 
			            } catch(e) { 
			            	alert('An error occur, try again. Error: '+ e);
			                //error handling 
			            } 
			        }
					
			        function removeHTML() {
			            var htmlSrc = tinyMCE.activeEditor.getContent();
			            var rexp = /<[^<>]+>/g;
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      
			            return cleanSrc;
			        }
					//Fin Conchita toggle tinymce 16-nov-2011	
</script>

<div id="displayImageArea" width="100%">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td>
				<strong><?=translate("Image")?></strong>
			</td>
		</tr>
		<tr>
			<td class="object_field_value_3" colspan="3">
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
				<div id="DisplayImageMenu" class="tabbed" style="height:40px;">
					<ul class="menu">
						<li name="Default" class="menuOption selectedTab" onclick="changeTab(this,'DisplayImage')">
							<a>Default</a>
				        </li>					
						<li name="iPod" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>iPod</a>
				        </li>					
						<li name="iPad" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>iPad</a>
				        </li>					
						<li name="iPadMini" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>iPad Mini</a>
				        </li>					
						<li name="iPhone" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>iPhone</a>
				        </li>					
						<li name="Cel" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>Android Cel</a>
				        </li>					
						<li name="Tablet" class="menuOption" onclick="changeTab(this,'DisplayImage')">
							<a>Android Tablet</a>
				        </li>					
					</ul>
				</div>
<?
		}
		//@JAPR
?>
				<div id="DisplayImageTabs">
					<div id="DisplayImageDefaultDiv">
						<textarea id="DisplayImage" name="DisplayImage" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImage?>
						</textarea>
					</div>
<?
		//@JAPR 2014-08-01: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					<div id="DisplayImageiPodDiv" style="display: none;">
						<textarea id="DisplayImageiPod" name="DisplayImageiPod" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageiPod?>
						</textarea>
					</div>
					<div id="DisplayImageiPadDiv" style="display: none;">
						<textarea id="DisplayImageiPad" name="DisplayImageiPad" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageiPad?>
						</textarea>
					</div>
					<div id="DisplayImageiPadMiniDiv" style="display: none;">
						<textarea id="DisplayImageiPadMini" name="DisplayImageiPadMini" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageiPadMini?>
						</textarea>
					</div>
					<div id="DisplayImageiPhoneDiv" style="display: none;">
						<textarea id="DisplayImageiPhone" name="DisplayImageiPhone" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageiPhone?>
						</textarea>
					</div>
					<div id="DisplayImageCelDiv" style="display: none;">
						<textarea id="DisplayImageCel" name="DisplayImageCel" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageCel?>
						</textarea>
					</div>
					<div id="DisplayImageTabletDiv" style="display: none;">
						<textarea id="DisplayImageTablet" name="DisplayImageTablet" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->DisplayImageTablet?>
						</textarea>
					</div>
<?
		}
		//@JAPR
?>
				</div>
			</td>
		</tr>
		
	</tbody>
	</table>
</div>