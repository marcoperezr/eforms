<?php
//@JAPRDescontinuada: Para V6 ya no se utiliza esta clase, los filtros de las preguntas se graban directamente en question.inc.php y de ahí como BITAMSurveyFilter
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("filtersv.inc.php");

/*Esta clase se debe tomar como un template, en caso de requerir un nuevo tipo de filtro para pregunta, se puede basar en esta y limpiar las funciones
innecesarias así como cambiar las propiedades específicas. En esta clase se dejaron funciones clones como addAttributeFields, 
addAttributeFieldsByeBavelForm y addAttributeFieldsByCatalog, simplemente para que se puedan replicar a la clase final que se pudiera agregar mas
adelante para un uso diferente, pero en realidad cada clase debería tener sólo la función sin la terminación ByeBavelForm o ByCatalog e implementar
esos casos específicos en el nombre default de cada función (en este caso de ejemplo, directo en addAttributeFields)
El campo Type está pensado en caso de que se decida unificar en una misma tabla, lo cual no es del todo necesario, pero es buena idea dejarlo incluso
si se separan las tablas, ya que las clases que se puedan agregar mas adelante deberían ser polimórficas y con este valor es mas sencillo identificar
el tipo de filtro
A la fecha de implementación sólo existían filtros para preguntas simple choice de catálogo y configuradas para usar mapa, donde los filtros definían
las condiciones bajo las cuales se mostrarían los markers con cierto color qftpMarkerColors
*/

class BITAMQuestionFilter extends BITAMObject
{
	//Propiedades genéricas de los filtros
	public $FilterID;						//ID único del filtro
	public $QuestionID;						//ID de pregunta para la que aplica
	public $FilterType;						//Tipo de filtro por si eventualmente hay diferentes posibilidades, así se puede reutilizar la tabla. En la fecha de implemetnación sólo aplicaban filtros para los colores de los markers en simple choice con mapa
	public $FilterName;
	public $FilterText;						//Texto traducido del filtro. En la implementación inicial esto corresponde a un filtro para ejecutar en un eval de JScript (preguntas simple choice de catálogo con despliegue tipo mapa que usan variables de preguntas), pero no se descarta un uso eventual como filtro SQL para otros casos o tipos de pregunta
	public $FilterTextUsr;					//Texto del filtro, a diferencia de FilterText, este contiene nombres de variables entendibles por el usuario (ej. @AttrZone) en lugar de las variables internas con IDs (ej. @eBFID15, donde 15 es el id_fieldform). Según el tipo de pregunta o caso en particular, puede usar cierto tipo diferente de variables
	public $FilterLevels;					//No aplica, dejado por compatibilidad con la tabla, aunque internamente contendrá los IDs de campos de eBavel usados como variables @AttrName en el FilterText
	public $eBavelFormID;					//ID de la forma de eBavel asociada a la pregunta (este valor no se define en esta instancia sino que se hereda de la pregunta correspondiente si es que el tipo de pregunta y filtro lo requieren)
	public $CatalogID;						//ID del catálogo asociado a la pregunta (este valor no se define en esta instancia sino que se hereda de la pregunta correspondiente si es que el tipo de pregunta y filtro lo requieren)
	public $SurveyID;						//ID de la encuesta de la sección a la que pertenece el filtro
	
	//Propiedades específicas de los filtros según el FilterType
	public $Color;							//Color del Marker a utilizar cuando se cumpla la condición del filtro
	
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $AttribPositions;
	public $AttribMemberIDs;
	public $AttribMemberVarNames;
	public $NumAttribFields;
	
	function __construct($aRepository, $aQuestionID, $aFilterType = null)
	{
		BITAMObject::__construct($aRepository);
		$this->FilterID = -1;
		$this->QuestionID = $aQuestionID;
		if (!is_null($aFilterType)) {
			$this->FilterType = $aFilterType;
		}
		else {
			$this->FilterType = qftpMarkerColors;
		}
		$this->FilterName = "";
		$this->FilterText = "";
		$this->FilterTextUsr = "";
		$this->FilterLevels = "";
		$this->eBavelFormID = 0;
		$this->CatalogID = 0;
		$this->SurveyID = 0;
		$this->Color = 'FF0000';
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->AttribIDs = array();
		$this->AttribPositions = array();
		$this->AttribMemberIDs = array();
		$this->AttribMemberVarNames = array();
		$this->NumAttribFields = 0;
		
		$objQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $aQuestionID);
		if (!is_null($objQuestion)) {
			//$this->eBavelFormID = $objQuestion->SectionFormID;
			$this->SurveyID = $objQuestion->SurveyID;
			switch ($this->FilterType) {
				case qftpMarkerColors:
					$this->CatalogID = $objQuestion->CatalogID;
					break;
			}
		}
	}
	
	static function NewInstance($aRepository, $aQuestionID, $aFilterType = null)
	{
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aQuestionID, $aFilterType);
	}
	
	static function NewInstanceWithID($aRepository, $aFilterID)
	{
		$anInstance = null;
		
		if (((int) $aFilterID) <= 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT B.SurveyID, A.QuestionID, A.FilterID, A.FilterType, A.FilterName, A.FilterText, A.FilterLevels, B.CatalogID, A.Color 
			FROM SI_SV_QuestionFilter A 
				INNER JOIN SI_SV_Question B ON A.QuestionID = B.QuestionID 
			WHERE A.FilterID = ".$aFilterID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aQuestionID = (int) @$aRS->fields["questionid"];
		$aFilterType = $aRS->fields["filtertype"];
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID, $aFilterType);
		//@JAPR
		$anInstance->FilterID = (int) @$aRS->fields["filterid"];
		$anInstance->FilterType = (int) @$aFilterType;
		$anInstance->FilterName = (string) @$aRS->fields["filtername"];
		$anInstance->FilterText = (string) @$aRS->fields["filtertext"];
		$anInstance->FilterLevels = (string) @$aRS->fields["filterlevels"];
		$anInstance->Color = (string) @$aRS->fields["color"];
		if (trim($anInstance->Color) == '') {
			$anInstance->Color = 'FF0000';
		}
		
		switch ($anInstance->FilterType) {
			case qftpMarkerColors:
				$anInstance->CatalogID = (int) @$aRS->fields["catalogid"];
				break;
		}
		
		$anInstance->addAttributeFields();
		return $anInstance;
	}
	
	//Basado en el tipo de filtro, invoca a la función para generar las listas de selección de elementos (si es que lo requiere)
	function addAttributeFields($bNullFilters = false) {
		switch ($this->FilterType) {
			case qftpMarkerColors:
				$this->addAttributeFieldsByCatalog($bNullFilters);
				break;
		}
	}
	
	function addAttributeFieldsByeBavelForm($bNullFilters = false) {
		if ($this->eBavelFormID <= 0) {
			return;
		}
		
		$arreBavelFields = @GetEBavelFieldsForms($this->Repository, true, null, array($this->eBavelFormID));
		$numFields = count($arreBavelFields);
		$arrUsedVarNames = array();
		$this->FilterTextUsr = $this->FilterText;
		$i = 0;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		foreach ($arreBavelFields as $inteBavelFieldID => $objeBavelField) {
			$fieldName = 'DSC_'.$objeBavelField['id'];
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = $fieldName;
			$this->AttribIDs[$i] = $objeBavelField['id'];
			$this->AttribNames[$i] = $objeBavelField['label'];
			$this->AttribMemberIDs[$fieldName] = $inteBavelFieldID;
			$this->AttribPositions[$fieldName] = $i;
			$strFieldVarName = GetEBavelFieldVariable($objeBavelField);
			if (isset($arrUsedVarNames[$strFieldVarName])) {
				$strFieldVarName .= $inteBavelFieldID;
			}
			$arrUsedVarNames[$strFieldVarName] = $strFieldVarName;
			$arrUsedVarNamesByPos[$inteBavelFieldID] = $strFieldVarName;
			$this->AttribMemberVarNames[$inteBavelFieldID] = $strFieldVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$inteBavelFieldID] = '@eBFID'.$inteBavelFieldID;
			//$this->FilterTextUsr = str_ireplace('@eBFID'.$inteBavelFieldID, $strFieldVarName, $this->FilterTextUsr);
			
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				$this->$fieldName = "sv_ALL_sv";
				$this->$fieldNameSTR = "All";
			}
			$i++;
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $inteBavelFieldID => $strMemberVarName)	{
			$strFieldVarName = (string) @$arrUsedVarNamesByPos[$inteBavelFieldID];
			$this->FilterTextUsr = str_ireplace($strMemberVarName, $strFieldVarName, $this->FilterTextUsr);
		}
		
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->FilterTextUsr = $this->TranslateVariableQuestionIDsByNumbers($this->FilterTextUsr);
	    //@JAPR
		
		$this->NumAttribFields = $numFields;
	}
	
	//Agrega las listas para seleccionar las variables a usar pero según el catálogo especificado en caso de ser un tipo de filtro que lo requiera
	//Los tipos de filtro que usan catálogos son: qftpMarkerColors
	function addAttributeFieldsByCatalog($bNullFilters = false) {
		if ($this->CatalogID <= 0) {
			return;
		}
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
		$this->FilterTextUsr = $this->FilterText;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++)
		{
			$objCatMember = $attributes->Collection[$i];
			$fieldName = 'DSC_'.$objCatMember->ClaDescrip;
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = 'DSC_'.$objCatMember->ClaDescrip;
			$this->AttribIDs[$i] = $objCatMember->ClaDescrip;
			$this->AttribNames[$i] = $objCatMember->MemberName;
			$this->AttribMemberIDs[$fieldName] = $objCatMember->MemberID;
			$this->AttribPositions[$fieldName] = $i;
			//@JAPR 2014-06-06: Agregado el refresh de catálogos dinámico
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
			$this->AttribMemberVarNames[$objCatMember->MemberID] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = '@CMID'.$objCatMember->MemberID;
			//$this->FilterTextUsr = str_ireplace('@CMID'.$objCatMember->MemberID, $strCatMemberVarName, $this->FilterTextUsr);
			//@JAPR
			
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				$this->$fieldName = "sv_ALL_sv";
				$this->$fieldNameSTR = "All";
			}
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$strCatMemberVarName = (string) @$arrUsedVarNamesByPos[$intMemberOrder];
			$this->FilterTextUsr = str_ireplace($strMemberVarName, $strCatMemberVarName, $this->FilterTextUsr);
		}
		
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->FilterTextUsr = $this->TranslateVariableQuestionIDsByNumbers($this->FilterTextUsr);
	    //@JAPR
		
		$this->NumAttribFields = $numFields;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}
		
		if (array_key_exists("FilterID", $aHTTPRequest->POST))
		{
			$aFilterID = $aHTTPRequest->POST["FilterID"];
			
			if (is_array($aFilterID))
			{
				$aCollection = BITAMQuestionFilterCollection::NewInstance($aRepository, $aQuestionID, $aFilterID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMQuestionFilter::NewInstance($aRepository, $aQuestionID);
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aFilterID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
						if (!is_null($anInstance)) {
							$anInstance->addAttributeFields();
						}
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("FilterID", $aHTTPRequest->GET))
		{
			$aFilterID = $aHTTPRequest->GET["FilterID"];
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aFilterID);
			
			if (is_null($anInstance))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
				if (!is_null($anInstance)) {
					$anInstance->addAttributeFields();
				}
			}
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
			if (!is_null($anInstance)) {
				$anInstance->addAttributeFields();
			}
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("FilterID", $anArray))
		{
			$this->FilterID = (int)$anArray["FilterID"];
		}
		
		if (array_key_exists("FilterName", $anArray))
		{
			$this->FilterName = rtrim($anArray["FilterName"]);
		}
		
		if (array_key_exists("FilterTextUsr", $anArray))
		{
			$this->FilterTextUsr = rtrim($anArray["FilterTextUsr"]);
		}
		
		if (array_key_exists("Color", $anArray))
		{
			$this->Color = rtrim($anArray["Color"]);
		}
		
		return $this;
	}
	
	//Basado en el tipo de filtro, invoca a la función para codificar a variables internas el filtro capturado
	function encodeFilterText($aFilterTextUsr) {
		$strResponse = '';
		
		switch ($this->FilterType) {
			case qftpMarkerColors:
				$strResponse = $this->encodeFilterTextByCatalog($aFilterTextUsr);
				break;
		}
		
		return $strResponse;
	}
		
	//Toma el valor del FilterTextUsr y cambia las variables de usuario por las variables internas con las que se debe grabar
	function encodeFilterTextByeBavelForm($aFilterTextUsr) {
		if ($this->eBavelFormID <= 0) {
			return '';
		}
		$strFilterText = $this->FilterTextUsr;
		
		$arreBavelFields = @GetEBavelFieldsForms($this->Repository, true, null, array($this->eBavelFormID));
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		foreach ($arreBavelFields as $inteBavelFieldID => $objeBavelField) {
			$strFieldVarName = GetEBavelFieldVariable($objeBavelField);
			if (isset($arrUsedVarNames[$strFieldVarName])) {
				$strFieldVarName .= $inteBavelFieldID;
			}
			$arrUsedVarNames[$strFieldVarName] = $strFieldVarName;
			$arrUsedVarNamesByPos[$inteBavelFieldID] = $strFieldVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$inteBavelFieldID] = '@eBFID'.$inteBavelFieldID;
			//$strFilterText = str_ireplace($strFieldVarName, '@eBFID'.$inteBavelFieldID, $strFilterText);
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $inteBavelFieldID => $strFieldVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$inteBavelFieldID];
			$strFilterText = str_ireplace($strFieldVarName, $strMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	
	//Toma el valor del FilterTextUsr y cambia las variables de usuario por las variables internas con las que se debe grabar
	function encodeFilterTextByCatalog($aFilterTextUsr) {
		if ($this->CatalogID <= 0) {
			return '';
		}
		$strFilterText = $this->FilterTextUsr;
		
		$attributes = BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		$numFields = count($attributes->Collection);
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++) {
			$objCatMember = $attributes->Collection[$i];
			$strCatMemberVarName = $objCatMember->getVariableName();
			if (isset($arrUsedVarNames[$strCatMemberVarName])) {
				$strCatMemberVarName .= $objCatMember->MemberOrder;
			}
			$arrUsedVarNames[$strCatMemberVarName] = $strCatMemberVarName;
			$arrUsedVarNamesByPos[$i] = $strCatMemberVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = '@CMID'.$objCatMember->MemberID;
			//$strFilterText = str_ireplace($strCatMemberVarName, '@CMID'.$objCatMember->MemberID, $strFilterText);
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $intMemberOrder => $strCatMemberVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$intMemberOrder];
			$strFilterText = str_ireplace($strCatMemberVarName, $strMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, esvExtendedModifInfo, otyQuestionFilter, $this->FilterID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, esvExtendedModifInfo, $bServerVariables);
	}
	//@JAPR
	
	/* Esta función se encargará de buscar todas las posibles variables de atributos del catálogo de la pregunta a la que pertenece este filtro,
	para posteriormente reemplazarlas por una versión delimitada de la la variable que además utilice los números de atributo en lugar de los IDs
	El parámetro $bServerVariables realmente sólo aplicaría si se intentara aplicar algún cálculo dentro de procesos del server y sólo si eventualmente
	se modificada la función para generar variables de preguntas, lo cual no es el caso
	El proceso de reemplazo no convertirá otras variables de preguntas, las cuales se asume que ya están utilizando el número de las preguntas, o bien
	que se tendrán que convertir a números de preguntas posteriormente si es el caso (según el parámetro $bServerVariables)
	*/
	function TranslateCatMemberVarsForApp($sText, $bServerVariables = false) {
		if ($this->CatalogID <= 0) {
			return $sText;
		}
		
		$objQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
		if (is_null($objQuestion)) {
			return $sText;
		}

		$attributes = @BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
		if (is_null($attributes) || count($attributes->Collection) == 0) {
			return $sText;
		}
		
		/*
		$strQuestionVar = '@Q'.$objQuestion->QuestionNumber.'(#)';
		if (!$bServerVariables) {
			$strQuestionVar = '{'.$strQuestionVar.'}';
		}
		*/
		
		$numFields = count($attributes->Collection);
		$strTransText = $sText;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		for($i=0; $i<$numFields; $i++)
		{
			$objCatMember = $attributes->Collection[$i];
			$arrUsedVarNamesByPos[$i] = '{@CM'.$objCatMember->MemberOrder.'}';
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$i] = '@CMID'.$objCatMember->MemberID;
			//@JAPR
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $intMemberOrder => $strMemberVarName)	{
			$strCatMemberVarName = (string) @$arrUsedVarNamesByPos[$intMemberOrder];
			$strTransText = str_ireplace($strMemberVarName, $strCatMemberVarName, $strTransText);
		}
		
		return $strTransText;
	}
	
	function save()
	{
		$this->FilterText = $this->encodeFilterText($this->FilterTextUsr);
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
		$this->FilterText = $this->TranslateVariableQuestionNumbersByIDs($this->FilterText, optyFilterText);
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterID)", "0")." + 1 AS FilterID".
						" FROM SI_SV_QuestionFilter";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->FilterID = (int)$aRS->fields["filterid"];
			$sql = "INSERT INTO SI_SV_QuestionFilter (".
						"QuestionID".
						",FilterID".
						",FilterType".
						",FilterName".
						",FilterText".
						",FilterLevels".
						",Color".
			            ") VALUES (".
						$this->QuestionID.
						",".$this->FilterID.
						",".$this->FilterType.
						",".$this->Repository->DataADOConnection->Quote($this->FilterName).
						",".$this->Repository->DataADOConnection->Quote($this->FilterText).
						",".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
						",".$this->Repository->DataADOConnection->Quote($this->Color).
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$sql = "UPDATE SI_SV_QuestionFilter SET ".
					" FilterName = ".$this->Repository->DataADOConnection->Quote($this->FilterName).
					", FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					", FilterLevels = ".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
					", Color = ".$this->Repository->DataADOConnection->Quote($this->Color).
					" WHERE FilterID = ".$this->FilterID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
        //@JAPR 2014-10-03: Corregido un bug, no estaba actualizando la versión de la definición al grabar
    	BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
        //@JAPR
		return $this;
	}
	
	function remove()
	{		
		$sql = "DELETE FROM SI_SV_QuestionFilter WHERE FilterID = ".$this->FilterID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->FilterID <= 0);
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Filter");
		}
		else
		{
			return $this->FilterName;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionFilter&QuestionID=".$this->QuestionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionFilter&QuestionID=".$this->QuestionID."&FilterID=".$this->FilterID;
		}
	}
	
	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return "FilterID";
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Color";
		$aField->Title = translate("Map marker color");
		$aField->Type = "String";
		$aField->Size = 10;
		$aField->CustomizedClass = "color";
		$myFields[$aField->Name] = $aField;
		
		//Campos para apoyo en la edición del FilterSQL
		switch ($this->FilterType) {
			case qftpMarkerColors:
				//Campos para apoyo en la edición del FilterSQL
				if ($this->CatalogID > 0 && $this->NumAttribFields > 0) {
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "FilterTextUsr";
					$aField->Title = translate("Filter");
					$aField->Type = "LargeString";
					$aField->Size = 5000;
					$myFields[$aField->Name] = $aField;
					
					//Genera la combo de atributos, agrega dinámicamente la propiedad con un valor de 0 para que no seleccione a alguno en particular
					$this->CatMemberID = 0;
					$arrCatMembers = array();
					$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($this->Repository, $this->CatalogID);
					if (!is_null($objCatalogMembersColl)) {
						foreach ($objCatalogMembersColl as $objCatalogMember) {
							$arrCatMembers[$objCatalogMember->MemberID] = $objCatalogMember->MemberName;
						}
					}
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "CatMemberID";
					$aField->Title = translate("Attributes");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrCatMembers;
					$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
						"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
						"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
						"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
						"onclick=\"this.style.color='#d5d5bc';addAttrName();\">";
					$myFields[$aField->Name] = $aField;
					
					//Genera las combos con los valores de cada atributo
					$arrDefaultValue = array("sv_ALL_sv" => "All");
					for ($i=0; $i < $this->NumAttribFields; $i++)
					{
						$fieldName = $this->AttribFields[$i];
						$labelText = $this->AttribNames[$i];
						
						$aField = BITAMFormField::NewFormField();
						$aField->Name = $fieldName;
						$aField->Title = $labelText;
						$aField->Type = "Object";
						$aField->VisualComponent = "Combobox";
						$aField->Options = $arrDefaultValue;
						$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
							"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
							"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
							"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
							"onclick=\"this.style.color='#d5d5bc';addAttrValue('{$fieldName}');\">";
						$myFields[$aField->Name] = $aField;
					}
				}
				break;
			
			case -1;
				if ($this->eBavelFormID > 0 && $this->NumAttribFields > 0) {
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "FilterTextUsr";
					$aField->Title = translate("Filter");
					$aField->Type = "LargeString";
					$aField->Size = 5000;
					$myFields[$aField->Name] = $aField;
					
					//Genera la combo de campos de eBavel, agrega dinámicamente la propiedad con un valor de 0 para que no seleccione a alguno en particular
					$this->eBavelFieldID = 0;
					$arreBavelFields = array();
					foreach ($this->AttribMemberIDs as $fieldName => $inteBavelFieldID) {
						if ($inteBavelFieldID > 0) {
							$intIndex = (int) @$this->AttribPositions[$fieldName];
							$strFieldLabel = (string) @$this->AttribNames[$intIndex];
							$arreBavelFields[$inteBavelFieldID] = $strFieldLabel;
						}
					}
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "eBavelFieldID";
					$aField->Title = translate("Fields");
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arreBavelFields;
					$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
						"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
						"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
						"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
						"onclick=\"this.style.color='#d5d5bc';addAttrName();\">";
					$myFields[$aField->Name] = $aField;
					
					//Genera las combos con los valores de cada atributo
					$arrDefaultValue = array("sv_ALL_sv" => "All");
					for ($i=0; $i < $this->NumAttribFields; $i++)
					{
						$fieldName = $this->AttribFields[$i];
						$labelText = $this->AttribNames[$i];
						
						$aField = BITAMFormField::NewFormField();
						$aField->Name = $fieldName;
						$aField->Title = $labelText;
						$aField->Type = "Object";
						$aField->VisualComponent = "Combobox";
						$aField->Options = $arrDefaultValue;
						$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
							"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
							"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
							"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
							"onclick=\"this.style.color='#d5d5bc';addAttrValue('{$fieldName}');\">";
						$myFields[$aField->Name] = $aField;
					}
				}
				break;
		}
		
		return $myFields;
	}
	
	//Basado en el tipo de filtro, invoca a la función correspondiente para construir la ventana de captura
	function generateBeforeFormCode($aUser) {
		switch ($this->FilterType) {
			case qftpMarkerColors:
				$this->generateBeforeFormCodeByCatalog($aUser);
				break;
		}
	}
	
	function generateBeforeFormCodeByeBavelForm($aUser)
	{
		if ($this->eBavelFormID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMembersPos = new Array;
		var filterAttribMembersVars = new Array;
		var filterAttribMemberFields = new Array;
		var filterAttribValGot = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName];
			$attribVarName = $this->AttribMemberVarNames[$attribMemberID];
?>
		filterAttribIDs[<?=$position?>]='<?=$attribID?>';
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMembersPos['<?=$position?>']=<?=$attribMemberID?>;
		filterAttribMembersVars[<?=$attribMemberID?>]='<?=$attribVarName?>';
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
		filterAttribValGot[<?=$position?>]=0;
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			var changedMemberID = filterAttribMembersPos[thisPosition -1];
			if (changedMemberID === undefined) {
				changedMemberID = -1;
			}
			
			if (filterAttribValGot[thisPosition]) {
				return;
			}
			
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
			if (event && event.currentTarget && event.currentTarget.style) {
				event.currentTarget.style.cursor = "wait"
			}
		 	xmlObj.open("POST", "geteBavelFieldValues.php?noFilters=1&eBavelFormID=<?=$this->eBavelFormID?>&eBavelFieldID="+filterAttribMembers[attribName], false);
		 	xmlObj.send(null);
		 	strResult = Trim(unescape(xmlObj.responseText));
			if (event && event.currentTarget && event.currentTarget.style) {
				event.currentTarget.style.cursor = ""
			}
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = new Array();
			 		if (atribInfo[1] != '') {
			 			attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		}
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	filterAttribValGot[thisPosition] = 1;
		}
		
		function addAttrValue(attribName) {
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			
			var objSelect = document.getElementsByName(attribName);
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			if (!filterAttribValGot[thisPosition]) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == 'sv_ALL_sv') {
				return;
			}
			strSelectedVal = "'" + strSelectedVal + "'";

			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strSelectedVal;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelEnd);
			}
		}
		
		function addAttrName() {
			var objSelect = document.getElementsByName("eBavelFieldID");
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == '0') {
				return;
			}
			var strAttribVar = filterAttribMembersVars[strSelectedVal];
			if (!strAttribVar) {
				return;
			}
			
			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strAttribVar;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelEnd);
			}
		}
<?
		}
?>
	</script>
<?
	}

	function generateBeforeFormCodeByCatalog($aUser)
	{
?>
	<script type="text/javascript" src="js/jscolor/jscolor.js"></script>
<?

		if ($this->CatalogID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMembersPos = new Array;
		var filterAttribMembersVars = new Array;
		var filterAttribMemberFields = new Array;
		var filterAttribValGot = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName];
			$attribVarName = $this->AttribMemberVarNames[$attribMemberID];
?>
		filterAttribIDs[<?=$position?>]=<?=$attribID?>;
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMembersPos['<?=$position?>']=<?=$attribMemberID?>;
		filterAttribMembersVars[<?=$attribMemberID?>]='<?=$attribVarName?>';
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
		filterAttribValGot[<?=$position?>]=0;
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			var changedMemberID = filterAttribMembersPos[thisPosition -1];
			if (changedMemberID === undefined) {
				changedMemberID = -1;
			}
			
			if (filterAttribValGot[thisPosition]) {
				return;
			}
			
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
			if (event && event.currentTarget && event.currentTarget.style) {
				event.currentTarget.style.cursor = "wait"
			}
		 	xmlObj.open("POST", "getAttribValues.php?noFilters=1&CatalogID=<?=$this->CatalogID?>&CatMemberID="+filterAttribMembers[attribName]+"&ChangedMemberID="+changedMemberID, false);
		 	xmlObj.send(null);
		 	strResult = Trim(unescape(xmlObj.responseText));
			if (event && event.currentTarget && event.currentTarget.style) {
				event.currentTarget.style.cursor = ""
			}
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	filterAttribValGot[thisPosition] = 1;
		}
		
		function addAttrValue(attribName) {
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			
			var objSelect = document.getElementsByName(attribName);
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			if (!filterAttribValGot[thisPosition]) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == 'sv_ALL_sv') {
				return;
			}
			strSelectedVal = "'" + strSelectedVal + "'";

			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strSelectedVal;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelEnd);
			}
		}
		
		function addAttrName() {
			var objSelect = document.getElementsByName("CatMemberID");
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == '0') {
				return;
			}
			var strAttribVar = filterAttribMembersVars[strSelectedVal];
			if (!strAttribVar) {
				return;
			}
			
			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strAttribVar;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelEnd);
			}
		}
<?
		}
?>
	</script>
<?
	}
	
	//Basado en el tipo de filtro, invoca a la función correspondiente para construir la ventana de captura
	function generateAfterFormCode($aUser) {
		switch ($this->FilterType) {
			case qftpMarkerColors:
				$this->generateAfterFormCodeByCatalog($aUser);
				break;
		}
	}
	
	function generateAfterFormCodeByeBavelForm($aUser)
	{
		if ($this->eBavelFormID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("focus", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onfocus", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}
	
	function generateAfterFormCodeByCatalog($aUser)
	{
		if ($this->CatalogID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("focus", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onfocus", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//Obtiene la definición en JSon de este objeto y sus dependencias
	function getJSonDefinition() {
		$arrDef = array();
		$arrDef['questionID'] = $this->QuestionID;
		$arrDef['type'] = $this->FilterType;
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strFilterText = $this->TranslateVariableQuestionIDsByNumbers($this->FilterText);
		//En este caso, dependiendo del tipo de filtro se podría requerir hacer una conversión adicional
		switch ($this->FilterType) {
			case qftpMarkerColors:
				//Si es un filtro para controlar los colores de preguntas Simple Choice de catálogo que usan mapa, entonces las variables de
				//atributos del catálogo se deben delimitar entre { } para facilitar la búsqueda en el App y evitar tener que hacer procesos de
				//ordenamiento entre otras cosas, además de convertir de IDs a números de atributo para facilitar ubicar los valores
				$strFilterText = $this->TranslateCatMemberVarsForApp($strFilterText);
				break;
		}
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['filterText'] = $strFilterText;
		
		//Según el tipo de filtro, se envían diferentes datos para que el App realice las acciones correspondientes
		$arrData = array();
		switch ($this->FilterType) {
			case qftpMarkerColors:
				//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				$arrData['color'] = $this->Color;
				break;
		}
		
		$arrDef['data'] = $arrData;

		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		if($gbDesignMode)
		{
			$arrDef['filterID'] = $this->FilterID;
			$arrDef['filterName'] = $this->FilterName;
		}
		return $arrDef;
	}
	//@JAPR
}

class BITAMQuestionFilterCollection extends BITAMCollection
{
	public $QuestionID;
	
	function __construct($aRepository, $aQuestionID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
	}
	
	static function NewInstance($aRepository, $aQuestionID, $anArrayOfFilterIDs = null, $anArrayOfFilterTypes = null)
	{
		//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		if( (is_null($anArrayOfFilterIDs) || count($anArrayOfFilterIDs)<=0) && (is_null($anArrayOfFilterTypes) || count($anArrayOfFilterTypes)<=0) )
		{
			$anInstance =& BITAMGlobalFormsInstance::GetQuestionFilterCollectionByQuestionWithID($aQuestionID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		
		//@JAPR
		
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aQuestionID);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$filter = "";
		$strAnd = '';
		if (!is_null($anArrayOfFilterIDs))
		{
			switch (count($anArrayOfFilterIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.FilterID = ".((int)$anArrayOfFilterIDs[0]);
					$strAnd = ' AND ';
					break;
				default:
					foreach ($anArrayOfFilterIDs as $aFilterID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aFilterID; 
					}
					
					if ($filter != "")
					{
						$filter = " AND A.FilterID IN (".$filter.")";
					}
					$strAnd = ' AND ';
					break;
			}
		}
		
		if (!is_null($anArrayOfFilterTypes))
		{
			$strTmpFilter = '';
			switch (count($anArrayOfFilterTypes))
			{
				case 0:
					break;
				case 1:
					$strTmpFilter = " AND A.FilterType = ".((int)$anArrayOfFilterTypes[0]);
					break;
				default:
					foreach ($anArrayOfFilterTypes as $aValue)
					{
						if ($strTmpFilter != "")
						{
							$strTmpFilter .= ", ";
						}
						$strTmpFilter .= (int) $aValue; 
					}
					
					if ($strTmpFilter != "")
					{
						$strTmpFilter = " AND A.FilterType IN (".$strTmpFilter.")";
					}
					break;
			}
			
			if ($strTmpFilter != '') {
				$filter .= $strAnd.$strTmpFilter;
				$strAnd = ' AND ';
			}
		}
		
		$sql = "SELECT B.SurveyID, A.QuestionID, A.FilterID, A.FilterType, A.FilterName, A.FilterText, A.FilterLevels, B.CatalogID, A.Color 
			FROM SI_SV_QuestionFilter A 
				INNER JOIN SI_SV_Question B ON A.QuestionID = B.QuestionID 
			WHERE A.QuestionID = ".$aQuestionID.$filter." 
			ORDER BY FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter, SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		if( (is_null($anArrayOfFilterIDs) || count($anArrayOfFilterIDs)<=0) && (is_null($anArrayOfFilterTypes) || count($anArrayOfFilterTypes)<=0) )
		{
			BITAMGlobalFormsInstance::AddQuestionFilterCollectionByQuestionWithID($aQuestionID, $anInstance);
		}
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Obtiene la colección de filtros de preguntas pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección para 
	cada forma además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance pero con todos
	los parámetros en null excepto el SectionID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrQuestionFiltersByQuestionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE B.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE B.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = "SELECT B.SurveyID, A.QuestionID, A.FilterID, A.FilterType, A.FilterName, A.FilterText, A.FilterLevels, B.CatalogID, A.Color 
			FROM SI_SV_QuestionFilter A 
				INNER JOIN SI_SV_Question B ON A.QuestionID = B.QuestionID 
			{$filter} 
			ORDER BY A.FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QuestionFilter, SI_SV_Question ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$objQuestionFilter = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$anInstance->Collection[] = $objQuestionFilter;
			
			$intQuestionID = $objQuestionFilter->QuestionID;
			if ( !isset($arrQuestionFiltersByQuestionID[$intQuestionID]) ) {
				$arrQuestionFiltersByQuestionID[$intQuestionID] = array();
			}
			$arrQuestionFiltersByQuestionID[$intQuestionID][] = $objQuestionFilter;
			
			$aRS->MoveNext();
		}
		
		//Finalmente procesa el array de preguntas para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché (en este punto se
		//asume que este método se ejecutó posterior a la carga masiva de todos los padres, así que se usarán las colecciones que ya deberían estar cachadas)
		if (isset(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey)) {
			$arrAllQuestionFilterCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey as $objQuestionCollection) {
				foreach ($objQuestionCollection->Collection as $objQuestion) {
					$intQuestionID = $objQuestion->QuestionID;
					//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
					$arrAllQuestionFilterCollection[$intQuestionID] = new $strCalledClassColl($aRepository, $intQuestionID);
					//$objQuestionFilterCollection = new $strCalledClassColl($aRepository, $intQuestionID);
					if ( isset($arrQuestionFiltersByQuestionID[$intQuestionID]) ) {
						$arrAllQuestionFilterCollection[$intQuestionID]->Collection = $arrQuestionFiltersByQuestionID[$intQuestionID];
					}
					BITAMGlobalFormsInstance::AddQuestionFilterCollectionByQuestionWithID($intQuestionID, $arrAllQuestionFilterCollection[$intQuestionID]);
				}
			}
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}
		
		return BITAMQuestionFilterCollection::NewInstance($aRepository, $aQuestionID);
	}
	
	function get_Parent()
	{
		return BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
	}
	
	function get_Title()
	{	
		return translate("Filters");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=Question&QuestionID=".$this->QuestionID;
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=QuestionFilter&QuestionID=".$this->QuestionID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FilterID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>