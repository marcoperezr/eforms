<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();
//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/model.inc.php");
require_once("../model_manager/modeldata.inc.php");
require_once("../model_manager/modeldimension.inc.php");

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	$aModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->ModelID);
	chdir($strOriginalWD);
	
	$aFactKeyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->FactKeyDimID);
	chdir($strOriginalWD);

	//Llave subrogada de la dimension FactKey
	$factkeyDimField = $aFactKeyInstanceDim->Dimension->TableName."KEY";
	
	//Campo de descripcion que en si va a ser el valor de la llave subrogada
	$fieldDescription = $aFactKeyInstanceDim->Dimension->FieldDescription;
	
	//Tabla de la dimension
	$tableName = $aFactKeyInstanceDim->Dimension->TableName;

	//Tablas de Hechos
	$tablaHechos = $aModel->nom_tabla;
	
	//Actualizar la tabla de hechos en la dimension FactKey con el valor del campo FactKey de la encuesta
	$sql = "UPDATE ".$tablaHechos." SET ".$factkeyDimField." = FactKey";

	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		echo(translate("Error accessing")." ".$tablaHechos." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
	}
	else
	{
		echo('UPDATE: '.$sql.'<br>');
	}

	//Actualizamos la tabla de dimension FactKey con los valores que hay en la tabla de hechos mientras dichos valores
	//no se encuentren en la tabla de hechos	
	$sql = "INSERT INTO ".$tableName." (".$factkeyDimField.", ".$fieldDescription.") 
	SELECT A.FactKey, A.FactKey FROM ".$tablaHechos." A WHERE A.FactKey NOT IN 
	(
		SELECT B.".$factkeyDimField." FROM ".$tableName." B 
	)";
	
	if($aRepository->DataADOConnection->Execute($sql) === false)
	{
		echo(translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
	}
	else
	{
		echo('INSERT->SQL: '.$sql.'<br>');
	}
}
?>