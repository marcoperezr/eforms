<?php
//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
//Funciones para mandar mensajes a la salida y al archivo de Log exclusivamente de los procesos de request que envían el parámetro DebugLog (originalmente utilizado para depurar los procesos
//del Agente de eForms con Sabormex, los cuales estaban entrando a un TimeOut inexplicable de 10 minutos y por tanto se moría el request antes de que terminara y pudiera generar el log de
//proceso de manera normal)
//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
function getMicroDateTimeLocal($sMicrotime = '') {
	if ( $sMicrotime ) {
		$microTime = $sMicrotime;
	}
	else {
		$microTime = microtime();
	}
	$date_array = explode(" ",$microTime);
	$date = date("Y-m-d H:i:s",@$date_array[1]).str_replace("0.", ".", @$date_array[0]);
	return $date;
}

//Función que escribe en un archivo log sin necesidad de tener cargada utils.inc.php, automáticamente valida si se recibió el parámetro DebugLog, ya que sólo así escribe en el log indicado
//por dicho parámetro
function ECHOStringLocal($sText, $iLnBefore = 1, $iLnAfter = 0, $sStyle = "") {
	if ( isset($_REQUEST['DebugLog']) && trim((string) @$_REQUEST['DebugLog']) != '' ) {
		if ( isset($_REQUEST['DebugBreak']) && trim((string) @$_REQUEST['DebugBreak']) == '1' ) {
			$ln = "\r\n";
			if ($iLnBefore) {
				echo(nl2br(str_repeat($ln, $iLnBefore)));
			}
			if ($sStyle) {
				echo("<span style=\"".$sStyle."\">");
			}
			//@JAPR 2018-05-08: Optimizada la carga de eForms (#G87C4W)
			echo(nl2br(getMicroDateTimeLocal()." - ".$sText));
			//@JAPR
			if ($sStyle) {
				echo("</span>");
			}
			if ($iLnAfter) {
				echo(nl2br(str_repeat($ln, $iLnAfter)));
			}
		}
		
		$sFilePath = getcwd().'\\log/';
		$sFileName = $sFilePath.((string) @$_REQUEST['DebugLog']);
		//@JAPR 2018-05-08: Optimizada la carga de eForms (#G87C4W)
		@error_log("\r\n".str_ireplace("<br>", '', getMicroDateTimeLocal()." - ".$sText), 3, $sFileName);
		//@JAPR
	}
}
//@JAPR

//@JAPR 2012-10-24: Agregados headers para permitir que las peticiones por XML (proceso de sincronización desde Browser) se completen correctamente
header("Access-Control-Allow-Origin: *");
//@JAPR 2012-12-13: Agregados los headers X-FileName y Content-Type para soportar upload crossDomain de fotos y documentos
header("Access-Control-Allow-Headers: X-Requested-With, X-File-Name, Content-Type, User-Agent, Referer, Origin");
header("Access-Control-Allow-Methods: GET, POST");

//@JAPR 2019-05-30: Corregido un bug, no se puede agregar un header si ya se había mandado una respuesta, ya que a partir de cierto evento comenzó a generar un 
//Internal Server Error (HTTP 500) si se hacía de esa manera, así que se validará que los headers sean excluyentes al parámetro DebugBreak, además de atrapar toda salida
//al buffer no atrapada para regresarla como una propiedad warningNotice de la respuesta en caso de haber algún texto (#7UZT8C)
if ( !isset($_REQUEST["DebugBreak"]) ) {
	//Debe iniciar el buffer de salida para evitar que algún warning o notice provoquen el Internal Server Error
	ob_start();
}

//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
ECHOStringLocal("Starting request", 1, 1, "color:purple;");
$strRequestParamsGET = (string) @var_export($_GET, true);
ECHOStringLocal("GET Params: ".$strRequestParamsGET);

//@JAPR 2012-06-26: Integrado servicio de "Hello" agregado por MBarragan en la V2
$return = array();
if (isset($_GET['serverVerifyMessage'])) 
{
	//$return = array();
	$return['hellomsg'] = 'Hello';
	//@MABH20121210
	$return['serverSurveyDate'] = date("Y-m-d");
	$return['serverSurveyHour'] = date("H:i:s");
	
	header("Content-Type: text/javascript");
	if (isset($_GET['jsonpCallback'])) 
	{
		echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
	} 
	else 
	{
		echo(json_encode($return));
	}
	exit();
}
//@JAPR

//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
$strOriginalWD = getcwd();

chdir($strOriginalWD);
require_once('config.php');
require_once("repository.inc.php");
require_once('appuser.inc.php');
//@JAPR 2013-07-11: Agregado el agente de eForms
require_once('eFormsAgentConfig.inc.php');
//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
ECHOStringLocal("Iniciando el proceso antes del session_start", 1, 1, "color:purple;");
//@JAPR
session_start();
//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
//y no hasta que ha terminado todo el proceso (#P5SE4W)
ECHOStringLocal("Después del session_start", 1, 1, "color:purple;");
//@JAPR

//@JAPR 2013-05-20: Agregado el parámetro para simular recibir como POST los datos
$blnConvertGETToPOST = getParamValue('POSTParams', 'both', "(boolean)");
if ($blnConvertGETToPOST) {
	foreach ($_GET as $strKey => $aValue) {
		if (!isset($_POST[$strKey])) {
			$_POST[$strKey] = $aValue;
		}
	}
}
//@JAPR

$blnDebugSave = getParamValue('debugSave', 'both', "(boolean)");
if ($blnDebugSave)
{
	ob_start();
	//echo("Post Data: <br>");
	echo('$POST = ');
	var_export($_POST);
	echo(";\r\n");
	//echo("Get Data: <br>");
	echo('$GET =');
	var_export($_GET);
	echo(";\r\n");
	$strData = trim(ob_get_contents());
	ob_end_clean();
	$statlogfile = "./uploaderror/ESurvey_SyncData_".date('YmdHis').".log";
  	@error_log($strData, 3, $statlogfile);
	header("Content-Type: text/plain");
	echo("Data file was stored in the server");
	exit();
}

//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
@setAppVersion();
//@JAPR

//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
global $arrMasterDetSections;
global $arrMasterDetSectionsIDs;
//@JAPR 2014-05-28: Agregado el tipo de sección Inline
global $arrInlineSections;
global $arrInlineSectionsIDs;
//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $dieOnError;
$dieOnError = false;
global $gblEFormsNA;
//@JAPR 2013-07-11: Movidas estas configuraciones al archivo eFormsAgentConfig.inc.php para permitir incluirlo tanto en el servicio como en el propio
//agente y que en ambos casos tome las configuraciones sin tener que mantenerlas en varios lugares
//@JAPR 2013-06-21: Agregado el agente de eForms
/*
global $blnIsAgentEnabled;
$blnIsAgentEnabled = true;
//Esta es una lista de bases de datos que se deben excluir del proceso del agente, de tal forma que trabajarán como antes grabando la información
//inmediatamente cuando es sincronizada al servidor. El formato es $arrDBsExcludedFromAgent['FBM_BMD_####'] = 1;
global $arrDBsExcludedFromAgent;
$arrDBsExcludedFromAgent = array();
//Esta es una lista de bases de datos que son las únicas que van a usar el agente. Si no contiene elementos entonces se asume que todas las bases
//de datos usarán el agente El formato es $arrDBsExcludedFromAgent['FBM_BMD_####'] = 1;
//Las bases de datos que no se encuentren en esta lista, funcionarán tal como si estuviera en el array $arrDBsExcludedFromAgent
global $arrDBsEnabledForAgent;
$arrDBsEnabledForAgent = array();
*/
//@JAPR 2012-05-21: Validado que las acciones sólo se generen una vez cuando existen secciones Maestro - Detalle o dinámicas (las preguntas que
//se encuentra dentro de ellas pueden grabar múltiples acciones, pero las que están fuera de ellas sólo pueden grabar una acción)
global $arrProcessedActionQuestions;
$arrProcessedActionQuestions = array();
global $arrQuestionOptions;
$arrQuestionOptions = array();
//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
global $usePhotoPaths;
//Por default no se usan photo paths, esto es, se recibe directamente el B64, sólo con la v4.01000 o posterior se pueden recibir rutas de
//imagenes por lo que dentro de eSurveyServiceMod.inc.php se cambiará esta bandera a true para variar como funciona SavePhoto
$usePhotoPaths = false;
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $useRecoveryPaths;
//Esta bandera indica que en lugar de usar la carpeta SyncData, se utilizaría SyncErrors (sólo aplica cuando $usePhotoPaths ==true)
$useRecoveryPaths = false;
//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
global $eBavelAppCodeName;
global $arrayPDF;
global $arrActionItems;
$arrActionItems = array();
global $arrNumericQAnswersColl;
$arrNumericQAnswersColl = array();
//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
global $blnEBavelSupLoaded;
$blnEBavelSupLoaded = false;
global $arrEBavelSupervisors;
$arrEBavelSupervisors = array();
//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
global $blnUseMaxLongDummies;
$blnUseMaxLongDummies = false;
//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
global $blnWebMode;
//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
global $arrEditedFactKeys;
$arrEditedFactKeys = array();
//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
global $strLastUpdateDate;
global $strLastUpdateEndDate;
global $strLastUpdateTime;
global $strLastUpdateEndTime;
$strLastUpdateDate = '';
$strLastUpdateEndDate = '';
$strLastUpdateTime = '';
$strLastUpdateEndTime = '';
//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
global $strPDFFileNamePrefix;
$strPDFFileNamePrefix = '';
global $intPDFFilaNamePrefixCont;
$intPDFFilaNamePrefixCont = 0;
//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
global $arrDynamicCatalogAttributes;
//@JAPR 2014-05-29: Agregado el tipo de sección Inline
global $arrInlineCatalogAttributes;
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $gstrOutboxFilePath;
$gstrOutboxFilePath = '';
global $gstrOutboxFileName;
$gstrOutboxFileName = '';
//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
global $arrAdditionalEMails;
$arrAdditionalEMails = array();
//@JAPR 2014-04-09: Agregada la variable para identificar el envío de reportes desde la captura o desde la opción para forzarlo en la lista de reportes
global $blnSendPDFEntries;
$blnSendPDFEntries = false;
//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
global $blnMultiPass;
$blnMultiPass = false;
//@JAPR

if (!function_exists('getFormsInfo'))
{
//@JAPR 2016-11-24: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
//@JAPRWarning: Esta función parece estar descontinuada, ya no se utiliza por lo menos para el proceso de este archivo que requiere $strRequest
function getFormsInfo($kpiUser, $kpiPassword) {
    global $server;
    global $server_user;
    global $server_pwd;
    global $masterdbname;
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	global $blnMultiPass;
	
    @require_once('../../../fbm/conns.inc.php');

    if (strlen($kpiUser) == 0) {
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Error, user email empty';
        return $return;
        //return 'Error, user email empty';
    }

    $mysql_Connection = @mysql_connect($server, $server_user, $server_pwd);

    if (!$mysql_Connection) {
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Connection error!';
        return $return;
        //return 'Error, connection error! '.mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
        //return 'Connection error! ';
    }

    $db_selected = @mysql_select_db($masterdbname, $mysql_Connection);

    if (!$db_selected) {
        //return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
        //return 'Error, could not select '.$masterdbname;
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Error, could not select ' . $masterdbname;
        return $return;
    }
	
    //Obtener el UserID de la tabla de SAAS_Users
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
    $aSQL = sprintf("SELECT userID, Email, AccPassword, ActiveAcc FROM saas_users WHERE Email = '%s'", mysql_real_escape_string($kpiUser, $mysql_Connection));
	
    $result = @mysql_query($aSQL, $mysql_Connection);
	//@JAPR 2015-05-04: Agregado soporte para php 5.6
    if (!$result || @mysql_num_rows($result) == 0) {
        //return 'Error, data not found: '.$aSQL."\n".mysql_error(); //conchita 1-nov-2011 ocultar mensajes sql
        //return 'Error, email or password incorrect';
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Error, email or password incorrect';
        return $return;
    }

    //Obtener resultado de la consulta
    $row = @mysql_fetch_assoc($result);
    $intUserID = $row["userID"];
    $strUserEmail = $row["Email"];
    $intUserActive = intval($row["ActiveAcc"]);
    $strEncryptedPass = $row["AccPassword"];
	
    //Verificar que la cuenta se encuentre activa
    if ($intUserActive == 2) {
        //return 'Error, the user account is not active'."\n".mysql_error();
        //return 'Error, the user account is not active';
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Error, the user account is not active';
        return $return;
    }
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Primero obtiene la configuración del uso de múltiples passwords para saber si ya se encuentra o no habilitada, esta configuración puede ser global, por repositorio o por usuario
	//según los parámetros recibidos en el request
	$aSQL = "SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = -2 AND settingValue > 0";
	//Si se recibió un repositorio, primero identifica el DataBaseID correspondiente
	$strProjectName = (isset($_REQUEST["projectName"]) ? trim($_REQUEST["projectName"]) : "");
	if ($strProjectName) {
		$intDBID = 0;
		$sql = "SELECT c.DatabaseID 
			FROM saas_databases c 
			WHERE c.Repository = '{$strProjectName}'";
		$result = @mysql_query($sql, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$intDBID = (int) @$row["DatabaseID"];
		}
		
		if ($intDBID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = {$intDBID} AND settingUserID = -2 AND settingValue > 0";
		}
	}
	
	//Si se recibió el EMail (que todo request siempre lo debe recibir) primero identifica el UserID correspondiente
	if ($strUserEmail) {
		$intSAASUserID = 0;
		$sql = "SELECT UserID 
			FROM saas_users 
			WHERE Email = '{$strUserEmail}'";
		$result = @mysql_query($sql, $mysql_Connection);
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$intSAASUserID = (int) @$row["UserID"];
		}
		if ($intSAASUserID) {
			$aSQL .= "
				UNION 
				SELECT settingValue FROM saas_settings WHERE settingID = ".kpisMultiPasswordEnabled." AND settingDBID = -2 AND settingUserID = {$intSAASUserID} AND settingValue > 0";
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Query to check Multi-Passwords enabled (getFormsInfo): {$aSQL}");
	}
	
	$result = @mysql_query($aSQL, $mysql_Connection);
	if ($result && @mysql_num_rows($result) > 0) {
		$row = @mysql_fetch_assoc($result);
		$blnMultiPass = (bool) @$row["settingValue"];
	}
	//@JAPR
	
    //Verificar que el password coincida
    $strUserPass = BITAMDecryptPassword($strEncryptedPass);
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	if ($kpiPassword !== $strUserPass && !$blnMultiPass) {
		//return 'Error, incorrect user or password'."\n".mysql_error();
		//return 'Error, incorrect user or password';
		$return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$return['errmsg'] = 'Error, incorrect user or password';
		return $return;
	}
	
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
	//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
	//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
	$strDBIDWhere = '';
	if ($blnMultiPass) {
		//Si se recibió el AppName se le dará prioridad a ese parámetro para identificar al repositorio (Apps de versión 6.0+)
		$strAppName = (isset($_REQUEST["appName"]) ? trim($_REQUEST["appName"]) : "");
		if (strlen($strAppName) > 0)
		{
			$aSQL = <<<EOS
SELECT c.DatabaseID 
FROM saas_eforms_apps a, saas_users b, saas_databases c 
WHERE a.appname = '$strAppName' 
AND a.masterAccount = b.EMail 
AND b.userID = c.UserID
EOS;
			$restemp = mysql_query($aSQL, $mysql_Connection);
			if ($restemp && mysql_num_rows($restemp) > 0)
			{
				$rowtemp = mysql_fetch_assoc($restemp);
				$intDBID = intval($rowtemp["DatabaseID"]);
				if ($intDBID > 0)
				{
					$strDBIDWhere = " AND A.DatabaseID = $intDBID ";
				}
			}
		}
		else {
			//En caso de no recibir el AppName, se verificará si se recibió el ProjectName (Apps de 4.0+ hasta antes de 6.0, aunque 6.0+ sigue enviando ese parámetro)
			$strProjectName = (isset($_REQUEST["projectName"]) ? trim($_REQUEST["projectName"]) : "");
			if (strlen($strProjectName) > 0) {
				$strProjectName = strtolower($strProjectName);
				$aSQL = <<<EOS
SELECT c.DatabaseID 
FROM saas_databases c 
WHERE c.Repository = '$strProjectName'
EOS;
				$restemp = mysql_query($aSQL, $mysql_Connection);
				if ($restemp && mysql_num_rows($restemp) > 0)
				{
					$rowtemp = mysql_fetch_assoc($restemp);
					$intDBID = intval($rowtemp["DatabaseID"]);
					if ($intDBID > 0)
					{
						$strDBIDWhere = " AND A.DatabaseID = $intDBID ";
					}
				}
			}
		}
	}
	
	$blnValideFormsAcc = false;
    //Obtener el nombre del repositorio de KPA al cual tiene acceso
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	$aSQL = sprintf("SELECT A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.Repository, A.DatabaseID, A.TemplateID, A.KPIServer, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse 
                                        FROM saas_dbxusr B, saas_databases A 
					WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
					AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	// SBF_2016-11-14: si ya está activa la funcionalidad de contraseña por repositorio, pedir en el query la contraseña de saas_dbxusr, si es que existe
	if ($blnMultiPass)
	{
		$aSQL = sprintf("SELECT A.KPIServer, A.ArtusPath, A.FormsPath, A.FormsVersion, A.Repository, A.DatabaseID, A.TemplateID, A.KPIServer, A.DBServer, A.DBServerUsr, A.DBServerPwd, A.DataWarehouse, COALESCE(B.AccPassword, C.AccPassword) AS PasswordEnc 
											FROM saas_dbxusr B, saas_databases A, saas_users C 
						WHERE B.UserID = %d AND (B.ArtusLogin = 1 OR B.ArtusLogin = 2) 
						AND B.UserID = C.UserID
						AND B.DatabaseID = A.DatabaseID AND A.Status = 1 $strDBIDWhere ORDER BY B.ArtusLogin", $intUserID);
	}
	
    $result2 = @mysql_query($aSQL, $mysql_Connection);
	if (!$result2 || @mysql_num_rows($result2) == 0) {
		$return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$return['errmsg'] = 'Error, no repository access found';
		return $return;
	}
	
	//@JAPR 2015-05-04: Agregado soporte para php 5.6
    $row2 = @mysql_fetch_assoc($result2);
    //@JAPR 2012-10-25: Validado que si la cuenta tiene acceso a múltiples proyectos, regrese los datos del primero que esté configurado
    //como de eForms
    do {
	    $intTemplateID = (int) @$row2["TemplateID"];
		//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	    $strServer = (string) @$row2["DBServer"];
		$strServerUsr = (string) @$row2["DBServerUsr"];
		$strServerPwd = (string) @$row2["DBServerPwd"];
		$strDWH = (string) @$row2["DataWarehouse"];
		//@JAPR
	    $strKPIServer = is_null($row2["KPIServer"]) ? '' : $row2["KPIServer"];
	    $strArtusPath = is_null($row2["ArtusPath"]) ? '' : $row2["ArtusPath"];
	    $strFormsPath = is_null($row2["FormsPath"]) ? '' : $row2["FormsPath"];
	    $strFormsVersion = is_null($row2["FormsVersion"]) ? '' : $row2["FormsVersion"];
	    //Se considera un template válido de eForms si pertenece al template propio de eForms, o bien si aunque no sea de dicho template está
	    //configurado hacia un path de eForms (en esos casos sólo pudo ser desde la v3 o posterior, por lo que es seguro que no es válido un
	    //path vacio para que utilice por default el de v2) 
	    $blnValideFormsAcc = ($intTemplateID == 5 || $strFormsPath != '');
		//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
		//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
		//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
		// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
		if ($blnMultiPass)
		{
			$strUserPass = BITAMDecryptPassword($row2["PasswordEnc"]);
			$blnValideFormsAcc = $blnValideFormsAcc && ($kpiPassword === $strUserPass);
		}
	    $row2 = @mysql_fetch_assoc($result2);
    } while($row2 && !$blnValideFormsAcc);
    
	//@JAPR 2016-11-23: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//Si ya se encuentra activado el soporte para múltiples passwords, se debe verificar si se recibió el parámetro "project" o alguno que identifique al request como específico para un
	//espacio de trabajo, si es así entonces consulta el password directo de dicho espacio, si no se recibió entonces deberá inferir el espacio a partir del password proporcionado y no
	//comprobaría el password en este punto, eso sólo lo haría si aún no estuviera habilitada la funcionalidad de múltiples passwords
	// SBF 2016-11-14: Checar si está activa la funcionalidad de contraseña por repositorio y hacer validaciones correspondientes
	if ($blnMultiPass && !$blnValideFormsAcc)
	{
        //return 'Error, incorrect user or password'."\n".mysql_error();
        //return 'Error, incorrect user or password';
        $return['error'] = true;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
        $return['errmsg'] = 'Error, incorrect user or password';
        return $return;
	}
    //@JAPR
	
    if ($strKPIServer == '') {
        $strKPIServer = 'kpionline.bitam.com';
    }
    if ($strArtusPath == '') {
        $strArtusPath = 'artus/genvi';
    }
    if ($strFormsPath == '') {
        $strFormsPath = 'ESurveyTMP';
    }
    if ($strFormsVersion == '') {
        $strFormsVersion = '1.1';
    }
    
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	//Servicio de eForms alternativo al que ciertas Apps (condicionado por versión) se conectarán en lugar del especificado en SAAS_Databases
	//Valida si existen para esta cuenta las tablas de configuración de eForms, si existen verifica si tienen las entradas que la forzarían
	//a redireccionar a un servicio alternativo y si la versión que hace este request es >= que la versión configurada para forzar a esta
	//redirección alterna (esto debido a que para probar con Wixo simultáneamente, quienes tenían v2, se requería probar a la par con una v3
	//pero como sólo hay una configuración de servicio no hubiera sido posible, por lo tanto para futuros casos similares se ofrecerá esta
	//alternativa de redirección controlada vía la versión de eForms que hace el request)
	$appVersion = '';
	if (isset($_POST['appVersion'])) {
		$appVersion = (string) @$_POST['appVersion'];
	}
	elseif (isset($_GET['appVersion'])) {
		$appVersion = (string) @$_GET['appVersion'];
	}
	if (is_null($appVersion) || trim($appVersion) == '' || !is_numeric($appVersion)) {
		$appVersion = '1.0';
	}
	
	//Realiza la conexión al DWH
	$blnGetAlteFormsService = false;
	$mysql_Connection_DWH = null;
	if (strlen($strServer) > 0)
	{
		//@JAPR 2015-05-04: Agregado soporte para php 5.6
		$mysql_Connection_DWH = @mysql_connect($strServer, $strServerUsr, BITAMDecryptPassword($strServerPwd));
		if ($mysql_Connection_DWH) 
		{
			$blnGetAlteFormsService = true;
		}
	}
	else
	{
		$mysql_Connection_DWH = $mysql_Connection;
		$blnGetAlteFormsService = true;
	}
	
	//Si se pudo realizar la conexión al server del DWH, obtiene las configuraciones de eForms
	//En este punto no importa hacer un mysql_select_db porque ya no se accesará nada mas de la FBM000
	$strAlternativeEFormsService = '';
	$strAlternativeEFormsServiceVersion = '';
	$strReqAppVersionForAltService = '3.00000';
	if ($blnGetAlteFormsService && !is_null($mysql_Connection_DWH) && trim($strDWH) != '') {
		//@JAPR 2015-05-04: Agregado soporte para php 5.6
		$blnGetAlteFormsService = @mysql_select_db($strDWH, $mysql_Connection_DWH);
		if ($blnGetAlteFormsService) {
		    $aSQL = "SELECT SettingID, SettingName, SettingValue 
		    	FROM SI_SV_Settings 
		    	WHERE SettingID IN (5, 6, 7)";
			//@JAPR 2015-05-04: Agregado soporte para php 5.6
		    $aRS = @mysql_query($aSQL, $mysql_Connection_DWH);
		    if ($aRS && @mysql_num_rows($aRS) != 0) {
				$aRow = @mysql_fetch_assoc($aRS);
		    	while($aRow) {
		    		$intSettingID = (int) @$aRow["SettingID"];
		    		$strSettingValue = (string) @$aRow["SettingValue"];
		    		switch ($intSettingID) {
		    			case 5:
		    				$strAlternativeEFormsService = $strSettingValue;
		    				break;
		    			case 6:
		    				$strAlternativeEFormsServiceVersion = $strSettingValue;
		    				break;
		    			case 7:
		    				$strReqAppVersionForAltService = $strSettingValue;
		    				break;
		    		}
		    		
		    		$aRow = @mysql_fetch_assoc($aRS);
		    	}
		    }
		}
	}
    
	//Si la versión del App del request es >= que la mínima requerida para validar servicios alternativos de eForms y si existe dicho servicio,
	//entonces lo regresa, de lo contrario regresará el servicio configurado originalmente
	if ((float) $appVersion >= (float) $strReqAppVersionForAltService && $strAlternativeEFormsService != '') {
	    $formsFullPath = $strAlternativeEFormsService;
	    $formsVersion = $strAlternativeEFormsServiceVersion;
	}
	else {
	    $formsFullPath = $strKPIServer . '/' . $strArtusPath . '/' . $strFormsPath;
	    $formsVersion = $strFormsVersion;
	}
    //@JAPR

    unset($row);
    unset($mysql_Connection);

    $return[0] = $formsFullPath;
    $return[1] = $formsVersion;
    return $return;
}
}

$appVersion = getParamValue('appVersion', 'both', '(string)', true);
if (is_null($appVersion))
{
	$appVersion = esvOriginalVersion;
}
//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
$blnWebMode = getParamValue('webMode', 'both', '(int)');
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
if ($blnWebMode) {
	//Desde browser siempre debe reportar el error para que se reintente la sincronización
	$dieOnError = true;
}
//@JAPR

$arrCatalogVersion = array();
if (isset($_GET["catalogID_Version"]))
{
	$strCatalogVersion = $_GET["catalogID_Version"];
}
elseif (isset($_POST["catalogID_Version"]))
{
	$strCatalogVersion = $_POST["catalogID_Version"];
}
if (isset($strCatalogVersion) && is_array($strCatalogVersion))
{
	foreach ($strCatalogVersion as $strVersionData)
	{
		$arrVersionData = explode('_', $strVersionData);
		if (count($arrVersionData) >= 2 && is_numeric($arrVersionData[0]))
		{
			$arrCatalogVersion[$arrVersionData[0]] = $arrVersionData[1];
		}
	}
}

//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
global $gbUseOperationsMonitor;
global $giOperationID;

$gbUseOperationsMonitor = false;
$giOperationID = 0;
clearstatcache();
$strOperationsMonitorFile = '../../../fbm/operationsMonitor.inc.php';
if (file_exists($strOperationsMonitorFile)) {
	@require_once($strOperationsMonitorFile);
	if (function_exists("RegisterOperation")) {
		$gbUseOperationsMonitor = true;
	}
}
//@JAPR

//getSurveyDefinition: Descontinuada!!!
//saveAnswersMobile2: Graba el contenido de una captura de encuesta enviado desde los móviles. Devuelve texto plano porque se invoca mediante XML en lugar de JQuery (text)
//getSurveyMessage: Devuelve una lista de encuestas y agendas a las que tiene acceso el usuario + la fecha de última modificación de su definición (JSon)
//getSurveyList: Devuelve todas las definiciones de encuentras + su catálogo principal (JSon)
//getSurveysByAttribute: Devuelve la lista de encuestas a las que tiene acceso el usuario y que contengan por lo menos una pregunta que 
	//involucre al atributo especificado (text list with delimiters)
//getAgendaList: Devuelve la lista de Agendas configuradas para el usuario especificado (JSon / Text)

//@JAPR 2012-03-12: Agregado el uso del servicio como un servidor Web para cualquier tipo de aplicación
$strRequest = strtolower(getParamValue('action', 'both'));
//$strResponseFormat puede tomar los siguientes valores:
	//text (o vacio - default): Se regresa como una cadena de texto con separadores dependiendo del servicio solicitado
	//xml (no implementado aun): Se regresa en formato XML
	//json (para móviles): Se regresa como un array u objeto según el servicio, convirtiendo el texto a formato JSon
$strResponseFormat = strtolower(getParamValue('responseformat', 'both'));

//Procesa el request especificado, todos estos servicios deberán cumplir con el mismo formato para su respuesta
//		Formato de texto:
//Líneas separadas por SV_DATA_FIELD con las siguientes especificaciones (los [ ] no se regresan, son sólo para identificar el texto de la respuesta del comentario correspondiente):
/*	StatusCode: 
		[0] Si no hay error
		[ErrNum + SV_SRV_ERROR + ErrDesc] en caso de error
	Version:
		[#.#####] Número de versión del Web Service
		[SV_DELIMITER] sólo un separador
		[#.#####] Número de versión de producto que hizo el Request
	Datos:
		Puede ser cualquier tipo de información, generalmente separada por SV_DELIMITER en el caso de listas, pero realmente depende del
		actión enviado lo que se espera recibir
		
	Ejemplo:
		Respuesta cuando se pide la lista de surveys basada en un AttributeID (action='getSurveysByAttribute')
	"0_SV_FLD_1_SV_Survey1_SV_SEP_2_SV_Survey2_SV_SEP_n_SV_Surveyn"
		La misma respuesta vista como líneas:
	"Renglón1: 0
	 Renglón 2: Lista de pares:
	 	1,Survey1
		2,Survey2
		n,Surveyn
	"
*/
global $arrDefDimValues;
//@JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
global $gbGettingDynamicValues;
if ($strRequest != '')
{
	$blnDebug = getParamValue('debug', 'both', "(boolean)");
	
	//JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
	if ($strRequest == 'getimage') {
		require_once('eSurveyServiceMod.inc.php');
		
		//Este request especial se utiliza exclusivamente para regresar la ruta de una imagen solicitada desde el App vía PhP debido a problemas con CORS en caso de intentar utilizarla
		//posteriormente dentro de un Canvas si simplemente se hubiera utilizado la ruta absoluta, basta recibir este request y la función correspondiente leerá el resto de los parámetros
		//para regresar el contenido de la imagen y evitar de esa manera problemas con CORS
		return GetCORSSafeCatalogAttributeImage();
	}
	//@JAPR
	
	//@JAPR 2012-09-25: Modificado para leer estos parámetros por GET o POST indistintamente para soportar request de v4
    if (!isset($_GET['UserID']) && !isset($_POST['UserID'])) {
    	return generateErrorMessage(-400, 'You must enter a valid UserID and Password to access this resource', 'HTTP/1.0 401 Unauthorized');
    }
    //$kpiUser = $_GET['UserID'];
    $kpiUser = getParamValue('UserID', 'both', "(string)");
	$arrDefDimValues['UserEMail'] = $kpiUser;
	
    if (!isset($_GET['Password']) && !isset($_POST['Password'])) {
    	return generateErrorMessage(-401, 'You must enter a valid UserID and Password to access this resource', 'HTTP/1.0 401 Unauthorized');
    }
	//JAPR 2016-06-30: Almacenar el password encriptado (#VRNM0H)
	//Dado a que se puede usar el parámetro Password desde $_REQUEST, $_GET o $_POST, pero en todos los casos representa lo mismo y ahora puede venir encriptado, si se recibió encriptado
	//se va a sobreescribir en cada variable donde fue recibido, para no tener que ejecutar este mismo proceso en puntos posteriores que lo necesiten desencriptado que es como originalmente
	//llegaba en todos los casos (por ejemplo dentro de saveAnswersMobileV6)
	$blnEncrypted = getParamValue('encryptedPwd', 'both', "(boolean)");
    $kpiPass = getParamValue('Password', 'both', "(string)");
	if ($blnEncrypted) {
		//En este caso el password debió haber llegado encriptado, esto ocurre a partir de la versión esvEncryptedPasswords que es la primera que mandaba el parámetro "encryptedPwd", sin
		//embardo no es necesario validar contra appVersion precisamente por este hecho, sólo contra encryptedPwd. En este caso sobreescribirá los arrays de parámetros que vinieran con
		//este valor a la versión desencriptada del password
		$kpiPass = BITAMDecryptPassword(BITAMDecode($kpiPass));
		if (isset($_GET['Password'])) {
			$_GET['Password'] = $kpiPass;
		}
		if (isset($_POST['Password'])) {
			$_POST['Password'] = $kpiPass;
		}
		if (isset($_REQUEST['Password'])) {
			$_REQUEST['Password'] = $kpiPass;
		}
	}
    //@JAPR
	
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	if ($gbUseOperationsMonitor) {
		global $arrEventParams;
		$arrEventParams = array();
		$arrEventParams["ProductID"] = 6;
		$arrEventParams["ProductVersion"] = $appVersion;
		$arrEventParams["StartDate"] = date('Y-m-d H:i:s');
		$arrEventParams["Email"] = $kpiUser;
		$arrEventParams["ProcessID"] = GeteFormsProcessID($strRequest);
		$arrEventParams["AplicationServer"] = $_SERVER["SERVER_NAME"];
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
		$arrEventParams["Ruta"] = $strScriptPath;
		$arrEventParams["MemoryCon"] = memory_get_peak_usage(true);
		$arrEventParams["Description"] = GeteFormsProcessDesc($strRequest);
		//@JAPR 2015-03-25: Agregados los datos de geolocalización
		if (isset($_REQUEST["syncLat"])) {
			$arrEventParams["Latitude"] = $_REQUEST["syncLat"];
		}
		if (isset($_REQUEST["syncLong"])) {
			$arrEventParams["Longitude"] = $_REQUEST["syncLong"];
		}
		if (isset($_REQUEST["syncAcc"])) {
			$arrEventParams["Accuracy"] = $_REQUEST["syncAcc"];
		}
		//Se asignará al momento del cierre para no arriesgar asignar hasta después de hacer la conexión al repositorio
		//$arrEventParams["DBServer"] = "";
		//$arrEventParams["Repository"] = "";
		//$arrEventParams["UserID"] = 0;
		//Se asignará al momento del cierre para saber el máximo de memoria utilizado durante todo el proceso
		//$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
		//$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
		//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
		//y no hasta que ha terminado todo el proceso (#P5SE4W)
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Inserting Operation Monitor Entry");
		}
		//@JAPR
		$giOperationID = (int) @RegisterOperation($arrEventParams);
	}
    
	//@JAPR 2019-02-12: Modificado el comportamiento para permitir generar el Log del agente en tiempo de ejecución, enviando el parámetro para que se escriban los eventos mientras suceden
	//y no hasta que ha terminado todo el proceso (#P5SE4W)
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Identifying Project and user");
	}
	//@JAPR
    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
    	return generateErrorMessage(-402, $strPrjAndUser);
    }
	//@JAPR
	
    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
    $strRepositoryName = $arrayPrjUsr[0];
    $strUserName = $arrayPrjUsr[1];
	//@JAPR 2015-03-02: Agregado el Monitor de Operaciones
    $intFormsDatabaseID = (int) @$arrayPrjUsr[2];
    $strDBServer = (string) @$arrayPrjUsr[3];
    //@JAPR
	
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	if ($gbUseOperationsMonitor) {
		$arrEventParams["DBServer"] = $strDBServer;
		$arrEventParams["Repository"] = $strRepositoryName;
		$arrEventParams["DBid"] = $intFormsDatabaseID;
		//$arrEventParams["UserID"] = $theUser->UserID;
		$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
		//$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Updating Operation Monitor Entry");
		}
		@RegisterOperation($arrEventParams, $giOperationID);
	}
    
    $theRepositoryName = $strRepositoryName;
    $theUserName = $strUserName;
    $thePassword = $kpiPass;
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing the request");
	}
	
	//@JAPR 2014-08-25: Agregado el log de requests
	if ($blnSaveRequestsLog) {
		$requestDir = getcwd()."\\"."syncrequests";
		if (!file_exists($requestDir)) {
			@mkdir($requestDir);
		}
		$requestDir .= "\\".$strRepositoryName;
		if (!file_exists($requestDir)) {
			@mkdir($requestDir);
		}
		$requestDir .= "\\Requests.log";
		$strUserAgent = @var_export(@$_SERVER['HTTP_USER_AGENT'], true);
	    //@JAPR 2015-03-05: Agregada la fecha al log de requests
		@error_log(date('Y-m-d H:i:s')."- ".$strUserAgent."\r\n", 3, $requestDir);
		$strRequestVars = @var_export($_REQUEST, true);
		@error_log($strRequestVars."\r\n", 3, $requestDir);
	}
	//@JAPR
	
    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
    	return generateErrorMessage(-403, 'Project does not exists', 'HTTP/1.0 401 Unauthorized');
    }

    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
    	return generateErrorMessage(-404, translate("Can not connect to Repository, please contact your System Administrator."), 'HTTP/1.0 401 Unauthorized');
    }
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Updating required Metadata");
	}
	
	//@JAPR 2018-10-23: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Agregado el path específico por repositorio en los casos de los requests de Agentes, además de agregar el nombre específico de agente, se agregará el repositorio cuando ya se trate
	//del procesamiento directamente del request, ya que el agente en si no está ligado a un repositorio, sino que hasta que identifica una tarea ya puede asociar el proceso a dicho
	//repositorio, pero sería demasiado tarde para combinar las salidas de los registros previos del log, así que solo se asignará el repositorio para encapsular la salida del proceso
	//de la tarea identificada. A la fecha de implementación, solo había algunos archivos php que utilizaban logString, es seguro continuar utilizándola, aunque debe entenderse que cada vez
	//que se registre un logString dentro del procesamiento de la tarea, se sobreescribirá en el mismo path que el Agente que invocó a dicha tarea, que en el caso en que no se especificada
	//un nombre de archivo, se trataría del mismo $strDefaultLogStringFile pero dentro del path final que incluye al repositorio, no en el path que solo incluye al $strAgentName
	//El punto que asignará $strLogRepositoryName será siempre eSurveyService.php, el cual es el punto de ingreso del WebService de eForms y el que realiza la conexión al repositorio
	//Adicionalmente los agentes pueden asignar este parámetro cuando deciden iniciar el procesamiento a nivel de repositorio de una tarea, o bien limpiarlo cuando solo registrarán sus
	//propios logs genéricos
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Se estandarizó la función que genera el path de los logs, este punto continuará asignando el repositorio como se indicó arriba, pero ya no directamente a la variable sino mediante la
	//nueva función estandarizada para concentrar las variables globales en un solo punto
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$strLogPath = GeteFormsLogPath($theRepositoryName);
	//@JAPR
	
	//@JAPR 2014-11-19: Agregada la dimensión Agenda
	@AutoUpdateRequiredMetadata($theRepository);
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	/*//@JAPR 2016-10-11: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
	//Debido a que este cambio se realiza independiente de la Metadata de BITAM, ya que fue un ajuste masivo involucrando a diferentes productos y que teóricamente agregaría las columnas
	//y actualizaría defaults en todos los repositorios, sin embargo eForms no puede depender de ello así que él mismo se encarga de aplicarlo para no provocar errores al usarlo
	//Había faltado validar que el servicio debe crear estas columnas, ya que el Agente de eForms invoca directamente al servicio y estaba reportando múltiples errores si no se había
	//actualizado la metadata
	$sql = "ALTER TABLE SI_USUARIO ADD EFORMS_ADMIN INT NULL";
	$theRepository->ADOConnection->Execute($sql);
	
	$sql = "ALTER TABLE SI_USUARIO ADD EFORMS_DESKTOP INT NULL";
	$theRepository->ADOConnection->Execute($sql);
	
	$sql = "ALTER TABLE SI_USUARIO ADD KPI_DEFAULT INT NULL";
	$theRepository->ADOConnection->Execute($sql);*/
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Loading the BITAMUser instance");
	}
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
    if (is_null($theUser)) {
    	return generateErrorMessage(-405, 'Your User Name does not exist or your Password is incorrect', 'HTTP/1.0 401 Unauthorized');
    }
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Loading the BITAMAppUser instance");
	}
    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
    if (is_null($theAppUser)) {
    	return generateErrorMessage(-406, 'Your User Name is not registered in the application', 'HTTP/1.0 401 Unauthorized');
    }
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Finished loading users");
	}
	
    //Establecer el lenguaje en una variable de sesión
    //0=DEFAULT, 1=Spanish, 2=English
    if ($theUser->UserLanguageID <= 0 || $theUser->UserLanguageID > 8) {
        $userLanguageID = 2;
        $userLanguage = 'EN';
    } else {
        $userLanguageID = $theUser->UserLanguageID;
        $userLanguage = $theUser->UserLanguageTerminator;
    }
	
    //Verificar si se encuentra en modo FBM
    $sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";
    $aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);
    $valueFBMMode = 0;
    if ($aRSFBM && !$aRSFBM->EOF) {
        if ($aRSFBM->fields["ref_configura"] !== null && trim($aRSFBM->fields["ref_configura"]) !== "") {
            $valueFBMMode = (int) trim($aRSFBM->fields["ref_configura"]);
        }
    }
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    /*session_register("PABITAM_RepositoryName");
    session_register("PABITAM_UserName");
    session_register("PABITAM_UserRole");
    session_register("PABITAM_Password");
    session_register("PABITAM_UserID");
    session_register("PAtheUserID");
    session_register("PAtheAppUserID");
    session_register("PAgenerateLog");
    session_register("SV_ADMIN");
    session_register("SV_USER");
    session_register("PAuserLanguageID");
    session_register("PAuserLanguage");
    session_register("PAchangeLanguage");
    session_register("PAArtusWebVersion");
    session_register("PAFBM_Mode");*/
	
    $_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
    $_SESSION["PABITAM_Password"] = $theUser->Password;
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
    $_SESSION["PAtheUserID"] = $theUser->UserID;
    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
    $_SESSION["PAgenerateLog"] = $generateLog;
    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
    $_SESSION["SV_USER"] = $theAppUser->SV_User;
    $_SESSION["PAuserLanguageID"] = $userLanguageID;
    $_SESSION["PAuserLanguage"] = $userLanguage;
    $_SESSION["PAchangeLanguage"] = false;
    $_SESSION["PAArtusWebVersion"] = 6.0;
    $_SESSION["PAFBM_Mode"] = $valueFBMMode;
    
	//@JAPR 2015-11-23: Corregido un bug, no estaba cargando el idioma del usuario que hacía el request, así que las notificaciones no llegaban traducidas
	if (array_key_exists("PAuserLanguage", $_SESSION))
	{
		InitializeLocale($_SESSION["PAuserLanguageID"]);
		LoadLanguageWithName($_SESSION["PAuserLanguage"]);
	}
	
    //@JAPR MABH 2014-11-28: Se agregó esta asignación ya que hasta cierta versión eBavel requería de esta variable de sesión para procesar
    //entre otras cosas la seguridad, sin embargo la asignación de la variable por parte de eForms antes de los procesos de eBavel se hacía
    //si no estaba previamente asignada, solo que durante la misma sesión de eForms de un dispositivo, la sesión es compartida incluso si se
    //cambia el usuario, así que ahora se sobreescribirá directamente la variable en cada request del Web Service de eForms, el cual realmente
    //no tendría por qué afectar a ningún Artus u otro producto vía browser ya que no debe usarse el Web Service desde el Admin (sólo podría
    //afectar desde la captura Web, que es el equivalente a un dispositivo, si es que se cargó en conjunto en el mismo browser/sesión de otros
    //productos, eso aún se tiene que validar de manera diferente mas adelante)
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//@session_register("BITAM_UserName");
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	
    $aResponse = '';
    //$blnExtend indica si las propiedades del array $aResponse se deben incluir directas al array $return en lugar de agregarlas a la propiedad 'data'
    $blnExtend = false;
    //@JAPR 2012-07-31: Reestructuración del modelo de datos de las Apps
    if (getAppVersion() >= (float) esvMultiDynamicAndStandarization)
    {
    	require_once('eSurveyServiceMod.inc.php');
    }
    
	//@JAPR 2014-11-19: Agregada la dimensión Agenda
	//Obtiene el filtro de la agenda, ya que ese es el nombre que tiene que usar al grabar en la dimensión agenda
    require_once('surveyAgenda.inc.php');
	$strAgendaFilter = $gblEFormsNA;
	$agendaID = getParamValue('agendaID', 'both', '(int)');
	if ($agendaID > 0) {
		$objAgenda = BITAMSurveyAgenda::NewInstanceWithID($theRepository, $agendaID);
		if (!is_null($objAgenda)) {
			$strAgendaFilter = (string) @$objAgenda->FilterText;
		}
	}
	$arrDefDimValues['AgendaFilter'] = $strAgendaFilter;
    //@JAPR
    
    switch (strtolower($strRequest))
    {
    	//Servicio para móviles. Regresa la lista de agendas con las referencias a los Surveys de cada una (ID, no la definición)
    	case 'getagendalist':
    		$aResponse = getAgendaList($theRepository);
    		break;
    	
    	//Servicio para la creación de agendas. Regresa la lista de encuestas que contienen alguna pregunta basada en el atributo indicado
    	case 'getsurveysbyattribute':
    		$aResponse = getSurveysByAttribute($theRepository);
    		break;
    	
    	case 'testsavefromfile':
    		$aResponse = testSaveMethod($theRepository);
    		break;
    		
    	case 'testsaveaction':
    		$aResponse = testSaveAction($theRepository);
    		break;

	    //@JAPR 2012-07-31: Reestructuración del modelo de datos de las Apps
    	case 'getdefinitions':
    		$aResponse = getEFormsDefinitions($theRepository);
    		break;
    	
		//@JAPR 2012-11-06: Agregada la edición de capturas en v4 desde el Browser
    	case 'loadanswers':
    		$aResponse = loadSurveyCapture($theRepository);
    		$blnExtend = true;
    		break;
    		
    	//@JAPR 2012-09-18: Agregada la sincronización en la v4 del App
    	case 'saveanswers':
    		//$aResponse = storeSurveyCapture($theRepository);
    		$aResponse = uploadOutboxData($theRepository, $theUser);
    		break;
    		
    	case 'testuploadanswers':
    		$aResponse = storeSurveyCapture($theRepository);
    		break;
    	//@JAPR
    	
    	//@JAPR 2012-08-08: Agregado el servicio para obtener las versiones de las definiciones que aplican al usuario
    	case 'getdefinitionsversions':
		    if (getAppVersion() >= (float) esvMultiDynamicAndStandarization) {
		    	$aResponse = getEFormsDefinitionsVersions($theRepository);
		    }
    		break;
    	//OMMC 2016-10-05: Agregado el servicio para obtener los mensajes del server que le corresponden al usuario (#L9T2IU)
    	case 'pushmessages':
    		$strActionType = getParamValue('subaction', 'both', "(string)");
    		switch (strtolower($strActionType)) {
    			case 'requestmessages':
    				$aResponse = getEFormsPushMessages($theRepository);
					break;
    			case 'updatemessages':
    				$aResponse = updateEFormsPushMessages($theRepository);
					break;
    		}
    		break;
		//@JAPR 2013-12-17: Agregada la validación para ignorar errores cuando si se logró procesar el Outbox
		//Verifica si se encuentra grabado el archivo de outbox en la ruta de SyncData para la cuenta que hace la petición, regresando simplemente
		//un boolean indicando si lo encontró
    	case 'checkoutboxfile':
    		$aResponse = @checkOutboxFile($theRepository);
    		if (is_null($aResponse) || !is_array($aResponse)) {
    			$aResponse = array();
    		}
    		break;
    		
    	//@JAPR 2012-08-27: Agregado el servicio para recibir archivos (Originalmente usado para las fotos "tomadas" desde captura en Web)
    	case 'uploadfile':
    		$strFileType = getParamValue('ftype', 'both', "(string)");
    		switch (strtolower($strFileType)) {
    			case 'photo':
    				$aResponse = uploadPhoto($theRepository);
    				break;
				case 'audio':
					$aResponse = uploadAudio($theRepository);
					break;
				case 'video':				
					$aResponse = uploadVideo($theRepository);
					break;
				case 'document':
					$aResponse = uploadDocument($theRepository);
    				break;
				//@JAPR 2014-12-17: Agregado el valor default para preguntas tipo Foto y Sketch
    			case 'sketch':
    				$aResponse = uploadSketch($theRepository);
    				break;
    		}
    		$blnExtend = true;
    		break;
    	
    	//@JAPR 2012-09-24: Agregado el servicio para sincronizar fotos de capturas y otros archivos (Log)
    	case 'uploadsyncdata':
    		$strFileType = getParamValue('ftype', 'both', "(string)");
    		switch (strtolower($strFileType)) {
    			case 'signature':
		    		$aResponse = uploadSyncSignatureData($theRepository);
		    		break;
    			case 'photo':
		    		$aResponse = uploadSyncPhotoData($theRepository);
		    		break;
		    	//OMMC 2016-01-25: Agregado para el upload de documentos desde movil
		    	case 'document':
		    		$aResponse = uploadSyncDocumentData($theRepository);
		    		break;
    			case 'outbox':
    				$aResponse = uploadOutboxData($theRepository, $theUser);
    				break;
				//@JAPR 2013-03-26: Agregado el botón de reporte de error en el App
    			case 'file':
		    		$aResponse = uploadFileData($theRepository);
		    		break;
				//JAPR 2014-08-11: Modificado el reporte de errores para hacerlos mas amigables
    			case 'log':
		    		$aResponse = uploadLogData($theRepository);
		    		break;
				//@JAPR
    		}
    		//En este caso no se agrega una respuesta sino que se regresa el texto tal cual lo hizo en la función invocada
    		$strResponseFormat = 'none';
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
            $intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
            if ($intDataDestinationID) {
                //Sólo si se trata de la sincronización de una pregunta Update Data, se espera una respuesta en formato JSON
                $strResponseFormat = 'json';
            }
			//@JAPR
    		break;
		
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
    	case 'getcatalogvalues':
			$gbGettingDynamicValues = true;
    		$aResponse = GetDynamicCatalogValues($theRepository);
    		break;
    		
		//@JAPR 2014-08-20: Agregads los datos dinámicos de secciones tipo Recap
    	case 'getsectionvalues':
    		$aResponse = GetDynamicSectionValues($theRepository);
    		break;
    		
		//@JAPR 2014-06-30: Agregado el soporte para multiples idiomas por usuario
    	case 'getlanguagefile':
    		$aResponse = GetLanguageFile($theRepository);
    		break;
    	//@JAPR
    	
		//@JAPR 2015-09-06: Agregado el cambio para que el agente procese los DataSource desde origenes de DropBox y demás en base a una frecuencia
		case 'syncdatasources':
			require_once('uploadSyncDataSources.php');
			//@JAPR 2015-11-19: Corregido un bug, este proceso se debe ejecutar con la instancia de AppUser, no BITAMeFormsUser
			$aResponse = SyncDataSources($theRepository, $theAppUser);
			break;
		
		//@JAPR 2015-10-27: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
		case 'syncagendas':
			require_once('uploadSyncAgendas.php');
			//@JAPR 2015-11-19: Corregido un bug, este proceso se debe ejecutar con la instancia de AppUser, no BITAMeFormsUser
			$aResponse = SyncAgendas($theRepository, $theAppUser);
			break;
		//@JAPR

		//GCRUZ 2016-01-19. Agregado proceso de destinos de datos incrementales desde el agente de eForms
		case 'syncincdatadestinations':
			require_once('syncIncDataDestinations.php');
			syncLogString('====Starting sync Incremental Data Destinations====');
			$aResponse = syncIncDataDestinations($theRepository, $theAppUser);
			syncLogString('====Finished sync Incremental Data Destinations====');
			break;
		
		//GCRUZ 2016-06-06. Agregado proceso de generacion de cubo de agendas
		case 'agendacubegeneration':
			require_once('agendaCubeGeneration.php');
			$processTime =  getParamValue('Time', 'both', "(string)");
			agendaCubeGenerationLogString("====Starting Agenda Cube Generation {$processTime} {$theRepository->RepositoryName}====");
			$aResponse = agendaCubeGeneration($theRepository, $theAppUser);
			agendaCubeGenerationLogString("====Finished Agenda Cube Generation {$processTime} {$theRepository->RepositoryName}====".PHP_EOL);
			break;
		
		//GCRUZ 2016-06-13. Agregado proceso de generacion de cubo de geocercas
		case 'geofencecubegeneration':
			require_once('geofenceCubeGeneration.php');
			geofenceCubeGenerationLogString("====Starting GeoFence Cube Generation {$theRepository->RepositoryName}====");
			$aResponse = geoFenceCubeGeneration($theRepository, $theAppUser);
			geofenceCubeGenerationLogString("====Finished GeoFence Cube Generation {$theRepository->RepositoryName}====".PHP_EOL);
			break;
		
    	default:
    		//No hay acción, puede servir como una forma de verificar que el Web Service está respondiendo
    		break;
    }
    
    switch (strtolower($strResponseFormat))
    {
    	case 'none':
    		break;
    	
    	case 'json':
		    $return = array();
			
			//@JAPR 2019-05-30: Corregido un bug, no se puede agregar un header si ya se había mandado una respuesta, ya que a partir de cierto evento comenzó a generar un 
			//Internal Server Error (HTTP 500) si se hacía de esa manera, así que se validará que los headers sean excluyentes al parámetro DebugBreak, además de atrapar toda salida
			//al buffer no atrapada para regresarla como una propiedad warningNotice de la respuesta en caso de haber algún texto (#7UZT8C)
			if ( !isset($_REQUEST["DebugBreak"]) ) {
				//Adicional a activar el header en esta situación, agregará la lectura y desactivación del buffer por cualquier error no controlado que hubiera sido regresado
				$strWarningsNotice = trim(ob_get_contents());
				if ( $strWarningsNotice ) {
					$return['warningNotice'] = $strWarningsNotice;
				}
				ob_end_clean();
				header("Content-Type: text/javascript");
			}
			//@JAPR
			
			$return['web_service_version'] = ESURVEY_SERVICE_VERSION;
			$return['request_version'] = $appVersion;
		    $return['error'] = false;
		    if ($blnExtend) {
	    		//En este caso se desea regresar directamente el array de respuesta del proceso sin agregarlo como una propiedad 'data', por lo que
	    		//nada mas se unen las propiedades de ambos arrays (no se agrega la propiedad 'error' porque se asume que el proceso ya la
	    		//regresó como parte del array $aResponse)
	    		if (!isset($aResponse) || !is_array($aResponse)) {
	    			$aResponse = array();
	    		}
	    		
	    		$return = array_merge($return, $aResponse);
		    }
		    else {
				//@JAPR 2015-07-19: Corregido un bug, si el proceso hubiera tenido algún error, no se hubiera reportado adecuadamente por encontrarse dentro de la propiedad data, así que
				//adicionalmente valida si hubo o no error para agregarlo directo en el return
		    	$return['data'] = $aResponse;
				$blnError = (int) @$aResponse['error'];
				if ($blnError) {
					$return['error'] = true;
					$return['errmsg'] = (string) @$aResponse['errmsg'];
				}
				//@JAPR
		    }
			
			//@JAPR 2012-03-14: Corregido un bug, la función de Callback se debe invocar siempre que se especifique y se regrese un JSon
			//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
			//Se detectó que no solo era el caso descrito arriba, sino que el grabado de valores de campos TEXT de eBavel o alguna fuente con demasiado texto dentro de preguntas con 
			//límite de caracteres o usadas previamente como atributos (las cuales están limitadas) en algunos casos circunstanciales cortaba los caracteres utf8 de 2 bytes en el primer 
			//byte, lo cual corrompía el string y generaba este error también
			//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
			//Modificado para utilizar una función global y reutilizar el código en diferentes puntos donde se requería el mismo procesamiento
			$return = FixNonUTF8CharsInArray($return);
			//@JAPR
			
			$blnEncodeReturn = json_encode($return);
			if (isset($_GET['jsonpCallback'])) {
				echo($_GET['jsonpCallback'] . '(' . $blnEncodeReturn . ');');
			} else {
				echo ($blnEncodeReturn);
			}
			if ( $blnEncodeReturn === false ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Encoding error: ".json_last_error()." => ".json_last_error_msg());
				}
			}
			break;
	
    	case 'txt':
    	default:
			//@JAPR 2019-05-30: Corregido un bug, no se puede agregar un header si ya se había mandado una respuesta, ya que a partir de cierto evento comenzó a generar un 
			//Internal Server Error (HTTP 500) si se hacía de esa manera, así que se validará que los headers sean excluyentes al parámetro DebugBreak, además de atrapar toda salida
			//al buffer no atrapada para regresarla como una propiedad warningNotice de la respuesta en caso de haber algún texto (#7UZT8C)
			$strWarningsNotice = '';
			if ( !isset($_REQUEST["DebugBreak"]) ) {
				//Adicional a activar el header en esta situación, agregará la lectura y desactivación del buffer por cualquier error no controlado que hubiera sido regresado
				$strWarningsNotice = trim(ob_get_contents());
				/*if ( $strWarningsNotice ) {
					$return['warningNotice'] = $strWarningsNotice;
				}*/
				ob_end_clean();
				
				header("Content-Type: text/plain");
			}
			//@JAPR
			
		    if (!is_string($aResponse))
		    {
		    	$aResponse = serialize($aResponse);
		    }
		    echo ('0'.(($blnDebug)?"\r\n\r\n":SV_DATA_FIELD).ESURVEY_SERVICE_VERSION.(($blnDebug)?";":SV_ITEM).$appVersion.(($blnDebug)?"\r\n\r\n":SV_DATA_FIELD).$aResponse);
	    	break;
    }
    
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	if ($gbUseOperationsMonitor) {
		$arrEventParams["DBServer"] = $strDBServer;
		$arrEventParams["Repository"] = $theRepositoryName;
		$arrEventParams["DBid"] = $intFormsDatabaseID;
		$arrEventParams["UserID"] = $theUser->UserID;
		$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
		$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
		@RegisterOperation($arrEventParams, $giOperationID);
	}
    
    //@JAPR 201208-27: Optimizado, los request que entran por aquí deberían terminar aqui mismo pero los estaba dejando continuar
	//@JAPR 2014-08-25: Agregado el log de requests
	if ($blnSaveRequestsLog) {
	    //@JAPR 2015-03-05: Agregada la fecha al log de requests
		@error_log(date('Y-m-d H:i:s')."- Request done\r\n", 3, $requestDir);
	}
	//@JAPR
    exit();
}
//@JAPR

// ***********************************************************************************************************************
/* saveAnswersMobile2 */
// ***********************************************************************************************************************
if (isset($_POST['saveAnswersMobile2'])) 
{
	saveAnswersMobile2();
}

// ***********************************************************************************************************************
/* getSurveyMessage */
// ***********************************************************************************************************************
//@JAPRDescontinuada en V6
if (isset($_GET['getSurveyMessage'])) {

    if (!isset($_GET['UserID'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiUser = $_GET['UserID'];

    if (!isset($_GET['Password'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiPass = $_GET['Password'];

    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
        $return['error'] = true;
        $return['errmsg'] = utf8_encode('USER OR PASSWORD INVALID');
        header("Content-Type: text/javascript");
        if (isset($_GET['jsonpCallback'])) {
            echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
        } else {
            echo(json_encode($return));
        }
        exit();
    }

    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
    $strRepositoryName = $arrayPrjUsr[0];
    $strUserName = $arrayPrjUsr[1];

    $theRepositoryName = $strRepositoryName;
    $theUserName = $strUserName;
    $thePassword = $kpiPass;

    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Project does not exists.";
        exit();
    }

    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
        header('HTTP/1.0 401 Unauthorized');
        echo translate("Can not connect to Repository, please contact your System Administrator.");
        exit();
    }
	
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
    if (is_null($theUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name does not exist or your Password is incorrect.";
        exit();
    }

    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);

    if (is_null($theAppUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name is not registered in the application.";
        exit();
    }

    //Establecer el lenguaje en una variable de sesión
    //0=DEFAULT, 1=Spanish, 2=English
    if ($theUser->UserLanguageID <= 0 || $theUser->UserLanguageID > 8) {
        $userLanguageID = 2;
        $userLanguage = 'EN';
    } else {
        $userLanguageID = $theUser->UserLanguageID;
        $userLanguage = $theUser->UserLanguageTerminator;
    }

    //Verificar si se encuentra en modo FBM
    $sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";

    $aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);

    $valueFBMMode = 0;

    if ($aRSFBM && !$aRSFBM->EOF) {
        if ($aRSFBM->fields["ref_configura"] !== null && trim($aRSFBM->fields["ref_configura"]) !== "") {
            $valueFBMMode = (int) trim($aRSFBM->fields["ref_configura"]);
        }
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    /*session_register("PABITAM_RepositoryName");
    session_register("PABITAM_UserName");
    session_register("PABITAM_UserRole");
    session_register("PABITAM_Password");
    session_register("PABITAM_UserID");
    session_register("PAtheUserID");
    session_register("PAtheAppUserID");
    session_register("PAgenerateLog");
    session_register("SV_ADMIN");
    session_register("SV_USER");
    session_register("PAuserLanguageID");
    session_register("PAuserLanguage");
    session_register("PAchangeLanguage");
    session_register("PAArtusWebVersion");
    session_register("PAFBM_Mode");*/

    $_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
    $_SESSION["PABITAM_Password"] = $theUser->Password;
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
    $_SESSION["PAtheUserID"] = $theUser->UserID;
    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
    $_SESSION["PAgenerateLog"] = $generateLog;
    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
    $_SESSION["SV_USER"] = $theAppUser->SV_User;
    $_SESSION["PAuserLanguageID"] = $userLanguageID;
    $_SESSION["PAuserLanguage"] = $userLanguage;
    $_SESSION["PAchangeLanguage"] = false;
    $_SESSION["PAArtusWebVersion"] = 6.0;
    $_SESSION["PAFBM_Mode"] = $valueFBMMode;


    $return = getSurveyMessage($theRepository);
    //@JAPR 2012-05-07: Agregada la versión del Web Service a todas las respuestas para permitir variar el código según la versión específica
    if (is_array($return))
    {
    	$return['web_service_version'] = ESURVEY_SERVICE_VERSION;
	    $return['request_version'] = $appVersion;
    }
	//@JAPR

    header("Content-Type: text/javascript");
    if (isset($_GET['jsonpCallback'])) {
        echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
    } else {
        echo(json_encode($return));
    }
    exit();
}

// ***********************************************************************************************************************
/* getSurveyList */
// ***********************************************************************************************************************
//@JAPRDescontinuada en V6
if (isset($_GET['getSurveyList'])) {
    if (!isset($_GET['UserID'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiUser = $_GET['UserID'];
	
    if (!isset($_GET['Password'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiPass = $_GET['Password'];

    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
        $return['error'] = true;
        $return['errmsg'] = utf8_encode('USER OR PASSWORD INVALID');
        header("Content-Type: text/javascript");
        if (isset($_GET['jsonpCallback'])) {
            echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
        } else {
            echo(json_encode($return));
        }
        exit();
    }

    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
    $strRepositoryName = $arrayPrjUsr[0];
    $strUserName = $arrayPrjUsr[1];

    $theRepositoryName = $strRepositoryName;
    $theUserName = $strUserName;
    $thePassword = $kpiPass;

    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Project does not exists.";
        exit();
    }

    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
        header('HTTP/1.0 401 Unauthorized');
        echo translate("Can not connect to Repository, please contact your System Administrator.");
        exit();
    }
	
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
    if (is_null($theUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name does not exist or your Password is incorrect.";
        exit();
    }

    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);

    if (is_null($theAppUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name is not registered in the application.";
        exit();
    }

    //Establecer el lenguaje en una variable de sesión
    //0=DEFAULT, 1=Spanish, 2=English
    if ($theUser->UserLanguageID <= 0 || $theUser->UserLanguageID > 8) {
        $userLanguageID = 2;
        $userLanguage = 'EN';
    } else {
        $userLanguageID = $theUser->UserLanguageID;
        $userLanguage = $theUser->UserLanguageTerminator;
    }

    //Verificar si se encuentra en modo FBM
    $sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";

    $aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);

    $valueFBMMode = 0;

    if ($aRSFBM && !$aRSFBM->EOF) {
        if ($aRSFBM->fields["ref_configura"] !== null && trim($aRSFBM->fields["ref_configura"]) !== "") {
            $valueFBMMode = (int) trim($aRSFBM->fields["ref_configura"]);
        }
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    /*session_register("PABITAM_RepositoryName");
    session_register("PABITAM_UserName");
    session_register("PABITAM_UserRole");
    session_register("PABITAM_Password");
    session_register("PABITAM_UserID");
    session_register("PAtheUserID");
    session_register("PAtheAppUserID");
    session_register("PAgenerateLog");
    session_register("SV_ADMIN");
    session_register("SV_USER");
    session_register("PAuserLanguageID");
    session_register("PAuserLanguage");
    session_register("PAchangeLanguage");
    session_register("PAArtusWebVersion");
    session_register("PAFBM_Mode");*/

    $_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
    $_SESSION["PABITAM_Password"] = $theUser->Password;
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
    $_SESSION["PAtheUserID"] = $theUser->UserID;
    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
    $_SESSION["PAgenerateLog"] = $generateLog;
    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
    $_SESSION["SV_USER"] = $theAppUser->SV_User;
    $_SESSION["PAuserLanguageID"] = $userLanguageID;
    $_SESSION["PAuserLanguage"] = $userLanguage;
    $_SESSION["PAchangeLanguage"] = false;
    $_SESSION["PAArtusWebVersion"] = 6.0;
    $_SESSION["PAFBM_Mode"] = $valueFBMMode;

    //@JAPR 2012-03-08: Agregado el buffer de salida para registrar cualquier Warning u otros errores generados por el proceso y en lugar
    //de generar un JSon dañado (o provocar un error peor por agregar headers después de la salida al buffer) regresar un código de error
    ob_start();
    $return = getSurveyList($theRepository);
    $strErrors = trim(ob_get_contents());
	ob_end_clean();
	
    if ($strErrors != '')
    {
    	if (!is_array($return))
    	{
    		$return = array('return' => $return);
    	}
	    $return['error'] = true;
	    $return['warnings'] = $strErrors;
    }
    
    //@JAPR 2012-05-07: Agregada la versión del Web Service a todas las respuestas para permitir variar el código según la versión específica
    if (is_array($return))
    {
    	$return['web_service_version'] = ESURVEY_SERVICE_VERSION;
	    $return['request_version'] = $appVersion;
    }
	//@JAPR

    header("Content-Type: text/javascript");
    if (isset($_GET['jsonpCallback'])) {
        echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
    } else {
        echo(json_encode($return));
    }
    exit();
}

function getSurveyMessage($aRepository) {
	global $kpiUser;
	global $kpiPass;
	
    $surveyMessage = array();
    $activeStatus = array(0 => 0, 1 => 1);
	
    require_once('survey.inc.php');
    require_once('section.inc.php');
    require_once('question.inc.php');
    require_once('catalog.inc.php');
    require_once('surveyAgenda.inc.php');

    $arraySurveys = array();
    $roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $_SESSION["PABITAM_UserID"]);
    //Obtener el listado de encuestas disponibles para el usuario logueado x user
    $sql = "SELECT DISTINCT A.SurveyID, A.LastDateID FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C
			WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = " . $_SESSION["PABITAM_UserID"] . " 
			AND A.Status IN (" . implode(",", $activeStatus) . ")";

    $recordset = $aRepository->DataADOConnection->Execute($sql);
    if ($recordset === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error Getting Survey LastDateIDs';
        //conchita 1-nov-2011 ocultar mensajes sql
        //$return['errmsg'] = 'Error Getting Survey LastDateIDs:'.$sql;
    }
    while (!$recordset->EOF) {
        $surveyID = (int) $recordset->fields["surveyid"];
        $surveyLastDateID = $recordset->fields["lastdateid"];
        $arraySurveys[$surveyID]['surveyID'] = $surveyID;
        $arraySurveys[$surveyID]['surveylastdateid'] = $surveyLastDateID;
        $recordset->MoveNext();
    }
    
    //@JAPR 2012-03-22: Agregada la actualización de encuestas asociadas a agendas
    //Obtener el listado de encuestas disponibles para el usuario logueado x user a partir de sus agendas
	//@JAPR 2012-06-01: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	
    $sql = "SELECT DISTINCT A.SurveyID, A.LastDateID 
    	FROM SI_SV_Survey A, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
		WHERE A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID AND C.UserID = " . $_SESSION["PABITAM_UserID"] . " 
			AND (B.Status IS NULL OR B.Status = 0) AND C.AgendaID NOT IN ( 
				SELECT DISTINCT AgendaID 
				FROM SI_SV_SurveyAgendaDet 
				WHERE Status = 2)";	
	$recordset = $aRepository->DataADOConnection->Execute($sql);
    if ($recordset === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error Getting Survey LastDateIDs by Agenda';
        //conchita 1-nov-2011 ocultar mensajes sql
        //$return['errmsg'] = 'Error Getting Survey LastDateIDs:'.$sql;
    }
    while (!$recordset->EOF) {
        $surveyID = (int) $recordset->fields["surveyid"];
        $surveyLastDateID = $recordset->fields["lastdateid"];
        $arraySurveys[$surveyID]['surveyID'] = $surveyID;
        $arraySurveys[$surveyID]['surveylastdateid'] = $surveyLastDateID;
        $recordset->MoveNext();
    }
    //@JAPR
    
    if (count($roles) > 0) {
        //Obtener el listado de encuestas disponibles para el usuario logueado x rol
        $sql = "SELECT DISTINCT A.SurveyID, A.LastDateID FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C
				WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (" . implode(",", $roles) . ") 
				AND A.Status IN (" . implode(",", $activeStatus) . ")";

        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset === false) {
            $return['error'] = true;
            $return['errmsg'] = 'Error Getting Survey LastDateID';
            //conchita 1-nov-2011 ocultar mensajes sql
            //$return['errmsg'] = 'Error Getting Survey LastDateID:'.$sql;
        }

        while (!$recordset->EOF) {
            $surveyID = (int) $recordset->fields["surveyid"];
            $surveyLastDateID = $recordset->fields["lastdateid"];

            $arraySurveys[$surveyID]['surveyID'] = $surveyID;
            $arraySurveys[$surveyID]['surveylastdateid'] = $surveyLastDateID;
            $recordset->MoveNext();
        }
    }

    foreach ($arraySurveys as $survey) {
        $surveyID = $survey['surveyID'];
        $surveyLastDateID = $survey['surveylastdateid'];

        $surveyMessage[] = array(
            'surveyID' => $surveyID,
            'surveyLastDateID' => utf8_encode($surveyLastDateID),
        );
    }
    
    //@JAPR 2012-03-15: Agregada la lista de Agendas del usuario
	//@JAPR 2012-06-01: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
    $sql = "SELECT DISTINCT A.AgendaID, A.LastDateID 
    	FROM SI_SV_SurveyAgenda A, SI_SV_SurveyAgendaDet B 
    	WHERE A.UserID = " . $_SESSION["PABITAM_UserID"] . " AND A.AgendaID = B.AgendaID AND 
    		(B.Status IS NULL OR B.Status = 0) AND A.AgendaID NOT IN ( 
				SELECT DISTINCT AgendaID 
				FROM SI_SV_SurveyAgendaDet 
				WHERE Status = 2)";
    $recordset = $aRepository->DataADOConnection->Execute($sql);
    if ($recordset === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error Getting Agenda LastDateIDs';
    }
    $agendaMessage = array();
    //@JAPR 2012-03-27: Corregido un bug, el arreglo no debía ser asociativo
    $agendaIdx = -1;
    while (!$recordset->EOF) {
    	$agendaIdx++;
        $agendaID = (int) $recordset->fields["agendaid"];
        $agendaLastDateID = $recordset->fields["lastdateid"];
        $agendaMessage[$agendaIdx]['agendaID'] = $agendaID;
        $agendaMessage[$agendaIdx]['agendaLastDateID'] = $agendaLastDateID;
        $recordset->MoveNext();
    }

    $return['error'] = false;
    $return['surveyMessage'] = $surveyMessage;
    $return['agendaMessage'] = $agendaMessage;
    
	//@JAPR 2012-03-30: Agregado el path a la respuesta en este servicio para contar con la ruta actualiza en todo momento
    $arrayFormsInfo = getFormsInfo($kpiUser, $kpiPass);
    if (is_null($arrayFormsInfo) || isset($arrayFormsInfo['error']))
    {
    	//En este caso este no es un error crítico, se puede trabajar con el Web Service que el dispositivo ya tiene asignado
	    $return['warnings'] = 'getFormsInfo error: '. (string) @$arrayFormsInfo['errmsg'];
    	/*
        $return['error'] = true;
        $return['errmsg'] = utf8_encode('Error retrieving');
        header("Content-Type: text/javascript");
        if (isset($_GET['jsonpCallback'])) {
            echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
        } else {
            echo(json_encode($return));
        }
        exit();
        */
    }
    else
    {
	    $strFullPath = $arrayFormsInfo[0];
	    $strFormsVersion = $arrayFormsInfo[1];
		$strFormsPub = '';
		
	    $return['formsFullPath'] = $strFullPath;
	    $return['formsVersion'] = $strFormsVersion;
		$return['formsPub'] = $strFormsPub;
    }
	//@JAPR
    return $return;
}

//@JAPRDescontinuada: Esto aplicaba para la captura Web de v3
function getSurveyList($aRepository) {
	global $arrCatalogVersion;
	global $appVersion;
	global $kpiUser;
	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	$catalogList = array();
	$arrSurveyIDs = array();
	//@JAPR 2012-05-15: Agregada la lista de usuarios para las preguntas tipo Action (sólo si realmente existen este tipo de preguntas)
	$userList = array($_SESSION["PABITAM_UserID"] => $kpiUser);
	//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
	$categoryList = array();
	//@JAPR
    $surveyList = array();
    $activeStatus = array(0 => 0, 1 => 1);
	$return['error'] = false;
	
    require_once('survey.inc.php');
    require_once('section.inc.php');
    require_once('question.inc.php');
    require_once('catalog.inc.php');

    $roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $_SESSION["PABITAM_UserID"]);
    $arraySurveys = array();

    //Obtener el listado de encuestas disponibles para el usuario logueado x user
	//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
	//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
	$strAdditionalFields = '';
	if (getMDVersion() >= esvExtendedActionsData) {
		$strAdditionalFields .= ', A.SyncWhenSaving';
	}
    $sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.CatalogID, A.HasSignature, A.LastDateID, A.AllowPartialSave, A.VersionNum, A.ElementQuestionID, A.EnableReschedule, A.RescheduleStatus $strAdditionalFields 
    	FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C
		WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = " . $_SESSION["PABITAM_UserID"] . " 
			AND A.Status IN (" . implode(",", $activeStatus) . ")";
    $recordset = $aRepository->DataADOConnection->Execute($sql);
    if ($recordset === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error Getting Survey List';
        //conchita 1-nov-2011 ocultar mensajes sql
        //$return['errmsg'] = 'Error Getting Survey List:'.$sql;
    }
	else 
	{
	    while (!$recordset->EOF) {
	        $surveyID = (int) $recordset->fields["surveyid"];
	        $surveyName = $recordset->fields["surveyname"];
	        $surveyCatalogID = $recordset->fields["catalogid"];
	        $surveyLastDateID = $recordset->fields["lastdateid"];
	        $surveyHasSignature = (int) $recordset->fields["hassignature"];
	        $surveyAllowPartialSave = (int) $recordset->fields["allowpartialsave"];
	        $arraySurveys[$surveyID]['surveyID'] = $surveyID;
	        $arraySurveys[$surveyID]['surveyName'] = $surveyName;
	        $arraySurveys[$surveyID]['surveyCatalogID'] = $surveyCatalogID;
	        $arraySurveys[$surveyID]['surveyLastDateID'] = $surveyLastDateID;
	        $arraySurveys[$surveyID]['surveyHasSignature'] = $surveyHasSignature;
	        $arraySurveys[$surveyID]['surveyAllowPartialSave'] = $surveyAllowPartialSave;
			//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
	        $versionNum = (int) $recordset->fields["versionnum"];
	        $arraySurveys[$surveyID]['versionNum'] = $versionNum;
			//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
	        $arraySurveys[$surveyID]['ElementQuestionID'] = (int) $recordset->fields["elementquestionid"];
			//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$arraySurveys[$surveyID]['enableReschedule'] = (int) $recordset->fields["enablereschedule"];
			$arraySurveys[$surveyID]['rescheduleStatus'] = (string) $recordset->fields["reschedulestatus"];
			//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
			if (getMDVersion() >= esvExtendedActionsData) {
				$arraySurveys[$surveyID]['syncWhenSaving'] = (int) @$recordset->fields["syncwhensaving"];
			}
	        //@JAPR
	        $recordset->MoveNext();
	    }
	}

	//@JAPR 2012-03-20: Agregado el soporte para Agendas
	//Obtiene el listado de encuestas disponibles para el usuario logeado x user según sus agendas configuradas
    //Obtener el listado de encuestas disponibles para el usuario logueado x user
	//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
	//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//@JAPR 2012-06-01: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
    $sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.CatalogID, A.HasSignature, A.LastDateID, A.AllowPartialSave, A.VersionNum, A.ElementQuestionID, A.EnableReschedule, A.RescheduleStatus $strAdditionalFields 
		FROM SI_SV_Survey A, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
		WHERE A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID AND C.UserID = " . $_SESSION["PABITAM_UserID"] . " 
			AND A.Status IN (" . implode(",", $activeStatus) . ") AND C.AgendaID NOT IN ( 
				SELECT DISTINCT AgendaID 
				FROM SI_SV_SurveyAgendaDet 
				WHERE Status = 2)";    
    $recordset = $aRepository->DataADOConnection->Execute($sql);
    if ($recordset === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error Getting Survey List';
    }
    else
    {
	    while (!$recordset->EOF) {
	        $surveyID = (int) $recordset->fields["surveyid"];
	        if (!isset($arraySurveys[$surveyID]))
	        {
		        $surveyName = $recordset->fields["surveyname"];
		        $surveyCatalogID = $recordset->fields["catalogid"];
		        $surveyLastDateID = $recordset->fields["lastdateid"];
		        $surveyHasSignature = (int) $recordset->fields["hassignature"];
		        $surveyAllowPartialSave = (int) $recordset->fields["allowpartialsave"];
		        $arraySurveys[$surveyID]['surveyID'] = $surveyID;
		        $arraySurveys[$surveyID]['surveyName'] = $surveyName;
		        $arraySurveys[$surveyID]['surveyCatalogID'] = $surveyCatalogID;
		        $arraySurveys[$surveyID]['surveyLastDateID'] = $surveyLastDateID;
		        $arraySurveys[$surveyID]['surveyHasSignature'] = $surveyHasSignature;
		        $arraySurveys[$surveyID]['surveyAllowPartialSave'] = $surveyAllowPartialSave;
				//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		        $versionNum = (int) $recordset->fields["versionnum"];
		        $arraySurveys[$surveyID]['versionNum'] = $versionNum;
				//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		        $arraySurveys[$surveyID]['ElementQuestionID'] = (int) $recordset->fields["elementquestionid"];
				//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
				$arraySurveys[$surveyID]['enableReschedule'] = (int) $recordset->fields["enablereschedule"];
				$arraySurveys[$surveyID]['rescheduleStatus'] = (string) $recordset->fields["reschedulestatus"];
				//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
				if (getMDVersion() >= esvExtendedActionsData) {
					$arraySurveys[$surveyID]['syncWhenSaving'] = (int) @$recordset->fields["syncwhensaving"];
				}
		        //@JAPR
	        }
	        $recordset->MoveNext();
	    }
    }    
	//@JAPR
    
    if (count($roles) > 0) {
        //Obtener el listado de encuestas disponibles para el usuario logueado x rol
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
        $sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.CatalogID, A.HasSignature, A.LastDateID, A.AllowPartialSave, A.VersionNum, A.ElementQuestionID, A.EnableReschedule, A.RescheduleStatus $strAdditionalFields 
        	FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C
			WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (" . implode(",", $roles) . ") 
				AND A.Status IN (" . implode(",", $activeStatus) . ")";
        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset === false) {
            $return['error'] = true;
            $return['errmsg'] = 'Error Getting Survey List';
            //conchita 1-nov-2011 ocultar mensajes sql
            //$return['errmsg'] = 'Error Getting Survey List:'.$sql;
        }
		else
		{
	        while (!$recordset->EOF) {
	            $surveyID = (int) $recordset->fields["surveyid"];
	            $surveyName = $recordset->fields["surveyname"];
	            $surveyCatalogID = $recordset->fields["catalogid"];
	            $surveyLastDateID = $recordset->fields["lastdateid"];
	            $surveyHasSignature = (int) $recordset->fields["hassignature"];
	            $surveyAllowPartialSave = (int) $recordset->fields["allowpartialsave"];
	            $arraySurveys[$surveyID]['surveyID'] = $surveyID;
	            $arraySurveys[$surveyID]['surveyName'] = $surveyName;
	            $arraySurveys[$surveyID]['surveyCatalogID'] = $surveyCatalogID;
	            $arraySurveys[$surveyID]['surveyLastDateID'] = $surveyLastDateID;
	            $arraySurveys[$surveyID]['surveyHasSignature'] = $surveyHasSignature;
	            $arraySurveys[$surveyID]['surveyAllowPartialSave'] = $surveyAllowPartialSave;
				//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		        $versionNum = (int) $recordset->fields["versionnum"];
		        $arraySurveys[$surveyID]['versionNum'] = $versionNum;
				//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		        $arraySurveys[$surveyID]['ElementQuestionID'] = (int) $recordset->fields["elementquestionid"];
				//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
				$arraySurveys[$surveyID]['enableReschedule'] = (int) $recordset->fields["enablereschedule"];
				$arraySurveys[$surveyID]['rescheduleStatus'] = (string) $recordset->fields["reschedulestatus"];
				//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
				if (getMDVersion() >= esvExtendedActionsData) {
					$arraySurveys[$surveyID]['syncWhenSaving'] = (int) @$recordset->fields["syncwhensaving"];
				}
				//@JAPR
	
	            $recordset->MoveNext();
	        }
		}
    }

    //@JAPR 2012-03-08: Optimización de carga de los catálogos
    //Por cada ID de catálogo pedido, se guardará el array de valores obtenidos usando como índice adicional la serialización y hash md5 del
    //array de los Atributos pedidos, ya que los atributos siempre vienen desde los padres hasta el Atributo especifico pedido para el caso
    //de las preguntas, mientras que vienen los atributos completos cuando se piden por encuesta, así que el hash permitirá agrupar aquellas
    //colecciones de valores cuyo contenido es el mismo y reutilizarla en llamadas posteriores
    $arrCatalogValuesColl = array();
	//@JAPR 2012-05-15: Agregada la lista de usuarios para las preguntas tipo Action (sólo si realmente existen este tipo de preguntas)
    $userDimClaDescrip = -1;
    //@JAPR
    
    foreach ($arraySurveys as $survey) {
        $surveyID = $survey['surveyID'];
        $surveyName = $survey['surveyName'];
        $surveyCatalogID = $survey['surveyCatalogID'];
        $surveyAllowPartialSave = $survey['surveyAllowPartialSave'];
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		$versionNum = $survey['versionNum'];
		//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		$elementQuestionID = $survey['ElementQuestionID'];
		//@JAPR

        //se genera instance del survey
        $surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
		//@JAPR 2012-05-15: Agregada la lista de usuarios para las preguntas tipo Action (sólo si realmente existen este tipo de preguntas)
        if ($userDimClaDescrip <= 0)
        {
        	$userDimClaDescrip = $surveyInstance->GblUserDimID;
        }

        //ahora vamos a enviar todo el catalogo
        $arraySurveyAttribIDs = array();
        $arraySurveyAttribValues = array();

        $surveyHasDynamicSection = 0;
        $surveyDynamicSections = 0;

		//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
        $intSrvUpdated = 0;
        $intSrvCatalogVersion = 0;
        
        //@JAPR 2012-05-03: Corregido un bug, las encuestas que tenían asignado un catálogo pero sin preguntas que lo usaran aun así estaban
        //descargando el catálogo lo cual era innecesario
        $exist = BITAMSurvey::existCatalogQuestionInThisSurvey($aRepository, $surveyID, $surveyInstance->CatalogID);
        if (!$exist)
        {
        	//En este caso como no hay preguntas que usen el catálogo, se va a modificar localmente la instancia para que no se considere
        	$surveyCatalogID = 0;
        	$surveyInstance->CatalogID = 0;
        	$arraySurveys[$surveyID]['surveyCatalogID'] = 0;
        }
        //@JAPR
        
        if ($surveyInstance->CatalogID > 0) {
			//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
			//Ahora se verificará la última versión del catálogo y se comparará contra la versión que el dispositivo está reportando como
			//sú última versión, si el dispositivo tiene una versión previa entonces si se regresa el catálogo completo, de lo contrario
			//se regresa sólo una array vacio y una indicación de que el catálogo no ha sufrido cambios
            $aSurveyCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $surveyInstance->CatalogID);
			$intSrvCatalogVersion = $aSurveyCatalog->getCatalogVersion();            
	        $intCurrCatVersion = (int) @$arrCatalogVersion[$aSurveyCatalog->CatalogID];
			$intSrvUpdated = abs($intCurrCatVersion == 0 || $intSrvCatalogVersion > $intCurrCatVersion);
            $arraySurveyAttribIDs = getSurveyAttributes($aRepository, $surveyInstance);
            
            if (count($arraySurveyAttribIDs) > 0 && $intSrvUpdated) {
			    //@JAPR 2012-03-08: Optimización de carga de los catálogos
            	$strAttribsHash = md5(serialize($arraySurveyAttribIDs));
            	if (isset($arrCatalogValuesColl[$surveyInstance->CatalogID]) && isset($arrCatalogValuesColl[$surveyInstance->CatalogID][$strAttribsHash]))
            	{
            		$arraySurveyAttribValues = $arrCatalogValuesColl[$surveyInstance->CatalogID][$strAttribsHash];
            	}
            	else 
            	{
                	$arraySurveyAttribValues = getSurveyAttributeValues($aRepository, $surveyInstance, $aSurveyCatalog, $arraySurveyAttribIDs);
                	$arrCatalogValuesColl[$surveyInstance->CatalogID][$strAttribsHash] = $arraySurveyAttribValues;
            	}
                //@JAPR
            }
        }
        //@JAPR
		
        //@JAPR 2012-03-07: Se estaba invocando 2 veces a este método pero no era realmente necesario, si no hay secciones dinámicas el
        //resultado de la función ya es 0, que es el default asignado originalmente en esta parte del código
        $surveyDynamicSections = BITAMSection::existDynamicSection($aRepository, $surveyID);
        //if (BITAMSection::existDynamicSection($aRepository, $surveyID) > 0) {
        if ($surveyDynamicSections > 0) {
            $surveyHasDynamicSection = 1;
            //$surveyDynamicSections = BITAMSection::existDynamicSection($aRepository, $surveyID);
        }
        //@JAPR

        $surveyLastDateID = $survey['surveyLastDateID'];
        $surveyHasSignature = $survey['surveyHasSignature'];
        //Obtenemos coleccion de secciones
        $sectionCollection = BITAMSectionCollection::NewInstance($aRepository, $surveyID);

        $sectionList = array();
        foreach ($sectionCollection->Collection as $sectionElement) {
            /*
              $arraySectionAttribIDs = array();
              $arraySectionAttribValues = array();

              //llamamos a funcion que obtiene los atributos
              $arraySectionAttribIDs = getSectionAttributes($aRepository, $sectionElement);

              //Realizamos la instancia de objeto Catalogo
              $aSectionCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $sectionElement->CatalogID);

              if(count($arraySectionAttribIDs)>0)
              {
              //Llamamos a la funcion que obtiene los valores de los atributos
              $arraySectionAttribValues = getSectionAttributeValues($aRepository, $sectionElement, $aSectionCatalog, $arraySectionAttribIDs);
              }
             */

            //se obtiene seccion dinamica
	        //@JAPR 2012-03-07: Realmente esto no tenía caso, cada una de las secciones de esta colección es seguro que pertenece al Survey
	        //del ciclo externo puesto que al cargar la colección así se especificó, y para este día sólo existe una sección dinámica por
	        //Survey, incluso aunque no fuera así, la función existDynamicSection estaría regresando siempre lo mismo para cada sección
	        //así que mejor se utiliza el valor que ya fué asignado en la llamada justo antes de este ciclo
	        $dynamicSectionID = $surveyDynamicSections;
            //$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $sectionElement->SurveyID);
            //@JAPR
			
            //Obtenemos coleccion de preguntas
            $questionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $sectionElement->SectionID);

            $questionList = array();
            foreach ($questionCollection->Collection as $questionElement) {
                $arrayAttribIDs = array();
                $arrayAttribValues = array();
                $arrayOptions = array();
                $arrayOptionsRules = array();
                $arrayOptionList = array();
                $arrayOptionColList = array();
                $arrayAllShowQuestionList = array();

                //Si la pregunta es de seleccion simple y obtiene los datos de un catalogo
                $intUpdated = 0;
                $intCatalogVersion = 0;
                if ($questionElement->QTypeID == 2 && $questionElement->UseCatalog == 1) {
                	//@JAPRWarning: Optimizar, se puede mantener un Array de catálogos para reutilizar los previamente cargados (md5(serializ($arrayAttribIDs))
					//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
					//Ahora se verificará la última versión del catálogo y se comparará contra la versión que el dispositivo está reportando como
					//sú última versión, si el dispositivo tiene una versión previa entonces si se regresa el catálogo completo, de lo contrario
					//se regresa sólo una array vacio y una indicación de que el catálogo no ha sufrido cambios
					
                    //Realizamos la instancia de objeto Catalogo
                    $aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $questionElement->CatalogID);
					$intCatalogVersion = $aCatalog->getCatalogVersion();            
			        $intCurrCatVersion = (int) @$arrCatalogVersion[$aCatalog->CatalogID];
					$intUpdated = abs($intCurrCatVersion == 0 || $intCatalogVersion > $intCurrCatVersion);
                    
                    //llamamos a funcion que obtiene los atributos
                    $arrayAttribIDs = getQuestionAttributes($aRepository, $questionElement);
					
                    if (count($arrayAttribIDs) > 0 && $intUpdated) {
                        //Llamamos a la funcion que obtiene los valores de los atributos
					    //@JAPR 2012-03-08: Optimización de carga de los catálogos
		            	$strAttribsHash = md5(serialize($arrayAttribIDs));
		            	if (isset($arrCatalogValuesColl[$questionElement->CatalogID]) && isset($arrCatalogValuesColl[$questionElement->CatalogID][$strAttribsHash]))
		            	{
		            		//@JAPR 2012-05-07: Optimización, se dejará de enviar el array de valores de los catálogos de preguntas para que sea
		            		//reutilizado el catálogo de la encuesta, de esta forma de ahorrará memoria y tiempo de descarga a las Apps
		            		if ($appVersion < esvGlobalCatalogs)
		            		{
			            		$arrayAttribValues = $arrCatalogValuesColl[$questionElement->CatalogID][$strAttribsHash];
		            		}
		            		//@JAPR
		            	}
		            	else 
		            	{
		            		//@JAPR 2012-05-07: Optimización, se dejará de enviar el array de valores de los catálogos de preguntas para que sea
		            		//reutilizado el catálogo de la encuesta, de esta forma de ahorrará memoria y tiempo de descarga a las Apps
		            		if ($appVersion < esvGlobalCatalogs)
		            		{
        	                	$arrayAttribValues = getQuestionAttributeValues($aRepository, $questionElement, $aCatalog, $arrayAttribIDs);
		            		}
        	                //@JAPR
        	                $arrCatalogValuesColl[$questionElement->CatalogID][$strAttribsHash] = $arrayAttribValues;
		            	}
		            	//@JAPR
                    }
                }

                if (($questionElement->QTypeID == 2 && $questionElement->UseCatalog == 0) || $questionElement->QTypeID == 3) {
                    $arrayOptions = getQuestionOptionValues($aRepository, $questionElement);
                    //$arrayOptionsRules = getQuestionOptionRules($aRepository, $questionElement);
                }

                if ($questionElement->QTypeID == 2 && $questionElement->UseCatalog == 0) {
                    $arrayOptionList = getQuestionOptionList($aRepository, $questionElement);
                    if($questionElement->QDisplayMode == 3) {
                        $arrayOptionColList = getQuestionOptionColList($aRepository, $questionElement);
                    }
                    $arrayAllShowQuestionList = getQuestionAllShowQuestionList($aRepository, $questionElement);
                }
				
				//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo (campo CatMemberID)
                $intCatMemberParentID = 0;
                if ($questionElement->CatalogID > 0 && $questionElement->CatMemberID > 0)
                {
                	$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $questionElement->CatMemberID);
                	if (!is_null($aCatalogMember))
                	{
                		$intCatMemberParentID = $aCatalogMember->ClaDescrip;
                	}
                }
                
                $formattedQuestionMessage = $questionElement->QuestionMessage;
                $formattedQuestionMessage = BITAMSection::convertImagesToBase64($formattedQuestionMessage);

                //@JAPR 2012-03-21: Agregada la propiedad questionCatalogID para permitir soportar múltiples catálogos en la misma encuesta
				//@JAPR 2012-04-03: Agregada la opcion de Otro para capturar respuestas adicionales (AllowAdditionalAnswers)
				//@JAPR 2012-04-30: Agregada la opción para configurar el estilo visual del componente (Campo QComponentStyleID)
				//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
				//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo (campo CatMemberID)
                $arrQuestionData = array(
                    'questionID' => $questionElement->QuestionID,
                    'questionNumber' => $questionElement->QuestionNumber,
                    'questionText' => utf8_encode($questionElement->DisplayQuestion),
                    'questionType' => $questionElement->QTypeID,
                    //@JAPR 2012-08-10: Validado que si la máscara contiene el símbolo de %, el valor sea dividido entre 100
                    'questionMax' => ((strpos($questionElement->FormatMask, '%') !== false)?($questionElement->MaxValue/100):$questionElement->MaxValue),
                    'questionMin' => ((strpos($questionElement->FormatMask, '%') !== false)?($questionElement->MinValue/100):$questionElement->MinValue),
                    //@JAPR
                    'qDisplayMode' => $questionElement->QDisplayMode,
                    'oneChoicePer' => $questionElement->OneChoicePer,
                    'qMCInputType' => $questionElement->MCInputType,
                    'qLength' => $questionElement->QLength,
                    'hasReqPhoto' => $questionElement->HasReqPhoto,
                    'hasReqComment' => $questionElement->HasReqComment,
                    'hasActions' => $questionElement->HasActions,
                    'isReqQuestion' => $questionElement->IsReqQuestion,
                    'useCatalog' => $questionElement->UseCatalog,
                    'questionCatalogID' => $questionElement->CatalogID,
                    'questionCatMemberID' => $intCatMemberParentID,
		            'surveyCatalogVersion' => $intCatalogVersion,
		            'surveyCatalogUpdated' => $intUpdated,
                    'attributeIDs' => $arrayAttribIDs,
                    'attributeValues' => $arrayAttribValues,
                    'options' => $arrayOptions,
                    //'optionsRules' => $arrayOptionsRules,
                    'optionList' => $arrayOptionList,
                    'optionColList' => $arrayOptionColList,
                    'allShowQuestionList' => $arrayAllShowQuestionList,
                    'fatherQuestionID' => $questionElement->FatherQuestionID,
                    'childQuestionID' => $questionElement->ChildQuestionID,
                    'outsideOfSection' => $questionElement->OutsideOfSection,
                    //'nextQuestion' => $questionElement->GoToQuestion,
                    'nextSection' => $questionElement->GoToQuestion,
                    'allowAdditionalAnswers' => $questionElement->AllowAdditionalAnswers,
                    'qComponentStyle' => $questionElement->QComponentStyleID,
                    'categoryID' => $questionElement->CategoryID,
                    'responsibleID' => $questionElement->ResponsibleID,
                    'daysDueDate' => $questionElement->DaysDueDate,
					//@JAPR 2012-08-02: Agregado el formato de preguntas numéricas (campo FormatMask)
					'format' => $questionElement->FormatMask,
					'sourceQuestionID' => $questionElement->SourceQuestionID,
					'maxChecked' => $questionElement->MaxChecked,
					//@JAPR
					'questionImageText' => BITAMSection::convertImagesToBase64($questionElement->QuestionImageText),
					'dateFormat' => utf8_encode(str_replace("m","M",$questionElement->DateFormatMask)),
					'skipSection' => $questionElement->GoToQuestion,
					'questionMessage' => utf8_encode($formattedQuestionMessage),
					'defaultValue' => $questionElement->DefaultValue,
					'questionsToEval' => $questionElement->QuestionsToEval
                );
                
				//@JAPR 2012-05-29: Agregada la opción para indicar el número de decimales de las preguntas numéricas
				if ($appVersion >= esvSurveyReschedule)
				{
					$arrQuestionData['decimalPlaces'] = $questionElement->DecimalPlaces;
				}
                //@JAPR
                
                //Se remueven opciones que no aplican para el tipo de pregunta, esto en busca de reducir el JSon a regresar a las Apps
                if ($questionElement->QTypeID != qtpAction)
                {
                	unset($arrQuestionData['categoryID']);
                	unset($arrQuestionData['responsibleID']);
                	unset($arrQuestionData['daysDueDate']);
                }
                $questionList[] = $arrQuestionData;
            }

            $sendThumbnails = 0;
            $enableHyperlinks = 0;
            $formattedText = "";

            if ($sectionElement->SectionType == sectFormatted) {
                $sendThumbnails = $sectionElement->SendThumbnails;
                $enableHyperlinks = $sectionElement->EnableHyperlinks;
                $formattedText = $sectionElement->FormattedText;

                //Si esta habilitado el envio de imagenes entonces vamos a pasarlas a codigo base64
                //en caso contrario se convierten en etiquetas con la descripcion definida en Image Description
                if ($sendThumbnails == 1) {
                    $formattedText = BITAMSection::convertImagesToBase64($formattedText);
                } else {
                    $formattedText = BITAMSection::convertImagesToLabels($formattedText);
                }

                //Si se deshabilito los hipervinculos entonces se eliminan dichos anchors
                if ($enableHyperlinks == 0) {
                    $formattedText = BITAMSection::removeHyperlinks($formattedText);
                }
            }

			//@JAPR 2012-07-26: Agregado el salto de sección a sección
            $sectionList[] = array(
                'sectionID' => $sectionElement->SectionID,
                'sectionName' => utf8_encode($sectionElement->SectionName),
                'sectionType' => $sectionElement->SectionType,
                'sectionDynamicID' => $dynamicSectionID,
                'formattedText' => utf8_encode($formattedText),
                'sendThumbnails' => $sendThumbnails,
                'enableHyperlinks' => $enableHyperlinks,
                'nextSectionID' => $sectionElement->NextSectionID,
                'questions' => $questionList
            );
        }

		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
        $arrSurveyData = array(
            'surveyID' => $surveyID,
            'surveyName' => utf8_encode($surveyName),
            'surveyAllowPartialSave' => $surveyAllowPartialSave,
            'surveyCatalogID' => $surveyCatalogID,
            'surveyHasDynamicSection' => $surveyHasDynamicSection,
            'surveyDynamicSections' => $surveyDynamicSections,
            'surveyCatalogVersion' => $intSrvCatalogVersion,
            'surveyCatalogUpdated' => $intSrvUpdated,
            'surveyLastDateID' => utf8_encode($surveyLastDateID),
            'versionNum' => $versionNum,
            'ElementQuestionID' => $elementQuestionID,
            'surveyHasSignature' => $surveyHasSignature,
            'syncWhenSaving' => (int) @$survey['syncWhenSaving']
        );

		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if ($appVersion >= esvSurveyReschedule)
		{
			$arrSurveyData['enableReschedule'] = $survey['enableReschedule'];
		}
        $arrSurveyData['surveyAttributeIDs'] = $arraySurveyAttribIDs;
        $arrSurveyData['surveyAttributeValues'] = $arraySurveyAttribValues;
		$arrSurveyData['sections'] = $sectionList;
        
		$surveyList[] = $arrSurveyData;
		
		//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
        $arrSurveyIDs[$surveyID] = $surveyID;
        //@JAPR
    }
	
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	//Obtiene la lista de los catálogos que son usados por preguntas en las encuestas usadas, pero que no están asociados a ninguna de ellas por
	//lo que no van incluidos ya como parte de las definiciones en el array de encuestas
	if ($appVersion >= esvGlobalCatalogs && count($arrSurveyIDs) > 0)
	{
		$strSurveyIDs = implode(',', $arrSurveyIDs);
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
        $sql = "SELECT DISTINCT A.CatalogID 
        	FROM SI_SV_Question A 
        	WHERE A.CatalogID > 0 AND A.QTypeID <> ".qtpOpenNumeric." AND A.SurveyID IN (".$strSurveyIDs.") AND A.CatalogID NOT IN ( 
				SELECT DISTINCT B.CatalogID 
				FROM SI_SV_Survey B 
				WHERE B.CatalogID > 0 AND B.SurveyID IN (".$strSurveyIDs. ") 
			)";		
        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset === false) {
            $return['error'] = true;
            $return['errmsg'] = 'Error Getting Catalog List';
        }
		
        $arrCatalogIDs = array();
        while (!$recordset->EOF) {
            $catalogID = (int) $recordset->fields["catalogid"];
	        $arrCatalogIDs[$catalogID] = $catalogID;
            $recordset->MoveNext();
        }
        
        if (count($arrCatalogIDs) > 0)
        {
        	foreach ($arrCatalogIDs as $catalogID)
        	{
	            $aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $catalogID);
	            $arrayCatalogAttribIDs = getCatalogAttributes($aRepository, $catalogID);
	            //@JAPR 2012-08-03: Corregido un bug, se estaba usando la variable del catálogo como si se hubiera leído de la encuesta
				$intSrvCatalogVersion = $aCatalog->getCatalogVersion();            
		        $intCurrCatVersion = (int) @$arrCatalogVersion[$aCatalog->CatalogID];
		        //@JAPR
				$intSrvUpdated = abs($intCurrCatVersion == 0 || $intSrvCatalogVersion > $intCurrCatVersion);
				$arrayCatalogAttribValues = array();
	            if (count($arrayCatalogAttribIDs) > 0 && $intSrvUpdated) 
	            {
					$arrayCatalogAttribValues = getSurveyAttributeValues($aRepository, null, $aCatalog, $arrayCatalogAttribIDs);
	            }
	            
		        $catalogList[] = array(
		            'catalogID' => $catalogID,
		            'catalogName' => utf8_encode($aCatalog->CatalogName),
		            'catalogVersion' => $intSrvCatalogVersion,
		            'catalogUpdated' => $intSrvUpdated,
		            'catalogAttributeIDs' => $arrayCatalogAttribIDs,
		            'catalogAttributeValues' => $arrayCatalogAttribValues
		        );
        	}
        }
	}
	
	//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
	$categoryCatalogID = 0;
	$categoryAttributeID = 0;
	if ($appVersion >= esvActionQuestions)
	{
		$categoryList = BITAMCatalog::GetCategories($aRepository, false, true);
		/*
		require_once('settingsvariable.inc.php');
		$objSettings = BITAMSettingsVariable::NewInstanceAll($aRepository);
		if (isset($objSettings->ArrayVariableValues['CATEGORYCATALOG']))
		{
			$categoryCatalogID = abs((int) $objSettings->ArrayVariableValues['CATEGORYCATALOG']);
		}
		if (isset($objSettings->ArrayVariableValues['CATEGORYMEMBER']))
		{
			$categoryAttributeID = abs((int) $objSettings->ArrayVariableValues['CATEGORYMEMBER']);
		}
		
		if ($categoryCatalogID > 0 && $categoryAttributeID > 0)
		{
			$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $categoryCatalogID);
			$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $categoryAttributeID);
			if (!is_null($aCatalog) && !is_null($aCatalogMember))
			{
				$fieldKey = "RIDIM_".$aCatalog->ParentID."KEY";
				$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
		        $sql = "SELECT MIN(".$fieldKey.") AS CategoryID, ".$fieldName." AS CategoryName FROM RIDIM_".$aCatalog->ParentID." WHERE ".$fieldKey." > 1 GROUP BY ".$fieldName." ORDER BY 2";
		        $recordset = $aRepository->DataADOConnection->Execute($sql);
		        if ($recordset === false) {
		            $return['error'] = true;
		            $return['errmsg'] = 'Error Getting Category List';
		        }
				
		        while (!$recordset->EOF) {
			        $categoryList[] = array(
			            'categoryID' => (int) $recordset->fields["categoryid"],
			            'categoryName' => utf8_encode((string) $recordset->fields["categoryname"])
			        );
		            $recordset->MoveNext();
		        }
			}
		}
		*/
	}
	
	//@JAPR 2012-05-15: Agregada la lista de usuarios para las preguntas tipo Action (sólo si realmente existen este tipo de preguntas)
	if ($appVersion >= esvActionQuestions && $userDimClaDescrip > 0)
	{
		//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
		$arrUserList = BITAMCatalog::GetUsers($aRepository, false, true);
		//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
		$arrDefaultOpts = array();
        $arrDefaultOpts[] = array(
            'userID' => 0,
            'userName' => utf8_encode("Myself")
        );
        $arrDefaultOpts[] = array(
            'userID' => -1,
            'userName' => utf8_encode("Supervisor 1")
        );
        $arrDefaultOpts[] = array(
            'userID' => -2,
            'userName' => utf8_encode("Supervisor 2")
        );
        $arrDefaultOpts[] = array(
            'userID' => -3,
            'userName' => utf8_encode("Supervisor 3")
        );
		$userList = array();
        $userList = array_merge($arrDefaultOpts, $arrUserList);
		
		/*
		$fieldKey = "RIDIM_".$userDimClaDescrip."KEY";
        $sql = "SELECT DISTINCT ".$fieldKey." AS UserId, DSC_".$userDimClaDescrip." AS UserName FROM RIDIM_".$userDimClaDescrip." WHERE ".$fieldKey." > 0";
        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset === false) {
            $return['error'] = true;
            $return['errmsg'] = 'Error Getting User List';
        }
		
        $arrCatalogIDs = array();
        while (!$recordset->EOF) {
	        $userList[] = array(
	            'userID' => (int) $recordset->fields["userid"],
	            'userName' => utf8_encode((string) $recordset->fields["username"])
	        );
            $recordset->MoveNext();
        }
        */
	}
    //@JAPR
    
    //$return['error'] = false;
    $return['surveyList'] = $surveyList;
	//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
	if ($appVersion >= esvGlobalCatalogs)
	{
    	$return['catalogList'] = $catalogList;
	}
	//@JAPR 2012-05-15: Agregada la lista de usuarios para las preguntas tipo Action (sólo si realmente existen este tipo de preguntas)
	if ($appVersion >= esvActionQuestions)
	{
		$return['userList'] = $userList;
	}
	if ($appVersion >= esvActionQuestions)
	{
		$return['categoryCatalogID'] = $categoryCatalogID;
		$return['categoryAttributeID'] = $categoryAttributeID;
		$return['categoryList'] = $categoryList;
	}
	
	//@JAPR 2012-08-02: Agregado el concepto de Atributo clave los cuales se ocultan de las capturas
	if ($appVersion >= esvFormatMaskHideAttribs)
	{
        $arrAttributesHidden = array();
        $arrAttributesHidden['-1'] = 1;	//Para forzar a que se regrese como objeto y no como array
        $sql = "SELECT A.MemberID, A.ParentID 
        	FROM SI_SV_CatalogMember A
        	WHERE A.DescriptorID > 0";		
        $recordset = $aRepository->DataADOConnection->Execute($sql);
        if ($recordset === false) {
            $return['error'] = true;
            $return['errmsg'] = 'Error getting key attributes';
        }
		else
		{
	        while (!$recordset->EOF) {
	            $parentID = (int) $recordset->fields["parentid"];
		        $arrAttributesHidden[$parentID] = 1;
	            $recordset->MoveNext();
	        }
		}
		
		$return['attributesHidden'] = $arrAttributesHidden;
	}
    //@JAPR
    return $return;
}

/* * *************** */

function getSectionAttributes($aRepository, $aSection) {
	//@JAPR 2012-03-07: Esto es innecesario, $parentCladescrip no se usa realmente
    //Obtener cla_descrip del catalogo
    /*
    $sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = " . $aSection->CatalogID;

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_Catalog " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $parentCladescrip = (int) $aRS->fields["parentid"];
    */

    $sql = "SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID = " . $aSection->CatalogID . " AND MemberID = " . $aSection->CatMemberID;

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $memberOrder = (int) $aRS->fields["memberorder"];

    $sql = "SELECT ParentID, MemberName FROM SI_SV_CatalogMember WHERE CatalogID = " . $aSection->CatalogID . " AND MemberOrder <= " . $memberOrder . " ORDER BY MemberOrder";

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $arrayAttribIDs = array();
    $i = 0;

    while (!$aRS->EOF) {
        $cla_descrip = (int) $aRS->fields["parentid"];
        $membername = $aRS->fields["membername"];
        $arrayAttribIDs[$i] = array();
        $arrayAttribIDs[$i]["sectionAttribID"] = $cla_descrip;
        $arrayAttribIDs[$i]["sectionAttribName"] = utf8_encode($membername);

        $i++;
        $aRS->MoveNext();
    }

    return $arrayAttribIDs;
}

//@JAPRDescontinuada: Al día 2014-06-11 no se encontró ningún proceso que la involucrara
function getSectionAttributeValues($aRepository, $aSection, $aCatalog, $arrayAttribIDs, $position=0, $filter="") {
    $i = 0;
    $attribValues = array();
    $resultFilter = $aCatalog->generateResultFilterByUser($_SESSION["PABITAM_UserID"]);

    $attribID = $arrayAttribIDs[$position]["attribID"];

    $fieldName = "DSC_" . $attribID;
    $fieldKey = $aCatalog->TableName . "KEY";
    $tableName = $aCatalog->TableName;

    $where = " WHERE " . $fieldKey . " <> 1 ";
    $where.=$filter;

    //Concatenamos la seguridad del usuario definida en el catalogo
    if ($resultFilter != "") {
        $where.=" AND ";
    }

    $where.=$resultFilter;

    $sql = "SELECT DISTINCT " . $fieldName . " FROM " . $tableName . $where . " ORDER BY " . $fieldName;

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    while (!$aRS->EOF) {
        $attribValues[$i] = array();

        $fieldValue = $aRS->fields[strtolower($fieldName)];
        $attribValues[$i]['elementValue'] = utf8_encode($fieldValue);
        $attribValues[$i]['elementArray'] = array();

        $newFilter = $filter . " AND " . $fieldName . " = " . $aRepository->DataADOConnection->Quote($fieldValue);

        if (($position + 1) < count($arrayAttribIDs)) {
            $attribValues[$i]['elementArray'] = getSectionAttributeValues($aRepository, $aSection, $aCatalog, $arrayAttribIDs, $position + 1, $newFilter);
        }

        $i++;
        $aRS->MoveNext();
    }

    return $attribValues;
}

/* * *************** */

function getSurveyAttributes($aRepository, $aSurvey) {

    $sql = "SELECT ParentID, MemberName FROM SI_SV_CatalogMember WHERE CatalogID = " . $aSurvey->CatalogID . " ORDER BY MemberOrder";

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }
    $arrayAttribIDs = array();
    $i = 0;

    while (!$aRS->EOF) {
        $cla_descrip = (int) $aRS->fields["parentid"];
        $membername = $aRS->fields["membername"];
        $arrayAttribIDs[$i] = array();
        $arrayAttribIDs[$i]["attribID"] = $cla_descrip;
        $arrayAttribIDs[$i]["attribName"] = utf8_encode($membername);

        $i++;
        $aRS->MoveNext();
    }

    return $arrayAttribIDs;
}

function getQuestionAttributes($aRepository, $aQuestion) {
	//@JAPR 2012-03-07: Esto es innecesario, $parentCladescrip no se usa realmente
	/*
    //Obtener cla_descrip del catalogo
    $sql = "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = " . $aQuestion->CatalogID;

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_Catalog " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $parentCladescrip = (int) $aRS->fields["parentid"];
    */

    $sql = "SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID = " . $aQuestion->CatalogID . " AND MemberID = " . $aQuestion->CatMemberID;

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $memberOrder = (int) $aRS->fields["memberorder"];

    $sql = "SELECT ParentID, MemberName FROM SI_SV_CatalogMember WHERE CatalogID = " . $aQuestion->CatalogID . " AND MemberOrder <= " . $memberOrder . " ORDER BY MemberOrder";

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }

    $arrayAttribIDs = array();
    $i = 0;

    while (!$aRS->EOF) {
        $cla_descrip = (int) $aRS->fields["parentid"];
        $membername = $aRS->fields["membername"];
        $arrayAttribIDs[$i] = array();
        $arrayAttribIDs[$i]["attribID"] = $cla_descrip;
        $arrayAttribIDs[$i]["attribName"] = utf8_encode($membername);

        $i++;
        $aRS->MoveNext();
    }

    return $arrayAttribIDs;
}

//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
//@JAPRWarning: En realidad el parámetro $aSurvey no se requiere, así que en lugar de generar una nueva función que sólo reciba el catálogo, se 
//reutilizará esta enviando $aSurvey como null puesto que el resultado en ambas funciones sería el mismo
//@JAPRDescontinuada: Esto aplicaba para la captura Web de v3
function getSurveyAttributeValues($aRepository, $aSurvey, $aCatalog, $arrayAttribIDs, $position=0, $filter="") 
{
	$i=0;
	$attribValues = array();
	$resultFilter = $aCatalog->generateResultFilterByUser($_SESSION["PABITAM_UserID"]);
	
	$strFields = "";
	$arrIdxs = array();
	$arrLast = array();
	
	$lim = count($arrayAttribIDs);
	
	for($i=0; $i<$lim; $i++)
	{
		if($strFields!="")
		{
			$strFields.=",";
		}

		$attribID = $arrayAttribIDs[$i]["attribID"];
		$strFields.="DSC_".$attribID;

		$arrIdxs[$i] = -1;
		$arrLast[$i] = null;

		eval('$attrib'.$i.'=array();');
	}

	$fieldKey = $aCatalog->TableName."KEY";
	$tableName = $aCatalog->TableName;
	
	$where = " WHERE ".$fieldKey." <> 1 ";
	$where.=$filter;
	
	//Concatenamos la seguridad del usuario definida en el catalogo
	if($resultFilter!="")
	{
		$where.=" AND ";
	}

	$where.=$resultFilter;

	$sql = "SELECT DISTINCT ".$fieldKey.", ".$strFields." FROM ".$tableName.$where." ORDER BY ".$strFields;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if($aRS->EOF)
	{
		return $attribValues;
	}
	
	while(!$aRS->EOF)
	{
		for($i=0; $i<$lim; $i++)
		{
			$field = "DSC_".$arrayAttribIDs[$i]["attribID"];
			$attribVal = $aRS->fields[strtolower($field)];
			
			if($i!=($lim-1))
			{
				if($attribVal!==$arrLast[$i])
				{
					if($arrLast[$i]!==null)
					{
						for($j=($lim-2); $j>=$i; $j--)
						{
							eval('$attrib'.$j.'[$arrIdxs[$j]]["elementArray"]=$attrib'.($j+1).';');
						}
						
						for($j=$i+1;$j<$lim;$j++)
						{
							eval('$attrib'.$j.'=array();');
							$arrIdxs[$j] = -1;
							$arrLast[$j] = null;
						}
					}
					
					$arrIdxs[$i] = $arrIdxs[$i] + 1;
					$valueFieldKey = $aRS->fields[strtolower($fieldKey)];
					eval('$attrib'.$i.'[$arrIdxs[$i]]["elementID"]=$valueFieldKey;');
					eval('$attrib'.$i.'[$arrIdxs[$i]]["elementValue"]=utf8_encode($attribVal);');
				}
			}
			else 
			{
				$valueFieldKey = $aRS->fields[strtolower($fieldKey)];

				$arrIdxs[$i]=$arrIdxs[$i]+1;
				eval('$attrib'.$i.'[$arrIdxs[$i]]["elementID"]=$valueFieldKey;');
				eval('$attrib'.$i.'[$arrIdxs[$i]]["elementValue"]=utf8_encode($attribVal);');
				eval('$attrib'.$i.'[$arrIdxs[$i]]["elementArray"]=array();');
			}
			
			$arrLast[$i] = $attribVal;
		}
		
		$aRS->MoveNext();
	}
	
	if($lim>1)
	{
		for($j=$lim-2;$j>=0;$j--)
		{
			eval('$attrib'.$j.'[$arrIdxs[$j]]["elementArray"]=$attrib'.($j+1).';');
		}
	}
	
	$attribValues = $attrib0;
	
	return $attribValues;
}

//@JAPRDescontinuada: Esto aplicaba para la captura Web de v3
function getQuestionAttributeValues($aRepository, $aQuestion, $aCatalog, $arrayAttribIDs, $position=0, $filter="")
{
	$i=0;
	$attribValues = array();
	$resultFilter = $aCatalog->generateResultFilterByUser($_SESSION["PABITAM_UserID"]);
	
	$strFields = "";
	$arrIdxs = array();
	$arrLast = array();
	
	$lim = count($arrayAttribIDs);
	
	for($i=0; $i<$lim; $i++)
	{
		if($strFields!="")
		{
			$strFields.=",";
		}

		$attribID = $arrayAttribIDs[$i]["attribID"];
		$strFields.="DSC_".$attribID;

		$arrIdxs[$i] = -1;
		$arrLast[$i] = null;

		eval('$attrib'.$i.'=array();');
	}

	$fieldKey = $aCatalog->TableName."KEY";
	$tableName = $aCatalog->TableName;
	
	$where = " WHERE ".$fieldKey." <> 1 ";
	$where.=$filter;
	
	//Concatenamos la seguridad del usuario definida en el catalogo
	if($resultFilter!="")
	{
		$where.=" AND ";
	}

	$where.=$resultFilter;

	$sql = "SELECT DISTINCT ".$strFields." FROM ".$tableName.$where." ORDER BY ".$strFields;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if($aRS->EOF)
	{
		return $attribValues;
	}
	
	while(!$aRS->EOF)
	{
		for($i=0; $i<$lim; $i++)
		{
			$field = "DSC_".$arrayAttribIDs[$i]["attribID"];
			$attribVal = $aRS->fields[strtolower($field)];
			
			if($i!=($lim-1))
			{
				if($attribVal!==$arrLast[$i])
				{
					if($arrLast[$i]!==null)
					{
						for($j=($lim-2); $j>=$i; $j--)
						{
							eval('$attrib'.$j.'[$arrIdxs[$j]]["elementArray"]=$attrib'.($j+1).';');
						}
						
						for($j=$i+1;$j<$lim;$j++)
						{
							eval('$attrib'.$j.'=array();');
							$arrIdxs[$j] = -1;
							$arrLast[$j] = null;
						}
					}
					
					$arrIdxs[$i] = $arrIdxs[$i] + 1;
					eval('$attrib'.$i.'[$arrIdxs[$i]]["elementValue"]=utf8_encode($attribVal);');
				}
			}
			else 
			{
				$arrIdxs[$i]=$arrIdxs[$i]+1;
				eval('$attrib'.$i.'[$arrIdxs[$i]]["elementValue"]=utf8_encode($attribVal);');
				eval('$attrib'.$i.'[$arrIdxs[$i]]["elementArray"]=array();');
			}
			
			$arrLast[$i] = $attribVal;
		}
		
		$aRS->MoveNext();
	}
	
	if($lim>1)
	{
		for($j=$lim-2;$j>=0;$j--)
		{
			eval('$attrib'.$j.'[$arrIdxs[$j]]["elementArray"]=$attrib'.($j+1).';');
		}
	}
	
	$attribValues = $attrib0;
	
	return $attribValues;
}

function getQuestionOptionValues($aRepository, $aQuestion) {
    $arrayOptions = array();

    foreach ($aQuestion->SelectOptions as $optionKey => $optionValue) {
        $arrayOptions[$optionKey] = array();
        $arrayOptions[$optionKey]["optionValue"] = utf8_encode($optionValue);
        
        //@JAPR 2012-07-24: Corregido un bug, faltaba un Quote en el filtro de DisplayText
        $sql = 'SELECT t1.displayImage FROM SI_SV_QAnswers t1
                where t1.QuestionID = '.$aQuestion->QuestionID.' AND t1.DisplayText = '.$aRepository->DataADOConnection->Quote($optionValue);
        
        $aRS = $aRepository->DataADOConnection->Execute($sql);
                
        if ($aRS === false)
        {
	    	//@JAPR 2012-07-24: Corregido un bug en el reporte de errores, estaba usando una instancia que no existía
            die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
        }
        
        $arrayOptions[$optionKey]["displayImage"] = BITAMSection::convertImagesToBase64((string)$aRS->fields["displayimage"]);
    }

    return $arrayOptions;
}

function getQuestionOptionRules($aRepository, $aQuestion) {
    $arrayOptionsRules = array();

    foreach ($aQuestion->NextQuestion as $optionKey => $optionRule) {
        $arrayOptionsRules[$optionKey] = array();
        $arrayOptionsRules[$optionKey]["optionRule"] = (int) $optionRule;
    }

    return $arrayOptionsRules;
}

function getQuestionOptionList($aRepository, $aQuestion) {
    $arrayOptionList = array();
    
    
    foreach ($aQuestion->SelectOptions as $optionKey => $optionValue) {
        
        $arrayOptionList[$optionKey] = array();
        $arrayOptionList[$optionKey]["optionName"] = utf8_encode($optionValue);
        $arrayOptionList[$optionKey]["consecutiveID"] = utf8_encode($aQuestion->QConsecutiveIDs[$optionKey]);
        
        //@JAPR 2012-07-24: Corregido un bug, faltaba un Quote en el filtro de DisplayText
        $sql = 'SELECT t1.NextSection FROM SI_SV_QAnswers t1
                where t1.QuestionID = '.$aQuestion->QuestionID.' AND t1.DisplayText = '.$aRepository->DataADOConnection->Quote($optionValue);
        
        $aRS = $aRepository->DataADOConnection->Execute($sql);
                
        if ($aRS === false)
        {
	    	//@JAPR 2012-07-24: Corregido un bug en el reporte de errores, estaba usando una instancia que no existía
            die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
        }
        
        $arrayOptionList[$optionKey]["nextSection"] = (int)$aRS->fields["nextsection"];
        
        $arrayOptionList[$optionKey]["showQuestionList"] = array();
        //@JAPR 2012-07-24: Corregido un bug, faltaba un Quote en el filtro de DisplayText
        $sql = 'SELECT t1.ShowQuestionID FROM SI_SV_ShowQuestions t1, SI_SV_QAnswers t2
                where t2.QuestionID = '.$aQuestion->QuestionID.' AND (t2.DisplayText = '.$aRepository->DataADOConnection->Quote($arrayOptionList[$optionKey]["optionName"]).' AND t2.QuestionID='.$aQuestion->QuestionID.') AND t1.ConsecutiveID = t2.ConsecutiveID;';
        
        $aRS = $aRepository->DataADOConnection->Execute($sql);
                
        if ($aRS === false)
        {
	    	//@JAPR 2012-07-24: Corregido un bug en el reporte de errores, estaba usando una instancia que no existía
            die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
        }
        
        $i=0;
        while (!$aRS->EOF)
        {
            $showQuestionID = $aRS->fields["showquestionid"];
            $toshowQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $showQuestionID);
            //@JAPR 2012-06-18: Corregido un bug, si la pregunta del Show Question no existía, estaba generando errores de PhP que provocaban
            //que la Aplicación móvil ya no continuara
            if (!is_null($toshowQuestion))
            {
	            $arrayOptionList[$optionKey]["showQuestionList"][$i]["showQuestionID"] = $showQuestionID;
	            $arrayOptionList[$optionKey]["showQuestionList"][$i]["questionType"] = $toshowQuestion->QTypeID;
	            $arrayOptionList[$optionKey]["showQuestionList"][$i]["qDisplayMode"] = $toshowQuestion->QDisplayMode;
	            $i++;
            }
            //@JAPR
            $aRS->MoveNext();
        }
    }

    return $arrayOptionList;
}

function getQuestionOptionColList($aRepository, $aQuestion) {
    $arrayOptionColList = array();
    foreach ($aQuestion->SelectOptionsCol as $optionKey => $optionValue) {
        $arrayOptionColList[$optionKey] = array();
        $arrayOptionColList[$optionKey]["optionName"] = utf8_encode($optionValue);
        $arrayOptionColList[$optionKey]["consecutiveID"] = utf8_encode($aQuestion->QConsecutiveIDsCol[$optionKey]);
    }

    return $arrayOptionColList;
}

function getQuestionAllShowQuestionList($aRepository, $aQuestion) {
    $arrayAllShowQuestionList = array();
    $sql = 'SELECT t1.ShowQuestionID from SI_SV_ShowQuestions t1 WHERE t1.QuestionID = '.$aQuestion->QuestionID;
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if ($aRS === false)
    {
    	//@JAPR 2012-07-24: Corregido un bug en el reporte de errores, estaba usando una instancia que no existía
        die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
    }
    $i=0;
    while (!$aRS->EOF)
    {
        $showQuestionID = $aRS->fields["showquestionid"];
        $toshowQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $showQuestionID);
        //@JAPR 2012-06-18: Corregido un bug, si la pregunta del Show Question no existía, estaba generando errores de PhP que provocaban
        //que la Aplicación móvil ya no continuara
        if (!is_null($toshowQuestion))
        {
	        $arrayAllShowQuestionList[$i]["showQuestionID"] = $showQuestionID;
	        $arrayAllShowQuestionList[$i]["questionType"] = $toshowQuestion->QTypeID;
	        $arrayAllShowQuestionList[$i]["qDisplayMode"] = $toshowQuestion->QDisplayMode;
	        $i++;
        }
        //@JAPR
        $aRS->MoveNext();
    }
    return $arrayAllShowQuestionList;
}

//@JAPR 2012-05-07: Agregada la colección de catalogos usados pero no asociados a ninguna encuesta
function getCatalogAttributes($aRepository, $aCatalogID) {

    $sql = "SELECT ParentID, MemberName FROM SI_SV_CatalogMember WHERE CatalogID = " . $aCatalogID . " ORDER BY MemberOrder";
    //@JAPR

    $aRS = $aRepository->DataADOConnection->Execute($sql);

    if (!$aRS || $aRS->EOF) {
        //conchita 1-nov-2011 ocultar mensajes sql
        die("(".__METHOD__.") ".translate("Error accessing") . " SI_SV_CatalogMember " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql);
    }
    $arrayAttribIDs = array();
    $i = 0;

    while (!$aRS->EOF) {
        $cla_descrip = (int) $aRS->fields["parentid"];
        $membername = $aRS->fields["membername"];
        $arrayAttribIDs[$i] = array();
        $arrayAttribIDs[$i]["attribID"] = $cla_descrip;
        $arrayAttribIDs[$i]["attribName"] = utf8_encode($membername);

        $i++;
        $aRS->MoveNext();
    }

    return $arrayAttribIDs;
}
//@JAPR


// ***********************************************************************************************************************
// ***********************************************************************************************************************
// ***********************************************************************************************************************
//Estos servicios inicialmente serán llamados desde los propios PhPs, pero se pudiesen llamar desde las aplicaciones o cualquier otro
//medio vía HTTP, sólo que habrá que extenderlos en esos casos para regresar la información formateada de manera que quien la invoque la
//pueda leer sin dejar de enviar lo que ya actualmente se regresa para el caso original (agreguen un parámetro "responseformat" o algo así)
//Todos se basan en el nuevo esquema de recibir los parámetros de 'responseformat' y son enviados mediante el parámetro 'action'
// ***********************************************************************************************************************
// ***********************************************************************************************************************
// ***********************************************************************************************************************

// ***********************************************************************************************************************
/* getSurveysByAttribute */
// ***********************************************************************************************************************
//Regresa la lista de encuestas a las que tiene acceso el usuario y que tienen por lo menos una pregunta que utiliza el atributo especificado
function getSurveysByAttribute($aRepository)
{
	global $strResponseFormat;
	
	$blnDebug = getParamValue('debug', 'both', "(boolean)");
	$intAttribID = getParamValue('attributeid', 'both', "(int)");
	if ($intAttribID <= 0)
	{
    	generateErrorMessage(-101, "The catalog's attribute was not specified");
	}
    $roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $_SESSION["PABITAM_UserID"]);

	//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
	//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
	//encadenan como Padres/Hijas de otras preguntas Single Choice
    $sql = "SELECT DISTINCT A.SurveyID, A.SurveyName".
		" FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C, SI_SV_Question D".
		" WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = ".$_SESSION["PABITAM_UserID"].
		" AND A.Status IN (0, 1) AND A.SurveyID = D.SurveyID AND D.CatMemberID = ".$intAttribID." AND D.QTypeID <> ".qtpOpenNumeric;
    //Obtener el listado de encuestas disponibles para el usuario logueado x rol
    if (count($roles) > 0)
    {
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
    	$sql .= " UNION ".
			" SELECT DISTINCT A.SurveyID, A.SurveyName".
			" FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C, SI_SV_Question D".
			" WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN ( ". implode(",", $roles). " )".
				" AND A.Status IN (0, 1) AND A.SurveyID = D.SurveyID AND D.CatMemberID = ".$intAttribID." AND D.QTypeID <> ".qtpOpenNumeric;
    }
    $sql .= " ORDER BY 2";

    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if ($aRS === false) {
    	generateErrorMessage(-100, 'Error getting survey list by attribute: '.$aRepository->DataADOConnection->ErrorMsg());
    }
    $arraySurveys = array();
    while (!$aRS->EOF) 
    {
        $surveyID = (int) $aRS->fields["surveyid"];
        $surveyName = $aRS->fields["surveyname"];
        $arraySurveys[$surveyID] = $surveyID.(($blnDebug)?";":SV_ITEM).$surveyName;
        $aRS->MoveNext();
    }
    
	return implode((($blnDebug)?"\r\n":SV_DELIMITER), $arraySurveys);
}

//Regresa la lista de agendas con las referencias a los Surveys de cada una (ID, no la definición)
function getAgendaList($aRepository)
{
	global $strResponseFormat;
	
	require_once('surveyAgenda.inc.php');
	
	$agendaList = array();
	$aDate = getParamValue('date', 'both', '(string)', true);
	if (is_null($aDate))
	{
		$aDate = date('Y-m-d');
	}
	$aTime = getParamValue('time', 'both', '(string)', true);
	if (is_null($aTime))
	{
		$aTime = date('H:I');
	}
	$arrTime = explode(':', $aTime);
	foreach ($arrTime as $iIdx => $aTimeVal)
	{
		if ((int) $aTimeVal < 10)
		{
			$arrTime[$iIdx] = '0'.$aTimeVal;
		}
	}
	$aTime = implode(':', $arrTime);
	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	//@JAPR 2012-03-15: Realmente no se va a necesitar el filtro de hora, ya que las agendas son por día, así que del día actual cualquier
	//agenda configurada se debe incluir
	$anAgendaCollection = BITAMSurveyAgendaCollection::NewInstanceByUserAndDateTime($aRepository, $_SESSION["PABITAM_UserID"], $aDate, null, true);
	//$aSurveyCollection = BITAMSurveyCollection::NewInstance($aRepository);
	if (!is_null($anAgendaCollection) && count($anAgendaCollection->Collection) > 0)
	{
		/*
		$arrSurveys = array();
		foreach ($aSurveyCollection as $aSurvey)
		{
			$arrSurveys[]
		}
		*/
		//@JAPR 2012-03-26: Agregado el filtro jerárquico de la agenda
		$arrCatalogMembersByCatalogID = array();
		foreach ($anAgendaCollection as $anAgenda)
		{
			//@JAPR 2012-03-26: Agregado el filtro jerárquico de la agenda
			//Obtiene la colección de atributos del catálogo para usar al armar el query
			$intCatalogID = $anAgenda->CatalogID;
			$arrCatFilter = array();
			if ($intCatalogID > 0)
			{
				$arrCatalogMembers = null;
				if (isset($arrCatalogMembersByCatalogID[$intCatalogID]))
				{
					$arrCatalogMembers = $arrCatalogMembersByCatalogID[$intCatalogID];
				}
				else 
				{
					$arrCatalogMembers = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
					if (is_null($arrCatalogMembers))
					{
						$arrCatalogMembers = array();
					}
					$arrCatalogMembersByCatalogID[$intCatalogID] = $arrCatalogMembers;
				}
				
				//Obtiene el valor de todos los miembros del catálogo hasta el atributo por el que se filtra la agenda, para generar el 
				//Array completo del filtro para ese catálogo
				if (!is_null($arrCatalogMembers) && count($arrCatalogMembers->Collection) > 0)
				{
					$tableName = $anAgenda->CatalogTable;
					$fieldKey = "RIDIM_".$anAgenda->CatalogDim."KEY";
					$sql = "SELECT ";
					foreach ($arrCatalogMembers as $aCatalogMember)
					{
						$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
						if ($aCatalogMember->MemberID == $anAgenda->AttributeID)
						{
							//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
							$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$aRepository->DataADOConnection->Quote($anAgenda->FilterText)." ORDER BY ".$fieldKey;
							break;
						}
						else 
						{
							//Es un padre, lo agrega directamente al query
							$sql .= $fieldName.', ';
						}
					}
					$aRSCat = $aRepository->DataADOConnection->Execute($sql);
					if ($aRSCat)
					{
						if (!$aRSCat->EOF)
						{
							$arrCatFilter = array();
							foreach ($arrCatalogMembers as $aCatalogMember)
							{
								$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
								$arrCatFilter[] = utf8_encode((string) $aRSCat->fields[$fieldName]);
								if ($aCatalogMember->MemberID == $anAgenda->AttributeID)
								{
									break;
								}
							}
						}
					}
				}
			}
			//@JAPR
			
			$surveysList = $anAgenda->SurveyIDs;
	        $agendaList[] = array(
	            'agendaID' => $anAgenda->AgendaID,
	            //@JAPR 2012-03-26: Cambiado el nombre de las Agendas en los disp.
	            'agendaName' => utf8_encode($anAgenda->FilterText),
	            //@JAPR
	            'catalogID' => $intCatalogID,
	            'attributeID' => $anAgenda->AttributeID,
	            'filter' => utf8_encode($anAgenda->FilterText),
	            'catalogFilter' => $arrCatFilter,
	            'userID' => $anAgenda->UserID,
	            'captureStartDate' => utf8_encode($anAgenda->CaptureStartDate),
	            'captureStartTime' => utf8_encode($anAgenda->getAgendaTime()),
	            'lastDateID' => utf8_encode($anAgenda->LastDateID),
	            'surveys' => $anAgenda->SurveyIDs
			);
		}
	}
	return $agendaList;
}

//Regresa una respuesta según el tipo de formato que se espera
function generateErrorMessage($iErrCode = 0, $sErrDesc = '', $sHeader = '')
{
	global $strResponseFormat;
	
	switch ($strResponseFormat)
	{
		case 'json':		//JSon
			generateJSonErrorResponse($iErrCode, $sErrDesc, $sHeader);
			break;
		
		case 'text':
		default:			//Texto plano
			generateTextErrorResponse($iErrCode, $sErrDesc, $sHeader);
			break;
	}
}

//Regresa una respuesta en texto plano formateada para identificación fácil en llamadas desde un request Ajax con XML Object
function generateJSonErrorResponse($iErrCode = 0, $sErrDesc = '', $sHeader = '')
{
	global $appVersion;
	
	$return = array();
	$return['web_service_version'] = ESURVEY_SERVICE_VERSION;
	$return['request_version'] = $appVersion;
    $return['error'] = true;
    $return['errcode'] = $iErrCode;
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
    $return['errmsg'] = $sErrDesc;
	//@JAPR 2019-05-30: Corregido un bug, no se puede agregar un header si ya se había mandado una respuesta, ya que a partir de cierto evento comenzó a generar un 
	//Internal Server Error (HTTP 500) si se hacía de esa manera, así que se validará que los headers sean excluyentes al parámetro DebugBreak, además de atrapar toda salida
	//al buffer no atrapada para regresarla como una propiedad warningNotice de la respuesta en caso de haber algún texto (#7UZT8C)
	$strWarningsNotice = '';
	if ( !isset($_REQUEST["DebugBreak"]) ) {
		//Adicional a activar el header en esta situación, agregará la lectura y desactivación del buffer por cualquier error no controlado que hubiera sido regresado
		$strWarningsNotice = trim(ob_get_contents());
		if ( $strWarningsNotice ) {
			$return['warningNotice'] = $strWarningsNotice;
		}
		ob_end_clean();
		
		if ($sHeader != '')
		{
			@header($sHeader);
		}
		header("Content-Type: text/javascript");
	}
	//@JAPR
	
    //@JAPR 2012-03-14: Corregido un bug, la función de Callback se debe invocar siempre que se especifique y se regrese un JSon
    if (isset($_GET['jsonpCallback'])) {
        echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
    } else {
	    echo(json_encode($return));
    }
    //@JAPR
    die();
}

//Regresa una respuesta en texto plano formateada para identificación fácil en llamadas desde un request Ajax con XML Object
function generateTextErrorResponse($iErrCode = 0, $sErrDesc = '', $sHeader = '')
{
	global $appVersion;
	
	$blnDebug = getParamValue('debug', 'both', "(boolean)");
	//@JAPR 2019-05-30: Corregido un bug, no se puede agregar un header si ya se había mandado una respuesta, ya que a partir de cierto evento comenzó a generar un 
	//Internal Server Error (HTTP 500) si se hacía de esa manera, así que se validará que los headers sean excluyentes al parámetro DebugBreak, además de atrapar toda salida
	//al buffer no atrapada para regresarla como una propiedad warningNotice de la respuesta en caso de haber algún texto (#7UZT8C)
	$strWarningsNotice = '';
	if ( !isset($_REQUEST["DebugBreak"]) ) {
		//Adicional a activar el header en esta situación, agregará la lectura y desactivación del buffer por cualquier error no controlado que hubiera sido regresado
		$strWarningsNotice = trim(ob_get_contents());
		/*if ( $strWarningsNotice ) {
			$return['warningNotice'] = $strWarningsNotice;
		}*/
		ob_end_clean();
		
		if ($sHeader != '')
		{
			@header($sHeader);
		}
	}
	//@JAPR
	
    echo $iErrCode.SV_SRV_ERROR.$sErrDesc.(($blnDebug)?"\r\n\r\n":SV_DATA_FIELD).ESURVEY_SERVICE_VERSION.(($blnDebug)?";":SV_ITEM).$appVersion;
    die();
}

//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
//Realiza la prueba de grabado de una acción del eBavel para verificar compatibilidad de conexión / configuraciones
function testSaveAction($aRepository)
{
	global $kpiUser;
	
    require_once('survey.inc.php');
	require_once('settingsvariable.inc.php');
	
    $surveyID = 0;
    if (array_key_exists("surveyID", $_GET)) {
        $surveyID = $_GET["surveyID"];
    }
	
	if ($surveyID <= 0)
	{
		generateErrorMessage(-410, "You must specify a survey for this process");
	}
	
	$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
	if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
	{
		generateErrorMessage(-409, "eBavel Web Service path is not defined");
	}
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if (is_null($surveyInstance))
	{
		generateErrorMessage(-412, "The specified survey does not exists");
	}
	
	$eBavelAppCodeName = '';
	$sql = "SELECT codeName FROM SI_FORMS_APPS WHERE id_app = ".$surveyInstance->eBavelAppID;
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS === false || $aRS->EOF)
    {
		generateErrorMessage(-411, "The eBavel application specified for this survey does not exists");
    }
	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
	//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
    $eBavelAppCodeName = (string) $aRS->fields["codename"];
	
	$arrURL = parse_url($objSetting->SettingValue);
	$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
	$strPath = str_replace('/', '\\', $strPath);
	$strPath = str_replace('\\\\', '\\', $strPath);
	require_once($strPath);
	
	global $path_eBavel;
	
	$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
    $manager = new Tables();
    $manager->applicationCodeName = $eBavelAppCodeName;
    $manager->init($aRepository->ADOConnection);
	$manager->createTables($aRepository->ADOConnection);
	
	$insertData = array();
	$insertData[-1]["id_actionitem"] = -1;		// Campo del formulario ([id_FRM###] = -1)
	$insertData[-1]["createdDate"] = date('Y-m-d H:i:s');
	$insertData[-1]["modifiedDate"] = date('Y-m-d H:i:s');
	$insertData[-1]["section_id"] = $surveyInstance->eBavelFormID;
	$insertData[-1]["createdUser"] = $_SESSION["PABITAM_UserID"];
	$insertData[-1]["user"] = $_SESSION["PABITAM_UserID"];
	$insertData[-1]["description"] = "ActionItem de prueba desde el eForms";		// Campo del formulario ([FFRMS_####] = 'Valor') -> Si es de catálogo, hay que poner el subrrogado del valor
	//$insertData[-1]["createdUserKey"] = cla_usuario
	//$insertData[-1]["modifiedUserKey"] = cla_usuario
	//Si el usuario no tiene permisos de agregar, no marcará error pero tampoco lo insertará
	$insertData[-1]["email"] = 0;
	$insertData[-1]["row_key"] = 0;
	$insertData[-1]["process_id"] = 0;
	
	//0 = 'In Progress (Open)';
   	//1 = 'Stand By';
   	//2 = 'Finished';
	$insertData[-1]["status"] = 0;
	$insertData[-1]["source"] = "eForms";
	$insertData[-1]["sourceID"] = $surveyInstance->SurveyID;
	$insertData[-1]["sourceRow_key"] = 1;
	$insertData[-1]["category"] = "Categoría de prueba";
	$insertData[-1]["element"] = "Elemento de prueba";
	$insertData[-1]["deadline"] = date('Y-m-d H:i:s');
   	$manager->insertData('ACTIONITEMS', $insertData, array(), $aRepository->ADOConnection);
   	//Cambia el formulario $manager->insertData('FRM_####', $insertData, array(), $aRepository->ADOConnection);
   	
   	$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
	
	die('$path_eBavel'.$path_eBavel);
}

//@JAPR 2012-04-17: Agregado el método de prueba de grabado de datos basado en las respuestas de un archivo generado con el parámetro saveDebug
//Realiza una invocación al grabado pero basado en los parámetros contenidos en un archivo específico que debe estar en el servidor en la ruta
//donde se graban los logs, el cual se genera previamente con el parámetro saveDebug dentro del método saveAnswersMobile2
function testSaveMethod($aRepository)
{
	global $strResponseFormat;
  	//debugSave
	
	$blnDebug = getParamValue('debug', 'both', "(boolean)");
	$strDataFileName = getParamValue('datafilename', 'both', "(string)");
	if ($strDataFileName == '')
	{
    	generateErrorMessage(-407, "The data file name was not specified");
	}
	
	$strParameters = @file_get_contents("./uploaderror/".$strDataFileName);
	if ($strParameters == false)
	{
    	generateErrorMessage(-408, "The data file name specified was not found: ./uploaderror/".$strDataFileName);
	}
    
	$GET = '';
	$POST = '';
	if (trim($strParameters) != '')
	{
		@eval($strParameters);
	}
	
    header("Content-Type: text/plain");
	//Finalmente invoca al método de grabado de respuestas
	echo("GET: \r\n");
	var_dump($GET);
	echo("\r\nPOST: \r\n");
	var_dump($POST);
	foreach($GET as $aKey => $aValue)
	{
		$_GET[$aKey] = $aValue;
	}
	foreach($POST as $aKey => $aValue)
	{
		$_POST[$aKey] = $aValue;
	}
	saveAnswersMobile2();
	die();
}

//@JAPR 2012-04-17: Convertida en función para poder ser invocada desde este mismo archivo PhP
function saveAnswersMobile2()
{
    global $server;
    global $server_user;
    global $server_pwd;
    global $masterdbname;
    global $BITAMRepositories;
    global $generateLog;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
	global $dieOnError;
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
	global $eBavelAppCodeName;
	global $arrayPDF;
	global $arrActionItems;
	global $arrNumericQAnswersColl;
	//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
	global $strPDFFileNamePrefix;
	global $intPDFFilaNamePrefixCntr;
	//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	global $jsonData;
	//@JAPR 2013-05-03: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	global $usePhotoPaths;
	global $blnWebMode;
	global $useRecoveryPaths;
	//@JAPR 2013-07-17: Agregado el archivo log de procesamiento de acciones
	global $blnSaveeBavelLogStringFile;
	//@JAPR
	//@MABH20121205
	global $appVersion;
	//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
	global $gbEnableIncrementalAggregations;
	global $gbTestIncrementalAggregationsData;
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	global $gbUseOperationsMonitor;
	global $giOperationID;
	//OMMC 2019-04-12: Modificada la instrucción del PhantomJS para que vuelva a resolver mapas
	global $gbKPIFormsService;
	
	//Este es el punto inicial de grabado de las capturas, así que aquí asigna el valor de la fecha/hora de sincronización
	$syncDate = date("Y-m-d");
	$syncTime = date("H:i:s");
	
	$blnDebugSave = getParamValue('debugSave', 'both', "(boolean)");
	if ($blnDebugSave)
	{
		ob_start();
		//echo("Post Data: <br>");
		echo('$POST = ');
		var_export($_POST);
		echo(";\r\n");
		//echo("Get Data: <br>");
		echo('$GET =');
		var_export($_GET);
		echo(";\r\n");
	    $strData = trim(ob_get_contents());
		ob_end_clean();
  		$statlogfile = "./uploaderror/ESurvey_SyncData_".date('YmdHis').".log";
	  	@error_log($strData, 3, $statlogfile);
	    header("Content-Type: text/plain");
	    echo("Data file was stored in the server");
	    exit();
	}
	elseif (false)
	{
		//Activar sólo si se quiere dejar registro de los datos subidos por los encuestadores, se puede incluso condicionar a sólo ciertas cuentas
		//Es similar al parámetro debugSave con la excepción de que no manda la salida de datos al buffer sino que lo maneja todo como variables
		//para simplemente grabarlas en un archivo y atrapa todos los errores para no interrumpir el proceso de grabado
		try
		{
			$kpiUser = '';
		    if (isset($_POST['UserID']))
		    {
			    $kpiUser = $_POST['UserID'];
		    }

			$thisDate = date("Y-m-d H:i:s");
			if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
			{
				$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
			}
			$thisDate = str_replace(':', '', $thisDate);
			$thisDate = str_replace('-', '', $thisDate);
			$thisDate = str_replace(' ', '', $thisDate);

			$agendaID = 0;
			if (array_key_exists("agendaID", $_POST))
			{
				$agendaID = (int) $_POST["agendaID"];
			}
			
		    $surveyID = 0;
		    if (array_key_exists("surveyID", $_POST)) {
		        $surveyID = (int) $_POST["surveyID"];
		    }
		    
		    if (trim($kpiUser) != '' && $thisDate != '' && $surveyID > 0)
		    {
		    	$strData = "";
				$strData .= '$POST = '.var_export($_POST, true).";\r\n";
				$strData .= '$GET ='.var_export($_GET, true).";\r\n";
			    
		  		$statlogfile = "./uploaderror/SyncData_".$kpiUser."_Ag".$agendaID."_Sv".$surveyID."_Dte".$thisDate.".log";
			  	@error_log($strData, 3, $statlogfile);
		    }
		}
		catch (Exception $e)
		{
			//No es necesario reportar este error
		}
	}
	$statlogfile = "./uploaderror/eforms.log";
	$friendlyDate = date('Y-m-d H:i:s');
	
	$thisDate = date("Y-m-d H:i:s");

	if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
	{
		$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
	}

	if(isset($_POST["surveyDate"]) && isset($_POST["surveyHour"]))
	{
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
	} else {
		$lastDate = $thisDate;
	}
	
	$stat_action = "[".$friendlyDate."] [Action: Connect Saving Answers Service First Step] [UserID - Email: ".@$_POST['UserID']."] [SurveyID: ".@$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
	@error_log($stat_action, 3, $statlogfile);

    if (!isset($_POST['UserID'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiUser = $_POST['UserID'];

    if (!isset($_POST['Password'])) {
        header('HTTP/1.0 401 Unauthorized');
        echo 'You must enter a valid UserID and Password to access this resource';
        exit();
    }
    $kpiPass = $_POST['Password'];

    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
        header('HTTP/1.0 401 Unauthorized');
        echo $strPrjAndUser;
        exit();
    }

    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
    $strRepositoryName = $arrayPrjUsr[0];
    $strUserName = $arrayPrjUsr[1];

    $theRepositoryName = $strRepositoryName;
    $theUserName = $strUserName;
    $thePassword = $kpiPass;

    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Project does not exists.";
        exit();
    }

    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
        header('HTTP/1.0 401 Unauthorized');
        echo translate("Can not connect to Repository, please contact your System Administrator.");
        exit();
    }
	
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
    if (is_null($theUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name does not exist or your Password is incorrect.";
        exit();
    }

    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);

    if (is_null($theAppUser)) {
        header('HTTP/1.0 401 Unauthorized');
        echo "Your User Name is not registered in the application.";
        exit();
    }

	//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
	//Asigna el prefijo que se utilizará como nombre de archivo de PDF
	//@JAPR 2013-02-26: Corregido un bugm, se movió este prefijo a esta parte porque estaba concatenando la cuenta de correo en lugar del CLA_USUARIO
	$strPDFFileNamePrefix = ($theAppUser->UserID).'_'.((string) @$_POST["surveyID"]).'_'.$lastDate;
	if ($lastDate != $thisDate) {
		$strPDFFileNamePrefix .= '_'.$thisDate;
	}
	$strPDFFileNamePrefix = str_replace(array('-', ':', ' '), '', $strPDFFileNamePrefix);
	//@JAPR
    
    //Establecer el lenguaje en una variable de sesión
    //0=DEFAULT, 1=Spanish, 2=English
    if ($theUser->UserLanguageID <= 0 || $theUser->UserLanguageID > 8) {
        $userLanguageID = 2;
        $userLanguage = 'EN';
    } else {
        $userLanguageID = $theUser->UserLanguageID;
        $userLanguage = $theUser->UserLanguageTerminator;
    }

    //Verificar si se encuentra en modo FBM
    $sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";

    $aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);

    $valueFBMMode = 0;

    if ($aRSFBM && !$aRSFBM->EOF) {
        if ($aRSFBM->fields["ref_configura"] !== null && trim($aRSFBM->fields["ref_configura"]) !== "") {
            $valueFBMMode = (int) trim($aRSFBM->fields["ref_configura"]);
        }
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
    /*session_register("PABITAM_RepositoryName");
    session_register("PABITAM_UserName");
    session_register("PABITAM_UserRole");
    session_register("PABITAM_Password");
    session_register("PABITAM_UserID");
    session_register("PAtheUserID");
    session_register("PAtheAppUserID");
    session_register("PAgenerateLog");
    session_register("SV_ADMIN");
    session_register("SV_USER");
    session_register("PAuserLanguageID");
    session_register("PAuserLanguage");
    session_register("PAchangeLanguage");
    session_register("PAArtusWebVersion");
    session_register("PAFBM_Mode");*/

    $_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
    $_SESSION["PABITAM_Password"] = $theUser->Password;
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
    $_SESSION["PAtheUserID"] = $theUser->UserID;
    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
    $_SESSION["PAgenerateLog"] = $generateLog;
    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
    $_SESSION["SV_USER"] = $theAppUser->SV_User;
    $_SESSION["PAuserLanguageID"] = $userLanguageID;
    $_SESSION["PAuserLanguage"] = $userLanguage;
    $_SESSION["PAchangeLanguage"] = false;
    $_SESSION["PAArtusWebVersion"] = 6.0;
    $_SESSION["PAFBM_Mode"] = $valueFBMMode;

    require_once("processsurveyfunctions.php");
	require_once("surveyActionMap.inc.php");
	require_once("questionActionMap.inc.php");
	require_once("questionOptionActionMap.inc.php");

    $surveyID = 0;
    if (array_key_exists("surveyID", $_POST)) {
        $surveyID = $_POST["surveyID"];
    }

	//@JAPR 2012-03-14: Agregado el grabado del Status de las Agendas
	$agendaID = 0;
	if (array_key_exists("agendaID", $_POST))
	{
		$agendaID = (int) $_POST["agendaID"];
	}
	//@JAPR
    
	//@MABH20121205
	if($appVersion < esvServerDateTime) {
		if(!isset($_POST["serverSurveyDate"])) {
			$_POST["serverSurveyDate"] = "";
		}
		if(!isset($_POST["serverSurveyHour"])) {
			$_POST["serverSurveyHour"] = "";
		}
		if(!isset($_POST["serverSurveyEndDate"])) {
			$_POST["serverSurveyDate"] = "";
		}
		if(!isset($_POST["serverSurveyEndHour"])) {
			$_POST["serverSurveyEndHour"] = "";
		}
	}
	
    $arrayData = $_POST;

    foreach ($arrayData as $key => $value) {///////AQUI
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
            	//@JAPR 2013-04-10: Agregado un nivel mas de conversión de arrays, si se requiere otro ya mejor cambiar a llamadas recursivas
            	if (is_array($value1)) {
            		foreach ($value1 as $key2 => $value2) {
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            			$arrayData[$key][$key1][$key2] = $arrayData[$key][$key1][$key2];
            		}
            	}
            	else {
					//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
                	$arrayData[$key][$key1] = $arrayData[$key][$key1];
            	}
            	//@JAPR
            }
        } else {
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            $arrayData[$key] = $arrayData[$key];
        }
    }/////AQUI
    
    //Agregamos el usuario de artus y de la aplicacion logueado en el arreglo $arrayData
    $arrayData["userID"] = $_SESSION["PABITAM_UserID"];
    $arrayData["appUserID"] = $_SESSION["PAtheAppUserID"];
	
    //Modificamos variable global $gblShowErrorSurvey para que no realice los die y que devuelva los errores
    global $gblShowErrorSurvey;
    $gblShowErrorSurvey = false;

    require_once('survey.inc.php');
    $surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);

    if (is_null($surveyInstance)) {
        $strStatus = "Survey data was saved successfully.";
        header("Content-Type: text/plain");
        echo($strStatus);
        exit();
    }
	
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//Verifica si existen los campos para almacenar el originen del registro de la tabla de datos
	$useSourceFields = false;
	$sql = "SELECT EntrySectionID, EntryCatDimID FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useSourceFields = true;
	}
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	$useRecVersionField = false;
	$sql = "SELECT eFormsVersionNum FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useRecVersionField = true;
	}
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	$useDynamicPageField = false;
	$sql = "SELECT DynamicPageDSC FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useDynamicPageField = true;
	}
	//@JAPR
    
	//@JAPR 2012-05-23: Agregado el log de proceso de los móviles a una tabla
	//Se movió $logdate para poder usarla durante la generación del log. Sólo se registra un error de log por sesión
	$logdate = date('YmdHis');
	$blnFirstLogError = true;
	$strLogStatus = '';
	//@JAPR
    
	//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
	//varios requests al mismo tiempo
	$blnIsAgent = getParamValue('isagent', 'both', "(int)", false);
	if ($blnIsAgent || ($handle = @fopen("./uploaderror/forms_" . $theRepository->RepositoryName . "_" . $theUser->UserID . ".lck", "w"))) {
        if ($blnIsAgent || @flock($handle, LOCK_EX)) {
			//$sql = "SELECT DateID FROM SVSurvey_".$surveyInstance->ModelID." WHERE DateID = '".$lastDate."' AND UserID = '".$arrayData["userID"]."'";	
			$sql = "SELECT DateID FROM SVSurvey_".$surveyInstance->ModelID." WHERE DateID = ".$theRepository->DataADOConnection->DBTimeStamp($lastDate)." AND UserID = ".$arrayData["userID"];
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			
			if ($aRS->EOF)
			{
				$repeatedDate = false;
			} else {
				$repeatedDate = true;
			}
			
			if(!$repeatedDate) {
				//@JAPR 2012-05-23: Agregado el log de proceso de los móviles a una tabla
				if (isset($arrayData['logMobile']))
				{
					//@JAPR 2012-06-14: No estaba haciendo el explode por <enter>
					$arrEntries = explode("\r\n", $arrayData['logMobile']);
					$arrLogLine = array();
					//Recorre cada línea del log y al encontrar una cuyo contenido es el texto "***" inserta los campos en la tabla del Log
					foreach ($arrEntries as $strLine)
					{
						if ($strLine == '***')
						{
							try
							{
								$arrLogLine[] = 'survey='.$surveyID;
								$arrLogLine[] = 'agenda='.$agendaID;
								$strLogStatus = insertAppLogLine($theRepository, $arrLogLine);
							}
							catch (Exception $e)
							{
								$strLogStatus = $e->getMessage();
							}
							
							//En caso de error durante el grabado del log en la tabla, registra la entrada en el propio log de grabado en archivo
							if ($strLogStatus != '' && $blnFirstLogError)
							{
								$blnFirstLogError = false;
								$thelog = "[".$friendlyDate."] [Action: SaveLog error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving mobile app log entry: " . $strLogStatus . " POST = " . print_r($_POST,true);
								$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
								@error_log($thelog, 3, $thefile);
								
								$stat_action = "[".$friendlyDate."] [Action: SaveLog error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
								@error_log($stat_action, 3, $statlogfile);
							}
							$arrLogLine = array();
						}
						else 
						{
							$arrLogLine[] = $strLine;
						}
					}
				}
				//@JAPR
				
		        $dynamicSectionID = BITAMSection::existDynamicSection($theRepository, $surveyID);
		        $hasDynamicSection = false;
		        $hasCategoryDimensions = false;
				
		        if ($dynamicSectionID > 0) {
		            //Aqui verificamos con el sectionValue que realmente si se crearon arreglos
		            //de preguntas en las encuestas para poder capturar respuestas para mas de una
		            //encuesta
		            $postKey = "sectionValue";
		            if (isset($arrayData[$postKey])) {
		                $hasDynamicSection = true;
		            }
		        }
		        
		        $hasCategoryDimensions = hasCategoryDimensions($theRepository, $surveyID);
		        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
		        $hasMasterDetSections = false;
		        $arrMasterDetSections = BITAMSection::existMasterDetSections($theRepository, $surveyID, true);
		        if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
		        {
					//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        	$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
		        	
		        	//Verifica si realmente se capturaron preguntas de la sección de Maestro detalle, ya que si no es así entonces simplemente
		        	//se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
		        	//por lo que no habría problema en procesarlas
		        	for($i = 0; $i < count($arrMasterDetSections); $i++)
		        	{
		        		$intSectionID = $arrMasterDetSections[$i];
		        		$postKey = "masterSectionRecs".$intSectionID;
		        		if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0))
		        		{
		        			//Con una sola sección que si capturara un Maestro - Detalle, se asume que si vienen preguntas como array por lo que
		        			//se usará el método alterno de grabado
		        			$hasMasterDetSections = true;
		        			//break;
		        		}
		        		else 
		        		{
		        			//Si esta sección siendo un Maestro - Detalle NO capturó ningún conjunto de respuestas, en el mejor de los casos
		        			//llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
		        			//pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
		        			//(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
		        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				            foreach ($questionCollection->Collection as $questionElement) {
				            	$postKey = "qfield".$questionElement->QuestionID;
				            	if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]))
				            	{
				            		//En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
				            		//"masterSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
				            		//no se conocía el concepto de Maestro - Detalle, en cuyo caso no debió haber llegado entonces un Array sino
				            		//las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
				            		//con el is_array también
				            		$arrayData[$postKey] = '';
				            	}
				            }
		        		}
		        	}
		        }
		        else {
		        	$arrMasterDetSections = array();
		        	$arrMasterDetSectionsIDs = array();
		        }
		        
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
		        $hasInlineSections = false;
		        $arrInlineSections = BITAMSection::existInlineSections($theRepository, $surveyID, true);
		        if (is_array($arrInlineSections) && count($arrInlineSections) > 0)
		        {
					//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        	$arrInlineSectionsIDs = array_flip($arrInlineSections);
		        	
		        	//Verifica si realmente se capturaron preguntas de la sección Inline, ya que si no es así entonces simplemente
		        	//se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
		        	//por lo que no habría problema en procesarlas
		        	for($i = 0; $i < count($arrInlineSections); $i++)
		        	{
		        		$intSectionID = $arrInlineSections[$i];
		        		$postKey = "inlineSectionRecs".$intSectionID;
		        		if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0))
		        		{
		        			//Con una sola sección que si capturara una Inline, se asume que si vienen preguntas como array por lo que
		        			//se usará el método alterno de grabado
		        			$hasInlineSections = true;
		        			//break;
		        		}
		        		else 
		        		{
		        			//Si esta sección siendo una Inline NO capturó ningún conjunto de respuestas, en el mejor de los casos
		        			//llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
		        			//pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
		        			//(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
		        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				            foreach ($questionCollection->Collection as $questionElement) {
				            	$postKey = "qfield".$questionElement->QuestionID;
				            	if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]))
				            	{
				            		//En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
				            		//"inlineSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
				            		//no se conocía el concepto de Inline, en cuyo caso no debió haber llegado entonces un Array sino
				            		//las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
				            		//con el is_array también
				            		$arrayData[$postKey] = '';
				            	}
				            }
		        		}
		        	}
		        }
		        else {
		        	$arrInlineSections = array();
		        	$arrInlineSectionsIDs = array();
		        }
		        //@JAPR
		        
				//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
				$blnSaveSingleStandardData = false;
				if ($surveyInstance->UseStdSectionSingleRec) {
					$blnSaveSingleStandardData = true;
				}
		        
				//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
				//Esta variable contendrá el Key asignado a esta captura de encuesta en los casos donde se deban invocar varios de los métodos
				//de grabado encadenados, por ejemplo encuestas que tienen Secciones Maestro - Detalle y Dinámica, ya que al invocar primero al
				//grabado de la Maestro - Detalle se debe respetar el $factKeyDimVal asignado ahí cuando se invoque al grabado de la Dinámica
		        $factKeyDimVal = 0;
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//Para permitir que se expanda mejor y se pudieran agregar nuevos tipos de secciones en el futuro, ya no se harán series de If y
				//elseif, sino que se enviará a grabar todo nuevo tipo de sección al inicio sobreescribiendo el array de datos que finalmente llegará
				//al If original para validar las Maestro-detalle y el resto de las secciones, lo cual es un código perfectamente probado, por lo
				//tanto se podrá desactivar perfectamente cualquier tipo de sección que no se desee grabar simplemente comentando su If correspondiente
				//Este array respaldará los datos originales, mas abajo se restaurará como corresponda al terminar el grabado de registros y antes
				//del postgrabado, donde se usa para generar acciones de eBavel
				$arrayDataOrig = $arrayData;
				
				//Si hay secciones Inline, se procesarán antes de cualquier otra y al terminar se removerán sus valores sobre el array de datos
				$blnMultiSectionStored = false;
				$strStatus = "";
				if ($hasInlineSections) {
		        	$blnMultiSectionStored = true;
		        	
					//A diferencia de los otros métodos múltiples, para este y los subsecuentes, sólo se invocará por sección en lugar de invocar
					//enviando todo el array de las secciones del mismo tipo, cada sección procesada se encargará de validar el correcto grabado
					//de las preguntas que corresponden a ella misma sin alterar valores de otras secciones múltiples. Como referencia, las secciones
					//dinámicas siguen este principio aunque por diseño sólo puede haber una, mientras que las secciones maestro-detalle si se
					//calculaba su grabado recibiendo el total de secciones y haciendo ajustes internos a un subarray sólo con los valores de ella
					//misma, pero hubiera sido mas fácil separarlo en diferentes llamadas por sección como se hará a partir de ahora
					foreach ($arrInlineSections as $intSectionID) {
		        		$strStatus = multipleSaveInlineData($theRepository, $surveyID, $arrayData, $thisDate, $intSectionID, $factKeyDimVal);
		        		if ($strStatus != "" && $strStatus != "OK") {
		        			break;
		        		}
					}
		        	
					//Independientemente de si hay o no mas tipos de secciones, elimina las respuestas de las pregunta de las secciones Inline
					//ya que todas esas fueron procesadas en el método anterior
					if ($strStatus == "" || $strStatus == "OK") {
			        	for($i = 0; $i < count($arrInlineSections); $i++)
			        	{
			        		$intSectionID = $arrInlineSections[$i];
		        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				            foreach ($questionCollection->Collection as $questionElement)
				            {
				            	//Elimina todas las respuestas de preguntas de Inline para no causar conflicto al grabar las demás secciones
								$postKey = "qfield".$questionElement->QuestionID;
				            	if (isset($arrayData[$postKey]))
				            	{
				            		unset($arrayData[$postKey]);
				            	}
				            	$otherPostKey = "qimageencode".$questionElement->QuestionID;
				            	if (isset($arrayData[$otherPostKey]))
				            	{
				            		unset($arrayData[$otherPostKey]);
				            	}
				            	$otherPostKey = "qcomment".$questionElement->QuestionID;
				            	if (isset($arrayData[$otherPostKey]))
				            	{
				            		unset($arrayData[$otherPostKey]);
				            	}
				            	$otherPostKey = "qactions".$questionElement->QuestionID;
				            	if (isset($arrayData[$otherPostKey]))
				            	{
				            		unset($arrayData[$otherPostKey]);
				            	}
				            }
			        	}
					}
					
		        	//No tiene caso invocar al resto de los tipos de grabado, al continuar se entrará a cada uno si es necesario, sin embargo si
		        	//activa la bandera $blnMultiSectionStored para evitar que se entre a un saveData por default si no existen además tipos de secciones múltiples
		        	//de las originales (Maestro-detalle, dinámica o categoría de dimensión), para así evitar doble grabado de registro estándar
				}
		        
				//Solo debe continuar si no hubo errores
				if ($strStatus == "" || $strStatus == "OK") {
			        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
			        //Si hay por lo menos una sección maestro detalle, por ahora no se permiten secciones dinámicas ni preguntas de categoría de
			        //dimensiones, ya que el procesamiento de este tipo de secciones es muy parecido al de las secciones dinámicas pero son métodos
			        //diferentes por lo que no se pueden combinar aun (eventualmente se podrían combinar con relativa facilidad secciones dinámicas
			        //y Maestros detalles reenviando a multipleSaveDynamicData dentro de multipleSaveMasterDetData, pero por ahora no se soporta.
			        //El grabado de preguntas de categorías de dimensión se complica aun en el uso de estos tipos de secciones especiales
			        //Si no hay seccion dinamica se sigue procesando los datos del POST como siempre se han hecho
			        if ($hasMasterDetSections)
			        {
						//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
			        	$strStatus = multipleSaveMasterDetData($theRepository, $surveyID, $arrayData, $thisDate, $arrMasterDetSections, $factKeyDimVal);
			        	
						//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
						//Si además de la sección Maestro - Detalle existe una sección dinámica, se invoca al grabado de la sección dinámica con el
						//mismo método que ya se tenía pero primero elimina las respuestas de las secciones Maestro - Detalle del Array recibido
						//ya que todas esas fueron procesadas en el método anterior
						if ($hasDynamicSection && ($strStatus == "" || $strStatus == "OK"))
						{
							$arrayDataWOMasterDet = $arrayData;
				        	for($i = 0; $i < count($arrMasterDetSections); $i++)
				        	{
				        		$intSectionID = $arrMasterDetSections[$i];
			        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
					            foreach ($questionCollection->Collection as $questionElement)
					            {
					            	//Elimina todas las respuestas de preguntas de Maestro - Detalle para no causar conflicto al grabar la dinámica
									$postKey = "qfield".$questionElement->QuestionID;
					            	if (isset($arrayDataWOMasterDet[$postKey]))
					            	{
					            		unset($arrayDataWOMasterDet[$postKey]);
					            	}
					            	$otherPostKey = "qimageencode".$questionElement->QuestionID;
					            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
					            	{
					            		unset($arrayDataWOMasterDet[$otherPostKey]);
					            	}
					            	$otherPostKey = "qcomment".$questionElement->QuestionID;
					            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
					            	{
					            		unset($arrayDataWOMasterDet[$otherPostKey]);
					            	}
					            	$otherPostKey = "qactions".$questionElement->QuestionID;
					            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
					            	{
					            		unset($arrayDataWOMasterDet[$otherPostKey]);
					            	}
					            }
				        	}
							
				        	//Se invoca al llenado de la sección dinámica, como ya se limpiaron todas las preguntas tipo Maestro - Detalle y el método
				        	//que graba las dinámicas no sabe diferenciar entre una sección estática o una Maestro - Detalle, simplemente procesará
				        	//dichas preguntas como si fueran no dinámicas, asignandole el valor de "NA" según su tipo de pregunta
				        	//@JAPR 2012-06-07: Corregido un bug, no se estaba enviando el $factKeyDimVal así que en la parte dinámica generaba un nuevo
				        	//registro de respuestas
							$strStatus = multipleSaveDynamicData($theRepository, $surveyID, $arrayDataWOMasterDet, $thisDate, $dynamicSectionID, $factKeyDimVal);
						}
			        	//@JAPR
			        }
			        else if($hasCategoryDimensions)
			        {
			        	//Realizamos modificaciones al arreglo $arrayData para agregar las nuevas claves
			        	$arrayData = getArrayDataWithCategoryDimension($theRepository, $surveyID, $arrayData);
						//@JAPR 2012-09-05: Agregado el cubo global de Scores (parámetro $factKeyDimVal)
			        	$strStatus = multipleSaveCategoryDimData($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
			        }
			        else if ($hasDynamicSection) 
			        {
						//@JAPR 2012-09-05: Agregado el cubo global de Scores (parámetro $factKeyDimVal)
			            $strStatus = multipleSaveDynamicData($theRepository, $surveyID, $arrayData, $thisDate, $dynamicSectionID, $factKeyDimVal);
			        } 
					//@JAPR 2014-05-29: Agregado el tipo de sección Inline
			        elseif (!$blnMultiSectionStored)
			        {
						//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
			        	//Es un registro de secciones estándar, así que no se graba información del SectionID ni de Categoría de dimensión
						//@JAPR 2012-09-05: Agregado el cubo global de Scores (parámetro $factKeyDimVal)
			            $strStatus = saveData($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
						//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
						//Si entró por aquí quiere decir que ya grabó un único registro, así que no es necesario grabar otro mas adelante
			            $blnSaveSingleStandardData = false;
			            //@JAPR
			        }
				}
		        
				//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
				//Graba el registro con las respuestas de secciones estándar (sólo si se procesaron otro tipo de secciones previamente)
				if ($blnSaveSingleStandardData && ($strStatus == "" || $strStatus == "OK")) {
					$strStatus = singleSaveStandardData($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
				}
		        
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//Restaura el array original de datos antes del proceso de postgrabado
				$arrayData = $arrayDataOrig;
				
				//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
				//Si se encuentra en la versión de eForms posterior a la integración con eBavel, entonces debe estar usando el nuevo esquema de
				//grabado de acciones de eBavel, de lo contrario se utilizará el método original de la tabla ACTIONITEMS
				$blnUseFormForActions = false;
				if (getMDVersion() >= esveBavelSuppActions) {
					$blnUseFormForActions = true;
				}
				
				//Por default (al menos con el método original de acciones) bastaba con tener una capturada para continuar con el proceso
				$blnCanSaveeBavelActions = (isset($arrActionItems) && is_array($arrActionItems) && count($arrActionItems) > 0);
				
				//Valida que se tengan las configuraciones necesarias para poder interactuar con el eBavel
				if ($blnCanSaveeBavelActions) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\nStarting eBavel action saving process: {$blnCanSaveeBavelActions}, Use Actions form: {$blnUseFormForActions}");
						ECHOString("<br>\r\nNumber of actions to process: ".count($arrActionItems));
					}
					@logeBavelString("Starting eBavel action saving process: {$blnCanSaveeBavelActions}, Use Actions form: {$blnUseFormForActions}");
					@logeBavelString("User: ".@$theUser->UserID.", Survey: ".@$surveyID.", Entry date: ".@$lastDate);
					@logeBavelString("Number of actions to process: ".count($arrActionItems));
					
					require_once('settingsvariable.inc.php');
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELURL');
					if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo eBavel path configured: ".$blnCanSaveeBavelActions);
						}
						@logeBavelString("No eBavel path configured: ".$blnCanSaveeBavelActions);
						$blnCanSaveeBavelActions = false;
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\nSkipping eBavel action saving process: ".$blnCanSaveeBavelActions);
						if (isset($arrActionItems) && is_array($arrActionItems)) {
							PrintMultiArray($arrActionItems);
						}
						else {
							ECHOString("<br>\r\nNo actions registered");
						}
					}
					@logeBavelString("Skipping eBavel action saving process: ".$blnCanSaveeBavelActions);
					if (isset($arrActionItems) && is_array($arrActionItems)) {
						
					}
					else {
						@logeBavelString("No actions registered");
					}
				}
				
				if ($blnCanSaveeBavelActions) {
					$eBavelAppCodeName = '';
					$sql = "SELECT codeName FROM SI_FORMS_APPS WHERE id_app = ".$surveyInstance->eBavelAppID;
				    $aRS = $theRepository->ADOConnection->Execute($sql);
				    if ($aRS === false || $aRS->EOF)
				    {
						$blnCanSaveeBavelActions = false;
				    }
				    else 
				    {
				    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				    	$eBavelAppCodeName = (string) $aRS->fields["codename"];
				    	//La propiedad eBavelFormID sólo se usaba para acciones antes de que se creara la forma exclusiva para acciones, así que
				    	//a falta de la misma no hay por qué impedir que se graben las acciones, asumiendo por supuesto que se trata del método
				    	//original. Si se tratara del nuevo método entonces por el contrario se tiene que verificar pero contra el campo de la
				    	//forma especial de acciones
				    	//if (trim($eBavelAppCodeName) == '' || ($surveyInstance->eBavelFormID <= 0 && !$blnUseFormForActions) ||
				    	//		($surveyInstance->eBavelActionFormID <= 0 && $blnUseFormForActions)) {
				    	if (trim($eBavelAppCodeName) == '' || ($surveyInstance->eBavelFormID <= 0 && !$blnUseFormForActions)) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo eBavel app found: ".$surveyInstance->eBavelAppID);
							}
							@logeBavelString("No eBavel app found: ".$surveyInstance->eBavelAppID);
				    		$blnCanSaveeBavelActions = false;
				    	}
				    }
				}
				
				//Si se utilizará una forma para las acciones, hay que verificar muchas cosas antes de intentar el grabado de las mismas, de tal
				//manera que si no todas se tienen correctamente entonces debe deshabilitar el grabado de acciones por completo
				if ($blnCanSaveeBavelActions) {
					//Independientemente del método de grabado, se verica si el código de eBavel está disponible
				    $strPath = '';
				    $streBavelPath = '';
					$arrURL = parse_url($objSetting->SettingValue);
					$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
					$strPath = str_replace('/', '\\', $strPath);
					$strPath = str_replace('\\\\', '\\', $strPath);
					//@JAPR 2012-07-10: Corregido un bug, si la ruta al eBavel configurada  no existe, a pesar de ignorar el error mediante el uso de @
					//estaba generando un error de compilación interrumpiendo con ello el grabado al primer registro de una captura que debió haber
					//grabado múltiples registros
					if (file_exists($strPath))
					{
						@require_once($strPath);
						$streBavelPath = str_ireplace('service.php', '', $strPath);
					}
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						if (!function_exists('NotifyError')) {
							ECHOString("<br>\r\nNo NotifyError function");
						}
						else {
							ECHOString("<br>\r\nNotifyError function exists");
						}
					}
					if (!function_exists('NotifyError')) {
						@logeBavelString("No NotifyError function");
					}
					else {
						@logeBavelString("NotifyError function exists");
					}
					//@JAPR
					
					if (!class_exists('Tables'))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo eBavel code loaded: ".$blnCanSaveeBavelActions);
						}
						@logeBavelString("No eBavel code loaded: ".$blnCanSaveeBavelActions);
						$blnCanSaveeBavelActions = false;
					}
					
					if ($blnCanSaveeBavelActions && $blnUseFormForActions) {
					    //Verifica si la forma especificada existe
				    	$eBavelFormTable = '';
					    //$arreBavelFieldsInfo = array();
					    $arreBavelFieldsColl = array();
						require_once('eBavelIntegration.inc.php');
				    	$arreBavelFormInfo = null;
				    	//Sólo cargará la forma de acciones default de la encuesta si esta se encuentra configurada, ya que si no se asume que se
				    	//usará la forma configurada por cada pregunta
				    	if ($surveyInstance->eBavelActionFormID > 0) {
					    	$arreBavelFormColl = @GetEBavelSections($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelActionFormID));
					    	if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[$surveyInstance->eBavelActionFormID])) {
					    		$arreBavelFormInfo = $arreBavelFormColl[$surveyInstance->eBavelActionFormID];
					    		$eBavelFormTable = $arreBavelFormInfo['name'];
					    		
					    		$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelActionFormID));
					    		if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl) || count($arreBavelFieldsColl) == 0) {
					    			//No se pudo recuperar la info de los campos o no hay campos de eBavel
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNo default eBavel action fields found: ".$surveyInstance->eBavelActionFormID);
										PrintMultiArray($arreBavelFieldsColl);
									}
									@logeBavelString("No default eBavel action fields found: ".$surveyInstance->eBavelActionFormID);
					    			$blnCanSaveeBavelActions = false;
					    		}
					    	}
					    	else {
					    		//No se pudo recuperar la info de la forma de eBavel
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\nNo default eBavel action form found: ".$surveyInstance->eBavelActionFormID);
									PrintMultiArray($arreBavelFormColl);
								}
								@logeBavelString("No default eBavel action form found: ".$surveyInstance->eBavelActionFormID);
					    		$blnCanSaveeBavelActions = false;
					    	}
				    	}
				    	
				    	$strDefaulteBavelFormTable = $eBavelFormTable;
				    	$arrDefaulteBavelFieldsColl = $arreBavelFieldsColl;
					}
				}
				
				//Array indexado por el Id de pregunta o nombre específico de un campo del que se deben obtener datos para grabar en eBavel
				$arreBavelEmptyMappedData = array();
				//Array con los IDs mapeados de campos de eBavel indexado por el Id de pregunta o nombre específico de campo de eForms
				$arreBavelMappedIDs = array();
				//Array con las preguntas y secciones indexadas por su ID
				$arrQuestionsColl = array();
				$arrSectionsColl = array();
				
				if ($blnCanSaveeBavelActions && $blnUseFormForActions) {
					require_once("surveyActionMap.inc.php");
					require_once("questionActionMap.inc.php");
					require_once("questionOptionActionMap.inc.php");
					
					if ($surveyInstance->eBavelActionFormID > 0) {
						$objSurveyFieldMap = BITAMSurveyActionMap::NewInstanceWithSurvey($theRepository, $surveyID);
						if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo fields mapped: ".$blnCanSaveeBavelActions);
							}
							@logeBavelString("No fields mapped: ".$blnCanSaveeBavelActions);
							//$blnCanSaveeBavelActions = false;
						}
						else {
							foreach($objSurveyFieldMap->ArrayVariables as $intSurveyFieldID => $strVariableName) {
								$intVariableValue = (int) @$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID];
								if ($intVariableValue > 0 && isset($arreBavelFieldsColl[$intVariableValue])) {
									$arreBavelEmptyMappedData[$strVariableName] = null;
									$arreBavelMappedIDs[$strVariableName] = $intVariableValue;
								}
							}
						}
					}
				}
				$arrDefaulteBavelEmptyMappedData = $arreBavelEmptyMappedData;
				$arrDefaulteBavelMappedIDs = $arreBavelMappedIDs;
				
				$arrUserNames = array();
				//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        //Si hay definida alguna acción de eBavel según la captura, procede a crear cada una mediante la instancia de eBavel
				//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		        //if (isset($arrActionItems) && is_array($arrActionItems) && count($arrActionItems) > 0) {
		        //@JAPR 2013-07-10: Corregido un bug, los datos del GPS se deben obtener ahora en este punto si es que son usados por acciones
		        $blnGPSDataLoaded = false;
				$arrGPSData = array();
				//@JAPR
		        if ($blnCanSaveeBavelActions) {
		        	//Si llegó a este punto, quiere decir que o bien se usará el método original para lo cual no requiere de muchas validaciones,
		        	//o bien que se usará el método de grabado en una forma para lo cual ya se verificó que todo estuviera correctamente configurado
		        	if ($blnUseFormForActions) {
						$blnDecodeUTF8 = getParamValue('utf8decode', 'both', "(int)", true);
						//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
						//Ahora se requiere cargar las colecciones de preguntas y secciones para validar ciertas cosas por tipos
						//En este caso se agregan todas las preguntas, no sólo aquellas mapeadas ya que se usará en este punto para procesar
						//en las acciones, no entre los datos mapeados a nivel de encuesta
						$questionCollection = BITAMQuestionCollection::NewInstance($theRepository, $surveyID);
						foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
							$arrQuestionsColl[$aQuestion->QuestionID] = $aQuestion;
						}
						
						$sectionCollection = BITAMSectionCollection::NewInstance($theRepository, $surveyID);
						foreach ($sectionCollection->Collection as $sectionKey => $aSection) {
							$arrSectionsColl[$aSection->SectionID] = $aSection;
						}
						//@JAPR
						
						$strDefaultActionStatus = '';
						$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELDEFAULTACTIONSTATUS');
						if (!(is_null($objSetting) || trim($objSetting->SettingValue == '')))
						{
							$strDefaultActionStatus = $objSetting->SettingValue;
						}
						if (trim($strDefaultActionStatus) == '') {
							$strDefaultActionStatus = "Request";
						}
						
						$strDefaultActionSource = '';
						$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELDEFAULTACTIONSOURCE');
						if (!(is_null($objSetting) || trim($objSetting->SettingValue == '')))
						{
							$strDefaultActionSource = $objSetting->SettingValue;
						}
						if (trim($strDefaultActionSource) == '') {
							$strDefaultActionSource = "eForms";
						}
						
				        //@JAPR 2013-07-10: Corregido un bug, los datos del GPS se deben obtener ahora en este punto si es que son usados por acciones
						$blnGetGPSData = false;
			        	foreach ($arrActionItems as $intActionIdx => $aSourceActionData) {
		        			$intQuestionID = (int) @$aSourceActionData["QuestionID"];
		        			if ($intQuestionID <= 0) {
		        				continue;
		        			}
							//@JAPR 2013-07-15: Agregado el mapeo de una forma de eBavel para acciones
							//Ya no es necesario que esté asignado el QuestionAnswer, ya que dependerá del tipo de pregunta y pudiera ser de una
							//pregunta tipo acción simplemente, si se quiere validar correctamente se tendría que agregar al IF el tipo de pregunta,
							//pero se supone que desde que generó el array de acciones ya estaba todo perfectamente validado
		        			$strOptionText = (string) @$aSourceActionData["QuestionAnswer"];
		        			//if (trim($strOptionText) == '') {
		        			//	continue;
		        			//}
		        			//@JAPR
		        			$inteBavelActionFormID = (int) @$aSourceActionData["eBavelActionFormID"];
		        			if ($inteBavelActionFormID <= 0) {
		        				continue;
		        			}
		        			
		        			//Ya identificada la pregunta y la opción de respuesta, obtiene su mapeo específico hacia eBavel, si no hay entonces
		        			//ignorará esta acción (dado a que la colección de opciones de respuesta no está optimizada para grabarse en caché,
		        			//se utilizará directamente un elemento del mismo array de datos de la acción para recuperarlo)
		        			$objQuestionOptionFieldMap = @$aSourceActionData["eBavelFieldMap"];
		        			if (is_null($objQuestionOptionFieldMap)) {
		        				continue;
		        			}
		        			
							foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
								if (!isset($objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) || !$objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) {
									//En este caso el campo no está mapeado, así que lo omite
									continue;
								}
								
								$intSurveyFieldID = ((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
								//Sólo se puede verificar con valores negativos, ya que los positivos para acciones representan preguntas
								if ($intSurveyFieldID < 0) {
									$intSurveyFieldID *= -1;
		        					switch ($intSurveyFieldID) {
		        						case sfidGPSCountry:
		        						case sfidGPSState:
		        						case sfidGPSCity:
		        						case sfidGPSAddress:
		        						case sfidGPSZipCode:
		        						case sfidGPSFullAddress:
		        							$blnGetGPSData = true;
		        							break;
		        					}
								}
	        					
		        				if ($blnGetGPSData) {
		        					break;
		        				}
							}
							
	        				if ($blnGetGPSData) {
	        					break;
	        				}
			        	}
			        	
			        	//Carga la información del GPS para las acciones, pero además marca la bandera para ya no tener que cargarla para el grabado
			        	//de datos
						//@JAPR 2018-08-20: Debido al alto consumo de llamadas al API de Geocoding en eForms, DAbdo autorizó a ya NO traducir las posiciones GPS
						//a direcciones para grabarlas como parte de los datos de cada captura, cubos ni poder mandarlas a ningún destino (#0MESM9)
						//@JAPR 2018-12-05: Se determinó que el uso de Geocoding de Google Maps desde el server no era el problema, así que se regresa esta funcionalidad (#09FEOR)
						if ($blnGetGPSData) {
							$blnGPSDataLoaded = true;
							try {
								if(isset($arrayData['surveyLatitude'])) 
								{
									$latitude = (double)$arrayData['surveyLatitude'];
								}
								else 
								{
									$latitude = 0;
								}
								
								if(isset($arrayData['surveyLongitude'])) 
								{
									$longitude = (double)$arrayData['surveyLongitude'];
								} 
								else 
								{
									$longitude = 0;
								}
								
								//Verifica si se trata de la pregunta especial con valor dummy para pruebas
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\nChecking special GPS data");
									ECHOString("<br>\r\nRepository: ".((string) @$_SESSION["PABITAM_RepositoryName"]));
									ECHOString("<br>\r\nSurveyID: ".((int) @$surveyInstance->SurveyID));
									ECHOString("<br>\r\nQuestion 32 data: ".((string) @$arrayData['qfield32']));
								}
								@logeBavelString("Checking special GPS data");
								@logeBavelString("Repository: ".((string) @$_SESSION["PABITAM_RepositoryName"]));
								@logeBavelString("SurveyID: ".((int) @$surveyInstance->SurveyID));
								
								if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
									$strMessageQuestion = trim((string) @$arrayData['qfield32']);
									if ($strMessageQuestion != '') {
										$arrData = explode(',', $strMessageQuestion);
										if (count($arrData) == 2) {
						        			$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
						        			$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
						        			if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
						        				$latitude = $strLatitude;
						        				$longitude = $strLongitude;
						        			}
										}
									}
								}
								
								//OMMC 2018-12-17: Proceso de obtención de coordenadas para el administrador usando mapkit y phantom
								$mapSetting = @BITAMSettingsVariable::NewInstanceWithName($theRepository, 'MAPTYPE');
								switch($mapSetting) {
									case maptApple:
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\nRetrieving GPS Data using Phantom for latitude $latitude, longitude $longitude");
										}
										
										//Variables del proceso. Ajustar a conveniencia.
										//OMMC 2019-04-12: Modificada la instrucción del PhantomJS para que vuelva a resolver mapas
										$commandLine = 'phantomjs --web-security=no --ignore-ssl-errors=true --ssl-protocol=any mapkitGeocoding.js "service='.$gbKPIFormsService.'" "timeout='.mapkitTimeout.'" "latitude='.$latitude.'" "longitude='.$longitude.'"';
										$intSleepTime = mapkitSleepLength;
										$intWhile = 0;
										$intCapWhile = mapkitWaitLength;
										$output = "";
										$pipesIn = array(
											0 => array('pipe', 'r'), // 0 is STDIN for process
											1 => array('pipe', 'w'), // 1 is STDOUT for process
										);
										$pipesOut = array();
										
										//Función para matar el proceso en el CMD, diferente para sistemas operativos de windows y unix
										//Mata también subprocesos inicializados por el proceso padre, requiere permisos de administrador
										//Programada como alternativa al proc_terminate ya que esta función de PHP en realidad no termina los procesos en windows
										function killProcessID($ppid){ 
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Matando el proceso: " . $ppid);
											}
											return stripos(php_uname('s'), 'win')>-1  ? exec("taskkill /pid ".$ppid." /t /f") : exec("kill -9 $ppid");
										}
										
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Proceso para el comando: " . $commandLine);
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Abriendo el proc... ");
										}
										
										$proc = @proc_open($commandLine, $pipesIn, $pipesOut, getcwd(), NULL);
										$procStatus = @proc_get_status($proc);
										
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Proc abierto... ");
											ECHOString("<br>\r\n ProcessID: " . $procStatus['pid']);
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Cerrando pipes... ");
										}
										if (isset($pipesOut[0])) @fclose($pipesOut[0]);

										//Ciclamos mientras el proceso está corriendo
										while( ($intWhile < $intCapWhile) && proc_get_status($proc)['running']) {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Resultado ". $intWhile ." de read en el ciclo ");
											}
											sleep($intSleepTime);
											$intWhile++;
										}
										
										//Evaluar la salida si el contador llegó al máximo pero el proceso aún sigue corriendo
										if ($intWhile == $intCapWhile && proc_get_status($proc)['running']) {
											killProcessID($procStatus['pid']);
										} else { //De lo contrario sólo se obtiene el output
											$output = stream_get_contents($pipesOut[1]);
										}
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Contenido del output: " . $output);
											ECHOString("<br>\r\n".date("Y-m-d H:i:s")." Cerrando pipe 1 y cerrando el proc ");
										}
										if (isset($pipesOut[1])) @fclose($pipesOut[1]);
										@proc_close($proc);
										if ($output) {
											if ( stripos($output, "error") ) {
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("Error retrieving GPS Data: " );
												}
											} else if ( stripos($output, "formattedAddress") ) {
												$arrGPSData = @getGPSDataFromAppleJson($output);
												if (is_null($arrGPSData) || !is_array($arrGPSData)) {
													$arrGPSData = array();
												}
												
												if (is_array($arrGPSData)) {
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nGPS Data: ");
														PrintMultiArray($arrGPSData);
													}
													if (@$blnSaveeBavelLogStringFile) {
														$strLogText = (string) @var_export(@$arrGPSData, true);
													}
												}
											}
										} else {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("Error retrieving GPS Data: ".(string) $output);
											}
										}
									break;
									case maptGoogle:
									default:
										//@JAPR 2018-03-02: Corregido un bug, faltaba el API Key de google en las llamadas para usar el API, así que excedía el límite de peticiones (#KRZ34J)
										//Se cambia el APIKey que era usada para móviles (googleAPIKey) por la versión para llamadas http (googleServerAPIKey), además necesita https
										//$strGPSURL = "http://maps.google.com/maps/api/geocode/json?sensor=false".googleAPIKey."&language=es&latlng=$latitude,$longitude";
										$strGPSURL = "https://maps.google.com/maps/api/geocode/json?sensor=false".googleServerAPIKey."&language=es&latlng=$latitude,$longitude";
										//@JAPR
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\nRetrieving GPS Data for: $strGPSURL");
										}
										@logeBavelString("Retrieving GPS Data for: $strGPSURL");

										$ch = curl_init(); 
										if ($ch) {
											curl_setopt($ch, CURLOPT_URL, $strGPSURL); 
											curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
											curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
											$strJSON = curl_exec($ch);
											if ($strJSON) {
												curl_close($ch);
												
												//Convierte la respuesta a un objeto para manipular el resultado
												$arrGPSData = @getGPSDataFromJson($strJSON);
												if (is_null($arrGPSData) || !is_array($arrGPSData)) {
													$arrGPSData = array();
												}
												
												if (is_array($arrGPSData)) {
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nGPS Data: ");
														PrintMultiArray($arrGPSData);
													}
													@logeBavelString("GPS Data:");
													if (@$blnSaveeBavelLogStringFile) {
														$strLogText = (string) @var_export(@$arrGPSData, true);
														@logeBavelString($strLogText);
													}
												}
											} else {
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("<br>\r\nError retrieving GPS Data: ".(string) @curl_error($ch));
												}
												@logeBavelString("Error retrieving GPS Data: ".(string) @curl_error($ch));
												curl_close($ch);
											}
										}
									break;
								}
								//OMMC
	        				} catch (Exception $e) {
			        			//Por el momento no reportará este error
			        			$strGPSException = (string) @$e->getMessage();
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\nError retrieving GPS Data for actions: ".$strGPSException);
								}
								@logeBavelString("Error retrieving GPS Data for actions: ".$strGPSException);
	        				}
	        			} else {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo GPS Data will be used");
							}
							@logeBavelString("No GPS Data will be used");
						}
			        	//@JAPR
						
						$arreBavelFieldsByForm = array();
		        		//Este es el método de grabado de acciones a partir de una forma de eBavel
			        	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
			        	foreach ($arrActionItems as $intActionIdx => $aSourceActionData) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nProcessing action #{".($intActionIdx +1)."}");
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									PrintMultiArray($aSourceActionData);
								}
							}
							@logeBavelString("Processing action #{".($intActionIdx +1)."}");
			        		$blnCanSaveeBavelActions = true;
			        		try {
			        			//Antes de comenzar a invocar a eBavel, se tiene que verificar si existe un mapeo de los datos de la acción
			        			//basado en la pregunta y opción de respuesta que la genera, ya que si no es así entonces significaría que en el
			        			//administrador se limitó la funcionalidad y esta debe ser una captura viejita que aun no tenía deshabilitadas
			        			//las acciones, por lo cual sigue enviando información pero ya no se debe grabar porque no habría un mapeo
			        			$intQuestionID = (int) @$aSourceActionData["QuestionID"];
			        			if ($intQuestionID <= 0) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNo QuestionID specified");
									}
									@logeBavelString("No QuestionID specified");
			        				continue;
			        			}
								//@JAPR 2013-07-15: Agregado el mapeo de una forma de eBavel para acciones
								//Ya no es necesario que esté asignado el QuestionAnswer, ya que dependerá del tipo de pregunta y pudiera ser de una
								//pregunta tipo acción simplemente, si se quiere validar correctamente se tendría que agregar al IF el tipo de pregunta,
								//pero se supone que desde que generó el array de acciones ya estaba todo perfectamente validado
			        			$strOptionText = (string) @$aSourceActionData["QuestionAnswer"];
			        			//if (trim($strOptionText) == '') {
			        			//	continue;
			        			//}
			        			//@JAPR
			        			$inteBavelActionFormID = (int) @$aSourceActionData["eBavelActionFormID"];
			        			if ($inteBavelActionFormID <= 0) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNo eBavel form specified");
									}
									@logeBavelString("No eBavel form specified");
			        				continue;
			        			}
			        			
			        			//Ya identificada la pregunta y la opción de respuesta, obtiene su mapeo específico hacia eBavel, si no hay entonces
			        			//ignorará esta acción (dado a que la colección de opciones de respuesta no está optimizada para grabarse en caché,
			        			//se utilizará directamente un elemento del mismo array de datos de la acción para recuperarlo)
			        			$objQuestionOptionFieldMap = @$aSourceActionData["eBavelFieldMap"];
			        			if (is_null($objQuestionOptionFieldMap)) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNo eBavel action field map specified");
									}
									@logeBavelString("No eBavel action field map specified");
			        				continue;
			        			}
			        			else {
			        				$arreBavelEmptyMappedData = array();
			        				$arreBavelMappedIDs = array();
			        				
								    //Verifica si la forma especificada existe
							    	$eBavelFormTable = '';
								    //$arreBavelFieldsInfo = array();
								    $arreBavelFieldsColl = array();
									require_once('eBavelIntegration.inc.php');
							    	$arreBavelFormInfo = null;
							    	//Sólo cargará la forma de acciones default de la encuesta si esta se encuentra configurada, ya que si no se asume que se
							    	//usará la forma configurada por cada pregunta
							    	if ($inteBavelActionFormID > 0) {
										$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
								    	$arreBavelFormColl = @GetEBavelSections($theRepository, true, $surveyInstance->eBavelAppID, array($inteBavelActionFormID));
										$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								    	if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[$inteBavelActionFormID])) {
								    		$arreBavelFormInfo = $arreBavelFormColl[$inteBavelActionFormID];
								    		$eBavelFormTable = $arreBavelFormInfo['name'];
								    		
											$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
								    		$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, $surveyInstance->eBavelAppID, array($inteBavelActionFormID));
											$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								    		if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl) || count($arreBavelFieldsColl) == 0) {
								    			//No se pudo recuperar la info de los campos o no hay campos de eBavel
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("<br>\r\nNo eBavel action fields found: ".$inteBavelActionFormID);
													PrintMultiArray($arreBavelFieldsColl);
												}
												@logeBavelString("No eBavel action fields found: ".$inteBavelActionFormID);
												if (@$blnSaveeBavelLogStringFile) {
													$strLogText = (string) @var_export(@$arreBavelFieldsColl, true);
													@logeBavelString($strLogText);
												}
								    			$blnCanSaveeBavelActions = false;
								    		}
								    	}
								    	else {
								    		//No se pudo recuperar la info de la forma de eBavel
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("<br>\r\nNo eBavel action form found: ".$inteBavelActionFormID);
												PrintMultiArray($arreBavelFormColl);
											}
											@logeBavelString("No eBavel action form found: ".$inteBavelActionFormID);
											if (@$blnSaveeBavelLogStringFile) {
												$strLogText = (string) @var_export(@$arreBavelFormColl, true);
												@logeBavelString($strLogText);
											}
								    		$blnCanSaveeBavelActions = false;
								    	}
							    	}
							    	else {
							    		//En este caso no hay una forma configurada por pregunta, así que se reutilizarán los datos default de la encuesta
							    		$arreBavelFieldsColl = $arrDefaulteBavelFieldsColl;
							    		$eBavelFormTable = $strDefaulteBavelFormTable;
							    	}
			        			}
			        			
			        			//@JAPR 2013-07-15: Faltaba validar en este punto si hubo algún error al recuperar los datos de eBavel para impedir
			        			//que se intente generar una acción que va a fallar (el error ya se reportó arriba)
			        			if (!$blnCanSaveeBavelActions) {
			        				continue;
			        			}
			        			
			        			//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
			        			//la clase equivocada
			        			$strActionMapClassName = (string) @$aSourceActionData["eBavelActionSrcType"];
			        			switch (strtolower($strActionMapClassName)) {
			        				case 'question':
			        					$strActionMapClassName = 'BITAMQuestionActionMap';
			        					break;
			        				//case 'answer':
			        				default:
			        					$strActionMapClassName = 'BITAMQuestionOptionActionMap';
			        					break;
			        			}
			        			//@JAPR

								foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
									if (!isset($objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) || !$objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) {
										//En este caso el campo no está mapeado, así que lo omite
										continue;
									}
									
									//Si el campo de eBavel especificado no existe, omite el mapeo
									if (!isset($arreBavelFieldsColl[$inteBavelFieldID])) {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\nMapped field do not exists: {$inteBavelFieldID} => $strVariableName");
										}
										@logeBavelString("Mapped field do not exists: {$inteBavelFieldID} => $strVariableName");
										continue;
									}
									
									$intSurveyFieldID = ((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
									if ($intSurveyFieldID != 0) {
			        					//Se puede usar la clase BITAMQuestionOptionActionMap indistintamente de si es o no una acción de opción de
			        					//respuesta, ya que todas estas acciones siguen la misma base y por ende sus clases usarían los mismos IDs
					        			//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
					        			//la clase equivocada
										//$strSurveyVariableName = (string) @BITAMQuestionOptionActionMap::GetSurveyColumnName($intSurveyFieldID);
										$strSurveyVariableName = call_user_func($strActionMapClassName.'::GetSurveyColumnName', $intSurveyFieldID);
										//@JAPR
										if (trim($strSurveyVariableName) != '' ) {
											$arreBavelEmptyMappedData[$strSurveyVariableName] = null;
											//@JAPR 2013-12-12: Corregido un bug, diferentes campos de eBavel se pueden mapear al mismo valor de
											//eForms, por lo tanto usar la llave del campo de eForms para el array no era correcto, ya que de esa
											//forma sólo el último en la configuración se habría grabado, ahora se manejará un array
											if (!isset($arreBavelMappedIDs[$strSurveyVariableName])) {
												$arreBavelMappedIDs[$strSurveyVariableName] = array();
											}
											$arreBavelMappedIDs[$strSurveyVariableName][] = $inteBavelFieldID;
											//@JAPR
										}
									}
									else {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("<br>\r\nInvalid field value: {$inteBavelFieldID} => $strVariableName");
										}
										@logeBavelString("Invalid field value: {$inteBavelFieldID} => $strVariableName");
									}
								}
			        			
			        			$anActionData = array();
			        			//Hay datos que siempre son fijos así que se agregan tal cual
								$anActionData["createdDate"] = @$aSourceActionData["createdDate"];
								$anActionData["modifiedDate"] = @$aSourceActionData["modifiedDate"];
			        			foreach ($arreBavelMappedIDs as $aneFormsFieldID => $aneBavelFieldArray) {		//$aneBavelFieldID
									//@JAPR 2013-12-12: Corregido un bug, diferentes campos de eBavel se pueden mapear al mismo valor de
									//eForms, por lo tanto usar la llave del campo de eForms para el array no era correcto, ya que de esa
									//forma sólo el último en la configuración se habría grabado, ahora se manejará un array
			        				if (!is_array($aneBavelFieldArray)) {
			        					$aneBavelFieldArray = array($aneBavelFieldArray);
			        				}
			        				
			        				//Recorre el ciclo de todos los campos de eBavel que apuntan a este mismo campo de eForms
			        				foreach ($aneBavelFieldArray as $aneBavelFieldID) {
			        					$arreBavelFieldInfo = @$arreBavelFieldsColl[$aneBavelFieldID];
			        					if (is_null($arreBavelFieldInfo) || !is_array($arreBavelFieldInfo)) {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("<br>\r\nNo eBavel field info: ".$aneBavelFieldID);
											}
											@logeBavelString("No eBavel field info: ".$aneBavelFieldID);
			        						continue;
			        					}
			        					
			        					$inteBavelQTypeID = convertEBavelToEFormsFieldType($arreBavelFieldInfo['tagname']);
			        					$streBavelFieldName = (string) @$arreBavelFieldInfo['label'];
			        					$aData = null;
			        					$blnSkipField = false;
				        				if (is_numeric(str_ireplace('qfield', '', $aneFormsFieldID))) {
				        					//Es una pregunta de la encuesta, así que se busca el valor que previamente se tuvo que haber agregado
				        					//al array de datos de la acción, utilizando el nombre de variable de preguntas
											//@JAPR 2014-10-29: Agregado el mapeo de Scores
				        					//En este punto las preguntas pudieran tener diferente valor configurado según el DataID utilizado, así
				        					//que ahora debe ajustar el elemento que busca en el array pues no siempre será la respuesta de la pregunta,
				        					//esto es así porque la variable $aneFormsFieldID se genera de forma genérica con la estructura qfield###
				        					//sin importarle el dato específico a pedir de la pregunta, así que en este punto se valida eso
				        					$intDataID = (int) @$objQuestionOptionFieldMap->ArrayVariablesDataID[$aneBavelFieldID];
				        					$strFormsFieldID = $aneFormsFieldID;
				        					switch ($intDataID) {
				        						case sdidScore:
				        							$strFormsFieldID .= "Score";
				        							break;
				        					}
				        					
				        					if (isset($aSourceActionData[$strFormsFieldID])) {
												//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
												//no se puede obtener su valor durante el procesamiento de registros que no sean el único de secciones estándar,
												//Se agregará un array con las respuestas a todas estas preguntas justo cuando se proceso este registro único para reasignarlo mas
												//adelante. Si viene un array con el dummy que así lo indica, entonces este valor se obtendrá del array especial de
												//preguntas numéricas
				        						$aData = $aSourceActionData[$strFormsFieldID];
				        						if (is_array($aData)) {
				        							//No puede venir como respuesta un array, así que automáticamente esta pregunta no es válida a menos que se
				        							//esté pidiendo el dummy para preguntas numéricas de secciones estandar
				        							if (((string) @$aData[0]) == 'GetFromStRec') {
				        								//Se obtiene el valor del dummy si es que existe
				        								$aData = @$arrNumericQAnswersColl[$aneFormsFieldID];
				        							}
				        							else {
				        								//Definitivamente no es una respuesta válida
				        								$blnSkipField = true;
				        							}
				        						}
				        						
				        						//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
												//@JAPR 2014-10-29: Agregado el mapeo de Scores
												//Estas validaciones sólo aplican si se está pidiendo el valor de la pregunta
				        						if (!$blnSkipField && $intDataID == sdidValue) {
					        						//Obtiene el ID de pregunta para validar ciertas cosas dependiendo del tipo
					        						$intQuestionID = str_ireplace('qfield', '', $aneFormsFieldID);
						        					$objQuestion = @$arrQuestionsColl[$intQuestionID];
						        					if (is_null($objQuestion)) {
														if (getParamValue('DebugBreak', 'both', '(int)', true)) {
															ECHOString("<br>\r\nQuestionID not found: $aneFormsFieldID");
														}
						        					}
						        					else {
						        						//Verifica por tipo de pregunta para saber como debe grabarla
						        						switch ($objQuestion->QTypeID) {
						        							case qtpMulti:
						        								//En el caso de las multiple choice, eBavel espera un array con las descripciones de
						        								//las opciones seleccionadas cuando se mapea a un campo multiple-choice, si se mapea
						        								//a un alfanumerico entonces serán las propias descripciones separadas por ","
						        								//Por el momento sólo se pueden mapear preguntas multiple-choice tipo CheckBox
																switch ($inteBavelQTypeID) {
																	case qtpMulti:
																		//En este caso se espera un array de las opciones seleccionadas
																		if (trim($aData) == '') {
																			$aData = array();
																		}
																		else {
																			$aData = @explode(';', $aData);
																		}
																		break;
																		
																	case qtpOpenString:
																	case qtpOpenAlpha:
																		//Se convierte a una lista de opciones separada por ","
																		$aData = str_replace(';', ',', $aData);
																		break;
																		
																	default:
																		//Tipo de conversión no soportado
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
																			PrintMultiArray($objAnswers);
																		}
																		break;
																}
																
						        								break;
						        							
						        							case qtpPhoto:
																$blnIsPhotoName = true;
																$insertImage = $aData;
																//Genera el nombre de la ruta de la imagen según eBavel
																$manager = new Tables();
															    @$manager->notifyErrorService = true;
															    $manager->applicationCodeName = $eBavelAppCodeName;
															    $manager->init($theRepository->ADOConnection);
																$params = array();
																$params['sectionname'] = $eBavelFormTable;
																$params['widgettype'] = 'widget_photo';
																$params['ext'] = 'jpg';
																$aData = $manager->getFilePath($params, null);
																$strCompletePath = $streBavelPath.$aData;
																if ($inteBavelQTypeID == qtpPhoto) {
																	//Sólo si el campo de eBavel es un tipo foto es como se copia la imagen
																	if ($usePhotoPaths && $blnIsPhotoName) {
																		//Realiza la copia del archivo de imagen
																		//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
																		if ($blnWebMode) {
																			$path = getcwd()."\\";
																		}
																		else {
																			//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
																			$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
																		}
																		$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertImage);
																		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
																		if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
																			$strCompleteSourcePath .= '.jpg';
																		}
																		$status = @copy($strCompleteSourcePath, $strCompletePath);
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			ECHOString("<br>\r\nSaving eBavel action photo from $strCompleteSourcePath to $strCompletePath: $status");
																		}
																	}
																	else {
																		$aData = null;
																		//Esta funcionalidad es exclusiva de v4, así que no se soportará el viejo formato para este proceso
																		/*
																        if($insertImage == "photoimage") {
																            $tmpfilename = $_FILES['photoimage'.$aQuestion->QuestionID]['tmp_name'];
																            $status = move_uploaded_file($tmpfilename, $strCompletePath);
																        } else {
																			$status = file_put_contents ($strCompletePath, $insertImage);
																        }
																        */
																	}
																}
																
																switch ($inteBavelQTypeID) {
																	case qtpPhoto:
																	case qtpOpenString:
																	case qtpOpenAlpha:
																		//En los 3 tipos se graba el dato como la ruta de la imagen
																		break;
																		
																	default:
																		//Tipo de conversión no soportado
																		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																			ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
																			PrintMultiArray($objAnswers);
																		}
																		$aData = null;
																		break;
																}
						        								break;
						        							case qtpSketch:	
						        							case qtpSignature:
															case qtpAudio:
															case qtpVideo:
															//Conchita agregados audio y video 20140924
						        							case qtpDocument:
																//@JAPRPendiente
						        								break;
						        						}
						        					}
				        						}
				        						//@JAPR
				        					}
				        					else {
				        						//No venía el dato, por lo tanto se ignorará ya que para las acciones se tiene que haber registrado
				        						//al momento de registrar la acción, esto por si la acción se encontrara dentro de una sección múltiple
				        						//por lo que no se hubiera podido determinar fácilmente su valor en este punto
				        						$blnSkipField = true;
				        					}
				        				}
				        				else {
				        					//Es un campo fijo de datos de la encuesta
				        					//Se puede usar la clase BITAMQuestionOptionActionMap indistintamente de si es o no una acción de opción de
				        					//respuesta, ya que todas estas acciones siguen la misma base y por ende sus clases usarían los mismos IDs
						        			//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
						        			//la clase equivocada
				        					//$intSurveyFieldID = BITAMQuestionOptionActionMap::GetSurveyColumnID($aneFormsFieldID);
				        					$intSurveyFieldID = call_user_func($strActionMapClassName.'::GetSurveyColumnID', $aneFormsFieldID);
											//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
				        					$intAttributeID = (int) @$objQuestionOptionFieldMap->ArrayVariablesMemberID[$aneBavelFieldID];
				        					//@JAPR
				        					switch ($intSurveyFieldID) {
												//*********************************************************************************************
				        						//Campos generales de la captura de encuesta
												//*********************************************************************************************
												case sfidStartDate:
													$aData = substr($arrayData["surveyDate"], 0, 10);
													break;
												case sfidStartHour:
													$aData = $arrayData["surveyHour"];
													break;
												case sfidEndDate:
													$aData = substr($thisDate, 0, 10);
													break;
												case sfidEndHour:
													$aData = substr($thisDate, 11);
													break;
												case sfidLocation:
													$blnSkipField = true;
													if(isset($arrayData['surveyLatitude'])) 
													{
														$latitude = (double)$arrayData['surveyLatitude'];
													}
													else 
													{
														$latitude = 0;
													}
													
													if(isset($arrayData['surveyLongitude'])) 
													{
														$longitude = (double)$arrayData['surveyLongitude'];
													} 
													else 
													{
														$longitude = 0;
													}
													
													if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
														$strMessageQuestion = trim((string) @$arrayData['qfield32']);
														if ($strMessageQuestion != '') {
															$arrData = explode(',', $strMessageQuestion);
															if (count($arrData) == 2) {
											        			$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
											        			$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
											        			if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
											        				$latitude = $strLatitude;
											        				$longitude = $strLongitude;
											        			}
															}
														}
													}
													
													$anActionData[$arreBavelFieldInfo['id']] = $latitude.', '.$longitude;
													break;
												//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
												case sfidLocationAcc:
													if(isset($arrayData['surveyAccuracy'])) 
													{
														$accuracy = (double)$arrayData['surveyAccuracy'];
													}
													else 
													{
														$accuracy = 0;
													}
													$aData = $accuracy;
													//@JAPR 2014-05-20: Corregido un bug, faltaba el break
													break;
												//@JAPR
												case sfidSyncDate:
													$aData = $syncDate;
													break;
												case sfidSyncHour:
													$aData = $syncTime;
													break;
												case sfidServerStartDate:
													if (isset($arrayData["serverSurveyDate"])) {
														$aData = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
													}
													break;
												case sfidServerStartHour:
													if (isset($arrayData["serverSurveyHour"])) {
														$aData = (string) @$arrayData["serverSurveyHour"];
													}
													break;
												case sfidServerEndDate:
													if (isset($arrayData["serverSurveyEndDate"])) {
														$aData = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
													}
													break;
												case sfidServerEndHour:
													if (isset($arrayData["serverSurveyEndHour"])) {
														$aData = (string) @$arrayData["serverSurveyEndHour"];
													}
													break;
												case sfidScheduler:
													$aData = null;	//@JAPRPendiente
													break;
												case sfidEMail:
													$aData = null;	//@JAPRPendiente
													break;
												case sfidDuration:
													$aData = null;	//@JAPRPendiente
													break;
												case sfidAnsweredQuestions:
													$aData = null;	//@JAPRPendiente
													break;
												case sfidEntrySection:
													$aData = 0;		//@JAPRPendiente
													break;
												case sfidDynamicPage:
													$aData = null;	//@JAPRPendiente
													break;
												case sfidDynamicOption:
													$aData = null;	//@JAPRPendiente
													break;
												case safidSourceRowKey:
												case sfidFactKey:
													$aData = (int) @$aSourceActionData["sourceRow_key"];
													break;
												case sfidEntryID:
													$aData = (int) $factKeyDimVal;
													break;
												//@JAPR 2013-05-03: Agregados campos extras al mapeo de datos general de la encuesta
												case sfidUserID:
													$aData = (int) @$theUser->UserID;
													break;
												//@JAPR 2013-05-06: Agregados campos extras al mapeo de datos general de la encuesta
												case sfidUserName:
													$aData = (string) @$theAppUser->UserName;
													break;
												//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
												case sfidGPSCountry:
													$aData = (string) @$arrGPSData['Country'];
													break;
												case sfidGPSState:
													$aData = (string) @$arrGPSData['State'];
													break;
												case sfidGPSCity:
													$aData = (string) @$arrGPSData['City'];
													break;
												case sfidGPSZipCode:
													$aData = (string) @$arrGPSData['ZipCode'];
													break;
												case sfidGPSAddress:
													$aData = (string) @$arrGPSData['Address'];
													break;
												case sfidGPSFullAddress:
													$aData = (string) @$arrGPSData['FullAddress'];
													break;
												//*********************************************************************************************
				        						//Campos exclusivos para acciones
												//*********************************************************************************************
												case safidDescription:
													$aData = (string) @$aSourceActionData["description"];
													break;
												//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
												case safidTitle:
													$aData = (string) @$aSourceActionData["title"];
													break;
												//@JAPR
												case safidResponsible:
													$intUserID = (int) @$aSourceActionData["user"];
													if ($intUserID <= 0) {
														$intUserID = 1;
													}
													
													if ($inteBavelQTypeID == qtpOpenNumeric) {
														//Si es un campo numérico entonces se graba directamente el ID del usuario
														$aData = $intUserID;
													}
													else {
														//En este caso se debe obtener el nombre del usuario
														if (!isset($arrUserNames[$intUserID])) {
															$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
															//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
															$objResponsible = BITAMeFormsUser::NewInstanceWithID($theRepository, $intUserID);
															$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
															if (!is_null($objResponsible)) {
																$strResponsibleName = $objResponsible->FullName;
															}
															$arrUserNames[$intUserID] = $strResponsibleName;
														}
														$strResponsibleName = (string) @$arrUserNames[$intUserID];
														//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
														$aData = $strResponsibleName;
													}
													break;
												case safidCategory:
													$aData = (string) @$aSourceActionData["category"];
													break;
												case safidDueDate:
													$aData = (string) @$aSourceActionData["deadline"];
													break;
												case safidStatus:
													//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
													$aData = $strDefaultActionStatus;
													break;
												case safidSource:
													//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
													$aData = $strDefaultActionSource;
													break;
												case safidSourceID:
													if ($inteBavelQTypeID == qtpOpenNumeric) {
														//Si es un campo numérico entonces se graba directamente el ID del usuario
														$aData = $surveyID;
													}
													else {
														//En este caso se debe obtener el nombre del usuario
														//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
														$aData = $surveyInstance->SurveyName;
													}
													break;
												//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
												case sfidFixedNumExpression:
													$aData = (float) @$aSourceActionData["value".$aneBavelFieldID]; 
													break;
												case sfidFixedStrExpression:
													$aData = (string) @$aSourceActionData["value".$aneBavelFieldID]; 
													break;
												//@JAPR 2014-10-29: Agregado el mapeo de Scores
												case sfidSurveyScore:
													$aData = (int) @$_POST['SurveyScore'];
													break;
												//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
												case sfidCatalogAttribute:
													$aData = (string) @$aSourceActionData["attribValue".$intAttributeID]; 
													break;
												//@JAPR
				        						default:
				        							//Campos no conocidos regresarán null para que no se consideren
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nUnknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");
													}
													@logeBavelString("Unknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");
				        							break;
				        					}
				        				}
				        				
				        				//Solo inserta el dato si no es null el valor o si se saltó por requerir trato especial, en cuyo caso en el switch
				        				//se habría insertado
				        				if (!$blnSkipField && !is_null($aData)) {
				        					$anActionData[$arreBavelFieldInfo['id']] = $aData;
				        				}
			        				}
			        				//@JAPR
			        			}
								
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\neBavel path: $strPath");
									ECHOString("<br>\r\neBavel app: $eBavelAppCodeName");
									ECHOString("<br>\r\neBavel table: $eBavelFormTable");
								}
								@logeBavelString("eBavel path: $strPath");
								@logeBavelString("eBavel app: $eBavelAppCodeName");
								@logeBavelString("eBavel table: $eBavelFormTable");
			        			
								//Inicializa datos fijos adicionales
								$anActionData["id_".$eBavelFormTable] = -1;
								
								//Invoca al grabado de eBavel
							    $manager = new Tables();
							    //@JAPR 2013-05-03: Agregado el control de errores con eBavel para evitar que interrumpa la ejecución del script y
							    //simplemente lance una excepción que puede ser atrapada por eForms
							    @$manager->notifyErrorService = true;
							    //@JAPR
							    $manager->applicationCodeName = $eBavelAppCodeName;
							    $manager->init($theRepository->ADOConnection);
				        		$insertData = array();
				        		$insertData[-1] = $anActionData;
				        		
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\neBavel insert data in table: $eBavelFormTable");
									PrintMultiArray($insertData);
								}
								@logeBavelString("eBavel insert data in table: $eBavelFormTable");
								if (@$blnSaveeBavelLogStringFile) {
									$strLogText = (string) @var_export(@$insertData, true);
									@logeBavelString($strLogText);
								}
			        			
								//@JAPR 2014-10-14: Agregada la edición de datos en eBavel
								//@JAPR 2014-11-28: Agregado el parámetro para permitir que los campos de eBavel que utilizan catálogo
								//puedan recibir siempre las descripciones y así no se confunda pensando que lo que recibe es el key
								$arrParams = array('alldescription' => true);
								$manager->insertData($eBavelFormTable, $insertData, array(), $theRepository->ADOConnection, 'overwrite', $arrParams);
			        		} catch (Exception $e) {
			        			//Por el momento no reportará este error
			        			$streBavelException = (string) @$e->getMessage();
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\neBavel exception saving action data: ".$streBavelException);
								}
								@logeBavelString("eBavel exception saving action data: ".$streBavelException);
								$thelog = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $streBavelException . " POST = " . print_r($_POST,true);
								$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
								@error_log($thelog, 3, $thefile);
								
								$stat_action = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
								@error_log($stat_action, 3, $statlogfile);
			        		}
			        	}
					   	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		        	}
		        	else {
		        		//Este es el método original que inserta en la tabla ACTIONITEMS
			        	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
			        	foreach ($arrActionItems as $intActionIdx => $anActionData) {
			        		try {
							    $manager = new Tables();
							    //@JAPR 2013-05-03: Agregado el control de errores con eBavel para evitar que interrumpa la ejecución del script y
							    //simplemente lance una excepción que puede ser atrapada por eForms
							    @$manager->notifyErrorService = true;
							    //@JAPR
							    $manager->applicationCodeName = $eBavelAppCodeName;
							    $manager->init($theRepository->ADOConnection);
				        		$insertData = array();
				        		$insertData[-1] = $anActionData;
								$manager->insertData('ACTIONITEMS', $insertData, array(), $theRepository->ADOConnection);
			        		} catch (Exception $e) {
			        			//Por el momento no reportará este error
			        			$streBavelException = (string) @$e->getMessage();
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("<br>\r\neBavel exception saving action data: ".$streBavelException);
								}
								@logeBavelString("eBavel exception saving action data: ".$streBavelException);
								$thelog = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $streBavelException . " POST = " . print_r($_POST,true);
								$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
								@error_log($thelog, 3, $thefile);
								
								$stat_action = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
								@error_log($stat_action, 3, $statlogfile);
			        		}
			        	}
					   	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		        	}
		        	
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\nFinishing eBavel action saving process");
					}
					@logeBavelString("Finishing eBavel action saving process");
		        }
		        
		        if ($agendaID > 0 && ($strStatus == "" || $strStatus == "OK"))
		        {
					//Si se envió el ID de la agenda y se logró grabar la captura de encuesta, se actualiza el Status de la misma para que ya no se
					//vuelva a considerar en la descarga de las Agendas hacia los móviles
					//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
					//Cuando se graba una encuesta de Agenda la cual está marcada con un estatus de recalendarización, la propia Agenda queda
					//marcada de esa manera, aunque realmente no hay que cambiar nada en su definición, simplemente se presenta de manera diferente
					//en la lista de Agendas
					$intStatus = agdAnswered;
					if ($surveyInstance->EnableReschedule)
					{
						$intStatus = agdRescheduled;
					}
					$sql = "UPDATE SI_SV_SurveyAgendaDet SET Status = ".$intStatus.
						" WHERE AgendaID = ".$agendaID." AND SurveyID = ".$surveyID;
					$theRepository->DataADOConnection->Execute($sql);
					
					//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
					//Ahora al grabar cualquier captura de encuesta de agenda, automáticamente se va a marcar la agenda como contestada, excepto
					//si previamente ya se encontraba cancelada o recalendarizada (sería un error que estuviera recalendarizada, pero no se
					//debe cambiar dicho estado, la cancelación ocurre durante el grabado si viene cierto parámetro así que esto tentativamente
					//o estaría ocurriendo aquí porque este es el grabado de la forma de Check-In, no podría haber una captura de alguna otra
					//forma ya con un estado de agenda cancelada)
					$strAdditionalFields = '';
					$strAdditionalValues = '';
					if (getMDVersion() >= esvAgendaRedesign) {
						$intStatus = agdAnswered;
						$strExtraFilters = " AND (Status IS NULL OR Status <= ".agdAnswered.")";
						if ((int) @$_POST['AgendaCancelled']) {
							$intStatus = agdCanceled;
							$strExtraFilters = '';
							//Si se está cancelando la agenda, la única validación que tiene prioridad es si la agenda había sido recalendarizada
							//desde el Administrador, en ese caso NO podría actualizar el status a cancelada porque recalendarizado tiene prioridad
							$strExtraFilters = " AND (Status IS NULL OR Status <> ".agdRescheduled.")";
						}
						else {
							//Si no se está cancelando la agenda, entonces verifica si aún había alguna encuesta que no estuviera terminada,
							//ya que si todas estaban terminadas entonces el status a grabar debería ser terminada en lugar de contestada
							$intNonFinishedAgendas = 0;
							$sql = "SELECT COUNT(*) AS NonFinishedNum 
								FROM SI_SV_SurveyAgendaDet 
								WHERE AgendaID = {$agendaID} AND (Status IS NULL OR Status = ".agdOpen.")";
							$tmpVal = $theRepository->DataADOConnection->Execute($sql);
							if ($tmpVal && !$tmpVal->EOF) {
								$intNonFinishedAgendas = (int) @$tmpVal->fields["nonfinishednum"];
							}
							
							if ($intNonFinishedAgendas == 0) {
								$intStatus = agdFinished;
							}
						}
						
						//Si el estado de esta agenda es cancelada, entonces si debe ejecutar el UPDATE sin restricciones pues va a cambiar
						//el estado, de lo contrario sólo puede cambiar el estado si la agenda no había sido ni terminada ni cancelada o 
						//recalendarizada
						$sql = "UPDATE SI_SV_SurveyAgenda SET Status = {$intStatus} 
							WHERE AgendaID = {$agendaID} ".$strExtraFilters;
						$theRepository->DataADOConnection->Execute($sql);
					}
					//@JAPR
					
					//@JAPR 2012-06-01: Temporalmente se modificará la fecha de actualización de la Agenda para que las Apps piensen que han cambiado
					//y eso las force a descargar las definiciones de nuevo, de lo contrario la App se quedaría con las mismas encuestas por agenda
					//incluso las ya contestadas y las intentaría capturar nuevamente
					$currentDate = date("Y-m-d H:i:s");
					$sql = "UPDATE SI_SV_SurveyAgenda SET LastDateID = ".$theRepository->DataADOConnection->DBTimeStamp($currentDate).
						" WHERE AgendaID = ".$agendaID;
					$theRepository->DataADOConnection->Execute($sql);
					//@JAPR
					
					//Conchita 2013-01-22 Se debe de modificar el versionnum para que se descarguen bien definiciones en el movil
					$sql = "SELECT MAX(VersionNum) AS Num FROM SI_SV_SurveyAgenda WHERE AgendaID = ".$agendaID;
					$tmpVal = $theRepository->DataADOConnection->Execute($sql);
					if ($tmpVal) {
						$oldVersion = (int) @$tmpVal->fields["num"];
						$oldVersion ++;
						$sql = "UPDATE SI_SV_SurveyAgenda SET VersionNum = ".$oldVersion." WHERE AgendaID = ".$agendaID;
						$theRepository->DataADOConnection->Execute($sql);
					}
					//Conchita
		        }
		        
				//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
				//Inicia el grabado de datos en las formas de eBavel
				$arrAnswers = @$jsonData['answers'];
				$blnCanSaveeBavelData = (!is_null($arrAnswers) && ($strStatus == "" || $strStatus == "OK"));
				
				//Valida que se tengan las configuraciones necesarias para poder interactuar con el eBavel
				if ($blnCanSaveeBavelData) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\nStarting eBavel data saving process: ".$blnCanSaveeBavelData);
					}
					
					require_once('settingsvariable.inc.php');
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELURL');
					if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo eBavel path configured: ".$blnCanSaveeBavelData);
						}
						$blnCanSaveeBavelData = false;
					}
				}
				
				if ($blnCanSaveeBavelData) {
					$eBavelAppCodeName = '';
					$sql = "SELECT codeName FROM SI_FORMS_APPS WHERE id_app = ".$surveyInstance->eBavelAppID;
				    $aRS = $theRepository->ADOConnection->Execute($sql);
				    if ($aRS === false || $aRS->EOF)
				    {
						$blnCanSaveeBavelData = false;
				    }
				    else 
				    {
				    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
				    	$eBavelAppCodeName = (string) $aRS->fields["codename"];
				    	if (trim($eBavelAppCodeName) == '' || $surveyInstance->eBavelFormID <= 0)
				    	{
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo eBavel app found: ".$surveyInstance->eBavelAppID);
							}
				    		$blnCanSaveeBavelData = false;
				    	}
				    }
				}
				
			    //Verifica si la forma especificada existe
		    	$eBavelFormTable = '';
			    //$arreBavelFieldsInfo = array();
			    $arreBavelFieldsColl = array();
			    if ($blnCanSaveeBavelData) {
					require_once('eBavelIntegration.inc.php');
			    	$arreBavelFormInfo = null;
			    	$arreBavelFormColl = @GetEBavelSections($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelFormID));
			    	if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[$surveyInstance->eBavelFormID])) {
			    		$arreBavelFormInfo = $arreBavelFormColl[$surveyInstance->eBavelFormID];
			    		$eBavelFormTable = $arreBavelFormInfo['name'];
			    		
			    		$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelFormID));
			    		if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl) || count($arreBavelFieldsColl) == 0) {
			    			//No se pudo recuperar la info de los campos o no hay campos de eBavel
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo eBavel fields found: ".$surveyInstance->eBavelFormID);
								PrintMultiArray($arreBavelFieldsColl);
							}
			    			$blnCanSaveeBavelData = false;
			    		}
			    	}
			    	else {
			    		//No se pudo recuperar la info de la forma de eBavel
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo eBavel form found: ".$surveyInstance->eBavelFormID);
							PrintMultiArray($arreBavelFormColl);
						}
			    		$blnCanSaveeBavelData = false;
			    	}
			    }
			    
			    //Verifica si el código de eBavel existe en la ruta indicada
			    $strPath = '';
			    $streBavelPath = '';
				if ($blnCanSaveeBavelData) {
					$arrURL = parse_url($objSetting->SettingValue);
					$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
					$strPath = str_replace('/', '\\', $strPath);
					$strPath = str_replace('\\\\', '\\', $strPath);
					//@JAPR 2012-07-10: Corregido un bug, si la ruta al eBavel configurada  no existe, a pesar de ignorar el error mediante el uso de @
					//estaba generando un error de compilación interrumpiendo con ello el grabado al primer registro de una captura que debió haber
					//grabado múltiples registros
					if (file_exists($strPath))
					{
						@require_once($strPath);
						$streBavelPath = str_ireplace('service.php', '', $strPath);
					}
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						if (!function_exists('NotifyError')) {
							ECHOString("<br>\r\nNo NotifyError function");
						}
						else {
							ECHOString("<br>\r\nNotifyError function exists");
						}
					}
					//@JAPR
					
					if (!class_exists('Tables'))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo eBavel code loaded: ".$blnCanSaveeBavelData);
						}
						$blnCanSaveeBavelData = false;
					}
				}
				
				//Array indexado por el Id de pregunta o nombre específico de un campo del que se deben obtener datos para grabar en eBavel
				$arreBavelEmptyMappedData = array();
				//Array con los IDs mapeados de campos de eBavel indexado por el Id de pregunta o nombre específico de campo de eForms
				$arreBavelMappedIDs = array();
				//Array con las preguntas y secciones indexadas por su ID
				$arrQuestionsColl = array();
				$arrSectionsColl = array();
				//@JAPR 2013-05-30: Validado que si no es la versión de Metadata correcta entonces no intente usar la funcionalidad
				//Obtiene los mapeos de campos default de eForms
				if (getMDVersion() >= esveBavelMapping) {
				}
				else {
					$blnCanSaveeBavelData = false;
				}
				//@JAPR
				
				if ($blnCanSaveeBavelData) {
					require_once("surveyFieldMap.inc.php");
					$objSurveyFieldMap = BITAMSurveyFieldMap::NewInstanceWithSurvey($theRepository, $surveyID);
					if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\nNo fields mapped: ".$blnCanSaveeBavelData);
						}
						$blnCanSaveeBavelData = false;
					}
					else {
						foreach($objSurveyFieldMap->ArrayVariables as $intSurveyFieldID => $strVariableName) {
							$intVariableValue = (int) @$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID];
							if ($intVariableValue > 0 && isset($arreBavelFieldsColl[$intVariableValue])) {
								$arreBavelEmptyMappedData[$strVariableName] = null;
								$arreBavelMappedIDs[$strVariableName] = $intVariableValue;
							}
						}
					}
				}
				
				//Por ahora sólo grabará un único registro de datos, por lo que secciones maestro-detalle no estarían soportadas
				if ($blnCanSaveeBavelData) {
					$blnDecodeUTF8 = getParamValue('utf8decode', 'both', "(int)", true);
					$questionCollection = BITAMQuestionCollection::NewInstance($theRepository, $surveyID);
					foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
						if ($aQuestion->eBavelFieldID > 0) {
							$intQuestionID = $aQuestion->QuestionID;
							if (isset($arreBavelFieldsColl[$aQuestion->eBavelFieldID])) {
								//Es un campo existente, se agrega a los arrays de mapeo
								$arreBavelEmptyMappedData[$intQuestionID] = null;
								$arreBavelMappedIDs[$intQuestionID] = $aQuestion->eBavelFieldID;
								$arrQuestionsColl[$intQuestionID] = $aQuestion;
							}
						}
					}
					
					$sectionCollection = BITAMSectionCollection::NewInstance($theRepository, $surveyID);
					foreach ($sectionCollection->Collection as $sectionKey => $aSection) {
						$arrSectionsColl[$aSection->SectionID] = $aSection;
					}
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\neBavel mapped ids: ");
						PrintMultiArray($arreBavelMappedIDs);
					}
					
					//Inicia la inserción de datos en el eBavel recorriendo el array de campos mapeados para verificar cual de ellos tiene valor
					//y con ello generar el registro
		        	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
	        		try {
						//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
						$blnGetGPSData = false;
	        			foreach ($arreBavelMappedIDs as $aneFormsFieldID => $aneBavelFieldID) {
	        				if (!is_numeric($aneFormsFieldID)) {
	        					switch (BITAMSurveyFieldMap::GetSurveyColumnID($aneFormsFieldID)) {
	        						case sfidGPSCountry:
	        						case sfidGPSState:
	        						case sfidGPSCity:
	        						case sfidGPSAddress:
	        						case sfidGPSZipCode:
	        						case sfidGPSFullAddress:
	        							$blnGetGPSData = true;
	        							break;
	        					}
	        				}
	        				
	        				if ($blnGetGPSData && !$blnGPSDataLoaded) {
	        					break;
	        				}
	        			}
	        			
	        			if (!$blnGPSDataLoaded) {
	        				$arrGPSData = array();
	        			}
	        			
						//@JAPR 2018-08-20: Debido al alto consumo de llamadas al API de Geocoding en eForms, DAbdo autorizó a ya NO traducir las posiciones GPS
						//a direcciones para grabarlas como parte de los datos de cada captura, cubos ni poder mandarlas a ningún destino (#0MESM9)
						//@JAPR 2018-12-05: Se determinó que el uso de Geocoding de Google Maps desde el server no era el problema, así que se regresa esta funcionalidad (#09FEOR)
						if ($blnGetGPSData && !$blnGPSDataLoaded) {
							if(isset($arrayData['surveyLatitude'])) 
							{
								$latitude = (double)$arrayData['surveyLatitude'];
							}
							else 
							{
								$latitude = 0;
							}
							
							if(isset($arrayData['surveyLongitude'])) 
							{
								$longitude = (double)$arrayData['surveyLongitude'];
							} 
							else 
							{
								$longitude = 0;
							}
							
							//Verifica si se trata de la pregunta especial con valor dummy para pruebas
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nChecking special GPS data");
								ECHOString("<br>\r\nRepository: ".((string) @$_SESSION["PABITAM_RepositoryName"]));
								ECHOString("<br>\r\nSurveyID: ".((int) @$surveyInstance->SurveyID));
								ECHOString("<br>\r\nQuestion 32 data: ".((string) @$arrayData['qfield32']));
							}
							
							if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
								$strMessageQuestion = trim((string) @$arrayData['qfield32']);
								if ($strMessageQuestion != '') {
									$arrData = explode(',', $strMessageQuestion);
									if (count($arrData) == 2) {
					        			$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
					        			$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
					        			if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
					        				$latitude = $strLatitude;
					        				$longitude = $strLongitude;
					        			}
									}
								}
							}

							//OMMC 2018-12-17: Proceso de obtención de coordenadas para el administrador usando mapkit y phantom
							$mapSetting = @BITAMSettingsVariable::NewInstanceWithName($theRepository, 'MAPTYPE');
							$intMapType = maptGoogle;
							if (!is_null($mapSetting) && trim($mapSetting->SettingValue != '')) {
								$intMapType = (int) @$mapSetting->SettingValue;
								if ( $intMapType < maptGoogle || $intMapType > maptApple ) {
									$intMapType = maptApple;
								}
							}
							switch($intMapType) {
								case maptApple:
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nRetrieving GPS Data using Phantom for latitude $latitude, longitude $longitude");
									}
									//OMMC 2019-04-12: Modificada la instrucción del PhantomJS para que vuelva a resolver mapas
									$commandLine = 'phantomjs --web-security=no --ignore-ssl-errors=true --ssl-protocol=any mapkitGeocoding.js "-debug" "service='.$gbKPIFormsService.'" "timeout='.mapkitTimeout.'" "latitude='.$latitude.'" "longitude='.$latitude.'"';
									$output = shell_exec($commandLine);
									
									if ($output) {
										if ( stripos($output, "error") ) {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("Error retrieving GPS Data: " );
											}
										} else if ( stripos($output, "formattedAddress") ) {
											$arrGPSData = @getGPSDataFromAppleJson($strJSON);
											if (is_null($arrGPSData) || !is_array($arrGPSData)) {
												$arrGPSData = array();
											}
											
											if (is_array($arrGPSData)) {
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("<br>\r\nGPS Data: ");
													PrintMultiArray($arrGPSData);
												}
												@logeBavelString("GPS Data:");
												if (@$blnSaveeBavelLogStringFile) {
													$strLogText = (string) @var_export(@$arrGPSData, true);
													@logeBavelString($strLogText);
												}
											}
										}
									} else {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("Error retrieving GPS Data: ".(string) $output);
										}
									}
								break;
								case maptGoogle:
								default:
									//@JAPR 2018-03-02: Corregido un bug, faltaba el API Key de google en las llamadas para usar el API, así que excedía el límite de peticiones (#KRZ34J)
									//Se cambia el APIKey que era usada para móviles (googleAPIKey) por la versión para llamadas http (googleServerAPIKey), además necesita https
									//$strGPSURL = "http://maps.google.com/maps/api/geocode/json?sensor=false".googleAPIKey."&language=es&latlng=$latitude,$longitude";
									$strGPSURL = "https://maps.google.com/maps/api/geocode/json?sensor=false".googleServerAPIKey."&language=es&latlng=$latitude,$longitude";
									//@JAPR
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nRetrieving GPS Data for: $strGPSURL");
									}
									@logeBavelString("Retrieving GPS Data for: $strGPSURL");

									$ch = curl_init(); 
									if ($ch) {
										curl_setopt($ch, CURLOPT_URL, $strGPSURL); 
										curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
										curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
										$strJSON = curl_exec($ch);
										if ($strJSON) {
											curl_close($ch);
											
											//Convierte la respuesta a un objeto para manipular el resultado
											$arrGPSData = @getGPSDataFromJson($strJSON);
											if (is_null($arrGPSData) || !is_array($arrGPSData)) {
												$arrGPSData = array();
											}
											
											if (is_array($arrGPSData)) {
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("<br>\r\nGPS Data: ");
													PrintMultiArray($arrGPSData);
												}
												@logeBavelString("GPS Data:");
												if (@$blnSaveeBavelLogStringFile) {
													$strLogText = (string) @var_export(@$arrGPSData, true);
													@logeBavelString($strLogText);
												}
											}
										} else {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("<br>\r\nError retrieving GPS Data: ".(string) @curl_error($ch));
											}
											@logeBavelString("Error retrieving GPS Data: ".(string) @curl_error($ch));
											curl_close($ch);
										}
									}
								break;
							}
							//OMMC
	        			} else {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nNo GPS Data will be used");
							}
						}
	        			//@JAPR
	        			
	        			$anActionData = array();
	        			foreach ($arreBavelMappedIDs as $aneFormsFieldID => $aneBavelFieldID) {
        					$arreBavelFieldInfo = @$arreBavelFieldsColl[$aneBavelFieldID];
        					if (is_null($arreBavelFieldInfo) || !is_array($arreBavelFieldInfo)) {
        						continue;
        					}
        					
        					$inteBavelQTypeID = convertEBavelToEFormsFieldType($arreBavelFieldInfo['tagname']);
        					$streBavelFieldName = (string) @$arreBavelFieldInfo['label'];
        					$aData = null;
        					$blnSkipField = false;
	        				if (is_numeric($aneFormsFieldID)) {
	        					//Es una pregunta de la encuesta
	        					$objQuestion = @$arrQuestionsColl[$aneFormsFieldID];
	        					if (is_null($objQuestion)) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nQuestionID not found: $aneFormsFieldID");
									}
	        						continue;
	        					}
	        					
	        					$objSection = @$arrSectionsColl[$objQuestion->SectionID];
	        					if (is_null($objSection)) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nSectionID not found: {$objQuestion->SectionID}");
									}
	        						continue;
	        					}
	        					
	        					$objAnswers = @$arrAnswers[$objQuestion->QuestionID];
	        					if (is_null($objAnswers)) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNo answers for this question: $aneFormsFieldID");
									}
	        						continue;
	        					}
	        					
	        					//Validación temporal para procesar sólo el primer registro de las secciones dinámicas y maestro-detalle
	        					$objAnswer = null;
	        					if (isset($objAnswers[0])) {
	        						$objAnswer = @$objAnswers[0];
	        					}
	        					else {
	        						$objAnswer = $objAnswers;
	        					}
	        					
	        					if (is_null($objAnswer)) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("<br>\r\nNull answer for this question: $aneFormsFieldID");
										PrintMultiArray($objAnswers);
									}
	        						continue;
	        					}
	        					
        						switch ($objQuestion->QTypeID) {
        							case qtpMulti:
        								/*
										if (count($objQuestion->SelectOptions) > 0 && $objQuestion->MCInputType != mpcCheckBox) {
											$arrSelectOptions = array_combine(array_keys(array_flip($objQuestion->SelectOptions)), array_fill(0, count($objQuestion->SelectOptions), ''));
										}
										else {
											$arrSelectOptions = array();
										}
										*/
										$arrSelectOptions = array();
        								
        								$arrMulti = @$objAnswer['multiAnswer'];
										if (!is_null($arrMulti) && is_array($arrMulti)) {
											foreach ($arrMulti as $strOption => $strValue) {
												$strOptionDec = $strOption;
												$strValueDec = $strValue;
												if ($blnDecodeUTF8) {
													//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
													$strOptionDec = $strOptionDec;
													$strValueDec = $strValueDec;
												}
												$arrSelectOptions[] = $strOptionDec;
												/*
												if ($objQuestion->MCInputType == mpcCheckBox) {
													//Se agrega al array el elemento seleccionado
													$arrSelectOptions[] = $strOptionDec;
												}
												else {
													//En este caso se sobreescribe del array el elemento seleccionado
													//@JAPR 2013-01-23: Como los datos vienen codificados en utf8, se tienen que decodificar para comparar contra el array local
													//que no está codificado
													//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
													if (isset($arrSelectOptions[$strOptionDec])) {
														$arrSelectOptions[$strOptionDec] = $strValueDec;
													}
													//@JAPR
												}
												*/
											}
											if (count($arrSelectOptions) > 0) {
												switch ($inteBavelQTypeID) {
													case qtpMulti:
														//En este caso se espera un array de las opciones seleccionadas
														$aData = $arrSelectOptions;
														break;
														
													case qtpOpenString:
													case qtpOpenAlpha:
														//Se convierte a una lista de opciones separada por ","
														$aData = implode(',', $arrSelectOptions);
														break;
														
													default:
														//Tipo de conversión no soportado
														if (getParamValue('DebugBreak', 'both', '(int)', true)) {
															ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
															PrintMultiArray($objAnswers);
														}
														break;
												}
											}
										}
        								break;
        							
        							case qtpSingle:
        							//@JAPR 2013-05-06: Agregado el tipo de pregunta CallList
        							case qtpCallList:
        								$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpSingle:
													//En este caso se espera un array de las opciones seleccionadas
													$aData = array($aData);
													break;
													
												case qtpOpenString:
												case qtpOpenAlpha:
													//Se convierte a una lista de opciones separada por ","
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
        								break;
        							
									case qtpOpenDate:
										$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpOpenDate:
												case qtpOpenString:
												case qtpOpenAlpha:
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break;
										
									case qtpOpenTime:
										$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpOpenTime:
												case qtpOpenString:
												case qtpOpenAlpha:
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break; 
										
									case qtpOpenNumeric:
									case qtpCalc:
										$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpOpenNumeric:
												case qtpCalc:
													$aData = $objAnswer['answer'];
													break;
												
												case qtpOpenString:
												case qtpOpenAlpha:
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break;
										
									case qtpOpenString:
									case qtpOpenAlpha:
									case qtpAction:				//Las preguntas tipo acción se van a tratar como alfanuméricas por lo pronto
									//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
									case qtpBarCode:
										$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpOpenString:
												case qtpOpenAlpha:
												//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
												case qtpBarCode:
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break;

									//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
									//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
									case qtpMyLocation:
									case qtpGPS:
										$aData = (string) @$objAnswer['answer'];
										if (trim($aData) == '') {
											$aData = null;
										}
										else {
											switch ($inteBavelQTypeID) {
												case qtpOpenString:
												case qtpOpenAlpha:
												case qtpLocation:
													$aData = (string) @$objAnswer['answer'];
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break;
									//@JAPR
									case qtpSketch:		
									case qtpSignature:
										//@JAPRPendiente
											switch ($inteBavelQTypeID) {
												case qtpSketch:	
												case qtpSignature:
													break;
												
												case qtpOpenString:
												case qtpOpenAlpha:
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										break;
										
									case qtpDocument:
									case qtpAudio:
									case qtpVideo:
										//Conchita 20140924 agregados audio y video
										//@JAPRPendiente
											switch ($inteBavelQTypeID) {
												case qtpDocument:
												case qtpAudio:
												case qtpVideo:
													break;
												
												case qtpOpenString:
												case qtpOpenAlpha:
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										break;
										
									case qtpPhoto:
										$aData = (string) @$objAnswer['photo'];
										$blnIsPhotoName = true;
										if (trim($aData) == '' || $streBavelPath == '') {
											$aData = null;
										}
										else {
											$insertImage = $aData;
											//Genera el nombre de la ruta de la imagen según eBavel
											$manager = new Tables();
										    @$manager->notifyErrorService = true;
										    $manager->applicationCodeName = $eBavelAppCodeName;
										    $manager->init($theRepository->ADOConnection);
											$params = array();
											$params['sectionname'] = $eBavelFormTable;
											$params['widgettype'] = 'widget_photo';
											$params['ext'] = 'jpg';
											$aData = $manager->getFilePath($params, null);
											$strCompletePath = $streBavelPath.$aData;
											if ($inteBavelQTypeID == qtpPhoto) {
												//Sólo si el campo de eBavel es un tipo foto es como se copia la imagen
												if ($usePhotoPaths && $blnIsPhotoName) {
													//Realiza la copia del archivo de imagen
													//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
													if ($blnWebMode) {
														$path = getcwd()."\\";
													}
													else {
														//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
														$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
													}
													$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertImage);
													$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
													if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
														$strCompleteSourcePath .= '.jpg';
													}
													$status = @copy($strCompleteSourcePath, $strCompletePath);
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nSaving eBavel photo from $strCompleteSourcePath to $strCompletePath: $status");
													}
												}
												else {
													$aData = null;
													//Esta funcionalidad es exclusiva de v4, así que no se soportará el viejo formato para este proceso
													/*
											        if($insertImage == "photoimage") {
											            $tmpfilename = $_FILES['photoimage'.$aQuestion->QuestionID]['tmp_name'];
											            $status = move_uploaded_file($tmpfilename, $strCompletePath);
											        } else {
														$status = file_put_contents ($strCompletePath, $insertImage);
											        }
											        */
												}
											}
											
											switch ($inteBavelQTypeID) {
												case qtpPhoto:
												case qtpOpenString:
												case qtpOpenAlpha:
													//En los 3 tipos se graba el dato como la ruta de la imagen
													break;
													
												default:
													//Tipo de conversión no soportado
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("<br>\r\nType mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
														PrintMultiArray($objAnswers);
													}
													$aData = null;
													break;
											}
										}
										break;
									
        							default:
        								//Tipos de preguntas no soportados no grabarán datos
        								$blnSkipField = true;
        								break;
        						}
	        				}
	        				else {
	        					//Es un campo fijo de datos de la encuesta
	        					switch (BITAMSurveyFieldMap::GetSurveyColumnID($aneFormsFieldID)) {
									case sfidStartDate:
										$aData = substr($arrayData["surveyDate"], 0, 10);
										break;
									case sfidStartHour:
										$aData = $arrayData["surveyHour"];
										break;
									case sfidEndDate:
										$aData = substr($thisDate, 0, 10);
										break;
									case sfidEndHour:
										$aData = substr($thisDate, 11);
										break;
									case sfidLocation:
										$blnSkipField = true;
										if(isset($arrayData['surveyLatitude'])) 
										{
											$latitude = (double)$arrayData['surveyLatitude'];
										}
										else 
										{
											$latitude = 0;
										}
										
										if(isset($arrayData['surveyLongitude'])) 
										{
											$longitude = (double)$arrayData['surveyLongitude'];
										} 
										else 
										{
											$longitude = 0;
										}
										
										if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
											$strMessageQuestion = trim((string) @$arrayData['qfield32']);
											if ($strMessageQuestion != '') {
												$arrData = explode(',', $strMessageQuestion);
												if (count($arrData) == 2) {
								        			$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
								        			$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
								        			if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
								        				$latitude = $strLatitude;
								        				$longitude = $strLongitude;
								        			}
												}
											}
										}
										
										$anActionData[$arreBavelFieldInfo['id']] = $latitude.', '.$longitude;
										break;
									//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
									case sfidLocationAcc:
										if(isset($arrayData['surveyAccuracy'])) 
										{
											$accuracy = (double)$arrayData['surveyAccuracy'];
										}
										else 
										{
											$accuracy = 0;
										}
										$aData = $accuracy;
									case sfidSyncDate:
										$aData = $syncDate;
										break;
									case sfidSyncHour:
										$aData = $syncTime;
										break;
									case sfidServerStartDate:
										if (isset($arrayData["serverSurveyDate"])) {
											$aData = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
										}
										break;
									case sfidServerStartHour:
										if (isset($arrayData["serverSurveyHour"])) {
											$aData = (string) @$arrayData["serverSurveyHour"];
										}
										break;
									case sfidServerEndDate:
										if (isset($arrayData["serverSurveyEndDate"])) {
											$aData = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
										}
										break;
									case sfidServerEndHour:
										if (isset($arrayData["serverSurveyEndHour"])) {
											$aData = (string) @$arrayData["serverSurveyEndHour"];
										}
										break;
									case sfidScheduler:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidEMail:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidDuration:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidAnsweredQuestions:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidEntrySection:
										$aData = 0;	//@JAPRPendiente
										break;
									case sfidDynamicPage:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidDynamicOption:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidFactKey:
										$aData = null;	//@JAPRPendiente
										break;
									case sfidEntryID:
										$aData = (int) $factKeyDimVal;
										break;
									//@JAPR 2013-05-03: Agregados campos extras al mapeo de datos general de la encuesta
									case sfidUserID:
										$aData = (int) @$theUser->UserID;
										break;
									//@JAPR 2013-05-06: Agregados campos extras al mapeo de datos general de la encuesta
									case sfidUserName:
										$aData = (string) @$theAppUser->UserName;
										break;
									//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
									case sfidGPSCountry:
										$aData = (string) @$arrGPSData['Country'];
										break;
									case sfidGPSState:
										$aData = (string) @$arrGPSData['State'];
										break;
									case sfidGPSCity:
										$aData = (string) @$arrGPSData['City'];
										break;
									case sfidGPSZipCode:
										$aData = (string) @$arrGPSData['ZipCode'];
										break;
									case sfidGPSAddress:
										$aData = (string) @$arrGPSData['Address'];
										break;
									case sfidGPSFullAddress:
										$aData = (string) @$arrGPSData['FullAddress'];
										break;
	        						default:
	        							//Campos no conocidos regresarán null para que no se consideren
	        							break;
	        					}
	        				}
	        				
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\nData for field id $aneFormsFieldID: $aData");
							}
	        				//Solo inserta el dato si no es null el valor o si se saltó por requerir trato especial, en cuyo caso en el switch
	        				//se habría insertado
	        				if (!$blnSkipField && !is_null($aData)) {
	        					$anActionData[$arreBavelFieldInfo['id']] = $aData;
	        				}
	        			}
						
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\neBavel path: $strPath");
							ECHOString("<br>\r\neBavel app: $eBavelAppCodeName");
							ECHOString("<br>\r\neBavel table: $eBavelFormTable");
						}
	        			
						//Inicializa datos fijos adicionales
						$anActionData["id_".$eBavelFormTable] = -1;
						
						//Invoca al grabado de eBavel
					    $manager = new Tables();
					    //@JAPR 2013-05-03: Agregado el control de errores con eBavel para evitar que interrumpa la ejecución del script y
					    //simplemente lance una excepción que puede ser atrapada por eForms
					    @$manager->notifyErrorService = true;
					    //@JAPR
					    $manager->applicationCodeName = $eBavelAppCodeName;
					    $manager->init($theRepository->ADOConnection);
		        		$insertData = array();
		        		$insertData[-1] = $anActionData;
		        		
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\neBavel insert data in table: $eBavelFormTable");
							PrintMultiArray($insertData);
						}
						
						//@JAPR 2014-10-14: Agregada la edición de datos en eBavel
						//@JAPR 2014-11-28: Agregado el parámetro para permitir que los campos de eBavel que utilizan catálogo
						//puedan recibir siempre las descripciones y así no se confunda pensando que lo que recibe es el key
						$arrParams = array('alldescription' => true);
						@$manager->insertData($eBavelFormTable, $insertData, array(), $theRepository->ADOConnection, 'overwrite', $arrParams);
	        		} catch (Exception $e) {
	        			//Por el momento no reportará este error
	        			$streBavelException = (string) @$e->getMessage();
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\neBavel exception saving form data: ".$streBavelException);
						}
						$thelog = "[".$friendlyDate."] [Action: eBavel form data saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $streBavelException . " POST = " . print_r($_POST,true);
						$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
						@error_log($thelog, 3, $thefile);
						
						$stat_action = "[".$friendlyDate."] [Action: eBavel form data saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
						@error_log($stat_action, 3, $statlogfile);
	        		}
				   	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("<br>\r\nFinishing eBavel data saving process<br>\r\n");
				}
		        //@JAPR
			} else {
				$strStatus="OK";
			}
			//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
			//varios requests al mismo tiempo
			if (!$blnIsAgent) {
				@flock($handle, LOCK_UN);
			}
			//@JAPR
        }
		//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
		//varios requests al mismo tiempo
		if (!$blnIsAgent) {
        	@fclose($handle);
		}
		//@JAPR
	} else {
		$strStatus = translate("Error saving answers.")." ".translate("The writing permissions must be assigned in the log directory for the IIS user.")." ".translate("Please contact your System Administrator");
	}
	
	//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
	//Almacena si realmente hubo o no error, ya que cuando las capturas son desde móviles siempre se regresa que todo estuvo Ok, sin embargo cierta
	//funcionalidad reciente debe realmente regresar un código de error al móvil para impedir continuar
	$blnSavingError = false;
	//@JAPR
    if ($strStatus == "OK") {
        $strStatus = "Survey data was saved successfully.";
		
		//guardamos en el log
		$stat_action = "[".$friendlyDate."] [Action: Upload] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
		@error_log($stat_action, 3, $statlogfile);

		//@AAL 17/06/2015 Agregado para actualizar el campo Status el cual indica si se grabaron los Datos capturados de la encuesta (0 => No grabado, 1 => grabado)
		$UserID = $theRepository->DataADOConnection->Quote(getParamValue('UserID', 'both', "(string)"));
		$sql = "UPDATE SI_SV_SurveyLog SET Status = 1 WHERE SurveyID = {$surveyID} AND UserID = {$UserID} AND SurveyDate = '{$lastDate}'";
		$aRS = $theRepository->DataADOConnection->Execute($sql);
		/*if ($aRS === false)
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyLog ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		*/
    } else {
		//Guardamos en el log de errores
		$thelog = "[".$friendlyDate."] [Action: Upload error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $strStatus . " POST = " . print_r($_POST,true);
		$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
		@error_log($thelog, 3, $thefile);
        
		//guardamos en el log ($statlogfile = "./uploaderror/eforms.log";)
		$stat_action = "[".$friendlyDate."] [Action: Upload error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
		@error_log($stat_action, 3, $statlogfile);
		
		//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		//Si el proceso de grabado no debe regresar el error al App, entonces se debe respaldar la información para que posteriormente sea corregida
		//mediante el proceso de ReUpload, para esto se empieza copiando el archivo log que contiene los datos del POST y posteriormente
		//identificará el resto de los archivos de la sincronización en base al outbox recibido
		if (!$dieOnError) {
			//Respalda también el archivo de log
			$strSrcLogFile = getcwd()."\\"."uploaderror\\surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
			$strTrgLogFile = getcwd()."\\"."syncerrors\\{$theRepository->RepositoryName}\\surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
			@copy($strSrcLogFile, $strTrgLogFile);
			
			$dieOnError = !((bool) @backUpSyncOutboxData());
			
			//@JAPR 2014-02-20: Agregado el reporte de error incluso para las capturas Web
			//No se removerá esté código, porque cuando es captura móvil se debe verificar que todo se respalde correctamente, por eso se vuelve a
			//validar contra la variable $dieOnError antes de determinar si va o no a continuar con el grabado
			//Envía la notificación del error vía EMail para que empiece a ser atendido, este proceso no debe generar otro error, así que se
			//atrapa cualquier posible error
			if (!$dieOnError) {
				try {
					@reportSurveyEntryError($theRepository, $strStatus, $theUser);
				}
				catch (Exception $e) {
					//No reporta mas errores, continua para indicar al App que todo estuvo ok pues para este momento ya respaldó los archivos
					//con los que se puede restaurar la captura
				}
			}
		}
		else {
			//@JAPR 2014-02-20: Agregado el reporte de error incluso para las capturas Web
			//Agregado este envió en forma separada, porque si entra aquí es seguro que es captura Web, así que ahora se notificarán también esos
			//errores vía EMail
			try {
				@reportSurveyEntryError($theRepository, $strStatus, $theUser);
			}
			catch (Exception $e) {
				//No reporta mas errores, continua para indicar al App que todo estuvo ok pues para este momento ya respaldó los archivos
				//con los que se puede restaurar la captura
			}
		}
		//JAPR
		
		//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
		if ($dieOnError) {
			$strStatus = "There was an error while saving the outbox, if you do not found it on the reports page, please contact your System Administrator. ".$strStatus;
		}
		else {
			$strStatus = "Survey data was saved successfully.";
		}
		//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
		//Almacena si realmente hubo o no error, ya que cuando las capturas son desde móviles siempre se regresa que todo estuvo Ok, sin embargo cierta
		//funcionalidad reciente debe realmente regresar un código de error al móvil para impedir continuar
		$blnSavingError = true;
    }
	
	//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
	//Verifica si hay un archivo para procesar el grabado de datos habilitado, si lo hay, prepara una perición con el array de respuestas (tal como
	//llegaron desde la captura) para que dicho servicio se encargue de realizar su proceso que es independiente al grabado de eForms
	//Hay un conjunto de parámetros fijos que se mandan (todos ellos como POST), entre ellos:
	//UserID: La cuenta del usuario que realizó la captura
	//SurveyDate: La fecha y hora de la captura
	//SurveyID: El ID de la encuesta capturada
	//Data: El conjunto de respuestas indexadas por el ID de la pregunta (el array como viene del archivo .dat + la propiedad Type agregada abajo)
	require_once('settingsvariable.inc.php');
	//@JAPR 2014-07-09: Corregido un bug, la referencia a la configuración estaba equivocada, así que nunca realizaba este proceso
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($theRepository, 'CUSTOMIZEDSTORESURVEYSERVICE');
	//@JAPR 2014-11-12: Corregido un bug no reelevante, cuando se intentaba hacer el grabado de una captura que ya se encontraba en el server,
	//intentaba usar la variable $factKeyDimVal la cual nunca se asignaba pues para ello tiene que hacer un grabado y en esos casos se omite
	//dicho proceso por ya encontrarse la captura previamente
	if (!$blnSavingError && isset($factKeyDimVal) && $factKeyDimVal > 0 && !is_null($objSetting) && trim($objSetting->SettingValue) != '') {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\nCustomized survey data saving service enabled");
		}
		
		//Realiza la invocación al grabado. Si hay alguna respuesta, se asume que es un error así que debe hacer rollback de la captura eliminando
		//el reporte que acaba de ser generado (aunque para este momento, ya se envió el reporte PDF vía EMail)
		try {
			//Agrega el ID de captura por si se utiliza para generar el ID de reporte
			$_POST['factKeyDimVal'] = $factKeyDimVal;
			$blnSavingError = trim((string) @saveAnswersCustomized($theRepository));
			if ($blnSavingError != '') {
				$strStatus = $blnSavingError." ({$objSetting->SettingValue})";
			}
			$blnSavingError = ($blnSavingError != '');
		} catch (Exception $e) {
			$blnSavingError = true;
			$strStatus = $e->getMessage()." ({$objSetting->SettingValue})";
		}
		
		//Si hubo un error durante la captura en este proceso, debe eliminar la captura y notificar al dispositivo que la envió (esto NO está
		//preparado para funcionar con el Agente de eForms, ya que el agente no permite retroalimentación al dispositivo de captura)
		if ($blnSavingError) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString ("<br>\r\n<br>\r\n Customized survey saving process finished with errors: {$strStatus}<br>\r\n");
			}
			
			//Primero obtiene el FactKey correspondiente con el registro de la sección estándar de la encuesta
			$intFactKey = 0;
			$sql = "SELECT A.FactKey 
				FROM {$surveyInstance->SurveyTable} A 
				WHERE A.FactKeyDimVal = {$factKeyDimVal} AND A.EntrySectionID = 0";
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$intFactKey = (int) @$aRS->fields["factkey"];
			}
			
			if ($intFactKey > 0) {
				require_once("report.inc.php");
				$anInstance = BITAMReport::NewInstanceWithID($theRepository, (int) $intFactKey, $surveyID);
				if (!is_null($anInstance)) {
					$anInstance->remove();
				}
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString ("<br>\r\n<br>\r\n Customized survey saving process finished without errors<br>\r\n");
			}
		}
	}
	//@JAPR
    
    @header("Content-Type: text/plain");
    echo($strStatus);
    
	//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("<br>\r\n<br>\r\ngbEnableIncrementalAggregations == {$gbEnableIncrementalAggregations}, gbTestIncrementalAggregationsData == {$gbTestIncrementalAggregationsData}");
	}
    if (!$gbEnableIncrementalAggregations && !$gbTestIncrementalAggregationsData) {
    	//Si no se van ni a grabar ni a probar agregaciones incrementales, entonces si termina la ejecución en este punto, de lo contrario
    	//regresa al método StoreSurveyCapture para procesar a los agregados incrementales
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\n<br>\r\nExit before finishing");
		}
		
		//@JAPR 2015-04-02: Corregido un bug, no estaba registrando la salida de las cargas del agente en el monitor de operaciones cuando
		//se tenían las agregaciones desactivadas
		if (@$gbUseOperationsMonitor) {
			//$arrEventParams["DBServer"] = $strDBServer;
			//$arrEventParams["Repository"] = $theRepositoryName;
			//$arrEventParams["DBid"] = $intFormsDatabaseID;
			//$arrEventParams["UserID"] = $theUser->UserID;
			$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
			$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
			@RegisterOperation($arrEventParams, @$giOperationID);
		}
		//@JAPR
		
    	exit();
    }
    //@JAPR
}

//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
//Verifica si hay un archivo para procesar el grabado de datos habilitado, si lo hay, prepara una perición con el array de respuestas (tal como
//llegaron desde la captura) para que dicho servicio se encargue de realizar su proceso que es independiente al grabado de eForms
//Hay un conjunto de parámetros fijos que se mandan (todos ellos como POST), entre ellos:
//UserID: La cuenta del usuario que realizó la captura
//SurveyDate: La fecha y hora de la captura
//SurveyID: El ID de la encuesta capturada
//Data: El conjunto de respuestas indexadas por el ID de la pregunta (el array como viene del archivo .dat + la propiedad Type agregada abajo)
function saveAnswersCustomized($aRepository) {
	global $jsonData;
	
	require_once('settingsvariable.inc.php');
	//@JAPR 2014-07-09: Corregido un bug, la referencia a la configuración estaba equivocada, así que nunca realizaba este proceso
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'CUSTOMIZEDSTORESURVEYSERVICE');
	if (is_null($objSetting) || trim($objSetting->SettingValue) == '') {
		return '';
	}
	$strCustomizedService = trim($objSetting->SettingValue);
	
    $surveyID = 0;
    if (array_key_exists("surveyID", $_POST)) {
        $surveyID = (int) @$_POST["surveyID"];
    }
    if ($surveyID <= 0) {
    	return '';
    }
	
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	$arrAnswers = @$jsonData['answers'];
	foreach ($questionCollection->Collection as $aQuestion) {
		$intQuestionID = $aQuestion->QuestionID;
		
		if (!isset($arrAnswers[$intQuestionID])) {
			continue;
		}
		
		$arrQAnswers = $arrAnswers[$intQuestionID];
		if (is_array($arrQAnswers) && !isset($arrQAnswers['id'])) {
			//Si es una pregunta de sección dinámica o múltiple, a cada instancia de registro se le complementa la información
			foreach ($arrQAnswers as $intRecNum => $arrRecData) {
				$arrAnswers[$intQuestionID][$intRecNum]['type'] = $aQuestion->QTypeID;
			}
		}
		else {
			//Es una pregunta de sección estándar, directamente se complementa la información
			$arrAnswers[$intQuestionID]['type'] = $aQuestion->QTypeID;
		}
	}
	
	$strStatus = '<BR>\r\nThere was an error while sending the survey data to the customized user service. ';
	$intConnectionTimeOut = 0;
	$intRequestTimeOut = 600;
	$timeStamp = date('YmdHis');
	$strURL = $strCustomizedService;
	try {
		$ch = curl_init();
		if ($ch === false) {
			return $strStatus."There was an error starting the curl session";
		}
		
		$_POST['answers'] = $arrAnswers;
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
		curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
		curl_setopt($ch, CURLOPT_URL, $strURL);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($_POST));
		$strResult = curl_exec($ch);
		if ($strResult === false) {
			$strStatus .= curl_error($ch);
		}
		else {
			//Si no hubo error en el request, verifica si hubo error durante el grabado
			$strStatus = trim($strResult);
		}
		curl_close($ch);
		
	} catch (Exception $e) {
		$strStatus .= "(Exception): ".$e->getMessage();
	}
	
	return $strStatus;
}

//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
/* Realiza el respaldo del archivo de Outbox actualmente procesado así como cualquier otro archivo que se pudiera necesitar, esto basado en que
todos los archivos de este Outbox deberían seguir el mismo patrón de captura ya que este proceso sólo se debe aplicar a capturas desde móvil, donde
si se sigue dicho patrón, esto es vital para no omitir ningún archivo.
Si por alguna razón el proceso NO pudiera respaldar algo, o se detecta algún problema, entonces se debe desactivar la posibilidad de recuperación
manual de la información a partir de archivos y reportar el error directamente al móvil, ya que de no ser así entonces simplemente se perdería
la información, por eso la función sólo regresará True si pudo respaldar todo lo que considera que se necesita para grabar correctamente la captura,
y dicho valor retornado debe sobreescribir a la variable que controla si se debe o no reportar el error en el App
*/
function backUpSyncOutboxData() {
	global $blnWebMode;
	global $gstrOutboxFilePath;
	global $gstrOutboxFileName;
	
	//En modo Web no se necesita el restore de capturas
	if ($blnWebMode) {
		return false;
	}
	
	//Verifica que estén asignados datos requeridos para el proceso
	$strUserName = (string) @$_POST['UserID'];
	$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
	if (trim($strUserName) == '' || trim($strRepositoryName) == '') {
		return false;
	}
	
	//Primero limpia el caché de archivos para que no ocurran errores de identificación o cosas así
	clearstatcache();
	
	//Si el archivo de outbox no está asignado, no puede continuar
	if (trim($gstrOutboxFilePath) == '' || trim($gstrOutboxFileName) == '') {
		return false;
	}
	
	//Si no es un directorio no puede continuar
	$strRecoveryPath = getcwd()."\\"."syncerrors\\".trim($strRepositoryName);
	if (!is_dir($gstrOutboxFilePath) || !is_dir($strRecoveryPath)) {
		return false;
	}
	
	//Verifica que el archivo origen aun exista
	$strCompleteSourcePath = $gstrOutboxFilePath."\\".$gstrOutboxFileName;
	if (!file_exists($strCompleteSourcePath)) {
		return false;
	}
	$strCompletePath = $strRecoveryPath."\\".$gstrOutboxFileName;
	
	//Copia el archivo de outbox a la nueva ruta
	$status = @copy($strCompleteSourcePath, $strCompletePath);
	if ($status === false) {
		return false;
	}
	
	//Identifica el patrón a buscar en los nombres de archivo para respaldar todos los de esta captura
	$arrFileData = unformatFileName($gstrOutboxFileName, $strUserName);
	if ($arrFileData === false || !is_array($arrFileData) || count($arrFileData) == 0) {
		return false;
	}
	
	$intSurveyID = (int) @$arrFileData['surveyID'];
	$strSurveyDate = (string) @$arrFileData['date'];
	$strSurveyHour = (string) @$arrFileData['hour'];
	if ($intSurveyID <= 0 || trim($strSurveyDate) == '' || trim($strSurveyHour) == '') {
		return false;
	}
	
	$strPattern = $strUserName.'_'.$strSurveyDate.'_'.$strSurveyHour.'_'.$intSurveyID;
	//Identifica los archivos de esta captura y los copia a la ruta de recuperación de errores
	if ($objDirHandler = @opendir($gstrOutboxFilePath)) {
		while (($file = @readdir($objDirHandler)) !== false) {
			if ($file != "." && $file != "..") {
				$intPos = strpos($file, $strPattern);
				if ($intPos !== false) {
					$strCompleteSourcePath = $gstrOutboxFilePath."\\".$file;
					$strCompletePath = $strRecoveryPath."\\".$file;
					$status = @copy($strCompleteSourcePath, $strCompletePath);
					if ($status === false) {
						@closedir($objDirHandler);
						return false;
					}
				}
			}
		}
		@closedir($objDirHandler);
	}
	
	//Si llega hasta aquí significa que todos los archivos fueron respaldados correctamente
	return true;
}
//@JAPR

//@JAPR 2012-05-23: Agregado el log de proceso de los móviles a una tabla
//Graba el contenido del array en la tabla 
function insertAppLogLine($aRepository, $arrLogEntry)
{
	if (is_null($arrLogEntry) || !is_array($arrLogEntry) || count($arrLogEntry) == '')
	{
		return '';
	}
	
	$arrFields = array('user' => 'UserID', 'Date' => 'EventDateID', 'SrvDate' => 'UploadDateID', 'Zone' => 'GMTZone', 'Lat' => 'Latitude', 
			'Long' => 'Longitude', 'DevName' => 'DeviceName', 'Plat' => 'PlataformName', 'OS' => 'OSVersion', 'AppsNo' => 'AppsRunnning', 
			'Mem' => 'UsedMemory', 'Act' => 'EventName', 'survey' => 'SurveyID', 'agenda' => 'AgendaID', 
			'SesID' => 'AppSessionID', 'Evt' => 'EventID', 'Step' => 'EventStepID', 'Connection' => 'ConnectionType');
	$arrDataType = array('user' => dtpNumeric, 'Date' => dtpDate, 'SrvDate' => dtpDate, 'Zone' => dtpNumeric, 'Lat' => dtpNumeric, 
			'Long' => dtpNumeric, 'DevName' => dtpString, 'Plat' => dtpString, 'OS' => dtpString, 'AppsNo' => dtpNumeric, 
			'Mem' => dtpNumeric, 'Act' => dtpString, 'survey' => dtpNumeric, 'agenda' => dtpNumeric,
			'SesID' => dtpString, 'Evt' => dtpNumeric, 'Step' => dtpNumeric, 'Connection' => dtpString);
	$arrValues = array('user' => $_SESSION["PABITAM_UserID"], 'Date' => date("Y-m-d H:i:s"), 'SrvDate' => date("Y-m-d H:i:s"), 'Zone' => -6, 'Lat' => 0, 
			'Long' => 0, 'DevName' => '', 'Plat' => '', 'OS' => '', 'AppsNo' => -1, 
			'Mem' => -1, 'Act' => '', 'survey' => 0, 'agenda' => 0,
			'SesID' => '', 'Evt' => -1, 'Step' => -1, 'Connection' => '');
	$strExtraData = '';
	foreach ($arrLogEntry as $strEntry)
	{
		$arrData = explode('=', $strEntry, 2);
		if (count($arrData) == 2)
		{
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$strDataID = $arrData[0];
			$strValue = $arrData[1];
			$strFieldName = @$arrFields[$strDataID];
			if (!is_null($strFieldName))
			{
				switch (strtolower($strDataID))
				{
					case 'zone':
						$arrValues[$strDataID] = (int) substr($strValue, 3);
						break;
					default:
						$arrValues[$strDataID] = $strValue;
						break;
				}
			}
			else 
			{
				//Cualquier campo adicional se grabará al final como parte de la acción, siempre y cuando no sobrepase de 255 caracteres
				//esto por si alguna versión posterior o de debug está enviando mas elementos del log que pudieran apoyar
				$strExtraData .= ','.$strDataID.'='.$strValue;
			}
		}
	}
	
	if (trim($strExtraData != '') && strlen($arrValues['Act'] < 255))
	{
		$arrValues['Act'] .= substr($strExtraData, 0, 255 - strlen($arrValues['Act']) -1);
	}
	
	//Si no se registró un texto para la acción, simplemente sale sin registrar nada en el log
	if (trim($arrValues['Act']) == '')
	{
		return '';
	}
	
	//Genera el estatuto SQL considerando los tipos de dato de cada columna
	$sqlFields = '';
	$sqlValues = '';
	foreach ($arrFields as $strDataID => $strFieldName)
	{
		$sqlFields .= ','.$strFieldName;
		switch ($arrDataType[$strDataID])
		{
			case dtpDate:
				if (trim($arrValues[$strDataID]) == '')
				{
					$arrValues[$strDataID] = date("Y-m-d H:i:s");
				}
				$sqlValues .= ','.$aRepository->DataADOConnection->DBTimeStamp($arrValues[$strDataID]);
				break;
			
			case dtpString:
				$sqlValues .= ','.$aRepository->DataADOConnection->Quote($arrValues[$strDataID]);
				break;
			
			default:
				//@JAPR 2012-07-11: Corregido un bug, para todos los tipos numéricos estaba haciendo una conversión a entero
				$arrValues[$strDataID] = trim($arrValues[$strDataID]);
				//@JAPR 2012-07-24: Corregido un bug, si el valor numérico viene vacio lo deja como 0
				if ($arrValues[$strDataID] == '' || !is_numeric($arrValues[$strDataID]))
				{
					$arrValues[$strDataID] = '0';
				}
				//@JAPR
				$sqlValues .= ','.$arrValues[$strDataID];
				break;
		}
	}
	
	$sqlFields = substr($sqlFields, 1);
	$sqlValues = substr($sqlValues, 1);
	global $saveDataForm;
	if($saveDataForm)
	{
	$sql = 'INSERT INTO SI_SV_AppLog ('.$sqlFields.') VALUES ('.$sqlValues.')';
    $aRS = $aRepository->DataADOConnection->Execute($sql);
    if (!$aRS) {
    	return translate("Error accessing") . " SI_SV_AppLog " . translate("table") . ": " . $aRepository->DataADOConnection->ErrorMsg() . ". " . translate("Executing") . ": " . $sql;
    }
    }
    return '';
}

//@JAPR 2013-02-28: Agregada la notificación vía eMail de errores de captura
/* Envía un eMail al personal especializado en resolver errores de captura de eForms cuando el servicio recibe una solicitud pero por alguna
causa no puede hacer el grabado correcto. Adicionalmente se enviará un eMail al sistema de soporte, aunque en ese caso el EMail que se envía
sólo ocurre una vez por día para no saturar de tickets de soporte al sistema
*/
function reportSurveyEntryError($aRepository, $strEntryError, $theUser = null) {
	global $gstrOutboxFileName;
	global $appVersion;
    global $server;
    global $server_user;
    global $server_pwd;
    global $masterdbname;
    
    @require_once('../../../fbm/conns.inc.php');
	@require_once("bpaemailconfiguration.inc.php");
	@require_once('survey.inc.php');
	@require_once('surveyAgenda.inc.php');
	
	$anInstanceEmailConf = @BPAEmailConfiguration::readConfiguration();
	if(is_null($anInstanceEmailConf)) {
		return false;
	}
	
	$SMTPServer = (string) @$anInstanceEmailConf->fbm_email_server;	
	$SMTPPort = (string) @$anInstanceEmailConf->fbm_email_port;
	$SMTPUserName = (string) @$anInstanceEmailConf->fbm_email_username;
	$SMTPPassword = (string) @$anInstanceEmailConf->fbm_email_pwd;
	$SMTPEmailSource = (string) @$anInstanceEmailConf->fbm_email_source;
	if (strlen($SMTPServer) == 0 && strlen($SMTPPort) == 0 && strlen($SMTPUserName) == 0) {
		return false;
	}
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if (!class_exists("PHPMailer")) {
		@require_once("mail/class.phpmailer.php");
		
		if (!class_exists("PHPMailer")) {
			return false;
		}
	}
	//@JAPR
	
	//Indentifica las cuentas a las que enviará los EMails basado en el tipo de cuenta maestra de la que se trata (si ocurre algún error, se
	//asumirá que se debe notificar a soporte interno por lo menos para que se verifique por qué sucede este error)
	$blnSendToSupport = false;
	$blnSendToQA = false;
	$blnSendToMasterAcc = false;
	$blnSendToBitamSupport = false;
	$blnSendToUserAcc = false;
	$blnSendToKPIAdmin = false;
	$blnSendToDevelopMent = true;
	//@JAPR 2015-12-10: Corregido un bug, esta es una variable global, así que no se debe asignar aquí sino hacer referencia a ella nada mas
	global $blnTestingServer;
	//$blnTestingServer = false;
	//@JAPR
	
	//Null significa que no se pudo determinar el CustomerType, por lo tanto se asumirá que se debe notificar solo a desarrollo para que verifique
	//por qué no se pudo determinar. Es diferente a que la tabla tenga un Null en cuyo caso se va a asumir que es Free Trial
	$intCustomerType = null;
	/* Customer Types:
		-9	- NA 
		1 	- Customer 
		2	- Free Trial 
		3	- Internal 
		4	- Partner 
		5	- POC 
		6	- Partner Prospect 
		7	- Free Trial Contactado 
		8	- Customer Dead 
		9	- Partner Dead 
		10	- Delete 
		11	- Demo 
		12	- Testing
	*/
	
	$strMasterAccount = '';
	$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
	//@JAPR 2018-05-10: Optimizada la carga de eForms (#G87C4W)
	//Reutilizada la conexión al metadata de KPIOnline mediante cache
	$mysql_Connection = GetKPIRepositoryConnection();
	//@JAPR
	$sql = '';
	if ($mysql_Connection) {
		//@JAPR 2018-05-10: Optimizada la carga de eForms (#G87C4W)
		$sql = "SELECT b.EMail, a.CustomerType 
			FROM saas_databases a, saas_users b 
			WHERE a.UserID = b.UserID AND a.Repository = '".(@mysql_real_escape_string($strRepositoryName, $mysql_Connection))."'";
		$result = @mysql_query($sql, $mysql_Connection);
		//@JAPR 2015-05-04: Agregado soporte para php 5.6
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$strMasterAccount = (string) @$row["EMail"];
			$intCustomerType = @$row["CustomerType"];
			if (is_null($intCustomerType)) {
				//Se tiene que revisar por qué del error, así que envía a desarrollo
				$intCustomerType = cmrtFreeTrial;
			}
			else {
				//Se convierte a numérico y se desactiva el envío a desarrollo
				$intCustomerType = (int) $intCustomerType;
				$blnSendToDevelopMent = false;
			}
		}
		//@JAPR
	}
    
    //Según el tipo de Customer habilita el envío de EMail a diferentes cuentas
    switch ($intCustomerType) {
    	case cmrtInternal:
    		//Las cuentas Internal deben ser dirigidas al responsable de la cuenta que generalmente es el Administrador y a soporte de Bitam, 
    		//ya que no son tan prioritarias como los clientes así que no generarán ticket de KPIOnline (ellos lo podrían generar automáticamente, 
    		//pero no se considera prioritario como para hacerlo automático)
    		$blnSendToMasterAcc = true;
    		$blnSendToBitamSupport = true;
    		break;
		
    	case cmrtCustomerDead:
    	case cmrtPartnerDead:
    		//Las cuentas de Customer o Partner Dead teoricamente ya no importan, sin embargo se notificará a los encargados de KPIOnline por si
    		//se tratara de algún tipo de error ya que continua recibiendo peticiones de eForms y tal vez se necesite cambiar el Status o darla
    		//de baja en definitiva
    		$blnSendToKPIAdmin = true;
    		break;	
    		
    	case cmrtDelete:
    		//Las cuentas eliminadas no tienen por qué reportar, así que termina el proceso
    		return false;
    		break;
    		
    	case cmrtTesting:
    		//Las cuentas de testing se deben notificar exclusivamente al equipo de Control de Calidad. Generalmente la cuenta Administradora no
    		//importa, pero si la cuenta específica del tester
    		$blnSendToQA = true;
    		$blnSendToUserAcc = true;
    		break;
    		
    	//Se asumirá cualquier tipo no identificado como si fuera de máxima prioridad, esto para no cometer errores e ignorar a cuentas importantes
    	//que pudieran estar estrenando un CustomerType recientemente definido. Estas cuentas no notifican al usuario que captura, solo al
    	//responsable que es la cuenta Administradora (Generan un ticket de KPIOnline automáticamente)
    	default:
			$blnSendToSupport = true;
			$blnSendToMasterAcc = true;
    		break;
    }
    
    if ($blnTestingServer) {
		//$blnSendToSupport = false;
		$blnSendToQA = true;
		//$blnSendToMasterAcc = false;
		//$blnSendToBitamSupport = false;
		//$blnSendToUserAcc = false;
		//$blnSendToDevelopMent = false;
		//$blnSendToKPIAdmin = false;
		//$blnSendToDevelopMent = false;
    }
    
	//Obtiene los datos específicos del error. Para haber llegado aqui es que se trata de un error que pudo ser atrapado por eForms y que generó
	//un log y respaldó los archivos para restaurarlo, así que tenemos los datos exactamente de que archivos son y el detalle del error
	$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
	$strUserAccount = (string) @$_POST['UserID'];
	//@JAPR 2015-12-02: Corregido un bug, estaba reportando la fecha y hora de captura pero con los datos de la hora final, no la inicial
	$strSurveyDate = ((string) @$_POST["surveyDate"]).' '.((string) @$_POST["surveyHour"]);
	//@JAPR
	$intSurveyID = (int) @$_POST["surveyID"];
	$intAgendaID = (int) @$_POST["agendaID"];
	$strBackIUpDataFileName = getcwd()."\\"."syncerrors\\".trim($strRepositoryName)."\\".$gstrOutboxFileName;
	$strEFormsVersion = ESURVEY_SERVICE_VERSION;
	$strNav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$strSurveyName = $intSurveyID;
	$surveyInstance = @BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
	if (!is_null($surveyInstance)) {
		$strSurveyName .= '- '.$surveyInstance->SurveyName;
	}
	$strAgendaName = $intAgendaID;
	$agendaInstance = @BITAMSurveyAgenda::NewInstanceWithID($aRepository, $intAgendaID);
	if (!is_null($agendaInstance)) {
		$strAgendaName = '- '.$agendaInstance->AgendaName;
	}
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	$mail->From = $SMTPEmailSource;
	$mail->IsHTML(TRUE);
	//@JAPR 2013-03-26: Reemplazados los <enters> porque en el EMail no se veía bien
	$strEntryError = str_ireplace("\r\n", "\r\n<br><br>", $strEntryError);
	//@JAPR 2015-11-23: Agregado el nombre del producto que reporta
	$strProductName = (($gbIsGeoControl)?"GeoControl":"eForms");
	//@JAPR
	$mail->Subject = $strProductName.": ".str_ireplace("{var2}", $strUserAccount, str_ireplace("{var1}", $strMasterAccount, translate("An error has been produced while saving a form for the {var1} account with the {var2} user")));
	//OMMC 2015-10-29: se añade decode al subject y htmlentities al body para corregir acentos
	$mail->Subject = utf8_decode($mail->Subject);
	$mail->Body = "
<h2>".translate("Error made on the following form during saving time").".</h2>
<b>".translate("Service release").": </b>{$strEFormsVersion}
<br>
<b>".translate("App version").": </b>{$appVersion}
<br>
<b>".translate("Mobile device").": </b>{$strNav}
<br>
<b>".translate("Account").": </b>{$strMasterAccount}
<br>
<b>".translate("User").": </b>{$strUserAccount}
<br>
<b>".translate("Repository").": </b>{$strRepositoryName}
<br>
<b>".translate("Survey").": </b>{$strSurveyName}
<br>
<b>".translate("Agenda").": </b>{$strAgendaName}
<br>
<b>".translate("Entry date").": </b>{$strSurveyDate}
<br>
<b>".translate("Service used").": </b>{$streFormsService}
<br>
<b>".translate("File to restore").": </b>{$strBackIUpDataFileName}
<br>
<br>
<b>".translate("Error Detail").": </b>
<br>
{$strEntryError}
<br>
";
$mail->Body = utf8_decode($mail->Body);
	
	//@JAPR 2015-12-10: Estandarizado el reporte de errores para dejar permanentemente en el proceso de la captura, la mismas cuentas permantes del proceso del Agente
	//@$mail->AddAddress("jpuente@bitam.com", "Juan Puente");
	@$mail->AddAddress("psanchez@bitam.com", "Pedro Sanchez");
	//@$mail->AddAddress("cgil@bitam.com", "Claudia Gil");
	@$mail->AddAddress("jgonzalez@bitam.com", "J Gonzalez");
	@$mail->AddAddress("glopez@bitam.com", "Gustavo López");
	@$mail->AddAddress("aceballos@bitam.com", "Adrian Ceballos");
	//@$mail->AddAddress("jagonzalez@bitam.com", "Jose A Gonzalez");
	//@$mail->AddAddress("jcuellar@bitam.com", "J Cuellar");
	//@$mail->AddAddress("csanchez@bitam.com", "Carlos Sanchez");
	
	//Agrega las direcciones de las personas encargadas de resolver problemas de eForms
	if ($blnSendToDevelopMent) {
		@$mail->AddAddress(eFormsDeveloper1, "{$strProductName} Developer 1");
		@$mail->AddAddress(eFormsDeveloper2, "{$strProductName} Developer 2");
		@$mail->AddAddress(eFormsDeveloper3, "{$strProductName} Developer 3");
		//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo
		@$mail->AddAddress(eFormsDevelopmentTeam, "{$strProductName} Development team");
	}
	if ($blnSendToKPIAdmin) {
		@$mail->AddAddress(KPIAdmin1, "KPI Admin 1");
		@$mail->AddAddress(KPIAdmin2, "KPI Admin 2");
		@$mail->AddAddress(KPIOnlineAdminTeam, "{$strProductName} Administrators");
	}
	if ($blnSendToQA) {
		@$mail->AddAddress(QAStaff1, "QA Staff 1");
		@$mail->AddAddress(QAStaff2, "QA Staff 2");
		@$mail->AddAddress(QAStaff3, "QA Staff 3");
		@$mail->AddAddress(QAStaff4, "QA Staff 4");
		@$mail->AddAddress(QAStaff5, "QA Staff 5");
		//@JAPR 2015-12-08: Agregada la lista de distribución para calidad
		@$mail->AddAddress(eFormsQualityTeam, "Quality Staff");
	}
	if ($blnSendToBitamSupport) {
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	/*if (trim($strUserAccount) != '' && strpos($strUserAccount, '@') !== false) {
		if ($blnSendToUserAcc) {
			@$mail->AddAddress($strUserAccount, "{$strProductName} User");
		}
	}
	if (trim($strMasterAccount) != '' && strpos($strMasterAccount, '@') !== false) {
		if ($blnSendToMasterAcc) {
			@$mail->AddAddress($strMasterAccount, "{$strProductName} Account Admin");
		}
	}*/
	if ($blnSendToSupport) {
		//@JAPR 2015-11-19: Esta cuenta ya no está funcional, se remueve este reporte por ahora
		//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
		@$mail->AddAddress(KPIOnlineSupportTeam, "KPI Online Support Team");
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//Siempre se manda a internal para que quede registro del evento
	@$mail->AddAddress(InternalEForms, "{$strProductName}");
	//@JAPR 2014-02-20: Agregado el reporte de error en las notificaciones vía EMail
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	//AddCuentaCorreoAltToMail($theUser, $mail, $appVersion);
	//@JAPR
	if (!@$mail->Send()) {
		//Enviamos al log el error generado
		global $SendMailLogFile;
		
		$aLogString = "\r\n\r\n Error sending report: ".date('Y-m-d H:i:s');
		$aLogString .= "\r\n".@$mail->ErrorInfo;
		$aLogString .= "\r\n Capture detail: \r\n".@$mail->Body."\r\n";
		
		//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		@error_log($aLogString, 3, GeteFormsLogPath().((string) @$SendMailLogFile));
		//@JAPR
	}
	//@JAPR
	
	return true;
}

//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
/* Envía un eMail al personal especializado en resolver errores de sincronización de DataSources de eForms cuando el servicio recibe una solicitud pero por alguna
causa no puede hacer el grabado correcto. Adicionalmente se enviará un eMail al sistema de soporte, aunque en ese caso el EMail que se envía
sólo ocurre una vez por día para no saturar de tickets de soporte al sistema
*/
function reportDataSourceError($aRepository, $arrErrorsColl = array(), $theAppUser = null) {
    global $server;
    global $server_user;
    global $server_pwd;
    global $masterdbname;
	global $gbIsGeoControl;
    
	if (is_null($arrErrorsColl) || !is_array($arrErrorsColl)) {
		$arrErrorsColl = array();
	}
	$strEntryError = implode("\r\n", $arrErrorsColl);
	
    @require_once('../../../fbm/conns.inc.php');
	@require_once("bpaemailconfiguration.inc.php");
	@require_once('survey.inc.php');
	@require_once('surveyAgenda.inc.php');
	
	$anInstanceEmailConf = @BPAEmailConfiguration::readConfiguration();
	if(is_null($anInstanceEmailConf)) {
		return false;
	}
	
	$SMTPServer = (string) @$anInstanceEmailConf->fbm_email_server;	
	$SMTPPort = (string) @$anInstanceEmailConf->fbm_email_port;
	$SMTPUserName = (string) @$anInstanceEmailConf->fbm_email_username;
	$SMTPPassword = (string) @$anInstanceEmailConf->fbm_email_pwd;
	$SMTPEmailSource = (string) @$anInstanceEmailConf->fbm_email_source;
	if (strlen($SMTPServer) == 0 && strlen($SMTPPort) == 0 && strlen($SMTPUserName) == 0) {
		return false;
	}
	
	//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
	//que sólo incluya eForms su versión si no lo hizo ya eBavel
	if (!class_exists("PHPMailer")) {
		@require_once("mail/class.phpmailer.php");
		
		if (!class_exists("PHPMailer")) {
			return false;
		}
	}
	//@JAPR
	
	//Indentifica las cuentas a las que enviará los EMails basado en el tipo de cuenta maestra de la que se trata (si ocurre algún error, se
	//asumirá que se debe notificar a soporte interno por lo menos para que se verifique por qué sucede este error)
	$blnSendToSupport = false;
	$blnSendToQA = false;
	$blnSendToMasterAcc = false;
	$blnSendToBitamSupport = false;
	$blnSendToUserAcc = false;
	$blnSendToKPIAdmin = false;
	$blnSendToDevelopMent = true;
	//@JAPR 2015-12-10: Corregido un bug, esta es una variable global, así que no se debe asignar aquí sino hacer referencia a ella nada mas
	global $blnTestingServer;
	//$blnTestingServer = false;
	//@JAPR
	
	//Null significa que no se pudo determinar el CustomerType, por lo tanto se asumirá que se debe notificar solo a desarrollo para que verifique
	//por qué no se pudo determinar. Es diferente a que la tabla tenga un Null en cuyo caso se va a asumir que es Free Trial
	$intCustomerType = null;
	/* Customer Types:
		-9	- NA 
		1 	- Customer 
		2	- Free Trial 
		3	- Internal 
		4	- Partner 
		5	- POC 
		6	- Partner Prospect 
		7	- Free Trial Contactado 
		8	- Customer Dead 
		9	- Partner Dead 
		10	- Delete 
		11	- Demo 
		12	- Testing
	*/
	
	$strMasterAccount = '';
	$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
	//@JAPR 2018-05-10: Optimizada la carga de eForms (#G87C4W)
	//Reutilizada la conexión al metadata de KPIOnline mediante cache
	$mysql_Connection = GetKPIRepositoryConnection();
	//@JAPR
	$sql = '';
	if ($mysql_Connection) {
		//@JAPR 2018-05-10: Optimizada la carga de eForms (#G87C4W)
		$sql = "SELECT b.EMail, a.CustomerType 
			FROM saas_databases a, saas_users b 
			WHERE a.UserID = b.UserID AND a.Repository = '".(@mysql_real_escape_string($strRepositoryName, $mysql_Connection))."'";
		$result = @mysql_query($sql, $mysql_Connection);
		//@JAPR 2015-05-04: Agregado soporte para php 5.6
		if ($result && @mysql_num_rows($result) > 0) {
			$row = @mysql_fetch_assoc($result);
			$strMasterAccount = (string) @$row["EMail"];
			$intCustomerType = @$row["CustomerType"];
			if (is_null($intCustomerType)) {
				//Se tiene que revisar por qué del error, así que envía a desarrollo
				$intCustomerType = cmrtFreeTrial;
			}
			else {
				//Se convierte a numérico y se desactiva el envío a desarrollo
				$intCustomerType = (int) $intCustomerType;
				$blnSendToDevelopMent = false;
			}
		}
		//@JAPR
	}
    
    //Según el tipo de Customer habilita el envío de EMail a diferentes cuentas
    switch ($intCustomerType) {
    	case cmrtInternal:
    		//Las cuentas Internal deben ser dirigidas al responsable de la cuenta que generalmente es el Administrador y a soporte de Bitam, 
    		//ya que no son tan prioritarias como los clientes así que no generarán ticket de KPIOnline (ellos lo podrían generar automáticamente, 
    		//pero no se considera prioritario como para hacerlo automático)
    		$blnSendToMasterAcc = true;
    		$blnSendToBitamSupport = true;
    		break;
		
    	case cmrtCustomerDead:
    	case cmrtPartnerDead:
    		//Las cuentas de Customer o Partner Dead teoricamente ya no importan, sin embargo se notificará a los encargados de KPIOnline por si
    		//se tratara de algún tipo de error ya que continua recibiendo peticiones de eForms y tal vez se necesite cambiar el Status o darla
    		//de baja en definitiva
    		$blnSendToKPIAdmin = true;
    		break;	
    		
    	case cmrtDelete:
    		//Las cuentas eliminadas no tienen por qué reportar, así que termina el proceso
    		return false;
    		break;
    		
    	case cmrtTesting:
    		//Las cuentas de testing se deben notificar exclusivamente al equipo de Control de Calidad. Generalmente la cuenta Administradora no
    		//importa, pero si la cuenta específica del tester
    		$blnSendToQA = true;
    		$blnSendToUserAcc = true;
    		break;
    		
    	//Se asumirá cualquier tipo no identificado como si fuera de máxima prioridad, esto para no cometer errores e ignorar a cuentas importantes
    	//que pudieran estar estrenando un CustomerType recientemente definido. Estas cuentas no notifican al usuario que captura, solo al
    	//responsable que es la cuenta Administradora (Generan un ticket de KPIOnline automáticamente)
    	default:
			$blnSendToSupport = true;
			$blnSendToMasterAcc = true;
    		break;
    }
    
    if ($blnTestingServer) {
		//$blnSendToSupport = false;
		$blnSendToQA = true;
		//$blnSendToMasterAcc = false;
		//$blnSendToBitamSupport = false;
		//$blnSendToUserAcc = false;
		//$blnSendToDevelopMent = false;
		//$blnSendToKPIAdmin = false;
		//$blnSendToDevelopMent = false;
    }
    
	//@JAPR 2015-11-19: Por ahora se habilita a Desarrollo para efectos de pruebas
	//$blnSendToDevelopMent = true;
	
	//Obtiene los datos específicos del error. Para haber llegado aqui es que se trata de un error que pudo ser atrapado por eForms y que generó
	//un log y respaldó los archivos para restaurarlo, así que tenemos los datos exactamente de que archivos son y el detalle del error
	$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
	$strUserAccount = (string) @$_POST['UserID'];
	$strEFormsVersion = ESURVEY_SERVICE_VERSION;
	
	$mail = new PHPMailer();
	$mail->Mailer = "smtp";
	$mail->Host = $SMTPServer;
	$mail->Port = $SMTPPort;
	$mail->SMTPAuth = TRUE;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
	$mail->Username = $SMTPUserName;
	$mail->Password = $SMTPPassword;
	$mail->From = $SMTPEmailSource;
	$mail->IsHTML(TRUE);
	//@JAPR 2013-03-26: Reemplazados los <enters> porque en el EMail no se veía bien
	$strEntryError = str_ireplace("\r\n", "\r\n<br><br>", $strEntryError);
	//@JAPR 2015-11-23: Agregado el nombre del producto que reporta
	$strProductName = (($gbIsGeoControl)?"GeoControl":"eForms");
	//@JAPR
	$mail->Subject = $strProductName.": ".translate("An error has been made during catalog synchronization")." ({$strMasterAccount})";
	//OMMC 2015-10-29: se añade decode al subject y htmlentities al body para corregir acentos
	$mail->Subject = utf8_decode($mail->Subject);
	$mail->Body = "
<h2>".translate("The following catalog synchronization mistakes have occurred").".</h2>
<b>".translate("Service release").": </b>{$strEFormsVersion}
<br>
<b>".translate("User").": </b>{$strMasterAccount}
<br>
<b>".translate("Repository").": </b>{$strRepositoryName}
<br>
<b>".translate("eForms Service used").": </b>{$streFormsService}
<br>
<br>
<b>".translate("Error Detail").": </b>
<br>
{$strEntryError}
<br>
";
	$mail->Body = utf8_decode($mail->Body);
	
	//@JAPR 2015-12-10: Estandarizado el reporte de errores para dejar permanentemente en el proceso de la captura, la mismas cuentas permantes del proceso del Agente
	//@$mail->AddAddress("jpuente@bitam.com", "Juan Puente");
	@$mail->AddAddress("psanchez@bitam.com", "Pedro Sanchez");
	//@$mail->AddAddress("cgil@bitam.com", "Claudia Gil");
	@$mail->AddAddress("jgonzalez@bitam.com", "J Gonzalez");
	@$mail->AddAddress("glopez@bitam.com", "Gustavo López");
	@$mail->AddAddress("aceballos@bitam.com", "Adrian Ceballos");
	//@$mail->AddAddress("jagonzalez@bitam.com", "Jose A Gonzalez");
	//@$mail->AddAddress("jcuellar@bitam.com", "J Cuellar");
	//@$mail->AddAddress("csanchez@bitam.com", "Carlos Sanchez");
	
	//Agrega las direcciones de las personas encargadas de resolver problemas de eForms
	if ($blnSendToDevelopMent) {
		@$mail->AddAddress(eFormsDeveloper1, "{$strProductName} Developer 1");
		@$mail->AddAddress(eFormsDeveloper2, "{$strProductName} Developer 2");
		@$mail->AddAddress(eFormsDeveloper3, "{$strProductName} Developer 3");
		//@JAPR 2015-12-08: Agregada la lista de distribución para desarrollo
		@$mail->AddAddress(eFormsDevelopmentTeam, "{$strProductName} Development team");
	}
	if ($blnSendToKPIAdmin) {
		@$mail->AddAddress(KPIAdmin1, "KPI Admin 1");
		@$mail->AddAddress(KPIAdmin2, "KPI Admin 2");
		@$mail->AddAddress(KPIOnlineAdminTeam, "{$strProductName} Administrators");
	}
	if ($blnSendToQA) {
		@$mail->AddAddress(QAStaff1, "QA Staff 1");
		@$mail->AddAddress(QAStaff2, "QA Staff 2");
		@$mail->AddAddress(QAStaff3, "QA Staff 3");
		@$mail->AddAddress(QAStaff4, "QA Staff 4");
		@$mail->AddAddress(QAStaff5, "QA Staff 5");
		//@JAPR 2015-12-08: Agregada la lista de distribución para calidad
		@$mail->AddAddress(eFormsQualityTeam, "Quality Staff");
	}
	if ($blnSendToBitamSupport) {
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	/*if (trim($strMasterAccount) != '' && strpos($strMasterAccount, '@') !== false) {
		if ($blnSendToMasterAcc) {
			@$mail->AddAddress($strMasterAccount, "{$strProductName} Account Admin");
		}
	}*/
	if ($blnSendToSupport) {
		//@JAPR 2015-11-19: Esta cuenta ya no está funcional, se remueve este reporte por ahora
		//@JAPR 2015-12-07: Modificada la cuenta de soporte ya que la original fue dada de baja (#NVL2WM)
		@$mail->AddAddress(KPIOnlineSupportTeam, "KPI Online Support Team");
		@$mail->AddAddress(SupportStaff1, "Support Staff 1");
		@$mail->AddAddress(SupportStaff2, "Support Staff 2");
	}
	//Siempre se manda a internal para que quede registro del evento
	@$mail->AddAddress(InternalEForms, "{$strProductName}");
	//@JAPR 2014-02-20: Agregado el reporte de error en las notificaciones vía EMail
	//@JAPR 2016-01-04: Deshabilitado el reporte de errores a las cuentas de los clientes, sólo se enviará a soporte o el resto de destinos según Customer Type (#UM9ZJU)
	//Este cambio se pidió a raiz que los clientes no desean recibir notificaciones de error, de manera permanente se notificará al equipo de soporte así que
	//eso cubrirá todo posible problema
	//AddCuentaCorreoAltToMail($theAppUser, $mail);
	//@JAPR
	if (!@$mail->Send()) {
		//Enviamos al log el error generado
		global $SendMailLogFile;
		
		$aLogString = "\r\n\r\n Error sending report: ".date('Y-m-d H:i:s');
		$aLogString .= "\r\n".@$mail->ErrorInfo;
		$aLogString .= "\r\n Capture detail: \r\n".@$mail->Body."\r\n";
		
		//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		@error_log($aLogString, 3, GeteFormsLogPath().((string) @$SendMailLogFile));
		//@JAPR
	}
	//@JAPR
	
	return true;
}

/*
	//OMMC 2018-12-17: Proceso de obtención de coordenadas para el administrador usando mapkit y phantom
	Dado un string de JSON que retorna el request de geocoding de Mapkit construye un array asociativo que contiene los datos de la dirección obtenida en base a las coordenadas de GPS de la captura.
	Dado el número de elementos separados por coma obtiene el array, en caso de que el API cambie habría que ajustar esta función para que retorne el objeto deseado.
	Esta función fue clonada de la encontrada en el gpsmanager del app, cualquier ajuste realizado aquí deberá ser realizado también en el app y viceversa.
*/
function getGPSDataFromAppleJson($sJSON) {
	try {
		require_once('json.class.php');
		$json = new Services_JSON();
		$arrGPSData = array();
		$tmpArray = array();
		$tmpString = "";
		$jsonData = $json->decode($sJSON);
		$jsonData = object_to_array($jsonData);
		if (!is_array($jsonData)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("<br>\r\ngetGPSDataFromJson error: Error decoding JSON response");
			}
			return false;
		}

		if (count($jsonData)) {
			foreach ($jsonData as $arrAddressData) {
				foreach ($arrAddressData as $strKey => $strValue) {
					switch ($strKey) {
						case 'formattedAddress':
							if ($arrAddressData[$strKey] != "") {
								$anAddress = explode(',', $arrAddressData[$strKey]);
								$anAddressLength = count($anAddress);
								$pattern = '/(^\d+)(\s{1})(.+)/';
								switch ($anAddressLength) {
									//Obtener la dirección y posiblemente la colonia
									case 5:
										$arrGPSData['Country'] = trim($anAddress[4]);
										$arrGPSData['State'] = trim($anAddress[3]);
										//Para el CP y la Ciudad
										$tmpString = trim($anAddress[2]);
										preg_match_all($pattern, $tmpString, $arrayAddressPieces);
										if (count($arrayAddressPieces[0]) > 0) {
											$arrGPSData['ZipCode'] = $arrayAddressPieces[1][0];										
											$arrGPSData['City'] = $arrayAddressPieces[3][0];
										} else {
											$arrGPSData['ZipCode'] = "";										
											$arrGPSData['City'] = $tmpString;
										}
										$arrGPSData['Address'] = trim($anAddress[0]) . " " . trim($anAddress[1]);
									break;
									//No se obtuvo colonia
									case 4:
										$arrGPSData['Country'] = trim($anAddress[3]);
										$arrGPSData['State'] = trim($anAddress[2]);
										$tmpString = trim($anAddress[1]);
										preg_match_all($pattern, $tmpString, $arrayAddressPieces);
										if (count($arrayAddressPieces[0]) > 0) {
											$arrGPSData['ZipCode'] = $arrayAddressPieces[1][0];										
											$arrGPSData['City'] = $arrayAddressPieces[3][0];
										} else {
											$arrGPSData['ZipCode'] = "";										
											$arrGPSData['City'] = $tmpString;
										}
										$arrGPSData['Address'] = trim($anAddress[0]);
									break;
									case 3:
										$arrGPSData['Country'] = trim($anAddress[2]);
										$arrGPSData['State'] = trim($anAddress[1]);
										$arrGPSData['City'] = trim($anAddress[0]);
										$arrGPSData['ZipCode'] = "";
										$arrGPSData['Address'] = "";
									break;
									case 2:
										$arrGPSData['Country'] = trim($anAddress[1]);
										$arrGPSData['State'] = trim($anAddress[0]);
										$arrGPSData['City'] = "";
										$arrGPSData['ZipCode'] = "";
										$arrGPSData['Address'] = "";
									break;
									case 1:
										$arrGPSData['Country'] = trim($anAddress[0]);
										$arrGPSData['State'] = "";
										$arrGPSData['City'] = "";
										$arrGPSData['ZipCode'] = "";
										$arrGPSData['Address'] = "";
									break;
									//Algun otro caso no validado
									default:
										$arrGPSData['Country'] = "";
										$arrGPSData['State'] = "";
										$arrGPSData['City'] = "";
										$arrGPSData['ZipCode'] = "";
										$arrGPSData['Address'] = "";
									break;
								}
							$arrGPSData['FullAddress'] = $arrAddressData[$strKey];
							}
						break;
						default:
						break;
					}
				}
			}
		} else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("<br>\r\ngetGPSDataFromAppleJson error: Wrong response format ('results')");
			}
			return false;
		}
	} catch (Exception $e) {
		//No es necesario reportar este error
		$arrGPSData = false;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\ngetGPSDataFromJson exception: ".@$e->getMessage());
		}
	}
	
	return $arrGPSData;
}

/* Convierte el objeto JSon a un array y lo parsea para extraer la información que se requiere en el producto, identificando los componentes de
posicionamiento según la documentación de google maps encontrada en el siguiente enlace:
https://developers.google.com/maps/documentation/geocoding/?hl=es

- street_address indica una dirección postal precisa.
- route indica una carretera identificada (por ejemplo, "US 101").
- intersection indica una intersección importante, normalmente de dos carreteras importantes.
- political indica una entidad política. Este tipo suele indicar un polígono de alguna administración civil.
- country indica la entidad política nacional y, normalmente, es el tipo de mayor escala que devuelve el geocoder.
- administrative_area_level_1 indica una entidad civil de primer nivel por debajo del nacional. En Estados Unidos, las entidades que corresponden a este nivel administrativo son los estados. No todos los países cuentan con este nivel administrativo.
- administrative_area_level_2 indica una entidad civil de segundo nivel por debajo del nacional. En Estados Unidos, las entidades que corresponden a este nivel administrativo son los condados. No todos los países cuentan con este nivel administrativo.
- administrative_area_level_3 indica una entidad civil de tercer nivel por debajo del nacional. Este tipo indica una división administrativa de menor tamaño. No todos los países cuentan con este nivel administrativo.
- colloquial_area indica un nombre alternativo usado con frecuencia para designar la entidad.
- locality indica una entidad política equivalente al municipio.
- sublocality indica una entidad política de primer nivel por debajo del municipal.
- neighborhood indica un barrio identificado.
- premise indica una ubicación identificada. Normalmente se trata de un edificio o de un complejo de edificios con un nombre común.
- subpremise indica una entidad de primer nivel por debajo del de ubicación identificada. Normalmente se trata de un edificio individual dentro de un complejo de edificios con un nombre común.
- postal_code indica un código postal utilizado para enviar correo postal dentro del país.
- natural_feature indica un paraje natural destacado.
- airport indica un aeropuerto.
- park indica un parque identificado.
- point_of_interest indica un lugar de interés identificado. Normalmente, estos lugares de interés son entidades locales importantes que no encajan con facilidad en otras categorías, como el "Edificio Empire State" o la "Estatua de la Libertad".

Según los casos de prueba, el primer elemento del array contiene la dirección completa dividida en mas elementos, si se parsea este subconjunto
de elementos, bastaría para poder identificar todo lo que se necesita de la dirección comparando los "types" descritos arriba
*/
//Identifica a partir del string JSON especificado, todos los componentes de la dirección según los datos regresados por el API de Google
function getGPSDataFromJson($sJSON) {
	$arrGPSData = array();
	
	try {
		require_once('json.class.php');
		$json = new Services_JSON();
		$jsonData = $json->decode($sJSON);
		$jsonData = object_to_array($jsonData);
		if (!is_array($jsonData)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("<br>\r\ngetGPSDataFromJson error: Error decoding JSON response");
			}
			return false;
		}
		
		if (isset($jsonData['status'])) {
			/*
			"OK" indica que no se ha producido ningún error; la dirección se ha analizado correctamente y se ha devuelto al menos un código geográfico.
			"ZERO_RESULTS" indica que la codificación geográfica se ha realizado correctamente pero no ha devuelto ningún resultado. Esto puede ocurrir si en la codificación geográfica se incluye una dirección (address) inexistente o un valor latlng en una ubicación remota.
			"OVER_QUERY_LIMIT" indica que se ha excedido el cupo de solicitudes.
			"REQUEST_DENIED" indica que la solicitud se ha denegado; normalmente se debe a la ausencia de un parámetro sensor.
			"INVALID_REQUEST" normalmente indica que no se ha especificado la solicitud (address o latlng).
			*/
			$strStatus = strtoupper($jsonData['status']);
			switch ($strStatus) {
				case 'OK':
					if (isset($jsonData['results'])) {
						if (count($jsonData['results']) > 0) {
							$blnFound = false;
							foreach ($jsonData['results'] as $arrAddressData) {
								$blnContinue = false;
								if (isset($arrAddressData['types']) && is_array($arrAddressData['types'])) {
									foreach ($arrAddressData['types'] as $strType) {
										if (stripos($strType, 'address') !== false) {
											$blnContinue = true;
											break;
										}
									}
								}
								
								if ($blnContinue && isset($arrAddressData['address_components']) && is_array($arrAddressData['address_components'])) {
									$arrGPSData = getGPSDataFromStreetAddress($arrAddressData);
									$blnFound = true;
									break;
								}
							}
							
							//Si no se hubiera encontrado un 'street address', entonces procesa todos los elementos del array principal uno a uno
							//sobreescribiendo la dirección por aquella mayor de entre todos los elementos, mientras que el resto de los datos
							//identificados teóricamente hubieran sido siempre iguales
							if (!$blnFound) {
								foreach ($jsonData['results'] as $arrAddressData) {
									$arrGPSDataTemp = getGPSDataFromStreetAddress($arrAddressData);
									
									foreach ($arrGPSDataTemp as $strKey => $strValue) {
										switch ($strKey) {
											case 'Country':
											case 'State':
											case 'City':
											case 'ZipCode':
												//Estos valores son únicos, así que se sobreescriben
												//@JAPR 2013-06-11: Este día se solicitó que si los valores no se habían encontrado, entonces se
												//utilizara un default, por lo que se debe validar que sólo sobreescriba si el valor regresado
												//no es precisamente ese default, o si va a sobreescribir a un default, o si es el primero
												if (!isset($arrGPSData[$strKey]) || $arrGPSData[$strKey] == '' || $arrGPSData[$strKey] == 'Unknown') {
													$arrGPSData[$strKey] = $strValue;
												}
												break;
												
											case 'Address';
												//En este caso, respeta aquella de mayor longitud
												if (!isset($arrGPSData[$strKey])) {
													$arrGPSData[$strKey] = $strValue;
												}
												else {
													if ($strValue != 'Unknown') {
														if (strlen($arrGPSData[$strKey]) < strlen($strValue)) {
															$arrGPSData[$strKey] = $strValue;
														}
													}
												}
												break;
												
											case 'FullAddress';
												//En este caso, respeta aquella de mayor longitud
												if (!isset($arrGPSData[$strKey])) {
													$arrGPSData[$strKey] = $strValue;
												}
												else {
													if ($strValue != 'Unknown') {
														if (strlen($arrGPSData[$strKey]) < strlen($strValue)) {
															$arrGPSData[$strKey] = $strValue;
														}
													}
												}
												break;
										}
									}
								}
							}
							
							//@JAPR 2013-06-11: Genera el campo de la dirección completa concatenando los otros elementos ya recibidos
							if (!isset($arrGPSData['FullAddress'])) {
								$arrGPSData['FullAddress'] = '';
								$strAnd = '';
								if (isset($arrGPSData['Address']) && $arrGPSData['Address'] != 'Unknown' && $arrGPSData['Address'] != '') {
									$arrGPSData['FullAddress'] = $arrGPSData['Address'];
									$strAnd = ', ';
								}
								if (isset($arrGPSData['ZipCode']) && $arrGPSData['ZipCode'] != 'Unknown' && $arrGPSData['ZipCode'] != '') {
									$arrGPSData['FullAddress'] .= $strAnd.$arrGPSData['ZipCode'];
									$strAnd = ', ';
								}
								if (isset($arrGPSData['City']) && $arrGPSData['City'] != 'Unknown' && $arrGPSData['City'] != '') {
									$arrGPSData['FullAddress'] .= $strAnd.$arrGPSData['City'];
									$strAnd = ', ';
								}
								if (isset($arrGPSData['State']) && $arrGPSData['State'] != 'Unknown' && $arrGPSData['State'] != '') {
									$arrGPSData['FullAddress'] .= $strAnd.$arrGPSData['State'];
									$strAnd = ', ';
								}
								if (isset($arrGPSData['Country']) && $arrGPSData['Country'] != 'Unknown' && $arrGPSData['Country'] != '') {
									$arrGPSData['FullAddress'] .= $strAnd.$arrGPSData['Country'];
									$strAnd = ', ';
								}
							}
							//@JAPR
						}
						else {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\ngetGPSDataFromJson error: No GPS data returned");
							}
						}
					}
					else {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\ngetGPSDataFromJson error: Wrong response format ('results')");
						}
						return false;
					}
					break;
				
				default:
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\ngetGPSDataFromJson error: $strStatus");
					}
					return false;
					break;
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("<br>\r\ngetGPSDataFromJson error: Wrong response format ('status')");
			}
			return false;
		}
	} catch (Exception $e) {
		//No es necesario reportar este error
		$arrGPSData = false;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\ngetGPSDataFromJson exception: ".@$e->getMessage());
		}
	}
	
	return $arrGPSData;
}

//Identifica a partir del componente 'street_address' los datos, completos, ya que no todas las posiciones GPS proporcionadas contienen este valor
//Se puede enviar también este valor desde el array padre y procesará lo que vaya encontrando como subcomponentes, que eso se debe hacer
//precisamente cuando no existe el subcomponente 'street_address'
function getGPSDataFromStreetAddress($arrAddressData) {
	$arrGPSData = array();
	if (is_null($arrAddressData) || !is_array($arrAddressData)) {
		return $arrGPSData;
	}
	
	try {
		$strFormattedAddress = '';
		foreach ($arrAddressData['address_components'] as $arrComponent) {
			if (!is_array($arrComponent)) {
				continue;
			}
			
			$strLongName = '';
			$strShortName = '';
			$strType = '';
			foreach ($arrComponent as $strKey => $strValue) {
				switch (strtolower($strKey)) {
					case 'long_name':
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strLongName = $strValue;
						break;
					
					case 'short_name':
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
						$strShortName = $strValue;
						break;
						
					case 'types':
						$blnFound = false;
						foreach ($strValue as $strCompType) {
							switch (strtolower($strCompType)) {
								case 'country':
								case 'administrative_area_level_1':
								case 'locality':
								case 'postal_code':
									$strType = strtolower($strCompType);
									$blnFound = true;
									break;
								
								default:
									$strType = 'address';
									break;
							}
							
							if ($blnFound) {
								break;
							}
						}
						break;
				}
			}
			
			//Agrega o concatena el componente identificado
			switch ($strType) {
				case 'country':
					$strType = 'Country';
					break;
				
				case 'administrative_area_level_1':
					$strType = 'State';
					break;
					
				case 'locality':
					$strType = 'City';
					break;
					
				case 'postal_code':
					$strType = 'ZipCode';
					break;
					
				case 'address';
					$strType = 'Address';
					break;
			}

			if (isset($arrGPSData[$strType])) {
				$arrGPSData[$strType] .= ', '.$strLongName;
			}
			else {
				$arrGPSData[$strType] = $strLongName;
			}
		}
		
		//La dirección completa será regresada como la cadena mas larga entre todas
		if (isset($arrAddressData['formatted_address'])) {
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
			$arrGPSData['FullAddress'] = $arrAddressData['formatted_address'];
		}
		
		//Agrega valores default si el proceso termina bien pero no logró identificar todos los elementos
		if (!isset($arrGPSData['Country'])) {
			$arrGPSData['Country'] = 'Unknown';
		}
		if (!isset($arrGPSData['State'])) {
			$arrGPSData['State'] = 'Unknown';
		}
		if (!isset($arrGPSData['City'])) {
			$arrGPSData['City'] = 'Unknown';
		}
		if (!isset($arrGPSData['Address'])) {
			$arrGPSData['Address'] = 'Unknown';
		}
	} catch (Exception $e) {
		//No es necesario reportar este error
		$arrGPSData = false;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\ngetGPSDataFromJson exception: ".@$e->getMessage());
		}
	}
	
	return $arrGPSData;
}
//@JAPR
?>