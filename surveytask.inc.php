<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once('survey.inc.php');

$typeID = 0;

class BITAMSurveyTask extends BITAMObject
{
	public $SurveyTaskID;
	public $CreationDateID;
	public $SurveyID;
	//public $SurveyName;
	public $User;
	public $SurveyDate;
	public $TaskStartDate;
	public $TaskStatus;
	//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	public $ForceNew;
	public $SourceTaskID;
	public $AgendaID;
	public $WebMode;
	public $FileName;
	public $RepositoryName;
	public $SyncLatitude;
	public $SyncLongitude;
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	public $SyncAccuracy;
	//@JAPR
	public $FormsPath;
	public $FormsVersion;
	public $UserAgent;
	public $LogFileName;
	public $SavingTries;
	public $Executing;
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	public $SessionID;
	public $DeviceName;
	public $UDID;
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	public $DeviceID;		//Identificador del dispositivo basado en las constantes "dev" de config.php
	public $Reprocess;
	public $DestinationIDs;
	//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	public $SurveyKey;		//Id de la captura, utilizado originalmente para reportar en los destinos de datos
	//@JAPR
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->SurveyTaskID = -1;
		$this->CreationDateID= "";
		$this->SurveyID = -1;
		//$this->SurveyName = "";
		$this->User = "";
		$this->SurveyDate = "";
		$this->TaskStartDate = "";
		$this->TaskStatus = stsPending;
		//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		$this->ForceNew = false;
		$this->SourceTaskID = -1;
		$this->AgendaID = 0;
		$this->WebMode = 0;
		$this->FileName = "";
		$this->RepositoryName = "";
		$this->SyncLatitude = 0;
		$this->SyncLongitude = 0;
		//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
		$this->SyncAccuracy = 0;
		//@JAPR
		$this->FormsPath = "";
		$this->FormsVersion = "";
		$this->UserAgent = "";
		$this->LogFileName = "";
		$this->SavingTries = 0;
		$this->Executing = 0;
		//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
		$this->SessionID = "";
		$this->DeviceName = "";
		$this->UDID = "";
		//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
		$this->DeviceID = dvcWeb;
		$this->Reprocess = false; //Reprocess es true cuando la tarea se reprograma debido a que se seleccionó para reprocesarla desde la tabla de destinos
		$this->DestinationIDs = array(); //IDs de destinos que se selccionaron para reprocesarse
		//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		$this->SurveyKey = -1;
		//@JAPR
	}
	
	static function NewInstance($aRepository)
	{
		return new BITAMSurveyTask($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aSurveyTaskID)
	{
		$anInstance = null;
		
		if (((int)  $aSurveyTaskID) < 0)
		{
			return $anInstance;
		}
		
		global $typeID;
		
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		if (!is_object($aBITAMConnection)) {
			return $anInstance;
		}
		
		if($typeID==0)
		{	
			//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			/*
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t2.SurveyName, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, 
					t1.FormsVersion, t1.UserAgent 
				FROM SI_SV_SurveyTasks t1, SI_SV_Survey t2 
				WHERE t1.SurveyID = t2.SurveyID 
					AND SurveyTaskID = ".((int) $aSurveyTaskID);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
			//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strAdditionalFields = '';
			if (getMDVersion() >= esvDataDestErrorCheck) {
				$strAdditionalFields .= ', t1.SurveyKey';
			}
			//@JAPR
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, t1.SavingTries, t1.SessionID, 
					t1.DeviceName, t1.UDID, t1.Executing, t1.FormsVersion, t1.UserAgent, t1.Accuracy, t1.DeviceID {$strAdditionalFields} 
				FROM SI_SV_SurveyTasks t1 
				WHERE t1.Repository = ".$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])." 
					AND t1.SurveyTaskID = ".((int) $aSurveyTaskID);
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasks ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//@JAPR
			
			if (!$aRS->EOF)
			{
				$anInstance = BITAMSurveyTask::NewInstanceFromRS($aRepository, $aRS);
			}
		}
		else
		{
			//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			/*
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate,  t1.TaskStatus, t2.SurveyName, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, 
					t1.FormsVersion, t1.UserAgent 
				FROM SI_SV_SurveyTasksLog t1, SI_SV_Survey t2 
				WHERE t1.SurveyID = t2.SurveyID 
					AND SurveyTaskID = ".((int) $aSurveyTaskID);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			*/
			//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
			//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strAdditionalFields = '';
			if (getMDVersion() >= esvDataDestErrorCheck) {
				$strAdditionalFields .= ', t1.SurveyKey';
			}
			//@JAPR
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, t1.SavingTries, t1.SessionID, 
					t1.DeviceName, t1.UDID, t1.FormsVersion, t1.UserAgent, t1.LogFileName, t1.Accuracy, t1.DeviceID {$strAdditionalFields} 
				FROM SI_SV_SurveyTasksLog t1 
				WHERE t1.Repository = ".$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])." 
					AND t1.SurveyTaskID = ".((int) $aSurveyTaskID);
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//@JAPR
			
			if (!$aRS->EOF)
			{
				$anInstance = BITAMSurveyTask::NewInstanceFromRS($aRepository, $aRS);
			}
		}
		
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMSurveyTask::NewInstance($aRepository);
		
		$anInstance->SurveyTaskID = (int) $aRS->fields["surveytaskid"];
		$anInstance->CreationDateID = trim($aRS->fields["creationdateid"]);
		$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
		//$anInstance->SurveyName = trim($aRS->fields["surveyname"]);
		$anInstance->User = trim($aRS->fields["userid"]);
		$anInstance->SurveyDate = trim($aRS->fields["surveydate"]);
		$anInstance->TaskStartDate = trim($aRS->fields["taskstartdate"]);
		$anInstance->TaskStatus = (int) $aRS->fields["taskstatus"];
		//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		$anInstance->SourceTaskID = (int) @$aRS->fields["sourcetaskid"];
		$anInstance->AgendaID = (int) @$aRS->fields["agendaid"];
		$anInstance->WebMode = (int) @$aRS->fields["webmode"];
		$anInstance->FileName = (string) @$aRS->fields["filename"];
		$anInstance->RepositoryName = (string) @$aRS->fields["repository"];
		$anInstance->SyncLatitude = (float) @$aRS->fields["latitude"];
		$anInstance->SyncLongitude = (float) @$aRS->fields["longitude"];
		//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
		$anInstance->SyncAccuracy = (float) @$aRS->fields["accuracy"];
		//@JAPR
		$anInstance->FormsPath = (string) @$aRS->fields["formspath"];
		$anInstance->FormsVersion = (string) @$aRS->fields["formsversion"];
		$anInstance->UserAgent = (string) @$aRS->fields["useragent"];
		$anInstance->LogFileName = (string) @$aRS->fields["logfilename"];
		$anInstance->SavingTries = (int) @$aRS->fields["savingtries"];
		$anInstance->Executing = (int) @$aRS->fields["executing"];
		//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
		$anInstance->SessionID = (string) @$aRS->fields["sessionid"];
		$anInstance->DeviceName = (string) @$aRS->fields["devicename"];
		$anInstance->UDID = (string) @$aRS->fields["udid"];
		//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
		$anInstance->DeviceID = (int) @$aRS->fields["deviceid"];
		//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		$anInstance->SurveyKey = (int) @$aRS->fields["surveykey"];
		if ( $anInstance->SurveyKey <= 0 ) {
			$anInstance->SurveyKey = -1;
		}
		//@JAPR
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $typeID;
		
		if(array_key_exists("TypeID", $aHTTPRequest->GET))
		{
			$typeID = (int) $aHTTPRequest->GET["TypeID"];
		}

		if (array_key_exists("SurveyTaskID", $aHTTPRequest->POST))
		{
			$aSurveyTaskID = $aHTTPRequest->POST["SurveyTaskID"];
			
			if (is_array($aSurveyTaskID))
			{
				if(array_key_exists("TypeID", $aHTTPRequest->POST))
				{
					$typeID = (int) $aHTTPRequest->GET["TypeID"];
				}

				$aCollection = BITAMSurveyTaskCollection::NewInstance($aRepository, $aSurveyTaskID, $typeID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMSurveyTask::NewInstanceWithID($aRepository, (int) $aSurveyTaskID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyTask::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyTask::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("SurveyTaskID", $aHTTPRequest->GET))
		{
			$intReschedule = getParamValue('Reschedule', 'both', '(int)');
			$aSurveyTaskID = $aHTTPRequest->GET["SurveyTaskID"];
			$anInstance = BITAMSurveyTask::NewInstanceWithID($aRepository,(int) $aSurveyTaskID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyTask::NewInstance($aRepository);
			}
			elseif ($intReschedule) {
				//Genera una nueva tarea basada en la solicitada, la cual tendrá estado de recalendarizada
				$anInstance->reschedule();
			}
		}
		else
		{
			$anInstance = BITAMSurveyTask::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("SurveyTaskID", $anArray))
		{
			$this->SurveyTaskID = (int) $anArray["SurveyTaskID"];
		}
		
		if (array_key_exists("CreationDateID", $anArray))
		{
			$this->CreationDateID = trim($anArray["CreationDateID"]);
		}
		
		/*
		if (array_key_exists("SurveyName", $anArray))
		{
			$this->SurveyName = trim($anArray["SurveyName"]);
		}
		*/

		if (array_key_exists("User", $anArray))
		{
			$this->User = trim($anArray["User"]);
		}
		
		if (array_key_exists("SurveyDate", $anArray))
		{
			$this->SurveyDate = trim($anArray["SurveyDate"]);
		}
		
		if (array_key_exists("TaskStartDate", $anArray))
		{
			$this->TaskStartDate = trim($anArray["TaskStartDate"]);
		}
		
		if (array_key_exists("TaskStatus", $anArray))
		{
			$this->TaskStatus = (int) $anArray["TaskStatus"];
		}
			
		return $this;
	}
	
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
	/* Graba la información de la tarea del Agente. Las tareas no se pueden editar, así que siempre que graba lo hace sobre una nueva tarea,
	sin embargo se puede grabar debido a una recalendarización de tarea, así que el método que invoca al save debe asignar correctamente todas
	las propiedades antes de que se haga el grabado para reflejar dicho estado
	*/
	function save()
	{
		global $gblShowErrorSurvey;
		
	 	if ($this->isNewObject())
		{
			require_once('eSurveyServiceMod.inc.php');
			//Agrega la tarea a través del método originalmente utilizado, de manera que se pueda compartir el mismo código, simplemente envía la 
			//instancia de la tarea que generará a la nueva para forzar a generarla como recalendarizada. Esto es así porque se requiere una 
			//conexión adicional al repositorio de KPIOnline que no siempre está disponible, así que se encapsulará en el mismo lugar para no 
			//realizar dichos accesos desde múltiples archivos
			$strStatus = "";
			try {
				$strFileName = $this->FileName;
				$theRepositoryName = $this->RepositoryName;
				$strStatus = addEFormsAgentTask($strFileName, $theRepositoryName, $this);
			} catch (Exception $e) {
				$strStatus = $e->getMessage();
			}
			
			if (!is_numeric($strStatus)) {
			    if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".$strStatus);
			    }
			    else {
					return("(".__METHOD__.") ".$strStatus);
			    }
			}
			
			//Actualiza el ID de la instancia con el recién generado
			$this->SurveyTaskID = (int) @$strStatus;
			$this->ForceNew = false;
		}
		
		return $this;
	}
	//@JAPR
	
	function remove()
	{	
	}
	
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
	/* Marca la tarea indicada como recalendarizada para que dentro de la tabla de log quede como terminada con error pero agendada para un
	nuevo proceso, al mismo tiempo que genera una nueva tarea para cargar los mismos datos ya que utiliza la misma definición
	Este proceso sólo se puede llevar a cabo "n" veces según una configuración global, después de los cuales en definitiva quedará marcada como
	fallida y ya no se generará una tarea recalendarizada correspondiente
	El parámetro $aConnection permitirá que el Agente proporcione la conexión que ya él mismo creó, en lugar de volver a conectarse en esta
	función
	*/
	function reschedule($aConnection = null) {
		
		if($this->Reprocess==false)
		{
		if (($this->SavingTries >= efagtTries && !getParamValue('ForceReschedule', 'both', '(int)')) || ($this->TaskStatus != stsFailed && $this->TaskStatus != stsRescheduled)) {
			//En este caso se han excedido la cantidad de intentos, por lo tanto ya no se recalendariza la tarea
			return;
		}
		}
		//Modifica el status en memoria para que se genere como nueva tarea recalendarizada, asigna además Executing == stsRescheduled para que
		//el agente no tome esta tarea inmediatamente, sino hasta que se cumpla la fecha y hora de ejecución configurada en TaskStartDate,
		//momento para el cual el propio agente cambiará el Executing == 0 para que ahora si sea tomada
		$this->TaskStatus = stsRescheduled;
		$this->Executing = stsRescheduled;
		$this->SavingTries++;
		$this->TaskStartDate = date('Y-m-d H:i:s', strtotime("+ ".efagtMinutes." minute"));
		//Antes de iniciar el grabado se debe cambiar la pregunta para hacer referencia a que es nueva y desde cual se está clonando
		$intSurveyTaskID = $this->SurveyTaskID;
		$this->SourceTaskID = $intSurveyTaskID;
		$this->ForceNew = true;
		
		//Genera la tarea recalendarizada equivalente a esta
		$resultSave = $this->save();
		//Si $resultSave es cadena significa que hubo error, si es objeto significa que no hubo error y continúa
		if(is_string($resultSave))
		{
			return false;
		}
		//Cuando la tarea se reprograma debido a que se seleccionó algún destino para reprocesarla entonces el status de la tarea no cambia.
		if($this->Reprocess==false)
		{
			if (!is_null($aConnection)) {
				$aBITAMConnection = $aConnection;
			}
			else {
				$aBITAMConnection = connectToKPIRepository();
				if (!is_object($aBITAMConnection)) {
					if (is_string($aBITAMConnection)) {
						return $aBITAMConnection;
					}
					else {
						return "Unknown connection error";
					}
				}
			}
		
			//Al terminar, si el ID es diferente al nuevo ID se asume que si se grabó la tarea, por tanto se actualiza la tarea original en la tabla
			//de log para cambiar su estado a Recalendarizada
			$sql = "UPDATE SI_SV_SurveyTasksLog SET TaskStatus = ".stsRescheduled." 
				WHERE SurveyTaskID = {$intSurveyTaskID}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aBITAMConnection->ADOConnection->Execute($sql);
		}
		
		return true;
	}
	//@JAPR
	
	function isNewObject()
	{
		return ($this->SurveyTaskID < 0 || $this->ForceNew);
	}

	function get_Title()
	{
		return $this->CreationDateID;
	}

	function get_QueryString()
	{
		global $typeID;
		
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyTask&TypeID=".$typeID;
		}
		else
		{
			return "BITAM_PAGE=SurveyTask&SurveyTaskID=".$this->SurveyTaskID."&TypeID=".$typeID;
		}
	}

	function get_Parent()
	{
		global $typeID;
		
		return BITAMSurveyTaskCollection::NewInstance($this->Repository, null, $typeID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SurveyTaskID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CreationDateID";
		$aField->Title = translate("Creation Date");
		$aField->Type = "String";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
		//Se cambia el tipo de campo a un Object para que permita mostrar el nombre sin ser parte del query que carga la instancia
		//$aField->Name = "SurveyName";
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		//$aField->Type = "String";
		//$aField->Size = 255;
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMSurvey::getSurveys($this->Repository, null, null, true);
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "User";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyDate";
		$aField->Title = translate("Entry date");
		$aField->Type = "String";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TaskStartDate";
		$aField->Title = translate("Processing date");
		$aField->Type = "String";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		
		$statusOptions=array();
		$statusOptions[stsPending]=translate("Pending");
		$statusOptions[stsActive]=translate("In progress");
		$statusOptions[stsFailed]=translate("Failed");
		$statusOptions[stsCompleted]=translate("Successful");
		//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		$statusOptions[stsRescheduled]=translate("Rescheduled");
		$statusOptions[stsManualUpload]=translate("Manual");
		$statusOptions[stsIgnored]=translate("Ignored");
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TaskStatus";
		$aField->Title = translate("Status");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $statusOptions;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
 	{
?>
 	<script language="JavaScript">
 		function rescheduleTask()
		{
			frmRescheduleTask.submit();
		}
 	</script>
<?
 	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
 	function addButtons($aUser)
	{
		if (!$this->isNewObject())
		{
			if($this->TaskStatus == stsFailed) {
	?>
		<button id="btnReschedule" class="alinkescfav" onclick="rescheduleTask();"><img src="images/reschedule.gif" alt="<?=translate("Reschedule")?>" title="<?=translate("Reschedule")?>" displayMe="1" /> <?=translate("Reschedule")?></button>
	<?
			}
		}
	
	}
	
	function generateAfterFormCode($aUser)
	{
		global $typeID;
?>
	<form name="frmRescheduleTask" action="main.php?BITAM_PAGE=SurveyTask&SurveyTaskID=<?=$this->SurveyTaskID?>&TypeID=<?=$typeID?>&Reschedule=1&ForceReschedule=1" method="post" target="body">
	</form>
<?
	}
	//@JAPR
}

class BITAMSurveyTaskCollection extends BITAMCollection
{
	//0: Pending, 1: Completed
	public $TypeID;
	
	function __construct($aRepository, $aTypeID)
	{
		BITAMCollection::__construct($aRepository);
		$this->TypeID = (int) $aTypeID;
	}
	
	static function NewInstance($aRepository, $anArrayOfSurveyTaskIDs = null, $aTypeID = 0)
	{
		global $typeID;
		$typeID = $aTypeID;
		
		$anInstance = null;
		$anInstance = new BITAMSurveyTaskCollection($aRepository, $aTypeID);
		//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		if (!is_object($aBITAMConnection)) {
			return $anInstance;
		}
		//@JAPR
		
		$where = "";
		if (!is_null($anArrayOfSurveyTaskIDs))
		{
			switch (count($anArrayOfSurveyTaskIDs))
			{
				case 0:
					break;
				case 1:
					$where = " AND t1.SurveyTaskID = ".((int) $anArrayOfSurveyTaskIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyTaskIDs as $aSurveyTaskID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $aSurveyTaskID; 
					}
					if ($where != "")
					{
						$where = " AND t1.SurveyTaskID IN (".$where.")";
					}
					break;
			}
		}
		
		if($anInstance->TypeID==0)
		{
			//Tabla de tareas pendientes
			//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			/*
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t2.SurveyName, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, 
					t1.FormsVersion, t1.UserAgent 
				FROM SI_SV_SurveyTasks t1, SI_SV_Survey t2 
				WHERE t1.SurveyID = t2.SurveyID 
					".$where." ORDER BY t1.CreationDateID";
			*/
			//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
			//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strAdditionalFields = '';
			if (getMDVersion() >= esvDataDestErrorCheck) {
				$strAdditionalFields .= ', t1.SurveyKey';
			}
			//@JAPR
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, t1.SavingTries, t1.SessionID, 
					t1.DeviceName, t1.UDID, t1.Executing, t1.FormsVersion, t1.UserAgent, t1.Accuracy, t1.DeviceID {$strAdditionalFields} 
				FROM SI_SV_SurveyTasks t1 
				WHERE t1.Repository = ".$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])." 
					".$where." ORDER BY t1.CreationDateID";
		}
		else
		{
			//Tabla de tareas realizadas
			//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			/*
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t2.SurveyName, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, 
					t1.FormsVersion, t1.UserAgent 
				FROM SI_SV_SurveyTasksLog t1, SI_SV_Survey t2 
				WHERE t1.SurveyID = t2.SurveyID 
					".$where." ORDER BY t1.CreationDateID";
			*/
			//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
			//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strAdditionalFields = '';
			if (getMDVersion() >= esvDataDestErrorCheck) {
				$strAdditionalFields .= ', t1.SurveyKey';
			}
			//@JAPR
			$sql = "SELECT t1.SurveyTaskID, t1.CreationDateID, t1.SurveyID, t1.SourceTaskID, t1.AgendaID, t1.WebMode, t1.UserID, t1.SurveyDate, 
					t1.TaskStartDate, t1.TaskStatus, t1.FileName, t1.Repository, t1.Latitude, t1.Longitude, t1.FormsPath, t1.SavingTries, t1.SessionID, 
					t1.DeviceName, t1.UDID, t1.FormsVersion, t1.UserAgent, t1.LogFileName, t1.Accuracy, t1.DeviceID {$strAdditionalFields} 
				FROM SI_SV_SurveyTasksLog t1 
				WHERE t1.Repository = ".$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])." 
					".$where." ORDER BY t1.CreationDateID";
		}
		
		$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_SurveyTasks ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSurveyTask::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("TypeID", $aHTTPRequest->GET))
		{
			$aTypeID = (int) $aHTTPRequest->GET["TypeID"];
		}
		else
		{
			$aTypeID = 0;
		}

		return BITAMSurveyTaskCollection::NewInstance($aRepository, null, $aTypeID);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Tasks");
	}

	function get_QueryString()
	{
		global $typeID;
		
		return "BITAM_SECTION=SurveyTaskCollection&TypeID=".$typeID;
	}
	
	function get_ChildQueryString()
	{
		return "BITAM_SECTION=SurveyTask";
	}

	function get_AddRemoveQueryString()
	{
		global $typeID;
		
		return "BITAM_PAGE=SurveyTask&TypeID=".$typeID;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SurveyTaskID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CreationDateID";
		$aField->Title = translate("Creation Date");
		$aField->Type = "String";
		$aField->Size = 20;
		//$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
		//Se cambia el tipo de campo a un Object para que permita mostrar el nombre sin ser parte del query que carga la instancia
		//$aField->Name = "SurveyName";
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		//$aField->Type = "String";
		//$aField->Size = 255;
		//$aField->IsLink = false;
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = BITAMSurvey::getSurveys($this->Repository, null, null, true);
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "User";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		//$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyDate";
		$aField->Title = translate("Entry date");
		$aField->Type = "String";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TaskStartDate";
		$aField->Title = translate("Processing date");
		$aField->Type = "String";
		$aField->Size = 20;
		$myFields[$aField->Name] = $aField;
		
		/*
		'stsPending' = 0
		'stsActive' = 1
		'stsFailed' = 2
		'stsCompleted' = 3
		*/
		
		$statusOptions=array();
		$statusOptions[stsPending]=translate("Pending");
		$statusOptions[stsActive]=translate("In progress");
		$statusOptions[stsFailed]=translate("Failed");
		$statusOptions[stsCompleted]=translate("Successful");
		//@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		$statusOptions[stsRescheduled]=translate("Rescheduled");
		$statusOptions[stsManualUpload]=translate("Manual");
		$statusOptions[stsIgnored]=translate("Ignored");
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TaskStatus";
		$aField->Title = translate("Status");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $statusOptions;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	function displayCheckBox($aUser)
	{
		return false;
	}
	
	function generateAfterFormCode($aUser)
	{
		global $typeID;
?>
    <form name="frmChangeFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="SurveyTaskCollection">
		<input type="hidden" name="TypeID" id="TypeID">
	</form>
<?
	}
	
	function generateBeforeFormCode($aUser)
	{
?>		<script language="JavaScript">
		function applyTaskFilter()
		{
			var objType = document.getElementById("TypeID");
			frmChangeFilter.TypeID.value=objType.value;
			frmChangeFilter.submit();
		}
		</script>
<?		
	}
	
	function afterTitleCode($aUser)
	{
		if($this->TypeID==0)
		{
			$selectPend="selected";
			$selectComp="";
		}
		else
		{
			$selectPend="";
			$selectComp="selected";
		}
		
?>
	<span><?=translate("Filter")?>&nbsp;&nbsp;&nbsp;</span><select id="TypeID" name="TypeID" style="font-size:11px" onchange="javascript:applyTaskFilter();">
	<option value="0" <?=$selectPend?>><?=translate("Pending")?></option>
	<option value="1" <?=$selectComp?>><?=translate("Completed")?></option>
	</select>
	<br><br>
<?
	}	
}
?>