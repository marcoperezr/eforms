<?php
//exit("cad => ".json_encode($_REQUEST));
@error_log(date("Y-m-d H:i:s")." - ".print_r($_REQUEST, true), 3, "formsRegisterService.log");

if (isset($_REQUEST["action"]) && $_REQUEST["action"] == "registerUser" && isset($_REQUEST["registerData"]) && isset($_REQUEST["appName"]))
{
  $str_post = "data=".json_encode($_REQUEST);
  $endpoint = 'http://kpionline.bitam.com/fbm/kpiESurveyServiceClient.php';
  $ch = @curl_init();
  @curl_setopt($ch, CURLOPT_POST, true);
  @curl_setopt($ch, CURLOPT_POSTFIELDS, $str_post);
  @curl_setopt($ch, CURLOPT_URL, $endpoint);
/*    
  @curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/x-www-form-urlencoded'
  ));
*/    
  @curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $response    = @curl_exec($ch); //Log the response from HubSpot as needed.
  $status_code = @curl_getinfo($ch, CURLINFO_HTTP_CODE); //Log the response status code
  $curl_errors = @curl_error($ch);
  @curl_close($ch);
  exit($response);
}
else
{
  // arreglo default de retorno
  $arrReturn = array(
    "error" => 0,
    "errorCode" => 0,
    "errorMsg" => "",
    "warning" => 0,
    "warningCode" => 0,
    "warningMsg" => "",
    "validAccount" => 1,
    "availableLicenses" => 0,
    "regType" => 0,
    "user" => "",
    "password" => "",
    "passwordEnc" => "",
    "encrypted" => 0
  );
  $arrReturn["error"] = 2;
  $arrReturn["errorCode"] = 200;
  $arrReturn["errorMsg"] = "Missing required parameters!";
  header("Content-Type: text/javascript");
  if (isset($_REQUEST['jsonpCallback'])) 
  {
    //@error_log(date("Y-m-d H:i:s")." - (case1) - ".print_r($_REQUEST, true), 3, "fb.log");
    echo($_REQUEST['jsonpCallback'] . '(' . json_encode($arrReturn) . ');');
  } 
  else 
  {
    //@error_log(date("Y-m-d H:i:s")." - (case2) - ".print_r($_REQUEST, true), 3, "fb.log");
    echo(json_encode($arrReturn));
  }
  
}


?>