<?php
//Se elimino el codigo que no se estaba utilizando
?>
<script language="JavaScript" type="text/JavaScript">
function handleError() {
	return true;
}
window.onerror = handleError;
//debugger;
</script>
<style>
body {
   	/*
	margin: 10px 10px 10px 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    color: black;
    font-size: 12px;
    */
    /*background-color: #ece9d8; /*#ebe2d2;*/
   
   
}
.tabForm {
    /*
	background-color: #fafaf0; /*#f6f0e0;*/
    /*
	border: 1px solid #dfd2c0;
	*/
    background-color:#f0f8ff;
	padding: 5px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    BORDER-TOP: #6699cc 1px outset;
    BORDER-BOTTOM: #6699cc 1px outset;
    BORDER-RIGHT: #6699cc 1px outset;
    BORDER-LEFT: #6699cc 1px outset;
}

.tabFormUp {
    /*
	background-color: #fafaf0; /*#f6f0e0;*/
    /*
	border: 1px solid #dfd2c0;
	*/
    background-color:#f0f8ff;
    padding: 5px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    BORDER-TOP: #6699cc 1px outset;
    BORDER-BOTTOM: #6699cc 1px outset;
    BORDER-RIGHT: #6699cc 1px outset;
    BORDER-LEFT: #6699cc 1px outset;
}
.tabFormBottom {
    /*
	background-color: #fafaf0; /*#f6f0e0;*/
    /*
	border: 1px solid #dfd2c0;
	*/
    background-color:#f0f8ff;
    padding: 5px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    BORDER-TOP: #6699cc 1px hidden;
    BORDER-BOTTOM: #6699cc 1px outset;
    BORDER-RIGHT: #6699cc 1px outset;
    BORDER-LEFT: #6699cc 1px outset;
}

.dataLabel {
    color: #59512b; /*#785130;*/
    vertical-align : center;
    font-size: 12px;
    padding: 3px;
}

.dataField {
    font-weight: normal;
    vertical-align : top;
}

.tabDetailViewDF {
    /*background-color: #fbf8f3;*/
    color: #062040;
	background-color: #f0f8ff;
    padding-right: 4px;
    padding-left: 4px;
    padding-top: 4px;
    padding-bottom: 2px;
    font-size: 11px;
    font-weight: normal;
}

.tabDetailViewDFOnClick {
    color: #062040;
    background-color: #ffffff;
    padding-right: 4px;
    padding-left: 4px;
    padding-top: 4px;
    padding-bottom: 2px;
    font-size: 11px;
    font-weight: normal;
}

.calendar_Monthly {
	/*background-color: #f6f0e0;*/
    color: #062040;
	background-color: #f0f8ff;
    padding-right: 4px;
    padding-left: 4px;
    padding-top: 4px;
    padding-bottom: 2px;
    font-size: 11px;
    font-weight: normal;
}
</style>
&nbsp;
<div style="font-weight:bold; font-size:14"><?=translate("When To Capture")?></div>
<br>

<div id="Monthly">
	<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tabFormUp">
		<tr>
			<td colspan="7" class="dataLabel" width="45%"><?=translate('Start Date')?>
			<input type="text" id="" name="CaptureStartDate" size="22" maxlength="19" value="<?= $this->CaptureStartDate ?>" onfocus="javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}" onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd'), event.keyCode);" onblur="javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;}	calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd');">
						<script language="JavaScript">
							function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
        						}
        						var v = BITAMSurvey_SaveForm.CaptureStartDate.value;
								v = v.substr(10);
        						v = year + '-' + month + '-' + day + v;
	            				BITAMSurvey_SaveForm.CaptureStartDate.value = v;
								BITAMSurvey_SaveForm.CaptureStartDate.value = formatDateTime(v, 'yyyy-MM-dd');
    						}
    						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
						</script>
			</td>
			<td colspan="7" class="dataLabel" width="45%"><?=translate('End Date')?>
			<input type="text" id="" name="CaptureEndDate" size="22" maxlength="19" value="<?= $this->CaptureEndDate ?>" onfocus="javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}" onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd'), event.keyCode);" onblur="javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;}	calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd');">
						<script language="JavaScript">
							function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
        						}
        						var v = BITAMSurvey_SaveForm.CaptureEndDate.value;
								v = v.substr(10);
        						v = year + '-' + month + '-' + day + v;
	            				BITAMSurvey_SaveForm.CaptureEndDate.value = v;
								BITAMSurvey_SaveForm.CaptureEndDate.value = formatDateTime(v, 'yyyy-MM-dd');
    						}
    						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
						</script>
			</td>
		</tr>
		<tr>
			<td colspan="7" class="dataLabel" width="45%"><input name="Monthly_opc" type="radio" value="All_Days" STYLE="border:0;background:transparent;" onclick="Enable_Date(); Disable_Weekly(); Disable_End_Month();Clear_All_Days();">&nbsp;&nbsp;<script language="JavaScript">window.document.write('<?=translate('All Days')?>');</script>
			</td>
		</tr>
		<tr>
			<td colspan="7" class="dataLabel" width="45%"><input name="Monthly_opc" type="radio" value="Days" STYLE="border:0;background:transparent;" onclick="Disable_Weekly(); Disable_End_Month(); Disable_Date();">&nbsp;&nbsp;<script language="JavaScript">window.document.write('<?=translate('Days')?>');</script></td>
			<td colspan="2" class="dataLabel" width="55%"><input name="Monthly_opc" type="radio" value="Weekly" STYLE="border:0;background:transparent;" onclick="Enable_Weekly(); Disable_End_Month(); Disable_Date();Clear_All_Days();">&nbsp;&nbsp;<script language="JavaScript">window.document.write('<?=translate('Weekly')?>');</script></td>
		</tr>
		<tr>
			<!-- Calendar -->
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td colspan="5" class="tabDetailViewDF" rowspan="5">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="tabForm">
					<tr>
						<td id="day_1" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_1);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_1" id="inpday_1">1</td>
						<td id="day_2" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_2);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_2" id="inpday_2">2</td>
						<td id="day_3" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_3);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_3" id="inpday_3">3</td>
						<td id="day_4" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_4);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_4" id="inpday_4">4</td>
						<td id="day_5" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_5);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_5" id="inpday_5">5</td>
						<td id="day_6" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_6);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_6" id="inpday_6">6</td>
						<td id="day_7" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_7);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_7" id="inpday_7">7</td>
					</tr>
					<tr>
						<td id="day_8" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_8);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_8" id="inpday_8">8</td>
						<td id="day_9" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_9);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_9" id="inpday_9">9</td>
						<td id="day_10" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_10);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_10" id="inpday_10">10</td>
						<td id="day_11" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_11);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_11" id="inpday_11">11</td>
						<td id="day_12" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_12);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_12" id="inpday_12">12</td>
						<td id="day_13" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_13);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_13" id="inpday_13">13</td>
						<td id="day_14" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_14);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_14" id="inpday_14">14</td>
					</tr>
					<tr>
						<td id="day_15" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_15);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_15" id="inpday_15">15</td>
						<td id="day_16" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_16);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_16" id="inpday_16">16</td>
						<td id="day_17" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_17);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_17" id="inpday_17">17</td>
						<td id="day_18" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_18);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_18" id="inpday_18">18</td>
						<td id="day_19" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_19);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_19" id="inpday_19">19</td>
						<td id="day_20" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_20);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_20" id="inpday_20">20</td>
						<td id="day_21" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_21);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_21" id="inpday_21">21</td>
					</tr>
					<tr>
						<td id="day_22" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_22);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_22" id="inpday_22">22</td>
						<td id="day_23" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_23);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_23" id="inpday_23">23</td>
						<td id="day_24" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_24);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_24" id="inpday_24">24</td>
						<td id="day_25" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_25);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_25" id="inpday_25">25</td>
						<td id="day_26" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_26);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_26" id="inpday_26">26</td>
						<td id="day_27" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_27);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_27" id="inpday_27">27</td>
						<td id="day_28" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_28);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_28" id="inpday_28">28</td>
					</tr>
					<tr>
						<td id="day_29" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_29);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_29" id="inpday_29">29</td>
						<td id="day_30" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_30);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_30" id="inpday_30">30</td>
						<td id="day_31" class="calendar_Monthly" align="center" onclick="Select_Day(this.id, BITAMSurvey_SaveForm.inp_day_31);" onmouseover=""style="cursor:hand""><input type="hidden" value="0" name="inp_day_31" id="inpday_31">31</td>
					</tr>
				</table>
			</td>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="tabDetailViewDF"><script language="JavaScript">window.document.write('<?=translate('Week')?>');</script></td>
			<td class="tabDetailViewDF"><select name="Monthly_week" disabled></select></td>
		</tr>
		<tr>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="tabDetailViewDF"><script language="JavaScript">window.document.write('<?=translate('Days')?>');</script></td>
			<td class="tabDetailViewDF"><select name="Monthly_days" disabled></select></td>
		</tr>
		<tr>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td colspan="2" class="tabDetailViewDF">&nbsp;</td>
		</tr>
		<tr>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td colspan="2" class="dataLabel"><input name="Monthly_opc" type="radio" value="End of the Month" STYLE="border:0;background:transparent;" onclick="Enable_End_Month(); Disable_Weekly(); Disable_Date();Clear_All_Days();">&nbsp;&nbsp;<script language="JavaScript">window.document.write('<?=translate('End of the Month')?>');</script></td>
		</tr>
		<tr>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td class="dataLabel" width="5%">&nbsp;</td>
			<td colspan="2" class="tabDetailViewDF"><input name='Monthly_days_bef' size="3" type="text" maxlength="2" value="<?= ($this->WhenToCaptureType==3) ? $this->WhenToCaptureValue :'0'; ?>" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" STYLE="text-align:right;" disabled>&nbsp;&nbsp;<script language="JavaScript">window.document.write('<?=translate('Days before the end of the month')?>');</script></td>
		</tr>
		<tr><td><br></td></tr>
	</table>
</div>
<!--</form>-->
<script language="Javascript">
Start();
function Start()
{
	//debugger;
	//Cargar Periodo y Plantilla Default
	BITAMSurvey_SaveForm.Monthly_week.length = 6;
	BITAMSurvey_SaveForm.Monthly_week.options[0].value = 'All Weeks';
    BITAMSurvey_SaveForm.Monthly_week.options[0].text  = '<?=translate('All Weeks')?>';
	BITAMSurvey_SaveForm.Monthly_week.options[1].value = 'First Week';
    BITAMSurvey_SaveForm.Monthly_week.options[1].text  = '<?=translate('First Week')?>';
    BITAMSurvey_SaveForm.Monthly_week.options[2].value = 'Second Week';
    BITAMSurvey_SaveForm.Monthly_week.options[2].text  = '<?=translate('Second Week')?>';
    BITAMSurvey_SaveForm.Monthly_week.options[3].value = 'Third Week';
    BITAMSurvey_SaveForm.Monthly_week.options[3].text  = '<?=translate('Third Week')?>';
    BITAMSurvey_SaveForm.Monthly_week.options[4].value = 'Fourth Week';
    BITAMSurvey_SaveForm.Monthly_week.options[4].text  = '<?=translate('Fourth Week')?>';
    BITAMSurvey_SaveForm.Monthly_week.options[5].value = 'Last Week';
    BITAMSurvey_SaveForm.Monthly_week.options[5].text  = '<?=translate('Last Week')?>';

	BITAMSurvey_SaveForm.Monthly_days.length = 7;
	BITAMSurvey_SaveForm.Monthly_days.options[0].value = 'Monday';
    BITAMSurvey_SaveForm.Monthly_days.options[0].text  = '<?=translate('Monday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[1].value = 'Tuesday';
    BITAMSurvey_SaveForm.Monthly_days.options[1].text  = '<?=translate('Tuesday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[2].value = 'Wednesday';
    BITAMSurvey_SaveForm.Monthly_days.options[2].text  = '<?=translate('Wednesday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[3].value = 'Thursday';
    BITAMSurvey_SaveForm.Monthly_days.options[3].text  = '<?=translate('Thursday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[4].value = 'Friday';
    BITAMSurvey_SaveForm.Monthly_days.options[4].text  = '<?=translate('Friday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[5].value = 'Saturday';
    BITAMSurvey_SaveForm.Monthly_days.options[5].text  = '<?=translate('Saturday')?>';
    BITAMSurvey_SaveForm.Monthly_days.options[6].value = 'Sunday';
    BITAMSurvey_SaveForm.Monthly_days.options[6].text  = '<?=translate('Sunday')?>';

	var capturetype = '<?=$this->WhenToCaptureType?>';
	var intcapturetype = capturetype * 1;
	switch(intcapturetype)
	{
		case 0:
			BITAMSurvey_SaveForm.Monthly_opc['0'].checked = true;
			BITAMSurvey_SaveForm.Monthly_opc['0'].defaultChecked = true;
			//Enable_Date(); Disable_Weekly(); Disable_End_Month();
			break;
		case 1:
			var capturedays = '<?=$this->WhenToCaptureValue?>';
			var next = 0;
			var strday = "";
			BITAMSurvey_SaveForm.Monthly_opc['1'].checked = true;
			BITAMSurvey_SaveForm.Monthly_opc['1'].defaultChecked = true;
			//Disable_Weekly(); Disable_End_Month(); Disable_Date();
			while (next != -1)
			{	
				next = capturedays.search(/,/);
				strday = capturedays.slice(0,next);
				capturedays = capturedays.slice(next+1);
				if (next == -1)
				{
					getSelected_Days(capturedays);	
				}
				else
				{
					getSelected_Days(strday);
				}
			}
			break;
		case 2:
			var week = '<?=substr($this->WhenToCaptureValue,0,1)?>';
			var intweek = week * 1;
			var day = '<?=substr($this->WhenToCaptureValue,-1)?>';
			var intday = day * 1;
			BITAMSurvey_SaveForm.Monthly_opc['2'].checked = true;
			BITAMSurvey_SaveForm.Monthly_opc['2'].defaultChecked = true;
			BITAMSurvey_SaveForm.Monthly_week.options[intweek].defaultSelected = true;
			BITAMSurvey_SaveForm.Monthly_week.options[intweek].selected = true;
			if(intday != 0)
			{
			    BITAMSurvey_SaveForm.Monthly_days.options[intday-1].defaultSelected = true;
			    BITAMSurvey_SaveForm.Monthly_days.options[intday-1].selected = true;
			}
			else
			{
			    BITAMSurvey_SaveForm.Monthly_days.options[6].defaultSelected = true;
			    BITAMSurvey_SaveForm.Monthly_days.options[6].selected = true;
			}
			//Enable_Weekly(); Disable_End_Month(); Disable_Date();
			break;
		case 3:
			BITAMSurvey_SaveForm.Monthly_opc['3'].checked = true;
			BITAMSurvey_SaveForm.Monthly_opc['3'].defaultChecked = true;
			//Enable_End_Month(); Disable_Weekly(); Disable_Date();
			break;
	}
}

function Disable_Date()
{
	//Inhabilitar Date
	//BITAMSurvey_SaveForm.DateWhen.disabled=true;
	//document.images['calendar_BITAMSurvey_DateWhen_img'].style.display = 'none';
}

function Enable_Date()
{
	//Habilitar Date
	//BITAMSurvey_SaveForm.DateWhen.disabled=false;
	//document.images['calendar_BITAMSurvey_DateWhen_img'].style.display = 'inline';
}

function Disable_Weekly()
{
	// Inhabilitar Weekly
	BITAMSurvey_SaveForm.Monthly_week.disabled=true;
	BITAMSurvey_SaveForm.Monthly_days.disabled=true;
}

function Enable_Weekly()
{
	// Habilitar Weekly
	BITAMSurvey_SaveForm.Monthly_week.disabled=false;
	BITAMSurvey_SaveForm.Monthly_days.disabled=false;
}

function Disable_End_Month()
{
	// Inhabilitar End of the Month
	BITAMSurvey_SaveForm.Monthly_days_bef.disabled=true;
}

function Enable_End_Month()
{
	// Habilitar End of the Month
	BITAMSurvey_SaveForm.Monthly_days_bef.disabled=false;
}

function Select_Day(day_id, inp_day_id)
{
	if (BITAMSurvey_SaveForm.Monthly_opc['1'].checked == true)
	{
		if (inp_day_id.value == '0')
		{
			inp_day_id.value = 1;
			var d = getObject(day_id);
			d.style.backgroundColor = "\#336699";
			d.style.color = "\#FFFFFF"
		}
		else
		{
			inp_day_id.value = 0;
			var d = getObject(day_id);
			d.style.backgroundColor = "\#f0f8ff";
			d.style.color = "\#062040"
		}
	}
}

function Clear_Day(day_id, inp_day_id)
{
	//if (inp_day_id.value == '1')
	//{
	inp_day_id.value = 0;
	var d = getObject(day_id);
	d.style.backgroundColor = "\#f0f8ff";
	d.style.color = "\#062040"
	//}
}

function Clear_All_Days()
{
//	debugger;
	for(var i=1; i<=31; i++){
		var obj = getObject("inpday_"+i);
		Clear_Day("day_"+i, obj);
	}
}

function getObject(id) 
{
	if (ie4) {return document.all[id];} else {return document.getElementById(id);}
}

function getSelected_Days(day){
//	debugger;
	var obj = getObject("inpday_"+day);
	Select_Day("day_"+day, obj);
}

var ie4 = false;if(document.all) {ie4 = true;}
</script>