<?php
require_once('checkCurrentSession.inc.php');
require_once('survey.inc.php');

require_once('csvExport/csvExport.inc.php');
if (!isset($_REQUEST['SurveyID']) || $_REQUEST['SurveyID'] == '')
{
	//Cargar TODAS las surveyIDs
	$loadedSurveys = BITAMSurvey::getSurveys($theRepository, true);
	$surveyID = array();
	foreach($loadedSurveys as $skey => $asurvey)
		$surveyID[] = $skey;
}
else
	$surveyID = explode(',', $_REQUEST['SurveyID']);

$date_begin = (isset($_REQUEST['startDate']) && trim($_REQUEST['startDate']) != '')? date('Y-m-d', strtotime($_REQUEST['startDate'])).' 00:00:00': null;
$date_end = (isset($_REQUEST['endDate']) && trim($_REQUEST['endDate']) != '')? date('Y-m-d', strtotime($_REQUEST['endDate'])).' 23:59:59': null;
$userIDs = (isset($_REQUEST['userID']) && trim($_REQUEST['userID']) != '')? explode(',', $_REQUEST['userID']): array();
$bOutputFile = (isset($_REQUEST['outputFile']) && trim($_REQUEST['outputFile']) != '')? intval($_REQUEST['outputFile']) : true;

//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/A�o
$dateTwoState = (isset($_REQUEST['dateTwoState']) && trim($_REQUEST['dateTwoState']) != '')? $_REQUEST['dateTwoState'] : '';
//GCRUZ 2015-08-27 Filtro de b�squeda de formas
$searchForm = (isset($_REQUEST['searchForm']) && trim($_REQUEST['searchForm']) != '')? $_REQUEST['searchForm'] : '';
switch($dateTwoState)
{
	case 'T':
		$date_begin = date('Y-m-d').' 00:00:00';
		$date_end = date('Y-m-d').' 23:59:59';
		break;
	case 'W':
		$weekDay = intval(date('w'));
		$date_begin = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
		$date_end = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
		break;
	case 'M':
		$tDay = date('t');
		$date_begin = date('Y-m-').'01 00:00:00';
		$date_end = date('Y-m-').$tDay.' 23:59:59';
		break;
	case 'Y':
		$date_begin = date('Y-').'01-01 00:00:00';
		$date_end = date('Y-').'12-31 23:59:59';
		break;
}

//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$filePath = GeteFormsLogPath();
//@JAPR
$file = date('YmdHis').'_ReportSurvey.xlsx';
$arrCSVFiles = array();
foreach ($surveyID as $aSurveyID)
{
	$survey = BITAMSurvey::NewInstanceWithID($theRepository, $aSurveyID);
	$sections = BITAMSectionCollection::NewInstance($theRepository, $aSurveyID);
	$questions = BITAMQuestionCollection::NewInstance($theRepository, $aSurveyID);
	if (is_null($questions)) {
		die('could not load questions!');
	}
	if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
		continue;

	$data = EsurveyGetDataToExport($theRepository, $survey, $sections, $questions, $date_begin, $date_end, $userIDs);
	if (is_array($data['data']) && count($data['data']) > 0)
	{
		if ($_REQUEST['ExportReportFormat'] == 'XLS')
			EsurveyXLSExport($data, $filePath.$file, utf8_decode($survey->SurveyName));
		elseif ($_REQUEST['ExportReportFormat'] == 'CSV')
		{
			$csvFile = date('YmdHis').'_ReportSurvey_'.$aSurveyID.'.csv';
			EsurveyCSVExport($data, $filePath.$csvFile);
			$arrCSVFiles[] = $csvFile;
		}
		else
		EsurveyXLSExport($data, $filePath.$file, utf8_decode($survey->SurveyName));
	}
}

if ($_REQUEST['ExportReportFormat'] == 'CSV' && count($arrCSVFiles) > 0)
{
	$file = date('YmdHis').'_ReportSurvey.zip';
	$zip = new ZipArchive;
	$res = $zip->open($filePath.$file, ZipArchive::CREATE);
	if ($res === TRUE) {
		foreach ($arrCSVFiles as $aCSVFile)
			$zip->addFile($filePath.$aCSVFile, $aCSVFile);
		$zip->close();
	} else {
		echo 'Failed to create zip file';
		exit;
	}
}

if (!isset($filePath) || !isset($file) || !file_exists($filePath.$file))
{
	echo translate('No data found to Export!');
	exit;
}

$sfile = $file;
$spathfile = $filePath.$file;

/*
header('Content-Type: text/csv');
header('Content-Disposition: attachment;filename="'.$sfile.'"');
header('Content-Length: '.filesize($spathfile));
header('Cache-Control: max-age=0');
*/
//fix for IE catching or PHP bug issue
if ($bOutputFile)
{
	header("Pragma: public");
	header("Expires: 0"); // set expiration time
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Content-Type: application/force-download");
	if (count($arrCSVFiles) > 1)
		header("Content-Type: application/zip");
	else
		header("Content-Type: application/octet-stream");
	header("Content-Type: application/download");
	header("Content-Disposition: attachment; filename=\"".$sfile."\";");
	header("Content-Transfer-Encoding: binary");
	header("Content-Length: ".filesize($spathfile));
	readfile($spathfile);
	exit();
}
else
{
	echo $spathfile;
	exit();
}

?>