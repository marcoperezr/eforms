<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("utils.inc.php");

class BITAMScheduler extends BITAMObject
{
	public $SurveyID;
	public $CapStartDate;
	public $CapEndDate;
	public $CaptureType;
	public $WhenToCapture;
	public $StartDay;
	public $Duration;

	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->SurveyID=-1;
		$this->CapStartDate=null;
		$this->CapEndDate=null;
		$this->CaptureType=-1;
		$this->WhenToCapture=null;
		$this->StartDay=0;
		$this->Duration=0;
	}

	static function NewInstance($aRepository)
	{
		return new BITAMScheduler($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aSurveyID)
	{
		$anInstance = null;
		
		if (((int)  $aSurveyID) < 0)
		{
			return $anInstance;
		}

		$sql = "SELECT SurveyID, CaptureStartDate, CaptureEndDate, CaptureType, WhenToCapture, StartDay, Duration FROM SI_SV_Scheduler WHERE SurveyID = ".$aSurveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Scheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMScheduler::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMScheduler::NewInstance($aRepository);

		$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
	 	$anInstance->CapStartDate = $aRS->fields["capturestartdate"];
	 	$anInstance->CapEndDate = $aRS->fields["captureenddate"];
 		$anInstance->CaptureType = (int) $aRS->fields["capturetype"];
	 	$anInstance->WhenToCapture = $aRS->fields["whentocapture"];
	 	$anInstance->StartDay = (int)$aRS->fields["startday"];
	 	$anInstance->Duration = (int)$aRS->fields["duration"];

	 	return $anInstance;
	}

	function isEnabledCaptureThisDay()
	{
		$schedulerInstance = $this;
		$isEnabledCapture = false;
		$today = date("Y-m-d");
		$captureStartDate = array();
		
		if(!is_null($schedulerInstance->CapStartDate) && trim($schedulerInstance->CapStartDate)!="")
		{
			$schedulerInstance->CapStartDate = substr($schedulerInstance->CapStartDate, 0, 10);
		}
		else 
		{
			$schedulerInstance->CapStartDate = "";
		}

		if(!is_null($schedulerInstance->CapEndDate) && trim($schedulerInstance->CapEndDate)!="")
		{
			$schedulerInstance->CapEndDate = substr($schedulerInstance->CapEndDate, 0, 10);
		}

		//Obtener fecha de inicio de captura
		switch($schedulerInstance->CaptureType)
		{
			//Caso en el que se selecciona la opción 'Todos los días'
			case 0:
			{
				if($today >= $schedulerInstance->CapStartDate && $today <= $schedulerInstance->CapEndDate)
				{
					$captureStartDate[0] = $today;
				}
				break;	
			}

			//Caso en el que se especifica frecuencia semanal y tales dias de la semana
			case 1:
			{
				if($today >= $schedulerInstance->CapStartDate && $today <= $schedulerInstance->CapEndDate)
				{
					$month = mon($today);
					$year = year($today);
	
					if(trim($schedulerInstance->WhenToCapture)!="")
					{
						$capInfo = explode(',',$schedulerInstance->WhenToCapture);
					
						//$week=0 (Todas las semanas), $week=1 (primer semana), ..., $week=4 (cuarta semana), $week=5 (última semana del mes)
						//$day=0 (Domingo), $day=1 (Lunes) ... $day=6 (Sábado)
						foreach($capInfo as $day)
						{
							$tempCaptureStartDate = getDatesByYearMonthDay($year, $month, $day);
							$numCaptureStartDates = count($tempCaptureStartDate);
							
							for($i=0; $i<$numCaptureStartDates ;$i++)
							{
								$captureStartDate[] = formatADateTime($tempCaptureStartDate[$i],'yyyy-mm-dd');
							}
						}
					}
				}
				break;
			}
			
			//Caso en el que se especifica frecuencia mensual, tal dia del mes y duracion en dias 
			case 2:
			{
				if($today >= $schedulerInstance->CapStartDate && $today <= $schedulerInstance->CapEndDate)
				{
					
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					if($month < 10)
					{
						$month='0'.$month;
					}

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCapture)!="")
					{
						$positionDay = (int)$schedulerInstance->WhenToCapture;
						
						if($positionDay<=$numDaysOfMonth)
						{
							$daysArray[] = $positionDay;
							$duration = (int)$schedulerInstance->Duration;

							for($i=1; $i<$duration; $i++)
							{
								$positionDay = $positionDay + 1;
	
								if($positionDay<=$numDaysOfMonth)
								{
									$daysArray[] = $positionDay;
								}
								else 
								{
									break;
								}
								
							}
							
							foreach ($daysArray as $day)
							{
								if($day < 10)
								{
									$day='0'.$day;
								}
	
								$captureStartDate[] = $year ."-".$month."-".$day;
							}
						}
					}
				}
				break;
			}
			
			
			//Caso en el que se especifica frecuencia bimestral, tal mes non o par, dia de inicio del mes y duracion en dias 
			case 3:
			{
				if($today >= $schedulerInstance->CapStartDate && $today <= $schedulerInstance->CapEndDate)
				{
					
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCapture)!="")
					{
						$typeMonth = (int)$schedulerInstance->WhenToCapture;
						
						if($typeMonth==2)
						{
							if(($month%2)==0)
							{
								$condition = true;
							}
							else 
							{
								$condition = false;
							}
						}
						else 
						{
							if(($month%2)!=0)
							{
								$condition = true;
							}
							else 
							{
								$condition = false;
							}
							
						}
						
						if($condition==true)
						{
							$positionDay = (int)$schedulerInstance->StartDay;
							
							if($positionDay<=$numDaysOfMonth)
							{
								$daysArray[] = $positionDay;
								$duration = (int)$schedulerInstance->Duration;
								
								for($i=1; $i<$duration; $i++)
								{
									$positionDay = $positionDay + 1;
		
									if($positionDay<=$numDaysOfMonth)
									{
										$daysArray[] = $positionDay;
									}
									else 
									{
										break;
									}
									
								}
								
								if($month < 10)
								{
									$month='0'.$month;
								}
		
								foreach ($daysArray as $day)
								{
									if($day < 10)
									{
										$day='0'.$day;
									}
		
									$captureStartDate[] = $year ."-".$month."-".$day;
								}
							}
						}
					}
				}
				break;
			}
			//Caso en el que se especifica frecuencia trimestral, tal mes 1,2 o 3, dia de inicio del mes y duracion en dias 
			case 4:
			{
				if($today >= $schedulerInstance->CapStartDate && $today <= $schedulerInstance->CapEndDate)
				{
					
					//Se considera el mes y año actual
					$year = year($today);
					$month = mon($today);
					$numDaysOfMonth = numDaysOfMonth($year, $month);

					//Se considera el primer día seleccionado en el calendario y si no está ninguno seleccionado se considera el día 1
					if(trim($schedulerInstance->WhenToCapture)!="")
					{
						$typeMonth = (int)$schedulerInstance->WhenToCapture;
						
						if($typeMonth==1)
						{
							switch($month)
							{
								case 1:
								case 4:
								case 7:
								case 10:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						else if($typeMonth==2)
						{
							switch($month)
							{
								case 2:
								case 5:
								case 8:
								case 11:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						else if($typeMonth==3)
						{
							switch($month)
							{
								case 3:
								case 6:
								case 9:
								case 12:
									$condition = true;
									break;
								default:
									$condition = false;
									break;
							}
						}
						
						if($condition==true)
						{
							$positionDay = (int)$schedulerInstance->StartDay;
							
							if($positionDay<=$numDaysOfMonth)
							{
								$daysArray[] = $positionDay;
								$duration = (int)$schedulerInstance->Duration;

								for($i=1; $i<$duration; $i++)
								{
									$positionDay = $positionDay + 1;
		
									if($positionDay<=$numDaysOfMonth)
									{
										$daysArray[] = $positionDay;
									}
									else 
									{
										break;
									}
								}
								
								if($month < 10)
								{
									$month='0'.$month;
								}
		
								foreach ($daysArray as $day)
								{
									if($day < 10)
									{
										$day='0'.$day;
									}
		
									$captureStartDate[] = $year ."-".$month."-".$day;
								}
							}
						}
					}
				}
				break;
			}
		}
		
		//Si la fecha de hoy cumple con la condición del scheduler entonces si se indica que el dia de hoy
		//esta habilitada la captura de encuestas
		if(in_array($today, $captureStartDate))
		{
			$isEnabledCapture = true;
		}
		
		return $isEnabledCapture;
	}
}

class BITAMSchedulerCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository)
	{
		$anInstance = new BITAMSchedulerCollection($aRepository);
		
		$sql = "SELECT SurveyID, CaptureStartDate, CaptureEndDate, CaptureType, WhenToCapture FROM SI_SV_Scheduler";
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			$strComment = translate("Repository").": ".$anInstance->Repository->RepositoryName."\r\n";
			$strComment.= translate("Error accessing")." SI_SV_Scheduler ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			error_log(date("Y-m-d H:i:s")." - ".$strComment."\r\n", 3, "log/RunServices.log");
			return null;
		}

		$i = 0;
		while (!$aRS->EOF)
		{
			$anInstance->Collection[$i] = BITAMScheduler::NewInstanceFromRS($anInstance->Repository, $aRS);
			$i++;
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
}
?>