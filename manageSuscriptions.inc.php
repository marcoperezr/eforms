<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("object.trait.php");
require_once('kpiWebServiceEsurveyClient.inc.php');

class BITAMManageSuscriptions extends BITAMObject
{

	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
	}
	
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Redise�ado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el m�todo
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-06-24: Redise�ado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el m�todo
		$strCalledClass = static::class;
		
		$anInstance = $strCalledClass::NewInstance($aRepository);
				
		return $anInstance;
	}

	function get_Title()
	{
		return translate("Manage Suscriptions");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=ManageSuscriptions";
	}

	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function getJSonDefinition()
	{
		$arrDef = array();
		return $arrDef;
	}

	function generateForm($aUser) {
		require_once("genericgrid.inc.php");

		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		$strRepository = $this->Repository->RepositoryName;
		// get the databaseid
		$aSQL = "SELECT DatabaseID FROM saas_databases WHERE Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepository);
		$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
		if ($rsAdminData && $rsAdminData->_numOfRows != 0)
		{
			$intDatabaseID = $rsAdminData->fields['DatabaseID'];
		}
		else
			die('kpiWebService.Error: Failed to obtain databaseid Data!');
		// get the admin email and productid
		$aSQL = "SELECT A.ProductID, B.Email FROM saas_regs A, saas_users B WHERE A.databaseid = ".$intDatabaseID." AND A.hostid = 0 AND A.userid = B.userid";
		$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
		if ($rsAdminData && $rsAdminData->_numOfRows != 0)
		{
			$strAdminEmail = $rsAdminData->fields['Email'];
			$intProductID = 5;
		}
		else
			die('kpiWebService.Error: Failed to obtain host Data!');

		$kpiWebServiceClient = new getKpiWebServiceClient();
		$users = $kpiWebServiceClient->getUsers($strAdminEmail, $intProductID);
		//Obtener usuarios de SI_SV_USERS
		$aSQL = "SELECT Email, SV_Admin, SV_User FROM SI_SV_USERS ORDER BY Email";
		$rsUsers = $this->Repository->ADOConnection->Execute($aSQL);
		$arrEsurveyUsers = array();
		$arrEsurveyUsers['Admin'] = array();
		$arrEsurveyUsers['Guest'] = array();
		if ($rsUsers && $rsUsers->_numOfRows != 0)
		{
			while(!$rsUsers->EOF)
			{
				if (intval($rsUsers->fields['SV_Admin']))
					$arrEsurveyUsers['Admin'][] = strtolower($rsUsers->fields['Email']);
				if (intval($rsUsers->fields['SV_User']))
					$arrEsurveyUsers['Guest'][] = strtolower($rsUsers->fields['Email']);
				$rsUsers->MoveNext();
			}
		}
		$intExcessAdmins = 0;
		$intExcessGuests = 0;
		$intMaxAdmins = 0;
		$intMaxGuests = 0;
		if (!is_array($users))
		{
			$intAvailableAdmins = 0;
			$intAvailableGuests = 0;
			$arrAdminSuscritpions = array();
			$arrGuestSuscritpions = array();
		}
		else
		{
			if (intval($users[5]['maxAdmins']) == -1)
			{
				//GCRUZ 2016-07-05. Ilimitados
				$intAvailableAdmins = -1;
				$arrAdminSuscritpions = $arrEsurveyUsers['Admin'];
				$intExcessAdmins = 0;
				$intMaxAdmins = -1;
			}
			else
			{
//				$intAvailableAdmins = intval($users[5]['maxAdmins']) - count($users[5]['currentAdmins']);
				$intMaxAdmins = intval($users[5]['maxAdmins']);
				$intAvailableAdmins = intval($users[5]['maxAdmins']) - count($arrEsurveyUsers['Admin']);
				if ($intAvailableAdmins < 0)
				{
					$intExcessAdmins = $intAvailableAdmins * -1;
					$intAvailableAdmins = 0;
				}
//				$arrAdminSuscritpions = $users[5]['currentAdmins'];
				$arrAdminSuscritpions = $arrEsurveyUsers['Admin'];
			}
			if (intval($users[5]['maxGuests']) == -1)
			{
				//GCRUZ 2016-07-05. Ilimitados
				$intAvailableGuests = -1;
				$arrGuestSuscritpions = $arrEsurveyUsers['Guest'];
				$intExcessGuests = 0;
				$intMaxGuests = -1;
			}
			else
			{
//				$intAvailableGuests = intval($users[5]['maxGuests']) - count($users[5]['currentGuests']);;
				$intMaxGuests = intval($users[5]['maxGuests']);
				$intAvailableGuests = intval($users[5]['maxGuests']) - count($arrEsurveyUsers['Guest']);
				if ($intAvailableGuests < 0)
				{
					$intExcessGuests = $intAvailableGuests * -1;
					$intAvailableGuests = 0;
				}
//				$arrGuestSuscritpions = $users[5]['currentGuests'];
				$arrGuestSuscritpions = $arrEsurveyUsers['Guest'];
			}
		}
?>
<html>
<?
?>
	<script>
		var tabAdminSuscriptions = "adminSuscriptions";
		var tabGuestSuscriptions = "guestSuscriptions";
		var objFormTabs;					//Cejillas de la opci�n de configuraciones
		var UsersList = [];
		var strAdmin = "<?=$strAdminEmail?>";
		var AdminSuscritpions = [];
		var MaxAdmins = <?=$intMaxAdmins?>;
		var AvailableAdmins = <?=$intAvailableAdmins?>;
		var ExcessAdmins = <?=$intExcessAdmins?>;
		var GuestSuscritpions = [];
		var MaxGuests = <?=$intMaxGuests?>;
		var AvailableGuests = <?=$intAvailableGuests?>;
		var ExcessGuests = <?=$intExcessGuests?>;
		var adminUsersGrid;
		var guestUsersGrid;
		
<?
		$objUserColl = BITAMAppUserCollection::NewInstance($this->Repository);
		foreach ($objUserColl as $objAppUser) {
			if (array_search($objAppUser->Email, $arrAdminSuscritpions) === false && array_search($objAppUser->Email, $arrGuestSuscritpions) === false && $objAppUser->Email != $strAdminEmail)
			{
?>
				UsersList.push("<?=$objAppUser->Email?>");
<?			
			}
		}

		foreach ($arrAdminSuscritpions as $aUser)
		{
			if ($aUser != $strAdminEmail)
			{
?>
				AdminSuscritpions.push("<?=$aUser?>");
<?
			}
		}

		foreach ($arrGuestSuscritpions as $aUser)
		{
			if ($aUser != $strAdminEmail)
			{
?>
				GuestSuscritpions.push("<?=$aUser?>");
<?
			}
		}

		$arrJSON = BITAMManageSuscriptions::getJSonDefinition($this->Repository);
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		$strJSONDefinitions = json_encode($arrJSON);
?>		
		var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
		var objSetttings = objFormsDefs.data;

		//******************************************************************************************************
		SuscriptionsCls.prototype = new GenericItemCls();
		SuscriptionsCls.prototype.constructor = SuscriptionsCls;
		function SuscriptionsCls() {
			this.objectType = AdminObjectType.otySetting;
		}
		
		/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
		//El par�metro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
		*/
		SuscriptionsCls.prototype.getDataFields = function(oFields) {
			
			var objParams = {};
			return objParams;
		}

		function doOnLoad() {
			console.log('doOnLoad');

			addClassPrototype();

			objMainLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});

			objMainLayout.cells("a").appendObject("divDesign");
			objMainLayout.cells("a").hideHeader();

			objFormTabs = new dhtmlXTabBar({
				parent:"divDesignBody"
			});
			objFormTabs.addTab(tabAdminSuscriptions, "<span class='tabFormsOpts'><?=translate("Administrators")?></span>", null, null, true);
			objFormTabs.addTab(tabGuestSuscriptions, "<span class='tabFormsOpts'><?=translate("Guests")?></span>");
			objFormTabs.setArrowsMode("auto");
			objFormTabs.tabs(tabAdminSuscriptions).attachLayout({
				parent: document.body,
				pattern: "1C"
			});
			objFormTabs.tabs(tabGuestSuscriptions).attachLayout({
				parent: document.body,
				pattern: "1C"
			});

			objFormsLayout = objFormTabs.tabs(tabAdminSuscriptions).getAttachedObject();
			//Evento del cambio de tab para permitir grabar informaci�n que hubiera cambiado
			objFormTabs.attachEvent("onTabClick", function(id, lastId) {
				console.log('objFormTabs.onTabClick ' + id + ', lastId == ' + lastId);
			});
			objFormTabs.attachEvent("onContentLoaded", function(id) {
				objFormTabs.tabs(id).progressOff();
			});
			//Genera la ventana de propiedades
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Administrators")?>");

			objFormsLayout = objFormTabs.tabs(tabGuestSuscriptions).getAttachedObject();
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Guests")?>");

			generateAdminsSuscriptions(tabAdminSuscriptions);
			generateGuestsSuscriptions(tabGuestSuscriptions);

			//Resize completo de la ventana y prepara el evento para los resize posteriores
			doOnResizeScreen();

			$(window).resize(function() {
				doOnResizeScreen();
			});
		}

		function doChangeTab(sTabId) {
			console.log('doChangeTab ' + sTabId);
//			debugger;
			var objEvent = this.event || window.event;
			if (objEvent) {
				var objTarget = objEvent.target || objEvent.srcElement;
				if (objTarget) {
					$("#divDesignHeader td").removeClass("linktdSelected");
					$(objTarget).addClass("linktdSelected");
				}
			}
			if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
				return;
			}

			objFormTabs.tabs(sTabId).setActive();
		}

		function generateAdminsSuscriptions(tabAdminSuscriptions)
		{
			var objFormData = [
				{type:"fieldset",  name:"fieldsAdminUsers", label:"<?=translate("Users")?>", width:380, className:"selection_list", list:[
					{type:"input", name:"txtSearchAdminUsers", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
					{type:"container", name:"gridAdminUsers", inputWidth:330, inputHeight:200, className:"selection_container"}
				]},
				{type:"newcolumn", offset:20},
				{type:"block", blockOffset:0, offsetTop:200, list:[
					{type:"button", name: "btnAddAdminUser", value:"", className:"assoc_button"},
					{type:"button", name: "btnRemoveAdminUser", value:"", className:"remove_button"}
				]},
				{type:"newcolumn", offset:20},
				{type:"fieldset",  name:"fieldsAdminSuscriptions", label:"<?=translate("Maximum Administrators").":".(($intMaxAdmins != -1)?$intMaxAdmins-1:translate('Unlimited'))?>", width:380, className:"selection_list", list:[
					{type:"input", name:"txtSearchAdminSuscriptions", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
					{type:"container", name:"gridAdminSuscriptions", inputWidth:330, inputHeight:400, className:"selection_container"}
				]},
			];

			objFormTabs.tabs(tabAdminSuscriptions).attachObject("divAdminSuscriptionsParent");
			objAdminSuscriptions = new dhtmlXForm("divAdminSuscriptions", objFormData);
			$(objAdminSuscriptions.cont).addClass('customGrid');

			//Contenido de la lista de usuarios disponibles
			var objAdminUsersGrid = new dhtmlXGridObject(objAdminSuscriptions.getContainer("gridAdminUsers"));
			objAdminUsersGrid.setImagePath("<?=$strScriptPath?>/images/");
			objAdminUsersGrid.setHeader("<?=translate("Users")?>");
			objAdminUsersGrid.setInitWidthsP("100");
			objAdminUsersGrid.setColAlign("left");
			objAdminUsersGrid.setColTypes("ro");
			objAdminUsersGrid.enableEditEvents(false);
//			objAdminUsersGrid.enableDragAndDrop(true);
			objAdminUsersGrid.setColSorting("str");
			objAdminUsersGrid.enableMultiselect(true);
			objAdminUsersGrid.init();
			objAdminUsersGrid.sortRows(0,"str","asc");

			//Contenido de la lista de usuarios Administradores
			var objAdminSuscritpionsGrid = new dhtmlXGridObject(objAdminSuscriptions.getContainer("gridAdminSuscriptions"));
			objAdminSuscritpionsGrid.setImagePath("<?=$strScriptPath?>/images/");
			if (ExcessAdmins > 0)
			{
				objAdminSuscritpionsGrid.setHeader("<?=translate('Suscripted Administrators').' ('.translate('Excess').':'.$intExcessAdmins.')'?>");
			}
			else
			{
				objAdminSuscritpionsGrid.setHeader("<?=translate('Suscripted Administrators').(($intAvailableAdmins != -1)?' ('.translate('Available').':'.$intAvailableAdmins.')':'')?>");
			}
			objAdminSuscritpionsGrid.setInitWidthsP("100");
			objAdminSuscritpionsGrid.setColAlign("left");
			objAdminSuscritpionsGrid.setColTypes("ro");
			objAdminSuscritpionsGrid.enableEditEvents(false);
//			objAdminSuscritpionsGrid.enableDragAndDrop(true);
			objAdminSuscritpionsGrid.setColSorting("str");
			objAdminSuscritpionsGrid.enableMultiselect(true);
			objAdminSuscritpionsGrid.init();
			objAdminSuscritpionsGrid.sortRows(0,"str","asc");

			for (var i=0; i<UsersList.length; i++) {
				if (AdminSuscritpions.indexOf(UsersList[i]) == -1 && GuestSuscritpions.indexOf(UsersList[i]) == -1)
				{
					objAdminUsersGrid.addRow(i, UsersList[i]);
					objAdminUsersGrid.setUserData(i, "id", UsersList[i]);
				}
			}

			var countAdmins = MaxAdmins;
			for (var i=0; i<AdminSuscritpions.length; i++) {
				if (countAdmins > 0 || MaxAdmins == -1)
				{
					objAdminSuscritpionsGrid.addRow(i, AdminSuscritpions[i]);
					objAdminSuscritpionsGrid.setUserData(i, "id", AdminSuscritpions[i]);
					countAdmins--;
				}
				else
				{
					objAdminSuscritpionsGrid.addRow(i, AdminSuscritpions[i]);
					objAdminSuscritpionsGrid.setUserData(i, "id", AdminSuscritpions[i]);
					objAdminSuscritpionsGrid.setRowTextStyle(i, "background-color: red;");
				}
			}

			objAdminUsersGrid.sortRows(0);
			objAdminSuscritpionsGrid.sortRows(0);

			objAdminSuscriptions.attachEvent("onKeyUp",function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				var objGrid = undefined;
				switch (name) {
					case "txtSearchAdminUsers":
						objGrid = objAdminUsersGrid;
						break;
					case "txtSearchAdminSuscriptions":
						objGrid = objAdminSuscritpionsGrid;
						break;
				}
				
				if (!objGrid) {
					return;
				}
				
				//Verifica si ya se hab�a aplicado esta b�squeda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterar�an el contenido)
				var strValue = inp.value;
				var strLastSearch = objGrid.getUserData(name, "lastSearch");
				if (strLastSearch == strValue) {
					return;
				}
				
				objGrid.setUserData(name, "lastSearch", strValue);
				objGrid.filterBy(0, strValue);
			});

			//JAPR 2015-07-15: Agregada la funcionalidad para mover los elementos entre grids
			objAdminSuscriptions.attachEvent("onButtonClick", function(name) {
				objFormTabs.tabs(tabAdminSuscriptions).progressOn();
				console.log('objAdminSuscriptions.onButtonClick '+name);
				switch (name) {
					case "btnAddAdminUser":
						var strSelection = objAdminUsersGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if ((arrSelection.length && arrSelection.length <= AvailableAdmins) || MaxAdmins == -1) {
								var blnMove = false;
								var movingUsers = new Object();
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										movingUsers[strId] = {id:strId, name:objAdminUsersGrid.cells(strId, 0).getValue()};
									}
								}
								
								if (blnMove) {
									var updated = updateSuscriptions(movingUsers, 'add', 'admin');
									if (updated)
									{
										for (var user in movingUsers)
										{
											objAdminUsersGrid.moveRow(user, "row_sibling", undefined, objAdminSuscritpionsGrid);
											//Buscar el usuario en el grid de invitados y eliminarlo
											var resultCells = guestUsersGrid.findCell(movingUsers[user].name, 0, true);
											if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
											{
												guestUsersGrid.deleteRow(resultCells[0][0]);
											}
										}
										AvailableAdmins -= arrSelection.length;
										var label = "<?=translate('Suscripted Administrators')?>";
										if (MaxAdmins != -1)
										{
											label = label + "<?=' ('.translate('Available').':'?>" + AvailableAdmins + ")";
										}
										objAdminSuscritpionsGrid.setColumnLabel(0, label);
									}
								}
							}
							else
							{
								if (arrSelection.length == 0)
								{
									alert("<?=translate('You must select at least one User')?>");
								}
								if (arrSelection.length > AvailableAdmins)
								{
									alert("<?=translate('You do not have enough available Administrator Suscriptions to perform this operation')?>");
								}
							}
						}
						break;
					
					case "btnRemoveAdminUser":
						var strSelection = objAdminSuscritpionsGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								var movingUsers = new Object();
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										movingUsers[strId] = {id:strId, name:objAdminSuscritpionsGrid.cells(strId, 0).getValue()};
									}
								}
								
								if (blnMove) {
									var updated = updateSuscriptions(movingUsers, 'remove', 'admin');
									if (updated)
									{
										var newId;
										var lastId;
										for (var user in movingUsers)
										{
											objAdminSuscritpionsGrid.moveRow(user, "row_sibling", undefined, objAdminUsersGrid);
											//Agregar el usuario al grid de Invitados
											newId = (new Date()).valueOf();
											while (newId == lastId)
											{
												newId = (new Date()).valueOf();
											}
											guestUsersGrid.addRow(newId, movingUsers[user].name);
											guestUsersGrid.setUserData(newId, "id", movingUsers[user].name);
											lastId = newId;
										}
										if (ExcessAdmins > 0)
										{
											ExcessAdmins -= arrSelection.length;
											if (ExcessAdmins <= 0)
											{
												AvailableAdmins += (ExcessAdmins * -1);
												ExcessAdmins = 0;
												objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Available').':'?>"+AvailableAdmins+")");
											}
											else
											{
												objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Excess').':'?>"+ExcessAdmins+")");
											}
										}
										else
										{
											AvailableAdmins += arrSelection.length;
											var label = "<?=translate('Suscripted Administrators')?>";
											if (MaxAdmins != -1)
											{
												label = label + "<?=' ('.translate('Available').':'?>" + AvailableAdmins + ")";
											}
											objAdminSuscritpionsGrid.setColumnLabel(0, label);
										}
									}
								}
							}
							else
							{
								if (arrSelection.length == 0)
								{
									alert("<?=translate('You must select at least one User')?>");
								}
							}
						}
						break;
				}
				objAdminUsersGrid.sortRows(0);
				objAdminSuscritpionsGrid.sortRows(0);
				guestUsersGrid.sortRows(0);
				updateGridExcess(objAdminSuscritpionsGrid, MaxAdmins);
				objFormTabs.tabs(tabAdminSuscriptions).progressOff();
			});
			
			//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
			objAdminUsersGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objAdminUsersGrid.onDrop User');
				objFormTabs.tabs(tabAdminSuscriptions).progressOn();
				if (sObj == tObj) {
					this.sortRows(0);
					objFormTabs.tabs(tabAdminSuscriptions).progressOff();
					return;
				}
				var arrSIds = sId.split(',');
				var movingUsers = new Object();
				for (var intSIdIndex in arrSIds)
				{
					movingUsers[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
				}
				var updated = updateSuscriptions(movingUsers, 'remove', 'admin');
				if (!updated)
				{
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					var newId;
					var lastId;
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objAdminSuscritpionsGrid);
						usersCount--;
					}
				}
				else
				{
					if (ExcessAdmins > 0)
					{
						ExcessAdmins -= arrSIds.length;
						if (ExcessAdmins <= 0)
						{
							AvailableAdmins += (ExcessAdmins * -1);
							ExcessAdmins = 0;
							objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Available').':'?>"+AvailableAdmins+")");
						}
						else
						{
							objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Excess').':'?>"+ExcessAdmins+")");
						}
					}
					else
					{
						AvailableAdmins += arrSIds.length;
						objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Available').':'?>"+AvailableAdmins+")");
					}
					var newId;
					var lastId;
					for (var user in movingUsers)
					{
						//Agregar el usuario al grid de Invitados
						newId = (new Date()).valueOf();
						while (newId == lastId)
						{
							newId = (new Date()).valueOf();
						}
						guestUsersGrid.addRow(newId, movingUsers[user].name);
						guestUsersGrid.setUserData(newId, "id", movingUsers[user].name);
						lastId = newId;
					}
				}
				objAdminSuscritpionsGrid.sortRows(0);
				this.sortRows(0);
				guestUsersGrid.sortRows(0);
				updateGridExcess(objAdminSuscritpionsGrid, MaxAdmins);
				objFormTabs.tabs(tabAdminSuscriptions).progressOff();
			});

			//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
			objAdminSuscritpionsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objAdminSuscritpionsGrid.onDrop User');
				objFormTabs.tabs(tabAdminSuscriptions).progressOn();
				if (sObj == tObj) {
					this.sortRows(0);
					objFormTabs.tabs(tabAdminSuscriptions).progressOff();
					return;
				}
				var arrSIds = sId.split(',');
				if (arrSIds.length > AvailableAdmins)
				{
					alert("<?=translate('You do not have enough available Administrator Suscriptions to perform this operation')?>");
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objAdminUsersGrid);
						usersCount--;
					}
					objAdminUsersGrid.sortRows(0);
					this.sortRows(0);
					objFormTabs.tabs(tabAdminSuscriptions).progressOff();
					return;
				}
				var movingUsers = new Object();
				for (var intSIdIndex in arrSIds)
				{
					movingUsers[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
				}
				var updated = updateSuscriptions(movingUsers, 'add', 'admin');
				if (!updated)
				{
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objAdminUsersGrid);
						usersCount--;
					}
				}
				else
				{
					AvailableAdmins -= arrSIds.length;
					objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Available').':'?>"+AvailableAdmins+")");
					for (var user in movingUsers)
					{
						//Buscar el usuario en el grid de invitados y eliminarlo
						var resultCells = guestUsersGrid.findCell(movingUsers[user].name, 0, true);
						if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
						{
							guestUsersGrid.deleteRow(resultCells[0][0]);
						}
					}
				}
				objAdminUsersGrid.sortRows(0);
				this.sortRows(0);
				guestUsersGrid.sortRows(0);
				objFormTabs.tabs(tabAdminSuscriptions).progressOff();
			});

			//JAPR 2015-07-15: Agregado el evento del doble click
			objAdminUsersGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objAdminUsersGrid.onRowDblClicked User');
				objFormTabs.tabs(tabAdminSuscriptions).progressOn();
				if (AvailableAdmins == 0 && MaxAdmins != -1)
				{
					alert("<?=translate('You do not have enough available Administrator Suscriptions to perform this operation')?>");
					objFormTabs.tabs(tabAdminSuscriptions).progressOff();
					return;
				}
				var movingUsers = new Object();
				movingUsers[rId] = {id:rId, name:objAdminUsersGrid.cells(rId, cInd).getValue()};
				var updated = updateSuscriptions(movingUsers, 'add', 'admin');
				if (updated)
				{
					this.moveRow(rId, "row_sibling", undefined, objAdminSuscritpionsGrid);
					AvailableAdmins -= 1;
					var label = "<?=translate('Suscripted Administrators')?>";
					if (MaxAdmins != -1)
					{
						label = label + "<?=' ('.translate('Available').':'?>" + AvailableAdmins + ")";
					}
					objAdminSuscritpionsGrid.setColumnLabel(0, label);
					//Buscar el usuario en el grid de invitados y eliminarlo
					var resultCells = guestUsersGrid.findCell(movingUsers[rId].name, 0, true);
					if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
					{
						guestUsersGrid.deleteRow(resultCells[0][0]);
					}
				}
				objAdminSuscritpionsGrid.sortRows(0);
				this.sortRows(0);
				guestUsersGrid.sortRows(0);
				objFormTabs.tabs(tabAdminSuscriptions).progressOff();
			});

			//JAPR 2015-07-15: Agregado el evento del doble click
			objAdminSuscritpionsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objAdminSuscritpionsGrid.onRowDblClicked User');
				objFormTabs.tabs(tabAdminSuscriptions).progressOn();
				var movingUsers = new Object();
				movingUsers[rId] = {id:rId, name:objAdminSuscritpionsGrid.cells(rId, cInd).getValue()};
				var updated = updateSuscriptions(movingUsers, 'remove', 'admin');
				if (updated)
				{
					this.moveRow(rId, "row_sibling", undefined, objAdminUsersGrid);
					if (ExcessAdmins > 0)
					{
						ExcessAdmins -= 1;
						if (ExcessAdmins <= 0)
						{
							AvailableAdmins += (ExcessAdmins * -1);
							ExcessAdmins = 0;
							objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Available').':'?>"+AvailableAdmins+")");
						}
						else
						{
							objAdminSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Administrators').' ('.translate('Excess').':'?>"+ExcessAdmins+")");
						}
					}
					else
					{
						AvailableAdmins += 1;
						var label = "<?=translate('Suscripted Administrators')?>";
						if (MaxAdmins != -1)
						{
							label = label + "<?=' ('.translate('Available').':'?>" + AvailableAdmins + ")";
						}
						objAdminSuscritpionsGrid.setColumnLabel(0, label);
					}
					var newId;
					//Agregar el usuario al grid de Invitados
					newId = (new Date()).valueOf();
					guestUsersGrid.addRow(newId, movingUsers[rId].name);
					guestUsersGrid.setUserData(newId, "id", movingUsers[rId].name);
				}
				objAdminUsersGrid.sortRows(0);
				this.sortRows(0);
				guestUsersGrid.sortRows(0);
				updateGridExcess(objAdminSuscritpionsGrid, MaxAdmins);
				objFormTabs.tabs(tabAdminSuscriptions).progressOff();
			});

			adminUsersGrid = objAdminUsersGrid;

		}

		function generateGuestsSuscriptions(tabGuestSuscriptions)
		{	//Correcci�n #LK81NH: Se le estaba restando 1 al desplegar el n�mero m�ximo de invitados, 
			//se quito esa resta y se despliega lo que est� en $intMaxGuests
			var objFormData = [
				{type:"fieldset",  name:"fieldsGuestUsers", label:"<?=translate("Users")?>", width:380, className:"selection_list", list:[
					{type:"input", name:"txtSearchGuestUsers", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
					{type:"container", name:"gridGuestUsers", inputWidth:330, inputHeight:200, className:"selection_container"}
				]},
				{type:"newcolumn", offset:20},
				{type:"block", blockOffset:0, offsetTop:200, list:[
					{type:"button", name: "btnAddGuestUser", value:"", className:"assoc_button"},
					{type:"button", name: "btnRemoveGuestUser", value:"", className:"remove_button"}
				]},
				{type:"newcolumn", offset:20},
				{type:"fieldset",  name:"fieldsGuestSuscriptions", label:"<?=translate("Maximum Guests").":".(($intMaxGuests != -1)?$intMaxGuests:translate('Unlimited'))?>", width:380, className:"selection_list", list:[
					{type:"input", name:"txtSearchGuestSuscriptions", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
					{type:"container", name:"gridGuestSuscriptions", inputWidth:330, inputHeight:400, className:"selection_container"}
				]},
			];

			objFormTabs.tabs(tabGuestSuscriptions).attachObject("divGuestSuscriptionsParent");
			objGuestSuscriptions = new dhtmlXForm("divGuestSuscriptions", objFormData);
			$(objGuestSuscriptions.cont).addClass('customGrid');

			//Contenido de la lista de usuarios disponibles
			var objGuestUsersGrid = new dhtmlXGridObject(objGuestSuscriptions.getContainer("gridGuestUsers"));
			objGuestUsersGrid.setImagePath("<?=$strScriptPath?>/images/");
			objGuestUsersGrid.setHeader("<?=translate("Users")?>");
			objGuestUsersGrid.setInitWidthsP("100");
			objGuestUsersGrid.setColAlign("left");
			objGuestUsersGrid.setColTypes("ro");
			objGuestUsersGrid.enableEditEvents(false);
//			objGuestUsersGrid.enableDragAndDrop(true);
			objGuestUsersGrid.setColSorting("str");
			objGuestUsersGrid.enableMultiselect(true);
			objGuestUsersGrid.init();
			objGuestUsersGrid.sortRows(0,"str","asc");

			//Contenido de la lista de usuarios Invitados
			var objGuestSuscritpionsGrid = new dhtmlXGridObject(objGuestSuscriptions.getContainer("gridGuestSuscriptions"));
			objGuestSuscritpionsGrid.setImagePath("<?=$strScriptPath?>/images/");
			if (ExcessGuests > 0)
			{
				objGuestSuscritpionsGrid.setHeader("<?=translate('Suscripted Guests').' ('.translate('Excess').':'.$intExcessGuests.')'?>");
			}
			else
			{
				objGuestSuscritpionsGrid.setHeader("<?=translate('Suscripted Guests').(($intAvailableGuests != -1)?' ('.translate('Available').':'.$intAvailableGuests.')':'')?>");
			}
			objGuestSuscritpionsGrid.setInitWidthsP("100");
			objGuestSuscritpionsGrid.setColAlign("left");
			objGuestSuscritpionsGrid.setColTypes("ro");
			objGuestSuscritpionsGrid.enableEditEvents(false);
//			objGuestSuscritpionsGrid.enableDragAndDrop(true);
			objGuestSuscritpionsGrid.setColSorting("str");
			objGuestSuscritpionsGrid.enableMultiselect(true);
			objGuestSuscritpionsGrid.init();
			objGuestSuscritpionsGrid.sortRows(0,"str","asc");

			for (var i=0; i<UsersList.length; i++) {
				if (GuestSuscritpions.indexOf(UsersList[i]) == -1 && GuestSuscritpions.indexOf(UsersList[i]) == -1)
				{
					objGuestUsersGrid.addRow(i, UsersList[i]);
					objGuestUsersGrid.setUserData(i, "id", UsersList[i]);
				}
			}

			var countGuests = MaxGuests;
			for (var i=0; i<GuestSuscritpions.length; i++) {
				if (countGuests > 0 || MaxGuests == -1)
				{
					objGuestSuscritpionsGrid.addRow(i, GuestSuscritpions[i]);
					objGuestSuscritpionsGrid.setUserData(i, "id", GuestSuscritpions[i]);
					countGuests--;
				}
				else
				{
					objGuestSuscritpionsGrid.addRow(i, GuestSuscritpions[i]);
					objGuestSuscritpionsGrid.setUserData(i, "id", GuestSuscritpions[i]);
					objGuestSuscritpionsGrid.setRowTextStyle(i, "background-color: red;");
				}
			}

			objGuestUsersGrid.sortRows(0);
			objGuestSuscritpionsGrid.sortRows(0);

			objGuestSuscriptions.attachEvent("onKeyUp",function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				var objGrid = undefined;
				switch (name) {
					case "txtSearchGuestUsers":
						objGrid = objGuestUsersGrid;
						break;
					case "txtSearchGuestSuscriptions":
						objGrid = objGuestSuscritpionsGrid;
						break;
				}
				
				if (!objGrid) {
					return;
				}
				
				//Verifica si ya se hab�a aplicado esta b�squeda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterar�an el contenido)
				var strValue = inp.value;
				var strLastSearch = objGrid.getUserData(name, "lastSearch");
				if (strLastSearch == strValue) {
					return;
				}
				
				objGrid.setUserData(name, "lastSearch", strValue);
				objGrid.filterBy(0, strValue);
			});

			//JAPR 2015-07-15: Agregada la funcionalidad para mover los elementos entre grids
			objGuestSuscriptions.attachEvent("onButtonClick", function(name) {
				console.log('objGuestSuscriptions.onButtonClick '+name);
				objFormTabs.tabs(tabGuestSuscriptions).progressOn();
				switch (name) {
					case "btnAddGuestUser":
						var strSelection = objGuestUsersGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if ((arrSelection.length && arrSelection.length <= AvailableGuests)  || MaxGuests == -1) {
								var blnMove = false;
								var movingUsers = new Object();
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										movingUsers[strId] = {id:strId, name:objGuestUsersGrid.cells(strId, 0).getValue()};
									}
								}
								
								if (blnMove) {
									var updated = updateSuscriptions(movingUsers, 'add', 'guest');
									if (updated)
									{
										for (var user in movingUsers)
										{
											objGuestUsersGrid.moveRow(user, "row_sibling", undefined, objGuestSuscritpionsGrid);
											//Buscar el usuario en el grid de invitados y eliminarlo
											var resultCells = adminUsersGrid.findCell(movingUsers[user].name, 0, true);
											if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
											{
												adminUsersGrid.deleteRow(resultCells[0][0]);
											}
										}
										AvailableGuests -= arrSelection.length;
										var label = "<?=translate('Suscripted Guests')?>";
										if (MaxGuests != -1)
										{
											label = label + "<?=' ('.translate('Available').':'?>" + AvailableGuests + ")";
										}
										objGuestSuscritpionsGrid.setColumnLabel(0, label);
									}
								}
							}
							else
							{
								if (arrSelection.length == 0)
								{
									alert("<?=translate('You must select at least one User')?>");
								}
								if (arrSelection.length > AvailableGuests)
								{
									alert("<?=translate('You do not have enough available Guest Suscriptions to perform this operation')?>");
								}
							}
						}
						break;
					
					case "btnRemoveGuestUser":
						var strSelection = objGuestSuscritpionsGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								var movingUsers = new Object();
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										movingUsers[strId] = {id:strId, name:objGuestSuscritpionsGrid.cells(strId, 0).getValue()};
									}
								}
								
								if (blnMove) {
									var updated = updateSuscriptions(movingUsers, 'remove', 'guest');
									if (updated)
									{
										var newId;
										var lastId;
										for (var user in movingUsers)
										{
											objGuestSuscritpionsGrid.moveRow(user, "row_sibling", undefined, objGuestUsersGrid);
											//Agregar el usuario al grid de Invitados
											newId = (new Date()).valueOf();
											while (newId == lastId)
											{
												newId = (new Date()).valueOf();
											}
											adminUsersGrid.addRow(newId, movingUsers[user].name);
											adminUsersGrid.setUserData(newId, "id", movingUsers[user].name);
											lastId = newId;
										}
										if (ExcessGuests > 0)
										{
											ExcessGuests -= arrSelection.length;
											if (ExcessGuests <= 0)
											{
												AvailableGuests += (ExcessGuests * -1);
												ExcessGuests = 0;
												objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Available').':'?>"+AvailableGuests+")");
											}
											else
											{
												objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Excess').':'?>"+ExcessGuests+")");
											}
										}
										else
										{
											AvailableGuests += arrSelection.length;
											var label = "<?=translate('Suscripted Guests')?>";
											if (MaxGuests != -1)
											{
												label = label + "<?=' ('.translate('Available').':'?>" + AvailableGuests + ")";
											}
											objGuestSuscritpionsGrid.setColumnLabel(0, label);
										}
									}
								}
							}
							else
							{
								if (arrSelection.length == 0)
								{
									alert("<?=translate('You must select at least one User')?>");
								}
							}
						}
						break;
				}
				objGuestUsersGrid.sortRows(0);
				objGuestSuscritpionsGrid.sortRows(0);
				adminUsersGrid.sortRows(0);
				updateGridExcess(objGuestSuscritpionsGrid, MaxGuests);
				objFormTabs.tabs(tabGuestSuscriptions).progressOff();
			});
			
			//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
			objGuestUsersGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objGuestUsersGrid.onDrop User');
				objFormTabs.tabs(tabGuestSuscriptions).progressOn();
				if (sObj == tObj) {
					this.sortRows(0);
					objFormTabs.tabs(tabGuestSuscriptions).progressOff();
					return;
				}
				var arrSIds = sId.split(',');
				var movingUsers = new Object();
				for (var intSIdIndex in arrSIds)
				{
					movingUsers[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
				}
				var updated = updateSuscriptions(movingUsers, 'remove', 'guest');
				if (!updated)
				{
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objGuestSuscritpionsGrid);
						usersCount--;
					}
				}
				else
				{
					if (ExcessGuests > 0)
					{
						ExcessGuests -= arrSIds.length;
						if (ExcessGuests <= 0)
						{
							AvailableGuests += (ExcessGuests * -1);
							ExcessGuests = 0;
							objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Available').':'?>"+AvailableGuests+")");
						}
						else
						{
							objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Excess').':'?>"+ExcessGuests+")");
						}
					}
					else
					{
						AvailableGuests += arrSIds.length;
						objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Available').':'?>"+AvailableGuests+")");
					}
					var newId;
					var lastId;
					for (var user in movingUsers)
					{
						//Agregar el usuario al grid de Invitados
						newId = (new Date()).valueOf();
						while (newId == lastId)
						{
							newId = (new Date()).valueOf();
						}
						adminUsersGrid.addRow(newId, movingUsers[user].name);
						adminUsersGrid.setUserData(newId, "id", movingUsers[user].name);
						lastId = newId;
					}
				}
				objGuestSuscritpionsGrid.sortRows(0);
				this.sortRows(0);
				adminUsersGrid.sortRows(0);
				updateGridExcess(objGuestSuscritpionsGrid, MaxGuests);
				objFormTabs.tabs(tabGuestSuscriptions).progressOff();
			});

			//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
			objGuestSuscritpionsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objGuestSuscritpionsGrid.onDrop User');
				objFormTabs.tabs(tabGuestSuscriptions).progressOn();
				if (sObj == tObj) {
					this.sortRows(0);
					objFormTabs.tabs(tabGuestSuscriptions).progressOff();
					return;
				}
				var arrSIds = sId.split(',');
				if (arrSIds.length > AvailableGuests)
				{
					alert("<?=translate('You do not have enough available Guest Suscriptions to perform this operation')?>");
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objGuestUsersGrid);
						usersCount--;
					}
					objGuestUsersGrid.sortRows(0);
					this.sortRows(0);
					objFormTabs.tabs(tabGuestSuscriptions).progressOff();
					return;
				}
				var movingUsers = new Object();
				for (var intSIdIndex in arrSIds)
				{
					movingUsers[arrSIds[intSIdIndex]] = {id:arrSIds[intSIdIndex], name:tObj.cells(arrSIds[intSIdIndex], 0).getValue()};
				}
				var updated = updateSuscriptions(movingUsers, 'add', 'guest');
				if (!updated)
				{
					//Regresar los elementos a la lista original
					var rowCount = this.getRowsNum();
					var usersCount = arrSIds.length;
					console.log('rowCount='+rowCount+' usersCount='+usersCount);
					for (var i=rowCount;usersCount>0 ; i--)
					{
						var rowId = this.getRowId(i-1);
						console.log('moving rowId='+rowId);
						this.moveRow(rowId, "row_sibling", undefined, objGuestUsersGrid);
						usersCount--;
					}
				}
				else
				{
					AvailableGuests -= arrSIds.length;
					objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Available').':'?>"+AvailableGuests+")");
					for (var user in movingUsers)
					{
						//Buscar el usuario en el grid de invitados y eliminarlo
						var resultCells = adminUsersGrid.findCell(movingUsers[user].name, 0, true);
						if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
						{
							adminUsersGrid.deleteRow(resultCells[0][0]);
						}
					}
				}
				objGuestUsersGrid.sortRows(0);
				this.sortRows(0);
				adminUsersGrid.sortRows(0);
				objFormTabs.tabs(tabGuestSuscriptions).progressOff();
			});

			//JAPR 2015-07-15: Agregado el evento del doble click
			objGuestUsersGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objGuestUsersGrid.onRowDblClicked User');
				objFormTabs.tabs(tabGuestSuscriptions).progressOn();
				if (AvailableGuests == 0 && MaxGuests != -1)
				{
					alert("<?=translate('You do not have enough available Guest Suscriptions to perform this operation')?>");
					objFormTabs.tabs(tabGuestSuscriptions).progressOff();
					return;
				}
				var movingUsers = new Object();
				movingUsers[rId] = {id:rId, name:objGuestUsersGrid.cells(rId, cInd).getValue()};
				var updated = updateSuscriptions(movingUsers, 'add', 'guest');
				if (updated)
				{
					this.moveRow(rId, "row_sibling", undefined, objGuestSuscritpionsGrid);
					AvailableGuests -= 1;
					var label = "<?=translate('Suscripted Guests')?>";
					if (MaxGuests != -1)
					{
						label = label + "<?=' ('.translate('Available').':'?>" + AvailableGuests + ")";
					}
					objGuestSuscritpionsGrid.setColumnLabel(0, label);
					//Buscar el usuario en el grid de invitados y eliminarlo
					var resultCells = adminUsersGrid.findCell(movingUsers[rId].name, 0, true);
					if (typeof(resultCells) == 'object' && typeof(resultCells[0]) == 'object' && typeof(resultCells[0][0]) != 'undefined')
					{
						adminUsersGrid.deleteRow(resultCells[0][0]);
					}
				}
				objGuestSuscritpionsGrid.sortRows(0);
				this.sortRows(0);
				adminUsersGrid.sortRows(0);
				objFormTabs.tabs(tabGuestSuscriptions).progressOff();
			});

			//JAPR 2015-07-15: Agregado el evento del doble click
			objGuestSuscritpionsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objGuestSuscritpionsGrid.onRowDblClicked User');
				objFormTabs.tabs(tabGuestSuscriptions).progressOn();
				var movingUsers = new Object();
				movingUsers[rId] = {id:rId, name:objGuestSuscritpionsGrid.cells(rId, cInd).getValue()};
				var updated = updateSuscriptions(movingUsers, 'remove', 'guest');
				if (updated)
				{
					this.moveRow(rId, "row_sibling", undefined, objGuestUsersGrid);
					if (ExcessGuests > 0)
					{
						ExcessGuests -= 1;
						if (ExcessGuests <= 0)
						{
							AvailableGuests += (ExcessGuests * -1);
							ExcessGuests = 0;
							objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Available').':'?>"+AvailableGuests+")");
						}
						else
						{
							objGuestSuscritpionsGrid.setColumnLabel(0, "<?=translate('Suscripted Guests').' ('.translate('Excess').':'?>"+ExcessGuests+")");
						}
					}
					else
					{
						AvailableGuests += 1;
						var label = "<?=translate('Suscripted Guests')?>";
						if (MaxGuests != -1)
						{
							label = label + "<?=' ('.translate('Available').':'?>" + AvailableGuests + ")";
						}
						objGuestSuscritpionsGrid.setColumnLabel(0, label);
					}
					var newId;
					//Agregar el usuario al grid de Invitados
					newId = (new Date()).valueOf();
					adminUsersGrid.addRow(newId, movingUsers[rId].name);
					adminUsersGrid.setUserData(newId, "id", movingUsers[rId].name);
				}
				objGuestUsersGrid.sortRows(0);
				this.sortRows(0);
				adminUsersGrid.sortRows(0);
				updateGridExcess(this, MaxGuests);
				objFormTabs.tabs(tabGuestSuscriptions).progressOff();
			});

			guestUsersGrid = objGuestUsersGrid;

		}

		//Actualizar los usuarios marcados como Excedentes
		function updateGridExcess(objGrid, maxAvailable)
		{
			var rowCount = objGrid.getRowsNum();
			for (var i=0;i<rowCount ; i++)
			{
				var rowId = objGrid.getRowId(i);
				if (i < maxAvailable || maxAvailable == -1)
				{
					objGrid.setRowTextStyle(rowId, "");
				}
				else
				{
					objGrid.setRowTextStyle(rowId, "background-color: red;");
				}
			}
		}

		//Grabado de las suscripciones
		function updateSuscriptions(movingUsers, operation, suscritpionType)
		{
			console.log('updateSuscriptions: '+operation);
			var tabSelected;
			if (suscritpionType == 'admin')
			{
				tabSelected = tabAdminSuscriptions;
			}
			else
			{
				tabSelected = tabGuestSuscriptions
			}
//			objFormTabs.tabs(tabSelected).progressOn();
			wsResult = true;
			var strMovingUsers = encodeURIComponent(JSON.stringify(movingUsers));
			var strParams = strMovingUsers;
			if (operation == 'add')
			{
				var action = 'suscribe';
			}
			else
			{
				var action = 'unsuscribe';
			}
			//@AAL En la parte de grabado de un Usuario se requiere de invocar primero el archivo uploadUsersFile, ya que guarda ciertos campos a la tabla de usuarios
//			window.dhx4.ajax.post("uploadUsersFile.php", "txtNewUserData=" + strParams, function(strParams){
			//GCRUZ 2015-11-04. Cambiar m�todo de agregar usuarios, ahora usar el webservice de kpi
			response = window.dhx4.ajax.postSync("kpiWebServiceEsurveyClient.php?action="+action+"&suscriptionType="+suscritpionType, "txtUsersData=" + strMovingUsers);
			if (response.xmlDoc.responseText.search('kpiWebService.Success') == -1)
			{
				alert(response.xmlDoc.responseText);
				wsResult = false;
			}

//			objFormTabs.tabs(tabSelected).progressOff();
			return wsResult;
		}

		/* Permite hacer ajustes a la pantalla donde DHTMLX no lo hace correctamente */
		function doOnResizeScreen() {
			console.log('doOnResizeScreen');
			//Para permitir que se pinten correctamente las ventanas de Invitar usuario y grupos, es necesario ocultar moment�neamente los divs que las contienen
			//ya que DHTMLX no estaba redimensionando sus componentes al maximizar la ventana
			var intTopAdjust = 120;
			//$('#divInviteUsers .selection_container div.dhxform_container').height($(window).height() - intTopAdjust - 50);
			var intHeight = $(window).height();
			$('.selection_container div.dhxform_container').height(intHeight - intTopAdjust - 50);
			$('.selection_container div.objbox').height(intHeight - intTopAdjust - 90);
			$('.selection_list fieldset.dhxform_fs').height(intHeight - intTopAdjust);
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal
//				if (objMainLayout) {
//					objMainLayout.setSizes();
//				}
//				if (objFormTabs) {
//					objFormTabs.setSizes();
//				}
			//JAPRWarning: Remover esto para regresar a Tabs como objeto principal (End)
		}

		function addClassPrototype() {
			if (!objSetttings) {
				return;
			}
			
			$.extend(objSetttings, new SuscriptionsCls());
		}
<?
?>
	</script>
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<div id="divAdminSuscriptionsParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divAdminSuscriptions" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divGuestSuscriptionsParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divGuestSuscriptions" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignHeader" class="linkToolbar" style="height:30px;width:100%">
				<table style="/*width:100%;*/height:100%;">
					<tbody>
						<tr>
						<td class="linkTD" tabName="adminSuscriptions" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Administrators")?></td>
						<td class="linkTD" tabName="guestSuscriptions" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Guests")?></td>
						<td class="linkTD" tabName="save" style="text-decoration: none !important; cursor:auto;width:150px;">
							<div id="divSaving" class="dhx_cell_progress_img" style="display:none;position:relative !important;background-size:20px 20px;">&nbsp;</div>
							<div id="divSavingError" style="display:none;position:relative !important;background-size:20px 20px;cursor:pointer;" onclick="doShowErrors()">&nbsp;</div>
						</td>
						</tr>
				  </tbody>
				</table>
			</div>
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
	</body>
</html>
<?
		die();
	}
}

?>