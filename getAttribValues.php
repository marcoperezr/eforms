<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$surveyID = 0;

if (array_key_exists("SurveyID", $_GET))
{
	$surveyID = $_GET["SurveyID"];
}

$catalogID = "";

if (array_key_exists("CatalogID", $_GET))
{
	$catalogID = $_GET["CatalogID"];
}

$catMemberID = 0;

if (array_key_exists("CatMemberID", $_GET))
{
	$catMemberID = $_GET["CatMemberID"];
}

$changedMemberID = 0;

if (array_key_exists("ChangedMemberID", $_GET))
{
	$changedMemberID = $_GET["ChangedMemberID"];
}

$strMemberIDs="";
if (array_key_exists("strMemberIDs", $_GET))
{
	$strMemberIDs = $_GET["strMemberIDs"];
}

$strParentIDs="";
if (array_key_exists("strParentIDs", $_GET))
{
	$strParentIDs = $_GET["strParentIDs"];
}

$strValues="";
if (array_key_exists("strValues", $_GET))
{
	$strValues = $_GET["strValues"];
}

//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
//Con este parámetro se permitirá extraer siempre todos los valores del nivel pedido sin aplicar filtros en los padres, pero sin enviar sus 
//valores como 'sv_ALL_sv' sino simplemente no enviar nada de ellos
$intIgnoreFilters = 0;
if (array_key_exists("noFilters", $_GET))
{
	$intIgnoreFilters = (int) @$_GET["noFilters"];
}
//@JAPR

include_once("report.inc.php");
$catTableName = BITAMReportCollection::getCatTableName($theRepository, $catalogID);
$filterAttributes = BITAMReportCollection::getAttributes($theRepository, $catalogID, $catMemberID);
$changedMemberOrder = (int) @$filterAttributes[$changedMemberID]["MemberOrder"];

$memberIDs = explode('_SVSEPSV_', $strMemberIDs);
$values = explode('_SVSEPSV_', $strValues);
$numMemberIDs=count($memberIDs);

$filterValues=array();

for($i=0; $i<$numMemberIDs; ++$i)
{
	$filterValues[$memberIDs[$i]] = $values[$i];
}

$strFilter="";
$attribValues = array();

//@JAPR 2014-05-15: Agregados los catálogos tipo eBavel
$objCatalog = BITAMCatalog::NewInstanceWithID($theRepository, $catalogID);
if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
	//En este caso se trata de un catálogo mapeado desde eBavel, en lugar de reutilizar BITAMReportCollection como se hace en catálogos propios, se
	//utilizará la funcionalidad de catalogFilter, donde ya se había implementado el método casi idéntico a lo requerido aqui, así de esa forma no
	//se duplicará código como sucedió con BITAMReportCollection, ya que el mismo tipo de consulta en catálogos normales también estaba en catalogFilter
	$catMemberOrder = $filterAttributes[$catMemberID]["MemberOrder"];
	$aCatalogFilter = BITAMCatalogFilter::NewInstance($theRepository, $catalogID, $intIgnoreFilters);
	//Genera las propiedades con los filtros aplicados hasta el momento
	foreach ($filterAttributes as $memberID => $memberInfo)
	{
		if (getParamValue('isAgenda', 'both', '(int)') && !((int) @$memberInfo["KeyOfAgenda"])) {
			continue;
		}
	
		if($memberInfo["MemberOrder"]<=$changedMemberOrder)
		{
			//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
			if(!$intIgnoreFilters && $filterValues[$memberID]!='sv_ALL_sv')
			{
				$strAttributeFieldName = 'DSC_'.$memberInfo['ParentID'];
				$aCatalogFilter->$strAttributeFieldName = $filterValues[$memberID];
			}
		}
		else
		{
			//Obtiene los valores utilizando la instancia del filtro
			//@JAPR 2014-11-14: Corregido un bug, el valor que se va a obtener es el siguiente al atributo modificado, no el último del total
			//de atributo solicitados
			if (getParamValue('isAgenda', 'both', '(int)')) {
				$attribValues[$memberID] = $aCatalogFilter->getCatalogValues($catMemberOrder-1);
			} else {
				$attribValues[$memberID] = $aCatalogFilter->getCatalogValues($changedMemberOrder);
			}
			unset($attribValues[$memberID]['sv_ALL_sv']);
			break;
		}
	}
}
else {
	foreach ($filterAttributes as $memberID => $memberInfo)
	{
		if (getParamValue('isAgenda', 'both', '(int)') && !((int) @$memberInfo["KeyOfAgenda"])) {
			continue;
		}
		
		if($memberInfo["MemberOrder"]<=$changedMemberOrder)
		{
			//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
			if(!$intIgnoreFilters && isset($filterValues[$memberID]) && $filterValues[$memberID]!='sv_ALL_sv')
			{
				$strFilter.= " AND ".$memberInfo["FieldName"]." = ".$theRepository->DataADOConnection->Quote($filterValues[$memberID]);
			}
		}
		else
		{
			$attribValues[$memberID]=BITAMReportCollection::getAttribValues($theRepository, $catTableName, $memberInfo["FieldName"], $strFilter);
			/*
			//Se toma el primer valor para considerarlo en el filtro para la obtención de valores del siguiente atributo a procesar.
			foreach ($attribValues[$memberID] as $key=>$value)
			{
				$filterValue = $value;
				break;
			}
	
			$strFilter.= " AND ".$memberInfo["FieldName"]." = ".$theRepository->DataADOConnection->Quote($filterValue);
			*/
			break;
		}
	}
}
//@JAPR
$strResult="";

foreach ($attribValues as $memberID => $values) 
{
	if($strResult!="")
	{
		$strResult.="_SV_AT_SV_";
	}
	
	$strResult.=$memberID."_SV_ID_SV_";
	$strResult.=implode("_SV_VAL_SV_", $values);
}

header('Content-Type: text/plain; charset=utf-8');

echo($strResult);

?>