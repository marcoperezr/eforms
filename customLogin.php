<?php

global $strAppUserName;
global $strAppUserPwd;
global $strAppUserLang;
global $strApplicationDefaultSurveyID;
//$strAppUserName = 'jpuente497@bitam.com';
//$strAppUserPwd = 'bitam';
$strAppUserLang = 'sp';
//@JAPR 2016-01-14: Agregado el parámetro con la forma default a cargar, así como la obtención de parámetros desde el propio request (#C8ZPCN)
//$strApplicationDefaultSurveyID = 244;

if (isset($_REQUEST['User'])) {
	$strAppUserName = trim(@$_REQUEST['User']);
}
if (isset($_REQUEST['Password'])) {
	$strAppUserPwd = trim(@$_REQUEST['Password']);
}
if (isset($_REQUEST['Language'])) {
	$strAppUserLang = trim(@$_REQUEST['Language']);
}
if (isset($_REQUEST['SurveyID'])) {
	$strApplicationDefaultSurveyID = (int) @$_REQUEST['SurveyID'];
}
//@JAPR

require_once('indexHTML.inc.php');

?>