<?php 

require_once("repository.inc.php");
require_once("usergroup.inc.php");
require_once("indicatorgroup.inc.php");
require_once("../model_manager/modeldata.inc.php");

class BITAMUserGroupIndicatorGroupCollection extends BITAMCollection
{
	public $UserGroupID;
	
	function __construct($aRepository, $aUserGroupID)
	{
		BITAMCollection::__construct($aRepository);
		$this->UserGroupID = $aUserGroupID;
	}

	static function NewUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID, $anArrayOfIndicatorGroupIDs = null)
	{
		$anInstance = new BITAMUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID);

		$filter = "";
		if (!is_null($anArrayOfIndicatorGroupIDs))
		{
			switch (count($anArrayOfIndicatorGroupIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.CLA_GPO_IND = ".((int) $anArrayOfIndicatorGroupIDs[0]);
					break;
				default:
					foreach ($anArrayOfIndicatorGroupIDs as $aIndicatorGroupID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aIndicatorGroupID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.CLA_GPO_IND IN (".$filter.")";
					}
					break;
			}
		}
		
		$sql = 		
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID".
		"		, t1.NOM_GPO_IND AS IndicatorGroupName".
		"		, t2.NOM_CORTO OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2, SI_ROL_IND t3 ".
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER ".
		"		and t3.CLA_GPO_IND = t1.CLA_GPO_IND ".
		"		and t3.CLA_ROL = ".$aUserGroupID." ".$filter;

		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewUserGroupIndicatorGroupCollectionForAssociation($aRepository, $aUserGroupID)
	{
		$anInstance = new BITAMUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID);

		$sql = 		
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID".
		"		, t1.NOM_GPO_IND AS IndicatorGroupName".
		"		, t2.NOM_CORTO OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2 ".
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER ";

		
// @EA 2010-02-04 Checar en tabla de modelos deshabilitados 
		if (InModeSAAS())
		{
			if ($anInstance->Repository->ADOConnection->Execute('select count(CLA_CONCEPTO) from SI_CONCEPTO_DESHAB'))
			{
				$sqlGruposDeIndicadores = 'select LLAVES as grupo_indicador_id from SI_CONCEPTO where not LLAVES is NULL and RI_DEFINITION = 6 and CLA_CONCEPTO in ( select CLA_CONCEPTO from SI_CONCEPTO_DESHAB )';
				$aRSGruposDeIndicadores = $anInstance->Repository->ADOConnection->Execute($sqlGruposDeIndicadores);
				if ($aRSGruposDeIndicadores)
				{
					$sListaGruposDeIndicadoresDeshabilitados = '0';
					while (!$aRSGruposDeIndicadores->EOF)
					{
						$sListaGruposDeIndicadoresDeshabilitados .= ','.trim($aRSGruposDeIndicadores->fields['grupo_indicador_id']);
						$aRSGruposDeIndicadores->MoveNext();
					}
					$sql .=	' and NOT t1.CLA_GPO_IND in ( '.$sListaGruposDeIndicadoresDeshabilitados.' )';
				}
				
			}
		}
//		
		$sql .= ' ORDER BY 2';
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewUserGroupIndicatorGroupCollectionForSaving($aRepository, $aUserGroupID, $anArrayOfIndicatorGroupIDs)
	{
		$anInstance = new BITAMUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID);

		$filter = "";
		switch (count($anArrayOfIndicatorGroupIDs))
		{
			case 0:
				break;
			case 1:
				$filter = "AND t1.CLA_GPO_IND = ".((int) $anArrayOfIndicatorGroupIDs[0]);
				break;
			default:
				foreach ($anArrayOfIndicatorGroupIDs as $aIndicatorGroupID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aIndicatorGroupID; 
				}
				if ($filter != "")
				{
					$filter = "AND t1.CLA_GPO_IND IN (".$filter.")";
				}
				break;
		}

		$sql = 		
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID".
		"		, t1.NOM_GPO_IND AS IndicatorGroupName".
		"		, t2.NOM_CORTO OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2 ".
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER ".
		$filter.
		"  		AND  t1.CLA_GPO_IND NOT IN ".
		"         (SELECT t3.CLA_GPO_IND ".
		"            FROM SI_ROL_IND t3 ".
		"            WHERE  t3.CLA_ROL = ".$aUserGroupID.") ".
		" ORDER BY 2";		
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("UserGroupID", $aHTTPRequest->GET))
		{
			$action = array_key_exists("action", $aHTTPRequest->GET) ? $aHTTPRequest->GET["action"] : "";
			$aUserGroupID = $aHTTPRequest->GET["UserGroupID"];
			if (array_key_exists("IndicatorGroupID", $aHTTPRequest->POST))
			{
				$aIndicatorGroupID = $aHTTPRequest->POST["IndicatorGroupID"];
				if (is_array($aIndicatorGroupID))
				{
					$anArrayOfIndicatorGroupIDs = $aIndicatorGroupID;
					switch ($action)
					{
						case 'associate':
							$filter = "";
							switch (count($anArrayOfIndicatorGroupIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_GPO_IND <> ".((int) $anArrayOfIndicatorGroupIDs[0]);
									break;
								default:
									foreach ($anArrayOfIndicatorGroupIDs as $aIndicatorGroupID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aIndicatorGroupID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_GPO_IND NOT IN (".$filter.")";
									}
									break;
							}
							$sql = "DELETE FROM SI_ROL_IND WHERE CLA_ROL = ".((int) $aUserGroupID).$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die("Error accessing SI_ROL_IND table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
							}

							$aCollection = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollectionForSaving($aRepository, $aUserGroupID, $anArrayOfIndicatorGroupIDs);
							foreach ($aCollection as $anInstanceToAssociate)
							{
								$anInstanceToAssociate->saveAssociation();
							}
							
							$sql =
							'delete  from SI_ROL_CONFIG '.
							'where   CLA_ROL = '.$aUserGroupID.' and NOT CLA_CONCEPTO in '.
							'(select  CLA_CONCEPTO '.
							' from    SI_INDICADOR '.
							' where	  CLA_INDICADOR in '.
							'(select	CLA_INDICADOR '.
							' from 	    SI_GPO_IND_DET '.
							' where	    CLA_GPO_IND in '.
							'(select CLA_GPO_IND from SI_ROL_IND WHERE CLA_ROL = '.$aUserGroupID.')))';
					
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die("Error accessing SI_ROL_CONFIG table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
							}
							
							ExportRolToPHP($aRepository, (int) $aUserGroupID);
								
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
						case 'dissociate':
							$filter = "";
							switch (count($anArrayOfIndicatorGroupIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_GPO_IND = ".((int) $anArrayOfIndicatorGroupIDs[0]);
									break;
								default:
									foreach ($anArrayOfIndicatorGroupIDs as $aIndicatorGroupID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aIndicatorGroupID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_GPO_IND IN (".$filter.")";
									}
									break;
							}
							if ($filter != "")
							{
								$sql = "DELETE FROM SI_ROL_IND WHERE CLA_ROL = ".((int) $aUserGroupID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die("Error accessing SI_ROL_IND table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
								}
								
								$sql =
								'delete  from SI_ROL_CONFIG '.
								'where   CLA_ROL = '.$aUserGroupID.' and NOT CLA_CONCEPTO in '.
								'(select  CLA_CONCEPTO '.
								' from    SI_INDICADOR '.
								' where	  CLA_INDICADOR in '.
								'(select	CLA_INDICADOR '.
								' from 	    SI_GPO_IND_DET '.
								' where	    CLA_GPO_IND in '.
								'(select CLA_GPO_IND from SI_ROL_IND WHERE CLA_ROL = '.$aUserGroupID.')))';
						
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die("Error accessing SI_ROL_CONFIG table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
								}
								
								ExportRolToPHP($aRepository, (int) $aUserGroupID);
								
							}
							$aCollection = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID);
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
					}
					return null;
				}
			}
			$anInstance = null;
			switch ($action)
			{
				case 'associate':
				case 'dissociate':
					$aHTTPRequest->RedirectTo = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID);
					break;
				default:
					$anInstance = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollectionForAssociation($aRepository, $aUserGroupID);
					break;
			}
			return $anInstance;
		}
		return null;
	}
	
	function erase_Security($aUserGroupID)
	{
// Elimina problemas de integridad con la seguridad de roles
		$sql =
		'delete  from SI_ROL_CONFIG '.
		'where   CLA_ROL = '.$aUserGroupID.' and NOT CLA_CONCEPTO in '.
		'(select  CLA_CONCEPTO '.
		' from    SI_INDICADOR '.
		' where	  CLA_INDICADOR in '.
		'(select	CLA_INDICADOR '.
		' from 	    SI_GPO_IND_DET '.
		' where	    CLA_GPO_IND in '.
		'(select CLA_GPO_IND from SI_ROL_IND WHERE CLA_ROL = '.$aUserGroupID.')))';

		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_CONFIG table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
	}
	
	function get_AssociatedIDs()
	{
		$anArray = array();

		$sql = 
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID, t1.NOM_GPO_IND AS IndicatorGroupName ".
		"FROM 	SI_GPO_IND t1, SI_ROL_IND t2 ".
		"WHERE	t2.CLA_GPO_IND = t1.CLA_GPO_IND ".
		"  AND  t2.CLA_ROL = ".((int) $this->UserGroupID).
		" ORDER BY 2";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anArray[(int) $aRS->fields["indicatorgroupid"]] = strtoupper(rtrim($aRS->fields["indicatorgroupname"]));
			$aRS->MoveNext();
		}
		
		return $anArray;
	}
	
	function get_Parent()
	{
		return BITAMUserGroup::NewUserGroupWithID($this->Repository, $this->UserGroupID);
	}
	
	function get_Image()
	{
		return "<img src=images/models.gif>";
	}
	
	function get_Title()
	{
		return translate("Models");
	}

	function get_QueryString()
	{
		return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=UserGroupIndicatorGroupCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=UserGroupIndicatorGroup&UserGroupID=".$this->UserGroupID;
	}
	
	function get_AssociateDissociateQueryString()
	{
		return "BITAM_SECTION=UserGroupIndicatorGroupCollection&UserGroupID=".$this->UserGroupID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorGroupID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorGroupName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;
			$myFields[] = $aField;		
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}	
		
}

class BITAMUserGroupIndicatorGroup extends BITAMObject
{
	public $UserGroupID;
	public $IndicatorGroup;
	
	
	function __construct($aRepository, $aUserGroupID)
	{
		BITAMObject::__construct($aRepository);
		$this->UserGroupID = (int) $aUserGroupID;
		$this->IndicatorGroup = BITAMIndicatorGroup::NewIndicatorGroup($aRepository);
/*		
		$sql = 
		"select REF_CONFIGURA AS pathArtusWEB ".
		"from	SI_CONFIGURA ".
		"where  CLA_CONFIGURA = 916 and CLA_USUARIO = -2";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$gsURLWEBHTML = rtrim($aRS->fields['pathartusweb']);
		}
*/
		unset ($aRS);
	}

	static function NewUserGroupIndicatorGroup($aRepository, $aUserGroupID)
	{
		return new BITAMUserGroupIndicatorGroup($aRepository, $aUserGroupID);
	}

	static function NewUserGroupIndicatorGroupWithUserID($aRepository, $aUserGroupID, $aIndicatorGroupID)
	{
		
		$anInstance = null;
		if (((int) $aUserGroupID) < 0)
		{
			return $anInstance;
		}
		if (((int) $aIndicatorGroupID) < 0)
		{
			return $anInstance;
		}
		
		$sql = 		
		"SELECT t1.CLA_GPO_IND AS IndicatorGroupID".
		"		, t1.NOM_GPO_IND AS IndicatorGroupName".
		"		, t2.NOM_CORTO OwnerName ".
		"FROM 	SI_GPO_IND t1, SI_USUARIO t2, SI_ROL_IND t3 ".
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER ".
		"  AND  t3.CLA_GPO_IND = t1.CLA_GPO_IND ".
		"  AND  t3.CLA_ROL = ".((int) $aUserGroupID).
		"  AND  t1.CLA_GPO_IND = ".((int) $aIndicatorGroupID);
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_GPO_IND table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupFromRS($aRepository, $aUserGroupID, $aRS);
		}
		return $anInstance;
	}

	static function NewUserGroupIndicatorGroupFromRS($aRepository, $aUserGroupID, $aRS)
	{
		$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroup($aRepository, $aUserGroupID);
		$anInstance->IndicatorGroup = BITAMIndicatorGroup::NewIndicatorGroupFromRS($aRepository, $aRS);
		
		$sql = 
		"select 	distinct t1.CLA_CONCEPTO AS CubeID, t4.CLA_INDICADOR AS IndicatorID ".
		"from		SI_CONCEPTO t1, SI_ROL_IND t2, SI_GPO_IND_DET t3, SI_INDICADOR t4 ".
		"where		t2.CLA_ROL = ".$aUserGroupID.
		"			and t2.CLA_GPO_IND = ".$anInstance->IndicatorGroup->IndicatorGroupID.
		"			and t3.CLA_GPO_IND = t2.CLA_GPO_IND".
		"			and t4.CLA_INDICADOR = t3.CLA_INDICADOR".
		"			and t4.CLA_CONCEPTO = t1.CLA_CONCEPTO";
		$aRSCube = $aRepository->ADOConnection->Execute($sql);
		if ($aRSCube === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		$theCubeID = -1;
		$theIndicatorID = -1;
		if (!$aRSCube->EOF)
		{
			$theCubeID = $aRSCube->fields['cubeid'];
			$theIndicatorID = $aRSCube->fields['indicatorid'];
		}
		
		$sql = 
		"select t1.NIVELES AS FilterLevels, t1.FILTRO AS FilterSQL ".
		"from	SI_ROL_CONFIG t1 ".
		"where	t1.CLA_ROL = ".$aUserGroupID." and ".
		"		t1.CLA_CONCEPTO = ".$theCubeID;
		$aRSFilter = $aRepository->ADOConnection->Execute($sql);			
		if ($aRSFilter === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$anInstance->IndicatorGroup->ModelID = $theCubeID;	
		$anInstance->IndicatorGroup->IndicatorID = $theIndicatorID;	
		
		$anInstance->IndicatorGroup->SecurityLevel = "";
		$anInstance->IndicatorGroup->SecurityFilter = "";
		
		if (!$aRSFilter->EOF)
		{
			$anInstance->IndicatorGroup->SecurityLevel = $aRSFilter->fields['filterlevels'];
			$anInstance->IndicatorGroup->SecurityFilter = $aRSFilter->fields['filtersql'];	
		}		
		
		//print '<BR>$anInstance->IndicatorGroup->SecurityLevel = '.$anInstance->IndicatorGroup->SecurityLevel;
		//print '<BR>$anInstance->IndicatorGroup->SecurityFilter = '.$anInstance->IndicatorGroup->SecurityFilter;		
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("UserGroupID", $aHTTPRequest->GET))
		{
			$aUserGroupID = (int) $aHTTPRequest->GET["UserGroupID"];
			if (array_key_exists("IndicatorGroupID", $aHTTPRequest->POST))
			{
				$aIndicatorGroupID = $aHTTPRequest->POST["IndicatorGroupID"];
				if (is_array($aIndicatorGroupID))
				{
					$aCollection = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollection($aRepository, $aUserGroupID, $aIndicatorGroupID);
					foreach ($aCollection as $anInstanceToRemove)
					{
						$anInstanceToRemove->remove();
					}
					$aHTTPRequest->RedirectTo = $aCollection;
				}
				else
				{
					$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupWithUserID($aRepository, $aUserGroupID, $aIndicatorGroupID);
					if (is_null($anInstance))
					{
						$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroup($aRepository, $aUserGroupID);
					}
					$anInstance->updateFromArray($aHTTPRequest->GET);
					$anInstance->updateFromArray($aHTTPRequest->POST);
					$anInstance->save();
					if (array_key_exists("go", $_POST))
					{
						$go = $_POST["go"];
					}
					else
					{
						$go = "self";
					}
					if ($go == "parent")
					{
						$anInstance = $anInstance->get_Parent();
					}
					else
					{
						if ($go == "new")
						{
							$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroup($aRepository, $aUserGroupID);
						}
					}
					$aHTTPRequest->RedirectTo = $anInstance;
				}
				return null;
			}
			$anInstance = null;
			if (array_key_exists("IndicatorGroupID", $aHTTPRequest->GET))
			{
				$aIndicatorGroupID = $aHTTPRequest->GET["IndicatorGroupID"];
				$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroupWithUserID($aRepository, $aUserGroupID, $aIndicatorGroupID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroup($aRepository, $aUserGroupID);
				}
			}
			else
			{
				$anInstance = BITAMUserGroupIndicatorGroup::NewUserGroupIndicatorGroup($aRepository, $aUserGroupID);
			}
			return $anInstance;
		}
		return null;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("UserGroupID", $anArray))
		{
			$this->UserGroupID = (int) $anArray["UserGroupID"];
		}
		
		$this->IndicatorGroup->updateFromArray($anArray);
		
 		if (array_key_exists("SecurityLevel", $anArray))
		{
			$this->IndicatorGroup->SecurityLevel = stripslashes($anArray["SecurityLevel"]);
		}
 		if (array_key_exists("SecurityFilter", $anArray))
		{
			$this->IndicatorGroup->SecurityFilter = stripslashes($anArray["SecurityFilter"]);
		}				
 		if (array_key_exists("IndicatorID", $anArray))
		{
			$this->IndicatorGroup->IndicatorID = stripslashes($anArray["IndicatorID"]);
		}						
 		if (array_key_exists("ModelID", $anArray))
		{
			$this->IndicatorGroup->ModelID = stripslashes($anArray["ModelID"]);
		}						
		
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			$this->IndicatorGroup->save();
			$this->saveAssociation();
		}
		else
		{
			$this->IndicatorGroup->save();
			$this->SaveSecurity();
		}
		return $this;
	}

	function SaveSecurity()
	{
		$sql = 
		"select CLA_CONCEPTO AS CubeID from SI_INDICADOR where CLA_INDICADOR = ".$this->IndicatorGroup->IndicatorID;
		
		$aRSCube = $this->Repository->ADOConnection->Execute($sql);
		if ($aRSCube === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		$CubeID = -1;
		if (!$aRSCube->EOF)
		{
			$CubeID = $aRSCube->fields['cubeid'];
		}
		
		$sql = 
		"select count(*) AS countconfig from SI_ROL_CONFIG ".
		"where	CLA_CONCEPTO = ".$CubeID.
		"		and CLA_ROL = ".$this->UserGroupID;
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}		
		
		if ($aRS->fields['countconfig'] > 0)
		{		
			$sql = 
			"update SI_ROL_CONFIG ".
			"set	NIVELES = ".$this->Repository->ADOConnection->Quote($this->IndicatorGroup->SecurityLevel).
			"		,FILTRO = ".$this->Repository->ADOConnection->Quote($this->IndicatorGroup->SecurityFilter)." ".
			"where	CLA_CONCEPTO = ".$CubeID.
			"		and CLA_ROL = ".$this->UserGroupID;
			
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
		}
		else 
		{
			$sql = 
			"insert into SI_ROL_CONFIG ( CLA_ROL, PRIORIDAD, CLA_CONCEPTO, NIVELES, FILTRO ) values (".
			$this->UserGroupID.",1,".$CubeID.",".$this->Repository->ADOConnection->Quote($this->IndicatorGroup->SecurityLevel).
			",".$this->Repository->ADOConnection->Quote($this->IndicatorGroup->SecurityFilter).")";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}			
		}
		
		ExportRolToPHP($this->Repository, $this->UserGroupID);
		
	}
	
	function saveAssociation()
	{
		$sql = "INSERT INTO SI_ROL_IND (".
					"CLA_ROL".
					",CLA_GPO_IND".
					") VALUES (".
					((int) $this->UserGroupID).
					",".((int) $this->IndicatorGroup->IndicatorGroupID).
					")";
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		return $this;
	}

	function remove()
	{		
		$sql = "DELETE FROM SI_ROL_IND WHERE CLA_ROL = ".((int) $this->UserGroupID)." AND CLA_GPO_IND = ".((int) $this->IndicatorGroup->IndicatorGroupID);
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		$this->IndicatorGroup->remove();
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->IndicatorGroup->IndicatorGroupID < 0);
	}

	function get_Image()
	{
		return "<img src=images/models.gif>";
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New Indicator Group");
		}
		else
		{
			return $this->IndicatorGroup->IndicatorGroupName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=UserGroupIndicatorGroup&UserGroupID=".$this->UserGroupID;
		}
		else
		{
			return "BITAM_PAGE=UserGroupIndicatorGroup&UserGroupID=".$this->UserGroupID."&IndicatorGroupID=".$this->IndicatorGroup->IndicatorGroupID;
		}
	}

	function get_Parent()
	{
		return BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollection($this->Repository, $this->UserGroupID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorGroupID';
	}

	function get_FieldValueOf($aFieldName)
	{
		if ($aFieldName == 'UserGroupID')
    	{
    		$aFieldValue = $this->get_FieldValueOf($aFieldName);
    	}
    	else
    	{
    		$aFieldValue = $this->IndicatorGroup->get_FieldValueOf($aFieldName);
    	}
		return $aFieldValue;
	}
	
	/*
	function get_Children($aUser)
	{
		return $this->IndicatorGroup->get_Children($aUser);
	}
	*/		

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		
		global $gsURLWEBHTML;
		
		$gsURLWEBHTML = GetLocalURLWEBG6();
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorGroupName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SecurityFilter";
		$aField->Type = "String";
		$aField->Size = 500;
		$aField->IsVisible = false;
		
		savelogfile( '$this->IndicatorGroup->IndicatorID = '. $this->IndicatorGroup->IndicatorID);
		savelogfile( '$this->IndicatorGroup->ModelID = '. $this->IndicatorGroup->ModelID);
		
		if ($gsURLWEBHTML != '' and $this->IndicatorGroup->IndicatorID != -1)
		{
			$aField->Title = translate("Filters");
			$aField->AfterMessage = 
			"<img id=indFilterImg
			name=indFilterImg 
			style=\"display:none; cursor:pointer\"
			src=\"images/indfilter.gif\"
	 		onclick=\"javascript: selectFilter();\"><input type=\"text\"
			style=\"background-color:transparent; border:none; width:30px;
			font-family:verdana; font-size:13px;\" readonly name=\"txtIndFilter\"
			id=\"txtIndFilter\" value=\"\">";
/*
			$aField->AfterMessage = 
			"<input type=\"button\" name=\"pbfilter\" value=\"".translate("Filter")."...\" style=\"text-align:center;vertical-align:top;width:64px\" ".
			"onclick=\"javascript:selectFilter();\">";			
*/			
		}
				
		$myFields[$aField->Name] = $aField;				
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SecurityLevel";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 500;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;				
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorID";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 10;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;				
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ModelID";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 10;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$aField->IsDisplayOnly = true;
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
		{
			return true;
		}
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}		
	
	function generateBeforeFormCode($aUser)
	{
		
		global $gsURLWEBHTML;
		
		$gsURLWEBHTML = GetLocalURLWEBG6();
		
		// Is the user using HTTPS?
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
		
		$php_self = dirname($_SERVER['PHP_SELF']);
		$http_host = $_SERVER['HTTP_HOST'];
		
		// Complete the URL
		$url .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
		
		$path = $url."/ApplyIndFilter.php";
		
//		$pathFilterPage = "http://stratego1/artus/eis/";
		$pathFilterPage = $gsURLWEBHTML;
		
		$pathFilterPage = str_ireplace('localhost', $_SERVER['HTTP_HOST'], $pathFilterPage);

		savelogfile('$gsURLWEBHTML ->'.$gsURLWEBHTML);
		savelogfile('$php_self ->'.$php_self);
		savelogfile('$http_host ->'.$http_host);
		savelogfile('$pathFilterPage ->'.$pathFilterPage);
		
//		$pathFilterPage = "http://edgardesktop/artus/genvi/";
//		$pathFilterPage = "http://demodmf/artus/genvi/";
		
//		print '<BR>$pathFilterPage = '.$pathFilterPage;
		
		$theRepositoryName = '';
		
		global $workingMode;
		
		if ($workingMode == wrkmBITAM)
		{
			$theRepositoryName = str_ireplace('projects/', '', $_SESSION["repository"]);

			require_once("../conf_rep/include/generador.inc.php");
			
			$theRepositoryNameG6 = Get_ProyectNames($theRepositoryName,false,true,'../');
			
			savelogfile('Get_ProyectNamesG6 = ['.$theRepositoryNameG6.']');
			savelogfile('Get_ProyectNames = ['.$theRepositoryName.']');
			
			if ($theRepositoryNameG6 != '')
				$theRepositoryName = $theRepositoryNameG6;
		}
		else
		{
			$theRepositoryName = $_SESSION["BITAM_RepositoryName"];

			savelogfile('BITAM_RepositoryName = ['.$theRepositoryName.']');

/*			
			require_once("../conf_rep/include/generador.inc.php");
			
			$theRepositoryNameG6 = Get_ProyectNames($theRepositoryName,false,true,'../');
			
			savelogfile('Get_ProyectNamesG6 = ['.$theRepositoryNameG6.']');
			savelogfile('Get_ProyectNames = ['.$theRepositoryName.']');
			
			if ($theRepositoryNameG6 != '')
				$theRepositoryName = $theRepositoryNameG6;
*/				
			
		}

		$thePassword = $_SESSION["BITAM_Password"];
		
		savelogfile('BITAM_Password = ['.$thePassword.']');
		
//		$thePassword = BITAMEncryptPassword($thePassword);
		
		// Obtenemos el id del usuario logeado, primero lo tomamos de la cookie
		$theUser = "SUPERVISOR";
		if (array_key_exists("BITAM_UserName", $_SESSION))
		{
			//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesión de eForms
			$theUser = @$_SESSION["PABITAM_UserName"];		
		}
		
		savelogfile('BITAM_UserName = ['.$theUser.']');
	
?>	

	<form action="<?=$pathFilterPage?>loadstage.php" name="frmFilter" method="post">
			<input name="txtRepositorio" type="hidden" value="<?=$theRepositoryName?>">
			<input name="txtUser" type="hidden" value="<?=$theUser?>">
			<input name="txtPassword" type="hidden" value="<?=htmlspecialchars($thePassword,ENT_QUOTES)?>">
			<input name="txtNumEsc" type="hidden" value="-3">
			<input name="txtParametros" type="hidden">
	</form>	
	
	<form id="frmData" name="frmData" >
		<input type="hidden" id="txtLevel" name="txtLevel" value="">
		<input type="hidden" id="txtFilter" name="txtFilter" value="">
	</form>
	

<script language="javascript">

 		//Evento del click para invocar a la forma del Filtro
		function selectFilter()
		{
			//cargar el filtro anterior, asignar los valores a
			//frmData.txtLevel.value y frmData.txtFilter.value q se mandan como parámetros a la página del 
			// filtro para q considere el filtro indicado anteriormente
			loadSavedFilter();
				
			//var anIndicatorID=BITAMBudget_SaveForm.IndicatorID.options(BITAMBudget_SaveForm.IndicatorID.selectedIndex).value;
			

			var nIndicatorID= BITAMUserGroupIndicatorGroup_SaveForm.IndicatorID.value;
		    var nModelID= BITAMUserGroupIndicatorGroup_SaveForm.ModelID.value;
			var sLevel= BITAMUserGroupIndicatorGroup_SaveForm.SecurityLevel.value;
			
			sLevel = sLevel.replace('|', ',')
			
			var sFilter= BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value;						
			
//			alert(nIndicatorID);
//			alert(sLevel);
//			alert(sFilter);
			
//			var nIndicatorID= 45;
//			var sLevel= '|1';			
//			var sFilter= '[AREAS].[areaorig_01].[NORTH]';									
			
//			alert(BITAMUserGroupIndicatorGroup_SaveForm.IndicatorID.value);

//			alert('<?=$path?>');

			frmFilter.txtParametros.value = 'TPParamsTPFILTER,<?=$path?>|AWCUBE:' + nModelID + '|' + 4 + '|' + sLevel + '|' + sFilter;
			
			x = new Date();
			y  = "mywindow";
			y = y + x.getHours() + x.getMinutes() + x.getSeconds();		
			
//			alert('<?=$pathFilterPage?>');
	
			window.open("<?=$pathFilterPage?>Loading.htm", y, "menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=no",1);		
			frmFilter.target = y;
			frmFilter.submit();
		}
		
		
		function loadSavedFilter()
		{
/*			
			var filterString = BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value;
			
			var anArray= new Array;
			anArray = filterString.split('|');
			if (trim(filterString) == "") 
			{
				//BITAMBudget_SaveForm.txtIndFilter.value = "No";
				frmData.txtLevel.value = "";
				frmData.txtFilter.value = "";
			}
			else
			{  
				if (trim(anArray[3])=="AND")
				{ 
					//BITAMBudget_SaveForm.txtIndFilter.value = "No"
					frmData.txtLevel.value = ""
					frmData.txtFilter.value = ""
				}
				else
				{
					//BITAMBudget_SaveForm.txtIndFilter.value = "Yes";
					frmData.txtLevel.value = anArray[2]; //Esta es la lista de consecutivos separada por ","
					frmData.txtFilter.value = anArray[3]; //Esta es la cadena del filtro tal como la regresa WebHTML
				}
			}
*/			
		}
	
		function changeFilter(argFilterString)
		{

			var anArray=new Array;
			anArray = argFilterString.split('|');
			
			if (Trim(argFilterString) == "") 
			{
				BITAMUserGroupIndicatorGroup_SaveForm.SecurityLevel.value = "";
				BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value = "";
			}
			else
			{  
				if (Trim(anArray[3])=="AND")
				{ 
					BITAMUserGroupIndicatorGroup_SaveForm.SecurityLevel.value = "";
					BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value= "";
				}
				else
				{
					anArray[2] = anArray[2].replace( '0,', '|' );
					anArray[2] = anArray[2].replace( ',', '|' );
					if (Trim(anArray[3].substring(0, 4)) == 'AND')
					{
						anArray[3] = anArray[3].substring(4);
					}
					BITAMUserGroupIndicatorGroup_SaveForm.SecurityLevel.value = anArray[2]; 
					BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value = anArray[3]; 
				}
			}
		}
		
		function Trim(TRIM_VALUE){
			if(TRIM_VALUE.length < 1){
				return"";
			}
			TRIM_VALUE = RTrim(TRIM_VALUE);
			TRIM_VALUE = LTrim(TRIM_VALUE);
			if(TRIM_VALUE==""){
				return "";
			}
			else{
				return TRIM_VALUE;
			}
		} //End Function

		function RTrim(VALUE){
			var w_space = String.fromCharCode(32);
			var v_length = VALUE.length;
			var strTemp = "";
			if(v_length < 0){
				return"";
			}
			var iTemp = v_length -1;
			
			while(iTemp > -1){
				if(VALUE.charAt(iTemp) == w_space){
				}
				else{
					strTemp = VALUE.substring(0,iTemp +1);
					break;
				}
				iTemp = iTemp-1;
				} //End While
			return strTemp;
		} //End Function
		
		function LTrim(VALUE){
			var w_space = String.fromCharCode(32);
			if(v_length < 1){
				return"";
			}
			var v_length = VALUE.length;
			var strTemp = "";
		
			var iTemp = 0;
		
			while(iTemp < v_length){
				if(VALUE.charAt(iTemp) == w_space){
				}
				else{
					strTemp = VALUE.substring(iTemp,v_length);
					break;
				}
					iTemp = iTemp + 1;
			} //End While
			return strTemp;
		} //End Function

		
</script>		
<?		
		
	}	
	
}
?>