<?php
global $RequiereBitamFont;
if(is_null($RequiereBitamFont) || !$RequiereBitamFont)
{
	echo "<script language=JavaScript src='js/EditorHTML/cBitamGridSel.js'></script>";
}

class cBitamGridSel{
	public $name;
	public $img_closed = 'images/advanced_search.gif';
	public $img_export = 'images/export.gif';
	public $items_per_line = 4;
	public $align = 'right';
	public $arr_items = array();
	//public $arr_item_

	public function cBitamGridSel($name){
		$this->name = $name;
	}

	public function addItem($key, $image, $hint){
		$item_added = array('key' => $key, 'image' => $image, 'hint' => $hint);
		array_push($this->arr_items, $item_added);
	}

	public function gen_selector($ar_item_def){
		$html_Code = '<input type="hidden" value="'.$ar_item_def['key'].'" name="'.$this->name.'" id="'.$this->name.'"><table cellspacing="0" border="0"><tr><td disabled="disabled" class=selector_left><img id=id_img_selection_'.$this->name.' name=img_selection_'.$this->name.' src="'.$ar_item_def['image'].'" height="15px" width="17px" title="'.$ar_item_def['hint'].'"></td><td id=id_grid_drilldown_'.$this->name.' class=selector_right style="cursor:pointer;" onclick="show_hide_sel(\'id_div_'.$this->name.'\', this, \''.$this->align.'\');" width="12px" align=center><img id="id_div_'.$this->name.'_img_up_down" src="'.$this->img_closed.'"></td></tr></table>';
		return $html_Code;
	}

	public function gen_control($left, $top, $item_def){
		$html_Code = '<div id=id_div_'.$this->name.' style="background-color:white; position:absolute; top:'.$top.'; left:'.$left.'; border:1px solid #dfd2c0; display:none;" onmouseout="set_Tempor(\'id_div_'.$this->name.'\');" onmouseover="clear_Tempor();"><table>';
		$i = 0;
		$bselector_generado = false;
		foreach ($this->arr_items as $value)
		{
			if ($i == 0)
				$html_Code .= '<tr>';
			$html_Code .= '<td style="background-color:#fafaf0; cursor:pointer;" id=id_'.$value['key'].' name='.$value['key'].'>';
			$html_Code .= '<img id=id_img_'.$value['key'].' style="border:1px ridge #dfd2c0; align: center;" src="'.$value['image'].'" height="16px" width="16px" title="'.$value['hint'].'" onclick="change_image(\''.$value['image'].'\', \''.'id_img_selection_'.$this->name.'\', \''.$value['hint'].'\', \''.$this->name.'\', \''.$value['key'].'\'); show_hide_sel(\'id_div_'.$this->name.'\', this, \''.$this->align.'\');"></td>';
			$i++;
			if ($i >= $this->items_per_line)
			{
				$html_Code .= '</tr>';
				$i = 0;
			}
			if ($value['key'] == $item_def)
			{
				$html_Code_Selector = $this->gen_selector($value);
				$bselector_generado = true;
			}
		}
		if (!$bselector_generado)
			$html_Code_Selector = $this->gen_selector(array('key' => $item_def, 'image' => 'images/transparent.gif', 'hint' => ''));
		if (($i < $this->items_per_line) && ($i > 0))
			$html_Code .= '</tr>';
		$html_Code .= '</table></div>'.$html_Code_Selector;
		return $html_Code;
	}

	public function gen_selector_color($ar_item_def){
		$html_Code = '<input type="hidden" value="'.$ar_item_def['key'].'" name="'.$this->name.'" id="'.$this->name.'"><table cellspacing="0" border="0" id="tb_'.$this->name.'"><tr><td class=selector_left><img id=id_img_selection_'.$this->name.' name=img_selection_'.$this->name.' style="background-color:'.$ar_item_def['image'].';" src="images/transparent.gif" height="15px" width="17px" title="'.$ar_item_def['hint'].'"></td><td id=id_choose_color_'.$this->name.' class=selector_right style="cursor:pointer;" onclick="fnShowChooseColorDlg(document.getElementById(\''.$this->name.'\').value, \''.$this->name.'\', \'\');" width="12px" align=center><img src="'.$this->img_export.'"></td></tr></table>';
		return $html_Code;
	}

	public function set_items_per_line($no_items){
		$this->items_per_line = $no_items;
	}
}
?>