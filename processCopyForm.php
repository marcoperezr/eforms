<?php
//Archivo que se manda llamar para copiar Forma
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("skipRule.inc.php");
//$aBITAMConnection = connectToKPIRepository();
$mapIDs = array();
$mapIDs["Survey"]=array();
$mapIDs["Sections"]=array();
$mapIDs["Questions"]=array();
$mapIDs["Options"]=array();
$aRepository = $theRepository;
//Clave de la forma que será copiada
$surveyID = getParamValue('SurveyID', 'both', '(int)');
//Feb2017: Agregar nuevo parámetro actionType (1: Copiar forma, 2: Generar forma de desarrollo)
$actionType = getParamValue('actionType', 'both', '(int)');

//@JAPR 2017-02-07: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
//Dejará la instancia de la forma con el nombre poco usual $form como global para reutilizar durante todo el proceso dentro de este archivo .php
global $form;

//Se modificó la respuesta para regresar un JSON, ya que en caso de error no se estaba reportando correctamente
$strErrDesc = "";
try {
	//Obtener instancia de la forma que se copiará
	$form = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	//Se forzará a invocar este método porque es necesario cambiar las posibles referencias mal grabadas de variables y el save no lo invocará si se trata de una copia, se tiene
	//que hacer en este punto antes de actualizar la referencia al SurveyID o de lo contrario tampoco funcionará porque no usará las colecciones correctas para los reemplazos
	$form->fixAllPropertiesWithVarsNotSet();
	//@JAPR
	$form->ForceNew = true;
	$form->ActionType = $actionType;
	$form->SurveyName = getNewFormName($aRepository, $form->SurveyName);
	$form->CopiedSurveyID = $surveyID;
	if ($form->ActionType == devAction)
	{
		//Si se genera la forma como desarrollo se cambia su campo FormType para indicar que está en desarrollo
		$form->FormType = formDevelopment; //La forma copia tendrá como tipo "Desarrollo"
	}
	//Copiar los datos básicos de la forma
	$form->save(false, true);
	$mapIDs["Survey"][$surveyID]=$form->SurveyID;
	//Copiar las secciones de la forma
	copySections($aRepository, $surveyID, $form->SurveyID);

	//2018-05-09 @JMRC #J8QPV6: Copiar los destinos de datos a la nueva forma
	copyDataDestinations($aRepository, $surveyID, $form->SurveyID);
	//El ajuste de los IDs en las fórmulas se requiere hacer antes que la función changeToNewIDs porque en esta se 
	//vuelve a realizar un save para la instancia de question en el cual se realiza el llenado de tablas de dependencias
	//relacionadas con las fórmulas.
	//Ajustar los IDs de secciones y preguntas en los campos donde se utiliza la sintaxis de fórmulas
	@BITAMSurvey::TranslateObjecIDsInCopy($aRepository, $surveyID, $form->SurveyID, $mapIDs["Sections"], $mapIDs["Questions"]);
	
	//@JAPR 2017-02-13: Corregido un bug, el proceso de copiado no estaba cambiando las referencias de los HTMLs personalizados con JavaScript (#DSN1B5)
	@BITAMSurvey::TranslateJavaScriptObjecIDsInCopy($aRepository, $surveyID, $form->SurveyID, $mapIDs["Sections"], $mapIDs["Questions"]);
	//@JAPR
	
	changeToNewIDs($aRepository, $form);
	//MAPR 2017-07-28: Se desactiva el forzado antes de guardar la forma con el nuevo QuestionIDForEntryDesc
	$form->ForceNew = false;
	$form->save(); 
} catch (Exception $e) {
	$strErrDesc = $e->getMessage();
}

$arrData = array();
if ($strErrDesc) {
	$arrData['error'] = array('desc' => $strErrDesc);
}
else {
	$arrData['name'] = $form->SurveyName;
}

//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
//Se modificó la respuesta para regresar un JSON, ya que en caso de error no se estaba reportando correctamente
echo(json_encode($arrData));
//echo($form->SurveyName);

//Actualizar en la copia los IDs de las nuevas secciones y preguntas que le corresponden
function changeToNewIDs($aRepository, $form)
{
	global $mapIDs;
	
	//MAPR 2017-07-24: Un error en el copiado de la forma, no permite pasar EntryDescription de Desarrollo a Producción (#S818ZS)
	if ( $form->QuestionIDForEntryDesc!== null && isset($mapIDs["Questions"][$form->QuestionIDForEntryDesc]) ) 
	{
		$form->QuestionIDForEntryDesc=$mapIDs["Questions"][$form->QuestionIDForEntryDesc];
	}
	
	$sections = BITAMSectionCollection::NewInstance($aRepository, $form->SurveyID);
	
	foreach ($sections->Collection as $aSection)
	{
		//En la definición de sección se tiene que ajustar los siguientes IDs a los nuevos correspondientes a la copia:
		// - Next Section
		if($aSection->NextSectionID!==null && isset($mapIDs["Sections"][$aSection->NextSectionID]))
		{
			$aSection->NextSectionID=$mapIDs["Sections"][$aSection->NextSectionID];
		}

		if($aSection->SelectorQuestionID!==null && isset($mapIDs["Questions"][$aSection->SelectorQuestionID]))
		{
			$aSection->SelectorQuestionID=$mapIDs["Questions"][$aSection->SelectorQuestionID];
		}
		//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if($aSection->XPosQuestionID!==null && isset($mapIDs["Questions"][$aSection->XPosQuestionID]))
		{
			$aSection->XPosQuestionID=$mapIDs["Questions"][$aSection->XPosQuestionID];
		}
		if($aSection->YPosQuestionID!==null && isset($mapIDs["Questions"][$aSection->YPosQuestionID]))
		{
			$aSection->YPosQuestionID=$mapIDs["Questions"][$aSection->YPosQuestionID];
		}
		if($aSection->ItemNameQuestionID!==null && isset($mapIDs["Questions"][$aSection->ItemNameQuestionID]))
		{
			$aSection->ItemNameQuestionID=$mapIDs["Questions"][$aSection->ItemNameQuestionID];
		}
		//@JAPR
		$aSection->CreateCat=false; //No tiene caso que se ejecute el código de creación de catálogo por que se creo al copiar la sección
		$aSection->save();
		$questions = BITAMQuestionCollection::NewInstanceBySection($aRepository, $aSection->SectionID);
		
		foreach ($questions->Collection as $aQuestion)
		{
		
			//En la definición de pregunta se tiene que ajustar los siguientes IDs a los nuevos correspondientes a la copia:
			// - Cuando en Fuente se selecciona "De las respuestas de otra pregunta de opción múltiple" cambiar el valor del campo "Otra pregunta"
			if($aQuestion->SourceQuestionID!==null && isset($mapIDs["Questions"][$aQuestion->SourceQuestionID]))
			{
				$aQuestion->SourceQuestionID=$mapIDs["Questions"][$aQuestion->SourceQuestionID];
			}
			// - Cuando en Fuente se selecciona "Compartir con otra pregunta" cambiar el valor del campo "Copiar de pregunta"
			if($aQuestion->SharedQuestionID!==null && isset($mapIDs["Questions"][$aQuestion->SharedQuestionID]))
			{
				$aQuestion->SharedQuestionID=$mapIDs["Questions"][$aQuestion->SharedQuestionID];
			}
			// - Pregunta para excluir
			if($aQuestion->SourceSimpleQuestionID!==null && isset($mapIDs["Questions"][$aQuestion->SourceSimpleQuestionID]))
			{
				$aQuestion->SourceSimpleQuestionID=$mapIDs["Questions"][$aQuestion->SourceSimpleQuestionID];
			}
			
			// - Para preguntas tipo "Salto de Sección" se tiene que cambiar el valor del campo GoToSection
			//En la instanca de pregunta la propiedad GoToQuestion representa el campo GoToSection
			if($aQuestion->GoToQuestion!==null && isset($mapIDs["Sections"][$aQuestion->GoToQuestion]))
			{
				$aQuestion->GoToQuestion=$mapIDs["Sections"][$aQuestion->GoToQuestion];
			}
			
			// - Para preguntas tipo "Sección tabla" se tiene que cambiar el valor del campo SourceSectionID
			if($aQuestion->SourceSectionID!==null && isset($mapIDs["Sections"][$aQuestion->SourceSectionID]))
			{
				$aQuestion->SourceSectionID=$mapIDs["Sections"][$aQuestion->SourceSectionID];
			}
			
			$aQuestion->CreateCat=false; //No tiene caso que se ejecute el código de creación de catálogo por que se creo al copiar la pregunta
			$aQuestion->save();
			
			//Después de ajustar los IDs en las preguntas es necesario actualizar unas tablas de dependencia que se utilizan en
			//preguntas que copian su respuesta de otras, preguntas calculadas y condiciones de visibilidad
			
			if($aQuestion->HasQuestionDep) {
				$aQuestion->setQuestionsToEval();
			}
			$aQuestion->setFormulaChildren();
			
			$aQuestion->setShowConditionChildren();
			
			$options = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestion->QuestionID);
			if(count($options->Collection)>0)
			{	//Copiar las opciones
				foreach($options as $anOption)
				{
					//En la definición de opción de pregunta se tiene que ajustar los siguientes IDs a los nuevos correspondientes a la copia:
					// - Next Section
					
					//En la instanca de option la propiedad NextQuestion representa el campo Next Section
					if($anOption->NextQuestion!==null && isset($mapIDs["Sections"][$anOption->NextQuestion]))
					{	
						$anOption->NextQuestion = $mapIDs["Sections"][$anOption->NextQuestion];
					}
					$anOption->save();
				}
			}
			
			//Copiar el campo mostrar preguntas
			$showQuestions = BITAMShowQuestionCollection::NewInstanceByQuestionID($aRepository, $aQuestion->QuestionID);
			foreach($showQuestions as $aShowQuestion)
			{	
				$aShowQuestion->ShowQuestionID = (int) @$mapIDs["Questions"][$aShowQuestion->ShowQuestionID];
				$aShowQuestion->save();
			}
			
			//@JAPR 2017-02-21: Corregido un bug, había faltado copiar las reglas de salto de las preguntas de selección (#DSN1B5)
			if ($aQuestion->QTypeID == qtpSingle) {
				$objSkipRulesColl = BITAMSkipRuleCollection::NewInstance($aRepository, $aQuestion->QuestionID);
				if (!is_null($objSkipRulesColl)) {
					foreach ($objSkipRulesColl->Collection as $objSkipRule) {
						if ($objSkipRule->NextSectionID > 0) {
							$objSkipRule->NextSectionID = (int) @$mapIDs["Sections"][$objSkipRule->NextSectionID];
						}
						$objSkipRule->save();
					}
				}
			}
			//@JAPR
		}
	}
}

function copySections($aRepository, $surveyID, $newSurveyID)
{
	global $mapIDs;
	
	//@JAPR 2017-02-17: Corregido un bug, no se pueden sincronizar dentro de copyQuestions la lista de preguntas OCR ni otras referencias, ya que podrían apuntar a preguntas / secciones que
	//aún no han sido copiadas, así que ahora copyQuestions regresará la referencia de las preguntas ya copiadas para mantener en un array que se usará para procesar todo lo pendiente
	//hasta que ya hubiera generado todas las preguntas correctamente (#DSN1B5)
	$arrAllQuestionsColl = array();
	
	$sections = BITAMSectionCollection::NewInstance($aRepository, $surveyID);
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	//Tiene que limpiar las referencias en memoria después de cargar la colección, ya que durante el proceso de grabado de la corrección de IDs se cargará de nuevo y como es la misma
	//colección que se va recorriendo para forzar a grabar en la copia, al reutilizar la referencia se procesaría con los IDs de la copia para los objetos ya copiados y no podría
	//hacer el reemplazo correcto
	BITAMGlobalFormsInstance::ResetCollections();
	//@JAPR
	
	foreach ($sections->Collection as $aSection) {
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Se forzará a invocar este método porque es necesario cambiar las posibles referencias mal grabadas de variables y el save no lo invocará si se trata de una copia, se tiene
		//que hacer en este punto antes de actualizar la referencia al SurveyID o de lo contrario tampoco funcionará porque no usará las colecciones correctas para los reemplazos
		$aSection->fixAllPropertiesWithVarsNotSet();
		//@JAPR
		$aSection->CopiedSectionID = $aSection->SectionID;
		$aSection->SurveyID = $newSurveyID;
		$aSection->ForceNew = true;
		$aSection->save(false, true);
		$mapIDs["Sections"][$aSection->CopiedSectionID]=$aSection->SectionID;
		//Copias las preguntas de la sección
		$arrQuestionsColl = copyQuestions($aRepository, $surveyID, $aSection);
		
		foreach ($arrQuestionsColl->Collection as $objQuestion) {
			$arrAllQuestionsColl[] = $objQuestion;
		}
	}
	
	//Hasta este punto realiza el ajuste a ciertas referencias que dependían de la creación de las preguntas
	//@JAPR 2017-02-17: Corregido un bug, no se podía sincronizar en el punto original las preguntas OCR (copyQuestions) porque podría ser que las copias de las preguntas de su lista aún 
	//no existan, se movió este código a esta parte que es cuando ya terminó de copiar todas las secciones y por tanto todas las preguntas (#DSN1B5)
	foreach ($arrAllQuestionsColl as $objQuestion) {
		if ($objQuestion->QTypeID == qtpOCR) {
			$arrQuestionList = array();
			foreach ($objQuestion->QuestionList as $intOpenQuestionID) {
				$intProdOpenQuestionID = (int) @$mapIDs["Questions"][$intOpenQuestionID];
				if ($intProdOpenQuestionID > 0) {
					$arrQuestionList[] = $intProdOpenQuestionID;
				}
			}
			$objQuestion->QuestionList = $arrQuestionList;
			$objQuestion->saveOCRQuestionIDs();
		}
	}
	//@JAPR
}

function copyQuestions($aRepository, $surveyID, $aSection)
{
	global $mapIDs;

	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	//No hay necesidad de resetear las colecciones al cargar esta colección, ya que el proceso de corrección de IDs utiliza la instancia completa de preguntas por forma, mientras que
	//el proceso de copiado carga la colección de preguntas por sección así que se trata de un cache diferente el cual no interferirá porque no se están sobreescribiendo las mismas instancias
	//de preguntas al ir grabando
	//@JAPR
	$questions = BITAMQuestionCollection::NewInstanceBySection($aRepository, $aSection->CopiedSectionID);
	foreach ($questions->Collection as $aQuestion) {
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Se forzará a invocar este método porque es necesario cambiar las posibles referencias mal grabadas de variables y el save no lo invocará si se trata de una copia, se tiene
		//que hacer en este punto antes de actualizar la referencia al SurveyID o de lo contrario tampoco funcionará porque no usará las colecciones correctas para los reemplazos
		$aQuestion->fixAllPropertiesWithVarsNotSet();
		//@JAPR
		$aQuestion->CopiedQuestionID =$aQuestion->QuestionID;
		$aQuestion->SurveyID = $aSection->SurveyID;
		$aQuestion->SectionID = $aSection->SectionID;
		$aQuestion->ForceNew = true;
		//$bJustMobileTables = false, $iMobileSurveyID = -1, $iMobileSectionID = -1, $iFromQuestionID = 0, bCopy=true
		$aQuestion->StrSelectOptions=""; //Las opciones se grabarán con el save de questionoption
		$aQuestion->save(false, -1, -1, 0, true);
		$mapIDs["Questions"][$aQuestion->CopiedQuestionID]=$aQuestion->QuestionID;
		$aQuestion->saveCatMembersIDs();
		//Copiar las opcines de preguntas (estas no se copian en el save de question por que no copiaría todos los datos)
		$options = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestion->CopiedQuestionID);
		if (count($options->Collection)>0)
		{
			foreach($options as $anOption)
			{	
				//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
				//Se forzará a invocar este método porque es necesario cambiar las posibles referencias mal grabadas de variables y el save no lo invocará si se trata de una copia, se tiene
				//que hacer en este punto antes de actualizar la referencia al SurveyID o de lo contrario tampoco funcionará porque no usará las colecciones correctas para los reemplazos
				$anOption->fixAllPropertiesWithVarsNotSet();
				//@JAPR
				$anOption->CopiedOptionID = $anOption->QuestionOptionID;
				$anOption->SurveyID=$aQuestion->SurveyID;
				$anOption->QuestionID=$aQuestion->QuestionID;
				$anOption->ForceNew = true;
				$anOption->bCopy = true;
				$anOption->save();
				$mapIDs["Options"][$anOption->CopiedOptionID]=$anOption->QuestionOptionID;
			}
			
			//Copiar el campo mostrar preguntas
			$showQuestions = BITAMShowQuestionCollection::NewInstanceByQuestionID($aRepository, $aQuestion->CopiedQuestionID);
			foreach($showQuestions as $aShowQuestion)
			{	
				$aShowQuestion->QuestionID=$aQuestion->QuestionID;
				$aShowQuestion->ConsecutiveID=$mapIDs["Options"][$aShowQuestion->ConsecutiveID];
				$aShowQuestion->ForceNew=true;
				$aShowQuestion->save();
			}
		}
		
		//@JAPR 2017-02-21: Corregido un bug, había faltado copiar las reglas de salto de las preguntas de selección (#DSN1B5)
		if ($aQuestion->QTypeID == qtpSingle) {
			$objSkipRulesColl = BITAMSkipRuleCollection::NewInstance($aRepository, $aQuestion->CopiedQuestionID);
			if (!is_null($objSkipRulesColl)) {
				foreach ($objSkipRulesColl->Collection as $objSkipRule) {
					//Se forzará a invocar este método porque es necesario cambiar las posibles referencias mal grabadas de variables y el save no lo invocará si se trata de una copia, se tiene
					//que hacer en este punto antes de actualizar la referencia al SurveyID o de lo contrario tampoco funcionará porque no usará las colecciones correctas para los reemplazos
					$objSkipRule->fixAllPropertiesWithVarsNotSet();
					$objSkipRule->SkipRuleID = -1;
					$objSkipRule->QuestionID = $aQuestion->QuestionID;
					//$objSkipRule->ForceNew=true;
					$objSkipRule->save();
				}
			}
		}
		//@JAPR
		
		//@JAPR 2017-02-13: Corregido un bug, faltaba copiar las referencias de preguntas de la pregunta tipo OCR (#DSN1B5)
		//@JAPR 2017-02-17: Corregido un bug, no se pueden sincronizar en este punto las preguntas OCR porque podría ser que las copias de las preguntas de su lista aún no existan, se
		//moverá este código a un nuevo ciclo posterior
		//Actualizar las referencias de preguntas OCR
		/*if ($aQuestion->QTypeID == qtpOCR) {
			$arrQuestionList = array();
			foreach ($aQuestion->QuestionList as $intOpenQuestionID) {
				$intProdOpenQuestionID = (int) @$mapIDs["Questions"][$intOpenQuestionID];
				if ($intProdOpenQuestionID > 0) {
					$arrQuestionList[] = $intProdOpenQuestionID;
				}
			}
			$aQuestion->QuestionList = $arrQuestionList;
			$aQuestion->saveOCRQuestionIDs();
		}*/
		//@JAPR
	}
	
	return $questions;
}

//2018-05-08 @JMRC #J8QPV6: Copia de destinos de datos al momento de copiar una forma.
function copyDataDestinations ($aRepository, $copiedSurveyID, $newSurveyID)
{
	require_once('eFormsDataDestinations.inc.php');
	//obtener destinos de datos de la forma a copiar
	$dataDestinations = BITAMeFormsDataDestinationsCollection::NewInstance($aRepository, $copiedSurveyID);
	$success = true;
	foreach ($dataDestinations->Collection as $dataDestination) {
		//2018-05-08 @JMRC #J8QPV6: asignar el destino de datos a la nueva forma.
    $dataDestination->id_survey = $newSurveyID;
    //2018-06-07 @JMRC #J8QPV6
    $dataDestinationCopiedId = $dataDestination->id;
    //2018-05-08 @JMRC #J8QPV6: id del destino de datos negativo, indica que se debe crear el destino de datos
    // condicion linea 349 eFormsDataDestinations.inc.php
    $dataDestination->id = -1;
    $dataDestination->save();
    //2018-05-11 @JMRC #J8QPV6: actualizar las referencias de ids en la condicion de envio
    //2018-06-07 @JMRC #J8QPV6: se envia el id original del destino de datos
    $dataDestination->updateConditionIDs($copiedSurveyID, $dataDestinationCopiedId);
  }
}

//Copiar las preguntas de cada sección
//Genera nombres para las copias de las formas, por ejemplo si la forma se llama "Forma" y se copia varias veces
//se generarán los nombres de esta manera:
//Copy of Forma
//Copy (1) of Forma 
//Copy (2) of Forma 
//Copy (3) of Forma 
//Y así sucesivamente
function getNewFormName($aRepository, $surveyName)
{
	//@JAPR 2017-02-07: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	global $form;
	
	//Establecer el nombre de la nueva copia de la forma
	$i=1;
	$descripFlag = true;
	
	while ($descripFlag)
	{
		$strCopySuffix = translate("Copy#");
		//@JAPR 2017-02-07: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (isset($form) && $form->ActionType == devAction) {
			//Si está en proceso de generación de la forma de desarrollo, cambia el sufijo a utilizar
			$strCopySuffix = translate("Development");
		}
		
		if ($i == 1)
		{
			$strDescrip = $surveyName." - ".$strCopySuffix;
		}
		else
		{
			$strDescrip = $surveyName. " - ".$strCopySuffix." (".$i.")";
		}
		//@JAPR
		
		$sql = "SELECT SurveyName FROM SI_SV_Survey WHERE SurveyName = ".$aRepository->DataADOConnection->Quote($strDescrip);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if ($aRS->EOF)
		{
			$descripFlag = false;
		}
		
		$i++;
	}
	
	return $strDescrip;
}
?>
