<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Obtenemos el prefijo de la ruta requerida en la dimension imagen
$script = $_SERVER["REQUEST_URI"];
//Eliminamos la primera diagonal que separa el archivo php de la carpeta
$position = strrpos($script, "/");
$dir = substr($script, 0, $position);
	
//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
$position = strrpos($dir, "/");
$mainDir = substr($dir, ($position+1));

$preffix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/";

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Se genera instancia de coleccion de preguntas dado un SurveyID
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);
	
	foreach ($questionCollection->Collection as $questionInstance) 
	{
		if(!is_null($questionInstance->HasReqPhoto) && $questionInstance->HasReqPhoto>0)
		{
			if(!is_null($questionInstance->ImgDimID) && $questionInstance->ImgDimID>0)
			{
				$factTableName = "RIFACT_".$surveyInstance->ModelID;
				$dimTableName = "RIDIM_".$questionInstance->ImgDimID;
				$dimFieldDesc = "DSC_".$questionInstance->ImgDimID;
				$dimFieldKey = $dimTableName."KEY";
				
				//Actualizamos el MainFactKey de la tabla SI_SV_SurveyAnswerImage con el valor de FactKey ya que son
				//identicos porque todas las preguntas pertenecieron a secciones estaticas
				$sql = "UPDATE SI_SV_SurveyAnswerImage A
						SET A.MainFactKey = A.FactKey 
						WHERE A.SurveyID = ".$surveyInstance->SurveyID." AND A.QuestionID = ".$questionInstance->QuestionID." 
						AND A.MainFactKey IS NULL";
				
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					echo(translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
				}
				else 
				{
					echo('UPDATE: '.$sql.'<br>');
				}

				
				//Realizamos el INSERT->SELECT con el recordeset de todas las imagenes capturadas para dicha pregunta
				$sql = "INSERT INTO ".$dimTableName." (".$dimFieldDesc.")
						SELECT CONCAT(".$aRepository->DataADOConnection->Quote($preffix).", A.PathImage) FROM SI_SV_SurveyAnswerImage A, ".$factTableName." B 
						WHERE A.SurveyID = ".$surveyInstance->SurveyID." AND A.QuestionID = ".$questionInstance->QuestionID." 
						AND A.FactKey = B.FactKey AND NOT EXISTS
						(
  							SELECT C.".$dimFieldDesc." FROM ".$dimTableName." C
  							WHERE C.".$dimFieldDesc." = CONCAT(".$aRepository->DataADOConnection->Quote($preffix).", A.PathImage)
						)
						GROUP BY A.PathImage ORDER BY A.FactKey";
				
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					echo(translate("Error accessing")." ".$factTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
				}
				else 
				{
					echo('INSERT->SQL: '.$sql.'<br>');
				}

				//Posterior a la insercion se tienen q actualizar la tabla de hechos con el valor correcto de dimension
				//en el campo dimension imagen
				$sql = "UPDATE ".$dimTableName." A, SI_SV_SurveyAnswerImage B, ".$factTableName." C
						SET C.".$dimFieldKey." = A.".$dimFieldKey."
						WHERE A.$dimFieldDesc = CONCAT(".$aRepository->DataADOConnection->Quote($preffix).", B.PathImage)
						AND B.SurveyID = ".$surveyInstance->SurveyID." AND B.QuestionID = ".$questionInstance->QuestionID." 
						AND B.FactKey = C.FactKey";
				
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					echo(translate("Error accessing")." ".$factTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
				}
				else 
				{
					echo('UPDATE: '.$sql.'<br>');
				}
			}
		}
	}
}
?>