<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("artusreporter.inc.php");
require_once("paginator.inc.php");
require_once("catalog.inc.php");
require_once("utils.inc.php");
require_once("eBavelCatalogValue.inc.php");
require_once("agendas.inc.php");
require_once("dataSource.inc.php");

$agStartDate=null;
$agEndDate=null;
$agUserID=null;
$agIpp=null;
$agCatalogID=null;

class BITAMSurveyAgendaScheduler extends BITAMObject
{
	public $AgendaSchedID;
	public $AgendaSchedName;					//Opcional, si se captura se utilizará este nombre, de lo contrario el nombre será la concatenación de los nombres de los elementos seleccionados en otras columnas
	public $CatalogID;					//Catálogo del que se obtendrá el valor al filtrar, las encuestas sólo pueden aplicarse si utilizan este catálogo, ya que al día de hoy en las aplicaciones móviles solo puede haber preguntas de catálogo si la encuesta está asociada al mismo catálogo 
	public $CatalogDim;					//No es un campo, se obtiene mediante Join
	public $CatalogName;				//No es un campo, se obtiene mediante Join
	public $CatalogTable;				//No es un campo, se obtiene mediante Join
	public $AttributeID;				//Atributo del catálogo por el cual se va a filtrar, las encuestas sólo pueden aplicarse si contienen alguna pregunta que utilice a este catálogo (por consiguiente, la propia encuesta está asociada a este catálogo)
	public $MemberName;					//No es un campo, se obtiene mediante Join
	public $FilterText;					//El valor exacto del atributo que se utilizará como filtro al aplicar la encuesta (sólo puede ser uno por ahora)
	public $UserID;						//El usuario al que se configuró la agenda. Por ahora son personalizadas, así que no se pueden aplicar a grupos de usuarios
	public $UserName;					//No es un campo, se obtiene mediante un subquery
	public $SurveyIDs;					//Lista de encuestas para la cual aplica la agenda (depende del catálogo y del atributo específicos seleccionados)
	public $surveyIDsStatus;			//Array indexado por ID de encuesta con los Status que tenían al momento de ser cargadas desde la base de datos (no cambian con el POST, pero se debe validar para que respeten su Status previo si aun sigue seleccionada la encuesta)
    public $CaptureStartTime;			//Hora de inicio de captura en formato hh:mm (Varchar)
	public $CaptureStartTime_Hours;		//Requerido por el componente Time para el campo CaptureStartTime
	public $CaptureStartTime_Minutes;	//Requerido por el componente Time para el campo CaptureStartTime
	public $LastDateID;					//Fecha de última modificación de la Agenda
	public $Status;						//SI_SV_SurveyAgendaSchedDet. El Status general de la agenda. Si hay una encuesta recalendariza entonces ese es el Status (agdRescheduled), si hay alguna encuesta aun no capturada entonces ese es el Status (agdOpen), sólo si todas las encuestas están capturadas se considera terminada (agdAnswered)
	public $VersionNum;
	public $EntryCheckInStartDate;		//Fecha y Hora de la captura de la encuesta de CheckIn para la agenda (corresponde con SVSurvey_####.DateID de la encuesta de CheckIn donde Agenda ID == a esta Agenda
    public $EntryCheckInStartDate_Hours;	//Requerido por el componente Time para el campo EntryCheckInStartDate
	public $EntryCheckInStartDate_Minutes;	//Requerido por el componente Time para el campo EntryCheckInStartDate
	public $CheckinSurvey;
	public $SurveysRadioForAgenda;
    public $RecurPatternType;           //JCEM para recurrencia ---
    public $WDays;                      //
    public $RecurPatternOpt;            //
    public $RecurPatternNum;            //
    public $RecurRangeOpt;              //
    public $Ocurrences;                 //
    public $CreationDateID;             //
    public $GenerateAgendasNextDayTime;       //
    public $GenerateAgendasNextDayTime_Hours;     //  
    public $GenerateAgendasNextDayTime_Minutes;     //--------------------------

    public $CheckPointName;
    public $CheckPointID;
    public $FilterTextAttr;
	//@JAPR 2015-08-27: Rediseñadas las agendas en v6
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
	public $FirstDayOfWeek;				//Indica el primer día inicial de la semana entre el domingo (0 == Default) o el Lunes (1)
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->AgendaSchedID = -1;
		$this->AgendasSchedName = '';
		$this->CatalogID = 0;
		$this->CatalogDim = 0;
		$this->CatalogName = '';
		$this->CatalogTable = '';
		$this->AttributeID = 0;
		$this->MemberName = '';
		$this->FilterText = '';
		$this->UserID = $_SESSION["PABITAM_UserID"];
		$this->UserName = '';
		$this->SurveyIDs = array();
		$this->surveyIDsStatus = array();
		$this->LastDateID = '';
		$this->VersionNum = 0;
		$this->EntryCheckInStartDate = '';
		$this->CheckinSurvey = 0;
        $this->SurveysRadioForAgenda = array();
        $this->RecurPatternType = frecNone;
        $this->WDays = array();
        $this->RecurPatternOpt = "1";
        $this->RecurPatternNum = 1 ;
        $this->RecurRangeOpt = date("Y-m-d").'|0|0';
        $this->Ocurrences = 0;
        $this->CreationDateID = date("Y-m-d");
        $this->GenerateAgendasNextDayTime = array('','');
        $this->GenerateAgendasNextDayTime_Hours = '';
        $this->GenerateAgendasNextDayTime_Minutes = '';
        $this->CaptureStartTime = array(0,0);
		$this->CaptureStartTime_Hours = 0;
		$this->CaptureStartTime_Minutes = 0;
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		$this->FirstDayOfWeek = asfwdSunday;
	}

	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		
		return new $strCalledClass($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $anAgendaID)
	{
		$anInstance = null;
		
		if (((int)  $anAgendaID) < 0)
		{
			return $anInstance;
		}
		//Conchita 10-nov-2011 Agregado EmailReplyTo
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.VersionNum ';
		}
		if(getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.CheckinSurvey ';
		}
		//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda

        if(getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.EntryCheckInStartDate, t1.`Status` ';
		}
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		if(getMDVersion() >= esveAgendas) {
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion, t1.checkpointID, t1.FilterTextAttr, t4.name';
		}
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		if (getMDVersion() >= esvAgendaFirsDayWeek) {
			$strAdditionalFields .= ', t1.FirstDayOfWeek';
		}
		//@JAPR
		$sql = "SELECT 
                    t1.AgendaSchedID, 
                    t1.AgendaSchedName, 
                    t1.CatalogID, 
                    t2.ParentID, 
                    t2.CatalogName, 
                    t2.TableName, 
                    t1.AttributeID, 
                    t3.MemberName, 
                    t1.FilterText, 
                    t1.UserID, 
                    t1.LastDateID 
                    ,t1.CaptureStartTime
                    ,t1.RecurPatternType
                    ,t1.RecurPatternOpt
                    ,t1.RecurPatternNum
                    ,t1.RecurRangeOpt
                    ,t1.Ocurrences
                    ,t1.CreationDateID
                    ,t1.GenerateAgendasNextDayTime 
					{$strAdditionalFields} 
				FROM SI_SV_SurveyAgendaScheduler t1 
					LEFT OUTER JOIN SI_SV_Catalog t2 ON 
                            t1.CatalogID = t2.CatalogID 
					LEFT OUTER JOIN SI_SV_CatalogMember t3 ON 
                            t1.CatalogID = t3.CatalogID 
                        AND t1.AttributeID = t3.MemberID 
                    ". ( getMDVersion() >= esveAgendas ?
                    " LEFT JOIN si_sv_checkpoint t4 ON t4.id = t1.checkpointID
                    LEFT JOIN si_sv_datasource t5 ON t5.datasourceID = t4.catalog_id
                    ":"" ) ."
				WHERE 
                    t1.AgendaSchedID = ".((int)$anAgendaID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyAgendaScheduler::NewInstanceFromRS($aRepository, $aRS);
			if(getMDVersion() < esvAgendaRedesign) {
				$sql = "SELECT 
                            A.AgendaSchedID
                            ,A.Status
                            ,B.EnableReschedule 
                        FROM 
                            SI_SV_SurveyAgendaSchedDet A, SI_SV_Survey B 
                        WHERE 
                            A.SurveyID = B.SurveyID 
                            AND A.AgendaSchedID = ".((int)$anAgendaID)." 
					GROUP BY A.AgendaSchedID";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaSchedDet ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$intStatus = (int) $aRS->fields['status'];
					switch ($intStatus)
					{
						case agdRescheduled:
							//Si hay por lo menos una encuesta recalendarizada, se considera toda la agenda como recalendarizada
							$anInstance->Status = $intStatus;
							break 2;
					
						case agdOpen:
							//Si hay por lo menos una encuesta sin contestar y que NO esté configurada para recalendarizar dicha encuesta, se considera 
							//toda la agenda como pendiente de contestar
							if (((int) $aRS->fields['enablereschedule']) == 0)
							{
								$anInstance->Status = $intStatus;
								break 2;
							}
							//En este caso es una encuesta no contestada pero que permite reagendar, así que esa no cuenta
							break;
						
						default:
							break 2;
					}
					$aRS->MoveNext();
				}
			}
		}
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
        $anInstance->AgendaSchedID = (int) $aRS->fields["agendaschedid"];
		$anInstance->AgendaSchedName = (string) $aRS->fields["agendaschedname"];
		$anInstance->CatalogID = (int) $aRS->fields["catalogid"];
		$anInstance->CatalogDim = (int) $aRS->fields["parentid"];
		$anInstance->CatalogTable = (string) $aRS->fields["tablename"];
		$anInstance->CatalogName = (string) $aRS->fields["catalogname"];
		$anInstance->AttributeID = (int) $aRS->fields["attributeid"];
		$anInstance->MemberName = (string) $aRS->fields["membername"];
		$anInstance->FilterText = (string) $aRS->fields["filtertext"];
		$anInstance->LastDateID = (string) $aRS->fields["lastdateid"];
		$anInstance->VersionNum = (int) @$aRS->fields["versionnum"];
		$anInstance->EntryCheckInStartDate = (string) @$aRS->fields["capturestartdate"];
        $arrDate = privateUnformatDateTime($anInstance->EntryCheckInStartDate, '');
        $intDateVal = (float) @implode('', $arrDate);
		$anInstance->CheckinSurvey = (int) @$aRS->fields["checkinsurvey"];
        $anInstance->Status = (int) $aRS->fields["status"];
        $anInstance->RecurPatternType = (int) @$aRS->fields["recurpatterntype"];
        $anInstance->RecurPatternOpt = (string) @$aRS->fields["recurpatternopt"];
        $anInstance->RecurPatternNum = (int) @$aRS->fields["recurpatternnum"];
        $anInstance->RecurRangeOpt = (string) @$aRS->fields["recurrangeopt"];
        $anInstance->CreationDateID = (string) @$aRS->fields["creationdateid"];

        $anInstance->CheckPointName = (string) @$aRS->fields['name'];
    	$anInstance->CheckPointID = (int) @$aRS->fields["checkpointid"];
    	$anInstance->FilterTextAttr = (string) @$aRS->fields["filtertextattr"];
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
    	$anInstance->FirstDayOfWeek = (int) @$aRS->fields["firstdayofweek"];
		//@JAPR
        
        $anInstance->EntryCheckInStartDate_Hours = 0;
		$anInstance->EntryCheckInStartDate_Minutes = 0;
		if ($intDateVal) {
			$anInstance->EntryCheckInStartDate_Hours = (int) @$arrDate[3];
			$anInstance->EntryCheckInStartDate_Minutes = (int) @$arrDate[4];
		}
                
        //para la opcion generar las agendas del dia siguiente despues de esta hora
        $sGenerateAgendasNextDayTime = trim((string) @$aRS->fields["GenerateAgendasNextDayTime"]);
        if ($sGenerateAgendasNextDayTime != '')
		{
			$arrHour = explode(':', $sGenerateAgendasNextDayTime);
			if (count($arrHour) == 2)
			{
				if (is_numeric($arrHour[0]))
				{
					$anInstance->GenerateAgendasNextDayTime[0] = $arrHour[0];
					$anInstance->GenerateAgendasNextDayTime_Hours =(int) $arrHour[0];
				}
				if (is_numeric($arrHour[1]))
				{
					$anInstance->GenerateAgendasNextDayTime[1] = $arrHour[1];
					$anInstance->GenerateAgendasNextDayTime_Minutes =(int) $arrHour[1];
				}
			}
		}
        
        $strCaptureStartTime = trim($aRS->fields["capturestarttime"]);
		$anInstance->CaptureStartTime_Hours = 0;
		$anInstance->CaptureStartTime_Minutes = 0;
		if ($strCaptureStartTime != '')
		{
			$arrHour = explode(':', $strCaptureStartTime);
			if (count($arrHour) == 2)
			{
				if (is_numeric($arrHour[0]))
				{
					$anInstance->CaptureStartTime[0] = $arrHour[0];
					$anInstance->CaptureStartTime_Hours =(int) $arrHour[0];
				}
				if (is_numeric($arrHour[1]))
				{
					$anInstance->CaptureStartTime[1] = $arrHour[1];
					$anInstance->CaptureStartTime_Minutes =(int) $arrHour[1];
				}
			}
		}
        
        $anInstance->Ocurrences = (int) $aRS->fields["Ocurrences"];
        $anInstance->UserID = (int) $aRS->fields["userid"];
		if ($anInstance->UserID <= 0)
		{
			$anInstance->UserID = $_SESSION["PABITAM_UserID"];
		}
        

        $anInstance->readSurveyIDs();
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->CheckinSurvey = (int) @$aRS->fields["checkinsurvey"];
		}
        
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $agStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$agStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $agEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$agEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $agUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$agUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $agIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$agIpp = $aHTTPRequest->GET["ipp"];
		}
		
		global $agCatalogID;
		if(array_key_exists("catalogID", $aHTTPRequest->GET))
		{
			$agCatalogID = $aHTTPRequest->GET["catalogID"];
		}
		
		if (array_key_exists("AgendaSchedID", $aHTTPRequest->POST))
		{
			$anAgendaID = $aHTTPRequest->POST["AgendaSchedID"];
			
			if (is_array($anAgendaID))
			{
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$agStartDate = $aHTTPRequest->POST["startDate"];
				}
		
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$agEndDate = $aHTTPRequest->POST["endDate"];
				}
		
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$agUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$agIpp = $aHTTPRequest->POST["ipp"];
				}
				
				if(array_key_exists("catalogID", $aHTTPRequest->POST))
				{
					$agCatalogID = $aHTTPRequest->POST["catalogID"];
				}
				
				$aCollection = BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, $anAgendaID, $agUserID, $agEndDate, null, false, $agStartDate, $agCatalogID, $agIpp);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMSurveyAgendaScheduler::NewInstanceWithID($aRepository, (int)$anAgendaID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("AgendaSchedID", $aHTTPRequest->GET))
		{
			$anAgendaID = $aHTTPRequest->GET["AgendaSchedID"];

			$anInstance = BITAMSurveyAgendaScheduler::NewInstanceWithID($aRepository, (int)$anAgendaID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMSurveyAgendaScheduler::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("AgendaSchedID", $anArray))
		{
			$this->AgendaSchedID = (int)$anArray["AgendaSchedID"];
		}
		
		if (array_key_exists("AgendaSchedName", $anArray))
		{
			$this->AgendaSchedName = (string)$anArray["AgendaSchedName"];
		}
		
	 	if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = rtrim($anArray["CatalogID"]);
			//$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->catalogID);
			$tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, $this->CatalogID, true);
			$attributeIDs = $tmpCatalogCollection[$this->CatalogID];
			$hasAttributes = false;
			$arrAttributeValues = array();
			
			foreach ($attributeIDs as $ckey => $attributeID) {
				//if (array_key_exists(("DSC_".$ckey), $anArray)) {
				if (array_key_exists(("DSC_".$attributeID), $anArray)) {
					if (!$hasAttributes) {
						$hasAttributes = true;
					}
					//$arrAttributeValues[$ckey] = $anArray[("DSC_".$ckey)];
					$arrAttributeValues[$ckey] = $anArray[("DSC_".$attributeID)];
				}
			}
			if ($hasAttributes) {
				$strAttributeValues = implode($arrAttributeValues, "|");
				$this->FilterText = $strAttributeValues;
			}
		}

		if (array_key_exists("CheckPointID", $anArray))
		{
			$this->CheckPointID = rtrim($anArray["CheckPointID"]);

			$anCheckpoint = BITAMCheckpoint::NewInstanceWithId( $this->Repository, $this->CheckPointID );

			$this->CatalogID = $anCheckpoint->catalog_id;

			$tmpCatalogCollection = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->CatalogID);

			$attributeIDs = $tmpCatalogCollection->Collection;
			$hasAttributes = false;
			$arrAttributeValues = array();
			$arrPosAttributeValues = array();
			
			foreach ($attributeIDs as $eachAttribute) {
				if (array_key_exists(("DSC_".$eachAttribute->MemberID), $anArray)) {
					if (!$hasAttributes) {
						$hasAttributes = true;
					}
					$arrAttributeValues[$eachAttribute->MemberID] = $anArray[("DSC_".$eachAttribute->MemberID)];
					$arrPosAttributeValues[$eachAttribute->MemberID] = $eachAttribute->MemberID;
				}
			}

			if ($hasAttributes) {
				$this->FilterText = implode($arrAttributeValues, "|");
				$this->FilterTextAttr = implode($arrPosAttributeValues, "|");
			}
		}
		
		if (array_key_exists("AttributeID", $anArray))
		{
			$this->AttributeID = (int)$anArray["AttributeID"];
		}
		
		if (array_key_exists("SurveyIDs", $anArray))
		{
			$this->SurveyIDs = $anArray["SurveyIDs"];
		}
		else 
		{
			if (array_key_exists("SurveyIDs_EmptySelection", $anArray))
			{
				if ((bool) $anArray["SurveyIDs_EmptySelection"])
				{
					$this->SurveyIDs = array();
				}
			}
		}

		if (array_key_exists("GenerateAgendasNextDayTime_Hours", $anArray))
		{
			$this->GenerateAgendasNextDayTime[0] = (int)$anArray["GenerateAgendasNextDayTime_Hours"];
			$this->GenerateAgendasNextDayTime_Hours = $this->GenerateAgendasNextDayTime[0];
		}
		if (array_key_exists("GenerateAgendasNextDayTime_Minutes", $anArray))
		{
			$this->GenerateAgendasNextDayTime[1] = (int)$anArray["GenerateAgendasNextDayTime_Minutes"];
			$this->GenerateAgendasNextDayTime_Minutes = $this->GenerateAgendasNextDayTime[1];
		}
		
        if (array_key_exists("CaptureStartTime_Hours", $anArray))
		{
			$this->CaptureStartTime[0] = (int)$anArray["CaptureStartTime_Hours"];
			$this->CaptureStartTime_Hours = $this->CaptureStartTime[0];
		}
		if (array_key_exists("CaptureStartTime_Minutes", $anArray))
		{
			$this->CaptureStartTime[1] = (int)$anArray["CaptureStartTime_Minutes"];
			$this->CaptureStartTime_Minutes = $this->CaptureStartTime[1];
		}
        
		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = (int)$anArray["UserID"];
		}
		
		if (array_key_exists("CheckinSurvey", $anArray))
		{
			$this->CheckinSurvey = (int)$anArray["CheckinSurvey"];
		}

		if (array_key_exists("RecurPatternType", $anArray))
		{
			$this->RecurPatternType = (int)$anArray["RecurPatternType"];
		}
		
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		if (array_key_exists("FirstDayOfWeek", $anArray))
		{
			$this->FirstDayOfWeek = (int) @$anArray["FirstDayOfWeek"];
		}
		//@JAPR
		
		return $this;
	}
	
	/**
	 * Guarda la instancia en la base de datos
	 * @param  boolean $bFull Si es false, calcula en base de las variables POST los valores correctos a insertar
	 */
	function save( $bFull = false )
	{
		$currentDate = date("Y-m-d H:i:s");
		$strOriginalWD = getcwd();
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
        if( !$bFull )
        {
	        switch ( $this->RecurPatternType) {
	            case frecDay:	//diario
	                $this->RecurPatternOpt = isset($_POST['DailyOpt'])?$_POST['DailyOpt']:"";
	                $this->RecurPatternNum = isset($_POST['DNumDays'])?$_POST['DNumDays']:0;
	                break;
	            case frecWeek:		//semanal
	                $tmp = (isset($_POST['WDaysSu'])?$_POST['WDaysSu']."|":"").(isset($_POST['WDaysM'])?$_POST['WDaysM']."|":"").(isset($_POST['WDaysTu'])?$_POST['WDaysTu']."|":"").(isset($_POST['WDaysW'])?$_POST['WDaysW']."|":"").(isset($_POST['WDaysTh'])?$_POST['WDaysTh']."|":"").(isset($_POST['WDaysF'])?$_POST['WDaysF']."|":"").(isset($_POST['WDaysSa'])?$_POST['WDaysSa']."|":"");
	                $this->RecurPatternNum = isset($_POST['WNumWeeks'])?$_POST['WNumWeeks']:0;
	                $this->RecurPatternOpt=substr($tmp, 0, strlen($tmp)-1);
	                break;
	            //@AAL 14/04/2015: Opción agregada para crear agendas por semanas del mes
	            case frecWeekOfMonth:		//Semanas del Mes
	                $this->RecurPatternNum = 1;
	                $The  = isset($_POST['The']) ? $_POST['The'] : "";
	                $FreqOrdinal = isset($_POST['The_FreqOrdinal']) ? $_POST['The_FreqOrdinal'] : "";
	                $DaysWeek = isset($_POST['The_DaysWeek']) ? $_POST['The_DaysWeek'] : "";
	                $this->RecurPatternOpt = $The . "/" . $FreqOrdinal . "|" .  $DaysWeek . "|1";
					//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
					$this->FirstDayOfWeek = (int) (isset($_POST['FirstDayOfWeek']) ? $_POST['FirstDayOfWeek'] : "");
					//@JAPR
	                break;
	            //@AAL
	            case frecMonth:	//mensual
	                $this->RecurPatternNum = isset($_POST['M_Day_NumMonth'])?$_POST['M_Day_NumMonth']:( $_POST['M_The_DaysWeek'] ? $_POST['M_The_DaysWeek'] : 0 );
	                switch(isset($_POST['MonthlyOpt'])?$_POST['MonthlyOpt']:""){
	                    case 0://day
	                        $this->RecurPatternOpt = (isset($_POST['MonthlyOpt'])?$_POST['MonthlyOpt']:"")."/".(isset($_POST['tDaysOfMonthSel'])?$_POST['tDaysOfMonthSel']:"");
	                        break;
	                    case 1://the
	                        $this->RecurPatternOpt = (isset($_POST['MonthlyOpt'])?$_POST['MonthlyOpt']:"")."/".(isset($_POST['M_The_FreqOrdinal'])?$_POST['M_The_FreqOrdinal']:"")."|".(isset($_POST['M_The_DaysWeek'])?$_POST['M_The_DaysWeek']:"")."|".(isset($_POST['M_The_NumMonth'])?$_POST['M_The_NumMonth']:"");
	                        break;
	                    default:
	                        break;
	                }
	                break;
	            case frecYear:	//anual
	                $this->RecurPatternNum = isset($_POST['YNumYears'])?$_POST['YNumYears']:0;
	                switch (isset($_POST['YearlyOpt'])?$_POST['YearlyOpt']:"" ){
	                    case "0"://on
	                        $this->RecurPatternOpt = (isset($_POST['YearlyOpt'])?$_POST['YearlyOpt']:"")."/".(isset($_POST['Y_On_NumDay'])?$_POST['Y_On_NumDay']:"")."|".(isset($_POST['Y_On_Months'])?$_POST['Y_On_Months']:"");
	                        break;
	                    case "1"://onthe
	                        $this->RecurPatternOpt = (isset($_POST['YearlyOpt'])?$_POST['YearlyOpt']:"")."/".(isset($_POST['Y_OnThe_FreqOrdinal'])?$_POST['Y_OnThe_FreqOrdinal']:"")."|".(isset($_POST['Y_OnThe_Days'])?$_POST['Y_OnThe_Days']:"")."|".(isset($_POST['Y_OnThe_Months'])?$_POST['Y_OnThe_Months']:(isset($_POST['Y_On_Months'])?$_POST['Y_On_Months']:""));
	                        break;
	                }
	                break;
	            default:	//?
	                break;
	        }

	        $this->RecurRangeOpt = (isset($_POST['RangeRecurrenceStart'])?$_POST['RangeRecurrenceStart']:"")."|".(isset($_POST['RangeRecurOpt'])?$_POST['RangeRecurOpt']:"")."|";
	        
	        switch(isset($_POST['RangeRecurOpt'])?$_POST['RangeRecurOpt']:"")
	        {
	            //0: sin fin, no se utiliza opciones
	            case "1"://numero ocurrencias
	                $this->RecurRangeOpt .= (isset($_POST['R_NumOcurrences'])?$_POST['R_NumOcurrences']:"");
	                break;
	            case "2"://fecha fin
	                $this->RecurRangeOpt .= (isset($_POST['RangeRecurrenceEnd'])?$_POST['RangeRecurrenceEnd']:"");
	                break;
	        }
        }
        
        //-----------------------------------------------------------------------------------------------
        
        $isnewobject = $this->isNewObject();
        
	 	if ($isnewobject)
		{
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			$this->CreationAdminVersion = $this->LastModAdminVersion;
			//@JAPR
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(AgendaSchedID)", "0")." + 1 AS AgendaSchedID".
						" FROM SI_SV_SurveyAgendaScheduler";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->AgendaSchedID = (int)$aRS->fields["AgendaSchedID"];
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			$strAdditionalFields = "";
			$strAdditionalValues = "";
 			if (getMDVersion() >= esveAgendas) {
				$strAdditionalFields .= ', CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$this->LastModAdminVersion.', '.$this->LastModAdminVersion;
			}
			//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
			if (getMDVersion() >= esvAgendaFirsDayWeek) {
				$strAdditionalFields .= ', FirstDayOfWeek';
				$strAdditionalValues .= ', '.$this->FirstDayOfWeek;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_SurveyAgendaScheduler
                    (
                        AgendaSchedID
                        ,AgendaSchedName
                        ,UserID
                        ,CatalogID
                        ,AttributeID
                        ,FilterText
                        ,CreationUserID
                        ,CreationDateID
                        ,LastUserID
                        ,LastDateID
                        ,VersionNum
                        ,CheckInSurvey
                        ,Status
                        ,CaptureStartTime
                        ,RecurPatternType
                        ,RecurPatternOpt
                        ,RecurPatternNum
                        ,RecurRangeOpt
                        ,GenerateAgendasNextDayTime
                        ,checkpointID
                        ,FilterTextAttr {$strAdditionalFields} 
                    )
                    VALUES 
                    (
			            ".$this->AgendaSchedID.
			            ",".$this->Repository->DataADOConnection->Quote($this->AgendaSchedName).
                        ",".$this->UserID.
                        ",".$this->CatalogID.
                        ",".$this->AttributeID.
                        ",".$this->Repository->DataADOConnection->Quote($this->FilterText).
						",".$_SESSION["PABITAM_UserID"].
                        ",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
                        ",".$_SESSION["PABITAM_UserID"].
                        ",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						",1".
                        ",".$this->CheckinSurvey.
                        ",".(is_null($this->Status)?0:$this->Status).
                        ",".$this->Repository->DataADOConnection->Quote($this->getAgendaCaptureStartTime()).
                        ",".$this->Repository->DataADOConnection->Quote((is_null($this->RecurPatternType)?"":$this->RecurPatternType)).
                        ",".$this->Repository->DataADOConnection->Quote((is_null($this->RecurPatternOpt)?"":$this->RecurPatternOpt)).
                        ",".(is_null($this->RecurPatternNum)?"":$this->RecurPatternNum).
                        ",".$this->Repository->DataADOConnection->Quote((is_null($this->RecurRangeOpt)?"":$this->RecurRangeOpt)).
                        ",".$this->Repository->DataADOConnection->Quote($this->getAgendaTime()).
                        ",".(is_null($this->CheckPointID)?0:$this->CheckPointID).
                        ",".$this->Repository->DataADOConnection->Quote((is_null($this->FilterTextAttr)?"":$this->FilterTextAttr)).
					"{$strAdditionalValues})";

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        else 
        {
			//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
        	$this->VersionNum++;
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			$strAdditionalValues = "";
			if (getMDVersion() >= esveAgendas) {
				$strAdditionalValues .= ', LastVersion = '.$this->LastModAdminVersion . ', CheckPointID = ' . $this->CheckPointID . ', CatalogID = ' . $this->CatalogID;
			}
			//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
			if (getMDVersion() >= esvAgendaFirsDayWeek) {
				$strAdditionalValues .= ', FirstDayOfWeek = '.$this->FirstDayOfWeek;
			}
			//@JAPR
            $sql = "UPDATE SI_SV_SurveyAgendaScheduler SET ".
            		" AgendaSchedName = ".$this->Repository->DataADOConnection->Quote($this->AgendaSchedName).
					",LastUserID = ".$_SESSION["PABITAM_UserID"].
					",LastDateID = ".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					",FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					",FilterTextAttr = ". $this->Repository->DataADOConnection->Quote((is_null($this->FilterTextAttr)?"":$this->FilterTextAttr)) .
					",RecurPatternType = ".$this->Repository->DataADOConnection->Quote($this->RecurPatternType).
                    ",RecurPatternOpt = ".$this->Repository->DataADOConnection->Quote($this->RecurPatternOpt).
                    ",RecurPatternNum = ".$this->RecurPatternNum.
                    ",RecurRangeOpt = ".$this->Repository->DataADOConnection->Quote($this->RecurRangeOpt).
					",VersionNum = ".$this->VersionNum.
                    ",CheckinSurvey = ".$this->CheckinSurvey.
                    ",Status = ".(is_null($this->Status)?0:$this->Status).
                    ",CaptureStartTime = ".$this->Repository->DataADOConnection->Quote($this->getAgendaCaptureStartTime()).
                    ",GenerateAgendasNextDayTime = ".$this->Repository->DataADOConnection->Quote($this->getAgendaTime()).
                    ",UserID = ".$this->UserID.
				"{$strAdditionalValues} WHERE AgendaSchedID = ".$this->AgendaSchedID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
		
        /** Actualizamos en la instancia el nombre de usuario */
        $rs = $this->Repository->ADOConnection->Execute( "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO = " . $this->UserID );
        if( $rs && !$rs->EOF )
        {
        	$this->UserName = $rs->fields['NOM_LARGO'];
        }
		
        /** Actualizamos en la instancia el nombre del punto de visita */
        $rs = $this->Repository->DataADOConnection->Execute( "SELECT `name` FROM si_sv_checkpoint WHERE id = " . $this->CheckPointID );
        if( $rs && !$rs->EOF )
        {
        	$this->CheckPointName = $rs->fields['name'];
        }
        
        $this->LastDateID = $currentDate;
        $this->UpdateSurveyIDs();
        
        chdir($strOriginalWD);
		
		return $this;
	}
    
	function remove()
	{
        $sql = "DELETE FROM SI_SV_SurveyAgendaSchedDet WHERE AgendaSchedID = ".$this->AgendaSchedID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveyAgendaScheduler WHERE AgendaSchedID = ".$this->AgendaSchedID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//Regresa el icono de Recalendarizada si la encuesta se encuentra en ese estado (@JAPRWarngin: eventualmente puede regresar un icono de Status
	//para cada agenda)
	function RescheduleStatus()
	{
		$htmlString = '';
		switch ($this->Status)
		{
			case agdRescheduled:
				$htmlString="<img src=images/agendaRescheduled.png title='".translate("Rescheduled")."'>";
				break;
			
			default:
				break;
		}
		
		return $htmlString;
	}

	function readSurveyIDs()
	{
		$this->UserIDs = array();
		$sql = "SELECT SurveyID, Status FROM SI_SV_SurveyAgendaSchedDet WHERE AgendaSchedID = ".$this->AgendaSchedID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaSchedDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$aSurveyID = (int)$aRS->fields["surveyid"];
			$this->SurveyIDs[] = $aSurveyID;
			$this->surveyIDsStatus[$aSurveyID] = (int)@$aRS->fields["status"];
			$tmpSurvey = BITAMSurvey::NewInstanceWithID($this->Repository, $aSurveyID);
            //2015.01.12 JCEM se corrige bug al venir en null
            if ($tmpSurvey){
                $this->SurveysRadioForAgenda[$aSurveyID] = $tmpSurvey->RadioForAgenda;
            }
			$aRS->MoveNext();
		}
	}
	
	function UpdateSurveyIDs()
	{
		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM SI_SV_SurveyAgendaSchedDet WHERE AgendaSchedID = ".$this->AgendaSchedID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaSchedDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		foreach ($this->SurveyIDs as $aSurveyID)
		{
            $intStatus = (int) @$this->surveyIDsStatus[$aSurveyID];
            
			$sqlIns = "INSERT INTO SI_SV_SurveyAgendaSchedDet (AgendaSchedID, SurveyID, Status) 
				VALUES (".$this->AgendaSchedID.",".$aSurveyID.",".$intStatus.")";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaSchedDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}
	
	function isNewObject()
	{
		return ($this->AgendaSchedID < 0);
	}
	
	function getAgendaName()
	{
		if ($this->AgendaSchedName != '')
		{
			return $this->AgendaSchedName;
		}
		else 
		{
			return $this->CatalogName.' - '.$this->MemberName.' - '.$this->FilterText;
		}
	}
	function getAgendaCaptureStartTime()
	{
		return (($this->CaptureStartTime_Hours < 10)?'0':'').$this->CaptureStartTime_Hours.':'.(($this->CaptureStartTime_Minutes < 10)?'0':'').$this->CaptureStartTime_Minutes;
	}
    
	function getAgendaTime()
	{
        $response = '';
        if ($this->GenerateAgendasNextDayTime_Hours !== ''&& $this->GenerateAgendasNextDayTime_Minutes !== ''){
            $response = (($this->GenerateAgendasNextDayTime_Hours < 10)?'0':'').$this->GenerateAgendasNextDayTime_Hours.':'.(($this->GenerateAgendasNextDayTime_Minutes < 10)?'0':'').$this->GenerateAgendasNextDayTime_Minutes;
        }
		return $response;
	}
	
	//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
	//Obtiene la hora de captura de la encuesta de CheckIn en formato de 24hrs (para despliegue, no es una hora completa ni se debe usar para fórmulas)
	function getCheckInTime() {
		return (($this->EntryCheckInStartDate_Hours < 10)?'0':'').$this->EntryCheckInStartDate_Hours.':'.(($this->EntryCheckInStartDate_Minutes < 10)?'0':'').$this->EntryCheckInStartDate_Minutes;
	}
    
	function getAgendaStatus() {
		$status = '';
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		switch($this->Status) {
			case agdOpen:
				$status = translate("Unanswered");
				break;
			case agdCanceled:
				$status = translate("Cancelled");
				break;
			case agdRescheduled:
				$status = translate("Rescheduled");
				break;
			case agdFinished:
				$status = translate("Finished");
				break;
			case agdAnswered:
			default:
				$status = translate("Incomplete");
				break;
		}
		//@JAPR
		return $status;
	}
	
	//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
	function getUserName()
	{
		return (($this->UserName != '')?$this->UserName:$this->UserID);
	}
	//@JAPR
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Agenda Scheduler");
		}
		else
		{
			return $this->getAgendaName();
			//return $this->CatalogName.' - '.$this->MemberName.' - '.$this->FilterText; // .' - '.$this->SurveyName;
		}
	}
	
	function get_QueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}

		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyAgendaScheduler".$strParameters;
		}
		else
		{
			return "BITAM_PAGE=SurveyAgendaScheduler&AgendaSchedID=".$this->AgendaSchedID.$strParameters;
		}
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_Parent()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;
		
		return BITAMSurveyAgendaSchedulerCollection::NewInstance($this->Repository, null, $agUserID, $agEndDate, null, false, $agStartDate, $agCatalogID, $agIpp);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'AgendaSchedID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		require_once("user.inc.php");
		require_once("catalog.inc.php");
		
		$myFields = array();
		
		$isReadOnly = false;
		if(!$this->isNewObject())
		{
			$isReadOnly = true;
		}
		$arrayStatusSurvey = array();
		$arrayStatusSurvey[0] = 0;
		$arrayStatusSurvey[1] = 1;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AgendaSchedName";
		$aField->Title = translate("Description");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//Al cambiar el usuario se deben refrescar los catálogos disponibles
		$userField = BITAMFormField::NewFormField();
		$userField->Name = "UserID";
		$userField->Title = translate("User");
		$userField->Type = "Object";
		$userField->VisualComponent = "Combobox";
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$userField->Options = BITAMeFormsUser::GetUsers($this->Repository, false);
		$userField->IsDisplayOnly = ($isReadOnly && $this->UserID > 0);
		$myFields[$userField->Name] = $userField;
		
		$usedCatalogs = BITAMCatalog::GetUsedCatalogs($this->Repository, null, true);
		//combo from catalog
		//Al cambiar el catálogo se deben refrescar tanto la combo de atributos como la del filtro de atributo y las encuestas que están
		//disponibles utilizando para el usuario y que utilizan dicho catálogo
		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "CatalogID";
		$catalogField->Title = translate("Catalog");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
  		//2015.01.08 JCEM #INHWFA Se agrega none cuando no recibe ningun resultado
        if (count($usedCatalogs) == 0){
            $usedCatalogs[0] = translate("None");
        }
    	$catalogField->Options = $usedCatalogs;
		//@JAPR 2012-03-26: Corregido un bug, al no tener el método OnChange, no sincronizaba las combos al cargar la página
		
		if ($this->isNewObject()) {
                $catalogField->OnChange = "updateAttributeFields()";
        }
		
		$catalogField->IsDisplayOnly = ($isReadOnly && $this->CatalogID > 0);
		$myFields[$catalogField->Name] = $catalogField;
		
		$tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true);
        $catFilterInstance = null;
        foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) {
            $lastField = null;
            $curCatMember = 0;
            $arrKeys = array_keys($tmpCatalog);

            foreach ($tmpCatalog as $ckey => $tmpCatMember) {
                if (!$this->isNewObject()) {
                    if ($this->CatalogID  != $catKey) {
                        break;
                    }
                    $catFilterInstance = BITAMCatalogFilter::NewInstance($this->Repository, $this->CatalogID);
                }
                $tmpMember = BITAMCatalogMember::NewInstanceWithID($this->Repository, $ckey);
                $attributeField = BITAMFormField::NewFormField();
                $attributeField->Name = "DSC_".$tmpMember->ClaDescrip;
                $attributeField->Title = translate($tmpMember->MemberName);
                $attributeField->Type = "Object";
                $attributeField->VisualComponent = "Combobox";
                $dynamicProperty = $attributeField->Name;
                $this->$dynamicProperty = $dynamicProperty;
                $attribFields = null;
                if (!$this->isNewObject()) {
                    if ($this->FilterText != "") {
                        $arrFilterText = explode("|", $this->FilterText);
                        if (isset($catFilterInstance)) {
                            if (!is_null($lastField)) {
                                $memberorder = ($lastField->MemberOrder-1);
                                $memberorder = ($tmpMember->MemberOrder-1);
                                $catFilterInstance->AttribFields[$memberorder] = 'DSC_'.$tmpMember->ClaDescrip;
                                $tempFieldName = $catFilterInstance->AttribFields[$memberorder];
                                $catFilterInstance->$tempFieldName = null;
                                $catFilterInstance->$tempFieldName = $arrFilterText[($curCatMember-1)];
                                /*if (!is_null($lastField)) {
                                    $catFilterInstance->$tempFieldName = $arrFilterText[($curCatMember-1)];
                                }*/
                            }
                        }
                    }
                    //var_dump(json_encode($catFilterInstance->AttribFields));
                    $catalogValues = $catFilterInstance->getCatalogValues(($tmpMember->MemberOrder-1));
                    //$attributeField->Options = (isset($arrFilterText[$curCatMember])) ? (array($arrFilterText[$curCatMember] => $arrFilterText[$curCatMember])) : null;
                    $attributeField->Options = $catalogValues;
                    $lastField = $tmpMember;
                }
                if ($this->FilterText != "") {
                    $arrFilterText = explode("|", $this->FilterText);
                    $this->$dynamicProperty = $arrFilterText[$curCatMember];
                }
                $nextKey = (isset($arrKeys[($curCatMember+1)])) ? $arrKeys[($curCatMember+1)] : null;
                //var_dump($nextKey);
                if (!is_null($nextKey)) {
                    $attribName = "DSC_".$nextKey;
                }

                $myFields[$attributeField->Name] = $attributeField;				
                
                $curCatMember++;
            }
			
		}
		
		//@JAPR 2012-05-30: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
		if ($this->isNewObject())
		{
			foreach ($catalogField->Options as $this->CatalogID => $strCatalogName)
			{
				break;
			}
		}
		
		//atributes combo
		//Al cambiar el atributo se debe refrescar sólo la combo con el filtro de atributo
		$catmemberField = BITAMFormField::NewFormField();
		$catmemberField->Name = "AttributeID";
		$catmemberField->Title = translate("Attribute");
		$catmemberField->Type = "Object";
		$catmemberField->VisualComponent = "Combobox";
		$catmemberField->Options = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository);
		$catmemberField->IsDisplayOnly = ($isReadOnly && $this->AttributeID > 0);
		$myFields[$catmemberField->Name] = $catmemberField;

		//@JAPR 2012-05-30: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
		if ($this->isNewObject())
		{
			if ($this->CatalogID > 0 && isset($catmemberField->Options[$this->CatalogID]) && count($catmemberField->Options[$this->CatalogID]) > 0)
			{
				foreach ($catmemberField->Options[$this->CatalogID] as $this->AttributeID => $strAttributeName)
				{
					break;
				}
			}
		}
				
		$defaultFilter = array();
		$defaultFilter[$this->AttributeID] = array('' => '');
		if ($this->AttributeID > 0 && ($this->isNewObject() || $this->FilterText != ''))
		{
			$defaultFilter[$this->AttributeID] = BITAMSurveyAgendaScheduler::getCatMemberValuesByAttributeID($this->Repository, $this->AttributeID, $this->UserID);
			if ($this->isNewObject())
			{
				foreach ($defaultFilter[$this->AttributeID] as $strKey => $strValue)
				{
					//$this->FilterText = $strValue;
					break;
				}
			}
		}
		$catValuesField = BITAMFormField::NewFormField();
		$catValuesField->Name = "FilterText";
		$catValuesField->Title = translate("Attribute value");
		$catValuesField->Type = "Object";
		$catValuesField->VisualComponent = "Combobox";
		$catValuesField->Options = $defaultFilter;
		
		$myFields[$catValuesField->Name] = $catValuesField;

		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "CheckinSurvey";
		$surveyField->Title = translate("Checkin Survey");
		$surveyField->Type = "Object";
		$surveyField->VisualComponent = "Combobox";
		$arraySurveys = array(0 => translate("None")) + BITAMSurvey::getSurveys($this->Repository);
		$surveyField->Options = $arraySurveys;
		
        $myFields[$surveyField->Name] = $surveyField;		
		
		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "SurveyIDs";
		$surveyField->Title = translate("Surveys");
		$surveyField->Type = "Array";
		$surveyField->Options = BITAMSurveyAgendaScheduler::getAllSurveys($this->Repository);

        $myFields[$surveyField->Name] = $surveyField;
		
        //2014.12.01 JCEM #O5YTV8 se agregan los siguientes campos para recurrencia  --------------------
        $aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartTime";
		$aField->Title = translate("Capture Start Time");
		$aField->Type = "Time";
        $aField->AfterMessage = "(HH:mm)";
		$myFields[$aField->Name] = $aField;
        
        $aField = BITAMFormField::NewFormField();
        $aField->Name = "RecurPatternType";
        $aField->Title = translate("Recurrence pattern");
        $aField->Type = "Object";
        $aField->VisualComponent = "Combobox";
        $aField->Options = array(0 => translate('Daily'), 1 => translate('Weekly'), 4 => translate('Weeks of the month'), 2 => translate('Monthly'), 3 => translate('Yearly'));

        $myFields[$aField->Name] = $aField;
               
        $strAx = '<div id="divRecurOptions"  style="margin-top: -13;"></div>';
        $aField = BITAMFormField::NewFormField();
        $aField->Name = "PatternOptions";
        $aField->Title = translate("Pattern options");
        $aField->AfterMessage = $strAx;

        $myFields[$aField->Name] = $aField;
        
        //la opcion End after no se utilizo
        $strAx1 = '&nbsp;
                    <div id="divRecurOptions" style="margin-top: 5px;display: inline;">
                        <select id="RangeRecurOpt" style="width: 120px;" name="RangeRecurOpt" onchange="javascript:ShowHideRecurrenceFields(\'OR\');">
                            <option selected="" value="0">'.translate("No end date").'</option>
                            <!-- <option value="1">End after</option> -->
                            <option value="2">'.translate("End by").'</option>
                        </select>
                        &nbsp;
                        <span id="spnRangeRecurOpt_EA" style="display:none">
                        :&nbsp;
                            <input type="text" id="R_NumOcurrences" name="R_NumOcurrences" value="1" style="width:30px;text-align: center;">
                            &nbsp;'.translate('occurrences').'.  
                        </span>';
        $aField = BITAMFormField::NewFormField();
		$aField->Name = "RangeRecurrenceStart";
		$aField->Title = translate("Range of recurrence");
		$aField->Type = "Date";
		$aField->FormatMask = "yyyy-MM-dd";
        $aField->BeforeMessage = '<label style="display: inline;">'.translate('Start').'</label>';
        $aField->AfterMessage = $strAx1;

        $myFields[$aField->Name] = $aField;
        
        $aField = BITAMFormField::NewFormField();
		$aField->Name = "RangeRecurrenceEnd";
		$aField->Title = " ";
		$aField->Type = "Date";
        $aField->BeforeMessage = '<label style="display: inline;">'.translate('End').'</label>';
		$aField->FormatMask = "yyyy-MM-dd";
        
        $myFields[$aField->Name] = $aField;
        
        $aField = BITAMFormField::NewFormField();
		$aField->Name = "GenerateAgendasNextDayTime";
		$aField->Title = translate("Generate agendas of next day after this time");
		$aField->Type = "Time";
        $aField->AfterMessage = "(HH:mm)";
        $myFields[$aField->Name] = $aField;

        //--------------------------------------------------------------------------------------------------
        
        return $myFields;
	}
	
	static function getCatMembersByCatalogID($aRepository, $justKeyOfAgendas = null, $catalogID = null, $justParentIDs = null)
	{		
	
		//@JAPR 2014-12-04: Corregido un bug, el campo KeyOfAgenda se utilizó a partir de cierta versión
		$sql = "SELECT CatalogID, MemberID, MemberName, MemberOrder, ParentID, KeyOfAgenda FROM SI_SV_CatalogMember ";
		//@JAPR 2014-12-04: Corregido un bug, el campo KeyOfAgenda se utilizó a partir de cierta versión
		$strWhere = ' WHERE ';
		if(!is_null($justKeyOfAgendas) && getMDVersion() >= esvAgendaRedesign) {
			$sql .= "WHERE KeyOfAgenda > 0 ";
			$strWhere = " AND ";
		}
		if (!is_null($catalogID)) {
			$sql .= $strWhere."CatalogID = ".$catalogID." ";
		}
		$sql .= "ORDER BY CatalogID, MemberOrder";

		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				if (is_null($justParentIDs)) {
					$aGroup[$aRS->fields["memberid"]] = $aRS->fields["membername"];
				} else {
					$aGroup[$aRS->fields["memberid"]] = $aRS->fields["parentid"];
				}
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	static function getAllSurveys ($aRepository) {
		return BITAMSurvey::getSurveys($aRepository, null, true);
	}
	
	static function getSurveysByCatMemberID($aRepository)
	{
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT B.CatMemberID, A.SurveyID, A.SurveyName".
			" FROM SI_SV_Survey A, SI_SV_Question B".
			" WHERE A.SurveyID = B.SurveyID AND B.CatMemberID > 0 AND B.QTypeID <>".qtpOpenNumeric.
			" ORDER BY B.CatMemberID, A.SurveyName";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catmemberid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catmemberid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catmemberid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["surveyid"]] = $aRS->fields["surveyname"];
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	//@JAPRDescontinuada, en v6 ya no aplica
	static function getCatMemberValuesByAttributeID($aRepository, $anAttributeID, $anUserID = null)
	{
		require_once("catalog.inc.php");
		require_once("catalogmember.inc.php");
		require_once("dimension.inc.php");
		
		if (is_null($anUserID))
		{
			$anUserID = $_SESSION["PAtheUserID"];
		}
		$attribValues = array();
		$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $anAttributeID);
		$aDimension = null;
		$strFilter = '';
		if (!is_null($aCatalogMember))
		{
			$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogMember->CatalogID);
			if (!is_null($aCatalog))
			{
				$strFilter = $aCatalog->generateResultFilterByUser($anUserID);
				//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
				if (trim($strFilter) != '')
				{
					$strFilter = " WHERE ".$strFilter;
				}
				//@JAPR
			}
			$aDimension = BITAMDimension::NewInstanceWithDescripID($aRepository, $aCatalogMember->ClaDescrip, -1, false);
		}
		
		$attribValues = array();
		$strResult="";
		if (!is_null($aDimension))
		{
			//Cambia el nombre de la llave para que apunte a la llave subrrogada del catálogo, ya que como es un DISTINCT para efectos de este caso
			//no importa cual valor de la subrrogada se espera, el que sea que regrese será capaz de obtener la descripción de esta dimensión
			//$aDimension->nom_fisicok = strtolower($aDimension->nom_tabla).'key';
			//$aDimension->nom_fisicok_bd = 'MAX('.$aDimension->nom_tabla.'KEY) AS '.$aDimension->nom_tabla.'KEY';
			//$aDimension->getDimensionValuesExt('', $aDimension->nom_fisico);
			//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
			$aDimension->getDimensionValuesExt($strFilter);
			//@JAPR
			//Los valores están contenidos en el Array propio de la dimensión, así que eso es lo que se devuelve
			$attribValues = $aDimension->values;
			if (isset($attribValues[$aDimension->NoApplyValue]))
			{
				unset($attribValues[$aDimension->NoApplyValue]);
			}
		}
		return $attribValues;
	}

	function generateBeforeFormCode($aUser)
 	{	
	?>
   <script type="text/javascript" src="js/jquery/jquery-1.8.0.js"></script>
    
<script language="JavaScript">		
		var initialValuesSet = false;
		
		var filterAttribIDs = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMemberFields = new Array;
		var numFilterAttributes = new Array;
		var lastAttributeID = new Array();
		var isEbavel = new Array();
		<? $tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, null, true);
		foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { ?>
			if (!numFilterAttributes[<?= $catKey ?>]) {
				numFilterAttributes[<?= $catKey ?>] = new Array;
				filterAttribIDs[<?= $catKey ?>] = new Array;
				filterAttribPos[<?= $catKey ?>] = new Array;
				filterAttribMembers[<?= $catKey ?>] = new Array;
				filterAttribMemberFields[<?= $catKey ?>] = new Array;
				numFilterAttributes[<?= $catKey ?>] = new Array;
				lastAttributeID[<?= $catKey ?>] = new Array;
			}
			numFilterAttributes[<?= $catKey ?>] = <?= count($tmpCatalog)?>;
			<? $tmpCurCat = 0;
			foreach ($tmpCatalog as $ckey => $tmpCatMember) {
				$position = $tmpCurCat;
				$attribID = $tmpCatMember;
				//$attribName = $this->AttribFields[$position];
				//$attribName = $tmpCatMember;
				$attribName = 'DSC_'.$tmpCatMember;
				//$attribMemberID = $this->AttribMemberIDs[$attribName]
				$attribMemberID = $ckey;
				?>
				filterAttribIDs[<?= $catKey ?>][<?=$position?>] = <?=$attribID?>;
				filterAttribPos[<?= $catKey ?>]['<?=$attribName?>'] = <?=$position?>;
				filterAttribMembers[<?= $catKey ?>]['<?=$attribName?>'] = <?=$attribMemberID?>;
				filterAttribMemberFields[<?= $catKey ?>][<?=$attribMemberID?>] = '<?=$attribName?>';
				<? 
				$tmpCurCat++; 
				$lastAttributeID = $ckey;
				?>
			<? } ?>
			lastAttributeID[<?= $catKey ?>] = <?=$lastAttributeID?>;
			<? 
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $catKey);
			$isEbavel = ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) ? 1 : 0;
			?>
			isEbavel[<?= $catKey ?>] = <?= $isEbavel ?>;
		<? } ?>
		
		<? if (count($tmpCatalogCollection) > 0) { ?>
		
			function refreshAttributes(attribName, event) {
				var event = (window.event) ? window.event : event;
				/*if ((event.target || event.srcElement).name == '') {
					editing = true;
				}*/
				var isCatalogID = false;
				if (!event) {
					isCatalogID = true;
				} else {
					var objElement = document.getElementsByName((event.target || event.srcElement).name)[0];
					if (objElement == undefined || objElement.name == 'CatalogID') {
						isCatalogID = true;
					}
				}
				objAttrib = document.getElementsByName(attribName)[0];
				var objCmbCatalog = BITAMSurveyAgendaScheduler_SaveForm.CatalogID;
				var intCatalogID = objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
				if (objAttrib.value == 'sv_ALL_sv') {
					for (i = 0;i < numFilterAttributes[intCatalogID];i++) {
						var thisPosition = filterAttribPos[intCatalogID][attribName];
						//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
						var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
						var position = filterAttribPos[intCatalogID][searchAttrib];
						if(position > thisPosition) {
							var obj = document.getElementsByName(searchAttrib)[0];
							if (obj) {
								obj.length = 0;
								var newOption = new Option('<?=translate("All")?>', 'sv_ALL_sv');
								obj.options[obj.options.length] = newOption;
							}
						}
					}
					return;
				}
				var xmlObj = getXMLObject();
				if (xmlObj == null) {
					alert("XML Object not supported");
					return(new Array);
				}
				strMemberIDs = "";
				strParentIDs = "";
				strValues = "";
				var nextAttrib = undefined;
				for(i = 0; i < numFilterAttributes[intCatalogID]; i++) {
					var thisPosition = filterAttribPos[intCatalogID][attribName];
					//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var position = filterAttribPos[intCatalogID][searchAttrib];
					var obj = document.getElementsByName(searchAttrib)[0];
					if(position <= thisPosition) {
						if(strMemberIDs!="") {
							strMemberIDs+="_SVSEPSV_";
						}
						strMemberIDs+=filterAttribMembers[intCatalogID][searchAttrib];
						if(strParentIDs!="") {
							strParentIDs+="_SVSEPSV_";
						}
						strParentIDs+=filterAttribIDs[intCatalogID][position];
						if(strValues!="") {
							strValues+="_SVSEPSV_";
						}
						strValues+=obj.value;
					} else {
                        if (thisPosition == 0 && isCatalogID) {
								nextAttrib = strMemberIDs;
						} else {
							var nextAttribSearch = 'DSC_' + filterAttribIDs[intCatalogID][i];
							nextAttrib = filterAttribMembers[intCatalogID][nextAttribSearch];
						}
						break;
					}
				}
				var catMemberID = (isEbavel[intCatalogID]) ? nextAttrib : lastAttributeID[intCatalogID];
				//var catMemberID = lastAttributeID[intCatalogID];
				var changedMemberID = filterAttribMembers[intCatalogID][attribName];
				if (isCatalogID) {
					changedMemberID = '';
				}
				if (catMemberID !== undefined) {
					xmlObj.open("POST", "getAttribValues.php?CatalogID="+intCatalogID+"&CatMemberID="+catMemberID+"&ChangedMemberID="+changedMemberID+"&strMemberIDs="+strMemberIDs+"&strParentIDs="+strParentIDs+"&strValues="+strValues+'&isAgenda=1', false, false);
					xmlObj.send(null);
					strResult = Trim(unescape(xmlObj.responseText));
					arrayAttrib = strResult.split("_SV_AT_SV_");
					var numAttrib = arrayAttrib.length;
					var refreshAttrib = new Array();
					for (var i = 0; i < numAttrib; i++) {
						if (arrayAttrib[i] != '') {
							var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
							var membID = atribInfo[0];
							var attribValues = atribInfo[1].split("_SV_VAL_SV_");
							var numAtVal = attribValues.length;
							var searchAttrib = filterAttribMemberFields[intCatalogID][membID];
							var obj = document.getElementsByName(searchAttrib)[0];
							if (obj) {
								obj.length = 0;
								var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
								obj.options[obj.options.length] = newOption;
									for (j = 0; j < numAtVal; j++) {
										var newOption = new Option(attribValues[j], attribValues[j]);
										obj.options[obj.options.length] = newOption;
									}
								refreshAttrib[i]=membID;
							}
						}	
					}
				}
				//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
				// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
				for (i = 0; i < numFilterAttributes[intCatalogID]; i++) {
					//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var obj = document.getElementsByName(searchAttrib)[0];
					if(filterAttribPos[intCatalogID][searchAttrib] > filterAttribPos[intCatalogID][attribName] && !inArray(refreshAttrib, filterAttribMembers[intCatalogID][searchAttrib])) {
						obj.length = 0;
						var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
						obj.options[obj.options.length] = newOption;
					}
				}
			}
			
		<? } ?>
		
		function updateUsers()
 		{
 			if (cancel==true || true)
 			{
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					BITAMSurveyAgendaScheduler_FilterText = new Array();
	 			}
 				
				//BITAMSurveyAgendaScheduler_CatalogID_Update();
 				
	 			//updateMemberValues();
 			}
 			//cancel=false;
 		}
		
		var catMemberArray = {};
		var catMemberOrder = {};
		var agendaFilterText = '<?=$this->FilterText?>';
		var firstTime = true;
		
		function setAgendaAttributes () {
			<? $tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, null, true); ?>
			<? foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { ?>
					<? $tmpCurCat = 0; ?>
					catMemberArray[<?=$catKey?>] = {};
					catMemberOrder[<?=$catKey?>] = new Array();
					<? foreach ($tmpCatalog as $ckey => $tmpCatMember) { ?>
						catMemberArray[<?=$catKey?>][<?=$ckey?>] = <?=$tmpCatMember?>;
						catMemberOrder[<?=$catKey?>][<?=$tmpCurCat?>] = <?=$tmpCatMember?>;
						<? $tmpCurCat++; ?>
					<? } ?>
			<? } ?>
		}
		setAgendaAttributes();
		
		
		
		function updateAttributeFields () {
			var objCmbCatalog = BITAMSurveyAgendaScheduler_SaveForm.CatalogID;
			//Se inicializa en null para luego comprobar si se trajeron catalogos y mostrar o no las opciones
			var intCatalogID = null;
			if (objCmbCatalog && objCmbCatalog.options && objCmbCatalog.options.length > 0){
				intCatalogID =objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
			}
			
			var firstMember = true;
			for (var ckey in catMemberOrder) {
				var tmpCat = catMemberOrder[ckey];
				for (var mkey in tmpCat) {
					if (!isNaN(mkey)) {
						var catmemberkey = tmpCat[mkey];
						var rowMember = document.getElementById("Row_DSC_"+catmemberkey);
						//se valida si intCatalogId is null se ocultan estos controles
						rowMember.style.display = (intCatalogID != null && ckey == intCatalogID) ? ((bIsIE)?"inline":"table-row") : "none";
						if (firstMember && (ckey == intCatalogID)) {
							var strAttributeID = 'DSC_'+catmemberkey;
							if (agendaFilterText != '' && firstTime) {
								continue;
							} else {
								refreshAttributes(strAttributeID);
							}
							firstMember = false;
						}
					}
				}
			}
			firstTime = false;
			cancel=false;
			return;
		}
 		
 		function updateFields()
 		{
 			if (cancel==true || true)
 			{
 				var catMember = BITAMSurveyAgendaScheduler_SaveForm.AttributeID;
 				
 				if (initialSelectionCatMember!=null)
 				{
 					for(i=0;i<catMember.options.length;i++)
 					{
 						if (catMember.options[i].value==initialSelectionCatMember)
 						{	
 							catMember.selectedIndex=i;
 							break;
 						}
 					}
 				}
 				
	 			//updateMemberValues();
 			}
 			//cancel=false;
 		}
 		
 		function updateMemberValues()
 		{
 			if (cancel==true || true) // || true)
 			{
 				var intUserID = 0;
 				var cmbUserID = BITAMSurveyAgendaScheduler_SaveForm.UserID;
 				if (cmbUserID != null && cmbUserID.selectedIndex >= 0)
 				{
 					intUserID = cmbUserID.options[cmbUserID.selectedIndex].value;
 				}
 				//Verifica si ya se habían cargado los valores de este atributo, si no es así hace una petición Ajax para obtenerlos
				var myParent = BITAMSurveyAgendaScheduler_SaveForm.AttributeID;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
	        		var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
	        	if (myParentSelection != null)
	        	{
	        		var myNewValues = BITAMSurveyAgendaScheduler_FilterText[myParentSelection];
	        	}
	        	if (myNewValues == null)
	        	{
	        		//No se habían cargado los valores de este atributo
   	        		var myNewValues = new Array();
   	        		
					var xmlObj = getXMLObject();
					
					if (xmlObj == null)
					{
						alert("XML Object not supported");
					}
					else
					{
						var objCmbCatalog = BITAMSurveyAgendaScheduler_SaveForm.CatalogID;
						var intCatalogID =objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
						
					 	xmlObj.open("POST", "getDistinctAttribValues.php?CatalogID="+intCatalogID+"&CatMemberID="+myParentSelection+"&userID="+intUserID, false);
						
					 	xmlObj.send(null);
					 	
					 	strResult = Trim(unescape(xmlObj.responseText));
					 	if (strResult.indexOf('<?=SV_SRV_ERROR?>') > 0)
					 	{
					 		var errorData = strResult.split('<?=SV_SRV_ERROR?>');
					 		if (errorData.length > 1)
					 		{
								alert("Error retrieving attribute's values: (" + errorData[0] + ') ' + errorData[1]);
					 		}
					 		else
					 		{
								alert("Error retrieving attribute's values: " + strResult);
					 		}
				 			//cancel=false;
							return(new Array);
					 	}
					 	else
					 	{
					 		var attribValues = strResult.split("_SV_VAL_");
					 		var numVal = attribValues.length;
					 		for (i = 0; i < numVal; i++)
					 		{
					 			arrValData = attribValues[i].split("<?=SV_DELIMITER?>");
					 			if (arrValData.length > 1)
					 			{
					 				myNewValues[i] = new Array(arrValData[0], arrValData[1]);
					 			}
					 			else
					 			{
					 				myNewValues[i] = new Array(i, arrValData[0]);
					 			}
					 		}
							//myNewValues = attribValues;
		   	        		BITAMSurveyAgendaScheduler_FilterText[myParentSelection] = myNewValues;
					 	}
					}
	        	}
	        	else
	        	{
	        		//Reutiliza los valores previamente cargados
   	        		var myNewValues = BITAMSurveyAgendaScheduler_FilterText[myParentSelection];
		        }
				
				//BITAMSurveyAgendaScheduler_SurveyIDs_Update();
 				BITAMSurveyAgendaScheduler_FilterText_Update();
 				/*
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					var surveyIDs = document.getElementsByName('SurveyIDs[]');
					if(initialSelectionSurveyIDs!=null)
					{
						for(i=0;i<initialSelectionSurveyIDs.length;i++)
						{
							for(j=0;j<surveyIDs.length;j++)
							{
								if(surveyIDs[j].value==initialSelectionSurveyIDs[i])
								{
									surveyIDs[j].checked = true;
									break;
								}
							}
						}
					}
	 			}
				*/
 				var filterText = BITAMSurveyAgendaScheduler_SaveForm.FilterText;
 				
 				if (initialSelectionFilterText!=null)
 				{
 					for(i=0;i<filterText.options.length;i++)
 					{
 						if (filterText.options[i].value==initialSelectionFilterText)
 						{	
 							filterText.selectedIndex=i;
 							break;
 						}
 					}
 				}
 			}
 			cancel=false;
 		}
 		
		//initialSelectionSurveyIDs=null;
 		initialSelectionCatMember = null;
 		initialSelectionFilterText = null;

 		function setInitialValues()
		{
            var catMember = BITAMSurveyAgendaScheduler_SaveForm.AttributeID;
            initialSelectionCatMember = null;
            if (catMember.selectedIndex >= 0)
            {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
            }
                    
            var filterText = BITAMSurveyAgendaScheduler_SaveForm.FilterText;
            initialSelectionFilterText = null;
            if (filterText.selectedIndex >= 0)
            {
	        	initialSelectionFilterText = filterText.options[filterText.selectedIndex].value;
            }
			
            initialValuesSet = true;
            
            
        
            //2014.12.01 JCEM #O5YTV8 se inicializan controles  -------------
            //cuando es nuevo...
            if (parseInt(BITAMSurveyAgendaScheduler_SaveForm.AgendaSchedID.value) < 0){


                ShowHideRecurrenceFields('OR');
                var datef = new Date();
                //se arma el formato 
                datef = datef.getFullYear() + '-' + ((datef.getMonth() + 1 < 10)? ('0' + (datef.getMonth() + 1)):(datef.getMonth() + 1))+ '-' + ((datef.getDate() < 10)? ('0' + datef.getDate()):datef.getDate());

                //inicializa fechas de dia de hoy
                objAx = window.parent.frames['body'].document.getElementsByName('RangeRecurrenceStart')[0];
                if (objAx){
                    objAx.value = datef;
                    objAx._value = datef;
                }
                objAx = window.parent.frames['body'].document.getElementsByName('RangeRecurrenceEnd')[0];
                if (objAx){
                    objAx.value = datef;
                    objAx._value = datef;
                }            
                //default semanalmente
                objAx = window.parent.frames['body'].document.getElementsByName('RecurPatternType')[0];
                if (objAx){objAx.value = 1;}
                //selecciono el dia actual como default
                objAx = window.parent.frames["body"].document.getElementsByClassName("chkDaysCls");
                if (objAx.length> 0){
                    var now = new Date();
                    now = now.getDay();
                    window.parent.frames["body"].document.getElementsByClassName("chkDaysCls")[now].checked = true;
                }
                
                ShowHideRecurrenceFields('RP');
                
                window.parent.frames['body'].document.getElementsByName('GenerateAgendasNextDayTime_Hours')[0].value = '';
                window.parent.frames['body'].document.getElementsByName('GenerateAgendasNextDayTime_Minutes')[0].value = '';
            }else{//si es update...
                //asigno valor a recurrencetype
                window.parent.frames['body'].document.getElementsByName('RecurPatternType')[0].value = BITAMSurveyAgendaScheduler_SaveForm.RecurPatternType.value;

                //mostramos los controles pertinentes
                ShowHideRecurrenceFields('RP');
                //asignamos valores de acuerdo al tipo
<?
                switch($this->RecurPatternType){
                    case frecDay:	//diario
?> 
                window.parent.frames['body'].document.getElementById('DailyOpt').value = "<?= $this->RecurPatternOpt ?>";
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('DailyOpt')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('DailyOpt')[0].disabled = true;
<?
                        }
                        if ($this->RecurPatternOpt == 0){
?> 
                window.parent.frames['body'].document.getElementById('DNumDays').value = "<?= $this->RecurPatternNum ?>";
<?
                            if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('DNumDays')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('DNumDays')[0].disabled = true;
<?
                            }
                        }
                        break;
                    case frecWeek:	//semanal
?> 
                window.parent.frames['body'].document.getElementById('WNumWeeks').value = "<?= $this->RecurPatternNum ?>"; 
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('WNumWeeks')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('WNumWeeks')[0].disabled = true;
<?
                        }
                        $days = explode("|", $this->RecurPatternOpt);
                        $cnt = count($days);
                        for($i=0;$i<$cnt;$i++)
                        {
?> 
                window.parent.frames["body"].document.getElementsByClassName("chkDaysCls")[<?= $days[$i] ?>].checked = true; 
<?
                        }
?>
                var obj = window.parent.frames["body"].document.getElementsByClassName("chkDaysCls");
                if (obj && (obj.length>0)){
                        for(i=obj.length-1;i>=0;i--){
                            var ax = obj[i];
                            ax.className = 'disabled';
                            ax.disabled = true;
                        }
                }
<?
                        break;
                    case frecWeekOfMonth:	//@AAL 14/04/2015: Opción agregada para crear agendas por semanas del mes

?> 
				var RecurPatternOpt = "<?= $this->RecurPatternOpt ?>";
				RecurPatternOpt = RecurPatternOpt.split('/');
				document.getElementById('The').value = RecurPatternOpt[0];
				RecurPatternOpt = RecurPatternOpt[1].split('|');
                document.getElementById('The_FreqOrdinal').value = RecurPatternOpt[0];
                document.getElementById('The_DaysWeek').value = RecurPatternOpt[1];
                /* //Este elemento no es indispensable en esta opción, sale sobrando
                document.getElementById('The_DaysWeek').value = RecurPatternOpt[2];*/
<?
					if($this->Mode =='view'){
?>
				document.getElementsByName('The')[0].className = 'disabled';
                document.getElementsByName('The')[0].disabled = true;
                document.getElementsByName('The_FreqOrdinal')[0].className = 'disabled';
                document.getElementsByName('The_FreqOrdinal')[0].disabled = true;
                document.getElementsByName('The_DaysWeek')[0].className = 'disabled';
                document.getElementsByName('The_DaysWeek')[0].disabled = true;
<?
					}
                        break;
                    case frecMonth:	//mensual
?> 
                window.parent.frames['body'].document.getElementById('M_Day_NumMonth').value = "<?= $this->RecurPatternNum ?>"; 
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('M_Day_NumMonth')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('M_Day_NumMonth')[0].disabled = true;
<?
                        }
                        
                        $mopt = explode("/",$this->RecurPatternOpt);
                        $opt = explode("|", $mopt[1]);
                        
?> 
                window.parent.frames['body'].document.getElementById('MonthlyOpt').value = "<?= $mopt[0] ?>"; 
                
                //mostramos los controles pertinentes
                ShowHideRecurrenceFields('OM');
                //asignamos valores de acuerdo al tipo
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('MonthlyOpt')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('MonthlyOpt')[0].disabled = true;
<?
                        }
                        //monthlyopt
                        switch($mopt[0]){
                            case "0"://day
                                $cnt = count($opt);
                                for($i=0;$i<$cnt;$i++){
?> 
                var obj = window.parent.frames['body'].document.getElementById('d<?= $opt[$i] ?>');
                if (obj){
                    obj.setAttribute('active','');
                    obj.style.backgroundColor = 'rgb(186, 210, 224)'; 
                }
<?
                                }        
        
                                if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('tDaysOfMonth')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('tDaysOfMonth')[0].disabled = true;
<?
                                }else{
?>
                window.parent.frames['body'].document.getElementsByName('tDaysOfMonth')[0].className = '';
<?
                                }
?> 
                window.parent.frames['body'].document.getElementById('tDaysOfMonthSel').value = "<?= $mopt[1] ?>"; 
<?
                                break;
                            case "1"://the
?> 
                window.parent.frames['body'].document.getElementById('M_The_FreqOrdinal').value = "<?= $opt[0] ?>"; 
                window.parent.frames['body'].document.getElementById('M_The_DaysWeek').value = "<?= $opt[1] ?>"; 
                window.parent.frames['body'].document.getElementById('M_The_NumMonth').value = "<?= $opt[2] ?>"; 
<?
                                if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('M_The_FreqOrdinal')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('M_The_FreqOrdinal')[0].disabled = true;
                window.parent.frames['body'].document.getElementsByName('M_The_DaysWeek')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('M_The_DaysWeek')[0].disabled = true;
                window.parent.frames['body'].document.getElementById('M_The_NumMonth').className = 'disabled';
                window.parent.frames['body'].document.getElementById('M_The_NumMonth').disabled = true;
<?
                                }

                                break;
                        }
                        break;
                    case frecYear:	//yearly
?> 
                window.parent.frames['body'].document.getElementById('YNumYears').value = "<?= $this->RecurPatternNum ?>"; 
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('YNumYears')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('YNumYears')[0].disabled = true;
<?
                        }
                        $mopt = explode("/", $this->RecurPatternOpt);
                        $opt = explode("|", $mopt[1]);
                        
?> 
                window.parent.frames['body'].document.getElementById('YearlyOpt').value = "<?= $mopt[0] ?>"; 
                
                //mostramos los controles pertinentes
                ShowHideRecurrenceFields('OY');
                //asignamos valores de acuerdo al tipo
                
<?
                        if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('YearlyOpt')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('YearlyOpt')[0].disabled = true;
<?
                        }
                        //yearlyopt
                        switch($mopt[0]){
                            case "0"://on
?> 
                window.parent.frames['body'].document.getElementById('Y_On_NumDay').value = "<?= $opt[0] ?>"; 
                window.parent.frames['body'].document.getElementById('Y_On_Months').value = "<?= $opt[1] ?>"; 
<?
                                if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('Y_On_NumDay')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('Y_On_NumDay')[0].disabled = true;
                window.parent.frames['body'].document.getElementsByName('Y_On_Months')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('Y_On_Months')[0].disabled = true;
<?
                                }
                                break;
                            case "1"://on the 
?> 
                window.parent.frames['body'].document.getElementById('Y_OnThe_FreqOrdinal').value = "<?= $opt[0] ?>"; 
                window.parent.frames['body'].document.getElementById('Y_OnThe_Days').value = "<?= $opt[1] ?>"; 
                window.parent.frames['body'].document.getElementById('Y_OnThe_Months').value = "<?= $opt[2] ?>"; 
<?
                                if($this->Mode =='view'){
?>
                window.parent.frames['body'].document.getElementsByName('YearlyOpt')[0].className = 'disabled';
                window.parent.frames['body'].document.getElementsByName('YearlyOpt')[0].disabled = true;
                
                window.parent.frames['body'].document.getElementById('Y_OnThe_FreqOrdinal').className = 'disabled';
                window.parent.frames['body'].document.getElementById('Y_OnThe_FreqOrdinal').disabled = true;
                window.parent.frames['body'].document.getElementById('Y_OnThe_Days').className = 'disabled';
                window.parent.frames['body'].document.getElementById('Y_OnThe_Days').disabled = true;
                window.parent.frames['body'].document.getElementById('Y_OnThe_Months').className = 'disabled';
                window.parent.frames['body'].document.getElementById('Y_OnThe_Months').disabled = true;
<?
                                }
                        }
                        break;
                }
                
                $mopt = explode("|", $this->RecurRangeOpt);
                //recurrange
?> 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceStart')[0].value = "<?= $mopt[0] ?>"; 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceStart')[0]._value = "<?= $mopt[0] ?>"; 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceStart')[0].defaultValue = "<?= $mopt[0] ?>"; 
                window.parent.frames['body'].document.getElementById('RangeRecurOpt').value = "<?= $mopt[1] ?>"; 
               
                ShowHideRecurrenceFields('OR');
<?
                switch ($mopt[1]){
                    case "1":
?> 
                window.parent.frames['body'].document.getElementById('R_NumOcurrences').value = "<?= $mopt[2] ?>"; 
<?
                        break;
                    case "2":
?> 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceEnd')[0].value = "<?= $mopt[2] ?>"; 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceEnd')[0]._value = "<?= $mopt[2] ?>"; 
                window.parent.frames['body'].document.getElementsByName('RangeRecurrenceEnd')[0].defaultValue = "<?= $mopt[2] ?>"; 
<?
                        break;
                }
                
                if (is_array($this->GenerateAgendasNextDayTime) && count($this->GenerateAgendasNextDayTime)==2 && $this->GenerateAgendasNextDayTime[0] == ''){
?>
            window.parent.frames['body'].document.getElementsByName('GenerateAgendasNextDayTime_Hours')[0].value = '';
<?
                }
                
                if (is_array($this->GenerateAgendasNextDayTime) && count($this->GenerateAgendasNextDayTime)==2 && $this->GenerateAgendasNextDayTime[1] == ''){
?>
            window.parent.frames['body'].document.getElementsByName('GenerateAgendasNextDayTime_Minutes')[0].value = '';
<?
                }
?>
            }
        //----------------------------------------------------------------
		}
		
 		function verifyFieldsAndSave(target)
 		{
			var strAlert = '';
            var objToEval;
			
            objToEval =BITAMSurveyAgendaScheduler_SaveForm.AgendaSchedName;
 			if(!(objToEval) || Trim(objToEval.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Description"))?>';
 			}
 			

  	 		objToEval = BITAMSurveyAgendaScheduler_SaveForm.CatalogID;
            if(!(objToEval) || parseInt(objToEval.value) <= 0){
                strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Catalog"))?>';
            }
 			
  	 		objToEval = BITAMSurveyAgendaScheduler_SaveForm.AttributeID;
            if(!(objToEval) || parseInt(objToEval.value) <= 0){
                strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Attribute"))?>';
            }
 			
            //JCEM validaciones para recurrencia, pattern options
            objToEval = BITAMSurveyAgendaScheduler_SaveForm.RecurPatternType;
            if (!(objToEval) || parseInt(objToEval.value) < 0) {
                    strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Recurrence pattern"))?>';
            }else{
                
                switch(parseInt(objToEval.value)){
                    case 0: //diario
                        objToEval = BITAMSurveyAgendaScheduler_SaveForm.DailyOpt;
                        if (!(objToEval) || parseInt(objToEval.value) < 0) {
                            strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Type")))?>';
                        }else{
                            objToEval = BITAMSurveyAgendaScheduler_SaveForm.DNumDays;
                            if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                                strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("day(s)")))?>';
                            }
                        }
                        break;
                    case 1: //semanal
                        objToEval = BITAMSurveyAgendaScheduler_SaveForm.WNumWeeks;
                        if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                            strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Recur every")." ".translate("week(s) on")))?>';
                        }
                        //los dias checked
                        objToEval = window.parent.frames["body"].document.querySelectorAll(".chkDaysCls:checked ");
                        if (!(objToEval) || objToEval.length == 0){
                            strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Day(s) of week")))?>';
                        }
                        break;
                    /*@AAL 14/04/2015: Opción agregada para crear agendas por semanas del mes */
                    case 4: //Semanas del mes
                    	//Por lo pronto no hay nada que validar
                    break;

                    case 2: //mensual
                         objToEval = BITAMSurveyAgendaScheduler_SaveForm.MonthlyOpt;
                        if (!(objToEval) || parseInt(objToEval.value) < 0){
                            strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Type")))?>';
                        }else{
                            
                            switch(parseInt(objToEval.value)){
                                case 0://dia
                                    objToEval = window.parent.frames["body"].document.querySelectorAll("#tDaysOfMonth td[active]");
                                    if (!(objToEval) || objToEval.length == 0){
                                        strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Day of the month")))?>';
                                    }
                                    
                                    objToEval = BITAMSurveyAgendaScheduler_SaveForm.M_Day_NumMonth;
                                    if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                                        strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("of each")." ".translate("month(s)")))?>';
                                    }
                                    
                                    break;
                                case 1://el
                                    objToEval = BITAMSurveyAgendaScheduler_SaveForm.M_The_NumMonth;
                                    if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                                        strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("of each")." ".translate("month(s)")))?>';
                                    }
                                    break;
                                default:
                                    break;
                            }
                       }
                        break;
                    case 3: //anual
                        objToEval = BITAMSurveyAgendaScheduler_SaveForm.YNumYears;
                        if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                            strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("Recur every")." ".translate("year(s)")))?>';
                        }
                        objToEval = BITAMSurveyAgendaScheduler_SaveForm.YearlyOpt
                        if (!(objToEval) || parseInt(objToEval.value) < 0){
                            if (objToEval.value == 0){
                                objToEval = BITAMSurveyAgendaScheduler_SaveForm.Y_On_NumDay
                                if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <= 0 || objToEval.value.toString().indexOf('.') != -1){
                                    strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),(translate("Pattern options").": ".translate("On")." ".translate("Day of the month")))?>';
                                }else{
                                        var ax = objToEval.value;
                                        objToEval = BITAMSurveyAgendaScheduler_SaveForm.Y_On_Months;
                                        if (objtoEval){
                                            var nMonthDays = numDaysOfMonth(2014,objtoEval.value);
                                            if (ax > nMonthDays){
                                                strAlert+= '\n'+'<?=translate("Pattern options").": ".translate("On")." ".translate("Day of the month").' '.translate('the value should be between').' 1 '.translate('and')?>' + ax;
                                            }
                                        }
                                        
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            
            var dteStart = new Date();
            dteStart = dteStart.getFullYear() + '-' + ((dteStart.getMonth() + 1 < 10)? ('0'+ dteStart.getMonth() + 1):(dteStart.getMonth() + 1))+ '-' + ((dteStart.getDate() < 10)? ('0' + dteStart.getDate()):dteStart.getDate());
            
            objToEval = BITAMSurveyAgendaScheduler_SaveForm.RangeRecurrenceStart;
            if (objToEval.value == '' || objToEval.value == '0000-00-00') {
                    objToEval.value = dteStart;
                    objToEval._value = dteStart;
            }else{
                dteStart = BITAMSurveyAgendaScheduler_SaveForm.RangeRecurrenceStart;
            }
            
            objToEval = BITAMSurveyAgendaScheduler_SaveForm.RangeRecurOpt;
            switch(parseInt(objToEval.value)){
                case 1://por ocurrencias
                    objToEval = BITAMSurveyAgendaScheduler_SaveForm.R_NumOcurrences;
                    if (!(objToEval) || isNaN(parseInt(objToEval.value)) || parseInt(objToEval.value) <=0 || objToEval.value.toString().indexOf('.') != -1){
                        strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Pattern options").": ".translate("occurrences") )?>';
                    }
                    break;
                case 2://por fecha fin
                    objToEval = BITAMSurveyAgendaScheduler_SaveForm.RangeRecurrenceEnd;
                    if (!(objToEval) || objToEval.value == '' || objToEval.value == '0000-00-00') {
                        strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Pattern options").": ".translate("End Date") )?>';
                    }else{
                        var dteToEval = BITAMSurveyAgendaScheduler_SaveForm.RangeRecurrenceEnd.value.split('-');
                        dteToEval = new Date(parseInt(dteToEval[0]), parseInt(dteToEval[1])-1, parseInt(dteToEval[2]));
                        dteStart = dteStart.value.split('-');
                        dteStart = new Date(parseInt(dteStart[0]), parseInt(dteStart[1])-1, parseInt(dteStart[2]));
                        
                        if (dteStart > dteToEval){
                            strAlert+= '\n'+'<?=sprintf(translate("The start date of capture must be greater than or equal to the end date of capture"),translate("Recurrence pattern"))?>';
                        }
                    } 
                    break;
            }
            
            //despliegue de los mensajes de error
            if(Trim(strAlert)!='')
            {
                alert(strAlert);
            }
            else
            {
                BITAMSurveyAgendaScheduler_Ok(target);
            }
            
        }
        //2014.12.01 JCEM #O5YTV8
        //funcion para cambiar los controles de las popciones de recurrencia
        function ShowHideRecurrenceFields(Type)
        {
            var Case = "";
            var divAx = null;
            
            if (Type === "RP"){ //rp:recurrence pattern
                Case = window.parent.frames['body'].document.getElementsByName('RecurPatternType')[0].value;
                divAx = window.parent.frames['body'].document.getElementById('divRecurOptions');
                var strAx = ""; 

                if (divAx)
                {
                    switch(Case) {  
                        case <?=frecDay?>:	//diario
                            strAx = '<select id="DailyOpt" name="DailyOpt" style="width: 200px;" onchange="javascript:ShowHideRecurrenceFields(\'OD\');">\n\
                                            <option selected="" value="0"><?= translate('Every')?></option>\n\
                                            <option value="1"><?= translate('Every weekday') ?> </option>\n\
                                    </select> &nbsp;\n\
                                    <span id="spnDNumDays"> \n\
                                        <input title="1-364" type="text" id="DNumDays" name="DNumDays" value="1" style="width:30px;text-align: center;">\n\
                                        <?= translate('day(s)')?> \n\
                                    </span> '; 
                                            
                            divAx.innerHTML = strAx;
                            
                            break;
                        case <?=frecWeek?>: //semanal
                            var now = new Date();
                            now = now.getDay();
                            strAx = '<span id="spnWNumWeeks" style="display: inline;">\n\
                                        &nbsp;&nbsp;<?= translate('Recur every') ?> \n\
                                        <input title="1-52" type="text" id="WNumWeeks" name="WNumWeeks" value="1" style="width:30px;text-align: center;">  \n\
                                        <?= translate('week(s) on')?>:\n\
                                    </span>\n\
                                    <br><br>\n\
                                     <div id="divWDays" name="divWDays" style="overflow:auto;border:1px solid #666;height:30px;width: 50%;display: table-cell;vertical-align: middle;">\n\
                                        &nbsp;\n\
                                        <input type="checkbox" value="0" class="chkDaysCls" id="WDaysSu" name="WDaysSu" style="vertical-align: middle;">&nbsp;<?= translate('Sunday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="1" class="chkDaysCls" id="WDaysM" name="WDaysM" style="vertical-align: middle;">&nbsp;<?= translate('Monday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="2" class="chkDaysCls" id="WDaysTu" name="WDaysTu" style="vertical-align: middle;">&nbsp;<?= translate('Tuesday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="3" class="chkDaysCls" id="WDaysW" name="WDaysW" style="vertical-align: middle;">&nbsp;<?= translate('Wednesday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="4" class="chkDaysCls" id="WDaysTh" name="WDaysTh" style="vertical-align: middle;">&nbsp;<?= translate('Thursday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="5" class="chkDaysCls" id="WDaysF" name="WDaysF" style="vertical-align: middle;">&nbsp;<?= translate('Friday') ?> &nbsp;&nbsp;\n\
                                        <input type="checkbox" value="6" class="chkDaysCls" id="WDaysSa" name="WDaysSa" style="vertical-align: middle;">&nbsp;<?= translate('Saturday') ?> &nbsp;&nbsp;\n\
                                        <input type="hidden" name="SurveyIDs_EmptySelection" value="0" class="">\n\
                                    </div>';
                                            
                            divAx.innerHTML = strAx;
                        break;
						
                        /*@AAL 09/04/2015: Modificado Para agendar solo en semanas del mes */
                        case <?=frecWeekOfMonth?>: //@AAL Semanas del Mes (Weeks of the month)

                            strAx ='<select id="The" style="width: 200px;" name="The">\n\
                                        <option value="1"><?= translate('The') ?></option>\n\
                                    </select>\n\
                                    <span style="display: inline;">\n\
                                        <select id="The_DaysWeek" style="width: 97px;" name="The_DaysWeek">\n\
                                            <option value="3"><?= translate('Sunday') ?></option>\n\
                                            <option value="4"><?= translate('Monday') ?></option>\n\
                                            <option value="5"><?= translate('Tuesday') ?></option>\n\
                                            <option value="6"><?= translate('Wednesday') ?></option>\n\
                                            <option value="7"><?= translate('Thursday') ?></option>\n\
                                            <option value="8"><?= translate('Friday') ?></option>\n\
                                            <option value="9"><?= translate('Saturday') ?></option>\n\
                                        </select> <?= translate('Of') ?> <?= translate('The') ?> \n\
                                        <select id="The_FreqOrdinal" style="width: 100px;" name="The_FreqOrdinal">\n\
                                            <option value="0"><?= translate('First') ?></option>\n\
                                            <option value="1"><?= translate('Second') ?></option>\n\
                                            <option value="2"><?= translate('Third') ?></option>\n\
                                            <option value="3"><?= translate('Fourth') ?></option>\n\
                                            <option value="4"><?= translate('Last') ?></option>\n\
                                        </select> <?= translate('Week') ?>\n\
                                    </span>';
                            divAx.innerHTML = strAx;
                            //Seleccionar el Dia actual de la semana.
                            document.getElementById("The_DaysWeek").value = new Date().getDay() + 3;
                        break;
                        //@AAL

                        case <?=frecMonth?>: //mensual
                            var now = new Date();
                            now = now.getDay() + 3 ;
                            
                            strAx ='<select id="MonthlyOpt" style="width: 200px;" name="MonthlyOpt" onchange="javascript:ShowHideRecurrenceFields(\'OM\');" >\n\
                                        <option selected="" value="0"><?= translate('Day') ?></option>\n\
                                        <option value="1"><?= translate('The') ?></option>\n\
                                    </select>\n\
                                    <span id="spnMonthOpt_Day" style="display: inline;">\n\
                                        &nbsp;&nbsp;\n\
                                        <div style="display:inline-flex">\n\
                                            <table id="tDaysOfMonth" name="tDaysOfMonth" style="font-size:12px;border-style:solid;border-width:1px">\n\
                                                <thead>\n\
                                                    <tr>\n\
                                                        <th colspan="7" class="datepicker-switch" style="text-align: center;background-color: rgb(64, 120, 153);color: white;">\n\
                                                            <?= translate('Days of month')?></th>\n\
                                                    </tr>\n\
                                                </thead>\n\
                                                <tbody style="cursor:pointer;border:1px;border-style:solid;">\n\
                                                    <tr>\n\
                                                        <td class="day" id="d1">1</td>\n\
                                                        <td class="day" id="d2">2</td>\n\
                                                        <td class="day" id="d3">3</td>\n\
                                                        <td class="day" id="d4">4</td>\n\
                                                        <td class="day" id="d5">5</td>\n\
                                                        <td class="day" id="d6">6</td>\n\
                                                        <td class="day" id="d7">7</td>\n\
                                                    </tr>\n\
                                                    <tr>\n\
                                                        <td class="day" id="d8">8</td>\n\
                                                        <td class="day" id="d9">9</td>\n\
                                                        <td class="day" id="d10">10</td>\n\
                                                        <td class="day" id="d11">11</td>\n\
                                                        <td class="day" id="d12">12</td>\n\
                                                        <td class="day" id="d13">13</td>\n\
                                                        <td class="day" id="d14">14</td>\n\
                                                    </tr>\n\
                                                    <tr>\n\
                                                        <td class="day" id="d15">15</td>\n\
                                                        <td class="day" id="d16">16</td>\n\
                                                        <td class="day" id="d17">17</td>\n\
                                                        <td class="day" id="d18">18</td>\n\
                                                        <td class="day" id="d19">19</td>\n\
                                                        <td class="day" id="d20">20</td>\n\
                                                        <td class="day" id="d21">21</td>\n\
                                                    </tr>\n\
                                                    <tr>\n\
                                                        <td class="day" id="d22">22</td>\n\
                                                        <td class="day" id="d23">23</td>\n\
                                                        <td class="day" id="d24">24</td>\n\
                                                        <td class="day" id="d25">25</td>\n\
                                                        <td class="day" id="d26">26</td>\n\
                                                        <td class="day" id="d27">27</td>\n\
                                                        <td class="day" id="d28">28</td>\n\
                                                    </tr>\n\
                                                    <tr>\n\
                                                        <td class="day" id="d29">29</td>\n\
                                                        <td class="day" id="d30">30</td>\n\
                                                        <td class="day" id="d31">31</td>\n\
                                                        <td class="dayoff" id="n0"></td>\n\
                                                        <td class="dayoff" id="n1"></td>\n\
                                                        <td class="dayoff" id="n2"></td>\n\
                                                        <td class="dayoff" id="n3"></td>\n\
                                                    </tr>\n\
                                                </tbody>\n\
                                                <tfoot>\n\
                                                   <input type="text" id="tDaysOfMonthSel" name="tDaysOfMonthSel" value="" style="display:none">\n\
                                                </tfoot>\n\
                                            </table>\n\
                                        </div> \n\
                                        &nbsp;&nbsp;\n\
                                        <?= translate('of each') ?> \n\
                                        <input type="text" id="M_Day_NumMonth" name="M_Day_NumMonth" value="1" style="width:30px;text-align: center;"> \n\
                                        <?= translate('month(s)') ?>. \n\
                                    </span>\n\
                                    <span id="spnMonthOpt_The" style="display: none;">\n\
                                        <select id="M_The_FreqOrdinal" style="width: 100px;" name="M_The_FreqOrdinal">\n\
                                            <option selected="" value="0"><?= translate('First') ?></option>\n\
                                            <option value="1"><?= translate('Second') ?></option>\n\
                                            <option value="2"><?= translate('Third') ?></option>\n\
                                            <option value="3"><?= translate('Fourth') ?></option>\n\
                                            <option value="4"><?= translate('Last') ?></option>\n\
                                        </select>\n\
                                        <select id="M_The_DaysWeek" style="width: 97px;" name="M_The_DaysWeek">\n\
                                            <option selected="" value="0"><?= translate('Day') ?></option>\n\
                                            <option value="1"><?= translate('Weekday') ?></option>\n\
                                            <option value="2"><?= translate('Weekend day') ?></option>\n\
                                            <option value="3"><?= translate('Sunday') ?></option>\n\
                                            <option value="4"><?= translate('Monday') ?></option>\n\
                                            <option value="5"><?= translate('Tuesday') ?></option>\n\
                                            <option value="6"><?= translate('Wednesday') ?></option>\n\
                                            <option value="7"><?= translate('Thursday') ?></option>\n\
                                            <option value="8"><?= translate('Friday') ?></option>\n\
                                            <option value="9"><?= translate('Saturday') ?></option>\n\
                                        </select>\n\
                                        <?= translate('of each') ?> \n\
                                        <input type="text" id="M_The_NumMonth" name="M_The_NumMonth" value="1" style="width:30px;text-align: center;"> \n\
                                        <?= translate('month(s)') ?>. \n\
                                    </span>';
                                            
                            divAx.innerHTML = strAx;
                            
                             //selecciono el dia actual como default
                            window.parent.frames["body"].document.getElementById("M_The_DaysWeek").value = now;
                            //agrego onlclick al calendar
                            $('.day').on('click', function() {
                                    if ( window.parent.frames["body"].document.getElementById("tDaysOfMonth").className !== 'disabled'){
                                        if ($(this).attr('active') == undefined){
                                             $(this).attr('active','');
                                             $(this).css('background-color', 'rgb(186, 210, 224)');
                                        } else{
                                             $(this).removeAttr('active');
                                             $(this).css('background-color', '');
                                        }
                                        var ob = window.parent.frames["body"].document.querySelectorAll("#tDaysOfMonth td[active]");
                                        var ob2 = window.parent.frames["body"].document.getElementById('tDaysOfMonthSel');
                                        if (ob){
                                            ob2.value = "";
                                            for(i=0; i < ob.length; i++){
                                               ob2.value+=ob[i].innerHTML+'|'; 
                                            }
                                            ob2.value=ob2.value.substring(0, ob2.value.length-1);
                                        }
                                    }
                            });
                            break;
                        case <?=frecYear?>: //anual
                            var now = new Date();
                            var nowd = now.getDay()+ 3;
                            var nowm= now.getMonth();
                            strAx ='<span id="spnYNumYears" style="display: inline;">&nbsp;&nbsp;<?= translate('Recur every') ?>: \n\
                                        <input type="text" id="YNumYears" name="YNumYears" value="1" style="width:30px;text-align: center;">  \n\
                                        <?= translate('year(s)') ?>.\n\
                                    </span>\n\
                                    <br><br>\n\
                                    <select id="YearlyOpt" style="width: 80px;" name="YearlyOpt" onchange="javascript:ShowHideRecurrenceFields(\'OY\');">\n\
                                        <option selected="" value="0"><?= translate('On') ?></option>\n\
                                        <option value="1"><?= translate('On the') ?></option>\n\
                                    </select>\n\
                                    &nbsp;\n\
                                    <span id="spnYearlyOpt_On" style="display:inline">\n\
                                        <input type="text" id="Y_On_NumDay" name="Y_On_NumDay" value="1" style="width:30px;text-align: center;">&nbsp;  \n\
                                        <select id="Y_On_Months" style="width: 117px;" name="Y_On_Months">\n\
                                            <option selected="" value="0"><?= translate('January') ?></option>\n\
                                            <option value="1"><?= translate('February') ?></option>\n\
                                            <option value="2"><?= translate('March') ?></option>\n\
                                            <option value="3"><?= translate('April') ?></option>\n\
                                            <option value="4"><?= translate('May') ?></option>\n\
                                            <option value="5"><?= translate('June') ?></option>\n\
                                            <option value="6"><?= translate('July') ?></option>\n\
                                            <option value="7"><?= translate('August') ?></option>\n\
                                            <option value="8"><?= translate('September') ?></option>\n\
                                            <option value="9"><?= translate('October') ?></option>\n\
                                            <option value="10"><?= translate('November') ?></option>\n\
                                            <option value="11"><?= translate('December') ?></option>\n\
                                        </select>\n\
                                    </span>\n\
                                    <span id="spnYearlyOpt_OnThe" style="display:none">\n\
                                        <select id="Y_OnThe_FreqOrdinal" style="width: 100px;" name="Y_OnThe_FreqOrdinal">\n\
                                            <option selected="" value="0"><?= translate('First') ?></option>\n\
                                            <option value="1"><?= translate('Second') ?></option>\n\
                                            <option value="2"><?= translate('Third') ?></option>\n\
                                            <option value="3"><?= translate('Fourth') ?></option>\n\
                                            <option value="4"><?= translate('Last') ?></option>\n\
                                        </select>\n\
                                        <select id="Y_OnThe_Days" style="width: 97px;" name="Y_OnThe_Days">\n\
                                            <option selected="" value="0"><?= translate('Day') ?></option>\n\
                                            <option value="1"><?= translate('Weekday') ?></option>\n\
                                            <option value="2"><?= translate('Weekend day') ?></option>\n\
                                            <option value="3"><?= translate('Sunday') ?></option>\n\
                                            <option value="4"><?= translate('Monday') ?></option>\n\
                                            <option value="5"><?= translate('Tuesday') ?></option>\n\
                                            <option value="6"><?= translate('Wednesday') ?></option>\n\
                                            <option value="7"><?= translate('Thursday') ?></option>\n\
                                            <option value="8"><?= translate('Friday') ?></option>\n\
                                            <option value="9"><?= translate('Saturday') ?></option>\n\
                                        </select>&nbsp; \n\
                                        <?= translate('Of') ?>&nbsp; \n\
                                        <select id="Y_OnThe_Months" style="width: 117px;" name="Y_OnThe_Months">\n\
                                             <option selected="" value="0"><?= translate('January') ?></option>\n\
                                            <option value="1"><?= translate('February') ?></option>\n\
                                            <option value="2"><?= translate('March') ?></option>\n\
                                            <option value="3"><?= translate('April') ?></option>\n\
                                            <option value="4"><?= translate('May') ?></option>\n\
                                            <option value="5"><?= translate('June') ?></option>\n\
                                            <option value="6"><?= translate('July') ?></option>\n\
                                            <option value="7"><?= translate('August') ?></option>\n\
                                            <option value="8"><?= translate('September') ?></option>\n\
                                            <option value="9"><?= translate('October') ?></option>\n\
                                            <option value="10"><?= translate('November') ?></option>\n\
                                            <option value="11"><?= translate('December') ?></option>\n\
                                        </select>\n\
                                    </span>\n\
                                    ';
                            
                            divAx.innerHTML = strAx;
                            
                            //selecciono el dia actual como default
                            window.parent.frames["body"].document.getElementById("Y_OnThe_Days").value = nowd;
                            //selecciono el mes actual default
                            window.parent.frames["body"].document.getElementById("Y_On_Months").value = nowm;
                            window.parent.frames["body"].document.getElementById("Y_OnThe_Months").value = nowm;
                            break;
                        default:
                            strAx = "";
                            break;
                    }
                   // divAx.innerHTML = strAx;
                }

            }
  
            if (Type === "OD"){ //op1:opcion 1: diariamente, cada...
                divAx = window.parent.frames["body"].document.getElementById("spnDNumDays");
                Case = window.parent.frames['body'].document.getElementById('DailyOpt').value;
                if (divAx){
                    switch(Case)    {
                        case "0":   //every...
                            divAx.style.display="inline";
                            break;
                        case "1":   //every weekday
                            divAx.style.display="none";
                            break;
                        default:
                            break;   
                    }
                }
            }
                        
            if (Type === "OM"){
                Case = window.parent.frames['body'].document.getElementById('MonthlyOpt').value;
                divAx = window.parent.frames["body"].document.getElementById("spnMonthOpt_Day");
                if (divAx){
                    divAx.style.display=((Case == "0")?"inline":"none");
                }
                divAx = window.parent.frames["body"].document.getElementById("spnMonthOpt_The");
                if (divAx){
                    divAx.style.display=((Case == "1")?"inline":"none");
                }
            }
            
            if (Type === "OY"){
                Case = window.parent.frames['body'].document.getElementById('YearlyOpt').value;
                divAx = window.parent.frames["body"].document.getElementById("spnYearlyOpt_On");
                if (divAx){
                    divAx.style.display=((Case == "0")?"inline":"none");
                }
                divAx = window.parent.frames["body"].document.getElementById("spnYearlyOpt_OnThe");
                if (divAx){
                    divAx.style.display=((Case == "1")?"inline":"none");
                }
            }
            //recurrangeopt
            if (Type === "OR"){
                Case = window.parent.frames['body'].document.getElementById('RangeRecurOpt').value;
                divAx = window.parent.frames["body"].document.getElementById("spnRangeRecurOpt_EA");
                if (divAx){
                    divAx.style.display=((Case == "1")?"inline":"none");
                }

                divAx = window.parent.frames["body"].document.getElementById("Row_RangeRecurrenceEnd");
                if (divAx){
                    divAx.style.display=((Case == "2")?"table-row":"none");
                }
            }
        }
	</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
?>
	<script language="JavaScript">
	 	setInitialValues();
        		
        <? if (getMDVersion() >= esvAgendaRedesign) {?>
			var rowAttribute = document.getElementById("Row_AttributeID");
			var rowFilterText = document.getElementById("Row_FilterText");
	 		rowAttribute.style.display = "none";
			rowFilterText.style.display = "none";
		<? } ?>
		
		/********/
		
		<? 
		$tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, null, true);
		foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { 
			foreach ($tmpCatalog as $ckey => $tmpCatMember) {
                if (!$this->isNewObject()) {
					if ($this->CatalogID != $catKey) {
						break;
					}
				}
				$attribName = "DSC_".$tmpCatMember; ?>
				if (document.getElementsByName('<?=$attribName?>')[0].addEventListener) {
					document.getElementsByName('<?=$attribName?>')[0].addEventListener("change", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);				
                } else  {
					document.getElementsByName('<?=$attribName?>')[0].attachEvent("onchange", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
				}
		<?
			}
		}
		?>
		
		/*********/
					 	
		objOkSelfButton = document.getElementById("BITAMSurveyAgendaScheduler_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyAgendaScheduler_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyAgendaScheduler_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
	</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{	
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			//$userCanEdit = false;
			return true;
		}
		else 
		{
			return false;	
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
  //Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
  //directamente de este
  function getJSonDefinition() {

  	global $appVersion;

	$arrDef = array();

	$arrDef['id'] = $this->AgendaSchedID;
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	$arrDef['name'] = str_replace("|", ", ", $this->FilterText);
	$arrDef['catalogID'] = $this->CatalogID;
	$arrDef['attributeID'] = $this->AttributeID;
	$arrDef['versionNum'] = $this->VersionNum;
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	$arrDef['filter'] = $this->FilterText;
	$arrDef['catalogFilter'] = array();
	$arrDef['userID'] = $this->UserID;
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
    $arrDef['captureStartTime'] = $this->getAgendaCaptureStartTime();
	$arrDef['lastDateID'] = $this->LastDateID;
	$arrDef['surveys'] = array_combine(array_values($this->SurveyIDs), $this->surveyIDsStatus);
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	$arrDef['RecurPatternType'] = 'RecurPatternType';
    $arrDef['RecurPatternOpt'] = $this->RecurPatternOpt;
    $arrDef['RecurPatternNum'] = $this->RecurPatternNum;
	//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
    $arrDef['RecurRangeOpt'] = $this->RecurRangeOpt;
    $arrDef['GenerateAgendasNextDayTime'] = $this->getAgendaTime();
	//@JAPR 2014-11-20: Modificado para considerar a todas las agendas incluso si sólo tienen forma de CheckIn
	if (count($this->SurveyIDs)) {
		$arrDef['surveys'] = array_combine(array_values($this->SurveyIDs), $this->surveyIDsStatus);
	}
	else {
		$arrDef['surveys'] = array();
	}

	if( getMDVersion() >= esveAgendas )
		return $arrDef;

	//@JAPR 2012-03-26: Agregado el filtro jerárquico de la agenda
	//Obtiene la colección de atributos del catálogo para usar al armar el query
	$intCatalogID = $this->CatalogID;
	$arrCatFilter = array();
	if ($intCatalogID > 0)
	{
		$arrCatalogMembers = null;
		$arrCatalogMembers = BITAMCatalogMemberCollection::NewInstance($this->Repository, $intCatalogID);
		if (is_null($arrCatalogMembers))
		{
			$arrCatalogMembers = array();
		}
		
		//Obtiene el valor de todos los miembros del catálogo hasta el atributo por el que se filtra la agenda, para generar el 
		//Array completo del filtro para ese catálogo
		if (!is_null($arrCatalogMembers) && count($arrCatalogMembers->Collection) > 0)
		{
			/***/
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
			if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
				//obtenemos los ids de los campos de ebavel
				$fieldsForAgenda = array();
				$countKeyOfAgenda = 0;
				foreach ($arrCatalogMembers as $aCatalogMember) {
					$eBavelFieldID = $aCatalogMember->eBavelFieldID;
					if ($aCatalogMember->KeyOfAgenda > 0) {
						if (!isset($fieldsForAgenda["KeyOfAgenda"])) {
							$fieldsForAgenda["KeyOfAgenda"] = array();
						}
						$fieldsForAgenda["KeyOfAgenda"][$aCatalogMember->ClaDescrip] = $eBavelFieldID;
						$countKeyOfAgenda++;
					}
					if (!isset($fieldsForAgenda["Fields"])) {
						$fieldsForAgenda["Fields"] = array();
					}
					$fieldsForAgenda["Fields"][$eBavelFieldID] = $eBavelFieldID;
				}
				
				//obtenemos los campo de ebavel (FFRMS_)
				$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n CatalogFilter #1 anBavelFieldIDsColl");
						PrintMultiArray($anBavelFieldIDsColl);
					}
				$ebavelFieldsForAgenda = array();
				$ebavelTypesForAgenda = array(); 	//array con los tipos para saber si poner quote o no
				$curKeyOfAgenda = 0;
				foreach ($fieldsForAgenda["KeyOfAgenda"] as $eBavelFieldID) {
					foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
						if (@$anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
							if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
								$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
								$ebavelTypesForAgenda["KeyOfAgenda"] = array();
							}
							$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
							$ebavelFieldTypeIsNumeric = 1;
							$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
							switch ($intQTypeID) {
								case qtpOpenNumeric:
								case qtpCalc:
									break;
								default:
									$ebavelFieldTypeIsNumeric = 0;
									break;
							}
							$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
							$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
							break;
						}
					}
				}
				
				//Llena el array de fields que se usará mas adelante para generar el catalogFilter
				foreach ($arrCatalogMembers as $aCatalogMember) {
					if ($countKeyOfAgenda == $curKeyOfAgenda) {
						break;
					}
					
					$eBavelFieldID = $aCatalogMember->eBavelFieldID;
					if ($eBavelFieldID <= 0) {
						continue;
					}
					
					foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
						if (@$anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
							$strSuffix = '';
							$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
							$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
							$intEBavelFieldID = (int) @$anAttributeInfo['id_fieldform']; 	/* id num de ebavel */
							if ($inteBavelFieldDataID > ebfdValue) {
								switch ($inteBavelFieldDataID) {
									case ebfdLatitude:
										$strEBavelFieldID += '_L';
										break;
									case ebfdLongitude:
										$strEBavelFieldID += '_A';
										break;
								}
							}
							
							if (isset($fieldsForAgenda["KeyOfAgenda"])) {
								if (in_array($intEBavelFieldID, $fieldsForAgenda["KeyOfAgenda"])) {
									$curKeyOfAgenda++;
								}
							}
							
							if (isset($fieldsForAgenda["Fields"])) {
								if (!isset($ebavelFieldsForAgenda["Fields"])) {
									$ebavelFieldsForAgenda["Fields"] = array();
								}
								$ebavelFieldsForAgenda["Fields"][$intEBavelFieldID] = $strEBavelFieldID;
							}
							break;
						}
					}
				}
				
				/*
				foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
					/
					if ($countKeyOfAgenda == $curKeyOfAgenda) {
						break;
					}
					/
					$strSuffix = '';
					$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
					$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			// FFRMS_ se usa en el where 
					$intEBavelFieldID = (int) @$anAttributeInfo['id_fieldform']; 	// id num de ebavel
					if ($inteBavelFieldDataID > ebfdValue) {
						switch ($inteBavelFieldDataID) {
							case ebfdLatitude:
								$strEBavelFieldID += '_L';
								break;
							case ebfdLongitude:
								$strEBavelFieldID += '_A';
								break;
						}
					}
					$ebavelFieldTypeIsNumeric = 1;
					$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
					switch ($intQTypeID) {
						case qtpOpenNumeric:
						case qtpCalc:
							break;
						default:
							$ebavelFieldTypeIsNumeric = 0;
							break;
					}
					/
					if (isset($fieldsForAgenda["KeyOfAgenda"])) {
						if (in_array($intEBavelFieldID, $fieldsForAgenda["KeyOfAgenda"])) {
							if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
								$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
								$ebavelTypesForAgenda["KeyOfAgenda"] = array();
							}
							$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
							$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
							$curKeyOfAgenda++;
						}
					}
					/
					if (isset($fieldsForAgenda["Fields"])) {
						if (!isset($ebavelFieldsForAgenda["Fields"])) {
							$ebavelFieldsForAgenda["Fields"] = array();
						}
						$ebavelFieldsForAgenda["Fields"][$intEBavelFieldID] = $strEBavelFieldID;
					}
				}
				*/
				
				//instanciamos el collection
				$strFilterArray = array();
				$curFilter = 0;
				if($this->FilterText) {
					//se genera el where
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n CatalogFilter #1 Values: {$this->FilterText}");
						PrintMultiArray($ebavelFieldsForAgenda["KeyOfAgenda"]);
					}
					$attributeValue = explode('|', $this->FilterText);
					if( isset($ebavelFieldsForAgenda["KeyOfAgenda"]) && is_array($ebavelFieldsForAgenda["KeyOfAgenda"]) )
					{
						foreach ($ebavelFieldsForAgenda["KeyOfAgenda"] as $ekey => $ebavelfield) {
							//checar si es numerico el campo o no para ponerle quotes en el where
							$ebavelValue = $attributeValue[$curFilter];
							$ebavelValue = ($ebavelTypesForAgenda["KeyOfAgenda"][$ekey]) ? $ebavelValue : $this->Repository->DataADOConnection->Quote($ebavelValue);
							array_push($strFilterArray, ($ebavelfield . " = " . $ebavelValue));
							$curFilter++;
						}
					}
				}
				
				if (count($strFilterArray)) {
					
					$strFilter = implode(" AND ",$strFilterArray);
					
					$bGetDistinctValues = true;
					$bGetRecordset = false;
					$bApplyFilters = false;
					$bIncludeSystemFields = false;
					
					//$aRS = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
					$ebavelCatalogValuesCollection = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
					
					//instanciamos con el recordset filtrado que obtuvimos
					//$ebavelCatalogValuesInstance = BITAMeBavelCatalogValue::NewInstanceFromRS($this->Repository, $aRS, $aCatalogID); /* esto ya no */
					
					if (!is_null($ebavelCatalogValuesCollection) && isset($ebavelCatalogValuesCollection->Collection[0])) {
						$ebavelCatalogValuesInstance = $ebavelCatalogValuesCollection->Collection[0];
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\n CatalogFilter #1");
							PrintMultiArray($ebavelCatalogValuesInstance);
						}
						
						$arrCatFilter = array();
						//foreach ($arrCatalogMembers as $aCatalogMember) {
						//var_dump($ebavelFieldsForAgenda["Fields"]);
						//exit();
						//$curKeyOfAgendaAgenda = 0;
						foreach ($ebavelFieldsForAgenda["Fields"] as $ebavelFieldID => $aCatalogMember) {
							//$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
							if (isset($fieldsForAgenda["Fields"][$eBavelFieldID]) ) {
								$fieldName = $aCatalogMember;
								if (isset($ebavelCatalogValuesInstance->$fieldName) && $ebavelFieldID != -1) {
									//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
									$arrCatFilter[] = (string) $ebavelCatalogValuesInstance->$fieldName;
									/*if ($aCatalogMember->MemberID == $this->AttributeID)
									{
										break;
									}*/
								}
							}
						}
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\n CatalogFilter #1 final");
							PrintMultiArray($arrCatFilter);
						}
						//echo(json_encode($arrCatFilter));
						//exit();						
					}
				}
			
			} else {
			
			/****/
			
			$tableName = $this->CatalogTable;
			$fieldKey = "RIDIM_".$this->CatalogDim."KEY";
			$sql = "SELECT ";
			
			//var_dump(1);
			//exit();
			if (getMDVersion() >= esvAgendaRedesign) {
				//$agendaAttributes = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, $this->CatalogID);
				//var_dump($this->FilterText);
				$agendaAttributeValues = explode("|", $this->FilterText);
				//var_dump($agendaAttributeValues);
				//exit();
				$agendaAttributesCount = count($agendaAttributeValues);
			}
			$agendaLastAttributeNumber = 0;
			foreach ($arrCatalogMembers as $aCatalogMember)
			{
				if ($aCatalogMember->KeyOfAgenda)
				{
					$agendaLastAttributeNumber = $aCatalogMember->MemberOrder;
				}
			}
			$agendaAttributeNumber = 0;
			
			$sqlWhere = "";
			foreach ($arrCatalogMembers as $aCatalogMember)
			{
				if (getMDVersion() >= esvAgendaRedesign) {
					$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
					//if ($aCatalogMember->MemberID == $this->AttributeID)
					if ($aCatalogMember->KeyOfAgenda) {						
						if ($agendaAttributeNumber+1 == $agendaAttributesCount) {
							if ($agendaAttributeNumber == 0) {
								$sqlWhere .= " WHERE ";
							} else {
								$sqlWhere .= " AND ";
							}
							$sqlWhere .= ($fieldName." = ".$this->Repository->DataADOConnection->Quote($agendaAttributeValues[$agendaAttributeNumber]));
							$sql .= $fieldName.' FROM '.$tableName. $sqlWhere . " ORDER BY ".$fieldKey;
							break;
						} else {
							$sql .= $fieldName.', ';
							if ($agendaAttributeNumber == 0) {
								$sqlWhere .= " WHERE ";
							} else {
								$sqlWhere .= " AND ";
							}
							//var_dump($agendaAttributeNumber);
							$sqlWhere .= ($fieldName." = ".$this->Repository->DataADOConnection->Quote($agendaAttributeValues[$agendaAttributeNumber]));
						}
						$agendaAttributeNumber++;
						//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
						//$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$this->Repository->DataADOConnection->Quote($this->FilterText)." ORDER BY ".$fieldKey;
						//break;
					} else {
						//Es un padre, lo agrega directamente al query
						$sql .= $fieldName.', ';
					}
				} else {
					$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
					if ($aCatalogMember->MemberID == $this->AttributeID)
					{
						//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
						$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$this->Repository->DataADOConnection->Quote($this->FilterText)." ORDER BY ".$fieldKey;
						break;
					}
					else 
					{
						//Es un padre, lo agrega directamente al query
						$sql .= $fieldName.', ';
					}
				}	
			}
			
			//var_dump($sql);
			//exit();
			
			$aRSCat = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRSCat)
			{
				if (!$aRSCat->EOF)
				{
					$arrCatFilter = array();
					foreach ($arrCatalogMembers as $aCatalogMember)
					{
						$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
						if (isset($aRSCat->fields[$fieldName])) {
							//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
							$arrCatFilter[] = (string) $aRSCat->fields[$fieldName];
							if ($aCatalogMember->MemberID == $this->AttributeID)
							{
								break;
							}
						}
					}
				}
			}
		}
		}
		$arrDef['catalogFilter'] = $arrCatFilter;
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$this->getAgendaRedesignDefinition($arrDef, $arrCatalogMembers);
		}
        //JAPR 2014-12-03: Agregado el array de valores del catálogo de la agenda
		if (getMDVersion() >= esvAgendaRedesign && $appVersion >= esvAgendaWithValues) {
		    $objCatalog = @BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		    if (!is_null($objCatalog)) {
			    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
			    $intMaxOrder = 0;
			    $strFilter = GetAgendaFilter($this->Repository, $this->AgendaSchedID);
			    $strFilter = BITAMCatalog::TranslateAttributesVarsToFieldNames($this->Repository, $this->CatalogID, $strFilter);
				$arrAttributesDef = BITAMCatalog::GetCatalogAttributes($this->Repository, $this->CatalogID, $intMaxOrder);
				$arrDef['attributeValues'] = @$objCatalog->getAttributeValues(array_values($arrAttributesDef), -1, $strFilter, null, null, false, true);
				if (is_null($arrDef['attributeValues'])) {
					unset($arrDef['attributeValues']);
				}
		    }
		}
	}
	//@JAPR
	
    return $arrDef;
  }
  
  function getAgendaRedesignDefinition (&$arrDef, $arrCatalogMembers) {
	$arrDef['status'] = $this->Status;
	$arrDef['checkInSurvey'] = $this->CheckinSurvey;
	$arrDef['checkInSurveyStatus'] = $this->CheckinSurvey;
	
	$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
	
		//obtenemos los ids de los campos de ebavel
		$fieldsForAgenda = array();
		foreach ($arrCatalogMembers as $aCatalogMember) {
			$eBavelFieldID = $aCatalogMember->eBavelFieldID;
			if ($aCatalogMember->KeyOfAgenda > 0) {
				if (!isset($fieldsForAgenda["KeyOfAgenda"])) {
					$fieldsForAgenda["KeyOfAgenda"] = array();
				}
				$fieldsForAgenda["KeyOfAgenda"][$aCatalogMember->ClaDescrip] = $eBavelFieldID;
			}
			if ($aCatalogMember->AgendaLatitude > 0) {
				$fieldsForAgenda["Latitude"] = $eBavelFieldID . "_L";
			} 
			if ($aCatalogMember->AgendaLongitude > 0) {
				$fieldsForAgenda["Longitude"] = $eBavelFieldID . "_A";
			} 
			if ($aCatalogMember->AgendaDisplay > 0) {
				if (!isset($fieldsForAgenda["Display"])) {
					$fieldsForAgenda["Display"] = array();
				}
				$fieldsForAgenda["Display"][$aCatalogMember->MemberName] = $eBavelFieldID;
			}
		}
	
		//obtenemos los campo de ebavel (FFRMS_)
		$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
		$ebavelFieldsForAgenda = array();
		$ebavelTypesForAgenda = array(); 	//array con los tipos para saber si poner quote o no

		foreach ($fieldsForAgenda["KeyOfAgenda"] as $eBavelFieldID) {
			foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
				if (@$anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
					if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
						$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
						$ebavelTypesForAgenda["KeyOfAgenda"] = array();
					}
					$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
					$ebavelFieldTypeIsNumeric = 1;
					$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
					switch ($intQTypeID) {
						case qtpOpenNumeric:
						case qtpCalc:
							break;
						default:
							$ebavelFieldTypeIsNumeric = 0;
							break;
					}
					$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
					$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
					break;
				}
			}
		}

		foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
			$strSuffix = '';
			$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
			$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
			$intEBavelFieldID = (string) @$anAttributeInfo['id_fieldform']; 	/* id num de ebavel */
			if ($inteBavelFieldDataID > ebfdValue) {
				switch ($inteBavelFieldDataID) {
					case ebfdLatitude:
						$strEBavelFieldID .= '_L';
						$intEBavelFieldID .= "_L";
						break;
					case ebfdLongitude:
						$strEBavelFieldID .= '_A';
						$intEBavelFieldID .= "_A";
						break;
				}
			}
			$ebavelFieldTypeIsNumeric = 1;
			$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
			switch ($intQTypeID) {
				case qtpOpenNumeric:
				case qtpCalc:
					break;
				default:
					$ebavelFieldTypeIsNumeric = 0;
					break;
			}
			/*
			if (isset($fieldsForAgenda["KeyOfAgenda"])) {
				if (in_array($intEBavelFieldID, $fieldsForAgenda["KeyOfAgenda"])) {
					if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
						$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
						$ebavelTypesForAgenda["KeyOfAgenda"] = array();
					}
					$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
					$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
				}
			}
			*/
			if (isset($fieldsForAgenda["Latitude"]) && ($fieldsForAgenda["Latitude"] == $intEBavelFieldID)) {
				$ebavelFieldsForAgenda["Latitude"] = $strEBavelFieldID;
			}
			if (isset($fieldsForAgenda["Longitude"]) && ($fieldsForAgenda["Longitude"] == $intEBavelFieldID)) {
				$ebavelFieldsForAgenda["Longitude"] = $strEBavelFieldID;
			}
			if (isset($fieldsForAgenda["Display"])) {
				foreach ($fieldsForAgenda["Display"] as $dkey => $fieldForDisplay) {
					if ($fieldForDisplay == $intEBavelFieldID) {
						if (!isset($ebavelFieldsForAgenda["Display"])) {
							$ebavelFieldsForAgenda["Display"] = array();
						}
						$ebavelFieldsForAgenda["Display"][$dkey] = $strEBavelFieldID;
						break;
					}
				}
			}
		}
		
		//instanciamos el collection
		$strFilterArray = array();
		$curFilter = 0;
		if($this->FilterText) {
			//se genera el where
			$attributeValue = explode('|', $this->FilterText);
			if( isset($ebavelFieldsForAgenda["KeyOfAgenda"]) && is_array($ebavelFieldsForAgenda["KeyOfAgenda"]) )
			{
				foreach ($ebavelFieldsForAgenda["KeyOfAgenda"] as $ekey => $ebavelfield) {
					//checar si es numerico el campo o no para ponerle quotes en el where
					$ebavelValue = $attributeValue[$curFilter];
					$ebavelValue = ($ebavelTypesForAgenda["KeyOfAgenda"][$ekey]) ? $ebavelValue : $this->Repository->DataADOConnection->Quote($ebavelValue);
					array_push($strFilterArray, ($ebavelfield . " = " . $ebavelValue));
					$curFilter++;
				}
			}
		}
		
		if (count($strFilterArray)) {
			
			$strFilter = implode(" AND ",$strFilterArray);
			
			$bGetDistinctValues = true;
			$bGetRecordset = false;
			$bApplyFilters = false;
			$bIncludeSystemFields = false;
			
			//$aRS = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
			$ebavelCatalogValuesCollection = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
			
			//instanciamos con el recordset filtrado que obtuvimos
			//$ebavelCatalogValuesInstance = BITAMeBavelCatalogValue::NewInstanceFromRS($this->Repository, $aRS, $aCatalogID); /* esto ya no */
			
			//var_dump($ebavelCatalogValuesCollection->Collection);
			//exit();
			
			if (!is_null($ebavelCatalogValuesCollection) && isset($ebavelCatalogValuesCollection->Collection[0])) {
			
				$ebavelCatalogValuesInstance = $ebavelCatalogValuesCollection->Collection[0];
			
				//obtenemos latitud
				$agendaLatitude = '';
				if (isset($ebavelFieldsForAgenda["Latitude"])) {
					$latitudeEBavelField = $ebavelFieldsForAgenda["Latitude"];
					if (property_exists($ebavelCatalogValuesInstance, $latitudeEBavelField)) {
						$agendaLatitude = $ebavelCatalogValuesInstance->$latitudeEBavelField;
					}
				}
				
				//obtenemos longitud
				$agendaLongitude = '';
				if (isset($ebavelFieldsForAgenda["Longitude"])) {
					$longitudeEBavelField = $ebavelFieldsForAgenda["Longitude"];
					if (property_exists($ebavelCatalogValuesInstance, $longitudeEBavelField)) {
						$agendaLongitude = $ebavelCatalogValuesInstance->$longitudeEBavelField;
					}
				}
				
				//obtenemos displays
				$agendaDisplays = array();
				if (isset($ebavelFieldsForAgenda["Display"])) {
					foreach ($ebavelFieldsForAgenda["Display"] as $dkey => $agendaDisplayField) {
						$displaysEBavelField = $agendaDisplayField;
						$agendaDisplays[$dkey] = $ebavelCatalogValuesInstance->$displaysEBavelField;
					}		
				}
				
				//seteamos la definition
				$arrDef['latitude'] = $agendaLatitude;
				$arrDef['longitude'] = $agendaLongitude;
				$arrDef['displays'] = $agendaDisplays;
				
			}
		}
	
	} else {
	
	$fieldsForAgenda = array();
	foreach ($arrCatalogMembers as $aCatalogMember) {
		$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
		if ($aCatalogMember->AgendaLatitude > 0) {
			$fieldsForAgenda["Latitude"] = $fieldName;
		} 
		if ($aCatalogMember->AgendaLongitude > 0) {
			$fieldsForAgenda["Longitude"] = $fieldName;
		} 
		if ($aCatalogMember->AgendaDisplay > 0) {
			if (!isset($fieldsForAgenda["Display"])) {
				$fieldsForAgenda["Display"] = array();
			}
			$fieldsForAgenda["Display"][$aCatalogMember->MemberName] = $fieldName;
		} 
		//if ($aCatalogMember->MemberID == $this->AttributeID) {
		//	$fieldsForAgenda["Attribute"] = $fieldName;
		//}
	}
	$fieldAttributeName = "";
	$sqlFields = "";
	
	//var_dump();
	//exit();
	if($this->FilterText) {
		$arrKeyOfAgendaFields = array();
		foreach ($arrCatalogMembers as $aCatalogMember) {
			if ($aCatalogMember->KeyOfAgenda) {
				$fieldAttributeName = "DSC_".$aCatalogMember->ClaDescrip;
				array_push($arrKeyOfAgendaFields, $fieldAttributeName);
			}
		}
		if (count($arrKeyOfAgendaFields)) {
			$sqlFields .= (implode(', ', $arrKeyOfAgendaFields));
		}
	}
	/*if (isset($fieldsForAgenda["Attribute"])) {
		$fieldAttributeName = $fieldsForAgenda["Attribute"];
		$sqlFields .= $fieldAttributeName;
	} else {
		return;
	}*/
	if (isset($fieldsForAgenda["Latitude"]) && isset($fieldsForAgenda["Longitude"])) {
		$sqlFields .= (", " . $fieldsForAgenda["Latitude"] . ", " . $fieldsForAgenda["Longitude"]);
	}
	if (isset($fieldsForAgenda["Display"])) {
		foreach ($fieldsForAgenda["Display"] as $dkey => $aDisplay) {
			$sqlFields .= (", " . $fieldsForAgenda["Display"][$dkey]);
		}
	}
	$where = " ";
	if($this->FilterText) {
		$whereArray = array();
		$attributeValue = explode('|', $this->FilterText);
		$countAttributeValue = 0;
		foreach ($arrCatalogMembers as $aCatalogMember) {
			if ($aCatalogMember->KeyOfAgenda) {
                if (isset($attributeValue[$countAttributeValue])) {
                    if ($countAttributeValue == 0) {
                        $where .= "WHERE ";
                    }
                    array_push($whereArray, ("DSC_".$aCatalogMember->ClaDescrip . " = " . $this->Repository->DataADOConnection->Quote($attributeValue[$countAttributeValue])));
                    $countAttributeValue++;
                }
            }
		}
		$where .= implode($whereArray, ' AND ');
	} else {
		return;
	}
	$tableName = $this->CatalogTable;
	$fieldKey = "RIDIM_".$this->CatalogDim."KEY";
	$sql = "SELECT " . $sqlFields." FROM ".$tableName.$where." ORDER BY ".$fieldKey;
	$aRSCat = $this->Repository->DataADOConnection->Execute($sql);
	
	//var_dump($sql);
	//exit();
	
	$agendaLatitude = 0;
	$agendaLongitude = 0;
	$agendaDisplays = array();
	if ($aRSCat) {
		if (!$aRSCat->EOF) {
			if (isset($fieldsForAgenda["Latitude"]) && isset($fieldsForAgenda["Longitude"])) {
				if (is_numeric($aRSCat->fields[$fieldsForAgenda["Latitude"]]) && is_numeric($aRSCat->fields[$fieldsForAgenda["Longitude"]])) {
					$agendaLatitude = $aRSCat->fields[$fieldsForAgenda["Latitude"]];
					$agendaLongitude = $aRSCat->fields[$fieldsForAgenda["Longitude"]];
				}
			}
			if (isset($fieldsForAgenda["Display"])) {
				foreach ($fieldsForAgenda["Display"] as $dkey => $aDisplay) {
					$anAgendaDisplay = @$aRSCat->fields[$fieldsForAgenda["Display"][$dkey]];
					$agendaDisplays[$dkey] = $anAgendaDisplay;
				}
			}
		}
	}
	$arrDef['latitude'] = $agendaLatitude;
	$arrDef['longitude'] = $agendaLongitude;
	$arrDef['displays'] = $agendaDisplays;
	}
	$arrDef['surveysRadio'] = $this->SurveysRadioForAgenda;
  }
    function insideEdit()
    {
?>
            window.parent.frames['body'].document.getElementsByName('RecurPatternType')[0].onchange = function(){ShowHideRecurrenceFields('RP');};      
            var obj = window.parent.frames['body'].document.getElementById('tDaysOfMonth')
            if(obj){
                obj.className = '';      
            }
            if(document.getElementById("WDaysSu")) window.parent.frames["body"].document.getElementById("WDaysSu").className = 'chkDaysCls';
            if(document.getElementById("WDaysM")) window.parent.frames["body"].document.getElementById("WDaysM").className = 'chkDaysCls';
            if(document.getElementById("WDaysTu")) window.parent.frames["body"].document.getElementById("WDaysTu").className = 'chkDaysCls';
            if(document.getElementById("WDaysW")) window.parent.frames["body"].document.getElementById("WDaysW").className = 'chkDaysCls';
            if(document.getElementById("WDaysTh")) window.parent.frames["body"].document.getElementById("WDaysTh").className = 'chkDaysCls';
            if(document.getElementById("WDaysF")) window.parent.frames["body"].document.getElementById("WDaysF").className = 'chkDaysCls';
            if(document.getElementById("WDaysSa")) window.parent.frames["body"].document.getElementById("WDaysSa").className = 'chkDaysCls';
<?       
    }
    function insideCancel()
    {
?>
                window.parent.frames['body'].document.getElementsByName('RecurPatternType')[0].onchange = null;
                
<?
    }
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
    function generateOnCancelFormCode($aUser)
    {
?>      
        ShowHideRecurrenceFields('RP');
        setInitialValues();
<?        
    }
    
     //validar si la fecha de la agenda aplica
    function isValidDate($dFechaValidar = null){
        $response = false;
        
        //si es null sale
        if (is_null($dFechaValidar)){
            return false;
        }else{
            $dFechaValidar = new DateTime($dFechaValidar);
        }
                         
        //rango valido
        $bRangoValido = false;
        
        //checamos rango recurrencia
        //RecurRangeOpt: [0]-Fecha inicio, [1]-opcion seleccionada 0:sin fin, 1:por ocurrencias, 2:por fecha fin, [2]- toma el valor de las ocurrencias o fecha fin
        $sRangoOpc = (string) $this->RecurRangeOpt;
        $aRangoOpc = explode("|", $sRangoOpc);
        
        //RecurRangeStart
		//@JAPR 2015-08-28: Validado que si no hay una fecha, tome la fecha de creación del Scheduler, y si esa no existe entonces fallará el proceso
		if (trim($aRangoOpc[0]) == '') {
			if ($this->CreationDateID) {
				$aRangoOpc[0] = $this->CreationDateID;
			}
		}
		
		//Validado que si no vienen los valores de estos índices, los genere como "0"
		$aRangoOpc[1] = (string) ((int) @$aRangoOpc[1]);
		//@JAPR 2016-02-12: Corregido un bug, se estaba convirtiendo a número la fecha en caso de que el índice 1 hubiera sido para fecha final, por lo que devolvía como timeStampt
		//una fecha y hora posterior a la actual y siempre consideraba que el rango se extendía infinitamente, así que generaba las agendas sin contemplar la fecha final configurada (#)
		$aRangoOpc[2] = (string) (@$aRangoOpc[2]);
		//@JAPR
		
        $dFechaIni = new DateTime($aRangoOpc[0]);
        $nUFechaValidar = strtotime($dFechaValidar->format('Y-m-d'));
        
        //si la fechavalidar es mayor o igual a fecha incio rango
        if ($nUFechaValidar >= strtotime($dFechaIni->format('Y-m-d'))){
            //opciones del rango
            switch($aRangoOpc[1]){
                case "0"://sin fecha final
                    $bRangoValido = true;
                    break;
                case "1"://por ocurrencias
                    $nOcurrenciasLim = (int) $aRangoOpc[2];
                    $nOcurrencias =  (is_null($this->Ocurrences)) ? 0 : $this->Ocurrences;
                    if ($nOcurrencias < $nOcurrenciasLim){
                        $bRangoValido = true;    
                    }
                    break;
                case "2"://por fecha final
                    $nUFechaFin = strtotime($aRangoOpc[2]);

                    if ($nUFechaValidar <= $nUFechaFin){
                        $bRangoValido = true;
                    }
            }
        }
        
        //rango valido
        if ($bRangoValido){
            $sTipo = (int) $this->RecurPatternType;
            $sOpc = (string) $this->RecurPatternOpt;
            $nNum = (int) $this->RecurPatternNum;
            
            //JCEM ahora se utiliza la fecha de inicio del rango que es cuando entra en vigor
            //$sFecCreacion = (string) $this->CreationDateID;
            //$dFecCreacion = new DateTime($sFecCreacion);
            $dFecCreacion = $dFechaIni;
            
            switch ($sTipo)
            {
                case frecDay:		//diario
                    //RecurPatternOpt : DailyOpt
                    //RecurPatternNum : DNumDays
                    //diff en dias
                    $nDifDias	= (strtotime($dFecCreacion->format('Y-m-d'))-strtotime($dFechaValidar->format('Y-m-d')))/86400;
                    $nDifDias 	= abs($nDifDias); 
                    $nDifDias = floor($nDifDias);	
                    
                    //cada x dias
                    if ($this->RecurPatternOpt == 0){
                    //si mod =0 es valida
                        $response = ($nDifDias == 0 || fmod($nDifDias, $nNum) == 0); 
                    }else{ //todos los dias
                        $response = true;
                    }
                    break;
                case frecWeek:		//semanal
                    //numero de semana del anio del dia de la creacion(no existe funcionalidad similar en utils.inc.php)
                    $nSemanaCreacion = date('W',  mktime(0,0,0,$dFecCreacion->format('m'),$dFecCreacion->format('d'),$dFecCreacion->format('Y')));  
                    //numero de semana del ano de dia de hoy
                    $nSemanaValidar = date('W',  mktime(0,0,0,$dFechaValidar->format('m'),$dFechaValidar->format('d'),$dFechaValidar->format('Y')));  
                    
                    $nAnioCreacion = intval($dFecCreacion->format('Y'));
                    $nAnioAct = intval($dFechaValidar->format('Y'));
                    
                    //diff de anios
                    $nDiffAnios = $nAnioAct - $nAnioCreacion;
                    //si es en los proximos anios le aumento el numero de semanas por anio 
                    if ($nDiffAnios > 0){
                        $nSemanaValidar = $nSemanaValidar + ($nDiffAnios * 52);
                    }

                    //semanas que han pasado
                    $nSemanasDiff = $nSemanaValidar - $nSemanaCreacion;

                    //si es esta semana o concuerda con el multiplo de semanas dado
                    if ($nSemanasDiff == 0 || fmod($nSemanasDiff, $nNum) == 0){
                        //los dias seleccionados
                        $aDiasSel = explode("|", $sOpc);
                        $cnt = count($aDiasSel);

                        if ($cnt > 0 && in_array(wday($dFechaValidar->format('Y-m-d')), $aDiasSel)){
                            $response = true;
                        }
                    }
                    break;
                //@AAL 14/04/2015: Opción agregada para crear agendas por semanas del mes.
                case frecWeekOfMonth:		//0 => The
                	$PatternOptions = explode("/", $sOpc);
                	//0 => nOrdinal, 1 => nDia, 2 => mensual (este ultmimo caso en particular no aplica)
                	$PatternOptions = explode("|", $PatternOptions[1]);

                    //0 => first, 1 => second, 2 => third, 3 => fourth, 4 => last
                    $nOrdinal = intval($PatternOptions[0]);
                    //3 => 'sunday', 4 => 'monday', 5 => 'tuesday', 6 => 'wednesday', 7 => 'thursday', 8 => 'friday', 9 => 'saturday';
                    $nDia = intval($PatternOptions[1]) - 3; //Restamos tres para a justar los días a 0, 1, 2, ..., 6

					//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
					//@JAPR 2016-02-11: Corregido un bug, el cálculo de estos valores hacía cosas innecesarias, no necesitamos generar el array de Sundays de esta manera, si iniciamos
					//el cálculo el día previo a este mes, obtendremos todos los días del tipo que queramos sin saltar ninguno, porque el avance con el objeto DateTime no considera al
					//mismo día en que estás si es del tipo que pediste, así que quitamos esa variable pidiendo desde el día previo al mes, los primeros 5 Sundays, si los hay entonces
					//los regresará correctamente (ej. 2016-May: 1, 8, 15, 22, 29) pero si no los hay, el último corresponderá al siguiente mes al pedido, y ahí ya podemos hacer el ajuste 
					//para dejarlo igual que el cuarto (ej. 2016-Feb: 7, 14, 21, 28, 2016-Mar: 6)
					/*
                    //Tomando como referencia la Fecha a validar, desglosamos en Año, Mes y Dia
					$D = $dFechaValidar->format('d');

                    //Verificamos Si pidieron la última semana (4ta o 5ta) y tambiém si los dias están entre 1 y 7. Si es así entonces restamos un mes
					if(($nOrdinal == 3 || $nOrdinal == 4) && $D >= 1 && $D < 7){
						$Y = $dFechaValidar->format('Y');
						$M = $dFechaValidar->format('m');
						$Date = date('Y-m-d', strtotime ('-1 month' , strtotime ($Y . "-" . $M . "-01")));
					}
					else
						$Date = date('Y-m-01', strtotime($dFechaValidar->format('Y-m-d')));
					*/
					//@JAPR
					
					//Procedemos a buscar los domingos del mes
					$Sundays = array(); $i = 0;
					//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
					//@JAPR 2016-02-11: Corregido un bug, el cálculo de estos valores hacía cosas innecesarias, no necesitamos generar el array de Sundays de esta manera, si iniciamos
					//el cálculo el día previo a este mes, obtendremos todos los días del tipo que queramos sin saltar ninguno, porque el avance con el objeto DateTime no considera al
					//mismo día en que estás si es del tipo que pediste, así que quitamos esa variable pidiendo desde el día previo al mes, los primeros 5 Sundays, si los hay entonces
					//los regresará correctamente (ej. 2016-May: 1, 8, 15, 22, 29) pero si no los hay, el último corresponderá al siguiente mes al pedido, y ahí ya podemos hacer el ajuste 
					//para dejarlo igual que el cuarto (ej. 2016-Feb: 7, 14, 21, 28, 2016-Mar: 6)
					//Iniciar las semanas en domingo o lunes
					$strWeekStart = "sunday";
					if ($this->FirstDayOfWeek == asfwdMonday) {
						$strWeekStart = "monday";
					}
					/*
					$Sdy  = strtotime("sunday {$Date}");
				    while($i < 4) { //deben de haber 4 domingos en el mes
				        $Sundays[$i++] = date("d", $Sdy);
				        $Sdy = strtotime('next sunday', $Sdy);
				    }
				    //Ahora hay que verificar si el primer domingo es 8, si verdadero entonces el primer domingo cayo el 01, 
				    //por lo tanto lo agregagos al principio del arreglo
				    if($Sundays[0] == 8)
				    	array_unshift($Sundays, 1);
				    else //Verificamos si existe un 5to domingo. Si no existe asignamos al 5to domingo el 4to y lo tomamos como la última semana
                    	$Sundays[$i] = (($Sundays[0] + 28) <= 31) ? $Sundays[0] + 28 : $Sundays[3];
					*/
					//Simplemente obtendrá los primeros 5 días iniciales de semana mientras sean del mes que se está validando
					$dteFirstDayMonth = new DateTime($dFechaValidar->format('Y-m-01'));
					$Date = $dteFirstDayMonth->format('Y-m-d');
					$dteFirstDayMonth->modify("-1 day");
					$dteFirstDayMonth->modify("first {$strWeekStart}");
					$Sundays[$i++] = $dteFirstDayMonth->format("d");
					$dteFirstDayMonth->modify("next {$strWeekStart}");
					$Sundays[$i++] = $dteFirstDayMonth->format("d");
					$dteFirstDayMonth->modify("next {$strWeekStart}");
					$Sundays[$i++] = $dteFirstDayMonth->format("d");
					$dteFirstDayMonth->modify("next {$strWeekStart}");
					$Sundays[$i++] = $dteFirstDayMonth->format("d");
					$dteFirstDayMonth->modify("next {$strWeekStart}");
					$Sundays[$i] = $dteFirstDayMonth->format("d");
					//Si el último día es < 20, es seguro que brincó de mes, porque la última semana tendría por fuerza cualquier día > 20
					if ((int) @$Sundays[$i] < 20) {
						$Sundays[$i] = $Sundays[$i-1];
					}
					
					//Si se pidió iniciar la semana en lunes, entonces $nDia no puede usarse como se recibió porque domingo no es igual que sumar 0, sino que eso corresponde a lunes,
					//ahora domingo sería igual a sumar 6 mientras que el resto deben disminuir
					if ($this->FirstDayOfWeek == asfwdMonday) {
						if ($nDia == 0) {
							$nDia = 6;
						}
						else {
							$nDia--;
						}
					}
					//@JAPR
					
                   	//Nomenclatura de las semanas 0 => 'first', 1 => 'second', 2 => 'third', 3 =>'fourth', 4 => 'last';
					$Ordinales = array(0 => $Sundays[0], 1 => $Sundays[1], 2 => $Sundays[2], 3 => $Sundays[3], 4 => $Sundays[4]);
                    
                    //Establecemos la fecha inicial considerando el domingo a partir del cual queremos obtener el dia de la semana para generar la agenda
                    $DateInit = date('Y-m-' . $Ordinales[$nOrdinal], strtotime($Date));
					$ThisDate = new DateTime($DateInit);
					$Str = "+{$nDia} day";
					$ThisDate->modify($Str);
					
					//Finalmente comparamos si ambas fechas son iguales
                    if ($dFechaValidar == $ThisDate)
                        $response = true;
					break;
                //@AAL
                case frecMonth:		//mensual
                    //RecurPatternOpt: [0]: tipo / [1]:opciones
                    $aMensualOpc = explode("/", $sOpc);
                    
                    //numero de mes del anio del dia de la creacion
                    $nMesCreacion = mon($dFecCreacion->format('Y-m-d'));
                    //numero de mes del anio de dia de hoy
                    $nMesValidar = mon($dFechaValidar->format('Y-m-d'));
                    
                    $nAnioCreacion = intval($dFecCreacion->format('Y'));
                    $nAnioAct = intval($dFechaValidar->format('Y'));
                    
                    //diff de años
                    $nDiffAnios = $nAnioAct - $nAnioCreacion;
                    
                    //si es en los proximos anios le aumento el numero de meses por anio 
                    if ($nDiffAnios > 0){
                        $nMesValidar = $nMesValidar +($nDiffAnios * 12);
                    }

                    //meses que han pasado
                    $nMesesDiff = $nMesValidar - $nMesCreacion;
                                                
                    //si es este mes o concuerda con el multiplo de meses dado
                    if ($nMesesDiff == 0 || fmod($nMesesDiff, $nNum) == 0 ){
                        switch($aMensualOpc[0]){
                            case "0"://dia- dias seleccionados
                                    //dia del mes
                                    $nDiaMes = $dFechaValidar->format('d'); 
                                    $aDiasMesSel = explode("|", $aMensualOpc[1]);
                                    $cnt = count($aDiasMesSel);
                                    
                                    if ($cnt > 0 && in_array($nDiaMes, $aDiasMesSel)){
                                        $response = true;
                                    }else{
                                        $nMaxDiasMes = numDaysOfMonth($nAnioAct, $nMesValidar);
                                        if ($nDiaMes == $nMaxDiasMes && ((($nMesValidar == 2) ? in_array('29', $aDiasMesSel): false) || in_array('30', $aDiasMesSel) || in_array('31', $aDiasMesSel)) ){
                                            $response = true ;
                                        }
                                    }
                                break;
                            case "1"://el dia
                                //aOpc- [0]: ordinal, [1] dia
                                $aOpc = explode("|", $aMensualOpc[1]);
                                //0first,1second,2third,3fourth,4last
                                $nOrdinal = intval($aOpc[0]);
                                
                                //0day, 1weekday, 2weekend day,3'sunday',4'monday',5'tuesday', 6'wednesday', 7'thursday', 8'friday', 9'saturday');
                                $nDia = intval($aOpc[1]);
                                
                                //cambiamos weekday a dias normales
                                switch($nDia){
                                    case 1://week day
                                        switch($nOrdinal){
                                            case 0://first
                                                $nDia = 4;//a lunes
                                                break;
                                            case 1://second
                                                $nDia = 5;//a martes
                                                break;
                                            case 2://third
                                                $nDia = 6;//mier
                                                break;
                                            case 3://fourth
                                                $nDia = 7;//jue
                                                break;
                                            case 4://last 
                                                $nDia = 8;//vie
                                                break;
                                        }
                                        break;
                                    case 2://weekend day
                                        $nDia = 9;//a sabado
                                        break;
                                }
                                
                                /* @AAL 15/04/2015 Issue #3UKIT8: Hay casos en que el dia programado para gener la agenda corresponden al dia 01
                                del mes y la función date('Y-m-01') no considera el dia 01 si no los dias depúes del 01. Entonces se seteo a 00*/
                                $D = $dFechaValidar->format('d');
                                if($D == "01")
                                	$dFechaM = date('Y-m-00', strtotime($dFechaValidar->format('Y-m-d')));
                                else
                                	$dFechaM = date('Y-m-01', strtotime($dFechaValidar->format('Y-m-d')));
                                //@AAL
                                
                                //2015.01.07 jcem corregido bug esta variable no era datetime class
                                $dFechaM = new DateTime($dFechaM);
                                
                                $aOrdinales = array('first','second','third','fourth', 'last');
                                $aDias = array('sunday','monday','tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
                                
                                //si dia no es 0 que seria opcion day cambiamos a dias normales -3 para quitar la opcion de day, week day y weekend day
                                if ($nDia == 0){
                                    $String = $aOrdinales[$nOrdinal].' day of this month';
                                }else{
                                    $nDia = $nDia - 3 ;
                                    
                                    $String = $aOrdinales[$nOrdinal].' '.$aDias[$nDia].' of this month'; 
                                }
                                
                                //la fecha supuesta segun la opcion 
                                $dFechaM->modify($String);
                                
                                if ($dFechaValidar == $dFechaM){
                                    $response = true;
                                }
                            break;
                        }
                    }
                    break;
                case frecYear:		//anual
                    //RecurPatternOpt: [0]: tipo / [1]:opciones
                    $aAnualOpc = explode("/", $sOpc);
                    
                    $nAnioCreacion = intval($dFecCreacion->format('Y'));
                    $nAnioAct = intval($dFechaValidar->format('Y'));
                    $nAniosDif = $nAnioAct - $nAnioCreacion;
                            
                    //si es este anio o concuerda con el multiplo de anios dado
                    if ($nAniosDif == 0 || fmod($nAniosDif, $nNum) == 0 ){
                        $aOpc = explode("|", $aAnualOpc[1]);
                        switch ($aAnualOpc[0]){
                            case "0"://on
                                $nDiaMes = intval($aOpc[0]);
                                //2015.01.09 jcem corregido un bug le mes era base 0 y numDaysOfMonth lo calcula en base 1 ademas $dFecCalculada se calculaba mal
                                $nMes = intval($aOpc[1]) + 1;
                                
                                //validamos que el dia este dentro del rango del mes
                                $nMaxDiasMes = numDaysOfMonth($nAnioAct, $nMes);
                                if ($nDiaMes > 0 && $nDiaMes<= $nMaxDiasMes ){
                                    $dFecCalculada = new DateTime(strval($nAnioAct).'-'.strval($nMes).'-'.strval($nDiaMes));
                                    if ($dFechaValidar == $dFecCalculada){
                                        $response = true;
                                    }
                                }
                                break;
                            case "1": //on the
                                //aOpc- [0]ordinal, [1]dia, [2]mes 
                                $aOpc = explode("|", $aAnualOpc[1]);
                                //0first,1second,2third,3fourth,4last
                                $nOrdinal = intval($aOpc[0]);
                                //0day, 1weekday, 2weekend day,3'sunday',4'monday',5'tuesday', 6'wednesday', 7'thursday', 8'friday', 9'saturday');
                                $nDia = intval($aOpc[1]);

                                $nMes = $aOpc[2];
                                
                                //cambiamos weekday a dias normales
                                switch($nDia){
                                    case 1://week day
                                        switch($nOrdinal){
                                            case 0://first
                                                $nDia = 4;//a lunes
                                                break;
                                            case 1://second
                                                $nDia = 5;//a martes
                                                break;
                                            case 2://third
                                                $nDia = 6;//mier
                                                break;
                                            case 3://fourth
                                                $nDia = 7;//jue
                                                break;
                                            case 4://last 
                                                $nDia = 8;//vie
                                                break;
                                        }
                                        break;
                                    case 2://weekend day
                                        $nDia = 9;//a sabado
                                        break;
                                }
                                
                                /* @AAL 15/04/2015 Issue #3UKIT8: Hay casos en que el dia programado para gener la agenda corresponden al dia 01
                                del mes y la función date('Y-m-01') no considera el dia 01 si no los dias depúes del 01. Entonces se setea a 00*/
                                $D = $dFechaValidar->format('d');
                                if($D == "01")
                                	$dFechaM = date('Y-m-00', strtotime($dFechaValidar->format('Y-m-d')));
                                else
                                	$dFechaM = date('Y-m-01', strtotime($dFechaValidar->format('Y-m-d')));
                                //@AAL
                                
                                 //2015.01.07 jcem corregido bug esta variable no era datetime class
                                $dFechaM = new DateTime($dFechaM);
                                
                                $aOrdinales = array('first','second','third','fourth', 'last');
                                $aDias = array('sunday','monday','tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
                                $aMeses = array('january', 'february', 'march', 'april', 'may', 'june','july', 'august', 'september', 'october','november','december');
                                
                                //si dia no es 0 que seria opcion day cambiamos a dias normales -3 para quitar la opcion de day, week day y weekend day
                                if ($nDia == 0){
                                    //ej. First day of August 2014
                                    $String = $aOrdinales[$nOrdinal].' day of '.$aMeses[$nMes].' '.strval($nAnioAct);
                                }else{
                                    $nDia = $nDia - 3 ;
                                    //ej. First monday of August 2014
                                    $String = $aOrdinales[$nOrdinal].' '.$aDias[$nDia].' of '.$aMeses[$nMes].' '.strval($nAnioAct); 
                                }
                                
                                //la fecha supuesta segun la opcion 
                                $dFechaM->modify($String);
                                
                                if ($dFechaValidar == $dFechaM){
                                    $response = true;
                                }
                                break;
                        }
                    }
                    break;
            }
        }
        return $response;
    }//fin fnc
}

class BITAMSurveyAgendaSchedulerCollection extends BITAMCollection
{
	public $UserID;
	public $CatalogID;
	public $StartDate;
	public $EndDate;
	public $ipp;				//Items per page
	public $Pages;
	
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->CatalogID = 0;
		$this->ipp = 100;
		$this->Pages = null;
	}
	
	static function NewInstance($aRepository, $anArrayOfAgendaIDs = null, $anArrayOdUserID = null, $anEndDate = null, $aTime = null, $aStartDate = null, $aCatalogID = null, $ipp = 100, $bUsePaginator = false, $attributes = null, $searchString = null, $statusID = null, $aCheckPointID = null)
	{
		$strCalledClass = static::class;

		$anInstance = new $strCalledClass($aRepository);
		//@JAPR 2015-10-26: Corregido un bug, no estaba considerando la posibilidad de que este parámetro fuera un array
		$anInstance->UserID = ((is_null($anArrayOdUserID) || is_array($anArrayOdUserID))?-2:$anArrayOdUserID);
		//@JAPR
		$anInstance->CatalogID = $aCatalogID;
		$anInstance->CheckPointID = $aCheckPointID;
		$anInstance->ipp = $ipp;
		$where = "";
		$strAnd = '';
		
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$agStartDate = $aStartDate;
		$agEndDate = $anEndDate;
		$agUserID = $anArrayOdUserID;
		$agIpp = $ipp;
		$agCatalogID = $aCatalogID;
		
		if (!is_null($anArrayOfAgendaIDs))
		{
			if (!is_array($anArrayOfAgendaIDs))
			{
				$anArrayOfAgendaIDs = array($anArrayOfAgendaIDs);
			}
			
			switch (count($anArrayOfAgendaIDs))
			{
				case 0:
					break;
				case 1:
					$where = " t1.AgendaSchedID = ".((int) $anArrayOfAgendaIDs[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($anArrayOfAgendaIDs as $anAgendaID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anAgendaID; 
					}
					if ($where != "")
					{
						$where = " t1.AgendaSchedID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($anArrayOdUserID))
		{
			if (!is_array($anArrayOdUserID))
			{
				//@JAPR 2015-10-26: Corregido un bug, no estaba considerando la posibilidad de que este parámetro fuera un array
				$anArrayOdUserID = (int) @$anArrayOdUserID;
				if ($anArrayOdUserID <= 0) {
					//En este caso no es un ID de usuario válido, así que se genera vacío para que no filtre la colección
					$anArrayOdUserID = array();
				}
				else {
					$anArrayOdUserID = array($anArrayOdUserID);
				}
				//@JAPR
			}
			
			switch (count($anArrayOdUserID))
			{
				case 0:
					$anInstance->UserID = -2;
					break;
				case 1:
					
					if($anArrayOdUserID[0]!=-2)
					{
						$where .= $strAnd." t1.UserID = ".((int) $anArrayOdUserID[0]);
						$strAnd = " AND ";
					}

					$anInstance->UserID = (int) $anArrayOdUserID[0];
					break;
				default:
					$in = "";
					foreach ($anArrayOdUserID as $anID)
					{
						if ((int) $anID > 0)
						{
							if ($in != "")
							{
								$in .= ", ";
							}
							else 
							{
								$anInstance->UserID = $anID;
							}
							$in .= (int)$anID; 
						}
					}
					if ($in != "")
					{
						$where .= $strAnd." t1.UserID IN (".$in.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
                
        if($anInstance->CatalogID!=null)
		{
			$where .= $strAnd." t1.CatalogID = ".$anInstance->CatalogID;
			$strAnd = " AND ";
		}

		if($anInstance->CheckPointID !=null && getMDVersion() >= esveAgendas)
		{
			$where .= $strAnd." t1.checkpointID ". ( is_array( $anInstance->CheckPointID ) ? ' IN ( '. implode(',', $anInstance->CheckPointID) .' )' : ' = ' . $anInstance->CheckPointID );

			$strAnd = " AND ";
		}
		
		if (!is_null($attributes)) {
		
			$strRLIKE = "";
			$countAttributes = 0;
			foreach ($attributes as $attribute) {
				$tmpAttribute = array();
				foreach ($attribute as $attrValue) {
					if ($attrValue == "") {
						array_push($tmpAttribute, '.*');
					} else {
						array_push($tmpAttribute, $attrValue);
					}
				}
				//@AAL 24/04/2015: Modificado por que estaba mal formando el query al armar el filtro
				if (count($tmpAttribute)) {
					$strRLIKE = ("(".implode("|", $tmpAttribute).")[|]?");
				}
				//@AAL
				if ($strRLIKE != "") {
					$where .= $strAnd." t1.FilterText RLIKE " . $aRepository->DataADOConnection->Quote($strRLIKE);
					if (count($attributes) < $countAttributes) {
						$where .= " OR ";
					}
				}
				$countAttributes++;
			}
			if ($strRLIKE != "") {
				$strAnd = " AND ";
			}
		}
        
        if ($where != '')
		{
			$where = "WHERE ".$where;
		}
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$strAdditionalFields = "";
		if(getMDVersion() >= esveAgendas) {
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion, t1.checkpointID, t1.FilterTextAttr, t4.name';
		}
		//@JAPR 2016-02-12: Agregada la configuración para permitir variar el inicio de la semana entre Domingo o Lunes (#KN0SQL)
		if (getMDVersion() >= esvAgendaFirsDayWeek) {
			$strAdditionalFields .= ', t1.FirstDayOfWeek';
		}
		//@JAPR
		$sql = "SELECT 
                    t1.AgendaSchedID
                    ,t1.AgendaSchedName
                    ,t1.CatalogID
                    ,t2.ParentID
                    ,t2.CatalogName
                    ,t2.TableName
                    ,t1.AttributeID
                    ,t3.MemberName
                    ,t1.FilterText
                    ,t1.CaptureStartTime
                    ,t1.UserID
                    ,t1.LastDateID
                    ,t1.VersionNum 
                    ,t1.CheckinSurvey
                    ,t1.`Status`
                    ,t1.RecurPatternType
                    ,t1.RecurPatternOpt
                    ,t1.RecurPatternNum
                    ,t1.RecurRangeOpt
                    ,t1.Ocurrences
                    ,t1.CreationDateID
                    ,t1.GenerateAgendasNextDayTime 
                    {$strAdditionalFields} 
				FROM SI_SV_SurveyAgendaScheduler t1 
					LEFT OUTER JOIN SI_SV_Catalog t2 ON 
                            t1.CatalogID = t2.CatalogID 
					LEFT OUTER JOIN SI_SV_CatalogMember t3 ON 
                            t1.CatalogID = t3.CatalogID 
                        AND t1.AttributeID = t3.MemberID 
                    ".( getMDVersion() >= esveAgendas ? "LEFT JOIN si_sv_checkpoint t4 ON t4.id = t1.checkpointID
                    LEFT JOIN si_sv_datasource t5 ON t5.datasourceID = t4.catalog_id" : "" )."
                ".$where."
                ORDER BY 
                    t1.UserID
                    ,t2.CatalogName
                    ,t3.MemberName
                    ,t1.FilterText";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaScheduler ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if ($bUsePaginator)
		{
			//Total de Registros
			$numRows = $aRS->RecordCount();
			
			$_GET["ipp"] = $ipp;
			
			if(!isset($_GET["page"]))
			{
				$_GET["page"] = 1;
			}
			
			if($numRows>0)
			{
				//Creamos el objeto de paginacion
				$anInstance->Pages = new Paginator();
				$anInstance->Pages->items_total = $numRows;
				$anInstance->Pages->items_per_page = $ipp;
				$anInstance->Pages->mid_range = 9;
				$anInstance->Pages->paginate();
				
				//Agregamos el elemento LIMIT al SELECT para obtener los registros que se requieren
				$sql .= $anInstance->Pages->limit;
				
				$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda, SI_SV_Catalog, SI_SV_CatalogMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$anInstance->Pages->current_num_items = $aRS->RecordCount();
				}
			}
		}
		
		$arrUserIDs = array();
 		$arrAgendas = array();
		while (!$aRS->EOF)
		{
			$anInstanceAgenda = BITAMSurveyAgendaScheduler::NewInstanceFromRS($anInstance->Repository, $aRS);
			if (count($anInstanceAgenda->SurveyIDs) > 0)
			{
				
				$arrUserIDs[$anInstanceAgenda->UserID] = $anInstanceAgenda->UserID;
			}
            $anInstance->Collection[] = $anInstanceAgenda;

			$arrAgendas[$anInstanceAgenda->AgendaSchedID] = $anInstanceAgenda;
			//@JAPR
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
		if (count($arrUserIDs) > 0)
		{
			$sql = "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO IN (".implode(',', $arrUserIDs).')';
			$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
			if ($aRS)
			{
				while (!$aRS->EOF)
				{
					$intUserID = $aRS->fields["cla_usuario"];
					$arrUserIDs[$intUserID] = (string) $aRS->fields["nom_largo"];
					$aRS->MoveNext();
				}
			}
			
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda)
			{
				$anInstanceAgenda->UserName = (string) @$arrUserIDs[$anInstanceAgenda->UserID];
			}
		}

		/*@AAL 24/04/2015: Agregado para realizar el filtrado por palabra, Copiado del Archivo surveyAgenda.inc.php*/
		if(!is_null($searchString) && $searchString != '') {
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda) {
				$arr = array();
				array_push($arr, $anInstanceAgenda->AgendaSchedName);
				array_push($arr, $anInstanceAgenda->UserName);
				array_push($arr, $anInstanceAgenda->CatalogName);
				array_push($arr, $anInstanceAgenda->FilterText);
				array_push($arr, $anInstanceAgenda->CheckPointName);
				array_push($arr, $anInstanceAgenda->getAgendaTime());
				array_push($arr, $anInstanceAgenda->getAgendaStatus());
				$foundString = false;
				foreach ($arr as $value) {
					if (strpos(strtolower($value), strtolower($searchString)) !== false) {
						$foundString = true;
					}
				}
				if (!$foundString) {
					unset($anInstance->Collection[$aKey]);
				}
			}
		}
		//@AAL
		
	return $anInstance;
	}
	
    //NO MODIFICADO!
	static function NewInstanceUniquePerDay($aRepository, $userID, $aDate, $catalogID, $attributeID, $filterText)
	{
		$strCalledClass = static::class;

		$anInstance = new $strCalledClass($aRepository);
		
		$where = "";
		$strAnd = '';
		if (!is_null($catalogID))
		{
			if (!is_array($catalogID))
			{
				$catalogID = array($catalogID);
			}
			
			switch (count($catalogID))
			{
				case 0:
					break;
				case 1:
					$where = " t1.CatalogID = ".((int) $catalogID[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($catalogID as $anID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anID; 
					}
					if ($where != "")
					{
						$where = " t1.CatalogID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		//@JAPR 2014-11-03: Modificado el esquema de agendas
		//Ya no se agregará el campo de CatalogAttribute
		if (!is_null($attributeID) && $attributeID > 0)
		{
			if (!is_array($attributeID))
			{
				$attributeID = array($attributeID);
			}
			
			switch (count($attributeID))
			{
				case 0:
					break;
				case 1:
					$where = " t1.AttributeID = ".((int) $attributeID[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($attributeID as $anID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anID; 
					}
					if ($where != "")
					{
						$where = " t1.AttributeID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($userID))
		{
			if (!is_array($userID))
			{
				$userID = array($userID);
			}
			
			switch (count($userID))
			{
				case 0:
					break;
				case 1:
					$where .= $strAnd." t1.UserID = ".((int) $userID[0]);
					$strAnd = " AND ";
					$anInstance->UserID = (int) $userID[0];
					break;
				default:
					$in = "";
					foreach ($userID as $anID)
					{
						if ((int) $anID > 0)
						{
							if ($in != "")
							{
								$in .= ", ";
							}
							else 
							{
								$anInstance->UserID = $anID;
							}
							$in .= (int)$anID; 
						}
					}
					if ($in != "")
					{
						$where .= $strAnd." t1.UserID IN (".$in.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($aDate) && substr($aDate,0,10)!="0000-00-00")
		{
			$where .= $strAnd." t1.CaptureStartDate = ".$aRepository->DataADOConnection->DBTimeStamp($aDate);
			$strAnd = " AND ";
		}
		
		if (!is_null($filterText) && $filterText != '')
		{
			$where .= $strAnd." t1.FilterText = ".$aRepository->DataADOConnection->Quote($filterText);
			$strAnd = " AND ";
		}
		
		if ($where != '')
		{
			$where = "WHERE ".$where;
		}
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$strAdditionalFields = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.VersionNum ';
		}
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		if(getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.CheckinSurvey, t1.Status ';
		}
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		if(getMDVersion() >= esveAgendas) {
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR
		$sql = "SELECT t1.AgendaSchedID, t1.AgendaName, t1.CatalogID, t2.ParentID, t2.CatalogName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, 
					t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID $strAdditionalFields 
				FROM SI_SV_SurveyAgenda t1 
					LEFT OUTER JOIN SI_SV_Catalog t2 ON t1.CatalogID = t2.CatalogID 
					LEFT OUTER JOIN SI_SV_CatalogMember t3 ON t1.CatalogID = t3.CatalogID AND t1.AttributeID = t3.MemberID 
					".$where.
				" ORDER BY t1.UserID, t2.CatalogName, t3.MemberName, t1.FilterText";
				
		//var_dump($sql);
		//exit();
				
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrUserIDs = array();
		$arrAgendas = array();
		while (!$aRS->EOF)
		{
			$anInstanceAgenda = BITAMSurveyAgendaScheduler::NewInstanceFromRS($anInstance->Repository, $aRS);
			if ( count($anInstanceAgenda->SurveyIDs) > 0)
			{
				$anInstance->Collection[] = $anInstanceAgenda;
				$arrUserIDs[$anInstanceAgenda->UserID] = $anInstanceAgenda->UserID;
			}
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$arrAgendas[$anInstanceAgenda->AgendaSchedID] = $anInstanceAgenda;
			//@JAPR
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
		if (count($arrUserIDs) > 0)
		{
			$sql = "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO IN (".implode(',', $arrUserIDs).')';
			$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
			if ($aRS)
			{
				while (!$aRS->EOF)
				{
					$intUserID = $aRS->fields["cla_usuario"];
					$arrUserIDs[$intUserID] = (string) $aRS->fields["nom_largo"];
					$aRS->MoveNext();
				}
			}
			
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda)
			{
				$anInstanceAgenda->UserName = (string) @$arrUserIDs[$anInstanceAgenda->UserID];
			}
		}
		
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		//Ahora el estado de la agenda se captura directo en ella misma, ya no depende del estado de cada encuesta
		if(getMDVersion() < esvAgendaRedesign) {
			if ($where != '')
			{
				$where = "AND ".substr($where, 6);
			}
			$sql = "SELECT A.AgendaSchedID, A.Status, B.EnableReschedule 
				FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaSchedDet A, SI_SV_Survey B 
				WHERE t1.AgendaSchedID = A.AgendaSchedID AND A.SurveyID = B.SurveyID ".$where." 
				ORDER BY A.AgendaSchedID, A.Status DESC";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaSchedDet ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$intAgendaID = (int) $aRS->fields['agendaschedid'];
				$intStatus = (int) $aRS->fields['status'];
				switch ($intStatus)
				{
					case agdRescheduled:
						//Si hay por lo menos una encuesta recalendarizada, se considera toda la agenda como recalendarizada
						$anInstanceAgenda = @$arrAgendas[$intAgendaID];
						if (!is_null($anInstanceAgenda))
						{
							$anInstanceAgenda->Status = $intStatus;
							unset($arrAgendas[$intAgendaID]);
						}
						break;
				
					case agdOpen:
						//Si hay por lo menos una encuesta sin contestar y que NO esté configurada para recalendarizar dicha encuesta, se considera 
						//toda la agenda como pendiente de contestar
						if (((int) $aRS->fields['enablereschedule']) == 0)
						{
							$anInstanceAgenda = @$arrAgendas[$intAgendaID];
							if (!is_null($anInstanceAgenda))
							{
								$anInstanceAgenda->Status = $intStatus;
								unset($arrAgendas[$intAgendaID]);
							}
							break;
						}
						//En este caso es una encuesta no contestada pero que permite reagendar, así que esa no cuenta
						break;
					
					default:
						break;
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
		
		return $anInstance;
	}
	
	static function NewInstanceByUser($aRepository, $userID)
	{
		return BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, null, array($userID));
	}
	
	static function NewInstanceByUserAndDateTime($aRepository, $userID, $aDate = null, $aTime = null)
	{
		return BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, null, array($userID), $aDate, $aTime);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$userID=null;
		if (array_key_exists("userID", $aHTTPRequest->POST))
		{
			if (getMDVersion() >= esvAgendaRedesign) {
				if ($aHTTPRequest->POST["userID"] != "") {
					$arrUsers = explode("|", $aHTTPRequest->POST["userID"]);
					$userID = $arrUsers;
				}
			} else {
				$userID = (int)$aHTTPRequest->POST["userID"];
			}
		}
		
		$statusID=null;
		if (array_key_exists("statusID", $aHTTPRequest->POST))
		{
			if (getMDVersion() >= esvAgendaRedesign) {
				if ($aHTTPRequest->POST["statusID"] != "") {
					$arrUsers = explode("|", $aHTTPRequest->POST["statusID"]);
					$statusID = $arrUsers;
				}
			} else {
				$statusID = (int)$aHTTPRequest->POST["statusID"];
			}
		}
		
		$catalogID=null;
		if (array_key_exists("catalogID", $aHTTPRequest->POST))
		{
			if($aHTTPRequest->POST["catalogID"] != 0)
			{
				//var_dump($aHTTPRequest->POST["catalogID"]);
				//exit();
				$catalogID = (int)$aHTTPRequest->POST["catalogID"];
			}
		}
		
		$attributesID = null;
		if (array_key_exists("attributesID", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["attributesID"] != "") {
				$attributesID = json_decode($aHTTPRequest->POST["attributesID"]);
			}
		}
		
		$searchString = null;
		if (array_key_exists("searchString", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["searchString"] != "") {
				$searchString = @$aHTTPRequest->POST["searchString"];
			}
		}
		
		$startDate=null;
		if (array_key_exists("startDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["startDate"])!="" && substr($aHTTPRequest->POST["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->POST["startDate"]);
			}
		}

		$endDate=null;
		if (array_key_exists("endDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["endDate"])!="" && substr($aHTTPRequest->POST["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->POST["endDate"]);
			}
		}
		
		$ipp=100;
		if (array_key_exists("ipp", $aHTTPRequest->POST))
		{
			$ipp = (int)$aHTTPRequest->POST["ipp"];
		}
		
		//var_dump("(".json_encode(BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, null, $userID, $endDate, null, null,$startDate, $catalogID, $ipp, null, $attributesID, $searchString, $statusID)).")");
		
		return BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, null, $userID, $endDate, null, $startDate, $catalogID, $ipp, null, $attributesID, $searchString, $statusID);
	}
	
	//@JAPR 2015-10-26: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
	/* Carga los Schedulers de agendas configurados para el usuario indicado y genera las Agendas que aún no se encuentren creadas en la metadata tanto para la fecha actual
	como para el día posterior si está configurado el Scheduler para tal acción. Si no se especifica un usuario, procesará todos los Schedulers del repositorio
	El proceso filtrado por usuario está pensado para ser invocado durante la sincronización del mismo, mientras que el proceso global está diseñado para ser invocado por el Agente
	de eForms o algún script independiente que se encuentre corriendo en Winddows
	*/
	static function GenerateAgendasFromAgendaSchedulersByUser($aRepository, $aUserID = null) {
		if (getMDVersion() < esvAgendaScheduler) {
			return true;
		}
		
        require_once('surveyAgenda.inc.php');
		require_once('settingsvariable.inc.php');
        require_once('utils.inc.php');
		
		if (is_null($aUserID)) {
			$aUserID = @$_SESSION["PABITAM_UserID"];
		}
		
		$aUserID = (int) $aUserID;
		//Si no se especifica un usuario, se procesarán las Agendas de todos los usuarios
		if ($aUserID <= 0) {
			$aUserID = -2;
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Processing Agendas for user: {$aUserID}");
		}
		
        //evaluamos la fecha de hoy    
        $dFechaHoy = date('Y-m-d');
        $dHoraHoy = date('H:i');
        
        //para propositos de testing se usa esta config que trae una fecha para simular la fecha del server
        $objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DATETIME_SERVER_EMULATION_4AGENDASCHEDULER');
        if (!is_null($objSetting) && ((string)@$objSetting->SettingValue)!= '') {
            //evaluamos la fecha de hoy    
            $tmpvar = (string) @$objSetting->SettingValue;
            
            $tmpvar = strtotime($tmpvar);
            if ($tmpvar !== false) {
                $dFechaHoy = date('Y-m-d', $tmpvar);
                $dHoraHoy = date('H:i', $tmpvar);
            }
        }
        //--------------------------------------------------------------------
        
        $dFechaSig = date('Y-m-d', strtotime($dFechaHoy.' + 1 day'));
         
        //traemos las agendas del dia
		//@JAPR 2015-10-26: Optimizado para generar un Array indexado por Usuario y Filtro para agilizar la búsqueda ahora que se podrían procesar todos los usuarios del repositorio
		$arrAgendasTodayByUserAndFilter = array();
		$arrAgendasTomorrorByUserAndFilter = array();
		
        //2015.02.25 JCEM #CIS9MF para el usuario que se loguea
        $objSurveyAgendaTodayColl = BITAMSurveyAgendaCollection::NewInstance($aRepository, null, $aUserID, null, null, null, $dFechaHoy);
		//@JAPR 2015-10-26: Optimizado para generar un Array indexado por Usuario y Filtro para agilizar la búsqueda ahora que se podrían procesar todos los usuarios del repositorio
		foreach ($objSurveyAgendaTodayColl->Collection as $objSurveyAgenda) {
			if (!isset($arrAgendasTodayByUserAndFilter[$objSurveyAgenda->UserID])) {
				$arrAgendasTodayByUserAndFilter[$objSurveyAgenda->UserID] = array();
			}
			$arrAgendasTodayByUserAndFilter[$objSurveyAgenda->UserID][$objSurveyAgenda->FilterText] = 1;
		}
		//@JAPR
        
        //coleccion de agendaschedulers
        //2015.02.25 JCEM #CIS9MF para el usuario que se loguea
        $objAgendaSchedulerColl = BITAMSurveyAgendaSchedulerCollection::NewInstance($aRepository, null, $aUserID);
		if (!is_null($objAgendaSchedulerColl)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Number of Agenda Schedulers found: ".count($objAgendaSchedulerColl->Collection));
			}
		}
        
		//@JAPR 2015-08-28: Corregido un bug, esta propiedad era un array, así que la validación original siempre generaba las agendas del día siguiente sin
		//importar realmente la hora actual
		$objSurveyAgendaNextDayColl = null;
        //si alguna agenda tiene cfg dia sig traemos las agendas del dia siguiente
        foreach($objAgendaSchedulerColl->Collection as $objAgendaScheduler){
			//@JAPR 2015-08-28: Corregido un bug, esta propiedad era un array, así que la validación original siempre generaba las agendas del día siguiente sin
			//importar realmente la hora actual
			$strGenerateAgendasNextDayTime = $objAgendaScheduler->getAgendaTime();
			//Valida que las 00:00 no se considere una hora válida, ya que será el default cuando no exista esta configuración
			if ((int) str_replace(":", "", $strGenerateAgendasNextDayTime) == 0) {
				$strGenerateAgendasNextDayTime = '';
			}
            if ($strGenerateAgendasNextDayTime !== '') {
                //2015.02.25 JCEM #CIS9MF para el usuario que se loguea
                $objSurveyAgendaNextDayColl = BITAMSurveyAgendaCollection::NewInstance($aRepository, null, $aUserID, null, null, null, $dFechaSig);
				//@JAPR 2015-10-26: Optimizado para generar un Array indexado por Usuario y Filtro para agilizar la búsqueda ahora que se podrían procesar todos los usuarios del repositorio
				foreach ($objSurveyAgendaNextDayColl->Collection as $objSurveyAgenda) {
					if (!isset($arrAgendasTomorrorByUserAndFilter[$objSurveyAgenda->UserID])) {
						$arrAgendasTomorrorByUserAndFilter[$objSurveyAgenda->UserID] = array();
					}
					$arrAgendasTomorrorByUserAndFilter[$objSurveyAgenda->UserID][$objSurveyAgenda->FilterText] = 1;
				}
				//@JAPR
                break;
            }
        }
		
        //lo recorremos para ver cual aplica
        foreach ($objAgendaSchedulerColl->Collection as $objAgendaScheduler) {
			//$objAgendaScheduler es un objeto agendascheduler
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Processing Agenda Scheduler: {$objAgendaScheduler->AgendaSchedID} => {$objAgendaScheduler->AgendaSchedName}");
			}
			
			//si aplica
			if ($objAgendaScheduler->isValidDate($dFechaHoy)) {
				//Generamos agenda
				//comprobamos existe agenda
				//@JAPR 2015-10-26: Optimizado para generar un Array indexado por Usuario y Filtro para agilizar la búsqueda ahora que se podrían procesar todos los usuarios del repositorio
				$blnFound = (int) @$arrAgendasTodayByUserAndFilter[$objAgendaScheduler->UserID][$objAgendaScheduler->FilterText];
				//@JAPR
				
				//si no se encontro
				if (!$blnFound) {
					//Crear agenda
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Generating Agenda for today");
					}
					
					$objSurveyAgenda = BITAMSurveyAgenda::NewInstance($aRepository);
					$objSurveyAgenda->UserID = $objAgendaScheduler->UserID;
					$objSurveyAgenda->AgendaName = $objAgendaScheduler->AgendaSchedName;
					$objSurveyAgenda->CatalogID = $objAgendaScheduler->CatalogID;
					$objSurveyAgenda->AttributeID = $objAgendaScheduler->AttributeID;
					$objSurveyAgenda->CaptureStartDate = $dFechaHoy;
					$objSurveyAgenda->CaptureStartTime_Hours = $objAgendaScheduler->CaptureStartTime_Hours;
					$objSurveyAgenda->CaptureStartTime_Minutes = $objAgendaScheduler->CaptureStartTime_Minutes;
					$objSurveyAgenda->FilterText = $objAgendaScheduler->FilterText;
					$objSurveyAgenda->CheckinSurvey = $objAgendaScheduler->CheckinSurvey;
					$objSurveyAgenda->Status = $objAgendaScheduler->Status;
					//$objSurveyAgenda->GenerationDate = $dFechaHoy;
					$objSurveyAgenda->AgendaSchedID = $objAgendaScheduler->AgendaSchedID;
					$objSurveyAgenda->SurveyIDs = $objAgendaScheduler->SurveyIDs;
					//@JAPR 2015-09-02: Corregido un bug, al generar las agendas desde el Scheduler faltaba que hererada estas configuraciones
					$objSurveyAgenda->CheckPointID = $objAgendaScheduler->CheckPointID;
					$objSurveyAgenda->FilterTextAttr = $objAgendaScheduler->FilterTextAttr;
					//@JAPR
					
					$objSurveyAgenda->save();
					$objSurveyAgendaTodayColl->Collection[] = $objSurveyAgenda;
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Agenda was already generated for today");
					}
				}
			}
			else {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Agenda Scheduler is not valid for today");
				}
			}
			
			//si tiene config dia siguiente procesar tambien las del dia siguiente
			//@JAPR 2015-08-28: Corregido un bug, esta propiedad era un array, así que la validación original siempre generaba las agendas del día siguiente sin
			//importar realmente la hora actual
			$strGenerateAgendasNextDayTime = $objAgendaScheduler->getAgendaTime();
			//Valida que las 00:00 no se considere una hora válida, ya que será el default cuando no exista esta configuración
			if ((int) str_replace(":", "", $strGenerateAgendasNextDayTime) == 0) {
				$strGenerateAgendasNextDayTime = '';
			}
			if (!is_null($objSurveyAgendaNextDayColl) && $strGenerateAgendasNextDayTime !== '') {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Checking Agenda Scheduler configuration for tomorrow");
				}
				if ($objAgendaScheduler->GenerateAgendasNextDayTime_Hours !== ''&& $objAgendaScheduler->GenerateAgendasNextDayTime_Minutes !== '') {
					$a = $objAgendaScheduler->GenerateAgendasNextDayTime;
					
					//si la hora actual es mas tarde que la hora configurada se procesa las del dia siguiente
					$nUFechaEval  = strtotime(make_date(date("Y"), date("m"), date("d"),$a[0], $a[1], 0)) ;
					$nUFechaHoy = strtotime($dFechaHoy.' '.$dHoraHoy);
					
					if ($nUFechaHoy >= $nUFechaEval ) {
						if ($objAgendaScheduler->isValidDate($dFechaSig)) {
							//Generamos agenda
							//comprobamos existe agenda
							//@JAPR 2015-10-26: Optimizado para generar un Array indexado por Usuario y Filtro para agilizar la búsqueda ahora que se podrían procesar todos los usuarios del repositorio
							$blnFound = (int) @$arrAgendasTomorrorByUserAndFilter[$objAgendaScheduler->UserID][$objAgendaScheduler->FilterText];
							//@JAPR
							
							if (!$blnFound) {
								//Crear agenda
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("Generating Agenda for tomorrow");
								}
								
								$objSurveyAgenda = BITAMSurveyAgenda::NewInstance($aRepository);
								$objSurveyAgenda->UserID = $objAgendaScheduler->UserID;
								$objSurveyAgenda->AgendaName = $objAgendaScheduler->AgendaSchedName;
								$objSurveyAgenda->CatalogID = $objAgendaScheduler->CatalogID;
								$objSurveyAgenda->AttributeID = $objAgendaScheduler->AttributeID;
								$objSurveyAgenda->CaptureStartDate = $dFechaSig;
								$objSurveyAgenda->CaptureStartTime_Hours = $objAgendaScheduler->CaptureStartTime_Hours;
								$objSurveyAgenda->CaptureStartTime_Minutes = $objAgendaScheduler->CaptureStartTime_Minutes;
								$objSurveyAgenda->FilterText = $objAgendaScheduler->FilterText;
								$objSurveyAgenda->CheckinSurvey = $objAgendaScheduler->CheckinSurvey;
								$objSurveyAgenda->Status = $objAgendaScheduler->Status;
								//$objSurveyAgenda->GenerationDate = $dFechaSig;
								$objSurveyAgenda->AgendaSchedID = $objAgendaScheduler->AgendaSchedID;
								$objSurveyAgenda->SurveyIDs = $objAgendaScheduler->SurveyIDs;
								//@JAPR 2015-09-02: Corregido un bug, al generar las agendas desde el Scheduler faltaba que hererada estas configuraciones
								$objSurveyAgenda->CheckPointID = $objAgendaScheduler->CheckPointID;
								$objSurveyAgenda->FilterTextAttr = $objAgendaScheduler->FilterTextAttr;
								//@JAPR
								
								$objSurveyAgenda->save();
								$objSurveyAgendaNextDayColl->Collection[] = $objSurveyAgenda;
							}
							else {
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("Agenda was already generated for tomorrow");
								}
							}
						}
						else {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Agenda Scheduler is not valid for tomorrow");
							}
						}
					}
					else {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Agenda for tomorrow can't be generated at this time");
						}
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Invalid Agenda Scheduler configuration");
					}
				}
			}
		}
		
		return true;
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Agendas Scheduler");
	}

	function get_QueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}
		
		return "BITAM_SECTION=SurveyAgendaSchedulerCollection".$strParameters;
	}
	
	function get_AddRemoveQueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}
		
		return "BITAM_PAGE=SurveyAgendaScheduler".$strParameters;
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'AgendaSchedID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AgendaSchedName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
      		
	    $aField = BITAMFormField::NewFormField();
		$aField->Name = "getUserName";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
				
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogName";
		$aField->Title = translate("Catalog");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterText";
		$aField->Title = translate("Attribute value");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//@AAL 14/05/2015 Comentado, puesto que esta columna no debe de aparecer.
		/*$aField = BITAMFormField::NewFormField();
		$aField->Name = "getAgendaStatus";
		$aField->Title = translate("Status");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;*/

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getCheckInTime";
		$aField->Title = translate("CheckIn time");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

        $aField = BITAMFormField::NewFormField();
		$aField->Name = "getAgendaTime";
		$aField->Title = translate("Agendas next day at");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function isEmptyEndDate()
	{
		return is_null($this->EndDate) || trim($this->EndDate) == "" || substr($this->EndDate,0,10) == "0000-00-00";
	}
	
	function isEmptyStartDate()
	{
		return is_null($this->StartDate) || trim($this->StartDate) == "" || substr($this->StartDate,0,10) == "0000-00-00";
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
   /* function afterTitleCode($aUser) {
        //dummy_fnc
    }*/
	function afterTitleCode($aUser)
	{
	
		?>
		<div id="boxFilter" style="float:left; width: 190px; height: 100%; background:white;border-right: 1px solid lightgray"></div>
		<?
	
		require_once("catalog.inc.php");
		/*Este código no aplica para el caso de Agendas, se podría aplicar para filtrar por el estado de la Agenda pero eso hasta que
			se implementen los estados mencionados por DABdo el 2012-03-15 (-1=Nueva definición (no enviada a la App), 0=Enviada a App pero no
		//$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		//$numSurveys = count($surveyCollection->Collection);
		*/
		
		$maxCell = 4;
		//$numColSpanForm = 2;
		$totalColSpanTable = 1 + ($maxCell*2);
		
		if($this->UserID != -2 || !$this->isEmptyStartDate() || !$this->isEmptyEndDate() || $this->ipp != 100)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = translate("Reapply");
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = translate("Apply");
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply filter").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">'.translate("Clear").'</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		
		$btnDelEntries = "";
		
		$arrFilterAttributes = array();
		$arrFilterAttributes['CatalogID'] = array("MemberName" => translate("Catalog"));
		$arrFilterAttributes['CatalogID']["Values"] = BITAMCatalog::getCatalogs($this->Repository);
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons);
		$countButtons = ($countButtons < 3)?3:0;
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo =
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

	
?>
	 <? $displayFilterTable = (getMDVersion() >= esvAgendaRedesign) ? "none" : "block"; ?>
	<table id="filterTable" style="display: <?= $displayFilterTable ?>;" cellpadding="0" cellspacing="0">

		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		$arrayUsers[-2] = translate("All");
		//$arrayUsers[-1] = "NA";
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			$sql.=" AND B.CLA_USUARIO > 0 ";
		}
		if($_SESSION["PABITAM_UserID"] != 1) {
			$sql .= (" AND A.Supervisor = ".$_SESSION["PABITAM_UserID"]);
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		if((count($arrFilterAttributes)+1)>$countButtons)
		{
			$maxRows = count($arrFilterAttributes)+1;
		}
		else 
		{
			$maxRows = $countButtons;
		}
		
		$selected = "";
		//if($this->SurveyID!=0)
		//{
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?=translate("Filters")?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser => $nameUser)
		{
			$selected="";
			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
			$countCell = 1;
		//}
		
		if(count($arrFilterAttributes)>0)
		{
			foreach($arrFilterAttributes as $memberID => $memberInfo)
			{
				$position++;
				$select="";

				if($position==$positionDates)
				{
					if($numRows==0)
					{
?>
			<td style="text-align:right;" class="styleBorderFilterTop">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
			</td>
			<td class="styleBorderFilterTop">
				<?=$strFrom?>
			</td>
<?
					}
					elseif($numRows==1)
					{
?>
			<td style="text-align:right;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
			</td>
			<td>
				<?=$strTo?>
			</td>
<?
					}
					elseif($numRows==2)
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}

?>
			<td style="text-align:right;" <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
			<td <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
			
				<?=$this->Pages->display_items_per_page_report()?>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					$classBorderTop = "";
					$classBorderRightTop = "";
					if($numRows==0)
					{
						$classBorderTop = ' class="styleBorderFilterTop"';
						$classBorderRightTop = ' class="styleBorderFilterRightTop"';
					}
					else 
					{
						$classBorderTop = '';
						$classBorderRightTop = ' class="styleBorderFilterRight"';
					}
					
					if(isset($arrayFilters[$numRows]))
					{
?>
			<td <?=$classBorderTop?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightTop?>>
				<?=$arrayFilters[$numRows]?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = "";
						$classBorderRightBottom = "";
						if($maxRows == ($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
							$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderBottom = '';
							$classBorderRightBottom = ' class="styleBorderFilterRight"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					if(isset($arrayButtons[$numRows]))
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td style="padding-top:5px;padding-bottom:5px;">
				<?=$arrayButtons[$numRows]?>
			</td>
<?
					}
					else 
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
					}
					
					$countCell++;
				}
				
				if(($countCell%$maxCell)==0)
				{
					$numRows++;
					$position = 1;
?>
		</tr>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
				else 
				{
					$position++;
				}
				
				$classBorderLeft = ' class="styleBorderFilterLeft"';
				$classBorderBottom = '';
				if($maxRows == ($numRows+1))
				{
					$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
					$classBorderBottom = ' class="styleBorderFilterBottom"';
				}

?>
			<td style="text-align:right;" <?=$classBorderLeft?>>
				&nbsp;&nbsp;&nbsp;<span><?=$memberInfo["MemberName"]?></span>
			</td>
			<td <?=$classBorderBottom?>>
				<select id="<?=$memberID?>" name="<?=$memberID?>" memberID="<?=$memberID?>" style="width:150px; font-size:11px">
<?
				global $idxSelSurveyID;
				$idxSelSurveyID = 0;
				$cmbIndex = 0;

?>
					<option value="0" ><?=translate("All")?></option>
<?
				$attribValues = $memberInfo["Values"];
				$attribSelected = null;
				eval('$attribSelected = $this->'.$memberID.";");
				
				if (is_null($attribSelected))
				{
					$attribSelected = 0;
				}
				foreach ($attribValues as $key=>$desc)
				{
					$select="";

					if($attribSelected == $key)
					{
						$idxSelSurveyID = $cmbIndex;
						$select = "selected";
					}
						
?>
						<option value="<?=$key?>" <?=$select?>><?=$desc?></option>
<?
					$cmbIndex++;
				}
?>
				</select>
			</td>
<?
				$countCell++;
			}
		}
		
		//if($this->SurveyID!=0)
		//{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
	?>
				<td style="text-align:right;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		//}
?>
	</table>
	<br>
<?
	}
	
	function generateBeforeFormCode($aUser)
	{
		$myFormName = get_class($this);
?>		

	<link type="text/css" rel="stylesheet" href="css/eBavel.css"/>

	<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery-ui-1.10.1.custom.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.jsperanto.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamApp.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamstorage.js"></script>
	<script type="text/javascript" src="js/eBavel/databases.js"></script>
	<script type="text/javascript" src="js/eBavel/section.class.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.widgets.js"></script>
	<script type="text/javascript" src="js/eBavel/date.js"></script>
	<script type="text/javascript" src="js/jsondefinition/bitamAppFormsJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jsondefinition/agendasJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jquery.widgets.js"></script>
	<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
	
		function applyFilterAttrib (filter, oldTags, descrTag, allCatalogs)
		{
			if (filter) {
				
				for (var fkey in filter) {
				
					//Usuarios
					if (fkey == 'Usuario') {
						var filterUsers = filter['Usuario'];
						var agendasUsers = new Array();
						for (var ukey in filterUsers) {
							var filterUser = filterUsers[ukey];
							agendasUsers.push(filterUser);
						}
						if (agendasUsers.length) {
							frmAttribFilter.userID.value = agendasUsers.join('|');
						}
						if (descrTag == 'Usuario') {
							frmAttribFilter.oldUserTags.value = oldTags;
						}
					}
					
					//Catalogo
					if (fkey =='Catalogo') {
						frmAttribFilter.catalogID.value = parseInt(filter['Catalogo'][0], 10);
					}
					frmAttribFilter.oldCatalogTags.value = allCatalogs;
					
					//Attributes
					if (fkey == 'Attributes') {
						var filterAttributes = filter['Attributes'];
						frmAttribFilter.attributesID.value = JSON.stringify(filterAttributes);
						if (descrTag.indexOf('AttribID_') != -1){
							frmAttribFilter.oldAttributesTags.value = oldTags;
						}
					}
					
					//SearchString
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}
					
					//Status
					if (fkey == 'Status') {
						var filterStatus = filter['Status'];
						var agendasStatus = new Array();
						for (var ukey in filterStatus) {
							var filterAgendaStatus = filterStatus[ukey];
							agendasStatus.push(filterAgendaStatus);
						}
						if (agendasStatus.length) {
							frmAttribFilter.statusID.value = agendasStatus.join('|');
						}
						if (descrTag == 'Status') {
							frmAttribFilter.oldStatusTags.value = oldTags;
						}
					}
					
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
					}
				}
				
				
			} else {
				var objCatalogID = document.getElementById("CatalogID");
				frmAttribFilter.catalogID.value = objCatalogID.value;

				var objStartDate = document.getElementById("CaptureStartDate");
				frmAttribFilter.startDate.value = objStartDate.value;

				var objEndDate = document.getElementById("CaptureEndDate");
				frmAttribFilter.endDate.value = objEndDate.value;
				
				var objUserID = document.getElementById("UserID");
				frmAttribFilter.userID.value = objUserID.value;
				
				var objIpp = document.getElementById("ipp");
				frmAttribFilter.ipp.value = objIpp.value;
			}

			frmAttribFilter.submit();
		}
		
		function clearFilter()
		{
<?
			if($this->UserID != -2 || !$this->isEmptyStartDate() || !$this->isEmptyEndDate() || $this->ipp != 100 || $this->CatalogID != 0)
			{
?>
			var objCatalogID = document.getElementById("CatalogID");
			frmAttribFilter.catalogID.value = "";

			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
			else 
			{
?>
<?
			}
?>
		}

		function extractNumber(obj)
		{
			//Obtener el valor del input text
			var temp = obj.value;

			//Crear nuestra expresion regular con digitos
			var reg0Str = '[0-9]*';

			//No se manejara signo negativo 
			//reg0Str = '^-?' + reg0Str;
			reg0Str = '^' + reg0Str;

			//Indicamos que nuestra expresion regular solo
			//aceptara digitos al final de la cadena
			reg0Str = reg0Str + '$';

			//Hacemos la instancia RegExp para que se cree
			//un objeto tipo expresion regular
			var reg0 = new RegExp(reg0Str);

			//Evaluamos nuestra cadena o valor que se
			//encuentra en nuestro input text
			//Si regresa true esto quiere decir
			//que hasta el momento si se ha cumplido
			//con la expresión regular
			if (reg0.test(temp)) return true;

			//Creamos una cadena con todos los caracteres permitidos
			//en nuestra entrada de texto, esto es digitos y el signo
			//negativo, el [^.....] indica que se hara una busqueda
			//para todos aquellos caracteres que no esten entre los
			//corchetes
			var reg1Str = '[^0-9' + ']';

			//Creamos nuestra expresión regular, pero se
			//le indica ahora, que es una expresión regular
			//con búsqueda globalpara todas las entradas de
			//texto que no cumplen con lo especificado en reg1Str
			var reg1 = new RegExp(reg1Str, 'g');
			//Se reemplaza dichos caracteres con vacío
			temp = temp.replace(reg1, '');

			//Se asigna nuevamente el valor a obj.value
			obj.value = temp;
		}
		
		function checkLimitNumber(obj)
		{
			// value is present
			var tval=Trim(obj.value);
			if (tval=='')
			{
				return true;
			}
			
			reg=/^0*/;
			tval=tval.replace(reg,'')

			if (tval!='')
			{
				val=parseInt(tval);
			}
			else
			{
				val=0;
			}

			var min=1;
			var max=1000;
			
			var msg="";

			if(min!='' && max!='')
			{
				msg='Max records should be in range of ' + min + ' to ' + max + '.' ;
			}
			else
			{
  				if(min!='')
  				{
  					msg='Input value should be greater than or equal to ' + min +'.';
  				}
				else
				{
    				if(max!='')
    				{
						msg='Input value should be less than or equal to ' + max + '.';
    				}
				}
			}

			if(min!='')
			{
				if (min>val)
				{
					alert(msg);
    				obj.value = 100;
				}
			}

			if (max!='')
			{
				if (val>max) 
				{
					alert(msg);
    				obj.value = 100;
				}
			}
		}

 		function downloadAgendasTemplate(bFill)
 		{
			window.open("downloadAgendasSchedulersTemplate.php?fill="+bFill, "downloadAgendasSchedulersTemplate", "menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no", 1)
 		}
 		/*function uploadAgendasFile()
 		{
 		  document.forms.frmuploadAgendasFile.style.display = "inline"
 		}*/
 		function appendAgendasFile()
 		{
 		  document.forms.frmappendAgendasFile.style.display = "inline"
 		}
		function agendaChange () {
			openWindowDialog('main.php?BITAM_PAGE=SurveyAgendaReschedule', [window], [], 600, 600, agendaReschedule, 'agendaReschedule');
		}
		function agendaReschedule(sValue, sParams)
		{
			var strInfo = sValue;
			if (strInfo) {
				try {
					var objAgendaReschedule = JSON.parse(strInfo);
					if (objAgendaReschedule.date) {
						var elements = document.getElementsByName("AgendaSchedID[]")
						if (elements && elements.length) {
							var arrAgendaIDs = new Array();
							for (var i = 0; i < elements.length; i++) {
								if (elements[i].checked) {
									var intAgendaID = elements[i].value;
									arrAgendaIDs.push(intAgendaID);
								}
			  }
		
							if (arrAgendaIDs.length) {
								frmAttribFilter.newStartDate.value = objAgendaReschedule.date;
								frmAttribFilter.newStartHour.value = objAgendaReschedule.hour;
								frmAttribFilter.newStartMinutes.value = objAgendaReschedule.minutes;
								frmAttribFilter.agendaRescheduleID.value = arrAgendaIDs.join(',');
								frmAttribFilter.reschedule.value = 1;
								frmAttribFilter.submit();
							}
						}
					}
				} catch (e) {}
			}
		}
		
		</script>
<?
	}

	//@JAPR 2016-02-16: Habilitada la importación de Schedulers de Agendas desde XLS
    function addButtons($aUser)
    {
		//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes
		//Se ocultó el botón de Append para sólo permitir el Upload completo del template de agengas
?>
    <nobr>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadAgendasTemplate(0);"><img src="images/download_xls_templates.gif"> ////<?=translate("Download agendas template")."   "?>  </span>-->
		<button id="btnDownloadAgendasTemplate0" class="alinkescfav" onclick="downloadAgendasTemplate(0);"><img src="images/download_xls_templates.gif" alt="////<?=translate('Download agenda schedulers template')?>" title="<?=translate('Download agenda schedulers template')?>" displayMe="1" /> <?=translate("Download agenda schedulers template")?></button>
		<button id="btnDownloadAgendasTemplate0" class="alinkescfav" onclick="downloadAgendasTemplate(0);"><img src="images/download_xls_templates.gif" alt="////<?=translate('Download agendas template')?>" title="<?=translate('Download agendas template')?>" displayMe="1" /> <?=translate("Download agendas template")?></button>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadAgendasTemplate(1);"><img src="images/download_xls_templates.gif"> ////<?=translate("Export agendas")."   "?>  </span>-->
		<!--<button id="btnDownloadAgendasTemplate1" class="alinkescfav" onclick="downloadAgendasTemplate(1);"><img src="images/download_xls_templates.gif" alt="////<?=translate('Export agendas')?>" title="<?=translate('Export agendas')?>" displayMe="1" /> <?=translate("Export agendas")?></button>-->
		
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="uploadAgendasFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> ////<?=translate("Upload agendas file")."   "?>  </span></span>-->
		<button id="btnUploadAgendasFile" class="alinkescfav" onclick="uploadAgendasFile();"><img src="images/load_data_XLS.gif" alt="////<?=translate('Upload agendas file')?>" title="<?=translate('Upload agendas file')?>" displayMe="1" /> <?=translate("Upload agendas file")?></button>
	    <form name="frmuploadAgendasFile" method="POST" action="uploadAgendas.php" enctype="multipart/form-data" target="frameuploadAgendasFile" style="display:none">
	      <input type="file" name="xlsfile">
	      <input type="submit" name="txtsubmit" value="////<?=translate("Upload file")?>">
	      <input type="button" name="txtcancel" value="////<?=translate("Cancel")?>" onclick="document.forms.frmuploadAgendasFile.style.display='none';document.forms.frmuploadAgendasFile.reset()">
	    </form>
	    
    	<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="appendAgendasFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> ////<?=translate("Append agendas file")."   "?>  </span></span>-->
    	<!--<button id="btnAppendAgendasFile" class="alinkescfav" onclick="appendAgendasFile();"><img src="images/load_data_XLS.gif" alt="////<?=translate('Append agendas file')?>" title="<?=translate('Append agendas file')?>" displayMe="1" /> <?=translate("Append agendas file")?></button>-->
	    <!--<form name="frmappendAgendasFile" method="POST" action="uploadAgendas.php?appendFile=1" enctype="multipart/form-data" target="frameappendAgendasFile" style="display:none">
	      <input type="file" name="xlsfile">
	      <input type="submit" name="txtsubmit" value="////<?=translate("Upload file")?>">
	      <input type="button" name="txtcancel" value="////<?=translate("Cancel")?>" onclick="document.forms.frmappendAgendasFile.style.display='none';document.forms.frmappendAgendasFile.reset()">
	    </form>-->
		
		<button id="btnChangeSchedule" class="alinkescfav" onclick="agendaChange();"><img src="images/load_data_XLS.gif" alt="////<?=translate('Reschedule')?>" title="<?=translate('Reschedule')?>" displayMe="1" /> <?=translate("Reschedule")?></button>
		
		<div id="dialog-modal" title="Basic modal dialog" style="display: none;">
			<p>Date: <input type="text" id="datepicker"></p>
		</div>
		
    </nobr>
<?
	}
	//@JAPR
		
	function generateAfterFormCode($aUser)
	{
	
?>

	<script>
	
		delete Array.prototype.contains;
		bitamApp.getAppFormsDefinition();
		$.extend($, { t: function (word) {
				return $.jsperanto.translate(word);
			}
		});
				
		function addDescriptionTag (tagName, tagDescription, element) {
			var realTagName = $.t(tagName);
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = 1;
			} else {
				theTag.elements[elementID].num = (theTag.elements[elementID].num+1);
			}
		}
		var descriptionTags = {};
				
		var searchString = '';
		<? if (isset($_POST["searchString"]) && $_POST["searchString"] != '') { ?>
			searchString = '<?= $_POST["searchString"] ?>';
		<? } ?>
		
		var filterDate = '';
		<? if ((isset($_POST["startDate"]) && $_POST["startDate"] != '') && (isset($_POST["endDate"]) && $_POST["endDate"] != '')) { ?>
			filterDate = '<?= $_POST["startDate"] ?>@<?= $_POST["endDate"] ?>';
		<? } ?>
				
		<? foreach ($this->Collection as $anAgenda) { ?>
				//addDescriptionTag('Fecha','Fecha' , { });
				addDescriptionTag('Catalogo','Catalogo' , { id: <?= $anAgenda->CatalogID ?>, name: "<?= $anAgenda->CatalogName ?>" });
				addDescriptionTag('Usuario', 'Usuario', { id: <?= $anAgenda->UserID ?>, name: "<?= $anAgenda->UserName ?>" });
				<?
				if (isset($_POST["catalogID"]) && $_POST["catalogID"] != '' && $_POST["catalogID"] != 0) {
					$tmpCatalogCollection = BITAMSurveyAgendaScheduler::getCatMembersByCatalogID($this->Repository, true, $_POST["catalogID"]);
					if (!empty($tmpCatalogCollection)) {
						$attributeIDs = $tmpCatalogCollection[$_POST["catalogID"]];
						$values = explode("|", $anAgenda->FilterText);
						$vkey = 0;
						foreach ($attributeIDs as $ckey => $attributeID) {
							if (isset($values[$vkey])) {
								$value = $values[$vkey++];
								?>
									addDescriptionTag('AttribID_'+ <?= $ckey ?>, "<?= $attributeID ?>", { id: "<?= $value ?>", name: "<?= $value ?>" });
								<?
							}
						}
					}
				} ?>
		//@AAL 04/05/2015 Comentado, puesto que no debe aparrecer en este apartado
		//addDescriptionTag('Status', 'Status', { id: <?= $anAgenda->Status ?>, name: "<?= $anAgenda->getAgendaStatus() ?>" });
				
				<?
			} 
		?>
		var oldUserTags = JSON.parse(<?= json_encode((isset($_POST['oldUserTags']) && $_POST['oldUserTags'] != '') ? $_POST['oldUserTags'] : '{}') ?>);
		var oldStatusTags = JSON.parse(<?= json_encode((isset($_POST['oldStatusTags']) && $_POST['oldStatusTags'] != '') ? $_POST['oldStatusTags'] : '{}') ?>);
		var oldCatalogTags = JSON.parse(<?= json_encode((isset($_POST['oldCatalogTags']) && $_POST['oldCatalogTags'] != '') ? $_POST['oldCatalogTags'] : '{}') ?>);
		var oldAttributesTags = JSON.parse(<?= json_encode((isset($_POST['oldAttributesTags']) && $_POST['oldAttributesTags'] != '') ? $_POST['oldAttributesTags'] : '{}') ?>);
		var checkedElemenTags = {};
		
		/*@AAL 24/04/2015: agregado para identificar si se trata de una agenda o AgendaScheduler y mostrar el filtro fecha,
		la cual solo debe de mostrarse en agendas, la validación se muestra em el archivo jquery.widgets.js*/
		var isAgenda = false;
		//@AAL
		function initBoxFilter () {

			var sectionID = bitamApp.formsSections.Agendas.id;
			var sectionView = bitamApp.formsSections.Agendas.sectionView;
			var aSection = bitamApp.application.payload.sections[sectionID];
			var checkedUserValues = ("<?= (isset($_POST["userID"]) ? $_POST["userID"] : "") ?>");
			if (checkedUserValues) {
				checkedElemenTags['Usuario'] = checkedUserValues.split('|');
				$.each(checkedElemenTags['Usuario'], function (index, value) {
					value = ('' + value);
				});
			}
			
			var checkedStatusValues = ("<?= (isset($_POST["statusID"]) ? $_POST["statusID"] : "") ?>");
			if (checkedStatusValues) {
				checkedElemenTags['Status'] = checkedStatusValues.split('|');
				$.each(checkedElemenTags['Status'], function (index, value) {
					value = ('' + value);
				});
			}
			
			var checkedCatalogValues = ("<?= (isset($_POST["catalogID"]) ? $_POST["catalogID"] : "") ?>");
			if (checkedCatalogValues) {
				checkedElemenTags['Catalogo'] = checkedCatalogValues.split('|');
				$.each(checkedElemenTags['Catalogo'], function (index, value) {
					value = ('' + value);
				});
			}
			
			<?
			
			$atValues = "";
			if (isset($_POST["attributesID"])) {
				if ($_POST["attributesID"] == "") {
					$atValues = "{}";
				} else {
					$atValues = "" . ($_POST['attributesID']);
				}
			} else {
				$atValues = "{}";
			}
			
			?>
			
			var checkedAttributesValues = JSON.parse('<?= $atValues ?>');
			//alert(JSON.stringify(checkedAttributesValues));
			if (checkedAttributesValues) {
				for (var akey in checkedAttributesValues) {
					//alert(akey);
					checkedElemenTags[akey] = checkedAttributesValues[akey];
				}
			}
			if (!$.isEmptyObject(oldUserTags) && descriptionTags['Usuario'] && descriptionTags['Usuario'].elements) {
				$.extend(descriptionTags['Usuario'].elements, oldUserTags);
			}
			if (!$.isEmptyObject(oldStatusTags) && descriptionTags['Status'] && descriptionTags['Status'].elements) {
				$.extend(descriptionTags['Status'].elements, oldStatusTags);
			}
			if (!$.isEmptyObject(oldCatalogTags) && descriptionTags['Catalogo'] && descriptionTags['Catalogo'].elements) {
				$.extend(descriptionTags['Catalogo'].elements, oldCatalogTags);
			}
			if (!$.isEmptyObject(oldAttributesTags)) {
				$.extend(descriptionTags, oldAttributesTags);
			}
			//alert('searchString: '+searchString);
			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate
			};
			
			if ($.isEmptyObject($('#boxFilter').data())) {
				$('#boxFilter').boxfilter(options);
			} else {
				$('#boxFilter').boxfilter('update');
			}
		}
		try {
			initBoxFilter();
		} catch (e) {
			alert('Search filter error: '+e);
		}
		
	</script>

	<form name="frmAttribFilter" action="main.php?BITAM_SECTION=SurveyAgendaSchedulerCollection" method="POST" target="body" accept-charset="utf-8">
		<input type="hidden" name="BITAM_SECTION" value="SurveyAgendaSchedulerCollection">
		<input type="hidden" name="catalogID" id="catalogID" value="">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">
		<input type="hidden" name="attributesID" id="attributesID" value="">
		<input type="hidden" name="statusID" id="statusID" value="">
		<input type="hidden" name="searchString" id="searchString" value="">
		
		<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
		<input type="hidden" name="oldCatalogTags" id="oldCatalogTags" value="">
		<input type="hidden" name="oldAttributesTags" id="oldAttributesTags" value="">
		<input type="hidden" name="oldStatusTags" id="oldStatusTags" value="">
		
		<input type="hidden" name="agendaRescheduleID" id="agendaRescheduleID" value="">
		<input type="hidden" name="reschedule" id="reschedule" value="0">
		<input type="hidden" name="newStartDate" id="newStartDate" value="">
		<input type="hidden" name="newStartHour" id="newStartHour" value="00">
		<input type="hidden" name="newStartMinutes" id="newStartMinutes" value="00">
	</form>
	
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}


	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
		<input type="hidden" id="catalogID" name="catalogID" value="<?=$this->CatalogID?>">
<?
	}
}
?>