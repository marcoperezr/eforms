<?php
/* Código del App de eForms incrustado en el Administrador de eForms, se tiene que generar de esta forma ya que se encuentra dentro de un iframe
para evitar que JQueryMobile mejore automáticamente los divs del Framework de PhP, puesto que el primer div lo estaba considerando como la primer página
siendo que eso no era cierto
	Se cargarán todas las instancias correspondientes con la forma especificada y se generaran los objetos, posterior a eso se inicializará el App y quedará
en un estado de espera hasta que el Frame principal le comience a enviar los comandos para que el App pinte las secciones que se necesiten
*/
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("eSurveyServiceMod.inc.php");

//@JAPR 2019-07-25: Compactados los archivos js mas pesados para reducir el tiempo de carga, y limpieza general del código para remover archivos ya no utilizados (#2UJ6SK)
global $blnMinified;
global $strMinifiedFiles;
$blnMinified = (bool) @$_REQUEST["minified"];
$strMinifiedFiles = '';
//El comportamiento default será cargar la versión minificada
if ( $blnMinified || !isset($_REQUEST["minified"]) ) {
	$strMinifiedFiles = '.min';
}
//@JAPR

global $widthByDisp;
global $heightByDisp;
global $gbDesignMode;
$gbDesignMode = true;
//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Inicializa objetos para que se carguen los elementos como modo Web, ya que finalmente así es como se accesarán aunque se simulen dispositivos
$blnWebMode = true;
$appVersion = ESURVEY_SERVICE_VERSION;
$strUserEMail = '';
$strUserPwd = '';
//@JAPR 2016-07-21: Agregadas variables faltantes soportadas por el App
$strUserShortName = '';
//@JAPR
if ($theUser) {
	$strUserEMail = (string) @$theUser->EMail;
	$strUserPwd = BITAMDecryptPassword((string) @$theUser->Password);
	//@JAPR 2016-07-21: Agregadas variables faltantes soportadas por el App
	$strUserShortName = (string) @$theUser->UserName;
	//@JAPR
}

$intSurveyID = @getParamValue('SurveyID', 'both', '(int)', true);
if ($intSurveyID <= 0) {
	$intSurveyID = 23;
}

$objSurvey = BITAMSurvey::NewInstanceWithID($theRepository, $intSurveyID);
if (is_null($objSurvey)) {
	return;
}

//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
//Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
$arrJSON = @getEFormsDefinitionsVersions($theRepository, array('Survey' => $intSurveyID));
if (is_null($arrJSON) || !is_array($arrJSON)) {
	$arrJSON = array('data' => array());
}
else {
	$arrJSON = array('data' => $arrJSON);
}
$strJSONDefinitionsVersions = json_encode($arrJSON);

$arrJSON = @getEFormsDefinitions($theRepository, array('Survey' => $intSurveyID));
if (is_null($arrJSON) || !is_array($arrJSON)) {
	$arrJSON = array('data' => array());
}
else {
	$arrJSON = array('data' => $arrJSON);
}
//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
$arrJSON = FixNonUTF8CharsInArray($arrJSON);
//@JAPR
$strJSONDefinitions = json_encode($arrJSON);

//@JAPR 2015-08-11: Corregido un bug, no estaba aplicando el idioma configurado al usuario logeado
$userLanguage = (string) @$_SESSION["PAuserLanguage"];
if ($userLanguage == '') {
	$userLanguage = 'EN';
}

$_POST['language'] = $userLanguage;
$arrJSON = @GetLanguageFile($theRepository);
$strJSONLanguage = (string) @$arrJSON['data'];
?>
<!DOCTYPE html>
<html>
	<head>
		<meta property="appviewport" name="viewport" content="width=device-width, user-scalable=no" />
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"><!--<meta http-equiv="x-ua-compatible" content="IE=10" />-->
		<title>KPIOnline Forms v6</title>
		
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<link type="text/css" rel="stylesheet" href="jquery.mobile-1.4.5/jquery.mobile-1.4.5<?=$strMinifiedFiles?>.css"/>
		<link type="text/css" rel="stylesheet" href="jquery.mobile-forms.css"/>
		<link rel="stylesheet" href="jquery_signaturepad/jquery.signaturepad.css"/>
      	<link rel="stylesheet" href="spinningwheel/spinningwheel.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="css/fileuploader.css" type="text/css" media="all"/>
		<link rel="stylesheet" href="js/jquery/demos.css" type="text/css"/>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<!-- solo se cambio de ui a jquery-ui-1.11.4 -->
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.autocomplete.css">
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.theme.css">
		<link rel="stylesheet" href="js/chosen/chosen.css" />
		<link type="text/css" rel="stylesheet" href="css/inline.css"/>
		<link type="text/css" rel="stylesheet" href="css/table.css"/>
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx<?=$strMinifiedFiles?>.css"/>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX<?=$strMinifiedFiles?>.css"/>
		<link href="fonts/fontawesome/css/fontawesome.css" rel="stylesheet">
		<link href="fonts/fontawesome/css/brands.css" rel="stylesheet">
		<link href="fonts/fontawesome/css/solid.css" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="css/customOpenLayers.css">
		<link rel="stylesheet" type="text/css" href="css/defaultStyles.css"/>
		<!--JAPR 2019-02-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)-->
		<link rel="stylesheet" href="css/openlayers/ol.css" type="text/css">
		<style type="text/css" media="screen">
			/* Conchita 2014-10-29 agregada esta linea para q los dialogos no tengan btn de close */
			//AAL 18/05/2015 issue AJSLOR: No permitía mostrar el botón empleado para cerrar el componente comboBox, por lo tanto se comenta.
            //.ui-dialog  .ui-header .ui-btn-icon-notext  { display:none;} 
			//AAL
			.ui-page {
				-webkit-touch-callout: none;
				-webkit-user-select: none;
			}
			
			.ui-footer.ui-bar {
				padding-left: 0px;
				padding-right: 0px;
			}
			
			.mdRecSelected {
				border: 1px solid #155678;
				background: #4596ce;
				font-weight: bold;
				color: #fff !important;
				cursor: pointer;
				text-shadow: 0 -1px 1px #145072;
				text-decoration: none;
				background-image: -webkit-gradient(linear, left top, left bottom, from( #85bae4 ), to( #5393c5 ));
				background-image: -webkit-linear-gradient(#85bae4 , #5393c5 );
				background-image: -moz-linear-gradient(#85bae4 , #5393c5 );
				background-image: -ms-linear-gradient(#85bae4 , #5393c5 );
				background-image: -o-linear-gradient(#85bae4 , #5393c5 );
				background-image: linear-gradient(#85bae4 , #5393c5 );
				font-family: Helvetica, Arial, sans-serif;
			}
			@font-face { font-family: Roboto; src: url('css/font/Roboto-Regular.ttf'); } 	
		</style>
		
		<!-- Sólo para el App -->
		<!-- <script type="text/javascript" src="cordova.js"></script> -->
		<!-- <script type="text/javascript" src="cordova_plugins.js"></script> -->
		<!-- <script type="text/javascript" src="GCMPlugin.js"></script> --> <!-- Conchita, agregado GCM Cordova plugin -->
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<script type="text/javascript" src="js/jquery-1.11.3<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/jquery/jquery-ui-1.11.4/jquery-ui<?=$strMinifiedFiles?>.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<script type="text/javascript" src="jquery.mobile-1.4.5/jquery.mobile-1.4.5<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="jquery_signaturepad/flashcanvas.js"></script>
		<script type="text/javascript" charset="utf-8" src="js/jpeg_encoder_basic.js"></script>
		<!--1-Dic-2014 se cambio x el sin minificar para modificar el comportamiento del mismo -->
		<!--OMMC 2015-12-04: Agregado librería de moment para las validaciones de fecha de las preguntas OCR-->
		<script type="text/javascript" src="js/moment-with-locales<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="jquery_signaturepad/jquery.signaturepad<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="spinningwheel/spinningwheel.js"></script>
		<script type="text/javascript" src="js/FileSaver/FileSaver.js"></script>
		<script type="text/javascript" src="js/BlobBuilder/BlobBuilder.js"></script>
		<script type="text/javascript" src="js/fileuploader<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/dateshtm.js"></script>
		<!-- JAPR 2019-03-25: Agregado el nuevo tipo de pregunta AR (#4HNJD9) -->
		<script type="text/javascript" src="js/underscore-min.js"></script>
		<script>
		$(document).bind("mobileinit", function() {
			$.mobile.defaultTransition = 'none';
		});
		</script>
        <script type="text/javascript" src="BITAMXMLObject.js"></script>
		<script type="text/javascript" src="js/utils<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/filemanager<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/gpsmanager<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/mobileDetection.js"></script>
		<script type="text/javascript" src="js/appConstants.js"></script>
		<script type="text/javascript" src="js/openLayersSketch<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="eSurveyServiceMod<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="eSurveyServiceModFns<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/chosen/chosen.jquery<?=$strMinifiedFiles?>.js"></script>
		<script type="text/javascript" src="js/defaultLanguage.js"></script>
		<script type="text/javascript" src="js/masonry/masonry.pkgd.min.js"></script>
		<script src="js/codebase/dhtmlxFmtd<?=$strMinifiedFiles?>.js"></script>
        <!--<script type="text/javascript" charset="utf-8" src="PushNotification.js"></script>-->	<!-- JAPR no se requieren en código que no es del App -->
        <link type="text/css" rel="stylesheet" href="css/stylepanel.css"/>
        <script type="text/javascript" src="js/jquery.jpanelmenu.js"></script>
        <!--<script type="text/javascript" src="jquery.touchSwipe.js"></script> <!--touch events supported -->	<!-- JAPR no se requieren en código que no es del App -->
		<!--JAPR 2019-02-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)-->
		<script type="text/javascript" src="js/openlayers/ol.js"></script>
		<script language="JavaScript">
			var jPM;
			
			/* Crea la instancia del App de eForms, configurandolo para el tipo de vista default y en modo de diseño
			*/
			function startUpDesignApp() {
				Debugger.register('startUpDesignApp');
				gbDesignMode = true;
				gbApplyFlowLogic = false;
				try {
					objApp = new ApplicationCls();
				} catch(e) {
					throwError(-1, translate("Failed to create the eForms application instance") + ": " + e, translate("Error initializing app"), 'startUpDesignApp');
					return;
				}
				if (!objApp) {
					throwError(-1, translate("Failed to create the eForms application instance"), translate("Error initializing app"), 'startUpDesignApp');
					return;
				}
				
				//Inicializa la aplicación
				objApp.init();
				
				//Carga la ventana inicial de logín o hace un autologín según el caso
				if (objSettings.webMode || gbDesignMode) {
					//JAPR 2012-11-28: Corregido el protocolo de acceso desde el browser
					//Si se trata del modo en browser, hay que revisar si está en modo seguro o no
					if (window && window.location && window.location.protocol) {
						if (objSettings) {
							objSettings.protocol = window.location.protocol;
						}
					}
					
					objSettings.esurveyFolder = "<?=GetBaseURLPath(true, false)?>";
					//JAPR
				}
				
				//Inicializa los objetos globales (ApplicationCls.getLocalDefinitionsVersion)
				objeObjects = new Object();
				objeSurvey = new CollectionCls();
				objeAgenda = new CollectionCls();
				objeStatusAgenda = new CollectionCls();
				objeCatalog = new CollectionCls();
				objeDrafts = new CollectionCls();
				objeOutbox = new CollectionCls();
				objeMenu = new CollectionCls();
				objSettings.tempUser = "<?=addslashes($strUserEMail)?>";
				objSettings.user = objSettings.tempUser;
				//JAPR 2016-07-07: Almacenar el password encriptado (#VRNM0H)
				objSettings.tempPassword = "<?=addslashes($strUserPwd)?>";
				//objSettings.tempPassword = "<?=addslashes(base64_encode(rawurlencode($strUserPwd)))?>";
				objSettings.password = objSettings.tempPassword;
				objSettings.passwordEnc = "<?=addslashes(BITAMEncode(BITAMEncryptPassword(($strUserPwd))))?>";
				//JAPR 2016-07-21: Agregadas variables faltantes soportadas por el App
				objSettings.userLogin = "<?=addslashes($strUserShortName)?>";
				//Se desactiva esta opción para que no intente refresh de definiciones antes de mostrar las formas
				objSettings.refreshDefinitionsBeforeEntry = false;
				//Asigna la configuración default como Browser
				objSettings.customWidth = "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["Default"]))?>";
				objApp.addFilesRead(undefined, true);
				objApp.getNewDefinitionsVersion(JSON.parse("<?=addslashes($strJSONDefinitionsVersions)?>"));
				objApp.getNewDefinitions(JSON.parse("<?=addslashes($strJSONDefinitions)?>"));
<?
				if (trim($strJSONLanguage) != '') {
?>
				$.extend(arrPhrases, JSON.parse("<?=addslashes($strJSONLanguage)?>"));
<?
				}
?>
				objSettings.backgroundImage = "";
				jPM = new Object();
				jPM.close = function() {};
			}
			
			/* Invoca la inicialización del App de eForms y la interface del diseñador
			*/
			function doOnLoad() {
				if (startUpDesignApp && Function == startUpDesignApp.constructor) {
					startUpDesignApp();
				}
				else {
					alert('Failed to load KPIOnline Forms library');
					return;
				}
			}
		</script>
	</head>
	
	<body onload="doOnLoad();">
		<div data-role="page" data-url="emptyPage" id="emptyPage">
		</div>
	</body>
</html>
