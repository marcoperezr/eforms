<?php

define('ADODB_ASSOC_CASE', 0);
//Constantes para los tipos de objeto utilizados en el método BITAMRepository->addPhPCacheRequest
define('MDOBJECT_Cube', 0);
define('MDOBJECT_User', 1);
define('MDOBJECT_UserGroup', 2);
define('MDOBJECT_IndicatorGroup', 3);
define('MDOBJECTDATA_All', 0);

require_once('adodb/adodb.inc.php');

global $ADODB_FETCH_MODE;

$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;

$BITAMDBTypes = array();
$BITAMDBTypes['SQL SERVER'] = 1;
$BITAMDBTypes['SQL ANYWHERE'] = 2;
$BITAMDBTypes['ORACLE'] = 3;
$BITAMDBTypes['FOXPRO'] = 6;
$BITAMDBTypes['PARADOX'] = 7;
$BITAMDBTypes['REDBRICK'] = 8;
$BITAMDBTypes['ACCESS'] = 10;
$BITAMDBTypes['INFORMIX'] = 11;
$BITAMDBTypes['PROGRESS'] = 12;
$BITAMDBTypes['DB2'] = 16;
$BITAMDBTypes['SYBASE'] = 19;
$BITAMDBTypes['EXCEL'] = 24;

require_once('utils.inc.php');
require_once("httprequest.inc.php");
require_once('globalInstance.inc.php');
require_once('globalformsinstance.inc.php');

class BITAMRepository
{
	public $RepositoryName;
	public $ADODBDriver;
	public $ADODBServer;
	public $ADODBUser;
	public $ADODBPassword;
	public $ADODBDatabase; 
	public $ADOConnection;
	public $ADODataDictionary;
	public $ArtusWebHtmlURL;

	function __construct($aRepositoryName)
	{
		$this->RepositoryName = $aRepositoryName;
		$this->ADODBDriver = "odbc"; // "mssql" or "oci8" or "odbc";
		$this->ADODBServer = "";
		$this->ADODBUser = "";
		$this->ADODBPassword = "";
		$this->ADODBDatabase = ""; // "database name" (mssql) or "service name" (oci8)
		$this->ADOConnection = null;
		$this->ADODataDictionary = null;
		$this->ArtusWebHtmlURL = "";
		
		$this->DataADODBDriver = ""; // "mssql" or "oci8" or "odbc";
		$this->DataADODBServer = "";
		$this->DataADODBUser = "";
		$this->DataADODBPassword = "";
		$this->DataADODBDatabase = ""; // "database name" (mssql) or "service name" (oci8)
		$this->DataADOConnection = null;
	}
	
	static function NewInstance($aRepositoryName)
	{
		return new BITAMRepository($aRepositoryName);
	}
		
	//@JAPR 2013-02-26: Agregada por compatibilidad con eBavel
	static function NewInstanceWithID($aRepositoryName)
	{
		$anInstance = null;
		
		$allRepositories = self::LoadAllRepositories();
				
		if (array_key_exists($aRepositoryName, $allRepositories))
		{
			$anInstance = $allRepositories[$aRepositoryName];
		}
		
		return $anInstance;
	}
	
	static function LoadAllRepositories()
	{
		require_once("ektrepository.inc.php");
		return BITAMEKTRepository::loadBITAMRepositories();
	}
	//@JAPR
	
	//@JAPR 2013-02-26: Agregado el parámetro $forceNew para forzar a una nueva conexión de repositorio en cada instancia (para el DWH siemre se hace)
	function open($forceNew = false)
	{
		$this->close();
		ob_start();
		$anADOConnection = NewADOConnection($this->ADODBDriver);
		//@JAPR 2013-02-26: Agregado el parámetro $forceNew para forzar a una nueva conexión de repositorio en cada instancia (para el DWH siemre se hace)
		$anADOConnection->Connect($this->ADODBServer, $this->ADODBUser, $this->ADODBPassword, $this->ADODBDatabase, $forceNew);
		$errorMessage = ob_get_contents();
		ob_end_clean();
		if (!$anADOConnection->IsConnected())
		{
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			@error_log("\r\n--------------------\r\n".date("Y-m-d H:i:s").$errorMessage, 3, (GeteFormsLogPath().$this->RepositoryName.".log"));
			//@JAPR
			return false;
		}
		// $this->ADODataDictionary = NewDataDictionary($this->ADOConnection);

		if ($this->DataADODBDriver == "")
		{
			$aDataADOConnection = $anADOConnection;
		}
		else
		{	ob_start();
			$aDataADOConnection = NewADOConnection($this->DataADODBDriver);
			$aDataADOConnection->Connect($this->DataADODBServer, $this->DataADODBUser, $this->DataADODBPassword, $this->DataADODBDatabase, true);
			$errorMessage = ob_get_contents();
			ob_end_clean();
			if (!$aDataADOConnection->IsConnected())
			{
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				@error_log("\r\n--------------------\r\n".date("Y-m-d H:i:s").$errorMessage, 3, (GeteFormsLogPath().$this->RepositoryName.".log"));
				//@JAPR
				return false;
			}
		}
		
		$this->ADOConnection = $anADOConnection;
		$this->DataADOConnection = $aDataADOConnection;
    
    //@JAPR 2015-09-18: Corregido el acceso a la base de datos para forzar a que siempre sea utf8 (código original de ARamirez en
    //eBavel donde se identificó el problema original cuando el MySQL de la BD no está configurado para utf8)
    if (version_compare(phpversion(), '5.6.0', '>=') )
    {
     /* Le indicamos a las conexiones que usaremos UTF8 (link http://stackoverflow.com/a/2159453) */
     $this->ADOConnection->Execute("set names 'utf8'");
     $this->DataADOConnection->Execute("set names 'utf8'");
    }
    
		if ($this->ADODBDriver == "mssql")
		{
			$sql = "SET DATEFORMAT ymd";
			if ($this->ADOConnection->Execute($sql) === false)
			{
				$errorMessage = $this->ADOConnection->ErrorMsg()." ==> ".$sql;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				@error_log("\r\n--------------------\r\n".date("Y-m-d H:i:s").$errorMessage, 3, (GeteFormsLogPath().$this->RepositoryName.".log"));				
				//@JAPR
			}
		}

		//@JAPR 2012-09-05: Agregada la lectura de la versión de Metadata de eForms
		//Sólo verifica la versión si aun no estuviera en la variable de sesión
		//@JAPR 2012-10-31: Por alguna razón cuando desde el App se invocaba a saveAnswersMobile2, al leer de la sesión ya venía esta variable
		//asignada con un 0 aun sin haber pasado por esta parte, así que mejor se quitó la validación para siempre reasignar esta variable
		//if (!isset($_SESSION["PAeFormsMDVersion"])) {
			$dbleFormsMDVersion = 0.0;
			$sql = 'SELECT TIPO_PRODUCTO, MAX(VERSION) AS Version '.
					'FROM SI_MD_VERSION '.
					'WHERE TIPO_PRODUCTO IN (6) '.
					'GROUP BY TIPO_PRODUCTO';
			$aRS = $this->ADOConnection->Execute($sql);
			if ($aRS) {
				while (!$aRS->EOF)
				{
					switch ($aRS->fields["tipo_producto"])
					{
						case 6:		//eForms
							$dbleFormsMDVersion = (float) $aRS->fields["version"];
							$dbleFormsMDVersion = round($dbleFormsMDVersion, 5);
							break;
					}
					$aRS->MoveNext();
				}
			}
			
			//Escribe la variable en la sesión para que esté disponible en todos lados y no se vuelva a cargar de la metadata
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//@session_register("PAeFormsMDVersion");
			@$_SESSION["PAeFormsMDVersion"] = $dbleFormsMDVersion;
		//}
		//@JAPR
		
		return true;
	}
	
	function close()
	{
		if (!is_null($this->DataADOConnection))
		{
			$this->DataADOConnection->Close();
		}
		$this->DataADOConnection = null;
		if (!is_null($this->ADOConnection))
		{
			$this->ADOConnection->Close();
		}
		$this->ADOConnection = null;
		$this->ADODataDictionary = null;
	}

	function PerformHTTPRequest($aHTTPRequest, $aUser)
	{
		if (array_key_exists("BITAM_PAGE", $aHTTPRequest->GET))
		{
			$aClassName = $aHTTPRequest->GET["BITAM_PAGE"];
			require_once(strtolower($aClassName).".inc.php");
			return eval('return BITAM'.$aClassName.'::PerformHTTPRequest($this, $aHTTPRequest, $aUser);');
		}
		if (array_key_exists("BITAM_SECTION", $aHTTPRequest->GET))
		{
			$aCollectionName = $aHTTPRequest->GET["BITAM_SECTION"];
			$aClassName = substr($aCollectionName, 0, -10);
			require_once(strtolower($aClassName).".inc.php");
			return eval('return BITAM'.$aCollectionName.'::PerformHTTPRequest($this, $aHTTPRequest, $aUser);');
		}

		require_once("survey.inc.php");

		return BITAMSurveyCollection::NewInstance($this,null);
	}
	
	function get_Title()
	{
		return translate("Home");
	}
	
	function get_QueryString()
	{
		return "";
	}

	function get_Parent()
	{
		return null;
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();
		
		return $myChildren;
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function generateForm($aUser)
	{
		require_once("formfield.inc.php");
		$children = $this->get_Children($aUser);
		foreach ($children as $child)
		{
			$child->generateDetailForm($aUser);
		}
	}
	
	/* Regresa el arreglo con la estructura de la tabla especificada
		El parámetro $bDataADOConnection indica si el MetaColumns se solicita a la conexión de la fuente (DataADOConnection) o
	a la del repositorio (ADOConnection)
	*/
	function GetTableSchema($aTableName,$bDataADOConnection=false)
	{
		$metaColumns = BITAMGlobalInstance::GetTableSchema($aTableName,$bDataADOConnection);
		if (!is_null($metaColumns))
		{
			return $metaColumns;
		}
		
		if ($bDataADOConnection)
		{
			$metaColumns = $this->DataADOConnection->MetaColumns($aTableName);
		}
		else 
		{
			$metaColumns = $this->ADOConnection->MetaColumns($aTableName);
		}
		
		BITAMGlobalInstance::AddTableSchema($aTableName,$bDataADOConnection,$metaColumns);
		return $metaColumns;
	}
	
	/*Agrega un registro a la tabla de solicitudes de generación de Cache de PhP de Ektos, para que el proceso que genera los archivos
	PhP de cache de Web Gen VI solo considere los elementos que se encuentren en esta tabla para la generación. 
	Los valores posibles del parámetro $iObjectType son:
		0 = Cubos
		1 = Usuarios
		2 = Grupos de usuarios
		3 = Grupos de Indicadores
		4 = Cubos
	Los valores posibles del parámetro $iCacheType son dependientes del $iObjectType, por lo pronto el único valor posible será:
		0 = Todo (dependiendo del tipo de objeto, las tablas exportadas serán:
			0 (Cubos) -> SI_CONCEPTO, SI_CPTO_LLAVE, SI_CPTO_ATRIB, SI_INDICADOR, SI_DESCRIP_ENC, SI_DESCRIP_DET, SI_AGREGACIONES, SI_AG_NEW, SI_PERIODOSxCUBO, SI_JERARQUIA (y las que falten que sean de cubos)
			1 (Usuarios) -> SI_USUARIO, SI_USR_IND, SI_USR_CONFIG (SI_ROL_USUARIO se omite porque esa tabla se exportará a nivel Grupo de usuario) (y las que falten que sean de usuarios)
			2 (Grupos de usuario) -> SI_ROL, SI_ROL_IND, SI_ROL_CONFIG, SI_ROL_USUARIO
	*/
	function addPhPCacheRequest($iObjectType, $iObjectKey, $iCacheType = 0)
	{
		$sql = 'INSERT INTO SI_EKT_PhPCacheMD (EKTObjectType, EKTObjectKey, EKTCacheType, EKTModificationDate) '.
				'VALUES ('.$iObjectType.','.$iObjectKey.','.$iCacheType.','.$this->ADOConnection->DBTimeStamp(date("Y-m-d H:i:s")).')';
				
		$this->ADOConnection->Execute($sql);
	}
	
	function getDataADODBMSNameByRepository($aRepository)
	{
		$dataDBMSName = "";
		
 		//Establecer cual es el manejador de base de datos que se esta utilizando
 		//si DataADODBDriver es diferente de vacio entonces si hay un 
 		//repositorio de datos separado de la repositorio de la metadata
 		if(trim($aRepository->DataADODBDriver)!="")
 		{
 			//Se obtiene el driver que se esta utilizando en la conexion
 			$driver = $aRepository->DataADODBDriver;
 			
 			//Si es odbc, se tiene q buscar el manejador de base de datos
 			//al cual hace referencia
 			if($driver=="odbc")
 			{
 				//Se obtiene el manejador de base de datos
 				$dbmsName = $aRepository->DataADOConnection->dbmsName();
 				
 				//Se verifica si es MS SQL Server
 				if(substr($dbmsName, 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
				    substr($dbmsName, 0, strlen('SQL SERVER')) == 'SQL SERVER' ||
				    strpos($dbmsName, 'NATIVE CLIENT') !== false)
 				{
 					$dataDBMSName = "mssql";
 				}
 				else if(stripos($dbmsName, 'ORACLE')!==false)
 				{
 					$dataDBMSName = "oracle";
 				}
 				else if(stripos($dbmsName, 'MYSQL')!==false)
 				{
 					$dataDBMSName = "mysql";
 				}
 				else 
 				{
 					$dataDBMSName = $aRepository->DataADOConnection->databaseType;
 				}
 			}
 			else 
 			{
 				$dataDBMSName = $driver;
 			}
 		}
 		else 
 		{
 			//Se obtiene el driver que se esta utilizando en la conexion
 			$driver = $aRepository->ADODBDriver;
 			
 			//Si es odbc, se tiene q buscar el manejador de base de datos
 			//al cual hace referencia
 			if($driver=="odbc")
 			{
 				//Se obtiene el manejador de base de datos
 				$dbmsName = $aRepository->ADOConnection->dbmsName();
 				
 				if(substr($dbmsName, 0, strlen('MICROSOFT SQL SERVER')) == 'MICROSOFT SQL SERVER' ||
				    substr($dbmsName, 0, strlen('SQL SERVER')) == 'SQL SERVER' ||
				    strpos($dbmsName, 'NATIVE CLIENT') !== false)
 				{
 					$dataDBMSName = "mssql";
 				}
 				else if(stripos($dbmsName, 'ORACLE')!==false)
 				{
 					$dataDBMSName = "oracle";
 				}
 				else if(stripos($dbmsName, 'MYSQL')!==false)
 				{
 					$dataDBMSName = "mysql";
 				}
 				else 
 				{
 					$dataDBMSName = $aRepository->ADOConnection->databaseType;
 				}
 			}
 			else 
 			{
 				$dataDBMSName = $driver;
 			}
 		}
 		
 		return $dataDBMSName;
	}
}

require_once("object.inc.php");
require_once("user.inc.php");
?>