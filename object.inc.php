<?php

require_once("repository.inc.php");

class BITAMObject
{
	public $Repository;
	public $Mode;
	public $Message;
	public $HasMenu;
	public $HasPath;
	public $IsDialog;
	public $NewSection;
	public $FieldSection;
	public $HasAutoKeyGeneration;
	public $GroupImage;
	public $MsgConfirmRemove;
	public $StyleCellTitle;
	//@JAPR 2015-01-21: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
	//si es utilizado y advertir al usuario (#75TPJE)
	//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
	//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
	//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
	//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
	public $enableRemove;
	//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
	//Marzo 2015: Para configurar si en la pantalla del objeto se muestra el botón de Grabar e ir al padre
	public $HasSaveAndGoParent;
	//Marzo 2015: Para configurar si en la pantalla del objeto se muestra el título de la forma
	public $HasFormTitle;
	//Marzo 2015: Para configurar si en la pantalla del objeto se muestra la línea horizontal
	public $HasHr;
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	public $HasCancel;						//Indica si se debe o no mostrar el botón de Cancelar (si se oculta, siempre debería estar en modo edición)
	public $HasEdit;						//Indica si se debe o no mostrar el botón de Edición (si se oculta, automáticamente entrará en modo edición al cargar el detalle)
	
	const MODE_VIEW = 'view';
	const MODE_EDIT = 'edit'; 

	function __construct($aRepository)
	{
		$this->Repository = $aRepository;
		$this->Mode = self::MODE_VIEW;
		$this->Message = "";
		$this->HasMenu = false;
		$this->HasPath = true;
		$this->IsDialog= false;
		$this->NewSection= false;
		$this->FieldSection= false;
		$this->HasAutoKeyGeneration = false;
		$this->GroupImage = "images/group.gif";
		$this->MsgConfirmRemove = "";
		$this->StyleCellTitle = "";
		//@JAPR 2015-01-21: Validado que antes de eliminar algún tipo de elemento, se verifiquen los puntos donde se pueden aplicar fórmulas para buscar
		//si es utilizado y advertir al usuario (#75TPJE)
		//Con esta propiedad se forzará dinámicamente a bloquear la posibilidad de remover el objeto bajo alguna condición asignada en forma
		//externa a la propia instancia (útil cuando se tiene una colección que debe bloquear ciertas preguntas a partir de consultas y no
		//se quieren lanzar dichas consultas por cada instancia de los objetos de la colección, o bien si no se quiere ocultar el botón para
		//eliminar de la propia instancia de objeto sino controlar que sólo no se pueda eliminar desde la colección)
		$this->enableRemove = true;
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = true;
		$this->HasFormTitle = true;
		$this->HasHr = true;
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasCancel = true;
		$this->HasEdit = true;
	}

	function get_Image()
	{
		return "";
	}
	
	function insideEdit()
	{
	}

	function insideCancel()
	{
	}

	function get_FormIDFieldName()
	{
		return "";
	}
	
	function get_SupportFormIDFieldName()
	{
		return "";
	}
		
	function get_FormFields($aUser)
	{
		return array();
	}
	
	function get_FormEncType()
	{
	  return "application/x-www-form-urlencoded";
	}
	
	function GetFieldNamesToReturn()
	{
		return array();
	}
	
	
	function GetNewID()
	{
		return -1;
	}
	
	function isNewObject()
	{
		return true;
	}
	
	function selectionButtons($aUser)
	{
	}	

	function addButtons($aUser)
	{
		
	}
	
	function get_Parent()
	{
		return null;
	}
	
	function get_Children($aUser)
	{
		return array();
	}

	function canRemove($aUser)
	{
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	function hideBackButton($aUser)
	{
		return false;
	}

	function canEdit($aUser)
	{
		return true;
	}
	
	function get_customTdClass($aFieldName)
	{
	}
	
	function get_FieldValueOf($aFieldName)
	{
		if (strpos($aFieldName, '[') !== false && strpos($aFieldName, ']') !== false)
		{
			$strTempFieldName = $aFieldName;
			//$strTempFieldName = str_replace(chr(92).chr(39), chr(39).'.chr(92).chr(39).'.chr(39), $strTempFieldName);
			$strTempFieldName = str_replace('['.chr(39), '[chr(39).'.chr(39), $strTempFieldName);
			$strTempFieldName = str_replace(chr(39).']', chr(39).'.chr(39)]', $strTempFieldName);
			$aFieldValue = eval('return $this->'.$strTempFieldName.';');
		}
		else 
		{
			if (method_exists($this, $aFieldName))
	    	{
	    		$aFieldValue = $this->$aFieldName();
	    	}
	    	else
	    	{
                $aFieldValue = (isset($this->$aFieldName) ? $this->$aFieldName : '');
	    	}
		}
		return $aFieldValue;
	}
	
	function generateForm($aUser)
	{
		if ($this->HasMenu)
		{
			include("menu_div.php");
		}
		if ($this->HasPath)
		{
			include("path_div.php");
		}

		if($this instanceOf KPA_Object_Interface
			&& $this->canParseView($aUser)){
				
			$this->parse($aUser);
			
		} else {
		require_once("formfield.inc.php");
		$this->generateDetailForm($aUser);
		}
		
		$children = $this->get_Children($aUser);
		foreach ($children as $child)
		{
			echo "";
			$child->generateDetailForm($aUser);
		}
	}
	
	function generateBeforeFormCode($aUser)
	{
	}
	
	function generateAfterFormCode($aUser)
	{
	}
	
    function generateCustomAnchorCode($aUser)
    {
    	
    }
	
	function generateInsideFormCode($aUser)
	{
	}
	
    function generateOnCancelFormCode($aUser)
    {
    }
    
	function insertPreviewRows($aUser)
	{
		
	}
	
	function generateDetailForm($aUser)
	{
		require_once("formfield.inc.php");
		
		global $widthByDisp;
		
    	$myFormName = get_class($this);
    	$myFormTitle = $this->get_Title();
 		$myFormIDFieldName = $this->get_FormIDFieldName();
    	$myFormFields = $this->get_FormFields($aUser);
    	//HTML Designer
    	//Si algún campo utilizará el diseñador HTML entonces se incluyen las librerías
    	$includeHTMLDes = false;
	    foreach ($myFormFields as $aField)
		{
			if($aField->UseHTMLDes)
			{
				$includeHTMLDes=true;
				break;
			}
		}

?>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/<?=$_SESSION["PAuserLanguage"]?>.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/genericComponents.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/components.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/menucomponent.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/menu.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/dialogs.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/EditorHTML/move_resize.js"></script>
		<link rel="stylesheet" type="text/css" href="css/menu.css">

		<script language="javascript">
		var visibleDivArray = new Array();
		</script>

	<div class="object" id="<?= $myFormName ?>">
	 	<script language="javascript">
			var cancel = false;
		</script>
<?
		$this->generateBeforeFormCode($aUser);
?>
	    <script language="javascript">
<?
	    foreach ($myFormFields as $aField)
		{
			if (!is_null($aField->Parent))
			{
?>
	        var <?= $myFormName ?>_<?= $aField->Name ?> = new Array();
<?
				foreach ($aField->Options as $owner => $values)
				{
			    	if ($aField->Type == "Object" || $aField->Type == "Array")
			    	{
?>
            <?= $myFormName ?>_<?= $aField->Name ?>['<?= $owner ?>'] = [
<?
						$first = true;
						foreach ($values as $key => $value)
						{
?>
			<?= $first ? "" : "," ?>['<?= $key ?>', '<?= str_replace("'", "\'", $value) ?>']
<?
							$first = false;
						}
?>
				];
<?
			    	}
			    	else
			    	{
?>
            <?= $myFormName ?>_<?= $aField->Name ?>['<?= $owner ?>'] = '<?= str_replace("'", "\'", $values) ?>';
<?
			    	}
				}
			}
		}
		if (!$this->IsNewObject())
 		{
?>
   	        function <?= $myFormName ?>_Remove()
	        {
<?
	        	$myFormTitleAux = str_replace("\\", "\\\\", $myFormTitle);
	        	$myFormTitleAux = str_replace("'", "\'", $myFormTitleAux);
	        	
	        	if($this->MsgConfirmRemove!="")
	        	{
?>
	        	var answer = confirm('<?=translate($this->MsgConfirmRemove)?>');
<?
	        	}
	        	else 
	        	{
?>
	        	var answer = confirm('<?= sprintf(translate("Do you want to remove this %s?"), $myFormTitleAux) ?>');
<?
	        	}
?>
				if (answer)
				{
                	<?= $myFormName ?>_RemoveForm.submit();
				}
			}
<?
		}
?>
 			function <?= $myFormName ?>_Edit()
			{
				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
 					var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}
				
				var elements = <?= $myFormName ?>_SaveForm.elements;
				var firstElement = null;
				var tinyEditorCreated = false;
				for (var i = 0; i < elements.length; i++)
				{
					if (firstElement == null && elements[i].type !== 'hidden' && elements[i].id !== '1')
					{
						firstElement = elements[i];
					}
					if (elements[i].id != '1')
					{
						elements[i].disabled = false;
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							elements[i].className = strCustomizedClass;
						}
						else {
							elements[i].className = '';
						}
					}
					
					try
					{
						var tinyEditor = null;
						var strID = elements[i].id;
						if (strID !== null && strID && isNaN(strID)) {
							tinyEditor = tinymce.get(elements[i].id);
						}
						if(tinyEditor && !tinyEditorCreated)
						{
							if(document.getElementById(elements[i].id+'_toolbargroup') == undefined) {
								tinyEditor.remove();
								tinyEditor.settings.readonly = 0;
								tinyEditor.init();
								tinyEditorCreated = true;
							}
						}
					}
					catch(err)
					{
					}
				}
				
				var images = document.images;
				var length=images.length;
				var showImg;
				for (var i = 0; i < length; i++)
				{
					showImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 0)
						{
							showImg = false;
						}
					}

					if (showImg)
					{
						images[i].style.display='inline';
					}

				}
				
<?
		foreach ($myFormFields as $aField)
    	{
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'inline';
<?
						break;
				}
			}
		}
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->SubmitOnChange)
    		{
?>
    			<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value;
<?
    		}
    	}
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->OnChange != "")
    		{
?>
				<?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
    		}
    		$aFieldIdx++;
    	}
?>
			if(typeof(<?= $myFormName ?>_OkSelfButton) == 'undefined'){
					var <?= $myFormName ?>_OkSelfButton = document.getElementById('<?= $myFormName ?>_OkSelfButton');
			}
			
			if(typeof(<?= $myFormName ?>_CancelButton) == 'undefined'){
					var <?= $myFormName ?>_CancelButton = document.getElementById('<?= $myFormName ?>_CancelButton');
			}
			
			if(typeof(<?= $myFormName ?>_OkParentButton) == 'undefined'){
					var <?= $myFormName ?>_OkParentButton = document.getElementById('<?= $myFormName ?>_OkParentButton');
			}

				if (firstElement !== null)
				{
					//firstElement.focus();
				}
				<?= $myFormName ?>_OkSelfButton.style.display = 'inline';
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'inline';
<?
			}
	    }
?>
<?
		if ($this->isNewObject())
		{
?>
<?
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'inline';
<?
		    }
?>
<?
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'inline';
<?
		}
				$this->insideEdit();
?>
			}
			
			function <?= $myFormName ?>_Ok(target)
			{
				
<?
	foreach ($myFormFields as $aField)
    	{
    		if ($aField->IsDisplayOnly)
    		{
    				if($aField->Type == "Array")
    				{
?>
						var items = GetElementsByName(<?= $myFormName ?>_SaveForm, "<?= $aField->Name ?>[]");
						for (var i = 0; i < items.length; i++)
						{
							items[i].disabled = false;
						}
<?
    				}
					else 
					{
?>
				<?= $myFormName ?>_SaveForm.elements.namedItem("<?= $aField->Name ?>").disabled = false;
<?
					}
    		}
    	}
	foreach ($myFormFields as $aField)
    	{
    		if ($aField->Type == "Array")
    		{
?>
	        	var hasChecked = false;
	        	var field = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
				for (var i = 0; (hasChecked == false) && (i < field.length); i++)
				{
					if (field[i].checked)
					{
	        			hasChecked = true;
					}
	        	}
	        	if (hasChecked)
	        	{
	        		<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value = '0';
	        	}
	        	else
	        	{
	        		<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value = '1';
	        	}
	        	//alert(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>_EmptySelection.value);
<?
    		}
    		if ($aField->FormatMask != "")
    		{
				switch ($aField->Type)
				{
					case "String":
					case "Password":
					case "Integer":
					case "Float":
					case "Date":
					case "DateTime":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'];
<?
						break;
				}	
			}
		}

		if( $this->isNewObject()&&$this->IsDialog && array_key_exists($myFormIDFieldName,$myFormFields) && !$this->HasAutoKeyGeneration)
		{
?>
			if(Trim(<?= $myFormName ?>_SaveForm.<?= $myFormIDFieldName ?>.value) == "")
			{
					//Se require de return porq aunq cierre la ventana la ejecucion continua hasta q termina la funcion
					window.returnValue = null;
   					top.close();
   					return;
			}
			else
			{
					//Es un nuevo objeto en el q se puede modificar el ID
					<?= $myFormName ?>_SaveForm.go.value = target;
					<?= $myFormName ?>_SaveForm.submit();
			}
<?
		}
		else
		{
?>			
				//Es un nuevo objeto en el q no se muestra ID y por lo tanto no se puede modificar el ID
				<?= $myFormName ?>_SaveForm.go.value = target;
				<?= $myFormName ?>_SaveForm.submit();
<?		
		}
		
    	foreach ($myFormFields as $aField)
    	{
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'none';
<?
						break;
				}
			}
		}
?>
				var elements = <?= $myFormName ?>_SaveForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
					if (elements[i].id != '2')
					{
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							strCustomizedClass = ' ' + strCustomizedClass;
						}
						else {
							strCustomizedClass = ' ';
						}
						elements[i].className = 'disabled' + strCustomizedClass;
						elements[i].disabled = true;
					}
				}
				
				var images = document.images;
				var length=images.length;
				var hideImg;
				for (var i = 0; i < length; i++)
				{
					hideImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 1)
						{
							hideImg = false;
						}
					}
					if (hideImg)
					{
						images[i].style.display='none';
					}
				}
				
				
				<?= $myFormName ?>_OkSelfButton.style.display = 'none';
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'none';
<?
			}
	    }
?>
<?
		if ($this->isNewObject())
		{
?>
<?
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'none';
<?
	    	}
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'none';

<?		}
		if($this->IsDialog)
	    {		
	    		$fieldsToReturn=$this->GetFieldNamesToReturn();
	    		$NewID=$this->GetNewID();
	    		
?>				
				<?= $myFormName ?>_arrayToReturn=new Array;
				<?= $myFormName ?>_NewID=<?=$NewID?>;
<?				
				for($iField=0;$iField<count($fieldsToReturn);$iField++)
				{
					
						if($fieldsToReturn[$iField]==$myFormIDFieldName&&$this->isNewObject()&&!array_key_exists($fieldsToReturn[$iField],$myFormFields))
						{
	?>					
						<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_NewID;
	<?
						}
						else
						{ 
							if(array_key_exists($fieldsToReturn[$iField],$myFormFields))
							{
								if($myFormFields[$fieldsToReturn[$iField]]->VisualComponent=="Radio")
								{
?>
							lengthRadio = <?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>.length;
							
							for(i=0; i<lengthRadio; i++)
							{
								if(<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[i].checked)
								{
							<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[i].value;
									break;
								}
							}
<?
								}
								else if($myFormFields[$fieldsToReturn[$iField]]->VisualComponent=="Checkbox" && $myFormFields[$fieldsToReturn[$iField]]->Type=="Boolean")
								{
?>									//Como es un dato tipo checkbox booleano se guardan el valor un arreglo de 2 elementos
									//el cual el primero campo (campo[0]) es un elemento tipo hidden y el segundo campo 
									//(campo[1]) es un elemento tipo checkbox
									//Cuando el campo[1] esta marcado entonces se toma su valor de dicho campo
									if(<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[1].checked)
									{
										<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[1].value;
									}
									else
									{
										//En caso contrario el valor se toma del campo[0]
										<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>[0].value;
									}
<?
								}
								else 
								{
	?>
							<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?= $myFormName ?>_SaveForm.<?=$fieldsToReturn[$iField]?>.value;
						
	<?
								}						
							}
							else 
							{
								eval('$valueField=$this->'.$fieldsToReturn[$iField].";");
	?>
								
								<?= $myFormName ?>_arrayToReturn["<?=$fieldsToReturn[$iField]?>"]=<?=$valueField?>;
	<?
							}
						}
				}
?>				
 				//top.returnValue = <?= $myFormName ?>_arrayToReturn;
 				window.top.returnValue = <?= $myFormName ?>_arrayToReturn;
   				//top.close();
<?
	    }
?>
			}
			
			function <?= $myFormName ?>_Cancel(user_click)
			{
				if(typeof(user_click) == 'undefined'){
					user_click = false;
				} else {
				}				
				
				cancel = true;
				
				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
 					var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}
				
				<?= $myFormName ?>_SaveForm.reset();
<?
                $this->generateOnCancelFormCode($aUser);
?>
				
<?
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->FormatMask != "")
    		{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.defaultValue;
<?
				switch ($aField->Type)
				{
					case "String":
					case "Password":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatString(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
<?
						break;
					case "Integer":
					case "Float":
?>

				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.style.textAlign = 'right';
<?
				if($aField->DefaultNumberZero == true)
				{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
<?
				}
				else
				{
?>
					if(Trim(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value)!='')
					{
					<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');
					}
					else
					{
						<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = '';
					}
<?
				}
						break;
						
					case "Date":
					case "DateTime":
?>

				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'], '<?= $aField->FormatMask ?>');

<?
						break;
				}
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.onblur();

<?
			}
    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    		{
				switch ($aField->Type)
				{
					case "Date":
					case "DateTime":
?>
				document.images['calendar_<?= $myFormName ?>_<?= $aField->Name ?>_img'].style.display = 'none';
<?
						break;
				}
			}
		}
		
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		if ($aField->OnChange != "")
    		{
?>
				<?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
    		}
    		$aFieldIdx++;
    	}
?>

				if(typeof(<?= $myFormName ?>_SaveForm) == 'undefined'){
						var <?= $myFormName ?>_SaveForm = document.getElementById('<?= $myFormName ?>_SaveForm');
				}

				var elements = <?= $myFormName ?>_SaveForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
					if (elements[i].id != '2')
					{
						var strCustomizedClass = elements[i].getAttribute('customizedClass');
						if (strCustomizedClass) {
							strCustomizedClass = ' ' + strCustomizedClass;
						}
						else {
							strCustomizedClass = ' ';
						}
						elements[i].className = 'disabled' + strCustomizedClass;
						elements[i].disabled = true;
					}
				}
				
				var images = document.images;
				var length=images.length;
				var hideImg;
				for (var i = 0; i < length; i++)
				{
					hideImg = true;
					attrValue = images[i].getAttribute('displayMe');
					if (attrValue != null && !isNaN(attrValue))
					{
						if (parseInt(attrValue) == 1)
						{
							hideImg = false;
						}
					}
					if (hideImg)
					{
						images[i].style.display='none';
					}
				}
				
				if(typeof(<?= $myFormName ?>_OkSelfButton) == 'undefined'){
 					var <?= $myFormName ?>_OkSelfButton = document.getElementById('<?= $myFormName ?>_OkSelfButton');
				}

				if(typeof(<?= $myFormName ?>_CancelButton) == 'undefined'){
 					var <?= $myFormName ?>_CancelButton = document.getElementById('<?= $myFormName ?>_CancelButton');
				}
				
				if(typeof(<?= $myFormName ?>_OkParentButton) == 'undefined'){
 					var <?= $myFormName ?>_OkParentButton = document.getElementById('<?= $myFormName ?>_OkParentButton');
				}

				<?= $myFormName ?>_OkSelfButton.style.display = 'none';
				
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
				<?= $myFormName ?>_OkParentButton.style.display = 'none';
<?
			}
	    }

	    if ($this->isNewObject())
		{
		    if(!$this->IsDialog)
		    {
?>

				<?= $myFormName ?>_OkNewButton.style.display = 'none';
<?
		    }
		}
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		if($this->HasCancel)
		{
?>
				<?= $myFormName ?>_CancelButton.style.display = 'none';
                    
<?
		}
				$this->insideCancel();
?>

			}
			
			function <?= $myFormName ?>_CancelDialog()
			{
				window.returnValue = null;
   				top.close();
			}
<?
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		$aFieldName = $aField->Name;
    		if (is_null($aField->Parent))
    		{
    			$aFieldParentName = "";
    		}
    		else
    		{    		
    			$aFieldParentName = $aField->Parent->Name;
    		}
    		if ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange)
    		{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_OnChange()
	        {
<?
				//@JAPR 2012-05-30: Agregada la propiedad OnChangeBeforeUpdate porque en un caso especial de carga de datos dinámica, cambiar uno 
				//de los padres necesitaba limpiar el array de datos dinámicos debido a filtros dependientes del padre, así que si primero se
				//ejecutaba el Update de los FormFields dependientes, mostraba los datos ya cargados en lugar de ir al server por nuevos datos,
				//lo cual se logra precisamente limpiando el array primero (tiene que ver con la propiedad DynamicLoadFn)
				if (isset($aField->OnChangeBeforeUpdate) && $aField->OnChangeBeforeUpdate == true)
				{
		    		if ($aField->OnChange != "")
	    			{
?>
				var result = <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
	    			}
	    			else
	    			{
?>
				var result = true;
<?
	    			}
				}
				//@JAPR

				foreach ($aField->Children as $aChild)
				{
?>
            	<?= $myFormName ?>_<?= $aChild->Name ?>_Update();
<?
				}
				
				//@JAPR 2012-05-30: Agregada la propiedad OnChangeBeforeUpdate porque en un caso especial de carga de datos dinámica, cambiar uno 
				//de los padres necesitaba limpiar el array de datos dinámicos debido a filtros dependientes del padre, así que si primero se
				//ejecutaba el Update de los FormFields dependientes, mostraba los datos ya cargados en lugar de ir al server por nuevos datos,
				//lo cual se logra precisamente limpiando el array primero (tiene que ver con la propiedad DynamicLoadFn)
				if (!isset($aField->OnChangeBeforeUpdate) || $aField->OnChangeBeforeUpdate == false)
				{
		    		if ($aField->OnChange != "")
	    			{
?>
				var result = <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed();
<?
	    			}
	    			else
	    			{
?>
				var result = true;
<?
	    			}
				}
				//@JAPR
    			if ($aField->SubmitOnChange)
    			{
?>
    				if (<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] != <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value)
    				{
    					<?= $myFormName ?>_Ok('<?= $aField->Name ?>');
    				}
    				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value;
<?
    			}
?>
				return result;
			}
<?
    		}
    		if ($aField->OnChange != "")
    		{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_<?= $aFieldIdx ?>_Changed()
	        {
	            <?= $aField->OnChange."\n" ?>
			}
			
<?
    		}	
			if (!is_null($aField->Parent))
			{
?>
	        function <?= $myFormName ?>_<?= $aField->Name ?>_Update()
	        {
<?
		    	switch ($aField->Type)
		    	{
		    		case "Object":
		        		switch ($aField->VisualComponent)
		        		{
		            		case "Radio":
?>
	        	var me = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>;
	        	var mySelection = null;
	        	if (me != null)
	        	{
					for (var i = 0; i < me.length; i++)
					{
						if (me[i].checked)
						{
	        				mySelection = me[i].value;
						}
					}
	        	}
<?
								switch ($aField->Parent->Type)
								{
									case "Object":
			        					switch ($aField->Parent->VisualComponent)
			        					{
			            					case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
												break;
			            					case "Combobox":
			            					case "List":
			            					default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
												break;
			        					}
?>
	        	if (myParentSelection == null)
	        	{
   	        		var myNewValues = new Array();
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	if (myParent != null)
	        	{
	        		myParentSelection = new Array();
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection[myParentSelection.length] = myParent[i].value;
	        				var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
	        				for (var t = 0; t < temp.length; t++)
	        				{
	        					myNewValues[myNewValues.length] = temp[t];
	        				}
						}
					}
	        	}
<?
	        							break;
			        			}
?>
	        	var myContainer = <?= $myFormName ?>_<?= $aField->Name ?>_Container;
	        	var newContents = '';
	        	var newSelection = null;
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
					    newContents += ('<input type="radio"'
										+ ' id="<?= $aField->IsDisplayOnly ?>"'
										+ '	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?>'
										+ ' name="<?= $aField->Name ?>"'
										+ ' value="'+myNewValues[i][0]+'"'
										+ ' <?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>'
										+ ' >&nbsp;'+myNewValues[i][1]+'<br>');
						if (mySelection == myNewValues[i][0])
						{
							newSelection = i;
						}
					}
	        	}
				myContainer.innerHTML = newContents;
				if (newSelection != null)
				{
					me[newSelection].checked = true;
				}
<?
								break;
		            		case "Combobox":
		            		case "List":
		            		default:
?>
	        	var me = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>;
	        	var mySelection = null;
	        	if (me.selectedIndex >= 0)
	        	{
	        		mySelection = me.options[me.selectedIndex].value;
	        	}
<?
								switch ($aField->Parent->Type)
								{
									case "Object":
			        					switch ($aField->Parent->VisualComponent)
			        					{
			            					case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
												break;
			            					case "Combobox":
			            					case "List":
			            					default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
												break;
			        					}
?>
	        	if (myParentSelection == null)
	        	{
   	        		var myNewValues = new Array();
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm, '<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	myParentSelection = new Array();
				for (var i = 0; i < myParent.length; i++)
				{
					if (myParent[i].checked)
					{
						myParentSelection[myParentSelection.length] = myParent[i].value;
						var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
						for (var t = 0; t < temp.length; t++)
						{
							myNewValues[myNewValues.length] = temp[t];
						}
					}
				}
<?
										break;			        			
			        			}
?>
	        	me.length = 0;
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
						var newOption = new Option(myNewValues[i][1], myNewValues[i][0]);
						me.options[me.options.length] = newOption;
						if (mySelection == myNewValues[i][0])
						{
							me.selectedIndex = i;
						}
					}
				}
<?
				if (isset($aField->DynamicLoadFn) && trim($aField->DynamicLoadFn) != '')
				{
?>
				else
				{
					<?=$aField->DynamicLoadFn?>;
				}
<?					
				}

								break;
		        		}
		        		break;
		    		case "Array":
?>
				var me = GetElementsByName(<?= $myFormName ?>_SaveForm, '<?= $aField->Name ?>[]');
								
	        	var mySelection = new Array();
	        	
	        	if (me != null)
	        	{
	        		if(me.length != undefined && me.length != null)
	        		{
						for (var i = 0; i < me.length; i++)
						{
							if (me[i].checked)
							{
		        				mySelection[mySelection.length] = me[i].value;
							}
						}
	        		}
	        		else
	        		{
						if (me.checked)
						{
	        				mySelection[mySelection.length] = me.value;
						}
	        		}
	        	}
	        	
<?
						switch ($aField->Parent->Type)
						{
							case "Object":
			        			switch ($aField->Parent->VisualComponent)
			        			{
			            			case "Radio":
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null)
	        	{
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection = myParent[i].value;
						}
					}
	        	}
<?
										break;
			            			case "Combobox":
			            			case "List":
			            			default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
					myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
<?
										break;
			        			}
?>
	        	if (myParentSelection == null)
	        	{
	        	}
	        	else
	        	{
   	        		var myNewValues = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
		        }
<?
			        					break;
			        				case "Array":
?>
				var myParent = GetElementsByName(<?= $myFormName ?>_SaveForm,'<?= $aField->Name ?>[]');
	        	var myParentSelection = null;
   	        	var myNewValues = new Array();
	        	if (myParent != null)
	        	{
	        		myParentSelection = new Array();
					for (var i = 0; i < myParent.length; i++)
					{
						if (myParent[i].checked)
						{
	        				myParentSelection[myParentSelection.length] = myParent[i].value;
	        				var temp = <?= $myFormName ?>_<?= $aField->Name ?>[myParent[i].value];
	        				for (var t = 0; t < temp.length; t++)
	        				{
	        					myNewValues[myNewValues.length] = temp[t];
	        				}
						}
					}
	        	}
<?
	        							break;
			        			}
?>
	        	var myContainer = <?= $myFormName ?>_<?= $aField->Name ?>_Container;
	        	var newContents = '';
	        	var newSelection = new Array();
	        	if (myNewValues != null)
	        	{
					for (var i = 0; i < myNewValues.length; i++)
					{
					    newContents += ('<input type="checkbox"'
										+ ' id="<?= $aField->IsDisplayOnly ?>"'
										+ ' <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?>'
										+ ' name="<?= $aField->Name ?>[]"'
										+ ' value="'+myNewValues[i][0]+'"'
										+ ' <?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>'
										+ ' >&nbsp;'+myNewValues[i][1]+'<br>');
						for (var t = 0; t < mySelection.length; t++)
						{ 
							if (mySelection[t] == myNewValues[i][0])
							{
								newSelection[newSelection.length] = i;
							}
						}
					}
	        	}
	        	newContents += '<input type="hidden"'
	        						+ ' name="<?= $aField->Name ?>_EmptySelection"'
	        						+ ' value="' + (newSelection.length == 0 ? '1' : '0') + '">';
				myContainer.innerHTML = newContents;
				
	        	if (me != null)
	        	{
	        		if(me.length != undefined && me.length != null)
	        		{
						for (var t = 0; t < newSelection.length; t++)
						{ 
							me[newSelection[t]].checked = true;
						}
	        		}
	        		else
	        		{
						if (newSelection.length>0)
						{
							me = <?= $myFormName ?>_SaveForm.elements.namedItem('<?= $aField->Name ?>[]');
	        				me.checked = true;
						}
	        		}
	        	}
<?
		        		break;
		        	default:
?>
				var myParent = <?= $myFormName ?>_SaveForm.<?= $aField->Parent->Name ?>;
				var myParentSelection = myParent.value;
				var myNewValue = <?= $myFormName ?>_<?= $aField->Name ?>[myParentSelection];
				if (myNewValue != null)
				{
<?
						if ($aField->FormatMask != "")
						{
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = myNewValue;
<?
							switch ($aField->Type)
							{
								case "String":
								case "Password":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatString(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
								case "Integer":
								case "Float":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatNumber(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
								case "Date":
								case "DateTime":
?>
				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(myNewValue, '<?= $aField->FormatMask ?>');
<?
									break;
							}
						}
						else
						{
?>
   	        	<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = myNewValue;
<?
						}
?>
				}
<?
						break;
	        	}
				foreach ($aField->Children as $aChild)
				{
?>
            	<?= $myFormName ?>_<?= $aChild->Name ?>_Update();
<?
				}
?>
			}
<?
			}
			$aFieldIdx++;
		}
?>
		function body_click()
		{	
		}
		
	    </script>
<?
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		if($this->HasFormTitle)
		{
?>
	    <div class="object_title">
			<?= $this->get_Image().$myFormTitle ?>

	    </div>
<?
		}
?>
<?
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		if($this->HasHr)
		{
?>

	    <hr class="object_title_hr">
<?
		}
?>
	    <div class="object_buttons">
<?
	    if(!$this->IsDialog)
	    {
	    	if(!$this->hideBackButton($aUser))
	    	{
?>
			<div class="collectionButtonsSelection">
			<button title="<?= translate('Home') ?>" class="object_homeButton" onclick="javascript: document.location.href = '<?= 'main.php?'.$this->get_Parent()->get_QueryString() ?>';">
				<img src="images/home.png" alt="<?= translate('Home') ?>" title="<?= translate('Home') ?>" displayMe="1" /> <?= translate("Home") ?>
            </button>
    	    </div>
<?
	    	}
	    }
?>

<?
		if (!$this->isNewObject())
		{
			$addSpaces = false;
			if ($this->canRemove($aUser) && !$this->hideRemoveButton($aUser))
			{
?>
			<button title="<?= translate('Remove') ?>" class="object_removeButton" onclick="javascript: <?= $myFormName ?>_Remove();" id="<?= $myFormName ?>_RemoveButton">
            	<img src="images/remove.png" alt="<?= translate('Remove') ?>" title="<?= translate('Remove') ?>" displayMe="1" /> <?= translate("Remove") ?>
            </button>
			
<?
				$addSpaces = true;
			}
			if ($this->canEdit($aUser))
			{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasEdit)
				{
?>
			<button title="<?= translate('Edit') ?>" id="<?= $myFormName ?>_EditButton" class="object_editButton" onclick="javascript: <?= $myFormName ?>_Edit();">
				<img src="images/edit.png" alt="<?= translate('Edit') ?>" title="<?= translate('Edit') ?>" displayMe="1" /> <?= translate("Edit") ?>
           </button>
<?
				}
				$addSpaces = true;
			}
			if ($addSpaces)
			{
?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
			}
		}
?>
			<button title="<?= translate('Save') ?>" id="<?= $myFormName ?>_OkSelfButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('self');">
				<img src="images/ok.png" alt="<?= translate('Save') ?>" title="<?= translate('Save') ?>" displayMe="1" /> <?= translate("Save") ?>
            </button>
			
<?
	    if(!$this->IsDialog)
	    {
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			if($this->HasSaveAndGoParent)
			{
?>
			<button title="<?= translate('Save & Go Parent') ?>" id="<?= $myFormName ?>_OkParentButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('parent');">
				<img src="images/okparent.png" alt="<?= translate('Save & Go Parent') ?>" title="<?= translate('Save & Go Parent') ?>" displayMe="1" /> <?= translate("Save & Go Parent") ?>
            </button>
			
<?	
			}
	    } else {
    	}
?>

<?
		if ($this->isNewObject())
		{
?>

<?
		    if(!$this->IsDialog)
	    	{
?>
			<button title="<?= translate('Save & Go New') ?>" id="<?= $myFormName ?>_OkNewButton" style="display:none" class="object_okButton" onclick="javascript: <?= $myFormName ?>_Ok('new');">
			<img src="images/oknew.png" alt="<?= translate('Save & Go New') ?>" title="<?= translate('Save & Go New') ?>" displayMe="1" /> <?= translate("Save & Go New") ?>
            </button>

<?
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasCancel)
				{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: document.location.href = '<?= 'main.php?'.$this->get_Parent()->get_QueryString() ?>';">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
<?
				}
	    	}
	    	else
	    	{
				//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
				if($this->HasCancel)
				{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: <?= $myFormName ?>_CancelDialog();">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
			
<?
				}
	    	}
		}
		else
		{
			//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
			if($this->HasCancel)
			{
?>
			<button title="<?= translate('Cancel') ?>" id="<?= $myFormName ?>_CancelButton" style="display:none" class="object_cancelButton" onclick="javascript: <?= $myFormName ?>_Cancel(true);">
				<img src="images/cancel.png" alt="<?= translate('Cancel') ?>" title="<?= translate('Cancel') ?>" displayMe="1" /> <?= translate("Cancel") ?>
            </button>
<?
			}
		}
        
		$this->addButtons($aUser);
?>
 		</div><div style="clear:both;"></div>
<?
		if (!$this->isNewObject())
		{
?>
 		<form id="<?= $myFormName ?>_RemoveForm" action="<?= 'main.php?'.$this->get_QueryString() ?>" method="post">
  			<input type="hidden" name="<?= $myFormIDFieldName ?>[]" value="<?= $this->get_FieldValueOf($myFormIDFieldName) ?>">
		</form>
<?
		}
?>
 		<form id="<?= $myFormName ?>_SaveForm" action="<?= 'main.php?'.$this->get_QueryString() ?>" method="post" enctype="<?=$this->get_FormEncType()?>">
  			<input type="hidden" name="go" value="self">
<?
		if(!array_key_exists($myFormIDFieldName,$myFormFields))
		{
?>
  			<input type="hidden" name="<?= $myFormIDFieldName ?>" value="<?= $this->get_FieldValueOf($myFormIDFieldName) ?>">
<?
		}
?>
    		<table class="object_table">
<?
		$this->insertPreviewRows($aUser);
?>

<?
		$rowCols = 0;
		$inTitle = false;
		$inCell = false;
		$aFieldIdx = 0;
    	foreach ($myFormFields as $aField)
    	{
    		$aFieldName = $aField->Name;
			if (!$inTitle && !$inCell && $rowCols > 1)
			{
?>
				</tr>
<?
				$rowCols = 0;
			}
			if (!$inTitle && !$inCell && $rowCols < 1)
			{
				if($aField->CodeBeforeField!=="")
				{
?>				
				<tr id="RowBeforeField_<?=$aField->Name?>"><td colspan="4"><?=$aField->CodeBeforeField?></td></tr>
<?
				}
				
				if(count($aField->InsertRowsBeforeField)>0)
				{
					foreach($aField->InsertRowsBeforeField as $rowKey=>$rowCode)
					{
?>				
				<tr id="RowInsertField_<?=$aField->Name?>_<?=$rowKey?>"><?=$rowCode?></tr>
<?
					}
				}
?>				
        		<tr id="Row_<?=$aField->Name?>">
<?
			}
			$cellCols = 0;
			switch ($aField->Type)
			{
				case "String":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
			    			if ($aField->Size <= 32)
			    			{
			    				$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							}
							else
							{
								$cellCols = 2;
							}
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td id="tdPrincipal" class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
					//@JAPR 2014-06-17: Agregada la propiedad para agregar estilos personalizados a los campos
					//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
					$strClass = ($aField->CustomizedClass)?" customizedClass=\"{$aField->CustomizedClass}\" ":"";
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?=$strClass?>
						        style="
								<?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth))? "width:".$aField->FixedWidth.";" : "" ?>
						        "
								name="<?= $aField->Name ?>"
								size="<?= $aField->Size < 86 ? ($aField->Size + 2) : 88 ?>"
								maxlength="<?= $aField->Size ?>"
								value="<?=htmlspecialchars($aFieldValue, ENT_QUOTES)?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatString(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatString(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
											this.value = formatString(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Integer":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
					//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        style="
						        <?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth))? "width:".$aField->FixedWidth.";" : "" ?>
						        "
								name="<?= $aField->Name ?>"
								size="12"
								maxlength="11"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
						if($aField->DefaultNumberZero==true)
						{
							$strOnBlur = "
											var v = unformatNumber(this.value, '".$aField->FormatMask."');
		            						this['_value'] = v;
											this.value = formatNumber(v, '".$aField->FormatMask."');\"";
						}
						else 
						{
							$strOnBlur = "	
											if(Trim(this.value)!='')
											{
												var v = unformatNumber(this.value, '".$aField->FormatMask."');
			            						this['_value'] = v;
												this.value = formatNumber(v, '".$aField->FormatMask."');
											}
											else
											{
												this['_value'] = '';
												this.value = '';
											}\"";
						}
?>
								onfocus="javascript:
											var v = formatNumber(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatNumberRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
  											<?=$strOnBlur?>
<?


					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Float":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="20"
								maxlength="18"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
						if($aField->DefaultNumberZero==true)
						{
							$strOnBlur = "
											var v = unformatNumber(this.value, '".$aField->FormatMask."');
		            						this['_value'] = v;
											this.value = formatNumber(v, '".$aField->FormatMask."');\"";
						}
						else 
						{
							$strOnBlur = "	
											if(Trim(this.value)!='')
											{
												var v = unformatNumber(this.value, '".$aField->FormatMask."');
			            						this['_value'] = v;
												this.value = formatNumber(v, '".$aField->FormatMask."');
											}
											else
											{
												this['_value'] = '';
												this.value = '';
											}\"";
						}
?>
								onfocus="javascript:
											var v = formatNumber(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatNumberRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
											<?=$strOnBlur?>
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Date":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="12"
								maxlength="10"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != NULL || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatDateTime(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatDate(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
<?
			    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    					{
?>
											var today = new Date();
											var year = new Number(this['_value'].substr(0, 4));
											if (year == 0)
											{
												year = today.getFullYear();
											}
											var month = new Number(this['_value'].substr(5, 2));
											if (month == 0)
											{
												month = today.getMonth() + 1;
											}
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentYear(year);
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentMonth(month - 1);
<?
						}
?>

											this.value = formatDateTime(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
<?
		    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    				{
?>
						<script language="JavaScript">
							function calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
								}
	            				var v = year + '-' + month + '-' + day;
	            				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = v;
								<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(v, '<?= $aField->FormatMask ?>');
    						}
    						calendar_<?= $myFormName ?>_<?= $aField->Name ?> = new calendar('calendar_<?= $myFormName ?>_<?= $aField->Name ?>', 'calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>');
						</script>
<?
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "DateTime":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="text"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="22"
								maxlength="19"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatDateTime(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatDateTime(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
<?
			    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    					{
?>
											var today = new Date();
											var year = new Number(this['_value'].substr(0, 4));
											if (year == 0)
											{
												year = today.getFullYear();
											}
											var month = new Number(this['_value'].substr(5, 2));
											if (month == 0)
											{
												month = today.getMonth() + 1;
											}
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentYear(year);
	            							calendar_<?= $myFormName ?>_<?= $aField->Name ?>.setCurrentMonth(month - 1);
<?
						}
?>
											this.value = formatDateTime(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
<?
		    		if (!$aField->IsDisplayOnly && $aField->IsVisible)
    				{
?>
						<script language="JavaScript">
							function calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>(day, month, year)
							{
								if (String(month).length == 1)
								{
									month = '0' + month;
								}
								if (String(day).length == 1)
								{
									day = '0' + day;
        						}
        						var v = <?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'];
								v = v.substr(10);
        						v = year + '-' + month + '-' + day + v;
	            				<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>['_value'] = v;
								<?= $myFormName ?>_SaveForm.<?= $aField->Name ?>.value = formatDateTime(v, '<?= $aField->FormatMask ?>');
    						}
    						calendar_<?= $myFormName ?>_<?= $aField->Name ?> = new calendar('calendar_<?= $myFormName ?>_<?= $aField->Name ?>', 'calendarCallback_<?= $myFormName ?>_<?= $aField->Name ?>');
						</script>
<?
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "LargeString":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = 2;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<textarea
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								cols="88"
								rows="5"
								onkeydown="javascript: return TextArea_onkeydown(this);"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								><?= $aFieldValue ?></textarea>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "LargeStringHTML":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = 2;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
						<script type="text/javascript">
							//var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
							if (!objTinyMCEmode) {
								var objTinyMCEmode = new Object();
							}
							objTinyMCEmode["<?=$aField->Name?>"] = true;
							
							visibleDivArray["<?=$aField->Name?>"] = '<?=$aField->Name?>DefaultDesHDiv';
							function changeTab(oEvt, sPropName) {
								
								if (!oEvt || !sPropName) {
									return;
								}
								
								objEditorType=<?=$myFormName?>_SaveForm.EditorType;
								if(objEditorType)
								{
									editorTypeValue = objEditorType.options[objEditorType.selectedIndex].value;
								}
								else
								{
									editorTypeValue = 0;
								}
								
								
								if(editorTypeValue==0)
								{
									var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
								}
								else
								{
									var strTabId = sPropName+oEvt.getAttribute('name')+'DesDiv';
									if (document.getElementById(strTabId))
									{
										visibleDivArray[sPropName] = sPropName+oEvt.getAttribute('name')+'DesHDiv';
									}
									else 
									{
										var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
									}									
								}
								
								document.getElementById(sPropName+'DefaultDiv').style.display = 'none';
								var objDiv = document.getElementById(sPropName+'iPodDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadMiniDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPhoneDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'CelDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'TabletDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								
								var objDiv = document.getElementById(sPropName+'DefaultDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPodDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPadMiniDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'iPhoneDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'CelDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								var objDiv = document.getElementById(sPropName+'TabletDesDiv');
								if (objDiv) {
									objDiv.style.display = 'none';
								}
								
								var objDiv = document.getElementById(strTabId);
								if (objDiv) {
									objDiv.style.display = 'block';
								}
								try {
									var objDiv = document.getElementById(sPropName+'Menu');
									if (objDiv) {
										var arrLi = objDiv.getElementsByTagName('li');
										if (arrLi) {
											for (var intIdx in arrLi) {
												arrLi[intIdx].className = "menuOption";
											}
										}
									}
									
									if (oEvt) {
										oEvt.className = "menuOption selectedTab"						
									}
								} catch(e) {}
							}
						</script>
						
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign && $aField->MultiDeviceNames && is_array($aField->MultiDeviceNames) && count($aField->MultiDeviceNames)) {
?>
						<div id="<?=$aField->Name?>Menu" class="tabbed" style="height:40px;">
							<ul class="menu">
								<li id="default<?=$aField->Name?>" name="Default" class="menuOption selectedTab" onclick="changeTab(this,'<?=$aField->Name?>')">
									<a><?=translate("Default")?></a>
						        </li>					
<?
						//Posteriormente se agregará un tab por cada opción específica de dispositivo asignada
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) {
							$strDeviceLabel = (string) @$aField->MultiDeviceLabels[$intIndex];
							if ($strDeviceLabel == '') {
								$strDeviceLabel = $strDevice;
							}
?>
								<li name="<?=$strDevice?>" class="menuOption" onclick="changeTab(this,'<?=$aField->Name?>')">
									<a><?=$strDeviceLabel?></a>
						        </li>					
<?
						}
?>
							</ul>
						</div>
<?
					}
					//@JAPR
					
					//HTML Designer
					$styleDefaultDiv = "";
					$styleDefaultDesDiv = "";
					if($aField->UseHTMLDes==1)
					{
						if($this->EditorType==0)
						{
							$styleDefaultDiv = '';
							$styleDefaultDesDiv = 'style="display: none;width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
						}
						else
						{
							$styleDefaultDiv = 'style="display: none;"';
							$styleDefaultDesDiv = 'style="width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
						}
					}
?>
						<div id="<?=$aField->Name?>Tabs">
							<div id="<?=$aField->Name?>DefaultDiv"  <?=$styleDefaultDiv?>>
								<textarea
							        id="<?= $aField->Name ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									cols="88"
									rows="5"
									onkeydown="javascript: return TextArea_onkeydown(this);"
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									><?= $aFieldValue ?></textarea><br>
									<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
									<div style="display: inline-flex">
										<a href="#" title="toogle TinyMCE" onclick="toogleEditorModeFields('<?= $aField->Name ?>');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
<?										/*@AAL 25/02/2015: En preguntas tipo QuestionImageSketch no se debe mostrar el botón que me permita abrir el editor de fórmulas */
										if($aField->Name != "QuestionImageSketch")
											echo '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\'; ShowWindowsDialogEditor(\'' . $aField->Name . '\');">';
?>
									</div>
									<!-- @AAL -->
							</div>
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign) {
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) {
							$strDeviceFieldName = $aField->Name.$strDevice;
							$aFieldValueTmp = $this->get_FieldValueOf($strDeviceFieldName);
?>
							<div id="<?=$strDeviceFieldName?>Div" style="display: none;">
								<textarea
							        id="<?=$strDeviceFieldName?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?=$strDeviceFieldName?>"
									cols="88"
									rows="5"
									onkeydown="javascript: return TextArea_onkeydown(this);"
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$strDeviceFieldName."_".$aFieldIdx."_OnChange();\"" : "" ?>
									><?= $aFieldValueTmp ?></textarea><br>
									<!-- @AAL 25/02/2015: Agregado Div y un Button que abre la ventana del editor de fórmulas -->
									<div style="display: inline-flex">
										<a href="#" title="toogle TinyMCE" onclick="toogleEditorModeFields('<?=$strDeviceFieldName?>');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
<?										/*@AAL 25/02/2015: En preguntas tipo QuestionImageSketch no se debe mostrar el botón que me permita abrir el editor de fórmulas */
										if(strpos($strDeviceFieldName, "QuestionImageSketch") === false)
											echo '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat; margin-left: 5px;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\'; ShowWindowsDialogEditor(\'' . $strDeviceFieldName . '\');">';
?>
									</div>
									<!-- @AAL -->
							</div>
<?
						}
					}
					//@JAPR
?>
						</div>
						
<?
					//HTML Designer
					//Nuevos divs para el nuevo editor
					if (getMDVersion() >= esvEditorHTML && $aField->UseHTMLDes)
					{
?>
							
				<span id="<?=$aField->Name?>SpanDesigner">
					<table id="<?=$aField->Name?>_tbl" style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC; border-collapse:collapse;">
					<tbody>
						<tr height=25px>
						<td margin="0" style="background: #F0F0EE;border: 0; border-bottom:1px solid #CCC;padding:0px">
						<div id="<?=$aField->Name?>_toolbargroup" tabindex="-1">
						<span>
							<table id="<?=$aField->Name?>_toolbar1" cellpadding="0" cellspacing="0" tabindex="-1">
								<tbody>
								<tr>
									<td style="position: relative;padding:0px">
									<?
									if(strpos($aField->CompToAdd, 'LBL,ADD',0)!==false)
									{
									?>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'LBL,ADD',0,0);" title="Insert Text">
											<img src="./images/insertText.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									if(strpos($aField->CompToAdd, 'BTN,ADD',0)!==false)
									{
									?>
									
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'BTN,ADD',0,0);" title="Insert Button">
											<img src="./images/insertButton.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									if(strpos($aField->CompToAdd, 'IMG,ADD',0)!==false)
									{
									
									?>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['<?=$aField->Name?>'],visibleDivArray['<?=$aField->Name?>'],'IMG,ADD',0,0);" title="Insert Image">
											<img src="./images/insertImage.png" style="display:inline;cursor:pointer;">
										</a>
									<?
									}
									?>
									</td>
								</tr>
								</tbody>
							</table>
						</span>
						</div>
						</td>
						</tr>
						<tr>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC;padding:0px">
					<div id="<?=$aField->Name?>DefaultDesDiv" <?=$styleDefaultDesDiv?>>
					<?$aFieldNameDes = $aField->Name.'Des';?>
						<textarea id="<?=$aField->Name?>Des" name="<?=$aField->Name?>Des" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->$aFieldNameDes?></textarea>
						<textarea id="<?=$aField->Name?>DesHTML" name="<?=$aField->Name?>DesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValue?></textarea>
					</div>

					<?
						//@JAPR 2014-07-30: Agregado el Responsive Design
						//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
						//el componente default
						if (getMDVersion() >= esvResponsiveDesign) 
						{
							
						foreach ($aField->MultiDeviceNames as $intIndex => $strDevice) 
						{
							$strDeviceFieldName = $aField->Name.$strDevice.'Des';
							$aFieldValueTmp = $this->get_FieldValueOf($strDeviceFieldName);
							//$widthDes=getWidthByDeviceID($intIndex);
							//$heightDes=getHeightByDeviceID($intIndex);
?>
					<div id="<?=$strDeviceFieldName?>Div"  style="display: none; width: <?=$widthByDisp[$strDevice]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="<?=$strDeviceFieldName?>" name="<?=$strDeviceFieldName?>" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValueTmp?></textarea>
						<textarea id="<?=$strDeviceFieldName?>HTML" name="<?=$strDeviceFieldName?>HTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$aFieldValueTmp?></textarea>
					</div>
<?
						}
						}
?>
						</td>
						</tr>
					</tbody>
					</table>
				</span>
		
				<script type="text/javascript">				
				 function onLoad<?=$aField->Name?>()
				 {
					aCompToAdd = new Array();
				 	aCompToAdd = [<?=$aField->CompToAdd?>];
				 	aCompToAddDes = new Array();
				 	aCompToAddDes = [<?=$aField->CompToAddDes?>];
					
				 	if(document.getElementById('<?=$aField->Name?>DefaultDesDiv'))
				 	{
				 		aHTMLDivObjects['<?=$aField->Name?>DefaultDesHDiv'] = new reportPart('<?=$aField->Name?>DefaultDesDiv', '', defaultWidth, defaultHeight, '<?=$aField->Name?>DefaultDesHDiv', '<?=$aField->Name?>Des');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPodDesDiv'))
				 	{				 	
						aHTMLDivObjects['<?=$aField->Name?>iPodDesHDiv'] = new reportPart('<?=$aField->Name?>iPodDesDiv', '', iPodWidth, iPodHeight, '<?=$aField->Name?>iPodDesHDiv', '<?=$aField->Name?>iPodDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPadDesDiv'))
				 	{				 	
						aHTMLDivObjects['<?=$aField->Name?>iPadDesHDiv'] = new reportPart('<?=$aField->Name?>iPadDesDiv', '', iPadWidth, iPadHeight, '<?=$aField->Name?>iPadDesHDiv', '<?=$aField->Name?>iPadDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPadMiniDesDiv'))
				 	{					
				 		aHTMLDivObjects['<?=$aField->Name?>iPadMiniDesHDiv'] = new reportPart('<?=$aField->Name?>iPadMiniDesDiv', '', iPadMiniWidth, iPadMiniHeight, '<?=$aField->Name?>iPadMiniDesHDiv', '<?=$aField->Name?>iPadMiniDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>iPhoneDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>iPhoneDesHDiv'] = new reportPart('<?=$aField->Name?>iPhoneDesDiv', '', iPhoneWidth, iPhoneHeight, '<?=$aField->Name?>iPhoneDesHDiv', '<?=$aField->Name?>iPhoneDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>CelDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>CelDesHDiv'] = new reportPart('<?=$aField->Name?>CelDesDiv', '', celAndroidWidth, celAndroidHeight, '<?=$aField->Name?>CelDesHDiv', '<?=$aField->Name?>CelDes');
				 	}
				 	if(document.getElementById('<?=$aField->Name?>TabletDesDiv'))
				 	{
						aHTMLDivObjects['<?=$aField->Name?>TabletDesHDiv'] = new reportPart('<?=$aField->Name?>TabletDesDiv', '', tabletAndroidWidth, tabletAndroidHeight, '<?=$aField->Name?>TabletDesHDiv', '<?=$aField->Name?>TabletDes');
				 	}
				 }
								 
				if (document.attachEvent){
					document.attachEvent("onload", onLoad<?=$aField->Name?>());
				}
				else if (document.addEventListener) {
					document.addEventListener("load", onLoad<?=$aField->Name?>(), false);
				}				 
				</script>
				
<?
					}
?>						
						<script type="text/javascript">
							if (!arrResponsiveDesignObjectsFields) {
								var arrResponsiveDesignObjectsFields = new Array();
								var arrResponsiveDesignObjectsFieldsLoaded = new Array();
							}
							arrResponsiveDesignObjectsFields.push("<?=$aField->Name?>");
							arrResponsiveDesignObjectsFieldsLoaded.push(0);
<?
					//@JAPR 2014-08-04: Agregado el Responsive Design
					//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
					//el componente default
					if (getMDVersion() >= esvResponsiveDesign) {
						//El campo se puede configurar para tener valores específicos por dispositivo, si lo hace entonces se prepara el
						//array para generar tantos componentes como dispositivos se especifiquen, de lo contrario sólo se agrega el
						//componente propio del campo, el cual a diferencia de los demás, simplemente no tendrá un sufijo asociado
						if ($aField->MultiDeviceNames && is_array($aField->MultiDeviceNames) && count($aField->MultiDeviceNames)) {
							foreach ($aField->MultiDeviceNames as $strDevice) {
								$strDeviceFieldName = $aField->Name.$strDevice;
?>
							arrResponsiveDesignObjectsFields.push("<?=$strDeviceFieldName?>");
							arrResponsiveDesignObjectsFieldsLoaded.push(0);
							objTinyMCEmode["<?=$strDeviceFieldName?>"] = true;
<?
							}
						}
?>
<?
					}
					
					//@JAPR 2014-08-04: No logré identificar por qué no creaba las instancias dle tinyMCE al hacerlo directo siendo que
					//otros casos como en FormattedText.php si lo hace, así que se tuvo que usar un setTimeout para forzar a crear las
					//instancias hasta que se terminaba de cargar la página
?>
							setTimeout(function () {
							for (var intIdx in arrResponsiveDesignObjectsFields) {
								if (intIdx == 'contains') {
									continue;
								}
								
								var strTinyMCEObjectName = arrResponsiveDesignObjectsFields[intIdx];
								if (arrResponsiveDesignObjectsFieldsLoaded[intIdx]) {
									continue;
								}
								
								arrResponsiveDesignObjectsFieldsLoaded[intIdx] = 1;
								tinyMCE.init({
									// General options
									mode : "exact",
									readonly : 0,
							        elements : strTinyMCEObjectName + ",ajaxfilemanager",
							      	theme : "advanced",
									file_browser_callback : "ajaxfilemanager",
									entity_encoding : "raw",
									// Theme options  //Conchita 9-nov agregado forecolor
						<?
							//@JAPR 2014-03-18: Agregado el texto en formato HTML para despliegues personalizados
							$strTinyMCEToolBar = "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image";
							if (isset($aField->HTMLFormatButtons) && trim($aField->HTMLFormatButtons) != '') {
								$strTinyMCEToolBar = $aField->HTMLFormatButtons;
							}
						?>
									theme_advanced_buttons1 : "<?=$strTinyMCEToolBar?>",
									theme_advanced_buttons2: "",
									theme_advanced_buttons3: "",
									theme_advanced_buttons4: "",		
									theme_advanced_toolbar_location : "top",
									theme_advanced_toolbar_align : "left",
									theme_advanced_statusbar_location : "none",
									theme_advanced_resizing : true,
									//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
									theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	                										"Arial=arial,helvetica,sans-serif;"+
	                										"Arial Black=arial black,avant garde;"+
	                										"Book Antiqua=book antiqua,palatino;"+
											                "Comic Sans MS=comic sans ms,sans-serif;"+
											                "Courier New=courier new,courier;"+
											                "Georgia=georgia,palatino;"+
											                "Helvetica=helvetica;"+
											                "Impact=impact,chicago;"+
											                "Symbol=symbol;"+
											                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
											                "Terminal=terminal,monaco;"+
											                "Times New Roman=times new roman,times;"+
											                "Trebuchet MS=trebuchet ms,geneva;"+
											                "Verdana=verdana,geneva;"+
											                "Webdings=webdings;"+
											                "Wingdings=wingdings,zapf dingbats",
									//Conchita 4 nov 2011 se agregaron estas opciones para el upload de imagenes
	           						relative_urls : false,
	        						content_css : "",
	        						convert_urls : false,   //Conchita 7nov2011 agregado para que tome las urls bien sin convertirlas                
									//fin conchita agregado
									// Drop lists for link/image/media/template dialogs
								//	external_image_list_url : "lists/image_list.js",  //Conchita 10-nov-2011 Eliminar image list
									external_link_list_url : "lists/link_list.js",
									save_callback : "myCustomSaveContent",
									oninit: "myInitTinyMCEFields",
									cleanup: false,
									verify_html : false,
									forced_root_block : false
								});
							}
							}, 100);
							
							function myInitTinyMCEFields() {
								for (var intIdx in arrResponsiveDesignObjectsFields) {
									if (intIdx == 'contains' || isNaN(intIdx)) {
										continue;
									}
									
									var strTinyMCEObjectName = arrResponsiveDesignObjectsFields[intIdx];
									if (tinyMCE && tinyMCE.editors && tinyMCE.editors[strTinyMCEObjectName]) {
										var htmlSrc = tinyMCE.editors[strTinyMCEObjectName].getContent();
										
										if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
											setTimeout(function () {toogleEditorModeFields(strTinyMCEObjectName);}, 300);
										}
									}
								}
							}
							
							function myCustomSaveContent(element_id, html, body) {
							        // Do some custom HTML cleanup
							        if (html) {
							        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
							        }
							
							        return html;
							}
							
							function ajaxfilemanager(field_name, url, type, win) 
							{
								var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
								var view = 'detail';
								switch (type) 
								{
									case "image":
										view = 'thumbnail';
										break;
									case "media":
										break;
									case "flash": 
										break;
									case "file":
										break;
									default:
										return false;
								}
            					
								tinyMCE.activeEditor.windowManager.open({
									url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
									width: 782,
                					height: 440,
									inline : "yes",
									close_previous : "no"
								},{
									window : win,
									input : field_name
								}); 
							}
							
							//Inicio conchita 16-nov-2011 toggle tinymce
							function toogleEditorModeFields(sEditorID) { 
								var x;
								var tmp;
								var form="<?= $myFormName ?>";
							    try {
							        if(objTinyMCEmode[sEditorID]) { 
							        	//x=removeHTML();
							       		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
							           	//document.getElementById(sEditorID).value=x; 
							            objTinyMCEmode[sEditorID] = false; 
							        } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
							            //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
							            objTinyMCEmode[sEditorID] = true; 
							        } 
							    } catch(e) { 
							    	alert('An error occur, try again. Error: '+ e);
							        //error handling 
							    } 
							}
							
						  	function removeHTML() {
								var htmlSrc = tinyMCE.activeEditor.getContent();
								var rexp = /<[^<>]+>/g;
								
								var cleanSrc = htmlSrc.replace(rexp,"");
								
								return cleanSrc;
						  	}
							//Fin Conchita toggle tinymce 16-nov-2011
						</script>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Boolean":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					switch ($aField->VisualComponent)
					{
						case "Checkbox":
		    				if (is_null($aField->Value))
		    				{
								if (intval($aFieldValue) == 0)
				    			{
									$disabledFalse = "disabled";
									$checkedTrue = "";
								}
								else
								{
									$disabledFalse = "";
									$checkedTrue = "checked";
								}
?>
						<input type="hidden"
						        id="boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden"
								name="<?= $aField->Name ?>"
								value="0" <?= $disabledFalse ?>
								>
						<input type="checkbox"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="1" <?= $checkedTrue ?>
								onclick="javascript: 
											if (this.checked) 
											{
												boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden.disabled = true;			
											}
											else
											{
												boolean_<?= $myFormName ?>_<?= $aField->Name ?>_hidden.disabled = false;
											}											
											<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();" : "" ?>"
								>
<?
		    				}
		    				else
		    				{
								if ($aFieldValue == $aField->Value)
				    			{
									$checked = "checked";
								}
								else
								{
									$checked = "";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="<?= $aField->Value ?>" <?= $checked ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
		    				}
							break;
		    			case "Radio":
		    			default:
		    				if (is_null($aField->Value))
		    				{
								if (intval($aFieldValue) == 0)
				    			{
									$checkedFalse = "checked";
									$checkedTrue = "";
								}
								else
								{
									$checkedFalse = "";
									$checkedTrue = "checked";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="0" <?= $checkedFalse ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>&nbsp;<?= "False" ?>
                        <br>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="1" <?= $checkedTrue ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>&nbsp;<?= "True" ?>
<?
		    				}
		    				else
		    				{
								if ($aFieldValue == $aField->Value)
				    			{
									$checked = "checked";
								}
								else
								{
									$checked = "";
								}
?>
						<input type="radio"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								value="<?= $aField->Value ?>" <?= $checked ?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
		    				}
							break;
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Password":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
			    			if ($aField->Size <= 32)
			    			{
			    				$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							}
							else
							{
								$cellCols = 2;
							}
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<input type="password"
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name ?>"
								size="<?= $aField->Size < 86 ? ($aField->Size + 2) : 88 ?>"
								maxlength="<?= $aField->Size ?>"
								value="<?= $aFieldValue ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
<?
					if ($aField->FormatMask != "")
					{
?>
								onfocus="javascript:
											var v = formatString(this['_value'], '<?= $aField->FormatMask ?>');
											this.value = v;
											if (this.createTextRange)
											{
												var r = this.createTextRange();
    											r.moveEnd('character', v.length);
    											r.select();
  											}"
								onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('<?= $aField->FormatMask ?>'), event.keyCode);"
								onblur="javascript:
	            							var v = unformatString(this.value, '<?= $aField->FormatMask ?>');
	            							this['_value'] = v;
											this.value = formatString(v, '<?= $aField->FormatMask ?>');"
<?
					}
?>
						>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Object":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?> <?=(rtrim($this->StyleCellTitle) == '')?'':$this->StyleCellTitle?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					switch ($aField->VisualComponent)
					{
		    			case "Radio":
							if (is_null($aField->Parent))
							{
								if($aField->IsVisible)
								{
									$styleDiv = "";
									$strStyleDiv = "";
								}
								else 
								{
									$styleDiv = "display:none;";
									$strStyleDiv = "style=\"display:none;\"";
								}
?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
								foreach ($aField->Options as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$checked = "checked";
									}
									else
									{
										$checked = "";
									}
?>
							<input type="radio"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
							else
							{
								if($aField->IsVisible)
								{
									$styleDiv = "";
								}
								else 
								{
									$styleDiv = "display:none;";
								}

?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
								$aFieldParentName = $aField->Parent->Name;
    							$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
								if (array_key_exists($aFieldParentValue, $aField->Options))
								{
									$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
								}
								else
								{
									$aSelectedFieldOptions = array();
								}
								foreach ($aSelectedFieldOptions as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$checked = "checked";
									}
									else
									{
										$checked = "";
									}
?>
							<input type="radio"
						        	id="<?= $aField->IsDisplayOnly ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
?>
					    </div>
<?
							break;
		    			case "Combobox":
		    			case "List":
				    	default:
				    		//@JAPR 2014-10-28: Agregado el soporte de FixedWidth para campos tipo Object->ComboBox/List
							//@JAPR 2014-10-29: Corregido un bug, agregando los dos Styles no estaba funcionando, se tenía que agregar como uno solo
							//@JAPR 2014-10-31: Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
							//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
							//su tamaño provocaba que si se considerara menor que 0px debido a eso
?>
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        style="
						        <?= $aField->IsVisible ? "" : "display:none;" ?> 
						        <?= (!is_null($aField->FixedWidth) && $aField->FixedWidth != '0')? "width:".$aField->FixedWidth.";" : "" ?>
						        "
						        <?= (!is_null($aField->FixedWidth))? " fixedWidth=\"".$aField->FixedWidth."\"" : "" ?>
								name="<?= $aField->Name ?>" <?= (!is_null($aField->FixedWidth))? " style=\"width:".$aField->FixedWidth."\"" : "" ?> 
<?
							$aBackgroundStyle = '';
							if (!is_null($aField->BackgroundColor))
							{
								$aBackgroundStyle = "style=\"background-color:".$aField->BackgroundColor."\"";
								echo ($aBackgroundStyle);
							}

							if ($aField->VisualComponent == "List")
							{
						    	if (is_null($aField->Parent))
						    	{
?>
							size="<?= count($aField->Options) > $aField->OptionsSize ? $aField->OptionsSize + 1 : (count($aField->Options) < 2 ?  2 : count($aField->Options) + 1) ?>"
<?
						    	}
						    	else
						    	{
?>
							size="<?= $aField->OptionsSize + 1 ?>"
<?
						    	}
							}
?>
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							if (is_null($aField->Parent))
							{
								foreach ($aField->Options as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$selectedOption = "selected";
									}
									else
									{
										$selectedOption = "";
									}
?>
							<option <?= $selectedOption ?> value="<?= $key ?>" <?=$aBackgroundStyle?>><?= $value ?></option>
<?
								}
							}
							else
							{
								$aFieldParentName = $aField->Parent->Name;
    							$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
								if (array_key_exists($aFieldParentValue, $aField->Options))
								{
									$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
								}
								else
								{
									$aSelectedFieldOptions = array();
								}
								foreach ($aSelectedFieldOptions as $key => $value)
								{
									if ($key == $aFieldValue)
									{
										$selectedOption = "selected";
									}
									else
									{
										$selectedOption = "";
									}
?>
							<option <?= $selectedOption ?> value="<?= $key ?>" <?=$aBackgroundStyle?>><?= $value ?></option>
<?
								}
							}
?>
						</select>
<?
							break;
					}
?>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Array":
					$aFieldValue = $this->get_FieldValueOf($aFieldName);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
<?
					if (is_null($aField->Parent))
					{
						if($aField->IsVisible)
						{
							$styleDiv = "";
						}
						else 
						{
							$styleDiv = "display:none;";
						}

?>
						<div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" <?= ($aField->OptionsSize > 0 && (count($aField->Options) > $aField->OptionsSize)) ? "style=\"overflow:auto;border:1px solid #666;height:".(20 + (20 * $aField->OptionsSize))."px;".$styleDiv."\"" : "style=\"overflow:auto;border:1px solid #666;".$styleDiv."\"" ?>>
<?
						switch($aField->VisualComponent){
							case 'Radio':
								
								$hasChecked = FALSE;
								
								foreach ($aField->Options as $key => $value)
								{
									if (in_array($key, $aFieldValue))
									{
										$hasChecked = true; 
										$checked = "checked";
									}
									else
									{
										$hasChecked = $hasChecked ? $hasChecked : false; 
										$checked = "";
									}
	?>
								<input type="radio"
							        	id="<?= $aField->IsDisplayOnly ?>"
							        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
										name="<?= $aField->Name ?>[]"
										value="<?= $key ?>" <?= $checked ?>
										<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
										>&nbsp;<?= $value ?><br>
	<?
								}								
								
								break;
							default:

						if($aField->IsSingleDimensionalOptionsArray==true)
						{
							$hasChecked = false;
							foreach ($aField->Options as $key => $value)
							{
								if (in_array($key, $aFieldValue))
								{
									$hasChecked = true; 
									$checked = "checked";
								}
								else
								{
									$hasChecked = $hasChecked ? $hasChecked : false; 
									$checked = "";
								}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
							}
						}
						else 
						{
							$hasChecked = false;
							foreach ($aField->Options as $arrayKey => $arrayValues)
							{
								foreach ($arrayValues as $key => $value)
								{
									if (in_array($key, $aFieldValue))
									{
										$hasChecked = true; 
										$checked = "checked";
									}
									else
									{
										$hasChecked = $hasChecked ? $hasChecked : false; 
										$checked = "";
									}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
						        	<?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									otherid="<?= $arrayKey ?>"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
								}
							}
						}
						}
					}
					else
					{
						if($aField->IsVisible)
						{
							$styleDiv = "";
						}
						else 
						{
							$styleDiv = "display:none;";
						}

?>
					    <div id="<?= $myFormName ?>_<?= $aField->Name ?>_Container" style="overflow:auto;border:1px solid #666;height:<?= 20 + (20 * $aField->OptionsSize) ?>px;<?=$styleDiv?>">
<?
						$aFieldParentName = $aField->Parent->Name;
    					$aFieldParentValue = $this->get_FieldValueOf($aFieldParentName);
						if (array_key_exists($aFieldParentValue, $aField->Options))
						{
							$aSelectedFieldOptions = $aField->Options[$aFieldParentValue];
						}
						else
						{
							$aSelectedFieldOptions = array();
						}
						$hasChecked = false;
						foreach ($aSelectedFieldOptions as $key => $value)
						{
							if (in_array($key, $aFieldValue))
							{
								$hasChecked = true; 
								$checked = "checked";
							}
							else
							{
								$hasChecked = $hasChecked ? $hasChecked : false; 
								$checked = "";
							}
?>
							<input type="checkbox"
						        	id="<?= $aField->IsDisplayOnly ?>"
							        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
									name="<?= $aField->Name ?>[]"
									value="<?= $key ?>" <?= $checked ?>
									<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onclick=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
									>&nbsp;<?= $value ?><br>
<?
						}
					}
?>
					      	<input type="hidden" name="<?= $aField->Name ?>_EmptySelection" value="<?= $hasChecked ? "1" : "0" ?>">
					    </div>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
				case "Time":
					$aFieldName_Hours = $aFieldName."_Hours";
					$aFieldValue_Hours = $this->get_FieldValueOf($aFieldName_Hours);
    				$aFieldName_Minutes = $aFieldName."_Minutes";
					$aFieldValue_Minutes = $this->get_FieldValueOf($aFieldName_Minutes);
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name."_Hours" ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							for ($i = 0; $i < 24; $i++)
							{
								if ($i == $aFieldValue_Hours)
								{
										$selectedOption = "selected";
								}
								else
								{
									$selectedOption = "";
								}
?>
							<option <?= $selectedOption ?> value="<?= sprintf("%02d", $i) ?>"><?= sprintf("%02d", $i) ?></option>
<?
							}
?>
						</select>
						:
						<select
						        id="<?= $aField->IsDisplayOnly ?>"
						        <?= $aField->IsVisible ? "" : "style=\"display:none\"" ?> 
								name="<?= $aField->Name."_Minutes" ?>"
								<?= ($aField->OnChange != "" || count($aField->Children) > 0 || $aField->SubmitOnChange) ? "onchange=\"javascript: return ".$myFormName."_".$aField->Name."_".$aFieldIdx."_OnChange();\"" : "" ?>
								>
<?
							for ($i = 0; $i < 60; $i++)
							{
								if ($i == $aFieldValue_Minutes)
								{
										$selectedOption = "selected";
								}
								else
								{
									$selectedOption = "";
								}
?>
							<option <?= $selectedOption ?> value="<?= sprintf("%02d", $i) ?>"><?= sprintf("%02d", $i) ?></option>
<?
							}
?>
						</select>
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
   				default:
					if ($aField->InTitle)
					{
						if ($inCell)
						{
							$inCell = false;
?>
					</td>
<?
							if ($rowCols > 1)
							{
?>
				</tr>
<?
								$rowCols = 0;
							}
							if ($rowCols < 1)
							{
?>
        		<tr>
<?
							}
						}
						$cellCols = 0;
					}
					else
					{
						if ($inCell)
						{
							$cellCols = 0;
						}
						else
						{
							$cellCols = $aField->FullRow || !$aField->CloseCell ? 2 : 1;
							if ($rowCols + $cellCols > 2)
							{
								if ($inTitle)
								{
									$inTitle = false;
?>
					</td>
<?
								}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
				<tr>
<?
								$rowCols = 0;
							}
						}
					}
    				if (!$inTitle && !$inCell)
    				{
?>
					<td class="<?= (rtrim($aField->Title) == "")?"":"object_field_title"?>" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>>
<?
    				}
					$inTitle = $aField->InTitle;
					if (!$inTitle && !$inCell)
					{
?>
						<?= $aField->Title ?>
					</td>
					<td colspan="<?= $cellCols * 2 - 1 ?>" class="object_field_value_<?= $cellCols * 2 - 1 ?>">
<?
					}
?>
						<?= $aField->BeforeMessage ?>
					    &nbsp;
						<?= $aField->AfterMessage ?>
<?
					if (!$inTitle)
					{
						$inCell = !$aField->CloseCell;
    					if (!$inCell)
    					{
?>
					</td>
<?
    					}
					}
					break;
			}
			$rowCols += $cellCols;
			$aFieldIdx++;
		}
		switch ($rowCols)
		{
			case 0:
				if ($inTitle)
				{
					$inTitle = false;
?>
					</td>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
<?
				}
				break;
			case 1:
				if ($inTitle)
				{
					$inTitle = false;
?>
					</td>
<?
				}
?>
					<td colspan="2" class="object_field_value_2">
					    &nbsp;
					</td>
				</tr>
<?
				break;
			case 2:
?>
				</tr>
<?
				break;
		}
?>
			</table>
<?
		$this->generateInsideFormCode($aUser);
?>
		</form>
 	    <script language="javascript">
		 	<?= $myFormName ?>_Cancel();

<?
		if ($this->isNewObject() || $this->Mode == self::MODE_EDIT)
		{
?>
   	        <?= $myFormName ?>_Edit();
<?
		}
?>
<?
		if ($this->Message != "")
		{
?>
   	        alert('<?= str_replace("'", "\'", $this->Message) ?>');
<?
		}
?>
		</script>
<?
		$this->generateAfterFormCode($aUser);
		//@JAPR 2014-10-28: Agregado el soporte de FixedWidth para campos tipo Object->ComboBox/List
?>
		<script language="javascript">
		var arraySelects = document.getElementsByTagName('select');
		var numSelect = arraySelects.length;
		
		for(var iSelect=0; iSelect<numSelect; iSelect++)
		{
			newNode = arraySelects[iSelect].cloneNode(true);
			document.body.appendChild(newNode);
			if(newNode.offsetWidth < 200 && !arraySelects[iSelect].getAttribute('fixedWidth'))
			{
				arraySelects[iSelect].style.width = "200px";
			}
			document.body.removeChild(newNode);
		}
		
		</script>
	</div>
<?
	}
}

class BITAMCollection extends BITAMObject implements ArrayAccess, IteratorAggregate
{   
	public $Collection;
	public $IsDialog;
	public $HasSupportValue;
	public $OrderPosField;
	public $MsgCancelRemove;
	public $Mode;
	public $MsgConfirmRemove;
	public $MsgNoSelectedRemove;
	public $MsgByElement;
	public $MsgValidateReorder;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->Collection = array();
		$this->IsDialog=false;
		$this->HasSupportValue=false;
		$this->OrderPosField="";
		$this->MsgCancelRemove = "";
		$this->Mode = "list";
		$this->MsgConfirmRemove = "";
		$this->MsgNoSelectedRemove = "";
		$this->MsgByElement=array();
		$this->MsgValidateReorder="";
	}
	
	function getIterator()
	{
		return new ArrayIterator($this->Collection);
	}
	
    function offsetExists($offset)
    {
        if (isset($this->Collection[$offset]))
        {
            return true;
        }
        return false;
    }

    function offsetGet($offset)
    {
        return $this->Collection[$offset];
    }

    function offsetSet($offset, $value)
    {
		if ($offset)
		{
			$this->Collection[$offset] = $value;
		}
		else
		{
			$this->Collection[] = $value;
		}
    }
    
	function offsetUnset($offset)
    {
        unset($this->Collection[$offset]);
    }
    
    function get_AssociatedIDs()
    {
    	return array();
    }
    
    function get_AddRemoveQueryString()
    {
    	return null;
    }

    function get_AssociateDissociateQueryString()
    {
    	return null;
    }
    
	function canAdd($aUser)
	{
		return true;
	}

	function canAssociateDissociate($aUser)
	{
		return true;
	}

	function canRemove($aUser)
	{
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	function get_Image()
	{
		return "";
	}

	//@JAPR 2008-11-24: Agregada esta propiedad para casos de dimensiones tipo SmallMoney o Flotantes que no pueden utilizar sus claves
	//en llamadas a métodos como array_key_exists que no aceptan dichos tipos de dato como parámetro
	function mustForceStringKeys()
	{
		return false;
	}
	//@JAPR
	
	function displayCheckBox($aUser)
	{
		return true;
	}
	
	function afterTitleCode($aUser)
	{
	}
	
	function selectionButtons($aUser){
	}
	
	function addButtons($aUser)
	{
	}
	
	function addButtonsAtBegin($aUser)
	{
	}
	
	function generateInsideTableCode($aUser)
	{
	}
	
	function generateInsideFormCode($aUser)
	{
	}
    
    function generateForm($aUser)
    {
		if (!is_null($this->get_AssociateDissociateQueryString()))
		{
			return $this->generateDetailForm($aUser, true);
		}
		return parent::generateForm($aUser);
    }
    
    function generateBeforeFormCode($aUser)
    {
    }

    
    function generateAfterFormCode($aUser)
    {
    }

    function generateCustomAnchorCode($aUser)
    {
    	
    }
    
    function inTitleDivCode($aUser)
    {
    	
    }
    
	public function setUpPagination($filter){
		
		$config_filename = realpath('.') . DIRECTORY_SEPARATOR;
		$config_filename .= 'config' . DIRECTORY_SEPARATOR . "paginator.ini";		
		
		if($this instanceOf KPA_PagedCollection_Interface && file_exists($config_filename) ){
		
			$count_rows_rs = $this->Repository->DataADOConnection->Execute($this->getCountRowsSQL($filter));
	
			if ($count_rows_rs === false)
			{
				die( translate("Error accessing")." : ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$this->getCountRowsSQL());
			}
			
			$total_rowcount = $count_rows_rs->fields['total'];
			
			$config = @parse_ini_file($config_filename);
			
			$this->row_count = isset($config['rowcount']) ? $config['rowcount'] : 25;
			$this->total_rowcount = $total_rowcount;
			$count_pages = ceil($total_rowcount / $this->row_count);
			
			$this->offset_count = floor($count_pages) == 0 ? 1 : (int)$count_pages;
		
		} else {
			$this->row_count = FALSE;
		}
		
	}    
    
    function validateReorder()
    {
    	
    }
	
    function generateDetailForm($aUser, $isAssociateDissociateMode = false)
    {
    	require_once("formfield.inc.php");
    	
    	$myFormName = get_class($this);
    	$myFormTitle = $this->get_Title();
		$myFormIDFieldName = $this->get_FormIDFieldName();
		$mySupportFormIDFieldName = $this->get_SupportFormIDFieldName();
    	$myFormFields = $this->get_FormFields($aUser);
    	
    	if($this->displayCheckBox($aUser)===false)
    	{
    		$styleDisplayCheckBox='style="display:none"';
    	}
    	else
    	{
    		$styleDisplayCheckBox="";
    	}
    	
?>
	<div class="collection" id="<?= $myFormName ?>" <?if (!is_null($this->get_AssociateDissociateQueryString()) && $this->canAssociateDissociate($aUser)) {?>style="overflow:scroll;height:100%"<?}?>>
<?
	$this->generateBeforeFormCode($aUser);
?>
	    <script language="javascript">
			function <?= $myFormName ?>_ToggleSelect()
			{
				var elements = <?= $myFormName ?>_SelectForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
		 			if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 			{
		 				elements[i].checked = <?= $myFormName ?>_SelectForm.toggle_select.checked;
		 				elements[i].disabled = elements[i].checked;
   					}
				}
			}
  	        function <?= $myFormName ?>_Remove()
	        {
	        	var selected = 0;
				var toignore = 0;
				var elements = <?= $myFormName ?>_SelectForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
		 			if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 			{
		 				if (elements[i].checked)
		 				{
		 					selected++;
		 					if (elements[i].id == "0")
		 					{
		 						toignore++;
		 					}
		 				}
   					}
				}
				if (selected == 0)
				{
				<?
					if(trim($this->MsgNoSelectedRemove)!="")
					{
?>
					alert('<?= str_replace("'", "\'", translate($this->MsgNoSelectedRemove)) ?>');
<?
					}
					else 
					{
?>
					alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to remove"), $myFormTitle)) ?>');
<?
					}
				?>				

					return;
				}
				<?
					if(trim($this->MsgCancelRemove)!="")
					{
						$strAllDelete = $this->MsgCancelRemove;
					}
					else 
					{
						$strAllDelete = translate("No selected %s can be removed");
					}			
				?>
				if (selected == toignore)
				{
					alert('<?= str_replace("'", "\'", sprintf($strAllDelete, $myFormTitle)) ?>');
					return;
				}
				<?
					if(trim($this->MsgCancelRemove)!="")
					{
						$strSomeDelete = translate("Some %s can not be removed")." ".translate("because")." ".$this->MsgCancelRemove.", ".translate("they will be deselected");
					}
					else 
					{
						$strSomeDelete = translate("Some %s can not be removed, they will be deselected");
					}			
				?>				
				if (toignore != 0)
				{
					alert('<?=str_replace("'", "\'", sprintf($strSomeDelete, $myFormTitle)) ?>');
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
		 					if (elements[i].id == "0")
		 					{
		 						elements[i].checked = false;
		 					}
   						}
					}
				}
				
				<?
					if(trim($this->MsgConfirmRemove)!="")
					{
?>
						var msgElements = "";
						var MsgByElement= new Array();
<?
						if(count($this->MsgByElement)>0)
						{
							foreach($this->MsgByElement as $msgElemID => $msg)
							{
?>
								MsgByElement[<?=$msgElemID?>] = "<?=$msg?>";
<?								
							}
?>
							var elements = <?= $myFormName ?>_SelectForm.elements;
							
							for (var i = 0; i < elements.length; i++)
							{
				 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
				 				{
				 					if (elements[i].checked == true)
				 					{
				 						if(MsgByElement[elements[i].value]!=undefined)
				 						{
				 							msgElements+=MsgByElement[elements[i].value];
				 						}
				 					}
								}
							}
<?							
						}
?>
						if(MsgByElement.length>0)
						{
							if(msgElements!="")
							{
								//14Feb2013: Si hay mensajes por elementos entonces 
								//MsgConfirmRemove se utiliza sólo cuando alguno de los elementos a remover
								//tiene mensaje personalizado los cuales están concatenados en msgElements
								var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmRemove)) ?>'+msgElements);
							}
							else
							{
								var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to remove all selected %s?"), $myFormTitle)) ?>');
							}
							
						}
						else
						{
							var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmRemove)) ?>');
						}
<?
					}
					else 
					{
?>
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to remove all selected %s?"), $myFormTitle)) ?>');
<?
					}
				?>
				
				if (answer)
				{
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
							elements[i].disabled = false;
   						}
					}
					<?= $myFormName ?>_SelectForm.action = '<?= 'main.php?'.$this->get_AddRemoveQueryString() ?>';
                	<?= $myFormName ?>_SelectForm.submit();
				}
			}
			
<?
		if (trim($this->OrderPosField) != "")
		{
?>
			var <?= $myFormName ?>_oDraggedObj = null;
			var <?= $myFormName ?>_nLastScreenXClicked = 0;
			var <?= $myFormName ?>_nLastScreenYClicked = 0;

  	        function <?= $myFormName ?>_Reorder(objectID, orderPos)
	        { 
	        	var canReorder = true;
<?
				//En esta función se puede realizar una validación para asignar el valor a la variable canDrop
				//y definir si es valido realizar el reordenamiento
				$this->validateReorder();
?>
	        	<?= $myFormName ?>_ReorderForm.<?= $myFormIDFieldName ?>.value = objectID;
				<?= $myFormName ?>_ReorderForm.<?= $this->OrderPosField ?>.value = orderPos;
				<?= $myFormName ?>_releaseDrag();
				
				if(canReorder)
				{
					<?= $myFormName ?>_ReorderForm.submit();
				}
				else
				{
					alert('<?=$this->MsgValidateReorder?>');
				}
			}
			
			function <?= $myFormName ?>_InitEventsToReorder()
	        {
	        	var i, objChild, objParent;
    			var arrayImgReorder = document.getElementsByName('<?= $myFormName ?>_imgReorder');

    			if(arrayImgReorder!=undefined && arrayImgReorder!=null)
    			{
    				for(i=0; i<arrayImgReorder.length; i++)
    				{
    					objChild = arrayImgReorder[i];
    					
    					if(bIsIE)
						{
							objChild.attachEvent("ondragover", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondragenter", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondragstart", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("ondrop", function(evt) { evt.returnValue = false; });
							objChild.attachEvent("onmousemove", function(evt) { <?= $myFormName ?>_mousemove(evt); });
							objChild.attachEvent("onmouseout", function() { <?= $myFormName ?>_mouseout(); });
							objChild.attachEvent("onmousedown", function(evt) { <?= $myFormName ?>_mousedown(evt); });
							objChild.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_mouseup(evt); });
							
							objParent = objChild.parentElement;
							objParent.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_tdmouseup(evt); } );
							objParent.attachEvent("onmousemove", function(evt) { <?= $myFormName ?>_tdmousemove(evt); } );
						}
						else
						{
							objChild.addEventListener("mousemove", function(evt) { <?= $myFormName ?>_mousemove(evt); }, true);
							objChild.addEventListener("mouseout", function() { <?= $myFormName ?>_mouseout(); }, true);
							objChild.addEventListener("mousedown", function(evt) { <?= $myFormName ?>_mousedown(evt); }, true);
							objChild.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_mouseup(evt); }, true);
							
							objParent = objChild.parentNode;
							objParent.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_tdmouseup(evt); }, true);
							objParent.addEventListener("mousemove", function(evt) { <?= $myFormName ?>_tdmousemove(evt); }, true);
						}
    				}
    			}

    			if (bIsIE)
				{
					document.body.attachEvent("onmouseup", function(evt) { <?= $myFormName ?>_releaseDrag(); } );
					document.body.attachEvent("onkeypress", function(evt) { <?= $myFormName ?>_body_keypress(evt); } );
				}
				else
				{
					document.documentElement.addEventListener("mouseup", function(evt) { <?= $myFormName ?>_releaseDrag(); }, false );
					document.documentElement.addEventListener("keypress", function(evt) { <?= $myFormName ?>_body_keypress(evt); }, false );
				}
			}
			
			function <?= $myFormName ?>_mousemove(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj == null && <?= $myFormName ?>_nLastScreenXClicked != 0 && (<?= $myFormName ?>_nLastScreenXClicked != evt.screenX || <?= $myFormName ?>_nLastScreenYClicked != evt.screenY))
				{
					<?= $myFormName ?>_oDraggedObj = bIsIE ? evt.srcElement.parentElement : evt.target.parentNode;
				}
			}
			
			function <?= $myFormName ?>_mouseout()
			{

			}

			function <?= $myFormName ?>_mousedown(evt)
			{
				if (bIsIE)
				{
					if (evt.button != 1) 
					{
						return;
					}
					
					evt.returnValue = false;
				}
				else
				{
					if (evt.button != 0) 
					{
						return;
					}
					evt.preventDefault();
				}
				
				<?= $myFormName ?>_nLastScreenXClicked = evt.screenX;
				<?= $myFormName ?>_nLastScreenYClicked = evt.screenY;
			}

			function <?= $myFormName ?>_mouseup(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					<?= $myFormName ?>_ObjDropped(evt);
				}
			}

			function <?= $myFormName ?>_releaseDrag()
			{
				<?= $myFormName ?>_oDraggedObj = null;
				<?= $myFormName ?>_releaseHint();
				<?= $myFormName ?>_nLastScreenXClicked = 0;
				<?= $myFormName ?>_nLastScreenYClicked = 0;
			}
			
			function <?= $myFormName ?>_releaseHint()
			{
				var oHint = document.getElementById("<?= $myFormName ?>_Hint");
				var oOutline = document.getElementById("<?= $myFormName ?>_Outline");
				oHint.innerHTML = '';
				oHint.style.display = "none";
				oOutline.style.display = "none";
			}
			
			function <?= $myFormName ?>_ObjDropped(evt)
			{
				if (bIsIE)
				{
					evt.cancelBubble = true;
				}
				else
				{
					evt.stopPropagation();
				}
				
				<?= $myFormName ?>_releaseHint()
				
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					var osrc = bIsIE ? evt.srcElement : evt.target;
					
					var objectId = parseInt(<?= $myFormName ?>_oDraggedObj.getAttribute("id"));
					var objectPos = parseInt(<?= $myFormName ?>_oDraggedObj.getAttribute("orderpos"));
				    var position = parseInt(osrc.getAttribute("orderpos"));

				    if(objectPos!=position)
				    {
						// a drop action has just happened!
						<?= $myFormName ?>_Reorder(objectId, position);
				    }
				    else
				    {
				    	<?= $myFormName ?>_releaseDrag();
				    }
				}
			}
			
			function <?= $myFormName ?>_tdmousemove(evt)
			{
				var osrc, position;
				if (<?= $myFormName ?>_oDraggedObj == null) 
				{ 
					return;
				}
				
				if (<?= $myFormName ?>_nLastScreenXClicked == evt.screenX && <?= $myFormName ?>_nLastScreenYClicked == evt.screenY)
				{
					return;
				}
				
				var oOutline = document.getElementById("<?= $myFormName ?>_Outline");
				var oHint = document.getElementById("<?= $myFormName ?>_Hint");
				
				oOutline.style.left = (document.body.scrollLeft + evt.clientX - 7) + "px";
				oOutline.style.top = (document.body.scrollTop + evt.clientY - 15) + "px";
				oOutline.style.display = "inline";
				
				oHint.style.left = (document.body.scrollLeft + evt.clientX - 40) + "px";
				oHint.style.top = (document.body.scrollTop + evt.clientY - 30) + "px";
				oHint.style.display = "inline";
				
				osrc = bIsIE ? evt.srcElement : evt.target;
				
				position = osrc.getAttribute("orderpos")
				oHint.innerHTML = "<nobr><?=translate("Position")?> " + position + "</nobr>"
			}

			function <?= $myFormName ?>_tdmouseup(evt)
			{
				if (<?= $myFormName ?>_oDraggedObj != null)
				{
					<?= $myFormName ?>_ObjDropped(evt);
				}
			}
			
			function <?= $myFormName ?>_body_keypress(evt)
			{
				if (evt.keyCode == 27)
				{
					if (<?= $myFormName ?>_oDraggedObj != null)
					{
						<?= $myFormName ?>_releaseDrag();
					}
				}
			}
<?
		}
?>

  	        function <?= $myFormName ?>_Dissociate()
	        {
				var selected = 0;
				var elements = <?= $myFormName ?>_SelectForm.elements;
				for (var i = 0; i < elements.length; i++)
				{
		 			if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 			{
		 				if (elements[i].checked)
		 				{
		 					selected++;
		 				}
   					}
				}
				if (selected == 0)
				{
					alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to dissociate"), $myFormTitle)) ?>');
					return;
				}
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to dissociate all selected %s?"), $myFormTitle)) ?>');
				if (answer)
				{
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
		 					elements[i].disabled = false;
   						}
					}
					<?= $myFormName ?>_SelectForm.action = '<?= 'main.php?action=dissociate&'.$this->get_AssociateDissociateQueryString() ?>';
                	<?= $myFormName ?>_SelectForm.submit();
				}
			}
  	        function <?= $myFormName ?>_Associate()
	        {
				var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to associate all selected %s?"), $myFormTitle)) ?>');
				if (answer)
				{
					var elements = <?= $myFormName ?>_SelectForm.elements;
					for (var i = 0; i < elements.length; i++)
					{
		 				if (elements[i].name == "<?= $myFormIDFieldName ?>[]")
		 				{
		 					elements[i].disabled = false;
   						}
					}
					<?= $myFormName ?>_SelectForm.action = '<?= 'main.php?action=associate&'.$this->get_AssociateDissociateQueryString() ?>';
                	<?= $myFormName ?>_SelectForm.submit();
				}
			}

<?
		if ($isAssociateDissociateMode)
		{
?>
	        window.returnValue = false;
	        
  	        function <?= $myFormName ?>_AssociateDone(answer)
	        {
	        	if (answer != undefined && answer != '')
	        	{
	        		if (parent && parent.window && parent.window.opener) {
	        			parent.window.opener.dialogValue = true;
	        		}
	        		window.returnValue = true;
	        		top.close();
	        	}
	        }

  	        function <?= $myFormName ?>_Cancel()
	        {
				top.close();
	        }
<?
		}
?>
	    function body_click()
		{	
		}
		</script>
	    <div class="collection_title" id="collection_title_div">
			<?= $this->get_Image().$myFormTitle ?>&nbsp;<?$this->inTitleDivCode($aUser)?>
 		</div>
<?
		$this->afterTitleCode($aUser)
?>
	    <hr class="collection_title_hr" />
		<div id="divCollectionButtons" class="collection_buttons">
<?
		$this->addButtonsAtBegin($aUser);
		
		if ($isAssociateDissociateMode)
		{
?>
	    	<button title="<?= translate("Save") ?>" class="collection_okButton" onclick="javascript: <?= $myFormName ?>_Associate();"><img src="images/ok.png" alt="<?= translate("Save") ?>" title="<?= translate("Save") ?>" displayMe="1" /> <?= translate("Save") ?></button>
			<button title="<?= translate("Cancel") ?>" class="collection_cancelButton" onclick="javascript: <?= $myFormName ?>_Cancel();"><img src="images/cancel.png" alt="<?= translate("Cancel") ?>" title="<?= translate("Cancel") ?>" displayMe="1" /> <?= translate("Cancel") ?></button>
<?
			$collectionCanRemove = false;
		}
		else
		{
			$collectionCanRemove = false;
			if (!is_null($this->get_AssociateDissociateQueryString()) && $this->canAssociateDissociate($aUser))
			{
?>
				    	<button title="<?= translate("Associate") ?>"

								class="collection_associateButton" 

								onclick="javascript: 
				        			window.dialogValue = undefined;
									var ok = showModalDialog('dialog.html', '<?= 'main.php?action=select&'.$this->get_AssociateDissociateQueryString() ?>', 'help: no; status: no; scroll: yes; resizable: yes; dialogHeight:400px; dialogWidth:700px;');
									if (ok == undefined) {
										ok = window.dialogValue;
									}
										if (ok)
										{
											document.location.reload(true);
										}">
							<img src="images/associate.png" alt="<?= translate("Associate") ?>" title="<?= translate("Associate") ?>" displayMe="1" /> <?= translate("Associate") ?>
                        </button>

						<button title="<?= translate("Dissociate") ?>" 
								class="collection_dissociateButton" 

								onclick="javascript: <?= $myFormName ?>_Dissociate();">
							<img src="images/dissociate.png" alt="<?= translate("Dissociate") ?>" title="<?= translate("Dissociate") ?>" displayMe="1" /> <?= translate("Dissociate") ?>
                        </button>
<?
			}
			if (!is_null($this->get_AddRemoveQueryString()))
			{
				$collectionCanRemove = $this->canRemove($aUser);
				if ($collectionCanRemove && !$this->hideRemoveButton($aUser))
				{
?>
					<div class="collectionButtonsSelection">
                   		<img class="collectionSelectionPointer" src="images/selectionPointer.gif" alt="" title="" />
				    	<button id="<?= $myFormName ?>_RemoveButton" class="collection_removeButton" title="<?= translate("Remove") ?>" onclick="javascript: <?= $myFormName ?>_Remove();"><img src="images/remove.png" alt="<?= translate("Remove") ?>" title="<?= translate("Remove") ?>" displayMe="1" /> <?= translate("Remove") ?></button>
				    	
				    	<?php $this->selectionButtons($aUser) ?>
				    	
					</div>
<?
				}
				if ($this->canAdd($aUser))

				{				
?>
				    	<button class="collection_addButton" title="<?= translate("New") ?>" onclick="javascript: window.location = '<?= 'main.php?'.$this->get_AddRemoveQueryString() ?>';"><img src="images/add.png" alt="<?= translate("New") ?>" title="<?= translate("New") ?>" displayMe="1" /> <?= translate("New") ?></button>
<?
				}
			}
		}
		$this->addButtons($aUser);
?>		
		<br>
		</div>
		
<?
	if (trim($this->OrderPosField) != "")
	{
?>
  		<form id="<?= $myFormName ?>_ReorderForm" action="<?= 'main.php?'.$this->get_QueryString()?>" method="post">
			<input type="hidden" name="<?= $myFormIDFieldName ?>" value="0">
			<input type="hidden" name="<?= $this->OrderPosField ?>" value="0">
  			<input type="hidden" name="go" value="parent">
		</form>
<?
	}
?>

<?
		if ($isAssociateDissociateMode)
		{
?>
		<iframe name="<?= $myFormName ?>_AssociateIFrame" style="display:none" onload="javascript: <?= $myFormName ?>_AssociateDone(<?= $myFormName ?>_AssociateIFrame.document.documentElement.outerText == undefined ? <?= $myFormName ?>_AssociateIFrame.document.body.innerHTML : <?= $myFormName ?>_AssociateIFrame.document.documentElement.outerText);">
		</iframe>
<?
		}
?>

<?php 		
		if($this instanceOf KPA_PagedCollection_Interface && $this->row_count !== FALSE){

			$offset_last = $this->offset_first + count($this->Collection);
			
			$pagina = floor($this->offset_first / $this->row_count);
		?>
		 
			<div class="paginator">
			<?php	
			if($this->offset_count > 1){
			?>
			
			<?php
			
			if($pagina == 0){
				//no hay anterior
			} else {
			?>
			
			<a onclick="<?= $this->getPaginatorAction('prev',array('offset' => ($pagina-1)*$this->row_count)) ?>"><span class="weekNav"> &lt; </span></a>
			
			<?php 
			}
			
			?> 
			
			<select onchange="<?= $this->getPaginatorAction('select') ?>" id="<?= get_class($this); ?>_paginator">
			<?php 
				for($i=0 ; $i < $this->offset_count ; $i++){
?>				
					<option value='<?= ($i*$this->row_count) ?>' <?= ($i == $pagina) ? 'selected="selected"' : '' ?> ><?= $i+1 ?></option>
<?php 			
				}
?>
			</select>
				
			<?php
			
			if(($pagina + 1) == $this->offset_count){
				//no hay siguiente
			} else {
			?>
			
			<a onclick="<?= $this->getPaginatorAction('next',array('offset' => ($pagina+1)*$this->row_count)) ?>"><span class="weekNav"> &gt; </span></a>
			
			<?php 
				
			}
			
			?> 
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?php 
			}
?>
			<?= $this->offset_first + 1 ?> - <?= $offset_last?> of <?= $this->total_rowcount ?>
			</div>
<?php
		} 
?>

		<form 
				id="<?= $myFormName ?>_SelectForm" 
				method="post"
				<?= $isAssociateDissociateMode ? "target=\"".$myFormName."_AssociateIFrame\"" : "" ?>
				>
 	    	<table id="<?= $myFormName ?>_Table" class="collection_table">
 	    		<thead>
<?
				//No separar tr y th de la siguiente linea porque causa error en ciertas funciones de JS que accesan elementos por childNodes y firstChild
				//ya que me regresaria como primer elemento el enter (\n)
?>
 	        	<tr id="tr_collection_checkbox" name="tr_collection_checkbox"><th <?=$styleDisplayCheckBox?> class="collection_title_checkbox" name="td_collection_checkbox"><input type="checkbox" name="toggle_select" onclick="<?= $myFormName ?>_ToggleSelect();"></th>
					
<?
	if (trim($this->OrderPosField) != "")
	{
?>
					<th class="collection_title_reorder" onclick="javascript: NumberQuicksortTable(<?= $myFormName ?>_Table, 1);">&nbsp;</th>
<?
		$iCol = 2;
	}
	else
	{
		$iCol = 1;
	}
?>

<?
    	foreach($myFormFields as $aField)
    	{
			switch ($aField->Type)
			{
				case "Integer":
				case "Float":
?>
					<th id="<?=$myFormName?>_HeaderCol<?=$iCol?>" class="collection_title_number" onclick="javascript: NumberQuicksortTable(<?= $myFormName ?>_Table, <?= $iCol ?>);" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>><?= $aField->Title ?></th>
<?
					break;
				default:
?>
					<th id="<?=$myFormName?>_HeaderCol<?=$iCol?>" class="collection_field_title" onclick="javascript: StringQuicksortTable(<?= $myFormName ?>_Table, <?= $iCol ?>);" <?=(rtrim($aField->ToolTip) == '')?'':'title="'.$aField->ToolTip.'"'?> <?=(rtrim($aField->TextAlign) == '')?'':'style="text-align:'.$aField->TextAlign.'"'?>><?= $aField->Title ?></th>
<?
			    	break;
			}
			$iCol++;
		}
?>
				</tr>
				</thead>
				<tbody>
<?
		$iRow = 0;
		if ($isAssociateDissociateMode)
		{
			$mySelectedIDs = $this->get_AssociatedIDs();
		}
		else
		{
			$mySelectedIDs = array();
		}
    	foreach ($this->Collection as $anObject)
    	{
    		$anID = $anObject->get_FieldValueOf($myFormIDFieldName);
		    if($this->HasSupportValue==true)
    		{
    			$aSupportID = $anObject->get_FieldValueOf($mySupportFormIDFieldName);
    		}
    		
    		if ($collectionCanRemove)
    		{
    			$objectCanRemove = $anObject->canRemove($aUser);
    		}
    		else
    		{
    			$objectCanRemove = false;
    		}
    		if($anObject->NewSection==true)
    		{
    			$field_label_section	= $anObject->FieldSection;

    			$colspan = count($myFormFields)+1;

    			if(trim($this->OrderPosField)!=="")
    			{
    				$colspan++;
    			}

?>
				</tbody>
				<tbody id="agrupador_<?= $anObject->$field_label_section ?>">
					
					<tr>
					<td class="collectionGroupRow" colspan="<?=$colspan?>" style="VERTICAL-ALIGN:middle"><img id="groupImage" src="<?=$anObject->GroupImage?>" alt="" border=0>&nbsp;<?=$anObject->$field_label_section?></td>
					</tr>

				</tbody>
				<tbody>
<?
    		}
    			//No separar tr y td de la siguiente linea porque causa error en ciertas funciones de JS que accesan elementos por childNodes y firstChild
    			//ya que me regresaria como primer elemento el enter (\n)
?>
				<tr onMouseOver="this.className='tab-list-over'" onMouseOut="this.className='tab-list-out'" id="tr_collection_checkbox" name="tr_collection_checkbox"><td id="<?= $iRow ?>" <?=$styleDisplayCheckBox?> class="collection_value_checkbox" name="td_collection_checkbox">
<?
			if ($this->mustForceStringKeys())
			{
				$strAnID = '\''.$anID.'\'';
			}
			else
			{
				$strAnID = $anID;
			}

		    if($this->HasSupportValue==true)
    		{
?>
						<input id="<?= $objectCanRemove ? 1 : 0 ?>" type="checkbox" name="<?= $myFormIDFieldName ?>[]" <?= array_key_exists($strAnID, $mySelectedIDs) ? "checked" : "" ?> value="<?= $anID ?>" supportvalue="<?= $aSupportID ?>">
<?
    		}
    		else 
    		{
?>
						<input id="<?= $objectCanRemove ? 1 : 0 ?>" type="checkbox" name="<?= $myFormIDFieldName ?>[]" <?= array_key_exists($strAnID, $mySelectedIDs) ? "checked" : "" ?> value="<?= $anID ?>">
<?
    		}
?>
					</td>
					
<?
			if (trim($this->OrderPosField) != "")
			{
?>
					<td id="<?=$anID?>" class="collection_value_reorder" name="<?= $myFormName ?>_tdReorder" orderpos="<?= $anObject->get_FieldValueOf($this->OrderPosField);?>">
						<img id="<?= $anObject->get_FieldValueOf($this->OrderPosField);?>" src="images/reorder.gif" displayMe="1" name="<?= $myFormName ?>_imgReorder" border="0" orderpos="<?= $anObject->get_FieldValueOf($this->OrderPosField);?>">
					</td>
<?
			}
?>
					
					
<?
		    foreach($myFormFields as $aField)
    		{
    			$aFieldName = $aField->Name;
    			
    			$custom_td_class = $anObject->get_customTdClass($aFieldName);
?>
					<td id="<?= htmlentities($anObject->get_FieldValueOf($aFieldName)) ?>" class="<?= $aField->Type == "Integer" || $aField->Type == "Float" ? "collection_value_number" : "collection_field_value" ?> <?= $custom_td_class ?>">
<?
				if($aField->BeforeMessage!="")
				{
					$strCode = '$befMEs = $anObject->'.$aField->BeforeMessage.";";
					eval($strCode);
					echo ($befMEs);
				}

				$anObjectQueryString = $anObject->get_QueryString();
				if (!is_null($anObjectQueryString) && $aField->IsLink && !$isAssociateDissociateMode)
				{
?>
						<a href="<?= 'main.php?'.$anObjectQueryString ?>">
<?
				}
				elseif($aField->CustomAnchor!='')
				{
?>
						<a <?=$aField->CustomAnchor?> <?=$anObject->generateCustomAnchorCode($aUser)?>>
<?
				}

				//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (sólo para BITAMCollection)
				if ($aField->Image != null && $aField->Image != '')
				{
					echo ($anObject->get_FieldValueOf($aField->Image));
				}
				//@JAPR
				
				//echo ($anObject->get_FieldValueOf($aField->BeforeMessage));
				
				switch ($aField->Type)
				{
					case "String":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
						if ($aField->FormatMask != "")
						{
?>
					    <script language="javascript">
					        document.write(formatString('<?= is_null($aFieldValue) ? '<NULL>' : $aFieldValue ?>', '<?= $aField->FormatMask ?>'));
					    </script>
<?
						}
						else
						{
?>
						<?= is_null($aFieldValue) ? '<NULL>' : $aFieldValue ?>
<?
						}
					    break;
					case "Integer":
					case "Float":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
						if ($aField->FormatMask != "")
						{
							if($aField->DefaultNumberZero == true)
							{
?>
				    	<script language="javascript">
				        	document.write(formatNumber(<?= is_null($aFieldValue) ? 0 : $aFieldValue ?>, '<?= $aField->FormatMask ?>'));
				    	</script>
<?
							}
							else 
							{
								if(trim($aFieldValue)!="")
								{
?>
				    	<script language="javascript">
				        	document.write(formatNumber(<?= is_null($aFieldValue) ? 0 : $aFieldValue ?>, '<?= $aField->FormatMask ?>'));
				    	</script>
<?
								}
								else 
								{
?>
				    	<script language="javascript">
				        	document.write(<?=$aFieldValue?>);
				    	</script>
<?
								}
							}
						}
						else
						{
?>
						<?= is_null($aFieldValue) ? 0 : $aFieldValue ?>
<?
						}
						break;
					case "Date":
					case "DateTime":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
						if ($aField->FormatMask != "")
						{
?>
					    <script language="javascript">
					        document.write(formatDateTime('<?= is_null($aFieldValue) ? '0000-00-00 00:00:00' : $aFieldValue ?>', '<?= $aField->FormatMask ?>'));
					    </script>
<?
						}
						else
						{
?>
						<?= is_null($aFieldValue) ? '0000-00-00 00:00:00' : $aFieldValue ?>
<?
						}
						break;
					case "LargeString":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
?>
						<?= is_null($aFieldValue) ? '<NULL>' : nl2br(htmlentities($aFieldValue)) ?>
<?
						break;
					case "LargeStringHTML":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
?>
						<?= is_null($aFieldValue) ? '<NULL>' : nl2br(htmlentities($aFieldValue)) ?>
<?
						break;
					case "Boolean":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
?>
						<?= $aFieldValue == 0 ? "False" : "True" ?>
<?
						break;
					case "Object":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
						if (array_key_exists($aFieldValue, $aField->Options))
						{
						    $aFieldValue = $aField->Options[$aFieldValue];
						}
						else
						{
					    	$aFieldValue = null;
						}
?>
						<?= is_null($aFieldValue) ? '<NULL>' : $aFieldValue ?>
<?
						break;
					case "Array":
						$aFieldValue = $anObject->get_FieldValueOf($aFieldName);
    					foreach($aFieldValue as $aKey)
    					{
    						if (array_key_exists($aKey, $aField->Options))
    						{
    							$aValue = $aField->Options[$aKey];
    						}
    						else
    						{
    							$aValue = null;
    						}
?>
						<?= is_null($aValue) ? '<NULL>' : $aValue ?><br>
<?
    					}
						break;
					case "Time":
						$aFieldName_Hours = $aFieldName."_Hours";
						$aFieldValue_Hours = $anObject->get_FieldValueOf($aFieldName_Hours);
						$aFieldName_Minutes = $aFieldName."_Minutes";
						$aFieldValue_Minutes = $anObject->get_FieldValueOf($aFieldName_Minutes);
?>
						<?= sprintf("%02d:%02d", $aFieldValue_Hours, $aFieldValue_Minutes) ?>
<?
						break;
					default:
						if($aField instanceOf KPA_Form_Field_Interface){
							
							echo $aField->parse($anObject);
							
						} else {
							echo '<NULL>'; 
						}
						
						break;
				}
				if ($aField->AfterMessage!="")
				{
?> 					
					  <?=$aField->AfterMessage?>
					 
<?				}  
				if ((!is_null($anObjectQueryString) && $aField->IsLink && !$isAssociateDissociateMode)||($aField->CustomAnchor!=''))
				{
?>
						</a>
<?
				}
?>

					</td>
<?
			}
?>
				</tr>
<?
			$iRow++;
		}
		
		$this->generateInsideTableCode($aUser);
?>
			</tbody>
			</table>
<?
		$this->generateInsideFormCode($aUser);
?>
		</form>
		
<?
		if (trim($this->OrderPosField) != "")
		{
?>
		<script language="JavaScript">
			<?= $myFormName ?>_InitEventsToReorder();
		</script>
<?
		}
?>
		<?$this->generateAfterFormCode($aUser);?>
<?
		if ($this->Message != "")
		{
?>
 	    <script language="JavaScript">
   	        alert('<?= str_replace("'", "\'", $this->Message) ?>');
		</script>
<?
		}
?>
		<script language="JavaScript">
		//06Ene2014: Si en la colección no se desplegó ningún botón, que no se aplique estilo al DIV por que se desplegaba de color
		//azul aún cuando no había botones
		var objDivColBut = document.getElementById('divCollectionButtons');
		
		if(Trim(objDivColBut.innerHTML)=='<br>' || Trim(objDivColBut.innerHTML)=='')
		{
			objDivColBut.className='';
		}
		
		</script>
	</div>
	<img id="<?= $myFormName ?>_Outline" src="images/pointer.gif" style="position:absolute; display:none" width="11" height="11" displayMe="0">
	<div id="<?= $myFormName ?>_Hint" style="position:absolute; width:auto; height:auto; display:none; background-color:#5C86B8; color:#ffffff; font-family:verdana; font-size:10px; font-weight:bold; padding:2px; text-align:center"></div>
<?
    }
}
?>