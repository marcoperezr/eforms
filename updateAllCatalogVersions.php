<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

require_once('catalog.inc.php');

echo("<BR>"."Loading catalogs collection".str_repeat(' ', 4096));
$aCatalogColl = BITAMCatalogCollection::NewInstance($aRepository);
foreach ($aCatalogColl as $aCatalog) {
	$intCatalogID = $aCatalog->CatalogID;
    $strCatalogName = $aCatalog->CatalogName;
	echo("<BR>"."Identifiying catalog version for '".$strCatalogName."'".str_repeat(' ', 4096));
    
	$intVersionNum = $aCatalog->getCatalogVersion();
	$sql = "UPDATE SI_SV_Catalog SET VersionNum = ".$intVersionNum." 
		WHERE CatalogID = ".$intCatalogID;
	if ($aRepository->DataADOConnection->Execute($sql) === false) {
		echo("<BR>".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>'.str_repeat(' ', 4096));
	}
	else {
		echo("<BR>"."Catalog '".$strCatalogName."', version: ".$intVersionNum.' updated'.str_repeat(' ', 4096));
	}
	echo("<BR><BR>".str_repeat(' ', 4096));
}
?>