<?php

require_once 'eFormsDataDestinations.inc.php';
require_once('settingsvariable.inc.php');
require_once('datadestinations/connectionCloud.class.php');
include_once("getArtusReporterV6.php");

class SyncDataDestination
{
	/** @var int Id de la encuesta */
	public $surveyID;

	/** @var array Guarda las respuestas a cada pregunta, la llave del array es el ID de la pregunta */
	public $answers;

	/** @var Object Instancia de repositorio */
	public $repository;

	/** @var object Instancia de la encuesta */
	public $surveyInstance;

	/** @var BITAMeFormsDataDestinationsCollection Coleccion de las definiciones de los destinos para los datos */
	public $dataDestinations;
	//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
	public $dataDestinationsIDsForUpdateData = array();			//Colección de IDs de destinos de datos que no se deben ejecutar durante un grabado normal (método sync) debido a que están asociados a preguntas tipo Update Data
	//@JAPR
	
	/** @var array Informacion de mapeo de la encuesta, incluye los diferentes mapeos que pueden ser a diferentes formas de diferentes aplicaciones en eBavel */
	public $mappingEbavelInfo;

	/** @var array Coleccion de preguntas de eForms */
	public $arrQuestionsColl;

	/** @var array Coleccion de secciones de eForms */
	public $arrSectionsColl;

	/** @var int Id del reporte */
	public $reportId;

	
	/** Configuraciones */

	/** @var bool indica si es posible guardar en eBavel */
	public $blnCanSaveeBavel;

	/** @var string Ruta en el servidor de la carpeta de eBavel */
	public $eBavelPath;

	/** @var array Datos a insertar en eBavel */
	public $dataInsertEBavel = array();
	//@JAPR 2019-08-09: Corregido un bug, separado el envío de los datos hacia destinos de eBavel por destino de datos, en lugar de usar un único paquete de datos en una única llamda (#MNUH3A)
	public $dataInsertEBavelKeys = array();			//Este array contendrá el último key utilizado por un registro de la forma mediante la cual de indexa, para que los nuevos registros continúen a partir de dicho key

	//GCRUZ 2016-01-13. Datos para cargas incrementales
	public $isIncremental = false;
	public $incrementalFile = '';

	static function NewInstance($aRepository, $surveyInstance, $reportId, $data)
	{
		$aSyncData = new SyncDataDestination();
		
		$aSyncData->surveyID = $data['surveyID'];

		$aSyncData->answers = $data['answers'];
		
		$aSyncData->repository = $aRepository;

		$aSyncData->surveyInstance = $surveyInstance;
		
		$aSyncData->reportId = $reportId;
		
		return $aSyncData;
	}
	
	//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
	/* Agregado el parámetro $aDataDestinationIDsColl con los IDs de los destinos que se deben ejecutar por excepción (si no se recibe - null -, se asume que se procesarán todos)
	*/
	function sync($aDataDestinationIDsColl = null)
	{
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		$this->loadDataDestinations($aDataDestinationIDsColl);
		
		$arrDataKeys = array();
		if (is_null($aDataDestinationIDsColl) || !is_array($aDataDestinationIDsColl)) {
			$aDataDestinationIDsColl = array();
		}
		//@JAPR
		
		$manager = null;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Recorrer destinos a procesar", 1, 1, "color:purple;");
		}
		
		//@JAPR 2016-04-05: Agregado el tipo de pregunta Update Data
		//Debe identificar si el destino de la pregunta Update Data se procesó o no debido a su condición de envío, ya que si no lo hizo, debe responder con false
		$intUpdateDataConditionStatus = null;
		$intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
		$intUpdateDataQuestionID = getParamValue('updateDataQuestionID', 'both', "(int)");
		///@JAPR
		for( $i = 0; $i < count( $this->dataDestinations->Collection ); $i++ )
		{
			$aDestination = $this->dataDestinations->Collection[$i];
			$this->isIncremental = $aDestination->isIncremental;
			
			/** No esta activa */
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			//Si el destino se encuentra entre aquellos que están asignados a preguntas tipo Update Data, entonces ya no es un destino válido para grabar durante el proceso normal de
			//grabado de los datos, pues se debió haber grabado cuando la pregunta Update Data lo invocó directamente, excto si precisamente es parte de los destinos que se piden grabar
			//por excepción en el parámetro
			//@JAPR 2016-06-01: Corregido un bug, el uso del array de excepciones no estaba validado contra warnings, así que generaba algunos durante el proceso de grabado (#MKM198)
			if( !$aDestination->active || (isset($this->dataDestinationsIDsForUpdateData[$aDestination->id]) && !in_array($aDestination->id, $aDataDestinationIDsColl)) ) {
				continue;
			}
			//@JAPR
			
			/**
			 * Destination->type == 1 / eBavel
			 * Destination->type == 2 / Google Drive
			 * Destination->type == 3 / Dropbox
			 * Destination->type == 4 / Dropbox
			 */
			if (getParamValue('DebugBreak', 'both', '(int)', true)){
				ECHOString("aDestination->type:".$aDestination->type, 1, 1, "color:purple;");
			}
			
			if( $aDestination->type == ddeseBavel )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination eBavel: {$aDestination->id}", 1, 1, "color:purple;");
				}
				
				$this->logString('Data destination eBavel');
				
				/** Revisamos si esta correctamente configurado eBavel */
				if( !$this->eBavelConfig( $aDestination ) ) continue;
				$this->logString('Data destination eBavel');
				$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
				
				/** Cargamos la API de eBavel */
				if( ( $manager = $this->loadeBavelApi( $aDestination ) ) === false ) continue;
				
				//@JAPR 2019-08-09: Corregido un bug, la optimización para agrupar todos los registros a enviar a eBavel de entre todos los destinos de datos, impedía que se pudieran
				//procesar casos donde primero se insertaba al registro padre, después a los hijos, y posteriormente se actualizaba a al padre para forzar a generar algo (como un correo)
				//que también utilizara a los hijos, o bien la incertidumbre de preveer que forma de entre todas las mapeadas era la que se ejecutaría primero por parte de eBavel, no permitía
				//planear anticipadamente una secuencia de inserción, así que se regresará el código para que funcione como en versiones previas y se procesen todos los registros de cada
				//destino de dato en el orden que estos están definidos en lugar de preparar un único paquete con todos los registros de la colección total de destinos (#MNUH3A)
				//Ahora antes de generar los registros a insertar para este destino de datos, se limpiará por completo la colección de registros previamente generada para solo se manden
				//los registros correspondientes a este destino de datos, sin embargo las keys se seguirán conservando en el último índice usado por los destinos previos incrementando a la
				//cantidad de los nuevos registros por compatibilidad con el proceso previo que preparaba un único paquete y le asignaba keys distintos a cada registro
				$this->dataInsertEBavel = array();
				//JAPR
				
				$this->mappingEbavel( $aDestination, $manager );
				
				$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				
				//@JAPR 2019-08-09: Corregido un bug, la optimización para agrupar todos los registros a enviar a eBavel de entre todos los destinos de datos, impedía que se pudieran
				//procesar casos donde primero se insertaba al registro padre, después a los hijos, y posteriormente se actualizaba a al padre para forzar a generar algo (como un correo)
				//que también utilizara a los hijos, o bien la incertidumbre de preveer que forma de entre todas las mapeadas era la que se ejecutaría primero por parte de eBavel, no permitía
				//planear anticipadamente una secuencia de inserción, así que se regresará el código para que funcione como en versiones previas y se procesen todos los registros de cada
				//destino de dato en el orden que estos están definidos en lugar de preparar un único paquete con todos los registros de la colección total de destinos (#MNUH3A)
				//Ahora inmediatamente después de generar los registros a mapear hacia eBavel, se procederá con el grabado de los datos
				
				/** Revisamos si existen insercciones a eBavel */
				if( count( $this->dataInsertEBavel ) && $manager )
				{
					/*foreach ($this->dataInsertEBavel as $sForm => $value)
					{
						$manager->insertData($sForm, $value, array(), $this->repository->ADOConnection, 'overwrite', array('alldescription' => true));
					}*/
					//@JAPR 2016-04-05: Agregado el tipo de pregunta Update Data
					//Debe identificar si el destino de la pregunta Update Data se procesó o no debido a su condición de envío, ya que si no lo hizo, debe responder con false
					$blnValidCondition = true;
					$inteBavelRowKey = -1;
					$strFormID = '';
					$arrKeyFieldValues = array();
					$arrAllFieldValues = array();
					//En este punto $aDestination estaría asignado como el último destino procesado independientemente de su tipo, pero sabemos que hubo alguno que fue de tipo eBavel o no habría
					//entrado a este punto, no podemos sin embargo comparar con el ID de destino de eBavel enviado por la pregunta Update data simplemente, porque si ese ID es también el último
					//en el orden de todos los destinos de la forma y estuviera en un grabado normal de una captura, habría entrado a esta validación, así que se agrega la comprobación de que
					//recibió el ID de la pregunta Update data para complementar la validación, en ese caso, $aDestination tuvo que ser por fuerza el único destino recibido, así que realmente
					//la validación de si corresponde con el ID de destino de la pregunta Update data ya saldría sobrando (se dejará de todas maneras para confirmar que sigue siendo el mismo
					//que el dispositivo planeaba actualizar, por si cambió la definición en el servidor durante una captura de dispositivo)
					if ($intUpdateDataQuestionID > 0 &&  $aDestination->id == $intDataDestinationID) {
						//La condición se evalúa en base a campos de eBavel, pero puede contener referencias a preguntas de eForms también (tentativamente sólo se soportan por ahora preguntas de
						//secciones estándar, ya que explícitamente el 2016-04-05 DAbdo dejó muy claro que única y exclusivamente vendría un valor en este punto, y aunque se hubiera enviado
						//desde una sección multi-registro siendo este el valor del registro #n, no tenemos un contexto adecuado por ahora para usar como # de row, se tendría que recibir como
						//parámetro desde el App para poder aplicarlo correctamente)
						$blnValidCondition = (trim($aDestination->condition) == '');
						
						//Para evaluar la condición de envío, primero se debe cargar el registro de eBavel (se asume por tanto que siempre pre-existirán si es que se define una condición de envío),
						//ya que el API de eBavel usada para evaluarla así lo requiere, y DAbdo prefirió que NO se hiciera un Query antes de inicar el proceso, sino cargar el row y sobre él aplicar
						//la validación, esto para que el usuario no tenga que repetir el conjunto de llaves dentro de la condición sino sólo los campos que explícitamente quiera valuar como 
						//condición, por tanto, como en este punto ya se tiene el row a insertar ($this->dataInsertEBavel, que como ya se estableció en el comentario previo, sólo debería traer
						//un único valor con un key == -1), se cargan la lista de campos para identificar cual de ellos son campos llave, de tal manera que se pueda pedir a eBavel que cargue dicho
						//row y así permitir la evaluación de la condición antes de continuar con la actualización de los datos
						$strFormID = (string) @$aDestination->eBavelForm;
						if (trim($aDestination->condition) != '' && count($this->dataInsertEBavel) && isset($this->dataInsertEBavel[$strFormID]) && is_array($this->dataInsertEBavel[$strFormID]) && isset($this->dataInsertEBavel[$strFormID][-1])) {
							$arreBavelRowToInsert = $this->dataInsertEBavel[$strFormID][-1];
							
							require_once('eBavelIntegration.inc.php');
							$inteBavelAppID = 0;
							$inteBavelFormID = 0;
							$arreBavelFormColl = @GetEBavelSections($this->repository, false, null, array($strFormID), true);
							$objeBavelFieldsColl = array();
							if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[0]) && is_array($arreBavelFormColl[0])) {
								$inteBavelAppID = (int) @$arreBavelFormColl[0]['appId'];
								$inteBavelFormID = (int) @$arreBavelFormColl[0]['id_section'];
								if ($inteBavelFormID) {
									$arreBavelFieldsColl = @GetEBavelFieldsForms($this->repository, true, null, array($inteBavelFormID));
									if (!is_null($arreBavelFieldsColl) && is_array($arreBavelFieldsColl)) {
										foreach ($arreBavelFieldsColl as $objeBavelField) {
											$objeBavelFieldsColl[$objeBavelField['id']] = $objeBavelField;
										}
									}
								}
							}
							
							//Si se lograron cargar los campos, obtiene el row de la tabla de eBavel generando un filtro a partir de los campos identificados como llave
							if ($inteBavelAppID && $inteBavelFormID && count($objeBavelFieldsColl)) {
								foreach ($arreBavelRowToInsert as $strFieldID => $aFieldValue) {
									//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
									//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
									//ser modificado por el destino de datos
									if ($strFieldID) {
										$arrAllFieldValues[$strFieldID] = 'null';
									}
									//@JAPR
									if (!$strFieldID || !isset($objeBavelFieldsColl[$strFieldID]) || !((int) @$objeBavelFieldsColl[$strFieldID]['keyfield'])) {
										//Se trata de un campo que no es válido o no es llave, así que lo ignora
										continue;
									}
									
									$arrKeyFieldValues[$strFieldID] = $aFieldValue;
								}
							}
							
							//Si hay por lo menos un campo llave, procede a cargar el registro desde eBavel con la clase DBForm
							if (count($arrKeyFieldValues)) {
								$aRS = null;
								$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
								//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
								//ser modificado por el destino de datos
								$objQuery = DBForm::name($strFormID)->fields(array_keys($arrAllFieldValues));
								//@JAPR
								if (!is_null($objQuery)) {
									//Se desea obtener el campo id_, así que pedimos también las llaves
									$objQuery->onlyFields(false);
									//Agrega el filtro de los campos llave
									$strAnd = '';
									foreach ($arrKeyFieldValues as $strFieldID => $aFieldValue) {
										$objQuery->where($strFieldID, '=', $aFieldValue, $strAnd);
										$strAnd = 'AND';
									}
									
									if (method_exists($objQuery, 'compileQuery')) {
										$sql = (string) @$objQuery->compileQuery($this->repository->ADOConnection);
										global $queriesLogFile;
										$aLogString="\r\n--------------------------------------------------------------------------------------";
										$aLogString.="\r\SyncDataDestination::sync for intDataDestinationID ({$intDataDestinationID}): \r\n";
										$aLogString.=$sql;
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											echo("<br>\r\n SyncDataDestination::sync for intDataDestinationID ({$intDataDestinationID}) Query: {$sql}");
										}
										@updateQueriesLogFile($queriesLogFile,$aLogString);
									}
									
									$aRS = $objQuery->get($this->repository->ADOConnection);
								}
								$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
								
								if ($aRS && is_object($aRS) && property_exists($aRS, 'EOF')) {
									//Sólo debería haber un registro, y sólo interesa el campo id_
									if (!$aRS->EOF) {
										$inteBavelRowKey = (int) @$aRS->fields['id_'.$strFormID];
										//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
										//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
										//ser modificado por el destino de datos
										//Debe respaldar los valores originales del registro para regresarlos al App pos si posteriormente se deben borrar (restaurar mas bien), porque el array
										//$this->dataInsertEBavel trae los valores que el destino de datos determinó son como los va a actualizar, así que no sirven para restaurar el registro
										foreach ($arrAllFieldValues as $strFieldID => $aTmpFieldValue) {
											$aFieldValue = @$aRS->fields[$strFieldID];
											//Los valores null son ignorados porque ya se inicializaron de esa manera al generar este array
											if (!is_null($aFieldValue)) {
												$arrAllFieldValues[$strFieldID] = $aFieldValue;
											}
										}
										//@JAPR
									}
								}
							}
							
							//Si se logró obtener el key del row, así que continua preparando los objetos para evaluar la condición
							if ($inteBavelRowKey > 0) {
								//Antes de evaluar contra eBavel la condición, primero se tienen que reemplazar las preguntas del lado de eForms
								$arrInfo = array();
								$strDataDestinationCond = replaceQuestionVariablesAdvV6($aDestination->condition, $this->answers, true, $this->repository, $this->surveyID, false, false, false, $arrInfo);
								$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
								//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
								//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
								//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
								//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
								//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
								global $eBavelServiceVersion;
								global $theUser;
								if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
									$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
									Metadata::setRepository($aneBavelRepository);
								}
								else {
									//die("Versión incorrecta de eBavel");
								}
								//@JAPR
								$aSection = BITAMAppSection::withName($strFormID, $this->repository->ADOConnection);
								if (!is_null($aSection)) {
									$aModel = BitamAppModel::getModel($inteBavelRowKey, $aSection, $this->repository->ADOConnection);
									if (!is_null($aModel)) {
										$arrMsgError = array();
										
										//Ahora se debe convertir la nomenclatura de campos lógica de eBavel a los IDs internos que son necesarios para el proceso de evaluación
										try {
											$strDataDestinationCond = Utils::replaceLogicalNameWithCodeFields($strDataDestinationCond, $inteBavelAppID, $this->repository->ADOConnection);
										} catch (Exception $e) {
											die("<br>\r\n Error converting from logical fields: ".$e->getMessage());
										}
										
										//Primero se comprueba si la condición es válida según eBavel, usando los dos últimos parámetros en true para forzar a que no use nada de JScript
										Utils::evaluateExpression(null, $strDataDestinationCond, $aModel, $aSection, $manager, $this->repository->ADOConnection, false, true, $arrMsgError, true);
										if (count($arrMsgError) == 0) {
											//Ahora si se realiza la evaluación de la condición removiendo los parámetros que se habían usado en el caso previo
											if ($strEvalResult = Utils::evaluateExpression(null, $strDataDestinationCond, $aModel, $aSection, $manager, $this->repository->ADOConnection, false, false, $arrMsgError)) {
												$blnValidCondition = true;
											}
										}
										else {
											if (getParamValue('DebugBreak', 'both', '(int)', true)){
												ECHOString("Invalid eBavel Data destination condition for {$intDataDestinationID}: ".implode(',', $arrMsgError), 1, 1, "color:purple;");
											}
										}
									}
								}
								$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
							}
						}
						
						//Si no es una condición válida, hay que regresar la respuesta como false (ya no entrará al if siguiente)
						$arrDataKeys['answer'] = ($blnValidCondition)?'True':'False';
					}
					
					//En todos los casos normales de grabado entra, o bien, únicamente si la condición de envío de un destino durante el grabado de una pregunta Update Data es válida
					if ($blnValidCondition) {
						//@JAPR 2016-04-07: Agregado el tipo de pregunta Update Data
						//Movido este código hasta después de validar la condición, de tal manera que sólo se elimine la captura previa si se va a intentar grabar la nueva por ser válida la condición
						//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
						//En este punto, si el request se estaba haciendo por medio de una pregunta Update Data, por lo que se trata sólo del grabado de eBavel, y además dicha pregunta envió un
						//paquete de IDs previos de eBavel, asumiendo que el destino de datos NO ha cambiado entre el request previo y el nuevo, utiliza el nuevo paquete de datos para obtener la
						//colección de campos que deberá grabar, y simplemente remueve los que se consideran llave, pero utiliza los Ids recibidos para tal fin
						if ($intUpdateDataQuestionID && $intDataDestinationID) {
							$arreBavelKeys = null;
							if (isset($this->answers[$intUpdateDataQuestionID]) && isset($this->answers[$intUpdateDataQuestionID]['keys'])) {
								$arreBavelKeys = $this->answers[$intUpdateDataQuestionID]['keys'];
							}
							
							//Inicia el borrado de los datos previamente insertados
							$arrayeBavelKeysUpdted = $this->clearPreviousSeteBavelData($arreBavelKeys);
						}
						
						//@JAPR 2018-06-22: Corregido un bug, se forzará a asignar el repositorio y con ello al usuario logeado en la sesión de eForms antes de empezar la
						//sincronización de datos con eBavel, para que ahora se aplique la seguridad o cualquier cosa dependiente del usuario en el contexto de eBavel, entre
						//ellas asignar precisamente como el usuario que edita los registros a quien se logea en este punto (teóricamente NO hay efectos adversos por invocar
						//en múltiples ocasiones este método de eBavel, así que dejarlo en este punto al que se puede entrar en varias ocasiones por sesión no sería problema) (#BOR02D)
						//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
						//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
						//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
						//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
						//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
						global $eBavelServiceVersion;
						global $theUser;
						if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
							$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
							Metadata::setRepository($aneBavelRepository);
						}
						//@JAPR
						
						$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
						
						$aSyncObject = SyncData::NewInstance($this->dataInsertEBavel, $manager->applicationCodeName, $this->repository);
						
						/** Indica a eBavel que todos los valores a insertar son descripciones, para que los numericos no los tome como los Ids de la base de datos */
						if( method_exists($aSyncObject,'setOption') ) {
							$aSyncObject->setOption('alldescription', true);
							$aSyncObject->setOption('overwrite', true);
							$aSyncObject->setOption('notifyErrorService', true);
						}
						
						try {
							$aSyncObject->start();
							//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
							//Obtiene los Keys de registros insertados en eBavel al terminar de procesar los destinos de datos de ese tipo
							if (!is_null($aSyncObject->keysInserted) && is_array($aSyncObject->keysInserted)) {
								//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
								//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
								//ser modificado por el destino de datos
								//Sólo se puede llevar a cabo este proceso si se trata de un destino invocado por una pregunta Update Data, ya que en ese caso sabemos con certeza que es una única
								//forma de eBavel actualizada así que $arrAllFieldValues trae sólo los campos de dicha forma
								if ($intUpdateDataQuestionID) {
									//$arrDataKeys['keys'] = $aSyncObject->keysInserted;
									$arrDataKeys['keys'] = array();
									$arrDataKeys['keys'][$strFormID] = array();
									$arrDataKeys['keys'][$strFormID][$inteBavelRowKey] = $arrAllFieldValues;
								}
								//@JAPR
							}
							//@JAPR
						} catch (Exception $e) {
							$this->insertDataDestinationLog(array('error' =>1 ,'msg' => $e->getMessage()),"eBavel");
							$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
							return;
						}			
						
						$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
						
						$this->insertDataDestinationLog(array('error' =>0 ,'msg' =>"The data is properly upload to eBavel"),"eBavel");
					}
					//@JAPR
				}
				//@JAPR
				
			} else if ( $aDestination->type == ddesDropBox )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination Dropbox: {$aDestination->id}", 1, 1, "color:purple;");
				}
				
				$this->logString('Data destination Dropbox');
				if( $this->evaluateCondition( $aDestination->condition, $i ) ){
					$this->sendDropBox( $aDestination );
				}else{
					$this->logString('Invalid condition: '.$aDestination->condition);
				}

			} else if ( $aDestination->type == ddesGoogleDrive ) /** Google drive */
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination Google Drive: {$aDestination->id}", 1, 1, "color:purple;");
				}
				
				$this->logString('Data destination Google Drive');
				if( $this->evaluateCondition( $aDestination->condition, $i ) ){
					$this->sendGoogleDrive( $aDestination );
				}else{
					$this->logString('Invalid condition: '.$aDestination->condition);
				}

			} else if ( $aDestination->type == ddesEMail )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination eMail: {$aDestination->id}", 1, 1, "color:purple;");
				}
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				//error_log("\r\n"."ESTO es datadestination: ". print_r($aDestination->additionaleMail, true), 3, GeteFormsLogPath().'ExporteMail.txt');
				//@JAPR

				$this->logString('Data destination eMail');
				if( $this->evaluateCondition( $aDestination->condition, $i ) ){
					if (getMDVersion()>=esvDataDestination) {
						$this->logString('eMail FileFormat = '.$aDestination->emailformat);
						if ($aDestination->emailformat > ddeftPDF){
							//En caso de ser un Reporte de dashboard o un ARS se va al codigo 
							$arrayPDF = array();
							$res = getArtusReporterV6($aDestination, $this->reportId, $this->repository);
							if($res!==false) {
								$arrayPDF[] = $res;
							}
							if(count($arrayPDF)>0) {
								foreach ($arrayPDF as $docPDF) {
									//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
									$success=connectionCloud::uploadFile('eMail', null, $docPDF, $this->repository, $this->surveyInstance, null,null,null,null,'',$aDestination->additionaleMail, $aDestination, $this->options);
									$this->insertDataDestinationLog($success,"eMail",$aDestination);
								}
							}
						}else {
							/** Aqui va la exportacion a PDF que debe regresar la ruta del PDF guardado */
							//GCRUZ 2016-01-17. Exportación según el formato del archivo
							//$fileMail = $this->getExportFile( 3 );
							$fileMail = $this->getExportFile( $aDestination->emailformat );
							$this->logString('eMail File = '.$fileMail["file"]);
							//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
							$success=connectionCloud::uploadFile('eMail', null, $fileMail["file"], $this->repository, $this->surveyInstance, null,null,null,null,'',$aDestination->additionaleMail, $aDestination, $this->options);
							$this->insertDataDestinationLog($success,"eMail", $aDestination);
							/** Borrar archivo PDF despues de mandar el email */
						}
					}else {
						/** Aqui va la exportacion a PDF que debe regresar la ruta del PDF guardado */
						$fileMail = $this->getExportFile( 3 );																									
						//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
						$success=connectionCloud::uploadFile('eMail', null, $fileMail["file"], $this->repository, $this->surveyInstance, null,null,null,null,'',$aDestination->additionaleMail, $aDestination, $this->options);
						$this->insertDataDestinationLog($success,"eMail", $aDestination);
						/** Borrar archivo PDF despues de mandar el email */
					}

				}else{
					$this->logString('Invalid condition: '.$aDestination->condition);
				}
			} else if( $aDestination->type == ddesFTP )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination FTP: {$aDestination->id}", 1, 1, "color:purple;");
				}
				
				$this->logString('Data destination FTP');
				if( $this->evaluateCondition( $aDestination->condition, $i ) ){
					$this->sendFTP( $aDestination );
				}else{
					$this->logString('Invalid condition: '.$aDestination->condition);
				}
			}else if( $aDestination->type == ddesHTTP )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)){
					ECHOString("Data destination HTTP: {$aDestination->id}", 1, 1, "color:purple;");
				}
				
				$this->logString('Data destination HTTP');
				if( $this->evaluateCondition( $aDestination->condition, $i ) ){
					$this->sendHTTP( $aDestination );
				}else{
					$this->logString('Invalid condition: '.$aDestination->condition);
				}	
			}
		}
		
		//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
		return $arrDataKeys;
		//@JAPR
	}
	
	/* Dada una colección de IDs indexados por la forma de eBavel y los keys dummy con los que se generaron así como los keys finales insertados en dicha forma, realizará una actualización
	de esos mismos keys para limpiar todos los campos que estando mapeados en el destino utilizado para procesar una pregunta Update Data (ya que sólo en ese caso habría entrado aquí, y
	sólo habría un posible destino por lo que sólo habría una posible forma de eBavel involucrada) y no siendo llave de la forma de eBavel, habrían sido modificados en el request previo
	*/
	function clearPreviousSeteBavelData($aneBavelKeysColl) {
		$arrayeBavelKeysUpdted = array();
		
		$intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
		$intUpdateDataQuestionID = getParamValue('updateDataQuestionID', 'both', "(int)");
		if ($intUpdateDataQuestionID <= 0 || $intDataDestinationID <= 0) {
			return $arrayeBavelKeysUpdted;
		}
		
		$aDestination = BITAMeFormsDataDestinations::NewInstanceWithId($this->repository, $intDataDestinationID);
		if (is_null($aDestination)) {
			return $arrayeBavelKeysUpdted;
		}
		$strFormID = (string) @$aDestination->eBavelForm;
		
		require_once('eBavelIntegration.inc.php');
		$inteBavelFormID = 0;
		$arreBavelFormColl = @GetEBavelSections($this->repository, false, null, array($strFormID), true);
		$objeBavelFieldsColl = array();
		if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[0]) && is_array($arreBavelFormColl[0])) {
			$inteBavelFormID = (int) @$arreBavelFormColl[0]['id_section'];
			if ($inteBavelFormID) {
				$arreBavelFieldsColl = @GetEBavelFieldsForms($this->repository, true, null, array($inteBavelFormID));
				if (!is_null($arreBavelFieldsColl) && is_array($arreBavelFieldsColl)) {
					foreach ($arreBavelFieldsColl as $objeBavelField) {
						$objeBavelFieldsColl[$objeBavelField['id']] = $objeBavelField;
					}
				}
			}
		}
		
		$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		if( ( $manager = $this->loadeBavelApi( $aDestination ) ) === false ) {
			$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
			return $arrayeBavelKeysUpdted;
		}
		
		//Si la forma del destino de datos no está entre las llaves recibidas, entonces no se puede procesar el borrado
		if (!isset($aneBavelKeysColl[$strFormID]) || !is_array($aneBavelKeysColl[$strFormID]) || !count($aneBavelKeysColl[$strFormID])) {
			$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
			return $arrayeBavelKeysUpdted;
		}
		
		//En este punto ya tiene cargados los campos totales de la forma de eBavel con la indicación de cuales son llave, además ya tiene cargados los campos que va a mapear como destino
		//de datos, así que se puede generar el array de datos a enviar incluyendo todos los campos mapeados como NULL pero sin incluir los campos llave, usando los keys recibidos
		$arrayeBavelKeysUpdted[$strFormID] = array();
		//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
		//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
		//ser modificado por el destino de datos
		//foreach ($aneBavelKeysColl[$strFormID] as $intDummyID => $inteBavelRowKey) {
		foreach ($aneBavelKeysColl[$strFormID] as $inteBavelRowKey => $arrAllFieldValues) {
			$arrayeBavelKeysUpdted[$strFormID][$inteBavelRowKey] = array();
			$arrayeBavelKeysUpdted[$strFormID][$inteBavelRowKey]['id_'.$strFormID] = $inteBavelRowKey;
			for($i = 0; $i < count( $aDestination->mapping ); $i++)
			{
				$strFieldID = (string) @$aDestination->mapping[$i]['fieldformid'];
				if (!$strFieldID || !isset($objeBavelFieldsColl[$strFieldID]) || (int) @$objeBavelFieldsColl[$strFieldID]['keyfield']) {
					//Se trata de un campo no válido para grabar por no existir ya o por ser llave
					continue;
				}
				
				//Se agrega el campo a mapear con un valor de null para que se limpie su valor
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				//Ahora se regresará al App la colección completa de datos en lugar de sólo las Keys, de tal manera que se pueda restaurar el valor exactamente como estaba antes
				//ser modificado por el destino de datos
				//Ahora recuperará el valor del array de valores recibido desde el App para resutarar el registro al dato original antes de haber sido modificado por el destino de datos
				$aFieldValue = @$arrAllFieldValues[$strFieldID];
				if (is_null($aFieldValue)) {
					$aFieldValue = 'null';
				}
				$arrayeBavelKeysUpdted[$strFormID][$inteBavelRowKey][$strFieldID] = $aFieldValue;
				//$arrayeBavelKeysUpdted[$strFormID][$inteBavelRowKey][$strFieldID] = 'null';
				//@JAPR
			}
		}
		
		$aSyncObject = SyncData::NewInstance($arrayeBavelKeysUpdted, $manager->applicationCodeName, $this->repository);
		
		/** Indica a eBavel que todos los valores a insertar son descripciones, para que los numericos no los tome como los Ids de la base de datos */
		if( method_exists($aSyncObject,'setOption') ) {
			$aSyncObject->setOption('alldescription', true);
			$aSyncObject->setOption('overwrite', true);
			$aSyncObject->setOption('notifyErrorService', true);
		}
		
		try {
			$aSyncObject->start();
		} catch (Exception $e) {
			$this->insertDataDestinationLog(array('error' =>1 ,'msg' => $e->getMessage()),"eBavel");
			$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
			return;
		}			
		
		$this->repository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		$this->insertDataDestinationLog(array('error' =>0 ,'msg' =>"The data is properly upload to eBavel"),"eBavel");
		
		return $arrayeBavelKeysUpdted;
	}
	
	/**
	 * Carga la informacion de mapeo de esta encuesta
	 * @return void
	//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
	Agregado el parámetro $aDataDestinationIDsColl con los IDs de los destinos que se deben ejecutar por excepción (si no se recibe - null -, se asume que se procesarán todos)
	 */
	function loadDataDestinations($aDataDestinationIDsColl = null)
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)){
			ECHOString("function loadDataDestinations", 1, 1, "color:purple;");
		}

		global $reprocess;
		//Ids de los destinos a reprocesar
		global $repDestinationIDs;
		
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		//Si se recibe el parámetro de IDs de destinos de datos, se le da preferencia por sobre el array global
		$arrDataDestinationIDsColl = $repDestinationIDs;
		if (!is_null($aDataDestinationIDsColl) && is_array($aDataDestinationIDsColl)) {
			$arrDataDestinationIDsColl = $aDataDestinationIDsColl;
		}
		$this->dataDestinations = BITAMeFormsDataDestinationsCollection::NewInstance( $this->repository, $this->surveyID, $arrDataDestinationIDsColl);
		
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		$this->dataDestinationsIDsForUpdateData = array();
		$sql = "SELECT DISTINCT A.DataDestinationID 
			FROM SI_SV_Question A 
			WHERE A.SurveyID = {$this->surveyID}";
		$aRS = $this->repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$intDataDestinationID = (int) @$aRS->fields["datadestinationid"];
				$this->dataDestinationsIDsForUpdateData[$intDataDestinationID] = $intDataDestinationID;
				$aRS->MoveNext();
			}
		}
		//@JAPR
	}

	function mappingEbavel( $aDestination, $manager )
	{
		global $blnSaveeBavelLogStringFile;
		/** Preparamos las preguntas de eForms que utilizaremos en el mapeo */
		$arrQuestionsColl = $this->getQuestionCollection();
		
		/** Cargamos las secciones de eForms */
		$arrSectionsColl = $this->getSectionCollection();
		if(!isset($arrSectionsColl))
			$this->insertDataDestinationLog(array('error' =>1 ,'msg' =>"Could not bring the collection of sections eBavel"),"eBavel", $aDestination);
		/** Array que representa los datos a insertar en eBavel */
		$arrInsertEBavel = array();
		
		/** Array que representa los datos a insertar en eBavel de una seccion sencilla en eForms */
		$arrInsertSingleEBavel = array();
		
		/** Array que representa los datos a insertar en eBavel de una seccion multiple en eForms */
		$arrInsertMulEbavel = array();
		
		/** @var array Guarda el indice de los mapeos que son de formula para poder ejecutarlos en su contexto */
		$arrConstant = array();
		
		/** Cargamos las instancias de los objetos de campo de eBavel */
		$arrFieldsforms = $this->getFieldsform( $aDestination );
		
		/** Obtenemos el ultimo ID que se a insertado en la forma */
		$nKeyInsert = -1;
		
		//@JAPR 2016-08-19: Corregido un bug, la manera de preparar el array estaba equivocada, si ya estaba asignada la forma entonces correctamente tomaba el número de elementos actual
		//para sumarle uno y convertirlo a negativo para usarlo como llave, pero al final cuando ya se actualizaba el array, literalmente se estaba sobrescribiendo así que a pesar de
		//haber generado una nueva llave con el consecutivo siguiente, terminaba siendo la única llave (o las únicas si hubiera sido una sección multi-registro) y eliminaba el conjunto
		//de datos preparados de alguna iteración previa de otro destino de datos. Esto supongo fue a consecuencia de que en este punto no se creaba el índice de la forma si no hubiera
		//estado asignado, ya que mas abajo en el código que anexa los registros simplemente se sobrescribía el array con lo cual precisamente se crea en ese punto dicho índice, pero
		//si se hubiera tratado de anextar nuevos registros entonces fallaba como se explicó al resetear todo el array limpiando los valores previos (#58KOYE)
		//NO se agregará un else para crear el índice en este punto, porque todavía falta determinar si la condición de envío se cumple o no, así que se dejará la creación del índice
		//justo antes de insertar los registros que finalmente fueran válidos
		
		//@JAPR 2019-08-09: Corregido un bug, separado el envío de los datos hacia destinos de eBavel por destino de datos, en lugar de usar un único paquete de datos en una única llamda (#MNUH3A)
		//Modificado nuevamente el método para obtener el último key usado, ahora no se basará en la colección de registros preparados hasta este punto, ya que dicha colección contiene solo
		//los registros del destino que actualmente se está procesando, en lugar de eso se basará en el array adicional que precisamente almacenará las llaves, sólo en caso de que no exista
		//un elemento para la forma correspondiente en dicho array adicional (lo cual representaría que se trata de la primera vez que inserta registros en ella) usaría la colección actual
		//de registros directamente
		//if( isset( $this->dataInsertEBavel[ $aDestination->eBavelForm ] ) && is_array( $this->dataInsertEBavel[ $aDestination->eBavelForm ] ) )
		if( isset( $this->dataInsertEBavelKeys[ $aDestination->eBavelForm ] ) )
		{
			//@JAPR 2019-08-09: Corregido un bug, separado el envío de los datos hacia destinos de eBavel por destino de datos, en lugar de usar un único paquete de datos en una única llamda (#MNUH3A)
			//En este caso ya existía una llave asignada, así que simplemente la utiliza para generar el siguiente key a utilizar, tomando en cuenta
			//$nKeyInsert = ( count( $this->dataInsertEBavel[ $aDestination->eBavelForm ] ) + 1 ) * -1;
			$nKeyInsert = (int) @$this->dataInsertEBavelKeys[ $aDestination->eBavelForm ] - 1 ;
			//@JAPR
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel form with previous data to send, nKeyInsert value: {$nKeyInsert}", 1, 1, "color:red;");
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel form without previous data to send, nKeyInsert value: {$nKeyInsert}", 1, 1, "color:red;");
			}
		}
		
		//Independientemente de si era el primer registro o se trata de un nuevo registro de la misma forma que usará la siguiente key en el orden de inserción, deberá actualizar la última
		//lave utilizada en este punto, ya que mas abajo solo se actualiza cuando se trata de secciones multi-registro, pero para secciones estándar ya no se ejecutará este proceso pues la
		//key ya no cambiará (se estaría creando este índice en este punto si fuera el primero registro, actualizándolo si ya hubieran existido registros previos)
		$this->dataInsertEBavelKeys[ $aDestination->eBavelForm ] = $nKeyInsert;
		//@JAPR

		$arrInsertSingleEBavel = $this->setEbavelDefaultValues( $arrInsertSingleEBavel, $aDestination->eBavelForm, $nKeyInsert );
		
		//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
		$intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
		//@JAPR
		
		/** Recorremos los campos que fueron mapeados de preguntas de eForms a eBavel, interactuamos por los campos de eBavel */
		for($i = 0; $i < count( $aDestination->mapping ); $i++)
		{
			if( $aDestination->mapping[$i]['questionid'] < 0 )
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					if( @$aDestination->mapping[ $i ]['questionid'] == -38 || @$aDestination->mapping[ $i ]['questionid'] == -39 ) {
						ECHOString("eBavel storing formula: ".(string) @$aDestination->mapping[ $i ]['formula']);
					}
					else {
						ECHOString("eBavel storing constant: ".(string) @$aDestination->mapping[$i]['questionid']);
					}
				}
				/** Es un mapeo a una constante string o integer, lo guardamos para poder ejecutarla en el contexto correcto */
				$arrConstant[] = $i;
			} else if( $aDestination->mapping[$i]['questionid'] > 0 && $arrQuestionsColl[ $aDestination->mapping[$i]['questionid'] ]->SectionType != 0 )
			{
				if(isset($arrFieldsforms[ $aDestination->mapping[$i]['fieldformid'] ])){
					/** Es una pregunta de seccion multiple */
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("eBavel getting multirow section Answers for : ".(int) @$aDestination->mapping[$i]['questionid']);
					}
					$arrInsertMulEbavel[ $aDestination->mapping[$i]['fieldformid'] ] = $this->getEFormsMultiSectionAnswers( $aDestination->mapping[$i], $arrFieldsforms[ $aDestination->mapping[$i]['fieldformid'] ], $manager );
				}
			} else 
			{
				if(isset($arrFieldsforms[ $aDestination->mapping[$i]['fieldformid'] ])){
					/** Colocamos en el campo mapeado de eBavel el valor que le corresponde de la pregunta de eForms */
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("eBavel getting standard section Answer for : ".(int) @$aDestination->mapping[$i]['questionid']);
					}
					$arrInsertSingleEBavel[ $aDestination->mapping[$i]['fieldformid'] ] = $this->getEFormsAnswers( $aDestination->mapping[$i], null, null, $arrFieldsforms[ $aDestination->mapping[$i]['fieldformid'] ], $manager );
				}
			}
		}
		
		/** Insertamos en eBavel la informacion nueva */
		/** Solo se usa una seccion multiple por cada Data Destination */
		if( count( $arrInsertMulEbavel ) )
		{
			/** Obtenemos el numero todal de registros de la seccion multiple */
			$nCount = count( $arrInsertMulEbavel[ key( $arrInsertMulEbavel ) ] );
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel processing data rows: {$nCount}", 1, 1, "color:purple;");
			}
			
			//@JAPR 2019-07-17: Corregido un bug, esta asignación del ID negativo incremental estaba mal colocada, ya que al evaluarse mas abajo la condición de envío por registro,
			//algunos de ellos estaban siendo ignorados y se usaba finalmente el ID por la posición dentro de la sección multi-registro + el ID del total de registro acumulados que se
			//van a mandar para esta forma, lo cual generaba IDs repetidos si la forma destino hubiera sido la misma, se usará el consecutivo siguiente a partir de ahora (#7N7R60)
			$intNextKeyInsert = $nKeyInsert;
			//@JAPR
			for( $i = 0; $i < $nCount; $i++)
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("eBavel processing data rows #{$i}");
				}
				//@JAPR 2019-07-17: Corregido un bug, esta asignación del ID negativo incremental estaba mal colocada, ya que al evaluarse mas abajo la condición de envío por registro,
				//algunos de ellos estaban siendo ignorados y se usaba finalmente el ID por la posición dentro de la sección multi-registro + el ID del total de registro acumulados que se
				//van a mandar para esta forma, lo cual generaba IDs repetidos si la forma destino hubiera sido la misma, se usará el consecutivo siguiente a partir de ahora (#7N7R60)
				//$iKey = ( ($i + 1) * -1 );
				//$iKey = $nKeyInsert + ( $i * -1 );
				//@JAPR
				
				/** checamos si la condicion de ejecucion de este destino es TRUE */
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				//Modificado el if para primero evaluar la condición, ya que en caso de que sea un grabado de destino por medio de una pregunta Update Data, la condición realmente NO se aplica
				//en este punto, así que debe forzar al grabado de los datos y evaluar la condición hasta mas adelante
				if (!$intDataDestinationID) {
					if( !$this->evaluateCondition( $aDestination->condition, $i ) ) 
					{
						$this->logString('Invalid condition: '.$aDestination->condition);
						continue;
					} 
					else {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Valid condition: ".$aDestination->condition, 1, 1, "color:purple;");
						}
					}
				}
				
				//@JAPR 2019-07-17: Corregido un bug, esta asignación del ID negativo incremental estaba mal colocada, ya que al evaluarse mas arriba la condición de envío por registro,
				//algunos de ellos estaban siendo ignorados y se usaba finalmente el ID por la posición dentro de la sección multi-registro + el ID del total de registro acumulados que se
				//van a mandar para esta forma, lo cual generaba IDs repetidos si la forma destino hubiera sido la misma, se usará el consecutivo siguiente a partir de ahora (#7N7R60)
				foreach ($arrInsertMulEbavel as $sField => $value)
				{
					$arrInsertEBavel[ $intNextKeyInsert ][ $sField ] = $value[$i];
				}
				
				for( $x = 0; $x < count( $arrConstant ); $x++ )
				{
					$arrInfo = array();
					//@JAPRWarning: Constantes sfidFixedStrExpression y sfidFixedNumExpression
					if( $aDestination->mapping[ $arrConstant[$x] ]['questionid'] == -38 || $aDestination->mapping[ $arrConstant[$x] ]['questionid'] == -39 )
					{
						$arrInsertEBavel[ $intNextKeyInsert ][ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] = $this->evaluateFormula( $aDestination->mapping[ $arrConstant[$x] ]['formula'], $i, $arrFieldsforms[ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] );	
					} else 
					{
						/** Dependiendo del valor de la constante se saca el valor */
						$arrInsertEBavel[ $intNextKeyInsert ][ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] =$this->getValueConstant($aDestination->mapping[$arrConstant[$x]]['questionid']);
					}				
				}
				
				$arrInsertEBavel[ $intNextKeyInsert ] = array_merge( $arrInsertEBavel[ $intNextKeyInsert ], $arrInsertSingleEBavel );
				$arrInsertEBavel[ $intNextKeyInsert ][ 'id_' . $aDestination->eBavelForm ] = $intNextKeyInsert;
				//@JAPR 2019-08-09: Corregido un bug, separado el envío de los datos hacia destinos de eBavel por destino de datos, en lugar de usar un único paquete de datos en una única llamda (#MNUH3A)
				//Por cada nuevo registro insertado actualiza la última key utilizada en el array que mantiene esta relación de llaves, para que el próximo destino pueda continuar a partir de este punto
				$this->dataInsertEBavelKeys[ $aDestination->eBavelForm ] = $intNextKeyInsert;
				//@JAPR
				//Decrementa el ID a utilizar para que varíe el registro
				$intNextKeyInsert--;
				//@JAPR
			}
		} else 
		{
			//@JAPRWarning: Este código está seguramente mal, pero por suerte funciona, ya que en este contexto la variable $i que identificaría el número de row de respuesta de secciones
			//multi-registro a usar para evaluar la condición, contiene realmente el número del último campo mapeado de eBavel en la propiedad $aDestination->mapping del ciclo for previo
			//a este if/else, pero como la función de reemplazo de variables intenta usar el # de row recibido buscándolo entre la lista de respuestas de cada pregunta que evalúa, cuando
			//NO la encuentra (como seguramente será el caso, pues para secciones estándar que es este caso, sólo hay una respuesta) entonces utiliza la primer (y única) respuesta que tiene
			//así que funciona ok. No siendo yo quien programó esto, no quise modificarlo hasta en un futuro recibir confirmación de QA de que funciona mal como está y/o hasta 
			//que se encuentre algún posible problema de implementación de código nuevo por este hecho, pero esa $i debió haber sido un 0 en este contexto
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			//Modificado el if para primero evaluar la condición, ya que en caso de que sea un grabado de destino por medio de una pregunta Update Data, la condición realmente NO se aplica
			//en este punto, así que debe forzar al grabado de los datos y evaluar la condición hasta mas adelante
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel processing single data row");
			}
			$blnValidCondition = true;
			if (!$intDataDestinationID) {
				if( !$this->evaluateCondition( $aDestination->condition, $i ) )
				{
					$blnValidCondition = false;
				}
			}
			
			if ($blnValidCondition) {
				for( $x = 0; $x < count( $arrConstant ); $x++ )
				{
					$arrInfo = array();
					//@JAPRWarning: Constantes sfidFixedStrExpression y sfidFixedNumExpression
					if( $aDestination->mapping[ $arrConstant[$x] ]['questionid'] == -38 || $aDestination->mapping[ $arrConstant[$x] ]['questionid'] == -39 )
					{
						$arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ] ][ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] = $this->evaluateFormula( $aDestination->mapping[ $arrConstant[$x] ]['formula'], null, $arrFieldsforms[ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] );	
					} else 
					{
						/** Dependiendo del valor de la constante se saca el valor */
						$arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ] ][ $aDestination->mapping[ $arrConstant[$x] ]['fieldformid'] ] =$this->getValueConstant($aDestination->mapping[$arrConstant[$x]]['questionid']);
					}
				}
				if( isset( $arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ] ] ) ){
		     		$arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ]] = array_merge( $arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ] ], $arrInsertSingleEBavel );
			    } else{
		     		$arrInsertEBavel[ $arrInsertSingleEBavel[ 'id_' . $aDestination->eBavelForm ] ] = $arrInsertSingleEBavel; 
			    }
			} else {
				$this->logString('Invalid condition: '.$aDestination->condition);
			}
		}
		
		if( count( $arrInsertEBavel ) )
		{
			//$this->repository->ADOConnection->Execute("set names 'utf8'");
			//@JAPR 2016-10-25: Agregada una validación a petición de ARamirez, en la cual se eliminarán todos los caracteres UTF8 arriba del rango soportado por MySQL como se explica en
			//la definición de la función utilizada, ya que los caracteres emojis de IOs causaban errores de grabado y eBavel no hace esta validación a nivel de su capa de datos
			foreach ($arrInsertEBavel as $inteBavelKey => $arreBavelData) {
				foreach ($arreBavelData as $strFieldID => $aFieldValue) {
					//Sólo sobrescribe los valores que son strings
					if (is_string($aFieldValue)) {
						$arrInsertEBavel[$inteBavelKey][$strFieldID] = RemoveUNICODEFromUtf8Str($aFieldValue);
					}
				}
			}
			//@JAPR
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel insert data in table: $aDestination->eBavelForm");
				PrintMultiArray($arrInsertEBavel);
			}
			//@JAPR 2018-02-07: Corregido un bug, la bitácora de eBavel Actions nunca fue pensada para registrar cualquier tipo de destino de datos, sólo los envíos
			//hacia eBavel, así que se cambiará el log por el original de la tarea del agente
			//Estos SI son registros que deben ir en la tabla de eBavel Actions, sin embargo también se agregará la referencia a la tabla de log normal solo de la
			//indicación de inserción, no de los datos completos
			@logeBavelString("eBavel insert data in table: $aDestination->eBavelForm");
			$this->logString("eBavel insert data in table: $aDestination->eBavelForm");
			if (@$blnSaveeBavelLogStringFile) {
				$strLogText = (string) @var_export(@$arrInsertEBavel, true);
				@logeBavelString($strLogText);
			}
			//$res = $manager->insertData($aDestination->eBavelForm, $arrInsertEBavel, array(), $this->repository->ADOConnection, 'overwrite', array('alldescription' => true));
			try {
				//@JAPR 2016-08-19: Corregido un bug, la manera de preparar el array estaba equivocada, si ya estaba asignada la forma entonces correctamente tomaba el número de elementos actual
				//para sumarle uno y convertirlo a negativo para usarlo como llave, pero al final cuando ya se actualizaba el array, literalmente se estaba sobrescribiendo así que a pesar de
				//haber generado una nueva llave con el consecutivo siguiente, terminaba siendo la única llave (o las únicas si hubiera sido una sección multi-registro) y eliminaba el conjunto
				//de datos preparados de alguna iteración previa de otro destino de datos (#58KOYE)
				//Ahora en este punto se creará el índice de la forma en el array de datos, ya que para este momento ya es válido insertar este conjunto de valores a dicha forma,
				//adicionalmente dado a que puede haber mas de un posible valor (todos con índices negativos), se aplicará un for para insertarlos con el mismo índice al array final
				if (!isset( $this->dataInsertEBavel[ $aDestination->eBavelForm ] )) {
					$this->dataInsertEBavel[ $aDestination->eBavelForm ] = array();
				}
				
				foreach ($arrInsertEBavel as $inteBavelKey => $arreBavelData) {
					$this->dataInsertEBavel[ $aDestination->eBavelForm ][$inteBavelKey] = $arreBavelData;
				}
				//$this->dataInsertEBavel[ $aDestination->eBavelForm ] = $arrInsertEBavel;
				//@JAPR
			} catch (Exception $e) {
				$this->insertDataDestinationLog(array('error' =>1 ,'msg' =>$e->getMessage()),"eBavel", $aDestination);
			}
		}
	}

	function evaluateCondition( $sCondition, $nRow )
	{
		$sCondition=trim($sCondition);
		if($sCondition==''){	
			return true;
		}
		$arrInfo = array();

		$bCondition = false;

		//@JAPR 2015-12-01: Corregido un bug, no se debe enviar el parámetro de Quote porque las comillas sólo son requeridas en casos muy específicos (#9IG3DH)
		$r = replaceQuestionVariablesAdvV6($sCondition, $this->answers, true, $this->repository, $this->surveyID, false, false, false, $arrInfo, $nRow);
		//@JAPR
		try {
    				
			eval('$bCondition = ' . $r . ';');

		} catch (Exception $e) {
			
			$bCondition = false;
			//@JAPR 2016-09-113: Agregado el despliegue de la condición con variables reemplazadas para permitir su análisis en la bitácora
			@$this->logString('Condition with error: '.$r);
			//@JAPR
		}

		return $bCondition;

	}

	function evaluateFormula( $sCondition, $nRow = null, $aFieldForm = null )
	{
		$arrInfo = array();

		$sRes = '';

		//@JAPR 2015-12-01: Corregido un bug, no se debe enviar el parámetro de Quote porque las comillas sólo son requeridas en casos muy específicos (#9IG3DH)
		$r = replaceQuestionVariablesAdvV6($sCondition, $this->answers, true, $this->repository, $this->surveyID, false, false, false, $arrInfo, $nRow);
		//@JAPR
		try {
			//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//Se necesitó escapar con addslashes debido a que si se usan variables string o que contienen " (comos los Vectores de la Sketch+) podrían cortar la cadena y generar un error
			eval('$sRes = "' . addslashes($r) . '";');
			//@JAPR
		} catch (Exception $e) {
			
			$sRes = '';

		}

		if( $aFieldForm && $aFieldForm->isMultipleChoice() )
		{
			$sRes = explode(',', $sRes);
		}

		return $sRes;
	}

	function getEFormsAnswers( $eachMapping, $anAnswer = null, $nRow = null, $aFieldForm, $manager )
	{
		global $blnWebMode;
		
		/** @var mixed El valor que tendra el campo de eBavel en la interaccion, puede ser integer o string*/
		if(!isset($aFieldForm) || $aFieldForm==NULL)
			return '';
		
		$sValue = null;

		$questionid = intval( $eachMapping['questionid'] );

		$arrQuestionsColl = $this->getQuestionCollection();

		$anQuestion = @$arrQuestionsColl[ $questionid ];

		if( is_null( $anAnswer ) && $questionid > 0 )
		{
			//@JAPR 2018-01-03: Corregido un bug, no había validación en caso de preguntas que no mandaron su respuesta, lo cual provocaba warnings y errores en el
			//proceso de grabado de las capturas o destinos de datos (#K3OLK4)
			$anAnswer = @$this->answers[ $questionid ];
			//@JAPR
		}

		//@JAPR 2019-03-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Identifica el tipo de información adicional a obtener, en la mayoría de las preguntas esto será ignorado, solo algunas muy específicas podrán variar este valor con un significado
		$intAdditionalData = (int) @$eachMapping['additionalData'];
		//@JAPR
		
		if ($anQuestion ) {
			//@JAPR 2016-08-12: Corregido el mapeo de preguntas firma/sketch hacia eBavel, para estas preguntas lo que llega en la propiedad answer.photo es un B64 que no se graba localmente
			//como archivo de imagen en la carpeta de sincronización, sino que se graba directo en la carpeta de imágenes de las capturas ya procesadas, asó que al mandar a eBavel la imagen,
			//lo cual hacía enviando la propiedad answer.photo como si fuera una ruta de imagen, fallaba el copiado. Ahora se sobrescribirá en el $_POST un array por pregunta al momento de
			//generar la imagen para almacenar el nombre de archivo con el que quedó y usar eso durante el proceso de mapeo hacia eBavel (#4L5RSC)
			$nRow = (int) $nRow;
			switch ($anQuestion->QTypeID) {
				//@JAPR 2019-04-17: Corregido un bug con el mapeo de preguntas de documentos hacia eBavel (#7D0C65)
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
					//Todos los archivos multimedia que representen algo distinto a una foto se manejarán como documentos para eBavel
					return $this->moveDocumentToEbavel( @$anAnswer['document'], $aFieldForm, $manager, false, $anQuestion->QTypeID);
					break;
				//@JAPR
				case qtpPhoto:
					return $this->movePhotoToEbavel( @$anAnswer['photo'], $aFieldForm, $manager );
					break;
				//@JAPR 2019-04-22: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
				case qtpSingle:
					if ( $anQuestion->QDisplayMode == dspAR ) {
						//El tipo de pregunta AR funciona exactamente como una pregunta Foto, así que recibirá tratamiento especial
						return $this->movePhotoToEbavel( @$anAnswer['photo'], $aFieldForm, $manager );
					}
					break;
				//@JAPR
				case qtpSignature:
					//En el caso de la firma, no se puede usar directamente la propiedad photo, se utilizará el nuevo array el cual sólo estaría asignado si el proceso de grabado de la firma
					//en eForms concluyó correctamente
					//@JAPR 2016-08-15: Corregido un bug, al aplicar la corrección para modo web se afectó a móvil, ya que la ruta leída desde questionB64Images ya incluye el path
					//de sincronización, así que movePhotoToEbavel lo estaba volviendo a agregar, lo cual es correcto para el caso de fotos pero no para el caso de firmas, así que como
					//este proceso ya funcionaba previo a esta corrección, se dejará el mismo código aunque la corrección podría aplicar para ambos eventualmente (#4L5RSC)
					if (!$blnWebMode) {
						return $this->movePhotoToEbavel( @$anAnswer['photo'], $aFieldForm, $manager );
					}
					else {
						//A la fecha de implementación, este array no tenía manera de soportar $useRecoveryPaths, aunque no existía un método para carga de tareas con error como en v5, por
						//lo que no afectaba, sin embargo si se retomará esa idea, entonces se tendrán que hacer ajustes para soportar ese caso
						$strCompletePath = trim((string) @$_POST["questionB64Images"][$anQuestion->QuestionID][$nRow]);
						if ($strCompletePath != '') {
							return $this->movePhotoToEbavel( $strCompletePath, $aFieldForm, $manager );
						}
					}
					break;
				case qtpSketch:
					//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					//Se volvió a separa la pregunta Sketch de la Sketch+ ya que ahora se pueden mandar diferente tipo de información en Sketch+, así que no tiene caso compartir si de todas
					//maneras habrá que seleccionar en base a otro parámetro en Sketch+
					return $this->movePhotoToEbavel( @$anAnswer['canvasPhoto'], $aFieldForm, $manager );
					break;
				//@JAPR 2019-03-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				case qtpSketchPlus:
					switch ($intAdditionalData) {
						case ebavMapStrokes:
							//Mapeo exclusivamente de los trazos de la pregunta Sketch, en este caso no hará el return para regresar la respuesta, la cual es el conjunto de vectores
							break;
						
						case ebavMapSrcImage:
							//Mapeo de la imagen original utilizada para generar el Sketch
							//En este caso puede haber o una foto tomada como respuesta, en cuyo caso tendrá preferencia, o bien una imagen default configurada en la pregunta
							$blnPathIncluded = false;
							$strImageToUse = (string) @$anAnswer['photo'];
							if ( !$strImageToUse ) {
								//Si se utiliza una imagen default, se asume que o estará directamente en la carpeta de eForms (customerImages)o que tendría que ser una referencia absoluta
								//http, en todo caso, se preparará para que la función no intente agregar nuevamente el path
								$strImageToUse = $anQuestion->QuestionImageSketch;
								if ( $strImageToUse ) {
									if ( strtolower(substr($strImageToUse, 0, 4)) != 'http' ) {
										//Si no se trata de una ruta absoluta, tiene que agregar el propio path de eForms para que la ruta relativa que debe traer esté completa
										$strImageToUse = getcwd()."\\".$strImageToUse;
									}
								}
								
								//En cualquier caso, ya no deberá anexar ningún path la función que realizará el copiado, así que se desactiva con este parámetro
								$blnPathIncluded = true;
							}
							
							return $this->movePhotoToEbavel( $strImageToUse, $aFieldForm, $manager, $blnPathIncluded );
							break;
						
						case ebavMapValue:
						default:
							//Mapeo de la imagen con los trazos del Sketch
							return $this->movePhotoToEbavel( @$anAnswer['canvasPhoto'], $aFieldForm, $manager );
							break;
					}
					break;
			}
			/*if( $anQuestion->QTypeID == qtpPhoto || $anQuestion->QTypeID == qtpSignature)
			{
				return $this->movePhotoToEbavel( @$anAnswer['photo'], $aFieldForm, $manager );
			}
			elseif ($anQuestion->QTypeID == qtpSketch) {
				return $this->movePhotoToEbavel( @$anAnswer['canvasPhoto'], $aFieldForm, $manager );
			}*/
			//@JAPR
		}

		if( is_array( @$anAnswer['multiAnswer'] ) && count( $anAnswer['multiAnswer'] ) )
		{

			if( !$aFieldForm->isMultipleChoice() )
				$sValue = implode(",", array_keys($anAnswer['multiAnswer']));
			else
				$sValue = array_keys($anAnswer['multiAnswer']);

		} else {
			//@JAPR 2018-01-03: Corregido un bug, no había validación en caso de preguntas que no mandaron su respuesta, lo cual provocaba warnings y errores en el
			//proceso de grabado de las capturas o destinos de datos (#K3OLK4)
			//@JAPR 2018-01-28: Corregido un bug, no había validación en caso de preguntas que no mandaron su respuesta, lo cual provocaba warnings y errores en el
			//proceso de grabado de las capturas o destinos de datos (esta vez no solo preguntar por NULL sino por la existencia de la propiedad) (#H5BG72)
			if ( !is_null($anAnswer) && isset($anAnswer['answer']) ) {
				if( !$aFieldForm->isMultipleChoice() )
					$sValue = $anAnswer['answer'];
				else
					$sValue = array( $anAnswer['answer'] );
			}
			//@JAPR
		}

		return $sValue;
	}

	function getEFormsMultiSectionAnswers( $eachMapping, $aFieldForm, $manager )
	{
		$arrValue = array();

		$questionid = intval( $eachMapping['questionid'] );
		//@JAPR 2019-07-17: Corregido un bug, no se estaba validando la existencia de la pregunta antes de procesarla, así que generaba warnings (#7N7R60)
		if ( !isset($this->answers[ $questionid ]) ) {
			return $arrValue;
		}
		//@JAPR
		
		for( $i = 0; $i < count($this->answers[ $questionid ]); $i++ )
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("eBavel getting value for row #{$i}");
			}
			$arrValue[] = $this->getEFormsAnswers($eachMapping, $this->answers[ $questionid ][$i], $i, $aFieldForm, $manager);
		}

		return $arrValue;
	}

	//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	/* Realiza el copiado de la imagen especificada hacia el folder correspondiente de eBavel ajustado al campo indicado
	El parámetro $bPathIncluded se usará para casos donde la imagen no necesariamente se encuentre en una de las ruta predefinidas originalmente asignadas por esta función según el tipo
	de dispositivo de la captura u otras condiciones, originalmente usada para poder enviar el path con la imagen default usada en la pregunta Sketch+ al ser mapeada la Imagen original) */
	function movePhotoToEbavel( $sPathPhotoForms, $aFieldForm, $manager, $bPathIncluded = false )
	{
		//@JAPR 2019-03-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Validado que si no existe una imagen entonces no intente realizar el copiado y regrese simplemente vacío (como si hubiera fallado), ya que estaba intentando copiar imágenes
		//inexistentes
		if ( !trim($sPathPhotoForms) ) {
			$this->logString( "movePhotoToEbavel: No image" );
			return '';
		}
		//@JAPR
		
		$this->logString( "movePhotoToEbavel: {$sPathPhotoForms}" );
		$params = array();
		$params['sectionname'] = $aFieldForm->sectionName;
		$params['widgettype'] = 'widget_photo';
		//@JAPR 2016-10-25: Corregido un bug con el mapeo hacia campos Firma y QR en eBavel 6, cambiaron la manera de accesar los paths de imagenes así que ahora hay que especificar
		//el tipo de Widget del que se trata el campo al que se realiza el mapeo para obtener la ruta correcta
		if (property_exists($aFieldForm, "tagname")) {
			$params['widgettype'] = (string) @$aFieldForm->tagname;
		}
		//@JAPR 2019-04-25: Corregido un bug con el mapeo de preguntas que generan imagenes hacia campos documento (#7D0C65)
		$params['filename'] = GetFileNameFromFilePath($sPathPhotoForms).(( !$bPathIncluded && strpos(strtolower($sPathPhotoForms), '.jpg') === false )?'.jpg':'');
		//@JAPR
		$params['ext'] = 'jpg';
		
		$strCompletePath = /*$this->eBavelPath.*/$manager->getFilePath($params, null, true);
		
		global $blnWebMode;
		global $useRecoveryPaths;
		
		//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ( $bPathIncluded ) {
			//En este caso se asumirá que la imagen ya contiene la ruta donde se encuentra, así que no se le concatenará nada adicional
			$path = '';
		}
		else {
			if ($blnWebMode) {
				$path = getcwd()."\\";
			}
			else {
				//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
				$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			}
			
			$path = $path."\\";
		}
		
		//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Se remueve la asignación de la \\ entre el path y el nombre de archivo para usar el nuevo parámetro $bPathIncluded
		$strCompleteSourcePath = str_replace("/", "\\", $path.$sPathPhotoForms);
		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
		//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Ahora si se recibe el parámetro $bPathIncluded se asumirá que ya contiene extensión, por lo que no le agregará ninguna adicional (la imagen fuente puede ser de algún formato
		//que no necesariamente es .jpg)
		if ( !$bPathIncluded && strpos(strtolower($strCompleteSourcePath), '.jpg') === false ) {
			$strCompleteSourcePath .= '.jpg';
		}
		//@JAPR
		
		//@JAPR 2016-10-25: Corregido un bug con el mapeo hacia campos Firma y QR en eBavel 6, cambiaron la manera de accesar los paths de imagenes así que ahora hay que especificar
		//el tipo de Widget del que se trata el campo al que se realiza el mapeo para obtener la ruta correcta
		//Ya no tiene caso aplicar este código, porque la función $manager->getFilePath utilizada arriba ajustada al tipo de widget del campo ya regresaría el path correcto
		/*if( method_exists($aFieldForm,'isSignature') && $aFieldForm->isSignature() )
		{
			//Cambiamos el nombre de la imagen por signature bft1696.Signature.{ext}
			$partes_ruta = pathinfo($strCompletePath);
			list($bftRandom) = explode('.', $partes_ruta['filename']);
			$strCompletePath = $partes_ruta['dirname'] . '/' . $bftRandom . '.Signature.' . $partes_ruta['extension'];
		}*/
		//@JAPR
		
		$status = @copy($strCompleteSourcePath, $strCompletePath);
		
		if( $status )
		{
			$this->logString( "Image copy complete: {$strCompleteSourcePath} to {$strCompletePath}." );
			$intPos = stripos($strCompletePath, "uploads/");
			if ($intPos !== false) {
				$strCompletePath = substr($strCompletePath, $intPos);
			}
			$this->logString( "movePhotoToEbavel final path: {$strCompletePath}" );
			return $strCompletePath;
		} else 
		{
			$this->logString( "Image copy fail: {$strCompleteSourcePath} to {$strCompletePath}." );
			return '';
		}
	}

	//@JAPR 2019-04-16: Corregido un bug con el mapeo de preguntas de documentos hacia eBavel (#7D0C65)
	/* Realiza el copiado del documento/audio/video especificado hacia el folder correspondiente de eBavel ajustado al campo indicado (todos los tipos de documentos de eBavel se graban
	en la misma carpeta, sin embargo el contenido de la ruta en el caso de eForms si depende del tipo de pregunta, por tanto puede ser reelevante esa información) */
	function moveDocumentToEbavel( $sPathDocumentForms, $aFieldForm, $manager, $bPathIncluded = false , $qTypeID = qtpDocument)
	{
		//@JAPR 2019-04-26: Corregido un bug, se había afectado al grabado con browser por no validar adecuadamente mediante la bandera que indica desde donde se mandó la información (#7D0C65)
		global $blnWebMode;
		//@JAPR
		
		//Validado que si no existe un documento entonces no intente realizar el copiado y regrese simplemente vacío (como si hubiera fallado), ya que estaba intentando copiar documentos
		//inexistentes
		if ( !trim($sPathDocumentForms) ) {
			$this->logString( "moveDocumentToEbavel: No document" );
			return '';
		}
		
		$this->logString( "moveDocumentToEbavel: {$sPathDocumentForms}" );
		$params = array();
		$params['sectionname'] = $aFieldForm->sectionName;
		$params['widgettype'] = 'widget_file';
		//@JAPR 2016-10-25: Corregido un bug con el mapeo hacia campos Firma y QR en eBavel 6, cambiaron la manera de accesar los paths de documentos así que ahora hay que especificar
		//el tipo de Widget del que se trata el campo al que se realiza el mapeo para obtener la ruta correcta
		if (property_exists($aFieldForm, "tagname")) {
			$params['widgettype'] = (string) @$aFieldForm->tagname;
		}
		//@JAPR
		$params['filename'] = GetFileNameFromFilePath($sPathDocumentForms);
		//$params['ext'] = 'jpg';
		
		$strCompletePath = /*$this->eBavelPath.*/$manager->getFilePath($params, null, true);
		
		global $blnWebMode;
		global $useRecoveryPaths;
		
		if ( $bPathIncluded ) {
			//En este caso se asumirá que el documento ya contiene la ruta donde se encuentra, así que no se le concatenará nada adicional
			$path = '';
		}
		else {
			if ($blnWebMode) {
				$path = getcwd()."\\";
			}
			else {
				//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
				$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			}
			
			$path = $path."\\";
		}
		
		//Se remueve la asignación de la \\ entre el path y el nombre de archivo para usar el nuevo parámetro $bPathIncluded
		//@JAPR 2019-04-23: Corregido un bug, en dispositivo por error en las Apps los nombres de archivos de documento incluían el path de fbm_bmd_#### (o cualquier otro), lo cual no era
		//esperado y por tanto producía resultados incorrectos al tratar de copiarlos hacia eBavel, pues el path origen generado incluía dos veces la subcarpeta fbm_bmd_####, así que
		//cuando sea un procesamiento desde dispositivo se asegurará de eliminar cualquier path adicional al nombre del archivo (#8NX2B9, #7D0C65)
		//@JAPR 2019-04-26: Corregido un bug, se había afectado al grabado con browser por no validar adecuadamente mediante la bandera que indica desde donde se mandó la información (#7D0C65)
		$strFilePath = ($blnWebMode)?$sPathDocumentForms:$params['filename'];
		$strCompleteSourcePath = str_replace("/", "\\", $path.$strFilePath);
		//@JAPR
		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
		/*if ( !$bPathIncluded && strpos(strtolower($strCompleteSourcePath), '.jpg') === false ) {
			$strCompleteSourcePath .= '.jpg';
		}*/
		//@JAPR
		
		$status = @copy($strCompleteSourcePath, $strCompletePath);
		
		if( $status )
		{
			$this->logString( "Document copy complete: {$strCompleteSourcePath} to {$strCompletePath}." );
			$intPos = stripos($strCompletePath, "uploads/");
			if ($intPos !== false) {
				$strCompletePath = substr($strCompletePath, $intPos);
			}
			$this->logString( "moveDocumentToEbavel final path: {$strCompletePath}" );
			return $strCompletePath;
		} else 
		{
			$this->logString( "Document copy fail: {$strCompleteSourcePath} to {$strCompletePath}." );
			return '';
		}
	}
	//@JAPR
	
	function getQuestionCollection()
	{
		if( $this->arrQuestionsColl ) return $this->arrQuestionsColl;

		$objQuestionsColl = BITAMQuestionCollection::NewInstance($this->repository, $this->surveyID);
		
		foreach ($objQuestionsColl->Collection as $aQuestion)
		{
			$arrQuestionsColl[$aQuestion->QuestionID] = $aQuestion;
		}

		$this->arrQuestionsColl = $arrQuestionsColl;

		return $this->arrQuestionsColl;
	}

	function getSectionCollection()
	{
		if( $this->arrSectionsColl ) return $this->arrSectionsColl;

		$objSectionsColl = @BITAMSectionCollection::NewInstance($this->repository, $this->surveyID);
		
		foreach ($objSectionsColl->Collection as $sectionKey => $aSection)
		{
			$arrSectionsColl[$aSection->SectionID] = $aSection;
		}

		$this->arrSectionsColl = $arrSectionsColl;

		return $this->arrSectionsColl;
	}

	function setEbavelDefaultValues($data, $sForm, $nKey)
	{
		//@JAPR 2018-02-28: Corregido un bug, dado a que eForms siempre inserta registros a eBavel y es eBavel quien decide si debe o no actualizar el registro, 
		//siempre se asignaban estos valores desde la captura de eForms pero en eBavel cuando estaban asignados desde aquí los sobrescribía siempre, así que se
		//validará para ya no mandarlos con eBavel 6 y será él quien se encargue de asignarlos correctamente (#JYK433)
		global $eBavelServiceVersion;
		
		if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
			//Si se trata de eBavel 6, es seguro que eBavel realizará el ajuste adecuado así que ya no se mandan estos datos default
		}
		else {
			//En caso de ser una versión previa de eBavel, por compatibilidad se seguirán mandando estos datos para no causar un comportamiento distinto
			$data['createdDate'] = date('Y-m-d H:i:s');
			$data['modifiedDate'] = date('Y-m-d H:i:s');
			$data['syncDate'] = date('Y-m-d H:i:s');
			$data['createdUserKey'] = $_SESSION["PABITAM_UserID"];
			$data['modifiedUserKey'] = $_SESSION["PABITAM_UserID"];
		}
		//@JAPR

		$data['id_' . $sForm] = $nKey;

		return $data;
	}

	function eBavelConfig( $aDestination )
	{
		if( !is_null( $this->blnCanSaveeBavel ) ) return $this->blnCanSaveeBavel;

		$this->blnCanSaveeBavel = true;

	    $objSetting = BITAMSettingsVariable::NewInstanceWithName($this->repository, 'EBAVELURL');

	    if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
		{
			
			$this->logString( "No eBavel path configured: ".$blnCanSaveeBavelActions );
			
			$this->blnCanSaveeBavel = false;
		}

		$arrURL = parse_url($objSetting->SettingValue);
		
		$strPath = str_replace('\\\\', '\\', str_replace('/', '\\', $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php'));

		if( !file_exists($strPath) ) {
			
			$this->blnCanSaveeBavel = false;

			$this->logString('Invalid eBavel path: ' . $strPath);

		}

		if( $this->blnCanSaveeBavel )
		{
			$this->logString( "Require_once " . $strPath );

			@require_once( $strPath );

			if (!function_exists('NotifyError'))
			{
				$this->logString("No NotifyError function");
			}
			else 
			{
				$this->logString("NotifyError function exists");
			}

			if (!class_exists('Tables'))
			{
				$this->logString("No eBavel code loaded");

				$this->blnCanSaveeBavel = false;
			}
		}

		$this->eBavelPath = str_ireplace('service.php', '', $strPath);

		return $this->blnCanSaveeBavel;
	}

	function loadeBavelApi( $aDestination )
	{
		
		$aRS = $this->repository->ADOConnection->Execute( "SELECT A.codeName FROM si_forms_apps A, si_forms_sections B WHERE B.id = '". $aDestination->eBavelForm ."' AND B.appid = A.id_app" );

		if ($aRS === false || $aRS->EOF)
		{
			$this->logString( "No eBavel app found: ".$surveyInstance->eBavelAppID );

	    	return false;
		}

		$manager = new Tables();
							
		$manager->notifyErrorService = true;
		
		$manager->applicationCodeName = $aRS->fields[0];
		
		$manager->init( $this->repository->ADOConnection );

		return $manager;
	}

	function getFieldsform( $aDestination )
	{
		/** Cargamos las instancias de los objetos de campo de eBavel */
		$arrFieldsEbavel = array();

		for($i = 0; $i < count( $aDestination->mapping ); $i++)
		{
			$arrFieldsEbavel[] = $aDestination->mapping[$i]['fieldformid'];
		}

		$resFields = BITAMAppFieldsForms::withIdsFromDB($arrFieldsEbavel, $this->repository->ADOConnection);

		$arrFieldsEbavel = array();
		for( $i = 0; $i < count( $resFields ); $i++ )
		{
			$arrFieldsEbavel[ $resFields[$i]->id ] = $resFields[$i];
		}

		return $arrFieldsEbavel;
	}

	function logString( $sMsg )
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString( $sMsg );
		}
		
		//@JAPR 2018-02-07: Corregido un bug, la bitácora de eBavel Actions nunca fue pensada para registrar cualquier tipo de destino de datos, sólo los envíos
		//hacia eBavel, así que se cambiará el log por el original de la tarea del agente
		//@logeBavelString( $sMsg );
		@logString( $sMsg, "", true, false, false );
		//@JAPR
	}
	function getValueConstant($intSurveyFieldID){		
		global $theUser;
		$intSurveyFieldID=abs($intSurveyFieldID);
		$arrUserNames=array();
		$arrayData=$this->options["arrayData"];
		$thisDate=$this->options["thisDate"];
		$syncDate=$this->options["syncDate"];
		$syncTime=$this->options["syncTime"];
		$theAppUser=$this->options["theAppUser"];
		$arrGPSData=$this->options["arrGPSData"];
		//@JAPR 2017-02-01: Agregado el mapeo del UUID (#TDYU15)
		$arrJSONData=$this->options["arrJSONData"];
		//@JAPR
		$factKeyDimVal=$this->reportId;
		$aSourceActionData=$this->options["aSourceActionData"];
		$anActionData = array();
		//Hay datos que siempre son fijos así que se agregan tal cual
		$anActionData["createdDate"] = @$aSourceActionData["createdDate"];
		$anActionData["modifiedDate"] = @$aSourceActionData["modifiedDate"];
		$aData='';
		switch ($intSurveyFieldID) {
			//*********************************************************************************************
			//Campos generales de la captura de encuesta
			//*********************************************************************************************
			case sfidStartDate:
				$aData = substr($arrayData["surveyDate"], 0, 10);
				break;
			case sfidStartHour:
				$aData = $arrayData["surveyHour"];
				break;
			case sfidEndDate:
				$aData = substr($thisDate, 0, 10);
				break;
			case sfidEndHour:
				$aData = substr($thisDate, 11);
				break;
			case sfidLocation:
				$blnSkipField = true;
				if(isset($arrayData['surveyLatitude'])) {
					$latitude = (double)$arrayData['surveyLatitude'];
				}
				else {
					$latitude = 0;
				}
				
				if(isset($arrayData['surveyLongitude'])) {
					$longitude = (double)$arrayData['surveyLongitude'];
				} 
				else {
					$longitude = 0;
				}
				
				if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $this->surveyInstance->SurveyID == 4) {
					$strMessageQuestion = trim((string) @$arrayData['qfield32']);
					if ($strMessageQuestion != '') {
						$arrData = explode(',', $strMessageQuestion);
						if (count($arrData) == 2) {
							$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
							$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
							if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
								$latitude = $strLatitude;
								$longitude = $strLongitude;
							}
						}
					}
				}
				
				$aData = $latitude.', '.$longitude;
				break;
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			case sfidLocationAcc:
				if(isset($arrayData['surveyAccuracy'])) 
				{
					$accuracy = (double)$arrayData['surveyAccuracy'];
				}
				else 
				{
					$accuracy = 0;
				}
				$aData = $accuracy;
				//@JAPR 2014-05-20: Corregido un bug, faltaba el break
				break;
			//@JAPR
			case sfidSyncDate:
				$aData = $syncDate;
				break;
			case sfidSyncHour:
				$aData = $syncTime;
				break;
			case sfidServerStartDate:
				if (isset($arrayData["serverSurveyDate"])) {
					$aData = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
				}
				break;
			case sfidServerStartHour:
				if (isset($arrayData["serverSurveyHour"])) {
					$aData = (string) @$arrayData["serverSurveyHour"];
				}
				break;
			case sfidServerEndDate:
				if (isset($arrayData["serverSurveyEndDate"])) {
					$aData = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
				}
				break;
			case sfidServerEndHour:
				if (isset($arrayData["serverSurveyEndHour"])) {
					$aData = (string) @$arrayData["serverSurveyEndHour"];
				}
				break;
			case sfidScheduler:
				$aData = null;	//@JAPRPendiente
				break;
			case sfidEMail:
				$aData = null;	//@JAPRPendiente
				break;
			case sfidDuration:
				$aData = null;	//@JAPRPendiente
				break;
			case sfidAnsweredQuestions:
				$aData = null;	//@JAPRPendiente
				break;
			case sfidEntrySection:
				$aData = 0;		//@JAPRPendiente
				break;
			case sfidDynamicPage:
				$aData = null;	//@JAPRPendiente
				break;
			case sfidDynamicOption:
				$aData = null;	//@JAPRPendiente
				break;
			case safidSourceRowKey:
			case sfidFactKey:
				$aData = (int) @$aSourceActionData["sourceRow_key"];
				break;
			case sfidEntryID:
				$aData = (int) $factKeyDimVal;
				break;
			//@JAPR 2013-05-03: Agregados campos extras al mapeo de datos general de la encuesta
			case sfidUserID:
				$aData = (int) @$theUser->UserID;
				break;
			//@JAPR 2013-05-06: Agregados campos extras al mapeo de datos general de la encuesta
			case sfidUserName:
				$aData = (string) @$theAppUser->UserName;
				break;
			//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
			case sfidGPSCountry:
				$aData = (string) @$arrGPSData['Country'];
				break;
			case sfidGPSState:
				$aData = (string) @$arrGPSData['State'];
				break;
			case sfidGPSCity:
				$aData = (string) @$arrGPSData['City'];
				break;
			case sfidGPSZipCode:
				$aData = (string) @$arrGPSData['ZipCode'];
				break;
			case sfidGPSAddress:
				$aData = (string) @$arrGPSData['Address'];
				break;
			case sfidGPSFullAddress:
				$aData = (string) @$arrGPSData['FullAddress'];
				break;
			//*********************************************************************************************
			//Campos exclusivos para acciones
			//*********************************************************************************************
			case safidDescription:
				$aData = (string) @$aSourceActionData["description"];
				break;
			//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
			case safidTitle:
				$aData = (string) @$aSourceActionData["title"];
				break;
			//@JAPR
			case safidResponsible:
				$intUserID = (int) @$aSourceActionData["user"];
				if ($intUserID <= 0) {
					$intUserID = 1;
				}
				
				if ($inteBavelQTypeID == qtpOpenNumeric) {
					//Si es un campo numérico entonces se graba directamente el ID del usuario
					$aData = $intUserID;
				}
				else {
					//En este caso se debe obtener el nombre del usuario
					if (!isset($arrUserNames[$intUserID])) {
						$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
						//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
						$objResponsible = BITAMeFormsUser::NewInstanceWithID($theRepository, $intUserID);
						$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
						if (!is_null($objResponsible)) {
							$strResponsibleName = $objResponsible->FullName;
						}
						$arrUserNames[$intUserID] = $strResponsibleName;
					}
					$strResponsibleName = (string) @$arrUserNames[$intUserID];
					$aData = $strResponsibleName;
				}
				break;
			case safidCategory:
				$aData = (string) @$aSourceActionData["category"];
				break;
			case safidDueDate:
				$aData = (string) @$aSourceActionData["deadline"];
				break;
			case safidStatus:
				$aData = $strDefaultActionStatus;
				break;
			case safidSource:
				$aData = $strDefaultActionSource;
				break;
			case safidSourceID:
				if ($inteBavelQTypeID == qtpOpenNumeric) {
					//Si es un campo numérico entonces se graba directamente el ID del usuario
					$aData = $surveyID;
				}
				else {
					//En este caso se debe obtener el nombre del usuario
					$aData = $this->surveyInstance->SurveyName;
				}
				break;
			//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
			case sfidFixedNumExpression:
				$aData = (float) @$aSourceActionData["value".$aneBavelFieldID]; 
				break;
			case sfidFixedStrExpression:
				$aData = (string) @$aSourceActionData["value".$aneBavelFieldID]; 
				break;
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			case sfidSurveyScore:
				$aData = (int) @$_POST['SurveyScore'];
				break;
			//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
			case sfidCatalogAttribute:
				$aData = (string) @$aSourceActionData["attribValue".$intAttributeID]; 
				break;
			//@JAPR 2017-02-01: Agregado el mapeo del UUID (#TDYU15)
			case sfidUUID:
				if (isset($arrJSONData["device"]["uuid"])) {
					$aData = (string) @$arrJSONData["device"]["uuid"];
				}
				break;
			//@JAPR
			default:
				$aData='';
				//Campos no conocidos regresarán null para que no se consideren
				/*if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Unknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");
				}
				@logeBavelString("Unknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");*/
				break;
		}
		return utf8_encode($aData);

	}
	function sendDropBox( $aDestination )
	{
		/** Obtenemos el archivo a colocar en DropBox */
		$sPath = $this->getExportFile( $aDestination->fileformat );
		//GCRUZ 2016-01-11. Obtener las imágenes de la captura para enviarlas
		$exportImages = $this->getExportImages();
		$sPath = $sPath["file"];

		$this->logString('Path Export file: ' . $sPath);
		$this->logString('Connection name: ' . $aDestination->connection['name']);

		//Enviar el archivo de datos sólo cuando no sea Incremental
		if (!$this->isIncremental)
		{
			$success=connectionCloud::uploadFile('dropBox', $aDestination->id_connection, $sPath, $this->repository, $this->surveyInstance, $this->getFileName( $sPath ),function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path);
			$this->insertDataDestinationLog($success,"Dropbox",$aDestination);
		}

		$this->logString('Finish dropBox');

		//GCRUZ 2016-01-11. Enviar las imagenes de la captura
		$pathImages = 'surveyimages/'.$this->repository->RepositoryName.'/';
		foreach ($exportImages as $anImage)
		{
			$this->logString('Path Image file: ' . $pathImages.$anImage);
			$this->logString('Connection name: ' . $aDestination->connection['name']);
			$self = $this;
			$success=connectionCloud::uploadFile('dropBox', $aDestination->id_connection, $pathImages.$anImage, $this->repository, $this->surveyInstance, $this->getImageName( $pathImages.$anImage ),function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path);
			$this->insertDataDestinationLog($success,"googleDrive", $aDestination);

			$this->logString('Finish dropBox');
		}

		if ($this->isIncremental)
			$this->saveIncDestinationEntry($aDestination->id);

	}

	function sendGoogleDrive( $aDestination )
	{
		/** Obtenemos el archivo a colocar en DRIVE */
		$sPath = $this->getExportFile( $aDestination->fileformat );
		//GCRUZ 2016-01-11. Obtener las imágenes de la captura para enviarlas
		$exportImages = $this->getExportImages();
		$sPath = $sPath["file"];

		$this->logString('Path Export file: ' . $sPath);
		$this->logString('Connection name: ' . $aDestination->connection['name']);

		//Enviar el archivo de datos sólo cuando no sea Incremental
		if (!$this->isIncremental)
		{
			$success=connectionCloud::uploadFile('googleDrive', $aDestination->id_connection, $sPath, $this->repository, $this->surveyInstance, $this->getFileName( $sPath ),null,$aDestination->realPath);
			$this->insertDataDestinationLog($success,"googleDrive", $aDestination);
		}

		$this->logString('Finish google drive');

		//GCRUZ 2016-01-11. Enviar las imagenes de la captura
		$pathImages = 'surveyimages/'.$this->repository->RepositoryName.'/';
		foreach ($exportImages as $anImage)
		{
			$this->logString('Path Image file: ' . $pathImages.$anImage);
			$this->logString('Connection name: ' . $aDestination->connection['name']);
			$self = $this;
			$success=connectionCloud::uploadFile('googleDrive', $aDestination->id_connection, $pathImages.$anImage, $this->repository, $this->surveyInstance, $this->getImageName( $pathImages.$anImage ),null,$aDestination->realPath);
			$this->insertDataDestinationLog($success,"googleDrive", $aDestination);

			$this->logString('Finish google drive');
		}

		if ($this->isIncremental)
			$this->saveIncDestinationEntry($aDestination->id);

	}

	function sendFTP($aDestination)
	{
		
		/** Obtenemos el archivo a colocar en FTP */
		$sPath = $this->getExportFile( $aDestination->fileformat );
		//GCRUZ 2016-01-11. Obtener las imágenes de la captura para enviarlas
		$exportImages = $this->getExportImages();
		$sPath = $sPath["file"];

		$this->logString('Path Export file: ' . $sPath);
		$this->logString('Connection name: ' . $aDestination->connection['name']);

		$this->logString('In incremental upload: ' . ( ($this->isIncremental)? 'Yes' : 'No' ));
		$self = $this;

		//Enviar el archivo de datos sólo cuando no sea Incremental
		if (!$this->isIncremental)
		{
			$success=connectionCloud::uploadFile('FTP', $aDestination->connection, $sPath, $this->repository, $this->surveyInstance, $this->getFileName( $sPath ), function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path);
			$this->insertDataDestinationLog($success,"FTP", $aDestination);
		}

		$this->logString('Finish FTP');

		//GCRUZ 2016-01-11. Enviar las imagenes de la captura
		$pathImages = 'surveyimages/'.$this->repository->RepositoryName.'/';
		foreach ($exportImages as $anImage)
		{
			$this->logString('Path Image file: ' . $pathImages.$anImage);
			$this->logString('Connection name: ' . $aDestination->connection['name']);
			$self = $this;
			$success=connectionCloud::uploadFile('FTP', $aDestination->connection, $pathImages.$anImage, $this->repository, $this->surveyInstance, $this->getImageName( $pathImages.$anImage ), function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path);
			$this->insertDataDestinationLog($success,"FTP", $aDestination);

			$this->logString('Finish FTP');
		}

		if ($this->isIncremental)
			$this->saveIncDestinationEntry($aDestination->id);
	}
	function sendHTTP($aDestination)
	{
		/** Obtenemos el archivo a colocar en HTTP */
		$sPath = $this->getExportFile( $aDestination->fileformat );
		//GCRUZ 2016-01-11. Obtener las imágenes de la captura para enviarlas
		$exportImages = $this->getExportImages();

		$sPath = $sPath["file"];

		$this->logString('Se mandara via HTTP');

		$this->logString('Path Export file: ' . $sPath);
		
		$this->logString('Connection name: ' . $aDestination->connection['name']);
		
		$self = $this;

		$pathImages = 'surveyimages/'.$this->repository->RepositoryName.'/';
		if ($aDestination->httpSendSinglePOST)
		{
			$arrDestinationFilesName = array();
			$arrDestinationFilesPath = array();
			//Enviar el archivo de datos
			$arrDestinationImgNames[] = $this->getFileName( $sPath );
			$arrDestinationImgPath[] = $sPath;
			//GCRUZ 2016-01-11. Enviar las imagenes de la captura
			foreach ($exportImages as $anImage)
			{
				$this->logString('Path Image file: ' . $pathImages.$anImage);
				$arrDestinationImgNames[] = $this->getImageName( $pathImages.$anImage );
				$arrDestinationImgPath[] = $pathImages.$anImage;
			}
			$this->logString('Connection name: ' . $aDestination->connection['name']);
			$self = $this;
			//GCRUZ 2016-01-12. Agregado parámetro getQueryString, para parámetros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
			$success=connectionCloud::uploadFile('HTTP', $aDestination->connection['name'], $arrDestinationImgPath, $this->repository, $this->surveyInstance, $arrDestinationImgNames, function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path, ($aDestination->Parameters? $aDestination->Parameters: NULL), 'fileType=ALL');
			$this->insertDataDestinationLog($success,"HTTP", $aDestination);
			$this->logString('Finish HTTP');
		}
		else
		{
		//Enviar el archivo de datos
		$success=connectionCloud::uploadFile('HTTP', $aDestination->connection['name'], $sPath, $this->repository, $this->surveyInstance, $this->getFileName( $sPath ), function($msg) use (&$self) {   
		  $self->logString( $msg ); 
		},$aDestination->path, ($aDestination->Parameters? $aDestination->Parameters: NULL));
		$this->insertDataDestinationLog($success,"HTTP", $aDestination);
		$this->logString('Finish HTTP');

		//GCRUZ 2016-01-11. Enviar las imagenes de la captura
		foreach ($exportImages as $anImage)
		{
			$this->logString('Path Image file: ' . $pathImages.$anImage);
			$this->logString('Connection name: ' . $aDestination->connection['name'].'?fileType=IMG');
			$self = $this;
			//GCRUZ 2016-01-12. Agregado parámetro getQueryString, para parámetros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
			$success=connectionCloud::uploadFile('HTTP', $aDestination->connection['name'], $pathImages.$anImage, $this->repository, $this->surveyInstance, $this->getImageName( $pathImages.$anImage ), function($msg) use (&$self) {   
			  $self->logString( $msg ); 
			},$aDestination->path, ($aDestination->Parameters? $aDestination->Parameters: NULL), 'fileType=IMG');
			$this->insertDataDestinationLog($success,"HTTP", $aDestination);
			$this->logString('Finish HTTP');
			}
		}
	}
	function insertDataDestinationLog($success,$destination, $aDestination=null){
		$SurveyTaskID=getParamValue('TaskID', 'both', '(int)', true);
		$creationDateID=date("Y-m-d H:i:s");
		$success["msg"]=str_replace('"','', $success["msg"]);
		if($aDestination && !is_null($aDestination->id))
		{
			$destinationID = $aDestination->id;
		}
		else
		{
			$destinationID = "NULL";
		}

		//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		//En este punto siempre debería haber un Id de captura asignado o no habría llegado hasta aquí
		$strSQL="INSERT INTO SI_SV_SurveyDestinationLog (`surveytasklogid`,`creationdateid`,`destination`,`description`,`status`,`SurveyID`,`userID`,`destinationID`, `SurveyKey`) 
			VALUES({$SurveyTaskID},'{$creationDateID}','{$destination}','{$success["msg"]}',{$success["error"]},0,'',{$destinationID},{$this->reportId})";
		//@JAPR
		$aBITAMConnection = connectToKPIRepository(); 
		$aBITAMConnection->ADOConnection->Execute($strSQL);
		if (getParamValue('DebugBreak', 'both', '(int)', true)){		
			ECHOString("insertDataDestinationLog ".$destinationID, 1, 1, "color:purple;");
			ECHOString($strSQL, 1, 1, "color:purple;");
		}
		
		//@JAPR 2016-10-10: Corregido un bug, no estaba registrando en el log de acciones este error, sólo en el log de depuración
		//@JAPR 2018-02-07: Corregido un bug, la bitácora de eBavel Actions nunca fue pensada para registrar cualquier tipo de destino de datos, sólo los envíos
		//hacia eBavel, así que se cambiará el log por el original de la tarea del agente
		$this->logString("Error processing the data destination: {$success["msg"]} - {$success["error"]}");
		//@logeBavelString("Error processing the data destination: {$success["msg"]} - {$success["error"]}");
		//@JAPR
	}

	function getExportFile( $type )
	{
		$this->logString('Export File type: ' . $type);
		if( $type == 1 )
		{
			return [ 'file' => realpath( $this->instaceExport->exportToCSV() ) ];
		}
		
		if( $type == 2 )
		{
			return [ 'file' => realpath( $this->instaceExport->exportToXLS() ) ];
		}

		if( $type == 3 )
			return $this->instaceExport->exportToPDF(false,$this->repository);

		return [ 'file' => '' ];
	}

	function getFileName( $sPath )
	{
		if ($this->isIncremental)
			return $this->incrementalFile . '.' . pathinfo( $sPath, PATHINFO_EXTENSION );
		else
			return $this->instaceExport->SurveyName . '_' . $this->reportId . '_'.date("YmdHis").'.' . pathinfo( $sPath, PATHINFO_EXTENSION );
	}

	//GCRUZ 2016-01-11. Obtener las imágenes de la captura para enviarlas
	function getExportImages()
	{
		$arrImages = array();
		$objectProperties = @get_object_vars($this->instaceExport);
		foreach (@$this->instaceExport->QuestionFields as $aQuestion)
		{
			if (isset($objectProperties[$aQuestion]))
			{
				if (isset($objectProperties[$aQuestion]['image']))
				{
					if (is_array($objectProperties[$aQuestion]['image']))
					{
						foreach ($objectProperties[$aQuestion]['image'] as $anImage)
						{
							if ($anImage != '' && $anImage != 'NA')
								$arrImages[] = $anImage;
						}
					}
					elseif ($objectProperties[$aQuestion]['image'] != '' && $objectProperties[$aQuestion]['image'] != 'NA')
						$arrImages[] = $objectProperties[$aQuestion]['image'];
				}
			}
		}
		return $arrImages;
	}

	function getImageName($sPath)
	{
		return pathinfo( $sPath, PATHINFO_BASENAME );
	}

	//GCRUZ 2016-01-21. Guardar el registro de la captura a envío de destino de datos. Sólo aplica para destinos FTP, DropBox y GoogleDrive
	function saveIncDestinationEntry($destinationID)
	{
		$aSQL = "INSERT INTO si_sv_incdatadestinations (datadestination_id, surveyid, surveykey, status) VALUES (";
		$aSQL .= $destinationID.", ";
		$aSQL .= $this->surveyID.", ";
		$aSQL .= $this->reportId.", ";
		$aSQL .= stsPending.")";
		$this->logString('Saving incremental Entry');
		$this->logString($aSQL);
		$this->repository->DataADOConnection->Execute($aSQL);
	}
}

?>