<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("surveyFieldMap.inc.php");
require_once('eBavelIntegration.inc.php');

class BITAMQuestionOptionActionMap extends BITAMObject
{
	public $FieldMapID;						//Identificador único de la tabla
	public $SurveyID;						//ID de la encuesta para la que se está configurando la columna
	public $QuestionID;						//ID de la pregunta para la que se está configurando la columna
	public $QuestionOptionID;				//ID de la opción de respuesta para la que se está configurando la columna
	public $SurveyFieldID;					//ID que identifica el tipo de columna de la que se trata (sólo los IDs negativos son los mismos ID entre todas las encuestas para el mismo tipo de columna, el resto de los IDs representan los QuestionIDs específicos de cada encuesta - no usados originalmente, pero por si alguna vez se solicitaban -. 0 (default) == No es un mapeo de columna)
	public $SurveyFieldText;				//Etiqueta para desplegar durante la configuración con el tipo de columna 
	public $eBavelFieldID;					//ID de FieldForm de eBavel al que se mapea esta columna
	public $eBavelAppID;					//ID de la aplicación de eBavel de la que se extraerán los campos para usar en el mapeo
	public $eBavelFormID;					//ID de la forma de eBavel de la que se extraerán los campos para usar en el mapeo
	//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
	public $Formula;						//Expresión que puede ser una constante fija (numérica o texto según el tipo de campo mapeado) que puede además contener variables, aunque estas variables se resolverían en el server y no todas aplican de la misma manera (ej. no se puede sacar el sumarizado de una sección múlti-registro en el server puesto que se procesa registro a registro)
	//@JAPR 2014-10-27: Agregado el mapeo de Scores
	public $DataID;							//ID del dato específico que se pide al elemento mapeado. Originalmente usado para las preguntas que son mapeadas a eBavel, sus valores eran sdidValue para pedir la respuesta de la pregunta, o bien sdidScore para pedir el score, pero se puede configurar cualquier posibilidad según el tipo de objeto al que se pide
	//@JAPR 2014-10-27: Agregado el mapeo de atributos de catálogos
	public $MemberID;						//ID del atributo específico que se está mapeando. Según el contexto, se puede obtener el valor de una sección (si se pide dentro de Inline o dinámica) o de una pregunta (si la sección donde se pide no existe asociado dicho catálogo entonces se trata de una pregunta estándar, o si está ese catálogo como una pregunta entonces se pide a ella, por ejemplo en Maestro-detalle)
	public $ArrayVariablesDataID;
	public $ArrayVariablesMemberID;
	public $ArrayVariablesFormula;
	//@JAPR
	public $ArrayVariables;					//Contenido de la tabla cuando se genera la instancia, utilizado para crear dinámicamente las propiedades cuando se carga como instancia para la página de configuración
	public $ArrayVariablesKeys;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	public $ArrayVariablesLabels;
	public $ArrayVariablesTypes;
	static $ArrayQuestionData = array();	//Datos utilizados de las preguntas (sólo son el Nombre, Etiqueta y tipo). Se usa así para no tener que cargar la instancia completa de las preguntas
	static $SurveysLoaded = array();		//Lista de las encuestas que ya han cargado la información de las preguntas
	
  	function __construct($aRepository, $aSurveyID = 0)
	{
		BITAMObject::__construct($aRepository);
		$this->FieldMapID = -1;
		$this->SurveyID = -1;
		$this->QuestionID = -1;
		$this->QuestionOptionID = -1;
		$this->SurveyFieldID = 0;
		$this->eBavelFieldID = -1;
		$this->eBavelAppID = -1;
		$this->eBavelFormID = -1;
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		$this->ArrayVariablesLabels = array();
		$this->ArrayVariablesTypes = array();
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		$this->ArrayVariablesDataID = array();
		$this->ArrayVariablesMemberID = array();
		$this->ArrayVariablesFormula = array();
		$this->Formula = '';
		$this->DataID = sdidValue;
		$this->MemberID = -1;
		//@JAPR
		if ($aSurveyID > 0) {
			self::GetQuestionData($aRepository, $aSurveyID);
		}
	}
	
	//Obtiene los nombres físicos y etiquetas de las preguntas de la encuesta especificada y llena los arrays estáticos con esa información
	static function GetQuestionData($aRepository, $aSurveyID) {
		if (isset(self::$SurveysLoaded[$aSurveyID])) {
			return;
		}
		
		$questionCollection = @BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		if (!is_null($questionCollection)) {
			foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
				$strQuestionField = 'qfield'.$aQuestion->QuestionID;
				self::$ArrayQuestionData[$strQuestionField] = array();
				self::$ArrayQuestionData[$strQuestionField]['Label'] = $aQuestion->QuestionText;
				self::$ArrayQuestionData[$strQuestionField]['Name'] = $strQuestionField;	//$aQuestion->AttributeName;
				self::$ArrayQuestionData[$strQuestionField]['Type'] = $aQuestion->QTypeID;
				//@JAPR 2013-07-19: Agregada la instancia porque se necesitaba mas datos, así que para no terminar duplicando el array se agregará
				//simplemente la instancia a la colección
				self::$ArrayQuestionData[$strQuestionField]['Obj'] = $aQuestion;
			}
		}
		
		self::$SurveysLoaded[$aSurveyID] = 1;
	}
	
	static function NewInstance($aRepository, $aSurveyID = 0)
	{
		return new BITAMQuestionOptionActionMap($aRepository, $aSurveyID);
	}
	
	static function NewInstanceWithID($aRepository, $aFieldMapID)
	{
		$anInstance = null;
		
		if (((int) $aFieldMapID) < 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-27: Agregado el mapeo de Scores
		//@JAPR 2014-10-27: Agregado el mapeo de atributos de catálogos
		$strAdditionalFields = "";
		if (getMDVersion() >= esveBavelScoreMapping) {
			$strAdditionalFields .= ', A.DataID, A.MemberID, A.Formula ';
		}
		//@JAPR
		$sql = "SELECT A.FieldMapID, A.SurveyID, A.QuestionID, A.QuestionOptionID, A.SurveyFieldID, A.eBavelFieldID $strAdditionalFields 
			FROM SI_SV_SurveyAnswerActionFieldsMap A 
			WHERE A.FieldMapID = ".((int) $aFieldMapID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMQuestionOptionActionMap::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$intSurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance = BITAMQuestionOptionActionMap::NewInstance($aRepository, $intSurveyID);
		$anInstance->FieldMapID = (int) @$aRS->fields["fieldmapid"];
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->QuestionID = (int) @$aRS->fields["questionid"];
		$anInstance->QuestionOptionID = (int) @$aRS->fields["questionoptionid"];
		$anInstance->SurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-27: Agregado el mapeo de Scores
		//@JAPR 2014-10-27: Agregado el mapeo de atributos de catálogos
		$anInstance->DataID = (int) @$aRS->fields["dataid"];
		$anInstance->MemberID = (int) @$aRS->fields["memberid"];
		$anInstance->Formula = (string) @$aRS->fields["formula"];
		//@JAPR
		$anInstance->SurveyFieldText = BITAMQuestionOptionActionMap::GetSurveyColumnText($anInstance->SurveyFieldID);
		return $anInstance;
	}
	
	//Regresa la etiqueta correspondiente a la columna indicada
	static function GetSurveyColumnText($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		if ($aSurveyFieldID < 0) {
			//Se trata de un campo default, internamente se graba como negativo, pero al ser identificado se tiene que hacer como positivo
			//Si es un campo específico de acciones, se obtiene la descripción de esta clase, de lo contrario se debe tratar de un campo default
			//de una captura así que se obtiene de la clase original
			$aSurveyFieldID *= -1;
			switch ($aSurveyFieldID) {
				case safidDescription:
					$strSurveyFieldText = translate('Action description');
					break;
				case safidResponsible:
					$strSurveyFieldText = translate('Action responsible');
					break;
				case safidCategory:
					$strSurveyFieldText = translate('Action category');
					break;
				case safidDueDate:
					$strSurveyFieldText = translate('Action due date');
					break;
				case safidStatus:
					$strSurveyFieldText = translate('Action status');
					break;
				case safidSource:
					$strSurveyFieldText = translate('Action source');
					break;
				case safidSourceID:
					$strSurveyFieldText = translate('Action source id');
					break;
				case safidSourceRowKey:
					$strSurveyFieldText = translate('Action source entry id');
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case safidTitle:
					$strSurveyFieldText = translate('Action title');
					break;
				//@JAPR
				default:
					$strSurveyFieldText = BITAMSurveyFieldMap::GetSurveyColumnText($aSurveyFieldID);
					break;
			}
		}
		else {
			//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
			$strQuestionField = 'qfield'.$aSurveyFieldID;
			$strSurveyFieldText = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Label'];
			if (is_null($strSurveyFieldText)) {
				$strSurveyFieldText = translate('Unknown');
			}
		}
		
		return $strSurveyFieldText;
	}
	
	//Regresa el nombre identificador de la columna para permitir la captura usando el Framework. Si regresa vacio entonces se trata de una columna
	//no soportada en esta versión
	static function GetSurveyColumnName($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		if ($aSurveyFieldID < 0) {
			//Se trata de un campo default, internamente se graba como negativo, pero al ser identificado se tiene que hacer como positivo
			//Si es un campo específico de acciones, se obtiene la descripción de esta clase, de lo contrario se debe tratar de un campo default
			//de una captura así que se obtiene de la clase original
			$aSurveyFieldID *= -1;
			switch ($aSurveyFieldID) {
				case safidDescription:
					$strSurveyFieldText = 'ActFldDescription';
					break;
				case safidResponsible:
					$strSurveyFieldText = 'ActFldResponsible';
					break;
				case safidCategory:
					$strSurveyFieldText = 'ActFldCategory';
					break;
				case safidDueDate:
					$strSurveyFieldText = 'ActFldDueDate';
					break;
				case safidStatus:
					$strSurveyFieldText = 'ActFldStatus';
					break;
				case safidSource:
					$strSurveyFieldText = 'ActFldSource';
					break;
				case safidSourceID:
					$strSurveyFieldText = 'ActFldSourceID';
					break;
				case safidSourceRowKey:
					$strSurveyFieldText = 'ActFldSourceEntryID';
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case safidTitle:
					$strSurveyFieldText = 'ActFldTitle';
					break;
				//@JAPR
				default:
					$strSurveyFieldText = BITAMSurveyFieldMap::GetSurveyColumnName($aSurveyFieldID);
					break;
			}
		}
		else {
			//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
			$strQuestionField = 'qfield'.$aSurveyFieldID;
			$strSurveyFieldText = @BITAMQuestionOptionActionMap::$ArrayQuestionData[$strQuestionField]['Name'];
			if (is_null($strSurveyFieldText)) {
				$strSurveyFieldText = '';
			}
		}
		
		return $strSurveyFieldText;
	}

	//Regresa id de la columna a partir del nombre identificador. Los valores se regresarán negativos para campos default o positivos para preguntas
	static function GetSurveyColumnID($aSurveyFieldName) {
		$aSurveyFieldName = strtolower(trim((string) $aSurveyFieldName));
		
		$intSurveyFieldID = 0;
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//Primero verifica si el nombre corresponde a un campo de pregunta, si no es así entonces ya intenta con los campos default de acciones
		//y posteriormente de la encuesta
		if (stripos($aSurveyFieldName, 'qfield') !== false) {
			//Se trata de una pregunta
			$intSurveyFieldID = (int) str_ireplace('qfield', '', $aSurveyFieldName);
		}
		else {
			//Es un campo default ya sea de acciones o de la encuesta
			switch ($aSurveyFieldName) {
				case 'actflddescription':
					$intSurveyFieldID = safidDescription;
					break;
				case 'actfldresponsible':
					$intSurveyFieldID = safidResponsible;
					break;
				case 'actfldcategory':
					$intSurveyFieldID = safidCategory;
					break;
				case 'actfldduedate':
					$intSurveyFieldID = safidDueDate;
					break;
				case 'actfldstatus':
					$intSurveyFieldID = safidStatus;
					break;
				case 'actfldsource':
					$intSurveyFieldID = safidSource;
					break;
				case 'actfldsourceid':
					$intSurveyFieldID = safidSourceID;
					break;
				case 'actfldsourceentryid':
					$intSurveyFieldID = safidSourceRowKey;
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case 'actfldtitle':
					$intSurveyFieldID = safidTitle;
					break;
				//@JAPR
				default:
					$intSurveyFieldID = BITAMSurveyFieldMap::GetSurveyColumnID($aSurveyFieldName);
					break;
			}
		}
		
		return $intSurveyFieldID;
	}
	
	//Llena la instancia de campos de eBavel con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		$this->ArrayVariablesLabels = array();
		$this->ArrayVariablesTypes = array();
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		$this->ArrayVariablesDataID = array();
		$this->ArrayVariablesMemberID = array();
		$this->ArrayVariablesFormula = array();
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		
		//Primero obtiene la lista de columnas de eBavel de la forma especificada
		//Este array obtendrá indexado por el tagname, objetos con datos genéricos de cada campo de eBavel de la Aplicación/Forma seleccionada
		try {
			$arreBavelFields = GetEBavelCollectionByField(@GetEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null), '', 'key', false, 'tagname');
		} catch (Exception $e) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nException reading eBavel action fields collection: ".$e->getMessage());
			}
			$arreBavelFields = array();
		}
		
		foreach ($arreBavelFields as $strTagName => $aFieldsColl) {
			//No se puede usar array_combine porque no se desea reorganizar los índices numéricos de los keys de cada campo en $aFieldsColl
			$intQTypeID = convertEBavelToEFormsFieldType($strTagName);
			
			//Si es un tipo de pregunta inválido continua con los siguientes campos
			//@JAPR 2013-05-02: Agregado el tipo de pregunta inválido, porque algunos tipos negativos son dummies para soporte de eBavel
			if ($intQTypeID == qtpInvalid) {
				continue;
			}
			
			foreach ($aFieldsColl as $intKey => $aField) {
				$inteBavelFieldID = (int) @$aField['id_fieldform'];
				if ($inteBavelFieldID <= 0) {
					continue;
				}
				
				$strFieldName = 'id_fieldform'.$inteBavelFieldID;
				$this->ArrayVariablesKeys[$inteBavelFieldID] = 0;
				$this->ArrayVariables[$inteBavelFieldID] = $strFieldName;
				$this->ArrayVariableValues[$inteBavelFieldID] = 0;
				$this->ArrayVariablesExist[$inteBavelFieldID] = false;
				$strFieldLabel = (string) @$aField['label'];
				if (trim($strFieldLabel) == '') {
					$strFieldLabel = $strFieldName;
				}
				$this->ArrayVariablesLabels[$inteBavelFieldID] = $strFieldLabel;
				$this->ArrayVariablesTypes[$inteBavelFieldID] = $intQTypeID;
				//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
				//@JAPR 2014-10-28: Agregado el mapeo de Scores
				//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
				//Por default no hay fórmulas, ni mapeo de atributos, y todos los campos regresan el value en lugar del score
				$this->ArrayVariablesDataID[$inteBavelFieldID] = sdidValue;
				$this->ArrayVariablesMemberID[$inteBavelFieldID] = 0;
				$this->ArrayVariablesFormula[$inteBavelFieldID] = '';
				//@JAPR
			}
		}
	}
	
	//Carga la configuración completa para una opción de respuesta (esta es la instancia que se deberá usar para desplegar la ventana de configuración)
	static function NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionOptionID, $aneBavelAppID = 0, $aneBavelFormID = 0)
	{
		require_once("survey.inc.php");
		require_once("questionoption.inc.php");
		
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$anInstanceQOption = BITAMQuestionOption::NewInstanceWithID($aRepository, $aQuestionOptionID);
		$intQuestionID = (int) @$anInstanceQOption->QuestionID;
		$inteBavelAppID = $aneBavelAppID;
		if ($inteBavelAppID <= 0) {
			$inteBavelAppID = (int) @$surveyInstance->eBavelAppID;
		}
		$inteBavelFormID = $aneBavelFormID;
		if ($inteBavelFormID <= 0) {
			$inteBavelFormID = (int) @$anInstanceQOption->eBavelActionFormID;
		}
		
		$anInstanceTemp = null;
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		$strAdditionalFields = "";
		if (getMDVersion() >= esveBavelScoreMapping) {
			$strAdditionalFields .= ', DataID, MemberID, Formula ';
		}
		//@JAPR
		$sql = "SELECT FieldMapID, SurveyID, QuestionID, QuestionOptionID, SurveyFieldID, eBavelFieldID $strAdditionalFields 
			FROM SI_SV_SurveyAnswerActionFieldsMap 
			WHERE SurveyID = $aSurveyID AND QuestionOptionID = $aQuestionOptionID 
			ORDER By FieldMapID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$anInstanceTemp = BITAMQuestionOptionActionMap::NewInstance($aRepository, $aSurveyID);
		$anInstanceTemp->SurveyID = $aSurveyID;
		$anInstanceTemp->QuestionID = $intQuestionID;
		$anInstanceTemp->QuestionOptionID = $aQuestionOptionID;
		$anInstanceTemp->eBavelAppID = $inteBavelAppID;
		$anInstanceTemp->eBavelFormID = $inteBavelFormID;
		$anInstanceTemp->resetDefaultSettings();
		
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariablesKeys = &$anInstanceTemp->ArrayVariablesKeys;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesLabels = &$anInstanceTemp->ArrayVariablesLabels;
		$ArrayVariablesTypes = &$anInstanceTemp->ArrayVariablesTypes;
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		$ArrayVariablesDataID = &$anInstanceTemp->ArrayVariablesDataID;
		$ArrayVariablesMemberID = &$anInstanceTemp->ArrayVariablesMemberID;
		$ArrayVariablesFormula = &$anInstanceTemp->ArrayVariablesFormula;
		//@JAPR
		$ArrayVariablesExist = array();
		
		while(!$aRS->EOF) {
			$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
			$intSurveyID = (int) @$aRS->fields["surveyid"];
			$intQuestionID = (int) @$aRS->fields["questionid"];
			$intQuestionOptionID = (int) @$aRS->fields["questionoptionid"];
			$intSurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
			$inteBavelFieldID = $aRS->fields["ebavelfieldid"];
			//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
			//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
			//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
			//@JAPR 2014-10-28: Agregado el mapeo de Scores
			//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
			$intDataID = (int) @$aRS->fields["dataid"];
			$intMemberID = (int) @$aRS->fields["memberid"];
			$strFormula = (string) @$aRS->fields["formula"];
			//@JAPR
			
			//Si el campo de la configuración aun existe, entonces actualiza los arrays, de lo contrario elimina la configuración de la tabla
			if (isset($ArrayVariables[$inteBavelFieldID])) {
				$ArrayVariablesKeys[$inteBavelFieldID] = $intFieldMapID;
				$ArrayVariableValues[$inteBavelFieldID] = $intSurveyFieldID;
				$ArrayVariablesExist[$inteBavelFieldID] = true;
				//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
				//@JAPR 2014-10-28: Agregado el mapeo de Scores
				//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
				$ArrayVariablesDataID[$inteBavelFieldID] = $intDataID;
				$ArrayVariablesMemberID[$inteBavelFieldID] = $intMemberID;
				$ArrayVariablesFormula[$inteBavelFieldID] = $strFormula;
				//@JAPR
			}
			else {
				$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap 
					WHERE QuestionOptionID = ".$aQuestionOptionID." AND EBavelFieldID = $inteBavelFieldID";
				$aRepository->DataADOConnection->Execute($sql);
			}
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMQuestionOptionActionMapExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMQuestionOptionActionMapExtended extends BITAMQuestionOptionActionMap'."\n";
		$strtemp = $strtemp.'	{'."\n";
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		foreach($ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$intVariableValue = (int) @$ArrayVariableValues[$inteBavelFieldID];
			$strtemp = $strtemp.'		public $'.$strVariableName.' = '.$intVariableValue.';'."\n";
			//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
			//@JAPR 2014-10-28: Agregado el mapeo de Scores
			//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
			$strtemp = $strtemp.'		public $'.$strVariableName.'DataID = '.$ArrayVariablesDataID[$inteBavelFieldID].';'."\n";
			$strtemp = $strtemp.'		public $'.$strVariableName.'MemberID = '.$ArrayVariablesMemberID[$inteBavelFieldID].';'."\n";
			//@JAPR 2015-01-22: Corregido un bug, las expresiones pueden contener variables con el nuevo formato, el cual es parecido a las
			//variables de php ({$Q}) así que no se puede delimitar entre "" o tratará de resolverlas como variables de PhP
			$strtemp = $strtemp.'		public $'.$strVariableName.'Formula = \''.addslashes($ArrayVariablesFormula[$inteBavelFieldID]).'\';'."\n";
			//@JAPR
		}
		
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMQuestionOptionActionMapExtended($aRepository);'."\r\n";
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		
		$theobject->SurveyID = $aSurveyID;
		$theobject->QuestionID = $intQuestionID;
		$theobject->QuestionOptionID = $aQuestionOptionID;
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariablesKeys = $ArrayVariablesKeys;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		$theobject->ArrayVariablesLabels = $ArrayVariablesLabels;
		$theobject->ArrayVariablesTypes = $ArrayVariablesTypes;
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		$theobject->ArrayVariablesDataID = $ArrayVariablesDataID;
		$theobject->ArrayVariablesMemberID = $ArrayVariablesMemberID;
		$theobject->ArrayVariablesFormula = $ArrayVariablesFormula;
		//@JAPR
		$theobject->IsDialog = true;
		
		return($theobject);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$inteBavelAppID = (int) @$aHTTPRequest->GET["eBavelAppID"];
		$inteBavelFormID = (int) @$aHTTPRequest->GET["eBavelFormID"];
		$aSurveyID = (int) @$aHTTPRequest->POST["SurveyID"];
		$aQuestionOptionID = (int) @$aHTTPRequest->POST["QuestionOptionID"];
		if ($aSurveyID > 0 && $aQuestionOptionID > 0)
		{
			$anInstance = BITAMQuestionOptionActionMap::NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionOptionID, $inteBavelAppID, $inteBavelFormID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMQuestionOptionActionMap::NewInstance($aRepository, $aSurveyID);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else {
			$aSurveyID = (int) @$aHTTPRequest->GET["SurveyID"];
			$aQuestionOptionID = (int) @$aHTTPRequest->GET["QuestionOptionID"];
		}
		
		$anInstance = BITAMQuestionOptionActionMap::NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionOptionID, $inteBavelAppID, $inteBavelFormID);
		if (!is_null($anInstance)) {
			$anInstance->eBavelAppID = $inteBavelAppID;
			$anInstance->eBavelFormID = $inteBavelFormID;
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			if (array_key_exists($strVariableName, $anArray))
			{
				$this->$strVariableName = $anArray[$strVariableName];
				$this->ArrayVariableValues[$inteBavelFieldID] = $anArray[$strVariableName];
				//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
				//@JAPR 2014-10-28: Agregado el mapeo de Scores
				//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
				$this->ArrayVariablesDataID[$inteBavelFieldID] = (int) @$anArray[$strVariableName.'DataID'];
				$this->ArrayVariablesMemberID[$inteBavelFieldID] = (int) @$anArray[$strVariableName.'MemberID'];
				$this->ArrayVariablesFormula[$inteBavelFieldID] = (int) @$anArray[$strVariableName.'Formula'];
				//@JAPR
			}
		}
		return $this;
	}
	
	function save()
	{
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$sql = "";
			$intVariableValue = (int) @$this->$strVariableName;
			//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
			//@JAPR 2014-10-28: Agregado el mapeo de Scores
			//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
			$intDataID = (int) @$this->ArrayVariablesDataID[$inteBavelFieldID];
			$intMemberID = (int) @$this->ArrayVariablesMemberID[$inteBavelFieldID];
			$strFormula = (string) @$this->ArrayVariablesFormula[$inteBavelFieldID];
			//@JAPR
			if (isset($this->ArrayVariablesExist[$inteBavelFieldID]) && $this->ArrayVariablesExist[$inteBavelFieldID])
			{
				if ($intVariableValue != 0) {
					//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
					//@JAPR 2014-10-28: Agregado el mapeo de Scores
					//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
					$strAdditionalValues = '';
					if (getMDVersion() >= esveBavelScoreMapping) {
						$strAdditionalValues .= ', DataID = '.$intDataID.
							', MemberID = '.$intMemberID.', Formula = '.$this->Repository->DataADOConnection->Quote($strFormula);
					}
					//@JAPR
					$sql = "UPDATE SI_SV_SurveyAnswerActionFieldsMap SET SurveyFieldID = ".$intVariableValue.$strAdditionalValues.
					" WHERE QuestionOptionID = ".$this->QuestionOptionID." AND EBavelFieldID = $inteBavelFieldID";
				}
				else {
					$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap 
						WHERE QuestionOptionID = ".$this->QuestionOptionID." AND EBavelFieldID = $inteBavelFieldID";
				}
			}
			else 
			{
				$sql =  "SELECT ".
					$this->Repository->DataADOConnection->IfNull("MAX(FieldMapID)", "0")." + 1 AS FieldMapID".
					" FROM SI_SV_SurveyAnswerActionFieldsMap";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
				
				if ($intVariableValue != 0) {
					//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
					//@JAPR 2014-10-28: Agregado el mapeo de Scores
					//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
					$strAdditionalFields = '';
					$strAdditionalValues = '';
					if (getMDVersion() >= esvInlineTypeOfFilling) {
						$strAdditionalFields .= ', DataID, MemberID, Formula';
						$strAdditionalValues .= ','.$intDataID.','.$intMemberID.', '.$this->Repository->DataADOConnection->Quote($strFormula);
					}
					//@JAPR
					$sql = "INSERT INTO SI_SV_SurveyAnswerActionFieldsMap (FieldMapID, SurveyID, QuestionID, QuestionOptionID, eBavelFieldID, SurveyFieldID".$strAdditionalFields.") 
						VALUES (".$intFieldMapID.", ".$this->SurveyID.", ".$this->QuestionID.", ".$this->QuestionOptionID.", ".$inteBavelFieldID.", ".$intVariableValue.$strAdditionalValues.")";
				}
				else {
					$sql = "";
				}
			}
			
			if ($sql != '' && $this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	function isNewObject()
	{
		return false;
	}
	
	function get_Title()
	{
		return translate("eBavel action field mapping");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=QuestionOptionActionMap";
	}
	
	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FieldMapID';
	}
	
	function get_FormFields($aUser)
	{
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
		global $garrActionFields;
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		global $arrOptionsWithDataID;
		global $arrOptionsWithMemberIdID;
		global $arrOptionsWithFormula;
		
		if (!isset($arrOptionsWithDataID)) {
			$arrOptionsWithDataID = array();
		}
		if (!isset($arrOptionsWithMemberIdID)) {
			$arrOptionsWithMemberIdID = array();
		}
		if (!isset($arrOptionsWithFormula)) {
			$arrOptionsWithFormula = array();
		}
		//@JAPR
		
		require_once("formfield.inc.php");
		require_once("question.inc.php");
		
		$myFields = array();
		$categoryCatalogField = null;
		
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		require_once('catalog.inc.php');
		$arrAllSurveyCatalogs = BITAMCatalog::GetUsedCatalogs($this->Repository, array($this->SurveyID));
		//Ajusta el tamaño de las etiquetas para que sean estandarizados al mismo ancho
		$intMaxLength = 0;
		foreach ($arrAllSurveyCatalogs as $intCatalogID => $strCatalogName) {
			$intLength = strlen($strCatalogName);
			if ($intLength > $intMaxLength) {
				$intMaxLength = $intLength;
			}
		}
		
		foreach ($arrAllSurveyCatalogs as $intCatalogID => $strCatalogName) {
			$intLength = strlen($strCatalogName);
			$intNumAddChars = (($intMaxLength - $intLength) + 6) / 2;
			$arrAllSurveyCatalogs[$intCatalogID] = str_repeat('-', $intNumAddChars).$strCatalogName.str_repeat('-', $intNumAddChars);
		}
		
		$arrCatalogIDs = array_keys($arrAllSurveyCatalogs);
		$arrSurveyAttributesByCatalog = array(0 => '('.translate("None").')');
		if (count($arrCatalogIDs)) {
			$arrAllSurveyAttributes = array();
			$arrAllSurveyAttributes = BITAMCatalogMember::GetCatMembersByCatalogID($this->Repository, $arrCatalogIDs);
			foreach ($arrAllSurveyAttributes as $intCatalogID => $arrCatalogAttributes) {
				//Agrega un elemento con ID negativo para que ningún valor lo pueda seleccionar, ya que sólo es un agrupador de atributos
				$arrSurveyAttributesByCatalog[$intCatalogID * -1] = (string) @$arrAllSurveyCatalogs[$intCatalogID];
				foreach ($arrCatalogAttributes as $intCatMemberID => $strCatMemberName) {
					//Agrega todos los atributos del catálogo correspondiente
					$arrSurveyAttributesByCatalog[$intCatMemberID] = "".(string) @$strCatMemberName;
				}
			}
		}
		
		//Agrega la encuesta ya que realmente la ventana graba en base a ella y no a un campo específico
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->SurveyID => 'Survey');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionID";
		$aField->Title = translate("Question");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->QuestionID => 'Question');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionOptionID";
		$aField->Title = translate("Question Option");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->QuestionOptionID => 'Question Option');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
		$arreBavelApps = array(0 => '('.translate('None').')');
		$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true);
		
		$aFieldApps = BITAMFormField::NewFormField();
		$aFieldApps->Name = "eBavelAppID";
		$aFieldApps->Title = translate("eBavel application");
		$aFieldApps->Type = "Object";
		$aFieldApps->VisualComponent = "Combobox";
		$aFieldApps->Options = $arreBavelApps;
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$aFieldApps->InTitle = true;
		$aFieldApps->IsVisible = false;
		$myFields[$aFieldApps->Name] = $aFieldApps;
		
		//Obtiene la lista de formas de eBavel creadas en el repositorio
		$arreBavelForms = array('' => array('' => '('.translate('None').')'));
		$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId');
		
		$aFieldForms = BITAMFormField::NewFormField();
		$aFieldForms->Name = "eBavelFormID";
		$aFieldForms->Title = translate("eBavel form");
		$aFieldForms->Type = "Object";
		$aFieldForms->VisualComponent = "Combobox";
		$aFieldForms->Options = $arreBavelForms;
		$aFieldForms->InTitle = true;
		$aFieldForms->IsVisible = false;
		$myFields[$aFieldForms->Name] = $aFieldForms;
		$aFieldForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldForms;
		
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//En este caso se generan arrays por tipos de preguntas, incluyendo adicionalmente los datos fijos de la acción ajustados a ciertos tipos,
		//posteriormente se generarán los arrays de las preguntas/datos fijos de la encuesta que apliquen según el tipo de campo de eBavel
		$arrEmptyeBavelForms = array(0 => '('.translate('None').')');
        $questionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
        if (is_null($questionCollection)) {
        	$questionCollection = new stdClass();
        	$questionCollection->Collection = array();
        }
		
        $arreFormsNumericQuestions = array();
        $arreFormsAlphaQuestions = array();
        $arreFormsTextQuestions = array();
        $arreFormsDateQuestions = array();
        $arreFormsTimeQuestions = array();
        $arreFormsSimpleChoiceQuestions = array();
        $arreFormsMultipleChoiceQuestions = array();
        $arreFormsPhotoQuestions = array();
        $arreFormsSignatureQuestions = array();
        $arreFormsDocumentQuestions = array();
        $arreFormsEMailQuestions = array();
        $arreFormsAllQuestions = array();
        $arreFormsLocationQuestions = array();
        
        //Agrega las columnas default
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
        //for ($intSurveyFieldID = sfidStartDate; $intSurveyFieldID <= safidSourceID; $intSurveyFieldID++) {
        foreach ($garrActionFields as $intSurveyFieldID) {
        	$intQTypeID = getDefaultEFormsColumnQTypeID($intSurveyFieldID);
        	if ($intQTypeID == qtpInvalid) {
        		continue;
        	}
        	
        	$intNegSurveyFieldID = $intSurveyFieldID * -1;
        	$strColumnName = BITAMQuestionOptionActionMap::GetSurveyColumnText($intNegSurveyFieldID);
        	$blnValid = true;
        	switch ($intQTypeID) {
        		case qtpOpenNumeric:
        		case qtpCalc:
        			$arreFormsNumericQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpSingle:
			        $arreFormsSimpleChoiceQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpMulti:
			        $arreFormsMultipleChoiceQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpOpenDate:
        			$arreFormsDateQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpOpenString:
        			$arreFormsTextQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpOpenAlpha:
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;

        		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpAction:
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpPhoto:
        			$arreFormsPhotoQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpSignature:
					//@JAPRPendiente
        			//$arreFormsSignatureQuestions[$intNegSurveyFieldID] = $strColumnName;
        			$blnValid = false;
        			break;
        		case qtpAudio:
				case qtpVideo:
        		case qtpDocument:
					//@JAPRPendiente
        			//$arreFormsDocumentQuestions[$intNegSurveyFieldID] = $strColumnName;
        			$blnValid = false;
        			break;
        			
        		case qtpOpenTime:
        			$arreFormsTimeQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
        		case qtpLocation:
        			$arreFormsLocationQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpEMail:
        			$arreFormsEMailQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
				//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
        		case qtpAttribute:
        			//En este caso se trata de un mapeo de atributo, no se regresa un único valor sino un array con todos los atributos aplicables
        			//a la forma en que se está haciendo el mapeo, donde el nombre de campo ya contendrá la referencia al ID de atributo (internamente
        			//el ID de campo a mapear es el mismo sfidCatalogAttribute, pero con campos adicionales se indicará que atributo específicamente
        			//es el que se mapeó)
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		//@JAPR
        			
        		case qtpShowValue:
        		case qtpMessage:
        		case qtpSkipSection:
        		case qtpSync:
        		case qtpCallList:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
        		case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
        		case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
        		default:
        			$blnValid = false;
        			break;
        	}
        	
        	if ($blnValid) {
        		$arreFormsAllQuestions[$intNegSurveyFieldID] = $strColumnName;
        	}
        }
        
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		//Llena los arrays que se van a utiliza en el App para validar que opciones adicionales deben mostrarse. Básicamente todas las preguntas
		//que son simple o múltiple choice podrán decidir entre Valor y Score, las demás no deberían mostrar ese componente DataID. Sólo si seleccionan
		//una expresión fija (ya sea numérica o texto) se mostrará el input de Fórmula, y sólo si seleccionan la opción de Catálogo mostrará el
		//selector del atributo
		$arrOptionsWithFormula[] = sfidFixedStrExpression * -1;
		$arrOptionsWithFormula[] = sfidFixedNumExpression * -1;
		$arrOptionsWithMemberIdID[] = sfidCatalogAttribute * -1;
        
        //Agrega las columnas de las preguntas
        foreach ($questionCollection->Collection as $objQuestion) {
        	$intQTypeID = $objQuestion->QTypeID;
        	$intQuestionID = $objQuestion->QuestionID;
        	$strQuestionNumber = $objQuestion->QuestionNumber.'- ';
        	$strQuestionText = $strQuestionNumber.$objQuestion->QuestionText;
        	$blnValid = true;
        	
        	switch ($intQTypeID) {
        		case qtpOpenNumeric:
        		case qtpCalc:
        			$arreFormsNumericQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpSingle:
			        $arreFormsSimpleChoiceQuestions[$intQuestionID] = $strQuestionText;
					//@JAPR 2014-10-28: Agregado el mapeo de Scores
        			$arreFormsNumericQuestions[$intQuestionID] = $strQuestionText;
			        $arrOptionsWithDataID[] = $intQuestionID;
        			break;
        		
        		case qtpMulti:
			        $arreFormsMultipleChoiceQuestions[$intQuestionID] = $strQuestionText;
					//@JAPR 2014-10-28: Agregado el mapeo de Scores
        			$arreFormsNumericQuestions[$intQuestionID] = $strQuestionText;
			        $arrOptionsWithDataID[] = $intQuestionID;
        			break;
        		
        		case qtpOpenDate:
        			//@JAPR 2013-07-15: Corregido un bug, se estaba mapeando hacia las columnas numéricas
        			$arreFormsDateQuestions[$intQuestionID] = $strQuestionText;
        			//@JAPR
        			break;
        			
        		case qtpOpenString:
        			$arreFormsTextQuestions[$intQuestionID] = $strQuestionText;
        			break;
        			
        		case qtpOpenAlpha:
        			$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;

        		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;

        		case qtpAction:
        			$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpPhoto:
        			$arreFormsPhotoQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
				//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
        			$arreFormsLocationQuestions[$intQuestionID] = $strQuestionText;
        			break;
				//@JAPR
				
        		case qtpSignature:
					//@JAPRPendiente
        			//$arreFormsSignatureQuestions[$intQuestionID] = $strQuestionText;
        			$blnValid = false;
        			break;
        		case qtpAudio:
				case qtpVideo:
        		case qtpDocument:
					//@JAPRPendiente
        			//$arreFormsDocumentQuestions[$intQuestionID] = $strQuestionText;
        			$blnValid = false;
        			break;
        			
        		case qtpOpenTime:
        			$arreFormsTimeQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpShowValue:
        		case qtpMessage:
        		case qtpSkipSection:
        		case qtpSync:
        		case qtpCallList:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
        		case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
        		case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
        		default:
        			$blnValid = false;
        			break;
        	}
        	
        	if ($blnValid) {
        		$arreFormsAllQuestions[$intQuestionID] = $strQuestionText;
        	}
        }
		
		//Genera todos los campos de cada columna a configurar, todos podrán configurarse hacia un campo alfanumérico de eBavel + el campo que
		//corresponde con el tipo específico de cada columna
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$intSurveyFieldID = (int) @$this->ArrayVariableValues[$inteBavelFieldID];
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName;
			//$aField->Title = BITAMQuestionOptionActionMap::GetSurveyColumnText($intSurveyFieldID);
			$aField->Title = $this->ArrayVariablesLabels[$inteBavelFieldID];
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			
			$intQTypeID = (int) @$this->ArrayVariablesTypes[$inteBavelFieldID];
			$arrMyeBavelFormsFields = array();
			//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
			//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
			$blnValid = true;
			switch ($intQTypeID) {
				case qtpEMail:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsEMailQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpOpenNumeric:
				case qtpCalc:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsNumericQuestions;
					break;
					
				case qtpSingle:
				case qtpCallList:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsSimpleChoiceQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpMulti:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsMultipleChoiceQuestions;
					break;
				
				case qtpOpenDate:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsDateQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpOpenString:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
					break;
				
				case qtpOpenAlpha:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
					break;

				//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
        			break;
					
				case qtpPhoto:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsPhotoQuestions;
					break;
				
				case qtpAction:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAlphaQuestions;
					break;
				
				case qtpSignature:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsSignatureQuestions;
					break;
				
				case qtpOpenTime:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsTimeQuestions;
					break;
				
				case qtpAudio:
				case qtpVideo:
				case qtpDocument:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsDocumentQuestions;
					break;
					
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
				case qtpLocation:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsLocationQuestions;
					break;
					
				default:
					//No se supone que debería haber llegado a este punto si fuera un campo no soportado, pero si lo hizo simplemente se ignorará
					$blnValid = false;
					break;
			}
			
			if (!$blnValid) {
				continue;
			}
			
			asort($arrMyeBavelFormsFields);
			$aField->Options = $arrMyeBavelFormsFields;
			$aField->OnChange = "showHideOptions();";
			$aField->CloseCell = false;
			$myFields[$aField->Name] = $aField;
			//$aField->Parent = $aFieldForms;
			//$aFieldForms->Children[] = $aField;
			
			//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
			//@JAPR 2014-10-28: Agregado el mapeo de Scores
			//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
			//Tiene que agregar los campos adicionales para determinar específicamente que dato se va a mapear
			//Estos campos aparecerán a la derecha del selector del dato a mapear
			if ($intQTypeID == qtpOpenNumeric || $intQTypeID == qtpCalc) {
				//Si el campo de eBavel es numérico, entonces sólo podría mapear scores de preguntas simple o múltiple choice
				$arrDataIDOptions = array(sdidScore => translate("Score"));
			}
			else {
				//Si es otro tipo de campo entonces podría mapear el Score (donde aplique) o el Valor de cualquier pregunta
				$arrDataIDOptions = array(sdidValue => translate("Value"), sdidScore => translate("Score"));
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName."DataID";
			$aField->Title = translate("Data");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrDataIDOptions;
			//@JAPR 2014-10-31: Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
			//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
			//su tamaño provocaba que si se considerara menor que 0px debido a eso
			$aField->FixedWidth = "0";
			$aField->CloseCell = false;
			if (!$intSurveyFieldID || !in_array($intSurveyFieldID, $arrOptionsWithDataID)) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
			
			$strSpanDisplay = '';
			if (!$intSurveyFieldID || !in_array($intSurveyFieldID, $arrOptionsWithMemberIdID)) {
				$strSpanDisplay = "display:none;";
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName."MemberID";
			$aField->Title = translate("Attribute");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrSurveyAttributesByCatalog;
			$aField->BeforeMessage = '<span name="'.$strVariableName.'MemberIDSpan" style="font:italic bold 12px arial,sans-serif;color:#007f7c;'.$strSpanDisplay.'">&nbsp;&nbsp;'.translate("Attribute").'&nbsp;</span>';
			//@JAPR 2014-10-31: Con el valor FixedWidth=0 se forzará a respetar el tamaño natural del combo box sin ajustarlo automáticamente a 200px
			//como lo hace el framework cuando es menor que dicho tamaño, esto porque este campo está oculto por default así que la manera de revisar
			//su tamaño provocaba que si se considerara menor que 0px debido a eso
			$aField->FixedWidth = "0";
			$aField->CloseCell = false;
			if (!$intSurveyFieldID || !in_array($intSurveyFieldID, $arrOptionsWithMemberIdID)) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
			
			$strSpanDisplay = '';
			if (!$intSurveyFieldID || !in_array($intSurveyFieldID, $arrOptionsWithFormula)) {
				$strSpanDisplay = "display:none;";
			}
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName."Formula";
			$aField->Title = translate("Formula");
			$aField->Type = "String";
			$aField->Size = 255;
			$aField->BeforeMessage = '<span name="'.$strVariableName.'FormulaSpan" style="font:italic bold 12px arial,sans-serif;color:#007f7c;'.$strSpanDisplay.'">&nbsp;&nbsp;'.translate("Expression").'&nbsp;</span>';
			$aField->FixedWidth = "100px";
			$aField->CloseCell = true;
			if (!$intSurveyFieldID || !in_array($intSurveyFieldID, $arrOptionsWithFormula)) {
				$aField->IsVisible = false;
			}
			$myFields[$aField->Name] = $aField;
			//@JAPR
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
 		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/EditorHTML/dialogs.js"></script>
 	<script language="JavaScript">
 		var strPreviousObjectName = '';
 		var arrOptionsWithDataID = new Array();
 		var arrOptionsWithMemberIdID = new Array();
 		var arrOptionsWithFormula = new Array();
<?
		//@JAPR 2014-10-28: Agregado el mapeo de Constantes fijas
		//@JAPR 2014-10-28: Agregado el mapeo de Scores
		//@JAPR 2014-10-28: Agregado el mapeo de atributos de catálogos
		global $arrOptionsWithDataID;
		global $arrOptionsWithMemberIdID;
		global $arrOptionsWithFormula;
		
		//Llena los arrays que serán utilizados por la función showHideOptions para mostrar y ocultar los componentes según se necesiten al seleccionar
		//el elemento que será mapeado a eBavel
		foreach ($arrOptionsWithDataID as $intOptionID) {
?>
		arrOptionsWithDataID.push(<?=$intOptionID?>);
<?
		}
		
		foreach ($arrOptionsWithMemberIdID as $intOptionID) {
?>
		arrOptionsWithMemberIdID.push(<?=$intOptionID?>);
<?
		}
		
		foreach ($arrOptionsWithFormula as $intOptionID) {
?>
		arrOptionsWithFormula.push(<?=$intOptionID?>);
<?
		}
		//@JAPR
?>
 		function returnFieldsData()
 		{
 			var objFieldNames = new Object();
 			var objFieldDataIDs = new Object();
 			var objFieldMemberIDs = new Object();
 			var objFieldFormulas = new Object();
<? 			
			//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
			foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
			{
?>
				objFieldNames['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName?>.value);
				if (<?=$myFormName?>_SaveForm.<?=$strVariableName.'DataID'?>) {
					objFieldDataIDs['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName.'DataID'?>.value);
				}
				if (<?=$myFormName?>_SaveForm.<?=$strVariableName.'MemberID'?>) {
					objFieldMemberIDs['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName.'MemberID'?>.value);
				}
				if (<?=$myFormName?>_SaveForm.<?=$strVariableName.'Formula'?>) {
					objFieldFormulas['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName.'Formula'?>.value);
				}
<?
			}
 ?>			
 			
 			var strAnd = '';
 			var sParamReturn = '';
 			var arrKeys = Object.keys(objFieldNames); 
 			var intLength = arrKeys.length;
			for (var intCont = intLength -1; intCont >= 0; intCont--) {
				var strFieldName = arrKeys[intCont];
				sParamReturn += strAnd + strFieldName + '=' + objFieldNames[strFieldName];
				var intOptionID = parseInt(objFieldNames[strFieldName]);
				if (intOptionID && objFieldDataIDs[strFieldName] || objFieldMemberIDs[strFieldName] || objFieldFormulas[strFieldName]) {
					var strDataID = '';
					var strMemberID = '';
					var strFormula = '';
		 			var blnDisplay = (arrOptionsWithDataID.indexOf(intOptionID) != -1);
		 			if (blnDisplay) {
		 				strDataID = (objFieldDataIDs[strFieldName])?objFieldDataIDs[strFieldName]:'';
		 			}
		 			var blnDisplay = (arrOptionsWithMemberIdID.indexOf(intOptionID) != -1);
		 			if (blnDisplay) {
		 				strMemberID = (objFieldMemberIDs[strFieldName])?objFieldMemberIDs[strFieldName]:'';
		 			}
		 			var blnDisplay = (arrOptionsWithFormula.indexOf(intOptionID) != -1);
		 			if (blnDisplay) {
		 				strFormula = (objFieldFormulas[strFieldName])?objFieldFormulas[strFieldName]:'';
		 			}
					sParamReturn += "=" + strDataID;
					sParamReturn += "=" + strMemberID;
					sParamReturn += "=" + strFormula;
				}
				strAnd = '|';
			}
 			
			try {
				if (window.setDialogReturnValue)
				{
					setDialogReturnValue(sParamReturn);
				}
			}
			catch (e) {
			}
			try {
				owner.returnValue = sParamReturn;
			}
			catch (e)
			{
			}
			
			//window.close();
			cerrar();
 		}
 		
 		function showHideObjectOptions(sObjName, bDisplay) {
 			if (!sObjName) {
 				return;
 			}
 			
 			var objSrcObj = document.getElementsByName(sObjName);
 			if (!objSrcObj.length || !objSrcObj[0]) {
 				return;
 			}
 			
 			objSrcObj = objSrcObj[0];
 			var intOptionID = parseInt(objSrcObj.value);
 			var blnDisplay = (arrOptionsWithDataID.indexOf(intOptionID) != -1);
 			var strObjectName = objSrcObj.name + 'DataID';
 			var objObject = document.getElementsByName(strObjectName);
 			if (objObject.length) {
 				objObject[0].style.display = (blnDisplay && bDisplay)?"inline":'none';
 			}
 			
 			var blnDisplay = (arrOptionsWithMemberIdID.indexOf(intOptionID) != -1);
 			var strObjectName = objSrcObj.name + 'MemberID';
 			var objObject = document.getElementsByName(strObjectName);
 			if (objObject.length) {
 				objObject[0].style.display = (blnDisplay && bDisplay)?"inline":'none';
 			}
			
 			var strObjectName = objSrcObj.name + 'MemberIDSpan';
 			var objObject = document.getElementsByName(strObjectName);
 			if (objObject.length) {
 				objObject[0].style.display = (blnDisplay && bDisplay)?"inline":'none';
 			}
 			
 			var blnDisplay = (arrOptionsWithFormula.indexOf(intOptionID) != -1);
 			var strObjectName = objSrcObj.name + 'Formula';
 			var objObject = document.getElementsByName(strObjectName);
 			if (objObject.length) {
 				objObject[0].style.display = (blnDisplay && bDisplay)?"inline":'none';
 			}
 			
 			var strObjectName = objSrcObj.name + 'FormulaSpan';
 			var objObject = document.getElementsByName(strObjectName);
 			if (objObject.length) {
 				//objObject[0].style.display = (blnDisplay && bDisplay)?((bIsIE)?"inline":"table-row"):'none';
 				objObject[0].style.display = (blnDisplay && bDisplay)?"inline":'none';
 			}
 		}
 		
 		function showHideOptions() {
 			if (!this || !this.event || !this.event.srcElement || !this.event.srcElement.name) {
 				return;
 			}
 			
 			var objSrcObj = this.event.srcElement;
 			strPreviousObjectName = objSrcObj.name;
 			showHideObjectOptions(objSrcObj.name, true);
 		}
	</script>
<?
 	}
 	
	function generateAfterFormCode($aUser)
	{
		$myFormName = get_class($this);
		
?>
	<script language="JavaScript">
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		
		if (objOkSelfButton != null) {
			objOkSelfButton.onclick = new Function("returnFieldsData();");
		}
		if (objOkParentButton != null) {
			objOkParentButton.style.display = 'none';
		}
		
		var arraySelects = document.getElementsByTagName('select');
		var intLength = arraySelects.length;
		var arrSelectsProcessed = new Array();
		for (var intIndex = 0; intIndex < intLength; intIndex++) {
			var objSelect = arraySelects[intIndex];
			if (objSelect && objSelect.name.indexOf('id_fieldform') == 0) {
				var strFieldName = objSelect.name;
				strFieldName = strFieldName.replace(/dataid|memberid|formula/gi, '');
				if (arrSelectsProcessed.indexOf(strFieldName) == -1) {
					arrSelectsProcessed.push(strFieldName);
					var objFieldColl = document.getElementsByName(strFieldName + "MemberID");
					if (objFieldColl.length) {
						objFieldColl[0].style.fontFamily = 'Verdana';
					}
				}
			}
		}
		
		var streBavelFieldData = '';
		var objField = null;
		if (window.opener && window.opener.BITAMQuestionOption_SaveForm && window.opener.BITAMQuestionOption_SaveForm.eBavelActionFieldsData) {
			objField = window.opener.BITAMQuestionOption_SaveForm.eBavelActionFieldsData.value;
		}
		try {
			if (objField != null) {
				streBavelFieldData = window.opener.document.getElementsByName('eBavelActionFieldsData')[0].value;
			}
			
			if (streBavelFieldData) {
				var arrFieldData = streBavelFieldData.split('|');
				for (var intIndex in arrFieldData) {
					var strFieldInfo = Trim(arrFieldData[intIndex]);
					if (strFieldInfo) {
						var arrFieldInfo = strFieldInfo.split('=');
						if (arrFieldInfo.length >= 2) {
							var strFieldName = arrFieldInfo[0];
							var intFieldID = parseInt(arrFieldInfo[1]);
							var objFieldColl = document.getElementsByName(strFieldName);
							if (objFieldColl.length) {
								objFieldColl[0].value = intFieldID;
							}
							
							var blnDisplay = false;
							if (arrFieldInfo[2]) {
								var objFieldColl = document.getElementsByName(strFieldName + "DataID");
								if (objFieldColl.length) {
									objFieldColl[0].value = parseInt(arrFieldInfo[2]);
									if (objFieldColl[0].value) {
										blnDisplay = true;
									}
								}
							}
							if (arrFieldInfo[3]) {
								var objFieldColl = document.getElementsByName(strFieldName + "MemberID");
								if (objFieldColl.length) {
									objFieldColl[0].value = parseInt(arrFieldInfo[3]);
									if (objFieldColl[0].value) {
										blnDisplay = true;
									}
								}
							}
							if (arrFieldInfo[4]) {
								var objFieldColl = document.getElementsByName(strFieldName + "Formula");
								if (objFieldColl.length) {
									objFieldColl[0].value = arrFieldInfo[4];
									if (objFieldColl[0].value) {
										blnDisplay = true;
									}
								}
							}
							
							if (blnDisplay) {
								showHideObjectOptions(strFieldName, true);
							}
						}
					}
				}
			}
		}
		catch (e) {
		}
	</script>
<?
	}
}
?>