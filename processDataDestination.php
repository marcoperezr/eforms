<?php
//Archivo que se manda llamar para reprocesar los destinos que fallaron
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("report.inc.php");
require_once("surveytask.inc.php");
require_once('eSurveyServiceMod.inc.php');
$aBITAMConnection = connectToKPIRepository();
$aRepository = $theRepository;
global $TypeID;
$TypeID = rptDataDest;
//$typeID se utiliza en el archivo surveytask.inc.php
global $typeID;
$typeID = 1;
$strDataDestinations = getParamValue('SurveyDestinationLogIDs', 'both', '(string)');
$strSelRows = getParamValue('SelRows', 'both', '(string)');
//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogPath = GeteFormsLogPath();
$strLogFilename = $strLogPath."testDestinos.log";
error_log("\r\n"." processDataDestination.php strDataDestinations=".$strDataDestinations,3,$strLogFilename);
error_log("\r\n"." processDataDestination.php strSelRows=".$strSelRows,3,$strLogFilename);
//@JAPR
//Arreglo que tiene como índice la clave de destino y como contenido la clave de row asociada a ese destino en el grid
$arrayRows = explode(",",$strSelRows);
$rowIDsByDestID = array();
foreach($arrayRows as $aRowID)
{
		$arrayElemOfRow = explode("_",$aRowID);
		//En arrayElemOfRow la posición 1 contiene el destinationlogid
		$idx = $arrayElemOfRow[1];
		$rowIDsByDestID[$idx]=$aRowID;
		//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log("\r\n"."[".$idx."]=".$aRowID,3,$strLogFilename);
		//@JAPR
}
//Valores default para los parámetros al obtener la colección
$searchString="";$startDate="";$endDate="";$GroupID="";$UserID="";$aSurveyID="";$FilterDate="";$ipp=100;
//IDs de destinos seleccionados por el usuario para el procesamiento
$surveyDestinationLogIDs=$strDataDestinations;
$fromProcessDataDest=true;
$dataDestinations = BITAMReportCollection::NewInstanceToFormsLog($aRepository, $searchString, $startDate, $endDate, $GroupID, $UserID, $aSurveyID, $FilterDate, $ipp,$surveyDestinationLogIDs, $fromProcessDataDest);

//$arrCap es un arreglo bidimensional que contiene como primer indice la clave de una captura unica
//compuesta por SurveyID, UserID y SurveyDate, cada elmento contendrá otro arreglo de las claves de destino seleccionadas
//para dicha captura, si la clave de destino (DestinationID) es null entonces significa que es el destino del modelo;
$arrCap = array();
//Contiene los SurveyDestinationLogIDs correspondientes a los destinos guardados en $arrCap = array(), agrupados por captura.
$arrSurveyDestLogID = array();
//Se recorren los objetos de cada destino seleccionado para el procesamiento
foreach ($dataDestinations->Collection as $aDest)
{
	//La clave de captura única se forma por SurveyID, UserID y SurveyDate
	$capID = $aDest->surveytaskid."_".$aDest->surveyid."_".$aDest->userid."_".$aDest->surveydate;
	if(isset($arrCap[$capID]))
	{
		//Si ya se habia agregado anteriormente esta captura entonces sólo se agrega la clave del destino
		$arrCap[$capID][]=$aDest->destinationid;
		$arrSurveyDestLogID[$capID][]=$aDest->surveydestinationlogid;
	}
	else
	{
		//Si no se había agregado anteriormente esta captura entonces se agrega asignandole un arreglo vacío y 
		//posteriormente se agrega la clave del destino
		$arrCap[$capID]=array();
		$arrCap[$capID][]=$aDest->destinationid;
		$arrSurveyDestLogID[$capID][]=$aDest->surveydestinationlogid;
	}
	//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	error_log("\r\n"." capID:".$capID."=>".$aDest->destinationid,3,$strLogFilename);
	//@JAPR
}

//Filas seleccionadas que fueron procesadas es decir que se agregó una tarea
$rowsPro=array();
$rowsPro["rows"]=array();
//Para cada una de las capturas únicas se leen los datos de la tabla SI_SV_SurveyTasksLog los cuales se ocuparán para agregar otra vez la tarea
 foreach($arrCap as $capID => $aCap)
 {
	$expIDs = explode("_",$capID);
	$surveyTaskID = $expIDs[0];
	$surveyID = $expIDs[1];
	$userID = $expIDs[2];
	$surveyDate = $expIDs[3];
	$objSurveyTask = BITAMSurveyTask::NewInstanceWithID($aRepository, $surveyTaskID);
	$objSurveyTask->Reprocess = true;
	$objSurveyTask->DestinationIDs = $arrCap[$capID];
	//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	error_log("\r\nSurveyTaskID: ".$objSurveyTask->SurveyTaskID,3,$strLogFilename);
	error_log("\r\nCreationDateID: ".$objSurveyTask->CreationDateID,3,$strLogFilename);
	error_log("\r\nSurveyID: ".$objSurveyTask->SurveyID,3,$strLogFilename);
	error_log("\r\nSurveyDate: ".$objSurveyTask->SurveyDate,3,$strLogFilename);
	error_log("\r\nUser: ".$objSurveyTask->User,3,$strLogFilename);
	//@JAPR 2018-02-08: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	error_log("\r\nSurveyKey: ".$objSurveyTask->SurveyKey,3,$strLogFilename);
	//@JAPR
	error_log("\r\nForceNew: ".$objSurveyTask->ForceNew,3,$strLogFilename);
	error_log("\r\n***count(DestinationIDs): ".count($objSurveyTask->DestinationIDs),3,$strLogFilename);
	//@JAPR
	//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	foreach($objSurveyTask->DestinationIDs as $aD)
	{
		error_log("\r\n: ".$aD,3,$strLogFilename);
	}
	error_log("\r\n--------------------------",3,$strLogFilename);
	//@JAPR
	$resultReschedule = $objSurveyTask->reschedule($aBITAMConnection);

	if($resultReschedule)
	{
		//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log("\r\nTarea agregada para reprocesar destinos",3,$strLogFilename);		
		//Cambiar el estado de los registros de destinos para marcar que ya se mandaron a reprocesar,
		//para que ya no sea desplegado en la pantalla de bitácoras de destinos
		updateDesinationReprocess($aBITAMConnection, $arrSurveyDestLogID[$capID]);
		
		//Para cada destinationLogID que fue procesado se agrega al arreglo $rowsPro la clave de la fila que tiene en el grid
		foreach($arrSurveyDestLogID[$capID] as $aDL)
		{
			$rowsPro["rows"][] = $rowIDsByDestID[$aDL];
			//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log("\r\n rowsPro[rows][]=".$rowIDsByDestID[$aDL],3,$strLogFilename);
			//@JAPR
		}
	}
	else
	{
		//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log("\r\nLa tarea no pudo ser agregada para reprocesar destinos",3,$strLogFilename);		
		//@JAPR
	}
 }
 echo(json_encode($rowsPro));
//Cambiar el estado de los registros de destinos para marcar que ya se mandaron a reprocesar,
//para que ya no sea desplegado en la pantalla de bitácoras de destinos
 function updateDesinationReprocess($aBITAMConnection, $surveyDestinationLogIDs)
 {
	if(!is_array($surveyDestinationLogIDs) || count($surveyDestinationLogIDs)<=0)
	{
		return;
	}
	$strSurveyDestLogIDs = implode(",",$surveyDestinationLogIDs);
	$sql = "UPDATE SI_SV_SurveyDestinationLog SET ProcessStatus = 1 WHERE SurveyDestinationLogID IN (".$strSurveyDestLogIDs.")";
	$aBITAMConnection->ADOConnection->Execute($sql);
	
	//@JAPR 2019-09-13: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	error_log("\r\nActualizar estado de destino a reprocesado:"."\r\n".$sql,3,$strLogFilename);
	//@JAPR
 }
 ?>