<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$data = "";
$surveyID = -1;
$folioID = -1;
$questionID = -1;


if(array_key_exists("surveyID",$_GET) && array_key_exists("folioID",$_GET) && array_key_exists("questionID",$_GET))
{
	$surveyID = $_GET["surveyID"];
	$folioID = $_GET["folioID"];
	$questionID = $_GET["questionID"];
	$mainFactkey=$_GET["secuencia_reng"];
	//@JAPR 2011-07-11: Unificada la tabla de comentarios en una sóla tabla de encuestas, ahora la llave es FactKey
	$sql = "SELECT StrComment FROM SI_SV_SurveyAnswerComment WHERE FactKey = ".$folioID." AND SurveyID = ".$surveyID." AND QuestionID = ".$questionID." AND MainFactKey=".$mainFactkey;
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	
	if($aRS && !$aRS->EOF)
	{
		$data = $aRS->fields["strcomment"];
	}
}

?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=translate("Comment")?></title>
<link rel="stylesheet" type="text/css" href="css/default2.css">
<script language="JavaScript" src="js/utils.js"></script>
<script language="JavaScript">
function cancel()
{
	top.close();
}
 
function body_keypress(evt)
{
	
	var myEvent = (window.event) ? window.event : evt;

	var code = (myEvent.which) ? myEvent.which : myEvent.keyCode;	
	
	if (code==27)
	{
		cancel();
	}
}
</script>
</head>
<body bgcolor="#F0F8FF">
<table cellspacing="5" cellpadding="1" width="100%" border="0" onkeypress="body_keypress(event)">
  <tr>
    <td style="font-family:verdana; font-size:11px;color: #000066"><?=translate("Comment")?>:</td>
    <td align="right">
    	&nbsp;
    </td>
  </tr>
  <tr>
	  <td colspan="2"><textarea id="txtComment" name="txtComment" cols="53" rows="5" maxlength="200" style="background-color:#FFFFFF" readonly><?=$data?></textarea></td>
  </tr>
</body>
</html>