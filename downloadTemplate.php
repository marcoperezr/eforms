<?php
  require_once('config.php');
  $strtype = array_key_exists("t", $_GET) ? $_GET["t"] : "";
  $strfilename = "users.xls";
  $strpath = dirname(__FILE__)."\\users.xls";
  $errorFilePath = "";
  if ($strtype == "c")
  {
    //Catalog_1165_Empresa.xls
    $strfilename = "Catalog_{$_GET['key']}_{$_GET['name']}.xls";
    session_start();
    global $SAAS;
    global $bSimulatedBitamMode;

    if($SAAS==true && $bSimulatedBitamMode==true)
    {
      $strpath = "../projects/{$_SESSION["PABITAM_RepositoryName"]}/models/".$strfilename;
    }
    else 
    {
    	if(isset($_SESSION['saasuser']))
    	{
      		$strpath = 'C:/inetpub/wwwroot/SAAS_FBM_Test/User'.$_SESSION['saasuser'].'/scripts/' . $strfilename;
    	}
    	else 
    	{
    		$errorFilePath = "You must authenticate to the site of kpionline.bitam.com to access this catalog template";
    	}
    }
  }

if(!is_file($strpath))
{
	echo "Error file path:{$strpath}";
	exit();
}
  
  /*
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="'.$strfilename.'"');
  header('Content-Length: '.filesize($strpath.$errorFilePath));
  header('Cache-Control: max-age=0');
  */
  //fix for IE catching or PHP bug issue
  header("Pragma: public");
  header("Expires: 0"); // set expiration time
  header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
  header("Content-Type: application/force-download");
  header("Content-Type: application/octet-stream");
  header("Content-Type: application/download");
  header("Content-Disposition: attachment; filename=\"".$strfilename."\";");
  header("Content-Transfer-Encoding: binary");
  header("Content-Length: ".filesize($strpath));
  readfile($strpath);
  exit();
?>