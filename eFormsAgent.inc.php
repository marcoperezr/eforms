<?php
class eFormsAgent
{
	public $server = "";
	public $port = 0;
	public $username = "";
	public $password = "";
	public $repository = "";
	public $syncronized = false;
	public $active = true;
	
	static function fromXMLElement($anXMLElement)
	{
		$anAgent = new eFormsAgent();

		$tag = $anXMLElement->getElementsByTagName("SERVER");
		if ($tag->item(0) != null)
			$anAgent->server = $tag->item(0)->nodeValue;

		$tag = $anXMLElement->getElementsByTagName("PORT");
		if ($tag->item(0) != null)
			$anAgent->port = intval($tag->item(0)->nodeValue);

		$tag = $anXMLElement->getElementsByTagName("USERNAME");
		if ($tag->item(0) != null)
			$anAgent->username = ($tag->item(0)->nodeValue);

		$tag = $anXMLElement->getElementsByTagName("PASSWORD");
		if ($tag->item(0) != null)
			$anAgent->password = $tag->item(0)->nodeValue;

		$tag = $anXMLElement->getElementsByTagName("ACTIVE");
		if ($tag->item(0) != null)
			$anAgent->active = $tag->item(0)->nodeValue == '1';

		$tag = $anXMLElement->getElementsByTagName("REPOSITORY");
		if ($tag->item(0) != null)
			$anAgent->repository = $tag->item(0)->nodeValue;

		$tag = $anXMLElement->getElementsByTagName("SYNCRONIZED");
		if ($tag->item(0) != null)
			$anAgent->syncronized = $tag->item(0)->nodeValue == '1';
			
		return($anAgent);
	}
	
	static function fromArray($anArray)
	{
		$anAgent = new eFormsAgent();

		$anAgent->server = $anArray[0];
		$anAgent->port = intval($anArray[1]);
		$anAgent->username = $anArray[2];
		$anAgent->password = $anArray[3];
		$anAgent->active = $anArray[4];
		$anAgent->repository = $anArray[5];
		$anAgent->syncronized = $anArray[6];
			
		return($anAgent);
	}

}
?>
