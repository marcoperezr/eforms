<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

if (array_key_exists("ObjectID", $_GET))
{
	$ObjectID = $_GET["ObjectID"];
}

if (array_key_exists("ProjectID", $_GET))
{
	$ProjectID = $_GET["ProjectID"];
}

if (array_key_exists("FolderID", $_GET))
{
	$FolderID = $_GET["FolderID"];
}

if (array_key_exists("RecordType", $_GET))
{
	$RecordType = $_GET["RecordType"];
}

$existElement = false;

switch ($RecordType)
{
	case 1:
		require_once("projectdocument.inc.php");
		$existElement = BITAMProjectDocument::existProjectFolder($theRepository, $ProjectID, $FolderID, $ObjectID);
		break;
}

if($existElement==true)
{
	$strBoolean = "true";
}
else 
{
	$strBoolean = "false";
}

header('Content-Type: text/xml; charset=utf-8');	
echo $strBoolean;
?>