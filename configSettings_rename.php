<?php
//Sólo lo debe ver la carpeta de testing
/* Este archivo contiene exclusivamente los ajustes directos de configuraciones que se hacen por servicio, así que >>>> NO se debe integrar <<<< al control de versiones 
>>>> NI SE DEBE COPIAR entre servicios <<<<
Las configuraciones ajustadas en este archivo son únicas por servicio de eForms, este archivo NO debe contener ninguna definición de constante ni requires de archivos adicionales
*/

$ekt_config = array (
  'upload_dir' => '',
  'upload_maxsize' => 3000000,
    'upload_badext' => 
  array (
    0 => 'php',
    1 => 'php3',
    2 => 'php4',
    3 => 'php5',
    4 => 'pl',
    5 => 'cgi',
    6 => 'py',
    7 => 'asp',
    8 => 'cfm',
    9 => 'js',
    10 => 'vbs',
    11 => 'html',
    12 => 'htm',
  )
);

$PHPExePath = "C:\\PHP\\PHP.EXE";

//Ruta donde se encuentra el archivo txt que contiene los repositorios dados de alta
//Este es el único proceso de asignación y carga que NO estará en este archivo, ya que es una ruta relativa y no se cambiará
//$EKTCfgPath='./config/EKT_cfg.ini';
//$BITAMRepositories = array();
//$BITAMRepositories=BITAMEKTRepository::loadBITAMRepositories();

//@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//$ImportXMLLogFile ="./log/importXML_".session_id().".log";
//Ahora estas variables contendrán solo el nombre de archivo pero no el path, ya que este depende del contexto donde se ejecuta la función para obtener el path de los logs
$queriesLogFile ="queries_".session_id().".log";
$SendMailLogFile = "sendMail_".session_id().".log";
//@JAPR

$ADODBLogFile = "";
//$ADODBLogFile = "./log/ADODB_".session_id().".log";

//Variable Global de Valor de No Aplica
$gblEFormsNA = 'NA';
$gblEFormsNAKey = 1;
$strNewAttribSep = '_SVElem_';
$strNewDataSep = '_SVSep_';
$intCreatedBy = CREATED_BY_EFORMS;
//@JAPRWarning: Temporalmente se creará con un tipo de cubo inexistente para verificar el proceso, remover esta línea al terminar las pruebas
//$intCreatedBy = 10;

$ekt_ArrDeniedExts = array('php','php2','php3','php4','php5','phtml','pwml','inc','asp','aspx','ascx','jsp','cfm','cfc','pl','bat','exe','com','dll','vbs','js','reg','cgi');

//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
global $garrActionFields;
$garrActionFields = array();
$garrActionFields[] = sfidStartDate;
$garrActionFields[] = sfidStartHour;
$garrActionFields[] = sfidEndDate;
$garrActionFields[] = sfidEndHour;
$garrActionFields[] = sfidLocation;
$garrActionFields[] = sfidSyncDate;
$garrActionFields[] = sfidSyncHour;
$garrActionFields[] = sfidServerStartDate;
$garrActionFields[] = sfidServerStartHour;
$garrActionFields[] = sfidServerEndDate;
$garrActionFields[] = sfidServerEndHour;
$garrActionFields[] = sfidScheduler;
$garrActionFields[] = sfidEMail;
$garrActionFields[] = sfidDuration;
$garrActionFields[] = sfidAnsweredQuestions;
$garrActionFields[] = sfidEntrySection;
$garrActionFields[] = sfidDynamicPage;
$garrActionFields[] = sfidDynamicOption;
$garrActionFields[] = sfidFactKey;
$garrActionFields[] = sfidEntryID;
$garrActionFields[] = sfidUserID;
$garrActionFields[] = sfidUserName;
$garrActionFields[] = sfidGPSCountry;
$garrActionFields[] = sfidGPSState;
$garrActionFields[] = sfidGPSCity;
$garrActionFields[] = sfidGPSAddress;
$garrActionFields[] = sfidGPSZipCode;
$garrActionFields[] = sfidGPSFullAddress;
$garrActionFields[] = safidDescription;
$garrActionFields[] = safidTitle;
$garrActionFields[] = safidResponsible;
$garrActionFields[] = safidCategory;
$garrActionFields[] = safidDueDate;
$garrActionFields[] = safidStatus;
$garrActionFields[] = safidSource;
$garrActionFields[] = safidSourceID;
$garrActionFields[] = safidSourceRowKey;
$garrActionFields[] = sfidLocationAcc;
//$garrActionFields[] = safidTitle;
//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
$garrActionFields[] = sfidFixedStrExpression;
$garrActionFields[] = sfidFixedNumExpression;
$garrActionFields[] = sfidSurveyScore;
$garrActionFields[] = sfidCatalogAttribute;

//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
global $gblModMngrErrorMessage;
$gblModMngrErrorMessage = '';
//Si se deja activada, algunos procesos que generalmente marcarían un error e interrumpirían la ejecución serán omitidos (en la interfaz del Admin).
//Cambiarla en este punto NO afecta al proceso de grabado de capturas, ya que para tal proceso siempre se asigna en False para no interrumpirlo
//Su uso especialmente es para procesos de migración que no deberían interrumpir la ejecución, pero las ventanas del Admin si deben reportar los
//errores directamente cuando sucedan
global $gblShowErrorSurvey;
$gblShowErrorSurvey = true;
//@JAPR
global $debugValuesProcess;						//Habilita la depuración en pantalla de los proceso relacionados con la captura de Valores de Dimensiones
$debugValuesProcess=false;
global $debugExcelProcess;						//Habilita la depuración en pantalla de los proceso relacionados con MS Excel
$debugExcelProcess=false;
global $aMSEIncludeStatCols;					//Determina si se deberán o no incluir las columnas estadísticas en el Template de MS Excel exportado
$aMSEIncludeStatCols = false;
global $showDebugProcessText;
$showDebugProcessText=false;
global $debugSaveBudget;
$debugSaveBudget=false;
global $keepTempFiles;
$keepTempFiles=false;
global $blnTestingServer;
$blnTestingServer = false;
//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
global $gbEnableIncrementalAggregations;
//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
//Si la forma a grabar fue definida con eForms v6 o posterior, es irrelevante el valor de esta configuración, se desactivará durante el proceso de grabado
$gbEnableIncrementalAggregations = false;
global $gbTestIncrementalAggregationsData;
//Colocando esta variable en true, se forzará a procesar el grabado como si fuera a usar agregaciones incrementales (aún si no existen agregaciones)
//pero no se invocará al método. Esto se debe usar en conjunto con DebugPrint para permitir analizar los datos previos a este proceso de agregaciones
//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
//Si la forma a grabar fue definida con eForms v6 o posterior, es irrelevante el valor de esta configuración, se desactivará durante el proceso de grabado
$gbTestIncrementalAggregationsData = false;
global $arrDefDimValues;
$arrDefDimValues = array();
//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
//Activar esta variable limitará la funcionalidad para ocultar opciones no aplicables a los proyectos como Vigilante o GeoControl, los cuales
//se basan sólo en captura con dispositivos y tienes páginas de inicio o reportes personalizados entre otras cosas
global $gbIsGeoControl;
$gbIsGeoControl = false;
//@JAPR

$generateLog=false;
//@JAPR 2013-07-17: Agregado el archivo log de procesamiento de acciones
//Activar esta variable si se desea grabar el archivo log. El nombre del archivo será el mismo siempre (eBavelActions.log), pero se puede variar modificando el código
//justo después de la conexión para asignar la variable $strDefaulteBavelLogStringFile con el nombre que se desee (anexando el repositorio por ejemplo)
//No dejar activada esta variable permanentemente porque generará un log demasiado grande cada vez que se haga una captura
$blnSaveeBavelLogStringFile = true;
//@JAPR 2014-08-25: Agregado el log de requests
$blnSaveRequestsLog = false;
//@JAPR

//Variables para el manejo de las librerias de Artus Web
global $SAAS, $ModelSincronize, $PHPSincronize, $gVersionGlobal, $sModelSQL, $bSimulatedBitamMode;
$SAAS = true;
$bSimulatedBitamMode = true;
     
$ModelSincronize = true;
$PHPSincronize = true;
$gVersionGlobal = '6.00130';
$sModelSQL = '';

$gsURLWEBHTML = '';

$KPAMailTesting = array (
    0 => 'internal_esurvey@bitam.com'
);

global $maxDimensionRecords;					//Cantidad máxima de valores de dimensiones a mostrar en la sección de valores. Si se intenta grabar como 0 (infinito) realmente no lo graba, sólo aplica durante la ejecución en memoria y al reiniciar se tomaría el valor realmente grabado (es un candado para evitar que se cicle)
if(isset($_SESSION["PAMaxDimRecords"]))
{
	$maxDimensionRecords = $_SESSION["PAMaxDimRecords"];
}
else
{
	$maxDimensionRecords = DEFAULT_MAX_DIM_RECORDS;
}

global $dimValuesFilter;
if(isset($_SESSION["DefDimValuesFilter"]))
{
	$dimValuesFilter = $_SESSION["DefDimValuesFilter"];
}
else
{
	$dimValuesFilter = '';
}

global $bShowNullParentValues;
if(isset($_SESSION["ShowNullParentValues"]))
{
	$bShowNullParentValues = $_SESSION["ShowNullParentValues"];
}
else
{
	$bShowNullParentValues = false;
}

global $appSettings;							//Variable de configuración de opciones habilitadas
$appSettings = array();
$appSettings["Cubes"] = 0;
$appSettings["Dimensions"] = 1;
$appSettings["DimensionFields"] = 0;
$appSettings["DimensionAttributes"] = 1;
$appSettings["DimensionValuesCols"] = array("Table" => 0, "Key" => 0, "Desc" => 1);
$appSettings["UseGlobalDimensions"] = 1;

$newExcelMethod = true;
//@JAPR

//Default 	(width: 700px)
//iPod 		(width: 320px)
//iPad		(width: 768px)
//iPadMini	(width: 768px)
//iPhone	(width: 640px) 
//Cel		(width: 640px)
//Tablet	(width: 800px)
$widthByDisp = array();
$widthByDisp["Default"]="1440px";
$widthByDisp["iPod"]="320px";
$widthByDisp["iPad"]="768px";
$widthByDisp["iPadMini"]="768px";
$widthByDisp["iPhone"]="320px";
$widthByDisp["Cel"]="360px";//"640px";
$widthByDisp["Tablet"]="600px";//"800px";

$heightByDisp = array();
$heightByDisp["Default"]="900px";
$heightByDisp["iPod"]="480px";
$heightByDisp["iPad"]="1024px";
$heightByDisp["iPadMini"]="1024px";
$heightByDisp["iPhone"]="568px";
$heightByDisp["Cel"]="640px";//"640px";
$heightByDisp["Tablet"]="1024px";//"800px";

$widthContainerByDisp = array();
$widthContainerByDisp["Default"]="1440";
$widthContainerByDisp["iPod"]="440";
$widthContainerByDisp["iPad"]="1050";
$widthContainerByDisp["iPadMini"]="768";
$widthContainerByDisp["iPhone"]="430";
$widthContainerByDisp["Cel"]="490";//"640px";
$widthContainerByDisp["Tablet"]="803";//"800px";

$heightContainerByDisp = array();
$heightContainerByDisp["Default"]="900";
$heightContainerByDisp["iPod"]="700";
$heightContainerByDisp["iPad"]="1300";
$heightContainerByDisp["iPadMini"]="1024";
$heightContainerByDisp["iPhone"]="805";
$heightContainerByDisp["Cel"]="880";//"640px";
$heightContainerByDisp["Tablet"]="1200";//"800px";

//Marzo 2015
global $gbDesignMode;
$gbDesignMode = false;

//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
global $garrReservedDimNames;
$garrReservedDimNames = array('agenda' => 1, 'date' => 1, 'email' => 1, 'end date' => 1, 'end time' => 1, 'latitude' => 1, 'longitude' => 1, 
	'scheduler' => 1, 'server start date' => 1, 'server start time' => 1, 'server end date' => 1, 'server end time' => 1, 'start date' => 1, 
	'start time' => 1, 'status survey' => 1, 'sync date' => 1, 'sync time' => 1, 'user' => 1);

//@JAPR 2015-10-05: Modificada la redirección en caso de pérdida de sesión para que dependa del servicio donde se encuentra (originalmente usado por GeoControl)
global $loginKpiOnline; 
//@JAPR 2017-06-09: Modificada la URL de redirección cuando se pierde la sesión
$loginKpiOnline = "https://www.bitam.com/";
if ($gbIsGeoControl) {
	//@JAPR 2015-10-07: Temporalmente en lo que obtienen el certificado, esta página entrará por http
	$loginKpiOnline = "http://geocontrol2.bitam.com/";
}
//OMMC 2019-04-12: Cambiada a que sea estática en el control de versión del código, cada servicio deberá tener un valor distinto el cual deberá estar asignado en configSettings.php
global $gbKPIFormsService;
$gbKPIFormsService = "https://kpionline10.bitam.com/artus/gen9_test/ESurveyV602_test/";

?>