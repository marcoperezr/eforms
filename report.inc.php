<?php
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("paginator.inc.php");
require_once("utils.inc.php");

$svStartDate=null;
$svEndDate=null;
$svUserID=null;
$svIpp=null;
$hasPic=array();
$hasDocument=array();
$hasComment=array(); 

//Indica el tipo de log a mostrar:
$TypeID = rptSyncs;

class BITAMReport extends BITAMObject
{
	public $ReportID;				//@JAPRWarning: Para v6 esta propiedad (así como FactKey y FactKeyDimVal) quedó sin asignar porque la consulta no se hace hacia la tabla de datos sino hacia la tabla de tareas/log, y ella no tiene referencia al ID de captura generado
	public $FolioID;
	public $SurveyID;
	public $UserID;
	public $UserName;
	public $DateID;
	public $TimeID;
	public $QuestionFields;
	public $NumQuestionFields;
	public $Questions;
	public $QuestionText;
	public $EmailID;
	public $EmailDesc;
	public $SurveyName;

	public $Survey;
	public $FactKeyDimVal;
	public $HasDynamicSection;
	public $DynamicSectionID;
	public $DynamicSection;
	public $DynQuestionCollection;
	public $Catalog;
	public $ChildDynamicAttribID;
	public $ArrayDynSectionValues;
	//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
	public $HasMasterDetSection;
	public $MasterDetSectionIDs;
	public $MasterDetQuestionCollection;
	public $ArrayMasterDetSectionValues;
	//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	public $exportToPDF;				//Indica si la instancia de cargó para exportación tipo PDF
	public $FactKey;					//Es lo mismo que la propiedad ReportID, agregada por compatibilidad con exportSurvey cuando la instancia de reporte se carga como exportToPDF == true
	public $QComments;					//Array con los comentarios de la encuesta (sólo si exportToPDF == true)
	public $QImages;					//Array con las fotos de la encuesta (sólo si exportToPDF == true)
	public $Signature;					//String con la ruta a la imagen de la firma (sólo si exportToPDF == true)
	public $QuestionIDs;
	public $QuestionIDsFlip;
	public $ArrayFactKeysBySection;		//Array con las posiciones indexadas por FactKey de la tabla paralela para cada sección, con lo cual se permite hacer match con las tablas adicionales
	public $QImagesIdxByMDSection;		//Array con las posiciones indexadas por FactKey de la tabla paralela para poder hacer match con los keys de las imagenes en secciones maestro.detalle
	public $QImagesIdxByDynSection;		//Array con las posiciones indexadas por FactKey de la tabla paralela para poder hacer match con los keys de las imagenes en secciones dinámicas
	//Requeridas exclusivamente para el proceso de generación del PDF (sólo si exportToPDF == true)
	public $doc;
	public $fileName = "";
	public $filePath = "";
	public $orientation;
	public $unidad;
	public $format;
	public $displayMode;
	public $aListFontsAdded = array();
	//@JAPR

	public $GroupID;
	public $GroupName;
	public $Map;
	public $ViewPdf;
	public $Duration;
	public $FormsNum;
	public $Fails;
	//@JAPR 2015-08-12: Agregada la bitácora de destinos extendida
	public $SurveyTaskLogID;
	public $DestiantionLog;
	//@JAPR

	//GCRUZ 2016-05-19. Agregados tiempo y distancia de traslado
	public $bHasTransferData;
	public $TransferTime;
	public $TransferDistance;

	function __construct($aRepository, $aSurveyID)
	{
		BITAMObject::__construct($aRepository);
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		$this->exportToPDF = false;
		$this->FactKey = -1;
		//@JAPR
		$this->ReportID = -1;
		$this->FolioID = -1;
		$this->SurveyID = $aSurveyID;
				
		$this->UserID = -1;
		$this->UserName = "";
		$this->DateID = "";
		$this->TimeID = "";
		$this->FactKeyDimVal = -1;
		//@JAPR 2015-08-13: Agregado el método NewInstanceEmpty y validado para no necesitar un SurveyID
		$this->Survey = null;
		if ($aSurveyID > 0) {
			$this->Survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		}
		
		$this->SurveyName = '';
		//@AAL Se genera un error cuando se borra una encuesta del admnistrador y se quiere acceder desde FormsLog
		if ($this->Survey !== NULL) {
			$this->SurveyName =  $this->Survey->SurveyName;
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			$this->Survey->getSpecialFieldsStatus();
			//@JAPR
		}
		else {
			$this->SurveyName = "NA";
		}
		
		$this->QuestionFields = array();
		$this->FieldTypes = array();
		$this->QuestionText = array();
		
		$this->EmailID = 1;
		$this->EmailDesc = "NA";

		$this->HasDynamicSection = false;
		$this->DynamicSectionID = -1;
		$this->DynamicSection = null;
		$this->DynQuestionCollection = null;
		$this->Catalog = null;
		$this->ChildDynamicAttribID = -1;
		$this->ArrayDynSectionValues = array();
		//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
		$this->HasMasterDetSection = false;
		$this->MasterDetSectionIDs = array();
		$this->MasterDetQuestionCollection = array();
		$this->ArrayMasterDetSectionValues = array();
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		$this->QuestionIDs = array();
		$this->QuestionIDsFlip = array();
		$this->QComments = array();
		$this->QImages = array();
		$this->Signature = "";
		$this->ArrayFactKeysBySection = array();
		$this->QImagesIdxByMDSection = array();
		$this->QImagesIdxByDynSection = array();
		//@JAPR

		$this->GroupID = -1;
		$this->GroupName = "";
		$this->Map = "";
		$this->ViewPdf = "";
		$this->Duration = 0;
	    $this->FormsNum = 0;
	    $this->Fails = 0;
		//@JAPR 2015-08-12: Agregada la bitácora de destinos extendida
		$this->SurveyTaskLogID = 0;
		$this->DestiantionLog = array();
		//@JAPR

		//GCRUZ 2016-05-19. Agregados tiempo y distancia de traslado
		$this->bHasTransferData = false;
		$this->TransferTime = 0;
		$this->TransferDistance = 0;
		
		$this->HasPath = false;
		$this->MsgConfirmRemove = "Do you wish to delete this entry?";
		
		//@JAPR 2015-08-13: Agregado el método NewInstanceEmpty y validado para no necesitar un SurveyID
		$numFields = 0;
		if ($aSurveyID > 0) {
			$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
			if ($dynamicSectionID > 0) {
				$this->HasDynamicSection = true;
				$this->DynamicSectionID = $dynamicSectionID;
				$this->DynamicSection = BITAMSection::NewInstanceWithID($aRepository, $this->DynamicSectionID);
				
				//Obtenemos el ChildDynamicAttribID
				$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, $this->DynamicSection->CatMemberID);
				
				//Obtener el siguiente CatMemberID
				$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->DynamicSection->CatalogID." AND MemberOrder > ".$catMemberInstance->MemberOrder." ORDER BY MemberOrder ASC";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				if (!$aRS->EOF) {
					//Asignamos el atributo hijo
					$this->ChildDynamicAttribID = (int)$aRS->fields["parentid"];
					
					//Obtenemos coleccion de las preguntas de la seccion dinamica
					$this->DynQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->DynamicSectionID);
					
					//Obtenemos el catalogo
					$this->Catalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DynamicSection->CatalogID);
				}
				else {
					//Se vuelven a quedar los valores como si no hubiera seccion dinamica
					$this->HasDynamicSection = false;
					$this->DynamicSectionID = -1;
					$this->DynamicSection = null;
				}
			}
		
			//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
			$arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $aSurveyID, true);
			if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
			{
				$this->HasMasterDetSection = true;
				$this->MasterDetSectionIDs = $arrMasterDetSections;
				
				//Obtiene la colección de preguntas para indexarlas por cada Id de sección Maestro - Detalle
				foreach ($this->MasterDetSectionIDs as $aSectionID)
				{
					$aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $aSectionID);
					if (!is_null($aQuestionCollection))
					{
						$this->MasterDetQuestionCollection[$aSectionID] = $aQuestionCollection;
					}
					else 
					{
						$this->MasterDetQuestionCollection[$aSectionID] = BITAMQuestionCollection::NewInstanceEmpty($this->Repository, $this->SurveyID);
					}
				}
			}
			//@JAPR
			
			$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, null, null);
			$this->Questions = $questions;
			$numFields = count($questions->Collection);
			
			for($i=0; $i<$numFields; $i++) {
				$fieldName = $questions->Collection[$i]->SurveyField;
				$fieldType = $questions->Collection[$i]->QTypeID;
				
				if ($fieldType==1) {
					$this->$fieldName = 0;
				}
				else
				{
					$this->$fieldName = "";
				}
				
				$this->QuestionFields[$i] = $fieldName;
				$this->FieldTypes[$i] = $fieldType;
				$this->QuestionText[$i] = $questions->Collection[$i]->QuestionText;
				//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
				//Modificado para que sea un array debido a las secciones Maestro-detalle y Dinámicas
				$this->QuestionIDs[$i] = $questions->Collection[$i]->QuestionID;
				$this->QComments[$i] = array();
				$this->QImages[$i] = array();
				//@JAPR
			}
		}
		//@JAPR
		
		$this->NumQuestionFields = $numFields;
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		$this->QuestionIDsFlip = array_flip($this->QuestionIDs);
		//@JAPR
	}

	static function NewInstance($aRepository, $aSurveyID)
	{
		return new BITAMReport($aRepository, $aSurveyID);
	}

	//@JAPR 2015-08-13: Agregado el método NewInstanceEmpty y validado para no necesitar un SurveyID
	static function NewInstanceEmpty($aRepository, $aSurveyID = 0)
	{
		$anInstance = new BITAMReport($aRepository, $aSurveyID);
		
		return $anInstance;
	}
	
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
	//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
	//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	//Agregado el parámetro $bExportToPDF para indicar que la instancia está cargada para este tipo de exportación, por lo que entre todas cosas
	//carga información adicional como comentarios y fotos
	static function NewInstanceWithID($aRepository, $aReportID, $aSurveyID, $bGetValues = true, $bExportToPDF = false)
	{
		$anInstance = null;
		
		if (((int)$aReportID) < 0 || ((int)$aSurveyID) < 0 )
		{
			return $anInstance;
		}
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Si se usarán registros únicos, lo que se necesita es el FactKeyDimVal y no el FactKey
		$intFactKeyDimVal = 0;
		if ($anInstance->Survey->UseStdSectionSingleRec) {
			$sql = "SELECT FactKeyDimVal FROM ".$aSurvey->SurveyTable." WHERE FactKey = ".$aReportID;
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			if (!$aRS->EOF)
			{
				$intFactKeyDimVal = (int) @$aRS->fields["factkeydimval"];
			}
		}
		//@JAPR
		
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSketch:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
				//@JAPR
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
	
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
									
					$sql.=", ".$catTable.".".$attribField;
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
	
					$arrayTables[$catTable] = $catTable;
	
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=" AND ".$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		//Ligamos las Tablas para lo de Email
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		$sql .= " WHERE ".$aSurvey->SurveyTable;
		if ($anInstance->Survey->UseStdSectionSingleRec && $intFactKeyDimVal > 0) {
			//El registro EntrySectionID == 0 es el que contiene los datos de las preguntas estándar, y para haber podido configurar que usara
			//registro único quiere decir que debe existir esta columna también
			$sql .= ".FactKeyDimVal = ".$intFactKeyDimVal." AND ".$aSurvey->SurveyTable.".EntrySectionID = 0 ";
		}
		else {
			$sql .= ".FactKey = ".$aReportID;
		}
		$sql .= " ".$joinCat." AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey;
		//@JAPR
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
			//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$anInstance = BITAMReport::NewInstanceFromRS($aRepository, $aSurveyID, $aRS, $bGetValues, $bExportToPDF);
			//@JAPR 2013-01-23: Agregada la opción para que las preguntas estándar se graben en un registro único
			//Esto sólo es útil si se intentó editar una captura que no fué hecha con esta funcionalidad, o bien que por alguna razón quedó dañada,
			//ya que en ese caso no tendría EntrySectionID == 0 por lo que no hubiera asignado el FactKeyDimVal dentro de NewInstanceFromRS, pero
			//si viene activada la nueva funcionalidad entonces seguramente ya lo habíamos identificado
			//@JAPRWarning: Se decidió a no activarlo para que genere un error, ya que aunque omitiera el error, la edición no funcionaría porque
			//los datos también dependen de la configuración nueva, así que si no están grabados en ese formato no se podrían leer de todas formas
			//if (!is_null($anInstance) && $anInstance->Survey->UseStdSectionSingleRec && $intFactKeyDimVal > 0 && $anInstance->FactKeyDimVal <= 0) {
			//	$anInstance->FactKeyDimVal = $intFactKeyDimVal;
			//}
			//@JAPR
		}
		
		return $anInstance;
	}
	
	//@JAPR 2015-07-14: Modificado para que la instancia se genere similar al POST de grabado de datos, donde cada propiedad de una pregunta de multi-registro será un array
	//Con los valores almacenados, mientras que cada pregunta de sección estándar tendrá el valor directo, además habrá propiedades value, image, comment o document en cada campo
	//de tal manera que se pueda generar un Grid. Se seguirán referenciando las propiedades con el 
	static function NewInstanceToFormsLogWithIDV6($aRepository, $DateID, $aSurveyID, $aUserID, $bGetValues = true, $bExportToPDF = false) {
		global $strNewAttribSep;
		global $strNewDataSep;
		
		$anInstance = null;
		try {
			$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
			$aSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
			if (is_null($aSurvey)) {
				return null;
			}
			
			//Primero obtiene los datos generales de la captura, estos generan propiedades con nombres fijos
			$strTableName = $aSurvey->SurveyTable;
			//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
			$strKeyField = BITAMSurvey::$KeyField;
			$intReportID = 0;

			//GCRUZ 2016-05-19. Validar si existen las columnas de tiempo y distancia de traslado
			$sql = "SHOW COLUMNS FROM {$strTableName} LIKE 'Transfer%'";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			$strTransferFields = '';
			if ($aRS && $aRS->_numOfRows >= 2)
			{
				$anInstance->bHasTransferData = true;
				$strTransferFields = ', TransferTime, TransferDistance';
			}
			
			//El query se filtra por la combinación Survey, User y SurveyDate, la cual siempre es única
			$sql = "SELECT {$strKeyField}, UserID, UserEMail, DateID, DateKey, StartDate, StartTime, EndDate, EndTime, ServerStartDate, ServerStartTime, ServerEndDate, ServerEndTime, 
				StartDateEdit, EndDateEdit, StartTimeEdit, EndTimeEdit, SyncDate, SyncTime, SyncEndDate, SyncEndTime, eFormsVersionNum, Latitude, Longitude, Accuracy, 
				Country, State, City, ZipCode, Address, SyncLatitude, SyncLongitude, SyncAccuracy{$strTransferFields} 
				FROM {$strTableName} 
				WHERE UserID = {$aUserID} AND DateID = ".$aRepository->DataADOConnection->DBTimeStamp($DateID);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF) {
				$intReportID = (int) @$aRS->fields[strtolower($strKeyField)];
				$anInstance->FactKey = $intReportID;
				$anInstance->ReportID = $anInstance->FactKey;
				$anInstance->UserID = $aUserID;
				$anInstance->UserName = (string) @$aRS->fields[strtolower("UserEMail")];
				$anInstance->EmailDesc = $anInstance->UserName;
				$anInstance->FactKeyDimVal = $anInstance->FactKey;
				$anInstance->DateID = $DateID;
				$anInstance->StartDate = (string) @$aRS->fields[strtolower("StartDate")];
				$anInstance->StartTime = (string) @$aRS->fields[strtolower("StartTime")];
				$anInstance->EndDate = (string) @$aRS->fields[strtolower("EndDate")];
				$anInstance->EndTime = (string) @$aRS->fields[strtolower("EndTime")];
				$anInstance->ServerStartDate = (string) @$aRS->fields[strtolower("ServerStartDate")];
				$anInstance->ServerStartTime = (string) @$aRS->fields[strtolower("ServerStartTime")];
				$anInstance->ServerEndDate = (string) @$aRS->fields[strtolower("ServerEndDate")];
				$anInstance->ServerEndTime = (string) @$aRS->fields[strtolower("ServerEndTime")];
				$anInstance->StartDateEdit = (string) @$aRS->fields[strtolower("StartDateEdit")];
				$anInstance->EndDateEdit = (string) @$aRS->fields[strtolower("EndDateEdit")];
				$anInstance->StartTimeEdit = (string) @$aRS->fields[strtolower("StartTimeEdit")];
				$anInstance->EndTimeEdit = (string) @$aRS->fields[strtolower("EndTimeEdit")];
				$anInstance->SyncDate = (string) @$aRS->fields[strtolower("SyncDate")];
				$anInstance->SyncTime = (string) @$aRS->fields[strtolower("SyncTime")];
				$anInstance->SyncEndDate = (string) @$aRS->fields[strtolower("SyncEndDate")];
				$anInstance->SyncEndTime = (string) @$aRS->fields[strtolower("SyncEndTime")];
				$anInstance->eFormsVersionNum = (string) @$aRS->fields[strtolower("eFormsVersionNum")];
				$anInstance->Latitude = (string) @$aRS->fields[strtolower("Latitude")];
				$anInstance->Longitude = (string) @$aRS->fields[strtolower("Longitude")];
				$anInstance->Accuracy = (string) @$aRS->fields[strtolower("Accuracy")];
				$anInstance->Country = (string) @$aRS->fields[strtolower("Country")];
				$anInstance->State = (string) @$aRS->fields[strtolower("State")];
				$anInstance->City = (string) @$aRS->fields[strtolower("City")];
				$anInstance->ZipCode = (string) @$aRS->fields[strtolower("ZipCode")];
				$anInstance->Address = (string) @$aRS->fields[strtolower("Address")];
				$anInstance->SyncLatitude = (string) @$aRS->fields[strtolower("SyncLatitude")];
				$anInstance->SyncLongitude = (string) @$aRS->fields[strtolower("SyncLongitude")];
				$anInstance->SyncAccuracy = (string) @$aRS->fields[strtolower("SyncAccuracy")];
				//GCRUZ 2016-05-19. Validar si existen las columnas de tiempo y distancia de traslado
				if ($anInstance->bHasTransferData)
				{
					$anInstance->TransferTime = (double) @$aRS->fields[strtolower("TransferTime")];
					$anInstance->TransferDistance = (double) @$aRS->fields[strtolower("TransferDistance")];
				}
				//$aRS->MoveNext();
			}
			
			$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objQuestionColl)) {
				return $anInstance;
			}
			
			//******************************************************************************************************************************************************
			//A continuación obtiene los datos de todas las preguntas, pero primero de las secciones estándar combinadas, ya que esas sólo contiene un único valor por pregunta
			//******************************************************************************************************************************************************
			$strTableName = $aSurvey->SurveyStdTable;
			//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
			$strKeyField = BITAMSurvey::$KeyField;
			$strAdditionalFields = "";
			$strAnd = ", ";
			$arrValidFields = array();
			$arrQuestionsByID = array();
			
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->SectionType != sectNormal) {
					continue;
				}
				
				$intQuestionID = $objQuestion->QuestionID;
				$strPropName = $objQuestion->SurveyField;
				$anInstance->$strPropName = array("value" => '', "image" => '', "document" => '', "comment" => '');
				$arrQuestionsByID[$intQuestionID] = $objQuestion;
				
				//Valida si la pregunta tiene o no un valor que capturar como respuesta
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					//Estas preguntas se generan en una tabla diferente, así que aunque tengan respuesta, no pueden ser parte de este query
					case qtpSingle:
					case qtpMulti:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				$arrValidFields[$intQuestionID]['value'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//Si es una pregunta con respuesta, agrega el campo al query
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldAttr} AS {$strPropName}";
				}
				
				//Valida si la pregunta tiene o no una imagen que grabar como parte de la respuesta
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpSignature:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpOCR:
						$blnValidQuestion = true;
						break;
					
					default:
						$blnValidQuestion = ($objQuestion->HasReqPhoto > 0);
						break;
				}
				
				$arrValidFields[$intQuestionID]['image'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//Si es una pregunta con imagen, agrega el campo al query
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} AS {$strPropName}Img";
				}
				
				//Valida si la pregunta tiene o no un documento que grabar como parte de la respuesta
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
						$blnValidQuestion = true;
						break;
					
					default:
						$blnValidQuestion = ($objQuestion->HasReqDocument > 0);
						break;
				}
				
				$arrValidFields[$intQuestionID]['document'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//Si es una pregunta con documento, agrega el campo al query
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} AS {$strPropName}Doc";
				}
				
				//Valida si es una pregunta con comentario
				$blnValidQuestion = ($objQuestion->HasReqComment > 0);
				$arrValidFields[$intQuestionID]['comment'] = $blnValidQuestion;
				
				if ($blnValidQuestion) {
					//Si es una pregunta con comentario, agrega el campo al query
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} AS {$strPropName}Comm";
				}
			}
			
			//El query se filtra directamente por el ID de reporte consultado
			$sql = "SELECT SectionKey {$strAdditionalFields} 
				FROM {$strTableName} 
				WHERE {$strKeyField} = {$intReportID}";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if (!$aRS->EOF) {
				foreach ($objQuestionColl->Collection as $objQuestion) {
					if ($objQuestion->SectionType != sectNormal) {
						continue;
					}
					
					$intQuestionID = $objQuestion->QuestionID;
					$strPropName = $objQuestion->SurveyField;
					$strFieldName = strtolower($objQuestion->SurveyField);
					if ((int) @$arrValidFields[$intQuestionID]['value']) {
						$strValue = (string) @$aRS->fields[$strFieldName];
						$strValue = str_replace("\r\n", "<br>", $strValue);
						$strValue = str_replace("\r", "<br>", $strValue);
						$strValue = str_replace("\n", "<br>", $strValue);
						//GCRUZ 2015-10-27. Aplicar formato a las preguntas
						if ($objQuestion->FormatMask != '')
							$strValue = formatNumber($strValue, $objQuestion->FormatMask);
						if ($objQuestion->DateFormatMask != '')
							$strValue = formatDateTime($strValue, $objQuestion->DateFormatMask);
						$anInstance->{$strPropName}["value"] = $strValue;
					}
					if ((int) @$arrValidFields[$intQuestionID]['image']) {
						$anInstance->{$strPropName}["image"] = (string) @$aRS->fields[$strFieldName."img"];
					}
					if ((int) @$arrValidFields[$intQuestionID]['document']) {
						$anInstance->{$strPropName}["document"] = (string) @$aRS->fields[$strFieldName."doc"];
					}
					if ((int) @$arrValidFields[$intQuestionID]['comment']) {
						$anInstance->{$strPropName}["comment"] = (string) @$aRS->fields[$strFieldName."comm"];
					}
				}
				//$aRS->MoveNext();
			}
			
			//******************************************************************************************************************************************************
			//A continuación obtiene los datos de todas las preguntas de secciones multi-registro sección por sección
			//******************************************************************************************************************************************************
			$objSectionColl = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objSectionColl)) {
				return $anInstance;
			}
			
			//Datos los keys de la sección multi-registro correspondiente, indica cual es la posición dentro del array de respuestas que le toca
			$arrSectionsByID = array();
			$arrSectionIndexByKeys = array();
			$anInstance->SectRowsCount = array();
			$anInstance->SectRowsKeys = array();
			$arrMultiRowSectionsByID = array();
			foreach ($objSectionColl->Collection as $objSection) {
				$arrSectionsByID[$objSection->SectionID] = $objSection;
				if ($objSection->SectionType != sectMasterDet && $objSection->SectionType != sectInline) {
					continue;
				}
				
				$arrSectionIndexByKeys[$objSection->SectionID] = array();
				$arrMultiRowSectionsByID[$objSection->SectionID] = $objSection;
				$anInstance->SectRowsCount[$objSection->SectionID] = 0;
				$anInstance->SectRowsKeys[$objSection->SectionID] = array();
				$strTableName = $objSection->SectionTable;
				$strKeyField = BITAMSection::$KeyField;
				//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
				$strDescField = '';
				$bInlineFirstQuestion = true;
				$intInlineFirstQuestion = 0;
				if ($objSection->SectionType == sectInline)
				{
					$strDescField = ', '.$objSection->DescField;
					//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas de la tabla
					if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0)
						$strDescField = ', '.$strTableName.'.*';
				}
				//GCRUZ
				$strAdditionalFields = "";
				$strAnd = ", ";
				$arrValidFields = array();
				$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $objSection->SectionID);

				//GCRUZ 2015-11-12. Modificar el orden de las preguntas para poner selector como inicial
				//Modificar el orden de las preguntas para poner a selector como la inicial
				if ($objSection->SelectorQuestionID != 0)
				{
					//Buscar la pregunta selector y sacarla del arreglo
					$questionKey = -1;
					foreach ($objQuestionColl->Collection as $key => $objQuestion)
					{
						if ($objQuestion->QuestionID == $objSection->SelectorQuestionID)
						{
							$questionKey = $key;
						}
					}
					//Validar si en realidad se encontró la pregunta selector
					if ($questionKey != -1)
					{
						//Sacar la pregunta del arreglo
						$question = array_splice($objQuestionColl->Collection, $questionKey, 1);
						$question = $question[0];
						//Meter de nuevo la pregunta al inicio del arreglo
						array_unshift($objQuestionColl->Collection, $question);
					}
				}

				foreach ($objQuestionColl->Collection as $objQuestion) {
					if ($objQuestion->SectionID != $objSection->SectionID) {
						continue;
					}
					
					//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
					//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta para meter el campo SectionDesc
					if ($objSection->SectionType == sectInline && $bInlineFirstQuestion)
					{
						$bInlineFirstQuestion = false;
						$intInlineFirstQuestion = $objQuestion->QuestionID;
					}
					
					$intQuestionID = $objQuestion->QuestionID;
					$strPropName = $objQuestion->SurveyField;
					$anInstance->$strPropName = array("value" => array(), "image" => array(), "document" => array(), "comment" => array());
					$arrQuestionsByID[$intQuestionID] = $objQuestion;
					
					//Valida si la pregunta tiene o no un valor que capturar como respuesta
					$blnValidQuestion = true;
					switch ($objQuestion->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpSignature:
						case qtpSketch:
						//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						case qtpSketchPlus:
						//@JAPR
						case qtpSkipSection:
						case qtpMessage:
						case qtpSync:
						case qtpPassword:
						case qtpAudio:
						case qtpVideo:
						case qtpSection:
						//Estas preguntas se generan en una tabla diferente, así que aunque tengan respuesta, no pueden ser parte de este query
						case qtpSingle:
						case qtpMulti:
						case qtpOCR:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							$blnValidQuestion = false;
							break;
					}
					
					$arrValidFields[$intQuestionID]['value'] = $blnValidQuestion;
					if ($blnValidQuestion) {
						//Si es una pregunta con respuesta, agrega el campo al query
						$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldAttr} AS {$strPropName}";
					}
					
					//Valida si la pregunta tiene o no una imagen que grabar como parte de la respuesta
					$blnValidQuestion = false;
					switch ($objQuestion->QTypeID) {
						case qtpPhoto:
						case qtpSignature:
						case qtpSketch:
						//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						case qtpSketchPlus:
						//@JAPR
						case qtpOCR:
							$blnValidQuestion = true;
							break;
						
						default:
							$blnValidQuestion = ($objQuestion->HasReqPhoto > 0);
							break;
					}
					
					$arrValidFields[$intQuestionID]['image'] = $blnValidQuestion;
					if ($blnValidQuestion) {
						//Si es una pregunta con imagen, agrega el campo al query
						$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} AS {$strPropName}Img";
					}
					
					//Valida si la pregunta tiene o no un documento que grabar como parte de la respuesta
					$blnValidQuestion = false;
					switch ($objQuestion->QTypeID) {
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
							$blnValidQuestion = true;
							break;
						
						default:
							$blnValidQuestion = ($objQuestion->HasReqDocument > 0);
							break;
					}
					
					$arrValidFields[$intQuestionID]['document'] = $blnValidQuestion;
					if ($blnValidQuestion) {
						//Si es una pregunta con documento, agrega el campo al query
						$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} AS {$strPropName}Doc";
					}
					
					//Valida si es una pregunta con comentario
					$blnValidQuestion = ($objQuestion->HasReqComment > 0);
					$arrValidFields[$intQuestionID]['comment'] = $blnValidQuestion;
					
					if ($blnValidQuestion) {
						//Si es una pregunta con comentario, agrega el campo al query
						$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} AS {$strPropName}Comm";
					}
				}
				
				//El query se filtra directamente por el ID de reporte consultado
				//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
				$sql = "SELECT {$strKeyField} {$strDescField} {$strAdditionalFields} 
					FROM {$strTableName} 
					WHERE SurveyKey = {$intReportID}";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS === false) {
					die("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$intRowIndex = 0;
				while (!$aRS->EOF) {
					$intSectionKey = (int) @$aRS->fields[strtolower($strKeyField)];
					$arrSectionIndexByKeys[$objSection->SectionID][$intSectionKey] = $intRowIndex;
					$anInstance->SectRowsKeys[$objSection->SectionID][$intRowIndex] = $intSectionKey;
					foreach ($objQuestionColl->Collection as $objQuestion) {
						if ($objQuestion->SectionID != $objSection->SectionID) {
							continue;
						}
						
						$intQuestionID = $objQuestion->QuestionID;
						$strPropName = $objQuestion->SurveyField;

						//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
						//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta para meter el campo SectionDesc
						if ($objSection->SectionType == sectInline && $intInlineFirstQuestion == $intQuestionID)
						{
							$strFieldName = strtolower($objSection->DescField);
							$strValue = (string) @$aRS->fields[$strFieldName];
							$anInstance->{$strPropName}["sectionDesc"][$intRowIndex] = $strValue;
							//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas de la tabla
							if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0)
							{
								$strAttributeDesc = (string) @$aRS->fields[strtolower('AttributeDesc')];
								$arrAttributeDesc = explode('_SVElem_', $strAttributeDesc);
								$anInstance->{$strPropName}["sectionDescAttributesNames"][$intRowIndex] = array();
								$anInstance->{$strPropName}["sectionDescAttributesValues"][$intRowIndex] = array();
								foreach ($arrAttributeDesc as $anAttribute)
								{
									$arrAttributeDef = explode('_', $anAttribute);
									$strFieldName = strtolower('Attribute_'.$arrAttributeDef[1]);
									$strValue = (string) @$aRS->fields[$strFieldName];
									$anInstance->{$strPropName}["sectionDescAttributesNames"][$intRowIndex][] = $arrAttributeDef[2];
									$anInstance->{$strPropName}["sectionDescAttributesValues"][$intRowIndex][] = $strValue;
									if (strpos($arrAttributeDef[0], '*') !== false)
										break;
								}
							}
						}

						$strFieldName = strtolower($objQuestion->SurveyField);
						if ((int) @$arrValidFields[$intQuestionID]['value']) {
							$strValue = (string) @$aRS->fields[$strFieldName];
							$strValue = str_replace("\r\n", "<br>", $strValue);
							$strValue = str_replace("\r", "<br>", $strValue);
							$strValue = str_replace("\n", "<br>", $strValue);
							//GCRUZ 2015-10-27. Aplicar formato a las preguntas
							if ($objQuestion->FormatMask != '')
								$strValue = formatNumber($strValue, $objQuestion->FormatMask);
							if ($objQuestion->DateFormatMask != '')
								$strValue = formatDateTime($strValue, $objQuestion->DateFormatMask);
							$anInstance->{$strPropName}["value"][$intRowIndex] = $strValue;
						}
						if ((int) @$arrValidFields[$intQuestionID]['image']) {
							$anInstance->{$strPropName}["image"][$intRowIndex] = (string) @$aRS->fields[$strFieldName."img"];
						}
						if ((int) @$arrValidFields[$intQuestionID]['document']) {
							$anInstance->{$strPropName}["document"][$intRowIndex] = (string) @$aRS->fields[$strFieldName."doc"];
						}
						if ((int) @$arrValidFields[$intQuestionID]['comment']) {
							$anInstance->{$strPropName}["comment"][$intRowIndex] = (string) @$aRS->fields[$strFieldName."comm"];
						}
					}
					$anInstance->SectRowsCount[$objSection->SectionID] = ++$intRowIndex;
					$aRS->MoveNext();
				}
			}
			
			//******************************************************************************************************************************************************
			//A continuación obtiene los datos de todas las preguntas simple y múltiple choice de todas las secciones por igual utilizando un union
			//******************************************************************************************************************************************************
			//Dado a que cada pregunta simple o múltiple choice tiene una tabla individual, se tendrá que generar un UNION de todas las tablas en un único query, siempre y cuando
			//las preguntas NO sean de catálogo, ya que si lo son, la estructura no se comparte debido a los diferentes atributos que se pueden emplear por catálogo, así que esas
			//tendrán que ser consultas separadas por cada pregunta
			
			//Primero extrae todas las preguntas que son simple o múltiple choice pero no de catálogo, ya que se pueden unificar sus queries
			$sqlUNION = '';
			$strAndUNION = '';
			$sqlStandard = '';
			$strAndStandard = '';
			$sqlMulti = '';
			$strAndMulti = '';
			foreach ($objSectionColl->Collection as $objSection) {
				if ($objSection->SectionType == sectFormatted) {
					continue;
				}
				
				$blnMulti = isset($arrMultiRowSectionsByID[$objSection->SectionID]);
				$strKeyField = BITAMSection::$KeyField;
				$strAnd = ", ";
				$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $objSection->SectionID);
				foreach ($objQuestionColl->Collection as $objQuestion) {
					if ($objQuestion->SectionID != $objSection->SectionID) {
						continue;
					}
					
					$intQuestionID = $objQuestion->QuestionID;
					$strPropName = $objQuestion->SurveyField;
					//$anInstance->$strPropName = array("value" => array(), "image" => array(), "document" => array(), "comment" => array());
					$strAdditionalFields = "";
					
					//Valida si la pregunta es o no simple o múltiple choice sin catálogo
					$blnValidQuestion = false;
					switch ($objQuestion->QTypeID) {
						case qtpSingle:
						case qtpMulti:
							if ($objQuestion->ValuesSourceType != tofCatalog) {
								$blnValidQuestion = true;
							}
							break;
					}
					
					if (!$blnValidQuestion) {
						continue;
					}
					
					//En este caso como se trata de un UNION, no se puede usar strPropName como Alias, debe ser un texto genérico
					//@JAPR 2016-04-27: Corregido un bug, la generación del query no incluía el Alias para los campos de las tablas detalle de preguntas, así que en ciertos casos
					//según los movimientos de preguntas, podría haber existido una referencia al mismo campo en la tabla de sección y provocar un error de ambigüedad (#)
					$strAdditionalFields .= $strAnd."A.{$objQuestion->DescFieldAttr} AS QOption";
					switch ($objQuestion->QTypeID) {
						case qtpSingle:
							//Las preguntas simple choice no tienen un campo adicional, sin embargo deben agregarlo para que no falle el SELECT con UNIONs combinando múltiple choice
							$strAdditionalFields .= $strAnd."NULL AS QOptionVal";
							break;
						case qtpMulti:
							//Las preguntas múltiple choice tienen un campo adicional con un valor extra que se debe consultar
							//@JAPR 2016-04-27: Corregido un bug, la generación del query no incluía el Alias para los campos de las tablas detalle de preguntas, así que en ciertos casos
							//según los movimientos de preguntas, podría haber existido una referencia al mismo campo en la tabla de sección y provocar un error de ambigüedad (#)
							$strAdditionalFields .= $strAnd."A.{$objQuestion->ValueField} AS QOptionVal";
							break;
					}
					
					$strTableName = $objQuestion->QuestionDetTable;
					//El query se filtra directamente por el ID de reporte consultado mediante un join a la tabla de la sección correspondiente
					$sql = "SELECT {$objSection->SectionID} AS SectionID, {$objQuestion->QuestionID} AS QuestionID, A.{$strKeyField}, ".
							$aRepository->DataADOConnection->Quote($strPropName)." AS QuestionField {$strAdditionalFields} 
						FROM {$strTableName} A 
							INNER JOIN {$objSection->SectionTable} B ON A.".BITAMSection::$KeyField." = B.".BITAMSection::$KeyField." 
						WHERE B.SurveyKey = {$intReportID}";
					$sqlUNION .= $strAndUNION.$sql;
					$strAndUNION = " UNION ";
				}
			}
			
			//Ejecuta el Query combinado de los UNION, deberá recorrer todas las rows identificando a que sección y pregunta pertenecía para actualizar sólo esa propiedad en el
			//índice correcto según lo determina la sección
			//@JAPR 2015-08-03: Corregido un bug, no se había validado para que en caso de no ser necesario no se intentara ejecutar el query
			if (trim($sqlUNION) != '') {
				$aRS = $aRepository->DataADOConnection->Execute($sqlUNION);
				if ($aRS === false) {
					die("(".__METHOD__.") ".translate("Error accessing")." ".translate("Simple choice")."/".translate("Multiple choice")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUNION);
				}
			}
			
			$strKeyField = strtolower($strKeyField);
			while ($aRS && !$aRS->EOF) {
				$intSectionID = (int) @$aRS->fields["sectionid"];
				$intQuestionID = (int) @$aRS->fields["questionid"];
				$intSectionKey = (int) @$aRS->fields[$strKeyField];
				$strPropName = (string) @$aRS->fields["questionfield"];
				$strOption = (string) @$aRS->fields["qoption"];
				$strOptionVal = (string) @$aRS->fields["qoptionval"];
				$objQuestion = @$arrQuestionsByID[$intQuestionID];
				if ($strPropName == '' || is_null($objQuestion)) {
					$aRS->MoveNext();
					continue;
				}
				
				$blnMulti = isset($arrMultiRowSectionsByID[$intSectionID]);
				$intRowIndex = 0;
				$strValue = '';
				$strAnd = '';
				$strSuffix = '';
				if ($blnMulti) {
					$intRowIndex = (int) @$arrSectionIndexByKeys[$objQuestion->SectionID][$intSectionKey];
					$strValue = @(string) $anInstance->{$strPropName}["value"][$intRowIndex];
				}
				else {
					$strValue = @(string) $anInstance->{$strPropName}["value"];
				}
				
				//En caso de que la pregunta sea múltiple choice, debe concatenar la respuesta con la anterior que tuviera ya grabada, si es simple choice sólo hay una respuesta
				//así que usa directamente la última
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Si la pregunta es simple choice, no debe permitir concatenar valores incluso si anteriormente fue múltiple choice y se grabó con múltiples respuestas
						$strValue = '';
						break;
						
					case qtpMulti:
						//Las preguntas múltiple choice adicionalmente pudieran tener un valor extra capturado por cada opción, si es que estaban configuradas de esa manera al capturar
						if ($strOptionVal != '') {
							$strSuffix = " = ".$strOptionVal;
						}
						
						$strAnd = ($strValue)?"<br>":"";
						break;
				}
				
				$strValue .= $strAnd.$strOption.$strSuffix;
				if ($blnMulti) {
					@$anInstance->{$strPropName}["value"][$intRowIndex] = $strValue;
				}
				else {
					@$anInstance->{$strPropName}["value"] = $strValue;
				}
				
				$aRS->MoveNext();
			}
			
			//******************************************************************************************************************************************************
			//A continuación obtiene los datos de todas las preguntas simple y múltiple choice de catálogo de todas las secciones por igual utilizando un query por cada una
			//******************************************************************************************************************************************************
			//Dado a que cada pregunta simple o múltiple choice tiene una tabla individual pero con estructura muy diferente entre cada pregunta, se tendrá que generar un query por cada
			//pregunta para extraer la información de los atributos exclusivos que cada una utilizó
			$strKeyField = BITAMSection::$KeyField;
			$strKeyField = strtolower($strKeyField);
			$fieldStruct = strtolower(BITAMQuestion::$CatStructField);
			$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->ValuesSourceType != tofCatalog) {
					continue;
				}
				
				$intQuestionID = $objQuestion->QuestionID;
				$intSectionID = $objQuestion->SectionID;
				//Valida si la pregunta es o no simple o múltiple choice sin catálogo
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
					case qtpMulti:
						$blnValidQuestion = true;
						break;
				}
				
				if (!$blnValidQuestion) {
					continue;
				}
				
				$blnMulti = isset($arrMultiRowSectionsByID[$objQuestion->SectionID]);
				$objSection = $arrSectionsByID[$objQuestion->SectionID];
				$strPropName = strtolower($objQuestion->SurveyField);
				$fieldAttrib = strtolower(BITAMQuestion::$DescFieldCatMember);
				$fieldVal = "";
				//La consulta se generará con todos los campos, ya que los campos de atributos históricos no se eliminan, además que existe un campo de definición de atributo que nos
				//indica exactamente la estructura que había al grabar cada valor, por tanto no es necesaria una referencia al catálogo actual de la pregunta sino sólo el contenido de
				//la tabla
				switch ($objQuestion->QTypeID) {
					case qtpMulti:
						//Las preguntas múltiple choice tienen un campo adicional con un valor extra que se debe consultar
						$fieldVal = strtolower($objQuestion->ValueField);
						break;
				}
				
				$strTableName = $objQuestion->QuestionDetTable;
				//El query se filtra directamente por el ID de reporte consultado mediante un join a la tabla de la sección correspondiente
				$sql = "SELECT A.* 
					FROM {$strTableName} A 
						INNER JOIN {$objSection->SectionTable} B ON A.".BITAMSection::$KeyField." = B.".BITAMSection::$KeyField." 
					WHERE B.SurveyKey = {$intReportID}";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS === false) {
					die("(".__METHOD__.") ".translate("Error accessing")." ".translate("Simple choice")."/".translate("Multiple choice")." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF) {
					$intSectionKey = (int) @$aRS->fields[$strKeyField];
					$strOptionVal = (string) @$aRS->fields[$fieldVal];
					$intRowIndex = 0;
					$strValue = '';
					$strAnd = '';
					$strSuffix = '';
					
					//En caso de que la pregunta sea múltiple choice, debe concatenar la respuesta con la anterior que tuviera ya grabada, si es simple choice sólo hay una respuesta
					//así que usa directamente la última
					switch ($objQuestion->QTypeID) {
						case qtpMulti:
							//Las preguntas múltiple choice adicionalmente pudieran tener un valor extra capturado por cada opción, si es que estaban configuradas de esa manera al capturar
							if ($strOptionVal != '') {
								//@JAPRWarning: Por ahora no existen preguntas múltiple choice de catálogo
								//RV Mayo2016: Agregar al reporte el valor capturado por cada opción de multiple choice
								$strSuffix = " = ".$strOptionVal;
							}
							break;
					}
					
					//El valor es la combinación de los valores de los atributos separados por un <enter>, así que se procesan de acuerdo al orden del campo de definición almacenado
					$strCatStructValues = (string) @$aRS->fields[$fieldStruct];
					if (trim($strCatStructValues) == '') {
						$aRS->MoveNext();
						continue;
					}
					
					$arrCatStructValues = explode($strNewAttribSep, $strCatStructValues);
					foreach ($arrCatStructValues as $strCatMemberDef) {
						$arrCatMemberParts = explode("_", $strCatMemberDef, 3);
						$strKey = (string) @$arrCatMemberParts[0];
						$strKey = strpos($strKey, "*");
						$strDataSourceMemberID = (int) @$arrCatMemberParts[1];
						$strDataSourceMemberName = (string) @$arrCatMemberParts[2];
						//Si encuentra una definición inválida debe terminar
						if ($strDataSourceMemberID <= 0) {
							break;
						}
						$strAttribValue = (string) @$aRS->fields[$fieldAttrib.$strDataSourceMemberID];
						//RV Mayo2016: Para multiple choice de catálogo no es necesario poner el nombre del campo
						if($objQuestion->QTypeID== qtpMulti)
						{
							$strValue .= $strAnd.$strAttribValue;
						}
						else
						{
							$strValue .= $strAnd.$strDataSourceMemberName." = ".$strAttribValue;
						}
						$strAnd = "<br>";
						
						//Al encontrar el atributo de la pregunta debe terminar, ya que no habrá mas valores a mostrar para ella
						if ($strKey !== false) {
							break;
						}
					}
					
					if ($blnMulti) {
						$intRowIndex = (int) @$arrSectionIndexByKeys[$objQuestion->SectionID][$intSectionKey];
						@$anInstance->{$strPropName}["value"][$intRowIndex] = $strValue;
					}
					else {
						//RV Mayo2016: Corrección para las multiple choice de catálogo sólo se estaba mostrando el último valor
						//al visualizar la captura desde HTML y al exportar PDF
						if($objQuestion->QTypeID== qtpMulti && @$anInstance->{$strPropName}["value"]!="")
						{
							@$anInstance->{$strPropName}["value"].="<BR>".$strValue.$strSuffix;
						}
						else
						{
							@$anInstance->{$strPropName}["value"] = $strValue.$strSuffix;
						}
					}
					
					$aRS->MoveNext();
				}
			}
		} catch (Exception $e) {
		}
		
		return $anInstance;
	}
	
	/*AAL 22/06/2015: Función Modificada, al accesar a las tablas paralelas, el ReortID ya no aplicará. 
	Ahora se empleara el DateID para realizar el filtrado de datos.*/
	//@JAPR 2015-07-14: Agregado el parámetro $aUserID ya que faltaba para el filtro
	static function NewInstanceToFormsLogWithID($aRepository, $DateID, $aSurveyID, $aUserID, $bGetValues = true, $bExportToPDF = false)
	{
		$anInstance = null;
		
		if (((int)$aSurveyID) < 0 ) {
			return $anInstance;
		}
		
		$aSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		//@JAPR 2015-07-14: Modificado para que la instancia se genere similar al POST de grabado de datos, donde cada propiedad de una pregunta de multi-registro será un array
		//Con los valores almacenados, mientras que cada pregunta de sección estándar tendrá el valor directo, además habrá propiedades value, image, comment o document en cada campo
		//de tal manera que se pueda generar un Grid. Se seguirán referenciando las propiedades con el 
		if ($aSurvey->CreationAdminVersion >= esveFormsv6) {
			return BITAMReport::NewInstanceToFormsLogWithIDV6($aRepository, $DateID, $aSurveyID, $aUserID, $bGetValues = true, $bExportToPDF = false);
		}
		//@JAPR
		
		$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSketch:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-12-10: Agregado preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
				//@JAPR
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
	
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
									
					$sql.=", ".$catTable.".".$attribField;
	
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
	
					$arrayTables[$catTable] = $catTable;
	
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=" AND ".$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		//Ligamos las Tablas para lo de Email
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		$sql .= " WHERE ";
		if ($anInstance->Survey->UseStdSectionSingleRec) {
			//El registro EntrySectionID == 0 es el que contiene los datos de las preguntas estándar, y para haber podido configurar que usara
			//registro único quiere decir que debe existir esta columna también
			$sql .= $aSurvey->SurveyTable . ".EntrySectionID = 0 ";
		}
		//@AAL 17/05/2015 Mpdificado el query, ya no se filtrara por factKey o ReportID si no por DateID
		$sql .= " ".$joinCat." AND ".$aSurvey->SurveyTable.".DateID = '{$DateID}' AND B.".$fieldEmailKey." = C.".$fieldEmailKey;
		//@JAPR
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
			//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$anInstance = BITAMReport::NewInstanceFromRS($aRepository, $aSurveyID, $aRS, $bGetValues, $bExportToPDF);
			//@JAPR 2013-01-23: Agregada la opción para que las preguntas estándar se graben en un registro único
			//Esto sólo es útil si se intentó editar una captura que no fué hecha con esta funcionalidad, o bien que por alguna razón quedó dañada,
			//ya que en ese caso no tendría EntrySectionID == 0 por lo que no hubiera asignado el FactKeyDimVal dentro de NewInstanceFromRS, pero
			//si viene activada la nueva funcionalidad entonces seguramente ya lo habíamos identificado
			//@JAPRWarning: Se decidió a no activarlo para que genere un error, ya que aunque omitiera el error, la edición no funcionaría porque
			//los datos también dependen de la configuración nueva, así que si no están grabados en ese formato no se podrían leer de todas formas
			//if (!is_null($anInstance) && $anInstance->Survey->UseStdSectionSingleRec && $intFactKeyDimVal > 0 && $anInstance->FactKeyDimVal <= 0) {
			//	$anInstance->FactKeyDimVal = $intFactKeyDimVal;
			//}
			//@JAPR
		}
		
		return $anInstance;
	}

	static function NewInstanceToLogFromRS($aRepository, $aSurveyID, $aRS, $Groups)
	{
		global $TypeID;		
		
		$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		//GCRUZ 2015-11-02. Escapar comillas.
		$anInstance->SurveyName = str_replace("\"", "\\\"", $anInstance->SurveyName);
		//EVEGA se necesita en la insantacio el idTaks
		if(isset($aRS->fields["surveytaskid"])){
			$anInstance->SurveyTaskID = $aRS->fields["SurveyTaskID"];
		}
		if ($TypeID == rptSyncs){
			//ears 2017-06-01
			//para bitácoras de sincronización surveydate es el alias del campo CreationDateID que es la fecha de sincronizacion y es la que debe mostrarse
			//en la información de bitácoras, sin embargo para los fines de localización en mapa queda registrado con el campo que realmente se llama
			//surveydate es por ello que se recurre a variables auxiliares para definir los datos de fecha para localización en el mapa
			$DateID = substr($aRS->fields["surveydate2"], 0, 10);
			$TimeID = substr($aRS->fields["surveydate2"], 11, strlen($aRS->fields["surveydate2"]));			
			
			$anInstance->DateID = substr($aRS->fields["surveydate"], 0, 10);
			$anInstance->TimeID = substr($aRS->fields["surveydate"], 11, strlen($aRS->fields["surveydate"]));						
		}
		else{
			$anInstance->DateID = substr($aRS->fields["surveydate"], 0, 10);
			$anInstance->TimeID = substr($aRS->fields["surveydate"], 11, strlen($aRS->fields["surveydate"]));			
		}
		$anInstance->UserName = strtolower((string) @$aRS->fields["username"]);
		$anInstance->UserID = (int) @$Groups[$anInstance->UserName][0];
		$anInstance->GroupID = (int) @$Groups[$anInstance->UserName][1];
		$anInstance->GroupName = (string) @$Groups[$anInstance->UserName][2];
		$anInstance->Duration = (int) @$aRS->fields["duration"];
		if($anInstance->Duration == 0) $anInstance->Duration = 1;
		$anInstance->FormsNum = @$aRS->fields["formsnum"];
		$anInstance->Fails = 0;
		//ears 2017-06-01
		//mismo propósito para manejo del dato de fecha en bitácoras de sincronización usado en localización en el mapa 
		if ($TypeID == rptSyncs){
			$Date = $DateID . " " . $TimeID;
		}
		else{
			$Date = $anInstance->DateID . " " . $anInstance->TimeID;	
		}
		
		$anInstance->Map = "googleMap(" . $aSurveyID . ", \'" . $anInstance->UserID . "\', \'" . $Date ."\', " . $anInstance->ReportID . ")";
		$anInstance->Map = htmlspecialchars(str_ireplace("\'", "'", $anInstance->Map));
		$anInstance->Map = htmlspecialchars("googleMap({$aSurveyID}, \"{$anInstance->UserID}\", \"{$Date}\", {$anInstance->ReportID}, 0)");
		/*$anInstance->ViewPdf = "exportPDF(" . $aSurveyID . ", \'" . $Date . "\', " . $anInstance->ReportID . ")";
		$anInstance->ViewPdf = htmlspecialchars(str_ireplace("\'", "'", $anInstance->ViewPdf));
		$anInstance->ViewPdf = htmlspecialchars("exportPDF({$aSurveyID}, \"{$Date}\", {$anInstance->ReportID})");*/

		return $anInstance;
	}

	static function NewInstanceToFormsLogFromRS($aRepository, $aSurveyID, $aRS, $anInstanceCol)
	{
		global $TypeID;
		
		$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		//GCRUZ 2015-11-02. Escapar comillas.
		$anInstance->SurveyName = str_replace("\"", "\\\"", $anInstance->SurveyName);
		
		if ($TypeID == rptSyncs){
			//ears 2017-06-01
			//para bitácoras de sincronización surveydate es el alias del campo CreationDateID que es la fecha de sincronizacion y es la que debe mostrarse
			//en la información de bitácoras, sin embargo para los fines de localización en mapa queda registrado con el campo que realmente se llama
			//surveydate es por ello que se recurre a variables auxiliares para definir los datos de fecha para localización en el mapa			
			$DateID = substr($aRS->fields["surveydate2"], 0, 10);
			$TimeID = substr($aRS->fields["surveydate2"], 11, strlen($aRS->fields["surveydate2"]));			
			
			$anInstance->DateID = substr($aRS->fields["surveydate"], 0, 10);
			$anInstance->TimeID = substr($aRS->fields["surveydate"], 11, strlen($aRS->fields["surveydate"]));
		}
		else{
			$anInstance->DateID = substr($aRS->fields["surveydate"], 0, 10);
			$anInstance->TimeID = substr($aRS->fields["surveydate"], 11, strlen($aRS->fields["surveydate"]));
		}
										
		$anInstance->UserName = strtolower((string) @$aRS->fields["username"]);
		$anInstance->UserID = (int) @$aRS->fields["userid"];
		$anInstance->GroupID = (int) @$anInstanceCol->UserGroup[$anInstance->UserID];
		$anInstance->GroupName = (string) @$anInstanceCol->GroupsNames[$anInstance->GroupID];
		//ears 2017-06-01
		//mismo propósito para manejo del dato de fecha en bitácoras de sincronización usado en localización en el mapa 
		if ($TypeID == rptSyncs){
			$Date = $DateID . " " . $TimeID;
		}
		else{
			$Date = $anInstance->DateID . " " . $anInstance->TimeID;	
		}
				
		$anInstance->Map = "googleMap(" . $aSurveyID . ", \'" . $anInstance->UserID . "\', \'" . $Date ."\', " . $anInstance->ReportID . ")";
		$anInstance->Map = htmlspecialchars(str_ireplace("\'", "'", $anInstance->Map));
		$anInstance->Map = htmlspecialchars("googleMap({$aSurveyID}, \"{$anInstance->UserID}\", \"{$Date}\", {$anInstance->ReportID}, 1)");
		$anInstance->ViewPdf = "exportPDF(" . $aSurveyID . ", \'" . $Date . "\', " . $anInstance->UserID . ")";
		$anInstance->ViewPdf = htmlspecialchars(str_ireplace("\'", "'", $anInstance->ViewPdf));
		$anInstance->ViewPdf = htmlspecialchars("exportPDF({$aSurveyID}, \"{$Date}\", {$anInstance->ReportID}, {$anInstance->UserID})");
		
		return $anInstance;
	}

	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	//El parámetro $bGetValues indicará si se desea cargar la instancia del reporte con todos los datos del mismo, o si sólo se desea la instancia
	//vacia para poder utilizar métodos de administración (como el borrado mediante FactKeys)
	//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	//Agregado el parámetro $bExportToPDF para indicar que la instancia está cargada para este tipo de exportación, por lo que entre todas cosas
	//carga información adicional como comentarios y fotos
	static function NewInstanceFromRS($aRepository, $aSurveyID, $aRS, $bGetValues = true, $bExportToPDF = false)
	{
		$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		$anInstance->FactKey = (int) $aRS->fields["factkey"];
		$anInstance->exportToPDF = $bExportToPDF;
		//@JAPR
		$anInstance->ReportID = $aRS->fields["factkey"];
		$anInstance->FolioID = (int) $aRS->fields["folioid"];
		$anInstance->UserID = (int) $aRS->fields["userid"];
		$anInstance->EmailID = (int) $aRS->fields["emailkey"];
		$anInstance->EmailDesc = $aRS->fields["emaildesc"];
		$anInstance->FactKeyDimVal = (int)$aRS->fields["factkeydimval"];
		
		if($anInstance->UserID!=-1)
		{
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$anInstance->UserName = BITAMeFormsUser::GetUserName($aRepository, $anInstance->UserID);
		}
		else 
		{
			$anInstance->UserName = "NA";
		}
		
		$strHour = $aRS->fields["starttime"];
		$anInstance->DateID = substr($aRS->fields["dateid"], 0, 10)." ".substr($strHour, 0, 8);
		$anInstance->TimeID = $strHour;
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		if (!$bGetValues) {
			return $anInstance;
		}
		//@JAPR
		
		//Obtenemos el arreglo $this->ArrayDynSectionValues si es que existe seccion dinamica
		if($anInstance->HasDynamicSection==true)
		{
			$anInstance->getDynamicSectionValues();
		}
		
		//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection)
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
			$anInstance->getMasterDetSectionValues();
		}
		//@JAPR
		
		for ($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
			//que tengan seccion dinamica
			if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
			{
				$fieldName =$anInstance->QuestionFields[$i];
				$strValue = "";
				//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayDynSectionValues
				//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
				$arrValues = array();		//Indexado por la descripción del elemento, si se repite se sobreescribirá
				//Obtenemos el valor de esta pregunta a partir del arreglo $this->ArrayDynSectionValues
				//Si la pregunta es diferente de Multiple Choice le daremos tratamiendonto diferente
				//porque todavia no se sabe exactamente como se va a comportar
				if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
				{
					//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
					if ($anInstance->Survey->UseStdSectionSingleRec) {
						//En este caso se sabe anticipadamente que el array contiene un campo especial para dinámicas, así que exclusivamente
						//obtenemos los valores del registro único (se generan mas elementos porque en caso de haber multiple-choice, cada elemento
						//representa a una de las opciones, si no las hubiera entonces hay justo la cantidad de elementos == registros de la sección)
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$aDynamicPageDSC = (string) @$element["DynamicPageDSC"];
							if ($aDynamicPageDSC != '') {
								$idxName = $anInstance->QuestionFields[$i];
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								if ($anInstance->Questions[$i]->FormatMask != '' && is_numeric($element[$idxName])) {
									$strFieldValue = formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
								}
								else {
									$strFieldValue = $element[$idxName];
								}
								
								$strValue.=$element["desc"]."=".$strFieldValue;
							}
						}
					}
					else {
						//En este caso el ciclo recorre todos los posibles registros, los cuales vienen mezclados entre maestro-detalle y dinámicas,
						//y no teníamos forma de identificar a cual sección pertenecía el registro
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
							//Conchita: Antes estaba este cast de INT pero eso evitaba que se mostraran los porcentajes, if (!(trim($element[$idxName]) === '' || (((int) $element[$idxName])==0 && is_numeric($element[$idxName]))))
							//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
							//entonces no se muestre, porque probablemente es un registro de otra sección
							if (!(trim($element[$idxName]) === '' || ($anInstance->Questions[$i]->QTypeID == qtpOpenNumeric && trim((string) $element[$idxName])=='0' && is_numeric($element[$idxName]))))
							{
								if ($anInstance->Questions[$i]->FormatMask != '' && is_numeric($element[$idxName])) {
									@$arrValues[$element["parentdesc"]] = $element["parentdesc"]."=".formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
								}
								else {
									@$arrValues[$element["parentdesc"]] = $element["parentdesc"]."=".$element[$idxName];
								}
							}
							//@$arrValues[$element["desc"]] = $element[$idxName];
							//Conchita**** Aplicar el formato a $element[$idxName]
							if ($anInstance->Questions[$i]->FormatMask != '' && is_numeric($element[$idxName])) {
								//$element[$idxName] = formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
								$tt = formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
								$strValue.=$element["desc"]."=".$tt;
							}
							else {
								$strValue.=$element["desc"]."=".$element[$idxName];
							}
							//@JAPR 2012-06-07: Las preguntas que no son multiple choice y que no vienen de catálogo (no están soportadas las single
							//choice de catálogo dentro de las dinámicas, así que entran en esta categoría) realmente repiten su valor en todos los
							//registros de la misma subsección dinámica, por lo que no tiene caso que se repita varias veces en el valor mostrado,
							//sin embargo como puede haber secciones Maestro - Detalle y esas se graban primero, el primer valor siempre vendría
							//repetido "n" veces, así que salimos hasta que encontramos por lo menos 2 valores o que ya en definitiva terminó de
							//recorrer todos los registros
							
							if ($anInstance->Questions[$i]->CatalogID <= 0)
							{
								if (count($arrValues) > 1)
								{
									$strValKey ='';
									foreach ($arrValues as $strValKeyTemp => $aValue)
									{
										$strValKey = $strValKeyTemp;
									}
									
									//unset($arrValues[$strValKey]);
									//NO funcionará, porque si hay varias subsecciones dinámicas, esto sólo traería el valor de la primera, así que
									//mejor se dejará comentado esta validación hasta pensar mejor como resolverlo
									//break;
								}
							}
							
							//@JAPR
						}
					}
					//@JAPR
				}
				else 
				{
					//Si es entrada de tipo Checkbox, el valor seran
					//todo los valores seleccionados concatenados con ;
					if($anInstance->Questions[$i]->MCInputType==0)
					{
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$valueInteger = (int)$element[$idxName];
							
							if($valueInteger!=0)
							{
								if($strValue!="")
								{
									$strValue.=";";
								}
								
								//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
								@$arrValues[$element["desc"]] = $element["desc"];
								$strValue.=$element["desc"];
							}
						}
					}
					else 
					{
						foreach ($anInstance->ArrayDynSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							
							if($strValue!="")
							{
								$strValue.="; ";
							}
							
							//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
							@$arrValues[$element["desc"]] = $element["desc"]."=".$element[$idxName];
							if ($anInstance->Questions[$i]->FormatMask!=''&& is_numeric($element[$idxName])) {
								$strValue.=$element["desc"]."=".formatNumber($element[$idxName],$anInstance->Questions[$i]->FormatMask);
							}
							else {
								$strValue.=$element["desc"]."=".$element[$idxName];
							}
						}
					}
				}
				
				//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones Maestro - Detalle (repiten primero siempre un elemento)
				//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
				if (!$anInstance->Survey->UseStdSectionSingleRec) {
					//En caso de usar un registro para preguntas estándar, ya no aplica la validación pues ya no existirían duplicados
					$strValue = @implode(';',$arrValues);
				}
				//@JAPR
				$anInstance->$fieldName = $strValue;
			}
			else if($anInstance->Questions[$i]->QTypeID==qtpMulti && $anInstance->Questions[$i]->QDisplayMode==dspVertical && $anInstance->Questions[$i]->MCInputType==1 && $anInstance->Questions[$i]->IsMultiDimension==0 && $anInstance->Questions[$i]->UseCategoryDimChoice!=0)
			{
				$fieldName =$anInstance->QuestionFields[$i];
				$fieldType =$anInstance->FieldTypes[$i];

				//@JAPR 2012-11-20: Validado el caso de categoría de dimensión junto a otras secciones de multiples registros
				if ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection) {
					//Si hay secciones maestro-detalle o dinámicas, a la fecha de hoy no se podían combinar con categoría de dimensión, así que
					//esta pregunta internamente se grababa como una multiple-choice IsMultuDimension==0 MCInputType==1 aunque quedara asociada
					//a una categoría de dimensión
					$anInstance->$fieldName = trim(@$aRS->fields[strtolower($fieldName)]);
				}
				else {
					//En el caso de las preguntas con dimension categoria vamos a obtener la informacion desde una funcion q realice 
					//en una cadena con enters las respuestas q fueron capturadas
					$LN = chr(13).chr(10);
					$strCatDimValAnswers = BITAMReportCollection::getStrCatDimValAnswers($aRepository, $anInstance->Survey, $anInstance->Questions[$i], $anInstance->FactKeyDimVal, $LN, false);
	
					$anInstance->$fieldName = $strCatDimValAnswers;
				}
				//@JAPR
				
				if(trim($anInstance->$fieldName)=="")
				{
					$anInstance->$fieldName = "NA";
				}
			}
			//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
			else if (isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
			{
				//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
				//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
				$fieldName =$anInstance->QuestionFields[$i];
				$fieldNameHierarchy = $fieldName."_Hierarchy";
				$fieldNameArray = $fieldName."Array";
				$fieldNameArrayHierarchy = $fieldNameArray."_Hierarchy";
				$strValue = "";
				//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
				$anInstance->$fieldNameArray = array();
				//Si se trata de una pregunta simple choice de catálogo, se debe mostrar la jerarquía completa del catálogo
				if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true) {
					$anInstance->$fieldNameArrayHierarchy = array();
				}
				
				//Obtenemos el valor de esta pregunta a partir del arrreglo $this->ArrayMasterDetSectionValues
				//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
				//porque todavia no se sabe exactamente como se va a comportar
				$intIndex = 1;
				//@JAPR 2013-05-21: Corregido un bug, cuando la sección Maestro-Detalle tiene sólo firmas, no se generaba el array así que al
				//hacer el for each de null provocaba un error (#29355)
				if (!isset($anInstance->ArrayMasterDetSectionValues[$anInstance->Questions[$i]->SectionID])) {
					$anInstance->$fieldName = $strValue;
					continue;
				}
				//@JAPR
				
				foreach ($anInstance->ArrayMasterDetSectionValues[$anInstance->Questions[$i]->SectionID] as $element)
				{
					$idxName = $anInstance->QuestionFields[$i];
					//@JAPR 2012-06-07: Agregada una validación para no mostrar varios registros vacios en los casos donde hay secciones dinámicas
					$strTempVal = $element[$idxName];
					if ($anInstance->HasDynamicSection)
					{
						//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
						//Conchita, antes estaba asi 2012-12-04: if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
						//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
						//entonces no se muestre, porque probablemente es un registro de otra sección
						if (trim($strTempVal) === '' || ($anInstance->Questions[$i]->QTypeID == qtpOpenNumeric && trim($strTempVal) == '0' && is_numeric($strTempVal)))
						{
							//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
							$strTempVal = false;
						}
					}
					
					if ($strTempVal !== false)
					{
						if($strValue!="")
						{
							$strValue.="; ";
						}
						
						if($anInstance->Questions[$i]->FormatMask!='') {
							$strTempVal = formatNumber($strTempVal,$anInstance->Questions[$i]->FormatMask);
						}
						
						$strValue .= "<".$intIndex."=".$strTempVal.">";
						//$strValue .= "<".$element[$idxName].">";
						//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
						//Si se trata de una pregunta simple choice de catálogo, se debe mostrar la jerarquía completa del catálogo
						if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true) {
							if (isset($element[$fieldNameHierarchy])) {
								$anInstance->{$fieldNameArrayHierarchy}[] = $element[$fieldNameHierarchy];
							}
						}
						$anInstance->{$fieldNameArray}[] = $strTempVal;
						//@JAPR
					}
					
					$intIndex++;
					//@JAPR
				}
				
				$anInstance->$fieldName = $strValue;
			}
			//@JAPR
			else 
			{
				if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
				{
					$fieldName =$anInstance->QuestionFields[$i];
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($anInstance->Survey->DissociateCatDimens) {
						$memberParentID = $objQuestion->CatIndDimID;
						$attribField = "DSC_".$memberParentID;
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
							
							//Obtiene la jerarquía del catálogo pero utilizando los datos grabados en la tabla paralela
							$strCatDimValAnswers = trim(@$aRS->fields[strtolower($objQuestion->SurveyCatField)]);
							$arrayCatValues = array();
							if ($strCatDimValAnswers != '') {
								$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
								$arrayCatClaDescrip = array();
								$arrayTemp = array();
								$arrayInfo = array();
								foreach($arrayCatPairValues as $element)
								{
									$arrayInfo = explode('_SVSep_', $element);
									//Obtener el cla_descrip y almacenar (key_1178)
									$arrayTemp = explode('_', $arrayInfo[0]);
									$arrayCatClaDescrip[] = $arrayTemp[1];
									//Obtener el valor del catalogo
									$arrayCatValues[] = $arrayInfo[1];
								}
							}
							
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$fieldNameHierarchy = $fieldName."_Hierarchy";
							$anInstance->$fieldNameHierarchy = implode(', ', $arrayCatValues);
						}
						
						if(trim($anInstance->$fieldName)=="")
						{
							$anInstance->$fieldName = "NA";
						}
					}
					else {
						$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID, $anInstance->Questions[$i]->CatMemberID);
						$attribField = "DSC_".$memberParentID;
						
						if(array_key_exists(strtolower($attribField),$aRS->fields))
						{
							$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
							
							//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
							$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
							if(array_key_exists(strtolower("RIDIM_".$catParentID."KEY"), $aRS->fields))
							{
								$fieldNameHierarchy = $fieldName."_Hierarchy";
								$anInstance->$fieldNameHierarchy = BITAMReport::getCatHierarchyByMemberValue($aRepository, $anInstance->Questions[$i]->CatalogID, $anInstance->Questions[$i]->CatMemberID, $attribField, $aRS->fields[strtolower("RIDIM_".$catParentID."KEY")], $aRS->fields[strtolower($attribField)]);
							}
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
					}
					//@JAPR
				}
				else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
				{
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];

					//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
					//en una cadena con enters las respuestas q fueron capturadas
					$strAnswersSCMatrix = $anInstance->getStrAnswersSCMatrix($anInstance->Questions[$i]);
					
					$anInstance->$fieldName = $strAnswersSCMatrix;
					
					if(trim($anInstance->$fieldName)=="")
					{
						$anInstance->$fieldName = "NA";
					}
				}
				else 
				{
					$fieldName =$anInstance->QuestionFields[$i];
					$fieldType =$anInstance->FieldTypes[$i];
	
					if(array_key_exists(strtolower($fieldName),$aRS->fields))
					{
						if($fieldType==1)
						{
							$anInstance->$fieldName = (float) $aRS->fields[strtolower($fieldName)];
						}
						else
						{
							if($fieldType == qtpMulti)
							{
								$anInstance->$fieldName = trim($aRS->fields[strtolower($fieldName)]);
								
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
							}
							else 
							{
								$anInstance->$fieldName = trim($aRS->fields[strtolower($fieldName)]);
	
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
							}
						}
					}
					else
					{
						if($fieldType==1)
						{
							$anInstance->$fieldName = 0;
							//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
							if ($bExportToPDF) {
								if ($this->Survey->UseNULLForEmptyNumbers) {
									$anInstance->$fieldName = "NA";
								}
							}
							//@JAPR
						}
						else
						{
							if(trim($anInstance->$fieldName)=="")
							{
								$anInstance->$fieldName = "NA";
							}
						}
					}
				}
			}
		}
		
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		if ($bExportToPDF) {
			//Obtenemos los comentarios 
			$anInstance->getAllQuestionComments();
			
			//Obtenemos las imagenes
			$anInstance->getAllQuestionImages();
	        
	        if($anInstance->Survey->HasSignature) {
	            $anInstance->getSignature();
	        }
	        
	        //Ordena los FactKeys obtenidos
	        foreach ($anInstance->ArrayFactKeysBySection as $intSectionID => $arrFactKeys) {
	        	//asort(array_keys($arrFactKeys));
	        	$arrFactKeys = array_keys($arrFactKeys);
	        	sort($arrFactKeys);
	        	$anInstance->ArrayFactKeysBySection[$intSectionID] = $arrFactKeys;
	        }
		}
		//@JAPR
        
		return $anInstance;
	}
	
	static function getCatHierarchyByMemberValue($aRepository, $catalogID, $catMemberID, $attribField, $id, $value)
	{
		$hierarchyValue="";
		
		$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID=".$catalogID." AND MemberOrder<(SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID = ".$catalogID." AND MemberID = ".$catMemberID.") ORDER BY MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$fieldNames = array();
		
		while(!$aRS->EOF)
		{
			$fieldNames[] = "DSC_".$aRS->fields["parentid"];
			
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-09-21: Corrección de RVega al reporte
 		if(count($fieldNames)==0)
		{
			$hierarchyValue = trim($value);
			return $hierarchyValue;	
		}
		//@JAPR		
		
		$sql = "SELECT TableName, ParentID FROM SI_SV_Catalog WHERE CatalogID =".$catalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$tableName = $aRS->fields["tablename"];
			$catParentID = $aRS->fields["parentid"];
		}
		
		$sql = "SELECT ".implode(",", $fieldNames)." FROM ".$tableName." WHERE "."RIDIM_".$catParentID."KEY"." = ".$id;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			foreach ($fieldNames as $fieldName)
			{
				if($hierarchyValue!="")
				{
					$hierarchyValue.=", ";
				}
				$hierarchyValue.= $aRS->fields[strtolower($fieldName)];
			}
		}
		
		if($hierarchyValue!="")
		{
			$hierarchyValue.=", ";			
		}
		
		$hierarchyValue.= $value;
		
		return $hierarchyValue;
	}	
	
	function getStrAnswersSCMatrix($questionInstance)
	{
		$strAnswersSCMatrix = "";
		$LN = chr(13).chr(10);
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT A.DisplayText, b.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$this->ReportID." AND B.FactKeyDimVal = ".$this->FactKeyDimVal." 
				ORDER BY A.SortOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strDisplayText." = ".$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	function getDynamicSectionValues()
	{
		//Obtenemos coleccion de las preguntas de la seccion dinamica
		$dynQuestionCollection = $this->DynQuestionCollection;
		//Obtenemos la instancia de la seccion dinamica
		$dynSectionInstance = $this->DynamicSection;
		//Obtenemos la instancia de Survey		
		$aSurvey = $this->Survey;
		//Obtenemos la instancia del catalogo
		$catalogInstance = $this->Catalog;
		//Obtenemos la instancia del atributo del catálogo que genera las secciones dinámicas
		$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $this->DynamicSection->CatMemberID);
		
		//Obtener la primera pregunta de seleccion simple de catalogo para obtener los valores del hijo del atributo 
		//de la seccion dinamica
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
				AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$dynSectionInstance->CatalogID." 
				ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		elseif ($aRS->EOF) {
			//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
			die("(".__METHOD__.") ".translate("There is an error in the form, a catalog simple choice question is needed to generate the dynamic section"));
		}
		
		//El key subrrogado del catálogo es irrelevante cuando se usa DissociateCatDimens, sin embargo se dejará sólo como referencia para no cambiar
		//la manera en como se generaba el Array y por compatibilidad con encuestas que aun no han sido migradas a esta característica
		$questionID = (int)$aRS->fields["questionid"];
		$dimCatFieldID = "dim_".$questionID;
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		if ($aSurvey->DissociateCatDimens) {
			//En este caso son las dimensiones usadas como atributo en la sección dinámica y la del siguiente atributo (en caso de tener preguntas
			//multiple choice, de lo contrario, se repite la propia dimensión de la dinámica) las que hacen join, ya que la dimensión del catálogo
			//se separó de la tabla paralela aunque internamente sigue una referencia a sus keys subrrogados
			if ($dynSectionInstance->ChildCatMemberID > 0) {
				$childCatMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $dynSectionInstance->ChildCatMemberID);
			}
			else {
				$childCatMemberInstance = $catMemberInstance;
			}
			
			//Atributo de la sección dinámica
			$catParentID = $catMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			$fromCat.=", ".$catTable;
			$joinField = "RIDIM_".$catParentID."KEY";
			$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
			$dimCatParentDsc = $catTable.".DSC_".$catMemberInstance->IndDimID;
			
			//Atributo de las preguntas múltiple choice (si existen, si no entonces es el mismo de arriba)
			$catParentID = $childCatMemberInstance->IndDimID;
			$catTable = "RIDIM_".$catParentID;
			if(!isset($arrayTables[$catTable])) {
				$fromCat.=", ".$catTable;
				$joinField = "RIDIM_".$catParentID."KEY";
				$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
			}
			
			$dimCatFieldDsc = $catTable.".DSC_".$childCatMemberInstance->IndDimID;
		}
		else {
			$dimCatFieldDsc = "B.DSC_".$this->ChildDynamicAttribID;
			$dimCatFieldJoin = "RIDIM_".$catalogInstance->ParentID."KEY";
			$dimCatTable = "RIDIM_".$catalogInstance->ParentID;
			$dimCatParentDsc = "B.DSC_".$catMemberInstance->ClaDescrip;
		}
		
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		$sql = "SELECT A.FactKey, A.".$dimCatFieldID." AS id, ".$dimCatFieldDsc." AS description, ".$dimCatParentDsc." AS parentdesc";
		//@JAPR 2012-11-21: Corregido un bug, las preguntas tipo foto y firma realmente no deben tener campo en la paralela, y si se genera
		//el query sin preguntas no se debe ejecutar para que no falle
		$blnHasFields = false;
		foreach($dynQuestionCollection as $aQuestion)
		{
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpSketch:
				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				case qtpSketchPlus:
				//@JAPR
				case qtpAudio:
				case qtpVideo:
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
				//OMMC 2015-10-12: Agregado para las preguntas OCR
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					break;
				
				default:
					$sql .= ", dim_".$aQuestion->QuestionID;
					$blnHasFields = true;
					break;
			}
		}

		if (!$blnHasFields) {
			return;
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseDynamicPageField) {
			$sql .= ", A.DynamicPageDSC";
		}
		
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if ($aSurvey->DissociateCatDimens) {
			$sql .= ", A.DynamicOptionDSC, A.DynamicValue";
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$aSurvey->FactTable." B ".$fromCat;
			$sql .= " WHERE A.FactKey = B.FactKey ".$joinCat." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		else {
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$dimCatTable." B ";
			$sql .= " WHERE A.".$dimCatFieldID." = B.".$dimCatFieldJoin." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseStdSectionSingleRec) {
			$sql .= " AND A.EntrySectionID = ".$dynSectionInstance->SectionID;
		}
		//@JAPR
		$sql .= " ORDER BY A.FactKey ";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
		if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$count = 0;
		while(!$aRS->EOF)
		{
			$this->ArrayDynSectionValues[$count] = array();
			$this->ArrayDynSectionValues[$count]["id"] = (int)$aRS->fields["id"];
			$this->ArrayDynSectionValues[$count]["desc"] = $aRS->fields["description"];
			$this->ArrayDynSectionValues[$count]["parentdesc"] = $aRS->fields["parentdesc"];
			foreach($dynQuestionCollection as $aQuestion)
			{
				$fieldName = "dim_".$aQuestion->QuestionID;
				
				if($aQuestion->QTypeID!=qtpMulti)
				{
					$this->ArrayDynSectionValues[$count][$fieldName] = @$aRS->fields[$fieldName];
				}
				else 
				{
					//Si es de tipo Multiple Choice Numerica le realizamos un float
					if($aQuestion->MCInputType==1)
					{
						$this->ArrayDynSectionValues[$count][$fieldName] = (float)$aRS->fields[$fieldName];
					}
					else 
					{
						$this->ArrayDynSectionValues[$count][$fieldName] = $aRS->fields[$fieldName];
					}
				}
			}
			
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			//Si se usa el registro único, entonces agrega el nuevo campo para indicar que si este es el registro a usar por cada página pues
			//sería el que contiene a las preguntas no repetibles
			$aDynamicPageDSC = (string) @$aRS->fields["dynamicpagedsc"];
			if ($this->Survey->UseDynamicPageField) {
				$this->ArrayDynSectionValues[$count]["DynamicPageDSC"] = $aDynamicPageDSC;
			}
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				$aDynamicValue = (string) @$aRS->fields["dynamicvalue"];
				$this->ArrayDynSectionValues[$count]["DynamicValue"] = $aDynamicValue;
			}
			
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$factKey = (int) @$aRS->fields["factkey"];
			if ($factKey > 0) {
				if (!isset($this->ArrayFactKeysBySection[$dynSectionInstance->SectionID])) {
					$this->ArrayFactKeysBySection[$dynSectionInstance->SectionID] = array();
				}
				$this->ArrayFactKeysBySection[$dynSectionInstance->SectionID][$factKey] = $count;
			}
			//@JAPR
			
			$count++;
			$aRS->MoveNext();
		}
	}
	
	//@JAPR 2012-04-17: Agregadas las secciones Maestro-Detalle
	//Obtiene los valores de todas las secciones Maestro - Detalle que incluye la encuesta y llena los campos correspondientes como un Array
	//indexado por cada registro de los conjuntos de respuestas
	function getMasterDetSectionValues()
	{
		foreach ($this->MasterDetSectionIDs as $aSectionID)
		{
			//Obtenemos coleccion de las preguntas de la seccion Maestro - Detalle
			$aQuestionCollection = @$this->MasterDetQuestionCollection[$aSectionID];
			if (is_null($aQuestionCollection))
			{
				continue;
			}
			
			//Obtenemos la instancia de Survey		
			$aSurvey = $this->Survey;
			//Obtenemos la instancia del catalogo
			$catalogInstance = $this->Catalog;
			
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$sql = "SELECT A.FactKey";
			$blnFirst = false;
			//@JAPR 2012-11-21: Corregido un bug, las preguntas tipo foto y firma realmente no deben tener campo en la paralela, y si se genera
			//el query sin preguntas no se debe ejecutar para que no falle
			$blnHasFields = false;
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$fromCat = "";
			$joinCat = "";
			$arrayTables = array();
			//@JAPR
			foreach($aQuestionCollection as $aQuestion)
			{
				switch ($aQuestion->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					case qtpDocument:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregado para las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
					
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					case qtpSingle:
						//Si la pregunta es simple choice de catálogo y usa catálogos con dimensiones independientes, entonces se puede pedir
						//directamente el valor a la dimensión en lugar de mostrar el key subrrogado que anteriormente mostraba
						if ($aQuestion->UseCatalog && $aQuestion->CatalogID > 0) {
							if ($aSurvey->DissociateCatDimens && $aQuestion->CatIndDimID > 0) {
								$catParentID = $aQuestion->CatIndDimID;
								$memberParentID = $catParentID;
								$catTable = "RIDIM_".$catParentID;
								$attribField = "DSC_".$memberParentID;
								//Por compatibilidad hacia atrás, en lugar de usar dim_ se usará dsc_ para no afectar al código preexistente
								//$sql.=(($blnFirst)?'':',').$catTable.".".$attribField." AS DSC_".$aQuestion->QuestionID;
								$sql.=(($blnFirst)?'':',').$catTable.".".$attribField." AS dim_".$aQuestion->QuestionID;
								$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
								$sql.=", A.".$aQuestion->SurveyCatField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$catParentID."KEY";
									$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
								}
							}
							//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
							else {
								$sql .= (($blnFirst)?'':',')." dim_".$aQuestion->QuestionID;
								//Ahora agregará las preguntas simple choice de catálogo con la jerarquía completa en secciones Maestro-Detalle
								$catParentID = BITAMReportCollection::getCatParentID($this->Repository, $aQuestion->CatalogID);
								$catTable = "RIDIM_".$catParentID;
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($this->Repository, $aQuestion->CatalogID, $aQuestion->CatMemberID);
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
								$sql.=", ".$catTable.".".$attribField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$catParentID."KEY";
									$joinCat.=" AND A.".$aQuestion->SurveyField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
								}
							}
							$blnFirst = false;
							$blnHasFields = true;
							//@JAPR
							break;
						}
					//@JAPR
					
					default:
						$sql .= (($blnFirst)?'':',')." dim_".$aQuestion->QuestionID;
						$blnFirst = false;
						$blnHasFields = true;
						break;
				}
			}
			
			$this->ArrayMasterDetSectionValues[$aSectionID] = array();
			if (!$blnHasFields) {
				return;
			}
			
			//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$aSurvey->FactTable." B ".$fromCat;
			$sql .= " WHERE A.FactKey = B.FactKey ".$joinCat." AND A.FactKeyDimVal = ".$this->FactKeyDimVal;
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			if ($this->Survey->UseStdSectionSingleRec) {
				$sql .= " AND A.EntrySectionID = ".$aSectionID;
			}
			//@JAPR
			$sql .= " ORDER BY A.FactKey ";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
			if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
	
			$count = 0;
			while(!$aRS->EOF)
			{
				$this->ArrayMasterDetSectionValues[$aSectionID][$count] = array();
				//$this->ArrayMasterDetSectionValues[$aSectionID][$count]["id"] = (int)$aRS->fields["id"];
				//$this->ArrayMasterDetSectionValues[$aSectionID][$count]["desc"] = $aRS->fields["description"];
				
				foreach($aQuestionCollection as $aQuestion)
				{
					$fieldName = "dim_".$aQuestion->QuestionID;
					//$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldName] = "<".$aRS->fields[$fieldName].">";
					$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldName] = @$aRS->fields[$fieldName];
					//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
					//Ahora agregará las preguntas simple choice de catálogo con la jerarquía completa en secciones Maestro-Detalle
					if ($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==true) {
						if ($aSurvey->DissociateCatDimens && $aQuestion->CatIndDimID > 0) {
							$memberParentID = $aQuestion->CatIndDimID;
							//$attribField = "DSC_".$memberParentID;
							if(array_key_exists(strtolower($aQuestion->SurveyCatField),$aRS->fields))
							{
								//$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
								
								//Obtiene la jerarquía del catálogo pero utilizando los datos grabados en la tabla paralela
								$strCatDimValAnswers = trim(@$aRS->fields[strtolower($aQuestion->SurveyCatField)]);
								$arrayCatValues = array();
								if ($strCatDimValAnswers != '') {
									$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
									$arrayCatClaDescrip = array();
									$arrayTemp = array();
									$arrayInfo = array();
									foreach($arrayCatPairValues as $element)
									{
										$arrayInfo = explode('_SVSep_', $element);
										//Obtener el cla_descrip y almacenar (key_1178)
										$arrayTemp = explode('_', $arrayInfo[0]);
										$arrayCatClaDescrip[] = $arrayTemp[1];
										//Obtener el valor del catalogo
										$arrayCatValues[] = $arrayInfo[1];
									}
								}
								
								//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
								$fieldNameHierarchy = $fieldName."_Hierarchy";
								$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldNameHierarchy] = implode(', ', $arrayCatValues);
								
								/*
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
								*/
							}
						}
						else {
							$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($this->Repository, $aQuestion->CatalogID, $aQuestion->CatMemberID);
							$attribField = "DSC_".$memberParentID;
							
							if(array_key_exists(strtolower($attribField),$aRS->fields))
							{
								//$anInstance->$fieldName = trim($aRS->fields[strtolower($attribField)]);
								
								//22-Ago-2012: Agregar un campo que contenga la respuesta pero con la jerarquía concatenada
								$catParentID = BITAMReportCollection::getCatParentID($this->Repository, $aQuestion->CatalogID);
								if(array_key_exists(strtolower("RIDIM_".$catParentID."KEY"), $aRS->fields))
								{
									$fieldNameHierarchy = $fieldName."_Hierarchy";
									$this->ArrayMasterDetSectionValues[$aSectionID][$count][$fieldNameHierarchy] = BITAMReport::getCatHierarchyByMemberValue($this->Repository, $aQuestion->CatalogID, $aQuestion->CatMemberID, $attribField, $aRS->fields[strtolower("RIDIM_".$catParentID."KEY")], $aRS->fields[strtolower($attribField)]);
								}
								/*
								if(trim($anInstance->$fieldName)=="")
								{
									$anInstance->$fieldName = "NA";
								}
								*/
							}
						}
					}
					//@JAPR
				}
				
				//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
				$factKey = (int) @$aRS->fields["factkey"];
				if ($factKey > 0) {
					if (!isset($this->ArrayFactKeysBySection[$aSectionID])) {
						$this->ArrayFactKeysBySection[$aSectionID] = array();
					}
					$this->ArrayFactKeysBySection[$aSectionID][$factKey] = $count;
				}
				//@JAPR
				$count++;
				$aRS->MoveNext();
			}
		}
	}
	
	//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	function getAllQuestionComments()
	{
		$sql = "SELECT QuestionID, StrComment, MainFactKey FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS !== false)
		{
			while(!$aRS->EOF)
			{
				$strComment = trim($aRS->fields["strcomment"]);
				$questionID = (int) $aRS->fields["questionid"];
				$index = $this->QuestionIDsFlip[$questionID];
				
				if(!is_null($strComment) && $strComment!="")
				{
					//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
					//Modificado para que sea un array debido a las secciones Maestro-detalle y Dinámicas
					if (isset($this->Questions->Collection[$index])) {
						$objQuestion = $this->Questions->Collection[$index];
						if (!is_null($objQuestion)) {
							$blnIsMultipleSection = ($objQuestion->SectionID==$this->DynamicSectionID || in_array($objQuestion->SectionID, $this->MasterDetSectionIDs));
							$factKey = (int) @$aRS->fields["mainfactkey"];
							if ($blnIsMultipleSection) {
								$this->QComments[$index][$factKey] = $strComment;
								if ($objQuestion->SectionID==$this->DynamicSectionID || in_array($objQuestion->SectionID, $this->MasterDetSectionIDs)) {
									if (!isset($this->ArrayFactKeysBySection[$objQuestion->SectionID])) {
										$this->ArrayFactKeysBySection[$objQuestion->SectionID] = array();
									}
									if (!isset($this->ArrayFactKeysBySection[$objQuestion->SectionID][$factKey])) {
										$this->ArrayFactKeysBySection[$objQuestion->SectionID][$factKey] = count($this->ArrayFactKeysBySection[$objQuestion->SectionID]);
									}
								}
							}
							else {
								$this->QComments[$index][$this->FactKey] = $strComment;
							}
						}
					}
					//@JAPR
				}

				$aRS->MoveNext();
			}
		}
	}
	
	function getAllQuestionImages()
	{
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$sql = "SELECT QuestionID, PathImage, MainFactKey FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS !== false)
		{
			while(!$aRS->EOF)
			{
				$strImage = $aRS->fields["pathimage"];
				$questionID = (int) $aRS->fields["questionid"];
				$index = $this->QuestionIDsFlip[$questionID];
				if(!is_null($strImage) && $strImage!="")
				{
					//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
					//Modificado para que sea un array debido a las secciones Maestro-detalle y Dinámicas
					if (isset($this->Questions->Collection[$index])) {
						$objQuestion = $this->Questions->Collection[$index];
						if (!is_null($objQuestion)) {
							$blnIsMultipleSection = ($objQuestion->SectionID==$this->DynamicSectionID || in_array($objQuestion->SectionID, $this->MasterDetSectionIDs));
							$factKey = (int) @$aRS->fields["mainfactkey"];
							if ($blnIsMultipleSection) {
								$this->QImages[$index][$factKey] = "surveyimages/".$theRepositoryName."/".$strImage;
								if ($objQuestion->SectionID==$this->DynamicSectionID || in_array($objQuestion->SectionID, $this->MasterDetSectionIDs)) {
									if (!isset($this->ArrayFactKeysBySection[$objQuestion->SectionID])) {
										$this->ArrayFactKeysBySection[$objQuestion->SectionID] = array();
									}
									if (!isset($this->ArrayFactKeysBySection[$objQuestion->SectionID][$factKey])) {
										$this->ArrayFactKeysBySection[$objQuestion->SectionID][$factKey] = count($this->ArrayFactKeysBySection[$objQuestion->SectionID]);
									}
								}
							}
							else {
								$this->QImages[$index][$this->FactKey] = "surveyimages/".$theRepositoryName."/".$strImage;
							}
						}
					}
					//@JAPR
				}

				$aRS->MoveNext();
			}
		}
	}
	
	function getSignature()
	{
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKey;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if($aRS !== false && !$aRS->EOF)
		{
			$strSignature = $aRS->fields["pathimage"];
			
			if($strSignature!="")
			{
				$this->Signature = "surveysignature/".$theRepositoryName."/".$strSignature;
			}
		}
	}
	//@JAPR

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
		global $arrDateValues;
		global $arrDimKeyValues;
		global $arrDimDescValues;
		global $arrIndicValues;
		global $arrAllDimensions;
		global $arrIndicFields;
		global $strIndicFieldsArrayCode;
		global $gbEnableIncrementalAggregations;
		global $gbTestIncrementalAggregationsData;
		global $intComboID;
		
		if (!isset($arrDateValues)) {
			$arrDateValues = array();
		}
		if (!isset($arrDimKeyValues)) {
			$arrDimKeyValues = array();
		}
		if (!isset($arrDimDescValues)) {
			$arrDimDescValues = array();
		}
		if (!isset($arrIndicValues)) {
			$arrIndicValues = array();
		}
		if (!isset($arrAllDimensions)) {
			$arrAllDimensions = array();
		}
		if (!isset($arrIndicFields)) {
			$arrIndicFields = array();
		}
		if (!isset($intComboID)) {
			$intComboID = 0;
		}
		if (!isset($strIndicFieldsArrayCode)) {
			$strIndicFieldsArrayCode = '';
		}
		//@JAPR
		
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = (int) $aHTTPRequest->GET["SurveyID"];
		}
		else
		{
			$aSurveyID = 0;
		}

		global $svStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$svStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $svEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$svEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $svUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$svUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $svIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$svIpp = $aHTTPRequest->GET["ipp"];
		}

		if (array_key_exists("ReportID", $aHTTPRequest->POST))
		{
			//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
			$arrDates = array();
			$aReportID = $aHTTPRequest->POST["ReportID"];
			if (is_array($aReportID))
			{
				//@JAPR 2014-01-24: Corregido un bug, se estaba intentando usar la variable $aSurveyID antes de asignarla durante el proceso de
				//agregaciones incrementales
				if (array_key_exists("SurveyID", $aHTTPRequest->POST))
				{
					$aSurveyID = (int) $aHTTPRequest->POST["SurveyID"];
				}
				//@AAL 27/05/2015 Se extrajo del if aninado para ser utilizado más adelante.
				$surveyInstance = @BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
				
				//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
				//Si se trata de un borrado de reportes, independientemente de cuantos son, verifica si está o no activo el recalculo inccremental
				//de agregaciones para que el método Remove pueda ya usar esa variable sin mas comprobaciones
				if ($gbEnableIncrementalAggregations && count($aReportID) > 0) {
					require_once('settingsvariable.inc.php');
					$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'USEINCREMENTALAGGREGATIONS');
					if (is_null($objSetting) || ((int) trim($objSetting->SettingValue)) == 0)
					{
						$gbEnableIncrementalAggregations = false;
					}
					
					if ($gbEnableIncrementalAggregations) {
						if (!is_null($surveyInstance)) {
							$sql = "SELECT COUNT(*) AS NumAggregations FROM SI_AG_NEW WHERE CLA_CONCEPTO = ".$surveyInstance->ModelID;
						    $aRS = $aRepository->ADOConnection->Execute($sql);
						    if ($aRS && !$aRS->EOF) {
						    	$gbEnableIncrementalAggregations = ((int) @$aRS->fields["numaggregations"] > 0);
						    }
						    else {
						    	$gbEnableIncrementalAggregations = false;
						    }
						}
					}
				}
				
				//Si en este punto aun sigue activo el uso de agregaciones incrementales, quiere decir que todo está perfectamente configurado así que prepara
				//los arrays para almacenar los datos por cada registro grabado/editado en la secuencia en que se llenará
				//@AAL 27/05/2015 Agregado para EformsV6 y siguientes, el AgregationsArrays no aplica.
				if ($gbEnableIncrementalAggregations && $surveyInstance->CreationAdminVersion < esveFormsv6) {
					BITAMSurvey::PrepareIncrementalAggregationsArrays($aRepository, $aSurveyID, true);
				}
				//@JAPR
				
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$svStartDate = $aHTTPRequest->POST["startDate"];
				}
		
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$svEndDate = $aHTTPRequest->POST["endDate"];
				}
		
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$svUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$svIpp = $aHTTPRequest->POST["ipp"];
				}
				
				$aCollection = BITAMReportCollection::NewInstanceForRemove($aRepository, $aSurveyID, $aReportID, $svStartDate, $svEndDate, $svUserID, $svIpp);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$strDateID = trim((string) @$anInstanceToRemove->DateID);
					if ($strDateID != '') {
						$strDateID = (string) @substr($strDateID, 0, 10);
						$arrDates[$strDateID] = $strDateID;
					}
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
				
				if ($gbEnableIncrementalAggregations) {
					if ($gbTestIncrementalAggregationsData) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo ("<br>\r\n<br>\r\n Incremental aggregations data arrays:");
							echo ("<br>\r\n arrDateValues:");
							PrintMultiArray($arrDateValues);
							echo ("<br>\r\n arrDimKeyValues: ".@count($arrDimKeyValues));
							PrintMultiArray($arrDimKeyValues);
							echo ("<br>\r\n arrDimDescValues: ".@count($arrDimDescValues));
							PrintMultiArray($arrDimDescValues);
							echo ("<br>\r\n arrIndicValues: ".@count($arrIndicValues));
							PrintMultiArray($arrIndicValues);
							echo ("<br>\r\n arrAllDimensions: ".@count($arrAllDimensions));
							PrintMultiArray($arrAllDimensions);
							echo ("<br>\r\n arrIndicFields: ".@count($arrIndicFields));
							PrintMultiArray($arrIndicFields);
							echo ("<br>\r\n strIndicFieldsArrayCode: ".$strIndicFieldsArrayCode);
						}
					}
					
					//Inicia el proceso de recalculo incremental de agregaciones el cual realmente realizará una cancelación de los valores que
					//acaban de ser eliminados del cubo
					require_once("../model_manager/cinfocube.inc.php");
					$theCube = @BITAMCInfoCube::NewInstance($aRepository, $surveyInstance->ModelID, false);
					if (!is_null($theCube)) {
						$strErrDesc = '';
						$aResult = @$theCube->IncrementalAgregations($arrDateValues, $arrDimKeyValues, $arrDimDescValues, $arrIndicValues, $arrAllDimensions, $arrIndicFields, $strErrDesc);
						if ($gbTestIncrementalAggregationsData) {
							die ("<br>\r\n<br>\r\n Incremental aggregations result: ".$aResult.', Error: '.$strErrDesc);
						}
					}
				}
			}
			else
			{
				$anInstance = BITAMReport::NewInstanceWithID($aRepository, (int) $aReportID, $aSurveyID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMReportVariable::NewInstance($aRepository, $aSurveyID);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			
			return null;
		}
		$anInstance = null;
		if (array_key_exists("ReportID", $aHTTPRequest->GET))
		{
			$aReportID = $aHTTPRequest->GET["ReportID"];
			
			$callExportPDF = false;
			if(isset($aHTTPRequest->GET["ReportAction"]))
			{
				$actionValue = $aHTTPRequest->GET["ReportAction"];
	
				if($actionValue=="ExportPDF")
				{
					$callExportPDF = true;
				}
			}

			$DateID = "";
			if(array_key_exists("DateID", $aHTTPRequest->GET))
				$DateID = $aHTTPRequest->GET["DateID"];
			
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$anInstance = BITAMReport::NewInstanceWithID($aRepository,(int)$aReportID, $aSurveyID, true, $callExportPDF);
			//@JAPR
			//@AAL 26/06/2015: Agregado el DateID en EformsV6 y siguientes La forma de extraer es diferente, reemplazado ReporId por DateID
			if (is_null($anInstance) && $DateID == "")
			{
				$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
			}
			else 
			{
				//@AAL 26/06/2015: Agregado para Mostrar los datos en una nueva ventana y para Exportar a PDF
				//@JAPR 2015-07-14: Agregado el parámetro $aUserID ya que faltaba para el filtro

				//if ($aSurvey->CreationAdminVersion >= esveFormsv6) {
					$anInstance = BITAMReport::NewInstanceToFormsLogWithIDV6($aRepository, $DateID, $aSurveyID, $svUserID, true, $callExportPDF);
				//}
				//$anInstance = BITAMReport::NewInstanceToFormsLogWithID($aRepository, $DateID,$aSurveyID,$svUserID, true, $callExportPDF);
				if($callExportPDF){
					$anInstance->exportToPDF(true,$aRepository);
				}

			}
		}
		else
		{
			$anInstance = BITAMReport::NewInstance($aRepository, $aSurveyID);
		}

		return $anInstance;
	}
	
	//@JAPR 2013-05-14 Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	//Agregado el parámetro $sendToFile para permitir reutilizar la función desde el proceso de grabado o desde la ventana de reporte según el caso
	function exportToPDF($sendToFile=true,$aRepository)
	{
		set_time_limit(0);
		//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		//Ahora el código de exportación a PDF va a estar en la propia instancia del reporte, ya que los datos se cargaban de la misma manera
		//así que no tenía caso estar manteniendo doble código
		//$this->exportSurveyToPDF($sendToFile);
		/*
		require_once("exportsurvey.inc.php");
		$docPDF = BITAMExportSurvey::NewInstanceWithID($this->Repository, $this->SurveyID, $this->ReportID);
		$docPDF->exportSurveyToPDF(false);
		*/
		//@JAPR
		//@RTORRES 2015-07-29 se cambia la forma de exportacion a una similar a la usada en ebavel.
		require_once ('wkhtmltopdf/exportSurveyToPDF.php');
		//PrintMultiArray($this);
		if($sendToFile)
		{
			exportSurveyToPDFF($this,$sendToFile,$aRepository);
		}else{
			return exportSurveyToPDFF($this,$sendToFile,$aRepository);
		}
		





	}

	//GCRUZ 2015-08-14. Para exportar a CSV, si no se recibe filePath se asume /logs/[date]_ReportSurvey_[surveyID].csv
	function exportToCSV($filePath = '')
	{
		require_once('checkCurrentSession.inc.php');
		require_once('survey.inc.php');
		//echo "<pre>";
		//var_dump($theRepository);
		//var_dump($theUser);
		//var_dump($_REQUEST);exit;

		require_once('csvExport/csvExport.inc.php');

		$survey = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		$sections = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
		$questions = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		if (is_null($questions)) {
			die('could not load questions!');
		}
		$date_begin = date('Y-m-d H:i:s', strtotime($this->DateID));
		$date_end = date('Y-m-d H:i:s', strtotime($this->DateID));
		$userIDs = array($this->UserName);


		$data = EsurveyGetDataToExport($this->Repository, $survey, $sections, $questions, $date_begin, $date_end, $userIDs);
		if ($filePath == '')
		{
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$filePath = GeteFormsLogPath();
			//@JAPR
		}
		//@JAPR 2019-05-27: Corregido un bug, no estaba aplicando el nombre recibido así que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
		//servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
		//Cuando se procesa la exportación por este punto únicamente corresponde a la captura que está siendo grabada por el agente (la instancia de BITAMReport donde estamos), así que
		//se puede utilizar dicha instancia para generar el nombre final del archivo que será enviado a los destinos de datos
		$strDatabaseNumber = str_ireplace("fbm_bmd_", "", $this->Repository->RepositoryName);
		$file = "KPISurveyCSV_{$strDatabaseNumber}_{$this->UserID}_{$this->SurveyID}_".formatDateTime($this->DateID, "yyyymmddhhmmss").".csv";
		//$file = date('YmdHis').'_ReportSurvey_'.$this->SurveyID.'.csv';
		//@JAPR
		EsurveyCSVExport($data, $filePath.$file);
		return $filePath.$file;
	}

	//GCRUZ 2015-08-14. Para exportar a XLS, si no se recibe filePath se asume /logs/[date]_ReportSurvey_[surveyID].xls
	function exportToXLS($filePath = '')
	{
		require_once('checkCurrentSession.inc.php');
		require_once('survey.inc.php');
		require_once('csvExport/csvExport.inc.php');

				$survey = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID);
		$sections = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
		$questions = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		if (is_null($questions)) {
			die('could not load questions!');
		}
		$date_begin = date('Y-m-d H:i:s', strtotime($this->DateID));
		$date_end = date('Y-m-d H:i:s', strtotime($this->DateID));
		$userIDs = array($this->UserName);


		$data = EsurveyGetDataToExport($this->Repository, $survey, $sections, $questions, $date_begin, $date_end, $userIDs);
		if ($filePath == '')
		{
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$filePath = GeteFormsLogPath();
			//@JAPR
		}
		//GCRUZ 2016-01-14. Cambiar formato de XLS por xlsx
		//@JAPR 2019-05-27: Corregido un bug, no estaba aplicando el nombre recibido así que por haber quedado de esta manera era susceptible a que se repitiera entre diferentes
		//servicios de Agente de eForms de repositorios diferentes que procesaban una tarea al mismo tiempo (#JFFOUE)
		//Cuando se procesa la exportación por este punto únicamente corresponde a la captura que está siendo grabada por el agente (la instancia de BITAMReport donde estamos), así que
		//se puede utilizar dicha instancia para generar el nombre final del archivo que será enviado a los destinos de datos
		$strDatabaseNumber = str_ireplace("fbm_bmd_", "", $this->Repository->RepositoryName);
		$file = "KPISurveyXLS_{$strDatabaseNumber}_{$this->UserID}_{$this->SurveyID}_".formatDateTime($this->DateID, "yyyymmddhhmmss").".xlsx";
		//$file = date('YmdHis').'_ReportSurvey_'.$survey->SurveyID.'.xlsx';
		//@JAPR
		EsurveyXLSExport($data, $filePath.$file);
		return $filePath.$file;
	}

	function updateFromArray($anArray)
	{
	}

	function save()
	{
	}

	//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
	//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
	//editando o agregando otros
	//El parámetro $anArrayFactKeys controla que FactKeys se desean eliminar de esta captura, si no se especifica entonces elimina toda la captura
	//completa, aunque en si el registro de la captura depende de si se manda el $bDeleteFactKeyDimVal == true
	function remove($bDeleteFactKeyDimVal = true, $anArrayFactKeys = null)
	{
		global $gbEnableIncrementalAggregations;
		
		//A partir del Encuesta ID se van a eliminar los registros
		$factTable = "RIFACT_".$this->Survey->ModelID;
		$factKeyDimValTable = "RIDIM_".$this->Survey->FactKeyDimID;
		$factKeyDimValField = "RIDIM_".$this->Survey->FactKeyDimID."KEY";
		$blnUseFactKeysFilter = false;
		//@JAPR 2012-11-16: Agregado el borrado selectivo de registros de una captura
		$arrayFactKeys = array();
		if (is_null($anArrayFactKeys)) {
			$sql = "SELECT FactKey FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			//@JAPR 2012-11-20: Un EOF no debería marcar un error sino continuar sin realizar acción alguna
			if (!$aRS)	//|| $aRS->EOF
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF) 
			{
				$arrayFactKeys[] = (int)$aRS->fields["factkey"];
				$aRS->MoveNext();
			}
		}
		else {
			$arrayFactKeys = $anArrayFactKeys;
			$blnUseFactKeysFilter = true;
		}
		
		if (is_null($arrayFactKeys) || !is_array($arrayFactKeys)) {
			$arrayFactKeys = array();
		}
		//@JAPR
		
		//Si no hay elementos FactKey por eliminar entonces no se realiza 
		//el proceso de eliminado
		if(count($arrayFactKeys)<=0)
		{
			return;
		}
		
		//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
		//Para este punto la variable del recalculo incremental ya debe estar correctamente validada, así que si es true entonces si se debe ejecutar
		//el proceso incremental de borrado (cancelación) de los indicadores para esta captura, el cual se define como una función estática para
		//permitir reutilizar esta misma clase durante el borrado de registros en la edición. Se usará el Array local en lugar del recibido, porque
		//dependiendo de que parámetros se recibieron, sólo se necesitan borrar algunos registros o bien la captura completa
		if ($gbEnableIncrementalAggregations && count($arrayFactKeys) > 0) {
			@BITAMReport::IncrementalAggregationsRemove($this->Repository, $this->SurveyID, $this->FactKeyDimVal, $arrayFactKeys);
		}
		//@JAPR
		
		$strFactKeys = implode(",", $arrayFactKeys);
		
		//Eliminamos las firmas
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta
		//La firma sólo se elimina si se está borrando la captura completa
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveysignature";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			while (!$aRS->EOF)
			{
				$pathImage = $aRS->fields["pathimage"];
				@unlink($RepositoryDir."/".$pathImage);
				$aRS->MoveNext();
			}
		
			$sql = "DELETE FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Eliminamos los comentarios
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND MainFactKey IN ('.$strFactKeys.') ';
		}
		$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las acciones por pregunta que ya no se ocupa por el momento
		$sql = "DELETE FROM SI_SV_SurveyQuestionActions WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$strFactKeys." )";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las acciones por opcion de pregunta
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$this->SurveyID." AND SurveyDimVal = ".$this->FactKeyDimVal;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos las fotos
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
		//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
		//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
		$arrayImgDimIDs = array();
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathImage FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveyimages";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			while (!$aRS->EOF)
			{
				$pathImage = $aRS->fields["pathimage"];
				@unlink($RepositoryDir."/".$pathImage);
				$aRS->MoveNext();
			}
		
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$this->FactKeyDimVal." )".$strFactKeysFilter;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "SELECT ImgDimID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND HasReqPhoto > 0 AND ImgDimID IS NOT NULL ORDER BY ImgDimID";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strSelectImgDim = "";
			while(!$aRS->EOF)
			{
				$imgdimid = (int)$aRS->fields["imgdimid"];
				//@JAPR 2012-10-22: Corregido un bug, si no está asignada esta dimensión no se debe intentar usar
				if ($imgdimid > 0) {
					$arrayImgDimIDs[] = $imgdimid;
					
					if($strSelectImgDim!="")
					{
						$strSelectImgDim.=", ";
					}
					
					$strSelectImgDim.="RIDIM_".$imgdimid."KEY";
				}
				//@JAPR
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND FactKey IN ('.$strFactKeys.') ';
		}
		if(count($arrayImgDimIDs)>0)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "SELECT ".$strSelectImgDim." FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				foreach ($arrayImgDimIDs as $imgdimid)
				{
					$tmpTable = "RIDIM_".$imgdimid;
					$tmpField = "RIDIM_".$imgdimid."KEY";
					$tmpValue = (int)$aRS->fields[strtolower($tmpField)];
					
					//Eliminamos cualquier valor excepto el de no Aplica
					//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
					//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
					//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
					if($tmpValue!=1 && $bDeleteFactKeyDimVal)
					{
						$sql = "DELETE FROM ".$tmpTable." WHERE ".$tmpField." = ".$tmpValue;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tmpTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}

				$aRS->MoveNext();
			}
		}
		
		//Eliminacion de documentos
		$arrayDocDimIDs = array();
		if ($bDeleteFactKeyDimVal) {
			$sql = "SELECT PathDocument FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$DocsDir = getcwd()."\\"."surveydocuments";
			$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
			while (!$aRS->EOF)
			{
				$pathDocument = $aRS->fields["pathdocument"];
				@unlink($RepositoryDir."/".$pathDocument);
				$aRS->MoveNext();
			}
		
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "DELETE FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey IN ( ".$this->FactKeyDimVal." )".$strFactKeysFilter;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "SELECT DocDimID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND HasReqDocument > 0 AND DocDimID IS NOT NULL ORDER BY DocDimID";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$strSelectDocDim = "";
			while(!$aRS->EOF)
			{
				$docdimid = (int)$aRS->fields["docdimid"];
				//@JAPR 2012-10-22: Corregido un bug, si no está asignada esta dimensión no se debe intentar usar
				if ($docdimid > 0) {
					$arrayDocDimIDs[] = $docdimid;
					
					if($strSelectDocDim!="")
					{
						$strSelectDocDim.=", ";
					}
					
					$strSelectDocDim.="RIDIM_".$docdimid."KEY";
				}
				//@JAPR
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$strFactKeysFilter = '';
		if ($blnUseFactKeysFilter) {
			$strFactKeysFilter = ' AND FactKey IN ('.$strFactKeys.') ';
		}
		if(count($arrayDocDimIDs)>0)
		{
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
			$sql = "SELECT ".$strSelectDocDim." FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				foreach ($arrayDocDimIDs as $docdimid)
				{
					$tmpTable = "RIDIM_".$docdimid;
					$tmpField = "RIDIM_".$docdimid."KEY";
					$tmpValue = (int)$aRS->fields[strtolower($tmpField)];
					
					//Eliminamos cualquier valor excepto el de no Aplica
					//@JAPRWarning: Por lo pronto no se eliminarán las imagenes, ya que como funciona esto, en el mismo registro se repite el
					//valor de las preguntas estándar, por ender, no podríamos por eliminar un registro de Maestro-detalle borrar las imagenes
					//de preguntas estándar a menos que ya no existan registros en la captura, así que por ahora se dejarán dichas imagenes
					if($tmpValue!=1 && $bDeleteFactKeyDimVal)
					{
						$sql = "DELETE FROM ".$tmpTable." WHERE ".$tmpField." = ".$tmpValue;
						if ($this->Repository->DataADOConnection->Execute($sql) === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." ".$tmpTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}

				$aRS->MoveNext();
			}
		}
		
		//Eliminamos registros de la tabla matrix
		if ($bDeleteFactKeyDimVal) {
			$tableMatrix = "SVSurveyMatrixData_".$this->Survey->ModelID;
			$sql = "DELETE FROM ".$tableMatrix." WHERE FactKeyDimVal = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableMatrix." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Eliminamos registros de la tabla de categorydimension
			$tableCatDimVal = "SVSurveyCatDimVal_".$this->Survey->ModelID;
			$sql = "DELETE FROM ".$tableCatDimVal." WHERE FactKeyDimVal = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDimVal." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		//Eliminamos registros de la tabla paralela
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$sql = "DELETE FROM ".$this->Survey->SurveyTable." WHERE FactKeyDimVal = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->Survey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos registros de la tabla de hechos
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		//Sólo se deben eliminar los registros de los FactKeys removidos, no de toda la captura (si está en modo de edición)
		$sql = "DELETE FROM ".$factTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal.$strFactKeysFilter;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
		//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
		//editando o agregando otros
		//No se tiene hasta el momento una manera de identificar en la tabla global de encuestas cuales son los registros a eliminar
		$this->removeDataInGlobalSurveyModel($this->FactKeyDimVal, $arrayFactKeys, $bDeleteFactKeyDimVal);
		
		if ($bDeleteFactKeyDimVal) {
			//Eliminamos el registro de la dimension Encuesta ID
			$sql = "DELETE FROM ".$factKeyDimValTable." WHERE ".$factKeyDimValField." = ".$this->FactKeyDimVal;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$factKeyDimValTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
    
	//@JAPR 2015-08-13: Agregado el borrado de reportes a partir del ID de la captura
	/* Elimina los datos de la captura tal como se hacía en versiones previas (las tablas con la información que eForms almacena, pero NO los archivos de imagenes, documentos y demás
	que fueron almacenados) para permitir Reupload de la misma captura o simplemente eliminar un reporte equivocado
	Este proceso NO altera la información enviada a eBavel, ni la generada como parte de los cubos mapeados a la estructura de eForms, exceptuando el registro del cubo global Surveys,
	tampoco elimina los archivos exportados a diferentes destinos tales como Google Drive o DropBox
	El parámetro $factKeyDimVal indica el ID de la captura, el cual corresponde al valor del campo BITAMSurvey::$KeyField de la $aSurveyID especificada
	La función no genera un menesaje de error, en caso de fallar algún DELETE, se asumirá que dada la naturaleza cambiante de las tablas de esta versión, simplemente era un dato que
	ya no aplicaba y continuará con el resto de las tablas de la definición actual de la forma (preguntas y secciones ya no existentes asumirán que sus tablas se eliminaron al ser
	borradas de la definición de la forma)
	*/
	static function RemoveV6($aRepository, $aSurveyID, $factKeyDimVal) {
		if ($aSurveyID <= 0 || $factKeyDimVal <= 0) {
			return false;
		}
		
		//Carga las instancias a utilizar para eliminar la definición
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		if (is_null($surveyInstance) || $surveyInstance->CreationAdminVersion < esveFormsv6) {
			return;
		}
		
		//Verifica si esta captura fue generada con la versión que contiene los campos necesarios para su correcta eliminación, si no es así entonces cancela el proceso
		$dblVersionNum = 0;
		//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
		$intUserID = 0;
		$strUserEMail = '';
		$strSurveyDate = '';
		$sql = "SELECT eFormsVersionNum, UserID, UserEMail, DateID 
			FROM {$surveyInstance->SurveyTable} 
			WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$dblVersionNum = (float) @$aRS->fields[strtolower(BITAMSurvey::$KeyField)];
			//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
			$intUserID = (int) @$aRS->fields[strtolower("UserID")];
			$strUserEMail = (string) @$aRS->fields[strtolower("UserEMail")];
			$strSurveyDate = (string) @$aRS->fields[strtolower("DateID")];
		}
		if ($dblVersionNum < esvDeleteReports && false) {
			return;
		}
		
		$objSectionColl = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
		if (is_null($objSectionColl)) {
			return;
		}
		
		$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		if (is_null($objQuestionColl)) {
			return;
		}
		
		//Primero elimina los registros del cubo global Surveys, ya que es un elemento independiente a la captura pero depende de que exista esta para poder accesarlo
		require_once("processSurveyFunctionsV6.php");
		$strOriginalWD = getcwd();
		require_once("cubeClasses.php");
		require_once("dimension.inc.php");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
		chdir($strOriginalWD);
		if (!is_null($anInstanceModel)) {
			$factTable = $anInstanceModel->nom_tabla;
			$surveyKey = -1;
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
			if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
				$surveyKey = BITAMReport::GetSurveyDimKey($aRepository, $anInstanceModelDim, $aSurveyID);
			}
			
			if ($surveyKey !== false && $surveyKey > 0) {
				$surveyGlobalKey = -1;
				$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyGlobalDimID);
				if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
					$searchSurveyGlobalValue = $surveyKey."-".$factKeyDimVal;
					$surveyGlobalKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $searchSurveyGlobalValue, false);
					
					//En esta dimensión el valor 1 correspondería con No aplica, así que no se considera válido
					if ($surveyGlobalKey !== false && $surveyGlobalKey > 0) {
						$strDimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
						$sql = "DELETE FROM {$factTable} WHERE {$strDimFieldKey} = {$surveyGlobalKey}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
			}
		}
		
		//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$sql = "DELETE FROM SI_SV_SurveyAnswerImageSrc WHERE SurveyID = {$aSurveyID} AND FactKey = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		//@JAPR
		
		//Elimina los registros de datos adicionales a la forma (Comentarios, Fotos y Documentos) sin eliminar sus archivos físicamente
		$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = {$aSurveyID} AND FactKey = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = {$aSurveyID} AND FactKey = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = {$aSurveyID} AND FactKey = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		
		//Elimina los datos de las preguntas simple y múltiple choice, que son las que se graban en tablas independientes, aunque en realidad elimina los de todas las
		//preguntas para considerar el eventual caso en que se permita cambiar el tipo de pregunta, mientras no se llegue a ese punto, la mayoría de estos DELETEs van a fallar
		foreach ($objQuestionColl->Collection as $objQuestion) {
			$sql = "DELETE FROM {$objQuestion->QuestionDetTable} WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal}";
			$aRepository->DataADOConnection->Execute($sql);
		}
		
		//Elimina los datos de todas las secciones, los cuales incluirán a las preguntas que se graban directamente en ellas, tal como el caso de las preguntas, se eliminarán los datos
		//de todas las secciones y no sólo de aquellas que no sean estándar, para considerar posibles casos de cambios en el tipo de sección
		foreach ($objSectionColl->Collection as $objSection) {
			$sql = "DELETE FROM {$objSection->SectionMultTable} WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal}";
			$aRepository->DataADOConnection->Execute($sql);
		}
		
		//Elimina los datos de las secciones estándar
		$sql = "DELETE FROM {$surveyInstance->SurveyStdTable} WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		
		//Al final elimina el registro de la captura
		$sql = "DELETE FROM {$surveyInstance->SurveyTable} WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal}";
		$aRepository->DataADOConnection->Execute($sql);
		
		//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
		//Para concluir hace el borrado lógico de los logs para que no se vean en la bitácora
		if (getMDVersion() >= esvDeleteReports && trim($strUserEMail) != '' && trim($strSurveyDate) != '') {
			$sql = "UPDATE SI_SV_SurveyLog SET Deleted = 1 
				WHERE SurveyID = {$aSurveyID} AND UserID = ".$aRepository->DataADOConnection->Quote($strUserEMail)." 
					AND SurveyDate = ".$aRepository->DataADOConnection->DBTimeStamp($strSurveyDate);
			$aRepository->DataADOConnection->Execute($sql);
		}
		//@JAPR
	}
	
	/* Realiza el borrado incremental de agregaciones, el cual consiste en reinsertar los registros especificados pero con todos sus indicadores
	negados, de tal forma que la Suma de los mismos arroje un valor de 0, asumiendo por supuesto que todos los indicadores de eForms son tipo SUM
		Sólo necesita recibir el ID de la encuesta y el array de los FactKeys, ya que a partir de esos datos se cargarán todas las instancias
	necesarias
	*/
	static function IncrementalAggregationsRemove($aRepository, $aSurveyID, $factKeyDimVal, $anArrayFactKeys) {
		global $gblEFormsNA;
		global $gblShowErrorSurvey;
		global $intComboID;
		global $strIndicFieldsArrayCode;
		global $arrDateValues;
		global $arrDimKeyValues;
		global $arrDimDescValues;
		global $arrIndicValues;
		//global $arrDefDimValues;
		global $arrLastCatClaDescrip;	
		global $arrLastCatValues;
		global $gbTestIncrementalAggregationsData;
		
		$aSurveyID = (int) $aSurveyID;
		$factKeyDimVal = (int) $factKeyDimVal;
		if (is_null($aRepository) || $aSurveyID <= 0 || $factKeyDimVal <= 0) {
			return;
		}
		
		if ($gbTestIncrementalAggregationsData) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo ("<br>\r\n<br>\r\n IncrementalAggregationsRemove anArrayFactKeys: ");
				PrintMultiArray($anArrayFactKeys);
			}
		}
		if (is_null($anArrayFactKeys) || !is_array($anArrayFactKeys) || count($anArrayFactKeys) == 0) {
			return;
		}
		
		$strOriginalWD = getcwd();
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");

		//Agrega el archivo que define algunas funciones requeridas para el proceso, pero respeta el valor actual de la variable $gblShowErrorSurvey
		//ya que si no estaba cargado este archivo entonces se activa automáticamente la variable porque piensa que es parte del proceso de grabado
		//así que desactiva los errores que terminan la ejecución, y no necesariamente debe ser así siempre
		$blShowErrorSurvey = $gblShowErrorSurvey;
		require_once('processSurveyFunctions.php');
		$gblShowErrorSurvey = $blShowErrorSurvey;
		
		//***********************************************************************************************************************************
		//Carga las colecciones de preguntas que se procesarán para esta encuesta
		$dynamicSectionWithMultiChoice = false;
		$dynamicSectionCatMemberID = -1;
		$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
		$dynamicSectionCatID = 0;
		$surveyInstance = @BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		if ($dynamicSectionID > 0) {
			$dynamicSection = BITAMSection::NewInstanceWithID($aRepository, $dynamicSectionID);
			if (!is_null($dynamicSection)) {
				$dynamicSectionWithMultiChoice = ($dynamicSection->ChildCatMemberID > 0);
				$dynamicSectionCatMemberID = $dynamicSection->CatMemberID;
				$dynamicSectionCatID = $dynamicSection->CatalogID;
			}
		}
		
		if ($surveyInstance->SyncDateDimID > 0) {
			$aSyncDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncDateDimID);
		}
		if ($surveyInstance->SyncTimeDimID > 0) {
			$aSyncTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncTimeDimID);
		}
		$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->UserDimID);
		$eMailDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EmailDimID);
		$statusDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StatusDimID);
		$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartDateDimID);
		$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndDateDimID);
		$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartTimeDimID);
		$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndTimeDimID);
		if (getAppVersion() >= esvServerDateTime && 
				$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
			$aServerStartDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartDateDimID);
			$aServerEndDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndDateDimID);
			$aServerStartTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartTimeDimID);
			$aServerEndTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndTimeDimID);
		}
		$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AnsweredQuestionsIndID);
		$fieldAnsweredQuestionsInd = $answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;		
		
		$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->DurationIndID);
		$fieldDurationInd = $durationIndicator->field_name.$durationIndicator->IndicatorID;
		
		$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LatitudeIndID);
		$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LongitudeIndID);
		$fieldLatitudeInd = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
		$fieldLongitudeInd = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;
		
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID > 0) {
			$accuracyIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AccuracyIndID);
			$fieldAccuracyInd = $accuracyIndicator->field_name.$accuracyIndicator->IndicatorID;
		}
		
		//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
		$anArrayTypes = array();
		$anArrayTypes[0] = qtpMulti;
		$dspMatrix = qtpMulti;
        $questionCollection = BITAMQuestionCollection::NewInstanceWithExceptTypeQDisplayMode($aRepository, $aSurveyID, $anArrayTypes, $dspMatrix);
		
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q pertenezcan a secciones dinamicas y esten dentro de ellas
		$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($aRepository, $aSurveyID);
        
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple
		//y los cuales siempre tengan activados que si son IsMultiDimension			//y q no pertenezcan a dimensiones dinamicas o bien esten fuera de ellas (porción extraída desde el SaveData, que se supone es equivalente a esta parte)
		$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $aSurveyID);
		
		//***********************************************************************************************************************************
		//Obtiene los valores de algunos campos/dimensiones fijos de la captura
		$intFirstFactKey = (int) @$anArrayFactKeys[0];
		$schedulerKey = 1;
		$schedulerID = -99999;
		$schedulerDesc = $gblEFormsNA;
		$tableDimScheduler = "RIDIM_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerKey = $tableDimScheduler."KEY";
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerDsc = "DSC_".$surveyInstance->SchedulerDimID;;
		
		//Obtiene la llave Subrrogada del Scheduler asociado a esta captura
		$sql = "SELECT ".$fieldDimSchedulerKey." FROM ".$surveyInstance->FactTable." WHERE FactKey = ".$intFirstFactKey;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
		}
		
		//Obtiene el ID y Descripción del Scheduler a partir de la llave Subrrogada
		$sql = "SELECT ".$fieldDimSchedulerClave.', '.$fieldDimSchedulerDsc." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerKey." = ".$schedulerKey;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$schedulerID = (int) @$aRS->fields[strtolower($fieldDimSchedulerClave)];
			$schedulerDesc = (string) @$aRS->fields[strtolower($fieldDimSchedulerDsc)];
		}
		
		$surveyDate = getFieldDescFromFactTable($aRepository, $surveyInstance, 'DateID', $intFirstFactKey, $factKeyDimVal);
		
		//***********************************************************************************************************************************
		//Recorre todos los FactKeys especificados actualizando la información del Array
		foreach ($anArrayFactKeys as $factKey) {
			if ($gbTestIncrementalAggregationsData) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\n IncrementalAggregationsRemove intComboID: {$intComboID}, FactKey == {$factKey}");
				}
			}
			
			//Obtiene el identificador de la sección de este registro
			$anEntrySectionID = getFieldDescFromFactTable($aRepository, $surveyInstance, 'EntrySectionID', $factKey, $factKeyDimVal);
			$anEntryCatDimID = getFieldDescFromFactTable($aRepository, $surveyInstance, 'EntryCatDimID', $factKey, $factKeyDimVal);;
			
			//Inicializa los arrays en el índice actual antes de empezar a agregarle los datos del registro
			//@JAPR 2014-01-13: Corregido un bug, la referencia no se puede hacer como en los procesos de grabado ya que en este caso el array se
			//llena localmente, así que dejar la referencia como el original provocaba que todos sus elementos fueran exactamente el mismo
			//$arrRowDimValuesOld = array();
			//$arrRowDimDescValuesOld = array();
			$arrDimKeyValues[$intComboID] = array();
			$arrDimDescValues[$intComboID] = array();
			//@JAPR
			$arrRowIndicValuesOld = array();
			if ($strIndicFieldsArrayCode != '') {
				@eval($strIndicFieldsArrayCode);
				if (isset($arrRowIndicValues)) {
					foreach ($arrRowIndicValues as $anIndex => $aValue) {
						$arrRowIndicValuesOld[$anIndex] = $aValue;
					}
					//@JAPR 2014-01-13: Corregido un bug, la referencia no se puede hacer como en los procesos de grabado ya que en este caso el array se
					//llena localmente, así que dejar la referencia como el original provocaba que todos sus elementos fueran exactamente el mismo
					//$arrIndicValues[$intComboID] =& $arrRowIndicValuesOld;
					//@JAPR
				}
			}
			
			//@JAPR 2014-01-13: Corregido un bug, la referencia no se puede hacer como en los procesos de grabado ya que en este caso el array se
			//llena localmente, así que dejar la referencia como el original provocaba que todos sus elementos fueran exactamente el mismo
			//$arrDimKeyValues[$intComboID] =& $arrRowDimValuesOld;
			//$arrDimDescValues[$intComboID] =& $arrRowDimDescValuesOld;
			$arrRowDimValuesOld =& $arrDimKeyValues[$intComboID];
			$arrRowDimDescValuesOld =& $arrDimDescValues[$intComboID];
			//@JAPR 2014-01-13: Corregido un bug, se estaba enviando la fecha con todo y hora, pero la tabla de tiempo y en generar eForms no
			//inserta al cubo referencias que incluyan horas sino sólo día, así que no debe enviar las horas en este punto
			$arrDateValues[$intComboID] = substr($surveyDate, 0, 10);
			//@JAPR
			
			$arrQuestionDimValuesOld = array();
			$arrQuestionDimValuesDescOld = array();
			$arrLastCatClaDescrip = array();
			$arrLastCatValues = array();
			$arrCatalogDimValuesByCatalogOld = array();
			
			//Agrega los datos default de la encuesta en el orden en que se utilizarían durante el grabado
			
			//Comienza a recorrer las preguntas en el orden que utilizaria el grabado para insertar los datos en los arrays
			$arrProcessedAttribsByCatalogForIncAggrsOld = array();
			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				//@JAPR 2012-05-17: Agregados nuevos tipos de pregunta Foto y Firma
				//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($aQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSketch:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				if (!$blnValidQuestion)
				{
					continue;
				}
				//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
				elseif ($aQuestion->QTypeID == qtpGPS || $aQuestion->QTypeID == qtpMyLocation) {
					//Si la pregunta es tipo GPS, se extrae el valor actualmente grabado tal como si fuera alfanumérica, pero adicionalmente se tiene
					//que separar en sus 3 componentes para utilizarlos en el resto de los atributos
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					chdir($strOriginalWD);
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
					
					//Primero desgloza el valor del GPS en sus componentes si es que contiene algo que asemeje a un dato de GPS
					if (stripos($strDimDescValue, ',') !== false || stripos($strDimDescValue, '(') !== false) {
						$arrGPSData = SplitGPSData($strDimDescValue);
						$strLatitude = $arrGPSData[0];
						$strLongitude = $arrGPSData[1];
						$strAccuracy = $arrGPSData[2];
						
						//Verifica cual de los atributos está asignado (deberían estar todos, si no está alguno, entonces hace falta editar la pregunta)
						if ($aQuestion->LatitudeAttribID > 0) {
							$arrQuestionDimValuesOld[] = $strLatitude;
							$arrQuestionDimValuesDescOld[] = $strLatitude;
						}
						if ($aQuestion->LongitudeAttribID > 0) {
							$arrQuestionDimValuesOld[] = $strLongitude;
							$arrQuestionDimValuesDescOld[] = $strLongitude;
						}
						if ($aQuestion->AccuracyAttribID > 0) {
							$arrQuestionDimValuesOld[] = $strAccuracy;
							$arrQuestionDimValuesDescOld[] = $strAccuracy;
						}
					}
				}
				//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
				//La pregunta tipo Action viene como un texto con separadores, por lo tanto obtenemos sólo la respuesta que es el Action para grabar
				//en la tabla paralela/hechos
				else if ($aQuestion->QTypeID == qtpAction)
				{
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					chdir($strOriginalWD);
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
				else if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
				{
					if($aQuestion->IsIndicator==1)
					{
						$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
						chdir($strOriginalWD);
						if (!is_null($anInstanceIndicator)) {
							$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
							$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
						}
					}
					
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					chdir($strOriginalWD);
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				}
				else 
				{
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					chdir($strOriginalWD);
					
					//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
					//@AAL 06/05/2015: Modificado para soportar preguntas tipo código de barras
					if($aQuestion->QTypeID==qtpOpenDate || $aQuestion->QTypeID==qtpOpenString || $aQuestion->QTypeID==qtpOpenAlpha || $aQuestion->QTypeID==qtpOpenTime || $aQuestion->QTypeID==qtpBarCode)
					{
					}
					else if($aQuestion->QTypeID==qtpSingle || $aQuestion->QTypeID==qtpCallList) //Conchita agregado tipo llamada
					{
						//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
						//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
						//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
						if(($aQuestion->QTypeID==qtpSingle) && $aQuestion->UseCatalog==0 && $aQuestion->IndicatorID>0)
						{
							$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
							chdir($strOriginalWD);
							
							if (!is_null($anInstanceIndicator)) {
								$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
								$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
							}
						}
					}
					
					if ($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID == qtpSingle && $aQuestion->UseCatalog == 1) {
						if (!isset($arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID])) {
							$arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID] = array();
						}
						
						//@JAPR 2014-01-13: Corregido un bug, siempre debe pedir el valor actual del registro mediante la función getCatQuestionValueArray
						//en el caso de las preguntas de catálogo, dicha función determinará si es o no un registro dinámico y actuará de acuerdo a eso
						//para obtener todos los atributos necesarios
						//if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
						//}
						//else {
						//@JAPR 2013-11-15: Agregado el grabado incremental de agregaciones
						//En esta parte como se trata de una edición, se tienen que agregar al array de valores de las dimensiones los valores
						//actualmente grabados para esta combinación (aunque no vayan a cambiar), de lo contrario el agregado se recalculará
						//equivocadamente. Aquí la combinación actual y la anterior valía lo mismo para esta pregunta/catálogo, así que se hace
						//el proceso de llenado de arrays por duplicado
						$arrCatalogValues = getCatQuestionValueArray($aRepository, $surveyInstance, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID, $dynamicSectionID, $dynamicSectionCatID);
						if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
							foreach ($arrCatalogValues as $aClaDescrip => $strCatValueDesc) {
								if (isset($arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID][$aClaDescrip])) {
									//Este atributo ya se había procesado para este registro, por lo tanto ya no se puede agregar al array de datos
									//para agregaciones incrementales nuevamente
									continue;
								}
								
								if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
									$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
								}
								
								$strDimValue = $strCatValueDesc;
								if (trim($strDimValue) == '') {
									$strDimValue = "*NA";
								}
								$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
								$arrProcessedAttribsByCatalogForIncAggrsOld[$aQuestion->CatalogID][$aClaDescrip] = true;
							}
						}
						//}
						//@JAPR
					}
					else {
						$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
						$arrQuestionDimValuesOld[] = $strDimDescValue;
						$arrQuestionDimValuesDescOld[] = $strDimDescValue;
					}
				}
			}
			
			//Completa los atributos hasta los usados para las secciones dinámicas cuando el registro procesado no corresponde a ella, porque en ese
			//caso el ciclo previo sólo hubiera llenado los atributos hasta el usado en la pregunta simple choice que genera a la sección dinámica
			if ($surveyInstance->DissociateCatDimens && ($dynamicSectionID > 0)) {
				if ($anEntrySectionID != $dynamicSectionID) {
					//En este caso el registro procesado (que puede ser 0 o el ID de la sección Maestro-detalle) no es el mismo que la sección dinámica,
					//se graban sus atributos con NA
					$arrCatalogValues = getCatValueArrayForSurveyField($aRepository, $surveyInstance, array(1), $dynamicSectionCatID, (($dynamicSectionWithMultiChoice)?-1:$dynamicSectionCatMemberID));
					$arrLastCatClaDescripFlipped = array_flip($arrLastCatClaDescrip);
					if (is_array($arrCatalogValues) && count($arrCatalogValues) > 0) {
						foreach ($arrCatalogValues as $aClaDescrip => $intCatValueKey) {
							if (isset($arrProcessedAttribsByCatalogForIncAggrsOld[$dynamicSectionCatID][$aClaDescrip])) {
								//Este atributo ya se había procesado para este registro, por lo tanto heredará el valor que la pregunta anterior del
								//mismo catálogo venía arrastrando pues todas las preguntas del mismo catálogo están ligadas entre si
								continue;
							}
							
							if (!isset($arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID])) {
								$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID] = array();
							}
							
							$intIndex = @$arrLastCatClaDescripFlipped[$aClaDescrip];
							$strDimValue = '';
							if (!is_null($intIndex)) {
								$strDimValue = (string) @$arrLastCatValues[$intIndex];
							}
							if (trim($strDimValue) == '') {
								$strDimValue = "*NA";
							}
							$arrCatalogDimValuesByCatalogOld[$aQuestion->CatalogID][] = $strDimValue;
							$arrProcessedAttribsByCatalogForIncAggrsOld[$dynamicSectionCatID][$aClaDescrip] = true;
						}
					}
				}
			}
			
			//@JAPR 2014-01-10: Faltaban las preguntas multiple choice dentro de secciones dinámicas
			//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
			foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
			{
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				chdir($strOriginalWD);
				$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
				$arrQuestionDimValuesOld[] = $strDimDescValue;
				$arrQuestionDimValuesDescOld[] = $strDimDescValue;
				
				//Esto aplica para las preguntas multiple choice con numeros o textos
				//entonces lo procesamos como dimensiones unicas
				if ($aQuestion->MCInputType > mpcCheckBox)
				{
					if ($aQuestion->MCInputType == mpcNumeric && $aQuestion->IsIndicatorMC == 1)
					{
						$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
						chdir($strOriginalWD);
						if (!is_null($anInstanceIndicator)) {
							$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
							$arrRowIndicValuesOld[$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
						}
					}
				}
				else 
				{
				}
			}
			//@JAPR
			
			//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
			$arrayExistMultiChoiceIndicators = array();
			foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
			{
				//Verificar si la pregunta tiene asignado indicadores por scores
				if($aQuestion->MCInputType == mpcCheckBox) 
				{
					//Verificamos si hay un score capturado para esta opcion de pregunta de seleccion multiple
					//que no sea matriz o bien revisamos si ya se crearon indicadores anteriormente
					$existIndicators = false;
					
					//Recorremos las opciones para revisar los indicadores
					foreach ($aQuestion->SelectOptions as $optionKey=>$optionValue)
					{
						$indicatorid = 0;
						
						if(isset($aQuestion->QIndicatorIds[$optionKey]))
						{
							$indicatorid = (int)$aQuestion->QIndicatorIds[$optionKey];
						}
						else 
						{
							$indicatorid = 0;
						}
			
						if(!is_null($indicatorid) && $indicatorid!=0)
						{
							$existIndicators = true;
							break;
						}
					}
					
					$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = $existIndicators;
				}
				else 
				{
					$arrayExistMultiChoiceIndicators[$aQuestion->QuestionID] = false;
				}
				
				$arrayTempInds = array();
				$QIndDimIDs = $aQuestion->QIndDimIds;
				
				//Obtenemos las instancias de las dimensiones de las opciones
				foreach ($QIndDimIDs as $qKey=>$qDimID) 
				{
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $qDimID);
					
					if($aQuestion->MCInputType == mpcCheckBox && $arrayExistMultiChoiceIndicators[$aQuestion->QuestionID]==true) 
					{
						$tmpInd = (int)$aQuestion->QIndicatorIds[$qKey];
						
						if($tmpInd!=0)
						{
							$arrayTempInds[$qKey] = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $tmpInd);
						}
						else 
						{
							$arrayTempInds[$qKey] = null;
						}
					}
					else 
					{
						$arrayTempInds[$qKey] = null;
					}
					
					$strDimDescValue = @getDimDescFromFactTable($aRepository, $surveyInstance, $anInstanceDimension, $aQuestion, $factKey, $factKeyDimVal);
					$arrQuestionDimValuesOld[] = $strDimDescValue;
					$arrQuestionDimValuesDescOld[] = $strDimDescValue;
					chdir($strOriginalWD);
				}
				
				if($aQuestion->MCInputType == mpcCheckBox) 
				{
					foreach ($QIndDimIDs as $qKey => $qDimID) {
						$indicatorid = (int) @$aQuestion->QIndicatorIds[$qKey];
						if ($indicatorid != 0) {
							$anInstanceIndicator = @$arrayTempInds[$qKey];
							$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $anInstanceIndicator, $aQuestion, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
							$arrRowIndicValuesOld['IND_'.$indicatorid] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
						}
					}
				}
				else 
				{
				}
			}
			
			//Agrega los valores de las dimensiones default al array de recalculo incremental de agregaciones
			if ($surveyInstance->SyncDateDimID > 0) {
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aSyncDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}
			if ($surveyInstance->SyncTimeDimID > 0) {
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aSyncTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $userDimension, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $eMailDimension, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $statusDimension, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$arrRowDimValuesOld[] = $schedulerID;
			$arrRowDimDescValuesOld[] = $schedulerDesc;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aStartDateInstanceDim, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $anEndDateInstanceDim, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aStartTimeInstanceDim, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $anEndTimeInstanceDim, null, $factKey, $factKeyDimVal);
			$arrRowDimValuesOld[] = $strDimDescValue;
			$arrRowDimDescValuesOld[] = $strDimDescValue;
			
			if (getAppVersion() >= esvServerDateTime && 
					$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
				$aServerStartDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartDateDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerStartDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$aServerEndDateInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndDateDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerEndDateInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$aServerStartTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartTimeDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerStartTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
				
				$aServerEndTimeInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndTimeDimID);
				$strDimDescValue = getDimDescFromFactTable($aRepository, $surveyInstance, $aServerEndTimeInstanceDim, null, $factKey, $factKeyDimVal);
				$arrRowDimValuesOld[] = $strDimDescValue;
				$arrRowDimDescValuesOld[] = $strDimDescValue;
			}
			
			while (count($arrQuestionDimValuesOld) > 0) {
				array_push($arrRowDimValuesOld, array_shift($arrQuestionDimValuesOld));
			}
			while (count($arrQuestionDimValuesDescOld) > 0) {
				array_push($arrRowDimDescValuesOld, array_shift($arrQuestionDimValuesDescOld));
			}
			
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $answeredQuestionsIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
			$arrRowIndicValuesOld[$fieldAnsweredQuestionsInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $durationIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
			$arrRowIndicValuesOld[$fieldDurationInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $latitudeIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
			$arrRowIndicValues[$fieldLatitudeInd] = $dblCurrValue;
			$arrRowIndicValuesOld[$fieldLatitudeInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $longitudeIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
			$arrRowIndicValues[$fieldLongitudeInd] = $dblCurrValue;
			$arrRowIndicValuesOld[$fieldLongitudeInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID > 0) {
				$dblCurrValue = (float) getIndicatorValue($aRepository, $surveyInstance, $accuracyIndicator, null, $factKey, $factKeyDimVal, $anEntrySectionID, $anEntryCatDimID);
				$arrRowIndicValues[$fieldAccuracyInd] = $dblCurrValue;
				$arrRowIndicValuesOld[$fieldAccuracyInd] = ($dblCurrValue == 0)?0:$dblCurrValue * -1;
			}
			//@JAPR
			
			chdir($strOriginalWD);
			
			foreach ($arrCatalogDimValuesByCatalogOld as $intCatalogID => $arrCatalogValues) {
				foreach ($arrCatalogValues as $strDimValue) {
					$arrRowDimValuesOld[] = $strDimValue;
					$arrRowDimDescValuesOld[] = $strDimValue;
				}
			}
			
			$arrRowDimValuesOld[] = $factKeyDimVal;
			$arrRowDimDescValuesOld[] = $factKeyDimVal;
			
			//@JAPR 2014-01-13: Corregido un bug, la referencia no se puede hacer como en los procesos de grabado ya que en este caso el array se
			//llena localmente, así que dejar la referencia como el original provocaba que todos sus elementos fueran exactamente el mismo
			$arrIndicValues[$intComboID] = $arrRowIndicValuesOld;
			//@JAPR
			
			if ($gbTestIncrementalAggregationsData) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\n IncrementalAggregationsRemove arrRowDimDescValuesOld ({$intComboID}): ");
					PrintMultiArray($arrRowDimDescValuesOld);
				}
			}
			
			//Al terminar de procesar el registro incrementa el índice del array
			$intComboID++;
		}
	}
	
	//Elimina del cubo global de capturas de encuestas, todos los registros especificados en $arrayFactKeys de la captura registrada en el key
	//$factKeyDimVal, sin embargo tiene un error, ya que asume que el $arrayFactKeys es la captura completa representada por $factKeyDimVal,
	//por lo que inmediatamente después borra en si el registro de la captura, con lo que dañaría a registros que aun existieran, por lo tanto
	//si se ha usado, ha sido sólo para eliminar reportes completos
	//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
	//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
	//editando o agregando otros
    function removeDataInGlobalSurveyModel($factKeyDimVal, $arrayFactKeys, $bDeleteFactKeyDimVal = true) {
        $strOriginalWD = getcwd();
		
        //Obtener el DateKey
        require_once("cubeClasses.php");
        require_once("dimension.inc.php");
		
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
        //session_register("BITAM_UserID");
        //session_register("BITAM_UserName");
        //session_register("BITAM_RepositoryName");
        $_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
        $_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
        $_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
        require_once("../model_manager/model.inc.php");
        require_once("../model_manager/modeldata.inc.php");
        require_once("../model_manager/modeldimension.inc.php");
        require_once("../model_manager/indicatorkpi.inc.php");
		
        $anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->Survey->GblSurveyModelID);
        chdir($strOriginalWD);
        
        //Obtenemos el SurveyKey
        $aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveyDimID);
		//@JAPR 2015-08-13: Agregado el borrado de reportes a partir del ID de la captura
		//Modificada esta función para convertirla en estática y así poder invocarla desde el proceso de borrado de reportes de V6
        $surveyKey = BITAMReport::GetSurveyDimKey($this->Repository, $aSurveyInstanceDim, $this->Survey->SurveyID);
		//@JAPR
        
        //Obtenemos las dims SurveyGlobalDimID y SurveySingleRecordDimID
        $aSurveyGlobalInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveyGlobalDimID);
        $aSurveySingleRecordInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->Survey->GblSurveyModelID, -1, $this->Survey->GblSurveySingleRecordDimID);
        
        //campos surveyglobalid y surveysinglerecord
        $surveyGlobalDimTable = $aSurveyGlobalInstanceDim->Dimension->TableName;
        $surveyGlobalDimField = $aSurveyGlobalInstanceDim->Dimension->TableName."KEY";
        $surveySingleRecordDimTable = $aSurveySingleRecordInstanceDim->Dimension->TableName;
        $surveySingleRecordDimField = $aSurveySingleRecordInstanceDim->Dimension->TableName."KEY";
		
        $factTable = $anInstanceModel->nom_tabla;
		$searchSurveyGlobalValue = $surveyKey."-".$factKeyDimVal;
		//@JAPR: Ejemplo: $searchSurveyGlobalValue == KeySubrrogadoDelSurveyID-FactKeyDimVal
		foreach ($arrayFactKeys as $factKey)
		{
			$searchSurveySingleRecordValue = $searchSurveyGlobalValue."-".$factKey;
			//@JAPR: Ejemplo: $searchSurveySingleRecordValue == KeySubrrogadoDelSurveyID-FactKeyDimVal-FactKey
	        //obtenemos el surveysinglerecordkey
	        $surveySingleRecordDesc = "DSC_".$this->Survey->GblSurveySingleRecordDimID;
	        $sql = "SELECT ".$surveySingleRecordDimField." FROM ".$surveySingleRecordDimTable." WHERE ".$surveySingleRecordDesc. " = ".$this->Repository->DataADOConnection->Quote($searchSurveySingleRecordValue);
	        $aRS = $this->Repository->DataADOConnection->Execute($sql);
	        if(!$aRS)
	        {
	            echo("(".__METHOD__.") ".translate("Error accessing")." ".$surveySingleRecordDimField." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
	        }
	        
	        if (!$aRS->EOF)
	        {
	            $foundSurveySingleRecordKey = (int) $aRS->fields[strtolower($surveySingleRecordDimField)];
				//@JAPR: Borra registro de la dimensión de captura - registro: DELETE FROM RIDIM_10017 WHERE RIDIM_10017KEY = ###
		        $sql = "DELETE FROM ".$surveySingleRecordDimTable." WHERE ".$surveySingleRecordDimField. " = ".$foundSurveySingleRecordKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo("(".__METHOD__.") ".translate("Error accessing")." ".$surveySingleRecordDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
		        
		        //se elimina el registro de la tabla global
		        //@JAPR: Borra registro de la tabla de hechos de capturas global: DELETE FROM RIFACT_10007 WHERE RIDIM_10017KEY = ###
		        $sql = "DELETE FROM ".$factTable." WHERE ".$surveySingleRecordDimField." = ".$foundSurveySingleRecordKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
	        }
		}
        
		//@JAPR 2012-11-16: Agregado el parámetro $bDeleteFactKeyDimVal para indicar si se debe o no eliminar también la captura de encuesta, esto en 
		//caso de que invocara a este método desde una edición de captura sólo para eliminar algunos registros de la captura pero se estuvieran
		//editando o agregando otros
		if ($bDeleteFactKeyDimVal) {
	        //obtenemos el surveyglobalkey
	        $surveyGlobalDesc = "DSC_".$this->Survey->GblSurveyGlobalDimID;
	        $sql = "SELECT ".$surveyGlobalDimField." FROM ".$surveyGlobalDimTable." WHERE ".$surveyGlobalDesc." = ".$this->Repository->DataADOConnection->Quote($searchSurveyGlobalValue);
	        $aRS = $this->Repository->DataADOConnection->Execute($sql);
	        if(!$aRS)
	        {
	            echo("(".__METHOD__.") ".translate("Error accessing")." ".$surveyGlobalDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
	        }
	        
	        if (!$aRS->EOF)
	        {
	            $foundSurveyGlobalKey = (int) $aRS->fields[strtolower($surveyGlobalDimField)];
		        //se elimina el elemento de la tabla d edimension
				//@JAPR: Borra registro de la dimensión de captura - global: DELETE FROM RIDIM_10016 WHERE RIDIM_10016KEY = ###
		        $sql = "DELETE FROM ".$surveyGlobalDimTable." WHERE ".$surveyGlobalDimField. " = ".$foundSurveyGlobalKey;
		        $aRS = $this->Repository->DataADOConnection->Execute($sql);
		        if(!$aRS)
		        {
		            echo("(".__METHOD__.") ".translate("Error accessing")." ".$surveyGlobalDimTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().'<br>');
		        }
	        }
		}
		//@JAPR
    }
    
	//@JAPR 2015-08-13: Agregado el borrado de reportes a partir del ID de la captura
	/* Modificada esta función para convertirla en estática y así poder invocarla desde el proceso de borrado de reportes de V6
	Existe una función equivalente y mas completa durante el proceso de grabado, pero en este caso para borrar no se deben agregar nuevos valores a la dimensión así que se dejará
	esta versión independiente a la del proceso de grabado
	Se removió el reporte de errores, si algo falla simplemente regresará false
	*/
	static function GetSurveyDimKey($aRepository, $aSurveyInstanceDim, $aSurveyID) {
		$aSurveyID = (int) $aSurveyID;
		try {
			if (!$aRepository || !$aSurveyInstanceDim || $aSurveyID <= 0) {
				return false;
			}
			
			$valueKey = -1;
			$tableName = $aSurveyInstanceDim->Dimension->TableName;
			$fieldSurrogateKey = $tableName."KEY";
			$fieldKey = $aSurveyInstanceDim->Dimension->FieldKey;
			
			$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$aSurveyID;
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$valueKey = (int) @$aRS->fields[strtolower($fieldSurrogateKey)];
			}
		}
		catch (Exception $e) {
			$valueKey = false;
		}
		
		return $valueKey;
	}
	
	function isNewObject()
	{
		return ($this->ReportID < 0);
	}

	function get_Title()
	{
		return translate("Entry Detail");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}

		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}
		
		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Report&SurveyID=".$this->SurveyID.$strParameters;;
		}
		else
		{
			return "BITAM_PAGE=Report&SurveyID=".$this->SurveyID."&ReportID=".$this->ReportID.$strParameters;
		}
	}

	function get_Parent()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		return BITAMReportCollection::NewInstance($this->Repository, $this->SurveyID, null, $svStartDate, $svEndDate, $svUserID, $svIpp);
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ReportID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		$this->HasSignature = $this->Survey->HasSignature;
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EmailDesc";
		$aField->Title = translate("EMail");
		$aField->ToolTip = translate("EMail");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TimeID";
		$aField->Title = translate("Time");
		$aField->ToolTip = translate("Time");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyName";
		$aField->Title = translate("Form");
		$aField->ToolTip = translate("Form");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		//@JAPR 2012-06-06: Corregido un bug, en las secciones Maestro - Detalle los valores de las preguntas numéricas salían mal porque
		//les aplicaba una máscara, siendo que vienen como String combinando todos los registros capturados de dicha sección separado por ","
        $arrMasterDetSections = BITAMSection::existMasterDetSections($this->Repository, $this->SurveyID, true);
        if (!is_array($arrMasterDetSections))
        {
        	$arrMasterDetSections = array();
        }
        $arrMasterDetSections = array_flip($arrMasterDetSections);
		//@JAPR
		
		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			$fieldType = $this->FieldTypes[$i];
			$toolTip = $this->QuestionText[$i];
			$insideDyamicSection = ($this->Questions[$i]->SectionID==$this->DynamicSectionID);

			//15Abril2013: Que se despliegue la descripción de la pregunta
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$questionText = $this->Questions[$i]->getAttributeName();
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $fieldName;
			$aField->Title = $questionText;
			$aField->ToolTip = $toolTip;

			if($fieldType==1)
			{
				//@JAPR 2012-06-06: Corregido un bug, en las secciones Maestro - Detalle los valores de las preguntas numéricas salían mal porque
				//les aplicaba una máscara, siendo que vienen como String combinando todos los registros capturados de dicha sección separado por ","
				if ($this->DynamicSectionID == $this->Questions[$i]->SectionID || isset($arrMasterDetSections[$this->Questions[$i]->SectionID]))
				{
					$aField->Type = "String";
					$aField->Size = 255;
				}
				else 
				{
					$aField->Type = "Float";
					$aField->FormatMask = "#,##0.00";
				}
				//@JAPR
			}
			else
			{
				if($fieldType==3) 
				{
					$aField->Type = "LargeString";
					$aField->Size = 5000;
				}
				else
				{
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					//Cambiado a un switch para mayor facilidad
					$blnValidQuestion = true;
					switch ($this->Questions[$i]->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpSignature:
						case qtpSketch:
						case qtpSkipSection:
						case qtpMessage:
						case qtpSync:
						case qtpPassword:
						case qtpAudio:
						case qtpVideo:
						case qtpSection:
						case qtpOCR:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							$blnValidQuestion = false;
							break;
					}
					//@JAPR
					
					if(!$insideDyamicSection)
					{
						if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==0 && $this->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$aField->Type = "LargeString";
							$aField->Size = 5000;
						}
						else if($this->Questions[$i]->QTypeID==qtpMulti && $this->Questions[$i]->QDisplayMode==dspVertical && $this->Questions[$i]->MCInputType==1 && $this->Questions[$i]->IsMultiDimension==0 && $this->Questions[$i]->UseCategoryDimChoice!=0)
						{
							$aField->Type = "LargeString";
							$aField->Size = 5000;
						}
                        //@MABH 2012-05-22: Si es tipo foto solo debe mostrarse el icono
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						//Cambiado a un switch para mayor facilidad
                        else if(!$blnValidQuestion) {
                        
                        }
						else 
						{
							$aField->Type = "String";
							$aField->Size = 255;
						}
					}
					else 
					{
						//@JAPR 2014-04-21: Corregido un bug, ya se pueden tener fotos, comentarios y documentos en secciones dinámicas, así que
						//se deben comportar como sus similares en las otras secciones
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						//Cambiado a un switch para mayor facilidad
                        if(!$blnValidQuestion) {
                        	
                        }
                        else {
							$aField->Type = "LargeString";
							$aField->Size = 5000;
                        }
                        //@JARP
					}
				}
			}
			
			//@JAPR 2014-04-21: Corregido un bug, ya se pueden tener fotos, comentarios y documentos en secciones dinámicas, así que esta validación no aplica
			//if($this->DynamicSectionID!=$this->Questions[$i]->SectionID)
			//{
			$aField->AfterMessage = $this->getAfterMessage($this->Questions[$i]);
			//}
			
			$myFields[$aField->Name] = $aField;
		}
		if ($this->HasSignature == 1) {//Preguntamos si el survey tiene firma
            $aField = BITAMFormField::NewFormField();
            $aField->Name = "AltSignature";
            $aField->Title = translate("Signature");
            $aField->Type = "String";
            $aField->ToolTip = translate("Signature");
            $aField->IsLink = false;
            $aField->Size = 255;
            $aField->IsDisplayOnly = true;
            $aField->AfterMessage = $this->getAfterImageSignature();
            $myFields[$aField->Name] = $aField;
        }

		return $myFields;
	}

	function getAfterMessage($aQuestion)
	{
		$htmlString = "";

		//Verificamos si dicha pregunta tiene imagen a mostrar
		if ($aQuestion->HasReqPhoto==1 || $aQuestion->HasReqPhoto==2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				if($aQuestion->QTypeID == qtpPhoto) {
                    $strIcon="images/camara.gif";
                    $strTitle="View photo";
                } else {
					if($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch) {
	                    $strIcon="images/firma.gif";
	                    $strTitle="View signature";
					}
					else {
						//28Ene2013: En las preguntas con foto opcional o requerida que se despliegue el ícono de la cámara
						//y no el ícono de la firma
                		$strIcon="images/camara.gif";
                    	$strTitle="View photo";
					}
                }
                $renglon = 0;
                $columna = 0;
                $htmlString.="<img width='16px' height='16px' title='".$strTitle."' src='".$strIcon."' style='cursor:hand' onclick='javascript:openImg(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
			}
            
            if($aQuestion->QTypeID == qtpPhoto || $aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch || $aQuestion->QTypeID == qtpOCR) {
                if (!($aRS !== false && !$aRS->EOF)) {
                    if($aQuestion->QTypeID == qtpPhoto || $aQuestion->QTypeID == qtpOCR) {
                        $htmlString .= "No photo taken";
                    } else {
                        $htmlString .= "No signature captured";
                    }
                }
            }
		}

		//Verificamos si dicha pregunta tiene un documento a mostrar
		if ($aQuestion->HasReqDocument == 1 || $aQuestion->HasReqDocument == 2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				if($aQuestion->QTypeID == qtpDocument) {                
                    $strIcon="images/document.gif";
                    $strTitle="View document";
                }
				if($aQuestion->QTypeID == qtpAudio) {                
                    $strIcon="images/audio.gif";
                    $strTitle="Listen audio";
                }
				if($aQuestion->QTypeID == qtpVideo) {                
                    $strIcon="images/video.gif";
                    $strTitle="View video";
                }
                $renglon = 0;
                $columna = 0;
                $htmlString.="<img width='16px' height='16px' title='".$strTitle."' src='".$strIcon."' style='cursor:hand' onclick='javascript:openDoc(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
			}
            
            if($aQuestion->QTypeID == qtpDocument || $aQuestion->QTypeID == qtpAudio || $aQuestion->QTypeID == qtpVideo) {
                if (!($aRS !== false && !$aRS->EOF)) {
                    //if($aQuestion->QTypeID == qtpDocument) {
                        $htmlString .= "No document attached";
                    //}
                }
            }
		}
		
		if ($aQuestion->HasReqComment==1 || $aQuestion->HasReqComment==2)
		{
			$sql = "SELECT FactKey, SurveyID, QuestionID, StrComment FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->FactKeyDimVal." AND QuestionID = ".$aQuestion->QuestionID;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS !== false && !$aRS->EOF)
			{
				$strComment = trim($aRS->fields["strcomment"]);
				if($strComment!="")
				{
					$strIcon="images/comment.gif";
					$renglon = 0;
					$columna = 0;
					$htmlString.="<img width='16px' height='16px' title='View comment' src='".$strIcon."' style='cursor:hand' onclick='javascript:openComment(".$this->SurveyID.", ".$aQuestion->QuestionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")' displayMe='1'>";//.ENTER
				}
			}
		}
		
		return $htmlString;
	}
	
	function getAfterImageSignature() {
        $htmlString = "";

        //Verificamos si el reporte tiene alguna firma capturada
        $sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = " . $this->SurveyID . " AND FactKey = " . $this->ReportID;
        $aRS = $this->Repository->DataADOConnection->Execute($sql);

        if (!$aRS || $aRS->EOF) { //$aRS !== false && !$aRS->EOF
            $htmlString.="";
        } else {
            $strIcon = "images/firma.gif";
            $htmlString.= "<img width='16px'  height='16px' title='View Signature' src='" . $strIcon . "' style='cursor:hand' onclick='javascript:showSignature(" . $this->SurveyID . "," . $this->ReportID . ")' displayMe='1'>"; //.ENTER;
          
        }
        return $htmlString;
    }

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	function Header()
	{
		$longitudLine = 190;
		$heightFont = 6;
		$countY = 5;
		$heightSpace = 5;
		
		$this->doc->SetFont('arial', 'B', 15);
		$title = "KPIOnline eSurvey";
		$widthText = $this->doc->GetStringWidth($title);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, 6, $title, 0, 0, 'C');
		$countY = $this->validateOrdenadaY($countY, $heightText + $heightSpace);
		
		//Imprimir datos de la encuesta
		$heightFont = 5;
		$this->doc->SetFont('arial', 'B', 12);
		$surveyLine = "Survey: ".$this->SurveyName;
		$widthText = $this->doc->GetStringWidth($surveyLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $surveyLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);
		
		$userLine = "User: ".$this->UserName;
		$widthText = $this->doc->GetStringWidth($userLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $userLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);
		
		$dateLine = "Date of application: ".date('F j, Y H:i:s', strtotime($this->DateID));
		$widthText = $this->doc->GetStringWidth($dateLine);
		$numLines = ceil($widthText / $longitudLine);
		$heightText = $numLines * $heightFont;
		$this->doc->SetXY(10,$countY);
		$this->doc->Cell($longitudLine, $heightFont, $dateLine, 0, 0, 'J');
		$countY = $this->validateOrdenadaY($countY, $heightText);
		
		return $countY;
	}
	
	function Body($lastY=25)
	{
		$heightSpace = 10;
		$longitudLine = 190;
		$countY = $lastY + $heightSpace;
		$heightFont = 5;
		$heightSpace = 5;
		
		//Imprimir el cuerpo de la encuesta
		for ($i=0; $i<$this->NumQuestionFields; $i++)
		{
			$fieldName = $this->QuestionFields[$i];
			//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
			$fieldNameArray = $fieldName."Array";
			$fieldNameArrayHierarchy = $fieldNameArray."_Hierarchy";
			$objQuestion = $this->Questions[$i];
			//@JAPR 2013-05-14: Removidos tipos de pregunta que no generan un valor imprimible
			$blnValid = true;
			switch ($objQuestion->QTypeID) {
				case qtpShowValue:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpMessage:
				case qtpSkipSection:
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValid = false;
					break;
			}
			if (!$blnValid) {
				continue;
			}
			//@JAPR
			
			$questionText = $objQuestion->QuestionNumber.". ".$this->QuestionText[$i];
			
			//@JAPR 2013-05-24: Corregido un bug, las secciones dinámicas aun no están convertidas al nuevo formato, así que se removió el IF
			//temporalmente para no incluirlas
			//$blnIsMultipleSection = ($objQuestion->SectionID==$this->DynamicSectionID || in_array($objQuestion->SectionID, $this->MasterDetSectionIDs));
			$blnIsMultipleSection = (in_array($objQuestion->SectionID, $this->MasterDetSectionIDs));
			$arrQuestionValues = array();
			if ($blnIsMultipleSection) {
				//@JAPR 2013-05-23: Corregido un bug, si no se pudo obtener la propiedad, regresa un array vacio
				$arrQuestionValues = @$this->$fieldNameArray;
				if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==true)
				{
					if (isset($this->$fieldNameArrayHierarchy)) {
						//@JAPR 2013-05-23: Corregido un bug, si no se pudo obtener la propiedad, regresa un array vacio
						$arrQuestionValues = @$this->$fieldNameArrayHierarchy;
					}
				}
				
				//@JAPR 2013-05-23: Corregido un bug, si no se pudo obtener la propiedad, regresa un array vacio
				if (is_null($arrQuestionValues)) {
					$arrQuestionValues = array();
				}
				//@JAPR
			}
			else {
				//22-Ago-2018: Si es pregunta de selección sencilla (combo) y utiliza catálogo
				//la respuesta se leerá del campo que incluye una cadena con la jerarquía del catálogo
				//correspondiente a la respuesta (este campo se llena en report.inc.php
				if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==true)
				{
					if (isset($this->{$fieldName."_Hierarchy"})) {
						$fieldName=$fieldName."_Hierarchy";
					}
				}
				
				$arrQuestionValues = array($this->$fieldName);
			}
			
			//Imprimir la pregunta
			$questionLine = $questionText;
			$widthText = $this->doc->GetStringWidth($questionLine);
			$numLines = ceil($widthText / $longitudLine);
			$heightText = $numLines * $heightFont;
			$this->doc->SetFont('arial', 'B', 12);
			$this->doc->SetXY(10,$countY);
			$this->doc->MultiCell($longitudLine, $heightFont, $questionLine, 0, 'J');
			$countY = $this->validateOrdenadaY($countY, $heightText);
			
			$intMaxRecords = count($arrQuestionValues);
			for ($intRecNum = 0; $intRecNum < $intMaxRecords; $intRecNum++) {
				$answerText = $arrQuestionValues[$intRecNum];
				if (trim($answerText) == '') {
					if ($objQuestion->QTypeID == qtpOpenNumeric) {
						//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
						$answerText = 0;
						if ($this->exportToPDF) {
							if ($this->Survey->UseNULLForEmptyNumbers) {
								$answerText = "NA";
							}
						}
						//@JAPR
					}
					else {
						$answerText = "NA";
					}
				}
				/*
				//22-Ago-2018: Si es pregunta de selección sencilla (combo) y utiliza catálogo
				//la respuesta se leerá del campo que incluye una cadena con la jerarquía del catálogo
				//correspondiente a la respuesta (este campo se llena en report.inc.php
				if($this->Questions[$i]->QTypeID==qtpSingle && $this->Questions[$i]->UseCatalog==true)
				{
					if (isset($this->{$fieldName."_Hierarchy"})) {
						$fieldName=$fieldName."_Hierarchy";
					}
				}
				
				$answerText = $this->$fieldName;
				*/
				
				if ($blnIsMultipleSection) {
					//Imprimir la pregunta
					$questionLine = translate('Record #'.($intRecNum +1));
					$widthText = $this->doc->GetStringWidth($questionLine);
					$numLines = ceil($widthText / $longitudLine);
					$heightText = $numLines * $heightFont;
					$this->doc->SetFont('arial', 'B', 12);
					$this->doc->SetXY(10,$countY);
					$this->doc->MultiCell($longitudLine, $heightFont, $questionLine, 0, 'J');
					$countY = $this->validateOrdenadaY($countY, $heightText);
				}
				
	            if($this->FieldTypes[$i] != qtpPhoto && $this->FieldTypes[$i] != qtpSignature && $this->FieldTypes[$i] != qtpSketch && $this->FieldTypes[$i] != qtpOCR) {
	                //Imprimir la respuesta
	                $answerText = str_replace(chr(13).chr(10), " ", $answerText);
	                $answerLine = $answerText;
	                $widthText = $this->doc->GetStringWidth($answerLine);
	                $countEnter = substr_count($answerLine, chr(10));
	
	                $numLines = ceil($widthText / $longitudLine) + $countEnter;
	                $heightText = $numLines * $heightFont;
	                $this->doc->SetFont('arial', 'I', 12);
	                $this->doc->SetXY(10,$countY);
	                $this->doc->MultiCell($longitudLine, $heightFont, $answerLine, 0, 'J');
	            }
				
				//Si la pregunta tiene comentarios entonces procedemos a insertarlo tambien
				//omitimos comentarios de preguntas de secciones dinamicas
				//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
				//Las imagenes ahora se regresan como un array, si la sección es Maestro-Detalle o dinámica, se identificará el FactKey que corresponde
				//con el registro procesado de dicha sección, de lo contrario simplemente hubiera sido el único índice del array de imagenes
				$blnHasComment = false;
				$commentStr = '';
				$factKey = 0;
				$arrFactKeys = array();
				if ($blnIsMultipleSection) {
					if (isset($this->ArrayFactKeysBySection[$objQuestion->SectionID])) {
						//Verifica si hay una imagen para el registro de este índice buscando el FactKey que le corresponde
						$arrFactKeys = $this->ArrayFactKeysBySection[$objQuestion->SectionID];
						//$factKey = array_search($intRecNum, $arrFactKeys);
						$factKey = @$arrFactKeys[$intRecNum];
						
						if (!is_null($factKey)) {
							$commentStr = @$this->QComments[$i][$factKey];
							$blnHasComment = !is_null($commentStr);
						}
					}
				}
				else {
					//Si no es una sección múltiple, se debe buscar usando el FactKey de la instancia
					$commentStr = @$this->QComments[$i][$this->FactKey];
					$blnHasComment = !is_null($commentStr);
				}
				
				//if($this->QComments[$i]!="" && $this->Questions[$i]->SectionID!=$this->DynamicSectionID)
				if ($blnHasComment)
				{
					$countY = $this->validateOrdenadaY($countY, $heightText);
					/*@AAL Faltaba agregar la etiqueta del comentario cuando ésta se personaliza, al no 
					si no se definio alguna en la pregunta se asigna la de la sección si es que la hay */
					$CommentLabel = $objQuestion->CommentLabel;
					if (trim($CommentLabel) == "") 
						$CommentLabel = BITAMSurvey::NewInstanceWithID($this->Repository, $this->SurveyID)->CommentLabel;
					$commentLine = $CommentLabel . ": ".$commentStr;
					//@AAL
					$commentLine = str_replace(chr(13).chr(10), " ", $commentLine);
					$widthText = $this->doc->GetStringWidth($commentLine);
					$countEnter = substr_count($commentLine, chr(10));
					
					$numLines = ceil($widthText / $longitudLine) + $countEnter;
					$heightText = $numLines * $heightFont;
					$this->doc->SetFont('arial', '', 12);
					$this->doc->SetXY(10,$countY);
					$this->doc->MultiCell($longitudLine, $heightFont, $commentLine, 0, 'J');
				}
				
				$width = 0;
				$height = 0;
				//Si la pregunta tiene imagen entonces procedemos a insertarla tambien
				//omitimos imagenes de preguntas de secciones dinamicas
				//@JAPR 2013-05-13: Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
				//Las imagenes ahora se regresan como un array, si la sección es Maestro-Detalle o dinámica, se identificará el FactKey que corresponde
				//con el registro procesado de dicha sección, de lo contrario simplemente hubiera sido el único índice del array de imagenes
				$blnHasImage = false;
				$imagePath = '';
				$factKey = 0;
				$arrFactKeys = array();
				if ($blnIsMultipleSection) {
					if (isset($this->ArrayFactKeysBySection[$objQuestion->SectionID])) {
						//Verifica si hay una imagen para el registro de este índice buscando el FactKey que le corresponde
						$arrFactKeys = $this->ArrayFactKeysBySection[$objQuestion->SectionID];
						//$factKey = array_search($intRecNum, $arrFactKeys);
						$factKey = @$arrFactKeys[$intRecNum];
						
						if (!is_null($factKey)) {
							$imagePath = @$this->QImages[$i][$factKey];
							$blnHasImage = !is_null($imagePath);
						}
					}
				}
				else {
					//Si no es una sección múltiple, se debe buscar usando el FactKey de la instancia
					$imagePath = @$this->QImages[$i][$this->FactKey];
					$blnHasImage = !is_null($imagePath);
				}
				
				//if($this->QImages[$i]!="" && $this->Questions[$i]->SectionID!=$this->DynamicSectionID)
				if ($blnHasImage)
				{
					//$imagePath = $this->QImages[$i];
					$validPhoto = false;
					
					if (($img_info = @getimagesize($imagePath)) === false)
					{
						$imagePath = rtrim($imagePath);
						if (($img_info = @getimagesize($imagePath)) === false)
						{
							$imagePath = str_replace(' ', '%20', $imagePath);
					    	if (($img_info = @getimagesize($imagePath)) === true)
					    	{
					    		$validPhoto = true;
							}
						}
						else 
						{
							$validPhoto = true;
						}
					}
					else 
					{
						$validPhoto = true;
					}
	                
					//Validar si es una imagen valida, si no lo es entonces no se debera 
					//indicar que existe foto
					if($validPhoto)
					{
						$countY = $this->validateOrdenadaY($countY, $heightText);
						
	                    if($this->FieldTypes[$i] == qtpSignature || $this->FieldTypes[$i] == qtpSketch) {
	                        $imageLine = "Signature: ";
	                    } else {
	                        $imageLine = "Photo: ";
	                    }
						$widthText = $this->doc->GetStringWidth($imageLine);
						$numLines = ceil($widthText / $longitudLine);
						$heightText = $numLines * $heightFont;
						$this->doc->SetXY(10,$countY);
						$this->doc->SetFont('arial', '', 12);
						$this->doc->MultiCell($longitudLine, $heightFont, $imageLine, 0, 'J');
	
						$tipo = 'jpg';
						//@JAPR 2015-02-06: Agregado soporte para php 5.6
				        $W_H_img = explode(' ',$img_info[3]);
				        $w = str_replace("width=",'',$W_H_img[0]);
				        $w = $this->ajustarUnidad((float)str_replace('"','',$w));
				        $w_img = $w;
				        $h = str_replace("height=",'',$W_H_img[1]);
				        $h = $this->ajustarUnidad((float)str_replace('"','',$h));
				        $h_img = $h;
				        
				        if($w_img>$longitudLine || $h_img>100)
				        {
				        	$arrayMeasures = $this->adjustPhotoMeasures($w_img, $h_img);
				        	$width = $arrayMeasures["width"];
				        	$height = $arrayMeasures["height"];
				        }
				        else 
				        {
				        	$width = $w_img;
				        	$height = $h_img;
				        }
		
		   				$countY = $this->validateOrdenadaY($countY, $heightText);
						
						//Ahora validaremos si la imagen puede caber en la hoja sino
						//para que se mueva a la siguiente hoja
						if( ($countY + $height)>285) 
						{
							$this->doc->AddPage();
							$countY = 5;
						}
						$this->doc->SetXY(10,$countY);
						//@JAPR 2012-08-10: Corregido un bug, a partir de cierta versión v3 los archivos de firmas realmente comenzaron a ser archivos
						//.jpg, antes de eso la exportación a PDF hubiera fallado porque eran archivos .png (aunque disfrazados como .jpg)
						
						//Identifica el tipo de archivo antes de intentar usarlo, ya que si no es correcto generará un Fatal Error
						$strJPGHead = chr(255).chr(216);
						$readFile = fopen($imagePath, "rb");
						$strFileHead = '';
						if (!feof($readFile)) {
							$strFileHead = fread($readFile, 2);
						}
						fclose($readFile);
						
						//Si no corresponde con una cabecera de archivo JPG, asumirá que era de una versión previa y usará PNG
						$strTempFileName = '';
						if ($strFileHead != $strJPGHead) {
							//@JAPR 2012-08-10: Por alguna razón el método imagecreatefrompng estaba generando una excepción de memoria al intentar
							//procesar la imagen, así que simplemente se ignorarán estas firmas cuando son archivos .png
							$tipo = '';
							/*
							$tipo = 'png';
							$strTempFileName = tempnam(".\log", "JPG");
							if ($strTempFileName !== false) {
	    						$image = @imagecreatefrompng($imagePath);
	    						if ($image) {
		    						imagejpeg($image, $strTempFileName, 100);
		    						imagedestroy($image);
	    						}
							}
							*/
						}
						
						if ($tipo != '') {
							$this->doc->Image($imagePath, 10, $countY, $width, $height, $tipo);
						}
						if ($strTempFileName != '') {
							@unlink($strTempFileName);
						}
						//@JAPR
					}
				}
				
				if($height==0)
				{
					$height = $heightText;
				}
	
				$countY = $this->validateOrdenadaY($countY, $height + $heightSpace);
			}
			//@JAPR
		}
        
        if($this->Survey->HasSignature) {
            $questionLine = "Survey signature";
			$widthText = $this->doc->GetStringWidth($questionLine);
			$numLines = ceil($widthText / $longitudLine);
			$heightText = $numLines * $heightFont;
			$this->doc->SetFont('arial', 'B', 12);
			$this->doc->SetXY(10,$countY);
			$this->doc->MultiCell($longitudLine, $heightFont, $questionLine, 0, 'J');
			$countY = $this->validateOrdenadaY($countY, $heightText);
            
            if($this->Signature != "") {
                $imagePath = $this->Signature;
				$validPhoto = false;
				
				if (($img_info = @getimagesize($imagePath)) === false)
				{
					$imagePath = rtrim($imagePath);
					if (($img_info = @getimagesize($imagePath)) === false)
					{
						$imagePath = str_replace(' ', '%20', $imagePath);
				    	if (($img_info = @getimagesize($imagePath)) === true)
				    	{
				    		$validPhoto = true;
						}
					}
					else 
					{
						$validPhoto = true;
					}
				}
				else 
				{
					$validPhoto = true;
				}
                
				//Validar si es una imagen valida, si no lo es entonces no se debera 
				//indicar que existe foto
				if($validPhoto)
				{
                    
					$tipo = 'jpg';
					//@JAPR 2015-02-06: Agregado soporte para php 5.6
			        $W_H_img = explode(' ',$img_info[3]);
			        $w = str_replace("width=",'',$W_H_img[0]);
			        $w = $this->ajustarUnidad((float)str_replace('"','',$w));
			        $w_img = $w;
			        $h = str_replace("height=",'',$W_H_img[1]);
			        $h = $this->ajustarUnidad((float)str_replace('"','',$h));
			        $h_img = $h;
			        
			        if($w_img>$longitudLine || $h_img>100)
			        {
			        	$arrayMeasures = $this->adjustPhotoMeasures($w_img, $h_img);
			        	$width = $arrayMeasures["width"];
			        	$height = $arrayMeasures["height"];
			        }
			        else 
			        {
			        	$width = $w_img;
			        	$height = $h_img;
			        }
	
	   				$countY = $this->validateOrdenadaY($countY, $heightText);
					
					//Ahora validaremos si la imagen puede caber en la hoja sino
					//para que se mueva a la siguiente hoja
					if( ($countY + $height)>285) 
					{
						$this->doc->AddPage();
						$countY = 5;
					}
					$this->doc->SetXY(10,$countY);
					//@JAPR 2012-08-10: Corregido un bug, a partir de cierta versión v3 los archivos de firmas realmente comenzaron a ser archivos
					//.jpg, antes de eso la exportación a PDF hubiera fallado porque eran archivos .png (aunque disfrazados como .jpg)
					
					//Identifica el tipo de archivo antes de intentar usarlo, ya que si no es correcto generará un Fatal Error
					$strJPGHead = chr(255).chr(216);
					$readFile = fopen($imagePath, "rb");
					$strFileHead = '';
					if (!feof($readFile)) {
						$strFileHead = fread($readFile, 2);
					}
					fclose($readFile);
					
					//Si no corresponde con una cabecera de archivo JPG, asumirá que era de una versión previa y usará PNG
					$strTempFileName = '';
					if ($strFileHead != $strJPGHead) {
						//@JAPR 2012-08-10: Por alguna razón el método imagecreatefrompng estaba generando una excepción de memoria al intentar
						//procesar la imagen, así que simplemente se ignorarán estas firmas cuando son archivos .png
						$tipo = '';
						/*
						$tipo = 'png';
						$strTempFileName = tempnam(".\log", "JPG");
						if ($strTempFileName !== false) {
    						$image = @imagecreatefrompng($imagePath);
    						if ($image) {
	    						imagejpeg($image, $strTempFileName, 100);
	    						imagedestroy($image);
    						}
						}
						*/
					}
					
					if ($tipo != '') {
						$this->doc->Image($imagePath, 10, $countY, $width, $height, $tipo);
					}
					if ($strTempFileName != '') {
						@unlink($strTempFileName);
					}
					//@JAPR
				}
            }
        }
	}
	
	function validateOrdenadaY($currentPosition, $height)
	{
		if( ($currentPosition + $height)>285) 
		{
			$this->doc->AddPage();
			$currentPosition = 5;
		}
		else 
		{
			$currentPosition = $currentPosition + $height;
		}
		
		return $currentPosition;
	}
	
	function adjustPhotoMeasures($width, $height)
	{
		$longitudLine = 190;
		$arrayMeasures = array();
		$arrayMeasures["width"] = $width;
		$arrayMeasures["height"] = $height;
		
		if($width>$longitudLine)
		{
			$porcentaje = ($longitudLine*100)/$width;
			$newWidth = $width*($porcentaje/100);
			$newHeight = $height*($porcentaje/100);
			
			if($newWidth>$longitudLine || $newHeight>100)
			{
				$arrayMeasures = $this->adjustPhotoMeasures($newWidth, $newHeight);
				return $arrayMeasures;
			}
			else 
			{
				$arrayMeasures["width"] = $newWidth;
				$arrayMeasures["height"] = $newHeight;
				return $arrayMeasures;
			}
		}
		
		if($height>100)
		{
			$porcentaje = (100*100)/$height;
			$newHeight = $height*($porcentaje/100);
			$newWidth = $width*($porcentaje/100);
			
			if($newWidth>$longitudLine || $newHeight>100)
			{
				$arrayMeasures = $this->adjustPhotoMeasures($newWidth, $newHeight);
				return $arrayMeasures;
			}
			else 
			{
				$arrayMeasures["width"] = $newWidth;
				$arrayMeasures["height"] = $newHeight;
				return $arrayMeasures;
			}
		}
		
		return $arrayMeasures;
	}
	
	function exportSurveyToPDF($sendToFile=true)
	{
		require_once ('pdf/fpdf.php');
		$this->orientation = "P";
		$this->unidad = "mm";
		$this->format = "A4";
		$this->displayMode = "fullpage";
		$title = "KPIOnline eSurvey";
		
		$this->doc = new FPDF($this->orientation, $this->unidad, $this->format);
		$this->doc->SetDisplayMode($this->displayMode);
		$this->doc->AddPage();
		$this->doc->SetTitle($title);
		$this->doc->SetFillColor(255,255,255);
		$this->AddFont("arial", "");
		$this->AddFont("arial", "B");
		$this->AddFont("arial", "I");
		
		$countY = $this->Header();
		$this->Body($countY);
		
		//Generamos el nombre del pdf
	    $this->fileName = 'Survey_'.$this->SurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.pdf';
	    //@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	    $this->filePath = GeteFormsLogPath().$this->fileName;
	    //@JAPR
	    if($sendToFile==true)
	    {
			$this->doc->Output($this->filePath);
	    }
	    else 
	    {
	    	$this->doc->Output();
	    }
	}
	
	function sendNotification()
	{
		if(!is_null($this->fileName) && trim($this->fileName)!="")
		{
			NotifyNewSurveyCapturedByMail($this);
		}
	}
	
    function AddFont($family, $style='')
    {
    	$family = strtolower($family);
    	$style = strtolower($style);
		
    	if (!isset($this->doc->aSupportedFonts[$family]))
    	{
    		$family = 'calibri';
    	}
		
    	if (strpos($style, 'u') !== false)
    	{
    		$style = str_replace('u', '', $style);
    	}
		
    	if($style=='ib') $style='bi';
    	$keyFont = $family.$style;
		
  		if (!isset($this->aListFontsAdded[$keyFont]))
		{
			$this->doc->AddFont($family,$style,$keyFont.'.php');
			$this->aListFontsAdded[$keyFont] = $keyFont;
		}
    }
    
	function ajustarUnidad($valor)
    {
        switch ($this->unidad) {
        	case 'pt':
				$valor = $valor/1;
				break;
        	case 'mm':
        		$valor = $valor/(72/25.4);
				break;
        	case 'cm':
				$valor = $valor/(72/2.54);
				break;
        	case 'in':
				$valor = $valor/72;
				break;
        }
        return $valor;
    }
	//@JAPR
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideBackButton($aUser)
	{
		return true;
	}

/*
	function addButtons($aUser)
	{
?>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="googleMap(<?=$this->SurveyID?>, '<?=$this->UserID?>', '<?=$this->DateID?>', <?=$this->ReportID?>);"><img src="images/map.gif" displayMe="1"> <?=translate("Show Data in Google Maps")."   "?>  </span>
		&nbsp;&nbsp;&nbsp;&nbsp;<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="exportPDF();"><img src="images/exportpdf.gif"  displayMe="1"> <?=translate("Export To PDF")."   "?>  </span>
<?
	}
*/

	function getImages($questionID)
	{
		global $hasPic;
		global $hasDocument;
		global $hasComment;

		$htmlString="";

		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		if(isset($hasPic[$this->FactKeyDimVal]))
		{
			if(isset($hasPic[$this->FactKeyDimVal][$questionID]))
			{
				if($hasPic[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/camara.gif";
					$renglon = 0;
					$hasPic[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasPic[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View photo' src='".$strIcon."' style='cursor:hand' onclick='javascript:openImg(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}

		if(isset($hasDocument[$this->FactKeyDimVal]))
		{
			if(isset($hasDocument[$this->FactKeyDimVal][$questionID]))
			{
				if($hasDocument[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/document.gif";
					$renglon = 0;
					$hasDocument[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasDocument[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View document' src='".$strIcon."' style='cursor:hand' onclick='javascript:openDoc(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}

		if(isset($hasComment[$this->FactKeyDimVal]))
		{
			if(isset($hasComment[$this->FactKeyDimVal][$questionID]))
			{
				if($hasComment[$this->FactKeyDimVal][$questionID]!=null)
				{
					$strIcon="images/comment.gif";
					$renglon = 0;
					$hasComment[$this->FactKeyDimVal][$questionID][0];
					$columna = 0;
					$hasComment[$this->FactKeyDimVal][$questionID][1];
					$htmlString.="<img width='16px' height='16px' title='View comment' src='".$strIcon."' style='cursor:hand' onclick='javascript:openComment(".$this->SurveyID.", ".$questionID.", ".$this->FactKeyDimVal.", ".$renglon.", ".$columna.")'>";//.ENTER
				}
			}
		}
		//@JAPR

		return $htmlString;
	}

	function generateBeforeFormCode($aUser)
	{
		require_once('settingsvariable.inc.php');
		$strAltEformsService = '';
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ALTEFORMSSERVICE');
		if (!is_null($objSetting) && trim($objSetting->SettingValue != ''))
		{
			$strAltEformsService = $objSetting->SettingValue;
			if (substr($strAltEformsService, -1) != "/") {
				$strAltEformsService .= "/";
			}
			
			if (strtolower(substr($strAltEformsService, 0, 4)) != 'http') {
				$strAltEformsService = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$strAltEformsService;
			}
		}
		
?>		<script language="JavaScript">

		function openImg(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyImage.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=800, height=600, left=300, top=100, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openDoc(surveyID, questionID, folioID, renglon, columna)
		{
			window.open('getSurveyDocument.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openComment(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'commentWin', 'width=400, height=150, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}
		
		function showSignature(surveyID, reportID)
        {
			var aWindow = window.open('getSurveySignature.php?surveyID='+surveyID+'&reportID='+reportID, 'signatureWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
        }

		function openGoogleMap(surveyID, userID, startDate, endDate, reportID, reportType)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
			console.log('abriendo googlemap');
			var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&userID='+userID+'&startDate='+startDate+'&endDate='+endDate+'&reportID='+reportID, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}

		//Exportar los datos del Reporte actual en GoogleMaps
 		function googleMap(surveyID, userID, dateID, reportID, reportType)
 		{
 	 		console.log('googleMap1');
			openGoogleMap(surveyID, userID, dateID, dateID, reportID, reportType);
 		}
 		
  		function exportPDF()
 		{
			frmExportPDF.submit();
 		}
 
 		function editEntry(entryID)
 		{
 			var surveyID = <?=$this->SurveyID?>;

 			openWindow('<?=$strAltEformsService?>loadEntry.php?surveyID=' + surveyID + '&extp=1&EntryID=' + entryID);
 		}
		</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
?>
	<form id="frmExportPDF" name="frmExportPDF" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_PAGE" value="Report">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportID" id="ReportID" value="<?=$this->ReportID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportPDF">
	</form>
<?
	}

	function Signature()
	{
		$htmlString = "";

		//Verificamos si el reporte tiene alguna firma capturada
		$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID." AND FactKey = ".$this->ReportID;
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if(!$aRS || $aRS->EOF)
		{
			$htmlString = "";
		}
		else
		{
			$htmlString="<a href=javascript:showSignature(".$this->SurveyID.",".$this->ReportID.")><img src='images/firma.gif' title='".translate("View Signature")."'></a>";
		}

		return $htmlString;
	}

	function AltSignature() 
	{
        $htmlString = "";

        //Verificamos si el reporte tiene alguna firma capturada
        $sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE SurveyID = " . $this->SurveyID . " AND FactKey = " . $this->ReportID;
        $aRS = $this->Repository->DataADOConnection->Execute($sql);
        if (!$aRS || $aRS->EOF) {
            $htmlString = "Not captured";
        } else {
            $htmlString = "Captured";
        }

        return $htmlString;
    }
	
	function Task()
	{
		$htmlString="<a id=\"linkView\" name=\"linkView\" href=\"main.php?".$this->get_QueryString()."\">View</a>&nbsp;&nbsp;&nbsp;";
		$htmlString.="<a id=\"linkEdit\" name=\"linkEdit\" style=\"display:none\" href=\"javascript:editEntry(".$this->FactKeyDimVal.");\">Edit</a>";

		return $htmlString;
	}
	
	function addButtons($aUser)
	{
?>
	<!--<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:document.location.href='<?='main.php?'.$this->get_Parent()->get_QueryString()?>';"><img src="images/home.png" displayMe="1">&nbsp;<?=translate("Back")?></span>-->
	<button id="btnHome"  onclick="javascript:document.location.href='<?='main.php?'.$this->get_Parent()->get_QueryString()?>';"><img src="images/home.png" alt="<?=translate('Back')?>" title="<?=translate('Back')?>" displayMe="1" /> <?=translate("Back")?></button>
	<!--<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editEntry(<?=$this->FactKeyDimVal?>);"><img src="images/edit.png" displayMe="1">&nbsp;<?=translate("Edit")?></span>-->
	<button id="btnEditEntry" onclick="javascript:editEntry(<?=$this->FactKeyDimVal?>);"><img src="images/edit.png" alt="<?=translate('Edit')?>" title="<?=translate('Edit')?>" displayMe="1" /> <?=translate("Edit")?></button>
	<!--<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:BITAMReport_Remove();"><img src="images/delete.png" displayMe="1">&nbsp;<?=translate("Delete")?></span>-->
	<button id="btnDeleteEntry" onclick="javascript:BITAMReport_Remove();"><img src="images/delete.png" alt="<?=translate('Delete')?>" title="<?=translate('Delete')?>" displayMe="1" /> <?=translate("Delete")?></button>
	<!--<span class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')"><img src="images/edit.png" displayMe="1">&nbsp;<?=translate("Audit trail")?></span>-->
	<!--<button id="btnATrail"><img src="images/edit.png" alt="<?=translate('Audit trail')?>" title="<?=translate('Audit trail')?>" displayMe="1" /> <?=translate("Audit trail")?></button>-->
	<!--<span style="float:right;" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="exportPDF();"><img src="images/exportpdf.gif"  displayMe="1">&nbsp;<?=translate("Export To PDF")?></span>-->
	<button id="btnExportToPDF" onclick="exportPDF();"><img src="images/exportpdf.gif" alt="<?=translate('Export To PDF')?>" title="<?=translate('Export To PDF')?>" displayMe="1" /> <?=translate("Export To PDF")?></button>
	<!--<span style="float:right;" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="googleMap(<?=$this->SurveyID?>, '<?=$this->UserID?>', '<?=$this->DateID?>', <?=$this->ReportID?>);"><img src="images/map.gif" displayMe="1">&nbsp;<?=translate("Map data")?></span>-->
	<button id="btnMap" onclick="googleMap(<?=$this->SurveyID?>, '<?=$this->UserID?>', '<?=$this->DateID?>', <?=$this->ReportID?>);"><img src="images/mapa.gif" alt="<?=translate('Map data')?>" title="<?=translate('Map data')?>" displayMe="1" /> <?=translate("Map data")?></button>
<?
	}
	
	function insertPreviewRows($aUser)
	{
?>
		<tr id="Row_SurveyName">
			<td class="object_field_value" title="<?=translate("Survey")?>"><b><?=translate("Survey")?></b></td>
			<td class="object_field_value_3" colspan="3"><?=$this->SurveyName?></td>
		</tr>
<?	
	}
	
	//Necesaria para que el grabado con processRequest no genere error
	function getJSonDefinition()
	{
		$arrDef = array();
		return $arrDef;
	}
}

class BITAMReportCollection extends BITAMCollection
{
	public $SurveyID;
	public $QuestionFields;
	public $FieldTypes;
	public $QuestionText;
	public $NumQuestionFields;
	public $FirstCatQuestion; //Instancia de la primera pregunta que usa catálogo en sus respuestas
	public $CatTableName; //Nombre del catálogo usado por la pregunta $FirstCatQuestion
	public $FilterAttributes; //Infomación de los atributos que están en el catálogo $CatTableName que serán utilizados como combos de filtros en la colección.
	public $NumFilterAttributes;
	public $AttribSelected;
	public $CatParentID;
	public $CatalogID;
	public $Questions;
	public $StartDate;
	public $EndDate;
	public $UserID;
	public $ipp;
	public $HasSignature;
	public $ShowAllQuestions;
	public $Pages;
	
	public $Survey;
	public $HasDynamicSection;
	public $DynamicSectionID;
	public $DynamicSection;
	public $DynQuestionCollection;
	public $Catalog;
	public $ChildDynamicAttribID;
	//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
	public $HasMasterDetSection;
	public $MasterDetSectionIDs;
	public $MasterDetQuestionCollection;
	public $ArrayMasterDetSectionValues;
	
	public $GroupsNames;
	public $UserGroup;
	//$UserWithAllGroups: Es un arreglo indexado por el correo del usuario y cada elemento contiene otro arreglo 
	//con las claves de gpo a los que pertenece el usuario
	public $UserWithAllGroups; 
	public $Groups;
	//ears 2017-01-05 manejo de UserID a partir de los ajustes a filtros de usuarios y grupos
	public $UserIDs;
 
	//@JAPR

	function __construct($aRepository, $aSurveyID, $ShowAllQuestions=true)
	{
		BITAMCollection::__construct($aRepository);

		$this->SurveyID = $aSurveyID;
		$this->QuestionFields = array();
		$this->FieldTypes = array();
		$this->QuestionText = array();
		$this->NumQuestionFields = 0;
		$this->ShowAllQuestions = $ShowAllQuestions;
		$this->HasSignature = 0;
		$this->MsgConfirmRemove = "Do you wish to delete the selected entries?";
		$this->MsgNoSelectedRemove = "Please select the entries you wish to delete.";
        
        $this->MsgConfirmSendPDF = "Do you wish to Send PDF of the selected entries?";
		$this->MsgNoSelectedSendPDF = "Please select the entries you wish to Send PDF.";
        $this->MsgCancelSendPDF = "Send PDF of selected entries cancelled.";
		
		$this->FirstCatQuestion = null;
		$this->CatTableName = "";
		$this->FilterAttributes = array();
		$this->CatParentID = null;
		$this->NumFilterAttributes = 0;
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->ipp = 100;
		
		$this->Pages = null;
		
		$this->HasDynamicSection = false;
		$this->DynamicSectionID = -1;
		$this->DynamicSection = null;
		$this->DynQuestionCollection = null;
		$this->Catalog = null;
		$this->ChildDynamicAttribID = -1;
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$this->HasMasterDetSection = false;
		$this->MasterDetSectionIDs = array();
		$this->MasterDetQuestionCollection = array();
		$this->ArrayMasterDetSectionValues = array();

		$this->GroupsNames = array();
		$this->UserGroup = array();
		$Groups = array();
		$UserIDs = array();

		//@JAPR

		if($aSurveyID != 0)
		{
			$this->Survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);

			$dynamicSectionID = BITAMSection::existDynamicSection($aRepository, $aSurveyID);
			
			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			$this->Survey->getSpecialFieldsStatus();
			//@JAPR
			
			if($dynamicSectionID>0)
			{
				$this->HasDynamicSection = true;
				$this->DynamicSectionID = $dynamicSectionID;
				$this->DynamicSection = BITAMSection::NewInstanceWithID($aRepository, $this->DynamicSectionID);

				//Obtenemos el ChildDynamicAttribID
				$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($aRepository, $this->DynamicSection->CatMemberID);
				
				//Obtener el siguiente CatMemberID
				$sql = "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$this->DynamicSection->CatalogID." AND MemberOrder > ".$catMemberInstance->MemberOrder." ORDER BY MemberOrder ASC";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
		
				if(!$aRS->EOF)
				{
					//Asignamos el atributo hijo
					$this->ChildDynamicAttribID = (int)$aRS->fields["parentid"];
					
					//Obtenemos coleccion de las preguntas de la seccion dinamica
					$this->DynQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $this->DynamicSectionID);
					
					//Obtenemos el catalogo
					$this->Catalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->DynamicSection->CatalogID);
				}
				else 
				{
					//Se vuelven a quedar los valores como si no hubiera seccion dinamica
					$this->HasDynamicSection = false;
					$this->DynamicSectionID = -1;
					$this->DynamicSection = null;
				}
			}
			
			$aSurvey = $this->Survey;
			$this->HasSignature = $aSurvey->HasSignature;

			if($this->ShowAllQuestions)
			{
				$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			}
			else
			{
				$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID, null, 1);
			}
	
			$this->Questions = $questions;
			$numFields = count($questions->Collection);
	
			for($i=0; $i<$numFields; $i++)
			{
				$this->QuestionFields[$i] = $questions->Collection[$i]->SurveyField;
				$this->FieldTypes[$i] = $questions->Collection[$i]->QTypeID;
				$this->QuestionText[$i] = $questions->Collection[$i]->QuestionText;
			}
	
			$this->NumQuestionFields = $numFields;
	
			$questionID = BITAMReportCollection::getFirtsCatalogQuestion($this->Repository, $this->SurveyID);
	
			if($questionID!==null)
			{
				$this->FirstCatQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $questionID);
				$this->CatTableName = BITAMReportCollection::getCatTableName($this->Repository, $this->FirstCatQuestion->CatalogID);
				$this->CatParentID = BITAMReportCollection::getCatParentID($this->Repository, $this->FirstCatQuestion->CatalogID);
				$this->FilterAttributes = BITAMReportCollection::getAttributes($this->Repository, $this->FirstCatQuestion->CatalogID, $this->FirstCatQuestion->CatMemberID);
				$this->NumFilterAttributes = count($this->FilterAttributes);
				$this->CatalogID = $this->FirstCatQuestion->CatalogID;
				foreach ($this->FilterAttributes as $memberID => $memberInfo)
				{
					$this->AttribSelected[$memberID]= "";
				}
			}
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$arrMasterDetSections = BITAMSection::existMasterDetSections($aRepository, $aSurveyID, true);
			if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
			{
				$this->HasMasterDetSection = true;
				$this->MasterDetSectionIDs = $arrMasterDetSections;
				
				//Obtiene la colección de preguntas para indexarlas por cada Id de sección Maestro - Detalle
				foreach ($this->MasterDetSectionIDs as $aSectionID)
				{
					$aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($this->Repository, $aSectionID);
					if (!is_null($aQuestionCollection))
					{
						$this->MasterDetQuestionCollection[$aSectionID] = $aQuestionCollection;
					}
					else 
					{
						$this->MasterDetQuestionCollection[$aSectionID] = BITAMQuestionCollection::NewInstanceEmpty($this->Repository, $this->SurveyID);
					}
				}
			}
			//@JAPR
		}
	}

	static function NewInstance($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2, $ipp=100)
	{
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, false);
		
		//Si es igual a cero el SurveyID no deberia hacer algo
		if($aSurveyID==0)
		{
			return $anInstance;
		}
		
		$aSurvey = $anInstance->Survey;
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		//Obtener los datos de la dimension Email
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSketch:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd." B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.= $strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
			}
		}
		
		$where = $joinCat;
		$filterJoin = "";
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$filterJoin .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$filterJoin .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					if($where!="")
					{
						$where.=" AND ";
					}
	
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Se movió el armado del FROM a esta parte porque se necesita agregar las tablas de las dimensiones filtradas si es que se aplicó filtros
		//por atributos y se usan dimensiones independientes
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		//@JAPR
		
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"").$aSurvey->SurveyTable.".FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey.$filterJoin;
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Total de Registros
		$numRows = $aRS->RecordCount();
		
		//Verificar si existen los indices ipp y page en el GET, sino es asi, asignarle valores por default
		/*
		if(!isset($_GET["ipp"]))
		{
			$_GET["ipp"] = 100;
		}
		*/
		
		$_GET["ipp"] = $ipp;
		
		if(!isset($_GET["page"]))
		{
			$_GET["page"] = 1;
		}
		
		if($numRows>0)
		{
			//Creamos el objeto de paginacion
			$anInstance->Pages = new Paginator();
			$anInstance->Pages->items_total = $numRows;
			//$anInstance->Pages->items_per_page = 100;
			//$anInstance->Pages->items_per_page = $_GET["ipp"];
			$anInstance->Pages->items_per_page = $ipp;
			$anInstance->Pages->mid_range = 9;
			$anInstance->Pages->paginate();
			
			//Agregamos el elemento LIMIT al SELECT para obtener los registros que se requieren
			$sql .= $anInstance->Pages->limit;
	
			$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
	
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				$anInstance->Pages->current_num_items = $aRS->RecordCount();
			}
		}
		
		//@JAPR 2013-05-24: Validado que no trate de obtener todos los valores en la colección de reportes para ciertas cuentas (temporal)
		$blnGetValues = true;
		if (trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0464" || trim($_SESSION["PABITAM_RepositoryName"])=="fbm_bmd_0336" || $anInstance->NumQuestionFields == 0) {
			$blnGetValues = false;
		}
		//@JAPR
		$arrFolios=array();
		$arrFolios2=array();
		while (!$aRS->EOF)
		{
			//@JAPR 2013-05-24: Validado que no trate de obtener todos los valores en la colección de reportes para ciertas cuentas (temporal)
			$anInstance->Collection[] = BITAMReport::NewInstanceFromRS($anInstance->Repository, $aSurveyID, $aRS, $blnGetValues);
			//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
			//Para los comentarios
			$arrFolios[(int)$aRS->fields["factkey"]]=(int)$aRS->fields["factkey"];
			//Para las imagenes
			$arrFolios2[(int)$aRS->fields["factkeydimval"]]=(int)$aRS->fields["factkeydimval"];
			//@JAPR
			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}

		global $hasPic;
		$hasPic = $aSurvey->getAnswerWithPictures($arrFolios2);
		global $hasDocument;
		$hasDocument = $aSurvey->getAnswerWithDocuments($arrFolios2);
		global $hasComment;
		$hasComment = $aSurvey->getAnswerWithComments($arrFolios2);

		return $anInstance;
	}
	
	static function fieldsToObject ($fields)
	{
		$objFields = new stdClass();
		
		$fieldNames = array();
		
		foreach ($fields as $fieldName => $value)
		{
			if(!isset($fieldNames[strtolower($fieldName)]))
			{
				$fieldNames[strtolower($fieldName)] = $value;
				$objFields->$fieldName = $value;
				
				if($fieldName=="surveydate" && $value!="")
				{
					$dateAndTime = explode(" ",$value);
					$fieldNames["dateid"] = $dateAndTime[0]; //Fecha
					$objFields->dateid = $dateAndTime[0];
					$fieldNames["timeid"] = $dateAndTime[1]; //Hora
					$objFields->timeid = $dateAndTime[1];
				}
			}
		}
		
		return $objFields;
	}
	/*@AAL: 22/06/2015: Modificado, ahora se mostraran solo cuatro campos: Fecha, hora, forma y usuario
	los cuales seran tomados de la tabla SI_SV_SurveyLog*/
	//Se agregó el parámetro $surveyDestinationLogIDs para agregar el filtro por el id SurveyDestinationLogID de la tabla si_sv_surveydestinationlog
	static function NewInstanceToFormsLog($aRepository, $searchString = "", $startDate = "", $endDate = "", $GroupID = "", $UserID = "",  $aSurveyID = "", $FilterDate = "", $ipp=100, $surveyDestinationLogIDs="", $fromProcessDataDest=false)
	{
		global $TypeID;
		$anInstance = new BITAMReportCollection($aRepository, 0, false);
		
		$where = "";
		if(trim($FilterDate) != ""){
			switch ($FilterDate) {
				case 'T':
					$CurrentDate = date("Y-m-d");
					if ($TypeID == rptSyncs){
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y-%m-%d')";
					}
					else {
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.SurveyDate, '%Y-%m-%d')";
					}
				break;
				case 'W':
					$CurrentDate = date("W");
					$CurrentYear = date("Y");
					if ($TypeID == rptSyncs){
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%v')" . " AND " . $aRepository->DataADOConnection->Quote($CurrentYear) . " = DATE_FORMAT(A.CreationDateID, '%Y')";
					}
					else {
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.SurveyDate, '%v')" . " AND " . $aRepository->DataADOConnection->Quote($CurrentYear) . " = DATE_FORMAT(A.SurveyDate, '%Y')";
					}							
				break;
				case 'M':
					$CurrentDate = date("Y-m");
					if ($TypeID == rptSyncs){
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y-%m')";
					}
					else {
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.SurveyDate, '%Y-%m')";
					}						
				break;
				case 'Y':
					$CurrentDate = date("Y");					
					if ($TypeID == rptSyncs){
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y')";
					}
					else {
						$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.SurveyDate, '%Y')";
					}	
				break;
				default:
				break;
			}			
			
		}else if ($startDate == "" && $endDate == ""){
			//EVEGA (issue:MR9XFH)Se definio que por default muestre lo del dia
			//El filtro de fecha por default NO se aplicará cuando se mande llamar la función desde	processDataDestination.php 
			//para el reproceso de destinos ya que se envia como parametro los IDs especificos que se desean procesar y no es necesario aplicar el filtro de fecha
			if($fromProcessDataDest==false)
			{
			$FilterDate='T';
			$CurrentDate = date("Y-m-d");
				if ($TypeID == rptSyncs){
				
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y-%m-%d')";
				}
				else {
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.SurveyDate, '%Y-%m-%d')";
				}	
			}
		}
		
		if(trim($startDate) != "" && trim($endDate) != "")
		{ 
			$endDate = $endDate . " 23:59:59";			
			if ($TypeID == rptSyncs){
				$where.= "AND A.CreationDateID >= " . $aRepository->DataADOConnection->DBTimeStamp($startDate) . " AND A.CreationDateID <= " . $aRepository->DataADOConnection->DBTimeStamp($endDate);
			}
			else {
				$where.= "AND A.SurveyDate >= " . $aRepository->DataADOConnection->DBTimeStamp($startDate) . " AND A.SurveyDate <= " . $aRepository->DataADOConnection->DBTimeStamp($endDate);
			}	
		}
		if($aSurveyID != "")
		{
			$where .= " AND A.SurveyID IN ({$aSurveyID})";
		}

		
		if($UserID != "" && $UserID!="NA")
		{
			$In = array();
			$Values = explode(",", $UserID);
			foreach ($Values as $val) {
				array_push($In, $aRepository->DataADOConnection->Quote($val));
			}
			$In = implode(",", $In);
			$where .= " AND A.UserID IN ({$In})"; 
		}

		/*	
		if(trim($startDate) != "" && trim($endDate) != "" && $GroupID != "" && $UserID != "" && $UserID != "NA")  {
			print($where);
			die();
		}
		*/	

		//GCRUZ 2015-08-05. Obtener todas las SurveyID existentes
		$aSQL = "SELECT SurveyID,surveyname FROM SI_SV_Survey";
		$allSurveyIDs = array();
		$allSurveyNames=array();
		$aRS = $aRepository->DataADOConnection->Execute($aSQL);
		if ($aRS && $aRS->_numOfRows != 0)
		{
			while (!$aRS->EOF)
			{
				$allSurveyIDs[] = $aRS->fields['SurveyID'];
				//EVEGA 13/08/2015 Todos los names de los surveys para el reporte de sincronizacion
				//GCRUZ 2015-11-02. Escapar comillas.
				$allSurveyNames[$aRS->fields['SurveyID']]= str_replace("\"", "\\\"", $aRS->fields['surveyname']);
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2015-08-05: Validado para que no genere un error en caso de no existir alguna tabla (generalmente por ser una BD nueva)
		//Se removió el die para que continue sin formas
		$allSurveyIDs = implode(',', $allSurveyIDs);
		if ($TypeID == rptEntries) {
			$sql = "SELECT A.NOM_LARGO, A.CUENTA_CORREO, B.CLA_USUARIO, C.CLA_ROL, C.NOM_ROL 
			FROM SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C 
			WHERE A.CLA_USUARIO = B.CLA_USUARIO AND B.CLA_ROL = C.CLA_ROL";
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while($aRS && !$aRS->EOF)
			{
				if(!isset($anInstance->Groups[(int)$aRS->fields["cla_rol"]]))
				{
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]= array();
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["name"]=GetValidJScriptText($aRS->fields["nom_rol"]);
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"]=array();
				}
				//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"][] = $aRS->fields["cuenta_correo"];
				
				//$UserWithAllGroups: Es un arreglo indexado por el correo del usuario y cada elemento contiene otro arreglo 
				//con las claves de gpo a los que pertenece el usuario
				if(!isset($anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]= array();
				}
				//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]][] = (int)$aRS->fields["cla_rol"];								
				
				//ears 2017-01-05 de apoyo para mantener los datos de userid 
				if(!isset($anInstance->UserIDs[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserIDs[$aRS->fields["cuenta_correo"]] = array((int)$aRS->fields["cla_usuario"], (int)$aRS->fields["cla_rol"], (string)$aRS->fields["nom_rol"]);
				}
							
				$aRS->MoveNext();
			}
						
			
			if($GroupID != "")
			{	
				$In = array();//arreglo con los usuarios que pertenecen a los grupos seleccionados en el filtro
				$filterGroupIDs = explode(",", $GroupID);
				foreach ($filterGroupIDs as $gID) 
				{
					if (isset($anInstance->Groups[$gID]))
					{
						foreach ($anInstance->Groups[$gID]["users"] as $email)
						{	
							array_push($In, $aRepository->DataADOConnection->Quote($email));
						}
					}
				}	
						
				$In = implode(",", $In);
				$where .= " AND A.UserID IN ({$In})";
			}
							
										
			$sql = "SELECT UserDimID FROM SI_SV_gblsurveymodel";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_gblsurveymodel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$userDimTable="";
			$userDimID=-1;
			if (!$aRS->EOF)
			{
				$userDimID = (int)$aRS->fields["userdimid"];
				$userDimTable="ridim_".$userDimID;
			}

			if($searchString!="")
			{
				$where.=" AND A.SurveyID IN (SELECT SurveyID FROM SI_SV_Survey WHERE SurveyName LIKE(".$aRepository->DataADOConnection->Quote("%".($searchString)."%")."))";
			}
			
			  
			//@JAPR 2015-07-14: Modificado el query para agrupar y ordenar, además de utilizar OUTER JOIN para evitar registros no visibles por inconsistencia de metadata
			//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
			$strWhere = '';
			if (getMDVersion() >= esvDeleteReports) {
				$strWhere = "AND A.Deleted = 0 ";
			}
			//EVEG si no es administrador el usuario en la bitacora solo se le mostraran sus capturas
			if($_SESSION["PABITAM_UserRole"]!=1)
				$guestUser = $_SESSION['PABITAM_UserID'];
			
			//@JAPR
			$sql = "SELECT A.SurveyDate, A.SurveyID, A.UserID AS UserName, B.RIDIM_{$userDimID}KEY AS UserID 
				FROM SI_SV_SurveyLog A 
					LEFT OUTER JOIN {$userDimTable} B ON A.UserID = B.DSC_{$userDimID} 
					LEFT OUTER JOIN SI_SV_Survey C ON A.SurveyID = C.SurveyID 
				WHERE A.Status = 1 AND SessionID IS NOT NULL AND SessionID <> '' {$strWhere} {$where} ".(($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')."
				GROUP BY A.SurveyDate, A.SurveyID, A.UserID, B.RIDIM_{$userDimID}KEY 
				ORDER BY A.SurveyID, C.SurveyName, A.UserID";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("function NewInstanceToFormsLog Obtener Surveys Bitacora: ", 1, 1, "color:purple;");
				ECHOString($sql, 1, 1, "color:purple;");
			}				

			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				//@JAPR 2015-08-05: Validado para que no genere un error en caso de no existir alguna tabla (generalmente por ser una BD nueva)
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyLog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while($aRS && !$aRS->EOF) {
				//EVEG si el usuario es invitado la variable $guestUser estara seteada y traera el nombre largo del usuario
				if(!isset($guestUser) || $guestUser==$aRS->fields["UserID"]){											
					$aSurveyID = $aRS->fields["SurveyID"];
					$anInstance->Collection[] = BITAMReport::NewInstanceToFormsLogFromRS($aRepository, $aSurveyID, $aRS, $anInstance);
				}
				$aRS->MoveNext();
			}

			
			/*
			$arrSurveyIDsB = array();			
			while($aRS && !$aRS->EOF) {
				//EVEG si el usuario es invitado la variable $guestUser estara seteada y traera el nombre largo del usuario
				if(!isset($guestUser) || $guestUser==$aRS->fields["UserID"]){											
					$arrSurveyIDsB[] = $aRS->fields["SurveyID"];
				}
				$aRS->MoveNext();
			}

			$arrSurveyIDsB = implode(',', $arrSurveyIDsB);
			$arrSurveysLogs = array();

			require_once('eSurveyServiceMod.inc.php');
			$aBITAMConnection = connectToKPIRepository();
			$sql = "SELECT A.SessionID, A.SurveyDate, A.SurveyID, A.UserID as UserName, 
				DATEDIFF(MAX(A.CreationDateID), MIN(A.CreationDateID)) AS Duration, 
				COUNT(A.SurveyID) AS FormsNum, A.TaskStatus AS Fails 
				FROM SI_SV_SurveyTasksLog A 
				WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' 
				AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])
				.$where . (($arrSurveyIDsB != '')?" AND A.SurveyID IN ({$arrSurveyIDsB}) ":'')
				." AND (A.TaskStatus=2 OR A.TaskStatus=3) AND (A.Reprocess IS NULL OR A.Reprocess=0)"
				." GROUP BY A.SessionID, A.UserID ORDER BY A.SessionID";
				
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("function NewInstanceToFormsLog Obtener Surveys Bitacora: ", 1, 1, "color:purple;");
				ECHOString($sql, 1, 1, "color:purple;");
			}				
				
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			while (!$aRS->EOF)
			{		
				if (isset($aRS->fields["sessionid"])) {										
					$aSurveyID = $aRS->fields["SurveyID"];
					$anInstance->Collection[] = BITAMReport::NewInstanceToFormsLogFromRS($aRepository, $aSurveyID, $aRS, $anInstance);
					
				}
				$aRS->MoveNext();
			}			
			*/
			
			/*			
			foreach ($arrSurveysLogs as $aLog) 
			{
				$anInstance->Collection[] = $aLog;
			}			
			*/
			
			if($searchString != "") {
				foreach ($anInstance->Collection as $aKey => $anInstanceLog) {
					$arr = array();					
					array_push($arr, $anInstanceLog->DateID);					
					array_push($arr, $anInstanceLog->TimeID);
					array_push($arr, $allSurveyNames[$anInstanceLog->SurveyID]);
					array_push($arr, $anInstanceLog->UserName);
					$foundString = false;
					foreach ($arr as $value) {
						if (strpos(strtolower($value), strtolower($searchString)) !== false) {
							$foundString = true;
							break;
						}
					}
					if (!$foundString) 
						unset($anInstance->Collection[$aKey]);
				}
			}
					
		//EVEGA Si es para el log de dataDestinations se construye el query	
		}
		//--------------------- DATA DESTINATIONS---------------------------------------------------------------------------------
		else if ($TypeID == rptDataDest) {
			//11Dic2015: Se realizaron correcciones para los filtros por grupos por que no se estaban realizando correctamente
			
			$sql = "SELECT A.NOM_LARGO, A.CUENTA_CORREO, B.CLA_USUARIO, C.CLA_ROL, C.NOM_ROL 
			FROM SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C 
			WHERE A.CLA_USUARIO = B.CLA_USUARIO AND B.CLA_ROL = C.CLA_ROL";
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while($aRS && !$aRS->EOF)
			{
				if(!isset($anInstance->Groups[(int)$aRS->fields["cla_rol"]]))
				{
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]= array();
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["name"]=GetValidJScriptText($aRS->fields["nom_rol"]);
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"]=array();
				} 
				//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"][] = $aRS->fields["cuenta_correo"];
				
				//$UserWithAllGroups: Es un arreglo indexado por el correo del usuario y cada elemento contiene otro arreglo 
				//con las claves de gpo a los que pertenece el usuario
				if(!isset($anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]= array();
				}
			  	//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]][] = (int)$aRS->fields["cla_rol"];

				//ears 2017-01-05 de apoyo para mantener los datos de userid 
				if(!isset($anInstance->UserIDs[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserIDs[$aRS->fields["cuenta_correo"]] = array((int)$aRS->fields["cla_usuario"], (int)$aRS->fields["cla_rol"], (string)$aRS->fields["nom_rol"]);
				}
				
				$aRS->MoveNext();
			}
			
			if($GroupID != "")
			{	
				$In = array();//arreglo con los usuarios que pertenecen a los grupos seleccionados en el filtro
				$filterGroupIDs = explode(",", $GroupID);
				foreach ($filterGroupIDs as $gID) 
				{
					if (isset($anInstance->Groups[$gID]))
					{
						foreach ($anInstance->Groups[$gID]["users"] as $email)
						{	
							array_push($In, $aRepository->DataADOConnection->Quote($email));
						}
					}
				}	
						
				$In = implode(",", $In);
				$where .= " AND A.UserID IN ({$In})";
			}
			
			/*
			if($GroupID != "")
			{	
				$In = array();//arreglo con los usuarios que pertenecen a los grupos seleccionados en el filtro
				$filterGroupIDs = explode(",", $GroupID);
				foreach ($filterGroupIDs as $gID) 
				{
					if (isset($anInstance->Groups[$gID]))
					{
						foreach ($anInstance->Groups[$gID]["users"] as $email)
						{	
							array_push($In, $aRepository->DataADOConnection->Quote($email));
						}
					}
				}	
						
				$In = implode(",", $In);
				if($UserID != "" && $UserID != "NA") {
					$where .= (($In != '')? " OR A.UserID IN ({$In}))" : ')');
				}
				else {
					$where .= (($In != '')? " AND A.UserID IN ({$In})" : ' ');	
				}	
			}
			else {
				if($UserID != "" && $UserID!="NA")
				{
					$where .= ")";
				}	
			}
			*/

			require_once('eSurveyServiceMod.inc.php');
			$aBITAMConnection = connectToKPIRepository();
			
			//Obtiene los IDs de las capturas ya procesadas por el agente
			$arrDestinationLogs=array();
			
			//Obtener los ids de destinos que aún existen para mostrar sólo los errores asociados a destinos aún existentes
			$sqlDest = "SELECT id FROM si_sv_datadestinations";
			$aRSDest = $aRepository->DataADOConnection->Execute($sqlDest);
			if ($aRSDest === false)
			{
				die( translate("Error accessing")." si_sv_datadestinations ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDest);
			}
			$arrDestIDs = array();
			while (!$aRSDest->EOF)
			{
				$arrDestIDs[] = (int) $aRSDest->fields["id"];
				$aRSDest->MoveNext();
			}
			
			$fieldDD = ", B.DestinationID";
			
			if($surveyDestinationLogIDs!="")
			{
				$surveyDestinationLogIDs = " AND B.SurveyDestinationLogID IN (".$surveyDestinationLogIDs.")";
			}
			
			//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strAdditionalFields = '';
			if (getMDVersion() >= esvDataDestErrorCheck) {
				$strAdditionalFields .= ', A.SurveyKey';
			}
			//@JAPR
			//Poner comentario aqui que el SurveyTaskLogID es realmente el SurveyTaskID
			$sql = "SELECT A.SurveyTaskID, A.SurveyDate, A.SurveyID, A.UserID, A.TaskStatus AS Fails,
					B.SurveyDestinationLogID, B.CreationDateID, 
					B.Destination, B.Description, B.`Status` 
					{$fieldDD}{$strAdditionalFields}
				FROM  SI_SV_SurveyTasksLog A
				INNER JOIN SI_SV_SurveyDestinationLog B ON (A.SurveyTaskID=B.SurveyTaskLogID) 
				WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"]).
				(($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'').
				" AND A.SessionID IS NOT NULL AND A.SessionID <> ''".$where.
				" AND B.Status = 1 AND (B.ProcessStatus IS NULL OR B.ProcessStatus=0) ".$surveyDestinationLogIDs.";";
				
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("function NewInstanceToFormsLog Obtener destinos: ", 1, 1, "color:purple;");
				ECHOString($sql, 1, 1, "color:purple;");
			}
			
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog INNER JOIN SI_SV_SurveyDestinationLog B ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				if(in_array($aRS->fields["destinationid"], $arrDestIDs) || $aRS->fields["destinationid"]==-1 || $aRS->fields["destination"]=="Model" || $aRS->fields["destination"]=="Data" )
				{
					$arrDestinationLogs[$aRS->fields["surveydestinationlogid"]] = BITAMReportCollection::fieldsToObject($aRS->fields);
					//Si el tipo de destino es Model entonces DestinationID debe ser -1
					if($arrDestinationLogs[$aRS->fields["surveydestinationlogid"]]->destination=="Model")
					{
						$arrDestinationLogs[$aRS->fields["surveydestinationlogid"]]->destinationid=-1;
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Es un destino Model, destinationid: ".$arrDestinationLogs[$aRS->fields["surveydestinationlogid"]]->destinationid, 1, 1, "color:purple;");
						}
					}
				}
				
				$aRS->MoveNext();
			}
			
			foreach ($arrDestinationLogs as $aLog) 
			{
				$anInstance->Collection[] = $aLog;
			}
			
			if($searchString != "") {
				foreach ($anInstance->Collection as $aKey => $anInstanceLog) {
					$arr = array();
					array_push($arr, $anInstanceLog->dateid);
					array_push($arr, $anInstanceLog->timeid);
					array_push($arr, $allSurveyNames[$anInstanceLog->surveyid]);
					array_push($arr, $anInstanceLog->userid);
					$foundString = false;
					foreach ($arr as $value) {
						if (strpos(strtolower($value), strtolower($searchString)) !== false) {
							$foundString = true;
							break;
						}
					}
					if (!$foundString) 
						unset($anInstance->Collection[$aKey]);
				}
			}
			
			//Si la función se llamó desde processDataDestination.php se retorna la instancia
			if($fromProcessDataDest)
			{
				return $anInstance;
			}
		}else {
			$sql = "SELECT A.NOM_LARGO, A.CUENTA_CORREO, B.CLA_USUARIO, C.CLA_ROL, C.NOM_ROL 
			FROM SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C 
			WHERE A.CLA_USUARIO = B.CLA_USUARIO AND B.CLA_ROL = C.CLA_ROL";
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while($aRS && !$aRS->EOF)
			{
				if(!isset($anInstance->Groups[(int)$aRS->fields["cla_rol"]]))
				{
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]= array();
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["name"]=GetValidJScriptText($aRS->fields["nom_rol"]);
					$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"]=array();
				}
				//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->Groups[(int)$aRS->fields["cla_rol"]]["users"][] = $aRS->fields["cuenta_correo"];
				
				//$UserWithAllGroups: Es un arreglo indexado por el correo del usuario y cada elemento contiene otro arreglo 
				//con las claves de gpo a los que pertenece el usuario
				if(!isset($anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]]= array();
				}
				//El arreglo $anInstance->Groups contendrá otro arreglo con los usuarios que pertenecen a ese grupo
				$anInstance->UserWithAllGroups[$aRS->fields["cuenta_correo"]][] = (int)$aRS->fields["cla_rol"];
				
				
				//ears 2017-01-05 de apoyo para mantener los datos de userid 
				if(!isset($anInstance->UserIDs[$aRS->fields["cuenta_correo"]]))
				{
					$anInstance->UserIDs[$aRS->fields["cuenta_correo"]] = array((int)$aRS->fields["cla_usuario"], (int)$aRS->fields["cla_rol"], (string)$aRS->fields["nom_rol"]);
				}
				
			
				$aRS->MoveNext();
			}

			if($GroupID != "")
			{	
				$In = array();//arreglo con los usuarios que pertenecen a los grupos seleccionados en el filtro
				$filterGroupIDs = explode(",", $GroupID);
				foreach ($filterGroupIDs as $gID) 
				{
					if (isset($anInstance->Groups[$gID]))
					{
						foreach ($anInstance->Groups[$gID]["users"] as $email)
						{	
							array_push($In, $aRepository->DataADOConnection->Quote($email));
						}
					}
				}	
						
				$In = implode(",", $In);
				$where .= " AND A.UserID IN ({$In})";

			}			
			
			
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			require_once('eSurveyServiceMod.inc.php');
			$aBITAMConnection = connectToKPIRepository();
			$sql = "SELECT A.SessionID, A.SurveyID, A.UserID AS UserName, MAX(A.CreationDateID) AS SurveyDate, MAX(A.SurveyDate) AS SurveyDate2,
				DATEDIFF(MAX(A.CreationDateID), MIN(A.CreationDateID)) AS Duration, 
				COUNT(A.SurveyID) AS FormsNum, A.TaskStatus AS Fails 
				FROM SI_SV_SurveyTasksLog A 
				WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' 
				AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])
				.$where . (($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')
				." AND (A.TaskStatus=2 OR A.TaskStatus=3) AND (A.Reprocess IS NULL OR A.Reprocess=0)"
				." GROUP BY A.SessionID, A.UserID ORDER BY A.SessionID";
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);

			/*	
			if(trim($startDate) != "" && trim($endDate) != "" && $GroupID != "" && $UserID != "" && $UserID != "NA")  {
				print($sql);
				die();
			}
			*/

			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$arrSesionsID=array();
			while (!$aRS->EOF)
			{
				$aSessionID = $aRS->fields["SessionID"];
				$aUserID = $aRS->fields["UserName"];
				if (!in_array($aSessionID, $arrSesionsID)){
					$arrSesionsID[]=$aSessionID;
				}
				$aSurveyID = $aRS->fields["SurveyID"];
				//$anInstance->Collection[$aSessionID.$aUserID] = BITAMReport::NewInstanceToLogFromRS($anInstance->Repository, $aSurveyID, $aRS, $anInstance->Groups);
				$anInstance->Collection[$aSessionID.$aUserID] = BITAMReport::NewInstanceToLogFromRS($anInstance->Repository, $aSurveyID, $aRS, $anInstance->UserIDs);
				$aRS->MoveNext();
			}
			//EVEGA Se lanza otro query para traer todas las encuestas deaceurdo al id de sesion
			$sql = "SELECT SessionID,SurveyID,UserID FROM SI_SV_SurveyTasksLog WHERE SessionID IN (".implode(",",$arrSesionsID).")";

			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);

			if ($aRS && $aRS->_numOfRows != 0)
			{
			while (!$aRS->EOF)
			{
				$aSessionID = $aRS->fields["sessionid"];
				$aSurveyID= $aRS->fields["surveyid"];
				$aUserID = $aRS->fields["UserID"];
				if(isset($allSurveyNames[$aSurveyID]) && isset($anInstance->Collection[$aSessionID.$aUserID])){
					$anInstance->Collection[$aSessionID.$aUserID]->surveySync[$aSurveyID]=$allSurveyNames[$aSurveyID];
				}
				$aRS->MoveNext();
			}
			}
			
			$sql = "SELECT A.SessionID, A.UserID, A.SurveyID, COUNT(A.SurveyID) AS FormsNum FROM SI_SV_SurveyTasksLog A WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"]) . $where . (($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')." AND TaskStatus = 2 GROUP BY A.SessionID, A.UserID";
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			while (!$aRS->EOF)
			{

				$aSessionID = $aRS->fields["SessionID"];
				$aUserID = $aRS->fields["UserID"];
				$intNumErrors = $aRS->fields["formsnum"];
				$aReports = $anInstance->Collection[$aSessionID.$aUserID];
				if (!is_null($aReports)) {
					$aReports->Fails = $intNumErrors;
				}
				$aRS->MoveNext();
			}
			
			if($searchString != "") {
				foreach ($anInstance->Collection as $aKey => $anInstanceLog) {
					$arr = array();
					array_push($arr, $anInstanceLog->DateID);
					array_push($arr, $anInstanceLog->TimeID);
					array_push($arr, $anInstanceLog->SurveyName);
					array_push($arr, $anInstanceLog->UserName);
					$foundString = false;
					foreach ($arr as $value) {
						if (strpos(strtolower($value), strtolower($searchString)) !== false) {
							$foundString = true;
						}
					}
					if (!$foundString) 
						unset($anInstance->Collection[$aKey]);
				}
			}
							
			/*			
			//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
			require_once('eSurveyServiceMod.inc.php');
			$aBITAMConnection = connectToKPIRepository();
			$sql = "SELECT A.SessionID, A.SurveyDate, A.SurveyID, A.UserID as UserName, 
				DATEDIFF(MAX(A.CreationDateID), MIN(A.CreationDateID)) AS Duration, 
				COUNT(A.SurveyID) AS FormsNum, A.TaskStatus AS Fails 
				FROM SI_SV_SurveyTasksLog A 
				WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' 
				AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])
				.$where . (($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')
				." AND (A.TaskStatus=2 OR A.TaskStatus=3) AND (A.Reprocess IS NULL OR A.Reprocess=0)"
				." GROUP BY A.SessionID, A.UserID ORDER BY A.SessionID";
				
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("function NewInstanceToFormsLog Obtener Surveys: ", 1, 1, "color:purple;");
				ECHOString($sql, 1, 1, "color:purple;");
			}				
				
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			$arrSessionLogs = array();
			while (!$aRS->EOF)
			{		
				if (isset($aRS->fields["sessionid"])) {					
					$arrSessionLogs[(string)$aRS->fields["sessionid"]] = BITAMReportCollection::fieldsToObject($aRS->fields);
				}
				$aRS->MoveNext();
			}
								
			foreach ($arrSessionLogs as $aLog) 
			{
				$anInstance->Collection[] = $aLog;
			}			
			
			
			if($searchString != "") {
				foreach ($anInstance->Collection as $aKey => $anInstanceLog) {
					$arr = array();					
					array_push($arr, $anInstanceLog->dateid);					
					array_push($arr, $anInstanceLog->timeid);
					array_push($arr, $allSurveyNames[$anInstanceLog->surveyid]);
					array_push($arr, $anInstanceLog->userid);
					$foundString = false;
					foreach ($arr as $value) {
						if (strpos(strtolower($value), strtolower($searchString)) !== false) {
							$foundString = true;
							break;
						}
					}
					if (!$foundString) 
						unset($anInstance->Collection[$aKey]);
				}
			}
			*/
						
						/*
						anteriormente
						
						$sql = "SELECT A.NOM_LARGO, B.CLA_USUARIO, C.CLA_ROL, C.NOM_ROL FROM SI_USUARIO A, SI_Rol_Usuario B, SI_Rol C WHERE A.CLA_USUARIO = B.CLA_USUARIO AND B.CLA_ROL = C.CLA_ROL";
						$aRS = $aRepository->ADOConnection->Execute($sql);
						if ($aRS === false)
						{
							die("(".__METHOD__.") ".translate("Error accessing")." SI_Rol ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						
						while($aRS && !$aRS->EOF)
						{
							if(!isset($anInstance->Groups[$aRS->fields["nom_largo"]])) {
								$anInstance->Groups[strtolower((string)$aRS->fields["nom_largo"])] = array((int)$aRS->fields["cla_usuario"], (int)$aRS->fields["cla_rol"], (string)$aRS->fields["nom_rol"]);
							}		
							$aRS->MoveNext();
						}
							
						if($GroupID != "")
						{ 
							$In = array();
							$Values = explode(",", $GroupID);
							for ($i = 0; $i < count($Values); $i++) { 
								foreach ($anInstance->Groups as $key => $group) {
									if (in_array($Values[$i], $group))						
										array_push($In, $aRepository->DataADOConnection->Quote($key));
								}		
							}	
							$In = implode(",", $In);
							//$where .= " AND A.UserID IN ({$In})";
							$where .= (($In != '')? " OR A.UserID IN ({$In})" : '');
						}
						
						//@JAPR 2015-02-03: Corregido un bug, no se pueden usar referencias al repositorio de KPIOnline desde el DWH
						require_once('eSurveyServiceMod.inc.php');
						$aBITAMConnection = connectToKPIRepository();
						$sql = "SELECT A.SessionID, A.SurveyDate, A.SurveyID, A.UserID AS UserName, 
							DATEDIFF(MAX(A.CreationDateID), MIN(A.CreationDateID)) AS Duration, 
							COUNT(A.SurveyID) AS FormsNum, A.TaskStatus AS Fails 
							FROM SI_SV_SurveyTasksLog A 
							WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' 
							AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])
							.$where . (($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')
							." AND (A.TaskStatus=2 OR A.TaskStatus=3) AND (A.Reprocess IS NULL OR A.Reprocess=0)"
							." GROUP BY A.SessionID, A.UserID ORDER BY A.SessionID";
						$aRS = $aBITAMConnection->ADOConnection->Execute($sql);

						if ($aRS === false)
						{
							die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						$arrSesionsID=array();
						while (!$aRS->EOF)
						{
							$aSessionID = $aRS->fields["SessionID"];
							$aUserID = $aRS->fields["UserName"];
							if (!in_array($aSessionID, $arrSesionsID)){
								$arrSesionsID[]=$aSessionID;
							}
							$aSurveyID = $aRS->fields["SurveyID"];
							$anInstance->Collection[$aSessionID.$aUserID] = BITAMReport::NewInstanceToLogFromRS($anInstance->Repository, $aSurveyID, $aRS, $anInstance->Groups);
							$aRS->MoveNext();
						}
						//EVEGA Se lanza otro query para traer todas las encuestas deaceurdo al id de sesion
						$sql = "SELECT SessionID,SurveyID,UserID FROM SI_SV_SurveyTasksLog WHERE SessionID IN (".implode(",",$arrSesionsID).")";

						$aRS = $aBITAMConnection->ADOConnection->Execute($sql);

						if ($aRS && $aRS->_numOfRows != 0)
						{
						while (!$aRS->EOF)
						{
							$aSessionID = $aRS->fields["sessionid"];
							$aSurveyID= $aRS->fields["surveyid"];
							$aUserID = $aRS->fields["UserID"];
							if(isset($allSurveyNames[$aSurveyID]) && isset($anInstance->Collection[$aSessionID.$aUserID])){
								$anInstance->Collection[$aSessionID.$aUserID]->surveySync[$aSurveyID]=$allSurveyNames[$aSurveyID];
							}
							$aRS->MoveNext();
						}
						}
						
						$sql = "SELECT A.SessionID, A.UserID, A.SurveyID, COUNT(A.SurveyID) AS FormsNum FROM SI_SV_SurveyTasksLog A WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"]) . $where . (($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')." AND TaskStatus = 2 GROUP BY A.SessionID, A.UserID";
						$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
						while (!$aRS->EOF)
						{

							$aSessionID = $aRS->fields["SessionID"];
							$aUserID = $aRS->fields["UserID"];
							$intNumErrors = $aRS->fields["formsnum"];
							$aReports = $anInstance->Collection[$aSessionID.$aUserID];
							if (!is_null($aReports)) {
								$aReports->Fails = $intNumErrors;
							}
							$aRS->MoveNext();
						}
						
						if($searchString != "") {
							foreach ($anInstance->Collection as $aKey => $anInstanceLog) {
								$arr = array();
								array_push($arr, $anInstanceLog->DateID);
								array_push($arr, $anInstanceLog->TimeID);
								array_push($arr, $anInstanceLog->SurveyName);
								array_push($arr, $anInstanceLog->UserName);
								$foundString = false;
								foreach ($arr as $value) {
									if (strpos(strtolower($value), strtolower($searchString)) !== false) {
										$foundString = true;
									}
								}
								if (!$foundString) 
									unset($anInstance->Collection[$aKey]);
							}
						}
						*/
		}
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<!-- Inician librerias de filtro de Ebavel>-->
		 <link type="text/css" rel="stylesheet" href="css/eBavel.css"/>
		
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery.jsperanto.js"></script>
		<script type="text/javascript" src="js/eBavel/bitamApp.js"></script>
		<script type="text/javascript" src="js/eBavel/bitamstorage.js"></script>
		<script type="text/javascript" src="js/eBavel/databases.js"></script>
		<script type="text/javascript" src="js/eBavel/section.class.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery.widgets.js"></script>
		<script type="text/javascript" src="js/eBavel/date.js"></script>
		<script type="text/javascript" src="js/jsondefinition/bitamAppFormsJsonDefinition.js"></script>
		<script type="text/javascript" src="js/jsondefinition/agendasJsonDefinition.js"></script>
		<script type="text/javascript" src="js/jquery.widgets.js"></script>
		<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
		<!-- GCRUZ 2016-07-29 Librería para generar marcadores con más de 1 caracter en la etiqueta >-->
		<script src="https://cdn.rawgit.com/googlemaps/v3-utility-library/master/markerwithlabel/src/markerwithlabel.js"></script>
	
		<!-- Termina librerias de filtro de Ebavel style="height: 100%; margin: 0px;" -->
		</head>

		<body style="width: 100%; height: 100%; margin: 0px; overflow: hidden;" onload="doOnload()">
		<div id="boxFilter" style="float:left; width: 200px; height: 100%; overflow-y: scroll; overflow-x: hidden; margin-top: 5px; background:white;border-right: 1px solid lightgray"></div>
		<style type="text/css">
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				text-transform: none !important;
			}
			/*div.dhx_toolbar_dhx_terrace div.dhxtoolbar_float_left {
  				float: right !important;
			}*/
			.labels {
				color: blue;
				background-color: white;
				font-family:"Lucida Grande", "Arial", sans-serif;
				font-size: 10px;
				text-align: center;
				width: 20px;
				white-space: nowrap;
			}
		</style>

		<script language="JavaScript">
		var Content;
		var ContentA;
		var ContentB;
		var objLogGrid;
		var Toolbar = undefined;
		var objRowsSelected = new Object();
		//Funciones
		var fnRefreshPage = undefined;
		//Diálogos
		var objWindows;
		var objDialog;
		
		delete Array.prototype.contains;
		bitamApp.getAppFormsDefinition();
		$.extend($, { t: function (word) {
				return $.jsperanto.translate(word);
			}
		});

		function doOnload () {
			var FilterDate = ("<?= (isset($_POST['FilterDate']) ? $_POST['FilterDate'] : "") ?>");
			if (FilterDate == "T" || FilterDate == "W" || FilterDate == "M" || FilterDate == "Y") {
				var Btns = $(".dhxtoolbar_text");
				for(var i = 0; i < Btns.length; i++){
					if(Btns[i].innerText == Toolbar.getItemText(FilterDate)){
						$(".dhxtoolbar_text").parent()[i].setAttribute("class", "dhx_toolbar_btn dhxtoolbar_btn_over");
						break;
					}
				}
			}
		}

		function applyFilterAttrib (filter, oldTags, descrTag, allForms)
		{
			if (filter) {
				 
				for (var fkey in filter) {

					//Search for Input String
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}
					
					//Search for Date										
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
						//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
						//Modificada la validación, ya que cuando se presionaba el botón de Search sin texto escrito, el filter venía con un elemento Date que era un array cuyo índice
						//0 era undefined, lo cual es correcto en el contexto normal de la ventana si se está limpiando el filtro de fecha con el botón para tal fin, pero eso implicaba
						//que debió haber existido una fecha previamente, en cuyo caso los botones de filtrado automático de fechas no habrían estado asignados, pero con este cambio
						//para realizar un refresh después de eliminar capturas, se invoca al click del filtro de fecha (independientemente de si había o no una) y en ese caso pudiera si
						//haber botones de filtro de fecha automáticos que habría que respetar, así que se validarán si no había fecha asignada en el filtro
						else {
							Toolbar.forEachItem(function(itemId){
								if(Toolbar.getItemState(itemId)){
									frmAttribFilter.FilterDate.value = itemId;
								}
							});
						}
					//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
					//Modificada la validación, ya que cuando se presionaba el botón de Search sin texto escrito, el filter venía con un elemento Date que era un array cuyo índice
					//0 era undefined, lo cual es realmente inválido porque si fuera un Date real traería un rango de fechas, así que si ocurre ese caso también deberá heredar el
					//filtro automático de fechas definido con los botones de la toolbar
					}else if(!filter['Date'] || ($.isArray(filter['Date']) && !filter['Date'][0])){
						//EVEGA Buscamos en si uno de los botones de periodo estan activados
						Toolbar.forEachItem(function(itemId){
						    if(Toolbar.getItemState(itemId)){
						    	frmAttribFilter.FilterDate.value = itemId;
						    }
						});
					}
					
					//Search for Group of Users
					if (fkey == 'Groups') {
						var filterGroups = filter['Groups'];
						var Groups = new Array();
						for (var ukey in filterGroups) {
							Groups.push(filterGroups[ukey]);
						}
						frmAttribFilter.GroupID.value = Groups.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldGroupsTags.value = oldTags;
					}	
					//Search for Users
					if (fkey == 'Users') {
						var filterUsers = filter['Users'];
						var Users = new Array();
						for (var ukey in filterUsers) {
							Users.push(filterUsers[ukey]);
						}
						frmAttribFilter.userID.value = Users.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldUserTags.value = oldTags;
					}
					//Search for Forms
					if (fkey =='Forms'){
						var filterForms = filter['Forms'];
						var Forms = new Array();
						for (var ukey in filterForms) {
							Forms.push(filterForms[ukey]);
						}
						frmAttribFilter.SurveyID.value = Forms.join(',');
						//frmAttribFilter.oldFormsTags.value = allForms;
						//frmAttribFilter.SurveyID.value = parseInt(filter['Forms'][0], 10);
					}
					frmAttribFilter.oldFormsTags.value = allForms;
				}
			} 
			else {
				/*var objCatalogID = document.getElementById("CatalogID");
				frmAttribFilter.catalogID.value = objCatalogID.value;*/

				var objStartDate = document.getElementById("CaptureStartDate");
				frmAttribFilter.startDate.value = objStartDate.value;

				var objEndDate = document.getElementById("CaptureEndDate");
				frmAttribFilter.endDate.value = objEndDate.value;
				
				var objUserID = document.getElementById("UserID");
				frmAttribFilter.userID.value = objUserID.value;
				
				var objIpp = document.getElementById("ipp");
				frmAttribFilter.ipp.value = objIpp.value;
			}

			frmAttribFilter.submit();
		}

		function addDescriptionTag (tagName, tagDescription, element) {
			var realTagName = tagName;
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = 1;
			} 
			else
				theTag.elements[elementID].num = (theTag.elements[elementID].num+1);
		}
		
		function addDescriptionTagByCount (tagName, tagDescription, element) {
			var realTagName = tagName;
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
			 	theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = element.count;
			}
		}
		
		var descriptionTags = {};
		
		var searchString = '';
		<? if (isset($_POST["searchString"]) && $_POST["searchString"] != '') { ?>
			searchString = '<?= $_POST["searchString"] ?>';
		<? } ?>
		
		var filterDate = '';
		<? if ((isset($_POST["startDate"]) && $_POST["startDate"] != '') && (isset($_POST["endDate"]) && $_POST["endDate"] != '')) { ?>
			filterDate = '<?= $_POST["startDate"] ?>@<?= $_POST["endDate"] ?>';
		<? } 
		
		if($TypeID != rptDataDest)
		{			
			/*
			foreach ($anInstance->Collection as $aReports)
			{
		?>
			addDescriptionTag('Groups','Groups',{id: <?= $aReports->GroupID ?>,  name:"<?= (isset($aReports->GroupName) && $aReports->GroupName!='')? $aReports->GroupName:"(".translate('None').")" ?>"});
			addDescriptionTag('Users','Users' , {id: "<?= $aReports->UserName ?>",   name:"<?= $aReports->UserName ?>" });
			<?if(isset($aReports->surveySync)){
				foreach ($aReports->surveySync as $aSurveyIDSync =>$aSurveyNameSync){?>
					addDescriptionTag('Forms', 'Forms', {id: <?= $aSurveyIDSync ?>, name:"<?= $aSurveyNameSync ?>"});
				<?}?>
			<?}else{?>	
				addDescriptionTag('Forms', 'Forms', {id: <?= $aReports->SurveyID ?>, name:"<?= $aReports->SurveyName ?>"});
			<?	}
			}
			*/
			$tagForms = array();
			$tagUsers = array();
			$tagGroups = array();
			
			foreach ($anInstance->Collection as $aReports)
			{
				
				if(isset($tagForms[$aReports->SurveyID]))
				{
					$tagForms[$aReports->SurveyID]["count"]=$tagForms[$aReports->SurveyID]["count"]+1;
				} 
				else
				{
					$tagForms[$aReports->SurveyID]=array();
					$tagForms[$aReports->SurveyID]["name"]=$allSurveyNames[$aReports->SurveyID];
					$tagForms[$aReports->SurveyID]["count"]=1;
				}
				
				if(isset($tagUsers[$aReports->UserName]))
				{
					$tagUsers[$aReports->UserName]["count"]=$tagUsers[$aReports->UserName]["count"]+1;
				}
				else
				{
					$tagUsers[$aReports->UserName]=array();
					$tagUsers[$aReports->UserName]["name"]=$aReports->UserName;
					$tagUsers[$aReports->UserName]["count"]=1;
				}
				
			}
			
			foreach($tagUsers as $userID => $aUser)
			{
				//Recorrer los grupos a los que pertenece el usuario para agregarlos al arreglo de grupos que se desplegará en el filtro
				//11Abr2016: Validar que sólo se haga cuando el usuario tiene algún grupo asignado, 
				//Si no se hacía esta validación se generaba error en PHP y la pantalla no se terminaba de cargar
				if(isset($anInstance->UserWithAllGroups[$userID]))
				{	
					foreach($anInstance->UserWithAllGroups[$userID] as $groupID)
					{
						if(isset($tagGroups[$groupID]))
						{
							$tagGroups[$groupID]["count"]=$tagGroups[$groupID]["count"]+$aUser["count"];
						}
						else
						{
							$tagGroups[$groupID]=array();
							$tagGroups[$groupID]["name"]=$anInstance->Groups[$groupID]["name"];
							$tagGroups[$groupID]["count"]=$aUser["count"];
						}
					}
				}
			}

			foreach ($tagGroups as $groupID => $aGroup)
			{
?>
				addDescriptionTagByCount('Groups', 'Groups', {id: <?=$groupID?>, name:"<?= $aGroup["name"]?>", count:<?=(int)$aGroup["count"]?>});
<?
			}
			
			foreach ($tagUsers as $userID => $aUser)
			{
?>
				addDescriptionTagByCount('Users', 'Users', {id: "<?=$userID?>", name:"<?= $aUser["name"]?>", count:<?= (int)$aUser["count"]?>});
<?
			}
			
			foreach ($tagForms as $formID => $aForm)
			{
?>
				addDescriptionTagByCount('Forms', 'Forms', {id: <?=$formID?>, name:"<?= $aForm["name"]?>", count:<?= (int)$aForm["count"]?>});
<?
			}
			
		}			
		else
		{
			$tagForms = array();
			$tagUsers = array();
			$tagGroups = array();
			
			foreach ($anInstance->Collection as $aReports)
			{
				
				if(isset($tagForms[$aReports->surveyid]))
				{
					$tagForms[$aReports->surveyid]["count"]=$tagForms[$aReports->surveyid]["count"]+1;
				} 
				else
				{
					$tagForms[$aReports->surveyid]=array();
					$tagForms[$aReports->surveyid]["name"]=$allSurveyNames[$aReports->surveyid];
					$tagForms[$aReports->surveyid]["count"]=1;
				}
				
				if(isset($tagUsers[$aReports->userid]))
				{
					$tagUsers[$aReports->userid]["count"]=$tagUsers[$aReports->userid]["count"]+1;
				}
				else
				{
					$tagUsers[$aReports->userid]=array();
					$tagUsers[$aReports->userid]["name"]=$aReports->userid;
					$tagUsers[$aReports->userid]["count"]=1;
				}
				
			}
			
			foreach($tagUsers as $userID => $aUser)
			{
				//Recorrer los grupos a los que pertenece el usuario para agregarlos al arreglo de grupos que se desplegará en el filtro
				//11Abr2016: Validar que sólo se haga cuando el usuario tiene algún grupo asignado, 
				//Si no se hacía esta validación se generaba error en PHP y la pantalla no se terminaba de cargar
				if(isset($anInstance->UserWithAllGroups[$userID]))
				{	
					foreach($anInstance->UserWithAllGroups[$userID] as $groupID)
					{
						if(isset($tagGroups[$groupID]))
						{
							$tagGroups[$groupID]["count"]=$tagGroups[$groupID]["count"]+$aUser["count"];
						}
						else
						{
							$tagGroups[$groupID]=array();
							$tagGroups[$groupID]["name"]=$anInstance->Groups[$groupID]["name"];
							$tagGroups[$groupID]["count"]=$aUser["count"];
						}
					}
				}
			}

			foreach ($tagGroups as $groupID => $aGroup)
			{
?>
				addDescriptionTagByCount('Groups', 'Groups', {id: <?=$groupID?>, name:"<?= $aGroup["name"]?>", count:<?=(int)$aGroup["count"]?>});
<?
			}
			
			foreach ($tagUsers as $userID => $aUser)
			{
?>
				addDescriptionTagByCount('Users', 'Users', {id: "<?=$userID?>", name:"<?= $aUser["name"]?>", count:<?= (int)$aUser["count"]?>});
<?
			}
			
			foreach ($tagForms as $formID => $aForm)
			{
?>
				addDescriptionTagByCount('Forms', 'Forms', {id: <?=$formID?>, name:"<?= $aForm["name"]?>", count:<?= (int)$aForm["count"]?>});
<?
			}
		}	
		
		?>
		
		var oldGroupsTags = JSON.parse(<?= json_encode((isset($_POST['oldGroupsTags']) && $_POST['oldGroupsTags'] != '') ? $_POST['oldGroupsTags'] : '{}') ?>);
		var oldUserTags = JSON.parse(<?= json_encode((isset($_POST['oldUserTags']) && $_POST['oldUserTags'] != '') ? $_POST['oldUserTags'] : '{}') ?>);
		var oldFormsTags = JSON.parse(<?= json_encode((isset($_POST['oldFormsTags']) && $_POST['oldFormsTags'] != '') ? $_POST['oldFormsTags'] : '{}') ?>);
		
		var checkedElemenTags = {};

		/*@AAL 24/04/2015: agregado para identificar si se trata de una agenda o AgendaScheduler y mostrar el filtro fecha,
		la cual solo debe de mostrarse en agendas, la validación se muestra em el archivo jquery.widgets.js*/
		var isAgenda = true;

		function initBoxFilter () {
			var checkedValues = ("<?= (isset($_POST['GroupID']) ? $_POST['GroupID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Groups'] = checkedValues.split(',');
				$.each(checkedElemenTags['Groups'], function (index, value) {
					value = ('' + value);
				});
			}

			var checkedValues = ("<?= (isset($_POST['userID']) ? $_POST['userID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Users'] = checkedValues.split(',');
				$.each(checkedElemenTags['Users'], function (index, value) {
					value = ('' + value);
				});
			}

			var checkedValues = ("<?= (isset($_POST['SurveyID']) ? $_POST['SurveyID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Forms'] = checkedValues.split(',');
				$.each(checkedElemenTags['Forms'], function (index, value) {
					value = ('' + value);
				});
			}
			
			if (!$.isEmptyObject(oldGroupsTags) && descriptionTags['Groups'] && descriptionTags['Groups'].elements) {
				$.extend(descriptionTags['Groups'].elements, oldGroupsTags);
			}
			if (!$.isEmptyObject(oldUserTags) && descriptionTags['Users'] && descriptionTags['Users'].elements) {
				//GCRUZ 2015-09-08. Duplicidad de grupos en bitácora. Issue: 75O07A
				//$.extend(descriptionTags['Users'].elements, oldUserTags);
			}
			if (!$.isEmptyObject(oldFormsTags) && descriptionTags['Forms'] && descriptionTags['Forms'].elements) {
				//$.extend(descriptionTags['Forms'].elements, oldFormsTags);
			}

			//GCRUZ 2015-10-15. Indicar un orden específico a los elementos del filtro
			for (var dkey in descriptionTags)
			{
				if (!descriptionTags[dkey].order) {
					descriptionTags[dkey].order = [];
					var thisTagOrder = {};
					for (var elkey in descriptionTags[dkey].elements)
					{
						thisTagOrder[descriptionTags[dkey].elements[elkey].id] = elkey;
					}
					var arrTagsOrder = Object.keys(thisTagOrder);
					arrTagsOrder.sort();
					var i=0;
					for (var descOrder in arrTagsOrder)
					{
						descriptionTags[dkey].order[i] = thisTagOrder[arrTagsOrder[descOrder]];
						i++;
					}
				}
			}

			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate
			};
			//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
			//JAPR 2016-08-25: Dado a que se necesita permitir invocar al submit de este widget (submitFilter) pero desde fuera, y el submit lo que hace es invocar a una función
			//de fuera que se pasa como parámetro o aquí mismo se obtiene su referencia global (applyFilterAttrib), a la cual le manda como primeros 3 parámetros el conjunto de
			//elementos marcados del Widget, para no repetir el código que analiza la selección del Widget, se verificará si en options se recibió una función "setRefreshFn" la cual
			//recibirá como parámetro precisamente a la función submitFilter para permitir invocarla desde fuera, ya sea especificando un objeto que simule al event de uno de los
			//componentes del widget, o no mandando nada con lo cual se simulará que se presionó el botón de Search, y el único uso de esta función será simular un refresh completo
			//de la ventana tal como lo haría el widget con su estado actual
			options.setRefreshFn = function(oWidgetSubmitFn) {
				if (oWidgetSubmitFn) {
					fnRefreshPage = function() {
						oWidgetSubmitFn();
					}
				}
			};
			//JAPR
			
			if ($.isEmptyObject($('#boxFilter').data())){
				$('#boxFilter').boxfilter(options);
				if(options.filterDate != '')
					$('.tagsFecha .clear.ico').show();
			}
			else 
				$('#boxFilter').boxfilter('update');
		}
		
		try { 
			initBoxFilter(); 
		} catch (e) { 
			alert('Search filter error: '+e);
		}

		var Content = new dhtmlXLayoutObject({
	            parent: document.body,
    			pattern: "2U"
	        });
		var toolBarItems=[];
		var eFormsMDVersion = <?=getMDVersion()?>;
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>

		toolBarItems.push({id: "T", type: "buttonTwoState", text: "<?=translate('Today')?>", img: "date_day.png"});
		toolBarItems.push({id: "W", type: "buttonTwoState", text: "<?=translate('This Week')?>", img: "date_week.png"});
		toolBarItems.push({id: "M", type: "buttonTwoState", text: "<?=translate('This Month')?>", img: "date_month.png"});
		toolBarItems.push({id: "Y", type: "buttonTwoState", text: "<?=translate('This Year')?>", img: "date_year.png"});
		<?if($TypeID != rptSyncs){?>
			toolBarItems.push({id: "remove", type: "button", text: "<?=translate('Edit')?>", img: "edit.png"});
			toolBarItems.push({id: "removeCancel", type: "button", text: "<?=translate('Cancel')?>", img: "edit.png"});
		<?}?>
		toolBarItems.push({id: "NA", type: "button", text: "<?=translate('Button')?>", img: ""});
		
		<?if($TypeID == rptEntries){?>
			toolBarItems.push({id: "D", type: "button", text: "<?=translate('Zip of documents')?>", img: "doc_zip.png"});
			toolBarItems.push({id: "Z", type: "button", text: "<?=translate('Zip of images')?>", img: "img_zip.png"});						
			toolBarItems.push({id: "P", type: "button", text: "<?=translate('Powerpoint of images')?>", img: "img_ppt.png"});
			toolBarItems.push({id: "E", type: "button", text: "<?=translate('Excel')?>", img: "csv.png"});
			toolBarItems.push({id: "C", type: "button", text: "<?=translate('CSV')?>", img: "csv2.png"});
			toolBarItems.push({id: "SP", type: "button", text: "<?=translate('SPSS')?>", img: "spss18.png"});
		<?}?>
		<?if($TypeID == rptDataDest){?>
			toolBarItems.push({id: "DD", type: "button", text: "<?=translate('Process Data Destination')?>", img: "processDD.PNG"});
			toolBarItems.push({id: "SA", type: "button", text: "<?=translate('Select All')?>", img: "processDD.PNG"});
			toolBarItems.push({id: "CS", type: "button", text: "<?=translate('Clear Selection')?>", img: "processDD.PNG"});
		<?}?>
		
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>
		//@ears 2016-07-20 se añade botón el cual se ocultará posteriormente como truco para que los botones Today, This Week, This Month, This Year se 
		//muevan hacia la izq (pantalla)
		/*<?if($TypeID == rptSyncs){?>
			toolBarItems.push({id: "NA", type: "button", text: "<?=translate('Button')?>", img: ""});
		<?}?>*/
		
		Toolbar = Content.attachToolbar({
			icons_path: "images/",
			items:toolBarItems
		});
		
		//EVEGA verificamos si se esta filtrando por tiempo seteamos el buttonTwoState en true
		<?if(trim($FilterDate) != ""){?>
		Toolbar.setItemState("<?=$FilterDate?>", true);
		<?}?>
		
		//Divide los botones del toolbar
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>
		/*<?if( $TypeID == rptEntries){?>			
			Toolbar.addSpacer('GM');
		<?}?>
		
		<?if( $TypeID == rptDataDest){?>
			Toolbar.addSpacer('CS');
		<?}?>
		
		<?if( $TypeID == rptSyncs){?>
			//@ears 2016-07-20 se mueve hacia la derecha y se oculta
			Toolbar.addSpacer('NA');
			Toolbar.hideItem('NA');
		<?}?>
		*/
		Toolbar.addSpacer('NA');
		Toolbar.hideItem('NA');
		Toolbar.hideItem('removeCancel');
		//@JAPR
		
		<?if (getMDVersion() < esvEntryMap) {?>
			Toolbar.hideItem('GM');
		<?}?>
		
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>
		//Ajusta los botones de filtrado por fecha
		//$( ".dhxtoolbar_float_right" ).attr("style", "position: absolute; margin: 0px 210px 0px;");
		
		//EVEGA Se agrego el escucha del evento para los buttonTwoState (filtros de tiempo)
		Toolbar.attachEvent("onStateChange", function(id) {
			Toolbar.forEachItem(function(itemId){
			    // your code here
			    if(id!=itemId && Toolbar.getItemState(itemId)){
			    	Toolbar.setItemState(itemId, false);
			    }
			});

			FilterToDate(id);
		});
		
		Toolbar.attachEvent("onClick", function(id) {
			//Para dejar seleccionado el elemento clikeado
			//$(".dhx_toolbar_btn").removeClass('dhxtoolbar_btn_def').addClass('dhxtoolbar_btn_over');
			//Toolbar.setItemState(id, !Toolbar.getItemState(id));
			
			//@AAL 29/07/2015 si se dio click al boton con Id Z,P,E entonces hay que buscar cuatas formas estan filtradas y y tambien ver si hay filtro por fecha.
			//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
			//GCRUZ 2015-08-27 Filtro de búsqueda de formas
			var SurveyIDs = [], startDate = "", endDate = "", UserID = "", GroupID = "", DateTwoState = "", searchForm = "";
			if (id == 'D' || id == 'Z' || id == 'P' || id == 'E' || id == 'C' || id == "SP"  || id == 'GM') {
				//Buscamos para todas las formas seleccionadas
				$el = $("li.tagsForms").find('input');
				$el.each(function(index) {
					if($(this)[0].checked)
						SurveyIDs.push($(this).attr('data-description-id'));
				});
				
				//verificamos si hemos filtrado por estartDate y endDate
				var strDate = $("input[data-fieldname='date']")[0].value;
				if (strDate != "") {
					strDate = strDate.split('@');
					startDate = strDate[0];
					endDate = strDate[1];
				}
				
				//Buscamos si hemos seleccionado un usuario y obtenemos su Email
				$el = $("li.tagsUsers").find('input');
				$el.each(function(index) {
					if($(this)[0].checked)
						UserID = $(this).attr('data-description-id');
				});
				//GCRUZ 2015-10-14. Buscar si se ha seleccionado un grupo
				$el = $("li.tagsGroups").find('input');
				$el.each(function(index) {
					if($(this)[0].checked)
						GroupID = $(this).attr('data-description-id');
				});
				//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
				if (this.getItemState('T'))
				{
					DateTwoState = 'T';
				}
				if (this.getItemState('W'))
				{
					DateTwoState = 'W';
				}
				if (this.getItemState('M'))
				{
					DateTwoState = 'M';
				}
				if (this.getItemState('Y'))
				{
					DateTwoState = 'Y';
				}
				//GCRUZ 2015-08-27 Filtro de búsqueda de formas
				if ($("#Search")[0].value != 'search')
				{
					searchForm = $("#Search")[0].value;
				}
			}
			
			//@AAL 29/07/2015 si se dio click al boton con Id Z,P,E entonces se trata de alguna descarga ya se de imagenes o datos
			//Por default son los filtros relacionados a Today, Week, Month and Year.
			//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
			//GCRUZ 2015-08-27 Filtro de búsqueda de formas
			switch (id) {
				case 'Z':     //UeserID = Email
					zipReport(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID);
					break;
				case 'P':
					exportPPTX(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID);
					break;
				case 'E':
					exportReport(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID, 'XLS');
					break;
				case 'C':
					exportReport(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID, 'CSV');
					break;
				case 'SP':
					exportToSPSS(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID);
					break;
				case 'D':
					//@ears 2016-07-15 se añade opción para Exportar Documentos (documento,audio,video)
					zipReportDoc(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID);
					break;
				case 'DD':
				{
					processDataDestination();
					break;
				}
				case 'SA':
				{
					objLogGrid.selectAll();
					break;
				}
				case 'CS':
				{
					objLogGrid.clearSelection();
					break;
				}
				//GCRUZ 2016-07-27. Mapa google de posiciones
				case 'GM':
				{
					showEntryGoogleMap(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID);
					break;
				}
				//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
				case "remove":
				{
					doRemoveItems();
					break;
				}
				case "removeCancel":
				{
					doCancelRemoveItems();
					break;
				}
				//JAPR
				default:
					break;
			}
		});
		//Add Style to Label Forms Log
		//$( ".dhx_toolbar_text" ).attr("style", "color: #285768; font: bold 20px arial,sans-serif;");
		
		$("#Search").attr("placeholder", "<?=translate('Search')?>");
		$("li.titleFecha").text("<?=translate('Date')?>");
		$("#CustomDateRange").text("<?=translate('Custom date range')?>");
		$("#From").text("<?=translate('From')?>");
		$("#To").text("<?=translate('To')?>");

		$("li.titleGroups").text("<?=translate('Groups')?>");
		$("li.titleUsers").text("<?=translate('Users')?>");
		$("li.titleForms").text("<?=translate('Forms')?>");
		
		var BoxFilter = Content.cells("a").attachLayout("1C");
		BoxFilter.cells("a").hideHeader();
		BoxFilter.cells("a").setWidth(200);
		Content.cells("a").fixSize(true, true); //Bloque el Rezise del Content a);
		BoxFilter.cells("a").attachObject("boxFilter");
		
		//JAPR 2015-08-08: Corregido un bug, el diálogo para mostrar un reporte utilizaba una variable con el mismo nombre que esta para el grid, así que se sobrescribía la instancia
		//y causaba que al ver un reporte, la lista de logs ya no funcionara correctamente y no pudiera mostrar los reportes siguientes sino una ventana en blanco
        var objLogGrid = Content.cells("b").attachGrid();
        Content.cells("b").hideHeader();
        Content.cells("b").fixSize(true, true); //Bloque el Rezise del Content b)
        
        if(<?=$TypeID?> == <?=rptEntries?>){
        	objLogGrid.setHeader("<?=translate('Date')?>,<?=translate('Time')?>,<?=translate('Survey')?>,<?=translate('User')?>, <?=translate('Map')?>, <?=translate('View PDF')?>",null,["text-align:left;","text-align:left;", "text-align:left;", "text-align:left;", "text-align:center", "text-align:center"]);
        	objLogGrid.setColAlign("left,left,left,left,center,center");
        	objLogGrid.setColTypes("ro,ro,ro,ro,img,img");
			objLogGrid.setColSorting("date,date,str,str,na,na");
			objLogGrid.setInitWidths("100,100,*,*,100,100");
			objLogGrid.enableResizing("false,false,false,false,false,false");
			objLogGrid.enableMultiselect(false);
        }else if(<?=$TypeID?> == <?=rptDataDest?>){
			<?//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
			$strSurveyKeyColHdr = (getMDVersion() >= esvDataDestErrorCheck)?(translate('Entry id').','):'';
			$strSurveyKeyColAln = (getMDVersion() >= esvDataDestErrorCheck)?'right,':'';
			$strSurveyKeyColTyp = (getMDVersion() >= esvDataDestErrorCheck)?'ro,':'';
			$strSurveyKeyColSrt = (getMDVersion() >= esvDataDestErrorCheck)?'int,':'';
			$strSurveyKeyColWdt = (getMDVersion() >= esvDataDestErrorCheck)?'50,':'';
			$strSurveyKeyColRsz = (getMDVersion() >= esvDataDestErrorCheck)?'false,':'';
			//@JAPR?>
        	objLogGrid.setHeader("<?=$strSurveyKeyColHdr?><?=translate('Date')?>,<?=translate('Time')?>,<?=translate('Survey')?>,<?=translate('User')?>, <?=translate('Destination')?>, <?=translate('Description')?>",null,["text-align:left;","text-align:left;", "text-align:left;", "text-align:left;", "text-align:center", "text-align:center"]);
        	objLogGrid.setColAlign("<?=$strSurveyKeyColAln?>left,left,left,left,center,center");
        	objLogGrid.setColTypes("<?=$strSurveyKeyColTyp?>ro,ro,ro,ro,ro,ro");
			objLogGrid.setColSorting("<?=$strSurveyKeyColSrt?>date,date,str,str,na,na");
			objLogGrid.setInitWidths("<?=$strSurveyKeyColWdt?>100,100,150,150,100,*");
			objLogGrid.enableResizing("<?=$strSurveyKeyColRsz?>false,false,false,false,false,false");
			objLogGrid.enableMultiselect(true);
        }
        else{
        	objLogGrid.setHeader("<?=translate('Date')?>,<?=translate('Time')?>,<?=translate('User')?>,<?=translate('No. Captures')?>,<?=translate('Duration')?>,<?=translate('Errors')?>,<?=translate('Map')?>",null,["text-align:left;","text-align:left;","text-align:left;", "text-align:left", "text-align:left", "text-align:left", "text-align:left"]);
        	objLogGrid.setColAlign("left,left,left,center,center,center,left");
        	objLogGrid.setColTypes("ro,ro,ro,ro,ro,ro,img");
        	objLogGrid.setInitWidths("170,170,*,150,100,100,100");
			objLogGrid.setColSorting("date,date,str,int,int,int,na");
			objLogGrid.enableResizing("false,false,false,false,false,false,false");
        }
		
		//objLogGrid.enableAutoHeight(true);
		//objLogGrid.enableAutoWidth(true);
		//objDialog.denyResize();
		//objLogGrid.objBox.style.overflowX = "scroll";
        //objLogGrid.objBox.style.overflowY = "auto";
		objLogGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");
		objLogGrid.attachEvent("onBeforeSelect", OnBeforeSelected);
		objLogGrid.attachEvent("onRowSelect", OnRowSelected);
		objLogGrid.init(); 
		<?
		$RowID = 0;
		if ($TypeID != rptDataDest) {
			foreach($anInstance->Collection as $aReport) {
		?>
			var rowValues = [];
			rowValues.push("<?=$aReport->DateID?>");
			rowValues.push("<?=$aReport->TimeID?>");
			//@AAL 24/07/2015 El Form Name solo debe mostrarse en la Bitacora de Formas
			if (<?=$TypeID?> == <?=rptEntries?>) {
				rowValues.push("<?=$aReport->SurveyName?>");
			}
			
			rowValues.push("<?=$aReport->UserName?>");
			if (<?=$TypeID?> == <?=rptEntries?>) {
				var map = ("<?= $aReport->Map ?>");
				var viewpdf = ("<?= $aReport->ViewPdf ?>");
				rowValues.push("images/mapa.gif^<?=translate('Map')?>^javascript:<?= $aReport->Map ?>;^_self");
				rowValues.push("images/pdf.gif^<?=translate('View PDF')?>^javascript:<?= $aReport->ViewPdf ?>;^_self");
			}
			else{
				rowValues.push("<?=$aReport->FormsNum?>");
				rowValues.push("<?=$aReport->Duration?>");
				rowValues.push("<?=$aReport->Fails?>");
				rowValues.push("images/mapa.gif^<?=translate('Map')?>^javascript:<?= $aReport->Map ?>;^_self");
			}
			
			var strRowID = "<?= $RowID . '_' . $aReport->SurveyID . '_' . $aReport->DateID . ' ' . $aReport->TimeID ?>";
			//JAPR 2015-08-08: Corregido un bug, no estaba cargando la instancia de reporte correcto pues no estaba considerando al usuario (#4PIE8N)
			objLogGrid.addRow(strRowID, rowValues);
			objLogGrid.setRowAttribute(strRowID, "UserID", <?=(int) @$aReport->UserID?>);
		<?
				$RowID++;
			}
		} 
		else {
			if ($TypeID == rptDataDest) {
				// *********************** DATA DESTINATIONS
				//DIC-15: Se cambio la manera en la que se recorre el arrreglo por que no se estaba realizando correctamente
				//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
				$blnAddEntryId = (getMDVersion() >= esvDataDestErrorCheck);
				//@JAPR
				foreach($anInstance->Collection as $objDestinationLog) {
	?>				
					var rowValues = [];
					<?//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
					if ( $blnAddEntryId ) {?>
					rowValues.push("<?=$objDestinationLog->surveykey?>");
					<?}//@JAPR?>
					rowValues.push("<?=$objDestinationLog->dateid?>");
					rowValues.push("<?=$objDestinationLog->timeid?>");
					rowValues.push("<?=$allSurveyNames[$objDestinationLog->surveyid]?>");
					rowValues.push("<?=$objDestinationLog->userid?>");
					rowValues.push("<?=translate($objDestinationLog->destination)?>");
					rowValues.push("<?=translate($objDestinationLog->description)?>");
					var strRowID = "<?= $RowID .'_' .$objDestinationLog->surveydestinationlogid.'_' . $objDestinationLog->surveyid. '_' . $objDestinationLog->surveydate. ' ' . "12:12:12" ?>";
					
					//JAPR 2015-08-08: Corregido un bug, no estaba cargando la instancia de reporte correcto pues no estaba considerando al usuario (#4PIE8N)
					objLogGrid.addRow(strRowID, rowValues);
					objLogGrid.setRowAttribute(strRowID, "UserID", <?=(int) @$objDestinationLog->userid?>);
					objLogGrid.setRowAttribute(strRowID, "SurveyDestinationLogID", <?=(int) @$objDestinationLog->surveydestinationlogid?>);
	<?
						$RowID++;
				}
			}			
		}
		?>
		
		function processDataDestination()
		{
			var arrDDs; //Contendrá los IDs de los destinos que se tienen que reprocesar
			arrDDs= new Array();
			var selRows = objLogGrid.getSelectedRowId();
			if(selRows==null)
			{
				return;
			}
			//alert('function processDataDestination:'+selRows);
			//Parsear la cadena para obtener los IDs de los registros de data destination que
			//se tienen que reprocesar
			var arrSelRows=selRows.split(",");
			var selRow;
			var iDD;
			iDD=0;
			var iRow;
			var strParams;
			strParams="";
			
			for (iRow in arrSelRows)
			{
				var selRow = arrSelRows[iRow];
				var arrRow = selRow.split("_");
				//En la posición 1 está el ID del data destination
				arrDDs[iDD]=arrRow[1];
				if(strParams!="")
				{
					strParams+=",";
				}
				strParams += arrDDs[iDD];
				iDD++;
				
			}
			strParams="SurveyDestinationLogIDs="+strParams+"&SelRows="+selRows;
			//alert('strParams='+strParams);
			window.dhx4.ajax.post("processDataDestination.php", strParams, function(loader) {
				console.log('after processDataDestination.php');
				afterProcessDataDestination(loader);
			});

		}
		
		function afterProcessDataDestination(loader)
		{
			console.log('afterProcessDataDestination');
			alert("<?=translate("A task was added to process data destination")?>");
			
			if (!loader || !loader.xmlDoc) {
				/*
				if (doCancelDragDropOperation) {
					doCancelDragDropOperation();
				}
				*/
				return;
			}
			
			if (loader.xmlDoc && loader.xmlDoc.status != 200) {
				alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
				/*
				if (doCancelDragDropOperation) {
					doCancelDragDropOperation();
				}
				*/
				return;
			}
			
			//Si llega a este punto es que se recibió una respuesta correctamente
			var response = loader.xmlDoc.responseText;
			try {
				var objResponse = JSON.parse(response);
			} catch(e) {
				alert(e + "\r\n" + response.substr(0, 1000));
				/*
				if (doCancelDragDropOperation) {
					doCancelDragDropOperation();
				}
				*/
				return;
			}
			
			if (objResponse.error) {
				alert(objResponse.error.desc);
				/*
				if (doCancelDragDropOperation) {
					doCancelDragDropOperation();
				}
				*/
				return;
			}
			else {
				if (objResponse.warning) {
					console.log(objResponse.warning.desc);
				}
			}
			
			//Ocultar filas procesadas
			if(objResponse.rows && objResponse.rows.length>0)
			{
				objLogGrid.clearSelection();
				for(iRow=0; iRow< objResponse.rows.length; iRow++)
				{
					//Ocultar fila con ID objResponse.rows[iRow]
					//alert("Ocultar fila con ID:"+objResponse.rows[iRow]);
					objLogGrid.setRowHidden(objResponse.rows[iRow], true);
				}
				
			}
		}
		
		//Descarga la instancia del diálogo abierto
		function doUnloadDialog() {
			console.log('doUnloadDialog');
			if (!objWindows || !objDialog) {
				return;
			}
			
			if (objWindows.unload) {
				objWindows.unload();
				objWindows = null;
			}
			objDialog = null;
		}
		
		//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
		function OnBeforeSelected(new_row, old_row, new_col_index) {
			if (!Toolbar || !Toolbar.bitRemoveState || Toolbar.bitDisableEvents || !objRowsSelected) {
				return true;
			}
			
			//Si se está haciendo click nuevamente a un elemento ya seleccionado, se debe limpiar la selección y remover del objeto de selecciones a la row a la que se hizo click
			Toolbar.bitDisableEvents = true;
			if (objRowsSelected[new_row]) {
				delete objRowsSelected[new_row];
				objLogGrid.clearSelection();
				for (selRow in objRowsSelected) {
					objLogGrid.selectRowById(selRow, true);
				}
			}
			else {
				//Se agrega el elemento a al objeto de selecciones y se forza a su selección, en este caso se conservan las selecciones previas así que no hay necesidad de volver
				//a seleccionar cada row como cuando se está removiendo una selección
				objRowsSelected[new_row] = 1;
				objLogGrid.selectRowById(new_row, true);
			}
			Toolbar.bitDisableEvents = false;
			
			//Cancela el evento para que no llegue a OnRowSelect, ya que en este punto se seleccionarán o removerán las selecciones de rows cuando está el modo de remover activo
			return false;
		}
		
		function OnRowSelected(RowId, ColumId) {
			//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
			//Si está habilitado el estado para remover, el click no debe cargar el detalle del reporte sino sólo permitir la selección múltiple
			if (Toolbar && Toolbar.bitRemoveState) {
				return;
			}
			//JAPR
		
			if (ColumId != 4 && ColumId != 5 && <?=$TypeID?> == <?=rptEntries?>) {
				/*var Date = objLogGrid.cells(RowId, 0);
				var Time = objLogGrid.cells(RowId, 1);
				var DateID = Date.getValue() + ' ' + Time.getValue();*/
				var SurveyID = RowId.split('_')[1];
				var DateID = RowId.split('_')[2];
				//"w1", 50, 50, 650, 500
				objWindows = new dhtmlXWindows();
				objDialog = objWindows.createWindow(
					{
						id:"w1",
						left:50,
						top:50,
						width:1000,
						height:600,
						center:true,
						modal:true
						
					});				
				objDialog.setText("<?=translate('Capture log')?>");
				$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
				//objDialog.denyResize();
				objDialog.centerOnScreen();
				objDialog.attachEvent("onClose", function() {
					//Despues de cerrar la ventana de Dialogo
					setTimeout(function () {
						doUnloadDialog()
					}, 100);
					return true;
				});
				
				//JAPR 2015-08-08: Corregido un bug, no estaba cargando la instancia de reporte correcto pues no estaba considerando al usuario (#4PIE8N)
				var intUserID = objLogGrid.getRowAttribute(RowId, "UserID");
				//JAPR 2015-08-13: Corregido un bug, se estaba enviando el parámetro  de Ajax en true así que los objetos sobreescribian a los locales
				objDialog.attachURL("ShowDataToFormsLog.php", null, {SurveyID:SurveyID, DateID: DateID, UserID:intUserID, RowID:RowId});
				//openWindowDialog("ShowAllQuestionsDataToFormsLog.php?SurveyID=" + SurveyID + "&DateID=" + DateID, [window], [], 650, 500);
				/*window.dhx4.ajax.get("ShowDataToFormsLog.php?SurveyID=" + SurveyID + "&DateID=" + DateID, function(){
				    // your code here
				});*/								
				//objDialog.setFocus();
			}
		}

		function str_custom(a,b,order){
	        return (a.toLowerCase() > b.toLowerCase() ? 1: -1) * (order == "asc" ? 1 : -1);
	    }

		//GCRUZ 2016-07-27. Mapa google de posiciones
		function showEntryGoogleMap(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID)
		{
			strParams = 'SurveyID=' + (SurveyIDs.toString()) + '&startDate=' + startDate + '&endDate=' + endDate + '&userID=' + UserID + '&dateTwoState=' + DateTwoState + '&searchForm=' + searchForm + '&GroupID=' + GroupID;
			window.dhx4.ajax.post("entryGoogleMapData.php", strParams, function(ajaxGMData) {
				if (ajaxGMData.xmlDoc.response.indexOf('GMDataError:') != -1)
				{
					alert("<?=translate('This function is only allowed when selecting a specific user, and a single day')?>");
					return;
				}
				var GMData = JSON.parse(ajaxGMData.xmlDoc.response);
				console.log(GMData);
				objWindows = new dhtmlXWindows();
				objDialog = objWindows.createWindow(
					{
						id:"w2",
						left:50,
						top:50,
						width:650,
						height:500,
						center:true,
						modal:true
				});
				objDialog.setText("<?=translate('Map')?>");
				$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
				objDialog.denyResize();
				objDialog.centerOnScreen();
				objDialog.attachEvent("onClose", function() {
					//Despues de cerrar la ventana de Dialogo
					setTimeout(function () {
						doUnloadDialog();
					}, 100);
					return true;
				});

				var bound = new google.maps.LatLngBounds();

				for (var i = 0; i < GMData.bounds.length; i++) {
					if (parseFloat(GMData.bounds[i].latitude) != 0 && parseFloat(GMData.bounds[i].longitude) != 0)
					{
						bound.extend( new google.maps.LatLng(GMData.bounds[i].latitude, GMData.bounds[i].longitude) );
					}
				}
				var mapCenter =  bound.getCenter();

				var mapParams = {
					center: mapCenter,
					zoom: 12,
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				entryMap = objDialog.attachMap(mapParams);
				entryMap.fitBounds(bound);

				// Define a symbol using a predefined path (an arrow)
				// supplied by the Google Maps JavaScript API.
				var lineSymbol = {
					path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
				};

				var markers = [];
				var indexMarker = 0;
				for (var userID in GMData.users)
				{
					var user = GMData.users[userID];
					for (var dateID in user)
					{
						var date = user[dateID];
						var route = [];
						var color = date.color;
						var startDate = date.surveydate;
						var userName = date.userid;
						for (var position in date.bounds)
						{
							route.push(new google.maps.LatLng(date.bounds[position].latitude, date.bounds[position].longitude));

							var userPosition = new google.maps.LatLng(date.bounds[position].latitude, date.bounds[position].longitude);
							/*
							markers[indexMarker] = new google.maps.Marker({
								position: userPosition,
								map: entryMap,
								title: String(date.bounds[position].title),
								label: String(date.bounds[position].label),
								info: String(date.bounds[position].info),
								draggable:false
							});
							*/

							markers[indexMarker] = new MarkerWithLabel({
								position: userPosition,
								map: entryMap,
								draggable: false,
								raiseOnDrag: true,
								labelContent: String(date.bounds[position].label),
								info: String(date.bounds[position].info),
								title: String(date.bounds[position].title),
								labelAnchor: new google.maps.Point(10, 39),
								labelClass: "labels", // the CSS class for the label
								labelInBackground: false,
								opacity: 1,
								icon: 'images/admin/free-map-marker-icon-blue-darker.png'
							});
							markers[indexMarker].addListener('click', function() {
								var infoWindow = new google.maps.InfoWindow({
									content: this.info
								});
								infoWindow.open(entryMap, this);
							});
							indexMarker++;
						}
						var userPath=new google.maps.Polyline({
							path:route,
							strokeColor:color,
							strokeOpacity:0.8,
							strokeWeight:2,
							map:entryMap,
							icons: [{
								icon: lineSymbol,
								offset: '100%',
								repeat: '20%'
							}]
						});
					}
				}
			});
		}

		//GCRUZ 2016-07-29. Globo del marcador (SVG)
		function pinSymbol(color) {
			return {
				path: 'M 0,0 L -2,-10 L -7,-10 L -7,-20 L 7,-20 L 7,-10 L 2,-10 L 0,0 z',
				fillColor: color,
				fillOpacity: 1,
				strokeColor: '#000',
				strokeWeight: 1,
				scale: 2
			};
		}

 		function openGoogleMap(surveyID, userID, startDate, endDate, reportID, reportType)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();

			objWindows = new dhtmlXWindows();
			objDialog = objWindows.createWindow(
				{
					id:"w2",
					left:50,
					top:50,
					width:650,
					height:500,
					center:true,
					modal:true
			});
			objDialog.setText("<?=translate('Map')?>");
			$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
			objDialog.denyResize();
			objDialog.centerOnScreen();
			objDialog.attachEvent("onClose", function() {
				//Despues de cerrar la ventana de Dialogo
				setTimeout(function () {
					doUnloadDialog();
				}, 100);
				return true;
			});
			objDialog.attachURL('gmeSurveyV6.php?surveyID='+surveyID+'&userID='+userID+'&startDate='+startDate+'&endDate='+endDate+'&reportID='+reportID+'&reportType='+reportType);

			//var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&userID='+userID+'&startDate='+startDate+'&endDate='+endDate+'&reportID='+reportID, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}

		//Exportar los datos del Reporte actual en GoogleMaps
 		function googleMap(surveyID, userID, dateID, reportID, reportType)
 		{
 	 		console.log('googleMap2');
			openGoogleMap(surveyID, userID, dateID, dateID, reportID, reportType);
 		}

 		function FilterToDate(FiterDate)
 		{
 			//EVEGA Obtenemos el widget del BoxFilter para vericar si tiene filtros activados
 			if (!$.isEmptyObject($('#boxFilter').data())){
 				var boxFilterWidget=$('#boxFilter').data().bitamBoxfilter;
 				var searchString = boxFilterWidget.element.find('input[name|="search"]').val();
 				var filter = boxFilterWidget.filter;
 				if(searchString != ''){
					filter['SearchString'] = searchString;
				}
				/*if ($('[data-fieldname="'+'date'+'"]').val() != '') {
					filter['Date'] = $('[data-fieldname="'+'date'+'"]').val();
				}*/
				for (var fkey in filter) {
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}
					//Search for Date
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
					}
					//Search for Group of Users
					if (fkey == 'Groups') {
						var filterGroups = filter['Groups'];
						var Groups = new Array();
						for (var ukey in filterGroups) {
							Groups.push(filterGroups[ukey]);
						}
						frmAttribFilter.GroupID.value = Groups.join(',');
						/*if(descrTag == 'Groups')
							frmAttribFilter.oldGroupsTags.value = oldTags;*/
					}	
					//Search for Users
					if (fkey == 'Users') {
						var filterUsers = filter['Users'];
						var Users = new Array();
						for (var ukey in filterUsers) {
							Users.push(filterUsers[ukey]);
						}
						frmAttribFilter.userID.value = Users.join(',');
						/*if(descrTag == 'Groups')
							frmAttribFilter.oldUserTags.value = oldTags;*/
					}
					//Search for Forms
					if (fkey =='Forms'){
						var filterForms = filter['Forms'];
						var Forms = new Array();
						for (var ukey in filterForms) {
							Forms.push(filterForms[ukey]);
						}
						frmAttribFilter.SurveyID.value = Forms.join(',');
						//frmAttribFilter.oldFormsTags.value = allForms;
						//frmAttribFilter.SurveyID.value = parseInt(filter['Forms'][0], 10);
					}
				}

				//EVEGA obtenemos los tags del box filter para mandarlo como oldTags
				var oldTags=JSON.stringify(boxFilterWidget.descriptionsTags);
				frmAttribFilter.oldFormsTags.value = oldTags;
 			}
 			if(FiterDate!=null){
 				frmAttribFilter.FilterDate.value = FiterDate;
 			}
			frmAttribFilter.submit();
 		}
		
<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//@JAPR 2016-08-24: Implementada la eliminación múltiple de reportes
?>
		//Si se encontraba en estado de selección, realiza el borrado de los elementos seleccionados, en caso contrario inicia el estado de selección de elementos
		function doRemoveItems() {
			if (!Toolbar || !objLogGrid) {
				return;
			}
			
			//Si aún no se había presionado el botón de eliminar, entonces cambia la imagen y deshabilita el click en el Grid para permitir selección múltiple
			if (!Toolbar.bitRemoveState) {
				objRowsSelected = new Object();
				var selRows = objLogGrid.getSelectedRowId();
				if (selRows) {
					var arrSelRows = selRows.split(",");
					for (iRow in arrSelRows) {
						var selRow = arrSelRows[iRow];
						if (selRow) {
							objRowsSelected[selRow] = 1;
						}
					}
				}
				
				Toolbar.bitRemoveState = 1;
				//@ears 2016-10-07 solo en versiones anteriores seguirá apareciendo texto				
				if (eFormsMDVersion < 6.03) {
					Toolbar.setItemText('remove', '<?= translate("Remove") ?>');
				}
				else {
					Toolbar.setItemToolTip("remove","<?= translate("Remove") ?>");
				}	
				Toolbar.setItemImage('remove', 'remove.png');
				Toolbar.showItem('removeCancel');
				objLogGrid.enableMultiselect(true);
				return;
			}
			
			//Los reportes no tienen la referencia directa del ID, así que se debe enviar la llave que contiene la combinación Forma + Usuario + Fecha-Hora para que se use al eliminar
			var selRows = objLogGrid.getSelectedRowId();
			if (selRows == null) {
				return;
			}
			
			var arrSelRows = selRows.split(",");
			for (iRow in arrSelRows) {
				var selRow = arrSelRows[iRow];
				if (selRow) {
					selRow += "_" + objLogGrid.getRowAttribute(selRow, "UserID");
					arrSelRows[iRow] = selRow;
				}
			}
			
			/*@ears 2016-10-10 comentadas las siguientes líneas no se debe permitir eliminar notificaciones*/						
			<?if($TypeID == rptEntries){?>
				var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove all selected %s?"), strtolower(translate("Entries")))?>");
				if (!blnAnswer) {
					return;
				}
				
				var objParams = {
					RequestType: <?=reqAjax?>,
					Process: "Delete",
					ObjectType: <?=otyReport?>,
					ReportRowID: arrSelRows
				};
			
			
				/*@ears 2016-10-10 comentada doSendDataAjax no debe permitir eliminar notificaciones, las líneas de código que estaban activas son las que tienen 3 slash*/
				doSendDataAjax(objParams, function(oResponse) {
					//Invoca al widget de boxfilter para forzar al refresh de la ventana utilizando los mismos valores seleccionados tanto en el filtro como en los botones de fechas
					fnRefreshPage();
					//objReportLayout.cells(dhxGridDataCell).progressOff();
					//Cierra el diálogo de este reporte al terminar el borrado
					//setTimeout(function () {
						//Refresca la ventana de reportes aplicando los mismos filtros que actualmente tenía seleccionados
						//Elimina la row de este reporte
						/*if (parent.objLogGrid && parent.objLogGrid.deleteRow && '') {
							parent.objLogGrid.deleteRow('');
						}
						
						if (parent.doUnloadDialog) {
							parent.doUnloadDialog();
						}*/
					//}, 100);
				});
			<?}?>
		}
		
		//Cancela el estado de selección de elementos
		function doCancelRemoveItems() {
			if (!Toolbar || !objLogGrid) {
				return;
			}
			
			Toolbar.bitRemoveState = 0;
			objLogGrid.clearSelection();
			objLogGrid.enableMultiselect(false);
			//Toolbar.setItemText('remove', '<?= translate("Edit") ?>');
			//@ears 2016-10-07 solo en versiones anteriores seguirá apareciendo texto				
			if (eFormsMDVersion < 6.03) {
				Toolbar.setItemText('remove', '<?= translate("Edit") ?>');
			}
			else {
				Toolbar.setItemToolTip("remove","<?= translate("Edit") ?>");
			}							
			Toolbar.setItemImage('remove', 'edit.png');
			Toolbar.hideItem('removeCancel');
		}
		
		/* Envía información al servidor para procesar algo, ya sea un grabado, borrado o para obtener información
		*/
		function doSendDataAjax(oFields, oCallbackFn, bRemoveProgress) {
			console.log("doSendDataAjax");
			
			if (bRemoveProgress === undefined) {
				bRemoveProgress = true;
			}
			
			//Prepara los parámetros con los datos a grabar
			var objFieldsData = oFields;
			if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
				return;
			}
			
			var objParams = $.extend({}, objFieldsData);
			if (!objParams) {
				return;
			}
			
			var strParams = '';
			var strAnd = '';
			for (var strProp in objParams) {
				//JAPR 2016-08-24: Agregado el soporte para parámetros array
				var objParamArray = objParams[strProp];
				if ($.isArray(objParamArray)) {
					for (var intIndex in objParamArray) {
						strParams += strAnd + strProp + "["+intIndex+"]=" + encodeURIComponent(objParamArray[intIndex]);
						strAnd = '&';
					}
				}
				else {
					strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
			}
			
			//objReportLayout.cells(dhxGridDataCell).progressOn();
			window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
				doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
			});
		}
		
		/* Procesa la respuesta de una invocación al servidor */
		function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress) {
			console.log('doSaveConfirmation');
			
			if (!loader || !loader.xmlDoc) {
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			if (loader.xmlDoc && loader.xmlDoc.status != 200) {
				alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			//Si llega a este punto es que se recibió una respuesta correctamente
			var response = loader.xmlDoc.responseText;
			try {
				var objResponse = JSON.parse(response);
			} catch(e) {
				alert(e + "\r\n" + response.substr(0, 1000));
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			if (objResponse.error) {
				alert(objResponse.error.desc);
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			else {
				if (objResponse.warning) {
					console.log(objResponse.warning.desc);
				}
			}
			
			//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
			//pueda ejecutar un código de callback
			if (objResponse.objectType) {
				if (objResponse.deleted) {
					if (oCallbackFn && typeof oCallbackFn == "function") {
						oCallbackFn(objResponse);
					}
					else {
						if (bRemoveProgress) {
							//objReportLayout.cells(dhxGridDataCell).progressOff();
						}
					}
				}
				return;
			}
			else {
				if (objResponse.newObject) {
					if (oCallbackFn && typeof oCallbackFn == "function") {
						oCallbackFn(objResponse.newObject);
					}
				}
				return;
			}
			
			if (bRemoveProgress) {
				//objReportLayout.cells(dhxGridDataCell).progressOff();
			}
		}
		
 		function exportPDF(SurveyID, DateID, ReportID, UserID)
 		{
 			frmExportPDF.SurveyID.value = SurveyID;
 			frmExportPDF.DateID.value = DateID;
 			frmExportPDF.ReportID.value = ReportID;
 			frmExportPDF.userID.value = UserID;
			frmExportPDF.submit();
 		}
 		 //UeserID = Email
		 //GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
		 //GCRUZ 2015-08-27 Filtro de búsqueda de formas
 		function zipReport(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID)
		{
			frmZipReport.SurveyID.value = SurveyIDs;
			frmZipReport.startDate.value = startDate;
 			frmZipReport.endDate.value = endDate;
 			frmZipReport.userID.value = UserID;
			frmZipReport.dateTwoState.value = DateTwoState;
			frmZipReport.searchForm.value = searchForm;
			frmZipReport.GroupID.value = GroupID;
			frmZipReport.submit();
		}
		
		//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
		//GCRUZ 2015-08-27 Filtro de búsqueda de formas
		function exportPPTX(SurveyIDs, startDate,endDate, UserID, DateTwoState, searchForm, GroupID)
		{
			frmPPTXReport.SurveyID.value = SurveyIDs;
			frmPPTXReport.startDate.value = startDate;
 			frmPPTXReport.endDate.value = endDate;
			frmPPTXReport.userID.value = UserID;
			frmPPTXReport.dateTwoState.value = DateTwoState;
			frmPPTXReport.searchForm.value = searchForm;
			frmPPTXReport.GroupID.value = GroupID;
			frmPPTXReport.submit();
		}
		
 		//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
		//GCRUZ 2015-08-27 Filtro de búsqueda de formas
		function exportReport(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID, ReportFormat)
 		{
 			frmExportReport.SurveyID.value = SurveyIDs;
 			frmExportReport.startDate.value = startDate;
 			frmExportReport.endDate.value = endDate;
 			frmExportReport.userID.value = UserID;
			frmExportReport.dateTwoState.value = DateTwoState;
			frmExportReport.searchForm.value = searchForm;
			frmExportReport.GroupID.value = GroupID;
			frmExportReport.ReportFormat.value = ReportFormat;
 			frmExportReport.submit();
 		}

		//GCRUZ 2016-06-23 Exportar a SPSS
		function exportToSPSS(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID)
 		{
 			frmExportToSPSS.SurveyID.value = SurveyIDs;
 			frmExportToSPSS.startDate.value = startDate;
 			frmExportToSPSS.endDate.value = endDate;
 			frmExportToSPSS.userID.value = UserID;
			frmExportToSPSS.dateTwoState.value = DateTwoState;
			frmExportToSPSS.searchForm.value = searchForm;
			frmExportToSPSS.GroupID.value = GroupID;
 			frmExportToSPSS.submit();
 		}
		
		//ears 2016-07-15 exportación de documentos, audio y video
 		function zipReportDoc(SurveyIDs, startDate, endDate, UserID, DateTwoState, searchForm, GroupID)
		{
			frmZipReportDoc.SurveyID.value = SurveyIDs;
			frmZipReportDoc.startDate.value = startDate;
 			frmZipReportDoc.endDate.value = endDate;
 			frmZipReportDoc.userID.value = UserID;
			frmZipReportDoc.dateTwoState.value = DateTwoState;
			frmZipReportDoc.searchForm.value = searchForm;
			frmZipReportDoc.GroupID.value = GroupID;
			frmZipReportDoc.submit();
		}
		 		
		</script>
		<form name="frmAttribFilter" action="main.php?BITAM_SECTION=ReportCollection" method="POST" accept-charset="utf-8">
			<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
			<input type="hidden" name="GroupID" id="GroupID" value="">
			<input type="hidden" name="SurveyID" id="SurveyID" value="">
			
			<input type="hidden" name="FilterDate" id="FilterDate" value="">
			<input type="hidden" name="startDate" id="startDate" value="">
			<input type="hidden" name="endDate" id="endDate" value="">
			<input type="hidden" name="userID" id="userID" value="">
			<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">

			<input type="hidden" name="searchString" id="searchString" value="">
			<input type="hidden" name="oldGroupsTags" id="oldGroupsTags" value="">
			<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
			<input type="hidden" name="oldFormsTags" id="oldFormsTags" value="">
		</form>

		<form id="frmExportPDF" name="frmExportPDF" action="main.php" method="GET" target="_self">
			<input type="hidden" name="BITAM_PAGE" value="Report">
			<input type="hidden" name="SurveyID" id="SurveyID" value="<?=(int) @$anInstance->SurveyID?>">
			<input type="hidden" name="DateID" id="DateID" value="<?=(string) @$anInstance->DateID?>">
			<input type="hidden" name="ReportID" id="ReportID" value="">
			<input type="hidden" name="userID" id="userID" value="">
			<input type="hidden" name="ReportAction" id="ReportAction" value="ExportPDF">
			<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
		</form>
		
		<form id="frmZipReportDoc" name="frmZipReportDoc" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ZipReportDoc">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="dateTwoState" id="dateTwoState" value="">
		<input type="hidden" name="searchForm" id="searchForm" value="">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
	</form>		

		<form id="frmZipReport" name="frmZipReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ZipReport">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="dateTwoState" id="dateTwoState" value="">
		<input type="hidden" name="searchForm" id="searchForm" value="">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
	</form>
	
	<form id="frmPPTXReport" name="frmPPTXReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="">
		<input type="hidden" name="ReportAction" id="ReportAction" value="PPTXReport">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="dateTwoState" id="dateTwoState" value="">
		<input type="hidden" name="searchForm" id="searchForm" value="">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
	</form>
	
	<form id="frmExportReport" name="frmExportReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportCVS">
		<input type="hidden" name="ReportFormat" id="ReportFormat" value="XLS">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="dateTwoState" id="dateTwoState" value="">
		<input type="hidden" name="searchForm" id="searchForm" value="">
		<input type="hidden" name="AddPhotos" id="AddPhotos" value="">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
	</form>

	<form id="frmExportToSPSS" name="frmExportToSPSS" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportSPSS">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="dateTwoState" id="dateTwoState" value="">
		<input type="hidden" name="searchForm" id="searchForm" value="">
		<input type="hidden" name="AddPhotos" id="AddPhotos" value="">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="TypeID" id="TypeID" value="<?=$TypeID?>">
	</form>
		</body>
		</HTML>
		<?
		die();
		//return $anInstance;
	}

	//@JAPR 2012-01-10: Agregado el método que genera el archivo .zip que contiene las imagenes del Reporte especificado
	static function NewInstanceToCompress($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$strText = $anInstance->Questions[$i]->getAttributeName();
			
			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';
		
		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		
		@updateQueriesLogFile($queriesLogFile,$aLogString);

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
		$strZipFileName = "Survey".$anInstance->SurveyID."Photos.zip";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullZipFileName = $strLogPath.$strZipFileName;
		//@JAPR
		$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		$strZipPath = "survey_".$anInstance->SurveyID;
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		//@JAPR 2015-02-10: Agregado el control de errores
		//if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		//No estoy seguro si tiene o no algo que ver, pero con ZIPARCHIVE::OVERWRITE no me estaba creando el archivo cuando no existía, siendo
		//que en otro server equivalente si lo hacía, se cambió a ZIPARCHIVE::CREATE y funcionó, antes de eso marcaba error 11 (ZIPARCHIVE::ER_OPEN)
		//y no importa si existe o no el archivo, OVERWRITE continuaba marcando el error
		if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::CREATE))!==TRUE)
		{
			$intFailures = 1;
		    echo("(".__METHOD__.") "."Cannot create zip file <$strFullZipFileName>, error # {$intErrCode}");
		}
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
			
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->getAttributeName();
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			echo("(".__METHOD__.") "."No photo file was compressed for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			/*
			//$strZipData = $aZipObj->file();
			header("Content-type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			//header("Content-length: ".strlen($strZipData));
			header("Content-length: ".filesize($file));
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
			*/
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
			header("Content-Length: ".filesize($file));

			readfile($file);
		}
		exit();
	}
	//@JAPR

	/*@AAL 22/06/2015: Modificado, el UserID ya no sera un entero, ahora sera un string (el Email)*/
	//GCRUZ 2015-07-31
	static function NewInstanceToCompressToFormsLogV6($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $searchForm = '', $bOutputFile = true, $zipFile = array())
	{
		global $queriesLogFile;
		global $nSurveysF;
		global $nSurveysFD;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			if(trim($anInstance->Questions[$i]->AttributeName)!="")
			{
				$strText = $anInstance->Questions[$i]->AttributeName;
			}
			else
			{
				$strText = $anInstance->Questions[$i]->QuestionText;
			}

			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';
		
		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if (strlen(substr($startDate,11,18)) <= 0) {	
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate.' 00:00:00');
			}
			else {  //@ears 2016-08-31 solo en los casos en donde se filtra por la fecha de hoy no se estaba incluyendo filtro de fecha y eso provoca
					//que al momento de generar el zip se descarguen los archivos de todas las capturas que se hayan realizado de la forma
					//incluyendo estas lineas se aplica filtro y se evita que se descarguen los archivos de todas las capturas.
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);							
			}
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if (strlen(substr($endDate,11,18)) <= 0) {	
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate.' 23:59:59');
			}
			else{	//@ears 2016-08-31 solo en los casos en donde se filtra por la fecha de hoy no se estaba incluyendo filtro de fecha y eso provoca
					//que al momento de generar el zip se descarguen los archivos de todas las capturas que se hayan realizado de la forma
					//incluyendo estas lineas se aplica filtro y se evita que se descarguen los archivos de todas las capturas.
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);			
			}			
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$userID = explode(',', $userID);
			$where.=" ".$aSurvey->SurveyTable.".UserEMail IN ('" . implode("','", $userID). "')";
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".SurveyKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".UserEMail, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, 'FactKeyDimVal', ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";

		$fromFirstCatQuestion = '';
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
*/
			//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
			if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
			{
				$sql .= ', '.$anInstance->FirstCatQuestion->QuestionDetTable.'.* ';
				$fromFirstCatQuestion = ', '.$anInstance->FirstCatQuestion->QuestionDetTable;
			}
		}

		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6.
		$sql .= $fromFirstCatQuestion;

		//GCRUZ 2015-11-25. No hacer AND con mainfactkey, sino no muestra secciones multiregistro
//		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens && false)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
		if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
		{
			$sql .= ' AND '.$aSurvey->SurveyTable.'.SurveyKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
			$sql .= ' AND '.$strImgsTable.'.FactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
//			$sql .= ' AND '.$strImgsTable.'.MainFactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
		}

		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);

//		echo "<pre>";
//		var_dump($sql);
//		exit;

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		//print($sql);
		//print('<br>');
		//print($aRS);
		//die();
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		if (count($zipFile) == 0)
		{
			$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
//			var_dump($file);
			$strZipFileName = "SurveyPhotos.zip";
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strFullZipFileName = $strLogPath.$strZipFileName;
			//@JAPR
			$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strZipPath = "survey_".$anInstance->SurveyID;
			$zipFile['fullPath'] = $file;
			$zipFile['fileName'] = $strZipFileName;
		}
		else
		{
			$file = $zipFile['fullPath'];
//			var_dump($file);
			$strZipFileName = $zipFile['fileName'];
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strFullZipFileName = $strLogPath.$strZipFileName;
			//@JAPR
			$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strZipPath = "survey_".$anInstance->SurveyID;
		}
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		//@JAPR 2015-02-10: Agregado el control de errores
		//if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		//No estoy seguro si tiene o no algo que ver, pero con ZIPARCHIVE::OVERWRITE no me estaba creando el archivo cuando no existía, siendo
		//que en otro server equivalente si lo hacía, se cambió a ZIPARCHIVE::CREATE y funcionó, antes de eso marcaba error 11 (ZIPARCHIVE::ER_OPEN)
		//y no importa si existe o no el archivo, OVERWRITE continuaba marcando el error
		if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::CREATE))!==TRUE)
		{
			$intFailures = 1;
		    echo("(".__METHOD__.") "."Cannot create zip file <$strFullZipFileName>, error # {$intErrCode}");
		}
		
		//GCRUZ 2015-09-01. Si no hay imágenes que consultar, no crear el directorio y regresar el zip vacío		
		if ($aRS->_numOfRows == 0)
		{			
			$aZipObj->close(); 
			//@ears 2016-08-05
			//si en la bitàcora de capturas hay mas de una forma, se analiza forma por forma, por lo que en caso de que exista
			//alguna que no considere imágenes se contará para evaluar al final si se manda mensaje de que no se generará zip de imágenes, esto es solo
			//si el total de las formas es igual al total de las formas sin documentos, mientras en cada recorrido se hace return
			//$bOutputFile = false (cuando es mas de una forma a analizar)
			//$bOutputfile = true (cuando solo es una forma)
			if (!$bOutputFile) { 
				$nSurveysFD++;
				//return $zipFile;
				//echo("(".__METHOD__.") "."No photo file was compressed for this survey");
				if ($nSurveysF == $nSurveysFD) {
					echo("(".__METHOD__.") "."No photo file was compressed for this survey");
					exit();				
				}
				else {
					return $zipFile;
				}
				
			}
			else {
				echo("(".__METHOD__.") "."No photo file was compressed for this survey");
				exit();								
			}	
	 		
		}
				
		/*
		if ($aRS->_numOfRows == 0)
		{
			$aZipObj->close();
			if (!$bOutputFile)
				return $zipFile;
			echo("(".__METHOD__.") "."No photo file was compressed for this survey");
			exit();
		}
		*/
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
			
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["surveykey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
				if (!is_null($anInstance->FirstCatQuestion) && false)
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
*/
				//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
				$strFirstCatQuestionAttrib = '';
				if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
				{
					global $strNewAttribSep;
					$fieldAttrib = strtolower(BITAMQuestion::$DescFieldCatMember);
					if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
					{
						$arrCatStructValues = explode($strNewAttribSep, $aRS->fields["attributedesc"]);
						foreach ($arrCatStructValues as $arrKey => $strCatMemberDef) {
							$arrCatMemberParts = explode("_", $strCatMemberDef, 3);
							$strKey = (string) @$arrCatMemberParts[0];
							$strKey = strpos($strKey, "*");
							$strDataSourceMemberID = (int) @$arrCatMemberParts[1];
							$strDataSourceMemberName = (string) @$arrCatMemberParts[2];
							//Si encuentra una definición inválida debe terminar
							if ($strDataSourceMemberID <= 0) {
								break;
							}
							$strAttribValue = (string) @$aRS->fields[$fieldAttrib.$strDataSourceMemberID];
							if (trim($strAttribValue) == 'NA' || trim($strAttribValue) == '')
								continue;

							$strFirstCatQuestionAttrib .= $strDataSourceMemberName.'_'.$strAttribValue.'_';

							//Al encontrar el atributo de la pregunta debe terminar, ya que no habrá mas valores a mostrar para ella
							if ($strKey !== false) {
								break;
							}
						}
					}
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->AttributeName;
					if (trim($strQuestionText) == '')
					{
						$strQuestionText = $arrQuestionsColl[$intQuestionID]->QuestionText;
					}
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//GCRUZ 2015-11-25. Usar Pathimage en ves de armar el nombre de la imagen
//				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				$strNewFileName .= $strFirstCatQuestionAttrib.'_'.$strQuestionText.'_'.(string)str_replace('survey_'.$aRS->fields["surveyid"], '', $aRS->fields["pathimage"]);
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			$aZipObj->close();
			if (!$bOutputFile)
				return $zipFile;
			echo("(".__METHOD__.") "."No photo file was compressed for this survey");
//			var_dump($arrFailedPicts);
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			if ($bOutputFile)
			{
				/*
				//$strZipData = $aZipObj->file();
				header("Content-type: application/force-download");
				header("Content-Transfer-Encoding: Binary");
				//header("Content-length: ".strlen($strZipData));
				header("Content-length: ".filesize($file));
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Transfer-Encoding: binary");
				header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
				header("Content-Length: ".filesize($file));

				readfile($file);
			}
			else
				return $zipFile;
		}
		exit();
	} //
	//@JAPR

	/*@AAL 22/06/2015: Modificado, el UserID ya no sera un entero, ahora sera un string (el Email)*/
	static function NewInstanceToCompressToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $searchForm = '')
	{
		//GCRUZ 2015-07-31. Para soportar varias surveyIDs
//		echo "<pre>";
		require_once('survey.inc.php');
		
		global $nSurveysF;
		
		$date_begin = (isset($_REQUEST['startDate']) && trim($_REQUEST['startDate']) != '')? date('Y-m-d', strtotime($_REQUEST['startDate'])).' 00:00:00': null;
		$date_end = (isset($_REQUEST['endDate']) && trim($_REQUEST['endDate']) != '')? date('Y-m-d', strtotime($_REQUEST['endDate'])).' 23:59:59': null;	
		$userIDs = (isset($_REQUEST['userID']) && trim($_REQUEST['userID']) != '')? explode(',', $_REQUEST['userID']): array();	
		$dateTwoState = (isset($_REQUEST['dateTwoState']) && trim($_REQUEST['dateTwoState']) != '')? $_REQUEST['dateTwoState'] : '';
		$searchForm = (isset($_REQUEST['searchForm']) && trim($_REQUEST['searchForm']) != '')? $_REQUEST['searchForm'] : '';

		switch($dateTwoState)
		{
			case 'T':
				$date_begin = date('Y-m-d').' 00:00:00';
				$date_end = date('Y-m-d').' 23:59:59'; 
				break;
			case 'W':
				$weekDay = intval(date('w'));
				$date_begin = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
				$date_end = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
				break; 
			case 'M':
				$tDay = date('t');
				$date_begin = date('Y-m-').'01 00:00:00';
				$date_end = date('Y-m-').$tDay.' 23:59:59';
				break;
			case 'Y':
				$date_begin = date('Y-').'01-01 00:00:00';
				$date_end = date('Y-').'12-31 23:59:59';
				break;
		}
		

		if (trim($aSurveyID) == '')
		{			
			$anADOConnection = $aRepository->DataADOConnection;
			$arrSurveyIDs = array();

			if (!isset($userID) || $userID == '') {
				//@ears 2016-07-15 modificaciones para filtar formas y no cargar todas 			
				//Obtener datos standard de la forma
				$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 ORDER BY SurveyID";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				while (!$aRS->EOF) {
					$arrSurveyIDs[] = $aRS->fields['SurveyID'];	
					$aRS->MoveNext();					
				}	
			}			
			else {
				foreach($userIDs as $iUserId => $aUser) {
					//El query se filtra por la combinación User y SurveyDate
					$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 AND UserID = '" .$aUser."' ORDER BY SurveyID";					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					while (!$aRS->EOF) {
						$arrSurveyIDs[] = $aRS->fields['SurveyID'];	
						$aRS->MoveNext();
					}
					//break;	
				}								
			}												
		}
		else
			$arrSurveyIDs = explode(',', $aSurveyID);
			
		$bOutputFile = true;
		if (count($arrSurveyIDs) > 1)
			$bOutputFile = false;
		$zipFile = array();
		$nSurveysF = count($arrSurveyIDs);

		if ($nSurveysF == 0) { 				
				echo("(".__METHOD__.") "."No photo file");
				exit();							
		}
			
		foreach ($arrSurveyIDs as $surveyID)
		{
			$survey = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
			if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
		  		continue;			
			//$zipFile = BITAMReportCollection::NewInstanceToCompressToFormsLogV6($aRepository, $surveyID, $attribSelected, $startDate, $endDate, $userID, $searchForm, $bOutputFile, $zipFile);
			$zipFile = BITAMReportCollection::NewInstanceToCompressToFormsLogV6($aRepository, $surveyID, $attribSelected, $date_begin, $date_end, $userID, $searchForm, $bOutputFile, $zipFile);
		}						
				
		/***************************************************
		if (trim($aSurveyID) == '')
		{
			//Cargar TODAS las surveyIDs
			$loadedSurveys = BITAMSurvey::getSurveys($aRepository, true);
		//	die('SurveyID not found');
			$arrSurveyIDs = array();
			foreach($loadedSurveys as $skey => $asurvey)
				$arrSurveyIDs[] = $skey;
		}
		else
		$arrSurveyIDs = explode(',', $aSurveyID);
		$bOutputFile = true;
		if (count($arrSurveyIDs) > 1)
			$bOutputFile = false;
		$zipFile = array();
		foreach ($arrSurveyIDs as $surveyID)
		{
			$survey = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
			if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
				continue;
			$zipFile = BITAMReportCollection::NewInstanceToCompressToFormsLogV6($aRepository, $surveyID, $attribSelected, $startDate, $endDate, $userID, $searchForm, $bOutputFile, $zipFile);
		}
		****************************************************/
		
//		var_dump($arrFiles);
//		exit;
		$sfile = $zipFile['fileName'];
		$spathfile = $zipFile['fullPath'];
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/zip");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		readfile($spathfile);
		exit;

		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$strText = $anInstance->Questions[$i]->getAttributeName();
			
			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';
		
		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = " . $anInstance->Repository->DataADOConnection->Quote($userID);
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
		$strZipFileName = "Survey".$anInstance->SurveyID."Photos.zip";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullZipFileName = $strLogPath.$strZipFileName;
		//@JAPR
		$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		$strZipPath = "survey_".$anInstance->SurveyID;
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		//@JAPR 2015-02-10: Agregado el control de errores
		//if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		//No estoy seguro si tiene o no algo que ver, pero con ZIPARCHIVE::OVERWRITE no me estaba creando el archivo cuando no existía, siendo
		//que en otro server equivalente si lo hacía, se cambió a ZIPARCHIVE::CREATE y funcionó, antes de eso marcaba error 11 (ZIPARCHIVE::ER_OPEN)
		//y no importa si existe o no el archivo, OVERWRITE continuaba marcando el error
		if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		{
			$intFailures = 1;
		    echo("(".__METHOD__.") "."Cannot create zip file <$strFullZipFileName>, error # {$intErrCode}");
		}
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
			
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->getAttributeName();
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			echo("(".__METHOD__.") "."No photo file was compressed for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			/*
			//$strZipData = $aZipObj->file();
			header("Content-type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			//header("Content-length: ".strlen($strZipData));
			header("Content-length: ".filesize($file));
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
			*/
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
			header("Content-Length: ".filesize($file));

			readfile($file);
		}
		exit();
	}
	//@JAPR
	
	//@ears 2016-07-18
	//basado en NewInstanceToCompressToFormsLogV6
	static function NewInstanceToCompressDocV6($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $searchForm = '', $bOutputFile = true, $zipFile = array())
	{
		global $queriesLogFile;
		global $nSurveys;
		global $nSurveysD;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			if(trim($anInstance->Questions[$i]->AttributeName)!="")
			{
				$strText = $anInstance->Questions[$i]->AttributeName;
			}
			else
			{
				$strText = $anInstance->Questions[$i]->QuestionText;
			}

			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerDocument';
		
		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if (strlen(substr($startDate,11,18)) <= 0) {	
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate.' 00:00:00');
			}
			
			else {	//@ears 2016-08-31 solo en los casos en donde se filtra por la fecha de hoy no se estaba incluyendo filtro de fecha y eso provoca
					//que al momento de generar el zip se descarguen los archivos de todas las capturas que se hayan realizado de la forma
					//incluyendo estas lineas se aplica filtro y se evita que se descarguen los archivos de todas las capturas.
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
			}
				
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if (strlen(substr($endDate,11,18)) <= 0) {	
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate.' 23:59:59');
			}
			
			else {	//@ears 2016-08-31 solo en los casos en donde se filtra por la fecha de hoy no se estaba incluyendo filtro de fecha y eso provoca
					//que al momento de generar el zip se descarguen los archivos de todas las capturas que se hayan realizado de la forma
					//incluyendo estas lineas se aplica filtro y se evita que se descarguen los archivos de todas las capturas.
				$where.=" AND ";
				$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
			}
			
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$userID = explode(',', $userID);
			$where.=" ".$aSurvey->SurveyTable.".UserEMail IN ('" . implode("','", $userID). "')";
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".SurveyKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".UserEMail, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, 'FactKeyDimVal', ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathDocument ";

		$fromFirstCatQuestion = '';
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
*/
			//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
			if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
			{
				$sql .= ', '.$anInstance->FirstCatQuestion->QuestionDetTable.'.* ';
				$fromFirstCatQuestion = ', '.$anInstance->FirstCatQuestion->QuestionDetTable;
			}
		}

		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6.
		$sql .= $fromFirstCatQuestion;

		//GCRUZ 2015-11-25. No hacer AND con mainfactkey, sino no muestra secciones multiregistro
//		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens && false)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
		if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
		{
			$sql .= ' AND '.$aSurvey->SurveyTable.'.SurveyKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
			$sql .= ' AND '.$strImgsTable.'.FactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
//			$sql .= ' AND '.$strImgsTable.'.MainFactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
		}

		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);

//		echo "<pre>";
//		var_dump($sql);
//		exit;

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		if (count($zipFile) == 0)
		{
			//$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
			$file = tempnam("Survey".$anInstance->SurveyID."Documents", "zip");
//			var_dump($file);
			//$strZipFileName = "SurveyPhotos.zip";
			$strZipFileName = "SurveyDocuments.zip";
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strFullZipFileName = $strLogPath.$strZipFileName;
			//@JAPR
			//$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strCurrPath = "surveydocuments/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strZipPath = "survey_".$anInstance->SurveyID;
			$zipFile['fullPath'] = $file;
			$zipFile['fileName'] = $strZipFileName;			
		}
		else
		{
			$file = $zipFile['fullPath'];			
//			var_dump($file);
			$strZipFileName = $zipFile['fileName'];
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strFullZipFileName = $strLogPath.$strZipFileName;
			//@JAPR
			//$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strCurrPath = "surveydocuments/".$_SESSION["PABITAM_RepositoryName"].'/';
			$strZipPath = "survey_".$anInstance->SurveyID;
		}
				
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		//@JAPR 2015-02-10: Agregado el control de errores
		//if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		//No estoy seguro si tiene o no algo que ver, pero con ZIPARCHIVE::OVERWRITE no me estaba creando el archivo cuando no existía, siendo
		//que en otro server equivalente si lo hacía, se cambió a ZIPARCHIVE::CREATE y funcionó, antes de eso marcaba error 11 (ZIPARCHIVE::ER_OPEN)
		//y no importa si existe o no el archivo, OVERWRITE continuaba marcando el error
		if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::CREATE))!==TRUE)
		{
			$intFailures = 1;
		    echo("(".__METHOD__.") "."Cannot create zip file <$strFullZipFileName>, error # {$intErrCode}");
		}		
		//GCRUZ 2015-09-01. Si no hay documentos que consultar, no crear el directorio y regresar el zip vacío
		if ($aRS->_numOfRows == 0)
		{			
			$aZipObj->close(); 
			//@ears 2016-08-05
			//si en la bitàcora de capturas hay mas de una forma, se analiza forma por forma, por lo que en caso de que exista
			//alguna que no considere documentos se contará para evaluar al final si se manda mensaje de que no se generará zip de documentos, esto es solo
			//si el total de las formas es igual al total de las formas sin documentos, mientras en cada recorrido se hace return
			//$bOutputFile = false (cuando es mas de una forma a analizar)
			//$bOutputfile = true (cuando solo es una forma)
			if (!$bOutputFile) { 
				$nSurveysD++;
				//return $zipFile;
				//echo("(".__METHOD__.") "."No photo file was compressed for this survey");
				if ($nSurveys == $nSurveysD) {
					echo("(".__METHOD__.") "."No document file was compressed for this survey");
					exit();				
				}
				else {
					return $zipFile;
				}
				
			}
			else {
				echo("(".__METHOD__.") "."No document file was compressed for this survey");
				exit();								
			}	
	 		
		}
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					//$blnValid = ($questionObj->HasReqPhoto > 0);
					$blnValid = ($questionObj->HasReqDocument > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
			
			$arrayDynSectionValues = array();
			//$strPhotoName = (string)$aRS->fields["pathimage"];
			$strPhotoName = (string)$aRS->fields["pathdocument"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["surveykey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
				if (!is_null($anInstance->FirstCatQuestion) && false)
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
*/
				//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
				$strFirstCatQuestionAttrib = '';
				if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
				{
					global $strNewAttribSep;
					$fieldAttrib = strtolower(BITAMQuestion::$DescFieldCatMember);
					if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
					{
						$arrCatStructValues = explode($strNewAttribSep, $aRS->fields["attributedesc"]);
						foreach ($arrCatStructValues as $arrKey => $strCatMemberDef) {
							$arrCatMemberParts = explode("_", $strCatMemberDef, 3);
							$strKey = (string) @$arrCatMemberParts[0];
							$strKey = strpos($strKey, "*");
							$strDataSourceMemberID = (int) @$arrCatMemberParts[1];
							$strDataSourceMemberName = (string) @$arrCatMemberParts[2];
							//Si encuentra una definición inválida debe terminar
							if ($strDataSourceMemberID <= 0) {
								break;
							}
							$strAttribValue = (string) @$aRS->fields[$fieldAttrib.$strDataSourceMemberID];
							if (trim($strAttribValue) == 'NA' || trim($strAttribValue) == '')
								continue;

							$strFirstCatQuestionAttrib .= $strDataSourceMemberName.'_'.$strAttribValue.'_';

							//Al encontrar el atributo de la pregunta debe terminar, ya que no habrá mas valores a mostrar para ella
							if ($strKey !== false) {
								break;
							}
						}
					}
				}
				else 
				{
					//$strNewFileName = "photo_";
					$strNewFileName = "document_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->AttributeName;
					if (trim($strQuestionText) == '')
					{
						$strQuestionText = $arrQuestionsColl[$intQuestionID]->QuestionText;
					}
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//GCRUZ 2015-11-25. Usar Pathimage en ves de armar el nombre de la imagen
//				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				//$strNewFileName .= $strFirstCatQuestionAttrib.'_'.$strQuestionText.'_'.(string)str_replace('survey_'.$aRS->fields["surveyid"], '', $aRS->fields["pathimage"]);
				$strNewFileName .= $strFirstCatQuestionAttrib.'_'.$strQuestionText.'_'.(string)str_replace('survey_'.$aRS->fields["surveyid"], '', $aRS->fields["pathdocument"]);
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			$aZipObj->close();
			if (!$bOutputFile)
				return $zipFile;
			//echo("(".__METHOD__.") "."No photo file was compressed for this survey");
				echo("(".__METHOD__.") "."No document file was compressed for this survey");
//			var_dump($arrFailedPicts);
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			if ($bOutputFile)
			{
				/*
				//$strZipData = $aZipObj->file();
				header("Content-type: application/force-download");
				header("Content-Transfer-Encoding: Binary");
				//header("Content-length: ".strlen($strZipData));
				header("Content-length: ".filesize($file));
				header("Content-Type: application/octet-stream");
				header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Transfer-Encoding: binary");
				header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
				header("Content-Length: ".filesize($file));

				readfile($file);
			}
			else
				return $zipFile;
		}
		exit();
	} //		
	//@ears 2016-07-18
	
	//@ears 2016-07-18
	//basado en NewInstanceToCompressToFormsLog
	static function NewInstanceToCompressDoc($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $searchForm = '')
	{

		require_once('survey.inc.php');		
		
		global $nSurveys; 
		
		$date_begin = (isset($_REQUEST['startDate']) && trim($_REQUEST['startDate']) != '')? date('Y-m-d', strtotime($_REQUEST['startDate'])).' 00:00:00': null;
		$date_end = (isset($_REQUEST['endDate']) && trim($_REQUEST['endDate']) != '')? date('Y-m-d', strtotime($_REQUEST['endDate'])).' 23:59:59': null;	
		$userIDs = (isset($_REQUEST['userID']) && trim($_REQUEST['userID']) != '')? explode(',', $_REQUEST['userID']): array();	
		$dateTwoState = (isset($_REQUEST['dateTwoState']) && trim($_REQUEST['dateTwoState']) != '')? $_REQUEST['dateTwoState'] : '';
		$searchForm = (isset($_REQUEST['searchForm']) && trim($_REQUEST['searchForm']) != '')? $_REQUEST['searchForm'] : '';

		switch($dateTwoState)
		{
			case 'T':
				$date_begin = date('Y-m-d').' 00:00:00';
				$date_end = date('Y-m-d').' 23:59:59'; 
				break;
			case 'W':
				$weekDay = intval(date('w'));
				$date_begin = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
				$date_end = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
				break; 
			case 'M':
				$tDay = date('t');
				$date_begin = date('Y-m-').'01 00:00:00';
				$date_end = date('Y-m-').$tDay.' 23:59:59';
				break;
			case 'Y':
				$date_begin = date('Y-').'01-01 00:00:00';
				$date_end = date('Y-').'12-31 23:59:59';
				break;
		}
						
		if (trim($aSurveyID) == '')
		{

			$anADOConnection = $aRepository->DataADOConnection;
			$arrSurveyIDs = array();
	
			if (!isset($userID) || $userID == '') {
				//@ears 2016-07-15 modificaciones para filtar formas y no cargar todas 			
				//Obtener datos standard de la forma
				$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 ORDER BY SurveyID";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				while (!$aRS->EOF) {
					$arrSurveyIDs[] = $aRS->fields['SurveyID'];	
					$aRS->MoveNext();
					
				}	
			}			
			else {
				foreach($userIDs as $iUserId => $aUser) {
					//El query se filtra por la combinación User y SurveyDate
					$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 AND UserID = '" .$aUser."' ORDER BY SurveyID";
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					while (!$aRS->EOF) {
						$arrSurveyIDs[] = $aRS->fields['SurveyID'];	
						$aRS->MoveNext();
					}
					//break;	
				}							
			}											
		}
		else
			$arrSurveyIDs = explode(',', $aSurveyID);
			
		$bOutputFile = true;
		if (count($arrSurveyIDs) > 1) 
			$bOutputFile = false;
		$zipFile = array();
		$nSurveys = count ($arrSurveyIDs);
		
		if ($nSurveys == 0) { 				
				echo("(".__METHOD__.") "."No document file");
				exit();							
		}		
		
		foreach ($arrSurveyIDs as $surveyID)
		{
			$survey = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
			if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
				continue;
			//$nSurveys = count ($arrSurveyIDs);
			//$zipFile = BITAMReportCollection::NewInstanceToCompressDocV6($aRepository, $surveyID, $attribSelected, $startDate, $endDate, $userID, $searchForm, $bOutputFile, $zipFile);
			$zipFile = BITAMReportCollection::NewInstanceToCompressDocV6($aRepository, $surveyID, $attribSelected, $date_begin, $date_end, $userID, $searchForm, $bOutputFile, $zipFile);
		}		
		
//		var_dump($arrFiles);
//		exit;
		$sfile = $zipFile['fileName'];
		$spathfile = $zipFile['fullPath'];
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/zip");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		readfile($spathfile);
		exit;

		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$strText = $anInstance->Questions[$i]->getAttributeName();
			
			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerDocument';
		
		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
			//@JAPR
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = " . $anInstance->Repository->DataADOConnection->Quote($userID);
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathDocument ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToCompress: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);

		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		//$file = tempnam("Survey".$anInstance->SurveyID."Photos", "zip");
		$file = tempnam("Survey".$anInstance->SurveyID."Documents", "zip");
		//$strZipFileName = "Survey".$anInstance->SurveyID."Photos.zip";
		$strZipFileName = "Survey".$anInstance->SurveyID."Documents.zip";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullZipFileName = $strLogPath.$strZipFileName;
		//@JAPR
		//$strCurrPath = "surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		$strCurrPath = "surveydocuments/".$_SESSION["PABITAM_RepositoryName"].'/';
		$strZipPath = "survey_".$anInstance->SurveyID;
		//$aZipObj = new zip();
		$aZipObj = new ZipArchive();
		//@JAPR 2015-02-10: Agregado el control de errores
		//if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		//No estoy seguro si tiene o no algo que ver, pero con ZIPARCHIVE::OVERWRITE no me estaba creando el archivo cuando no existía, siendo
		//que en otro server equivalente si lo hacía, se cambió a ZIPARCHIVE::CREATE y funcionó, antes de eso marcaba error 11 (ZIPARCHIVE::ER_OPEN)
		//y no importa si existe o no el archivo, OVERWRITE continuaba marcando el error
		if (($intErrCode = $aZipObj->open($file, ZIPARCHIVE::OVERWRITE))!==TRUE)
		{
			$intFailures = 1;
		    echo("(".__METHOD__.") "."Cannot create zip file <$strFullZipFileName>, error # {$intErrCode}");
		}
		
		$aZipObj->addEmptyDir($strZipPath);
		//$aZipObj->add_dir($strZipPath);
		$strZipPath = $strZipPath.'/';
		ob_start();
		ob_clean();
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					//$blnValid = ($questionObj->HasReqPhoto > 0);
					$blnValid = ($questionObj->HasReqDocument > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
			
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathDocument"];
			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					//$strNewFileName = "photo_";
					$strNewFileName = "document_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->getAttributeName();
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
				$strZipError = '';
				$aZipObj->addFile($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				//$aZipObj->add_file($strCurrPath.$strPhotoName, $strZipPath.$strNewFileName);
				$strZipError = ob_get_contents();
				ob_clean();
				$blnError = ($strZipError != '');
				if ($blnError)
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = $strZipError;
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		ob_end_clean();
		
		if($intFailures == $intCount)
		{
			//echo("(".__METHOD__.") "."No photo file was compressed for this survey");
			echo("(".__METHOD__.") "."No document file was compressed for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe compression process failed for the following documents: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			$aZipObj->close();
			/*
			//$strZipData = $aZipObj->file();
			header("Content-type: application/force-download");
			header("Content-Transfer-Encoding: Binary");
			//header("Content-length: ".strlen($strZipData));
			header("Content-length: ".filesize($file));
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\"");
			*/
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strZipFileName."\";");
			header("Content-Length: ".filesize($file));

			readfile($file);
		}
		exit();
	}
	//@ears 2016-07-18
			
	//@ 2012-04-12: Agregado el método que genera el archivo .pptx que contiene las imagenes del Reporte especificado
	static function NewInstanceToPPTX($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
    	$sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$strText = $anInstance->Questions[$i]->getAttributeName();
			
			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';

		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToPPTX: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		// pptx stuff
	    set_include_path(get_include_path() . PATH_SEPARATOR . './phppowerpoint/Classes/');
	    require_once("PHPPowerpoint.php");
	    require_once('PHPPowerPoint/IOFactory.php');
	    
	    $slideWidth = 1024;
	    $slideHeight = 768;
	    $titleHeight = 38;
	    $fontSize = 16;
	    
	    $powerpoint = new PHPPowerPoint();
	    
	    //$powerpoint->removeSlideByIndex(0);
		
	    // Create slide
	    //$currentSlide = $powerpoint->createSlide();
	    
	    // first slide
	    $currentSlide = $powerpoint->getActiveSlide();
		
	    $powerpoint->setSlideWidth(round($slideWidth/1.333333333));
	    $powerpoint->setSlideHeight(round($slideHeight/1.333333333));
		
	    // add the date
	    $shape = $currentSlide->createRichTextShape();
	    $shape->setWidth($slideWidth);
	    $shape->setHeight($titleHeight);
	    // create the text run
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($aSurvey->SurveyName);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(32);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FF000000'));
	    $shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
	    //$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_BOTTOM);
	    $shape->setOffsetY(300);
	    $shape->createBreak();
	    $shape->createBreak();
		
	    $strDatesRange = "";
	    if (strlen($startDate) > 0 && strlen($endDate) > 0)
	    {
	      $strDatesRange = date("F d, Y", strtotime($startDate))."   -    ".date("F d, Y", strtotime($endDate));
	    }
	    elseif (strlen($startDate) > 0)
	    {
	      $strDatesRange = "From: ".date("F d, Y", strtotime($startDate));
	    }
	    elseif (strlen($endDate) > 0)
	    {
	      $strDatesRange = "To: ".date("F d, Y", strtotime($endDate));
	    }
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($strDatesRange);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(26);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
		
	    //Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("./log", "pptx");
		$strPPTXFileName = "Survey".$anInstance->SurveyID."Photos.pptx";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullPPTXFileName = $strLogPath.$strPPTXFileName;
		//@JAPR
		$strCurrPath = "./surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
		
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];

			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->getAttributeName();
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
        		$strNewFileName .= $strQuestionText;
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);

				// sbf: agregar imagen
				if (file_exists($strCurrPath.$strPhotoName))
				{
					// add a new slide
					$currentSlide = $powerpoint->createSlide();
					$powerpoint->setSlideWidth(round($slideWidth/1.333333333));
					$powerpoint->setSlideHeight(round($slideHeight/1.333333333));
					
					// create the drawing shape
					$shape = $currentSlide->createDrawingShape();
					$shape->setName($strPhotoName);
					$shape->setDescription($strNewFileName);
					$sPathPict = $strCurrPath.$strPhotoName;
					$shape->setPath($sPathPict);
					$shape->setOffsetY($titleHeight);
					
					// check image size
					$maxwidth = $slideWidth;
					$maxheight = $slideHeight - $titleHeight * 2;
					if (($img_info = @getimagesize($sPathPict)) !== false)
					{
						//@JAPR 2015-02-06: Agregado soporte para php 5.6
						$aImg = explode(' ',$img_info[3]);
						$wImg = str_replace("width=",'',$aImg[0]);
						$wImg = (float)str_replace('"','',$wImg);
						$hImg = str_replace("height=",'',$aImg[1]);
						$hImg = (float)str_replace('"','',$hImg);
						if ($wImg > $maxwidth || $hImg > $maxheight)
						{
						  $shape->setResizeProportional(true);
						  $shape->setWidthAndHeight($maxwidth, $maxheight);
						  $wImg = $shape->getWidth(); //$maxwidth;
						  $hImg = $shape->getHeight(); //$maxheight;
						}
						// center image
						$shape->setOffsetX(($maxwidth - $wImg) / 2);
						$shape->setOffsetY($titleHeight + ($maxheight - $hImg) / 2);
						//$shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
						//$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_CENTER);
					}
					
					// create the text shape
					$shape = $currentSlide->createRichTextShape();
					$shape->setWidth($slideWidth);
					$shape->setHeight($titleHeight);
					// background-color
					//          $shape->getFill()->setFillType(PHPPowerPoint_Style_Fill::FILL_SOLID);
					//          $shape->getFill()->getStartColor()->setRGB('4F81BD');
					// create the text run
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$textRun = $shape->createTextRun($strNewFileName);
					$textRun->getFont()->setName('Calibri');
					$textRun->getFont()->setSize($fontSize);
					$textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
				}
				else 
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = "File not exists";
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		
		if($intFailures == $intCount)
		{
			echo("(".__METHOD__.") "."No photo file was included for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe pptx creation process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			// save pptx
			$objWriter = PHPPowerPoint_IOFactory::createWriter($powerpoint, 'PowerPoint2007');
			$objWriter->save($file);
			@rename($file, $file .".pptx");
			$file .= ".pptx";
			$powerpoint = null;
		  	
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strPPTXFileName."\";");
			header("Content-Length: ".filesize($file));
			
			readfile($file);
		}
		exit();
	}
	//@SBF

	/*@AAL 22/06/2015: Modificado, el UserID ya no sera un entero, ahora sera un string (el Email)*/
	//GCRUZ 2015-07-30.
	static function NewInstanceToPPTXToFormsLogV6($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $bOutputFile = true)
	{
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
    	$sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			if(trim($anInstance->Questions[$i]->AttributeName)!="")
			{
				$strText = $anInstance->Questions[$i]->AttributeName;
			}
			else
			{
				$strText = $anInstance->Questions[$i]->QuestionText;
			}

			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';

		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate.' 00:00:00');
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate.' 23:59:59');
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$userID = explode(',', $userID);
			$where.=" ".$aSurvey->SurveyTable.".UserEMail IN ('" . implode("','", $userID). "')";
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".SurveyKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".UserEMail, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, 'FactKeyDimVal', ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";

		$fromFirstCatQuestion = '';
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
*/
			//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
			if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
			{
				$sql .= ', '.$anInstance->FirstCatQuestion->QuestionDetTable.'.* ';
				$fromFirstCatQuestion = ', '.$anInstance->FirstCatQuestion->QuestionDetTable;
			}
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens && false)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6.
		$sql .= $fromFirstCatQuestion;

		//GCRUZ 2015-11-25. No hacer AND con mainfactkey, sino no muestra secciones multiregistro
//		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		$sql .= " WHERE ".$aSurvey->SurveyTable.".SurveyKey = ".$strImgsTable.".FactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens && false)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
*/
		//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
		if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
		{
			$sql .= ' AND '.$aSurvey->SurveyTable.'.SurveyKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
			$sql .= ' AND '.$strImgsTable.'.FactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
//			$sql .= ' AND '.$strImgsTable.'.MainFactKey = '.$anInstance->FirstCatQuestion->QuestionDetTable.'.SurveyKey';
		}

		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToPPTX: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);

//		echo "<pre>";
//		var_dump($sql);
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

//		var_dump($aRS);
//		exit;
		
		// pptx stuff
	    set_include_path(get_include_path() . PATH_SEPARATOR . './phppowerpoint/Classes/');
	    require_once("PHPPowerpoint.php");
	    require_once('PHPPowerPoint/IOFactory.php');
	    
	    $slideWidth = 1024;
	    $slideHeight = 768;
	    $titleHeight = 38;
	    $fontSize = 16;
	    
	    $powerpoint = new PHPPowerPoint();
	    
	    //$powerpoint->removeSlideByIndex(0);
		
	    // Create slide
	    //$currentSlide = $powerpoint->createSlide();
	    
	    // first slide
	    $currentSlide = $powerpoint->getActiveSlide();
		
	    $powerpoint->setSlideWidth(round($slideWidth/1.333333333));
	    $powerpoint->setSlideHeight(round($slideHeight/1.333333333));
		
	    // add the date
	    $shape = $currentSlide->createRichTextShape();
	    $shape->setWidth($slideWidth);
	    $shape->setHeight($titleHeight);
	    // create the text run
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($aSurvey->SurveyName);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(32);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FF000000'));
	    $shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
	    //$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_BOTTOM);
	    $shape->setOffsetY(300);
	    $shape->createBreak();
	    $shape->createBreak();
		
	    $strDatesRange = "";
	    if (strlen($startDate) > 0 && strlen($endDate) > 0)
	    {
	      $strDatesRange = date("F d, Y", strtotime($startDate))."   -    ".date("F d, Y", strtotime($endDate));
	    }
	    elseif (strlen($startDate) > 0)
	    {
	      $strDatesRange = "From: ".date("F d, Y", strtotime($startDate));
	    }
	    elseif (strlen($endDate) > 0)
	    {
	      $strDatesRange = "To: ".date("F d, Y", strtotime($endDate));
	    }
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($strDatesRange);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(26);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
		
	    //Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("./log", "pptx");
		$strPPTXFileName = "Survey".$anInstance->SurveyID."Photos.pptx";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullPPTXFileName = $strLogPath.$strPPTXFileName;
		//@JAPR
		$strCurrPath = "./surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//GCRUZ 2016-02-23. Validar que exista la pregunta. Pudieron haberla borrado, pero sigue existiendo en la tabla de SI_SV_SurveyAnswerImage
			if (is_null($questionObj))
			{
				$aRS->MoveNext();
				continue;
			}
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
		
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];

			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["surveykey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				//GCRUZ 2015-11-24. FirstCatQuestion. Código de V3. Ya no se usa
/*
				if (!is_null($anInstance->FirstCatQuestion) && false)
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
*/
				//GCRUZ 2015-11-24. FirstCatQuestion. Nuevo código V6. Restricción: sólo para secciones estándar
				$strFirstCatQuestionAttrib = '';
				if (!is_null($anInstance->FirstCatQuestion) && $anInstance->FirstCatQuestion->SectionType == sectNormal)
				{
					global $strNewAttribSep;
					$fieldAttrib = strtolower(BITAMQuestion::$DescFieldCatMember);
					if ($anInstance->FirstCatQuestion->SectionType == sectNormal)
					{
						$arrCatStructValues = explode($strNewAttribSep, $aRS->fields["attributedesc"]);
						foreach ($arrCatStructValues as $arrKey => $strCatMemberDef) {
							$arrCatMemberParts = explode("_", $strCatMemberDef, 3);
							$strKey = (string) @$arrCatMemberParts[0];
							$strKey = strpos($strKey, "*");
							$strDataSourceMemberID = (int) @$arrCatMemberParts[1];
							$strDataSourceMemberName = (string) @$arrCatMemberParts[2];
							//Si encuentra una definición inválida debe terminar
							if ($strDataSourceMemberID <= 0) {
								break;
							}
							$strAttribValue = (string) @$aRS->fields[$fieldAttrib.$strDataSourceMemberID];
							if (trim($strAttribValue) == 'NA' || trim($strAttribValue) == '')
								continue;

							$strFirstCatQuestionAttrib .= ' '.$strDataSourceMemberName.' = '.$strAttribValue.' ';

							//Al encontrar el atributo de la pregunta debe terminar, ya que no habrá mas valores a mostrar para ella
							if ($strKey !== false) {
								break;
							}
						}
					}
				}
				else 
				{
					$strNewFileName = "photo : ";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->AttributeName;
					if (trim($strQuestionText) == '')
					{
						$strQuestionText = $arrQuestionsColl[$intQuestionID]->QuestionText;
					}
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
        		//GCRUZ 2015-07-31. Agregar fecha de la captura y usuario
				$strNewFileName .= (string)$aRS->fields["dateid"].' : '.(string)$aRS->fields["useremail"].' : ';
				$strNewFileName .= $strFirstCatQuestionAttrib.','.$strQuestionText.' : '.(string)str_replace('survey_'.$aRS->fields["surveyid"], '', $aRS->fields["pathimage"]);
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);
//var_dump($strCurrPath.$strPhotoName);
				// sbf: agregar imagen
				if (file_exists($strCurrPath.$strPhotoName))
				{
					// add a new slide
					$currentSlide = $powerpoint->createSlide();
					$powerpoint->setSlideWidth(round($slideWidth/1.333333333));
					$powerpoint->setSlideHeight(round($slideHeight/1.333333333));
					
					// create the drawing shape
					$shape = $currentSlide->createDrawingShape();
					$shape->setName($strPhotoName);
					$shape->setDescription($strNewFileName);
					$sPathPict = $strCurrPath.$strPhotoName;
					$shape->setPath($sPathPict);
					$shape->setOffsetY($titleHeight);
					
					// check image size
					$maxwidth = $slideWidth;
					$maxheight = $slideHeight - $titleHeight * 2;
					if (($img_info = @getimagesize($sPathPict)) !== false)
					{
						//@JAPR 2015-02-06: Agregado soporte para php 5.6
						$aImg = explode(' ',$img_info[3]);
						$wImg = str_replace("width=",'',$aImg[0]);
						$wImg = (float)str_replace('"','',$wImg);
						$hImg = str_replace("height=",'',$aImg[1]);
						$hImg = (float)str_replace('"','',$hImg);
						if ($wImg > $maxwidth || $hImg > $maxheight)
						{
						  $shape->setResizeProportional(true);
						  $shape->setWidthAndHeight($maxwidth, $maxheight);
						  $wImg = $shape->getWidth(); //$maxwidth;
						  $hImg = $shape->getHeight(); //$maxheight;
						}
						// center image
						$shape->setOffsetX(($maxwidth - $wImg) / 2);
						$shape->setOffsetY($titleHeight + ($maxheight - $hImg) / 2);
						//$shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
						//$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_CENTER);
					}
					
					// create the text shape
					$shape = $currentSlide->createRichTextShape();
					$shape->setWidth($slideWidth);
					$shape->setHeight($titleHeight);
					// background-color
					//          $shape->getFill()->setFillType(PHPPowerPoint_Style_Fill::FILL_SOLID);
					//          $shape->getFill()->getStartColor()->setRGB('4F81BD');
					// create the text run
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$textRun = $shape->createTextRun($strNewFileName);
					$textRun->getFont()->setName('Calibri');
					$textRun->getFont()->setSize($fontSize);
					$textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FF000000'));
				}
				else 
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = "File not exists";
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}

//		var_dump($arrProcessedPicts);
//		exit;
		
		if($intFailures == $intCount)
		{
			$file .= ".pptx";
			if (!$bOutputFile)
				return (array('fullFilePath' => $file, 'fileName' => $strPPTXFileName));
			echo("(".__METHOD__.") "."No photo file was included for this survey");
		}

		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe pptx creation process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			// save pptx
			$objWriter = PHPPowerPoint_IOFactory::createWriter($powerpoint, 'PowerPoint2007');
			$objWriter->save($file);
			@rename($file, $file .".pptx");
			$file .= ".pptx";
			$powerpoint = null;

			if (!$bOutputFile)
				return (array('fullFilePath' => $file, 'fileName' => $strPPTXFileName));
		  	
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strPPTXFileName."\";");
			header("Content-Length: ".filesize($file));
			
			readfile($file);
		}
		exit();
	}
	//@SBF

	/*@AAL 22/06/2015: Modificado, el UserID ya no sera un entero, ahora sera un string (el Email)*/
	static function NewInstanceToPPTXToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $searchForm = '')
	{
		//GCRUZ 2015-07-31. Soportar multiples surveyID
//		echo "<pre>";
		require_once('survey.inc.php');
		if (trim($aSurveyID) == '')
		{
			//Cargar TODAS las surveyIDs
			$loadedSurveys = BITAMSurvey::getSurveys($aRepository, true);
		//	die('SurveyID not found');
			$arrSurveyIDs = array();
			foreach($loadedSurveys as $skey => $asurvey)
				$arrSurveyIDs[] = $skey;
		}
		else
		$arrSurveyIDs = explode(',', $aSurveyID);
		$bOutputFile = true;
		if (count($arrSurveyIDs) > 1)
			$bOutputFile = false;
		$arrFiles = array();
		foreach ($arrSurveyIDs as $surveyID)
		{
			$survey = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
			if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
				continue;
			$arrFiles[] = BITAMReportCollection::NewInstanceToPPTXToFormsLogV6($aRepository, $surveyID, $attribSelected, $startDate, $endDate, $userID, $bOutputFile);
		}
//		var_dump($arrFiles);
//		exit;
		$zip = new ZipArchive;
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$zipPath = GeteFormsLogPath();
		//@JAPR
		$zipFile = date('YmdHis').'_SurveyPhotos.zip';
		$zipRes = $zip->open($zipPath.$zipFile, ZipArchive::CREATE);
		if ($zipRes === TRUE) {
			foreach ($arrFiles as $aPPTFile)
			{
				if (file_exists($aPPTFile['fullFilePath']))
				$zip->addFile($aPPTFile['fullFilePath'], $aPPTFile['fileName']);
			}
			$zip->close();
		} else {
			die ('failed to open Zip File. ErrCode:'.$zipRes);
		}
		$sfile = $zipFile;
		$spathfile = $zipPath.$zipFile;
		if (!isset($spathfile) || !file_exists($spathfile))
			die('Error creating Photo file report: no images found.');
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/zip");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		readfile($spathfile);
		exit();
		
		global $queriesLogFile;
		
		//require_once('zip.inc.php');
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
    	$sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$spathfile = $zipPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		$arrQuestionsColl = array();
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
			$strText = $anInstance->Questions[$i]->getAttributeName();
			
			$line.=',"'.str_replace('"', '""', $strText).'"';
			$arrQuestionsColl[$anInstance->Questions[$i]->QuestionID] = $anInstance->Questions[$i];
		}

		array_push($afile, $line);

		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$strImgsTable = 'SI_SV_SurveyAnswerImage';

		//Filtro de combos de atributos (si es que existen)
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$fromCat = '';
		$where = '';
		$arrayTables = array();
		//@JAPR
		if($attribSelected!=null)
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				if ($aSurvey->DissociateCatDimens) {
					//Si hay algún filtro habilitado entonces tiene que incluir a la tabla de hechos para permitir el join con las dimensiones
					//independientes de los atributos del catálogo
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
							}
							
							$where .= " AND ".$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
	
					$where.=" AND ";
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
			}
		}

		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID != "")
		{
			$where.=" AND ";
			$where.=" ".$aSurvey->SurveyTable.".UserID = " . $anInstance->Repository->DataADOConnection->Quote($userID);
		}
		
		$sql = "SELECT ".$aSurvey->SurveyID." AS SurveyID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal, ".$strImgsTable.".QuestionID, ".$strImgsTable.".PathImage ";
		if (!is_null($anInstance->FirstCatQuestion))
		{
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if ($aSurvey->DissociateCatDimens) {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						//@JAPR 2013-05-13: Corregido un bug, estaba reseteando la variable en lugar de concatenarla
						$where .= " AND ".$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
					}
					
					$catParentID = $memberInfo["IndDimID"];
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql .= ', '.$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$where .= " AND B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
					}
				}
			}
			else {
				foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
				{
					$sql .= ', '.$anInstance->CatTableName.".".$memberInfo["FieldName"];
				}
			}
			//@JAPR
		}
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", ".$strImgsTable;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= ", ".$anInstance->CatTableName;
		}
		$sql .= " WHERE ".$aSurvey->SurveyTable.".FactKeyDimVal = ".$strImgsTable.".FactKey AND ".$aSurvey->SurveyTable.".FactKey = ".$strImgsTable.".MainFactKey AND ".$strImgsTable.".SurveyID = ".$anInstance->SurveyID.$where;
		if (!is_null($anInstance->FirstCatQuestion) && !$aSurvey->DissociateCatDimens)
		{
			$sql .= " AND ".$aSurvey->SurveyTable.".".$anInstance->FirstCatQuestion->SurveyField." = ".$anInstance->CatTableName."."."RIDIM_".$anInstance->CatParentID."KEY";
		}
		$sql .= " ORDER BY 7, 2";	//FactKeyDimVal, Factkey
		$aLogString="\r\n--------------------------------------------------------------------------------------";
		$aLogString.="\r\nBITAMReport->NewInstanceToPPTX: \r\n";
		$aLogString.=$sql;
		@updateQueriesLogFile($queriesLogFile,$aLogString);
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		// pptx stuff
	    set_include_path(get_include_path() . PATH_SEPARATOR . './phppowerpoint/Classes/');
	    require_once("PHPPowerpoint.php");
	    require_once('PHPPowerPoint/IOFactory.php');
	    
	    $slideWidth = 1024;
	    $slideHeight = 768;
	    $titleHeight = 38;
	    $fontSize = 16;
	    
	    $powerpoint = new PHPPowerPoint();
	    
	    //$powerpoint->removeSlideByIndex(0);
		
	    // Create slide
	    //$currentSlide = $powerpoint->createSlide();
	    
	    // first slide
	    $currentSlide = $powerpoint->getActiveSlide();
		
	    $powerpoint->setSlideWidth(round($slideWidth/1.333333333));
	    $powerpoint->setSlideHeight(round($slideHeight/1.333333333));
		
	    // add the date
	    $shape = $currentSlide->createRichTextShape();
	    $shape->setWidth($slideWidth);
	    $shape->setHeight($titleHeight);
	    // create the text run
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($aSurvey->SurveyName);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(32);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FF000000'));
	    $shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
	    //$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_BOTTOM);
	    $shape->setOffsetY(300);
	    $shape->createBreak();
	    $shape->createBreak();
		
	    $strDatesRange = "";
	    if (strlen($startDate) > 0 && strlen($endDate) > 0)
	    {
	      $strDatesRange = date("F d, Y", strtotime($startDate))."   -    ".date("F d, Y", strtotime($endDate));
	    }
	    elseif (strlen($startDate) > 0)
	    {
	      $strDatesRange = "From: ".date("F d, Y", strtotime($startDate));
	    }
	    elseif (strlen($endDate) > 0)
	    {
	      $strDatesRange = "To: ".date("F d, Y", strtotime($endDate));
	    }
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	    $textRun = $shape->createTextRun($strDatesRange);
	    $textRun->getFont()->setName('Calibri');
	    $textRun->getFont()->setSize(26);
	    $textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
		
	    //Contiene la lista de los nombres originales de imagenes, ya que debido a las secciones dinámicas, hay casos donde MainFactKeys diferentes
		//contienen la misma imagen, pero no tenemos por qué duplicarla dentro del .zip
		$arrProcessedPicts = array();
		$arrFailedPicts = array();
		//Arrays para el reemplazo de caracteres inválidos
		$arrEmpty = array();
		$arrSearch = array('\'','/',':','*','?','"','<','>','|');
		$intCount = 0;
		$intFailures = 0;
		$file = tempnam("./log", "pptx");
		$strPPTXFileName = "Survey".$anInstance->SurveyID."Photos.pptx";
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strFullPPTXFileName = $zipPath.$strPPTXFileName;
		//@JAPR
		$strCurrPath = "./surveyimages/".$_SESSION["PABITAM_RepositoryName"].'/';
		
		while (!$aRS->EOF)
		{
			$questionObj = BITAMQuestion::NewInstanceWithID($anInstance->Repository, (int)$aRS->fields["questionid"]);
			//@JAPR 2013-05-14: Corregido un bug, la condición que se agregó excluía a todas las preguntas que no son exactamente foto, por lo
			//que la mayoría de los otros tipos de preguntas con foto opcional no estaban incluyendo sus imagenes
			$blnValid = true;
			switch ($questionObj->QTypeID) {
				case qtpShowValue:
//				case qtpSketch:
				case qtpSignature:
					$blnValid = false;
					break;
				
				default:
					$blnValid = ($questionObj->HasReqPhoto > 0);
					break;
			}
			if (!$blnValid) {
				$aRS->MoveNext();
				continue;
			}
			/*
			if($questionObj->QTypeID != qtpPhoto) {
				$aRS->MoveNext();
				continue;
			}
			*/
			//@JAPR
		
			$arrayDynSectionValues = array();
			$strPhotoName = (string)$aRS->fields["pathimage"];

			if (!isset($arrProcessedPicts[$strPhotoName]))
			{
				$factKey = (int)$aRS->fields["factkey"];
				$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
				$intQuestionID = (int)$aRS->fields["questionid"];
				$intSurveyID = (int)$aRS->fields["surveyid"];
				$strNewFileName = '';
				if (!is_null($anInstance->FirstCatQuestion))
				{
					//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$catParentID = $memberInfo["IndDimID"];
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							$strNewFileName .= $aRS->fields[strtolower($attribField)]."_";
						}
					}
					else {
						foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
						{
							$strNewFileName .= $aRS->fields[strtolower($memberInfo["FieldName"])]."_";
						}
					}
					//@JAPR
				}
				else 
				{
					$strNewFileName = "photo_";
				}
				
				$strQuestionText = '';
				if (isset($arrQuestionsColl[$intQuestionID]))
				{
					//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
					$strQuestionText = $arrQuestionsColl[$intQuestionID]->getAttributeName();
				}
				if (trim($strQuestionText) == '')
				{
					$strQuestionText = $intQuestionID;
				}
				
				//$strNewFileName .= $strQuestionText."_".$factKey."_".$factKeyDimVal."_".$intSurveyID.".jpg";
        		$strNewFileName .= $strQuestionText;
				$strNewFileName = str_replace($arrSearch, $arrEmpty, $strNewFileName);

				// sbf: agregar imagen
				if (file_exists($strCurrPath.$strPhotoName))
				{
					// add a new slide
					$currentSlide = $powerpoint->createSlide();
					$powerpoint->setSlideWidth(round($slideWidth/1.333333333));
					$powerpoint->setSlideHeight(round($slideHeight/1.333333333));
					
					// create the drawing shape
					$shape = $currentSlide->createDrawingShape();
					$shape->setName($strPhotoName);
					$shape->setDescription($strNewFileName);
					$sPathPict = $strCurrPath.$strPhotoName;
					$shape->setPath($sPathPict);
					$shape->setOffsetY($titleHeight);
					
					// check image size
					$maxwidth = $slideWidth;
					$maxheight = $slideHeight - $titleHeight * 2;
					if (($img_info = @getimagesize($sPathPict)) !== false)
					{
						//@JAPR 2015-02-06: Agregado soporte para php 5.6
						$aImg = explode(' ',$img_info[3]);
						$wImg = str_replace("width=",'',$aImg[0]);
						$wImg = (float)str_replace('"','',$wImg);
						$hImg = str_replace("height=",'',$aImg[1]);
						$hImg = (float)str_replace('"','',$hImg);
						if ($wImg > $maxwidth || $hImg > $maxheight)
						{
						  $shape->setResizeProportional(true);
						  $shape->setWidthAndHeight($maxwidth, $maxheight);
						  $wImg = $shape->getWidth(); //$maxwidth;
						  $hImg = $shape->getHeight(); //$maxheight;
						}
						// center image
						$shape->setOffsetX(($maxwidth - $wImg) / 2);
						$shape->setOffsetY($titleHeight + ($maxheight - $hImg) / 2);
						//$shape->getAlignment()->setHorizontal(PHPPowerPoint_Style_Alignment::HORIZONTAL_CENTER);
						//$shape->getAlignment()->setVertical(PHPPowerPoint_Style_Alignment::VERTICAL_CENTER);
					}
					
					// create the text shape
					$shape = $currentSlide->createRichTextShape();
					$shape->setWidth($slideWidth);
					$shape->setHeight($titleHeight);
					// background-color
					//          $shape->getFill()->setFillType(PHPPowerPoint_Style_Fill::FILL_SOLID);
					//          $shape->getFill()->getStartColor()->setRGB('4F81BD');
					// create the text run
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$textRun = $shape->createTextRun($strNewFileName);
					$textRun->getFont()->setName('Calibri');
					$textRun->getFont()->setSize($fontSize);
					$textRun->getFont()->setColor(new PHPPowerPoint_Style_Color('FFA7A7A7'));
				}
				else 
				{
					$arrFailedPicts[][0] = $strPhotoName;
					$arrFailedPicts[][1] = "File not exists";
					$intFailures++;
				}
				$arrProcessedPicts[$strPhotoName] = ++$intCount;
			}
			
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			$aRS->MoveNext();
		}
		
		if($intFailures == $intCount)
		{
			echo("(".__METHOD__.") "."No photo file was included for this survey");
		}
		
		if ($intFailures > 0)
		{
			$aLogString="\r\n--------------------------------------------------------------------------------------";
			$aLogString.="\r\nThe pptx creation process failed for the following pictures: \r\n";
			@updateQueriesLogFile($queriesLogFile,$aLogString);
			foreach ($arrFailedPicts as $aPictZipError)
			{
				$aLogString = implode(': ', $aPictZipError)."\r\n";
				@updateQueriesLogFile($queriesLogFile,$aLogString);
			}
		}
		
		/*
		$spathfile = getcwd().'/'.$strCurrPath.$strZipPath.$strNewFileName;
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$strNewFileName.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		readfile($spathfile);
		*/
		
		if($intFailures < $intCount)
		{
			// save pptx
			$objWriter = PHPPowerPoint_IOFactory::createWriter($powerpoint, 'PowerPoint2007');
			$objWriter->save($file);
			@rename($file, $file .".pptx");
			$file .= ".pptx";
			$powerpoint = null;
		  	
			//fix for IE catching or PHP bug issue
			header("Pragma: public");
			header("Expires: 0"); // set expiration time
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			header("Content-Transfer-Encoding: binary");
			header("Content-Disposition: attachment; filename=\"".$strPPTXFileName."\";");
			header("Content-Length: ".filesize($file));
			
			readfile($file);
		}
		exit();
	}
	//@SBF

	//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
	//Agregado el parámetro $bIncludePhotos para indicar si se debe o no agregar una columna para cada pregunta tipo Foto/Firma o que tenga foto
	//opcional configurada, de tal manera que se exporte la ruta de la imagen si es que contiene alguna
	static function NewInstanceToExportDesg($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2, $bIncludePhotos = false, $blnNewExcelMethod = false)
	{
		if($blnNewExcelMethod)
		{
			return BITAMReportCollection::NewInstanceToExportDesg2($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID, $bIncludePhotos);
		}
		
		$strOriginalWD = getcwd();
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		$line = '"User Name","Date","Latitude","Longitude"';
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		//@JAPR 2014-04-28: Si se deja un espacio, las comillas dobles aparecen en el XLS
		if (getMDVersion() >= esvAccuracyValues) {
			$line .= ',"Accuracy"';
		}
		
		//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
        //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection) 
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
		}
		//@JAPR
		
		//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
		//Carga las definiciones de catálogos utilizadas y dependiendo de las preguntas va asignando que atributos se deberían agregar a cada una,
		//ya que múltiples preguntas de catálogo en la misma encuesta sólo podrían usar atributos posteriores a los previamente usados, por lo 
		//tanto dichos atributos ni los padres no se deben mostrar en preguntas posteriores, sólo aquellos que aun no se estén desplegando
		$arrCatalogs = array();
		$arrCatalogMembers = array();
		$arrLastShownAttributeIdx = array();
		$arrAttributesByQuestion = array();
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrQuestionsByID = array();
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			$objQuestion = $anInstance->Questions[$i];
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
			//@JAPR
			
			if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode != dspMatrix)
			{
				$intCatalogID = $anInstance->Questions[$i]->CatalogID;
				$intCatMemberID = $anInstance->Questions[$i]->CatMemberID;
				if ($intCatalogID > 0 && $intCatMemberID > 0)
				{
					if (isset($arrCatalogs[$intCatalogID]))
					{
						$aCatalog = $arrCatalogs[$intCatalogID];
						$aCatalogMembersColl = $arrCatalogMembers[$intCatalogID];
						$intLastShownAttribute = $arrLastShownAttributeIdx[$intCatalogID];
					}
					else 
					{
						$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
						$aCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
						$aCatalogMembersColl = $aCatalogMembersColl->Collection;
						$intLastShownAttribute = -1;
						$arrCatalogs[$intCatalogID] = $aCatalog;
						$arrCatalogMembers[$intCatalogID] = $aCatalogMembersColl;
						$arrLastShownAttributeIdx[$intCatalogID] = $intLastShownAttribute;
					}
					
					//Primero verifica si el Atributo de esta pregunta ya había sido desplegado, si lo fué entonces se trata de un error y por
					//tanto simplemente se agregará a este atributo, de lo contrario se incluirían otros posteriores lo cual estaría incorrecto
					$blnAttributeUsed = true;
					$intNumAttributes = count($aCatalogMembersColl);
					for ($intIdx = $intLastShownAttribute + 1; $intIdx < $intNumAttributes; $intIdx++)
					{
						if ($aCatalogMembersColl[$intIdx]->MemberID == $intCatMemberID)
						{
							$blnAttributeUsed = false;
							break;
						}
					}
					
					$arrAttributesColl = array();
					//Si el atributo ya estaba usado, simplemente brincará este If para sólo agregar el atributo en cuestión, de lo contrario
					//agrega todos los padres hasta llegar al atributo de la pregunta
					if (!$blnAttributeUsed)
					{
						//Crea la entrada para esta pregunta usando su índice, el contenido será el Array de todos los Atributos que debe imprimir
						//los cuales consisten en los Atributos Padres del asociado a la pregunta, incluyendo al propio atributo de la pregunta, siemre
						//y cuando NO se hubieran desplegado ya en otra pregunta previa
						for($intLastShownAttribute = $intLastShownAttribute +1; $intLastShownAttribute < $intNumAttributes; $intLastShownAttribute++)
						{
							if ($aCatalogMembersColl[$intLastShownAttribute]->MemberID == $intCatMemberID)
							{
								break;
							}
							
							$arrAttributesColl[] = $aCatalogMembersColl[$intLastShownAttribute];
						}
					}

					//En realidad no se necesita agregar el Atributo de la pregunta, ya que el código mas abajo simplemente usará el nombre de la
					//pregunta, sólo se necesitan los atributos padre pues esos no se conocían y hay que anteponerlos a la pregunta en cuestión
					//$arrAttributesColl[] = null;	// NO es necesario activar esta línea, si el count de este array es 0 aun así funciona abajo
					
					//Agrega el atributo de la pregunta como el texto de la misma directamente
					$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID] = $arrAttributesColl;
				}
			}
		}
		//@JAPR
		
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if($blnValidQuestion) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $anInstance->Questions[$i]->getAttributeName();
				
                //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
                if ($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
                {
                    foreach ($anInstance->Questions[$i]->SelectOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
                    //Se genera un nuevo Array vacio según el tipo de respuestas múltiples para ser reutilizado al recorrer las encuestas capturadas
                    if($anInstance->Questions[$i]->QTypeID == qtpMulti && count($anInstance->Questions[$i]->SelectOptions) > 0)
                    {
                        if($anInstance->Questions[$i]->MCInputType==0)
                        {
                            //En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se toma tal cual
                            //y sólo se rellena con cadenas vacias en su valores
                            $arrAnswers = array_flip(array_values($anInstance->Questions[$i]->SelectOptions));
                        }
                        else 
                        {
                            //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                            //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                            //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                            $arrAnswers = array_values($anInstance->Questions[$i]->SelectOptions);
                        }

                        $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                        foreach ($arrAnswers as $intID => $strValue)
                        {
                            $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                        }
                    }
                    //@JAPR
                }
                else if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode == dspMatrix)
                {
                    if($anInstance->Questions[$i]->OneChoicePer==0)
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptions;
                    }
                    else 
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptionsCol;
                    }

                    foreach ($theOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                    //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                    //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                    $arrAnswers = array_values($theOptions);

                    $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                    foreach ($arrAnswers as $intID => $strValue)
                    {
                        $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                    }
                }
                else 
                {
                    //@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
                    //Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
                    //de catálogo por lo que deben ser selección sencilla exclusivamente
                    if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
                    {
                        $arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
                        if (!is_null($arrAttributesColl))
                        {
                            foreach ($arrAttributesColl as $aCatMember)
                            {
                                $line.=',"'.getQuotedStringForExport($aCatMember->MemberName).'"';
                            }
                        }
                    }
                    //@JAPR
                    $line.=',"'.getQuotedStringForExport($strText).'"';
                }
            }
            
			//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
			//Si la pregunta tiene foto configurada y se pidió incluirlas, entonces agrega una columna adicional al archivo
			if ($bIncludePhotos && $objQuestion->HasReqPhoto > 0) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                if ($objQuestion->QTypeID === qtpSignature) {
                	$strText .= ' '.translate('Signature');
                }
                else {
                	$strText .= ' '.translate('Photo');
                }
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
            
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			if ($objQuestion->HasReqComment) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                $strText .= ' '.translate('Comment');
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			
			//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
			//Verifica si esta pregunta contiene o no Score forma de calcular Score
			if ($objQuestion->HasScores) {
                $strText .= ' '.translate('Score');
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			//@JAPR
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
		$fieldAttrLatitude = '0';
		$fieldAttrLongitude = '0';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = $catTable."."."DSC_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = $catTable."."."DSC_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$fieldAttrAccuracy = ', 0';
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = ", ".$catTable."."."DSC_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		$sql.=", ".$fieldAttrLatitude.", ".$fieldAttrLongitude.$fieldAttrAccuracy;
		
		//Se modifica para usarse al leer el recordset
		$fieldAttrLatitude = '';
		$fieldAttrLongitude = '';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = "dsc_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = "dsc_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = "dsc_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		//Se forzará a utilizar la tabla de hechos para extraer la información adicional
		if ($aSurvey->FactKeyDimID > 0) {
			$fromCat.=", ".$aSurvey->FactTable." B";
			$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
			$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
			$strAnd = " AND ";
			
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			$joinField = "RIDIM_".$aSurvey->FactKeyDimID."KEY";
			$fromCat.=", ".$catTable;
			$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
		}
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//Modificada la condición para omitir el ciclo en lugar de preguntar si debe entrar o no a procesar algo
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($objQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$objQuestion->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = $aCatMember->IndDimID;
								$catTable = "RIDIM_".$memberParentID;
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$memberParentID."KEY";
									$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
									$strAnd = " AND ";
								}
							}
						}
					}
					//@JAPR
					
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
							}
						}
					}
					//@JAPR
					
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
				//@JAPR
			}
		}
		
		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					if($where!="")
					{
						$where.=" AND ";
					}
					
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
			$strAnd = " AND ";
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
			$strAnd = " AND ";
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
			$strAnd = " AND ";
		}

		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		//El filtro completo se usará tal cual entre las tablas paralela, hechos y catálogos, pero filtros adicionales ya no aplican para extraer
		//los comentarios así que esos se ignorarán
		$strJoins = $where;
		$strJoinsScores = $where;
		if ($strJoins != '') {
			$strJoins = "AND ".$strJoins;
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$blnMultiRecordSection = ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection);
		if ($aSurvey->UseStdSectionSingleRec) {
			//Si se usa el registro único, entonces se trae exclusivamente los datos de dichos registros, de lo contrario puede traer cualquier
			//registro ya que antes de este cambio en todos se grababa lo mismo para las preguntas estándar
			if ($where != "") {
				$where .= " AND ";
			}
			$where .= $aSurvey->SurveyTable.".EntrySectionID = 0";
		}
		//@JAPR
		
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
		if ($bIncludePhotos) {
			//Obtenemos el prefijo de la ruta requerida en la dimension imagen
			$script = $_SERVER["REQUEST_URI"];
			//Eliminamos la primera diagonal que separa el archivo php de la carpeta
			$position = strrpos($script, "/");
			$dir = substr($script, 0, $position);
			
			//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
			$position = strrpos($dir, "/");
			$mainDir = substr($dir, ($position+1));
			//@JAPR 2013-06-03: Corregido un bug, estaba incluyendo 2 veces la ruta del Survey
			//$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$aSurveyID."/";
			$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/";
			//@JAPR
			
			$arrAllImages = array();
			$sqlImages = "SELECT tImg.FactKey as FactKeyDimVal, tImg.MainFactKey as FactKey, tImg.SurveyID, tImg.QuestionID, tImg.PathImage 
				FROM SI_SV_SurveyAnswerImage tImg, $aSurvey->SurveyTable $fromCat 
				WHERE tImg.SurveyID = $aSurveyID AND tImg.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
				ORDER BY tImg.FactKey, tImg.MainFactKey";	// survey_4/photo_4_26_1_1.jpg
			$aRSImg = $anInstance->Repository->DataADOConnection->Execute($sqlImages);
			if ($aRSImg) {
				while (!$aRSImg->EOF) {
					$intQuestionID = (int) $aRSImg->fields["questionid"];
					$objQuestion = @$arrQuestionsByID[$intQuestionID];
					if (!is_null($objQuestion) && $objQuestion->HasReqPhoto) {
						$factKey = (int) @$aRSImg->fields["factkey"];
						$factKeyDimVal = (int) @$aRSImg->fields["factkeydimval"];
						$strFieldDocumentName = (string) @$aRSImg->fields["pathimage"];
						if (!isset($arrAllImages[$factKeyDimVal])) {
							$arrAllImages[$factKeyDimVal] = array();
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						elseif (!isset($arrAllImages[$factKeyDimVal][$intQuestionID])) {
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						
						$strFieldDocumentName2 = '';
						if (trim($strFieldDocumentName) != '') {
							$strFieldDocumentName2 = $strPhotoPrefix.$strFieldDocumentName;
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
							if (count($arrAllImages[$factKeyDimVal][$intQuestionID]) == 0) {
								$arrAllImages[$factKeyDimVal][$intQuestionID][0] = $strFieldDocumentName2;
							}
						}
					}
					$aRSImg->MoveNext();
				}
			}
		}
		
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrAllComments = array();
		$sqlComments = "SELECT tComm.FactKey as FactKeyDimVal, tComm.MainFactKey as FactKey, tComm.SurveyID, tComm.QuestionID, tComm.StrComment 
			FROM SI_SV_SurveyAnswerComment tComm, $aSurvey->SurveyTable $fromCat 
			WHERE tComm.SurveyID = $aSurveyID AND tComm.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
			ORDER BY tComm.FactKey, tComm.MainFactKey";
		$aRSComm = $anInstance->Repository->DataADOConnection->Execute($sqlComments);
		if ($aRSComm) {
			while (!$aRSComm->EOF) {
				$intQuestionID = (int) $aRSComm->fields["questionid"];
				$objQuestion = @$arrQuestionsByID[$intQuestionID];
				if (!is_null($objQuestion) && $objQuestion->HasReqComment) {
					$factKey = (int) @$aRSComm->fields["factkey"];
					$factKeyDimVal = (int) @$aRSComm->fields["factkeydimval"];
					$strComment = (string) @$aRSComm->fields["strcomment"];
						if (!isset($arrAllComments[$factKeyDimVal])) {
						$arrAllComments[$factKeyDimVal] = array();
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					elseif (!isset($arrAllComments[$factKeyDimVal][$intQuestionID])) {
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					
					//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
					//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
					//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
					if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
						//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
						//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					else {
						//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
						if (count($arrAllComments[$factKeyDimVal][$intQuestionID]) == 0) {
							$arrAllComments[$factKeyDimVal][$intQuestionID][0] = $strComment;
						}
					}
				}
				$aRSComm->MoveNext();
			}
		}
		
		//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
		$arrAllScores = array();
		$fromScores = $fromCat;
		if(!isset($arrayTables[$aSurvey->FactTable])) {
			$fromScores .= ", ".$aSurvey->FactTable." B ";
			$strJoinsScores .= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
		}
		if ($strJoinsScores != '') {
			$strJoinsScores = " WHERE ".$strJoinsScores;
		}
		
		//Los campos se deben procesar dependiendo del tipo de grabado que tiene configurado la encuesta, aunque en un query se podrían obtener
		//aplicando funciones de agrupación, se optará por procesarlos en memoria para controlar mejor los datos obtenidos
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$dynamicSectionWithMultiChoice = false;
		if ($anInstance->HasDynamicSection && !is_null($anInstance->DynamicSection)) {
			$dynamicSectionWithMultiChoice = ($anInstance->DynamicSection->ChildCatMemberID > 0);
		}
		$blnHasScoreQuestions = false;
		$strScoreFields = '';
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			if ($objQuestion->HasScores) {
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Las preguntas simple choice no deben ser de catálogo para poder tener Score
						if ($objQuestion->QTypeID == qtpSingle && $objQuestion->UseCatalog == 0 && $objQuestion->IndicatorID > 0) {
							$fieldName = '';
							$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
							if (!is_null($objBITAMIndicator)) {
								$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
							}
							if ($fieldName != '') {
								$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_0';
								$strScoreFields .= ', '.$fieldName;
								$blnHasScoreQuestions = true;
							}
						}
						break;
						
					case qtpMulti:
						//Las preguntas múltiples deben ser multi-dimensión tipo checkbox para poder tener Score
						if ($objQuestion->IsMultiDimension && $objQuestion->MCInputType == mpcCheckBox) {
							//En este caso se recorre cada una de las opciones de respuesta, ya que cada una genera un indicador
							foreach ($objQuestion->QIndicatorIds as $intIdex => $intIndicatorID) {
								if ($intIndicatorID > 0) {
									$fieldName = '';
									$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $intIndicatorID);
									if (!is_null($objBITAMIndicator)) {
										$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
									}
									if ($fieldName != '') {
										$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_'.$intIdex;
										$strScoreFields .= ', '.$fieldName;
										$blnHasScoreQuestions = true;
									}
								}
							}
						}
						break;
						
					default:
						//El resto de las preguntas no contiene scores
						break;
				}
			}
		}
		chdir($strOriginalWD);
		
		if ($blnHasScoreQuestions && $strScoreFields != '') {
			//$strScoreFields = substr($strScoreFields, 2);
			$strAdditionalFields = '';
			$sqlScores = "SELECT $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey $strAdditionalFields $strScoreFields 
				FROM $aSurvey->SurveyTable $fromScores 
				$strJoinsScores 
				ORDER BY $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey";
			$aRSScores = $anInstance->Repository->DataADOConnection->Execute($sqlScores);
			if ($aRSScores) {
				while (!$aRSScores->EOF) {
					$factKey = (int) @$aRSScores->fields["factkey"];
					$factKeyDimVal = (int) @$aRSScores->fields["factkeydimval"];
					$blnFirstRecord = false;
					if (!isset($arrAllScores[$factKeyDimVal])) {
						$blnFirstRecord = true;
						$arrAllScores[$factKeyDimVal] = array();
					}
					
					//Recorre todos los campos e identifica la pregunta, ya que hay menos campos con Scores que preguntas por lo que es mas óptimo así
					foreach ($aRSScores->fields as $strFieldName => $sValue) {
						$arrFieldName = explode('_', $strFieldName);
						//Si no es un campo de pregunta se omite
						if (count($arrFieldName) != 3) {
							continue;
						}
						
						$intQuestionID = (int) @$arrFieldName[1];
						if ($intQuestionID <= 0) {
							continue;
						}
						
						$objQuestion = @$arrQuestionsByID[$intQuestionID];
						if (is_null($objQuestion)) {
							continue;
						}
						
						if (!isset($arrAllScores[$factKeyDimVal][$intQuestionID])) {
							$arrAllScores[$factKeyDimVal][$intQuestionID] = array();
						}
						
						//Obtiene el Score de esta pregunta en este registro específico
						$dblScore = (float) @$sValue;
						
						//Dependiendo de las opciones de la encuesta, pudieran venir scores repetidos o ya venir los correctos, así que se procesa
						//para agregar en el array solo cuando sea necesario
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][0];
						}
						
						if ($aSurvey->UseStdSectionSingleRec) {
							//Si está activado el registro único, entonces ya es seguro hacer la suma puesto que solo se graban los scores en los
							//registros donde realmente aplican en lugar de repetirlos múltiples veces
							$dblScore += $dblPrevScore;
						}
						else {
							//Si no se tiene registro único, el comportamiento variaba dependiendo del tipo de sección:
							/*- Si la  pregunta era de maestro-detalle, sólo grababa en el registro correspondiente así que era seguro sumar el
								score en ese caso
							  - Si la pregunta era de sección estándar, repetía su valor en cada registro de las secciones múltiples así que solo
							  	se debía tomar el primer valor
							  - Si la pregunta era de sección dinámica, es un problema porque dependería de si había o no una multiple-choice, si
							  	la había entonces se multiplicaban los scores por la cantidad de checkboxes de cada página, de lo contrario se
							  	podía sumar sin problema y los valores solo aplicaban para esta sección
							*/
							if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
								//En estas secciones no hay multiple-choice con score, así que simplemente usa el valor tal como venga, si hubiera
								//alguna multiple choice entonces generaría varios registros, pero al procesar el reporte se usará solo el primero
								//por página así que debería tomar el valor adecuado
							}
							elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice
								if ($objQuestion->QTypeID == qtpMulti) {
									$dblScore += $dblPrevScore;
								}
							}
							else {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice, pero además solo si se trata del primer
								//registro ya que de lo contrario daría el resultado multiplicado
								if ($objQuestion->QTypeID == qtpMulti && $blnFirstRecord) {
									$dblScore += $dblPrevScore;
								}
								else {
									//Si fuera simple choice entonces hubiera repetido el mismo valor en todos los registros, por lo tanto
									//se toma el primero procesado simplemente y el resto envía Null para no sobreescribir
									if ($objQuestion->QTypeID == qtpSingle && $blnFirstRecord) {
									}
									else {
										//Cualquier otra condición pone null para no sobreescribir el valor
										$dblScore = null;
									}
								}
							}
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							if (!is_null($dblScore)) {
								$arrAllScores[$factKeyDimVal][$intQuestionID][0] = $dblScore;
							}
						}
					}
					
					$aRSScores->MoveNext();
				}
			}
		}
		//@JAPR
		
		$objReportData = BITAMReport::NewInstance($aRepository, $aSurveyID);
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$factKey = (int)$aRS->fields["factkey"];
			//Se modificó la lógica para cargar una instancia de report mejor, ya que eso incluiría a la sección Maestro-detalle o cualquier nueva
			//funcionalidad que se agregue en el futuro en lugar de tener una función independiente como getDynamicSectionValuesByFactKeyDimVal
			//para cargar por separado cada cosa, al final la instancia de report ya incluía el mismo array que genera la función mencionada así
			//que se tenía código duplicado realmente
			if ($blnMultiRecordSection) {
				require_once('reportExt.inc.php');
				$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $factKey, $aSurveyID);
				$arrayDynSectionValues = @$objReportData->ArrayDynSectionValues[$anInstance->DynamicSectionID];
				if (is_null($arrayDynSectionValues) || !is_array($arrayDynSectionValues)) {
					$arrayDynSectionValues = array();
				}
			}
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = getQuotedStringForExport(BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = getQuotedStringForExport($aRS->fields["dateid"]);
			//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
			$latitude = (float) @$aRS->fields[$fieldAttrLatitude];
			$longitude = (float) @$aRS->fields[$fieldAttrLongitude];
			$line='"'.$userName.'","'.$dateValue.'","'.$latitude.'","'.$longitude.'"';
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$accuracy = (float) @$aRS->fields[$fieldAttrAccuracy];
				$line .= ',"'.$accuracy.'"';
			}
			//@JAPR
			$validateValue = '';
			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{
				//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
				$objQuestion = $anInstance->Questions[$i];
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
	            if ($blnValidQuestion) {
	                $strCatParentAttibuteValues = '';
					//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
					//que tengan seccion dinamica
					if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						//NO se soportan preguntas de selección sencilla que usen catálogo dentro de secciones dinámicas, por lo tanto no es necesario
						//programar esta parte, si se hubiesen creado algunas encuestas con este caso simplemente tronarían desde la App así que no
						//habría necesidad de llegar al reporte (aunque el código de Web generalmente soporta mas cosas, no es algo permitido en realidad)
						//@JAPR
						
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
						{
							foreach ($arrayDynSectionValues as $element)
							{
								$idxName = $anInstance->QuestionFields[$i];
								
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								$strValue.=$element["desc"]."=".(string) @$element[$idxName];
							}
						}
						else 
						{
							//Si es entrada de tipo Checkbox, el valor seran
							//todo los valores seleccionados concatenados con ;
							if($anInstance->Questions[$i]->MCInputType==0)
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									$valueInteger = (int) @$element[$idxName];
									
									if($valueInteger!=0)
									{
										if($strValue!="")
										{
											$strValue.=";";
										}
										
										$strValue.=$element["desc"];
									}
								}
							}
							else 
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue.=$element["desc"]."=".(string) @$element[$idxName];
								}
							}
						}
						
						$validateValue = $strValue;
						//$line.=',"'.getQuotedStringForExport($strValue).'"';
					}
					//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
					{
						//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
						//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $anInstance->ArrayMasterDetSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
						if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
							$arrMasterDetSectionValues = array();
						}
						
						$intIndex = 1;
						foreach ($arrMasterDetSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$strTempVal = (string) @$element[$idxName];
							//Si tiene registro único activado, ya no hay forma en que este valor no corresponda con la sección maestro-detalle
							if (!$aSurvey->UseStdSectionSingleRec && $anInstance->HasDynamicSection)
							{
								//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
								//Conchita, antes estaba asi 2012-12-04: if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
								//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
								//entonces no se muestre, porque probablemente es un registro de otra sección
								if (trim($strTempVal) === '' || ($objQuestion->QTypeID == qtpOpenNumeric && trim($strTempVal) == '0' && is_numeric($strTempVal)))
								{
									//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
									$strTempVal = false;
								}
							}
							
							if ($strTempVal !== false)
							{
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								//if($anInstance->Questions[$i]->FormatMask!='') {
								//	$strTempVal = formatNumber($strTempVal,$anInstance->Questions[$i]->FormatMask);
								//}
								
								$strValue .= "<".$intIndex."=".$strTempVal.">";
								//$strValue .= "<".$element[$idxName].">";
							}
							$intIndex++;
							//@JAPR
						}
						
						$validateValue = $strValue;
					}
					//@JAPR
					else 
					{
						if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
						{
							//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
							//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
							//de catálogo por lo que deben ser selección sencilla exclusivamente
							if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
							{
								$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
								if (!is_null($arrAttributesColl))
								{
									foreach ($arrAttributesColl as $aCatMember)
									{
										//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
										if ($aSurvey->DissociateCatDimens) {
											$memberParentID = $aCatMember->IndDimID;
											$attribField = "DSC_".$memberParentID;
										}
										else {
											$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
											$attribField = "DSC_".$memberParentID;
										}
										$lowerFieldName = strtolower($attribField);
										$strCatParentAttibuteValues .= ',"'.$aRS->fields[$lowerFieldName].'"';
										//@JAPR
									}
									//$strCatParentAttibuteValues = substr($strCatParentAttibuteValues, 1);
								}
							}
							//@JAPR
							
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
							
							$attribField = "DSC_".$memberParentID;
							$lowerFieldName = strtolower($attribField);
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=','.$aRS->fields[$lowerFieldName];
							}
							else
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
							}
						}
						else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$factKey = (int)$aRS->fields["factkey"];
							$separator = "_SVSep_";
							//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
							//en una cadena con enters las respuestas q fueron capturadas
							$strAnswersSCMatrix = $anInstance->getOnlyAnswersSCMatrix($anInstance->Questions[$i], $factKeyDimVal, $factKey, $separator);
							
							$validateValue = $strAnswersSCMatrix;
						}
						else 
						{
							$lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
							
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								//$line.=','.$aRS->fields[$lowerFieldName];
								$validateValue = (float) $aRS->fields[$lowerFieldName];
								//$line.=','.$validateValue;
							}
							else
							{
								if($anInstance->FieldTypes[$i]==qtpMulti)
								{
									//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
									$newValidateValue = '';
									$validateValue = $aRS->fields[$lowerFieldName];
									
									if(is_null($validateValue))
									{
										$validateValue = "";
									}
									
									if($anInstance->Questions[$i]->MCInputType==0)
									{
										$newValidateValue = $validateValue;
										
										//se debe reemplazar por numeros si es ranking
										//si es dsplabelnum
										if($anInstance->Questions[$i]->QDisplayMode==6){
											//si es tipo ranking debe de poner numeros en vez de la opcion
											//hay que convertir los valores en posiciones
											
											$tmp = explode(";",$newValidateValue);
											$numElem = count($tmp);
											
											$indice = 1; //el numerito comienza en 1
											$cadtmp = '';
											foreach($tmp as $x){
												$cadtmp.=$indice.';';
												$indice++;
											}
											//eliminar ultimo ; de la cadena
											$cadtmp = substr($cadtmp,0,-1);
											//reemplazar validate value con los numeros separados por punto y coma
											//$newValidateValue = $cadtmp;
											
										}
									}
									else
									{
										$arrayTemporal = explode(';', $validateValue);
										
										foreach($arrayTemporal as $elementTemp)
										{
											if($elementTemp=='')
											{
												$elementTemp = 'NA';
											}
											
											if($newValidateValue!='')
											{
												$newValidateValue.=';';
											}
											
											$newValidateValue.=$elementTemp;
										}
									}
									$validateValue = $newValidateValue;
									//$line.=',"'.getQuotedStringForExport($newValidateValue).'"';
								}
								else
								{
	                                $validateValue = $aRS->fields[$lowerFieldName];
									
	                                if(is_null($validateValue) || $validateValue=="")
	                                {
	                                    $validateValue = "NA";
	                                }
	                                //$line.=',"'.getQuotedStringForExport($validateValue).'"';
								}
							}
						}
					}
					
					//@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
					if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
					{
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						
						if(trim($validateValue)!="")
						{
							$arrayTemporal = explode(';', $validateValue);
						}
						else 
						{
							$arrayTemporal = array();
						}
						
						if($anInstance->Questions[$i]->MCInputType==0)
						{
							//En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se reemplaza
							//tal cual
							foreach($arrayTemporal as $elementTemp)
							{
								@$arrAnswers[$elementTemp] = $elementTemp;
							}
						}
						else 
						{
							//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
							//posibles respuestas no es asociativo en este caso)
							$intNumAnsw = count($arrayTemporal);
							for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
							{
								$elementTemp = $arrayTemporal[$intIdx];
								if (!is_null($elementTemp) && trim($elementTemp) != '')
								{
									@$arrAnswers[$intIdx] = $elementTemp;
								}
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						//tengo elementos en orden, y los NA
						//si es dsplabelnum hay que recorrer 
						if($anInstance->Questions[$i]->QDisplayMode==6){
							$idx= 1;
							$cadwithNA ='';
							$clon = array();
							$cont1 = count($tmp);
							//for($i=0;$i<count($arrAnswers);$i++){
							foreach ($arrAnswers as $ix=>$elementTemp){
								$clon[$ix]='NA';
								for($jx=0;$jx<$cont1;$jx++){
									if($arrAnswers[$ix]==$tmp[$jx]){
										$clon[$ix]=$jx+1;
										
									}
								}
							}
							
							/*foreach($arrAnswers as $x){
								foreach($tmp as $y){	//el tmp no trae NAs solo trae los valores
									$idx++;
								}
								$idx= 1;
							}*/
						}
						if($anInstance->Questions[$i]->QDisplayMode==6){
							foreach ($clon as $elementTemp2)
						{
							$line.=',"'.getQuotedStringForExport($elementTemp2).'"';
						}
						}
						else{
							foreach ($arrAnswers as $elementTemp)
							{
								$line.=',"'.getQuotedStringForExport($elementTemp).'"';
							}
						}
					}
					else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
					{
						$separator = "_SVSep_";
						
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						$arrayTemporal = explode($separator, $validateValue);
						//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
						//posibles respuestas no es asociativo en este caso)
						$intNumAnsw = count($arrayTemporal);
						for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
						{
							$elementTemp = $arrayTemporal[$intIdx];
							if (!is_null($elementTemp) && trim($elementTemp) != '')
							{
								@$arrAnswers[$intIdx] = $elementTemp;
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						foreach ($arrAnswers as $elementTemp)
						{
							$line.=',"'.getQuotedStringForExport($elementTemp).'"';
						}
					}
					else 
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						$line.= $strCatParentAttibuteValues.',"'.getQuotedStringForExport($validateValue).'"';
					}
				}
				
				//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
				if ($bIncludePhotos && $objQuestion->HasReqPhoto) {
					//El array de fotos viene ordenado por factKey dentro de la pregunta, esto es loa fotos por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrImages = @$arrAllImages[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrImages) || !is_array($arrImages)) {
						$arrImages = array();
					}
					
					$strValue = '';
					if (count($arrImages) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrImages as $intImgFactKey => $strImage) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intImgFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan fotos por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strImage) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strImage;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strImage = (string) @$arrImages[$intFactKey];
								if (trim($strImage) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strImage.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber una foto
							$strValue = (string) @$arrImages[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
				}
				
				//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
				if ($objQuestion->HasReqComment) {
					//El array de comentarios viene ordenado por factKey dentro de la pregunta, esto es los comentarios por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrComments = @$arrAllComments[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrComments) || !is_array($arrComments)) {
						$arrComments = array();
					}
					
					$strValue = '';
					if (count($arrComments) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrComments as $intCommFactKey => $strComment) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									//@JAPR 2013-03-07: Corregido un bug, estaba comparando contra el factKey del reporte en lugar del comentario (#28777)
									if ($intFactKey == $intCommFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan comentarios por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strComment) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strComment;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strComment = (string) @$arrComments[$intFactKey];
								if (trim($strComment) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strComment.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un comentario
							$strValue = (string) @$arrComments[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
				}
				
				//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
				if ($objQuestion->HasScores) {
					//El array de scores viene ordenado por factKey dentro de la pregunta, esto es los scores por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrScores = @$arrAllScores[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrScores) || !is_array($arrScores)) {
						$arrScores = array();
					}
					
					$strValue = '';
					if (count($arrScores) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrScores as $intScoreFactKey => $strScore) {
								//@JAPR 2013-03-11: Corregido un bug, se estaba calculando un registro que no pertenecía a esta sección y lo
								//agregaba como parte de las respuestas (#28772)
								$blnFound = false;
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intScoreFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$blnFound = true;
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan scores por página así que se descartan los registros de otros CheckBoxes
								if ($blnFound && !isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strScore) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strScore;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strScore = (string) @$arrScores[$intFactKey];
								if (trim($strScore) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strScore.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un score
							$strValue = (string) @$arrScores[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					
					/*
					$strValue = '';
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					*/
				}
				//@JAPR
			}
			
			array_push($afile, $line);

			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;

		$fStats = fopen($spathfile, 'w+');

		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}

		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
			exit();
		}
	}

	/*@AAL 22/06/2015: Modificado, el UserID ya no sera un entero, ahora sera un string (el Email)*/
	static function NewInstanceToExportDesgToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID = "", $bIncludePhotos = false, $blnNewExcelMethod = false)
	{
		if($blnNewExcelMethod)
		{
			return BITAMReportCollection::NewInstanceToExportDesg2($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $userID, $bIncludePhotos);
		}
		
		$strOriginalWD = getcwd();
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		$line = '"User Name","Date","Latitude","Longitude"';
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		//@JAPR 2014-04-28: Si se deja un espacio, las comillas dobles aparecen en el XLS
		if (getMDVersion() >= esvAccuracyValues) {
			$line .= ',"Accuracy"';
		}
		
		//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
        //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection) 
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
		}
		//@JAPR
		
		//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
		//Carga las definiciones de catálogos utilizadas y dependiendo de las preguntas va asignando que atributos se deberían agregar a cada una,
		//ya que múltiples preguntas de catálogo en la misma encuesta sólo podrían usar atributos posteriores a los previamente usados, por lo 
		//tanto dichos atributos ni los padres no se deben mostrar en preguntas posteriores, sólo aquellos que aun no se estén desplegando
		$arrCatalogs = array();
		$arrCatalogMembers = array();
		$arrLastShownAttributeIdx = array();
		$arrAttributesByQuestion = array();
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrQuestionsByID = array();
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			$objQuestion = $anInstance->Questions[$i];
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
			//@JAPR
			
			if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode != dspMatrix)
			{
				$intCatalogID = $anInstance->Questions[$i]->CatalogID;
				$intCatMemberID = $anInstance->Questions[$i]->CatMemberID;
				if ($intCatalogID > 0 && $intCatMemberID > 0)
				{
					if (isset($arrCatalogs[$intCatalogID]))
					{
						$aCatalog = $arrCatalogs[$intCatalogID];
						$aCatalogMembersColl = $arrCatalogMembers[$intCatalogID];
						$intLastShownAttribute = $arrLastShownAttributeIdx[$intCatalogID];
					}
					else 
					{
						$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
						$aCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
						$aCatalogMembersColl = $aCatalogMembersColl->Collection;
						$intLastShownAttribute = -1;
						$arrCatalogs[$intCatalogID] = $aCatalog;
						$arrCatalogMembers[$intCatalogID] = $aCatalogMembersColl;
						$arrLastShownAttributeIdx[$intCatalogID] = $intLastShownAttribute;
					}
					
					//Primero verifica si el Atributo de esta pregunta ya había sido desplegado, si lo fué entonces se trata de un error y por
					//tanto simplemente se agregará a este atributo, de lo contrario se incluirían otros posteriores lo cual estaría incorrecto
					$blnAttributeUsed = true;
					$intNumAttributes = count($aCatalogMembersColl);
					for ($intIdx = $intLastShownAttribute + 1; $intIdx < $intNumAttributes; $intIdx++)
					{
						if ($aCatalogMembersColl[$intIdx]->MemberID == $intCatMemberID)
						{
							$blnAttributeUsed = false;
							break;
						}
					}
					
					$arrAttributesColl = array();
					//Si el atributo ya estaba usado, simplemente brincará este If para sólo agregar el atributo en cuestión, de lo contrario
					//agrega todos los padres hasta llegar al atributo de la pregunta
					if (!$blnAttributeUsed)
					{
						//Crea la entrada para esta pregunta usando su índice, el contenido será el Array de todos los Atributos que debe imprimir
						//los cuales consisten en los Atributos Padres del asociado a la pregunta, incluyendo al propio atributo de la pregunta, siemre
						//y cuando NO se hubieran desplegado ya en otra pregunta previa
						for($intLastShownAttribute = $intLastShownAttribute +1; $intLastShownAttribute < $intNumAttributes; $intLastShownAttribute++)
						{
							if ($aCatalogMembersColl[$intLastShownAttribute]->MemberID == $intCatMemberID)
							{
								break;
							}
							
							$arrAttributesColl[] = $aCatalogMembersColl[$intLastShownAttribute];
						}
					}

					//En realidad no se necesita agregar el Atributo de la pregunta, ya que el código mas abajo simplemente usará el nombre de la
					//pregunta, sólo se necesitan los atributos padre pues esos no se conocían y hay que anteponerlos a la pregunta en cuestión
					//$arrAttributesColl[] = null;	// NO es necesario activar esta línea, si el count de este array es 0 aun así funciona abajo
					
					//Agrega el atributo de la pregunta como el texto de la misma directamente
					$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID] = $arrAttributesColl;
				}
			}
		}
		//@JAPR
		
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if($blnValidQuestion) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $anInstance->Questions[$i]->getAttributeName();
				
                //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
                if ($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
                {
                    foreach ($anInstance->Questions[$i]->SelectOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
                    //Se genera un nuevo Array vacio según el tipo de respuestas múltiples para ser reutilizado al recorrer las encuestas capturadas
                    if($anInstance->Questions[$i]->QTypeID == qtpMulti && count($anInstance->Questions[$i]->SelectOptions) > 0)
                    {
                        if($anInstance->Questions[$i]->MCInputType==0)
                        {
                            //En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se toma tal cual
                            //y sólo se rellena con cadenas vacias en su valores
                            $arrAnswers = array_flip(array_values($anInstance->Questions[$i]->SelectOptions));
                        }
                        else 
                        {
                            //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                            //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                            //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                            $arrAnswers = array_values($anInstance->Questions[$i]->SelectOptions);
                        }

                        $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                        foreach ($arrAnswers as $intID => $strValue)
                        {
                            $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                        }
                    }
                    //@JAPR
                }
                else if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode == dspMatrix)
                {
                    if($anInstance->Questions[$i]->OneChoicePer==0)
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptions;
                    }
                    else 
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptionsCol;
                    }

                    foreach ($theOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                    //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                    //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                    $arrAnswers = array_values($theOptions);

                    $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                    foreach ($arrAnswers as $intID => $strValue)
                    {
                        $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                    }
                }
                else 
                {
                    //@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
                    //Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
                    //de catálogo por lo que deben ser selección sencilla exclusivamente
                    if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
                    {
                        $arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
                        if (!is_null($arrAttributesColl))
                        {
                            foreach ($arrAttributesColl as $aCatMember)
                            {
                                $line.=',"'.getQuotedStringForExport($aCatMember->MemberName).'"';
                            }
                        }
                    }
                    //@JAPR
                    $line.=',"'.getQuotedStringForExport($strText).'"';
                }
            }
            
			//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
			//Si la pregunta tiene foto configurada y se pidió incluirlas, entonces agrega una columna adicional al archivo
			if ($bIncludePhotos && $objQuestion->HasReqPhoto > 0) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                if ($objQuestion->QTypeID === qtpSignature) {
                	$strText .= ' '.translate('Signature');
                }
                else {
                	$strText .= ' '.translate('Photo');
                }
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
            
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			if ($objQuestion->HasReqComment) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                $strText .= ' '.translate('Comment');
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			
			//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
			//Verifica si esta pregunta contiene o no Score forma de calcular Score
			if ($objQuestion->HasScores) {
                $strText .= ' '.translate('Score');
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			//@JAPR
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
		$fieldAttrLatitude = '0';
		$fieldAttrLongitude = '0';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = $catTable."."."DSC_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = $catTable."."."DSC_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$fieldAttrAccuracy = ', 0';
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = ", ".$catTable."."."DSC_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		$sql.=", ".$fieldAttrLatitude.", ".$fieldAttrLongitude.$fieldAttrAccuracy;
		
		//Se modifica para usarse al leer el recordset
		$fieldAttrLatitude = '';
		$fieldAttrLongitude = '';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = "dsc_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = "dsc_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = "dsc_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		//Se forzará a utilizar la tabla de hechos para extraer la información adicional
		if ($aSurvey->FactKeyDimID > 0) {
			$fromCat.=", ".$aSurvey->FactTable." B";
			$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
			$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
			$strAnd = " AND ";
			
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			$joinField = "RIDIM_".$aSurvey->FactKeyDimID."KEY";
			$fromCat.=", ".$catTable;
			$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
		}
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//Modificada la condición para omitir el ciclo en lugar de preguntar si debe entrar o no a procesar algo
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($objQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpSketch:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$objQuestion->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = $aCatMember->IndDimID;
								$catTable = "RIDIM_".$memberParentID;
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$memberParentID."KEY";
									$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
									$strAnd = " AND ";
								}
							}
						}
					}
					//@JAPR
					
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
							}
						}
					}
					//@JAPR
					
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
				//@JAPR
			}
		}
		
		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					if($where!="")
					{
						$where.=" AND ";
					}
					
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
			$strAnd = " AND ";
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
			$strAnd = " AND ";
		}
		
		if($userID != "")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
			$strAnd = " AND ";
		}

		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		//El filtro completo se usará tal cual entre las tablas paralela, hechos y catálogos, pero filtros adicionales ya no aplican para extraer
		//los comentarios así que esos se ignorarán
		$strJoins = $where;
		$strJoinsScores = $where;
		if ($strJoins != '') {
			$strJoins = "AND ".$strJoins;
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$blnMultiRecordSection = ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection);
		if ($aSurvey->UseStdSectionSingleRec) {
			//Si se usa el registro único, entonces se trae exclusivamente los datos de dichos registros, de lo contrario puede traer cualquier
			//registro ya que antes de este cambio en todos se grababa lo mismo para las preguntas estándar
			if ($where != "") {
				$where .= " AND ";
			}
			$where .= $aSurvey->SurveyTable.".EntrySectionID = 0";
		}
		//@JAPR
		
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
		if ($bIncludePhotos) {
			//Obtenemos el prefijo de la ruta requerida en la dimension imagen
			$script = $_SERVER["REQUEST_URI"];
			//Eliminamos la primera diagonal que separa el archivo php de la carpeta
			$position = strrpos($script, "/");
			$dir = substr($script, 0, $position);
			
			//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
			$position = strrpos($dir, "/");
			$mainDir = substr($dir, ($position+1));
			//@JAPR 2013-06-03: Corregido un bug, estaba incluyendo 2 veces la ruta del Survey
			//$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$aSurveyID."/";
			$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/";
			//@JAPR
			
			$arrAllImages = array();
			$sqlImages = "SELECT tImg.FactKey as FactKeyDimVal, tImg.MainFactKey as FactKey, tImg.SurveyID, tImg.QuestionID, tImg.PathImage 
				FROM SI_SV_SurveyAnswerImage tImg, $aSurvey->SurveyTable $fromCat 
				WHERE tImg.SurveyID = $aSurveyID AND tImg.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
				ORDER BY tImg.FactKey, tImg.MainFactKey";	// survey_4/photo_4_26_1_1.jpg
			$aRSImg = $anInstance->Repository->DataADOConnection->Execute($sqlImages);
			if ($aRSImg) {
				while (!$aRSImg->EOF) {
					$intQuestionID = (int) $aRSImg->fields["questionid"];
					$objQuestion = @$arrQuestionsByID[$intQuestionID];
					if (!is_null($objQuestion) && $objQuestion->HasReqPhoto) {
						$factKey = (int) @$aRSImg->fields["factkey"];
						$factKeyDimVal = (int) @$aRSImg->fields["factkeydimval"];
						$strFieldDocumentName = (string) @$aRSImg->fields["pathimage"];
						if (!isset($arrAllImages[$factKeyDimVal])) {
							$arrAllImages[$factKeyDimVal] = array();
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						elseif (!isset($arrAllImages[$factKeyDimVal][$intQuestionID])) {
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						
						$strFieldDocumentName2 = '';
						if (trim($strFieldDocumentName) != '') {
							$strFieldDocumentName2 = $strPhotoPrefix.$strFieldDocumentName;
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
							if (count($arrAllImages[$factKeyDimVal][$intQuestionID]) == 0) {
								$arrAllImages[$factKeyDimVal][$intQuestionID][0] = $strFieldDocumentName2;
							}
						}
					}
					$aRSImg->MoveNext();
				}
			}
		}
		
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrAllComments = array();
		$sqlComments = "SELECT tComm.FactKey as FactKeyDimVal, tComm.MainFactKey as FactKey, tComm.SurveyID, tComm.QuestionID, tComm.StrComment 
			FROM SI_SV_SurveyAnswerComment tComm, $aSurvey->SurveyTable $fromCat 
			WHERE tComm.SurveyID = $aSurveyID AND tComm.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
			ORDER BY tComm.FactKey, tComm.MainFactKey";
		$aRSComm = $anInstance->Repository->DataADOConnection->Execute($sqlComments);
		if ($aRSComm) {
			while (!$aRSComm->EOF) {
				$intQuestionID = (int) $aRSComm->fields["questionid"];
				$objQuestion = @$arrQuestionsByID[$intQuestionID];
				if (!is_null($objQuestion) && $objQuestion->HasReqComment) {
					$factKey = (int) @$aRSComm->fields["factkey"];
					$factKeyDimVal = (int) @$aRSComm->fields["factkeydimval"];
					$strComment = (string) @$aRSComm->fields["strcomment"];
						if (!isset($arrAllComments[$factKeyDimVal])) {
						$arrAllComments[$factKeyDimVal] = array();
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					elseif (!isset($arrAllComments[$factKeyDimVal][$intQuestionID])) {
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					
					//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
					//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
					//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
					if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
						//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
						//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					else {
						//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
						if (count($arrAllComments[$factKeyDimVal][$intQuestionID]) == 0) {
							$arrAllComments[$factKeyDimVal][$intQuestionID][0] = $strComment;
						}
					}
				}
				$aRSComm->MoveNext();
			}
		}
		
		//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
		$arrAllScores = array();
		$fromScores = $fromCat;
		if(!isset($arrayTables[$aSurvey->FactTable])) {
			$fromScores .= ", ".$aSurvey->FactTable." B ";
			$strJoinsScores .= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
		}
		if ($strJoinsScores != '') {
			$strJoinsScores = " WHERE ".$strJoinsScores;
		}
		
		//Los campos se deben procesar dependiendo del tipo de grabado que tiene configurado la encuesta, aunque en un query se podrían obtener
		//aplicando funciones de agrupación, se optará por procesarlos en memoria para controlar mejor los datos obtenidos
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$dynamicSectionWithMultiChoice = false;
		if ($anInstance->HasDynamicSection && !is_null($anInstance->DynamicSection)) {
			$dynamicSectionWithMultiChoice = ($anInstance->DynamicSection->ChildCatMemberID > 0);
		}
		$blnHasScoreQuestions = false;
		$strScoreFields = '';
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			if ($objQuestion->HasScores) {
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Las preguntas simple choice no deben ser de catálogo para poder tener Score
						if ($objQuestion->QTypeID == qtpSingle && $objQuestion->UseCatalog == 0 && $objQuestion->IndicatorID > 0) {
							$fieldName = '';
							$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
							if (!is_null($objBITAMIndicator)) {
								$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
							}
							if ($fieldName != '') {
								$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_0';
								$strScoreFields .= ', '.$fieldName;
								$blnHasScoreQuestions = true;
							}
						}
						break;
						
					case qtpMulti:
						//Las preguntas múltiples deben ser multi-dimensión tipo checkbox para poder tener Score
						if ($objQuestion->IsMultiDimension && $objQuestion->MCInputType == mpcCheckBox) {
							//En este caso se recorre cada una de las opciones de respuesta, ya que cada una genera un indicador
							foreach ($objQuestion->QIndicatorIds as $intIdex => $intIndicatorID) {
								if ($intIndicatorID > 0) {
									$fieldName = '';
									$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $intIndicatorID);
									if (!is_null($objBITAMIndicator)) {
										$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
									}
									if ($fieldName != '') {
										$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_'.$intIdex;
										$strScoreFields .= ', '.$fieldName;
										$blnHasScoreQuestions = true;
									}
								}
							}
						}
						break;
						
					default:
						//El resto de las preguntas no contiene scores
						break;
				}
			}
		}
		chdir($strOriginalWD);
		
		if ($blnHasScoreQuestions && $strScoreFields != '') {
			//$strScoreFields = substr($strScoreFields, 2);
			$strAdditionalFields = '';
			$sqlScores = "SELECT $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey $strAdditionalFields $strScoreFields 
				FROM $aSurvey->SurveyTable $fromScores 
				$strJoinsScores 
				ORDER BY $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey";
			$aRSScores = $anInstance->Repository->DataADOConnection->Execute($sqlScores);
			if ($aRSScores) {
				while (!$aRSScores->EOF) {
					$factKey = (int) @$aRSScores->fields["factkey"];
					$factKeyDimVal = (int) @$aRSScores->fields["factkeydimval"];
					$blnFirstRecord = false;
					if (!isset($arrAllScores[$factKeyDimVal])) {
						$blnFirstRecord = true;
						$arrAllScores[$factKeyDimVal] = array();
					}
					
					//Recorre todos los campos e identifica la pregunta, ya que hay menos campos con Scores que preguntas por lo que es mas óptimo así
					foreach ($aRSScores->fields as $strFieldName => $sValue) {
						$arrFieldName = explode('_', $strFieldName);
						//Si no es un campo de pregunta se omite
						if (count($arrFieldName) != 3) {
							continue;
						}
						
						$intQuestionID = (int) @$arrFieldName[1];
						if ($intQuestionID <= 0) {
							continue;
						}
						
						$objQuestion = @$arrQuestionsByID[$intQuestionID];
						if (is_null($objQuestion)) {
							continue;
						}
						
						if (!isset($arrAllScores[$factKeyDimVal][$intQuestionID])) {
							$arrAllScores[$factKeyDimVal][$intQuestionID] = array();
						}
						
						//Obtiene el Score de esta pregunta en este registro específico
						$dblScore = (float) @$sValue;
						
						//Dependiendo de las opciones de la encuesta, pudieran venir scores repetidos o ya venir los correctos, así que se procesa
						//para agregar en el array solo cuando sea necesario
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][0];
						}
						
						if ($aSurvey->UseStdSectionSingleRec) {
							//Si está activado el registro único, entonces ya es seguro hacer la suma puesto que solo se graban los scores en los
							//registros donde realmente aplican en lugar de repetirlos múltiples veces
							$dblScore += $dblPrevScore;
						}
						else {
							//Si no se tiene registro único, el comportamiento variaba dependiendo del tipo de sección:
							/*- Si la  pregunta era de maestro-detalle, sólo grababa en el registro correspondiente así que era seguro sumar el
								score en ese caso
							  - Si la pregunta era de sección estándar, repetía su valor en cada registro de las secciones múltiples así que solo
							  	se debía tomar el primer valor
							  - Si la pregunta era de sección dinámica, es un problema porque dependería de si había o no una multiple-choice, si
							  	la había entonces se multiplicaban los scores por la cantidad de checkboxes de cada página, de lo contrario se
							  	podía sumar sin problema y los valores solo aplicaban para esta sección
							*/
							if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
								//En estas secciones no hay multiple-choice con score, así que simplemente usa el valor tal como venga, si hubiera
								//alguna multiple choice entonces generaría varios registros, pero al procesar el reporte se usará solo el primero
								//por página así que debería tomar el valor adecuado
							}
							elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice
								if ($objQuestion->QTypeID == qtpMulti) {
									$dblScore += $dblPrevScore;
								}
							}
							else {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice, pero además solo si se trata del primer
								//registro ya que de lo contrario daría el resultado multiplicado
								if ($objQuestion->QTypeID == qtpMulti && $blnFirstRecord) {
									$dblScore += $dblPrevScore;
								}
								else {
									//Si fuera simple choice entonces hubiera repetido el mismo valor en todos los registros, por lo tanto
									//se toma el primero procesado simplemente y el resto envía Null para no sobreescribir
									if ($objQuestion->QTypeID == qtpSingle && $blnFirstRecord) {
									}
									else {
										//Cualquier otra condición pone null para no sobreescribir el valor
										$dblScore = null;
									}
								}
							}
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							if (!is_null($dblScore)) {
								$arrAllScores[$factKeyDimVal][$intQuestionID][0] = $dblScore;
							}
						}
					}
					
					$aRSScores->MoveNext();
				}
			}
		}
		//@JAPR
		
		$objReportData = BITAMReport::NewInstance($aRepository, $aSurveyID);
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$factKey = (int)$aRS->fields["factkey"];
			//Se modificó la lógica para cargar una instancia de report mejor, ya que eso incluiría a la sección Maestro-detalle o cualquier nueva
			//funcionalidad que se agregue en el futuro en lugar de tener una función independiente como getDynamicSectionValuesByFactKeyDimVal
			//para cargar por separado cada cosa, al final la instancia de report ya incluía el mismo array que genera la función mencionada así
			//que se tenía código duplicado realmente
			if ($blnMultiRecordSection) {
				require_once('reportExt.inc.php');
				$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $factKey, $aSurveyID);
				$arrayDynSectionValues = @$objReportData->ArrayDynSectionValues[$anInstance->DynamicSectionID];
				if (is_null($arrayDynSectionValues) || !is_array($arrayDynSectionValues)) {
					$arrayDynSectionValues = array();
				}
			}
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = getQuotedStringForExport(BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = getQuotedStringForExport($aRS->fields["dateid"]);
			//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
			$latitude = (float) @$aRS->fields[$fieldAttrLatitude];
			$longitude = (float) @$aRS->fields[$fieldAttrLongitude];
			$line='"'.$userName.'","'.$dateValue.'","'.$latitude.'","'.$longitude.'"';
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$accuracy = (float) @$aRS->fields[$fieldAttrAccuracy];
				$line .= ',"'.$accuracy.'"';
			}
			//@JAPR
			$validateValue = '';
			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{
				//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
				$objQuestion = $anInstance->Questions[$i];
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					case qtpSketch:
					case qtpBarCode:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
	            if ($blnValidQuestion) {
	                $strCatParentAttibuteValues = '';
					//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
					//que tengan seccion dinamica
					if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						//NO se soportan preguntas de selección sencilla que usen catálogo dentro de secciones dinámicas, por lo tanto no es necesario
						//programar esta parte, si se hubiesen creado algunas encuestas con este caso simplemente tronarían desde la App así que no
						//habría necesidad de llegar al reporte (aunque el código de Web generalmente soporta mas cosas, no es algo permitido en realidad)
						//@JAPR
						
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
						{
							foreach ($arrayDynSectionValues as $element)
							{
								$idxName = $anInstance->QuestionFields[$i];
								
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								$strValue.=$element["desc"]."=".(string) @$element[$idxName];
							}
						}
						else 
						{
							//Si es entrada de tipo Checkbox, el valor seran
							//todo los valores seleccionados concatenados con ;
							if($anInstance->Questions[$i]->MCInputType==0)
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									$valueInteger = (int) @$element[$idxName];
									
									if($valueInteger!=0)
									{
										if($strValue!="")
										{
											$strValue.=";";
										}
										
										$strValue.=$element["desc"];
									}
								}
							}
							else 
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue.=$element["desc"]."=".(string) @$element[$idxName];
								}
							}
						}
						
						$validateValue = $strValue;
						//$line.=',"'.getQuotedStringForExport($strValue).'"';
					}
					//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
					{
						//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
						//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $anInstance->ArrayMasterDetSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
						if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
							$arrMasterDetSectionValues = array();
						}
						
						$intIndex = 1;
						foreach ($arrMasterDetSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$strTempVal = (string) @$element[$idxName];
							//Si tiene registro único activado, ya no hay forma en que este valor no corresponda con la sección maestro-detalle
							if (!$aSurvey->UseStdSectionSingleRec && $anInstance->HasDynamicSection)
							{
								//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
								//Conchita, antes estaba asi 2012-12-04: if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
								//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
								//entonces no se muestre, porque probablemente es un registro de otra sección
								if (trim($strTempVal) === '' || ($objQuestion->QTypeID == qtpOpenNumeric && trim($strTempVal) == '0' && is_numeric($strTempVal)))
								{
									//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
									$strTempVal = false;
								}
							}
							
							if ($strTempVal !== false)
							{
								if($strValue!="")
								{
									$strValue.="; ";
								}
								
								//if($anInstance->Questions[$i]->FormatMask!='') {
								//	$strTempVal = formatNumber($strTempVal,$anInstance->Questions[$i]->FormatMask);
								//}
								
								$strValue .= "<".$intIndex."=".$strTempVal.">";
								//$strValue .= "<".$element[$idxName].">";
							}
							$intIndex++;
							//@JAPR
						}
						
						$validateValue = $strValue;
					}
					//@JAPR
					else 
					{
						if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
						{
							//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
							//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
							//de catálogo por lo que deben ser selección sencilla exclusivamente
							if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
							{
								$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
								if (!is_null($arrAttributesColl))
								{
									foreach ($arrAttributesColl as $aCatMember)
									{
										//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
										if ($aSurvey->DissociateCatDimens) {
											$memberParentID = $aCatMember->IndDimID;
											$attribField = "DSC_".$memberParentID;
										}
										else {
											$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
											$attribField = "DSC_".$memberParentID;
										}
										$lowerFieldName = strtolower($attribField);
										$strCatParentAttibuteValues .= ',"'.$aRS->fields[$lowerFieldName].'"';
										//@JAPR
									}
									//$strCatParentAttibuteValues = substr($strCatParentAttibuteValues, 1);
								}
							}
							//@JAPR
							
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
							
							$attribField = "DSC_".$memberParentID;
							$lowerFieldName = strtolower($attribField);
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=','.$aRS->fields[$lowerFieldName];
							}
							else
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
							}
						}
						else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$factKey = (int)$aRS->fields["factkey"];
							$separator = "_SVSep_";
							//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
							//en una cadena con enters las respuestas q fueron capturadas
							$strAnswersSCMatrix = $anInstance->getOnlyAnswersSCMatrix($anInstance->Questions[$i], $factKeyDimVal, $factKey, $separator);
							
							$validateValue = $strAnswersSCMatrix;
						}
						else 
						{
							$lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
							
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								//$line.=','.$aRS->fields[$lowerFieldName];
								$validateValue = (float) $aRS->fields[$lowerFieldName];
								//$line.=','.$validateValue;
							}
							else
							{
								if($anInstance->FieldTypes[$i]==qtpMulti)
								{
									//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
									$newValidateValue = '';
									$validateValue = $aRS->fields[$lowerFieldName];
									
									if(is_null($validateValue))
									{
										$validateValue = "";
									}
									
									if($anInstance->Questions[$i]->MCInputType==0)
									{
										$newValidateValue = $validateValue;
										
										//se debe reemplazar por numeros si es ranking
										//si es dsplabelnum
										if($anInstance->Questions[$i]->QDisplayMode==6){
											//si es tipo ranking debe de poner numeros en vez de la opcion
											//hay que convertir los valores en posiciones
											
											$tmp = explode(";",$newValidateValue);
											$numElem = count($tmp);
											
											$indice = 1; //el numerito comienza en 1
											$cadtmp = '';
											foreach($tmp as $x){
												$cadtmp.=$indice.';';
												$indice++;
											}
											//eliminar ultimo ; de la cadena
											$cadtmp = substr($cadtmp,0,-1);
											//reemplazar validate value con los numeros separados por punto y coma
											//$newValidateValue = $cadtmp;
											
										}
									}
									else
									{
										$arrayTemporal = explode(';', $validateValue);
										
										foreach($arrayTemporal as $elementTemp)
										{
											if($elementTemp=='')
											{
												$elementTemp = 'NA';
											}
											
											if($newValidateValue!='')
											{
												$newValidateValue.=';';
											}
											
											$newValidateValue.=$elementTemp;
										}
									}
									$validateValue = $newValidateValue;
									//$line.=',"'.getQuotedStringForExport($newValidateValue).'"';
								}
								else
								{
	                                $validateValue = $aRS->fields[$lowerFieldName];
									
	                                if(is_null($validateValue) || $validateValue=="")
	                                {
	                                    $validateValue = "NA";
	                                }
	                                //$line.=',"'.getQuotedStringForExport($validateValue).'"';
								}
							}
						}
					}
					
					//@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
					if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
					{
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						
						if(trim($validateValue)!="")
						{
							$arrayTemporal = explode(';', $validateValue);
						}
						else 
						{
							$arrayTemporal = array();
						}
						
						if($anInstance->Questions[$i]->MCInputType==0)
						{
							//En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se reemplaza
							//tal cual
							foreach($arrayTemporal as $elementTemp)
							{
								@$arrAnswers[$elementTemp] = $elementTemp;
							}
						}
						else 
						{
							//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
							//posibles respuestas no es asociativo en este caso)
							$intNumAnsw = count($arrayTemporal);
							for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
							{
								$elementTemp = $arrayTemporal[$intIdx];
								if (!is_null($elementTemp) && trim($elementTemp) != '')
								{
									@$arrAnswers[$intIdx] = $elementTemp;
								}
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						//tengo elementos en orden, y los NA
						//si es dsplabelnum hay que recorrer 
						if($anInstance->Questions[$i]->QDisplayMode==6){
							$idx= 1;
							$cadwithNA ='';
							$clon = array();
							$cont1 = count($tmp);
							//for($i=0;$i<count($arrAnswers);$i++){
							foreach ($arrAnswers as $ix=>$elementTemp){
								$clon[$ix]='NA';
								for($jx=0;$jx<$cont1;$jx++){
									if($arrAnswers[$ix]==$tmp[$jx]){
										$clon[$ix]=$jx+1;
										
									}
								}
							}
							
							/*foreach($arrAnswers as $x){
								foreach($tmp as $y){	//el tmp no trae NAs solo trae los valores
									$idx++;
								}
								$idx= 1;
							}*/
						}
						if($anInstance->Questions[$i]->QDisplayMode==6){
							foreach ($clon as $elementTemp2)
						{
							$line.=',"'.getQuotedStringForExport($elementTemp2).'"';
						}
						}
						else{
							foreach ($arrAnswers as $elementTemp)
							{
								$line.=',"'.getQuotedStringForExport($elementTemp).'"';
							}
						}
					}
					else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
					{
						$separator = "_SVSep_";
						
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						$arrayTemporal = explode($separator, $validateValue);
						//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
						//posibles respuestas no es asociativo en este caso)
						$intNumAnsw = count($arrayTemporal);
						for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
						{
							$elementTemp = $arrayTemporal[$intIdx];
							if (!is_null($elementTemp) && trim($elementTemp) != '')
							{
								@$arrAnswers[$intIdx] = $elementTemp;
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						foreach ($arrAnswers as $elementTemp)
						{
							$line.=',"'.getQuotedStringForExport($elementTemp).'"';
						}
					}
					else 
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						$line.= $strCatParentAttibuteValues.',"'.getQuotedStringForExport($validateValue).'"';
					}
				}
				
				//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
				if ($bIncludePhotos && $objQuestion->HasReqPhoto) {
					//El array de fotos viene ordenado por factKey dentro de la pregunta, esto es loa fotos por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrImages = @$arrAllImages[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrImages) || !is_array($arrImages)) {
						$arrImages = array();
					}
					
					$strValue = '';
					if (count($arrImages) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrImages as $intImgFactKey => $strImage) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intImgFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan fotos por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strImage) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strImage;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strImage = (string) @$arrImages[$intFactKey];
								if (trim($strImage) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strImage.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber una foto
							$strValue = (string) @$arrImages[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
				}
				
				//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
				if ($objQuestion->HasReqComment) {
					//El array de comentarios viene ordenado por factKey dentro de la pregunta, esto es los comentarios por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrComments = @$arrAllComments[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrComments) || !is_array($arrComments)) {
						$arrComments = array();
					}
					
					$strValue = '';
					if (count($arrComments) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrComments as $intCommFactKey => $strComment) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									//@JAPR 2013-03-07: Corregido un bug, estaba comparando contra el factKey del reporte en lugar del comentario (#28777)
									if ($intFactKey == $intCommFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan comentarios por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strComment) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strComment;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strComment = (string) @$arrComments[$intFactKey];
								if (trim($strComment) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strComment.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un comentario
							$strValue = (string) @$arrComments[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
				}
				
				//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
				if ($objQuestion->HasScores) {
					//El array de scores viene ordenado por factKey dentro de la pregunta, esto es los scores por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrScores = @$arrAllScores[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrScores) || !is_array($arrScores)) {
						$arrScores = array();
					}
					
					$strValue = '';
					if (count($arrScores) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrScores as $intScoreFactKey => $strScore) {
								//@JAPR 2013-03-11: Corregido un bug, se estaba calculando un registro que no pertenecía a esta sección y lo
								//agregaba como parte de las respuestas (#28772)
								$blnFound = false;
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intScoreFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$blnFound = true;
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan scores por página así que se descartan los registros de otros CheckBoxes
								if ($blnFound && !isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strScore) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strScore;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strScore = (string) @$arrScores[$intFactKey];
								if (trim($strScore) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strScore.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un score
							$strValue = (string) @$arrScores[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					
					/*
					$strValue = '';
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					*/
				}
				//@JAPR
			}
			
			array_push($afile, $line);

			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;

		$fStats = fopen($spathfile, 'w+');

		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}

		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
			exit();
		}
	}

	//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
	//Agregado el parámetro $bIncludePhotos para indicar si se debe o no agregar una columna para cada pregunta tipo Foto/Firma o que tenga foto
	//opcional configurada, de tal manera que se exporte la ruta de la imagen si es que contiene alguna
	static function NewInstanceToExportDesg2($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2, $bIncludePhotos = false)
	{
		$strOriginalWD = getcwd();
		
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		$line = '"User Name","Date","Latitude","Longitude"';
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		//@JAPR 2014-04-28: Si se deja un espacio, las comillas dobles aparecen en el XLS
		if (getMDVersion() >= esvAccuracyValues) {
			$line .= ',"Accuracy"';
		}
		
		//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
        //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
		$arrMasterDetSectionIds = array();
		if ($anInstance->HasMasterDetSection) 
		{
			$arrMasterDetSectionIds = array_flip($anInstance->MasterDetSectionIDs);
		}
		//@JAPR
		
		//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
		//Carga las definiciones de catálogos utilizadas y dependiendo de las preguntas va asignando que atributos se deberían agregar a cada una,
		//ya que múltiples preguntas de catálogo en la misma encuesta sólo podrían usar atributos posteriores a los previamente usados, por lo 
		//tanto dichos atributos ni los padres no se deben mostrar en preguntas posteriores, sólo aquellos que aun no se estén desplegando
		$arrCatalogs = array();
		$arrCatalogMembers = array();
		$arrLastShownAttributeIdx = array();
		$arrAttributesByQuestion = array();
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrQuestionsByID = array();
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			$objQuestion = $anInstance->Questions[$i];
			$arrQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
			//@JAPR
			
			if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode != dspMatrix)
			{
				$intCatalogID = $anInstance->Questions[$i]->CatalogID;
				$intCatMemberID = $anInstance->Questions[$i]->CatMemberID;
				if ($intCatalogID > 0 && $intCatMemberID > 0)
				{
					if (isset($arrCatalogs[$intCatalogID]))
					{
						$aCatalog = $arrCatalogs[$intCatalogID];
						$aCatalogMembersColl = $arrCatalogMembers[$intCatalogID];
						$intLastShownAttribute = $arrLastShownAttributeIdx[$intCatalogID];
					}
					else 
					{
						$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
						$aCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
						$aCatalogMembersColl = $aCatalogMembersColl->Collection;
						$intLastShownAttribute = -1;
						$arrCatalogs[$intCatalogID] = $aCatalog;
						$arrCatalogMembers[$intCatalogID] = $aCatalogMembersColl;
						$arrLastShownAttributeIdx[$intCatalogID] = $intLastShownAttribute;
					}
					
					//Primero verifica si el Atributo de esta pregunta ya había sido desplegado, si lo fué entonces se trata de un error y por
					//tanto simplemente se agregará a este atributo, de lo contrario se incluirían otros posteriores lo cual estaría incorrecto
					$blnAttributeUsed = true;
					$intNumAttributes = count($aCatalogMembersColl);
					for ($intIdx = $intLastShownAttribute + 1; $intIdx < $intNumAttributes; $intIdx++)
					{
						if ($aCatalogMembersColl[$intIdx]->MemberID == $intCatMemberID)
						{
							$blnAttributeUsed = false;
							break;
						}
					}
					
					$arrAttributesColl = array();
					//Si el atributo ya estaba usado, simplemente brincará este If para sólo agregar el atributo en cuestión, de lo contrario
					//agrega todos los padres hasta llegar al atributo de la pregunta
					if (!$blnAttributeUsed)
					{
						//Crea la entrada para esta pregunta usando su índice, el contenido será el Array de todos los Atributos que debe imprimir
						//los cuales consisten en los Atributos Padres del asociado a la pregunta, incluyendo al propio atributo de la pregunta, siempre
						//y cuando NO se hubieran desplegado ya en otra pregunta previa
						for($intLastShownAttribute = $intLastShownAttribute +1; $intLastShownAttribute < $intNumAttributes; $intLastShownAttribute++)
						{
							if ($aCatalogMembersColl[$intLastShownAttribute]->MemberID == $intCatMemberID)
							{
								break;
							}
							
							$arrAttributesColl[] = $aCatalogMembersColl[$intLastShownAttribute];
						}
					}

					//En realidad no se necesita agregar el Atributo de la pregunta, ya que el código mas abajo simplemente usará el nombre de la
					//pregunta, sólo se necesitan los atributos padre pues esos no se conocían y hay que anteponerlos a la pregunta en cuestión
					//$arrAttributesColl[] = null;	// NO es necesario activar esta línea, si el count de este array es 0 aun así funciona abajo
					
					//Agrega el atributo de la pregunta como el texto de la misma directamente
					$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID] = $arrAttributesColl;
				}
			}
		}
		//@JAPR
		
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		$titleDynamicPageDSC = false;
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $anInstance->Questions[$i]->getAttributeName();
				
                //@JAPR 2013-03-07: Corregido un bug, había faltado que las preguntas múltiples de maestro-detalle no se separaran por respuesta (#28777)
                if ($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
                {
                    foreach ($anInstance->Questions[$i]->SelectOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
                    //Se genera un nuevo Array vacio según el tipo de respuestas múltiples para ser reutilizado al recorrer las encuestas capturadas
                    if($anInstance->Questions[$i]->QTypeID == qtpMulti && count($anInstance->Questions[$i]->SelectOptions) > 0)
                    {
                        if($anInstance->Questions[$i]->MCInputType==0)
                        {
                            //En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se toma tal cual
                            //y sólo se rellena con cadenas vacias en su valores
                            $arrAnswers = array_flip(array_values($anInstance->Questions[$i]->SelectOptions));
                        }
                        else 
                        {
                            //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                            //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                            //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                            $arrAnswers = array_values($anInstance->Questions[$i]->SelectOptions);
                        }

                        $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                        foreach ($arrAnswers as $intID => $strValue)
                        {
                            $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                        }
                    }
                    //@JAPR
                }
                else if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->QDisplayMode == dspMatrix)
                {
                    if($anInstance->Questions[$i]->OneChoicePer==0)
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptions;
                    }
                    else 
                    {
                        $theOptions = $anInstance->Questions[$i]->SelectOptionsCol;
                    }

                    foreach ($theOptions as $displayText)
                    {
                        $line.=',"'.getQuotedStringForExport($strText.'-'.$displayText).'"';
                    }

                    //En este caso las respuestas posibles se encuentran siempre en el string de la respuesta capturada separadas por ";", 
                    //simplemente si una respuesta no fué capturada entonces viene vacia, pero se deja su lugar en el mismo orden que en la lista
                    //de posibles respuestas, por tanto el array original se queda indexado por posición y simplemente agregamos vacio como el valor
                    $arrAnswers = array_values($theOptions);

                    $anInstance->Questions[$i]->EmptyAnswers = $arrAnswers;
                    foreach ($arrAnswers as $intID => $strValue)
                    {
                        $anInstance->Questions[$i]->EmptyAnswers[$intID] = 'NA';
                    }
                }
                else 
                {
                    //@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
                    //Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
                    //de catálogo por lo que deben ser selección sencilla exclusivamente
                    if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
                    {
                        $arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
                        if (!is_null($arrAttributesColl))
                        {
                            foreach ($arrAttributesColl as $aCatMember)
                            {
                                $line.=',"'.getQuotedStringForExport($aCatMember->MemberName).'"';
                            }
                        }
                    }
                    
                    if($titleDynamicPageDSC==false && $anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID)
                    {
                    	$line.=',"DyamicPageDSC"';
                    	$titleDynamicPageDSC = true;
                    }

                    //@JAPR
                    $line.=',"'.getQuotedStringForExport($strText).'"';
                    //Julio2013: Que las preguntas multiple choice de la sección dinámica desplieguen todos
                    //los elementos de los check box indicando en una columna aparte si está seleccionado el
                    //elemento
                    if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID)
                    {
                    	$line.=',"Selected"';
                    }

                }
            }
            
			//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
			//Si la pregunta tiene foto configurada y se pidió incluirlas, entonces agrega una columna adicional al archivo
			if ($bIncludePhotos && $objQuestion->HasReqPhoto > 0) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                if ($objQuestion->QTypeID === qtpSignature) {
                	$strText .= ' '.translate('Signature');
                }
                else {
                	$strText .= ' '.translate('Photo');
                }
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
            
			//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
			if ($objQuestion->HasReqComment) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $objQuestion->getAttributeName();
                /*@AAL Faltaba agregar la etiqueta del comentario cuando ésta se personaliza, al no 
					si no se definio alguna en la pregunta se asigna la de la sección si es que la hay */
					$CommentLabel = $objQuestion->CommentLabel;
					if (trim($CommentLabel) == "") 
						$CommentLabel = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID)->CommentLabel;
					$strText = ' '. $CommentLabel;
					//@AAL
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			
			//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
			//Verifica si esta pregunta contiene o no Score forma de calcular Score
			if ($objQuestion->HasScores) {
                $strText .= ' '.translate('Score');
                $line.=',"'.getQuotedStringForExport($strText).'"';
			}
			//@JAPR
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
		$fieldAttrLatitude = '0';
		$fieldAttrLongitude = '0';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = $catTable."."."DSC_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = $catTable."."."DSC_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$fieldAttrAccuracy = ', 0';
				if ($aSurvey->AccuracyAttribID > 0) {
					//@JAPR 2014-01-29: Corregido un bug, faltaba la ', ' y provocaba un error en el query
					$fieldAttrAccuracy = ', '.$catTable."."."DSC_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		$sql.=", ".$fieldAttrLatitude.", ".$fieldAttrLongitude.$fieldAttrAccuracy;
		
		//Se modifica para usarse al leer el recordset
		$fieldAttrLatitude = '';
		$fieldAttrLongitude = '';
		$fieldAttrAccuracy = '';
		if ($aSurvey->FactKeyDimID > 0) {
			if ($aSurvey->LatitudeAttribID > 0) {
				$fieldAttrLatitude = "dsc_".$aSurvey->LatitudeAttribID;
			}
			if ($aSurvey->LongitudeAttribID > 0) {
				$fieldAttrLongitude = "dsc_".$aSurvey->LongitudeAttribID;
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				if ($aSurvey->AccuracyAttribID > 0) {
					$fieldAttrAccuracy = "dsc_".$aSurvey->AccuracyAttribID;
				}
			}
			//@JAPR
		}
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		//@JAPR 2013-04-22: Agregadas la latitud y longitud al reporte
		//Se forzará a utilizar la tabla de hechos para extraer la información adicional
		if ($aSurvey->FactKeyDimID > 0) {
			$fromCat.=", ".$aSurvey->FactTable." B";
			$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
			$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
			$strAnd = " AND ";
			
			$catTable = "RIDIM_".$aSurvey->FactKeyDimID;
			$joinField = "RIDIM_".$aSurvey->FactKeyDimID."KEY";
			$fromCat.=", ".$catTable;
			$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
			$arrayTables[$catTable] = $catTable;
		}
		//@JAPR
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//Modificada la condición para omitir el ciclo en lugar de preguntar si debe entrar o no a procesar algo
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($objQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat.=", ".$aSurvey->FactTable." B";
						$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($objQuestion->QTypeID == qtpSingle && $objQuestion->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$objQuestion->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = $aCatMember->IndDimID;
								$catTable = "RIDIM_".$memberParentID;
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
								
								//Si se vuelve a repetir el catalogo, con este arreglo
								//se evita que se vuelva a dar de alta en el FROM dicha tabla
								if(!isset($arrayTables[$catTable]))
								{
									$fromCat.=", ".$catTable;
									$joinField = "RIDIM_".$memberParentID."KEY";
									$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
									$arrayTables[$catTable] = $catTable;
									$strAnd = " AND ";
								}
							}
						}
					}
					//@JAPR
					
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = $catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
					//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
					//de catálogo por lo que deben ser selección sencilla exclusivamente
					if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
					{
						$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
						if (!is_null($arrAttributesColl))
						{
							foreach ($arrAttributesColl as $aCatMember)
							{
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
								$attribField = "DSC_".$memberParentID;
								$sql.=", ".$catTable.".".$attribField;
							}
						}
					}
					//@JAPR
					
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$strAnd.$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
					$strAnd = " AND ";
				}
				//@JAPR
			}
		}
		
		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					if($where!="")
					{
						$where.=" AND ";
					}
					
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
			$strAnd = " AND ";
		}
		
		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
			$strAnd = " AND ";
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
			$strAnd = " AND ";
		}

		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		//El filtro completo se usará tal cual entre las tablas paralela, hechos y catálogos, pero filtros adicionales ya no aplican para extraer
		//los comentarios así que esos se ignorarán
		$strJoins = $where;
		$strJoinsScores = $where;
		if ($strJoins != '') {
			$strJoins = "AND ".$strJoins;
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		
		//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
		$blnMultiRecordSection = ($anInstance->HasDynamicSection || $anInstance->HasMasterDetSection);
		if ($aSurvey->UseStdSectionSingleRec) {
			//Si se usa el registro único, entonces se trae exclusivamente los datos de dichos registros, de lo contrario puede traer cualquier
			//registro ya que antes de este cambio en todos se grababa lo mismo para las preguntas estándar
			if ($where != "") {
				$where .= " AND ";
			}
			$where .= $aSurvey->SurveyTable.".EntrySectionID = 0";
		}
		//@JAPR
		
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
		if ($bIncludePhotos) {
			//Obtenemos el prefijo de la ruta requerida en la dimension imagen
			$script = $_SERVER["REQUEST_URI"];
			//Eliminamos la primera diagonal que separa el archivo php de la carpeta
			$position = strrpos($script, "/");
			$dir = substr($script, 0, $position);
			
			//Obtenemos el siguiente diagonal para de ahi obtener la carpeta principal de trabajo
			$position = strrpos($dir, "/");
			$mainDir = substr($dir, ($position+1));
			//@JAPR 2013-06-03: Corregido un bug, estaba incluyendo 2 veces la ruta del Survey
			//$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/"."survey_".$aSurveyID."/";
			$strPhotoPrefix = $mainDir."/"."surveyimages"."/".trim($_SESSION["PABITAM_RepositoryName"])."/";
			//@JAPR
			
			$arrAllImages = array();
			$sqlImages = "SELECT tImg.FactKey as FactKeyDimVal, tImg.MainFactKey as FactKey, tImg.SurveyID, tImg.QuestionID, tImg.PathImage 
				FROM SI_SV_SurveyAnswerImage tImg, $aSurvey->SurveyTable $fromCat 
				WHERE tImg.SurveyID = $aSurveyID AND tImg.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
				ORDER BY tImg.FactKey, tImg.MainFactKey";	// survey_4/photo_4_26_1_1.jpg
			$aRSImg = $anInstance->Repository->DataADOConnection->Execute($sqlImages);
			if ($aRSImg) {
				while (!$aRSImg->EOF) {
					$intQuestionID = (int) $aRSImg->fields["questionid"];
					$objQuestion = @$arrQuestionsByID[$intQuestionID];
					if (!is_null($objQuestion) && $objQuestion->HasReqPhoto) {
						$factKey = (int) @$aRSImg->fields["factkey"];
						$factKeyDimVal = (int) @$aRSImg->fields["factkeydimval"];
						$strFieldDocumentName = (string) @$aRSImg->fields["pathimage"];
						if (!isset($arrAllImages[$factKeyDimVal])) {
							$arrAllImages[$factKeyDimVal] = array();
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						elseif (!isset($arrAllImages[$factKeyDimVal][$intQuestionID])) {
							$arrAllImages[$factKeyDimVal][$intQuestionID] = array();
						}
						
						$strFieldDocumentName2 = '';
						if (trim($strFieldDocumentName) != '') {
							$strFieldDocumentName2 = $strPhotoPrefix.$strFieldDocumentName;
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
							$arrAllImages[$factKeyDimVal][$intQuestionID][$factKey] = $strFieldDocumentName2;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
							if (count($arrAllImages[$factKeyDimVal][$intQuestionID]) == 0) {
								$arrAllImages[$factKeyDimVal][$intQuestionID][0] = $strFieldDocumentName2;
							}
						}
					}
					$aRSImg->MoveNext();
				}
			}
		}
		
		//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
		$arrAllComments = array();
		$sqlComments = "SELECT tComm.FactKey as FactKeyDimVal, tComm.MainFactKey as FactKey, tComm.SurveyID, tComm.QuestionID, tComm.StrComment 
			FROM SI_SV_SurveyAnswerComment tComm, $aSurvey->SurveyTable $fromCat 
			WHERE tComm.SurveyID = $aSurveyID AND tComm.MainFactKey = $aSurvey->SurveyTable.FactKey $strJoins 
			ORDER BY tComm.FactKey, tComm.MainFactKey";
		$aRSComm = $anInstance->Repository->DataADOConnection->Execute($sqlComments);
		if ($aRSComm) {
			while (!$aRSComm->EOF) {
				$intQuestionID = (int) $aRSComm->fields["questionid"];
				$objQuestion = @$arrQuestionsByID[$intQuestionID];
				if (!is_null($objQuestion) && $objQuestion->HasReqComment) {
					$factKey = (int) @$aRSComm->fields["factkey"];
					$factKeyDimVal = (int) @$aRSComm->fields["factkeydimval"];
					$strComment = (string) @$aRSComm->fields["strcomment"];
						if (!isset($arrAllComments[$factKeyDimVal])) {
						$arrAllComments[$factKeyDimVal] = array();
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					elseif (!isset($arrAllComments[$factKeyDimVal][$intQuestionID])) {
						$arrAllComments[$factKeyDimVal][$intQuestionID] = array();
					}
					
					//Debido a la forma en que se graban las encuestas, se generan múltiples registros de comentarios para las preguntas de secciones
					//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
					//multiples comentarios mientras que en las demás solo insertará el primero recuperado por captura
					if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
						//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
						//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los comentarios
						$arrAllComments[$factKeyDimVal][$intQuestionID][$factKey] = $strComment;
					}
					else {
						//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al comentario
						if (count($arrAllComments[$factKeyDimVal][$intQuestionID]) == 0) {
							$arrAllComments[$factKeyDimVal][$intQuestionID][0] = $strComment;
						}
					}
				}
				$aRSComm->MoveNext();
			}
		}
		
		//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
		$arrAllScores = array();
		$fromScores = $fromCat;
		if(!isset($arrayTables[$aSurvey->FactTable])) {
			$fromScores .= ", ".$aSurvey->FactTable." B ";
			$strJoinsScores .= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
		}
		if ($strJoinsScores != '') {
			$strJoinsScores = " WHERE ".$strJoinsScores;
		}
		
		//Los campos se deben procesar dependiendo del tipo de grabado que tiene configurado la encuesta, aunque en un query se podrían obtener
		//aplicando funciones de agrupación, se optará por procesarlos en memoria para controlar mejor los datos obtenidos
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		$dynamicSectionWithMultiChoice = false;
		if ($anInstance->HasDynamicSection && !is_null($anInstance->DynamicSection)) {
			$dynamicSectionWithMultiChoice = ($anInstance->DynamicSection->ChildCatMemberID > 0);
		}
		$blnHasScoreQuestions = false;
		$strScoreFields = '';
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			if ($objQuestion->HasScores) {
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Las preguntas simple choice no deben ser de catálogo para poder tener Score
						if ($objQuestion->QTypeID == qtpSingle && $objQuestion->UseCatalog == 0 && $objQuestion->IndicatorID > 0) {
							$fieldName = '';
							$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $objQuestion->IndicatorID);
							if (!is_null($objBITAMIndicator)) {
								$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
							}
							if ($fieldName != '') {
								$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_0';
								$strScoreFields .= ', '.$fieldName;
								$blnHasScoreQuestions = true;
							}
						}
						break;
						
					case qtpMulti:
						//Las preguntas múltiples deben ser multi-dimensión tipo checkbox para poder tener Score
						if ($objQuestion->IsMultiDimension && $objQuestion->MCInputType == mpcCheckBox) {
							//En este caso se recorre cada una de las opciones de respuesta, ya que cada una genera un indicador
							foreach ($objQuestion->QIndicatorIds as $intIdex => $intIndicatorID) {
								if ($intIndicatorID > 0) {
									$fieldName = '';
									$objBITAMIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $intIndicatorID);
									if (!is_null($objBITAMIndicator)) {
										$fieldName = @$objBITAMIndicator->field_name.@$objBITAMIndicator->IndicatorID;
									}
									if ($fieldName != '') {
										$fieldName .= ' AS ind_'.$objQuestion->QuestionID.'_'.$intIdex;
										$strScoreFields .= ', '.$fieldName;
										$blnHasScoreQuestions = true;
									}
								}
							}
						}
						break;
						
					default:
						//El resto de las preguntas no contiene scores
						break;
				}
			}
		}
		chdir($strOriginalWD);
		
		if ($blnHasScoreQuestions && $strScoreFields != '') {
			//$strScoreFields = substr($strScoreFields, 2);
			$strAdditionalFields = '';
			$sqlScores = "SELECT $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey $strAdditionalFields $strScoreFields 
				FROM $aSurvey->SurveyTable $fromScores 
				$strJoinsScores 
				ORDER BY $aSurvey->SurveyTable.FactKeyDimVal, $aSurvey->SurveyTable.FactKey";
			$aRSScores = $anInstance->Repository->DataADOConnection->Execute($sqlScores);
			if ($aRSScores) {
				while (!$aRSScores->EOF) {
					$factKey = (int) @$aRSScores->fields["factkey"];
					$factKeyDimVal = (int) @$aRSScores->fields["factkeydimval"];
					$blnFirstRecord = false;
					if (!isset($arrAllScores[$factKeyDimVal])) {
						$blnFirstRecord = true;
						$arrAllScores[$factKeyDimVal] = array();
					}
					
					//Recorre todos los campos e identifica la pregunta, ya que hay menos campos con Scores que preguntas por lo que es mas óptimo así
					foreach ($aRSScores->fields as $strFieldName => $sValue) {
						$arrFieldName = explode('_', $strFieldName);
						//Si no es un campo de pregunta se omite
						if (count($arrFieldName) != 3) {
							continue;
						}
						
						$intQuestionID = (int) @$arrFieldName[1];
						if ($intQuestionID <= 0) {
							continue;
						}
						
						$objQuestion = @$arrQuestionsByID[$intQuestionID];
						if (is_null($objQuestion)) {
							continue;
						}
						
						if (!isset($arrAllScores[$factKeyDimVal][$intQuestionID])) {
							$arrAllScores[$factKeyDimVal][$intQuestionID] = array();
						}
						
						//Obtiene el Score de esta pregunta en este registro específico
						$dblScore = (float) @$sValue;
						
						//Dependiendo de las opciones de la encuesta, pudieran venir scores repetidos o ya venir los correctos, así que se procesa
						//para agregar en el array solo cuando sea necesario
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey];
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							$dblPrevScore = (float) @$arrAllScores[$factKeyDimVal][$intQuestionID][0];
						}
						
						if ($aSurvey->UseStdSectionSingleRec) {
							//Si está activado el registro único, entonces ya es seguro hacer la suma puesto que solo se graban los scores en los
							//registros donde realmente aplican en lugar de repetirlos múltiples veces
							$dblScore += $dblPrevScore;
						}
						else {
							//Si no se tiene registro único, el comportamiento variaba dependiendo del tipo de sección:
							/*- Si la  pregunta era de maestro-detalle, sólo grababa en el registro correspondiente así que era seguro sumar el
								score en ese caso
							  - Si la pregunta era de sección estándar, repetía su valor en cada registro de las secciones múltiples así que solo
							  	se debía tomar el primer valor
							  - Si la pregunta era de sección dinámica, es un problema porque dependería de si había o no una multiple-choice, si
							  	la había entonces se multiplicaban los scores por la cantidad de checkboxes de cada página, de lo contrario se
							  	podía sumar sin problema y los valores solo aplicaban para esta sección
							*/
							if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
								//En estas secciones no hay multiple-choice con score, así que simplemente usa el valor tal como venga, si hubiera
								//alguna multiple choice entonces generaría varios registros, pero al procesar el reporte se usará solo el primero
								//por página así que debería tomar el valor adecuado
							}
							elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice
								if ($objQuestion->QTypeID == qtpMulti) {
									$dblScore += $dblPrevScore;
								}
							}
							else {
								//Sólo debe sumar el Score previo si la pregunta es múltiple-choice, pero además solo si se trata del primer
								//registro ya que de lo contrario daría el resultado multiplicado
								if ($objQuestion->QTypeID == qtpMulti && $blnFirstRecord) {
									$dblScore += $dblPrevScore;
								}
								else {
									//Si fuera simple choice entonces hubiera repetido el mismo valor en todos los registros, por lo tanto
									//se toma el primero procesado simplemente y el resto envía Null para no sobreescribir
									if ($objQuestion->QTypeID == qtpSingle && $blnFirstRecord) {
									}
									else {
										//Cualquier otra condición pone null para no sobreescribir el valor
										$dblScore = null;
									}
								}
							}
						}
						
						//Debido a la forma en que se graban las encuestas, se generan múltiples registros de scores para las preguntas de secciones
						//estándar si estas se encuentran acompañadas de algunas secciones múltiples, así que sólo para las secciones múltiples agregará
						//multiples scores mientras que en las demás solo insertará el primero recuperado por captura
						if ($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//Sección dinámica, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//Sección maestro-detalle, si tiene registros múltiples. Se usa el FactKey para accesar a los scores
							$arrAllScores[$factKeyDimVal][$intQuestionID][$factKey] = $dblScore;
						}
						else {
							//Sección estándar, sólo agrega el registro si no había ya uno, se usa el índice 0 para accesar al score
							if (!is_null($dblScore)) {
								$arrAllScores[$factKeyDimVal][$intQuestionID][0] = $dblScore;
							}
						}
					}
					
					$aRSScores->MoveNext();
				}
			}
		}
		//@JAPR
		
		$objReportData = BITAMReport::NewInstance($aRepository, $aSurveyID);
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
			
			//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
			$factKey = (int)$aRS->fields["factkey"];
			//Se modificó la lógica para cargar una instancia de report mejor, ya que eso incluiría a la sección Maestro-detalle o cualquier nueva
			//funcionalidad que se agregue en el futuro en lugar de tener una función independiente como getDynamicSectionValuesByFactKeyDimVal
			//para cargar por separado cada cosa, al final la instancia de report ya incluía el mismo array que genera la función mencionada así
			//que se tenía código duplicado realmente
			if ($blnMultiRecordSection) {
				require_once('reportExt.inc.php');
				$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $factKey, $aSurveyID);
				//PrintMultiArray($objReportData);
				$arrayDynSectionValues = @$objReportData->ArrayDynSectionValues[$anInstance->DynamicSectionID];
				if (is_null($arrayDynSectionValues) || !is_array($arrayDynSectionValues)) {
					$arrayDynSectionValues = array();
				}
			}
			/*
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			*/
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = getQuotedStringForExport(BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = getQuotedStringForExport($aRS->fields["dateid"]);
			//@JAPR 2013-04-23: Agregadas la latitud y longitud al reporte
			$latitude = (float) @$aRS->fields[$fieldAttrLatitude];
			$longitude = (float) @$aRS->fields[$fieldAttrLongitude];
			$line='"'.$userName.'","'.$dateValue.'","'.$latitude.'","'.$longitude.'"';
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$accuracy = (float) @$aRS->fields[$fieldAttrAccuracy];
				$line .= ',"'.$accuracy.'"';
			}
			//@JAPR
			$arrayLinesDyn = array();
			$arrayLinesMD = array();
			$iLineMD = 0;
			$factKeyByMD = array();
			//@JAPR
			$validateValue = '';
			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{	
				$spaceBefore=0;
				//@JAPR 2013-03-05: Agregado el soporte para secciones Maestro-detalle (#27957)
				$objQuestion = $anInstance->Questions[$i];
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
	            if ($blnValidQuestion) {
	                $strCatParentAttibuteValues = '';
					//El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
					//que tengan seccion dinamica
					if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
					{
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						//NO se soportan preguntas de selección sencilla que usen catálogo dentro de secciones dinámicas, por lo tanto no es necesario
						//programar esta parte, si se hubiesen creado algunas encuestas con este caso simplemente tronarían desde la App así que no
						//habría necesidad de llegar al reporte (aunque el código de Web generalmente soporta mas cosas, no es algo permitido en realidad)
						//@JAPR
						
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
						{
							$lastValue = "";
							$iLine = 0;
							if(count($arrayLinesDyn)>0)
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									/*
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue.=$element["desc"]."=".(string) @$element[$idxName];
									*/
									if(isset($element[$idxName]))
									{
										$arrayLinesDyn[$iLine] .=	',"'.getQuotedStringForExport($element[$idxName]).'"';
										$lastValue = $element[$idxName];
									}
									else
									{
										if($anInstance->Questions[$i]->QTypeID!=qtpOpenNumeric && $anInstance->Questions[$i]->QTypeID!=qtpCalc)
										{
											$arrayLinesDyn[$iLine] .=	',"'.getQuotedStringForExport($lastValue).'"';
										}
										else
										{
											$arrayLinesDyn[$iLine] .=	',""';
										}
									}
									$iLine++;
								}
							}
							else
							{
								$lastDynamicPageDSC = "";
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									
									if(isset($element["DynamicPageDSC"]) && $element["DynamicPageDSC"]!="")
									{
										$dynamicPageDSCValue = $element["DynamicPageDSC"];
										$lastDynamicPageDSC = $element["DynamicPageDSC"];
									}
									else
									{
										$dynamicPageDSCValue = $lastDynamicPageDSC;
									}
									
									if(isset($element[$idxName]))
									{
										$arrayLinesDyn[$iLine] = $line.',"'.getQuotedStringForExport($dynamicPageDSCValue).'","'.getQuotedStringForExport($element[$idxName]).'"';
										$lastValue = $element[$idxName];
									}
									else
									{
										if($anInstance->Questions[$i]->QTypeID!=qtpOpenNumeric && $anInstance->Questions[$i]->QTypeID!=qtpCalc)
										{
											$arrayLinesDyn[$iLine] = $line.',"'.getQuotedStringForExport($dynamicPageDSCValue).'","'.getQuotedStringForExport($lastValue).'"';
										}
										else
										{
											$arrayLinesDyn[$iLine] = $line.',"'.getQuotedStringForExport($dynamicPageDSCValue).'",""';
										}
									}
									$iLine++;
								}
								$spaceBefore++;
							}
							$strValue="";
						}
						else 
						{
							//Si es entrada de tipo Checkbox, el valor seran
							//todo los valores seleccionados concatenados con ;
							if($anInstance->Questions[$i]->MCInputType==0)
							{
								$lastValue = "";
								$iLine = 0;
								if(count($arrayLinesDyn)==0)
								{
									$lastDynamicPageDSC = "";
									foreach ($arrayDynSectionValues as $element)
									{
										$idxName = $anInstance->QuestionFields[$i];
										$valueInteger = (int) @$element[$idxName];
										
										if(isset($element["DynamicPageDSC"]) && $element["DynamicPageDSC"]!="")
										{
											$dynamicPageDSCValue = $element["DynamicPageDSC"];
											$lastDynamicPageDSC = $element["DynamicPageDSC"];
										}
										else
										{
											$dynamicPageDSCValue = $lastDynamicPageDSC;
										}
										
										if($valueInteger!=0)
										{	/*
											if($strValue!="")
											{
												$strValue.=";";
											}
											
											$strValue.=$element["desc"];
											*/
											$arrayLinesDyn[]=$line.',"'.getQuotedStringForExport($dynamicPageDSCValue).'","'.getQuotedStringForExport($element["desc"]).'","YES"';
										}
										else
										{
											$arrayLinesDyn[]=$line.',"'.getQuotedStringForExport($dynamicPageDSCValue).'","'.getQuotedStringForExport($element["desc"]).'","NO"';
										}
										
									}
									$spaceBefore++;
								}
								else
								{
									$lastDynamicPageDSC = "";
									foreach ($arrayDynSectionValues as $element)
									{
										$idxName = $anInstance->QuestionFields[$i];
										$valueInteger = (int) @$element[$idxName];
										
										if(isset($element["DynamicPageDSC"]) && $element["DynamicPageDSC"]!="")
										{
											$dynamicPageDSCValue = $element["DynamicPageDSC"];
											$lastDynamicPageDSC = $element["DynamicPageDSC"];
										}
										else
										{
											$dynamicPageDSCValue = $lastDynamicPageDSC;
										}
										
										if($valueInteger!=0)
										{	/*
											if($strValue!="")
											{
												$strValue.=";";
											}
											
											$strValue.=$element["desc"];
											*/
											$arrayLinesDyn[$iLine].=',"'.getQuotedStringForExport($element["desc"]).'","YES"';
										}
										else
										{
											$arrayLinesDyn[$iLine].=',"'.getQuotedStringForExport($element["desc"]).'","NO"';
										}
										$iLine++;
									}
								}
								$strValue="";
								
							}
							else 
							{
								foreach ($arrayDynSectionValues as $element)
								{
									$idxName = $anInstance->QuestionFields[$i];
									
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue.=$element["desc"]."=".(string) @$element[$idxName];
								}
							}
						}
						
						$validateValue = $strValue;
						//$line.=',"'.getQuotedStringForExport($strValue).'"';
					}
					//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
					elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID]))
					{
						//Se trata de una pregunta de una sección Maestro - Detalle, por lo que su tratamiendo es muy similar a las preguntas de secciones
						//dinámicas con la única diferencia que las preguntas de tipo Multiple Choice no reciben tratamiento especial
						$fieldName =$anInstance->QuestionFields[$i];
						$strValue = "";
						//Obtenemos el valor de esta pregunta a partir del arrreglo $anInstance->ArrayMasterDetSectionValues
						//Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
						//porque todavia no se sabe exactamente como se va a comportar
						$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
						if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
							$arrMasterDetSectionValues = array();
						}
						
						$intIndex = 1;
						
						foreach ($arrMasterDetSectionValues as $element)
						{
							$idxName = $anInstance->QuestionFields[$i];
							$strTempVal = (string) @$element[$idxName];
							//Si tiene registro único activado, ya no hay forma en que este valor no corresponda con la sección maestro-detalle
							if (!$aSurvey->UseStdSectionSingleRec && $anInstance->HasDynamicSection)
							{
								//Estas validaciones sólo aplican si hay secciones dinámicas, de lo contrario todos los valores se agregan tal cual
								//Conchita, antes estaba asi 2012-12-04: if (trim($strTempVal) === '' || (((int) $strTempVal) == 0) && is_numeric($strTempVal))
								//@JAPR 2012-12-04: Ok, se remueve el (int) para permitir flotantes, pero aun así se debe validar que si es exactamente '0'
								//entonces no se muestre, porque probablemente es un registro de otra sección
								if (trim($strTempVal) === '' || ($objQuestion->QTypeID == qtpOpenNumeric && trim($strTempVal) == '0' && is_numeric($strTempVal)))
								{
									//Se trata de una valor completamente vacio, no se agrega pues podría ser de una sección dinámica
									$strTempVal = false;
								}
							}
							
							if ($strTempVal !== false)
							{	/*
								if($strValue!="")
								{
									$strValue.="; ";
								}
								*/
								//if($anInstance->Questions[$i]->FormatMask!='') {
								//	$strTempVal = formatNumber($strTempVal,$anInstance->Questions[$i]->FormatMask);
								//}
								
								//$strValue .= "<".$intIndex."=".$strTempVal.">";
								//$strValue .= "<".$element[$idxName].">";
								//@JAPR 2014-04-28: Corregido un bug, las pregunta simple choice de catálogo en secciones maestro-detalle requieren
								//tratamiento especial
								if ($aSurvey->DissociateCatDimens && $anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true && $anInstance->Questions[$i]->CatalogID > 0) {
									$strCatDimValAnswers = (string) @$element[$anInstance->Questions[$i]->SurveyCatField];
									$arrayCatValues = array();
									if ($strCatDimValAnswers != '') {
										$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
										$arrayCatClaDescrip = array();
										$arrayTemp = array();
										$arrayInfo = array();
										foreach($arrayCatPairValues as $elementTmp)
										{
											$arrayInfo = explode('_SVSep_', $elementTmp);
											//Obtener el cla_descrip y almacenar (key_1178)
											$arrayTemp = explode('_', $arrayInfo[0]);
											$arrayCatClaDescrip[] = $arrayTemp[1];
											//Obtener el valor del catalogo
											$arrayCatValues[] = $arrayInfo[1];
										}
									}
									
									//En este caso se extrae la descripción del filtro de la respuesta que corresponde con la posición de cada atributo
									//asociado a esta pregunta, el valor propio de la pregunta se extrae directo de la respuesta en lugar del filtro
									$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
									if (!is_null($arrAttributesColl))
									{
										foreach ($arrAttributesColl as $aCatMember)
										{
											$strCatTempVal = (string) @$arrayCatValues[$aCatMember->MemberOrder -1];
											if(isset($arrayLinesMD[$element["FactKey"]]))
											{
												$arrayLinesMD[$element["FactKey"]].=',"'.getQuotedStringForExport($strCatTempVal).'"';
											}
											else {
												$arrayLinesMD[$element["FactKey"]] =$line.',"'.getQuotedStringForExport($strCatTempVal).'"';
												
												if(!isset($factKeyByMD[$anInstance->Questions[$i]->SectionID]))
												{
													$factKeyByMD[$anInstance->Questions[$i]->SectionID] = array();
												}
												
												if(!in_array($element["FactKey"], $factKeyByMD[$anInstance->Questions[$i]->SectionID]))
												{
													$factKeyByMD[$anInstance->Questions[$i]->SectionID][] = $element["FactKey"];
												}
											}
										}
									}
								}
								
								//Al finalizar de procesar los atributos previos al de la pregunta, el valor de la respuesta del atributo principal
								//se extrae de la misma manera que antes
								//@JAPR
								
								if(isset($arrayLinesMD[$element["FactKey"]]))
								{
									$arrayLinesMD[$element["FactKey"]].=',"'.getQuotedStringForExport($strTempVal).'"';
								}
								else
								{
									$arrayLinesMD[$element["FactKey"]]=$line.',"'.getQuotedStringForExport($strTempVal).'"';
									
									if(!isset($factKeyByMD[$anInstance->Questions[$i]->SectionID]))
									{
										$factKeyByMD[$anInstance->Questions[$i]->SectionID] = array();
									}
									
									if(!in_array($element["FactKey"], $factKeyByMD[$anInstance->Questions[$i]->SectionID]))
									{
										$factKeyByMD[$anInstance->Questions[$i]->SectionID][] = $element["FactKey"];
									}
								}
							}
							$intIndex++;
							//@JAPR
						}
						
						$validateValue = $strValue;
					}
					//@JAPR
					else 
					{
						if ($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==true)
						{
							//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
							//Obtiene el array de los atributos padre del seleccionado en la pregunta para agregarlos como columna (sólo aplica para preguntas
							//de catálogo por lo que deben ser selección sencilla exclusivamente
							if($anInstance->Questions[$i]->QTypeID == qtpSingle && $anInstance->Questions[$i]->CatalogID > 0)
							{
								$arrAttributesColl = @$arrAttributesByQuestion[$anInstance->Questions[$i]->QuestionID];
								if (!is_null($arrAttributesColl))
								{
									foreach ($arrAttributesColl as $aCatMember)
									{
										//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
										if ($aSurvey->DissociateCatDimens) {
											$memberParentID = $aCatMember->IndDimID;
											$attribField = "DSC_".$memberParentID;
										}
										else {
											$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$aCatMember->MemberID);
											$attribField = "DSC_".$memberParentID;
										}
										$lowerFieldName = strtolower($attribField);
										$strCatParentAttibuteValues .= ',"'.$aRS->fields[$lowerFieldName].'"';
										//@JAPR
									}
									//$strCatParentAttibuteValues = substr($strCatParentAttibuteValues, 1);
								}
							}
							//@JAPR
							
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
								$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
							
							$attribField = "DSC_".$memberParentID;
							$lowerFieldName = strtolower($attribField);
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=','.$aRS->fields[$lowerFieldName];
							}
							else
							{
								$validateValue = $aRS->fields[$lowerFieldName];
								//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
							}
						}
						else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
						{
							$factKey = (int)$aRS->fields["factkey"];
							$separator = "_SVSep_";
							//En el caso de las matrices de simple choice vamos a obtener la informacion desde una funcion q realice 
							//en una cadena con enters las respuestas q fueron capturadas
							$strAnswersSCMatrix = $anInstance->getOnlyAnswersSCMatrix($anInstance->Questions[$i], $factKeyDimVal, $factKey, $separator);
							
							$validateValue = $strAnswersSCMatrix;
						}
						else 
						{
							$lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
							if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
							{
								//$line.=','.$aRS->fields[$lowerFieldName];
								$validateValue = (float) $aRS->fields[$lowerFieldName];
								//$line.=','.$validateValue;
							}
							else
							{
								if($anInstance->FieldTypes[$i]==qtpMulti)
								{
									//$line.=',"'.getQuotedStringForExport($aRS->fields[$lowerFieldName]).'"';
									$newValidateValue = '';
									$validateValue = $aRS->fields[$lowerFieldName];
									
									if(is_null($validateValue))
									{
										$validateValue = "";
									}
									
									if($anInstance->Questions[$i]->MCInputType==0)
									{
										$newValidateValue = $validateValue;
										
										//se debe reemplazar por numeros si es ranking
										//si es dsplabelnum
										if($anInstance->Questions[$i]->QDisplayMode==6){
											//si es tipo ranking debe de poner numeros en vez de la opcion
											//hay que convertir los valores en posiciones
											
											$tmp = explode(";",$newValidateValue);
											$numElem = count($tmp);
											
											$indice = 1; //el numerito comienza en 1
											$cadtmp = '';
											foreach($tmp as $x){
												$cadtmp.=$indice.';';
												$indice++;
											}
											//eliminar ultimo ; de la cadena
											$cadtmp = substr($cadtmp,0,-1);
											//reemplazar validate value con los numeros separados por punto y coma
											//$newValidateValue = $cadtmp;
											
										}
									}
									else
									{
										$arrayTemporal = explode(';', $validateValue);
										
										foreach($arrayTemporal as $elementTemp)
										{
											if($elementTemp=='')
											{
												$elementTemp = 'NA';
											}
											
											if($newValidateValue!='')
											{
												$newValidateValue.=';';
											}
											
											$newValidateValue.=$elementTemp;
										}
									}
									$validateValue = $newValidateValue;
									//$line.=',"'.getQuotedStringForExport($newValidateValue).'"';
								}
								else
								{
	                                $validateValue = $aRS->fields[$lowerFieldName];
									
	                                if(is_null($validateValue) || $validateValue=="")
	                                {
	                                    $validateValue = "NA";
	                                }
	                                //$line.=',"'.getQuotedStringForExport($validateValue).'"';
								}
							}
						}
					}
					
					//@JAPR 2012-01-10: Modificado para que las respuestas de tipo selección múltiple se generen con una columna por cada posible respuesta
					if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID != $anInstance->DynamicSectionID && !isset($arrMasterDetSectionIds[$anInstance->Questions[$i]->SectionID]))
					{
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						
						if(trim($validateValue)!="")
						{
							$arrayTemporal = explode(';', $validateValue);
						}
						else 
						{
							$arrayTemporal = array();
						}
						
						if($anInstance->Questions[$i]->MCInputType==0)
						{
							//En este caso sólo vienen los nombres de las opciones seleccionadas, así que el Array de respuestas asociativo se reemplaza
							//tal cual
							foreach($arrayTemporal as $elementTemp)
							{
								@$arrAnswers[$elementTemp] = $elementTemp;
							}
						}
						else 
						{
							//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
							//posibles respuestas no es asociativo en este caso)
							$intNumAnsw = count($arrayTemporal);
							for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
							{
								$elementTemp = $arrayTemporal[$intIdx];
								if (!is_null($elementTemp) && trim($elementTemp) != '')
								{
									@$arrAnswers[$intIdx] = $elementTemp;
								}
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						//tengo elementos en orden, y los NA
						//si es dsplabelnum hay que recorrer 
						if($anInstance->Questions[$i]->QDisplayMode==6){
							$idx= 1;
							$cadwithNA ='';
							$clon = array();
							$cont1 = count($tmp);
							//for($i=0;$i<count($arrAnswers);$i++){
							foreach ($arrAnswers as $ix=>$elementTemp){
								$clon[$ix]='NA';
								for($jx=0;$jx<$cont1;$jx++){
									if($arrAnswers[$ix]==$tmp[$jx]){
										$clon[$ix]=$jx+1;
										
									}
									
									
								}
							}
							
							/*foreach($arrAnswers as $x){
								foreach($tmp as $y){	//el tmp no trae NAs solo trae los valores
									$idx++;
								}
								$idx= 1;
							}*/
						}
						if($anInstance->Questions[$i]->QDisplayMode==6){
							foreach ($clon as $elementTemp2)
							{
								$line.=',"'.getQuotedStringForExport($elementTemp2).'"';
								
								if($anInstance->Questions[$i]->SectionType==sectNormal)
								{
									foreach ($arrayLinesDyn as $idxDyn=>$aLineDyn)
									{
										$arrayLinesDyn[$idxDyn].=',"'.getQuotedStringForExport($elementTemp2).'"';
									}
									
									foreach ($arrayLinesMD as $idxMD=>$aLineMD)
									{
										$arrayLinesMD[$idxMD].=',"'.getQuotedStringForExport($elementTemp2).'"';
									}
								}
							}
						}
						else{
							foreach ($arrAnswers as $elementTemp)
							{
								$line.=',"'.getQuotedStringForExport($elementTemp).'"';
								if($anInstance->Questions[$i]->SectionType==sectNormal)
								{
									foreach ($arrayLinesDyn as $idxDyn=>$aLineDyn)
									{
										$arrayLinesDyn[$idxDyn].=',"'.getQuotedStringForExport($elementTemp).'"';
									}
									
									foreach ($arrayLinesMD as $idxMD=>$aLineMD)
									{
										$arrayLinesMD[$idxMD].=',"'.getQuotedStringForExport($elementTemp).'"';
									}
								}
							}
						}
					}
					else if($anInstance->Questions[$i]->QTypeID==qtpSingle && $anInstance->Questions[$i]->UseCatalog==false && $anInstance->Questions[$i]->QDisplayMode==dspMatrix)
					{
						$separator = "_SVSep_";
						
						//Copia el arreglo de datos vacios de la instancia de la pregunta para rellenar con el valor correspondiente de las respuestas
						//seleccionadas en esta encuesta capturada
						$arrAnswers = $anInstance->Questions[$i]->EmptyAnswers;
						$arrayTemporal = explode($separator, $validateValue);
						//En este caso todas las posibles respuestas vienen en el String así que índice a índice se reemplaza el valor (el array de
						//posibles respuestas no es asociativo en este caso)
						$intNumAnsw = count($arrayTemporal);
						for($intIdx = 0;$intIdx < $intNumAnsw; $intIdx++)
						{
							$elementTemp = $arrayTemporal[$intIdx];
							if (!is_null($elementTemp) && trim($elementTemp) != '')
							{
								@$arrAnswers[$intIdx] = $elementTemp;
							}
						}
						
						//Separa los valores por "," para que sean incluidos como múltiples columnas
						$validateValue = implode(',', $arrAnswers);
						foreach ($arrAnswers as $elementTemp)
						{
							$line.=',"'.getQuotedStringForExport($elementTemp).'"';
						}
					}
					else 
					{
						for($iSpace=0; $iSpace<$spaceBefore; $iSpace++)
						{
							$line.=',""';
						}
						
						//@JAPR 2012-04-26: Agregadas las columnas de los Atributos padre para las preguntas de tipo catálogo
						$line.= $strCatParentAttibuteValues.',"'.getQuotedStringForExport($validateValue).'"';
						
	                    //Julio2013: Que las preguntas multiple choice de la sección dinámica desplieguen todos
	                    //los elementos de los check box indicando en una columna aparte si está seleccionado el
	                    //elemento, esta columna vacía se agrega al renglon correspondiente a la seccion estandar y correponde
	                    //a la columna SELECTED con valor YES O NO
						
						if($anInstance->Questions[$i]->QTypeID == qtpMulti && $anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID)
	                    {
	                    	$line.=',""';
	                    }
	                    
	                    if($anInstance->Questions[$i]->SectionType==sectNormal)
	                    {
	                    	if($anInstance->Questions[$i]->QTypeID!=qtpOpenNumeric && $anInstance->Questions[$i]->QTypeID!=qtpCalc)
	                    	{
		                    	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
		                    	{
		                    		$arrayLinesDyn[$idxDyn].=$strCatParentAttibuteValues.',"'.getQuotedStringForExport($validateValue).'"';
		                    	}
		                    	
		                    	foreach ($arrayLinesMD as $idxMD => $aLineMD)
		                    	{
		                    		$arrayLinesMD[$idxMD].=$strCatParentAttibuteValues.',"'.getQuotedStringForExport($validateValue).'"';
		                    	}
	                    	}
	                    	else
	                    	{	//las preguntas númericas de una sección estándar 
	                    		//no se repiten en las dinámicas y maestro detalle
		                    	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
		                    	{
		                    		$arrayLinesDyn[$idxDyn].=$strCatParentAttibuteValues.',""';
		                    	}
		                    	
		                    	foreach ($arrayLinesMD as $idxMD => $aLineMD)
		                    	{
		                    		$arrayLinesMD[$idxMD].=$strCatParentAttibuteValues.',""';
		                    	}
	                    	}
	                    }
	                    
	                    if($anInstance->Questions[$i]->SectionType==sectMasterDet)
	                    {
	                    	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
	                    	{
	                    		$arrayLinesDyn[$idxDyn].=',""';
	                    	}
	                    	
	                    	foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                    	{
	                    		if(isset($factKeyByMD[$anInstance->Questions[$i]->SectionID]) && is_array($factKeyByMD[$anInstance->Questions[$i]->SectionID]))
	                    		{
		                    		if(!in_array($idxMD, $factKeyByMD[$anInstance->Questions[$i]->SectionID]))
		                    		{
		                    			$arrayLinesMD[$idxMD].=',""';
		                    		}
	                    		}
	                    		else
	                    		{
	                    			$arrayLinesMD[$idxMD].=',""';
	                    		}
	                    	}
	                    }
					}
				}
				
				//@JAPR 2013-05-20: Agregado el parámetro para incluir las imagenes en el reporte XLS
				if ($bIncludePhotos && $objQuestion->HasReqPhoto) {
					//El array de fotos viene ordenado por factKey dentro de la pregunta, esto es loa fotos por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrImages = @$arrAllImages[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrImages) || !is_array($arrImages)) {
						$arrImages = array();
					}
					$arrayFactKeyMDImg = array();					
					$strValue = '';
					if (count($arrImages) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							/*
							foreach ($arrImages as $intImgFactKey => $strImage) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intImgFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan fotos por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strImage) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strImage;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
							*/
							//Se agrega cada imagen en la fila que le corresponda
							$iLine = 0;
							foreach ($arrayDynSectionValues as $element)
							{
								$factKey = $element["FactKey"];
								if(isset($arrImages[$factKey]))
								{
									$arrayLinesDyn[$iLine].=',"'.$arrImages[$factKey].'"';
								}
								else
								{
									$arrayLinesDyn[$iLine].=',""';
								}
								$iLine++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strImage = (string) @$arrImages[$intFactKey];
								/*
								if (trim($strImage) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strImage.">";
								}
								*/
								//Se cambió porque ahora las Maestro Detalle están en varias filas
								//en el archivo de excel
								if (trim($strImage) != '')
								{
									$arrayLinesMD[$intFactKey].=',"'."<".$intIndex."=".$strImage.'>"';
									$arrayFactKeyMDImg[]=$intFactKey;
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber una foto
							$strValue = (string) @$arrImages[0];
						}
					}
					
					
					if($anInstance->Questions[$i]->SectionType==sectNormal)
					{
						$line.=',"'.getQuotedStringForExport($strValue).'"';
						
	                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
	                	{
	                		$arrayLinesDyn[$idxDyn].=',"'.getQuotedStringForExport($strValue).'"';
	                	}
	                	
	                	foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		$arrayLinesMD[$idxMD].=',"'.getQuotedStringForExport($strValue).'"';
	                	}
					}
					
					if($anInstance->Questions[$i]->SectionType==sectDynamic)
					{
	                	$line.=',""';
	                	
	                	if(count($arrImages)==0)
	                	{
		                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
		                	{
		                		$arrayLinesDyn[$idxDyn].=',""';
		                	}
	                	}

						foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		$arrayLinesMD[$idxMD].=',""';
	                	}
					}
					
					if($anInstance->Questions[$i]->SectionType==sectMasterDet)
					{
	                	$line.=',""';
	                	
	                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
	                	{
	                		$arrayLinesDyn[$idxDyn].=',""';
	                	}
	                	
						foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		if(!in_array($idxMD,$arrayFactKeyMDImg))
	                		{
	                			$arrayLinesMD[$idxMD].=',""';
	                		}
	                	}
					}
				}
				
				//@JAPR 2013-03-06: Agregados los comentarios al reporte (#28011)
				if ($objQuestion->HasReqComment) {
					//El array de comentarios viene ordenado por factKey dentro de la pregunta, esto es los comentarios por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrComments = @$arrAllComments[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrComments) || !is_array($arrComments)) {
						$arrComments = array();
					}
					
					$arrayFactKeyMDCom = array();
					$strValue = '';
					if (count($arrComments) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							/*
							foreach ($arrComments as $intCommFactKey => $strComment) {
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									//@JAPR 2013-03-07: Corregido un bug, estaba comparando contra el factKey del reporte en lugar del comentario (#28777)
									if ($intFactKey == $intCommFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan comentarios por página así que se descartan los registros de otros CheckBoxes
								if (!isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strComment) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strComment;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
							*/
							//Se agrega cada imagen en la fila que le corresponda
							$iLine = 0;
							foreach ($arrayDynSectionValues as $element)
							{
								$factKey = $element["FactKey"];
								if(isset($arrComments[$factKey]))
								{
									$arrayLinesDyn[$iLine].=',"'.$arrComments[$factKey].'"';
								}
								else
								{
									$arrayLinesDyn[$iLine].=',""';
								}
								$iLine++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strComment = (string) @$arrComments[$intFactKey];
								/*
								if (trim($strComment) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strComment.">";
								}
								*/
								if (trim($strComment) != '')
								{
									$arrayLinesMD[$intFactKey].=',"'."<".$intIndex."=".$strComment.'>"';
									$arrayFactKeyMDCom[]=$intFactKey;
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un comentario
							$strValue = (string) @$arrComments[0];
						}
					}
					
					if($anInstance->Questions[$i]->SectionType==sectNormal)
					{
						$line.=',"'.getQuotedStringForExport($strValue).'"';
						
	                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
	                	{
	                		$arrayLinesDyn[$idxDyn].=',"'.getQuotedStringForExport($strValue).'"';
	                	}
	                	
	                	foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		$arrayLinesMD[$idxMD].=',"'.getQuotedStringForExport($strValue).'"';
	                	}
					}
					
					if($anInstance->Questions[$i]->SectionType==sectDynamic)
					{
	                	$line.=',""';
	                	
	                	if(count($arrComments)==0)
	                	{
		                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
		                	{
		                		$arrayLinesDyn[$idxDyn].=',""';
		                	}
	                	}

						foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		$arrayLinesMD[$idxMD].=',""';
	                	}
					}
					
					if($anInstance->Questions[$i]->SectionType==sectMasterDet)
					{
	                	$line.=',""';
	                	
	                	foreach ($arrayLinesDyn as $idxDyn => $aLineDyn)
	                	{
	                		$arrayLinesDyn[$idxDyn].=',""';
	                	}
	                	
						foreach ($arrayLinesMD as $idxMD => $aLineMD)
	                	{
	                		if(!in_array($idxMD,$arrayFactKeyMDCom))
	                		{
	                			$arrayLinesMD[$idxMD].=',""';
	                		}
	                	}
					}
				}
				
				//@JAPR 2013-03-08: Agrega el Score al reporte (#28772)
				if ($objQuestion->HasScores) {
					//El array de scores viene ordenado por factKey dentro de la pregunta, esto es los scores por registros o página
					//dinámica según corresponda, si pertenece a una sección estándar entonces siempre trae sólo un elemento
					$arrScores = @$arrAllScores[$factKeyDimVal][$objQuestion->QuestionID];
					if (is_null($arrScores) || !is_array($arrScores)) {
						$arrScores = array();
					}
					
					$strValue = '';
					if (count($arrScores) > 0) {
						if($objQuestion->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true) {
							//En secciones dinámicas se antepone el descriptor de cada página dinámica
							$intIndex = 0;
							$arrProcessedPages = array();
							foreach ($arrScores as $intScoreFactKey => $strScore) {
								//@JAPR 2013-03-11: Corregido un bug, se estaba calculando un registro que no pertenecía a esta sección y lo
								//agregaba como parte de las respuestas (#28772)
								$blnFound = false;
								$strDynamicPageDSC = '';
								foreach ($arrayDynSectionValues as $element) {
									$intFactKey = (int) @$element['FactKey'];
									if ($intFactKey == $intScoreFactKey) {
										//Si encuentra el FactKey buscado simplemente sale pues ya se identificó el descriptor
										$blnFound = true;
										$strDynamicPageDSC = (string) @$element['parentdesc'];
										break;
									}
								}
								
								//Sólo se agregan scores por página así que se descartan los registros de otros CheckBoxes
								if ($blnFound && !isset($arrProcessedPages[$strDynamicPageDSC]) && trim($strScore) != '') {
									if ($strValue != '') {
										$strValue .= "; ";
									}
									
									$strValue .= $strDynamicPageDSC."=".$strScore;
									$arrProcessedPages[$strDynamicPageDSC] = $intIndex;
								}
								
								$intIndex++;
							}
						}
						elseif (isset($arrMasterDetSectionIds[$objQuestion->SectionID])) {
							//En secciones maestro-detalle simplemente se antepone el número de registro
							$arrMasterDetSectionValues = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (is_null($arrMasterDetSectionValues) || !is_array($arrMasterDetSectionValues)) {
								$arrMasterDetSectionValues = array();
							}
							
							$intIndex = 1;
							foreach ($arrMasterDetSectionValues as $element) {
								$intFactKey = (int) @$element['FactKey'];
								$strScore = (string) @$arrScores[$intFactKey];
								if (trim($strScore) != '') {
									if($strValue!="")
									{
										$strValue.="; ";
									}
									
									$strValue .= "<".$intIndex."=".$strScore.">";
								}
								$intIndex++;
							}
						}
						else {
							//En secciones estándar sólo debe haber un score
							$strValue = (string) @$arrScores[0];
						}
					}
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					
					if($anInstance->Questions[$i]->SectionType==sectNormal)
					{
						foreach ($arrayLinesDyn as $idxDyn=>$aLineDyn)
						{
							//$arrayLinesDyn[$idxDyn].=',"'.getQuotedStringForExport($strValue).'"';
							$arrayLinesDyn[$idxDyn].=',""';
							
						}
						
						foreach ($arrayLinesMD as $idxMD=>$aLineMD)
						{
							//$arrayLinesMD[$idxMD].=',"'.getQuotedStringForExport($strValue).'"';
							$arrayLinesMD[$idxMD].=',""';
						}
					}
					
					/*
					$strValue = '';
					$line.=',"'.getQuotedStringForExport($strValue).'"';
					*/
				}
				
				//@JAPR
			}
			
			$arrayCells =explode(",", $line);
			$newLine="";
			foreach ($arrayCells as $aCell)
			{
				if($aCell!="" && isset($aCell[1]) && ($aCell[1]=='-' || $aCell[1]=='+') && !is_numeric(str_replace('"', '', $aCell)))
				{
					$aCell[0]="";
					$aCell = '"\''.$aCell;
				}
				
				if($newLine!="")
				{
					$newLine.=",";
				}
				
				$newLine.=$aCell;
			}
			//Debe usar un caracter especial para las "," porque si no harán que se vea mal el texto (preguntas GPS)
			$line = $newLine;
			array_push($afile, $line);
			//Julio2013: Agregado para que las secciones dinámicas y maestro detalle se exporten en varias
			//líneas en el excel
			foreach ($arrayLinesDyn as $aLine)
			{
				$arrayCells =explode(",", $aLine);
				$newLine="";
				foreach ($arrayCells as $aCell)
				{
					if($aCell!="" && isset($aCell[1]) && ($aCell[1]=='-' || $aCell[1]=='+') && !is_numeric(str_replace('"', '', $aCell)))
					{
						$aCell[0]="";
						$aCell = '"\''.$aCell;
					}

					if($newLine!="")
					{
						$newLine.=",";
					}
					
					$newLine.=$aCell;
				}
				$aLine = $newLine;
				
				array_push($afile, $aLine);
			}
			
			foreach ($arrayLinesMD as $aLine)
			{
				$arrayCells =explode(",", $aLine);
				$newLine="";
				foreach ($arrayCells as $aCell)
				{
					if($aCell!="" && isset($aCell[1]) && ($aCell[1]=='-' || $aCell[1]=='+') && !is_numeric(str_replace('"', '', $aCell)))
					{
						$aCell[0]="";
						$aCell = '"\''.$aCell;
					}
					
					if($newLine!="")
					{
						$newLine.=",";
					}
					
					$newLine.=$aCell;
				}
				$aLine = $newLine;
				
				array_push($afile, $aLine);
			}

			$aRS->MoveNext();
		}

		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}

		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;

		$fStats = fopen($spathfile, 'w+');

		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}

		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
			exit();
		}
	}
	
	static function NewInstanceToExport($aRepository, $aSurveyID, $attribSelected, $startDate="", $endDate="", $userID=-2)
	{
        
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, true);
		
		$aSurvey=BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		//Generamos el arreglo que utilizaremos para nuestro archivo a exportar
	    $sfile = 'Report_Survey_'.$aSurveyID.'-'.session_id().'-DTE'.date('YmdHis').'.csv';
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath();
		$spathfile = $strLogPath.$sfile;
		//@JAPR
		$afile = array();
		
		//Insertamos el encabezado del array
		$line = '"User Name","Date"';
		
		//Y recorremos todos los QuestionText de la coleccion de preguntas
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
            
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
				//@JAPR 2015-07-26: Removido el shortName automático, ahora será totalmente capturado por el usuario si es que lo quiere utilizar
				$strText = $anInstance->Questions[$i]->getAttributeName();
				
                $line.=',"'.str_replace('"', '""', $strText).'"';
            }
		}
		
		array_push($afile, $line);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($anInstance->Questions[$i]->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
            if ($blnValidQuestion) {
				//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
				if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
                {
					//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
					switch ($anInstance->Questions[$i]->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
						case qtpSignature:
						case qtpShowValue:
						case qtpMessage:
						case qtpSkipSection:
						//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
						//OMMC 2015-10-12: Agregadas las preguntas OCR
						case qtpOCR:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							break;
							
						default:
    	                	$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
    	                	break;
					}
                }
                else
                {
					//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
					if ($aSurvey->DissociateCatDimens) {
						//Si se vuelve a repetir el catalogo, con este arreglo
						//se evita que se vuelva a dar de alta en el FROM dicha tabla
						if(!isset($arrayTables[$aSurvey->FactTable]))
						{
							$fromCat.=", ".$aSurvey->FactTable." B";
							$joinCat.= $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
							$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
							$strAnd = " AND ";
						}
						
						//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
						$catParentID = $objQuestion->CatIndDimID;
						$memberParentID = $catParentID;
						$catTable = "RIDIM_".$catParentID;
						$attribField = "DSC_".$memberParentID;
						$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
						$sql.=", ".$catTable.".".$attribField;
						$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
						
						//Si se vuelve a repetir el catalogo, con este arreglo
						//se evita que se vuelva a dar de alta en el FROM dicha tabla
						if(!isset($arrayTables[$catTable]))
						{
							$fromCat.=", ".$catTable;
							$joinField = "RIDIM_".$catParentID."KEY";
							$joinCat.=" AND B.".$joinField." = ".$catTable.".".$joinField;
							$arrayTables[$catTable] = $catTable;
							$strAnd = " AND ";
						}
					}
					else {
	                    $catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
	                    $catTable = "RIDIM_".$catParentID;
	                    $memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
	                    $attribField = "DSC_".$memberParentID;
						
	                    $sql.=", ".$catTable.".".$attribField;
						
	                    //Si se vuelve a repetir el catalogo, con este arreglo
	                    //se evita que se vuelva a dar de alta en el FROM dicha tabla
	                    if(!isset($arrayTables[$catTable]))
	                    {
	                        $fromCat.=", ".$catTable;
	                    }
	
	                    $arrayTables[$catTable] = $catTable;
	
	                    if($joinCat!="")
	                    {
	                        $joinCat.=" AND ";
	                    }
	                    $joinField = "RIDIM_".$catParentID."KEY";
	                    $joinCat.=$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
	                    $strAnd = " AND ";
					}
					//@JAPR
                }
            }
		}
		
		$where = $joinCat;
		//Filtro de combos de atributos (si es que existen)
		if($attribSelected!=null)
		{
			$anInstance->AttribSelected=$attribSelected;
			
			$isFiltered = false;
			foreach($anInstance->AttribSelected as $idx => $value)
			{
				if($value!="sv_ALL_sv")
				{
					$isFiltered = true;
					break;
				}
			}
			
			if($isFiltered)
			{
				//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$aSurvey->FactTable]))
					{
						$fromCat = ", ".$aSurvey->FactTable." B ";
						$where = $strAnd.$aSurvey->SurveyTable.".FactKey = B.FactKey";
						$arrayTables[$aSurvey->FactTable] = $aSurvey->FactTable;
						$strAnd = " AND ";
					}
					
					//En caso de haber un filtro de atributos, se utilizarán las dimensiones independientes y no el key subrrogado del catálogo
					foreach ($anInstance->FilterAttributes as $memberID=>$memberInfo)
					{
						if($anInstance->AttribSelected[$memberID]!="sv_ALL_sv")
						{
							$catParentID = $memberInfo["IndDimID"];
							$catTable = "RIDIM_".$catParentID;
							$memberParentID = $catParentID;
							$attribField = "DSC_".$memberParentID;
							
							//Si se vuelve a repetir el catalogo, con este arreglo
							//se evita que se vuelva a dar de alta en el FROM dicha tabla
							if(!isset($arrayTables[$catTable]))
							{
								$fromCat.=", ".$catTable;
								$joinField = "RIDIM_".$catParentID."KEY";
								$where .= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
								$arrayTables[$catTable] = $catTable;
								$strAnd = " AND ";
							}
							
							$where .= $strAnd.$catTable.".".$attribField." = ".$anInstance->Repository->DataADOConnection->Quote($anInstance->AttribSelected[$memberID]);
							$strAnd = " AND ";
						}
					}
				}
				else {
					$key = $anInstance->getCatalogKey();
					
					if($where!="")
					{
						$where.=" AND ";
					}
					
					if($key!==null)
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (".implode(',',$key).")";
					}
					else
					{
						$where .= " ".$aSurvey->SurveyTable.".dim_".$anInstance->FirstCatQuestion->QuestionID." IN (-1)";
					}
				}
				//@JAPR
			}
		}
		
		if(trim($startDate)!="" && substr($startDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID >= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($startDate);
		}

		if(trim($endDate)!="" && substr($endDate,0,10)!="0000-00-00")
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".DateID <= ".$anInstance->Repository->DataADOConnection->DBTimeStamp($endDate);
		}
		
		if($userID!=-2)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			$where.=" ".$aSurvey->SurveyTable.".UserID = ".$userID;
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat;
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$arrayDynSectionValues = array();
			$factKeyDimVal = (int)$aRS->fields["factkeydimval"];
			
			//Si la encuesta tiene seccion dinamica entonces procedemos a obtener los 
			//valores de la seccion dinamica
			if($anInstance->HasDynamicSection==true)
			{
				//Realizamos una llamada a la funcion que devuelve el conjunto de valores de la seccion dinamica
				$arrayDynSectionValues = $anInstance->getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal);
			}
			
			$userID = $aRS->fields["userid"];
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
			$userName = str_replace('"', '""', BITAMeFormsUser::GetUserName($anInstance->Repository, $userID));
			$dateValue = str_replace('"', '""', $aRS->fields["dateid"]);
			
			$line='"'.$userName.'","'.$dateValue.'"';
			
			for($i=0; $i<$anInstance->NumQuestionFields; $i++)
			{
				
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				//Cambiado a un switch para mayor facilidad
				$blnValidQuestion = true;
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValidQuestion = false;
						break;
				}
				
				//Esta condición a diferencia de las otras similares, aplica cuando la pregunta si es válida
                if ($blnValidQuestion) {
                    //El primer IF es para identificar si las preguntas son dinamicas y adicionalmente que la propiedad si lo indique
                    //que tengan seccion dinamica
                    if($anInstance->Questions[$i]->SectionID == $anInstance->DynamicSectionID && $anInstance->HasDynamicSection==true)
                    {
                        $fieldName =$anInstance->QuestionFields[$i];
                        $strValue = "";
                        //Obtenemos el valor de esta pregunta a partir del arrreglo $arrayDynSectionValues
                        //Si la pregunta es diferente de Multiple Choice le daremos tratamiento diferente
                        //porque todavia no se sabe exactamente como se va a comportar
                        if($anInstance->Questions[$i]->QTypeID!=qtpMulti)
                        {
                            foreach ($arrayDynSectionValues as $element)
                            {
                                $idxName = $anInstance->QuestionFields[$i];

                                if($strValue!="")
                                {
                                    $strValue.="; ";
                                }
								
                                $strValue.=$element["desc"]."=".$element[$idxName];
                            }
                        }
                        else 
                        {
                            //Si es entrada de tipo Checkbox, el valor seran
                            //todo los valores seleccionados concatenados con ;
                            if($anInstance->Questions[$i]->MCInputType==0)
                            {
                                foreach ($arrayDynSectionValues as $element)
                                {
                                    $idxName = $anInstance->QuestionFields[$i];
                                    $valueInteger = (int)$element[$idxName];
									
                                    if($valueInteger!=0)
                                    {
                                        if($strValue!="")
                                        {
                                            $strValue.=";";
                                        }

                                        $strValue.=$element["desc"];
                                    }
                                }
                            }
                            else 
                            {
                                foreach ($arrayDynSectionValues as $element)
                                {
                                    $idxName = $anInstance->QuestionFields[$i];
									
                                    if($strValue!="")
                                    {
                                        $strValue.="; ";
                                    }
									
                                    $strValue.=$element["desc"]."=".$element[$idxName];
                                }
                            }
                        }
						
                        $line.=',"'.str_replace('"', '""', $strValue).'"';
                    }
                    else 
                    {
						//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
						if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
                        {
                            $lowerFieldName = strtolower($anInstance->QuestionFields[$i]);
							
                            if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
                            {
                                //$line.=','.$aRS->fields[$lowerFieldName];
                                $validateValue = (float) $aRS->fields[$lowerFieldName];
                                $line.=','.$validateValue;
                            }
                            else
                            {
                                if($anInstance->FieldTypes[$i]==qtpMulti)
                                {
                                    //$line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
                                    $newValidateValue = '';
                                    $validateValue = $aRS->fields[$lowerFieldName];
									
                                    if(is_null($validateValue) || $validateValue=="")
                                    {
                                        $newValidateValue = "NA";
                                    }
                                    else
                                    {
                                        $arrayTemporal = explode(';', $validateValue);
										
                                        foreach($arrayTemporal as $elementTemp)
                                        {
                                            if($elementTemp=='')
                                            {
                                                $elementTemp = 'NA';
                                            }
											
                                            if($newValidateValue!='')
                                            {
                                                $newValidateValue.=';';
                                            }
											
                                            $newValidateValue.=$elementTemp;
                                        }
                                    }
                                    $line.=',"'.str_replace('"', '""', $newValidateValue).'"';
                                }
                                else
                                {
                                    $validateValue = $aRS->fields[$lowerFieldName];
									
                                    if(is_null($validateValue) || $validateValue=="")
                                    {
                                        $validateValue = "NA";
                                    }
                                    $line.=',"'.str_replace('"', '""', $validateValue).'"';
                                }
                            }
                        }
                        else
                        {
							//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
							if ($aSurvey->DissociateCatDimens) {
								$memberParentID = $anInstance->Questions[$i]->CatIndDimID;
							}
							else {
                            	$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
							}
							//@JAPR
							
                            $attribField = "DSC_".$memberParentID;
                            $lowerFieldName = strtolower($attribField);
                            if($anInstance->FieldTypes[$i]==qtpOpenNumeric)
                            {
                                $line.=','.$aRS->fields[$lowerFieldName];
                            }
                            else
                            {
                                $line.=',"'.str_replace('"', '""', $aRS->fields[$lowerFieldName]).'"';
                            }
                        }
                    }
                }
			}
			
			array_push($afile, $line);

			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		//Salto de linea
		$LN = chr(13).chr(10);
		$blnWriteFile = true;
		
		$fStats = fopen($spathfile, 'w+');
		
		if ($fStats)
		{
			//fwrite($fStats, implode($LN, $afile));
			if(!fwrite($fStats, implode($LN, $afile)))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
				//@JAPR
			}

			fclose($fStats);
		}
		else
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportReports.txt');
			//@JAPR
		}
		
		if($blnWriteFile)
		{
			if(!is_file($spathfile))
			{
				echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
				exit();
			}
			else
			{
				/*
				header('Content-Type: text/csv');
				header('Content-Disposition: attachment;filename="'.$sfile.'"');
				header('Content-Length: '.filesize($spathfile));
				header('Cache-Control: max-age=0');
				readfile($spathfile);
				exit();
				*/
				//fix for IE catching or PHP bug issue
				header("Pragma: public");
				header("Expires: 0"); // set expiration time
				header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
				header("Content-Type: application/force-download");
				header("Content-Type: application/octet-stream");
				header("Content-Type: application/download");
				header("Content-Disposition: attachment; filename=\"".$sfile."\";");
				header("Content-Transfer-Encoding: binary");
				header("Content-Length: ".filesize($spathfile));
				readfile($spathfile);
				exit();
			}
		}
		else
		{
			echo ("(".__METHOD__.") "."Error file path:{$spathfile}");
			exit();
		}
	}
	
	static function NewInstanceForRemove($aRepository, $aSurveyID, $anArrayOfReportIDs=null, $startDate="", $endDate="", $userID=-2, $ipp=100)
	{
		$anInstance = new BITAMReportCollection($aRepository, $aSurveyID, false);
		
		//Si es igual a cero el SurveyID no deberia hacer algo
		if($aSurveyID==0)
		{
			return $anInstance;
		}
		
		$aSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		$whereFilter = "";
		if (!is_null($anArrayOfReportIDs))
		{
			switch (count($anArrayOfReportIDs))
			{
				case 0:
					break;
				case 1:
					$whereFilter = " AND B.FactKey = ".((int)$anArrayOfReportIDs[0]);
					break;
				default:
					foreach ($anArrayOfReportIDs as $aReportID)
					{
						if ($whereFilter != "")
						{
							$whereFilter .= ", ";
						}
						
						$whereFilter .= (int)$aReportID; 
					}
					
					if ($whereFilter != "")
					{
						$whereFilter = " AND B.FactKey IN (".$whereFilter.")";
					}
					break;
			}
		}
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		//Obtener los datos de la dimension Email
		$fieldEmailKey = "RIDIM_".$aSurvey->EmailDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->EmailDimID;
		
		$sql = "SELECT ".$aSurvey->SurveyTable.".FolioID, ".$aSurvey->SurveyTable.".FactKey, ".$aSurvey->SurveyTable.".UserID, ".$aSurvey->SurveyTable.".DateID, ".$aSurvey->SurveyTable.".StartTime, ".$aSurvey->SurveyTable.".EndTime, C.".$fieldEmailKey." AS EmailKey, C.".$fieldEmailDesc." AS EmailDesc, ".$aSurvey->SurveyTable.".FactKeyDimVal ";
		
		//Arreglo para llevar el control de las tablas catalogos que se estan imprimiendo en la sentencia SQL
		$arrayTables = array();
		$fromCat = "";
		$joinCat = "";
		$strAnd = "";
		for($i=0; $i<$anInstance->NumQuestionFields; $i++)
		{
			$objQuestion = $anInstance->Questions[$i];
			//@JAPR 2012-06-04: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
			if($anInstance->Questions[$i]->UseCatalog==false || $anInstance->Questions[$i]->QTypeID == qtpOpenNumeric)
			{
				//@JAPR 2012-11-22: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				switch ($anInstance->Questions[$i]->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						break;
						
					default:
						$sql.=", ".$aSurvey->SurveyTable.".".$anInstance->QuestionFields[$i];
						break;
				}
			}
			else
			{
				//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
				if ($aSurvey->DissociateCatDimens) {
					//Si se usan dimensiones independientes entonces ya no hay necesidad de consultar el catálogo directamente
					$catParentID = $objQuestion->CatIndDimID;
					$memberParentID = $catParentID;
					$catTable = "RIDIM_".$catParentID;
					$attribField = "DSC_".$memberParentID;
					$sql.=", ".$catTable."."."RIDIM_".$catParentID."KEY";
					$sql.=", ".$catTable.".".$attribField;
					$sql.=", ".$aSurvey->SurveyTable.".".$objQuestion->SurveyCatField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
						$joinField = "RIDIM_".$catParentID."KEY";
						$joinCat.= $strAnd."B.".$joinField." = ".$catTable.".".$joinField;
						$arrayTables[$catTable] = $catTable;
						$strAnd = " AND ";
					}
				}
				else {
					$catParentID = BITAMReportCollection::getCatParentID($aRepository, $anInstance->Questions[$i]->CatalogID);
					$catTable = "RIDIM_".$catParentID;
					$memberParentID = BITAMReportCollection::getParentIDFromByCatMember($aRepository, $anInstance->Questions[$i]->CatalogID,$anInstance->Questions[$i]->CatMemberID);
					$attribField = "DSC_".$memberParentID;
					
					$sql.=", ".$catTable.".".$attribField;
					
					//Si se vuelve a repetir el catalogo, con este arreglo
					//se evita que se vuelva a dar de alta en el FROM dicha tabla
					if(!isset($arrayTables[$catTable]))
					{
						$fromCat.=", ".$catTable;
					}
					
					$arrayTables[$catTable] = $catTable;
					
					if($joinCat!="")
					{
						$joinCat.=" AND ";
					}
					
					$joinField = "RIDIM_".$catParentID."KEY";
					$joinCat.=$aSurvey->SurveyTable.".".$anInstance->Questions[$i]->SurveyField." = ".$catTable.".".$joinField;
				}
				//@JAPR
			}
		}
		
		$sql .= " FROM ".$aSurvey->SurveyTable.$fromCat.", RIFACT_".$aSurvey->ModelID." B, RIDIM_".$aSurvey->EmailDimID." C";
		$where = $joinCat;
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"").$aSurvey->SurveyTable.".FactKey = B.FactKey AND B.".$fieldEmailKey." = C.".$fieldEmailKey;
		
		$sql .= " WHERE ".$where.$whereFilter;
		$sql .= " GROUP BY ".$aSurvey->SurveyTable.".FactKeyDimVal";
		$sql .= " ORDER BY ".$aSurvey->SurveyTable.".DateID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMReport::NewInstanceFromRS($anInstance->Repository, $aSurveyID, $aRS);
			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}
		
		return $anInstance;
	}
	
	function getStrAnswersSCMatrix($questionInstance, $factkeydimval, $factkey, $separator="; ")
	{
		$strAnswersSCMatrix = "";
		$LN = $separator;
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT A.DisplayText, B.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$factkey." AND B.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strDisplayText." = ".$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	function getOnlyAnswersSCMatrix($questionInstance, $factkeydimval, $factkey, $separator=";")
	{
		$strAnswersSCMatrix = "";
		$LN = $separator;
		
		$tableMatrixData = "SVSurveyMatrixData_".$this->Survey->ModelID;
		
		if($questionInstance->OneChoicePer==0)
		{
			$tableName = "SI_SV_QAnswers";
		}
		else 
		{
			$tableName = "SI_SV_QAnswersCol";
		}

		$sql = "SELECT B.dim_value FROM ".$tableName." A LEFT JOIN ".$tableMatrixData." B 
				ON A.QuestionID = B.QuestionID AND A.ConsecutiveID = B.ConsecutiveID 
				AND A.QuestionID = ".$questionInstance->QuestionID." 
				WHERE B.FactKey = ".$factkey." AND B.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableMatrixData." ".translate("tables").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strAnswerText = $aRS->fields["dim_value"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strAnswersSCMatrix!="")
			{
				$strAnswersSCMatrix.=$LN;
			}
			
			$strAnswersSCMatrix.=$strAnswerText;

			$aRS->MoveNext();
		}
		
		return $strAnswersSCMatrix;
	}
	
	static function getStrCatDimValAnswers($aRepository, $surveyInstance, $questionInstance, $factkeydimval, $separator="; ", $onlyAnswer=false)
	{
		$strCatDimValAnswers = "";
		$LN = $separator;
		
		$tableCatDimVal = "SVSurveyCatDimVal_".$surveyInstance->ModelID;
		$tableDimension = "RIDIM_".$questionInstance->ElementDimID;
		$tableName = "SI_SV_QAnswers";

		$sql = "SELECT A.DisplayText, C.ResponseValue FROM (".$tableName." A INNER JOIN ".$tableDimension." B 
				ON A.QuestionID = ".$questionInstance->QuestionID." AND A.DisplayText = B.DSC_".$questionInstance->ElementDimID.") 
				LEFT JOIN ".$tableCatDimVal." C ON B.RIDIM_".$questionInstance->ElementDimID."KEY = C.ElemDimVal 
				AND A.QuestionID = C.QuestionID AND C.FactKeyDimVal = ".$factkeydimval." 
				ORDER BY A.SortOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$tableDimension.", ".$tableCatDimVal." ".translate("tables").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$strDisplayText = $aRS->fields["displaytext"];
			$strAnswerText = $aRS->fields["responsevalue"];
			
			if(trim($strAnswerText)=="")
			{
				$strAnswerText = "NA";
			}
			
			if($strCatDimValAnswers!="")
			{
				$strCatDimValAnswers.=$LN;
			}
			
			if(!$onlyAnswer)
			{
				$strCatDimValAnswers.=$strDisplayText." = ".$strAnswerText;
			}
			else 
			{
				$strCatDimValAnswers.=$strAnswerText;
			}

			$aRS->MoveNext();
		}
		
		return $strCatDimValAnswers;
	}
	
	//@JAPR 2013-03-06: Agregado el soporte para secciones Maestro-detalle (#27957)
	//@JAPRWarning: Descontinuada
	/* Si se requería usar esta función, mejor se debería cargar una instancia de record pues dicha instancia además de contener ya el propio
	resultado de esta función, contendría todas las nuevas funcionalidades que se agregan y evitaría tener que generar funciones como esta para
	cargar por separado cada una de ellas (por ejemplo las propias secciones maestro-detalle)
	*/
	function getDynamicSectionValuesByFactKeyDimVal($factKeyDimVal)
	{
		//Obtenemos coleccion de las preguntas de la seccion dinamica
		$dynQuestionCollection = $this->DynQuestionCollection;
		//Obtenemos la instancia de la seccion dinamica
		$dynSectionInstance = $this->DynamicSection;
		//Obtenemos la instancia de Survey		
		$aSurvey = $this->Survey;
		//Obtenemos la instancia del catalogo
		$catalogInstance = $this->Catalog;
		
		//Obtener la primera pregunta de seleccion simple de catalogo para obtener los valores del hijo del atributo 
		//de la seccion dinamica
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." 
				AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$dynSectionInstance->CatalogID." 
				ORDER BY QuestionNumber";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
		if (!$aRS)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		elseif ($aRS->EOF) {
			//@JAPR 2013-03-01: Corregida la descripción de error ya que no describía el problema real
			die("(".__METHOD__.") ".translate("There is an error in the form, a catalog simple choice question is needed to generate the dynamic section"));
		}
		
		$questionID = (int)$aRS->fields["questionid"];
		$factTable = "";
		$factTableJoin = "";
		if ($aSurvey->DissociateCatDimens) {
			$intCatMemberID = $dynSectionInstance->CatMemberID;
			if ($dynSectionInstance->ChildCatMemberID > 0) {
				$intCatMemberID = $dynSectionInstance->ChildCatMemberID;
			}
			
			$catMemberInstance = BITAMCatalogMember::NewInstanceWithID($this->Repository, $intCatMemberID);
			$factTable = $aSurvey->FactTable." C, ";
			$factTableJoin = "A.FactKey = C.FactKey AND ";
			$dimCatFieldID = "C.RIDIM_".$catMemberInstance->IndDimID."KEY";
			$dimCatFieldDsc = "B.DSC_".$catMemberInstance->IndDimID;
			$dimCatFieldJoin = "RIDIM_".$catMemberInstance->IndDimID."KEY";
			$dimCatTable = "RIDIM_".$catMemberInstance->IndDimID;
		}
		else {
			$dimCatFieldID = "A.dim_".$questionID;
			$dimCatFieldDsc = "B.DSC_".$this->ChildDynamicAttribID;
			$dimCatFieldJoin = "RIDIM_".$catalogInstance->ParentID."KEY";
			$dimCatTable = "RIDIM_".$catalogInstance->ParentID;
		}
		
		$sql = "SELECT ".$dimCatFieldID." AS id, ".$dimCatFieldDsc." AS description ";
		foreach($dynQuestionCollection as $aQuestion)
		{
			//@JAPR 2013-01-07: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
			switch ($aQuestion->QTypeID) {
				case qtpShowValue:
				case qtpPhoto:
				case qtpDocument:
				case qtpAudio:
				case qtpVideo:
				case qtpSignature:
				case qtpMessage:
				case qtpSkipSection:
				//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
				case qtpSync:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
				case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
				case qtpSection:
				//OMMC 2015-10-12: Agregadas las preguntas OCR
				case qtpOCR:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					break;
				
				default:
					$sql .= ", dim_".$aQuestion->QuestionID;
					break;
			}
			//@JAPR
		}
		
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseDynamicPageField) {
			$sql .= ", A.DynamicPageDSC";
		}
		//@JAPR
		
		$sql .= " FROM ".$aSurvey->SurveyTable." A, ".$factTable.$dimCatTable." B ";
		$sql .= " WHERE ".$factTableJoin.$dimCatFieldID." = B.".$dimCatFieldJoin." AND A.FactKeyDimVal = ".$factKeyDimVal;
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		if ($this->Survey->UseStdSectionSingleRec) {
			$sql .= " AND A.EntrySectionID = ".$dynSectionInstance->SectionID;
		}
		//@JAPR
		$sql .= " ORDER BY A.FactKey ";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
		//No debe marcar error si no hay datos, por lo menos no a partir de la versión donde existen registros únicos
		if (!$aRS || ($aRS->EOF && !$this->Survey->UseStdSectionSingleRec))
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$aSurvey->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$count = 0;
		$arrayDynSectionValues = array();
		while(!$aRS->EOF)
		{
			$arrayDynSectionValues[$count] = array();
			$arrayDynSectionValues[$count]["id"] = (int)$aRS->fields["id"];
			$arrayDynSectionValues[$count]["desc"] = $aRS->fields["description"];
			
			foreach($dynQuestionCollection as $aQuestion)
			{
				//@JAPR 2013-01-07: Corregido un bug, hay varios tipos de pregunta que no deben tener campo en la paralela
				$blnValid = true;
				switch ($aQuestion->QTypeID) {
					case qtpShowValue:
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpMessage:
					case qtpSkipSection:
					//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//OMMC 2015-10-12: Agregadas las preguntas OCR
					case qtpOCR:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValid = false;
						break;
				}
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				$fieldName = "dim_".$aQuestion->QuestionID;
				
				if($aQuestion->QTypeID!=qtpMulti)
				{
					$arrayDynSectionValues[$count][$fieldName] = $aRS->fields[$fieldName];
				}
				else 
				{
					//Si es de tipo Multiple Choice Numerica le realizamos un float
					if($aQuestion->MCInputType==1)
					{
						$arrayDynSectionValues[$count][$fieldName] = (float)$aRS->fields[$fieldName];
					}
					else 
					{
						$arrayDynSectionValues[$count][$fieldName] = $aRS->fields[$fieldName];
					}
				}
			}

			//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
			//Si se usa el registro único, entonces agrega el nuevo campo para indicar que si este es el registro a usar por cada página pues
			//sería el que contiene a las preguntas no repetibles
			$aDynamicPageDSC = (string) @$aRS->fields["dynamicpagedsc"];
			if ($this->Survey->UseDynamicPageField) {
				//@JAPR 2013-03-06: Corregido un bug, se estaba usando una propiedad de la instancia pero en esta función es un array local
				$arrayDynSectionValues[$count]["DynamicPageDSC"] = $aDynamicPageDSC;
			}
			//@JAPR
			
			$count++;
			$aRS->MoveNext();
		}
		
		return $arrayDynSectionValues;
	}
	
	function getCatalogKey()
	{
		$key=null;

		$fieldKey="RIDIM_".$this->CatParentID."KEY";

		$sql= "SELECT ".$fieldKey. " FROM ".$this->CatTableName;

		$where="";

		foreach ($this->FilterAttributes as $memberID=>$memberInfo)
		{
			if($this->AttribSelected[$memberID]!="sv_ALL_sv")
			{
				if($where!="")
				{
					$where.= " AND ";
				}
				$where.= " DSC_".$memberInfo["ParentID"]." = ".$this->Repository->DataADOConnection->Quote($this->AttribSelected[$memberID]);
			}
		}

		if($where!="")
		{
			$where=" WHERE ".$where;
		}

		$sql.= $where;

		$aRS = $this->Repository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->CatTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$key=array();

			while (!$aRS->EOF)
			{
				$key[] = $aRS->fields[strtolower($fieldKey)];
				$aRS->MoveNext();
			}
		}

		return $key;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $TypeID;
		
		$TypeID = getParamValue('TypeID', 'both', '(int)');
		$aSurveyID = "";
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$aSurveyID = $aHTTPRequest->GET["SurveyID"];
		}
		elseif (array_key_exists("SurveyID", $aHTTPRequest->POST)) {
			$aSurveyID = $aHTTPRequest->POST["SurveyID"];
		}
		
		// --------------------------------------------------------------------------------------
		$searchString = "";
		if (array_key_exists("searchString", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["searchString"] != "")
				$searchString = @$aHTTPRequest->POST["searchString"];
		}
		
		$startDate = "";
		if (array_key_exists("startDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["startDate"])!="" && substr($aHTTPRequest->POST["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->POST["startDate"]);
			}
		}
		elseif (array_key_exists("startDate", $aHTTPRequest->GET)) {
			$startDate = trim($aHTTPRequest->GET["startDate"]);
		}
		
		$endDate = "";
		if (array_key_exists("endDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["endDate"])!="" && substr($aHTTPRequest->POST["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->POST["endDate"]);
			}
		}
		elseif (array_key_exists("endDate", $aHTTPRequest->GET)) {
			$endDate = trim($aHTTPRequest->GET["endDate"]);
		}
		
		$UserID = "";
		if (array_key_exists("userID", $aHTTPRequest->GET))
		{
			$UserID = $aHTTPRequest->GET["userID"];
		}
		elseif (array_key_exists("userID", $aHTTPRequest->POST)) {
			$UserID = $aHTTPRequest->POST["userID"];
		}

		//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
		$dateTwoState = "";
		if (array_key_exists("dateTwoState", $aHTTPRequest->GET))
		{
			$dateTwoState = $aHTTPRequest->GET["dateTwoState"];
		}
		elseif (array_key_exists("dateTwoState", $aHTTPRequest->POST)) {
			$dateTwoState = $aHTTPRequest->POST["dateTwoState"];
		}
		switch($dateTwoState)
		{
			case 'T':
				$startDate = date('Y-m-d').' 00:00:00';
				$endDate = date('Y-m-d').' 23:59:59';
				break;
			case 'W':
				$weekDay = intval(date('w'));
				$startDate = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
				$endDate = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
				break;
			case 'M':
				$tDay = date('t');
				$startDate = date('Y-m-').'01 00:00:00';
				$endDate = date('Y-m-').$tDay.' 23:59:59';
				break;
			case 'Y':
				$startDate = date('Y-').'01-01 00:00:00';
				$endDate = date('Y-').'12-31 23:59:59';
				break;
		}
		//GCRUZ 2015-08-27 Filtro de búsqueda de formas
		$searchForm = "";
		if (array_key_exists("searchForm", $aHTTPRequest->GET))
		{
			$searchForm = $aHTTPRequest->GET["searchForm"];
		}
		elseif (array_key_exists("searchForm", $aHTTPRequest->POST)) {
			$searchForm = $aHTTPRequest->POST["searchForm"];
		}
		
		$GroupID = "";
		if (array_key_exists("GroupID", $aHTTPRequest->POST)) {
			$GroupID = $aHTTPRequest->POST["GroupID"];
		}
		elseif (array_key_exists("GroupID", $aHTTPRequest->GET)) {
			$GroupID = $aHTTPRequest->GET["GroupID"];
		}
		
		$ipp=100;
		if (array_key_exists("ipp", $aHTTPRequest->POST))
		{
			$ipp = (int)$aHTTPRequest->POST["ipp"];
		}
		
		$FilterDate = "";
		if (array_key_exists("FilterDate", $aHTTPRequest->POST))
		{
			$FilterDate = $aHTTPRequest->POST["FilterDate"];
		}
		
		// --------------------------------------------------------------------------------------
		
		$attribSelected=null;
		$callExportProcess = false;
		$callExportToSPSS = false;
		$actionValue = '';
		
		//Verificamos sino viene de una accion de exportacion
		if (array_key_exists("ReportAction", $aHTTPRequest->GET))
		{
			$actionValue = $aHTTPRequest->GET["ReportAction"];

			if($actionValue=="ExportCVS")
			{
				$callExportProcess = true;
				$ExportReportFormat = $aHTTPRequest->GET["ReportFormat"];
			}
			if($actionValue=="ExportSPSS")
				$callExportToSPSS = true;
		}
		
		$sql = "SELECT UserDimID FROM SI_SV_gblsurveymodel";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_gblsurveymodel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$userDimTable="";
		$userDimID=-1;
		if (!$aRS->EOF)
		{
			$userDimID = (int)$aRS->fields["userdimid"];
			$userDimTable="ridim_".$userDimID;
		}
		

		if(($TypeID!=rptDataDest && /*$TypeID != rptEntries &&*/ $TypeID != rptSyncs) && $GroupID != "" && $UserID == '')
		{
			$sql= "SELECT CLA_USUARIO FROM SI_ROL_USUARIO WHERE CLA_ROL IN (".$GroupID.")";
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}

			//Contiene las claves de los usuarios pertenecientes a los grupos seleccionados
			$groupUsersIDs = array();
			while (!$aRS->EOF)
			{
				$groupUsersIDs[] = (int) $aRS->fields["cla_usuario"];
				$aRS->MoveNext();
			}
			//GCRUZ 2015-09-30 Validar GroupID=0 (Ninguno)
			$groupIDs = explode(',', $GroupID);
			if (array_search('0', $groupIDs) !== false)
			{
				$aSQL = "SELECT GROUP_CONCAT(DISTINCT(CLA_USUARIO)) AS 'CLA_USUARIO' FROM SI_ROL_USUARIO";
				$rsUsersWithGroup = $aRepository->ADOConnection->Execute($aSQL);
				$strUsersWithGroup = '';
				if ($rsUsersWithGroup && $rsUsersWithGroup->_numOfRows != 0)
					$strUsersWithGroup = trim($rsUsersWithGroup->fields['CLA_USUARIO']);
				$strWhereUsersWithGroup = '';
				if ($strUsersWithGroup != '')
					$strWhereUsersWithGroup = ' '.$userDimTable.'KEY NOT IN ('.$strUsersWithGroup.') AND ';
				$aSQL = "SELECT DISTINCT(".$userDimTable."KEY) AS 'UserKey' FROM ".$userDimTable." WHERE ".$strWhereUsersWithGroup." ".$userDimTable."KEY NOT IN (-1)";
				$rsNoGroup = $aRepository->DataADOConnection->Execute($aSQL);
				if ($rsNoGroup && $rsNoGroup->_numOfRows != 0)
				{
					while(!$rsNoGroup->EOF)
					{
						$groupUsersIDs[] = (int) $rsNoGroup->fields['UserKey'];
						$rsNoGroup->MoveNext();
					}
				}
			}
			if (count($groupUsersIDs) > 0)
			{
				$aSQL = "SELECT DSC_".$userDimID." AS 'UserEmail' FROM ".$userDimTable." WHERE ".$userDimTable."KEY IN (". implode(',', $groupUsersIDs).")";
				$rsUsers = $aRepository->DataADOConnection->Execute($aSQL);
				$UserID = '';
				$bFirst = true;
				if ($rsUsers && $rsUsers->_numOfRows != 0)
				{
					while(!$rsUsers->EOF)
					{
						if ($bFirst)
							$bFirst = false;
						else
							$UserID .= ',';
						$UserID .= $rsUsers->fields['UserEmail'];
						$rsUsers->MoveNext();
					}
				}
			}
		}

		
		if($callExportProcess)
		{
			//@JAPR 2012-01-11: Modificado el método de exportación para que cada columna de selección múltiple se divida en tantas columnas como
			//posibles respuestas existan
/*
			$blnIncludePhotos = getParamValue('AddPhotos', 'both', '(int)');
			$blnNewExcelMethod = getParamValue('newExcelMethod', 'both', '(int)');
			return BITAMReportCollection::NewInstanceToExportDesgToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $UserID, $blnIncludePhotos, $blnNewExcelMethod);
*/
			//GCRUZ 2015-07-28. Exportar a CSV
//			var_dump($groupUsersIDs);var_dump($UserID);exit;
			$_REQUEST['userID'] = $UserID;
			//EVEG si no es administrador el usuario en la bitacora solo se le mostraran sus capturas
			if($_SESSION["PABITAM_UserRole"]!=1)
				$_REQUEST['userID'] = @$_SESSION['saas_email'];
			$_REQUEST['ExportReportFormat'] = $ExportReportFormat;
			include('csvExport.php');
		}
		if ($callExportToSPSS)
		{
			$_REQUEST['userID'] = $UserID;
			include('SPSSio.php');
			exit();
		}
		
		if($actionValue=="ZipReport")
		{
			//Falta agregar que sea para multiples formas o combinaciones de filtro
			//$aSurveyIDs = json_decode($aSurveyID, true);
			//@AAL 22/06/2015: Modificado el nombre de la funcion NewInstanceToCompress. El parametro UserID ahora sera un String(el Email)
			return BITAMReportCollection::NewInstanceToCompressToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $UserID, $searchForm);
		}
		elseif($actionValue=="PPTXReport")
		{
			//@AAL 22/06/2015: Modificado el nombre de la funcion NewInstanceToPPTX. El parametro UserID ahora sera un String(el Email)
			return BITAMReportCollection::NewInstanceToPPTXToFormsLog($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $UserID, $searchForm);
		}
		elseif($actionValue=="ZipReportDoc")
		{
			return BITAMReportCollection::NewInstanceToCompressDoc($aRepository, $aSurveyID, $attribSelected, $startDate, $endDate, $UserID, $searchForm);
		}		
		else
		{
			//@AAL 22/06/2015: Modificado, la función NewInstanceToFormsLog mostrará cuatro campos obtenidos de la tabla SI_SV_SurveyLog
			return BITAMReportCollection::NewInstanceToFormsLog($aRepository, $searchString, $startDate, $endDate, $GroupID, $UserID, $aSurveyID, $FilterDate, $ipp);	
		}
	}

	function get_Parent()
	{
		return $this->Repository;
	}

	function get_Title()
	{
		return translate("Forms Log");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_SECTION=ReportCollection&SurveyID=".$this->SurveyID.$strParameters;
	}

	function get_ChildQueryString()
	{
		return "BITAM_SECTION=Report";
	}

	function get_AddRemoveQueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_PAGE=Report".$strParameters;
	}

	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'ReportID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TimeID";
		$aField->Title = translate("Time");
		$aField->ToolTip = translate("Time");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyName";
		$aField->Title = translate("Survey");
		$aField->ToolTip = translate("Survey");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserName";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Map";
		$aField->Title = translate("Map");
		$aField->ToolTip = translate("Map");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ViewPdf";
		$aField->Title = translate("View PDF");
		$aField->ToolTip = translate("View PDF");
		$aField->Type = "String";
		$aField->Size = 255;
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;

		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return true;
	}

	static function getCatTableName($aRepository, $aCatalogID)
	{
		$tableName = "";

		$sql= "SELECT TableName FROM SI_SV_Catalog WHERE CatalogID = ".$aCatalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$tableName = trim($aRS->fields["tablename"]);
		}

		return $tableName;
	}

	static function getCatParentID($aRepository, $aCatalogID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetClaDescripWithCatalogID($aCatalogID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$parentID = "";

		$sql= "SELECT ParentID FROM SI_SV_Catalog WHERE CatalogID = ".$aCatalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$parentID = (int) $aRS->fields["parentid"];
		}
		
		BITAMGlobalFormsInstance::AddClaDescripWithCatalogID($aCatalogID, $parentID);

		return $parentID;
	}

	static function getParentIDFromByCatMember($aRepository, $aCatalogID, $aMemberID)
	{
		$anInstance =& BITAMGlobalFormsInstance::GetClaDescripWithCatMemberID($aCatalogID, $aMemberID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$parentID = "";

		$sql= "SELECT ParentID FROM SI_SV_CatalogMember WHERE CatalogID = ".$aCatalogID. " AND MemberID = ".$aMemberID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{
			$parentID = (int) $aRS->fields["parentid"];
		}
		
		BITAMGlobalFormsInstance::AddClaDescripWithCatMemberID($aCatalogID, $aMemberID, $parentID);

		return $parentID;
	}

	static function getAttribValues($aRepository, $tableName, $fieldName, $filter="")
	{
		$values = array();

		$sql= "SELECT DISTINCT ".$fieldName." FROM ".$tableName.
		" WHERE ".$fieldName." <> ".$aRepository->DataADOConnection->Quote("*NA");

		if($filter!="")
		{
			$sql.=" ".$filter;
		}

		$sql.=" ORDER BY ".$fieldName;

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$value = trim($aRS->fields[strtolower($fieldName)]);
			$values[$value]	= $value;
			$aRS->MoveNext();
		}

		return $values;
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	//La función no estaba definida como static pero siempre se utilizaba de esa manera
	static function getAttributes($aRepository, $aCatalogID, $aCatMemberID)
	{
		$attributes = array();
		
		//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$strAdditionalFields = "";
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', IndDimID';
		}
		//@JAPR
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', KeyOfAgenda';
		}
		$sql= "SELECT MemberID, MemberName, ParentID, MemberOrder $strAdditionalFields FROM SI_SV_CatalogMember WHERE CatalogID=".$aCatalogID.
		" AND MemberOrder <= (SELECT MemberOrder FROM SI_SV_CatalogMember WHERE CatalogID=".$aCatalogID." AND MemberID=". $aCatMemberID.")".
		" ORDER BY MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$memberID = $aRS->fields["memberid"];
			$attributes[$memberID]["MemberID"] = $memberID;
			$attributes[$memberID]["MemberName"] = $aRS->fields["membername"];
			$attributes[$memberID]["ParentID"] = $aRS->fields["parentid"];
			$attributes[$memberID]["MemberOrder"] = $aRS->fields["memberorder"];
			$attributes[$memberID]["FieldName"] = "DSC_".$aRS->fields["parentid"];
			//@JAPR 2013-02-06: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			$attributes[$memberID]["IndDimID"] = (int) @$aRS->fields["inddimid"];
			//@JAPR
			$attributes[$memberID]["KeyOfAgenda"] = (int) @$aRS->fields["keyofagenda"];
			$aRS->MoveNext();
		}

		return $attributes;
	}

	static function getFirtsCatalogQuestion($aRepository, $aSurveyID)
	{
		$questionID=null;
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_QUESTION WHERE UseCatalog=1 AND SurveyID = ". $aSurveyID. " AND QTypeID <> ".qtpOpenNumeric." ORDER BY QuestionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QUESTION ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if (!$aRS->EOF)
		{	//Se considera sólo la primera pregunta encontrada que usa catálogo
			$questionID = (int) $aRS->fields["questionid"];
		}

		return $questionID;
	}

	function generateBeforeFormCode($aUser)
	{
		require_once('settingsvariable.inc.php');
		$strAltEformsService = '';
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ALTEFORMSSERVICE');
		if (!is_null($objSetting) && trim($objSetting->SettingValue != ''))
		{
			$strAltEformsService = $objSetting->SettingValue;
			if (substr($strAltEformsService, -1) != "/") {
				$strAltEformsService .= "/";
			}
			
			if (strtolower(substr($strAltEformsService, 0, 4)) != 'http') {
				$strAltEformsService = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$strAltEformsService;
			}
		}
		
?>		
        <link type="text/css" rel="stylesheet" href="css/eBavel.css"/>
		
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery-ui-1.10.1.custom.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.jsperanto.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamApp.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamstorage.js"></script>
	<script type="text/javascript" src="js/eBavel/databases.js"></script>
	<script type="text/javascript" src="js/eBavel/section.class.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.widgets.js"></script>
	<script type="text/javascript" src="js/eBavel/date.js"></script>
	<script type="text/javascript" src="js/jsondefinition/bitamAppFormsJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jsondefinition/agendasJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jquery.widgets.js"></script>
	<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
 		
        function reportcollectionSendPDF() {
            var selected = 0;
            var toignore = 0;
            var elements = BITAMReportCollection_SelectForm.elements;
            for (var i = 0; i < elements.length; i++)
            {
                if (elements[i].name == "ReportID[]")
                {
                    if (elements[i].checked)
                    {
                        selected++;
                        if (elements[i].id == "0")
                        {
                            toignore++;
                        }
                    }
                }
            }
            if (selected == 0)
            {
            <?
                if(trim($this->MsgNoSelectedSendPDF)!="")
                {
?>
                alert('<?= str_replace("'", "\'", translate($this->MsgNoSelectedSendPDF)) ?>');
<?
                }
                else 
                {
?>
                alert('<?= str_replace("'", "\'", sprintf(translate("Please select which %s to Send PDF"), "BITAM Report Send PDF")) ?>');
<?
                }
            ?>				

                return;
            }
            <?
                if(trim($this->MsgCancelSendPDF)!="")
                {
                    $strAllDelete = $this->MsgCancelSendPDF;
                }
                else 
                {
                    $strAllDelete = translate("No selected %s can be send");
                }			
            ?>
            if (selected == toignore)
            {
                alert('<?= str_replace("'", "\'", sprintf($strAllDelete, "BITAM Report Send PDF")) ?>');
                return;
            }
            <?
                if(trim($this->MsgCancelSendPDF)!="")
                {
                    $strSomeDelete = translate("Some %s can not be send")." ".translate("because")." ".$this->MsgCancelSendPDF.", ".translate("they will be deselected");
                }
                else 
                {
                    $strSomeDelete = translate("Some %s can not be sended, they will be deselected");
                }			
            ?>				
            if (toignore != 0)
            {
                alert('<?=str_replace("'", "\'", sprintf($strSomeDelete, "BITAM Report Send PDF")) ?>');
                var elements = BITAMReportCollection_SelectForm.elements;
                for (var i = 0; i < elements.length; i++)
                {
                    if (elements[i].name == "ReportID[]")
                    {
                        if (elements[i].id == "0")
                        {
                            elements[i].checked = false;
                        }
                    }
                }
            }

            <?
                if(trim($this->MsgConfirmRemove)!="")
                {
?>
            var answer = confirm('<?= str_replace("'", "\'", translate($this->MsgConfirmSendPDF)) ?>');
<?
                }
                else 
                {
?>
            var answer = confirm('<?= str_replace("'", "\'", sprintf(translate("Do you want to Send PDF of all selected %s?"), "BITAM Report Send PDF")) ?>');
<?
                }
            ?>

            if (answer)
            {
                var elements = BITAMReportCollection_SelectForm.elements;
                var elementsSendPDF = new Array();
                var s = 0;
                for (var i = 0; i < elements.length; i++)
                {
                    if (elements[i].name == "ReportID[]")
                    {
                        //elements[i].disabled = false;
                        if (elements[i].checked)
                        {
                            selected++;
                            if (elements[i].id != "0")
                            {
                            	if (elements[i].parentNode && elements[i].parentNode.parentNode && elements[i].parentNode.parentNode.tagName.toLowerCase() == "tr") {
                            		if (elements[i].parentNode.parentNode.getElementsByClassName('collection_value_number').length) {
                            			if (elements[i].parentNode.parentNode.getElementsByClassName('collection_value_number')[0]) {
	                            			var intReportID = parseInt(elements[i].parentNode.parentNode.getElementsByClassName('collection_value_number')[0].id);
			                                elementsSendPDF[s] = intReportID;	//elements[i].value;
			                                s++;
                            			}
                            		}
                            	}
                            }
                        }
                    }
                }
                var strReportIDs = elementsSendPDF.join();
                var inputReportIDs = document.getElementById("ReportSendPDFIDs");
                
                var objUserID = document.getElementById("UserID");
                frmSendPDF.userID.value = objUserID.value;
                
                inputReportIDs.value = strReportIDs;
                frmSendPDF.submit();
                //BITAMReportCollection_SelectForm.action = '<?= 'main.php?'.$this->get_AddRemoveQueryString() ?>';
                //BITAMReportCollection_SelectForm.submit();
            }
        }
        
        function editEntry(entryID)
 		{
 			var surveyID = <?=$this->SurveyID?>;

 			openWindow('<?=$strAltEformsService?>loadEntry.php?surveyID=' + surveyID + '&extp=1&EntryID=' + entryID);
 		}

		function openImg(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyImage.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'picWin', 'width=800, height=600, left=300, top=100, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openComment(surveyID, questionID, folioID, renglon, columna)
		{
			var aWindow = window.open('getSurveyComment.php?surveyID='+surveyID+'&questionID='+questionID+'&folioID='+folioID+'&secuencia_reng='+renglon+'&secuencia_col='+columna, 'commentWin', 'width=400, height=150, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function showSignature(surveyID, reportID)
		{
			var aWindow = window.open('getSurveySignature.php?surveyID='+surveyID+'&reportID='+reportID, 'signatureWin', 'width=500, height=300, left=350, top=350, menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
		}

		function openGoogleMap(surveyID, userID, startDate, endDate, reportID, reportType)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
			var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&userID='+userID+'&startDate='+startDate+'&endDate='+endDate+'&reportID='+reportID, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}

		/*function openGoogleMap(surveyID, startDate, endDate, userID, AttribIDs, AttribVals)
		{
			d = new Date();
			y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
			var aWindow = window.open('gmeSurvey.php?surveyID='+surveyID+'&startDate='+startDate+'&endDate='+endDate+'&userID='+userID+'&AttribIDs='+AttribIDs+'&AttribVals='+AttribVals, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no',1);
		}*/

		function applyFilterSurvey()
		{
			var objSurvey = document.getElementById("SurveyID");
			frmChangeFilter.SurveyID.value=objSurvey.value;
			frmChangeFilter.submit();
		}

		function applyFilterAttrib (filter, oldTags, descrTag, allForms)
		{
			if (filter) {
				
				for (var fkey in filter) {

					//Search for Input String
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}

					//Search for Date
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
					}
					/*<input type="hidden" name="searchString" id="searchString" value="">
					<input type="hidden" name="oldGroupsTags" id="oldGroupsTags" value="">
					<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
					<input type="hidden" name="oldFormsTags" id="oldFormsTags" value="">*/

					//Search for Group of Users
					if (fkey == 'Groups') {
						var filterGroups = filter['Groups'];
						var Groups = new Array();
						for (var ukey in filterGroups) {
							Groups.push(filterGroups[ukey]);
						}
						frmAttribFilter.GroupID.value = Groups.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldGroupsTags.value = oldTags;
					}	
					//Search for Users
					if (fkey == 'Users') {
						var filterUsers = filter['Users'];
						var Users = new Array();
						for (var ukey in filterUsers) {
							Users.push(filterUsers[ukey]);
						}
						frmAttribFilter.userID.value = Users.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldUserTags.value = oldTags;
					}
					//Search for Forms
					if (fkey =='Forms'){
						var filterForms = filter['Forms'];
						var Forms = new Array();
						for (var ukey in filterForms) {
							Forms.push(filterForms[ukey]);
						}
						frmAttribFilter.SurveyID.value = Forms.join(',');
						//frmAttribFilter.oldFormsTags.value = allForms;
						//frmAttribFilter.SurveyID.value = parseInt(filter['Forms'][0], 10);
					}
					frmAttribFilter.oldFormsTags.value = allForms;
				}
			} 
			else {
				/*var objCatalogID = document.getElementById("CatalogID");
				frmAttribFilter.catalogID.value = objCatalogID.value;*/

				var objStartDate = document.getElementById("CaptureStartDate");
				frmAttribFilter.startDate.value = objStartDate.value;

				var objEndDate = document.getElementById("CaptureEndDate");
				frmAttribFilter.endDate.value = objEndDate.value;
				
				var objUserID = document.getElementById("UserID");
				frmAttribFilter.userID.value = objUserID.value;
				
				var objIpp = document.getElementById("ipp");
				frmAttribFilter.ipp.value = objIpp.value;
			}

			frmAttribFilter.submit();
		}
		/*function applyFilterAttrib()
		{
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = objStartDate.value;

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = objEndDate.value;
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = objUserID.value;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = objIpp.value;

			frmAttribFilter.submit();
		}*/


		
		function clearFilter()
		{
<?
			$hasAttribFilter = false;
			//Revisamos todos los filtros de los catalogos si los hay
			if(!is_null($this->AttribSelected))
			{
				foreach ($this->AttribSelected as $idx=>$value)
				{
					if($value!="sv_ALL_sv")
					{
						$hasAttribFilter = true;
						break;
					}
				}
				
			}
			
			if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100 || $hasAttribFilter==true)
			{
				foreach($this->FilterAttributes as $memberID => $memberInfo)
				{
?>
			var obj_<?=$memberID?> = document.getElementById("AttribID_<?=$memberID?>");
			frmAttribFilter.AttribID_<?=$memberID?>.value = "sv_ALL_sv";
<?
				}
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
			else 
			{
?>
<?
			}
?>
		}
		
		var ctrlEditAudit;
		ctrlEditAudit = -1;
		
		function editAudit()
		{
			ctrlEditAudit = ctrlEditAudit*(-1);
			var obDivTitle = document.getElementById("collection_title_div");
			var objBtnBack = document.getElementById("btnBack");
			var objBtnEditLog = document.getElementById("btnEditLog");
			//var objBtnAuditTrail = document.getElementById("btnAuditTrail");
			var objBtnDelEntries = document.getElementById("btnDelEntries");
            var objBtnSendPDFEntries = document.getElementById("btnSendPDFEntries");
			var objBtnMapData = document.getElementById("btnMapData");
			var arrayLinkEdit = document.getElementsByName("linkEdit");
			var arrayTrCollection = document.getElementsByName("tr_collection_checkbox");
			var i;
			
			if(ctrlEditAudit==1)
			{
				objBtnBack.style.display = 'inline';
				objBtnEditLog.style.display = 'none';
				objBtnMapData.style.display = 'none';
				//objBtnAuditTrail.style.display = 'inline';
                objBtnDelEntries.style.display = 'inline';
                objBtnSendPDFEntries.style.display = 'inline';
				//obDivTitle.innerHTML = 'Edit Log<hr>';
				
				if(arrayLinkEdit!=undefined && arrayLinkEdit!=null)
				{
    				for(i=0; i<arrayLinkEdit.length; i++)
    				{
    					objLinkEdit = arrayLinkEdit[i];
    					objLinkEdit.style.display = "inline";
    				}
				}

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
      				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = ((bIsIE)?"inline":"table-cell");
    				}
    			}
			}
			else
			{
				objBtnBack.style.display = 'none';
				objBtnEditLog.style.display = 'inline';
				objBtnMapData.style.display = 'inline';
				//objBtnAuditTrail.style.display = 'none';
				objBtnDelEntries.style.display = 'none';
                bjBtnSendPDFEntries.style.display = 'none';
				//obDivTitle.innerHTML = 'Log: Data Captures<hr>';

				if(arrayLinkEdit!=undefined && arrayLinkEdit!=null)
				{
    				for(i=0; i<arrayLinkEdit.length; i++)
    				{
    					objLinkEdit = arrayLinkEdit[i];
    					objLinkEdit.style.display = "none";
    				}
				}

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
    				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = 'none';
    				}
    			}		
			}
		}
		
		function extractNumber(obj)
		{
			//Obtener el valor del input text
			var temp = obj.value;

			//Crear nuestra expresion regular con digitos
			var reg0Str = '[0-9]*';

			//No se manejara signo negativo 
			//reg0Str = '^-?' + reg0Str;
			reg0Str = '^' + reg0Str;

			//Indicamos que nuestra expresion regular solo
			//aceptara digitos al final de la cadena
			reg0Str = reg0Str + '$';

			//Hacemos la instancia RegExp para que se cree
			//un objeto tipo expresion regular
			var reg0 = new RegExp(reg0Str);

			//Evaluamos nuestra cadena o valor que se
			//encuentra en nuestro input text
			//Si regresa true esto quiere decir
			//que hasta el momento si se ha cumplido
			//con la expresión regular
			if (reg0.test(temp)) return true;

			//Creamos una cadena con todos los caracteres permitidos
			//en nuestra entrada de texto, esto es digitos y el signo
			//negativo, el [^.....] indica que se hara una busqueda
			//para todos aquellos caracteres que no esten entre los
			//corchetes
			var reg1Str = '[^0-9' + ']';

			//Creamos nuestra expresión regular, pero se
			//le indica ahora, que es una expresión regular
			//con búsqueda globalpara todas las entradas de
			//texto que no cumplen con lo especificado en reg1Str
			var reg1 = new RegExp(reg1Str, 'g');
			//Se reemplaza dichos caracteres con vacío
			temp = temp.replace(reg1, '');

			//Se asigna nuevamente el valor a obj.value
			obj.value = temp;
		}
		
		function checkLimitNumber(obj)
		{
			// value is present
			var tval=Trim(obj.value);
			if (tval=='')
			{
				return true;
			}
			
			reg=/^0*/;
			tval=tval.replace(reg,'')

			if (tval!='')
			{
				val=parseInt(tval);
			}
			else
			{
				val=0;
			}

			var min=1;
			var max=1000;
			
			var msg="";

			if(min!='' && max!='')
			{
				msg='Max records should be in range of ' + min + ' to ' + max + '.' ;
			}
			else
			{
  				if(min!='')
  				{
  					msg='Input value should be greater than or equal to ' + min +'.';
  				}
				else
				{
    				if(max!='')
    				{
						msg='Input value should be less than or equal to ' + max + '.';
    				}
				}
			}

			if(min!='')
			{
				if (min>val)
				{
					alert(msg);
    				obj.value = 100;
				}
			}

			if (max!='')
			{
				if (val>max) 
				{
					alert(msg);
    				obj.value = 100;
				}
			}
		}

		function zipReport()
		{
			frmZipReport.submit();
		}
		
		function exportPPTX()
		{
			frmPPTXReport.submit();
		}
		
 		function exportReport()
 		{
 			var objXLSImages = document.getElementById("chkXLSImages");
			frmExportReport.AddPhotos.value = "";
 			if (objXLSImages != null && objXLSImages.checked) {
 				frmExportReport.AddPhotos.value = "1";
 			}
			frmExportReport.submit();
 		}

 		//Exportar los datos del Reporte actual en GoogleMaps
 		function googleMap(surveyID, userID, dateID, reportID, reportType)
 		{
 	 		console.log('googleMap3');
			openGoogleMap(surveyID, userID, dateID, dateID, reportID, reportType);
 		}

		var filterAttributes = new Array;
<?
		$i=0;
		foreach ($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			filterAttributes[<?=$i?>]=<?=$memberID?>;
<?			$i++;
		}
?>
		var numFilterAttributes = <?=$this->NumFilterAttributes?>;

<?
		if(count($this->FilterAttributes)>0)
		{
?>
		function refreshAttributes(objAttrib)
		{
			if(objAttrib.value=='sv_ALL_sv')
			{
				for(i=0; i<numFilterAttributes; i++)
				{
					var obj = document.getElementById("AttribID_"+filterAttributes[i]);
					if(obj.getAttribute("memberOrder") > objAttrib.getAttribute("memberOrder"))
					{
				 		obj.length = 0;

						var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
						obj.options[obj.options.length] = newOption;
					}
				}
				return;
			}

			var xmlObj = getXMLObject();

			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}

			strMemberIDs="";
			strParentIDs="";
			strValues="";

			for(i=0; i<numFilterAttributes; i++)
			{
				var obj = document.getElementById("AttribID_"+filterAttributes[i]);
				if(obj.getAttribute("memberOrder") <= objAttrib.getAttribute("memberOrder"))
				{
					if(strMemberIDs!="")
					{
						strMemberIDs+="_SVSEPSV_";
					}
					strMemberIDs+=obj.getAttribute("memberID");

					if(strParentIDs!="")
					{
						strParentIDs+="_SVSEPSV_";
					}
					strParentIDs+=obj.getAttribute("parentID");

					if(strValues!="")
					{
						strValues+="_SVSEPSV_";
					}

					strValues+=obj.value;
				}
				else
				{
					break;
				}
			}

		 	xmlObj.open("POST", "getAttribValues.php?SurveyID=<?=$this->SurveyID?>&CatalogID=<?=$this->CatalogID?>&CatMemberID=<?=$this->FirstCatQuestion->CatMemberID?>&ChangedMemberID="+objAttrib.getAttribute("memberID")+"&strMemberIDs="+strMemberIDs+"&strParentIDs="+strParentIDs+"&strValues="+strValues, false);

		 	xmlObj.send(null);

		 	strResult = Trim(unescape(xmlObj.responseText));

		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();

		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		var numAtVal = attribValues.length;

			 		var obj = document.getElementById("AttribID_"+membID);

			 		obj.length = 0;

					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;


			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}

			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
		 	// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
			for(i=0; i<numFilterAttributes; i++)
			{
				var obj = document.getElementById("AttribID_"+filterAttributes[i]);
				if(obj.getAttribute("memberOrder") > objAttrib.getAttribute("memberOrder") && !inArray(refreshAttrib, obj.getAttribute("memberID")))
				{
			 		obj.length = 0;

					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
				}
			}
		}
<?
		}
?>
		</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	<script>
	
		delete Array.prototype.contains;
		bitamApp.getAppFormsDefinition();
		$.extend($, { t: function (word) {
				return $.jsperanto.translate(word);
			}
		});
				
		function addDescriptionTag (tagName, tagDescription, element) {
			var realTagName = $.t(tagName);
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = 1;
			} 
			else
				theTag.elements[elementID].num = (theTag.elements[elementID].num+1);
		}
		
		var descriptionTags = {};
		
		var searchString = '';
		<? if (isset($_POST["searchString"]) && $_POST["searchString"] != '') { ?>
			searchString = '<?= $_POST["searchString"] ?>';
		<? } ?>
		
		var filterDate = '';
		<? if ((isset($_POST["startDate"]) && $_POST["startDate"] != '') && (isset($_POST["endDate"]) && $_POST["endDate"] != '')) { ?>
			filterDate = '<?= $_POST["startDate"] ?>@<?= $_POST["endDate"] ?>';
		<? } ?>
		//addDescriptionTag('Groups','Groups' , {id: 1, name:"Pepito"})
		<?
		foreach ($this->Collection as $aReports) { ?>
			addDescriptionTag('Groups','Groups',{id: <?= $aReports->GroupID ?>,  name:"<?= $aReports->GroupName ?>"});
			addDescriptionTag('Users','Users' , {id: "<?= $aReports->UserName ?>",   name:"<?= $aReports->UserName ?>" });
			addDescriptionTag('Forms', 'Forms', {id: <?= $aReports->SurveyID ?>, name:"<?= $aReports->SurveyName ?>"});
	<?	} ?>
		
		/*addDescriptionTag('Groups','Groups' , {id: 1, name:"Pepito"});
		addDescriptionTag('Users', 'Users', {id: 2, name:"Marissa"});
		addDescriptionTag('Forms', 'Forms', { id: 30, name: "BarCode WXD1Z5" });
		addDescriptionTag('Forms', 'Forms', {id: 3, name:"MyFormsOne"});*/
		
		var oldGroupsTags = JSON.parse(<?= json_encode((isset($_POST['oldGroupsTags']) && $_POST['oldGroupsTags'] != '') ? $_POST['oldGroupsTags'] : '{}') ?>);
		var oldUserTags = JSON.parse(<?= json_encode((isset($_POST['oldUserTags']) && $_POST['oldUserTags'] != '') ? $_POST['oldUserTags'] : '{}') ?>);
		var oldFormsTags = JSON.parse(<?= json_encode((isset($_POST['oldFormsTags']) && $_POST['oldFormsTags'] != '') ? $_POST['oldFormsTags'] : '{}') ?>);
		
		var checkedElemenTags = {};

		/*@AAL 24/04/2015: agregado para identificar si se trata de una agenda o AgendaScheduler y mostrar el filtro fecha,
		la cual solo debe de mostrarse en agendas, la validación se muestra em el archivo jquery.widgets.js*/
		var isAgenda = true;

		function initBoxFilter () {
			var checkedValues = ("<?= (isset($_POST['GroupID']) ? $_POST['GroupID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Groups'] = checkedValues.split(',');
				$.each(checkedElemenTags['Groups'], function (index, value) {
					value = ('' + value);
				});
			}

			var checkedValues = ("<?= (isset($_POST['userID']) ? $_POST['userID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Users'] = checkedValues.split(',');
				$.each(checkedElemenTags['Users'], function (index, value) {
					value = ('' + value);
				});
			}

			var checkedValues = ("<?= (isset($_POST['SurveyID']) ? $_POST['SurveyID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Forms'] = checkedValues.split(',');
				$.each(checkedElemenTags['Forms'], function (index, value) {
					value = ('' + value);
				});
			}
			
			if (!$.isEmptyObject(oldGroupsTags) && descriptionTags['Groups'] && descriptionTags['Groups'].elements) {
				$.extend(descriptionTags['Groups'].elements, oldGroupsTags);
			}
			if (!$.isEmptyObject(oldUserTags) && descriptionTags['Users'] && descriptionTags['Users'].elements) {
				$.extend(descriptionTags['Users'].elements, oldUserTags);
			}
			if (!$.isEmptyObject(oldFormsTags) && descriptionTags['Forms'] && descriptionTags['Forms'].elements) {
				$.extend(descriptionTags['Forms'].elements, oldFormsTags);
			}
						
			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate
			};
			
			if ($.isEmptyObject($('#boxFilter').data())) 
				$('#boxFilter').boxfilter(options);
			else 
				$('#boxFilter').boxfilter('update');
			
		}
		
		try { 
			initBoxFilter(); 
		} catch (e) { 
			alert('Search filter error: '+e);
		}
		
	</script>
	
    <form name="frmSendPDF" action="reportSendPDF.php" method="GET" target="_self">
		<input type="hidden" name="ReportSendPDFIDs" id="ReportSendPDFIDs" value="">
        <input type="hidden" name="surveyID" id="surveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="userID" id="userID" value="">
	</form>
        
    <form name="frmChangeFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID">
	</form>

	<form name="frmAttribFilter" action="main.php?BITAM_SECTION=ReportCollection" method="POST" target="body" accept-charset="utf-8">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="GroupID" id="GroupID" value="">
		<input type="hidden" name="SurveyID" id="SurveyID" value=<?=($this->SurveyID != 0) ? $this->SurveyID : "" ?>>
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">
	
		<input type="hidden" name="searchString" id="searchString" value="">
		<input type="hidden" name="oldGroupsTags" id="oldGroupsTags" value="">
		<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
		<input type="hidden" name="oldFormsTags" id="oldFormsTags" value="">
	</form>

	<form id="frmZipReport" name="frmZipReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ZipReport">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
	
	<form id="frmPPTXReport" name="frmPPTXReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="PPTXReport">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">

<?
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
	
	<form id="frmExportReport" name="frmExportReport" action="main.php" method="GET" target="_self">
		<input type="hidden" name="BITAM_SECTION" value="ReportCollection">
		<input type="hidden" name="SurveyID" id="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" name="ReportAction" id="ReportAction" value="ExportCVS">
		<input type="hidden" name="startDate" id="startDate" value="<?=htmlentities($this->StartDate)?>">
		<input type="hidden" name="endDate" id="endDate" value="<?=htmlentities($this->EndDate)?>">
		<input type="hidden" name="userID" id="userID" value="<?=$this->UserID?>">
		<input type="hidden" name="AddPhotos" id="AddPhotos" value="">
<?
		global $newExcelMethod;
		if($newExcelMethod)
		{
?>		
		<input type="hidden" name="newExcelMethod" id="newExcelMethod" value="1">
<?
		}
		
		foreach($this->FilterAttributes as $memberID => $memberInfo)
		{
?>
			<input type="hidden" name="AttribID_<?=$memberID?>" id="AttribID_<?=$memberID?>" value="<?=$this->AttribSelected[$memberID]?>">
<?
		}
?>
	</form>
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}

	function displayCheckBox($aUser)
	{
		return false;
	}

	function afterTitleCodeBack($aUser)
	{
		?>
		<div id="boxFilter" style="float:left; width: 190px; height: 100%; background:white;border-right: 1px solid lightgray"></div>
		<?
	}

	function afterTitleCode_($aUser)
	{
		?>
		<div id="boxFilter" style="float:left; width: 190px; height: 100%; background:white;border-right: 1px solid lightgray"></div>
		<?

		$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		$numSurveys = count($surveyCollection->Collection);
		
		$numFilters = 1 + count($this->FilterAttributes) + 3;
		$numRows = ceil($numFilters/3);
		$countCell = 0;
		$maxCell = 4;
		//@JAPR 2012-04-04: Modificado el combo de encuestas para que aparezca mas grande y no corte nombres de encuestas largos
		$numColSpanForm = 4; // 2;
		//@JAPR
		$totalColSpanTable = 1 + ($maxCell*2);
		
		$hasAttribFilter = false;
		//Revisamos todos los filtros de los catalogos si los hay
		if(!is_null($this->AttribSelected))
		{
			foreach ($this->AttribSelected as $idx=>$value)
			{
				if($value!="sv_ALL_sv")
				{
					$hasAttribFilter = true;
					break;
				}
			}
			
		}
		
		if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100 || $hasAttribFilter==true)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = translate("Reapply");
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = translate("Apply");
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply filter").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">'.translate("Clear").'</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		//$btnMapData='<span id="btnMapData" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="googleMap('.$this->SurveyID.');"><img src="images/map.gif">&nbsp;'.translate("Map data").'</span>';
		$btnMapData='<div class="collection_buttons" style="border:0px; background:transparent"><button id="btnMapData" onclick="googleMap('.$this->SurveyID.');"><img src="images/img_mapa.png" alt="'.translate("Map data").'" title="'.translate("Map data").'" displayMe="1" />'.translate("Map data").'</button></div>';
		//$btnZipData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="zipReport();"><img src="images/zip.gif">&nbsp;'.translate("Download images").'</span>';
		$btnZipData='<div class="collection_buttons" style="border:0px; background:transparent"><button onclick="zipReport();"><img src="images/zip.gif" alt="'.translate("Download images").'" title="'.translate("Download images").'" displayMe="1" />'.translate("Download images").'</button></div>';
		//$btnPPTXData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="exportPPTX();"><img src="images/pptx.gif">&nbsp;'.translate("Download images").'</span>';
		$btnPPTXData='<div class="collection_buttons" style="border:0px; background:transparent"><button onclick="exportPPTX();"><img src="images/pptx.gif" alt="'.translate("Download images").'" title="'.translate("Download images").'" displayMe="1" />'.translate("Download images").'</button></div>';		
//		$btnExportData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="exportReport();"><img src="images/download.gif">&nbsp;'.translate("Export data").'</span>&nbsp;<div style="display:inline-block;height:16px"><input type="checkbox" id="chkXLSImages" name="chkXLSImages"/><label for="chkXLSImages" style="display:inline">'.translate('Include images').'</label></div>';
		$btnExportData='<div class="collection_buttons" style="border:0px; background:transparent"><button onclick="exportReport();"><img src="images/download.gif" alt="'.translate("Export data").'" title="'.translate("Export data").'" displayMe="1" />'.translate("Export data").'</button></div>&nbsp;<div style="display:inline-block;height:16px"><input type="checkbox" id="chkXLSImages" name="chkXLSImages"/><label for="chkXLSImages" style="display:inline">'.translate('Include images').'</label></div>';
//		$btnEditAudit='<span id="btnEditLog" style="display:inline" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:editAudit();"><img src="images/edit.png">&nbsp;'.translate("Edit log").'</span>';
		$btnEditAudit='<div class="collection_buttons" style="border:0px; background:transparent"><button id="btnEditLog" style="display:inline" onclick="javascript:editAudit();"><img src="images/edit.png" alt="'.translate("Edit log").'" title="'.translate("Edit log").'" displayMe="1" />'.translate("Edit log").'</button></div>';
//		$btnEditAudit.='<span id="btnAuditTrail" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:return false;"><img src="images/edit.png">&nbsp;'.translate("Audit trail").'</span>';
		//$btnEditAudit.='<div class="collection_buttons" style="border:0px; background:transparent"><button id="btnAuditTrail" style="display:none" onclick="javascript:return false;"><img src="images/edit.png" alt="'.translate("Audit trail").'" title="'.translate("Audit trail").'" displayMe="1" />'.translate("Audit trail").'</button></div>';
		
		$arrayButtons[0]=$btnMapData;
		$arrayButtons[1]=$btnZipData;
		// mostrar botón de pptx report???
		if (true) //(isset($_SESSION["PABITAM_RepositoryName"]) && $_SESSION["PABITAM_RepositoryName"] == "fbm_bmd_0375")
		{
		  $arrayButtons[2]=$btnPPTXData;
  		$arrayButtons[3]=$btnExportData;
  		$arrayButtons[4]=$btnEditAudit;
		}
		else 
		{
  		$arrayButtons[2]=$btnExportData;
  		$arrayButtons[3]=$btnEditAudit;
		}
		
		/*
		$arrayButtons[1]=$btnExportData;
		$arrayButtons[2]=$btnEditAudit;
		*/
		$btnDelEntries = '<span id="btnDelEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:BITAMReportCollection_Remove();"><img src="images/delete.png">&nbsp;'.translate("Delete entries").'</span>';
        $btnSendPDFEntries = '<span id="btnSendPDFEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript: reportcollectionSendPDF();"><img src="images/delete.png">&nbsp;'.translate("Send PDF entries").'</span>';
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons);
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo=
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
							if(Trim(v)=='00:00:00' || Trim(v)=='')
							{
								v=' 23:59:59';
							}
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

?>
	<span id="btnBack" style="display:none" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editAudit();"><img src="images/home.png">&nbsp;<?="Back"?></span>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
			<?=translate("Survey")?>&nbsp;
			</td>
			<td colspan="<?=$numColSpanForm?>">
				<select id="SurveyID" name="SurveyID" style="width:100%;font-size:11px" onchange="javascript:applyFilterSurvey();">
<?
			global $idxSelSurveyID;
			$idxSelSurveyID = 0;
			$cmbIndex = 0;

			for ($i=0; $i<$numSurveys; $i++)
			{
				$anSurveyID = $surveyCollection->Collection[$i]->SurveyID;
				$select="";
				if($this->SurveyID == $anSurveyID)
				{
					$idxSelSurveyID = $cmbIndex;
					$select = "selected";
				}
?>
					<option value="<?=$anSurveyID?>" <?=$select?>><?=($surveyCollection->Collection[$i]->SurveyName)?></option>
<?
				$cmbIndex++;
			}
?>
				</select>
			</td>
<?
			$countCell = 1;
			
			while(($countCell%$maxCell)!=0)
			{
?>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
				$countCell++;
			}
?>
		</tr>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		
		if($_SESSION["PABITAM_UserRole"] == 1) {
			$arrayUsers[-2] = translate("All");
			$arrayUsers[-1] = "NA";
		}
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		
		/*@AAL 31/03/2015: Modificado para mostrar los Emails o el Nombre largo si el Rol del usuario logeado es User y además tiene subordinados*/
		if($_SESSION["PAFBM_Mode"]==1)
		{
			if($_SESSION["PABITAM_UserRole"] == 0) {
				$sql.=" AND B.CLA_USUARIO = " . $_SESSION["PABITAM_UserID"] . " UNION 
				        SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B 
				        WHERE A.CLA_USUARIO = B.CLA_USUARIO AND A.Supervisor = " . $_SESSION["PABITAM_UserID"];
			} else {
				//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
				//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
				$sql.=" AND B.CLA_USUARIO > 0 ";
				//@JAPR
			}
		}

		$sql.=" ORDER BY NOM_LARGO ";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;

			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		
		if((count($this->FilterAttributes)+1)>$countButtons)
		{
			$maxRows = count($this->FilterAttributes)+1;
		}
		else 
		{
			$maxRows = $countButtons;
		}

		$selected = "";
		if($this->SurveyID!=0)
		{
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?=translate("Filters")?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser=>$nameUser)
		{
			$selected="";

			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
			$countCell=1;
		}
		
		$loadCat = true;
		$strAtribFilter = "";
		if(count($this->FilterAttributes)>0)
		{
			foreach($this->FilterAttributes as $memberID => $memberInfo)
			{
				$position++;
				$select="";

				if($position==$positionDates)
				{
					if($numRows==0)
					{
?>
			<td style="text-align:right;" class="styleBorderFilterTop">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
			</td>
			<td class="styleBorderFilterTop">
				<?=$strFrom?>
			</td>
<?
					}
					else if($numRows==1)
					{
?>
			<td style="text-align:right;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
			</td>
			<td>
				<?=$strTo?>
			</td>
<?
					}
					else if($numRows==2)
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}

?>
			<td style="text-align:right;" <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("Number of records")?></span>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
			<td <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
			
				<?=$this->Pages->display_items_per_page_report()?>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					$classBorderTop = "";
					$classBorderRightTop = "";
					if($numRows==0)
					{
						$classBorderTop = ' class="styleBorderFilterTop"';
						$classBorderRightTop = ' class="styleBorderFilterRightTop"';
					}
					else 
					{
						$classBorderTop = '';
						$classBorderRightTop = ' class="styleBorderFilterRight"';
					}
					
					if(isset($arrayFilters[$numRows]))
					{
?>
			<td <?=$classBorderTop?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightTop?>>
				<?=$arrayFilters[$numRows]?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = "";
						$classBorderRightBottom = "";
						if($maxRows == ($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
							$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderBottom = '';
							$classBorderRightBottom = ' class="styleBorderFilterRight"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					if(isset($arrayButtons[$numRows]))
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td style="padding-top:5px;padding-bottom:5px;">
				<?=$arrayButtons[$numRows]?>
			</td>
<?
					}
					else 
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
					}
					
					$countCell++;
				}
				
				if(($countCell%$maxCell)==0)
				{
					$numRows++;
					$position = 1;
?>
		</tr>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
				else 
				{
					$position++;
				}
				
				$classBorderLeft = ' class="styleBorderFilterLeft"';
				$classBorderBottom = '';
				if($maxRows == ($numRows+1))
				{
					$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
					$classBorderBottom = ' class="styleBorderFilterBottom"';
				}

?>
			<td style="text-align:right;" <?=$classBorderLeft?>>
				&nbsp;&nbsp;&nbsp;<span><?=$memberInfo["MemberName"]?></span>
			</td>
			<td <?=$classBorderBottom?>>
				<select id="AttribID_<?=$memberID?>" name="AttribID_<?=$memberID?>" memberID="<?=$memberID?>" parentID="<?=$memberInfo["ParentID"]?>" memberOrder="<?=$memberInfo["MemberOrder"]?>" onchange="javascript:refreshAttributes(this)" style="width:150px; font-size:11px">
<?
				global $idxSelSurveyID;
				$idxSelSurveyID = 0;
				$cmbIndex = 0;

				if($loadCat==true)
				{

?>
					<option value="sv_ALL_sv" ><?=translate("All")?></option>
<?
					$attribValues = BITAMReportCollection::getAttribValues($this->Repository, $this->CatTableName, $memberInfo["FieldName"], $strAtribFilter);
					foreach ($attribValues as $key=>$desc)
					{
						$select="";

						if($this->AttribSelected[$memberID] == $key)
						{
							$idxSelSurveyID = $cmbIndex;
							$select = "selected";
						}

?>
						<option value="<?=$key?>" <?=$select?>><?=$desc?></option>
<?
						$cmbIndex++;
					}

					if($this->AttribSelected[$memberID]!='sv_ALL_sv')
					{
						$strAtribFilter.= " AND ".$memberInfo["FieldName"]." = ".$this->Repository->DataADOConnection->Quote($this->AttribSelected[$memberID]);
					}

				}
				else
				{
?>
					<option value="sv_ALL_sv" selected><?=translate("All")?></option>
<?
				}
?>
				</select>
			</td>
<?
				if($this->AttribSelected[$memberID]=='sv_ALL_sv')
				{
					$loadCat = false;
				}
				
				$countCell++;
			}
		}
		
		if($this->SurveyID!=0)
		{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
	?>
				<td style="text-align:right;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("Number of records")?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
                <?=$btnSendPDFEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		}
?>
	</table>
	<br>
<?
	}
	
	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="SurveyID" name="SurveyID" value="<?=$this->SurveyID?>">
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
<?
	}
}
?>