<?php
require_once("survey.inc.php");
require_once("object.trait.php");

class BITAMSurveyExt extends BITAMSurvey
{
	use BITAMObjectExt;
	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {	
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyExt";
		}
		else
		{
			//@JAPR 2015-04-15: Rediseñado el Admin con DHTMLX
			//Cuando se cargue la instancia para edición desde la colección, debe hacerlo en modo de Diseño
			//return "BITAM_PAGE=SurveyExt&SurveyID=".$this->SurveyID;
			if (getParamValue("Design", 'both', '(int)')) {
				return "BITAM_SECTION=SurveyExtCollection&Design=1&SurveyID=".$this->SurveyID;
			}
			else {
				return "BITAM_PAGE=SurveyExt&SurveyID=".$this->SurveyID;
			}
		}
	}
	
	//@JAPR 2015-06-10: Agregada la propiedad para obtener la imagen a usar en la vista tipo Metro
	function get_Image() {
		$strImage = $this->SurveyImageText;
		//@JAPR 2015-07-27: Corregida la ruta de las imagenes, ahora ya no es un Tag IMG así que se le debe agregar manualmente (#7TONG4)
		if ($strImage) {
			$strImage = "<img src=\"{$strImage}\" />";
		} else {
			$strImage = "<img src=\"images/admin/formImg.png\" />";
		}
		
		return $strImage;
	}
	
	/* Genera la ventana de definición de la forma específicada según el rediseño, la cual contempla el árbol con los elementos de la forma y la sección del
	preview
	*/
	function generateDesignWindow() {
		global $widthByDisp;
		global $heightByDisp;
		global $gbDesignMode;
		$gbDesignMode = true;
		//@JAPR 2015-10-02: Validado que en caso de error no detenga la ejecución si se encuentra en modo diseño (#MBN30C)
		global $gblShowErrorSurvey;
		$gblShowErrorSurvey = false;
		
		//@JAPR 2015-08-22: Corregido un bug, no estaba aplicando la versión del App así que al pedir definiciones no hacía todas la validaciones adecuadamente
		global $appVersion;
		$appVersion = ESURVEY_SERVICE_VERSION;
		
		$arrQTypeIDs = BITAMQuestion::getQuestionTypes($this->Repository);
		if (is_null($arrQTypeIDs) || !is_array($arrQTypeIDs)) {
			$arrQTypeIDs = array();
		}
		
		require_once("eSurveyServiceMod.inc.php");
		require_once("appuser.inc.php");
		require_once("usergroup.inc.php");
		require_once("catalog.inc.php");
		require_once("surveyScheduler.inc.php");
		require_once("dataSource.inc.php");
		require_once("eFormsDataDestinations.inc.php");
		
		//Carga el scheduler asociado a la forma (en esta versión debe ser un Scheduler único)
		$objSurveyScheduler = null;
		if ($this->SchedulerID > 0) {
			$objSurveyScheduler = BITAMSurveyScheduler::NewInstanceWithID($this->Repository, $this->SchedulerID);
		}
		
		//Carga todos los usuarios y demás objetos requeridos que se encuentran definidos en el repositorio
		$objUserColl = BITAMAppUserCollection::NewInstance($this->Repository);
		$objUserGroupsColl = BITAMUserGroupCollection::NewUserGroupCollection($this->Repository);
		$objCatalogColl = BITAMCatalogCollection::NewInstance($this->Repository);
		$objAttributesByCatalog = array();
		foreach ($objCatalogColl->Collection as $objCatalog) {
			$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($this->Repository, $objCatalog->CatalogID);
			$objAttributesByCatalog[$objCatalog->CatalogID] = $objCatalogMembersColl;
		}
		
		$objDataSourceColl = BITAMDataSourceCollection::NewInstance($this->Repository);
		$objAttributesByDataSource = array();
		foreach ($objDataSourceColl->Collection as $objDataSource) {
			$objDataSourceMembersColl = @BITAMDataSourceMemberCollection::NewInstance($this->Repository, $objDataSource->DataSourceID);
			$objAttributesByDataSource[$objDataSource->DataSourceID] = $objDataSourceMembersColl;
		}
		
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Asignada la variable para que se grabe en caché de secciones esta petición, así podrá ser reutilizada mas abajo por getEFormsDefinitions
		$objSectionCollection = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID, null, null, true);
		if (is_null($objSectionCollection)) {
			return;
		}
		$objQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		if (is_null($objQuestionCollection)) {
			return;
		}
		
		//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
		//Agregado el parámetro $oDataToGet con la opción para no cargar los valores de los catálogos en este punto
		$arrJSON = @getEFormsDefinitions($this->Repository, array('Survey' => $this->SurveyID), -1, array('surveys' => 1, 'catalogs' => 1, 'skipCatalogValues' => 1));
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		//@JAPR 2018-10-29: Corregido un bug, algunos caracteres fuera del rango válido de utf8 provocaban que el response fallara, por lo tanto se removerán cambiándolos por "?" (#KGW0CU)
		$arrJSON = FixNonUTF8CharsInArray($arrJSON);
		//@JAPR
		$strJSONDefinitions = json_encode($arrJSON);
/*
?>
	<PRE>
<?
		echo("SurveyID: {$this->SurveyID}:");
		echo("<br>\r\n".htmlspecialchars($strJSONDefinitions));
?>
	</PRE>
<?
	die();
*/
?>
<html>
	<head>
		<meta http-equiv="x-ua-compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="js/libs/metisMenu/src/metisFolder.css"/>
		<link rel="stylesheet" type="text/css" href="js/libs/selectize.js/dist/css/selectize.bootstrap3.css"/>
		<link rel="stylesheet" type="text/css" href="css/materialModal.css"/>
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
		<!--<link rel="stylesheet" type="text/css" href="js/codebase/skins/bitam2/dhtmlx.css"/>-->
		<!--<script src="js/codebase/dhtmlx.js"></script>-->
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
		<script type="text/javascript" src="js/variables.js"></script>
		<!--------------------------------------------------------------------->
		<!-- Inician librerías para el nuevo editor HTML!-->
			<!--Librerias de Redactor-->
			<link rel="stylesheet" href="js/redactor/redactor/redactor.css" />
			<script src="js/redactor/redactor/redactor.js"></script>
			<!--Librería de font-awesome-->
			<!--link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"-->
			
			<!--Plugins para la toolbar-->
			<script src="js/redactor/imagemanage.js"></script>
			<script src="js/redactor/underline.js"></script>
			<script src="js/redactor/fontsize.js"></script>
			<script src="js/redactor/fontfamily.js"></script>
			<!--En el archivo "fontcolor.js" buscar la línea que dice: $dropdown.width(242); 
			Cambiar el valor de (242) por: (287) Esto hace que se puedan alinear los colores de la paleta-->
			<script src="js/redactor/fontcolor.js"></script>
			<script src="js/redactor/formulaEditor.js"></script>
			
			<!--Selección del lenguaje-->
			<script src="js/redactor/lang/es.js"></script>
			<script src="js/redactor/lang/en.js"></script>
		<!-- Terminan librerías para el nuevo editor HTML!-->
		<!------------------------------------------------------------------- -->
		
		<style>
			/* Estilos para ocultar el icono de las ventanas modales (todas las skins utilizadas) */
			.dhxwins_vp_dhx_skyblue div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			.dhxwins_vp_dhx_web div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			/* Estilo para los botones de búsqueda asociados a grids */
			.search_button input {
				border:1px solid #e1e1e1 !important;
			}
			.selection_container div.dhxform_container {
				border:1px solid #e1e1e1 !important;
				//height:90% !important;
				////overflow-y:auto;
			}
			.selection_container div.objbox {
				//overflow-y:auto;
			}
			.selection_list fieldset.dhxform_fs {
				border:1px solid #e1e1e1 !important;
				//height:95% !important;
			}
			div.dhxform_item_label_left.assoc_button div.dhxform_btn_txt {
				background-image:url(images/add_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
				cursor: pointer !important;
			}
			div.dhxform_item_label_left.remove_button div.dhxform_btn_txt {
				background-image:url(images/del_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
				cursor: pointer !important;
			}
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			span.tabFormsOpts {
				font-size:12px;
			}
			span.tabFormsOptsNoSel {
				cursor: default;
				font-size:12px;
			}
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				 text-transform: none !important;
			}
			/*
			.dhx_dataview_customSection_item_selected{
				border:0px !important;
			}
			.dhx_dataview_customSection_item{
				border:0px !important;
			}
			*/
			/* Estilos para el editor HTML */
			.dhx_cell_cont_editor {
				width: 100% !important;
				border: 1px solid !important;
			}
			div.dhxform_item_label_right div.dhxform_control {
				margin-right: 10px !important;
			}
			.editor_compact div.dhx_cell_stb {
				height:20px !important;
			}
			/*
			.editor_compact div.dhx_cell_cont_editor {
				top:21px !important;
			}
			*/
			.editor_compact div.dhx_cell_stb_button {
				margin-top:2px !important;
			}
			.editor_compact div.dhx_cell_toolbar_def {
				padding:0px !important;
				height:20px !important;
			}
			.editor_compact div.dhx_toolbar_btn {
				border: 0px !important;
				margin-top:0px !important;
				padding: 0px !important;
			}
			.editor_compact div.dhx_cell_editor {
			  width: 100% !important;
			}
			.editor_compact iframe.dhxeditor_mainiframe {
			  width: 100% !important;
			}
			.dhx_cell_editor div.dhx_cell_toolbar_def {
				border: 0px !important;
			}
			.editor_compact div.dhx_toolbar_text {
				padding:0px !important;
				margin:0px !important;
				height: 18px !important;
			}
			/* Estilos para ocultar las tabs pero permitir ver sus celdas */
			.hiddenTabs div.dhx_cell_tabbar {
				position:none !important;	//Este estilo no existe, pero efectivamente bloquea el aplicado a la clase que sobreescribe
			}
			.hiddenTabs div.dhxtabbar_tabs {
				display:none !important;
			}
			.hiddenTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			.visibleTabs div.dhx_cell_tabbar {
				position:absolute !important;	//Utilizado para pintar tabs que estarían dentro de la tab principal oculta
			}
			.visibleTabs div.dhxtabbar_tabs {
				display:inline !important;
			}
			.visibleTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			/* Estilo para los enlaces de las opciones principales (tabs ocultos) de la ventana de diseño */
			.linkToolbar {
				background-color: #264B83 !important;
			}
			.linktd {
				cursor: pointer;
				font-family: Roboto Regular !important;
				font-size: 13px !important;
				//font-weight: bold;
				//text-align: center;
				//text-decoration: underline;
				color: white !important;	//#333333;
				//padding-left: 24px;
				//padding-right: 24px;
				//width: 150px;
				white-space: nowrap;
			}
			.dhxtabbar_base_dhx_terrace div.dhxtabbar_tabs div.dhxtabbar_tab.dhxtabbar_tab_actv.linktd div.dhxtabbar_tab_text {
				color: white !important;
				text-decoration: underline;
			}
			.linktdSelected {
				text-decoration: underline;
			}
			/* Estilo para mantener seleccionado el botón de selección de la toolbar de preguntas */
			//Finalmente no sirvió esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, así que se perdía
			//la clase definida en este archivo, se optó por cambiar directamente el estilo
			.tbOptionSelected {
				  background-color: #fff3a1 !important;
			}
			/* Agrega el border a las celdas del grid */
			table.obj tr td {
				//border-left-width: 1px !important;
				border: 0px !important;
				padding-left: 0px !important;
				padding-right: 0px !important;
			}
			.dhx_textarea {
				margin-left: 0px;
			}
			/* Estilos para los subgrids de propiedades de las tabs de colecciones */
			.dhx_sub_row {
				//overflow: visible !important;
				border: 1px solid #808080 !important;
			}
			.dhx_sub_row table.obj tr td {
				border-left-width:1px solid #808080 !important;
			}
			.dhxdataview_placeholder div.dhx_dataview_item {
				border: 0px !important;
			}
			/* Estilos de los Layouts */
			.dhx_cell_hdr {
				background-color: #f5c862 !important;
			}
			/* Ajuste de botonera de secciones y preguntas (#J2BJYD) */
			.dhx_toolbar_dhx_terrace div.dhx_toolbar_btn div.dhxtoolbar_text {
				padding: 0px;
				margin: 2px 1px !important;
			}
			.dhxlayout_base_dhx_terrace div.dhxcelltop_toolbar {
				position: relative;
				padding-bottom: 6px;
				padding-top: 6px;
				overflow: hidden;
			}
			/* Estilo para corregir el diálogo de selección de colores en el Editor */
			.dhxcp_inputs_cont {
				width: 150px !important;
			}
			.dhxcp_inputs_cont input.dhxcp_input_hsl, .dhxcp_inputs_cont input.dhxcp_input_rgb {
				width: 30px;
			}
			.dhxcp_value_cont .dhxcp_value {
				width: 60px;
			}
			.dhx_clist label {
				display: inline;
			}
			@font-face {font-family: Lato; src:url('css/font/Lato-Regular.ttf'); }
		</style>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
		<script>
			//******************************************************************************************************
			//Objeto que contiene la versión del servicio y los updates manejados en el admin
			// contiene .serviceVersion y .updateVersion
			//******************************************************************************************************
			vObject = new VARIABLES();
			vObject.serviceVersion = '<?=getMDVersion()?>';

			//******************************************************************************************************
			//Types definitions
			//******************************************************************************************************
			//Tipos de objetos (basados en el config.php del Administrator, no en el App)
<?
		echo("
			AdminObjectType = {
				otyItem: ".otyItem.",
				otySurvey: ".otySurvey.",
				otySection: ".otySection.",
				otyQuestion: ".otyQuestion.",
				otyAnswer: ".otyAnswer.",
				otyAgenda: ".otyAgenda.",
				otyCatalog: ".otyCatalog.",
				otyAttribute: ".otyAttribute.",
				otyDraft: ".otyDraft.",
				otyOutbox: ".otyOutbox.",
				otyPhoto: ".otyPhoto.",
				otySignature: ".otySignature.",
				otySetting: ".otySetting.",
				otySurveyFilter: ".otySurveyFilter.",
				otySectionFilter: ".otySectionFilter.",
				otyMenu: ".otyMenu.",
				otyAudio: ".otyAudio.",
				otyVideo: ".otyVideo.",
				otyQuestionFilter: ".otyQuestionFilter.",
				otyStatusAgenda: ".otyStatusAgenda.",
				otyStatusDocument: ".otyStatusDocument.",
				otySketch: ".otySketch.",
				otyOption: ".otyOption.",
				otyShowQuestion: ".otyShowQuestion.",
				otyUser: ".otyUser.",
				otyUserGroup: ".otyUserGroup.",
				otyDataSource: ".otyDataSource.",
				otyDataSourceMember: ".otyDataSourceMember.",
				otyDataDestinations: ".otyDataDestinations.",
				otyArtusModels: ".otyArtusModels.",
				otyReport: ".otyReport.",
				otyAppCustomization: ".otyAppCustomization.",
				otyAgendasCheckpoint: ".otyAgendasCheckpoint.",
				otyAgendasScheduler: ".otyAgendasScheduler.",
				otyAgendasMonitor: ".otyAgendasMonitor.",
				otyLink: ".otyLink.",
				otyAppCustStyles: ".otyAppCustStyles.",
				otySkipRule: ".otySkipRule."
			};");
?>
			
			//Tipos de secciones
<?
		echo("
			SectionType = {
				Standard: ".sectNormal.",
				Dynamic: ".sectDynamic.",
				Formatted: ".sectFormatted.",
				Multiple: ".sectMasterDet.",
				Inline: ".sectInline.",
				Recap: ".sectRecap."
			};");
?>
		
			//Tipos de preguntas
<?
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		echo("
			Type = {
				number: ".qtpOpenNumeric.",
				simplechoice: ".qtpSingle.",
				multiplechoice: ".qtpMulti.",
				date: ".qtpOpenDate.",
				text: ".qtpOpenString.",
				alfanum: ".qtpOpenAlpha.",
				photo: ".qtpPhoto.",
				action: ".qtpAction.",
				signature: ".qtpSignature.",
				message: ".qtpMessage.",
				time: ".qtpOpenTime.",
				skipsection: ".qtpSkipSection.",
				calc: ".qtpCalc.",
				document: ".qtpDocument.",
				sync: ".qtpSync.",
				callList: ".qtpCallList.",
				gps: ".qtpGPS.",
				password: ".qtpPassword.",
				mapped: ".qtpMapped.",
				audio: ".qtpAudio.",
				video: ".qtpVideo.",
				sketch: ".qtpSketch.",
				section: ".qtpSection.",
				barCode: ".qtpBarCode.",
				ocr: ".qtpOCR.",
				exit: ".qtpExit.",
				updateDest: ".qtpUpdateDest.",
				myLocation: ".qtpMyLocation."
				, sketchPlus: ".qtpSketchPlus.",
			};");
?>
			
			//Tipos extendidos de preguntas para utilizar en los Fields de las propiedades (Ids para subclasificar los tipos de Open, Simple y demás)
			//Utilizan la numeración básica para las que son identificables con el objeto Type, pero para las que no tenían subclasificación usará un ID nuevo
			//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
			//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
			//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
			//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//OMMC 2019-03-25: Pregunta AR #4HNJD9
			QTypesExt = {
				openNumber: Type.number,
				simpleMenu: Type.simplechoice,
				simpleVert: 201,
				simpleHoriz: 202,
				simpleAuto: 203,
				simpleMap: 204,
				simpleGetData: 205,
				simpleMetro: 206,
				simpleAR: 207,
				multiMenu: Type.multiplechoice,
				multiVert: 301,
				multiHoriz: 302,
				multiAuto: 303,
				multiRank: 304,
				openDate: Type.date,
				openText: Type.text,
				openAlpha: Type.alfanum,
				photo: Type.photo,
				signature: Type.signature,
				message: Type.message,
				html: 1100,
				openTime: Type.time,
				skipsection: Type.skipsection,
				calc: Type.calc,
				document: Type.document,
				sync: Type.sync,
				callList: Type.callList,
				gps: Type.gps,
				openPassword: Type.password,
				audio: Type.audio,
				video: Type.video,
				sketch: Type.sketch,
				section: Type.section,
				barCode: Type.barCode,
				ocr: Type.ocr,
				exit: Type.exit,
				updateDest: Type.updateDest,
				myLocation: Type.myLocation,
				menuMenu: 401,
				menuVert: 402,
				menuHoriz: 403,
				image: 801
				, sketchPlus: Type.sketchPlus
			}
			
			//Tipos de despliegue
<?
		//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
		//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
		//OMMC 2019-03-25: Pregunta AR #4HNJD9
		echo("
			DisplayMode = {
				dspVertical: ".dspVertical.",
				dspHorizontal: ".dspHorizontal.",
				dspMenu: ".dspMenu.",
				dspMatrix: ".dspMatrix.",
				dspEntry: ".dspEntry.",
				dspAutocomplete: ".dspAutocomplete.",
				dspLabelnum: ".dspLabelnum.",
				dspDefault: ".dspDefault.",
				dspCustomized: ".dspCustomized.",
				dspMap: ".dspMap.",
				dspGetData: ".dspGetData.",
				dspMetro: ".dspMetro.",
				dspImage: ".dspImage.",
				dspAR: ".dspAR."
			};");
?>
			
<?
		echo("
			TypeOfFill = {
				tofFixedAnswers: ".tofFixedAnswers.",
				tofCatalog: ".tofCatalog.",
				tofMCHQuestion: ".tofMCHQuestion.",
				tofSharedAnswers: ".tofSharedAnswers.",
				tofInlineSection: ".tofInlineSection."
			};");
?>
			
			//JAPR 2015-07-15: Agregado el componente selList
			GFieldTypes = {
				alphanum: "edtxt",			//Texto en una sola linea editado inline
				text: "txttxt",				//Text area multiline
				combo: "coro",				//Combo simple (Combo methods && props: save(), restore(), size(), get(sidx), getKeys(), clear(), put(sidx, val), remove(sidx) .keys [array], .values [array]
				comboSync: "comboExt",		//Combo sincronizable con otras combos (Padres e hijas, por ejemplo, los atributos del catálogo seleccionado) (Personalizado)
				comboEdit: "combo",			//Combo editable (es un componente DHTMLX Combo)
				check: "ch",				//Checkbox
				editor: "editor",			//DHTMLXEditor
				number: "edn",				//Valores numéricos con máscara de captura
				date: "dhxCalendarA",		//Calendario con posibilidad de escribir la fecha
				hour: "time",				//Componente para capturar hora y minutos
				multiList: "clist",			//Lista con checkbox para seleccionar varios elementos
				color: "cp",				//Permite seleccionar el color de un conjunto predeterminado de posibilidades (personalizarlo al JColor que manejamos actualmente)
				image: "img",				//Una imagen con un click a una función
				grid: "sub_row_grid",		//Un grid dentro de otro con la funcionalidad default de propiedades
				readOnly: "rotxt",			//No contiene texto editable, sólo una etiqueta (opcional)
				uploadImg: "uploadImg",		//Subir imágenes al servidor
				selList: "selList",			//Un par de listas para mover elementos desde los disponibles hacia los seleccionados, permitiendo un ordenamiento
				formula: "edFormula",		//Un texto editable pero con un botón de editor
				editorHTML: "editorHTML",	//Nuevo editor HTML				
				editorHTMLDialog: "editorHTMLDialog" //Editor en diálogo
			};
			
			var optionsFormsSizes = new Object;
			optionsFormsSizes[0] = "<?=translate("Default")?>";
			optionsFormsSizes[100] = "<?=translate("Size")." x1"?>";
			optionsFormsSizes[200] = "<?=translate("Size")." x2"?>";
			optionsFormsSizes[300] = "<?=translate("Size")." x3"?>";
			
			<?//JAPR 2016-11-10: Corregido un bug, no estaba habilitada la configuración de sólo imagen y el orden era incorrecto (#FSMUZC)?>
			var optionsFormsLayOutTextPos = new Object;
			optionsFormsLayOutTextPos[<?=svtxpDefault?>] = "<?=translate("Default")?>";
			optionsFormsLayOutTextPos[<?=svtxpNone?>] = "<?=translate("Image only")?>";
			optionsFormsLayOutTextPos[<?=svtxpBottom?>] = "<?=translate("Text below image")?>";
			optionsFormsLayOutTextPos[<?=svtxpLeft?>] = "<?=translate("Text before image")?>";
			optionsFormsLayOutTextPos[<?=svtxpRight?>] = "<?=translate("Text after image")?>";
			optionsFormsLayOutTextPos[<?=svtxpTop?>] = "<?=translate("Text above image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverTop?>] = "<?=translate("Text inside and above image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverMiddle?>] = "<?=translate("Text inside and center image")?>";
			optionsFormsLayOutTextPos[<?=svtxOverBottom?>] = "<?=translate("Text inside and below image")?>";
			optionsFormsLayOutTextPos[<?=svtxpCustom?>] = "<?=translate("Form custom layout html")?>";
			var optionsFormsLayOutTextPosOrder = new Array;
			optionsFormsLayOutTextPosOrder.push(<?=svtxpDefault?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpNone?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpBottom?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpLeft?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpRight?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpTop?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverTop?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverMiddle?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxOverBottom?>);
			optionsFormsLayOutTextPosOrder.push(<?=svtxpCustom?>);
			
			optionsRegisterGPSAtOptsNew = [
				{value:<?=rgpsAtEnd?>, text:"<?=translate("When changing to another section")?>"},
				{value:<?=rgpsAtStart?>, text:"<?=translate("When starting data entry in the section")?>"}
			]

			optionsRegisterGPSAtOpts = {
				<?=rgpsAtEnd?>:"<?=translate("When changing to another section")?>",
				<?=rgpsAtStart?>:"<?=translate("When starting data entry in the section")?>"
			}
			
			optionsYesNo = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>"
			};
			
			optionsYesNoOpt = {
				0:"<?=translate("No")?>",
				1:"<?=translate("Yes")?>",
				2:"<?=translate("Optional")?>",
			};
			
			optionsYesNoOptPhoto = {
				0: {name:"<?=translate("No")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:false}]}},
				1: {name:"<?=translate("Yes")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:true}]}},
				2: {name:"<?=translate("Optional")?>", visibilityRules:{fields:[{tabId:"generalTab", fieldId:"allowGallery", show:true}]}}
			};
			
			//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
			optionsYesNoDef = {
				"-1": "<?=translate("Default")?>",
				"0": "<?=translate("No")?>",
				"1": "<?=translate("Yes")?>"
			};
			
			optionsGallery = {
				0:"<?=translate("Camera")?>",
				1:"<?=translate("Camera and Gallery")?>"
			};
			
			//JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
			var optionsAutoSelection = {
				1:"<?=translate("Empty")?>",
				0:"<?=translate("First value")?>"
			};
			
			var optionsAutoSelectionOrder = [1, 0];
			
			//OMMC 2015-11-26: Agregado el listado de tipos de despliegue de sección.
			SectionDisplayMode = {
				sdspDefault: 0,
				sdspInline: 1,
				sdspPages: 2
			};
			
			propTabType = {
				properties: 0,
				collection: 1
			}
			
			/* Opciones para utilizar en la definición de las configuraciones en los grids */
			var objCheck = {type:GFieldTypes.combo, options:optionsYesNo, default:0};
			var objCheckPlusOpt = {type:GFieldTypes.combo, options:optionsYesNoOpt, default:0};
			var objCheckPlusDef = {type:GFieldTypes.combo, options:optionsYesNoDef, default:"-1"};
			var objCheckPlusOptPhoto = {type:GFieldTypes.combo, options:optionsYesNoOptPhoto, default:0};
			var objCheckGallery = {type:GFieldTypes.combo, options:optionsGallery, default:0};
			var objAlpha = {type:GFieldTypes.alphanum, length:255};
			//JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
			var objCheckAutoSelection = {type:GFieldTypes.combo, options:optionsAutoSelection, optionsOrder:optionsAutoSelectionOrder, default:0};
			//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
			//Agregados los tipos de celdas (columnas) numéricos con una máscara genérica única
			var objInteger = {type:GFieldTypes.number};
			//02Oct2015: 
			//Cuando es sección de tipo "Repeticiones basadas en datos", quitar los campos:
			//Mostrar Casilla, Comportamiento invertido y Mínimo de selección
			var opSectionsType = {
				<?=sectNormal?>:{name:"<?=translate('Repeat once')?>"},
				<?=sectDynamic?>:{name:"<?=translate('Repeat based on data')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"strSelectOptions"}, 
							{tabId:"advTab", fieldId:"valuesSourceType"}
						]
					}
				},
				<?=sectMasterDet?>:{name:"<?=translate('Repeat multiple times')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"summaryInMDSection"}
						]
					}
				},
				<?=sectInline?>:{name:"<?=translate('Table')?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"strSelectOptions"}, 
							{tabId:"advTab", fieldId:"valuesSourceType"}, 
							{tabId:"advTab", fieldId:"showSelector"},
							{tabId:"advTab", fieldId:"switchBehaviour"},
							{tabId:"advTab", fieldId:"MinRecordsToSelect"}
							//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
							, {tabId:"advTab", fieldId:"useDescriptorRow"}
						]
					}
				}
			};
			//Ahora existirá un campo para indicar el tipo de sección como repeticiones de registros, así que adapta esa configuracion al tipo de sección definido
			//JAPR 2015-08-10: Por ahora no aplica aún la opción de catálogo
			//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
			var opSectionsRepetitionsType = {
				<?=srptSingle?>:{name:"<?=translate("One")?>"},
				<?=srptMulti?>:{name:"<?=translate("Multiples")?>", visibilityRules:{
						fields:[{tabId:"advTab", fieldId:"displayMode"}]
						//fields:[{tabId:"advTab", fieldId:"summaryInMDSection"}]
					}
				}
			};
			
			//JAPR 2015-07-16: Agregada la combo para el formato
			var strOptTabName = "generalTab";
			var opQuestionNumericFormat = {
				"mask1":'',
				"mask2":'0',
				"mask3":'#',
				"mask4":'#,##0',
				"mask5":'$#,##0',
				"mask6":'#,##0.00',
				"mask7":'$#,##0.00',
				"mask8":'# %',
				"mask9":'0 %',
				"mask10":'0.0 %'
			};
			
			var opQuestionDateFormat = {
				"mask0":'dd/mmm/yyyy',
				"mask1":'dd/yyyy/mmm',
				"mask2":'mmm/dd/yyyy',
				"mask3":'mmm/yyyy/dd',
				"mask4":'yyyy/mmm/dd',
				"mask5":'yyyy/dd/mmm',
				"mask6":'dd/yyyy/mm',
				"mask7":'dd/mm/yyyy',
				"mask8":'mm/dd/yyyy',
				"mask9":'mm/yyyy/dd',
				"mask10":'yyyy/mm/dd',
				"mask11":'yyyy/dd/mm',
				"mask12":'dd/yyyy/mmmm',
				"mask13":'dd/mmmm/yyyy',
				"mask14":'mmmm/dd/yyyy',
				"mask15":'mmmm/yyyy/dd',
				"mask16":'yyyy/mmmm/dd',
				"mask17":'yyyy/dd/mmmm',
				"mask18":'dd/yy/mmm',
				"mask19":'dd/mmm/yy',
				"mask20":'mmm/dd/yy',
				"mask21":'mmm/yy/dd',
				"mask22":'yy/mmm/dd',
				"mask23":'yy/dd/mmm',
				"mask24":'dd/yy/mm',
				"mask25":'dd/mm/yy',
				"mask26":'mm/dd/yy',
				"mask27":'mm/yy/dd',
				"mask28":'yy/mm/dd',
				"mask29":'yy/dd/mm',
				"mask30":'dd/yy/mmmm',
				"mask31":'dd/mmmm/yy',
				"mask32":'mmmm/dd/yy',
				"mask33":'mmmm/yy/dd',
				"mask34":'yy/mmmm/dd',
				"mask35":'yy/dd/mmmm'
			};
			
			//JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
			var opQuestionSorting = {
				<?=ordbAsc?>:"<?=translate("Ascending")?>",
				<?=ordbDsc?>:"<?=translate("Descending")?>"
			}
			//JAPR
			
			var strOptTabName = "advTab";
			//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			var opSectionTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataSourceFilter"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofInlineSection?>:{name:"<?=translate("From the responses of an Inline section")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceSectionID"}]
					}
				}
			};
			
			var strOptTabName = "advTab";
			//JAPR 2015-08-19: Agregada la configuración para cambiar el tipo de despliegue de las preguntas
			//Estas opciones aplican para múltiple choice y para simple choice cuando no son de catálogo
			//RV Ene2017: Se agregaron las opciones de Ancho y Alto para que se muestren en despliegue vertical y horizontal
			//ya que al agregar el código para que estuvieran ocultas en el despliegue tipo Menú si se cambiaba de Menú a Horizontal 
			//o Vertical estos campos no se desplegaban dinámicamente
			var opQuestionDisplayMode = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"}
						]
					}
				},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				},
				<?=dspAutocomplete?>: {name:"<?=translate("Autocomplete")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				}
			}
			
			//RV Ene2017: Es necesario utilizar otro arreglo de visibilidad para las multiples choices para indicar 
			//que en el despliegue Vertical y Horizontal se deben visualizar los campos Tipo de Captura en Opción,
			//Ancho y Alto. No se realizó el cambio necesario para multiple choice sobre el arreglo opQuestionDisplayMode por que este se utiliza también
			//para las preguntas simple choice y esto provocaba que en simple choice se visualizara también el campo
			//Tipo de captura en opción el cual no debe desplegarse en simple choice.
			var opMCHQuestionDisplayMode = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false},
							{tabId:strOptTabName, fieldId:"mcInputType"},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false},
							{tabId:strOptTabName, fieldId:"mcInputType"},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"}
							
						]
					}
				},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				},
				<?=dspAutocomplete?>: {name:"<?=translate("Autocomplete")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				}
			}
			
			//OMMC 2016-04-22: Agregados los tipos de despliegue para los menús
			var opMenuDisplayMode = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				}
			}
			
			//OMMC 2016-04-22: Agregados los tipos de despliegue para los menús con catálogo
			var opMenuDisplayModeCatalog = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"catMembersList"}
						]
					}
				}
			}
			
			
			//OMMC 2015-11-25: Agregada la configuracion para cambiar el tipo de despliegue de preguntas MChoice cuando están en tabla
			//Ya que había comportamientos erráticos con los componentes de jQuery, las preguntas MChoice tipo menú y autocomplete se despligan como modo vertical
			//Sin embargo, se pueden cambiar entre horizontal y vertical mientras estén en secciones tabla.
			var opQuestionDisplayModeTable = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID", show:false},
							{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID", show:false}
						]
					}
				}
			}
			
			//Estas opciones aplican para simple choice de catálogo
			//JAPR 2016-01-19: Habilitado el tipo de despliegue vertical/horizontal para preguntas simple choice de catálogo (un sólo atributo) (#2YCZE0)
			//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			var opSCHCatQuestionDisplayMode = {
				<?=dspVertical?>: {name:"<?=translate("Vertical")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				<?=dspHorizontal?>: {name:"<?=translate("Horizontal")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							//{tabId:strOptTabName, fieldId:"catMembersList", show:false},
							{tabId:strOptTabName, fieldId:"canvasWidth"},
							{tabId:strOptTabName, fieldId:"canvasHeight"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				<?=dspMenu?>: {name:"<?=translate("Menu")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"catMembersList"}
						]
					}
				},
				<?=dspAutocomplete?>: {name:"<?=translate("Autocomplete")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"}
						]
					}
				},
				//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
				//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				//JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				<?=dspMetro?>: {name:"<?=translate("Metro")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"catMembersList"},
							{tabId:strOptTabName, fieldId:"dataMemberImageID"},
							{tabId:strOptTabName, fieldId:"dataMemberOrderID"},
							{tabId:strOptTabName, fieldId:"dataMemberOrderDir"},
							{tabId:strOptTabName, fieldId:"customLayout"}
						]
					}
				},
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				<?=dspAR?>: {name:"<?=translate("AR")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberZipFileID"},
							{tabId:strOptTabName, fieldId:"dataMemberVersionID"},
							{tabId:strOptTabName, fieldId:"dataMemberPlatformID"},
							{tabId:strOptTabName, fieldId:"dataMemberTitleID"}
						]
					}
				},
			}
			
			//JAPR 2016-04-22: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
			//Las preguntas de tipo múltiple choice de catálogo se configuran igual que las simple choice excepto por las tipo menú, ya que para múltiple choice sólo hay un atributo
			var opMCHCatQuestionDisplayMode = JSON.parse(JSON.stringify(opSCHCatQuestionDisplayMode));
			if (opMCHCatQuestionDisplayMode) {
				if (opMCHCatQuestionDisplayMode[DisplayMode.dspMenu] && opMCHCatQuestionDisplayMode[DisplayMode.dspMenu]['visibilityRules'] && opMCHCatQuestionDisplayMode[DisplayMode.dspMenu]['visibilityRules']['fields']) {
					if (opMCHCatQuestionDisplayMode[DisplayMode.dspMenu]['visibilityRules']['fields'][1]) {
						opMCHCatQuestionDisplayMode[DisplayMode.dspMenu]['visibilityRules']['fields'][1] = {tabId:strOptTabName, fieldId:"dataSourceMemberID"};
					}
				}
				//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
				//JAPR 2016-05-30: Corregido un bug, al eliminar esta opción ya no se permitían preguntas de catálogo Menú para MChoice (#VOPSK0)
				//delete opMCHCatQuestionDisplayMode[DisplayMode.dspMenu];
				//JAPR 2016-05-10: Corregido un bug, estaba dejando el tipo de despliegue Metro en preguntas múltiple choice
				delete opMCHCatQuestionDisplayMode[DisplayMode.dspMetro];
			}
			
			//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
			var opAgFunctions = {
				"SUM":"SUM",
				"AVG":"AVG",
				"MAX":"MAX",
				"MIN":"MIN",
				"COUNT":"COUNT"
			}
			//JAPR
			
			var opQuestionMCInputType = {
				<?=mpcCheckBox?>: {name:"<?=translate("None")?>"},
				<?=mpcNumeric?>: {name:"<?=translate("Numeric")?>"},
				<?=mpcText?>: {name:"<?=translate("Text")?>"}
			}
			
			//JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas numéricas
			var opQuestionKeyboardType = {
				<?=kybNumeric?>: {name:"<?=translate("Numeric keypad")?>"},
				<?=kybTelephone?>: {name:"<?=translate("Telephone keypad")?>"}
			}
			
			//OMMC 2016-04-22: Agregado para las preguntas tipo menu
			var opQuestionTypeOfFillingRealMenu = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				}/*,
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"catMembersList"},
							//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
							{tabId:strOptTabName, fieldId:"dataSourceFilter"}
						]
					}
				}*/
			}
			
			//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			var opQuestionTypeOfFillingMenu = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"},
							{tabId:strOptTabName, fieldId:"allowAdditionalAnswers"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"catMembersList"},
							//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
							{tabId:strOptTabName, fieldId:"dataSourceFilter"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				}
			};
			
			var opQuestionTypeOfFillingMap = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"}],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataMemberLatitudeID"},
							{tabId:strOptTabName, fieldId:"dataMemberLongitudeID"},
							//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
							{tabId:strOptTabName, fieldId:"dataSourceFilter"},
							//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"}]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"}]
					}
				}
			};
			
			var opQuestionTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"},
							{tabId:strOptTabName, fieldId:"allowAdditionalAnswers"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						],
						tabs:["valuesTab"]
					}
				},
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							//JAPR 2016-06-07: Corregido el despliegue de este campo para que sólo aplique si es de catálogo (#VZ88H6)
							{tabId:strOptTabName, fieldId:"dataMemberImageID"},
							//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
							{tabId:strOptTabName, fieldId:"dataSourceFilter"}
						],
						tabs:["skipRulesTab"]
					}
				},
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				}
			};
			
			var opMultiTypeOfFilling = {
				<?=tofFixedAnswers?>:{name:"<?=translate("User input")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"strSelectOptions"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						],
						tabs:["valuesTab"]
					}
				},
				//JAPR 2016-04-21: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
				<?=tofCatalog?>:{name:"<?=translate("Catalog")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"dataSourceID"},
							{tabId:strOptTabName, fieldId:"dataSourceMemberID"},
							{tabId:strOptTabName, fieldId:"dataSourceFilter"},
							//JAPR 2016-09-02: Corregido un bug, no estaba mostrando/ocultando esta configuración correctamente para MChoice (#VOPSK0)
							{tabId:strOptTabName, fieldId:"dataMemberImageID"}
						]
					}
				},
				//JAPR
				<?=tofMCHQuestion?>:{name:"<?=translate("From another multiple choice's answers")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sourceQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				},
				<?=tofSharedAnswers?>:{name:"<?=translate("Share with another question")?>", visibilityRules:{
						fields:[{tabId:strOptTabName, fieldId:"sharedQuestionID"},
							{tabId:strOptTabName, fieldId:"excludeFromQuestionID"}
						]
					}
				}
			};
			
			//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
			//@OMMC 2015/09/01 Variable de lenguaje para redactor
			var redactorLang = "<?=$_SESSION['PAuserLanguage']?>";
			if (redactorLang) {
				switch (redactorLang) {
					case "SP":
						redactorLang = "es";
						break;
					case 'ES':
						redactorLang = "es";
						break;
					case 'EN':
						redactorLang = "es";
						break;
					case 'BE':
						redactorLang = "es";
						break;
					default:
						redactorLang = "es";
						break;
				}
			}
			//JAPR
			
			var gc_ListOptionSelected = "#fff3a1";
			//True = Envía todas las propiedades del objeto sin importar cual cambió (el grabado puede ser en un botón luego de actualizar varias cosas)
			//False = Sólo se envía la propiedad que acaba de ser modificada (el grabado es en línea)
			var gbFullSave = false;
			var intDialogWidth = 380;
			var intDialogHeight = 350;
			var reqAjax = 1;
			var intMinCellWidth = 300;
			var intSurveyID = <?=$this->SurveyID?>;
			var giSectionID = 0;
			var giQuestionID = 0;
			var gsLastQGroupSelected = "";		//Indica cual fue el último grupo de tipos de pregunta del cual se creó uno o intentó crear, para abrir ese mismo la siguiente ocasión que se intente crear una pregunta
			var goErrors = new Array();
			//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
			var goObjectToDelete = undefined;	//Contiene la información para ejecutar nuevamente un proceso de borrado de algún elemento
			//Variables para funciones dinámicas
			var doCancelDragDropOperation;		//Utilizado cuando falla el Drag&Drop de una sección y/o pregunta para restaurarlas a su posición original
			
			//var strSkin = "skyblue";
			//var strSkin = "web";
			//Diálogos
			var objWindows;						//Referencia al componente que almacena todas las ventanas
			var objDialog;						//Diálogo modal común para cualquier proceso que requiera captura de algún valor específico (cada proceso lo volverá a definir)
			//Sólo para el editor de fórmulas
			var objWindowsFn;					//Referencia al componente que almacena la ventana del editor de Formulas
			var objDialogFn;					//Diálogo modal sólo para el editor de fórmulas
			//Sólo para el resize de imágenes
			var objWindowsImg;					//Referencia al componente que almacena la ventana del resize de imágenes
			var objDialogImg;					//Diálogo modal sólo para la ventana del resize de imágenes
			//Para el editor HTML
			var objWindowsEd;					//Referencia al componente que almacena la ventana del resize de imágenes
			var objDialogEd;					//Diálogo modal sólo para la ventana del resize de imágenes
			//Forms
			var objUsersForm;					//Ventana para asociar los usuarios a la Forma
			var objUserGroupsForm;				//Ventana para asociar los grupos de usuarios a la Forma
			//TabBars
			var objFormTabs;					//Cejillas de las configuraciones de la forma
			var tabDesign = "form";
			var tabUsers = "users";
			var tabGroups = "usergroups";
			var tabDataDestinations = "datadestinations";
			var tabModel = "model";
			var tabDetail = "details";
			var tabOnlineEntry = "onlineEntry";
			var tabCopyForm = "copyform";
			var tabDevForm = "devForm";
			var copyAction = <?=copyAction?>;
			var devAction = <?=devAction?>;
			var formProduction = <?=formProduction?>;
			var formDevelopment = <?=formDevelopment?>;
			var formProdWithDev = <?=formProdWithDev?>;
			
			var objPropsTabs;					//Cejillas de la ventana de propiedades
			//Ribbons
			//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
			//var objNewQuestionRib;				//Ribbon para seleccionar los tipos de preguntas
			//Accordions
			var objNewQuestionsAcc;				//Selector de la clasificación de preguntas
			//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
			//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
			//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
			var objQTypeOpenDataView;
			var objQTypeSimpleDataView;
			var objQTypeMultiDataView;
			var objQTypeMediaDataView;
			var objQTypeOtherDataView;
			//LayOuts							//Opciones principales de la forma
			var objMainLayout;					//LayOut que se agregó para separar la tabbar con el contenido de la página de un header HTML que represente a las Tabs
			var objFormsLayout;					//LayOut principal de la forma, contiene a los demás
			var objTreeLayout;					//LayOut de secciones, contiene el DataView de secciones y anteriormente el arbol completo de la forma
			var objNewSectionLayOut;			//LayOut para seleccionar el tipo de sección a agregar
			var objPropsLayout;					//Layout para mostrar el detalle de las propiedades de un objeto
			//Toolbars
			var objSectionsToolbar;					//Toolbar con las opciones para manipular secciones
			var objQuestionsToolbar;			//Toolbar con las opciones para manipular las preguntas
			//PopUps
			var objMousePopUp;					//PopUp para usar con el movimiento del mouse
			var objSectionPopUp;				//PopUp para seleccionar los tipos de secciones
			var objQuestionPopUp;				//PopUp para seleccionar los tipos de preguntas
			//DataViews
			var objSectionsDataView;			//DataView para selecionar la sección desplegada
			//JAPR 2015-05-22: Se agrega nuevamente el DataView de secciones y ahora una toolbar con tipos de preguntas
			//Celdas del LayOut principal de la página de diseño
			//var dhxHeaderCell = "a";
			var dhxBodyCell = "a";
			//Celdas del LayOut principal de la forma
			var dhxLayOutCell = "a";
			var dhxPreviewCell = "b";
			var dhxPropsCell = "c";
			//Celdas del Layout de propiedades
			var dhxPropsDetailCell = "a";
			
			var dhxTreeCell = "a";
			//var dhxQTypeIDsCell = "c";
			var objTabsColl = new Object();			//Array con la definición de campos para el objeto del cual se está mostrando el detalle (siempre contiene el último desplegado en la celda de propiedades)
			var objSurveyTabsColl = new Object();	//Array con la definición de campos para la forma (se separa ya que esta no está en la celda de propiedades, así que los métodos como getFieldDefinition no pueden usar objTabsColl)
			var intPropsTBPos;
			//JAPR
			var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
			var selSurvey = objFormsDefs.data.surveyList[intSurveyID];
			var objArrPopUpsByTime = new Object();
			var timePopup="";
			//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
			var Templates = new Object();				//Guarda el nombre de los templates disponibles
			var TemplatesOrder = new Array();
			var arrCatalogs = new Object();
			var arrCatalogsOrder = new Array();
			var arrAttributes = new Object();
			var arrDataDestinations = new Object();		//Colección de destinos de datos para preguntas UpdateData
			var arrDataDestinationsOrder = new Array();
			
			//JAPR 2015-10-05: Removida la opción de iPadMini ya que es idéntica a iPad realmente (#YC8H1Y)
			var objDevicesColl = {
				"Default": {name: "Browser", pos:0, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["Default"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["Default"]))?>", image:"browserwhite.png", label:"<?=translate("Browser")?>"},
				"iPod": {name:"iPod", pos:1, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["iPod"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["iPod"]))?>", image:"ipodwhite.png"},
				"iPad": {name:"iPad", pos:2, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["iPad"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["iPad"]))?>", image:"ipadwhite.png"},
				//"iPadMini": {name:"iPadMini", pos:3, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["iPadMini"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["iPadMini"]))?>", image:"ipadwhite.png"},
				"iPhone": {name:"iPhone", pos:3, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["iPhone"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["iPhone"]))?>", image:"iphonewhite.png"},
				"Cel": {name:"AndroidCel", pos:4, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["Cel"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["Cel"]))?>", image:"celwhite.png"},
				"Tablet": {name:"AndroidTab", pos:5, width: "<?=trim(str_ireplace("px", "", (string) @$widthByDisp["Tablet"]))?>", height: "<?=trim(str_ireplace("px", "", (string) @$heightByDisp["Tablet"]))?>", image:"tabletwhite.png"}
			};
			var objOrientationsColl = {
				"Portrait": {},
				"Landscape": {}
			};
			var objTreeState = {};		//Contiene el estado de los nodos del árbol antes de invocar al método de pintado para restaurarlo en el mismo estado
			var arrQTypeIDs = new Object();
<?
			foreach ($arrQTypeIDs as $intQTypeID => $strQTypeID) {
?>
			arrQTypeIDs[<?=$intQTypeID?>] = "<?=$strQTypeID?>";
<?
			}
?>
			
			//GCRUZ 2015-10-16. Establecer el orden de los usuarios y grupos
			var arrUsers = new Object();
			var arrUsersOrder = [];
<?
			//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
			$intUserID = $_SESSION["PABITAM_UserID"];
			$theAppUser = null;
			//@JAPR
			$i=0;
			foreach ($objUserColl as $objAppUser) {
				echo("\t\t\t"."arrUsers[{$objAppUser->CLA_USUARIO}] = {id:{$objAppUser->CLA_USUARIO}, name:'".addslashes($objAppUser->Email)."'};\r\n");
				echo("\t\t\t"."arrUsersOrder[{$i}] = {$objAppUser->CLA_USUARIO};\r\n");
				$i++;
				
				//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
				if ( $objAppUser->UserID == $intUserID ) {
					$theAppUser = $objAppUser;
				}
				//@JAPR
			}
			
			//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
			//Carga el array de colores personalizados
			if ( $theAppUser ) {
				$arrCustomColors = $theAppUser->getCustomColorsArray();
				if ( is_null($arrCustomColors) ) {
					//En este caso no existen grabados colores personalizados, se dejarán los default del componente
?>
			var arrCustomizedColors = null;
<?
				}
				else {
					//En este caso genera el array de colores personalizados basados en el usuario que diseña
?>
			var arrCustomizedColors = new Array();
<?
					$i = 0;
					foreach($arrCustomColors as $strCustomColor) {
						echo("\t\t\t"."arrCustomizedColors[{$i}] = '".((string) $strCustomColor)."';\r\n");
						$i++;
					}
				}
			}
			//@JAPR
?>
			var arrRoles = new Object();
			var arrRolesOrder = [];
<?
			$i=0;
			foreach ($objUserGroupsColl as $objRole) {
				echo("\t\t\t"."arrRoles[{$objRole->UserGroupID}] = {id:{$objRole->UserGroupID}, name:'".addslashes($objRole->UserGroupName)."'};\r\n");
				echo("\t\t\t"."arrRolesOrder[{$i}] = {$objRole->UserGroupID};\r\n");
				$i++;
			}
?>
			var arrSelUsers = new Object();
			var arrSelRoles = new Object();
<?
			if ($objSurveyScheduler) {
				foreach ($objSurveyScheduler->UserIDs as $intUserID) {
					echo("\t\t\t"."arrSelUsers[{$intUserID}] = {$intUserID};\r\n");
				}
				foreach ($objSurveyScheduler->UsrRoles as $intUserGroupID) {
					echo("\t\t\t"."arrSelRoles[{$intUserGroupID}] = {$intUserGroupID};\r\n");
				}
			}
?>
			
<?
			//@JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
			foreach ($objDataSourceColl as $objDataSource) {
				$intCatalogID = $objDataSource->DataSourceID;
				echo("\t\t\t"."arrCatalogsOrder.push({$intCatalogID});\r\n");
				/*echo("\t\t\t"."arrCatalogs[{$intCatalogID}] = {id:{$intCatalogID}, name:'".addslashes($objDataSource->DataSourceName)."', childrenOrder:[], children:{}};\r\n");*/
				//ears 2017-01-13 validación de caracteres especiales	
				echo("\t\t\t"."arrCatalogs[{$intCatalogID}] = {id:{$intCatalogID}, name:'".GetValidJScriptText($objDataSource->DataSourceName,array("fullhtmlspecialchars" => 1))."', childrenOrder:[], children:{}};\r\n");
				$objDataSourceMembersColl = @$objAttributesByDataSource[$intCatalogID];
				if (!is_null($objDataSourceMembersColl)) {
					foreach ($objDataSourceMembersColl as $objCatMember) {
						$intMemberID = $objCatMember->MemberID;
						//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
						/*echo("\t\t\t"."arrAttributes[{$intMemberID}] = {id:{$intMemberID}, name:'".addslashes($objCatMember->MemberName)."', isImage:".(($objCatMember->IsImage)?1:0)."};\r\n");*/
						//ears 2017-01-13 validación de caracteres especiales	
						echo("\t\t\t"."arrAttributes[{$intMemberID}] = {id:{$intMemberID}, name:'".GetValidJScriptText($objCatMember->MemberName,array("fullhtmlspecialchars" => 1))."', isImage:".(($objCatMember->IsImage)?1:0)."};\r\n");
						//JAPR
						echo("\t\t\t"."arrCatalogs[{$intCatalogID}].childrenOrder.push({$intMemberID});");
						echo("\t\t\t"."arrCatalogs[{$intCatalogID}].children[{$intMemberID}] = arrAttributes[{$intMemberID}];");
					}
				}
			}
			
			//@JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
			if (getMDVersion() >= esvTemplateStyles) {
				$objColorTemplatesColl = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
				if (!is_null($objColorTemplatesColl)) {
					foreach($objColorTemplatesColl->Collection as $objColorTemplate) {
						//@JAPR 2018-11-09: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
				Templates[<?=$objColorTemplate->TemplateID?>] = "<?=GetValidJScriptText($objColorTemplate->TemplateName)?>";
				TemplatesOrder.push(<?=$objColorTemplate->TemplateID?>);
<?
					}
				}
			}
			
			//@JAPR 2016-03-31: Agregado el tipo de pregunta Update Data
			if (getMDVersion() >= esvUpdateData) {
				$objDataDestinationColl = BITAMeFormsDataDestinationsCollection::NewInstance($this->Repository, $this->SurveyID);
				if (!is_null($objDataDestinationColl)) {
					foreach($objDataDestinationColl->Collection as $objDataDestination) {
						//@JAPR 2016-04-07: Corregido un bug, se estaban permitiendo seleccionar destinos de tipos que no eran eBavel
						if ($objDataDestination->type != ddeseBavel) {
							continue;
						}
						//@JAPR
						
						echo("\t\t\t"."arrDataDestinationsOrder.push({$objDataDestination->id});\r\n");
						echo("\t\t\t"."arrDataDestinations[{$objDataDestination->id}] = {id:{$objDataDestination->id}, name:'".GetValidJScriptText($objDataDestination->name,array("fullhtmlspecialchars" => 1))."'};\r\n");
					}
				}
			}
			//@JAPR
?>
			
			//JAPR 2015-07-30: Agregados botones para seleccionar/deseleccionar todos los elementos
			/* Selecciona todos los elementos (bCheck=true) o ninguno (bCheck=false) del componente CList indicado en el primer parámetro
			*/
			function fnSelectAllCListItems(oCList, bCheck) {
				if (!oCList) {
					return;
				}
				
				$(oCList).find(':checkbox').prop('checked', (bCheck?true:false));
			}
			
			/* Actualiza el valor de campos hijos del que invoca esta función cuando cambia algún valor, de esa manera se puede refrescar el componente de los hijos según el nuevo
			valor del campo padre
			*/
			function fnSetChildrenValues() {
				if (!this.children || !$.isArray(this.children)) {
					return;
				}
				
				for (var intIdx in this.children) {
					var strFieldName = this.children[intIdx];
					if (strFieldName) {
						var blnForForm = (this.parentType == AdminObjectType.otySurvey);
						var objChildField = getFieldDefinition(this.tab, strFieldName, blnForForm);
						if (!objChildField || !objChildField.refreshValue) {
							continue;
						}
						
						objChildField.refreshValue();
					}
				}
			}
			
			/*@OMMC 2015/09/04: Se agrega funcionalidad para agregar fórmulas al editor HTML*/
			/* Abre el editor de fórmula con el texto por default sText, y sobreescribe el objeto oObj al terminar si no se cancela la edición
			//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
			*/
			function fnOpenFormulaEditor(oObj, sText, iDataSourceID, htmlRange, sourceID, sourceType) {
				var intDialogWidth = 800;
				var intDialogHeight = 575;
				objDialogFn = objWindowsFn.createWindow({
					id:"editFormula",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogFn.setText("<?=translate("Edit formula")?>");
				objDialogFn.denyPark();
				objDialogFn.denyResize();
				objDialogFn.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialogFn.attachEvent("onClose", function() {
					return true;
				});
				
				//Para permitir accesar a la celda que se recibió como parámetro, se tuvo que agregar una propiedad dinámica al diálogo, ya que se perdía la referencia dentro del
				//evento onContentLoaded
				objDialogFn.bitCellObj = oObj;
				
				//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
				if (iDataSourceID === undefined || !$.isNumeric(iDataSourceID)) {
					iDataSourceID = 0;
				}
				//Entrada por editor HTML
				if(htmlRange != undefined){
					objDialogFn.attachURL("formulaEditor.php?SurveyID=" + intSurveyID + "&aTextEditor=" + encodeURIComponent('') + "&DataSourceID=" + iDataSourceID);
				}else{
					objDialogFn.attachURL("formulaEditor.php?SurveyID=" + intSurveyID + "&aTextEditor=" + encodeURIComponent(sText) + "&DataSourceID=" + iDataSourceID + "&sourceID=" + sourceID + "&sourceType=" + sourceType);
				}
				
				objDialogFn.attachEvent("onContentLoaded", function(win) {
					var objFrame = objDialogFn.getFrame();
					if (objFrame && objFrame.contentWindow && objFrame.contentWindow.document) {
						var objButton = $(objDialogFn.getFrame().contentWindow.document).find("#closeButton");
						if (objButton && objButton.length) {
							//objButton.attr('click', '');
							objButton.on('click', function() {
								if(this.getAttribute('allowClose') == 'false'){
									return;
								}
								var strValue = '';
								if (objDialogFn.bitCellObj) {
									try {
										strValue = objFrame.contentWindow.sParamReturn;
										//Si es un editor HTML el que llama al editor de Fórmulas
										if(htmlRange != undefined){
											//Obtiene el texto que se va a agregar y se lo concatena al final.
											var htmlValue = objDialogFn.bitCellObj[0].value;
											htmlValue = htmlValue.concat(strValue);
											objDialogFn.bitCellObj[0].value = htmlValue;
										}else{
											//Para un input dentro de un componente GFieldTypes.formula de un grid de propiedades
											if (objDialogFn.bitCellObj.childNodes[1]) {
												objDialogFn.bitCellObj.childNodes[1].value = strValue;
											}
											else {
												//Para un input dentro de un div cuando es su primer elemento hijo
												if (objDialogFn.bitCellObj.childNodes[0]) {
													objDialogFn.bitCellObj.childNodes[0].value = strValue;
												}
												else {
													//Para un input directo (por ejemplo, en el diálogo de preguntas, el input de la fórmula)
													objDialogFn.bitCellObj.value = strValue;
													$(objDialogFn.bitCellObj).focus();
												}
											}
										}
									} catch(e) {
										console.log("Error updating the formula : " + e);
									}
								}
								
								setTimeout(function () {
									var strRowId = "";
									var objGrid = undefined;
									if (objDialogFn.bitCellObj && objDialogFn.bitCellObj.parentNode && objDialogFn.bitCellObj.parentNode.grid) {
										var strRowId = objDialogFn.bitCellObj.parentNode.idd;
										objGrid = objDialogFn.bitCellObj.parentNode.grid;
									}
									
									if (objGrid) {
										//Por la manera en que funcionan el grid, al aceptar la fórmula se va a forzar un editStop, esto automáticamente cancelaría el evento blur porque
										//el input que ya no tenía el foco de todas maneras no habría detectado el evento keydown con <enter> así que no se consideraría pérdida de foco,
										//por el contrario se hará el grabado directamente en este punto. En resumen, NO se pondrá el foco en el input para delegarle el grabado a él, sino
										//que cancelará el modo edición y se grabará aquí mismo
										//$(objDialogFn.bitCellObj).find("input:first").focus();
										doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strValue);
										//Se envía el parámetro en true, porque de lo contrario el grid revierte el valor a lo que él consideraba el último "grabado"
										objGrid.editStop(true);
									}
									objDialogFn.bitCellObj = undefined;
									//OMMC 2015/09/08: Si el editor de fórmulas fue llamado del editor HTML, antes de cerrarse, regresa el foco al editor HTML lo que permite al valor refrescarse.
									//OMMC 2015/10/15: Validación modificada.
									if(htmlRange != undefined){
										doUnloadDialogFn();
										var redactorArea = $(htmlRange.startContainer).parent('div').last();
										var areaCount = redactorArea.find('div').length;
										if(areaCount && areaCount > 0){
											redactorArea = redactorArea.find(".redactor-editor").focus();
										}else{
											redactorArea.focus();
										}
										//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
										if (objDialogEd) {
											objDialogEd.bringToTop();
										}
										//JAPR
									}else{
										doUnloadDialogFn();
									}
									
								}, 100);
							});
						}
					}
				});
			}
			
			function fnOpenEditorHTML(oObj, sText, qTypeID) {
				var intDialogWidth = 690;
				var intDialogHeight = 480;
				objDialogEd = objWindowsEd.createWindow({
					id:"editHTML",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogEd.setText("<?=translate("Editor")?>");
				objDialogEd.denyPark();
				objDialogEd.denyResize();
				objDialogEd.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialogEd.attachEvent("onClose", function() {
					return true;
				});
				
				//Para permitir accesar a la celda que se recibió como parámetro, se tuvo que agregar una propiedad dinámica al diálogo, ya que se perdía la referencia dentro del
				//evento onContentLoaded
				objDialogEd.bitCellObj = oObj;
					
				/*
				//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
				if (iDataSourceID === undefined || !$.isNumeric(iDataSourceID)) {
					iDataSourceID = 0;
				}
				*/
				//OMMC 2016-01-19: Se obtiene la referencia del objeto que se va a editar.
				//Puede ser header o footer de sección o pregunta HTML
				//OMMC 2016-01-20: Agregado el caso para cuando el editor HTML se manda llamar desde la creación de la pregunta.
				//El parámetro se envía como 0 para que no haya conflicto (el estado del editor no se guarda ya que no existe la pregunta en la base de datos).
				//Ya que se unificaron los editores para las preguntas mensaje y HTML, se envía como parámetro el tipo de pregunta que es.
				//Esto le dice al editorHTML que oculte o no el ícono de HTML.
				//OMMC 2016-05-01: Cambiado a que los parametros que se envían al redactor sean por POST
				if(oObj.parentNode.grid == undefined){
					if(qTypeID == QTypesExt.html){
						objDialogEd.attachURL("editorHTML.php", null, {aTextEditor: sText, HTMLEditorState: "0"});
					}else{
						objDialogEd.attachURL("editorHTML.php", null, {aTextEditor: sText, HTMLEditorState: "0", qTypeID: "message"});
					}
				}else{
					var aElement = getObject(oObj.parentNode.grid.bitObjectType, oObj.parentNode.grid.bitObjectID);
					if(aElement){
						var editorState;
						switch(aElement.objectType){
							case AdminObjectType.otySection:
								if(oObj.parentNode.grid.bitLastSelectedRowId == 'htmlHeaderDes'){
									editorState = aElement.HTMLHEditorState;
								}else if(oObj.parentNode.grid.bitLastSelectedRowId == 'htmlFooterDes'){
									editorState = aElement.HTMLFEditorState;
								}
							break;
							case AdminObjectType.otyQuestion:
								if(qTypeID == QTypesExt.html){
									editorState = aElement.HTMLEditorState;
								}
								else {
									//JAPR 2016-05-10: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
									if (qTypeID == QTypesExt.simpleMetro) {
										editorState = aElement.HTMLEditorState;
									}
								}
							break;
						}
						if(editorState != undefined){
							objDialogEd.attachURL("editorHTML.php", null, {aTextEditor: sText, HTMLEditorState: editorState});
						}else{
							//OMMC 2016-04-14: Corregido para que aparezca el botón de HTML en diseño de HTML para forma
							//JAPR 2016-05-10: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
							var blnAllowHTML = false;
							switch (aElement.objectType) {
								case AdminObjectType.otySurvey:
									blnAllowHTML = true;
									break;
								case AdminObjectType.otyQuestion:
									switch (qTypeID) {
										case QTypesExt.simpleMetro:
										case QTypesExt.html:
											blnAllowHTML = true;
											break;
									}
									break;
							}
							if(blnAllowHTML){
								objDialogEd.attachURL("editorHTML.php", null, {aTextEditor: sText, HTMLEditorState: editorState});
							}else{
								objDialogEd.attachURL("editorHTML.php", null, {aTextEditor: sText, HTMLEditorState: "0", qTypeID: "message"});
							}
						}
					}
				}
				objDialogEd.attachEvent("onContentLoaded", function(win) {
					var objFrame = objDialogEd.getFrame();
					if (objFrame && objFrame.contentWindow && objFrame.contentWindow.document) {
						var objButton = $(objDialogEd.getFrame().contentWindow.document).find("#closeButton");
						if (objButton && objButton.length) {
							//objButton.attr('click', '');
							objButton.on('click', function() {
								var strValue = '';
								var intState = 0;
								
								//OMMC 2015-01-19: Validado el estado del editor HTML antes de cerrar la ventana.
								if(objFrame.contentWindow.aState){
									intState = parseInt(objFrame.contentWindow.aState);
								}
								
								//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
								strValue = objFrame.contentWindow.sParamReturn;
								
								if ($.trim(strValue) == '') {
									var strValue = "<?=translate("You must enter a value for this field")?>";
									alert(strValue);
									return;
								}
								//JAPR
								
								if (objDialogEd.bitCellObj) {
									try {
										//Para un input dentro de un componente GFieldTypes.formula de un grid de propiedades
										if (objDialogEd.bitCellObj.childNodes[1]) {
											objDialogEd.bitCellObj.childNodes[1].value = strValue;
										}
										else {
											//Para un input dentro de un div cuando es su primer elemento hijo
											if (objDialogEd.bitCellObj.childNodes[0]) {
												objDialogEd.bitCellObj.childNodes[0].value = strValue;
											}
											else {
												//Para un input directo (por ejemplo, en el diálogo de preguntas, el input de la fórmula)
												objDialogEd.bitCellObj.value = strValue;
												$(objDialogEd.bitCellObj).focus();
											}
										}
									} catch(e) {
										console.log("Error updating the formula : " + e);
									}
								}
								
								setTimeout(function () {
									var strRowId = "";
									var objGrid = undefined;
									if (objDialogEd.bitCellObj && objDialogEd.bitCellObj.parentNode && objDialogEd.bitCellObj.parentNode.grid) {
										var strRowId = objDialogEd.bitCellObj.parentNode.idd;
										objGrid = objDialogEd.bitCellObj.parentNode.grid;
									}
									
									if (objGrid) {
										//OMMC 2016-01-19: Se actualiza el estado de el editor HTML.
										var aElement = getObject(objGrid.bitObjectType, objGrid.bitObjectID);
										if(aElement){
											switch(aElement.objectType){
												case AdminObjectType.otySection:
													if(objGrid.bitLastSelectedRowId == 'htmlHeaderDes'){
														aElement.HTMLHEditorState = intState;
													}else if(objGrid.bitLastSelectedRowId == 'htmlFooterDes'){
														aElement.HTMLFEditorState = intState;
													}
												break;
												case AdminObjectType.otyQuestion:
													switch(aElement.type){
														case Type.message:
															aElement.HTMLEditorState = intState;
															break;
														case Type.simplechoice:
															//JAPR 2016-05-10: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
															if (aElement.displayMode == DisplayMode.dspMetro) {
																aElement.HTMLEditorState = intState;
															}
															break;
													}
												break;
											}
										}
										//Por la manera en que funcionan el grid, al aceptar la fórmula se va a forzar un editStop, esto automáticamente cancelaría el evento blur porque
										//el input que ya no tenía el foco de todas maneras no habría detectado el evento keydown con <enter> así que no se consideraría pérdida de foco,
										//por el contrario se hará el grabado directamente en este punto. En resumen, NO se pondrá el foco en el input para delegarle el grabado a él, sino
										//que cancelará el modo edición y se grabará aquí mismo
										//$(objDialogEd.bitCellObj).find("input:first").focus();
										//console.log("doUpdateProperty:"+strValue);
										//debugger;
										doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strValue);
										//Se envía el parámetro en true, porque de lo contrario el grid revierte el valor a lo que él consideraba el último "grabado"
										//objGrid.editStop(true);
									}
									objDialogEd.bitCellObj = undefined;
									doUnloadDialogEd();
								}, 100);
							});
						}
					}
				});
			}
			
			//Prepara una función para mandar el option y que se ejecute el proceso correspodiente de show/Hide
			//var fnShowHideItems = function(oOption, bShow) {
			function fnShowHideItems(oOption, bShow) {
				if (!oOption) {
					return;
				}
				
				if (oOption && oOption.visibilityRules) {
					var objVisibilityRules = oOption.visibilityRules;
					//Oculta las tabs indicadas
					if (objVisibilityRules.tabs && objVisibilityRules.tabs.length) {
						for (var intIndex in objVisibilityRules.tabs) {
							var strTabId = objVisibilityRules.tabs[intIndex];
							if (strTabId && objPropsTabs.tabs(strTabId)) {
								try {
									if (bShow) {
										objPropsTabs.tabs(strTabId).show();
									}
									else {
										objPropsTabs.tabs(strTabId).hide();
									}
								} catch(e) {};
							}
						}
					}
					
					//Oculta los campos indicados
					if (objVisibilityRules.fields && objVisibilityRules.fields.length) {
						for (var intIndex in objVisibilityRules.fields) {
							var objHideField = objVisibilityRules.fields[intIndex];
							var strTabId = objHideField['tabId'];
							var strFieldId = objHideField['fieldId'];
							//JAPR 2015-08-19: Agregada la propiedad show a los campos de visibilidad, si es undefined o true se muestra, si no entonces se ocultará
							var blnShow = objHideField['show'];
							blnShow = (blnShow === undefined || blnShow);
							//JAPR
							try {
								var objTempGrid = undefined;
								if (objPropsTabs.tabs(strTabId)) {
									objTempGrid = objPropsTabs.tabs(strTabId).getAttachedObject();
								}
								
								if (objTempGrid && strFieldId) {
									//JAPR 2015-08-19: Agregada la propiedad show a los campos de visibilidad, si es undefined o true se muestra, si no entonces se ocultará
									objTempGrid.setRowHidden(strFieldId, ((bShow && blnShow)?false:true));
								}
							} catch(e) {};
						}
					}
				}
			}
						
			//******************************************************************************************************
			//Class definitions
			//******************************************************************************************************
			function GenericItemCls() {
			}
			GenericItemCls.prototype.getDataFields = function(oFields) {}
			
			//Regresa el arra POST con los datos genéricos que identifica el tipo de objeto del que se trata
			GenericItemCls.prototype.getObjFields = function() {
				var objParams = {
					SurveyID:intSurveyID,
					Design:1,
					RequestType:reqAjax,
					Process:"Edit",
					ObjectType:this.objectType
				};
				
				//Agrega el campo ID específico del objeto. Siempre debe existir una propiedad id correspondiente la instancia
				if (this.objectIDFieldName) {
					objParams[this.objectIDFieldName] = this.id;
				}
				//Agrega los campos padres del objeto. En este caso es una colección con la lista de padres en la forma {fieldName:propName, ...} y todos se deben
				//agregar a la lista de parámetros además de existir en la instancia con el nombre indicado
				if (this.parentFields) {
					for (var strFieldName in this.parentFields) {
						var strPropName = this.parentFields[strFieldName];
						if (strPropName && this[strPropName]) {
							objParams[strFieldName] = this[strPropName];
						}
					}
				}
				
				return objParams;
			}
			
			/* Invoca el método de reordenamiento de datos en el server, el cual en caso de haberse ejecutado correctamente, deberá hacer un refresh de definiciones
			y del previo para reflejar el cambio en la información. En caso de error, el proceso simplemente desactivará el icono de espera ya que el Drag&Drop se
			cancela antes de llegar aquí, así que no habría que cambiar nada realmente
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			GenericItemCls.prototype.reorder = function(oFields) {
				console.log("GenericItemCls.reorder");
				
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = this.getDataFields(oFields);
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				objParams["Process"] = "Reorder";
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					doReorderConfirmation(loader);
				});
			}
			
			/* A partir de la definición del objeto (el cual ya debería estar creado en el servidor), prepara el array de datos a enviar para actualizar su
			información mediante Ajax, además de reportar cualquier error ocurrido en el proceso
			La actualización de información se realizará dependiendo del método configurado con la variable gbFullSave
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			El parámetro oCallbackFn permite ejecutar dicha función al terminar el proceso
			*/
			GenericItemCls.prototype.save = function(oFields, oCallbackFn, bRemoveProgress) {
				console.log("GenericItemCls.save");
				
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = this.getDataFields(oFields);
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					var varValue = objParams[strProp];
					if ($.isArray(varValue) || $.isPlainObject(varValue)) {
						//En este caso el parámetro se genera también como array
						for (var intIndex in varValue) {
							strParams += strAnd + strProp + "[" + intIndex + "]=" + encodeURIComponent(varValue[intIndex]);
							strAnd = '&';
						}
					}
					else {
						strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					}
					strAnd = '&';
				}
				
				$('#divSaving').show();
				//JAPR 2015-08-10: Agregado el callback de la función de grabado
				if (oCallbackFn && typeof oCallbackFn == "function") {
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
					});
				}
				else {
					window.dhx4.ajax.post("processRequest.php", strParams, doSaveConfirmation);
				}
				//JAPR
			}
			
			/* Similar a la función save, a excepción que el parámetro oFields no representa propiedades de la instancia sino el conjunto de parámetros a enviar
			directamente, así que se puede usar para basar el request en un tipo de objeto pero mandando datos que no pertenecen a él (originalmente usado para
			invitar usuarios/grupos a una forma). El parámetro oFields puede sobreescribir las propiedades defalt de getObjFields
			El parámetro oCallbackFn permite ejecutar dicha función al terminar el proceso
			*/
			GenericItemCls.prototype.send = function(oFields, oCallbackFn, bRemoveProgress) {
				console.log("GenericItemCls.send");
			
				//Prepara los parámetros con los datos a grabar
				var objFieldsData = oFields;
				if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
					return;
				}
				
				var objParams = $.extend({}, this.getObjFields(), objFieldsData);
				if (!objParams) {
					return;
				}
				
				var strParams = '';
				var strAnd = '';
				for (var strProp in objParams) {
					strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
				
				$('#divSaving').show();
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
				});
			}
			
			/* Remueve todo tag HTML del nombre del objeto para regresar un nombre que sea presentable a los usuarios, en caso de que el nombre quedara vacío, intenta utilizar
			el shortName del objeto si es que tiene uno
			*/
			GenericItemCls.prototype.getName = function() {
				var strName = (this.nameHTMLDes)?this.nameHTMLDes:((this.nameHTML)?this.nameHTML:"");
				//JAPR 2015-12-11: Corregido un bug, se remueven las HTML Entities ya que se estaban desplegando en diferentes configuraciones o diálogos (#QHYOU7)
				strName = html_entity_decode($.trim(strName.replace(new RegExp("<[^>]*>", "gi"), '')));
				//JAPR
				
				if (!strName) {
					if (this.shortName) {
						strName = this.shortName;
					}
				}
				
				return strName;
			}
			
			//******************************************************************************************************
			SurveyCls.prototype = new GenericItemCls();
			SurveyCls.prototype.constructor = SurveyCls;
			function SurveyCls() {
				this.objectType = AdminObjectType.otySurvey;
			}
			
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			SurveyCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["name"]) {
					objParams["SurveyName"] = this.name;
				}
				if (blnAllFields || oFields["descrip"]) {
					objParams["SurveyDesc"] = this.descrip;
				}
				if (blnAllFields || oFields["hasSignature"]) {
					objParams["HasSignature"] = this.hasSignature;
				}
				if (blnAllFields || oFields["syncWhenSaving"]) {
					objParams["SyncWhenSaving"] = this.syncWhenSaving;
				}
				if (blnAllFields || oFields["allowPartialSave"]) {
					objParams["AllowPartialSave"] = this.allowPartialSave;
				}
				if (blnAllFields || oFields["elementQuestionID"]) {
					objParams["ElementQuestionID"] = this.elementQuestionID;
				}
				/*if (blnAllFields || oFields["eBavelAppID"]) {
					objParams["eBavelAppID"] = this.eBavelAppID;
				}*/
				if (blnAllFields || oFields["enableReschedule"]) {
					objParams["EnableReschedule"] = this.enableReschedule;
				}
				if (blnAllFields || oFields["rescheduleCatalogID"]) {
					objParams["RescheduleCatalogID"] = this.rescheduleCatalogID;
				}
				if (blnAllFields || oFields["rescheduleCatMemberID"]) {
					objParams["RescheduleCatMemberID"] = this.rescheduleCatMemberID;
				}
				if (blnAllFields || oFields["rescheduleStatus"]) {
					objParams["RescheduleStatus"] = this.rescheduleStatus;
				}
				if (blnAllFields || oFields["additionalEMails"]) {
					objParams["AdditionalEMails"] = this.additionalEMails;
				}
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				if (blnAllFields || oFields["displayImageDes"]) {
					objParams["SurveyImageText"] = this.displayImageDes;
				}
				//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
				if (blnAllFields || oFields["displayMenuImageDes"]) {
					objParams["SurveyMenuImage"] = this.displayMenuImageDes;
				}
				/*
				if (blnAllFields || oFields["displayImage"]) {
					objParams["SurveyImageText"] = this.displayImage;
				}
				*/
				//JAPR
				if (blnAllFields || oFields["hideNavButtons"]) {
					objParams["HideNavButtons"] = this.hideNavButtons;
				}
				if (blnAllFields || oFields["hideSectionNames"]) {
					objParams["HideSectionNames"] = this.hideSectionNames;
				}
				if (blnAllFields || oFields["disableEntry"]) {
					objParams["DisableEntry"] = this.disableEntry;
				}
				if (blnAllFields || oFields["isAgendable"]) {
					objParams["IsAgendable"] = this.isAgendable;
				}
				if (blnAllFields || oFields["radioForAgenda"]) {
					objParams["RadioForAgenda"] = this.radioForAgenda;
				}
				if (blnAllFields || oFields["otherLabel"]) {
					objParams["OtherLabel"] = this.otherLabel;
				}
				if (blnAllFields || oFields["commentLabel"]) {
					objParams["CommentLabel"] = this.commentLabel;
				}
				//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
				if (blnAllFields || oFields["incrementalFile"]) {
					objParams["incrementalFile"] = this.incrementalFile;
				}
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
				if (blnAllFields || oFields["width"]) {
					objParams["Width"] = this.width;
				}
				if (blnAllFields || oFields["height"]) {
					objParams["Height"] = this.height;
				}
				if (blnAllFields || oFields["textPos"]) {
					objParams["TextPosition"] = this.textPos;
				}
				if (blnAllFields || oFields["textSize"]) {
					objParams["TextSize"] = this.textSize;
				}
				if (blnAllFields || oFields["customLayout"]) {
					objParams["CustomLayout"] = this.customLayout;
				}
				//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
				if (blnAllFields || oFields["templateID"]) {
					objParams["TemplateID"] = this.templateID;
				}
				//JAPR
				//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
				if (blnAllFields || oFields["showQNumbers"]) {
					objParams["ShowQNumbers"] = this.showQNumbers;
				}
				//JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
				if (blnAllFields || oFields["plannedDuration"]) {
					objParams["PlannedDuration"] = this.plannedDuration;
				}
				//JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
				if (blnAllFields || oFields["enableNavHistory"]) {
					objParams["EnableNavHistory"] = this.enableNavHistory;
				}
				//JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
				if (blnAllFields || oFields["autoFitImage"]) {
					objParams["AutoFitImage"] = this.autoFitImage;
				}
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				if (blnAllFields || oFields["questionIDForEntryDesc"]) {
					objParams["QuestionIDForEntryDesc"] = this.questionIDForEntryDesc;
				}
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				if (blnAllFields || oFields["reloadSurveyFromSection"]) {
					objParams["ReloadSurveyFromSection"] = this.reloadSurveyFromSection;
				}
				
				return objParams;
			}
			
			/* Obtiene la lista de secciones existentes en la forma. Utilizada para despliegue de campos donde se configura una sección cuando se usa como columna de una colección,
			ya que en ese caso puede ser cualquier sección de la forma porque el grid no permite de manera natural variar el contenido de la combo row por row
			//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			Agregado el parámetro oSectionTypes con el cual se especificarían que tipo de secciones son válidas para regresar con la función, si no se especifica se regresan todas
			*/
			SurveyCls.prototype.getAllSectionItems = function(oSectionTypes) {
				var arrObjectsIDs = new Array();
				var objObjectsColl = new Object();
				this.allSectionsIDsColl = arrObjectsIDs;
				this.allSectionsColl = objObjectsColl;
				
				if (!this.sectionsOrder) {
					return;
				}
				
				if (oSectionTypes === undefined) {
					oSectionTypes = new Object();
					oSectionTypes[SectionType.Standard] = 1;
					oSectionTypes[SectionType.Multiple] = 1;
					oSectionTypes[SectionType.Inline] = 1;
					oSectionTypes[SectionType.Dynamic] = 1;
				}
				
				for (var intObjectNum in selSurvey.sectionsOrder) {
					var intObjectID = selSurvey.sectionsOrder[intObjectNum];
					var objObject = selSurvey.sections[intObjectID];
					//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
					if (objObject && oSectionTypes[objObject.sectionType]) {
						arrObjectsIDs.push(intObjectID);
						objObjectsColl[intObjectID] = {id:intObjectID, name:objObject.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.allSectionsIDsColl = arrObjectsIDs;
				this.allSectionsColl = objObjectsColl;
				
				return objObjectsColl;
			}
			
			SurveyCls.prototype.getAllSectionItemsOrder = function() {
				return this.allSectionsIDsColl;
			}
			
			/* Obtiene la lista de secciones existentes en la forma y que no se hubieran utilizado previamente en preguntas tipo Sección, o bien que estén incluidas entre las del
			parámetro oValidSections que serían las usadas por el proceso que invoca a esta función
			*/
			SurveyCls.prototype.getComboUnusedInlineSectionItems = function(bAddEmpty, oValidSections) {
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				
				if (!this.sectionsOrder || !this.questionsOrder) {
					return arrObjects;
				}
				
				//Primero determina que secciones ya han sido usadas por otra pregunta para excluirlas
				var objInvalidSections = new Object();
				for (var intObjectNum in this.questionsOrder) {
					var intObjectID = this.questionsOrder[intObjectNum];
					var objObject = this.questions[intObjectID];
					if (objObject && objObject.type == Type.section) {
						if (objObject.sourceSectionID > 0) {
							objInvalidSections[objObject.sourceSectionID] = 1;
						}
					}
				}
				
				if (oValidSections === undefined) {
					oValidSections = new Object();
				}
				
				//Obtiene todas las secciones tipo Inline que no han sido usadas o están entre las marcadas como válidas
				for (var intObjectNum in this.sectionsOrder) {
					var intObjectID = this.sectionsOrder[intObjectNum];
					var objObject = this.sections[intObjectID];
					//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType
					if (objObject && (objObject.sectionType == SectionType.Dynamic || objObject.sectionType == SectionType.Inline)) {
						if (!objInvalidSections[objObject.id] || oValidSections[objObject.id]) {
							arrObjects.push({value:intObjectID, text:objObject.getName()});
						}
					}
				}
				
				return arrObjects;
			}
			
			/* Obtiene la lista de secciones existentes en la forma y que no se hubieran utilizado previamente en preguntas tipo Sección, o bien que estén incluidas entre las del
			parámetro oValidSections que serían las usadas por el proceso que invoca a esta función
			*/
			SurveyCls.prototype.getUnusedInlineSectionItems = function(oValidSections) {
				var arrObjectsIDs = new Array();
				var objObjectsColl = new Object();
				this.unusedInlineSectionsIDsColl = arrObjectsIDs;
				this.unusedSectionsColl = objObjectsColl;
				
				if (!this.sectionsOrder || !this.questionsOrder) {
					return;
				}
				
				//Primero determina que secciones ya han sido usadas por otra pregunta para excluirlas
				var objInvalidSections = new Object();
				for (var intObjectNum in this.questionsOrder) {
					var intObjectID = this.questionsOrder[intObjectNum];
					var objObject = this.questions[intObjectID];
					if (objObject && objObject.type == Type.section) {
						if (objObject.sourceSectionID > 0) {
							objInvalidSections[objObject.sourceSectionID] = 1;
						}
					}
				}
				
				if (oValidSections === undefined) {
					oValidSections = new Object();
				}
				
				//Obtiene todas las secciones tipo Inline que no han sido usadas o están entre las marcadas como válidas
				for (var intObjectNum in this.sectionsOrder) {
					var intObjectID = this.sectionsOrder[intObjectNum];
					var objObject = this.sections[intObjectID];
					//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType
					if (objObject && (objObject.sectionType == SectionType.Dynamic || objObject.sectionType == SectionType.Inline)) {
						if (!objInvalidSections[objObject.id] || oValidSections[objObject.id]) {
							arrObjectsIDs.push(intObjectID);
							objObjectsColl[intObjectID] = {id:intObjectID, name:objObject.getName()};
						}
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.unusedInlineSectionsIDsColl = arrObjectsIDs;
				this.unusedSectionsColl = objObjectsColl;
				
				return objObjectsColl;
			}
			
			SurveyCls.prototype.getUnusedInlineSectionItemsOrder = function() {
				return this.unusedInlineSectionsIDsColl;
			}
			
			/* Obtiene la lista de preguntas existentes en la forma. Utilizada para despliegue de campos donde se configura una pregunta cuando se usa como columna de una colección,
			ya que en ese caso puede ser cualquier sección de la forma porque el grid no permite de manera natural variar el contenido de la combo row por row
			//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
			Agregados parámetros para filtrar por tipo de pregunta o varias cosas mas
			*/
			SurveyCls.prototype.getAllQuestionItems = function(options) {
				var arrObjectsIDs = new Array();
				var objObjectsColl = new Object();
				this.allQuestionsIDsColl = arrObjectsIDs;
				this.allQuestionsColl = objObjectsColl;
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				if (!options) {
					options = {};
				}
				//JAPR
				
				if (!this.questionsOrder) {
					return;
				}
				
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				//Agrega el valor de ninguno
				if (options && options.addEmpty) {
					var intObjectID = (options.emptyId !== undefined)?options.emptyId:-1;
					var strObjectName = options.emptyDesc || "(<?=translate("None")?>)";
					arrObjectsIDs.push(intObjectID);
					objObjectsColl[intObjectID] = {id:intObjectID, name:strObjectName};
				}
				//JAPR
				
				for (var intObjectNum in selSurvey.questionsOrder) {
					var intObjectID = selSurvey.questionsOrder[intObjectNum];
					var objObject = selSurvey.questions[intObjectID];
					if (objObject) {
						//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
						if (options && $.isPlainObject(options.filterTypes)) {
							if (!options.filterTypes[objObject.type]) {
								continue;
							}
						}
						//JAPR 2017-04-26: Agregado el filtro por tipo de sección (#UO4Q4M)
						if (options && $.isPlainObject(options.filterSectionTypes)) {
							var objSection = selSurvey.sections[objObject.sectionID];
							if (objSection && !options.filterSectionTypes[objSection.type]) {
								continue;
							}
						}
						//JAPR
						
						arrObjectsIDs.push(intObjectID);
						//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
						objObjectsColl[intObjectID] = {id:intObjectID, name: ((options.addNumber)?(objObject.number+'- '):'') + objObject.getName()};
						//JAPR
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.allQuestionsIDsColl = arrObjectsIDs;
				this.allQuestionsColl = objObjectsColl;
				
				return objObjectsColl;
			}
			
			SurveyCls.prototype.getAllQuestionsItemsOrder = function() {
				return this.allQuestionsIDsColl;
			}
			
			/* 
				Obtiene toda la lista de preguntas abiertas de la forma actual
			*/
			SurveyCls.prototype.getAllOpenQuestionItems = function() {
				var arrObjectIDs = new Array();
				var objObjectsColl = new Array();
				var openQuestions = [Type.number, Type.alfanum, Type.text, Type.date, Type.time, Type.password];
				this.allOpenQuestionsIDsColl = arrObjectIDs;
				this.allOpenQuestionsColl = objObjectsColl;
				
				if (!selSurvey.questions || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intObjectNum in selSurvey.questionsOrder) {
					var intObjectID = selSurvey.questionsOrder[intObjectNum];
					var objObject = selSurvey.questions[intObjectID];
					//Busca si el tipo de pregunta es abierta, de ser así se agrega a las posibles opciones a seleccionar.
					if (objObject && (openQuestions.indexOf(objObject.type) > -1)) {
						arrObjectIDs.push(intObjectID);
						objObjectsColl.push(objObject.number + " - " + objObject.getName());
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.allOpenQuestionsIDsColl = arrObjectIDs;
				this.allOpenQuestionsColl = objObjectsColl;
				
				return objObjectsColl;
			}
			
			SurveyCls.prototype.getAllOpenQuestionItemsOrder = function() {
				return this.allOpenQuestionsIDsColl;
			}

			
			/* Obtiene la lista de preguntas que se pueden ocultar/mostrar desde esta pregunta
				Inicialmente este obejto se preparó para un componente GFieldTypes.multiList, el cual solo soporta textos sin IDs, así que antepone el # de pregunta a cada texto para
				hacerlos únicos, internamente se convertirán a IDs mientras se procesa el componente
			*/
			SurveyCls.prototype.getAllShowQuestionItems = function() {
				var arrObjectIDs = new Array();
				var objObjectsColl = new Array();
				this.allShowQuestionsIDsColl = arrObjectIDs;
				this.allshowQuestionsColl = objObjectsColl;
				
				if (!selSurvey.questions || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intObjectNum in selSurvey.questionsOrder) {
					var intObjectID = selSurvey.questionsOrder[intObjectNum];
					var objObject = selSurvey.questions[intObjectID];
					if (objObject) {
						arrObjectIDs.push(intObjectID);
						objObjectsColl.push(objObject.number + " - " + objObject.getName());
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.allShowQuestionsIDsColl = arrObjectIDs;
				this.allshowQuestionsColl = objObjectsColl;
				
				return objObjectsColl;
			}
			
			SurveyCls.prototype.getAllShowQuestionItemsOrder = function() {
				return this.allShowQuestionsIDsColl;
			}
			
			//******************************************************************************************************
			SectionCls.prototype = new GenericItemCls();
			SectionCls.prototype.constructor = SectionCls;
			function SectionCls() {
				this.objectType = AdminObjectType.otySection;
				this.objectIDFieldName = "SectionID";
			}
			
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			SectionCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["name"]) {
					objParams["SectionName"] = this.name;
					//objParams["SectionNameHTML"] = this.nameHTML;
				}
				if (blnAllFields || oFields["number"]) {
					objParams["SectionNumber"] = this.number;
				}
				if (blnAllFields || oFields["type"]) {
					objParams["SectionType"] = this.type;
				}
				//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
				if (blnAllFields || oFields["sectionType"]) {
					objParams["SectionTypeDes"] = this.sectionType;
				}
				//JAPR
				if (blnAllFields || oFields["repetitions"]) {
					objParams["Repetitions"] = this.repetitions;
				}
				//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
				if (blnAllFields || oFields["dataSourceID"]) {
					objParams["DataSourceID"] = this.dataSourceID;
				}
				if (blnAllFields || oFields["dataSourceMemberID"]) {
					objParams["DataSourceMemberID"] = this.dataSourceMemberID;
				}
				//JAPR
				if (blnAllFields || oFields["elementQuestionID"]) {
					objParams["ElementQuestionID"] = this.elementQuestionID;
				}
				if (blnAllFields || oFields["formattedText"]) {
					objParams["FormattedText"] = this.formattedText;
				}
				/*
				if (blnAllFields || oFields["sendThumbnails"]) {
					objParams["SendThumbnails"] = this.sendThumbnails;
				}
				if (blnAllFields || oFields["enableHyperlinks"]) {
					objParams["EnableHyperlinks"] = this.enableHyperlinks;
				}
				*/
				if (blnAllFields || oFields["nextSectionID"]) {
					objParams["NextSectionID"] = this.nextSectionID;
				}
				if (blnAllFields || oFields["autoRedraw"]) {
					objParams["AutoRedraw"] = this.autoRedraw;
				}
				if (blnAllFields || oFields["showCondition"]) {
					objParams["ShowCondition"] = this.showCondition;
				}
				if (blnAllFields || oFields["selectorQuestionID"]) {
					objParams["SelectorQuestionID"] = this.selectorQuestionID;
				}
				if (blnAllFields || oFields["displayMode"]) {
					objParams["DisplayMode"] = this.displayMode;
				}
				if (blnAllFields || oFields["switchBehaviour"]) {
					objParams["SwitchBehaviour"] = this.switchBehaviour;
				}
				if (blnAllFields || oFields["htmlFooterDes"]) {
					objParams["HTMLFooter"] = this.htmlFooterDes;
				}
				if (blnAllFields || oFields["HTMLFEditorState"]) {
					objParams["HTMLFEditorState"] = this.HTMLFEditorState;
				}
				if (blnAllFields || oFields["htmlHeaderDes"]) {
					objParams["HTMLHeader"] = this.htmlHeaderDes;
				}
				if (blnAllFields || oFields["HTMLHEditorState"]) {
					objParams["HTMLHEditorState"] = this.HTMLHEditorState;
				}
				if (blnAllFields || oFields["summaryInMDSection"]) {
					objParams["SummaryInMDSection"] = this.summaryInMDSection;
				}
				if (blnAllFields || oFields["showInNavMenu"]) {
					objParams["ShowInNavMenu"] = this.showInNavMenu;
				}
				//JAPR 2015-08-24: Corregido un bug, no estaba aplicando la propiedad por usar un nombre equivocado
				if (blnAllFields || oFields["showSelector"]) {
					objParams["ShowSelector"] = this.showSelector;	//(this.showSelector)?0:1;
				}
				if (blnAllFields || oFields["sourceQuestionID"]) {
					objParams["SourceQuestionID"] = this.sourceQuestionID;
				}
				if (blnAllFields || oFields["sourceSectionID"]) {
					objParams["SourceSectionID"] = this.sourceSectionID;
				}
				if (blnAllFields || oFields["MinRecordsToSelect"]) {
					objParams["MinRecordsToSelect"] = this.MinRecordsToSelect;
				}
				if (blnAllFields || oFields["valuesSourceType"]) {
					objParams["ValuesSourceType"] = this.valuesSourceType;
				}
				if (blnAllFields || oFields["strSelectOptions"]) {
					objParams["StrSelectOptions"] = this.strSelectOptions;
				}
				//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
				if (blnAllFields || oFields["dataSourceFilter"]) {
					objParams["DataSourceFilter"] = this.dataSourceFilter;
				}
				//@RTORRES 2015-08-28: Agregado ocultar boton next,back
				if (blnAllFields || oFields["hideNavButtons"]) {
					objParams["HideNavButtons"] = this.hideNavButtons;
				}
				//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
				if (blnAllFields || oFields["useDescriptorRow"]) {
					objParams["UseDescriptorRow"] = this.useDescriptorRow;
				}
				//JAPR
				
				return objParams;
			}
			
			/* Actualiza la información en pantalla de la sección que acaba de ser modificada en el servidor (originalmente usada para forzar a reconstruir el DataView de secciones
			//JAPR 2015-11-13: Corregido un bug, los campos de Catálogo, Atributo y Filtro se estaban mostrando para tipos de sección que no aplican
			Agregado el parámetro bUpdateFromServer para indicar que el evento ocurrió al haber recibido una actualización desde el servidor, por tanto se deben actualizar algunas
			propiedades pues sus valores pudieron haber cambiado, el objeto oNewDefinition es la definición como se recibió del servidor así que contiene las configuraciones reales
			*/
			SectionCls.prototype.refreshView = function(oNewDefinition, bUpdateFromServer) {
				console.log('SectionCls.refreshView');
				
				if (!oNewDefinition) {
					return;
				}
				
				//Actualiza las propiedades del objeto local con la nueva definición obtenida desde el server
				if (oNewDefinition.name !== undefined) {
					this.name = oNewDefinition.name;
				}
				if (oNewDefinition.type !== undefined) {
					this.type = oNewDefinition.type;
				}
				if (oNewDefinition.selectorQuestionID !== undefined) {
					this.selectorQuestionID = oNewDefinition.selectorQuestionID;
				}
				
				//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if (oNewDefinition.xPosQuestionID !== undefined) {
					this.xPosQuestionID = oNewDefinition.xPosQuestionID;
				}
				if (oNewDefinition.yPosQuestionID !== undefined) {
					this.yPosQuestionID = oNewDefinition.yPosQuestionID;
				}
				if (oNewDefinition.itemNameQuestionID !== undefined) {
					this.itemNameQuestionID = oNewDefinition.itemNameQuestionID;
				}
				//JAPR
				
				//Para este punto ya se actualizó el objeto local antes de ir al servidor, así que sólo refresca la DataView completa
				doDrawSections();
				
				//JAPR 2015-11-13: Corregido un bug, los campos de Catálogo, Atributo y Filtro se estaban mostrando para tipos de sección que no aplican
				if (bUpdateFromServer) {
					//Si se está actualizando desde el servidor, se debe verificar si el objeto local ha cambiado respecto a la definición recibida para propiedades clave, si es así
					//se debe actualizar la ventana de propiedades (sólo podría haber lanzado esta función si precisamente estuviera cambiando este objeto, así que es válido asumir
					//que se están desplegando sus propiedades)
					if (this.valuesSourceType != oNewDefinition.valuesSourceType) {
						this.valuesSourceType = oNewDefinition.valuesSourceType;
						var objField = getFieldDefinition("advTab", "valuesSourceType");
						if (objField) {
							objField.setValue(undefined, undefined, oNewDefinition.valuesSourceType);
							objField.refreshValue(oNewDefinition.valuesSourceType);
							
							//Encadena la función para hacer refresh de componentes independientemente de si cambió el tipo de llenado, ya que por haber cambiado el
							//tipo de despliegue es probable que ya no apliquen algunas configuraciones, así que se refresca todo
							objField.applyVisibilityRules();
							if (objField.setValuesFn) {
								objField.setValuesFn();
							}
							
							//Por excepción debido a que no se puede con el framework actual definir la visibilidad a partir de otros dos campos padre, el campo de Valores se ocultará
							//directamente en este punto si no aplica según el tipo de llenado y de despliegue de la sección, basado e la premisa que este campo a validar es precisamente
							//el tipo de llenado, y si cambió sin ser igual que el local, debe ser porque otra propiedad fue actualizada (probablemente el tipo de despliegue), y porque el
							//tipo de llenado contiene el mismo valor default para diferentes tipo de despliegue pero sólo en algunos es válido
							if (oNewDefinition.sectionType == SectionType.Standard || oNewDefinition.sectionType == SectionType.Multiple) {
								var objValuesField = getFieldDefinition("advTab", "strSelectOptions");
								if (objValuesField && objValuesField.hide) {
									objValuesField.hide();
								}
							}
						}
					}
				}
			}
			
			/* Obtiene la lista de secciones posteriores a la de esta instancia, a las cuales se puede permitir un salto cuando se termina la captura de esta sección */
			SectionCls.prototype.getNextSectionItems = function() {
				var arrSectionsIDs = new Array();
				var objSectionsColl = new Object();
				this.nextSectionsIDsColl = arrSectionsIDs;
				this.nextSectionsColl = objSectionsColl;
				
				if (!selSurvey || !selSurvey.sectionsOrder) {
					return;
				}
				
				for (var intSectionNum in selSurvey.sectionsOrder) {
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection && objSection.number > this.number) {
						arrSectionsIDs.push(intSectionID);
						objSectionsColl[intSectionID] = {id:intSectionID, name:objSection.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.nextSectionsIDsColl = arrSectionsIDs;
				this.nextSectionsColl = objSectionsColl;
				
				return objSectionsColl;
			}
			
			SectionCls.prototype.getNextSectionItemsOrder = function() {
				return this.nextSectionsIDsColl;
			}
			
			//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
			/* Obtiene la lista de preguntas múltiple choice o sección inline previas a la de esta sección sólo si pertenecen a una sección estándar, ya que no puede obtener como fuente
			los valores combinados de otras secciones multi-registros
			*/
			SectionCls.prototype.getSourceQuestionItems = function() {
				var arrQuestionsIDs = new Array();
				var objQuestionsColl = new Object();
				this.sourceQuestionsIDsColl = arrQuestionsIDs;
				this.sourceQuestionsColl = objQuestionsColl;
				
				if (!selSurvey || !selSurvey.questionsOrder || !selSurvey.sections) {
					return;
				}
				
				for (var intQuestionNum in selSurvey.questionsOrder) {
					var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
					var objQuestion = selSurvey.questions[intQuestionID];
					if (objQuestion && (objQuestion.type == Type.multiplechoice || objQuestion.type == Type.section)) {
						var objSection = selSurvey.sections[objQuestion.sectionID];
						if (objSection && objSection.number >= this.number) {
							break;
						}
						
						arrQuestionsIDs.push(intQuestionID);
						objQuestionsColl[intQuestionID] = {id:intQuestionID, name:objQuestion.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.sourceQuestionsIDsColl = arrQuestionsIDs;
				this.sourceQuestionsColl = objQuestionsColl;
				
				return objQuestionsColl;
			}
			
			SectionCls.prototype.getSourceQuestionItemsOrder = function() {
				return this.sourceQuestionsIDsColl;
			}
			
			/* Obtiene la lista de secciones inline previas a esta sección, ya que sólo ellas contienen valores que pueden ser reutilizados
			*/
			SectionCls.prototype.getSourceSectionItems = function() {
				var arrSectionsIDs = new Array();
				var objSectionsColl = new Object();
				this.sourceSectionsIDsColl = arrSectionsIDs;
				this.sourceSectionsColl = objSectionsColl;
				
				if (!selSurvey || !selSurvey.sectionsOrder || !selSurvey.sections) {
					return;
				}
				
				for (var intSectionNum in selSurvey.sectionsOrder) {
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection) {
						if (objSection.number >= this.number) {
							break;
						}
						
						if (objSection.sectionType == SectionType.Dynamic || objSection.sectionType == SectionType.Inline) {
							arrSectionsIDs.push(intSectionID);
							objSectionsColl[intSectionID] = {id:intSectionID, name:objSection.getName()};
						}
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.sourceSectionsIDsColl = arrSectionsIDs;
				this.sourceSectionsColl = objSectionsColl;
				
				return objSectionsColl;
			}
			
			SectionCls.prototype.getSourceSectionItemsOrder = function() {
				return this.sourceSectionsIDsColl;
			}
			
			//******************************************************************************************************
			QuestionCls.prototype = new GenericItemCls();
			QuestionCls.prototype.constructor = QuestionCls;
			function QuestionCls() {
				this.objectType = AdminObjectType.otyQuestion;
				this.objectIDFieldName = "QuestionID";
				this.parentFields = {SectionID:"sectionID"};
			}
			
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			QuestionCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["nameHTMLDes"]) {
					//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
					//objParams["QuestionText"] = this.name;
					objParams["QuestionTextHTML"] = this.nameHTMLDes;
				}
				if (blnAllFields || oFields["number"]) {
					objParams["QuestionNumber"] = this.number;
				}
				if (blnAllFields || oFields["type"]) {
					objParams["QTypeID"] = this.type;
				}
				if (blnAllFields || oFields["valuesSourceType"]) {
					objParams["ValuesSourceType"] = this.valuesSourceType;
				}
				if (blnAllFields || oFields["canvasWidth"]) {
					objParams["canvasWidth"] = this.canvasWidth;
				}
				if (blnAllFields || oFields["canvasHeight"]) {
					objParams["canvasHeight"] = this.canvasHeight;
				}
				//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
				if (blnAllFields || oFields["imageWidth"]) {
					objParams["imageWidth"] = this.imageWidth;
				}
				if (blnAllFields || oFields["imageHeight"]) {
					objParams["imageHeight"] = this.imageHeight;
				}
				//JAPR 2019-02-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if (blnAllFields || oFields["sketchImageWidth"]) {
					objParams["sketchImageWidth"] = this.imageWidth;
				}
				if (blnAllFields || oFields["sketchImageHeight"]) {
					objParams["sketchImageHeight"] = this.imageHeight;
				}
				//JAPR
				if (blnAllFields || oFields["minValue"]) {
					objParams["MinValue"] = this.minValue;
				}
				if (blnAllFields || oFields["maxValue"]) {
					objParams["MaxValue"] = this.maxValue;
				}
				if (blnAllFields || oFields["shortName"]) {
					objParams["AttributeName"] = this.shortName;
				}
				if (blnAllFields || oFields["isReqQuestion"]) {
					objParams["IsReqQuestion"] = this.isReqQuestion;
				}
				if (blnAllFields || oFields["isInvisible"]) {
					objParams["IsInvisible"] = this.isInvisible;
				}
				if (blnAllFields || oFields["hasReqComment"]) {
					objParams["HasReqComment"] = this.hasReqComment;
				}
				if (blnAllFields || oFields["hasReqPhoto"]) {
					objParams["HasReqPhoto"] = this.hasReqPhoto;
				}
				//OMMC 2016-03-29: Agregada la propiedad para la galería
				if (blnAllFields || oFields["allowGallery"]) {
					objParams["AllowGallery"] = this.allowGallery;
				}
				if (blnAllFields || oFields["displayMode"]) {
					objParams["QDisplayMode"] = this.displayMode;
				}
				if (blnAllFields || oFields["componentStyle"]) {
					objParams["QComponentStyleID"] = this.componentStyle;
				}
				if (blnAllFields || oFields["qLength"]) {
					objParams["QLength"] = this.qLength;
				}
				if (blnAllFields || oFields["mcInputType"]) {
					objParams["MCInputType"] = this.mcInputType;
				}
				//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
				if (blnAllFields || oFields["dataSourceID"]) {
					objParams["DataSourceID"] = this.dataSourceID;
				}
				if (blnAllFields || oFields["dataSourceMemberID"]) {
					objParams["DataSourceMemberID"] = this.dataSourceMemberID;
				}
				//JAPR 2015-08-02: Agregado el tipo de pregunta GetData
				if (blnAllFields || oFields["dataSourceFilter"]) {
					objParams["DataSourceFilter"] = this.dataSourceFilter;
				}
				//JAPR
				if (blnAllFields || oFields["useCatalog"]) {
					objParams["UseCatalog"] = this.useCatalog;
				}
				if (blnAllFields || oFields["useMap"]) {
					objParams["UseMap"] = this.useMap;
				}
				if (blnAllFields || oFields["mapRadio"]) {
					objParams["MapRadio"] = this.mapRadio;
				}
				if (blnAllFields || oFields["dataMemberLatitudeID"]) {
					objParams["DataMemberLatitudeID"] = this.dataMemberLatitudeID;
				}
				if (blnAllFields || oFields["dataMemberLongitudeID"]) {
					objParams["DataMemberLongitudeID"] = this.dataMemberLongitudeID;
				}
				//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				if (blnAllFields || oFields["dataMemberImageID"]) {
					objParams["DataMemberImageID"] = this.dataMemberImageID;
				}
				//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				if (blnAllFields || oFields["dataMemberOrderID"]) {
					objParams["DataMemberOrderID"] = this.dataMemberOrderID;
				}
				if (blnAllFields || oFields["dataMemberOrderDir"]) {
					objParams["DataMemberOrderDir"] = this.dataMemberOrderDir;
				}
				if (blnAllFields || oFields["customLayout"]) {
					objParams["CustomLayout"] = this.customLayout;
				}
				//JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
				//JAPR
				/*if (blnAllFields || oFields["catMemberLatitudeID"]) {
					objParams["CatMemberLatitudeID"] = this.catMemberLatitudeID;
				}
				if (blnAllFields || oFields["catMemberLongitudeID"]) {
					objParams["CatMemberLongitudeID"] = this.catMemberLongitudeID;
				}*/
				if (blnAllFields || oFields["nextSectionID"]) {
					objParams["GoToQuestion"] = this.nextSectionID;
				}
				if (blnAllFields || oFields["allowAdditionalAnswers"]) {
					objParams["AllowAdditionalAnswers"] = this.allowAdditionalAnswers;
				}
				if (blnAllFields || oFields["decimalPlaces"]) {
					objParams["DecimalPlaces"] = this.decimalPlaces;
				}
				if (blnAllFields || oFields["sourceQuestionID"]) {
					objParams["SourceQuestionID"] = this.sourceQuestionID;
				}
				if (blnAllFields || oFields["sharedQuestionID"]) {
					objParams["SharedQuestionID"] = this.sharedQuestionID;
				}
				if (blnAllFields || oFields["format"]) {
					objParams["FormatMask"] = this.format;
				}
				if (blnAllFields || oFields["dateFormat"]) {
					objParams["DateFormatMask"] = this.dateFormat;
				}
				if (blnAllFields || oFields["maxChecked"]) {
					objParams["MaxChecked"] = this.maxChecked;
				}
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				if (blnAllFields || oFields["displayImageDes"]) {
					objParams["QuestionImageText"] = this.displayImageDes;
				}
				/*
				if (blnAllFields || oFields["displayImage"]) {
					objParams["QuestionImageText"] = this.displayImage;
				}
				*/
				//JAPR
				if (blnAllFields || oFields["skipSection"]) {
					objParams["GoToQuestion"] = this.skipSection;
				}
				if (blnAllFields || oFields["formattedTextDes"]) {
					objParams["QuestionMessage"] = this.formattedTextDes;
				}
				//JAPR 2015-08-15: Agregado el ipo de pregunta HTML
				if (blnAllFields || oFields["htmlCode"]) {
					objParams["HTMLCode"] = this.htmlCode;
				}
				//OMMC 2016-01-19: Agregado para el estado de las preguntas HTML
				if (blnAllFields || oFields["HTMLEditorState"]) {
					objParams["HTMLEditorState"] = this.HTMLEditorState;
				}
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				if (blnAllFields || oFields["imageSketchDes"]) {
					objParams["QuestionImageSketch"] = this.imageSketchDes;
				}
				/*
				if (blnAllFields || oFields["imageSketch"]) {
					objParams["QuestionImageSketch"] = this.imageSketch;
				}
				*/
				//JAPR
				if (blnAllFields || oFields["defaultValue"]) {
					objParams["DefaultValue"] = this.defaultValue;
				}
				//JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
				if (blnAllFields || oFields["autoSelectEmptyOpt"]) {
					objParams["AutoSelectEmptyOpt"] = this.autoSelectEmptyOpt;
				}
				//JAPR
				if (blnAllFields || oFields["excludeSelected"]) {
					objParams["ExcludeSelected"] = this.excludeSelected;
				}
				if (blnAllFields || oFields["excludeFromQuestionID"]) {
					objParams["SourceSimpleQuestionID"] = this.excludeFromQuestionID;
				}
				if (blnAllFields || oFields["summaryTotal"]) {
					objParams["SummaryTotal"] = this.summaryTotal;
				}
				if (blnAllFields || oFields["readOnly"]) {
					objParams["ReadOnly"] = this.readOnly;
				}
				if (blnAllFields || oFields["formula"]) {
					objParams["Formula"] = this.formula;
				}
				if (blnAllFields || oFields["showCondition"]) {
					objParams["ShowCondition"] = this.showCondition;
				}
				if (blnAllFields || oFields["showImageAndText"]) {
					objParams["ShowImageAndText"] = this.showImageAndText;
				}
				if (blnAllFields || oFields["registerGPSAt"]) {
					objParams["RegisterGPSAt"] = this.registerGPSAt;
				}
				if (blnAllFields || oFields["inputSize"]) {
					objParams["InputWidth"] = this.inputSize;
				}
				if (blnAllFields || oFields["pictureLayOut"]) {
					objParams["PictLayOut"] = this.pictureLayOut;
				}
				if (blnAllFields || oFields["enableCheckBox"]) {
					objParams["EnableCheckBox"] = this.enableCheckBox;
				}
				if (blnAllFields || oFields["switchBehaviour"]) {
					objParams["SwitchBehaviour"] = this.switchBehaviour;
				}
				if (blnAllFields || oFields["showInSummary"]) {
					objParams["ShowInSummary"] = this.showInSummary;
				}
				if (blnAllFields || oFields["columnWidth"]) {
					objParams["ColumnWidth"] = this.columnWidth;
				}
				if (blnAllFields || oFields["sourceSectionID"]) {
					objParams["SourceSectionID"] = this.sourceSectionID;
				}
				if (blnAllFields || oFields["autoRedraw"]) {
					objParams["AutoRedraw"] = this.autoRedraw;
				}
				if (blnAllFields || oFields["catMembersList"]) {
					objParams["CatMembersList"] = this.catMembersList;
				}
				if (blnAllFields || oFields["telephoneNum"]) {
					objParams["TelephoneNum"] = this.telephoneNum;
				}
				//JAPR 2015-08-15: Agregado el ipo de pregunta HTML
				if (blnAllFields || oFields["editorType"]) {
					objParams["EditorType"] = this.editorType;
				}
				//OMMC 2015-11-30: Agregado paraa las preguntas OCR
				if(blnAllFields || oFields["savePhoto"]){
					objParams["SavePhoto"] = this.savePhoto;
				}
				if(blnAllFields || oFields["questionList"]){
					objParams["QuestionList"] = this.questionList;
				}
				//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
				if (blnAllFields || oFields["showTotals"]) {
					objParams["ShowTotals"] = this.showTotals;
				}
				if (blnAllFields || oFields["totalsFunctionDes"]) {
					objParams["TotalsFunction"] = this.totalsFunctionDes;
				}
				//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
				if (blnAllFields || oFields["dataDestinationID"]) {
					objParams["DataDestinationID"] = this.dataDestinationID;
				}
				if (blnAllFields || oFields["nextSectionIfTrue"]) {
					objParams["NextSectionIDIfTrue"] = this.nextSectionIfTrue;
				}
				if (blnAllFields || oFields["nextSectionIfFalse"]) {
					objParams["NextSectionIDIfFalse"] = this.nextSectionIfFalse;
				}
				//JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
				if (blnAllFields || oFields["uploadMediaFiles"]) {
					objParams["UploadMediaFiles"] = this.uploadMediaFiles;
				}
				//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú 
				if (blnAllFields || oFields["noFlow"]) {
					objParams["NoFlow"] = this.noFlow;
				}
				//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
				if (blnAllFields || oFields["disablePhoneNum"]) {
					objParams["DisablePhoneNum"] = this.disablePhoneNum;
				}
				if (blnAllFields || oFields["disableSync"]) {
					objParams["DisableSync"] = this.disableSync;
				}
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				if (blnAllFields || oFields["dataMemberZipFileID"]) {
					objParams["DataMemberZipFileID"] = this.dataMemberZipFileID;
				}
				if (blnAllFields || oFields["dataMemberVersionID"]) {
					objParams["DataMemberVersionID"] = this.dataMemberVersionID;
				}
				if (blnAllFields || oFields["dataMemberPlatformID"]) {
					objParams["DataMemberPlatformID"] = this.dataMemberPlatformID;
				}
				if (blnAllFields || oFields["dataMemberTitleID"]) {
					objParams["DataMemberTitleID"] = this.dataMemberTitleID;
				}
				//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
				if (blnAllFields || oFields["recalculateDependencies"]) {
					objParams["RecalculateDependencies"] = this.recalculateDependencies;
				}
				//JAPR
				
				return objParams;
			}
			
			/* Obtiene la lista de secciones a las que se puede saltar desde esta pregunta, básicamente, las mismas que la sección donde pertenece la pregunta */
			QuestionCls.prototype.getNextSectionItems = function() {
				var objSection = getObject(AdminObjectType.otySection, this.sectionID);
				if (!objSection || !objSection.getNextSectionItems) {
					return;
				}
				
				return objSection.getNextSectionItems();
			}
			
			QuestionCls.prototype.getNextSectionItemsOrder = function() {
				var objSection = getObject(AdminObjectType.otySection, this.sectionID);
				if (!objSection || !objSection.getNextSectionItems) {
					return new Array();
				}
				
				return objSection.nextSectionsIDsColl;
			}
			
			//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			/* Obtiene la lista de secciones a las que se puede saltar desde esta pregunta preparadas para una forma */
			QuestionCls.prototype.getComboNextSectionItems = function(bAddEmpty) {
				if (bAddEmpty === undefined) {
					bAddEmpty = true;
				}
				
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				if (!selSurvey || !selSurvey.sections || !selSurvey.sectionsOrder) {
					return arrObjects;
				}
				
				for (var intObjectNum in selSurvey.sectionsOrder) {
					var intObjectID = selSurvey.sectionsOrder[intObjectNum];
					var objObject = selSurvey.sections[intObjectID];
					//JAPR 2016-04-26: Corregido un bug, estaba evaluando el número de sección respecto al número de pregunta (#OZGBL9)
					var objSection = selSurvey.sections[this.sectionID];
					if (objSection && objObject && objObject.number > objSection.number) {
						arrObjects.push({value:intObjectID, text:objObject.getName()});
					}
					//JAPR
				}
				
				return arrObjects;
			}
			
			/* Obtiene la lista de secciones a utilizar dentro de un DHTMLX Combo, el cual requiere una estructura definida como un array de objetos con ciertas propiedades
			Esta función no guardará internamente objetos, ya que se utiliza sólo para una forma flotante la cual es temporal
			*/
			QuestionCls.prototype.getComboSectionItems = function(bAddEmpty) {
				if (bAddEmpty === undefined) {
					bAddEmpty = true;
				}
				
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				if (!selSurvey || !selSurvey.sections || !selSurvey.sectionsOrder) {
					return arrObjects;
				}
				
				for (var intObjectNum in selSurvey.sectionsOrder) {
					var intObjectID = selSurvey.sectionsOrder[intObjectNum];
					var objObject = selSurvey.sections[intObjectID];
					if (objObject) {
						arrObjects.push({value:intObjectID, text:objObject.getName()});
					}
				}
				
				return arrObjects;
			}
			
			/* Obtiene la lista de catálogos a utilizar dentro de un DHTMLX Combo, el cual requiere una estructura definida como un array de objetos con ciertas propiedades
			Esta función no guardará internamente objetos, ya que se utiliza sólo para una forma flotante la cual es temporal
			*/
			QuestionCls.prototype.getComboCatalogItems = function(bAddEmpty) {
				if (bAddEmpty === undefined) {
					bAddEmpty = true;
				}
				
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				if (!arrCatalogs || !arrCatalogsOrder) {
					return arrObjects;
				}
				
				for (var intObjectNum in arrCatalogsOrder) {
					var intObjectID = arrCatalogsOrder[intObjectNum];
					var objObject = arrCatalogs[intObjectID];
					if (objObject) {
						arrObjects.push({value:intObjectID, text:objObject.name});
					}
				}
				
				return arrObjects;
			}
			
			/* Obtiene la lista de atributos del catálogo especificado a utilizar dentro de un DHTMLX Combo, el cual requiere una estructura definida como un array de objetos 
			con ciertas propiedades
			Esta función no guardará internamente objetos, ya que se utiliza sólo para una forma flotante la cual es temporal
			//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
			Agregado el parámetro oFilterProp para que tal como la propiedad de FieldCls del mismo nombre, sirva como filtro para identificar que objetos se incluirán
			*/
			QuestionCls.prototype.getComboCatalogMemberItems = function(iCatalogID, bAddEmpty, oFilterProp) {
				if (bAddEmpty === undefined) {
					bAddEmpty = true;
				}
				
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				if (!arrCatalogs || !arrCatalogs[iCatalogID] || !arrAttributes) {
					return arrObjects;
				}
				
				//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
				if (!oFilterProp || !$.isPlainObject(oFilterProp)) {
					oFilterProp = {};
				}
				//JAPR
				
				var objCatalog = arrCatalogs[iCatalogID];
				if (!objCatalog || !objCatalog.children || !objCatalog.childrenOrder) {
					return arrObjects;
				}
				
				for (var intObjectNum in objCatalog.childrenOrder) {
					var intObjectID = objCatalog.childrenOrder[intObjectNum];
					var objObject = arrAttributes[intObjectID];
					if (objObject) {
						//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
						var blnValid = true;
						for (var strPropName in oFilterProp) {
							if (objObject[strPropName] !== undefined && objObject[strPropName] != oFilterProp[strPropName]) {
								blnValid = false;
								break;
							}
						}
						
						if (!blnValid) {
							continue;
						}
						//JAPR
						
						arrObjects.push({value:intObjectID, text:objObject.name});
					}
				}
				
				return arrObjects;
			}
			
			//JAPR 2016-03-31: Agregado el tipo de pregunta Update Data
			/* Obtiene la lista de data destinations a utilizar dentro de un DHTMLX Combo, el cual requiere una estructura definida como un array de objetos con ciertas propiedades
			Esta función no guardará internamente objetos, ya que se utiliza sólo para una forma flotante la cual es temporal
			*/
			QuestionCls.prototype.getComboDataDestinationsItems = function(bAddEmpty) {
				if (bAddEmpty === undefined) {
					bAddEmpty = true;
				}
				
				var arrObjects = new Array();
				if (bAddEmpty) {
					arrObjects.push({value:0, text:"(<?=translate("None")?>)"});
				}
				if (!arrDataDestinations || !arrDataDestinationsOrder) {
					return arrObjects;
				}
				
				for (var intObjectNum in arrDataDestinationsOrder) {
					var intObjectID = arrDataDestinationsOrder[intObjectNum];
					var objObject = arrDataDestinations[intObjectID];
					if (objObject) {
						arrObjects.push({value:intObjectID, text:objObject.name});
					}
				}
				
				return arrObjects;
			}
			
			//JAPR 2015-07-28: Corregido un bug, no estaba llenando la lista de preguntas para usar las opciones de respuesta contestadas desde ella (#XL50YS)
			/* Obtiene la lista de preguntas múltiple choice previas a la de esta instancia, y en el caso de que esta pregunta se encuentre en una sección maestro-detalle adicionalmente
			permite a esta propia pregunta como la fuente para excluir valores
			*/
			QuestionCls.prototype.getSourceQuestionItems = function() {
				var arrQuestionsIDs = new Array();
				var objQuestionsColl = new Object();
				this.sourceQuestionsIDsColl = arrQuestionsIDs;
				this.sourceQuestionsColl = objQuestionsColl;
				
				if (!selSurvey || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intQuestionNum in selSurvey.questionsOrder) {
					var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
					var objQuestion = selSurvey.questions[intQuestionID];
					if (objQuestion && objQuestion.type == Type.multiplechoice && objQuestion.number < this.number) {
						arrQuestionsIDs.push(intQuestionID);
						objQuestionsColl[intQuestionID] = {id:intQuestionID, name:objQuestion.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.sourceQuestionsIDsColl = arrQuestionsIDs;
				this.sourceQuestionsColl = objQuestionsColl;
				
				return objQuestionsColl;
			}
			
			QuestionCls.prototype.getSourceQuestionItemsOrder = function() {
				return this.sourceQuestionsIDsColl;
			}
			
			/* Obtiene la lista de preguntas simple o múltiple choice previas a la de esta instancia, y en el caso de que esta pregunta se encuentre en una sección maestro-detalle 
			adicionalmente permite a esta propia pregunta como la fuente para excluir valores
			*/
			QuestionCls.prototype.getSourceExcludeQuestionItems = function() {
				var arrQuestionsIDs = new Array();
				var objQuestionsColl = new Object();
				this.sourceExcludeQuestionsIDsColl = arrQuestionsIDs;
				this.sourceExcludeQuestionsColl = objQuestionsColl;
				
				if (!selSurvey || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intQuestionNum in selSurvey.questionsOrder) {
					var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
					var objQuestion = selSurvey.questions[intQuestionID];
					if (objQuestion && (objQuestion.type == Type.simplechoice || objQuestion.type == Type.multiplechoice) && objQuestion.number < this.number) {
						arrQuestionsIDs.push(intQuestionID);
						objQuestionsColl[intQuestionID] = {id:intQuestionID, name:objQuestion.getName()};
					}
				}
				
				var objSection = undefined;
				if ((this.type == Type.simplechoice || this.type == Type.multiplechoice) && selSurvey && selSurvey.sections) {
					var objSection = selSurvey.sections[this.sectionID];
					//Si la propia pregunta es de sección maestro-detalle, entonces puede excluir de ella misma
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType
					if (objSection && (objSection.sectionType == SectionType.Multiple)) {	//|| objSection.repetitions
						arrQuestionsIDs.push(this.id);
						objQuestionsColl[this.id] = {id:this.id, name:this.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.sourceExcludeQuestionsIDsColl = arrQuestionsIDs;
				this.sourceExcludeQuestionsColl = objQuestionsColl;
				
				return objQuestionsColl;
			}
			
			QuestionCls.prototype.getSourceExcludeQuestionItemsOrder = function() {
				return this.sourceExcludeQuestionsIDsColl;
			}
			
			/* Obtiene la lista de preguntas simple y múltiple choice previas a la de esta instancia, que no tengan compartidas sus opciones de respuesta, para permitir que esta pregunta
			comparta con ella sus opciones */
			QuestionCls.prototype.getSharedQuestionItems = function() {
				var arrQuestionsIDs = new Array();
				var objQuestionsColl = new Object();
				this.sharedQuestionsIDsColl = arrQuestionsIDs;
				this.sharedQuestionsColl = objQuestionsColl;
				
				if (!selSurvey || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intQuestionNum in selSurvey.questionsOrder) {
					var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
					var objQuestion = selSurvey.questions[intQuestionID];
					//JAPR 2015-08-08: Validado que sólo apliquen preguntas simple o múltiple choice (#L39D3J)
					if (!objQuestion || (objQuestion.type != Type.simplechoice && objQuestion.type != Type.multiplechoice) || objQuestion.valuesSourceType != TypeOfFill.tofFixedAnswers) {
						continue;
					}
					
					if (objQuestion.number < this.number && (!objQuestion.sharedQuestionID || objQuestion.sharedQuestionID <= 0)) {
						arrQuestionsIDs.push(intQuestionID);
						objQuestionsColl[intQuestionID] = {id:intQuestionID, name:objQuestion.getName()};
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.sharedQuestionsIDsColl = arrQuestionsIDs;
				this.sharedQuestionsColl = objQuestionsColl;
				
				return objQuestionsColl;
			}
			
			QuestionCls.prototype.getSharedQuestionItemsOrder = function() {
				return this.sharedQuestionsIDsColl;
			}
			
			/* Obtiene la lista de preguntas que se pueden ocultar/mostrar desde esta pregunta
				Inicialmente este obejto se preparó para un componente GFieldTypes.multiList, el cual solo soporta textos sin IDs, así que antepone el # de pregunta a cada texto para
				hacerlos únicos, internamente se convertirán a IDs mientras se procesa el componente
			*/
			QuestionCls.prototype.getShowQuestionItems = function() {
				var arrQuestionsIDs = new Array();
				var objQuestionsColl = new Array();
				this.showQuestionsIDsColl = arrQuestionsIDs;
				this.showQuestionsColl = objQuestionsColl;
				
				if (!selSurvey || !selSurvey.questions || !selSurvey.questionsOrder) {
					return;
				}
				
				for (var intQuestionNum in selSurvey.questionsOrder) {
					var intQuestionID = selSurvey.questionsOrder[intQuestionNum];
					var objQuestion = selSurvey.questions[intQuestionID];
					if (objQuestion && objQuestion.number > this.number) {
						arrQuestionsIDs.push(intQuestionID);
						objQuestionsColl.push(objQuestion.number + " - " + objQuestion.getName());
					}
				}
				
				//Actualiza la instancia con la última colección calculada
				this.showQuestionsIDsColl = arrQuestionsIDs;
				this.showQuestionsColl = objQuestionsColl;
				
				return objQuestionsColl;
			}
			
			QuestionCls.prototype.getShowQuestionItemsOrder = function() {
				return this.showQuestionsIDsColl;
			}
			
			//******************************************************************************************************
			OptionCls.prototype = new GenericItemCls();
			OptionCls.prototype.constructor = OptionCls;
			function OptionCls() {
				this.objectType = AdminObjectType.otyOption;
				this.objectIDFieldName = "QuestionOptionID";
				this.parentFields = {QuestionID:"questionID"};
			}
			
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			OptionCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["name"]) {
					//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
					//objParams["QuestionText"] = this.name;
					objParams["QuestionOptionName"] = this.name;
				}
				if (blnAllFields || oFields["nextSection"]) {
					objParams["NextQuestion"] = this.nextSection;
				}
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				if (blnAllFields || oFields["displayImageDes"]) {
					objParams["DisplayImage"] = this.displayImageDes;
				}
				//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
				if (blnAllFields || oFields["displayMenuImageDes"]) {
					objParams["DisplayMenuImage"] = this.displayMenuImageDes;
				}
				/*
				if (blnAllFields || oFields["displayImage"]) {
					objParams["DisplayImage"] = this.displayImage;
				}
				*/
				//JAPR
				if (blnAllFields || oFields["displayText"]) {
					objParams["HTMLText"] = this.displayText;
				}
				if (blnAllFields || oFields["phoneNum"]) {
					objParams["phoneNum"] = this.phoneNum;
				}
				if (blnAllFields || oFields["hidden"]) {
					objParams["Visible"] = this.hidden;
				}
				if (blnAllFields || oFields["cancelAgenda"]) {
					objParams["CancelAgenda"] = this.cancelAgenda;
				}
				if (blnAllFields || oFields["defaultValue"]) {
					objParams["DefaultValue"] = this.defaultValue;
				}
				if (blnAllFields || oFields["score"]) {
					objParams["Score"] = this.score;
				}
				if (blnAllFields || oFields["showQuestionList"]) {
					objParams["ShowQuestionList"] = this.showQuestionList;
				}
				
				return objParams;
			}
			
			//JAPR 2016-04-15: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			//******************************************************************************************************
			SkipRuleCls.prototype = new GenericItemCls();
			SkipRuleCls.prototype.constructor = SkipRuleCls;
			function SkipRuleCls() {
				this.objectType = AdminObjectType.otySkipRule;
				this.objectIDFieldName = "SkipRuleID";
				this.parentFields = {QuestionID:"questionID"};
			}
			
			/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
			//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
			*/
			SkipRuleCls.prototype.getDataFields = function(oFields) {
				var blnAllFields = false;
				if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
					blnAllFields = true;
				}
				
				var objParams = {};
				if (blnAllFields || oFields["name"]) {
					objParams["RuleName"] = this.name;
				}
				if (blnAllFields || oFields["nextSection"]) {
					objParams["NextSectionID"] = this.nextSection;
				}
				if (blnAllFields || oFields["skipCondition"]) {
					objParams["SkipCondition"] = this.skipCondition;
				}
				
				return objParams;
			}
			//JAPR
			
			//******************************************************************************************************
			function TabCls(sObjectDef) {
				this.values = new Object();							//Colección de los valores en los campos tipo Combo o Lista
				this._values = new Object();						//Array interno posterior a evaluar getvalues
				this.valuesOrder = new Array();						//Array con los Ids de opciones ordenados tal como se deben ver
				this._valuesOrder = new Array();					//Array interno posterior a evaluar getvalues
				
				//Extiende las propiedades default si es que fueron especificadas
				if (sObjectDef) {
					$.extend(this, sObjectDef);
				}
				
				/* En caso de que el objeto values sea una función, devuelve la lista real de valores y actualiza el array valuesOrder utilizando dicha función */
				this.getValues = function() {
					this.useOrder = true;
					this._values = this.values;
					this._valuesOrder = this.valuesOrder;
					if (!this._valuesOrder || ($.isArray(this._valuesOrder) && !this._valuesOrder.length)) {
						this.useOrder = false;
						this._valuesOrder = this._values;
					}
					
					//Si se trata de funciones, las ejecuta para obtener la colección real de valores
					if (typeof this.values == "function") {
						this._values = this.values();
						if (!this._values) {
							this._values = new Object();
						}
					}
					if (typeof this.valuesOrder == "function") {
						this._valuesOrder = this.valuesOrder();
						if (!this._valuesOrder) {
							this._valuesOrder = new Array();
						}
					}
					
					return this._values;
				}
			}
			
			//******************************************************************************************************
			function FieldCls(sObjectDef) {
				//Constructor
				this.id = 0;											//ID numérico del objeto que se está editando (SurveyID/SectionID/QuestionID según el caso)
				this.name = "";											//Nombre único del campo
				this.label = "";										//Etiqueta para desplegar en la lista de configuraciones del objeto
				this.type = GFieldTypes.alphanum;						//Tipo de componente para captura
				this.parentType = AdminObjectType.otyQuestion;			//Tipo de objeto del que se extraen los valores (Survey/Section/Question)
				this.maxLength = 0;										//Sólo para tipos con texto, es la longitud del texto a capturar
				this.showWhenNew = true;								//Indica si el campo debe ser o no capturable cuando el objeto es nuevo (id == -1) o sólo al editarlo
				this.useOrder = true;									//Indica si está o no definido un array de optionsOrder para ordenar el contenido de la combo (se asigna automáticamente al ejecutar getOptions)
				this.options = new Object();							//Colección de los valores en los campos tipo Combo o Lista (puede ser una función que obtiene dicho array)
				this._options = new Object();							//Array interno posterior a evaluar getOptions
				this.optionsOrder = new Array();						//Array con los Ids de opciones ordenados tal como se deben ver (puede ser una función que obtiene dicho array)
				this._optionsOrder = new Array();						//Array interno posterior a evaluar getOptions
				this.parentField = undefined;							//Referencia al campo del que dependen las opciones de respuesta
				this.tab = "";											//Tab al cual pertenece el campo
				this.customButtons = new Array;							//Definir cuales son los botones personalizados que se desplegarán en el editor
																		//Botones por default: bold, italic, underline
																		//Botones personalizados (configurables en customButtons):
																		//backColor, fontName, fontSize, foreColor
				this.readOnly = false;									//Indica si se debe o no permitir editar el campo (si no se permite, se cambia a "ro" su tipo interno)
				this.resize = undefined;								//Sólo modo "collection". Indica si la columna permitirá el auto-resize o no (default == true)
				this.default = undefined;								//Valor default del campo (puede ser una función que regrese el valor a utilizar)
				this.required = false;									//Indica si el campo es requerido. Eventualmente se podrá validar, en este momento sólo toma el default como valor si no se captura nada
				//JAPR 2015-09-02: Agregada la configuración del Height para el componente Editor
				this.height = undefined;								//Indica el alto de ciertos componentes (originalmente usado para el componente Editor DHTMLX)
				//JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				this.disabled = undefined;								//Indica si el campo debe permanecer deshabilitado. A diferencia de readOnly, esto no cambia el tipo interno, simplemente no permitirá iniciar el EditCell de las celdas (y en el caso de componentes especiales bloqueará u ocultará parte del mismo para deshabilitar)
																		//Se integró después de readOnly y su uso fue mas general para bloquear la edición masiva de propiedades y extenderla a las celdas que no seguían el estandar de DHTML como el EditorDHTMLX, el cual se genera en el setValue en lugar del método edit del tipo personalizado
				
				//Extiende las propiedades default si es que fueron especificadas
				if (sObjectDef) {
					$.extend(this, sObjectDef);
				}
			}
			
			//JAPR 2015-07-31: Corregido el problema de refresh de los campos tipo selList, el problema era que la función refreshValue no era parte del prototype
			//sino que se agregaba dinámicamente con cada nueva instancia, eso impedía que el parámetro con una pre-definición enviado al constructor aplicara
			//una función personalizada porque inmediatamente era reemplazada por la default, así que se cambió la implementación a un prototype para permitir la
			//correcta herencia de las funciones default y aún así sobreescribir lo necesario al crear instancias, no se tabuló correctamente (#2X9V34)
				/* En caso de que el objeto options sea una función, devuelve la lista real de objetos y actualiza el array optionsOrder utilizando dicha función
				JAPR 2015-07-15: Agregado el caso en el que el campo tiene un campo padre del cual depende para obtener sus options (sólo aplica a ciertos tipos de campos como combo o 
				selList)
				//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
				Agregado el parámetro iParentValue para indicar que se deben heredar las opciones de una de las opciones específico del padre que no necesariamente es la respuesta actual
				de ese campo (usado desde onEditCell, ya que se repetía la asignación de las opciones a partir de los children del Padre) basado en el valor de la Row, aunque debería
				haber sido lo mismo que el valor del Field que ya se obtiene internamente en este punto)
				*/
				FieldCls.prototype.getOptions = function(iParentValue) {
					this.useOrder = true;
					this._options = this.options;
					this._optionsOrder = this.optionsOrder;
					
					//JAPR 2015-07-15: Agregado el caso en el que el campo tiene un campo padre del cual depende para obtener sus options (sólo aplica a ciertos tipos de campos)
					//Si hubiera un campo padre configurado, entonces tiene que obtener las opciones directamente de él, por lo que no aplica la parte de ejecutar la función de options
					//que se encuentra mas abajo
					if (this.parent) {
						var blnForForm = (this.parentType == AdminObjectType.otySurvey);
						var objParentField = getFieldDefinition(this.tab, this.parent, blnForForm);
						if (objParentField && objParentField.options) {
							//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
							if (iParentValue !== undefined) {
								var intParentValue = iParentValue;
							}
							else {
								var intParentValue = objParentField.getValue(true);
								if (!$.isNumeric(intParentValue)) {
									if (objParentField.default !== undefined) {
										intParentValue = objParentField.default;
									}
								}
							}
							//JAPR
							
							var blnUseOrder = true;
							var objParentOptions = new Object();
							var objParentOptionsOrder = new Array();
							if (objParentField.getOptions) {
								var objParentOptions = objParentField.getOptions();
								var objParentOptionsOrder = objParentField._optionsOrder;
								var blnUseOrder = objParentField.useOrder;
							}
							
							var objParentOption = objParentOptions[intParentValue];
							if (objParentOption && objParentOption.children) {
								//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
								//Agregado el concepto de hijos con condiciones especiales (originalmente usado para los atributos tipo imagen). En este caso la función debe agregar
								//todas las posibles condiciones buscando una propiedad específica en el campo, para comparar elemento con elemento en los children en busca de coincidencia
								//con la misma propiedad
								if (this.filterProp) {
									//Se buscan sólo los elementos (atributos) imagen de la colección
									this._options = new Object();
									
									//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
									var blnUserOder = false;
									if (objParentOption.childrenOrder) {
										this._optionsOrder = new Array();
										blnUserOder = true;
									}
									
									//Agrega los elementos que cumplen la condición
									if (blnUserOder) {
										for (var intIndex in objParentOption.childrenOrder) {
											var intObjectID = objParentOption.childrenOrder[intIndex];
											if (intObjectID !== undefined) {
												var objObject = objParentOption.children[intObjectID];
												if (objObject && objObject[this.filterProp]) {
													this._options[intObjectID] = objObject;
													this._optionsOrder.push(intObjectID);
												}
											}
										}
									}
									else {
										for (var intObjectID in objParentOption.children) {
											var objObject = objParentOption.children[intObjectID];
											if (objObject && objObject[this.filterProp]) {
												this._options[intObjectID] = objObject;
												if (blnUserOder) {
													//Agrega el orden de los elementos según la condición
													this._optionsOrder.push(intObjectID);
												}
											}
										}
									}
								}
								else {
									this._options = objParentOption.children;
									if (!this._options) {
										this._options = new Object();
									}
									
									//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
									this._optionsOrder = objParentOption.childrenOrder;
								}
								
								if (!this._optionsOrder || ($.isArray(this._optionsOrder) && !this._optionsOrder.length)) {
									this.useOrder = false;
									this._optionsOrder = this._options;
								}
								//JAPR
							}
						}
					}
					else {
						if (!this._optionsOrder || ($.isArray(this._optionsOrder) && !this._optionsOrder.length)) {
							this.useOrder = false;
							this._optionsOrder = this._options;
						}
						
						//Si se trata de funciones, las ejecuta para obtener la colección real de valores
						if (typeof this.options == "function") {
							this._options = this.options();
							if (!this._options) {
								this._options = new Object();
							}
						}
						if (typeof this.optionsOrder == "function") {
							this._optionsOrder = this.optionsOrder();
							if (!this._optionsOrder) {
								this._optionsOrder = new Array();
							}
						}
					}
					
					return this._options;
				}
				
				/* Actualiza el valor de un campo hijo a partir del valor del campo padre según el tipo de campo del que se trate
				Si el componente es complejo, o si por alguna razón requiere un tratamiendo diferente, se debe sobreescribir esta función directamente en la definición del campo
				durante la definición de la lista de campos de un Grid específico, de lo contrario usará la función default tal como se programe en este punto
				//JAPR 2015-08-19: Agregado el parámetro vValue para forzar un valor a usar en el refresh, si es undefined se asume que se usarán los defaults previstos
				*/
				FieldCls.prototype.refreshValue = function(vValue) {
					console.log('FieldCls.refreshValue ' + this.name);
					
					switch (this.type) {
						case GFieldTypes.combo:
							//Cuando se trata de una combo, verifica si el valor actual de este campo se encuentra entre la lista de elementos hijo del padre, si no es así
							//cambia el valor default a 0 (o al default si hay alguno definido) ya que se asume que todos son IDs > 0 cuando son válidos
							var intValue = this.getValue(true);
							var blnUseOrder = true;
							var cmbOptions = new Object();
							var cmbOptionsOrder = new Array();
							if (this.getOptions) {
								var cmbOptions = this.getOptions();
								var cmbOptionsOrder = this._optionsOrder;
								var blnUseOrder = this.useOrder;
							}
							
							var blnFound = false;
							for (var intOption in cmbOptions) {
								if (intOption == intValue) {
									blnFound = true;
									break;
								}
							}
							
							//Si al final no encontró el valor entre la lista de opciones, debe utilizar la opción defualt o simplemente limpiar el contenido
							//JAPR 2015-08-19: Corregido un bug, el if de valor no encontrado también englobaba a la actualización del grid
							if (!blnFound) {
								//JAPR 2015-08-19: Agregado el parámetro vValue para forzar un valor a usar en el refresh, si es undefined se asume que se usarán los defaults previstos
								if (vValue !== undefined) {
									intValue = vValue;
								}
								else {
									if (this.default !== undefined) {
										intValue = this.default;
									}
									else {
										intValue = 0;
									}
								}
								//JAPR
							}
							
							var strValue = this.getValue(false);
							this.setValue(undefined, undefined, intValue);
							if (objPropsTabs) {
								var objTempGrid = undefined;
								if (objPropsTabs.tabs(this.tab)) {
									objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
									if (objTempGrid) {
										objTempGrid.bitDisableEvents = true;
										objTempGrid.setRowAttribute(this.name, "value", intValue);
										objTempGrid.cells(this.name, 1).setValue(strValue);
										objTempGrid.bitDisableEvents = false;
									}
								}
							}
							break;
						
						//JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
						case GFieldTypes.formula:
							var strValue = this.getValue(false);
							/*this.setValue(undefined, undefined, strValue);*/
							
							if (objPropsTabs) {
								var objTempGrid = undefined;
								if (objPropsTabs.tabs(this.tab)) {
									objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
									if (objTempGrid) {
										objTempGrid.bitDisableEvents = true;
										objTempGrid.setRowAttribute(this.name, "value", strValue);
										objTempGrid.cells(this.name, 1).setValue(strValue);
										objTempGrid.bitDisableEvents = false;
									}
								}
							}
							break;
						//JAPR
					}
				}
				
				/* Obtiene el valor del campo directamente del objeto en memoria que representa a la forma (selSurvey)
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				//JAPR 2015-06-29: Agregados los parámetros iObjectType e iObjectID para permitir variar el tipo de objeto del que se extraen las propiedades. Utilizado durante la generación
				de tabs tipo collection en la que los valores representan objetos hijos del definido inicialmente en la tab. Si no se especifican, se usarán los defindos en el FieldCls
				*/
				FieldCls.prototype.getValue = function(bRaw, iObjectType, iObjectID) {
					console.log('FieldCls.getValue ' + this.name + ', bRaw == ' + bRaw);
					
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return "";
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					var varValue = objObject[this.name];
					if (bRaw) {
						//El valor raw es en que finalmente se graba en la metadata, así que se obtiene directamente del objeto en cuestión
						switch (this.type) {
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
						}
					}
					else {
						//El valor no raw es el que el componente requiere para pintarse correctamente en el grid, así que puede recibir tratamiento especial
						switch (this.type) {
							//JAPR 2015-07-15: Agregado el componente selList
							case GFieldTypes.selList:
								//En el caso de una lista de selección, el valor por default es un array con los Ids de cada elemento de la lista de seleccionados, así que es igual que
								//el valor real del campo por lo que no es necesario hacer ajustes (es un array para permitir la lista ordenada según lo que define el usuario, las descripciones
								//se obtienen a partir de las opciones del campo)
								/*
								var varTempValue = new Object();
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intIdx in varValue) {
										var intObjectID = varValue[intIdx];
										for (var intId in cmbOptionsOrder) {
											var intTmpObjectID = cmbOptionsOrder[intId];
											if (intTmpObjectID == intObjectID) {
												varTempValue[intObjectID] = cmbOptions[intObjectID];
												break;
											}
										}
									}
								}
								varValue = varTempValue;
								*/
								break;
						
							case GFieldTypes.multiList:
								//En el caso de una lista múltiple de selección, el valor por default es el conjunto de las descripciones del valor real del campo, el cual es un array
								var varTempValue = '';
								var strAnd = '';
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intIdx in varValue) {
										var intObjectID = varValue[intIdx];
										for (var intId in cmbOptionsOrder) {
											var intTmpObjectID = cmbOptionsOrder[intId];
											if (intTmpObjectID == intObjectID) {
												varTempValue += strAnd + cmbOptions[intId];
												strAnd = ',';
												break;
											}
										}
									}
								}
								varValue = varTempValue;
								break;
						
							case GFieldTypes.grid:
								//Si se trata de un grid, se usará un valor dummy ya que se va a generar programáticamente
								varValue = 1;
								break;
						
							case GFieldTypes.image:
								//Si se trata de una imagen, en caso de no venir asignado el valor directo con el nombre del campo, se verifica si está definida una imagen
								//en el campo y en ese caso se regresa directamente la imagen de la definición, la cual funciona como un default
								if (varValue === undefined && this.image) {
									varValue = this.image;
								}
								
								//Para el grid la imagen separa sus propiedades en el formato: urlImagen^alttext^javascript_OR_url^target
								var strLink = this.click;
								if (strLink) {
									//JAPR 2019-08-23: Corregido un bug, por alguna razón sin especificar el target, comenzó a abrir una nueva ventana (#FHTNG4)
									varValue = varValue + "^^" + strLink + "^_self";
									//JAPR
								}
								break;
						
							//case GFieldTypes.comboSync:
							case GFieldTypes.combo:
								//Si se trata de una combo, debe buscar el valor dentro de las opciones disponibles del campo y regresar el texto que la representa
								//Los posibles valores de estas combos tienen que ser IDs enteros, así que en caso de no ser un valor entero, se asumirá 0
								//(esto puede pasar si el objeto en cuestión no tiene la propiedad indicada porque no aplicaba, y generalmente pasará sólo con
								//las combos que tienen opciones de Si/No)
								if (!$.isNumeric(varValue) && this.default !== undefined) {
									varValue = this.default;
								}
								
								var blnFound = false;
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (this.getOptions) {
									var cmbOptions = this.getOptions();
									var cmbOptionsOrder = this._optionsOrder;
									var blnUseOrder = this.useOrder;
								}
								
								if (cmbOptions) {
									for (var intId in cmbOptions) {
										if (intId == varValue) {
											var objValue = cmbOptions[intId];
											varValue = (objValue.name)?objValue.name:objValue;
											blnFound = true;
											break;
										}
									}
								}
								
								//Si se permite un elemento vacío, automáticamente usa esa descripción como el valor inicial
								if (!blnFound && this.empty) {
									varValue = "(<?=translate("None")?>)";
									blnFound = true;
								}
								
								if (!blnFound) {
									varValue = "<?=translate("Unknown")?>";
								}
								break;
						}
					}
					
					return  varValue;
				}
				
				FieldCls.prototype.setValue = function(iObjectType, iObjectID, val) {
					console.log('FieldCls.setValue ' + this.name + ', val == ' + val);
					
					//Accesar al objeto selSurvey (al nivel del frame principal), al Survey/Section/Question correspondiente, para pedirle la propiedad del name
					var objObject = getObject(this.parentType, this.id, iObjectType, iObjectID);
					if (!objObject) {
						return;
					}
					
					//El valor numérico en caso de tratarse de una propiedad tipo combo
					objObject[this.name] = val;
				}

				/* Obtiene el valor del campo directamente del grid donde se está definiendo la propiedad, así que es el valor que habría configurado el usuario en caso de haberlo
				cambiado
				Si se especifica el parámetro bRaw, entonces se obtendrá el valor natural del campo (por ejemplo el integer con el id en caso de ser una combo), de lo contrario se
				obtendrá el valor string para ser presentado en el Grid, el cual es sólo una representación visual y no se debe usar para grabar nada
				*/
				FieldCls.prototype.getPropValue = function(bRaw) {
					console.log('FieldCls.getPropValue ' + this.name + ', bRaw == ' + bRaw);
					
					if (!objPropsTabs) {
						return "";
					}
					
					var varValue = ""
					var objTempGrid = undefined;
					if (objPropsTabs.tabs(this.tab)) {
						objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
						if (objTempGrid) {
							if (bRaw) {
								varValue = objTempGrid.getRowAttribute(this.name, "value");
							}
							else {
								if (objTempGrid.cells(this.name, 1)) {
									varValue = objTempGrid.cells(this.name, 1).getValue();
								}
							}
							
							if (varValue === undefined || varValue === null || varValue === NaN) {
								varValue = "";
							}
						}
					}
					
					return varValue;
				}
				
				/* Basado en el valor del campo y el tipo, recorre las opciones para aplicar visibilidad encadenada de otros campos si tiene alguna regla configurada*/
				FieldCls.prototype.applyVisibilityRules = function() {
					console.log('FieldCls.applyVisibilityRules ' + this.name);
					
					var intValue = this.getValue(true);
					var blnUseOrder = true;
					var cmbOptions = new Object();
					var cmbOptionsOrder = new Array();
					if (this.getOptions) {
						var cmbOptions = this.getOptions();
						var cmbOptionsOrder = this._optionsOrder;
						var blnUseOrder = this.useOrder;
					}
					
					//Primero oculta todos los elementos que otras opciones deben mostrar
					var objSelectedOption = undefined;
					for (var intOption in cmbOptions) {
						var objOption = undefined;
						if (intOption == intValue) {
							objSelectedOption = cmbOptions[intOption];
						}
						else {
							var objOption = cmbOptions[intOption];
						}
						
						if (objOption && objOption.visibilityRules) {
							fnShowHideItems(objOption, false);
						}
					}
					
					//Al terminar muestra los elementos de la opción seleccionada
					if (objSelectedOption && objSelectedOption.visibilityRules) {
						fnShowHideItems(objSelectedOption, true);
						
						//JAPR 2015-08-21: Modificado para encadenar la visibilidad de campos cuando se está aplicando a algún padre
						var objFields = objSelectedOption.visibilityRules.fields;
						if (objFields) {
							var blnForForm = (this.parentType == AdminObjectType.otySurvey);
							for (var intIndex = 0; intIndex < objFields.length; intIndex++) {
								var objFieldData = objFields[intIndex];
								if (!objFieldData) {
									continue;
								}
								
								var strFieldName = objFieldData.fieldId;
								var strTabName = objFieldData.tabId;
								var objField = getFieldDefinition(strTabName, strFieldName, blnForForm);
								if (objField && objField.applyVisibilityRules) {
									objField.applyVisibilityRules();
								}
							}
						}
					}
				}
				
				// Utilizada para validar mínimos, máximos o cualquier otra cosa que se requiera en el field
				FieldCls.prototype.validate = function() {
					return true;
				}
				
				// Determina si es o no visible la configuración
				FieldCls.prototype.visible = function() {
					return true;
				}
				
				/* Oculta el campo especificado
				*/
				FieldCls.prototype.hide = function() {
					console.log('FieldCls.hide ' + this.name);
					
					if (!objPropsTabs) {
						return;
					}
					
					var objTempGrid = undefined;
					if (objPropsTabs.tabs(this.tab)) {
						objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
						if (objTempGrid) {
							objTempGrid.setRowHidden(this.name, true);
						}
					}
				}
				
				/* Muestra el campo especificado
				*/
				FieldCls.prototype.show = function() {
					console.log('FieldCls.show ' + this.name);
					
					if (!objPropsTabs) {
						return;
					}
					
					var objTempGrid = undefined;
					if (objPropsTabs.tabs(this.tab)) {
						objTempGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
						if (objTempGrid) {
							objTempGrid.setRowHidden(this.name, false);
						}
					}
				}
				
/*
			GFieldTypes = {
				alphanum: "ed",				//Texto en una sola linea editado inline
				text: "edtxt",				//Text area multiline
				combo: "coro",				//Combo simple (Combo methods && props: save(), restore(), size(), get(sidx), getKeys(), clear(), put(sidx, val), remove(sidx) .keys [array], .values [array]
				comboSync: "comboExt",		//Combo sincronizable con otras combos (Padres e hijas, por ejemplo, los atributos del catálogo seleccionado) (Personalizado)
				check: "ch",				//Checkbox
				editor: "editor",			//DHTMLXEditor
				number: "edn",				//Valores numéricos con máscara de captura
				date: "dhxCalendarA",		//Calendario con posibilidad de escribir la fecha
				hour: "time",				//Componente para capturar hora y minutos
				multiList: "clist",			//Lista con checkbox para seleccionar varios elementos
				color: "cp"					//Permite seleccionar el color de un conjunto predeterminado de posibilidades (personalizarlo al JColor que manejamos actualmente)
				image: "img"				//Una imagen con un click a una función
			};
*/
				//Agrega la row que representa este campo/configuración en el grid correspondiente
				FieldCls.prototype.draw = function(oGrid, iObjectType, iObjectID) {
					console.log('FieldCls.draw ' + this.name);
					
					rowValues = new Array;
					//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
					//Si el componente es una lista seleccionable, se debe colocar en la primer celda y no en la segunda como los demás, además se aplica un colSpan para que se vea a lo
					//largo de las dos columnas
					//JAPR 2015-11-12: Corregido un bug, por la manera en que funciona DHTMLX, si se va a cambiar el tipo de la celda, no se puede asignar primero un valor pues al
					//invocar al cambio del tipo, primero se leerá el valor con el tipo anterior y tal cual se va a sobreescribir en el campo con el tipo nuevo, así que podría quedar
					//dañado (esto es especialmente evidente cuando el tipo original permitía HTML, se le asigna un HTML Entity, y luego se cambia a un tipo solo texto, la HTML Entity
					//quedará visible en lugar de mostrar el caracter que representa (#WN65X7)
					if (this.type == GFieldTypes.selList) {
						rowValues[0] = this.getValue(undefined, iObjectType, iObjectID);
						rowValues[0] = "";
					}
					else {
						rowValues[0] = this.label;
						//JAPR 2015-11-12: Para corregir el bug identificado en el comentario de este día, ahora se asignará el valor luego de cambiar el tipo (#WN65X7)
						rowValues[1] = "";	//this.getValue(undefined, iObjectType, iObjectID);
						//JAPR
					}
					
					//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
					//agregando una bandera temporal al grid
					oGrid.bitDisableEvents = true;
					oGrid.addRow(this.name, rowValues);
					oGrid.setRowAttribute(this.name, "tabName", this.tab);
					oGrid.setRowAttribute(this.name, "fieldName", this.name);
					//oGrid.setRowAttribute(this.name, "options", this.options);
					oGrid.setRowAttribute(this.name, "type", this.type);
					oGrid.setRowAttribute(this.name, "objectType", this.parentType);
					oGrid.setRowAttribute(this.name, "value", this.getValue(true, iObjectType, iObjectID));
					if (this.hidden) {
						oGrid.setRowHidden(this.name, true);
					}
					//JAPR 2016-12-15: Agregada la validación de ciertos campos según la propiedad validate de su definición (#IRERJD)
					/*if (this.validate) {
						//Si existe una regla de validación la agrega a la celda exclusivamente
						oGrid.cells(this.name, 1).setAttribute("validate", this.validate);
					}*/
					
					//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
					//Si el componente es una lista seleccionable, se debe colocar en la primer celda y no en la segunda como los demás, además se aplica un colSpan para que se vea a lo
					//largo de las dos columnas
					if (this.type == GFieldTypes.selList) {
						oGrid.enableColSpan(true);
						oGrid.setCellExcellType(this.name, 0, this.type);
						oGrid.setColspan(this.name, 0, 2);
					}
					else {
						//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
						//Debido a que durante el cambio de tipo de celda se va a asignar automáticamente el valor, en el caso de los campos tipo Editor se estaban generando los popups
						//tanto durante la asignación del valor dentro de setCellExcellType, como posteriormente en la llamada subsecuente a setValue que ya es la que va a dejar el valor
						//real del campo, así que el proceso agregado este día para remover los popups previamente creados lo hacía bien, pero esta doble invocación a setValue generaba
						//pares de popups siendo que solo uno de ellos en cada caso quedaba registrado (el de la llamada a setValue), así que se estaban agregando componentes al DOM que no
						//eran removidos en ningún evento, con esta propiedad bitChangingCellType se evitarán generar los eventos no deseados durante el cambio de tipo de celda
						oGrid.bitChangingCellType = true;
						oGrid.setCellExcellType(this.name, 1, this.type);
						oGrid.bitChangingCellType = false;
						//JAPR 2015-11-12: Para corregir el bug identificado en el comentario de este día, ahora se asignará el valor luego de cambiar el tipo (#WN65X7)
						oGrid.cells(this.name, 1).setValue(this.getValue(undefined, iObjectType, iObjectID));
						//JAPR
					}
					if (this.type == GFieldTypes.multiList) {
						var arrOptions = new Array();
						for (var intOptionNum in this.options) {
							arrOptions.push(this.options[intOptionNum]);
						}
						oGrid.registerCList(1, arrOptions);
					}
					
					//JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if (this.disabled) {
						oGrid.cells(this.name, 1).setDisabled(true);
					}
					
					oGrid.bitDisableEvents = false;
					//JAPR
					
					return true;
				}
			
			RegExp.quote = function(str) {
				return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
			};
			
			/* Invoca la inicialización del App de eForms y la interfaz del diseñador
			*/
			function doOnLoad() {
				console.log('doOnLoad');
				
				if (!selSurvey) {
					alert('<?=translate("You can not access this survey.")?>');
					return;
				}
				
				//Agrega clases extendidas a los objetos de definición leídos
				addClassPrototype();
				
				//Agrega estilos adicionales para corregir issues estéticos del DHTMLX
				//Para la DataView de secciones, aparecía un borde azul a la derecha y se veía mal, así que se remueve dinámicamente
				//$("head").append("<style> .simpleClass{ display:none; } </style>");
				
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
				objWindowsFn = new dhtmlXWindows({
					image_path:"images/"
				});
				objWindowsEd = new dhtmlXWindows({
					image_path:"images/"
				});
				
				//@JAPR 2016-07-08: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				objMainLayout = new dhtmlXLayoutObject({
					parent: document.body,
					pattern:"1C"
				});
				//objMainLayout.cells(dhxHeaderCell).appendObject("divDesignHeader");
				//objMainLayout.cells(dhxHeaderCell).hideHeader();
				objMainLayout.setSizes();
				
				//Prepara la tabbar con las opciones principales de la forma
				//@JAPR 2016-07-08: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				objFormTabs = objMainLayout.cells(dhxBodyCell).attachTabbar({align:"left", mode:"top"});
				objFormTabs.addTab(tabDesign, "<span class='tabFormsOpts'><?=translate("Design")?></span>", null, null, true);
				objFormTabs.addTab(tabUsers, "<span class='tabFormsOpts'><?=translate("Invite users")?></span>");
				objFormTabs.addTab(tabGroups, "<span class='tabFormsOpts'><?=translate("Invite groups")?></span>");
				objFormTabs.addTab(tabModel, "<span class='tabFormsOpts'><?=translate("Model")?></span>");
				objFormTabs.addTab(tabDataDestinations, "<span class='tabFormsOpts'><?=translate("Data destinations")?></span>");
				objFormTabs.addTab(tabDetail, "<span class='tabFormsOpts'><?=translate("Form details")?></span>");
				objFormTabs.addTab(tabOnlineEntry, "<span class='tabFormsOpts'><?=translate("Online Entry")?></span>");
<?				if (getMDVersion() >= esvCopyForms) {?>
				objFormTabs.addTab(tabCopyForm, "<span class='tabFormsOpts'><?=translate("Copy form")?></span>");
<?				}

				if (getMDVersion() >= esvCopyForms) {
					if ($this->FormType > formProduction) {
?>
				objFormTabs.addTab(tabDevForm, "<span class='tabFormsOpts<?=(($this->FormType == formProdWithDev)?"NoSel":"")?>'><?=translate(($this->FormType == formProdWithDev)?"Production":"Synchronize to production")?></span>");
<?
					}
					else {
?>
				objFormTabs.addTab(tabDevForm, "<span class='tabFormsOpts'><?=translate("Development")." ".translate("Form")?></span>");
<?					}
				}
?>
				objFormTabs.setArrowsMode("auto");
				
				//******************************************************************************************************
				//Prepara el Layout del diseñador
				//RV-Mar2015: El div del dataview de secciones ya no se utilizará por lo tanto se cambia el pattern a 3W para manejar 3 frames verticales
				objFormTabs.tabs(tabDesign).attachLayout({
					parent: document.body,
					pattern: "3W"
				});
				objFormsLayout = objFormTabs.tabs(tabDesign).getAttachedObject();
				
				//JAPR 2015-06-04: Agregada la ventana para asociar usuarios a la forma
				doDrawUsersAssoc();
				//JAPR 2015-06-05: Agregada la ventana para asociar grupos de usuarios a la forma
				doDrawUserGroupsAssoc();
				
				//Evento del cambio de tab para permitir grabar información que hubiera cambiado
				objFormTabs.attachEvent("onTabClick", function(id, lastId) {
					console.log('objFormTabs.onTabClick ' + id + ', lastId == ' + lastId);
					
					//Se deben restaurar las clases personalizadas ya que DHTMLX restablece la clase originalmente aplicada al componente cada vez que ocurre una selección
					setTimeout(function() {
						$(objFormTabs.tabsArea).find(".dhxtabbar_tab").addClass("linkToolbar");
						$(objFormTabs.tabsArea).find(".dhxtabbar_tab").addClass("linktd");
					}, 100);
				});
				
				//@JAPR 2016-07-08: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				objFormTabs.attachEvent("onSelect", function(id, lastId) {
					console.log('objFormTabs.onSelect ' + id + ', lastId == ' + lastId);
					
					switch (id) {
						case tabOnlineEntry:
							browserApp();
							return false;
							break;
						case tabCopyForm:
							//RV Agosto2016: Agregar funcionalidad para copiar formas
							doCopyForm(copyAction);
							return false;
							break;
						case tabDevForm:
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							//La forma de producción no tiene evento, sólo una indicación de que se trata de producción, eso sólo si tiene asociada una forma de desarrollo
							if ($this->FormType == formProdWithDev) {?>
							return false;
							<?
							}
							//La forma de desarrollo agrega el botón de Construir forma de producción para sincronizar los cambios de desarrollo
							elseif ($this->FormType == formDevelopment) {?>
								doBuildProdForm();
								return false;
							<?}
							//La forma de producción sin desarollo contendrá el botón para generar la forma de desarrollo por primera vez
							//@JAPR
							?>
							//Feb 2017: Agregar funcionalidad para generar formas en desarrollo reutilizando el código de copiado de formas
							doCopyForm(devAction);
							return false;
							break;
					}
					
					doChangeTab(id);
					return true;
				});
				
				//JAPR 2016-07-07: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
				//******************************************************************************************************
				//Prepara la Toolbar para manipular las preguntas
				objFormsLayout.cells(dhxPreviewCell).attachToolbar({
					parent:"questionsCreationCell"
				});
				objQuestionsToolbar = objFormsLayout.cells(dhxPreviewCell).getAttachedToolbar();
				objQuestionsToolbar.setIconsPath("<?=$strScriptPath?>/images/admin/");
				var intPos = 0;
				/*2015-06-15@JRPP se descarta la opcion de redimencionar el layout*/
				objFormsLayout.cells(dhxLayOutCell).fixSize(true, true);
				objFormsLayout.cells(dhxPreviewCell).fixSize(true, true);
				objFormsLayout.cells(dhxPropsCell).fixSize(true, true);
				objQuestionsToolbar.addButton("add", intPos++, "<?=translate("Add")?>", "add.png", "add_dis.png");
				objQuestionsToolbar.addButton("move", intPos++, "<?=translate("Move")?>", "move.png", "move_dis.png");
				objQuestionsToolbar.addButton("remove", intPos++, "<?=translate("Remove")?>", "remove.png", "remove_dis.png");
				objQuestionsToolbar.addButton("refresh", intPos++, "<?=translate("Refresh")?>", "refresh.png");
				
				var objSelOptions = new Array();
				for (var strDeviceName in objDevicesColl) {
					var objDevice = objDevicesColl[strDeviceName];
					objSelOptions.push([strDeviceName, "obj", ((objDevice.label)?objDevice.label:objDevice.name), "../devices/icons/"+objDevice.image]);
				}
				objSelOptions.push(["sepDevices", "sep"]);
				objSelOptions.push(["Portrait", "obj", "<?=translate("Portrait")?>", "../devices/icons/portraiticon.png"]);
				objSelOptions.push(["Landscape", "obj", "<?=translate("Landscape")?>", "../devices/icons/landscapeicon.png"]);
				objQuestionsToolbar.addButtonSelect("view", intPos++, "<?=translate("View")?>", objSelOptions, null, null, true, true, 10, "select")
				objQuestionsToolbar.setListOptionSelected("view", "iPhone");
				objQuestionsToolbar.setUserData("view", "deviceSelected", "iPhone");
				//JAPR 2016-07-07: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
				objQuestionsToolbar.setUserData("view", "deviceOrientation", "Portrait");
				
				//OMMC 2015-10-19: Nuevos botones para mover preguntas
				objQuestionsToolbar.addButton("moveQuestionUp", intPos++, "", "up.png", "up_dis.png");
				objQuestionsToolbar.setItemToolTip("moveQuestionUp", "<?=translate("Move question up")?>");
				objQuestionsToolbar.addButton("moveQuestionDown", intPos++, "", "down.png", "down_dis.png");
				objQuestionsToolbar.setItemToolTip("moveQuestionDown", "<?=translate("Move question down")?>");
				//objQuestionsToolbar.addButton("copy", intPos++, "", "remove.png");
				//objQuestionsToolbar.setItemToolTip("copy", "<?=translate("Copy").' '.translate("Form")?>");
				/*Busca con jQuery la toolbar y reduce el tamaño de fuente a 11px para que los botones quepan*/
				$(".dhxlayout_base_dhx_terrace .dhx_cell_toolbar_def").css("font-size", "11px");
				//OMMC
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				if ($this->FormType == formProdWithDev) {?>
				objQuestionsToolbar.disableItem("add");
				objQuestionsToolbar.disableItem("move");
				objQuestionsToolbar.disableItem("remove");
				objQuestionsToolbar.disableItem("moveQuestionUp");
				objQuestionsToolbar.disableItem("moveQuestionDown");
				<?}
				//@JAPR
				?>
				
				//Prepara el PopUp para permitir agregar el selector de secciones
				objQuestionPopUp = new dhtmlXPopup({ 
					toolbar: objQuestionsToolbar,
					id: "add"
				});
				
				objQuestionsToolbar.attachEvent("onClick", function(id) {
					console.log('objQuestionsToolbar.onClick ' + id);
					
					//Se invocará al mismo método que el Ribbon de selección del tipo de pregunta, ya que no se podrían repetir los Ids de las opciones
					if(id=="copy")
					{
						doCopyForm(copyAction);
					}
					else
					{
						doQuestionAction(id);
					}
				});
				
				objQuestionPopUp.attachEvent("onShow", function() {
					console.log('objQuestionPopUp.onShow');
					
					this.clear();
					objNewQuestionsAcc = objQuestionPopUp.attachAccordion(200, 500);
					objNewQuestionsAcc.addItem("open", "<?=translate("Open")?>");
					objNewQuestionsAcc.addItem("single", "<?=translate("Simple choice")?>");
					objNewQuestionsAcc.addItem("multi", "<?=translate("Multiple choice")?>");
					objNewQuestionsAcc.addItem("menu", "<?=translate("Menus")?>");
					objNewQuestionsAcc.addItem("media", "<?=translate("Media")?>");
					objNewQuestionsAcc.addItem("other", "<?=translate("Other")?>");
					
					objNewQuestionsAcc.attachEvent("onActive", function(id, state) {
						console.log("objNewQuestionsAcc.onActive " + id);
						
						gsLastQGroupSelected = id;
					});
					
					//Preguntas abiertas
					//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
					//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
					//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
					objNewQuestionsAcc.cells("open").attachHTMLString("<div id='openQuestionTypesCell' style='height:290px;'><div>")
					//var objQTypeOpenDataView = objNewQuestionsAcc.cells("open").attachDataView({
					objQTypeOpenDataView = new dhtmlXDataView({
						container:"openQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
					});
						
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					//validamos que idioma se esta usando para concatenar la ruta
					//Conchita 2015-07-27
					var langg = 'SP';
					langg = '<?=(string)@$_SESSION["PAuserLanguage"]?>';
					if (langg) {
						switch (langg) {
							case 'SP':
							case 'ES':
								langg = 'SP';
								break;
							case 'EN':
							case 'BE':
								langg = 'EN';
								break;
							default:
								break;
						}
					}
					var OpenDataViewArray = [
						{id:"add"+QTypesExt.openNumber, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'numeric.png', type:"<?=translate("Number")?>", description:strDescription},
						{id:"add"+QTypesExt.openText, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'text.png', type:"<?=translate("Text")?>", description:strDescription},
						{id:"add"+QTypesExt.openAlpha, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'alphanum.png', type:"<?=translate("Alphanumeric")?>", description:strDescription},
						{id:"add"+QTypesExt.openDate, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'date.png', type:"<?=translate("Date")?>", description:strDescription},
						{id:"add"+QTypesExt.openTime, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'time.png', type:"<?=translate("Time")?>", description:strDescription},
						{id:"add"+QTypesExt.openPassword, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'password.png', type:"<?=translate("Password")?>", description:strDescription}
					];
					objQTypeOpenDataView.parse(OpenDataViewArray, "json");
					
					//JAPR 2015-06-15: Corregido un bug, no se puede volver a agregar el evento cuando se pinta la DataView porque se acumulan
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeOpenDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeOpenDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
					
					//Preguntas selección sencilla
					//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
					//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
					//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
					objNewQuestionsAcc.cells("single").attachHTMLString("<div id='singleQuestionTypesCell' style='height:290px;'><div>")
					//var objQTypeSimpleDataView = objNewQuestionsAcc.cells("single").attachDataView({
					objQTypeSimpleDataView = new dhtmlXDataView({
						container:"singleQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
					});
						
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					var SimpleDataViewArray = [
						{id:"add"+QTypesExt.simpleMenu, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleMenu.png', type:"<?=translate("Menu")?>", description:strDescription},
						{id:"add"+QTypesExt.simpleVert, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleVert.png', type:"<?=translate("Vertical")?>", description:strDescription},
						{id:"add"+QTypesExt.simpleHoriz, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleHoriz.png', type:"<?=translate("Horizontal")?>", description:strDescription},
						{id:"add"+QTypesExt.simpleAuto, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleAuto.png', type:"<?=translate("Autocomplete")?>", description:strDescription},
						{id:"add"+QTypesExt.simpleMap, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleMap.png', type:"<?=translate("Map")?>", description:strDescription},
						//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
						{id:"add"+QTypesExt.simpleMetro, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleMetro.png', type:"<?=translate("Metro")?>", description:strDescription}
					];
					objQTypeSimpleDataView.parse(SimpleDataViewArray, "json");
					
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeSimpleDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeSimpleDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
					
					//Preguntas selección múltiple
					//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
					//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
					//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
					objNewQuestionsAcc.cells("multi").attachHTMLString("<div id='multiQuestionTypesCell' style='height:290px;'><div>")
					//var objQTypeMultiDataView = objNewQuestionsAcc.cells("multi").attachDataView({
					objQTypeMultiDataView = new dhtmlXDataView({
						container:"multiQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
					});
						
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					var MultiDataViewArray = [
						{id:"add"+QTypesExt.multiMenu, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'multiMenu.png', type:"<?=translate("Menu")?>", description:strDescription},
						{id:"add"+QTypesExt.multiVert, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'multiVert.png', type:"<?=translate("Vertical")?>", description:strDescription},
						{id:"add"+QTypesExt.multiHoriz, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'multiHoriz.png', type:"<?=translate("Horizontal")?>", description:strDescription},
						{id:"add"+QTypesExt.multiAuto, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'multiAuto.png', type:"<?=translate("Autocomplete")?>", description:strDescription},
						{id:"add"+QTypesExt.multiRank, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'multiRanking.png', type:"<?=translate("Ranking")?>", description:strDescription}
					];
					objQTypeMultiDataView.parse(MultiDataViewArray, "json");
					
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeMultiDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeMultiDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
					
					//Preguntas de menú
					//OMMC 2016-04-21: Agregadas las preguntas Menú 
					objNewQuestionsAcc.cells("menu").attachHTMLString("<div id='menuQuestionTypesCell' style='height:290px;'><div>")
					objQTypeMenuDataView = new dhtmlXDataView({
						container:"menuQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
					});
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					var MenuDataViewArray = [
						{id:"add"+QTypesExt.menuMenu, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleMenu.png', type:"<?=translate("Menu")?>", description:strDescription},
						{id:"add"+QTypesExt.menuVert, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleVert.png', type:"<?=translate("Vertical")?>", description:strDescription},
						{id:"add"+QTypesExt.menuHoriz, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'simpleHoriz.png', type:"<?=translate("Horizontal")?>", description:strDescription}
					];
					objQTypeMenuDataView.parse(MenuDataViewArray, "json");
					
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeMenuDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeMenuDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
					//OMMC
					
					
					//Preguntas de Multimedia
					//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
					//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
					//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
					objNewQuestionsAcc.cells("media").attachHTMLString("<div id='mediaQuestionTypesCell' style='height:290px;'><div>")
					//var objQTypeMediaDataView = objNewQuestionsAcc.cells("media").attachDataView({
					objQTypeMediaDataView = new dhtmlXDataView({
						container:"mediaQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
					});
						
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					var MediaDataViewArray = [
						{id:"add"+QTypesExt.photo, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'photo.png', type:"<?=translate("Photo")?>", description:strDescription},
						{id:"add"+QTypesExt.audio, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'audio.png', type:"<?=translate("Audio")?>", description:strDescription},
						{id:"add"+QTypesExt.video, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'video.png', type:"<?=translate("Video")?>", description:strDescription},
						{id:"add"+((vObject.serviceVersion >= vObject.updateVersion.esvSketchPlus)?QTypesExt.sketchPlus:QTypesExt.sketch), picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'sketch.png', type:"<?=translate("Sketch")?>", description:strDescription},
						{id:"add"+QTypesExt.document, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'document.png', type:"<?=translate("Document")?>", description:strDescription}
					];
					if(vObject.serviceVersion >= vObject.updateVersion.esvOCRQuestionFields){
						MediaDataViewArray.push({id:"add"+QTypesExt.ocr, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'ocr.png', type:"<?=translate("OCR")?>", description:strDescription});
					}
					//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
					if(vObject.serviceVersion >= vObject.updateVersion.esvImageDisplay){
						MediaDataViewArray.push({id:"add"+QTypesExt.image, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'image.png', type:"<?=translate("Image")?>", description:strDescription});
					}
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					if(vObject.serviceVersion >= vObject.updateVersion.esvQuestionAR) {
						MediaDataViewArray.push({id:"add"+QTypesExt.simpleAR, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'ar.png', type:"<?=translate("AR")?>", description:strDescription});
					}
					
					objQTypeMediaDataView.parse(MediaDataViewArray, "json");
					
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeMediaDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeMediaDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
					
					//Preguntas de otros tipos
					//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
					//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
					//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema (#D3YHO1)
					objNewQuestionsAcc.cells("other").attachHTMLString("<div id='otherQuestionTypesCell' style='height:290px;'><div>")
					//var objQTypeOtherDataView = objNewQuestionsAcc.cells("other").attachDataView({
					objQTypeOtherDataView = new dhtmlXDataView({
						container:"otherQuestionTypesCell",
						drag:false, 
						select:true,
						type:{
							template:"<div style='font-size:16px;font-weight:bold;'><div>#type#</div><img src=#picture#><div style='font-weight:normal;'>#description#</div></div>",
							width:"100%",
							border:1,
							height:85
						},
						x_count: 1
						/*tooltip:{
							//template:"Type: #type#"
						}*/
					});
					
					//El ID contendrá la combinación del ID numérico base, separado por un "_" del tipo específico QDisplayMode o cualquier otra propiedad según el tipo base
					var strDescription = "";
					var OtherDataViewArray = [
						{id:"add"+QTypesExt.skipsection, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'skip.png', type:"<?=translate("Skip to section")?>", description:strDescription},
						{id:"add"+QTypesExt.calc, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'calc.png', type:"<?=translate("Calculated")?>", description:strDescription},
						{id:"add"+QTypesExt.signature, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'signature.png', type:"<?=translate("Signature")?>", description:strDescription},
						{id:"add"+QTypesExt.message, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'message.png', type:"<?=translate("Message")?>", description:strDescription},
						{id:"add"+QTypesExt.html, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'html.png', type:"<?=translate("HTML")?>", description:strDescription},
						{id:"add"+QTypesExt.callList, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'callList.png', type:"<?=translate("Call List")?>", description:strDescription},
						{id:"add"+QTypesExt.barCode, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'barcode.png', type:"<?=translate("Barcode")?>", description:strDescription},
						{id:"add"+QTypesExt.gps, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'gps.png', type:"<?=translate("GPS")?>", description:strDescription},
						{id:"add"+QTypesExt.sync, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'sync.png', type:"<?=translate("Synchronize")?>", description:strDescription},
						{id:"add"+QTypesExt.simpleGetData, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'getdata.png', type:"<?=translate("Get data")?>", description:strDescription},
						{id:"add"+QTypesExt.section, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'inline.png', type:"<?=translate("Section")." ".strtolower(translate("Table"))?>", description:strDescription}
					];
					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					if (vObject.serviceVersion >= vObject.updateVersion.esvExitQuestion) {
						OtherDataViewArray.push({id:"add"+QTypesExt.exit, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'exit.png', type:"<?=translate("Exit")?>", description:strDescription});
					}
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					if (vObject.serviceVersion >= vObject.updateVersion.esvUpdateData) {
						OtherDataViewArray.push({id:"add"+QTypesExt.updateDest, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'updateData.png', type:"<?=translate("Update data")?>", description:strDescription});
					}
					//JAPR
					//OMMC 2016-04-14: Agregado el tipo de pregunta Mi localización
					if (vObject.serviceVersion >= vObject.updateVersion.esvMyLocation) {
						OtherDataViewArray.push({id:"add"+QTypesExt.myLocation, picture:'<?=$strScriptPath?>/images/samples/'+langg+'/'+'location.png', type:"<?=translate("Visual geolocation")?>", description:strDescription});
					}
					objQTypeOtherDataView.parse(OtherDataViewArray, "json");
					
					//Agrega el evento del click para seleccionar el tipo de sección a crear
					objQTypeOtherDataView.attachEvent("onItemClick", function (id, ev, html){
						console.log('objQTypeOtherDataView.onItemClick ' + id);
						doQuestionAction(id);
					});
				});
				
				//RV-Mar2015: Establecer la primera sección cómo seleccionada
				for (var intSectionNum in selSurvey.sectionsOrder)
				{
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection)
					{
						if (!giSectionID)
						{
							giSectionID = intSectionID;
							break;
						}
					}
				}

				//******************************************************************************************************
				//Prepara la vista previa del App
				//JAPR 2015-06-09: Se cambia el título de la celda de preview al nombre de la sección, por default dirá "Questions"
				objFormsLayout.cells(dhxPreviewCell).setText("<?=translate("Questions")?>");	//Preview
				objFormsLayout.cells(dhxPreviewCell).progressOn();
				objFormsLayout.cells(dhxPreviewCell).attachURL("indexContainer.php", false, {SurveyID: intSurveyID});
				
				//******************************************************************************************************
				//Genera la lista de secciones y preguntas en el árbol de navegación
				objFormsLayout.cells(dhxLayOutCell).setText("<?=translate("Sections")?>");
				//JAPR 2015-05-22: Se agrega nuevamente el DataView de secciones y ahora una toolbar con tipos de preguntas, así que se cambia el Layout a 1C -> 3E
				objTreeLayout = objFormsLayout.cells(dhxLayOutCell).attachLayout({pattern: "1C"});
				objTreeLayout.cells(dhxLayOutCell).setText("<?=translate("Sections")?>");
				
				//Prepara la Toolbar para las opciones del árbol de la forma
				objTreeLayout.attachToolbar({
						parent:"sectionsCreationCell"
					});
				objTreeLayout.cells(dhxLayOutCell).hideHeader();
				objSectionsToolbar = objTreeLayout.getAttachedToolbar();
				objSectionsToolbar.setIconsPath("<?=$strScriptPath?>/images/admin/");
				var intPos = 0;
				objSectionsToolbar.addButton("add", intPos++, "<?=translate("Add")?>", "add.png", "add_dis.png");
				objSectionsToolbar.addButton("remove", intPos++, "<?=translate("Remove")?>", "remove.png", "remove_dis.png");
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				if ($this->FormType == formProdWithDev) {?>
				objSectionsToolbar.disableItem("add");
				objSectionsToolbar.disableItem("remove");
				<?}
				//@JAPR
				?>
				
				objSectionsToolbar.attachEvent("onClick", function(id) {
					console.log('objSectionsToolbar.onClick ' + id);
					
					//Se crea una nueva sección, así que no se envía el ID
					doSectionAction(id);
				});
				
				//Lista de secciones
				//JAPR 2015-06-24: Corregido un bug, hay que cancelar el DragStart de las imagenes en el DataView
				objSectionsDataView = objTreeLayout.cells(dhxLayOutCell).attachDataView({
					container:"sectionsSelectorCell",
					drag:true, 
					select:true,
					type:{
						//Last Ok
						//template:"<div style='font-size:10px;font-weight:bold;'><img style='float:left;padding-right:5px;height:60px;' ondragstart='return false' src=#picture#>#name#</div>",
						template:"<div style='font-family:Arial Regular;font-size:12px;font-weight:bold;display: table;'><div style='display: table-cell;'><img style='padding-right:5px;height:38px;' ondragstart='return false' src=#picture#></div><div style='display: table-cell;vertical-align: middle;display:inline-block;width:130px;'>#name#</div></div>",
						//width:"100%",
						//width:"100%; widthNA:100",
						//border: 0,
						//css:"customSection",
						padding: 0,
						height:46	//60
					}//,
					//x_count: 1
				});
				
				//Asigna el tamaño de los componentes del template en este punto, ya que por alguna razón se debe dejar el width:"100%" para que el DataView utilice toda el área de la
				//celda, pero eso dejaba inconsistentes los tamaños de los elementos agregados, así que con este customize se asigna del mismo ancho que la celda
				var intWidth = objFormsLayout.cells(dhxLayOutCell).getWidth();
				//objSectionsDataView.customize({width: intWidth});
				
				//Evento para actualizar el orden de las secciones
				objSectionsDataView.attachEvent("onAfterDrop", function(context,ev) {
					<?
					//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if ($this->FormType == formProdWithDev) {?>
					return;
					<?}
					//@JAPR
					?>
					
					console.log('objSectionsDataView.onAfterDrop');
					
					var intStartingPos = 0;
					if (selSurvey.sections[context.start]) {
						intStartingPos = selSurvey.sections[context.start].number;
					}
					var intEndingPos = 0;
					if (selSurvey.sections[context.target]) {
						intEndingPos = selSurvey.sections[context.target].number;
					}
					
					//Prepara el método para cancelar el reordenamiento que acaba de concluir
					doCancelDragDropOperation = function() {
						doRestoreDroppedItem(AdminObjectType.otySection, context.start, intStartingPos);
					};
					
					objFormsLayout.cells(dhxLayOutCell).progressOn();
					//JAPR 2015-06-22: Agregado el reordenamiento de objetos
					//Se invoca al grabado del nuevo orden, en caso de error se restaurará el orden previo
					if (intStartingPos && intEndingPos && intStartingPos != intEndingPos) {
						doReorderObject(AdminObjectType.otySection, context.start, intEndingPos);
					}
				});
				
				objSectionsDataView.attachEvent("onBeforeDrag",function(context,ev) {
					console.log("objSectionsDataView.onBeforeDrag");
					return true;
				});
				
				//Evento para cambiar una pregunta de sección
				objSectionsDataView.attachEvent("onBeforeDrop",function(context,ev) {
					<?
					//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if ($this->FormType == formProdWithDev) {?>
					return;
					<?}
					//@JAPR
					?>
					
					console.log('objSectionsDataView.onBeforeDrop');
					
					//Verifica que el Drop ocurra debido a un div _area de pregunta
					if (context.from == dhtmlx.DragControl) {
						var blnAllow = false;
						
						alert('This must be a question: ' + context.start);
						return blnAllow;
					}
					
					//En caso de que sea la propia DataView, si se permitirá el Drop
					var intStartingPos = 0;
					if (selSurvey.sections[context.start]) {
						intStartingPos = selSurvey.sections[context.start].number;
					}
					var intEndingPos = 0;
					if (selSurvey.sections[context.target]) {
						intEndingPos = selSurvey.sections[context.target].number;
					}
					
					//Prepara el método para cancelar el reordenamiento que acaba de concluir
					doCancelDragDropOperation = function() {
						doRestoreDroppedItem(AdminObjectType.otySection, context.start, intStartingPos);
					};
					
					//JAPR 2015-06-22: Agregado el reordenamiento de objetos
					//Se invoca al grabado del nuevo orden, en caso de error se restaurará el orden previo
					if (intStartingPos && intEndingPos && intStartingPos != intEndingPos) {
						//JAPR 2015-06-24: Corregido un bug, la progress bar se debe activar hasta que se confirme que se hará el proceso
						objFormsLayout.cells(dhxLayOutCell).progressOn();
						doReorderObject(AdminObjectType.otySection, context.start, intEndingPos);
					}
					
					//JAPR 2015-06-22: Agregado el reordenamiento de objetos
					//Se cancelará el Drop para que el component no lo haga, sino que sea el refresh del método de move el que finalmente regenere el
					//componente en caso de que termine todo ok
					return false;
					//return true;
				});
				
				//Agrega el evento del click para cambiar la sección a mostrar en el preview y el elemento a desplegar en las preguntas
				objSectionsDataView.attachEvent("onItemClick", function (id, ev, html){
					console.log('objSectionsDataView.onItemClick ' + id);
					
					//JAPR 2015-09-23: Corregido un bug, al cambiar el foco a la DataView de secciones, el grid no estaba cancelando la edición instantáneamente por lo que se perdía
					//lo que se había editado, internamente si lanzaba el evento de perdida de foco y mandaba a grabar, pero inmediatamente hacía una invocación del destructor lo cual
					//llamaba al método para detener la edición y esto provocaba que se hiciera rollback al dato grabado al perder el foco, causando que al final no grabara el cambio (#57AN5Y)
					//Detiene la edición del grid actualmente mostrado (esto sólo puede suceder en la vista de propiedades de algun elemento de la forma, no de
					//la forma misma, ya que para esa no se tiene acceso al preview)
					if (objPropsTabs) {
						var strActiveTabID = objPropsTabs.getActiveTab();
						if (strActiveTabID) {
							var objTab = objPropsTabs.tabs(strActiveTabID);
							if (objTab) {
								var objGrid = objTab.getAttachedObject();
								if (objGrid && objGrid.editStop) {
									objGrid.editStop();
								}
							}
						}
					}
					//JAPR
					
					doSelectSection(id);
					doShowSectionProps("s" + id);
				});
				
				//Genera la lista de secciones
				doDrawSections();
				
				//******************************************************************************************************
				//Genera la ventana de propiedades
				objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?>");
				objPropsLayout = objFormsLayout.cells(dhxPropsCell).attachLayout({pattern: "1C"});
				objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Properties")?>");
				objPropsLayout.cells(dhxPropsDetailCell).hideHeader();
				objFormsLayout.cells(dhxPropsCell).showHeader();
				
				//Agrega el evento para procesos cuando termina de cargar cada frame del layout
				objPropsLayout.attachEvent("onContentLoaded", function(id) {
					console.log('objPropsLayout.onContentLoaded ' + id);
					objPropsLayout.cells(id).progressOff();
				});
				
				//******************************************************************************************************
				//Configuraciones adicionales de la interfaz
				//JAPR 2015-06-08: Agregado el tamaño mínimo de las celdas. En el caso de las celdas que internamente tengan otros Layouts, el setMinWidth se debe aplicar sobre el Layout
				//interno ya que de lo contrario no lo toma el externo
				//objTreeLayout.cells(dhxTreeCell).setMinWidth(intMinCellWidth);
				objTreeLayout.cells(dhxTreeCell).setMinWidth(intMinCellWidth - 140);	//105
				objPropsLayout.cells(dhxPropsDetailCell).setMinWidth(intMinCellWidth);
				objTreeLayout.cells(dhxTreeCell).setWidth(intMinCellWidth);
				objPropsLayout.cells(dhxPropsDetailCell).setWidth(intMinCellWidth);
				//@JAPR 2016-07-11: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				objFormsLayout.cells(dhxPreviewCell).setMinWidth(550);
				
				//Agrega el evento para procesos cuando termina de cargar cada frame del layout
				objFormsLayout.attachEvent("onContentLoaded", function(id) {
					console.log('objFormsLayout.onContentLoaded ' + id);
					
					//Al terminar de cargar este Layout es cuando extiende las propiedades de la instancia, ya que antes de esto no se encuentra creada la clase,
					//además lo hace antes de que se utilicen los eventos que se le van a agregar
					/*
					if (window.addClassPrototype) {
						addClassPrototype();
					}
					*/
					
					objFormsLayout.cells(id).progressOff();
					//Al terminar de cargar el iframe del preview, debe mostrar la forma ya dibujada
					switch (id) {
						case dhxPreviewCell:
							var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
							try {
								if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
									return;
								}
								
								var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
								//JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
								//Sobreescribe las funciones para eventos que el App no implementa ya que son exclusivos del modo de diseño
								//Carga de propiedades de la sección inicial al terminar de mostrar una forma
								objApp.notifySurveyShown = function() {
									console.log('objApp.notifySurveyShown');
									
									doSelectSection(giSectionID);
									doShowSurveyProps("sv" + intSurveyID);
									//JAPR 2015-06-20: Ahora se mostrarán las propiedades de la sección inicial al generar la pantalla
									if (giSectionID) {
										doShowSectionProps("s" + giSectionID);
									}
									//JAPR
								}
								
								//Carga de las propiedades de la pregunta sobre la que se hace click
								objApp.notifyQuestionClick = function(iQuestionID, ev) {
									console.log('objApp.notifyQuestionClick ' + iQuestionID);
									
									//JAPR 2015-09-22: Corregido un bug, al cambiar el foco al iframe del preview, algunos componentes no se actualizaban, así que ahora se forzará
									//a detener la edición para asegurar el grabado (#57AN5Y)
									//Detiene la edición del grid actualmente mostrado (esto sólo puede suceder en la vista de propiedades de algun elemento de la forma, no de
									//la forma misma, ya que para esa no se tiene acceso al preview)
									if (objPropsTabs) {
										var strActiveTabID = objPropsTabs.getActiveTab();
										if (strActiveTabID) {
											var objTab = objPropsTabs.tabs(strActiveTabID);
											if (objTab) {
												var objGrid = objTab.getAttachedObject();
												if (objGrid && objGrid.editStop) {
													objGrid.editStop();
												}
											}
										}
									}
									//JAPR
									
									giQuestionID = iQuestionID;
									doShowQuestionProps(iQuestionID);
									//Detiene la propagación del evento de tal forma que no muestre las propiedades de la sección Inline al hacer click en sus preguntas
									if (ev && ev.stopPropagation) {
										ev.stopPropagation();
									}
								}
								
								//JAPR 2015-06-02: Habilitado el Drag&Drop para reordenamiento de preguntas
								//Agrega al DataView de secciones la posibilidad de recibir un Drop de un div _area de Preguntas
								/*
								No es posible el Drag & Drop entre iframes, específicamente en el archivo de DHTMLX falla esta parte de código porque intenta buscar el id agregado en
								addDrag mediante el document, pero no es el mismo document del contexto donde están las preguntas, así que no lo localiza:
								dhtmlx.toNode = function(a) {
									if (typeof a == "string") {
										return document.getElementById(a)
										  //Se necesita buscar el div dos iFrames debajo del contexto donde corre esta función que es parte de DHTMLX
										  //(objiFrame.contentWindow.frmDevice.document || objiFrame.contentWindow.frmDevice.contentWindow.document).getElementById(a)
									}
									return a
								};
								
								objApp.notifyDraggableQuestion = function(id) {
									if (id && dhtmlx && dhtmlx.DragControl && dhtmlx.DragControl.addDrag) {
										dhtmlx.DragControl.addDrag(id);
									}
								}
								*/
								
								//JAPR 2015-06-08: Modificado para que el dispositivo default sea iPhone
								doChangeDevice("iPhone", false);
								doShowSurveyProps("sv" + intSurveyID);
								//JAPR 2015-06-20: Ahora se mostrarán las propiedades de la sección inicial al generar la pantalla
								if (giSectionID) {
									doShowSectionProps("s" + giSectionID);
								}
								//JAPR
							} catch (e) {
								alert('onContentLoaded error: ' + e);
							}
						break;
					}

					//GCRUZ 2015-10-01. Poner el foco sobre la ventana de nueva seccion. Issue: KSFELW
					if (objWindows.isWindow('newObject'))
					{
						objWindows.window('newObject').getAttachedObject().setItemFocus('SectionName');
					}
				});
				
				//Al redimensionar el LayOut, se debe hacer resize del DataView de secciones, ya que por default no se logró quitar el borde que genera y no se le puede poner un width
				//en porcentaje porque el borde en ese caso se muestra sólo a la derecha y desfasado, así qe se optó por default el border default y redimensionar el componente
				objFormsLayout.attachEvent("onPanelResizeFinish", function() {
					console.log('objFormsLayout.onPanelResizeFinish');
					var intWidth = objFormsLayout.cells(dhxLayOutCell).getWidth();
					//objSectionsDataView.customize({width: intWidth});
				});
				
				objFormsLayout.attachEvent("onResizeFinish", function() {
					console.log('objFormsLayout.onResizeFinish');
					if (objSectionsDataView) {
						var intWidth = objFormsLayout.cells(dhxLayOutCell).getWidth();
						//objSectionsDataView.customize({width: intWidth});
					}
				});
				
				objFormsLayout.cells(dhxLayOutCell).showHeader();
				
				//Marca la opción de diseño como seleccionada
				$("#divDesignHeader").find("[tabname='form']").addClass("linktdSelected");
				//Resize completo de la ventana y prepara el evento para los resize posteriores
				doOnResizeScreen();
				objFormsLayout.cells(dhxLayOutCell).setWidth(195);
				objFormsLayout.cells(dhxPreviewCell).setWidth(550);
				intWidth = objFormsLayout.cells(dhxLayOutCell).getWidth();
				//JAPR 2015-06-30: Modificado el ancho de las líneas separadoras para hacerlas mas estrechas
				objFormsLayout.setSeparatorSize(0, 2);
				objFormsLayout.setSeparatorSize(1, 2);
				//objSectionsDataView.customize({width: intWidth-29});
				$(window).resize(function() {
					doOnResizeScreen();
				});
				
				//Si no hay secciones, lo primero que debe hacer es forzar a crear una
				if (selSurvey && selSurvey.sectionsOrder && !selSurvey.sectionsOrder.length) {
					addNewSection();
				}
				
				//Asigna estilos personalizados a la tabbar
				$(objFormTabs.tabsArea).find(".dhxtabbar_tabs_base").addClass("linkToolbar");
				$(objFormTabs.tabsArea).find(".dhxtabbar_tab").addClass("linkToolbar");
				$(objFormTabs.tabsArea).find(".dhxtabbar_tab").addClass("linktd");
			}
			
			/* Permite hacer ajustes a la pantalla donde DHTMLX no lo hace correctamente */
			function doOnResizeScreen() {
				//@JAPR 2016-07-11: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				//Se dejarán de realizar los ajustes manuales ahora que toda la ventana se construye 100% con DHTMLX, ya no debería haber desajustes de componentes y scrolls faltantes
				if (objFormsLayout && objFormsLayout.cells(dhxPreviewCell)) {
					objFormsLayout.cells(dhxPreviewCell).setWidth(550)
				}
				return;
				console.log('doOnResizeScreen');
				//Para permitir que se pinten correctamente las ventanas de Invitar usuario y grupos, es necesario ocultar momentáneamente los divs que las contienen
				//ya que DHTMLX no estaba redimensionando sus componentes al maximizar la ventana
				var intTopAdjust = 120;
				//$('#divInviteUsers .selection_container div.dhxform_container').height($(window).height() - intTopAdjust - 50);
				var intHeight = $(window).height();
				$('.selection_container div.dhxform_container').height(intHeight - intTopAdjust - 50);
				//GCRUZ 2015-09-10. Ajustar el tamaño debido a que algunos elementos no se alcanzan a ver. Issue: XNH7MD
				//$('.selection_container div.objbox').height(intHeight - intTopAdjust - 50);
				$('.selection_container div.objbox').height(intHeight - intTopAdjust - 90);
				$('.selection_list fieldset.dhxform_fs').height(intHeight - intTopAdjust);
				if (objMainLayout) {
					objMainLayout.setSizes();
				}
				if (objFormTabs) {
					objFormTabs.setSizes();
				}
			}
			
			/* Descarga las ventanas adicionales creadas así como cualquier otro componente */
			function doOnUnload() {
				console.log('doOnUnload');
				if (objWindows && objWindows.unload) {
					objWindows.unload();
				}
			}
			function doOnUnloadFn() {
				console.log('doOnUnloadFn');
				if (objWindowsFn && objWindowsFn.unload) {
					objWindowsFn.unload();
				}
			}
			function doOnUnloadEd() {
				console.log('doOnUnloadEd');
				if (objWindowsEd && objWindowsEd.unload) {
					objWindowsEd.unload();
				}
			}
			
			/* Guarda el estado de los nodos del árbol antes de ser reconstruido
			*/
			function doSaveTreeState() {
				console.log('doSaveTreeState');
				if (!objTreeLayout) {
					return;
				}
				
				objTreeState = new Object();
				var objTree = objTreeLayout.cells(dhxTreeCell).getAttachedObject();
				var strKey = "sv" + intSurveyID;
				var strAllItems = objTree.getAllSubItems(strKey);
				if (strAllItems != 0) {
					var arrAllItems = strAllItems.split(',');
					if (arrAllItems) {
						objTreeState[strKey] = {check: objTree.isItemChecked(strKey), open: ((objTree.getOpenState(strKey) == 1)?1:0)};
						for (var intIndex in arrAllItems) {
							var strKey = arrAllItems[intIndex];
							objTreeState[strKey] = {check: objTree.isItemChecked(strKey), open: ((objTree.getOpenState(strKey) == 1)?1:0)};
						}
					}
				}
			}
			
			/* Restaura el estado de los nodos del árbol luego de ser reconstruido
			*/
			function doRestoreTreeState() {
				if (!objTreeLayout) {
					return;
				}
				
				var objTree = objTreeLayout.cells(dhxTreeCell).getAttachedObject();
				for (var strKey in objTreeState) {
					var objState = objTreeState[strKey];
					if (objState) {
						if (objState.open) {
							objTree.openItem(strKey);
						}
						else {
							objTree.closeItem(strKey);
						}
						
						objTree.setCheck(strKey, ((objState.check)?1:0));
					}
				}
				objTreeState = new Object();
			}
			
			/* Realiza el request para grabar la lista de usuarios/grupos asociados al scheduler según el tipo indicado, ya que se dividió en 2 Grids, por lo
			que el grabado durante edición sólo actualiza la parte correspondiente al cambio realizado
			*/
			function doUpdateScheduler(iAssocItemType, oGrid) {
				console.log("doUpdateScheduler type == " + iAssocItemType);
				
				if (!oGrid) {
					return;
				}
				
				var intRows = oGrid.getRowsNum();
				var strAnd = "";
				var strUserRol = "";
				var strType = "";
				switch (iAssocItemType) {
					case AdminObjectType.otyUserGroup:
						strType = "Rol_";
						break;
						
					default:
						strType = "User_";
						break;
				}
				
				for (var intIndex = 0; intIndex < intRows; intIndex++) {
					var strRowId = oGrid.getRowId(intIndex);
					if (strRowId) {
						var intObjectID = oGrid.getUserData(strRowId, "id");
						if (intObjectID) {
							strUserRol += strAnd + strType + intObjectID;
							strAnd = "_AWElem_";
						}
					}
				}
				
				var objObject = getObject(AdminObjectType.otySurvey, intSurveyID);
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (!gbFullSave && objObject && objObject.send) {
					var objParams = {};
					objParams["StrUserRol"] = strUserRol;
					objParams["Process"] = "Invite";
					objParams["ObjectType"] = iAssocItemType;
					setTimeout(objObject.send(objParams), 100);
				}
			}
			
			/* Dado un grid, regresa la lista de elementos que contiene en la propiedad id (sólo aplica obviamente para grids que se generan de dicha manera)
			El valor devuelto es un array, o bien se puede pedir directamente el string con el valor bGetArray = false
			*/
			function doGetGridSelectedIDs(oGrid, sSep, bGetArray) {
				console.log("doGetGridSelectedIDs");
				
				if (!oGrid) {
					return;
				}
				if (!sSep) {
					sSep = ', ';
				}
				if (bGetArray === undefined) {
					bGetArray = true;
				}
				
				var intRows = oGrid.getRowsNum();
				var strAnd = "";
				var varList = "";
				//JAPR 2015-08-11: Corregido un bug, al hacer el split los IDs quedaban como String y no se podían usar en un indexOf adecuadamente (#GUE3VX)
				var arrList = new Array();
				for (var intIndex = 0; intIndex < intRows; intIndex++) {
					var strRowId = oGrid.getRowId(intIndex);
					if (strRowId) {
						var intObjectID = oGrid.getUserData(strRowId, "id");
						if (intObjectID) {
							varList += strAnd + intObjectID;
							strAnd = sSep;
							//JAPR 2015-08-11: Corregido un bug, al hacer el split los IDs quedaban como String y no se podían usar en un indexOf adecuadamente (#GUE3VX)
							arrList.push(intObjectID);
						}
					}
				}
				
				//JAPR 2015-08-11: Corregido un bug, al hacer el split los IDs quedaban como String y no se podían usar en un indexOf adecuadamente (#GUE3VX)
				if (bGetArray) {
					//varList = varList.split(sSep);
					varList = arrList;
				}
				//JAPR
				
				return varList;
			}
			
			//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
			/* Dado un objeto Grid, remueve el filtro aplicado para permitir hacer el borrado de elementos y evitar duplicidad de los elementos del grid */
			function removeGridFilter(oGrid, oForm) {
				if (!oGrid) {
					return;
				}
				
				oGrid.filterBy(0,"");
				oGrid._f_rowsBuffer = null;
			}
			
			/* Dado un objeto Grid, restaura el filtro aplicado para permitir hacer el borrado de elementos */
			function restoreGridFilter(oGrid, oForm) {
				if (!oGrid || !oGrid.bitSearchFieldName) {
					return;
				}
				
				var strLastSearch = oForm.getUserData(oGrid.bitSearchFieldName, "lastSearch");
				if (strLastSearch) {
					oGrid.filterBy(0, strLastSearch);
				}
			}
			
			//JAPR 2015-06-04: Agregada la ventana para asociar usuarios a la forma
			/* Genera la forma para asociar usuarios (el equivalente al Scheduler) */
			function doDrawUsersAssoc() {
				console.log('doDrawUsersAssoc');
				if (objUsersForm || !selSurvey) {
					return;
				}
				
				/* La ventana para asociar usuarios a la forma contendrá 2 grids con su respectivo combo para filtrar/buscar, el primer grid corresponde a los usuarios disponibles
				mientras que el segundo grid contiene a los usuarios ya asociados, son mutuamente excluyentes y ambos permitirán Drag & Drop, adicionalmente se pueden seleccionar
				múltiples elementos y utilizar un par de botones en la parte central para mover todo lo seleccionado de un lado a otro
				*/
				var objFormData = [
					{type:"fieldset",  name:"fldsAvailUsers", label:"<?=translate("Available users")?>", offsetLeft:20, offsetTop:20, width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAvailUser", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAvailUsers", inputWidth:330, inputHeight:400, className:"selection_container"}
					]},
					{type:"newcolumn", offset:20},
					{type:"block", blockOffset:0, offsetTop:200, list:[
						{type:"button", name: "btnAssocUser", value:"", className:"assoc_button"},
						{type:"button", name: "btnRemoveUser", value:"", className:"remove_button"}
					]},
					{type:"newcolumn", offset:20},
					{type:"fieldset",  name:"fldsAssocUsers", label:"<?=translate("Invited users")?>", offsetTop:20, width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAssocUser", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAssocUsers", inputWidth:330, inputHeight:400, className:"selection_container"}
					]},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true}
				];
				
				//Se tuvo que invocar a la creación de la forma mediante un div, ya que de lo contrario al hacer resize a la forma, sus fieldset y blocks se movían cuando no
				//cupiedan horizontalmente, así con el div se puede poner un Scroll y tenerlos siempre en la misma posición
				//objFormTabs.tabs(tabUsers).attachObject("divInviteUsersParent");
				//objUsersForm = new dhtmlXForm("divInviteUsers", objFormData);
				objUsersForm = objFormTabs.tabs(tabUsers).attachForm(objFormData);
				
				//Contenido de la lista de usuarios disponibles
				var objAvailGrid = new dhtmlXGridObject(objUsersForm.getContainer("grdAvailUsers"));
				//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
				//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#K6K46P)
				objAvailGrid.dontSetSizes = true
				//JAPR
				objAvailGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAvailGrid.setHeader("<?=translate("User")?>");
				objAvailGrid.setInitWidthsP("100");
				objAvailGrid.setColAlign("left");
				objAvailGrid.setColTypes("ro");
				objAvailGrid.enableEditEvents(false);
				objAvailGrid.enableDragAndDrop(true);
				objAvailGrid.setColSorting("str");
				objAvailGrid.enableMultiselect(true);
				objAvailGrid.init();
				objAvailGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailGrid.bitSearchFieldName = "txtSearchAvailUser";
				
				//Contenido de la lista de usuarios invitados
				var objAssocGrid = new dhtmlXGridObject(objUsersForm.getContainer("grdAssocUsers"));
				//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
				//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#K6K46P)
				objAssocGrid.dontSetSizes = true
				//JAPR
				objAssocGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAssocGrid.setHeader("<?=translate("User")?>");
				objAssocGrid.setInitWidthsP("100");
				objAssocGrid.setColAlign("left");
				objAssocGrid.setColTypes("ro");
				objAssocGrid.enableEditEvents(false);
				objAssocGrid.enableDragAndDrop(true);
				objAssocGrid.setColSorting("str");
				objAssocGrid.enableMultiselect(true);
				objAssocGrid.init();
				objAssocGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocGrid.bitSearchFieldName = "txtSearchAssocUser";
				
				for (var i=0; i<arrUsersOrder.length; i++) {
					var strId = arrUsersOrder[i];
					var strRowId = "usr"+strId;
					var objObject = arrUsers[strId];
					if (arrSelUsers[objObject.id]) {
						objAssocGrid.addRow(strRowId, objObject.name);
						objAssocGrid.setUserData(strRowId, "id", objObject.id);
					}
					else {
						objAvailGrid.addRow(strRowId, objObject.name);
						objAvailGrid.setUserData(strRowId, "id", objObject.id);
					}
				}
				
				//GCRUZ 2015-10-06. Ordenar la lista de usuarios después de haberla llenado
				objAvailGrid.sortRows(0,"str","asc");
				objAssocGrid.sortRows(0,"str","asc");
				
				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAvailGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAvailGrid.onDrop Users');
					
					if (sObj == tObj) {
						return;
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUsersForm);
					restoreGridFilter(tObj, objUsersForm);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUser, objAssocGrid);
				});
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAvailGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					console.log('objAvailGrid.onRowDblClicked Users');
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailGrid, objUsersForm);
					removeGridFilter(objAssocGrid, objUsersForm);
					this.moveRow(rId, "row_sibling", undefined, objAssocGrid);
					restoreGridFilter(objAvailGrid, objUsersForm);
					restoreGridFilter(objAssocGrid, objUsersForm);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUser, objAssocGrid);
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUsersForm);
					removeGridFilter(tObj, objUsersForm);
					
					return true;
				});
				//JAPR
				
				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAssocGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAssocGrid.onDrop Users');
					
					if (sObj == tObj) {
						return;
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUsersForm);
					restoreGridFilter(tObj, objUsersForm);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUser, this);
				});
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAssocGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					console.log('objAssocGrid.onRowDblClicked Users');
				
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailGrid, objUsersForm);
					removeGridFilter(objAssocGrid, objUsersForm);
					this.moveRow(rId, "row_sibling", undefined, objAvailGrid);
					restoreGridFilter(objAvailGrid, objUsersForm);
					restoreGridFilter(objAssocGrid, objUsersForm);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUser, objAssocGrid);
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUsersForm);
					removeGridFilter(tObj, objUsersForm);
					
					return true;
				});
				//JAPR
				
				objUsersForm.attachEvent("onKeyUp",function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					var objGrid = undefined;
					switch (name) {
						case "txtSearchAvailUser":
							objGrid = objAvailGrid;
							break;
						case "txtSearchAssocUser":
							objGrid = objAssocGrid;
							break;
					}
					
					if (!objGrid) {
						return;
					}
					
					//Verifica si ya se había aplicado esta búsqueda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterarían el contenido)
					var strValue = inp.value;
					var strLastSearch = objUsersForm.getUserData(name, "lastSearch");
					if (strLastSearch == strValue) {
						return;
					}
					
					objUsersForm.setUserData(name, "lastSearch", strValue);
					objGrid.filterBy(0, strValue);
				});
				
				//JAPR 2015-07-15: Agregada la funcionalidad para mover los elementos entre grids
				objUsersForm.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnAssocUser":
							var strSelection = objAvailGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailGrid, objUsersForm);
											removeGridFilter(objAssocGrid, objUsersForm);
											objAvailGrid.moveRow(strId, "row_sibling", undefined, objAssocGrid);
											restoreGridFilter(objAvailGrid, objUsersForm);
											restoreGridFilter(objAssocGrid, objUsersForm);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUser, objAssocGrid);
									}
								}
							}
							break;
						
						case "btnRemoveUser":
							var strSelection = objAssocGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailGrid, objUsersForm);
											removeGridFilter(objAssocGrid, objUsersForm);
											objAssocGrid.moveRow(strId, "row_sibling", undefined, objAvailGrid);
											restoreGridFilter(objAvailGrid, objUsersForm);
											restoreGridFilter(objAssocGrid, objUsersForm);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUser, objAssocGrid);
									}
								}
							}
							break;
					}
				});
			}
			
				
			//JAPR 2015-06-05: Agregada la ventana para asociar grupos de usuarios a la forma
			/* Genera la forma para asociar grupos de usuarios (el equivalente al Scheduler) */
			function doDrawUserGroupsAssoc() {
				console.log('doDrawUserGroupsAssoc');
				if (objUserGroupsForm || !selSurvey) {
					return;
				}
				
				/* La ventana para asociar grupos de usuarios a la forma contendrá 2 grids con su respectivo combo para filtrar/buscar, el primer grid corresponde a los grupos disponibles
				mientras que el segundo grid contiene a los grupos ya asociados, son mutuamente excluyentes y ambos permitirán Drag & Drop, adicionalmente se pueden seleccionar
				múltiples elementos y utilizar un par de botones en la parte central para mover todo lo seleccionado de un lado a otro
				*/
				var objFormData = [
					{type:"fieldset",  name:"fldsAvailGroups", label:"<?=translate("Available groups")?>", offsetLeft:20, offsetTop:20, width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAvailGroup", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAvailGroups", inputWidth:330, inputHeight:400, className:"selection_container"}
					]},
					{type:"newcolumn", offset:20},
					{type:"block", blockOffset:0, offsetTop:200, list:[
						{type:"button", name: "btnAssocGroup", value:"", className:"assoc_button"},
						{type:"button", name: "btnRemoveGroup", value:"", className:"remove_button"}
					]},
					{type:"newcolumn", offset:20},
					{type:"fieldset",  name:"fldsAssocGroups", label:"<?=translate("Invited groups")?>", offsetTop:20, width:380, className:"selection_list", list:[
						{type:"input", name:"txtSearchAssocGroup", label:"<?=translate("Search")?>", value:"", labelWidth:100, inputWidth:230, className:"search_button"},
						{type:"container", name:"grdAssocGroups", inputWidth:330, inputHeight:400, className:"selection_container"}
					]},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true}
				];
				
				objFormTabs.tabs(tabGroups).attachObject("divInviteGroupsParent");
				//objUserGroupsForm = objFormTabs.tabs(tabGroups).attachForm(objFormData);
				objUserGroupsForm = new dhtmlXForm("divInviteGroups", objFormData);
				
				//Contenido de la lista de grupos de usuarios disponibles
				var objAvailGrid = new dhtmlXGridObject(objUserGroupsForm.getContainer("grdAvailGroups"));
				//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
				//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#K6K46P)
				objAvailGrid.dontSetSizes = true
				//JAPR
				objAvailGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAvailGrid.setHeader("<?=translate("User group")?>");
				objAvailGrid.setInitWidthsP("100");
				objAvailGrid.setColAlign("left");
				objAvailGrid.setColTypes("ro");
				objAvailGrid.enableEditEvents(false);
				objAvailGrid.enableDragAndDrop(true);
				objAvailGrid.setColSorting("str");
				objAvailGrid.enableMultiselect(true);
				objAvailGrid.init();
				objAvailGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailGrid.bitSearchFieldName = "txtSearchAvailGroup";
				
				//Contenido de la lista de grupos de usuarios invitados
				var objAssocGrid = new dhtmlXGridObject(objUserGroupsForm.getContainer("grdAssocGroups"));
				//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
				//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#K6K46P)
				objAssocGrid.dontSetSizes = true
				//JAPR
				objAssocGrid.setImagePath("<?=$strScriptPath?>/images/");
				objAssocGrid.setHeader("<?=translate("User group")?>");
				objAssocGrid.setInitWidthsP("100");
				objAssocGrid.setColAlign("left");
				objAssocGrid.setColTypes("ro");
				objAssocGrid.enableEditEvents(false);
				objAssocGrid.enableDragAndDrop(true);
				objAssocGrid.setColSorting("str");
				objAssocGrid.enableMultiselect(true);
				objAssocGrid.init();
				objAssocGrid.sortRows(0,"str","asc");
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocGrid.bitSearchFieldName = "txtSearchAssocGroup";
				
				for (var i=0; i<arrRolesOrder.length; i++) {
					var strId = arrRolesOrder[i];
					var strRowId = "grp"+strId;
					var objObject = arrRoles[strId];
					if (arrSelRoles[objObject.id]) {
						objAssocGrid.addRow(strRowId, objObject.name);
						objAssocGrid.setUserData(strRowId, "id", objObject.id);
					}
					else {
						objAvailGrid.addRow(strRowId, objObject.name);
						objAvailGrid.setUserData(strRowId, "id", objObject.id);
					}
				}
				
				//GCRUZ 2015-10-06. Ordenar lista de grupos después de haberla llenado.
//				objAvailGrid.sortRows(0,"str","asc");
//				objAssocGrid.sortRows(0,"str","asc");
				
				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAvailGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAvailGrid.onDrop UserGroups');
					
					if (sObj == tObj) {
						return;
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUserGroupsForm);
					restoreGridFilter(tObj, objUserGroupsForm);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUserGroup, objAssocGrid);
				});
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAvailGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					console.log('objAvailGrid.onRowDblClicked UserGroups');
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailGrid, objUserGroupsForm);
					removeGridFilter(objAssocGrid, objUserGroupsForm);
					this.moveRow(rId, "row_sibling", undefined, objAssocGrid);
					restoreGridFilter(objAvailGrid, objUserGroupsForm);
					restoreGridFilter(objAssocGrid, objUserGroupsForm);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUserGroup, objAssocGrid);
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAvailGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUserGroupsForm);
					removeGridFilter(tObj, objUserGroupsForm);
					
					return true;
				});
				//JAPR
				
				//JAPR 2015-06-22: Agregado el grabado del Scheduler de la forma
				objAssocGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
					console.log('objAssocGrid.onDrop UserGroups');
					
					if (sObj == tObj) {
						return;
					}
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					//Durante el drop restaura el filtro que tuviera aplicado cada grid
					restoreGridFilter(sObj, objUserGroupsForm);
					restoreGridFilter(tObj, objUserGroupsForm);
					//JAPR
					
					doUpdateScheduler(AdminObjectType.otyUserGroup, this);
				});
				//JAPR
				
				//JAPR 2015-07-15: Agregado el evento del doble click
				objAssocGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
					console.log('objAssocGrid.onRowDblClicked UserGroups');
					
					//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
					removeGridFilter(objAvailGrid, objUserGroupsForm);
					removeGridFilter(objAssocGrid, objUserGroupsForm);
					this.moveRow(rId, "row_sibling", undefined, objAvailGrid);
					restoreGridFilter(objAvailGrid, objUserGroupsForm);
					restoreGridFilter(objAssocGrid, objUserGroupsForm);
					//JAPR
					doUpdateScheduler(AdminObjectType.otyUserGroup, objAssocGrid);
				});
				
				//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
				objAssocGrid.attachEvent("onDrag", function(sId,tId,sObj,tObj,sInd,tInd) {
					//Durante el drag (antes de dejar caer el elemento) limpia el filtro aplicado a los grids
					removeGridFilter(sObj, objUserGroupsForm);
					removeGridFilter(tObj, objUserGroupsForm);
					
					return true;
				});
				//JAPR
				
				objUserGroupsForm.attachEvent("onKeyUp",function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					var objGrid = undefined;
					switch (name) {
						case "txtSearchAvailGroup":
							objGrid = objAvailGrid;
							break;
						case "txtSearchAssocGroup":
							objGrid = objAssocGrid;
							break;
					}
					
					if (!objGrid) {
						return;
					}
					
					//Verifica si ya se había aplicado esta búsqueda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterarían el contenido)
					var strValue = inp.value;
					var strLastSearch = objUserGroupsForm.getUserData(name, "lastSearch");
					if (strLastSearch == strValue) {
						return;
					}
					
					objUserGroupsForm.setUserData(name, "lastSearch", strValue);
					objGrid.filterBy(0, strValue);
				});
				
				//JAPR 2015-07-15: Agregada la funcionalidad para mover los elementos entre grids
				objUserGroupsForm.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnAssocGroup":
							var strSelection = objAvailGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailGrid, objUserGroupsForm);
											removeGridFilter(objAssocGrid, objUserGroupsForm);
											objAvailGrid.moveRow(strId, "row_sibling", undefined, objAssocGrid);
											restoreGridFilter(objAvailGrid, objUserGroupsForm);
											restoreGridFilter(objAssocGrid, objUserGroupsForm);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUserGroup, objAssocGrid);
									}
								}
							}
							break;
						
						case "btnRemoveGroup":
							var strSelection = objAssocGrid.getSelectedRowId();
							if (strSelection) {
								var arrSelection = strSelection.split(",");
								if (arrSelection.length) {
									var blnMove = false;
									for (var intIdx in arrSelection) {
										var strId = arrSelection[intIdx];
										if (strId) {
											blnMove = true;
											//JAPR 2016-07-21: Corregida la duplicidad de elementos al haber aplicado filtro a los grids (#4RYSU9)
											removeGridFilter(objAvailGrid, objUserGroupsForm);
											removeGridFilter(objAssocGrid, objUserGroupsForm);
											objAssocGrid.moveRow(strId, "row_sibling", undefined, objAvailGrid);
											restoreGridFilter(objAvailGrid, objUserGroupsForm);
											restoreGridFilter(objAssocGrid, objUserGroupsForm);
											//JAPR
										}
									}
									
									if (blnMove) {
										doUpdateScheduler(AdminObjectType.otyUserGroup, objAssocGrid);
									}
								}
							}
							break;
					}
				});
			}
			
			/* Reconstruye la lista de secciones de la forma, este método se puede utilizar cuando se finaliza el proceso de Delete de componentes
			*/
			function doDrawSections() {
				console.log('doDrawSections');
				if (!objTreeLayout) {
					return;
				}
				
				var objDataView = objSectionsDataView;
				objDataView.clearAll();
				for (var intSectionNum in selSurvey.sectionsOrder) {
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection) {
						if (!giSectionID) {
							giSectionID = intSectionID;
						}
						
						var strType = '';
						var strImage = '';
						//JAPR 2015-09-02: Validado este proceso porque al actualizar la propiedad se grababa como string
						switch (parseInt(objSection.sectionType)) {
							case SectionType.Dynamic:
								strType = '<?=translate("List-based")?>';
								strImage = 'dynamic.png';
								break;
							case SectionType.Formatted:
								strType = '<?=translate("Formatted")?>';
								strImage = 'formatted.png';
								break;
							case SectionType.Multiple:
								strType = '<?=translate("Master-Detail")?>';
								strImage = 'masterDetail.png';
								break;
							case SectionType.Inline:
								strType = '<?=translate("Inline")?>';
								strImage = 'inline.png';
								break;
							default:
								strType = '<?=translate("Standard")?>';
								strImage = 'standard.png';
								break;
						}
						//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano //objSection.name
						objDataView.add({id:objSection.id, number:objSection.number, name:objSection.getName(), picture:'<?=$strScriptPath?>/images/'+strImage, type:strType});
					}
				}
				
				//Selecciona la primer sección
				if (giSectionID) {
					objDataView.select(giSectionID);
				}
			}
			
			/*Muestra la ventana de propiedades de la encuesta y la sección seleccionada en el previo*/
			function doShowSurveyProps(sNodeId) {
				console.log('doShowSurveyProps ' + sNodeId);
				
				//El id del nodo de la forma empieza con el prefijo "sv"	
				var intSurveyID = sNodeId.substr(2);
				objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
				
				//RV-Mar2015: Establecer la primera sección cómo seleccionada
				for (var intSectionNum in selSurvey.sectionsOrder)
				{
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection)
					{
						giSectionID = intSectionID;
						giQuestionID = 0;
						break;
					}
				}
				
				//RV-Mar2015: Cuando se da click en un nodo de secciones en el árbol, aparte de mostrar las propiedades de las secciones
				//también se debe mostrar en el preview la sección seleccionada
				doSelectSection(intSectionID);
				generateSurveyProps(sNodeId);
			}
			
			/* Regresa la definición del campo a partir de la combinación tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
			que se está desplegando en el momento en que se solicita, ya que esos son los que están almacenados en el array
			//JAPR 2015-06-25: Agregado el parámetro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
			que se está desplegando en la celda de propiedades
			*/
			function getFieldDefinition(sTabName, sFieldName, bForForm) {
				console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
				var objField = undefined;
				
				var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						for (var intCont in objTab.fields) {
							var objFieldTmp = objTab.fields[intCont];
							if (objFieldTmp && objFieldTmp.name == sFieldName) {
								objField = objFieldTmp;
								break;
							}
						}
					}
				}
				
				return objField;
			}
			
			/* Idéntica a getFieldDefinition pero en lugar del nombre de campo el cual es útil en un grid de propiedades, se utiliza el índice del campo, el cual es útil en
			un grid de valores */
			function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
				console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
				var objField = undefined;
				
				var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
				if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
					var objTab = objTmpTabsColl[sTabName];
					if (objTab && objTab.fields) {
						objField = objTab.fields[iFieldIdx];
					}
				}
				
				return objField;
			}
			
			/* Agrega una nueva opción de respuesta vacia en el grid de valores */
			function addNewOption(oGrid, rid, ind) {
				console.log('addNewOption');
				
				if (!oGrid) {
					return;
				}
				
				var intNewRowIndex = oGrid.getRowIndex(rid);
				var dteNow = new Date();
				var intMinutes = dteNow.getMinutes();
				var intSeconds = dteNow.getSeconds();
				var strNewID = '-' + dteNow.getHours() + ((intMinutes < 10)?'0':'') + intMinutes + ((intSeconds < 10)?'0':'') + intSeconds;
				var intObjectType = oGrid.bitObjectType;
				var intObjectID = oGrid.bitObjectID;
				if (!intObjectType || !intObjectID) {
					return;
				}
				
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("New option")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});
				
				/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"input", name:"QuestionOptionName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
					{type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"QuestionOptionID", value:-1, hidden:true},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true},
					{type:"input", name:"QuestionID", value:intObjectID, hidden:true},
					{type:"input", name:"Design", value:1, hidden:true},
					{type:"input", name:"RequestType", value:reqAjax, hidden:true},
					{type:"input", name:"Process", value:"Add", hidden:true},
					{type:"input", name:"ObjectType", value:AdminObjectType.otyOption, hidden:true}
				];
				
				var objForm = objDialog.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("QuestionOptionName");
				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					console.log('objForm.onButtonClick ' + name);
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									doSendData(function(oResponse) {
										//Al terminar de grabar con éxito, debe agregar al objeto en memoria la nueva opción creada, además de agregar la nueva row al grid
										//con la nueva opción con todo preparado para configurarla
										
										//Se recibirá como parámetro la respuesta del server en la cual debe venir la nueva definición (para este punto ya se validó que no hubiera
										//algún error en el request, así que debe ser una respuesta válida)
										if (!oResponse || !oResponse.newObject) {
											console.log("<?=translate("No response object was received during this process")?>");
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otyOption) {
											return
										}
										
										var objObject = getObject(intObjectType, intObjectID);
										if (!objObject) {
											return;
										}
										
										//Agrega el nuevo elemento a la forma en memoria
										$.extend(objNewObject, new OptionCls());
										var intValueID = objNewObject.id;
										var strOptionName = objNewObject.name;
										objObject.options[strOptionName] = objNewObject;
										objObject.optionsByID[intValueID] = objNewObject;
										objObject.optionsOrder.push(strOptionName);
										
										//Agrega la nueva row al grid
										var strTabName = oGrid.bitTabName;
										var objTab = objTabsColl[strTabName];
										if (objTab) {
											var rowValues = new Array();
											for (var iField in objTab.fields) {
												var aField = objTab.fields[iField];
												if (aField) {
													var varValue = aField.getValue(undefined, objTab.childType, intValueID);
													if (varValue === undefined) {
														varValue = '';
													}
													rowValues.push(varValue);
												}
											}
											
											oGrid.bitDisableEvents = true;
											oGrid.addRow(intValueID, rowValues, oGrid.getRowIndex("new"));
											oGrid.setRowAttribute(intValueID, "tabName", objTab.id);
											oGrid.setRowAttribute(intValueID, "objectType", objTab.childType);
											oGrid.bitDisableEvents = false;
										}
									});
								}
							}, 100);
							break;
					}
				});
			}
			
			//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			/* Agrega una nueva configuración de salto vacia en el grid */
			function addNewSkipRule(oGrid, rid, ind) {
				console.log('addNewSkipRule');
				
				if (!oGrid) {
					return;
				}
				
				var intNewRowIndex = oGrid.getRowIndex(rid);
				var dteNow = new Date();
				var intMinutes = dteNow.getMinutes();
				var intSeconds = dteNow.getSeconds();
				var strNewID = '-' + dteNow.getHours() + ((intMinutes < 10)?'0':'') + intMinutes + ((intSeconds < 10)?'0':'') + intSeconds;
				var intObjectType = oGrid.bitObjectType;
				var intObjectID = oGrid.bitObjectID;
				if (!intObjectType || !intObjectID) {
					return;
				}
				
				var objObject = getObject(intObjectType, intObjectID);
				if (!objObject) {
					return;
				}
				
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("New skip rule")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});
				
				/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"input", name:"RuleName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
					{type:"input", name:"SkipCondition", label:"<?=translate("Formula")?>", position:"label-top", value:"", labelWidth:100, inputWidth:420, rows:5, required:true},
					{type:"button", name: "btnSkipConditionEd", value:"", className:"formulaButtonRight"},
					{type:"combo", name:"NextSectionID", label:"<?=translate("Section")?>", labelWidth:100, inputWidth:320, required:true, options:objObject.getComboNextSectionItems(true)},
					{type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"SkipRuleID", value:-1, hidden:true},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true},
					{type:"input", name:"QuestionID", value:intObjectID, hidden:true},
					{type:"input", name:"Design", value:1, hidden:true},
					{type:"input", name:"RequestType", value:reqAjax, hidden:true},
					{type:"input", name:"Process", value:"Add", hidden:true},
					{type:"input", name:"ObjectType", value:AdminObjectType.otySkipRule, hidden:true}
				];
				
				var objForm = objDialog.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("RuleName");
				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					console.log('objForm.onButtonClick ' + name);
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnSkipConditionEd":
							var objInput = this.getInput("SkipCondition");
							if (objInput) {
								fnOpenFormulaEditor(objInput, objInput.value);
							}
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									doSendData(function(oResponse) {
										//Al terminar de grabar con éxito, debe agregar al objeto en memoria la nueva opción creada, además de agregar la nueva row al grid
										//con la nueva opción con todo preparado para configurarla
										
										//Se recibirá como parámetro la respuesta del server en la cual debe venir la nueva definición (para este punto ya se validó que no hubiera
										//algún error en el request, así que debe ser una respuesta válida)
										if (!oResponse || !oResponse.newObject) {
											console.log("<?=translate("No response object was received during this process")?>");
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otySkipRule) {
											return
										}
										
										var objObject = getObject(intObjectType, intObjectID);
										if (!objObject) {
											return;
										}
										
										//Agrega el nuevo elemento a la forma en memoria
										$.extend(objNewObject, new SkipRuleCls());
										var intValueID = objNewObject.id;
										objObject.skipRules[intValueID] = objNewObject;
										objObject.skipRulesOrder.push(intValueID);
										
										//Agrega la nueva row al grid
										var strTabName = oGrid.bitTabName;
										var objTab = objTabsColl[strTabName];
										if (objTab) {
											var rowValues = new Array();
											for (var iField in objTab.fields) {
												var aField = objTab.fields[iField];
												if (aField) {
													var varValue = aField.getValue(undefined, objTab.childType, intValueID);
													if (varValue === undefined) {
														varValue = '';
													}
													rowValues.push(varValue);
												}
											}
											
											oGrid.bitDisableEvents = true;
											oGrid.addRow(intValueID, rowValues, oGrid.getRowIndex("new"));
											oGrid.setRowAttribute(intValueID, "tabName", objTab.id);
											oGrid.setRowAttribute(intValueID, "objectType", objTab.childType);
											oGrid.bitDisableEvents = false;
										}
									});
								}
							}, 100);
							break;
					}
				});
			}
			//JAPR
			
			/* Invoca al proceso que elimina el objeto indicado, además de redireccionar exactamente al objeto que se define como seleccionado (opcional).
			Bloquea toda la forma de diseño para evitar utilizar alguna otra opción mientras ocurre el borrado si se utiliza el parámetro iRequestType != reqAjax (default)
			Si se utiliza el parámetro iRequestType == reqAjax, entonces se mandará el request de ese modo y se procesará con una función genérica para procesar el resultado,
			excepto si se especifica el parámetro oCallbackFn en cuyo caso ejecutará esa función como callback del proceso de grabado con el response recibido del server
			*/
			function doDeleteObject(iObjectType, iObjectID, iSelObjectType, iSelObjectID, iRequestType, oCallbackFn, bRemoveProgress) {
				console.log("doDeleteObject iObjectType == " + iObjectType + ", iObjectID == " + iObjectID + ", iSelObjectType == " + iSelObjectType + ", iSelObjectID == " + iSelObjectID);
				
				if (!iObjectType || !iObjectID) {
					return;
				}
				
				objFormTabs.tabs(tabDesign).progressOn();
				var strSelection = "";
				if ($.isNumeric(iSelObjectType) && $.isNumeric(iSelObjectID)) {
					strSelection = "&selItemType=" + iSelObjectType + "&selItemID=" + iSelObjectID;
				}
				if (iRequestType !== undefined) {
					strSelection += "&RequestType=" + iRequestType;
				}
				
				if (iRequestType == reqAjax) {
					//Invoca al método que realiza el borrado pero sin actualizar definiciones, simplemente ejecutará un ajax y recibirá un objeto de como callback invocando a la
					//función de callback (oCallbackFn) especificada si es que está definida, en caso contrario se invocará a la función genérica de callback de borrado
					//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					if (iObjectType == AdminObjectType.otyOption || iObjectType == AdminObjectType.otySkipRule) {
						var objObject = getObject(iSelObjectType, iSelObjectID, iObjectType, iObjectID);
					}
					else {
						var objObject = getObject(iObjectType, iObjectID);
					}
					if (objObject) {
						var objParams = {};
						objParams["Process"] = "Delete";
						objParams["ObjectType"] = iObjectType;
						objParams["ObjectID"] = iObjectID;
						
						//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
						//Almacena los parámetro de la última invocación de borrado para volver a invocar al método si se desea continuar con el proceso después de una confirmación
						//Si el objeto ya existiera y tuviera el parámetro para evitar validaciones activo, lo tiene que enviar como parte del request
						if (goObjectToDelete && goObjectToDelete.SkipValidation) {
							objParams["SkipValidation"] = 1;
						}
						
						goObjectToDelete = new Object();
						goObjectToDelete.objectType = iObjectType;
						goObjectToDelete.objectID = iObjectID;
						goObjectToDelete.selObjectType = iSelObjectType;
						goObjectToDelete.selObjectID = iSelObjectID;
						goObjectToDelete.requestType = iRequestType;
						goObjectToDelete.callbackFn = oCallbackFn;
						goObjectToDelete.removeProcess = bRemoveProgress;
						//JAPR
						setTimeout(objObject.send(objParams, oCallbackFn, bRemoveProgress), 100);
					}
					else {
						//Si no se logró encontrar el objeto especificado, por lo menos quita la progress bar para seguir trabajando en la ventana
						objFormTabs.tabs(tabDesign).progressOff();
					}
				}
				else {
					//Invoca al método que actualiza las definiciones en la celda de Preview y vuelve a generar la forma, por lo cual se hace un request mediante una forma que
					//permitirá ejecutar código cuando termine
					//JAPR 2015-07-28: Modificado para usar el request de Ajax y dejar el proceso de refresh localmente
					/*
					frmRequests.action = "processRequest.php?Process=Delete&SelSurveyID=" + intSurveyID + "&ObjectType=" + iObjectType + "&ObjectID=" + iObjectID + "&dte=<?=date('YmdHis')?>" + strSelection;
					frmRequests.target = "frameRequests";
					frmRequests.submit();
					*/
				}
			}
			
			/* Elimina la opción de simple/multiple choice seleccionada */
			function doDeleteOption(sTabName, iObjectID) {
				console.log ("doDeleteOption sTabName == " + sTabName + ", iObjectID == " + iObjectID);
				
				if (!sTabName || !objPropsTabs || !objPropsTabs.tabs(sTabName)) {
					return;
				}
				
				var objGrid = objPropsTabs.tabs(sTabName).getAttachedObject();
				if (!objGrid) {
					return;
				}
				
				var intObjectID = objGrid.bitLastSelectedRowId;
				if (!intObjectID) {
					alert("<?=str_ireplace("{var1}", translate("Option"), translate("Please select a {var1} to remove"))?>");
					return;
				}
				else {
					var intParentType = objGrid.bitObjectType;
					var intParentID = objGrid.bitObjectID;
					var objObject = getObject(intParentType, intParentID, AdminObjectType.otyOption, intObjectID);
					if (!objObject) {
						return;
					}
					
					var strObjectName = objObject.name;
					var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove this %s?"), translate("Option"))?>" + " " + strObjectName);
					if (blnAnswer) {
						doDeleteObject(AdminObjectType.otyOption, intObjectID, intParentType, intParentID, reqAjax, function(oResponse) {
							//Remueve el elemento de la lista si todo estuvo ok
							if (!oResponse || !oResponse.deleted || !oResponse.newObject) {
								objFormTabs.tabs(tabDesign).progressOff();
								return;
							}
							
							var objNewObject = oResponse.newObject;
							//Identifica que sea realmente un objeto del tipo esperado
							if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otyOption) {
								objFormTabs.tabs(tabDesign).progressOff();
								return
							}
							
							var intValueID = objNewObject.id;
							var strOptionName = objNewObject.name;
							var objParentObject = getObject(intParentType, intParentID);
							if (objParentObject && objParentObject.options) {
								delete objParentObject.options[strOptionName];
								delete objParentObject.optionsByID[intValueID];
								var intIndex = objParentObject.optionsOrder.indexOf(strOptionName);
								if (intIndex >= 0) {
									objParentObject.optionsOrder.splice(intIndex, 1);
								}
							}
							
							try {
								objGrid.deleteRow(intObjectID);
							} catch(e) {
							}
							objFormTabs.tabs(tabDesign).progressOff();
						}, true);
					}
				}
			}
			
			//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
			/* Elimina la configuración de salto seleccionada */
			function doDeleteSkipRule(sTabName, iObjectID) {
				console.log ("doDeleteSkipRule sTabName == " + sTabName + ", iObjectID == " + iObjectID);
				
				if (!sTabName || !objPropsTabs || !objPropsTabs.tabs(sTabName)) {
					return;
				}
				
				var objGrid = objPropsTabs.tabs(sTabName).getAttachedObject();
				if (!objGrid) {
					return;
				}
				
				var intObjectID = objGrid.bitLastSelectedRowId;
				if (!intObjectID) {
					alert("<?=str_ireplace("{var1}", translate("Skip rule"), translate("Please select a {var1} to remove"))?>");
					return;
				}
				else {
					var intParentType = objGrid.bitObjectType;
					var intParentID = objGrid.bitObjectID;
					var objObject = getObject(intParentType, intParentID, AdminObjectType.otySkipRule, intObjectID);
					if (!objObject) {
						return;
					}
					
					var strObjectName = objObject.name;
					var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove this %s?"), translate("Skip rule"))?>" + " " + strObjectName);
					if (blnAnswer) {
						doDeleteObject(AdminObjectType.otySkipRule, intObjectID, intParentType, intParentID, reqAjax, function(oResponse) {
							//Remueve el elemento de la lista si todo estuvo ok
							if (!oResponse || !oResponse.deleted || !oResponse.newObject) {
								objFormTabs.tabs(tabDesign).progressOff();
								return;
							}
							
							var objNewObject = oResponse.newObject;
							//Identifica que sea realmente un objeto del tipo esperado
							if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otySkipRule) {
								objFormTabs.tabs(tabDesign).progressOff();
								return
							}
							
							var intValueID = objNewObject.id;
							var objParentObject = getObject(intParentType, intParentID);
							if (objParentObject && objParentObject.skipRules) {
								delete objParentObject.skipRules[intValueID];
								var intIndex = objParentObject.skipRulesOrder.indexOf(intValueID);
								if (intIndex >= 0) {
									objParentObject.skipRulesOrder.splice(intIndex, 1);
								}
							}
							
							try {
								objGrid.deleteRow(intObjectID);
							} catch(e) {
							}
							objFormTabs.tabs(tabDesign).progressOff();
						}, true);
					}
				}
			}
			//JAPR
			
			/* Configura un grid previamente generado basado en el objeto con el formato similar a un TabCls, de tal manera que se puedan generar subGrids e invocar a este
			método para generarlos. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function doGenerateGrid(oGrid, oSettings, iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objGrid = oGrid;
				var objTab = oSettings;
				if (!objGrid || !objTab) {
					return;
				}
				
				var objValues = new Object();
				var objValuesOrder = new Array();
				objGrid.setImagePath("<?=$strScriptPath?>/images/");          //the path to images required by grid
				switch (objTab.type) {
					case propTabType.collection:
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						var strHeader = "";
						var strHeaderWidthPerc = "";
						var strHeaderWidth = "";
						var strHeaderAlign = "";
						var strHeaderTypes = "";
						var strHeaderResize = "";
						var arrHeaderStyle = new Array();
						var strAnd = "";
						var intPos = -1;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								strHeader += strAnd + aField.label;
								var strWidth = $.trim(aField.width);
								if ((intPos = strWidth.indexOf("%")) != -1) {
									strWidth = strWidth.substr(0, intPos);
									strHeaderWidthPerc += strAnd + ((strWidth)?strWidth:10);
								}
								else {
									strHeaderWidth += strAnd + ((strWidth)?strWidth:10);
								}
								strHeaderAlign += strAnd + ((aField.align)?aField.align:"left");
								strHeaderTypes += strAnd + ((aField.type)?aField.type:"ro");
								strHeaderResize += strAnd + ((aField.resize !== undefined)?aField.resize:true);
								arrHeaderStyle.push("background-color:cccccc;");
								strAnd = ",";
							}
						}
						
						objGrid.setHeader(strHeader, null, arrHeaderStyle);
						if (strHeaderWidthPerc) {
							objGrid.setInitWidthsP(strHeaderWidthPerc);
						}
						else {
							objGrid.setInitWidths(strHeaderWidth);
						}
						objGrid.setColAlign(strHeaderAlign);
						objGrid.setColTypes(strHeaderTypes);
						//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
						//Agregados los tipos de celdas (columnas) numéricos con una máscara genérica única
						//objGrid.setNumberFormat("0,000",2,".",",");
						//JAPR
						objGrid.enableMarkedCells();
						objGrid.enableResizing(strHeaderResize);
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitTabName = objTab.id;
						
						objGrid.bitValuesRendered = 0;
						var objValues = new Object();
						var objValuesOrder = new Array();
						if (objTab.getValues) {
							objValues = objTab.getValues();
							objValuesOrder = objTab._valuesOrder;
						}
						objGrid.bitValuesLength = objValuesOrder.length;
						
						//Realiza ajustes específicos a las columnas según el tipo de cada una
						var intColIndex = 0;
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							if (aField) {
								switch (aField.type) {
									case GFieldTypes.multiList:
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										objGrid.registerCList(intColIndex, cmbOptions);
										break;
								
									case GFieldTypes.combo:
										//Obtiene la referencia a la combo y le asigna los valores por default que tiene el field
										var cmbOptions = new Object();
										var cmbOptionsOrder = new Array();
										if (aField.getOptions) {
											var cmbOptions = aField.getOptions();
											var cmbOptionsOrder = aField._optionsOrder;
											var blnUseOrder = aField.useOrder;
										}
										
										var objCombo = objGrid.getCombo(intColIndex);
										if (objCombo) {
											objCombo.clear();
											if (aField.empty) {
												var idOption = 0;
												if (aField.emptyId !== undefined) {
													var idOption = aField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												objCombo.put(idOption, strName);
											}
											
											for (var idOption in cmbOptions) {
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												objCombo.put(idOption, strName);
											}
										}
										break;
								}
								intColIndex++;
							}
						}
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (rid == "new") {
								<?
								//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
								if ($this->FormType == formProdWithDev) {?>
								return;
								<?}
								//@JAPR
								?>
								
								//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
								var intObjectType = AdminObjectType.otyOption;
								var strTabName = this.bitTabName;
								var objTab = objTabsColl[strTabName];
								if (objTab) {
									intObjectType = objTab.childType;
								}
								
								switch(intObjectType) {
									case AdminObjectType.otySkipRule:
										addNewSkipRule(this, rid, ind);
										break;
									case AdminObjectType.otyOption:
									default:
										addNewOption(this, rid, ind);
										break;
								}
								//JAPR
							}
						});
						
						objGrid.attachEvent("onEditCell", function(stage,id,index) {
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otySurvey);
							var strTabName = this.getRowAttribute(id, "tabName");
							var objField = getFieldDefinitionByIndex(strTabName, index, blnForForm);
							if (objField) {
								if (objField.readOnly) {
									//Desactiva la edición de la celda
									return false;
								}
							}
							
							//Habilita la edición de la celda
							return true;
						});
						
						objGrid.attachEvent("onRowCreated", function(rId,rObj,rXml) {
							this.bitValuesRendered += 1;
							if (this.entBox && this.bitValuesRendered >= this.bitValuesLength) {
								setTimeout(function() {
									$(objGrid.entBox).find('img').css('display', 'inline');
									//$(objGrid.entBox).find('img').css('width', '5px');
									//$(objGrid.entBox).find('img').css('height', '8px');
									$(objGrid.entBox).find('img').css('cursor', 'pointer');
									$(objGrid.entBox).find('img').parents('td').attr('align', 'center');
								}, 100);
							}
						});						
						
						objGrid.attachEvent("onSubGridCreated", function(subgrid, rId, rInd) {
							//Primero obtiene la definición de la estructura del grid de detalles mediante la combinación TabName + FieldIdx, ya que las colecciones son
							//las únicas que generan un subGrid
							var strTabName = this.bitTabName;
							if (!objTabsColl || !strTabName || !objTabsColl[strTabName]) {
								return false;
							}
							
							var objCollectionTab = objTabsColl[strTabName];
							if (!objCollectionTab.fields || !objCollectionTab.fields[rInd]) {
								return false;
							}
							
							var objGridField = objCollectionTab.fields[rInd];
							if (!objGridField.definition || !objTabsColl[objGridField.definition]) {
								return false;
							}
							var objDefinitionTab = objTabsColl[objGridField.definition];
							
							//Genera el grid utilizando la definición del tab de detalles
							var strGridStyle = "font-family:Roboto Regular;font-size:12px;"
							subgrid.setStyle(strGridStyle, strGridStyle);
							subgrid.setNoHeader(true);
							subgrid.bitCollectionTabName = objGridField.tab;
							doGenerateGrid(subgrid, objDefinitionTab, this.bitObjectType, this.bitObjectID, objCollectionTab.childType, rId);
							if (subgrid.entBox) {
								setTimeout(function() {
									var intTop = parseInt($(subgrid.entBox).css('top').replace('px', ''));
									if (!$.isNumeric(intTop)) {
										intTop = 0;
									}
									
									if (subgrid.hdrBox) {
										var intHeight = $(subgrid.hdrBox).find('div.xhdr').css('height');
										$(subgrid.hdrBox).find('div.xhdr').css('height', '26px');
										$(subgrid.hdrBox).find('div.hdrcell').css('line-height', '26px');
									}
								}, 100);
							}
						});
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						objGrid.setHeader("<?=translate("Property")?>,<?=translate("Value")?>", null, ["background-color:cccccc;","background-color:cccccc;"]);
						objGrid.setInitWidthsP("30,70");	//50,50
						objGrid.setColAlign("left,left");
						objGrid.setColTypes("ro,ed");
						//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
						//Agregados los tipos de celdas (columnas) numéricos con una máscara genérica única
						//objGrid.setNumberFormat("0,000",1,".",",");
						//JAPR
						objGrid.enableEditEvents(true);
						objGrid.enableMarkedCells();
						objGrid.bitObjectType = iObjectType;
						objGrid.bitObjectID = iObjectID;
						objGrid.bitChildObjectType = iChildObjectType;
						objGrid.bitChildObjectID = iChildObjectID;
						objGrid.bitTabName = objTab.id;
						
						objGrid.attachEvent("onEditCancel", function(rId,cInd,val) {
							console.log('onEditCancel rId == ' + rId + ', cInd == ' + cInd + ', val == ' + val);
							
							var strTabName = this.getRowAttribute(rId, "tabName");
							var intObjectType = this.getRowAttribute(rId, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otySurvey);
							var objField = getFieldDefinition(strTabName, rId, blnForForm);
							if (objField) {
								switch (objField.type) {
									case GFieldTypes.formula:
										//Para este componente, por la forma en que se tuvieron que implementar los eventos de blur y keydown para actualizar al perder el foco, hay un
										//punto en el que el editor permanece abierto pero el foco cambia a otro componente (por ejemplo el icono de la fórmula) y eso invoca al grabado,
										//sin embargo internamente el editor continua con el valor original ya que no ha sido cerrado, por lo que al cancelar la edición finalmente, no
										//sólo cancela el posible valor que hubiera cambiado con el editor de fórmula, sino que revierte al valor que se tecleo antes de entrar al editor,
										//así que se pierde uno de los valores grabdos, por eso se tiene que obtener directamente de la propiedad value la cual si se actualizó al grabar,
										//en lugar de dejar que el grid revierta a su valor original antes entrar en modo de edición
										var strValue = this.getRowAttribute(rId, "value");
										this.cells(rId, cInd).setValue(strValue);
										break;
								}
							}
						});
						
						//JAPR 2016-12-15: Agregada la validación de ciertos campos según la propiedad validate de su definición (#IRERJD)
						/*objGrid.attachEvent("onValidationError", function(id, index, value, rule) {
							console.log("id = " + id + ", index = " + index + ", value = " + value + ", rule = " + rule);
							//En este punto se puede mostrar el mensaje de error correspondiente a la regla que falló la validación, además de agregar una propiedad bitValidated
							//a la celda para evitar que una segunda regla muestre un nuevo mensaje, dicha propiedad se limpiaría al iniciar la edición de la celda
						});*/
						//JAPR
						
						objGrid.attachEvent("onCellChanged", function(rId, cInd, nValue) {
							console.log('onCellChanged rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
							
							//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
							//agregando una bandera temporal al grid
							if (this.bitDisableEvents) {
								return;
							}
							
							//JAPR 2016-12-15: Agregada la validación de ciertos campos según la propiedad validate de su definición (#IRERJD)
							//En este punto se pueve aplicar la validación de la celda para confirmar si puede o no continuar con el grabado, en caso de no poder continuar porque
							//alguna regla falle, el método regresará false y podemos regresar al valor anterior pidiendo el getValue del FieldCls
							/*objPropsTabs.cells("generalTab").getAttachedObject().validateCell("shortName", 1)*/
							//JAPR
							
							var varValue = nValue;
							var varCollValue = nValue;
							try {
								//El componente GFieldTypes.multiList se actualiza con este evento, pero sus valores no se deben grabar como String sino como un array de IDs, así que
								//primero hace la conversión basado en el array de opciones que presentó el componente mientras estaba visible
								var blnUpdate = false;
								var strTabName = this.getRowAttribute(rId, "tabName");
								var intObjectType = this.getRowAttribute(rId, "objectType");
								var blnForForm = (intObjectType == AdminObjectType.otySurvey);
								var objField = getFieldDefinition(strTabName, rId, blnForForm);
								if (objField) {
									switch (objField.type) {
										case GFieldTypes.comboEdit:
											//Si se trata de un combo editable, hay dos posibilidades, se está recibiendo un ID de elemento del options, en cuyo caso es numérico (aunque
											//no se debe basar en ese hecho, simplemente hay que buscar el Id en las opciones disponibles) o se trata de un texto personalizado, así que
											//identifica el caso para grabar sólo el texto personalizado, ya que la combo es Editable
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											if (objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											for (var intIdx in cmbOptions) {
												if (intIdx == nValue) {
													varValue = cmbOptions[intIdx];
													break;
												}
											}
											blnUpdate = true;
											//@OMMC 2015/09/02: Agregado para evitar que se agregue &nbsp en campos numéricos al seleccionar formato vacío
											if(varValue == '&nbsp;'){
												varValue = "";
											}
											break;
									
										case GFieldTypes.multiList:
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											if (objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											var arrShowQuestionIDs = new Object();
											if (nValue) {
												var arrSelection = nValue.split(",");
												if (arrSelection && $.isArray(arrSelection) && arrSelection.length) {
													//Debe buscar la descripción (que para este componente debería ser única) en el array de opciones del campo para almacenar
													//internamente la colección de IDs seleccionados
													for (var intSelIdx in arrSelection) {
														var strDescription = $.trim(arrSelection[intSelIdx]);
														//JAPR 2016-07-29: Corregido un bug, removidos los doble espacios convertidos a &nbsp; por DHTMLX (#W34WOC) (#QHYOU7)
														strDescription = html_entity_decode($.trim(strDescription)); //strDescription.replace(/&nbsp;/gi, ' ');
														//JAPR
														for (var intIdx in cmbOptions) {
															//JAPR 2016-11-15: Corregido un bug, para estandarizar se hace el mismo trim que con el texto a comparar (#W34WOC)
															var strOptionName = $.trim(cmbOptions[intIdx]);
															if (strDescription == strOptionName) {
																//Esta opción fue seleccionada, debe grabar internamente el ID
																var intQuestionID = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
																arrShowQuestionIDs[intQuestionID] = intQuestionID;
																break;
															}
														}
													}
												}
											}
											
											blnUpdate = true;
											//JAPR 2015-07-30: Corregido un bug, si no se selecciona algún elemento sino que se quiere remover la selección, envía un elemento inválido
											//para permitir que el request procese algo, ya que de lo contrario considera que no hay valor y lo ignora (#65Z4K7)
											if ($.isEmptyObject(arrShowQuestionIDs)) {
												arrShowQuestionIDs[-1] = -1;
											}
											//JAPR
											varValue = arrShowQuestionIDs;
											//JAPR 2016-07-29: Corregido un bug, removidos los doble espacios convertidos a &nbsp; por DHTMLX (#W34WOC) (#QHYOU7)
											varCollValue = html_entity_decode($.trim(varCollValue)); //varCollValue.replace(/&nbsp;/gi, ' ');
											//JAPR
											//this.setRowAttribute(rId, "value", arrShowQuestionIDs);
											break;
										case GFieldTypes.uploadImg:
											blnUpdate = false;
											break;
										case GFieldTypes.formula:
											//Los inputs de las fórmulas no se actualizarán al cambiar el valor de la celda, ya que en un click sobre un área fuera de ella no se lanzaría
											//este evento en todos los casos
											break;
										//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
										//Agregados los tipos de celdas (columnas) numéricos con una máscara genérica única
										case GFieldTypes.number:
											//Si el campo es numérico, convertirá el valor a float para extraer la porción numérica y remover cualquier texto no válido antes de grabar
											if (!$.isNumeric(varValue)) {
												varValue = parseFloat(varValue);
												if (isNaN(varValue)) {
													varValue = "";
													varCollValue = "";
												}
												else {
													varCollValue = varValue;
												}
												
												this.bitDisableEvents = true;
												this.cells(rId, cInd).setValue(varValue);
												this.bitDisableEvents = false;
											}
											blnUpdate = true;
											break;
										//JAPR
										default:
											blnUpdate = true;
											break;
									}
								}
								
								if (blnUpdate) {
									doUpdateProperty(this, iObjectType, iObjectID, rId, cInd, varValue);
								}
								
								//Independientemente de si se mandó o no a grabar la configuración, si esto fuera un subgrid, debe actualizar la columna correspondiente en caso de
								//tener un detalle desplegado también en ella
								var strCollectionTabName = this.bitCollectionTabName;
								if (objField && strCollectionTabName && this.bitChildObjectID) {
									var blnForForm = (objField.parentType == AdminObjectType.otySurvey);
									var objTabComponent = undefined;
									try {
										if (blnForForm) {
											var objTabComponent = objFormTabs.tabs(tabDetail).getAttachedObject();
										}
										else {
											var objTabComponent = objPropsTabs;
										}
										
										if (objTabComponent && objTabComponent.tabs && objTabComponent.tabs(strCollectionTabName)) {
											var objCollGrid = objTabComponent.tabs(strCollectionTabName).getAttachedObject();
											//Obtiene la referencia al grid y tab que contienen la definición, para determinar si hay o no un campo que sea equivalente al editado,
											//si existe entonces si actualiza el valor en la columna correspondiente
											var objDefinitionTab = objTabsColl[strCollectionTabName];
											//JAPR 2015-07-30: Agregada una validación, al cambiar de página generaba una Excepción porque el Grid se destruía pero el
											//input o componente en modo de edición no, así que llegaba a este punto pero sin cells, se valida para no generar Excepción
											if (objCollGrid && objCollGrid.cells && objDefinitionTab && objDefinitionTab.fields) {
												var objCollField = getFieldDefinition(strCollectionTabName, rId);
												if (objCollField && objCollField.columnNum !== undefined) {		//columnNum
													objCollGrid.cells(this.bitChildObjectID, parseInt(objCollField.columnNum) -1).setValue(varCollValue);
												}
											}
										}
									} catch (e) {
										debugger;
									}
								}
							} catch(e) {
								console.log ("Error updating property value (type=" + iObjectType + ", id=" + iObjectID + ", prop=" + rId + "):" + e);
								debugger;
							}
						});
						
						objGrid.attachEvent("onCellMarked", function(rid, ind) {
							this.bitLastSelectedRowId = rid;
							this.bitLastSelectedColId = ind;
							if (ind != 1) {
								this.mark(rid, ind, false);
								this.mark(rid, 1, true);
							}
						});
						
						objGrid.attachEvent("onEditCell", /*$.proxy(*/function(stage,id,index) {
							var ind = this.getRowIndex(id);	//this.objGrid == this with Proxy
							if (index != 1) {
								return true;
							}
							
							var intObjectType = this.getRowAttribute(id, "objectType");
							var blnForForm = (intObjectType == AdminObjectType.otySurvey);
							switch (this.getRowAttribute(id, "type")) {
								case GFieldTypes.multiList:
									var strTabName = this.getRowAttribute(id, "tabName");
									//var objField = getFieldDefinition(strTabName, id, blnForForm);
									var cmbOptions = new Object();
									var cmbOptionsOrder = new Array();
									var objField = getFieldDefinition(strTabName, id, blnForForm);
									if (objField && objField.getOptions) {
										var cmbOptions = objField.getOptions();
										var cmbOptionsOrder = objField._optionsOrder;
										var blnUseOrder = objField.useOrder;
									}
									
									switch (stage) {
										case 0:	//Before start
											//Antes de iniciar, se debe llenar la columna con la lista de opciones corresponiente utilizando sólo las descripciones, ya que este
											//componente no soporta uso de ids
											this.registerCList(1, cmbOptions);
											break;
										
										case 1:
											//JAPR 2015-07-30: Agregados botones para seleccionar/deseleccionar todos los elementos
											$('.dhx_clist input:button').before('<img src="images/admin/selectall.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], true);" />');
											$('.dhx_clist input:button').before('<img src="images/admin/unselect.jpg" style="width:22px; height:22px;" onclick="javascript:fnSelectAllCListItems($(this).parents(\'.dhx_clist\')[0], false);" />');
											//JAPR
											$('.dhx_clist input:button').attr('value', '<?=translate("Apply")?>')
											//JAPR 2019-08-07: Corregido el despliegue de los componentes CList para que siempre estén visibles (#BT0NMN)
											if ( $('.dhx_clist').position().top + $('.dhx_clist').height() > $(window).height() ) {
												$('.dhx_clist').css('top', $('.dhx_clist').position().top - $('.dhx_clist').height())
											}
											//JAPR
											break;
										
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											/*
											var strSelection = this.cells(id, 1).getValue();
											if (strSelection) {
												var arrShowQuestionIDs = new Object();
												var arrSelection = strSelection.split(",");
												if (arrSelection && $.isArray(arrSelection) && arrSelection.length) {
													//Debe buscar la descripción (que para este componente debería ser única) en el array de opciones del campo para almacenar
													//internamente la colección de IDs seleccionados
													for (var intSelIdx in arrSelection) {
														var strDescription = arrSelection[intSelIdx];
														for (var intIdx in cmbOptions) {
															var strOptionName = cmbOptions[intIdx];
															if (strDescription == strOptionName) {
																//Esta opción fue seleccionada, debe grabar internamente el ID
																var intQuestionID = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
																arrShowQuestionIDs[intQuestionID] = intQuestionID;
																break;
															}
														}
													}
												}
												else {
													this.cells(id, 1).setValue('');
													this.setRowAttribute(id, "value", arrShowQuestionIDs);
												}
											}
											*/
											break;
									}
									break;
							
								case GFieldTypes.alphanum:
									switch (stage) {
										case 1:	//The editor is opened
											//Cuando se muestra el componente para captura, verifica si hay un límite de caracteres y dinámicamente modifica el input
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.maxLength > 0) {
												$(this.editor.obj).attr("maxlength", objField.maxLength);
											}
											break;
									}
									break;
								
								case GFieldTypes.comboEdit:
									//Se trata de un DHTMLCombo
									switch (stage) {
										case 0:	//Before start
											var objCombo = objGrid.cells(id, 1).getCellCombo()
											var strTabName = this.getRowAttribute(id, "tabName");
											
											var blnUseOrder = true;
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											objCombo.clearAll();
											var intSelectedIdx = -1;
											var strValue = this.getRowAttribute(id, "value");
											for (var intIdx in cmbOptionsOrder) {
												var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												
												objCombo.addOption(idOption, strName);
												if (strName == strValue) {
													intSelectedIdx = intIdx;
												}
											}
											
											//Selecciona la opción del combo que corresponde con el valor asignado a esta propiedad, si no hay alguno, deja el texto de la propiedad
											if (intSelectedIdx >= 0) {
												objCombo.selectOption(intSelectedIdx);
											}
											else {
												objCombo.setComboText(strValue);
											}
											break;
											
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											var strValue = this.cells(id, 1).getValue();
											
											//Si hubo un cambio, entonces esta combo habría regresado un Id, de lo contrario regresaría el texto de la celda del grid
											//así que se valida para que sólo los ids actualicen el valor real de la row
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField) {
												var cmbOptions = new Object();
												var cmbOptionsOrder = new Array();
												if (objField.getOptions) {
													var cmbOptions = objField.getOptions();
													var cmbOptionsOrder = objField._optionsOrder;
													var blnUseOrder = objField.useOrder;
												}
												
												for (var intIdx in cmbOptions) {
													if (intIdx == strValue) {
														strValue = cmbOptions[intIdx];
														break;
													}
												}
											}
											
											this.setRowAttribute(id, "value", strValue);
											break;
									}
									break;
								
								case GFieldTypes.combo:
									switch (stage) {
										case 0:	//Before start
											//Al iniciar la edición, se debe modificar el contenido del combo a los valores válidos para esta Row, además se debe
											//asignar como valor de la row el Id en lugar del texto correspondiente para que la combo pueda seleccionar correctamente
											//su valor al abrir la lista
											var combo = this.getCombo(index);
											combo.clear();
											var strTabName = this.getRowAttribute(id, "tabName");
											var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.empty) {
												var idOption = 0;
												if (objField.emptyId !== undefined) {
													var idOption = objField.emptyId;
												}
												var strName = "(<?=translate("None")?>)";
												combo.put(idOption, strName);
											}
											
											var blnUseOrder = true;
											var cmbOptions = new Object();
											var cmbOptionsOrder = new Array();
											//var objField = getFieldDefinition(strTabName, id, blnForForm);
											if (objField && objField.getOptions) {
												var cmbOptions = objField.getOptions();
												var cmbOptionsOrder = objField._optionsOrder;
												var blnUseOrder = objField.useOrder;
											}
											
											//JAPR 2016-04-14: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
											//Se comentó este código porque la función getOptions del field automáticamente obtiene sus opciones desde el campo Padre si es que hay alguno,
											//así que era código prácticamente duplicado
											/*if (objField && objField.parent) {
												var intParentValue = this.getRowAttribute(objField.parent, "value");
												var objParentField = getFieldDefinition(strTabName, objField.parent, blnForForm);
												if (objParentField && objParentField.options) {
													if (!$.isNumeric(intParentValue)) {
														if (objParentField.default !== undefined) {
															intParentValue = objParentField.default;
														}
													}
													var objParentOptions = objParentField.options[intParentValue];
													if (objParentOptions && objParentOptions.children) {
														cmbOptions = objParentOptions.children;
														//Si hay una configuración de orden, se utiliza para agregar los elementos en dicha secuencia, de lo contrario se usa el orden recibido
														var cmbOptionsOrder = objParentOptions.childrenOrder;
														if (cmbOptionsOrder) {
															blnUseOrder = true;
														}
														else {
															blnUseOrder = false;
															cmbOptionsOrder = cmbOptions;
														}
													}
												}
											}*/
											//JAPR
											
											for (var intIdx in cmbOptionsOrder) {
												var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
												var strName = cmbOptions[idOption].name;
												if (strName === undefined) {
													strName = cmbOptions[idOption];
												}
												combo.put(idOption, strName);
											}
											
											var intValue = this.getRowAttribute(id, "value");
											if (!$.isNumeric(intValue)) {
												if (objField && objField.default !== undefined) {
													intValue = objField.default;
												}
											}
											
											if ($.isNumeric(intValue)) {
												//JAPR 2015-06-18: Validado que al forzar el llenado manual de los componentes, no se lance el evento de onCellChanged
												//agregando una bandera temporal al grid
												//En este caso se está llenando así debido a como funciona la combo, pero realmente no se está asignando un valor
												this.bitDisableEvents = true;
												this.cells(id, 1).setValue(intValue.toString());
												this.bitDisableEvents = false;
											}
											break;
										
										case 2:	//Editor closed
											//Al terminar la edición hay que actualizar el atributo con el nuevo valor seleccionado
											var intValue = this.cells(id, 1).getValue();
											if ($.isNumeric(intValue)) {
												//Si hubo un cambio, entonces esta combo habría regresado un Id, de lo contrario regresaría el texto de la celda del grid
												//así que se valida para que sólo los datos numéricos actualicen el valor real de la row (incluso un 0 es válido como ID,
												//así que no deberían agregarse posibles textos con descripción numérica para no causar problemas)
												this.setRowAttribute(id, "value", intValue);
												
												//Se deben aplicar las reglas de visibilidad de la opción de respuesta seleccionada, ocultando todos los campos/tabs que otras
												//opciones debían mostrar, y mostrando sólo los que está configurada para ver
												var strTabName = this.getRowAttribute(id, "tabName");
												var objField = getFieldDefinition(strTabName, id, blnForForm);
												//Invoca al proceso que aplica la visbilidad de las opciones del campo
												objField.applyVisibilityRules();
												if (objField.setValuesFn) {
													objField.setValuesFn();
												}
											}
											break;
									}
									break;
							}
							
							//Return true permite la edición, false la cancela
							return true;
						}/*, {objGrid:objGrid})*/);
						break;
				}
				
				objGrid.enableTooltips("false, false");
				//JAPR 2016-12-15: Agregada la validación de ciertos campos según la propiedad validate de su definición (#IRERJD)
				/*objGrid.enableValidation(true)*/
				//JAPR
				objGrid.init();
				
				//Dependiendo del tipo de grid, configura las rows
				switch (objTab.type) {
					case propTabType.collection:
						$(objGrid.hdr).find('td div').css('padding-left', '0px');
						
						//Se debe generar el Grid sólo como una colección de valores con un mínimo de propiedades id y Name, siendo el id el rowId y el Name la columna default
						//var objValues = new Object();
						//var objValuesOrder = new Array();
						//if (objTab.getValues) {
						if (objValues && objValuesOrder && objValuesOrder.length) {
							//var objValues = objTab.getValues();
							//var objValuesOrder = objTab._valuesOrder;
							var blnUseOrder = objTab.useOrder;
							for (var intIdx in objValuesOrder) {
								var intValueID = (blnUseOrder)?objValuesOrder[intIdx]:intIdx;
								var objValue = objValues[intValueID];
								if (objValue) {
									var rowValues = new Array();
									for (var iField in objTab.fields) {
										var aField = objTab.fields[iField];
										if (aField) {
											var varValue = aField.getValue(undefined, objTab.childType, intValueID);
											if (varValue === undefined) {
												varValue = '';
											}
											rowValues.push(varValue);
										}
									}
									
									objGrid.bitDisableEvents = true;
									objGrid.addRow(intValueID, rowValues);
									objGrid.setRowAttribute(intValueID, "tabName", objTab.id);
									//objGrid.setRowAttribute(intValueID, "fieldName", intValueID);
									//objGrid.setRowAttribute(intValueID, "options", this.options);
									//objGrid.setRowAttribute(intValueID, "type", this.type);
									objGrid.setRowAttribute(intValueID, "objectType", objTab.childType);
									//objGrid.setRowAttribute(intValueID, "value", this.getValue(true));
									//if (this.hidden) {
									//	objGrid.setRowHidden(intValueID, true);
									//}
									//objGrid.setCellExcellType(intValueID, 1, this.type);
									objGrid.bitDisableEvents = false;
								}
							}
						}
						
						<?
						//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
						if ($this->FormType != formProdWithDev) {?>
						//Al final agrega la row para crear un nuevo elemento
						var strNewItemID = "new";
						var rowValues = new Array();
						rowValues.push("images/admin/new.gif");
						rowValues.push("(<?=translate("Add")?>)");
						rowValues.push("");
						objGrid.addRow(strNewItemID, rowValues);
						objGrid.setCellExcellType(strNewItemID, 0, GFieldTypes.image);
						objGrid.setCellExcellType(strNewItemID, 2, GFieldTypes.readOnly);
						objGrid.setRowTextStyle(strNewItemID, "cursor:pointer;");
						objGrid.cells(strNewItemID, 2).setValue("");
						<?}
						//@JAPR
						?>
						break;
					
					default:
						//Cada row representa una propiedad que se puede configurar de manera diferente según el tipo de campo
						//objGeneralGrid.setColSorting("int,str,str,int");
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							aField.draw(objGrid, iChildObjectType, iChildObjectID);
							//Asigna la visibilidad por default del campo
							if (objTab.defaultVisibility && !objTab.defaultVisibility[aField.name]) {
								objGrid.setRowHidden(aField.name, true);
							}
						}
						
						//Al terminar de generar el grid, aplica la visibilidad encadenada de los componentes que la tengan definida y cuya visibilidad default sea visible
						for (var iField in objTab.fields) {
							var aField = objTab.fields[iField];
							//Asigna la visibilidad por default del campo
							if (objTab.defaultVisibility && objTab.defaultVisibility[aField.name]) {
								aField.applyVisibilityRules();
							}
						}
						break;
				}
			}
			
			/* Genera la ventana de propiedades de la forma
			*/
			function generateSurveyProps(sNodeId) {
				console.log('generateSurveyProps ' + sNodeId);
				//El id del nodo de la forma empieza con el prefijo "sv"	
				var intSurveyID = sNodeId.substr(2);
				objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?> - " + selSurvey.name);
				
				//Generar todos los fields
				/*@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta*/
				/*GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental*/
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
				//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
				objSurveyTabsColl = {
					"generalTab": {id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true, defaultVisibility:{
							name:true, descrip:true, displayImageDes:true, displayMenuImageDes:true
						}
					},
					"advTab": {id:"advTab", name:"<?=translate("Advanced")?>", defaultVisibility:{
							syncWhenSaving:true, hideNavButtons:true, disableEntry:true, commentLabel:true, additionalEMails:true, incrementalFile:true
						}
					}
				};
				
				//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
				if (vObject.serviceVersion >= vObject.updateVersion.esvTemplateStyles) {
					objSurveyTabsColl["advTab"]["defaultVisibility"]["templateID"] = true;
				}
				//JAPR
				//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
				if (vObject.serviceVersion >= vObject.updateVersion.esvShowQNumbers) {
					objSurveyTabsColl["advTab"]["defaultVisibility"]["showQNumbers"] = true;
				}
				//JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
				if (vObject.serviceVersion >= vObject.updateVersion.esvFormsDuration) {
					objSurveyTabsColl["advTab"]["defaultVisibility"]["plannedDuration"] = true;
				}
				//JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
				if (vObject.serviceVersion >= vObject.updateVersion.esvNavigationHistory) {
					objSurveyTabsColl["advTab"]["defaultVisibility"]["enableNavHistory"] = true;
				}
				
				//JAPR 2016-06-01: Validada la versión de metadata para la opción de templates (#FSMUZC)
				if (vObject.serviceVersion >= vObject.updateVersion.esvTemplateStyles) {
					objSurveyTabsColl["layoutTab"] = {id:"layoutTab", name:"<?=translate("Format")?>", defaultVisibility:{width:true, height:true, textPos:true, textSize:true, customLayout:true} };
				}
				//JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
				if (vObject.serviceVersion >= vObject.updateVersion.esvAutoFitFormsImage) {
					objSurveyTabsColl["layoutTab"]["defaultVisibility"]["autoFitImage"] = true;
				}
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				if (vObject.serviceVersion >= vObject.updateVersion.esvEntryDescription) {
					objSurveyTabsColl["advTab"]["defaultVisibility"]["questionIDForEntryDesc"] = true;
				}
				if ( vObject.serviceVersion >= vObject.updateVersion.esvReloadSurveyFromSection ){
					objSurveyTabsColl["advTab"]["defaultVisibility"]["reloadSurveyFromSection"] = true;
				}
				var objCommon = {id:intSurveyID, parentType:AdminObjectType.otySurvey, tab:"generalTab"};
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				objSurveyTabsColl["generalTab"].fields =  new Array(
					//JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>" <?if ($this->FormType == formDevelopment) {?>, disabled:true<?} elseif ($this->FormType == formProdWithDev) {?>, disabled:false<?}?>})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"descrip", label:"<?=translate("Description")?>", maxLength:255})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"displayImageDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg, maxLength:255})),
					//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"displayMenuImageDes", label:"<?=translate("Menu Image")?>", type:GFieldTypes.uploadImg, maxLength:255}))
				);
				
				var objCommon = {id:intSurveyID, parentType:AdminObjectType.otySurvey, tab:"advTab"};
				objSurveyTabsColl["advTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"hasSignature", label:"<?=translate("Has signature?")?>"})),	//Has signature?
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"allowPartialSave", label:"<?=translate("Allow partial Save")?>"})),	//Allow partial Save?
					//new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"elementQuestionID", label:"<?=translate("Action element")?>", type:GFieldTypes.combo})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"syncWhenSaving", label:"<?=translate("Auto syncronize")?>"})),	//Synchronize when saving?
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"hideNavButtons", label:"<?=translate("Navigation")?>"})),	//Hide navigation buttons?
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"hideSectionNames", label:"<?=translate("Section titles")?>"})),	//Hide section names?
					//JAPR 2016-12-16: Renombrada la opción (#2UT02L)
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"disableEntry", label:"<?=translate("Hide save button")?>"})),	//Disable entry?
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"otherLabel", label:"<?=translate("Label for other")?>"})),		//Label for other
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"commentLabel", label:"<?=translate("Label for comment")?>"})),	//Label for comment
					//OMMC 2015-12-10: Agregada celda de formula editor a la lista de correos, se quería poder hacer referencia al email que pudiese ser capturado en las preguntas de la forma. #R6RODN
					new FieldCls($.extend(true, {}, objCommon, {name:"additionalEMails", label:"<?=translate("Additional emails")?>", type:GFieldTypes.formula})),	//Additional emails  for capture report delivery
					//new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"isAgendable", label:"<?=translate("Agendable")?>"})),	//Is agendable?
					//new FieldCls($.extend(true, {}, objCommon, {name:"radioForAgenda", label:"<?=translate("Agenda radio")?>", type:GFieldTypes.number})),	//Agenda radio
					//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"incrementalFile", label:"<?=translate("Incremental file name")?>"})),
					//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
					new FieldCls($.extend(true, {}, objCommon, {name:"templateID", label:"<?=translate("Default colour template")?>", type:GFieldTypes.combo, options:Templates, optionsOrder:TemplatesOrder, empty:true, emptyId:-1, default:-1}))
				);
				//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
				if(vObject.serviceVersion >= vObject.updateVersion.esvShowQNumbers){
					objSurveyTabsColl["advTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, objCheckPlusDef, {name:"showQNumbers", label:"<?=translate("Show question numbers")?>"})));
				}
				//JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
				if(vObject.serviceVersion >= vObject.updateVersion.esvFormsDuration){
					objSurveyTabsColl["advTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, {name:"plannedDuration", label:"<?=translate("Planned duration")." (".translate("Minutes").")"?>", type:GFieldTypes.number})));
				}
				//JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
				if (vObject.serviceVersion >= vObject.updateVersion.esvNavigationHistory) {
					objSurveyTabsColl["advTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, objCheckPlusDef, {name:"enableNavHistory", label:"<?=translate("Enable navigation history")?>"})));
				}
				//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
				if (vObject.serviceVersion >= vObject.updateVersion.esvEntryDescription) {
					var objFilterTypes = {};
					//JAPR 2017-04-24: Por solicitud de DAbdo, se limita esta configuración únicamente a preguntas alfanuméricas (#UO4Q4M)
					//objFilterTypes[Type.number] = 1;
					//objFilterTypes[Type.simplechoice] = 1;
					//objFilterTypes[Type.date] = 1;
					//objFilterTypes[Type.text] = 1;
					objFilterTypes[Type.alfanum] = 1;
					//objFilterTypes[Type.time] = 1;
					//objFilterTypes[Type.calc] = 1;
					//objFilterTypes[Type.callList] = 1;
					//objFilterTypes[Type.gps] = 1;
					//objFilterTypes[Type.barcode] = 1;
					//objFilterTypes[Type.myLocation] = 1;
					//JAPR 2017-04-26: Agregado el filtro por tipo de sección (#UO4Q4M)
					var objFilterSectionTypes = {};
					objFilterSectionTypes[SectionType.Standard] = 1;
					objSurveyTabsColl["advTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, {name:"questionIDForEntryDesc", label:"<?=translate("Question for entry description")?>", type:GFieldTypes.combo, empty:true, emptyId:0, default:0, 
						options:((selSurvey.getAllQuestionItems)?(function() {return selSurvey.getAllQuestionItems({addEmpty: true, emptyId: 0, addNumber: true, filterTypes: objFilterTypes, filterSectionTypes: objFilterSectionTypes});}):{}),
						optionsOrder:((selSurvey.getAllQuestionsItemsOrder)?(function() {return selSurvey.getAllQuestionsItemsOrder();}):{})
					})));
				}
				if (vObject.serviceVersion >= vObject.updateVersion.esvReloadSurveyFromSection) {
					objSurveyTabsColl["advTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, {name:"reloadSurveyFromSection", label:"<?=translate("Section for activate save and new function")?>", type:GFieldTypes.combo, empty:true, emptyId:0, default:0, 
						options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
						optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
					})));
				}
				//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
				//JAPR 2016-06-01: Validada la versión de metadata para la opción de templates (#FSMUZC)
				if (vObject.serviceVersion >= vObject.updateVersion.esvTemplateStyles) {
					var objCommon = {id:intSurveyID, parentType:AdminObjectType.otySurvey, tab:"layoutTab"};
					objSurveyTabsColl["layoutTab"].fields =  new Array(
						new FieldCls($.extend(true, {}, objCommon, {name:"width", label:"<?=translate("Form layout width")?>", type:GFieldTypes.combo, options:optionsFormsSizes, default:300})),
						new FieldCls($.extend(true, {}, objCommon, {name:"height", label:"<?=translate("Form layout height")?>", type:GFieldTypes.combo, options:optionsFormsSizes, default:100})),
						new FieldCls($.extend(true, {}, objCommon, {name:"textPos", label:"<?=translate("Form layout text pos")?>", type:GFieldTypes.combo, options:optionsFormsLayOutTextPos, optionsOrder:optionsFormsLayOutTextPosOrder, default:<?=svtxpDefault?>})),
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"textSize", label:"<?=translate("Form layout text size")?>"})),
						new FieldCls($.extend(true, {}, objCommon, {name:"customLayout", label:"<?=translate("Form custom layout html")?>", empty:true, type:GFieldTypes.editorHTMLDialog, size:5000}))
					);
				}
				//JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
				if (vObject.serviceVersion >= vObject.updateVersion.esvAutoFitFormsImage) {
					objSurveyTabsColl["layoutTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, objCheckPlusDef, {name:"autoFitImage", label:"<?=translate("Autofit image")?>"})));
				}
				
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Marcará todos los campos como deshabilitados según el tipo de forma
				if ($this->FormType == formProdWithDev) {?>
				for (var strTabID in objSurveyTabsColl) {
					if (objSurveyTabsColl[strTabID].fields && objSurveyTabsColl[strTabID].fields.length) {
						for (var intFieldNum in objSurveyTabsColl[strTabID].fields) {
							if (objSurveyTabsColl[strTabID].fields[intFieldNum].disabled === undefined) {
								objSurveyTabsColl[strTabID].fields[intFieldNum].disabled = true;
							}
						}
					}
				}
				<?}
				//@JAPR
				?>
				
				//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
				//Los detalles de la forma se muestran en el tab de propiedades
				generateFormDetail(objSurveyTabsColl, AdminObjectType.otySurvey, intSurveyID, objFormTabs.tabs(tabDetail));
			}
			
			/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default para la sección del id especificado) según su configuración */
			function getSectionFieldsDefaulVisibility(id, oTabsColl) {
				console.log("getSectionFieldsDefaulVisibility " + id);
				
				var intSectionID = id;
				var objSection = selSurvey.sections[intSectionID];
				if (!objSection) {
					return oTabsColl;
				}
				
				var strTabId = "generalTab";
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};
					//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
					oTabsColl[strTabId].defaultVisibility["name"] = true;
					//oTabsColl[strTabId].defaultVisibility["type"] = true;
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
					//oTabsColl[strTabId].defaultVisibility["repetitions"] = true;
					oTabsColl[strTabId].defaultVisibility["sectionType"] = true;
					//JAPR
				}
				
				var strTabId = "advTab";
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};
					oTabsColl[strTabId].defaultVisibility["nextSectionID"] = true;
					oTabsColl[strTabId].defaultVisibility["showCondition"] = true;
					oTabsColl[strTabId].defaultVisibility["showInNavMenu"] = true;
					//@RTORRES 2015-08-28: Agregado ocultar boton next,back
					oTabsColl[strTabId].defaultVisibility["hideNavButtons"] = true;
					oTabsColl[strTabId].defaultVisibility["htmlHeaderDes"] = true;
					oTabsColl[strTabId].defaultVisibility["htmlFooterDes"] = true;
				}
				
				//JAPR 2015-09-02: Validado este proceso porque al actualizar la propiedad se grababa como string
				switch (parseInt(objSection.sectionType)) {
					case SectionType.Formatted:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["formattedText"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["autoRedraw"] = true;
						}
						break;
					
					case SectionType.Dynamic:
					case SectionType.Inline:
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
							//oTabsColl[strTabId].defaultVisibility["displayMode"] = true;
							oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = true;
							//Ticket #QE5M4Y: Cuando configuras una sección como Repeticiones basadas en datos, no deben aparecer en Avanzado las opciones:
							//Mostrar casilla, comportamiento invertido y minimo de seleccion
							if(objSection.displayMode==<?=sdspPages?>)
							{
								oTabsColl[strTabId].defaultVisibility["showSelector"] = false;
								oTabsColl[strTabId].defaultVisibility["switchBehaviour"] = false;
								oTabsColl[strTabId].defaultVisibility["MinRecordsToSelect"] = false;
								//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
								oTabsColl[strTabId].defaultVisibility["useDescriptorRow"] = false;
								//JAPR
							}
							else
							{
								oTabsColl[strTabId].defaultVisibility["showSelector"] = true;
								oTabsColl[strTabId].defaultVisibility["switchBehaviour"] = true;
								oTabsColl[strTabId].defaultVisibility["MinRecordsToSelect"] = true;
								//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
								oTabsColl[strTabId].defaultVisibility["useDescriptorRow"] = true;
								//JAPR
							}
							//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
							if (objSection.valuesSourceType == TypeOfFill.tofCatalog) {
								oTabsColl[strTabId].defaultVisibility["dataSourceFilter"] = true;
							}
						}
						break;
						
					case SectionType.Multiple:
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
							//oTabsColl[strTabId].defaultVisibility["displayMode"] = true;
							oTabsColl[strTabId].defaultVisibility["summaryInMDSection"] = true;
						}
						break;
						
					case SectionType.Standard:
					default:
						break;
				}
				
				return oTabsColl;
			}
			
			/* Genera el grid con las propiedades de la sección */
			function generateSectionProps(sNodeId) {
				console.log('generateSectionProps ' + sNodeId);
				
				//El id de la sección empieza con el prefijo "s"	
				var intSectionID = sNodeId.substr(1);
				var objObject = getObject(AdminObjectType.otySection, intSectionID);
				if (!objObject) {
					return;
				}
				
				objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?> - " + objObject.getName());
				objTabsColl = {
					"generalTab": new TabCls($.extend(true, {}, {id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true})),
					"advTab": new TabCls($.extend(true, {}, {id:"advTab", name:"<?=translate("Advanced")?>"}))
				};
				
				var objCommon = {id:intSectionID, parentType:AdminObjectType.otySection, tab:"generalTab"};
				objTabsColl["generalTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>", maxLength:255})),
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
					//new FieldCls($.extend(true, {}, objCommon, {name:"repetitions", label:"<?=translate("Repetitions")?>", type:GFieldTypes.combo, options:opSectionsRepetitionsType})),
					new FieldCls($.extend(true, {}, objCommon, {name:"sectionType", label:"<?=translate("Type")?>", type:GFieldTypes.combo, options:opSectionsType,
						setValuesFn:function() {
							//Al cambiar esta configuración se debe actualizar el preview con el nuevo tipo de despliegue aplicado
							var intSectionType = parseInt(this.getValue(true));
							
							//Obtiene la referencia a la instancia de sección en memoria
							var intObjectType = this.parentType;
							var intObjectID = this.id;
							var objObject = getObject(intObjectType, intObjectID);
							if (!objObject || objObject.type === undefined) {
								return;
							}
							
							if (!objFormsLayout || !objFormsLayout.cells || !objFormsLayout.cells(dhxPreviewCell)) {
								return;
							}
							
							try {
								//Obtiene la referencia a la sección en el preview
								var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
								if (!objiFrame.contentWindow.frmDevice) {
									return;
								}
								if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
									if (!objiFrame.contentWindow.frmDevice.selSurvey) {
										return;
									}
								} else {
									if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
										return;
									}
								}
								
								var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
								if (!objselSurvey || !objselSurvey.sections) {
									return;
								}
								
								var objSection = objselSurvey.sections[intObjectID];
								if (!objSection) {
									return;
								}
								
								//Antes de generar el HTML, lo elimina basado en la configuración anterior porque al cambiar el tipo y número de registros habrá alguno que ya no sea válido
								//Obtiene la referencia a JQuery directo del frame del preview
								var objJQPrev = objiFrame.contentWindow.frmDevice.$ || objiFrame.contentWindow.frmDevice.contentWindow.$;
								if (objJQPrev) {
									var strPageId = 'section_'+objSection.number;
									objJQPrev('[data-role="dialog"][owner='+strPageId+']').remove();
									objJQPrev('#'+strPageId).remove();
									//Si está en modo de diseño y sin simulación, es seguro que no hay datos así que sólo podría haber un único registro
									var strPageId = 'section_'+objSection.number+'_rec_0';
									objJQPrev('[data-role="dialog"][owner='+strPageId+']').remove();
									objJQPrev('#'+strPageId).remove();
									//JAPR 2015-09-02: Dado a que se pueden generar secciones Inline en modo paginación, el caso mencionado arriba no necesariamente es cierto, puede
									//haber tanto resgistros como la configuración de la sección lo permita, así que remueve hasta maxRecordNum asumiendo que el registro 0 ya se borró
									for (var intRecordNum = 1; intRecordNum < objSection.maxRecordNum; intRecordNum++) {
										var strPageId = 'section_'+objSection.number+'_rec_'+intRecordNum;
										objJQPrev('[data-role="dialog"][owner='+strPageId+']').remove();
										objJQPrev('#'+strPageId).remove();
									}
									//JAPR
								}
								
								//Actualiza la definición del tipo de seccióny despliegue del objeto en el preview así como el resto de las configuraciones relacionadas, y ejecuta el reDraw
								//para que se vean reflejados los cambios
								if (intSectionType == SectionType.Dynamic) {
									objObject.type = SectionType.Inline;
								}
								else {
									objObject.type = intSectionType;
								}
								objSection.type = objObject.type;
								objSection.sectionType = intSectionType;
								switch (intSectionType) {
									case SectionType.Dynamic:
										objObject.displayMode = <?=sdspPages?>;
										break;
									case SectionType.Inline:
										objObject.displayMode = <?=sdspInline?>;
										break;
									default:
										objObject.displayMode = parseInt(objObject.displayMode);
										break;
								}
								objSection.displayMode = objObject.displayMode;
								objSection.selectorQuestionID = objObject.selectorQuestionID;
								//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
								objSection.xPosQuestionID = objObject.xPosQuestionID;
								objSection.yPosQuestionID = objObject.yPosQuestionID;
								objSection.itemNameQuestionID = objObject.itemNameQuestionID;
								//@JAPR
								
								//Regenera las referencias internas requeridas por el cambio de tipo de sección
								if (objSection.addClassPrototype) {
									objSection.addClassPrototype();
								}
								
								//Actualiza otras configuraciones que son reelevantes para esta específicamente
								var intData = parseInt(objObject.sourceQuestionID);
								if (!$.isNumeric(intData)) {
									intData = 0;
								}
								objSection.sourceQuestionID = intData;
								var intData = parseInt(objObject.sourceSectionID);
								if (!$.isNumeric(intData)) {
									intData = 0;
								}
								objSection.sourceSectionID = intData;
								
								//Vuelve a generar el HTML de la sección y cambia la página a ella
								if (objSection.reDraw) {
									objSection.reDraw();
								}
								
								objObject.refreshView(objObject);
							} catch(e) {
								console.log("Error updating sectionType in preview: " + e);
							}
						}
					})),
					//JAPR
					new FieldCls($.extend(true, {}, objCommon, {name:"formattedText", label:"<?=translate("HTML Code")?>", type:GFieldTypes.editor}))
				);
				
				var objCommon = {id:intSectionID, parentType:AdminObjectType.otySection, tab:"advTab"};
				objTabsColl["advTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, {name:"nextSectionID", label:"<?=translate("Next section")?>", type:GFieldTypes.combo, empty:true,
						options:((objObject.getNextSectionItems)?(function() {return objObject.getNextSectionItems();}):{}),
						optionsOrder:((objObject.getNextSectionItems)?(function() {return objObject.getNextSectionItemsOrder();}):{})
					})),	//After response, skip to section
					new FieldCls($.extend(true, {}, objCommon, {name:"showCondition", label:"<?=translate("Show condition")?>", type:GFieldTypes.formula})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"autoRedraw", label:"<?=translate("Auto redraw")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"summaryInMDSection", label:"<?=translate("Show summary")?>"})),
					//JAPR 2016-06-07: Removida esta configuración ya que no aplica desde que se ocultó el menú selector de secciones del App (#VED4D4)
					//new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"showInNavMenu", label:"<?=translate("Show in navigation menu?")?>", default:1})),
					//@RTORRES 2015-08-28: Agregado ocultar boton next,back
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"hideNavButtons", label:"<?=translate("Hide next and back")?>", default:0})),
					//JAPR 2018-11-05: Agregada la configuración para agregar la tr de la descripción del registro en lugar de la columna correspondiente (#VU53QY)
					<?if (getMDVersion() >= esvTableSectDescRow) {?>
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"useDescriptorRow", label:"<?=translate("Show descriptor row")?>"})),
					<?}?>
					//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
					/*new FieldCls($.extend(true, {}, objCommon, {name:"displayMode", label:"<?=translate("Display mode")?>", type:GFieldTypes.combo, options:opDisplayModes, default:0})),*/
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"showSelector", label:"<?=translate("Show checkbox")?>"})),
					//new FieldCls($.extend(true, {}, objCommon, {name:"selectorQuestionID", label:"<?=translate("Selector question")?>", type:GFieldTypes.combo})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"switchBehaviour", label:"<?=translate("Switch behaviour")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"MinRecordsToSelect", label:"<?=translate("Minimum checked")?>", type:GFieldTypes.number})),
					new FieldCls($.extend(true, {}, objCommon, {name:"valuesSourceType", label:"<?=translate("Source")?>", type:GFieldTypes.combo, options:opSectionTypeOfFilling,
						setValuesFn:function() {
							//Al cambiar esta configuración se deben cambiar las opciones de despliegue dependiendo de si la fuente de valores es o no un catálogo
							var intObjectType = this.parentType;
							var intObjectID = this.id;
							var objObject = getObject(intObjectType, intObjectID);
							if (!objObject || !objObject.type) {
								return;
							}
							
							//JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
							//Independientemente del tipo de pregunta, si se dejó de utilizar un catálogo se debe aplicar limpieza a las propiedades asociadas
							if (objObject.valuesSourceType != TypeOfFill.tofCatalog) {
								var objField = getFieldDefinition(this.tab, "dataSourceID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataSourceMemberID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataSourceFilter");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
							}
							//JAPR
						}
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"strSelectOptions", label:"<?=translate("Values")?>", type:GFieldTypes.text, size:5000})),
					new FieldCls($.extend(true, {}, objCommon, {name:"sourceSectionID", label:"<?=translate("Source section")?>", type:GFieldTypes.combo, empty:true, default:0,
						options:((objObject.getSourceSectionItems)?(function() {return objObject.getSourceSectionItems();}):{}),
						optionsOrder:((objObject.getSourceSectionItemsOrder)?(function() {return objObject.getSourceSectionItemsOrder();}):{})
					})),	//Obtain response items from the following Inline section
					new FieldCls($.extend(true, {}, objCommon, {name:"sourceQuestionID", label:"<?=translate("Source question")?>", type:GFieldTypes.combo, empty:true, default:0,
						options:((objObject.getSourceQuestionItems)?(function() {return objObject.getSourceQuestionItems();}):{}),
						optionsOrder:((objObject.getSourceQuestionItemsOrder)?(function() {return objObject.getSourceQuestionItemsOrder();}):{}),
						setValuesFn:function() {
							//Al cambiar esta opción hará el mismo tipo de refresh que al cambiar el modo de despliegue
							//JAPR 2015-09-02: Modificado de nuevo el comportamiento, ahora se solicitó que se configure un SectionType, así que ya estaría descontinuada esta propiedad
							//var objField = getFieldDefinition(this.tab, "displayMode");
							var objField = getFieldDefinition(this.tab, "sectionType");
							//JAPR
							if (objField) {
								if (objField.setValuesFn) {
									objField.setValuesFn(false);
								}
							}
						}
					})),	//Obtain response items from the following question
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceID", label:"<?=translate("Catalog")?>", type:GFieldTypes.combo, options:arrCatalogs, optionsOrder:arrCatalogsOrder, empty:true,
						setValuesFn:fnSetChildrenValues, children:["dataSourceMemberID"]
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceMemberID", label:"<?=translate("Attribute")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceFilter", label:"<?=translate("Filter")?>", type:GFieldTypes.formula})),
					new FieldCls($.extend(true, {}, objCommon, {name:"htmlHeaderDes", label:"<?=translate("Customized header")?>", type:GFieldTypes.editorHTMLDialog})),
					new FieldCls($.extend(true, {}, objCommon, {name:"htmlFooterDes", label:"<?=translate("Customized footer")?>", type:GFieldTypes.editorHTMLDialog}))
				);
				
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Marcará todos los campos como deshabilitados según el tipo de forma
				if ($this->FormType == formProdWithDev) {?>
				for (var strTabID in objTabsColl) {
					if (objTabsColl[strTabID].fields && objTabsColl[strTabID].fields.length) {
						for (var intFieldNum in objTabsColl[strTabID].fields) {
							if (objTabsColl[strTabID].fields[intFieldNum].disabled === undefined) {
								objTabsColl[strTabID].fields[intFieldNum].disabled = true;
							}
						}
					}
				}
				<?}
				//@JAPR
				?>
				
				generateFormDetail(getSectionFieldsDefaulVisibility(intSectionID, objTabsColl), AdminObjectType.otySection, intSectionID);
			}
			
			/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default para la pregunta del id especificado) según su configuración */
			function getQuestionFieldsDefaulVisibility(id, oTabsColl) {
				console.log("getQuestionFieldsDefaulVisibility " + id);
				
				var intQuestionID = id;
				var objQuestion = selSurvey.questions[intQuestionID];
				if (!objQuestion) {
					return oTabsColl;
				}
				
				//Todos los tipos de pregunta tienen por lo menos la etiqueta y el ShortName
				var strTabId = "generalTab";
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};
					oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
				}
				
				//JAPR 2016-05-12: Modificado el esquema de visibilidad de preguntas a partir de una condición (#NMW148)
				var strTabId = "advTab";
				if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
					oTabsColl[strTabId].defaultVisibility = {};
					oTabsColl[strTabId].defaultVisibility["showCondition"] = true;
					//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
					oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = true;
				}
				
				//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
				var objSection = selSurvey.sections[objQuestion.sectionID];
				//JAPR
				switch (objQuestion.type) {
					case Type.simplechoice:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = true;
							//OMMC 2016-03-29: Agregada la propiedad para la galería
							if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = true;
							}
							//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
							if (objQuestion.displayMode == DisplayMode.dspGetData || objQuestion.displayMode == DisplayMode.dspMetro) {
								//JAPR 2016-08-09: Validado que la pregunta metro si pueda configurarse como requerida (#XRKG3R)
								if (objQuestion.displayMode == DisplayMode.dspGetData) {
									oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = false;
								}
								//JAPR
								oTabsColl[strTabId].defaultVisibility["hasReqComment"] = false;
								oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = false;
								//OMMC 2016-03-29: Agregada la propiedad para la galería
								if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
									oTabsColl[strTabId].defaultVisibility["allowGallery"] = false;
								}
							}
							//OMMC 2019-03-25: Pregunta AR #4HNJD9
							if (objQuestion.displayMode == DisplayMode.dspAR) {
								oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = false;
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = false;
							}
							//JAPR 2016-05-16: Agregado el soporte para defaultValue en preguntas simple choice de catálogo (#ZQIT35)
							if (!objQuestion.useMap) {
								oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
							}
							//JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
							if ( objQuestion.displayMode == DisplayMode.dspMenu && objSection.selectorQuestionID != objQuestion.id ) {
								oTabsColl[strTabId].defaultVisibility["autoSelectEmptyOpt"] = (objQuestion.defaultValue == '');
							}
							//OMMC 2016-04-21: Agregado para las preguntas Menú 
							if (objQuestion.noFlow) {
								oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = false;
								oTabsColl[strTabId].defaultVisibility["hasReqComment"] = false;
								oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = false;
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = false;
								oTabsColl[strTabId].defaultVisibility["defaultValue"] = false;
							}
						}
						
						var strTabId = "valuesTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							//JAPR 2015-06-29: Modificada la visibilidad para usar el parámetro "self" para referirse a la propia tab y permitir ocultar campos de la misma
							oTabsColl[strTabId].defaultVisibility["self"] = (objQuestion.valuesSourceType == TypeOfFill.tofFixedAnswers);
							//oTabsColl[strTabId].defaultVisibility["typeOfFilling"] = true;
							//oTabsColl[strTabId].defaultVisibility["strSelectOptions"] = true;
						}
						
						var strTabId = "valuesDetailTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = false;
							oTabsColl[strTabId].defaultVisibility["name"] = true;
							oTabsColl[strTabId].defaultVisibility["score"] = true;
							//OMMC 2016-05-26: Agregado para las preguntas Menú 
							if (!objQuestion.noFlow) {
								oTabsColl[strTabId].defaultVisibility["showQuestionList"] = true;
							}
							oTabsColl[strTabId].defaultVisibility["nextSection"] = true;
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							
							if(objQuestion.displayMode==DisplayMode.dspMenu || objQuestion.displayMode==DisplayMode.dspAutocomplete)
							{
								oTabsColl[strTabId].defaultVisibility["displayImageDes"] = false;
							}
							else
							{
								oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							}
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-08-19: Agregada la configuración para cambiar el tipo de despliegue de las preguntas
							oTabsColl[strTabId].defaultVisibility["displayMode"] = true;
							//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
							//OMMC 2019-03-25: Pregunta AR #4HNJD9
							if (objQuestion.displayMode != DisplayMode.dspGetData && objQuestion.displayMode != DisplayMode.dspMetro && objQuestion.displayMode != DisplayMode.dspAR) {
								oTabsColl[strTabId].defaultVisibility["readOnly"] = true;
								oTabsColl[strTabId].defaultVisibility["isInvisible"] = true;
							}
							oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = true;
							//Las preguntas que son de catálogo auto complete deben tener habilitado sólo el selector de un único atributo en lugar de la lista seleccionable
							if (objQuestion.valuesSourceType == TypeOfFill.tofCatalog) {
								switch (objQuestion.displayMode) {
									case DisplayMode.dspAutocomplete:
										oTabsColl[strTabId].defaultVisibility["dataSourceMemberID"] = true;
										break;
									//JAPR 2015-08-11: Corregido un bug, las preguntas tipo Mapa no tienen despliegue tipo Mapa (lo cual no existía originalmente) sino vertical, para
									//determinar si usa o no map es una propiedad diferente
									case DisplayMode.dspVertical:
										if (objQuestion.useMap) {
											//JAPR 2015-08-11: Agregada la propiedad para el radio del mapa
											oTabsColl[strTabId].defaultVisibility["mapRadio"] = true;
											//JAPR 2015-09-11: Corregido un bug, en la pregunta tipo mapa como se ocultará el valuesSourceType, no se auto-encadena la visibilidad
											//de esa propiedad, así que se debe habilitar directamente la propiedad dataSourceID (#CS3FSS)
											oTabsColl[strTabId].defaultVisibility["dataSourceID"] = true;
											//JAPR
											oTabsColl[strTabId].defaultVisibility["dataSourceMemberID"] = true;
											oTabsColl[strTabId].defaultVisibility["dataMemberLatitudeID"] = true;
											oTabsColl[strTabId].defaultVisibility["dataMemberLongitudeID"] = true;
											//Se deshabilita la configuración del tipo de fuente ya que esta pregunta depende siempre de un catálogo
											oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = false;
											//JAPR 2015-08-20: La pregunta con despliegue tipo mapa no debe permitir cambiar el tipo de despliegue
											oTabsColl[strTabId].defaultVisibility["displayMode"] = false;
										}
										else {
											//EVEG se agrego estas opciones para el resize de imagenes
											oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
											oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
											//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
											oTabsColl[strTabId].defaultVisibility["dataMemberImageID"] = true;
											//JAPR
										}
										break;
									case DisplayMode.dspHorizontal:
										oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
										oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
										break;
									//JAPR 2015-08-02: Agregado el tipo de pregunta GetData
									case DisplayMode.dspGetData:
										oTabsColl[strTabId].defaultVisibility["dataSourceMemberID"] = true;
										//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
										//oTabsColl[strTabId].defaultVisibility["dataSourceFilter"] = true;
										oTabsColl[strTabId].defaultVisibility["displayMode"] = false;
										//JAPR 2016-05-12: Modificado el esquema de visibilidad de preguntas a partir de una condición (#NMW148)
										oTabsColl[strTabId].defaultVisibility["showCondition"] = false;
										//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
										oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
										break;
									//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
									case DisplayMode.dspMetro:
										//Las preguntas tipo Metro no pueden cambiar de modo de despliegue ni si usan o no catálogo, así que se bloquearán esas configuraciones, asimismo
										//puede permitir imagen, por lo que se habilitará su configuración
										oTabsColl[strTabId].defaultVisibility["dataSourceID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataMemberImageID"] = true;
										oTabsColl[strTabId].defaultVisibility["catMembersList"] = true;
										oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = false;
										oTabsColl[strTabId].defaultVisibility["displayMode"] = false;
										oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
										oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
										//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
										oTabsColl[strTabId].defaultVisibility["dataMemberOrderID"] = true;
										//JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
										oTabsColl[strTabId].defaultVisibility["dataMemberOrderDir"] = true;
										oTabsColl[strTabId].defaultVisibility["customLayout"] = true;
										break;
									//OMMC 2019-03-25: Pregunta AR #4HNJD9
									case DisplayMode.dspAR:
										oTabsColl[strTabId].defaultVisibility["dataSourceID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataSourceMemberID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataMemberTitleID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataMemberZipFileID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataMemberVersionID"] = true;
										oTabsColl[strTabId].defaultVisibility["dataMemberPlatformID"] = true;
										oTabsColl[strTabId].defaultVisibility["catMembersList"] = false;
										oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = false;
										oTabsColl[strTabId].defaultVisibility["displayMode"] = false;
										oTabsColl[strTabId].defaultVisibility["showCondition"] = false;
										break;
								}
								//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
								oTabsColl[strTabId].defaultVisibility["dataSourceFilter"] = true;
								//JAPR
							}
							else {
								oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
								oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							}
							//RV Ene2017: Estos campos no deben mostrarse en SCH Menú
							if(objQuestion.displayMode==DisplayMode.dspMenu)
							{	
								oTabsColl[strTabId].defaultVisibility["canvasWidth"] = false;
								oTabsColl[strTabId].defaultVisibility["canvasHeight"] = false;
							}

							//OMMC 2016-04-21: Agregado para las preguntas Menú 
							if (objQuestion.noFlow) {
								oTabsColl[strTabId].defaultVisibility["readOnly"] = false;
								oTabsColl[strTabId].defaultVisibility["isInvisible"] = false;
								oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = false;
							}
						}
						
						//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
						var strTabId = "skipRulesTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = (objQuestion.valuesSourceType == TypeOfFill.tofCatalog);
						}
						
						var strTabId = "skipRulesDetailTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = false;
							oTabsColl[strTabId].defaultVisibility["name"] = true;
							oTabsColl[strTabId].defaultVisibility["skipCondition"] = true;
							oTabsColl[strTabId].defaultVisibility["nextSection"] = true;
						}
						//JAPR
						break;
					case Type.multiplechoice:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = true;
							//OMMC 2016-03-29: Agregada la propiedad para la galería
							if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = true;
							}
							//JAPR 2015-10-28: Se oculta esta propiedad porque la manera en que está implementada es a nivel de opción de respuesta, así que no aplica por ahora
							//a nivel de la pregunta
							//if (objQuestion.displayMode != DisplayMode.dspAutocomplete) {
							//	oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
							//}
						}
						
						var strTabId = "valuesTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = (objQuestion.valuesSourceType == TypeOfFill.tofFixedAnswers);
						}
						//OMMC 2015-12-11: Corregido para que las preguntas MChoice no presenten la opción de salto de sección en sus valores
						//A pesar que no se guardaba la configuración se optó por ocultarla.
						var strTabId = "valuesDetailTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = false;
							oTabsColl[strTabId].defaultVisibility["name"] = true;
							oTabsColl[strTabId].defaultVisibility["score"] = true;
							oTabsColl[strTabId].defaultVisibility["showQuestionList"] = true;
							oTabsColl[strTabId].defaultVisibility["nextSection"] = false;
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							
							if(objQuestion.displayMode==DisplayMode.dspMenu || objQuestion.displayMode==DisplayMode.dspAutocomplete)
							{
								oTabsColl[strTabId].defaultVisibility["displayImageDes"] = false;
							}
							else
							{
								oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							}
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-08-19: Agregada la configuración para cambiar el tipo de despliegue de las preguntas
							oTabsColl[strTabId].defaultVisibility["displayMode"] = true;
							oTabsColl[strTabId].defaultVisibility["readOnly"] = true;
							oTabsColl[strTabId].defaultVisibility["isInvisible"] = true;
							oTabsColl[strTabId].defaultVisibility["valuesSourceType"] = true;
							oTabsColl[strTabId].defaultVisibility["maxChecked"] = true;
							switch (objQuestion.displayMode) {
								case DisplayMode.dspAutocomplete:
								case DisplayMode.dspMenu:
									break;
								default:
									oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
									oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
									//JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas numéricas
									if (objQuestion.mcInputType == <?=mpcNumeric?>) {
										oTabsColl[strTabId].defaultVisibility["componentStyle"] = true;
									}
									
									//JAPR 2016-06-02: Validado que esta configuración no aplique para preguntas con despliegue tipo ranking (#POLNQA)
									if (objQuestion.displayMode == DisplayMode.dspLabelnum) {
										oTabsColl[strTabId].defaultVisibility["displayMode"] = false;
									}
									else {
										oTabsColl[strTabId].defaultVisibility["mcInputType"] = true;
									}
									//JAPR
									break;
							}
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
							//JAPR
						}
						break;
					case Type.photo:
					case Type.audio:
					case Type.video:
					case Type.document:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
						}
						
						//JAPR 2016-01-11: Agregado el defaultValue para preguntas tipo Photo
						if (objQuestion.type == Type.photo) {
							oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
						}
						
						//OMMC 2016-03-29: Agregada la propiedad para la galería
						//Para fotos y videos
						if (objQuestion.type == Type.photo || objQuestion.type == Type.video) {
							if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = true;
							}
						}
						
						//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
						//OMMC 2017-01-13: Agregada para los tamaños de pregunta imagen / foto / sketch
						if (objQuestion.type == Type.photo && objQuestion.displayMode == DisplayMode.dspImage) {
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = false;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = false;
							oTabsColl[strTabId].defaultVisibility["allowGallery"] = false;
							oTabsColl[strTabId].defaultVisibility["imageWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["imageHeight"] = true;
						}
						
						//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							if (objQuestion.type != Type.photo) {
								oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
							}
						}
						//JAPR
						
						break;
					case Type.ocr:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							oTabsColl[strTabId].defaultVisibility["savePhoto"] = true;
							//OMMC 2016-03-29: Agregada la propiedad para la galería
							if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = true;
							}
							oTabsColl[strTabId].defaultVisibility["questionList"] = true;
						}
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
							//JAPR
						}
						break;
					case Type.action:
						break;
					case Type.signature:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
						}
						break;
					case Type.message:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							//JAPR 2015-09-01: Por instrucción de DAbdo, se oculta el nombre de pregunta para este tipo, sólo se configurará el HTML/Texto con Formato, y shortname
							//oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							//JAPR
							if (objQuestion.editorType == <?=edtpHTML?>) {
								oTabsColl[strTabId].defaultVisibility["htmlCode"] = true;
							}
							else {
								oTabsColl[strTabId].defaultVisibility["formattedTextDes"] = true;
							}
						}
						
						if (objQuestion.editorType == <?=edtpHTML?>) {
							var strTabId = "advTab";
							if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
								oTabsColl[strTabId].defaultVisibility["autoRedraw"] = true;
							}
						}
						
						//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						//JAPR
						
						break;
					case Type.skipsection:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["skipSection"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
							//Se usarán propiedades alternativas tal como están grabadas en la metadata
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
							//oTabsColl[strTabId].defaultVisibility["displayMenuImageDes"] = true;
							//EVEG se agrego estas opciones para el resize de imagenes
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						break;
					case Type.calc:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["formula"] = true;
							oTabsColl[strTabId].defaultVisibility["format"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["isInvisible"] = true;
							
							//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
							if (objSection && objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline) {
								oTabsColl[strTabId].defaultVisibility["showTotals"] = true;
								oTabsColl[strTabId].defaultVisibility["totalsFunctionDes"] = true;
							}
							//JAPR
						}
						break;
					case Type.sync:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
							//Se usarán propiedades alternativas tal como están grabadas en la metadata
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
							//oTabsColl[strTabId].defaultVisibility["displayMenuImageDes"] = true;
							//EVEG se agrego estas opciones para el resize de imagenes
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						break;
					case Type.callList:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["telephoneNum"] = true;
							//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
							if (vObject.serviceVersion >= vObject.updateVersion.esvPhoneCall) {
								oTabsColl[strTabId].defaultVisibility["disablePhoneNum"] = true;
								oTabsColl[strTabId].defaultVisibility["disableSync"] = true;
							}
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
							//Se usarán propiedades alternativas tal como están grabadas en la metadata
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
							//oTabsColl[strTabId].defaultVisibility["displayMenuImageDes"] = true;
						}
						break;
					case Type.barCode:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							//JAPR 2016-06-15: Modificado el comportamiento del tamaño del texto de preguntas BarCode para que sea como las alfanuméricas (#TEI1OO)
							oTabsColl[strTabId].defaultVisibility["qLength"] = true;
						}
						break;
					case Type.gps:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["registerGPSAt"] = true;
						}
						
						//JAPR 2016-05-12: Modificado el esquema de visibilidad de preguntas a partir de una condición (#NMW148)
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["showCondition"] = false;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						//JAPR
						break;
					case Type.password:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
						}
						
						//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						//JAPR
						break;
					case Type.mapped:
						break;
					//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case Type.sketchPlus:
						//JAPR 2019-04-15: Ajustes de funcionalidad para Sketch+ (#ZDK5CO)
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
						}
						//JAPR
						
						var strTabId = "itemsTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = true;
						}
						
						var strTabId = "itemsDetailTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["self"] = false;
							oTabsColl[strTabId].defaultVisibility["name"] = true;
							oTabsColl[strTabId].defaultVisibility["nextSection"] = true;
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
						}
						//No se agrega un break ya que el resto de las propiedades son comunes entre el viejo Sketch y el nuevo Sketch
						//JAPR
						
					case Type.sketch:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							//JAPR 2019-04-15: Ajustes de funcionalidad para Sketch+ (#ZDK5CO)
							//oTabsColl[strTabId].defaultVisibility = {};
							//JAPR
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							//JAPR 2019-02-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
							if ( objQuestion.type == Type.sketchPlus ) {
								oTabsColl[strTabId].defaultVisibility["sketchImageWidth"] = true;
								oTabsColl[strTabId].defaultVisibility["sketchImageHeight"] = true;
							}
							//JAPR
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
							//Se usarán propiedades alternativas tal como están grabadas en la metadata
							oTabsColl[strTabId].defaultVisibility["imageSketchDes"] = true;
						}
						break;
					
					case Type.section:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["sourceSectionID"] = true;
						}
						
						//JAPR 2016-11-16: Habilitada la configuración de Invisible para preguntas tipo sección tabla que se utilizan sólo para obtener un conjunto de valores de catálogo
						//o algún tipo de cálculo masivo para registros de la sección
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["isInvisible"] = true;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						break;
					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case Type.exit:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
						}
						break;
						
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					case Type.updateDest:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["displayImageDes"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
							oTabsColl[strTabId].defaultVisibility["dataDestinationID"] = true;
							oTabsColl[strTabId].defaultVisibility["nextSectionIfTrue"] = true;
							oTabsColl[strTabId].defaultVisibility["nextSectionIfFalse"] = true;
							//JAPR 2019-03-04: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
							if(vObject.serviceVersion >= vObject.updateVersion.esvUpdateDataWithFiles){
								oTabsColl[strTabId].defaultVisibility["uploadMediaFiles"] = true;
							}
							//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
							oTabsColl[strTabId].defaultVisibility["recalculateDependencies"] = false;
							//JAPR
						}
						break;
					//JAPR
					//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
					case Type.myLocation:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
						}
						
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["canvasWidth"] = true;
							oTabsColl[strTabId].defaultVisibility["canvasHeight"] = true;
						}
						break;
					case Type.number:
					case Type.text:
					case Type.alfanum:
					case Type.date:
					case Type.time:
					default:
						var strTabId = "generalTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano
							oTabsColl[strTabId].defaultVisibility["nameHTMLDes"] = true;
							oTabsColl[strTabId].defaultVisibility["shortName"] = true;
							oTabsColl[strTabId].defaultVisibility["isReqQuestion"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqComment"] = true;
							oTabsColl[strTabId].defaultVisibility["hasReqPhoto"] = true;
							//OMMC 2016-03-29: Agregada la propiedad para la galería
							if(vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
								oTabsColl[strTabId].defaultVisibility["allowGallery"] = true;
							}
							oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
						}
						/*
						var strTabId = "defaultTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility = {};
							oTabsColl[strTabId].defaultVisibility["defaultValue"] = true;
						}
						*/
						var strTabId = "advTab";
						if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
							oTabsColl[strTabId].defaultVisibility["readOnly"] = true;
							oTabsColl[strTabId].defaultVisibility["isInvisible"] = true;
						}
						
						switch (objQuestion.type) {
							case Type.alfanum:
								var strTabId = "generalTab";
								if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
									oTabsColl[strTabId].defaultVisibility["qLength"] = true;
								}
								break;
							case Type.date:
								var strTabId = "generalTab";
								if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
									oTabsColl[strTabId].defaultVisibility["dateFormat"] = true;
								}
								break;
							case Type.number:
								var strTabId = "generalTab";
								if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
									oTabsColl[strTabId].defaultVisibility["format"] = true;
								}
								
								//JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas numéricas
								var strTabId = "advTab";
								if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
									oTabsColl[strTabId].defaultVisibility["componentStyle"] = true;
									
									//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
									if (objSection && objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline) {
										oTabsColl[strTabId].defaultVisibility["showTotals"] = true;
										oTabsColl[strTabId].defaultVisibility["totalsFunctionDes"] = true;
									}
									//JAPR
								}
								//JAPR
								
								var strTabId = "rulesTab";
								if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
									oTabsColl[strTabId].defaultVisibility = {};
									oTabsColl[strTabId].defaultVisibility["minValue"] = true;
									oTabsColl[strTabId].defaultVisibility["maxValue"] = true;
								}
								break;
						}
						break;
				}
				
				//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
				//Todas las preguntas pueden tener un Width siempre y cuando la sección a la que pertenezcan sea Maestro-detalle o Inline, aunque esto sólo aplica para preguntas
				//que tendrán visibilidad, ya que de lo contrario no es necesario configurarlo
				//OMMC 2016-04-14: Agregado el tipo de pregunta mi localización.
				//JAPR 2016-06-27: Removidas las secciones Maestro-detalle y Basadas en dato, ya que actualmente no se puede aplicar esta configuración en ellas (#X7HCN1)
				//if (objSection && (objSection.type == SectionType.Multiple || objSection.type == SectionType.Dynamic || (objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline))) {
				if (objSection && objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline) {
					var blnValid = true;
					switch (objQuestion.type) {
						case Type.gps:
						case Type.section:
						case Type.myLocation:
							var blnValid = false;
							break;
						//JAPR 2017-01-18: Validado que la pregunta Imagen no muestre la configuración de Ancho de columna
						case Type.photo:
							if (objQuestion.displayMode == DisplayMode.dspImage) {
								var blnValid = false;
							}
							break;
						//JAPR
					}
					
					var strTabId = "advTab";
					if (blnValid && oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
						oTabsColl[strTabId].defaultVisibility["columnWidth"] = true;
					}
				}
				
				//JAPR 2016-06-08: Validadas configuraciones que no aplican para secciones tabla (#ALOXYM)
				if (objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline) {
					var strTabId = "generalTab";
					if (oTabsColl[strTabId] && oTabsColl[strTabId].defaultVisibility) {
						if (oTabsColl[strTabId].defaultVisibility["hasReqComment"]) {
							delete oTabsColl[strTabId].defaultVisibility["hasReqComment"];
						}
						
						if (oTabsColl[strTabId].defaultVisibility["hasReqPhoto"]) {
							delete oTabsColl[strTabId].defaultVisibility["hasReqPhoto"];
						}
						//OMMC 2017-10-20: Corregido para que no elimine la opción de galería para foto, video y OCR en tablas.
						if (oTabsColl[strTabId].defaultVisibility["allowGallery"] && (objQuestion.type != Type.video && objQuestion.type != Type.photo && objQuestion.type != Type.ocr)) {
							delete oTabsColl[strTabId].defaultVisibility["allowGallery"];
						}
					}
				}
				//JAPR
				
				return oTabsColl;
			}
			
			/* Genera el array de los campos capturables de la definición de una pregunta */
			function generateQuestionProps(sNodeId) {
				console.log('generateQuestionProps ' + sNodeId);
				
				//El id de la pregunta
				var intQuestionID = sNodeId;
				var objObject = getObject(AdminObjectType.otyQuestion, intQuestionID);
				if (!objObject) {
					return;
				}
				
				/*29Sep2015: Agregar tipo de pregunta y tipo de despliegue en el header de la pregunta*/
				objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?> - " + objObject.getName() + ' - ' + getQuestionTypeDescByTypeExt(objObject.typeExt, objObject.displayMode));
				//Define el subgrid con el detalle de las opciones de respuesta
				/*
				var objOptionDetail = new TabCls($.extend(true, {}, {id:"detail", name:"detail",
						property:"options",		//Nombre de la propiedad del objeto padre que representa la coleccion de valores a la que pertenece este detalle, para permitir obtener los valores del subgrid
						fields:new Array(
							new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"phoneNum", label:"<?=translate("Phone number")?>"})),
							new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"score", label:"<?=translate("Score")?>"}))
						)
					}
				));
				*/
				
				//Los fields de las collection deben tener una referencia a que número de columna representan, así como información adicional
				intColumnNum = 1;
				objTabsColl = {
					"generalTab": new TabCls($.extend(true, {}, {id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true})),
					//"defaultTab": new TabCls($.extend(true, {}, {id:"defaultTab", name:"<?=translate("Default")?>"})),
					"advTab": new TabCls($.extend(true, {}, {id:"advTab", name:"<?=translate("Advanced")?>"})),
					"valuesTab": new TabCls($.extend(true, {}, {id:"valuesTab", name:"<?=translate("Values")?>",
						type:propTabType.collection, 										//Este elemento es una colección de otros objetos, no un grid de propiedades
						childType:AdminObjectType.otyOption,								//Tipo de objeto de cada elemento de la colección
						values:((objObject)?objObject.optionsByID:undefined),				//Lista de los objetos a pintar (deben tener por lo menos propiedades id y name)
						valuesOrder:((objObject)?objObject.optionsOrderByID:undefined)		//Array del orden en que se pintan los objetos de values
					})),
					"rulesTab": new TabCls($.extend(true, {}, {id:"rulesTab", name:"<?=translate("Rules")?>"})),
					"valuesDetailTab": new TabCls($.extend(true, {}, {id:"valuesDetailTab", name:"detail",
						type:propTabType.detail, 	//Este elemento es una definición de detalle, como tal no se muestra como tab realmente
						property:"options"			//Nombre de la propiedad del objeto padre que representa la coleccion de valores a la que pertenece este detalle, para permitir obtener los valores del subgrid
					})),
					//JAPR 2016-04-15: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					"skipRulesTab": new TabCls($.extend(true, {}, {id:"skipRulesTab", name:"<?=translate("Skip rules")?>",
						type:propTabType.collection, 										//Este elemento es una colección de otros objetos, no un grid de propiedades
						childType:AdminObjectType.otySkipRule,								//Tipo de objeto de cada elemento de la colección
						values:((objObject)?objObject.skipRules:undefined),					//Lista de los objetos a pintar (deben tener por lo menos propiedades id y name)
						valuesOrder:((objObject)?objObject.skipRulesOrder:undefined)		//Array del orden en que se pintan los objetos de values
					})),
					"skipRulesDetailTab": new TabCls($.extend(true, {}, {id:"skipRulesDetailTab", name:"detail",
						type:propTabType.detail, 	//Este elemento es una definición de detalle, como tal no se muestra como tab realmente
						property:"skipRules"		//Nombre de la propiedad del objeto padre que representa la coleccion de valores a la que pertenece este detalle, para permitir obtener los valores del subgrid
					})),
					//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					"itemsTab": new TabCls($.extend(true, {}, {id:"itemsTab", name:"<?=translate("Drawable items")?>",
						type:propTabType.collection, 										//Este elemento es una colección de otros objetos, no un grid de propiedades
						childType:AdminObjectType.otyOption,								//Tipo de objeto de cada elemento de la colección
						values:((objObject)?objObject.optionsByID:undefined),				//Lista de los objetos a pintar (deben tener por lo menos propiedades id y name)
						valuesOrder:((objObject)?objObject.optionsOrderByID:undefined)		//Array del orden en que se pintan los objetos de values
					})),
					"itemsDetailTab": new TabCls($.extend(true, {}, {id:"itemsDetailTab", name:"detail",
						type:propTabType.detail, 	//Este elemento es una definición de detalle, como tal no se muestra como tab realmente
						property:"options"			//Nombre de la propiedad del objeto padre que representa la coleccion de valores a la que pertenece este detalle, para permitir obtener los valores del subgrid
					}))
				};
				
				//JAPR 2016-04-22: Validado que si no se cumple con la versión correcta no agregue las cejillas de reglas (#OZGBL9)
				if (vObject.serviceVersion < vObject.updateVersion.esvSimpleChoiceCatOpts) {
					delete objTabsColl["skipRulesTab"];
					delete objTabsColl["skipRulesDetailTab"];
				}
				//JAPR
				
				//En las preguntas tipo Sección se debe pasar como parámetro la sección actualmente seleccionada, de lo contrario también será excluída entre las secciones válidas por
				//ya encontrarse usada en alguna pregunta tipo Sección
				var objValidSourceSectionsColl = new Object();
				if (objObject.type == Type.section && objObject.sourceSectionID > 0) {
					objValidSourceSectionsColl[objObject.sourceSectionID] = 1;
				}
				
				/****************************************************************************
					Inicia el apartado de generalTab
				*****************************************************************************/
				var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"generalTab"};
				objTabsColl["generalTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, {name:"nameHTMLDes", label:"<?=translate("Message")?>", type:GFieldTypes.editor,
						customButtons:{
							"backColor":{label:"<?=translate("BackColor")?>",type:"buttonColor",img:"higlighttext.gif"},
							"foreColor":{label:"<?=translate("ForeColor")?>",type:"buttonColor",img:"fontColor.gif"},
							"fontName":{label:"<?=translate("FontName")?>",type:"combo",img:"fontName.gif"},
							"fontSize":{label:"<?=translate("FontSize")?>",type:"combo",img:"fontSize.gif"},
							"uploadImg":{label:"<?=translate("Image")?>",type:"uploadImg",img:"insertImg.png"}
						}
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"typeExt", label:"<?=translate("Type")?>", type:GFieldTypes.number, hidden:true})),
					//JAPR 2016-12-15: Agregada la validación de ciertos campos según la propiedad validate de su definición (#IRERJD)
					//JAPR 2016-12-15: Agregado el maxLength a esta propiedad (#IRERJD)
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"shortName", label:"<?=translate("Short Name")?>", maxLength:255 /*validate:"NotEmpty,ValidInteger"*/})),
					new FieldCls($.extend(true, {}, objCommon, {name:"skipSection", label:"<?=translate("Section")?>", type:GFieldTypes.combo, 
						options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
						optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"registerGPSAt", label:"<?=translate("REGISTERGPSDATAAT")?>", type:GFieldTypes.combo, options:optionsRegisterGPSAtOpts, default:<?=rgpsAtEnd?>})),
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"telephoneNum", label:"<?=translate("Phone number")?>"})),
					<?//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)?>
					new FieldCls($.extend(true, {}, objCommon, {name:"defaultValue", label:"<?=translate("Default value")?>", type:GFieldTypes.formula, onChange: function() {
						/* Muestra u oculta la configuración de Auto selección en base a si existe o no un defaultValue */
						if ( this.getValue ) {
							var objField = getFieldDefinition(this.tab, "autoSelectEmptyOpt", false);
							if ( objField ) {
								var objGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
								if ( objGrid ) {
									var strDefaultValue = this.getValue(true);
									objGrid.setRowHidden("autoSelectEmptyOpt", (strDefaultValue !== undefined && String(strDefaultValue).length?true:false));
								}
							}
						}
					}})),
					<?//@JAPR 2019-09-10: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
					if (getMDVersion() >= esvAutoSelectSChOpt) {
					?>
					new FieldCls($.extend(true, {}, objCommon, objCheckAutoSelection, {name:"autoSelectEmptyOpt", label:"<?=translate("Automatic selection")?>"})),
					<?}?>
					new FieldCls($.extend(true, {}, objCommon, {name:"formula", label:"<?=translate("Formula")?>", type:GFieldTypes.formula})),
					new FieldCls($.extend(true, {}, objCommon, {name:"format", label:"<?=translate("Format")?>", type:GFieldTypes.comboEdit, options:opQuestionNumericFormat})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dateFormat", label:"<?=translate("Format")?>", type:GFieldTypes.comboEdit, options:opQuestionDateFormat})),
					//OMMC 2016-01-20: Unificado el editor HTML para las preguntas tipo Mensaje
					new FieldCls($.extend(true, {}, objCommon, {name:"formattedTextDes", label:"<?=translate("Message")?>", type:GFieldTypes.editorHTMLDialog, size:5000, required:true, default:objObject.name})),
					//JAPR 2015-09-01: Por instrucción de DAbdo, se oculta el nombre de pregunta para este tipo, sólo se configurará el HTML/Texto con Formato, y shortname
					/*new FieldCls($.extend(true, {}, objCommon, {name:"formattedTextDes", label:"<?=translate("Message")?>", type:GFieldTypes.editor, required:true, default:objObject.name, height:300,
						customButtons:{
							"backColor":{label:"<?=translate("BackColor")?>",type:"buttonColor",img:"higlighttext.gif"},
							"foreColor":{label:"<?=translate("ForeColor")?>",type:"buttonColor",img:"fontColor.gif"},
							"fontName":{label:"<?=translate("FontName")?>",type:"combo",img:"fontName.gif"},
							"fontSize":{label:"<?=translate("FontSize")?>",type:"combo",img:"fontSize.gif"},
							"uploadImg":{label:"<?=translate("Image")?>",type:"uploadImg",img:"insertImg.png"}
						}
					})),*/
					new FieldCls($.extend(true, {}, objCommon, {name:"htmlCode", label:"<?=translate("HTML Code")?>", type:GFieldTypes.editorHTMLDialog, size:5000, required:true, default:objObject.name})),
					new FieldCls($.extend(true, {}, objCommon, {name:"sourceSectionID", label:"<?=translate("Source section")?>", type:GFieldTypes.combo, empty:true, default:0,
						options:((selSurvey.getUnusedInlineSectionItems)?selSurvey.getUnusedInlineSectionItems(objValidSourceSectionsColl):{}),
						optionsOrder:((selSurvey.getUnusedInlineSectionItemsOrder)?selSurvey.getUnusedInlineSectionItemsOrder():{})
					})),	//Obtain response items from the following Inline section
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"qLength", label:"<?=translate("Length")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"isReqQuestion", label:"<?=translate("Required")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"disablePhoneNum", label:"<?=translate("Hide phone number")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"disableSync", label:"<?=translate("Sync on call")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheckPlusOpt, {name:"hasReqComment", label:"<?=translate("Comments")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheckPlusOpt, {name:"hasReqPhoto", label:"<?=translate("Photo")?>"})),
					//OMMC 2016-03-29: Agregado el acceso a la galería
					new FieldCls($.extend(true, {}, objCommon, objCheckGallery, {name:"allowGallery", label:"<?=translate("Allow files from: ")?>"})),
					//OMMC 2015-11-27: Agregados los campos para preguntas OCR
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"savePhoto", label:"<?=translate("Save photo")?>"})),
					//JAPR 2016-06-07: Corregido un bug, se había dejado un campo que incrementaba intColumnNum, por lo que la tab de Values quedó desfasada y se comportaba extraño (#4SLSWI)
					new FieldCls($.extend(true, {}, objCommon, {name:"questionList", label:"<?=translate("Questions list")?>", width:150, type:GFieldTypes.multiList, readOnly:true,
						options:((selSurvey.getAllOpenQuestionItems)?(function() {return selSurvey.getAllOpenQuestionItems();}):{}),
						optionsOrder:((selSurvey.getAllOpenQuestionItemsOrder)?(function() {return selSurvey.getAllOpenQuestionItemsOrder();}):{})
					})),
					//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"imageWidth", label:"<?=translate("Width")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"imageHeight", label:"<?=translate("Height")?>"}))
				);
				
				//OMMC 2016-03-30: Agregado el recorrido de campos default para agregar comportamiento específico a una versión
				$.grep(objTabsColl["generalTab"].fields, function(field, i){
					//OMMC 2016-03-30: Agregado el tipo de opciones especiales para cuando se permite galería
					if(field.name == "hasReqPhoto" && vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
						field.options = objCheckPlusOptPhoto.options;
					}
					//OMMC 2016-03-30: Agregado acceso a la galería para las preguntas OCR
					if(objObject.type == Type.ocr && field.name == "allowGallery" && vObject.serviceVersion >= vObject.updateVersion.esvAllowGallery){
						field.show();
					}
					//OMMC 2016-09-01: Modificado el comportamiento default de la pregunta llamada
					if (objObject.type == Type.callList && (field.name == 'disablePhoneNum' || field.name == 'disableSync') && vObject.serviceVersion >= vObject.updateVersion.esvPhoneCall) {
						field.show();
					}
					//OMMC 2017-01-11: Agregado el tipo de despliegue Imagen para preguntas photo
					if((field.name == "imageWidth" || field.name == "imageHeight") && vObject.serviceVersion >= vObject.updateVersion.esvImageDisplay){
						field.show();
					}
					
				});
				
				/****************************************************************************
					Inicia el apartado de advTab
				*****************************************************************************/
				//OMMC 2016-04-22: Esta porción de código se debería mover al final en un intento para sobreescribir comportamientos específicos de ciertos campos
				//Al final del código de la tab se encuentra un $.grep que recorre todos los campos con el fin de personalizar la configuración
				var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"advTab"};
				var objValuesSourceType = opQuestionTypeOfFilling;
				switch (objObject.displayMode) {
					case DisplayMode.dspMenu:
						objValuesSourceType = opQuestionTypeOfFillingMenu;
						break;
				}
				if (objObject.useMap) {
					objValuesSourceType = opQuestionTypeOfFillingMap;
				}
				
				//JAPR 2015-08-19: Agregada la configuración para cambiar el tipo de despliegue de las preguntas
				var objQuestionDisplayMode = opQuestionDisplayMode;
				if (objObject.valuesSourceType == TypeOfFill.tofCatalog) {
					objQuestionDisplayMode = opSCHCatQuestionDisplayMode;
				}
				
				if (objObject.type == Type.multiplechoice) {
					objValuesSourceType = opMultiTypeOfFilling;
					//RV Ene2017: Para multiple choice se maneja el arreglo opMCHQuestionDisplayMode el cual incluye la configuración de visibilidad
					//para los campos Tipo de Captura en opción, Ancho y Alto
					objQuestionDisplayMode = opMCHQuestionDisplayMode;
					
					//JAPR 2016-04-22: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
					//No aplica tipo Menú para múltiple choice de catálogo
					if (vObject.serviceVersion >= vObject.updateVersion.esvMChoiceCat && objObject.valuesSourceType == TypeOfFill.tofCatalog) {
						objQuestionDisplayMode = opMCHCatQuestionDisplayMode;
					}
					
					//OMMC 2015-11-25: Cambiar las opciones de despliegue de las MChoice en secciones tabla a vertical y horizontal.
					var objSection = getObject(AdminObjectType.otySection, objObject.sectionID);
					if(objSection.type == SectionType.Inline && objSection.displayMode == SectionDisplayMode.sdspInline){
						objQuestionDisplayMode = opQuestionDisplayModeTable;
					}
				}
				//OMMC
				
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				objTabsColl["advTab"].fields =  new Array(
					//JAPR 2016-07-28: Corregido un bug, esta configuración estaba como numérica pero debe permitir texto (#XYTODV)
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"columnWidth", label:"<?=translate("Column width")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"readOnly", label:"<?=translate("Read only")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"isInvisible", label:"<?=translate("Invisible")?>"})),
					//JAPR 2016-01-22: Agregada la configuración para mostrar los totales de las secciones Inline (#C2FTQ8)
					<?if (getMDVersion() >= esvShowTotalsInline) {?>
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"showTotals", label:"<?=translate("Show totals")?>", default:0})),
					new FieldCls($.extend(true, {}, objCommon, {name:"totalsFunctionDes", label:"<?=translate("Totals function")?>", type:GFieldTypes.comboEdit, options:opAgFunctions})),
					<?}?>
					//JAPR
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"autoRedraw", label:"<?=translate("Auto redraw")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"mcInputType", label:"<?=translate("Add input to options")?>", type:GFieldTypes.combo, options:opQuestionMCInputType,
						setValuesFn:function() {
							//Al cambiar esta configuración se debe actualizar el preview con el nuevo tipo de captura aplicado
							//Obtiene la referencia a la instancia de pregunta en memoria
							var intObjectType = this.parentType;
							var intObjectID = this.id;
							var objObject = getObject(intObjectType, intObjectID);
							if (!objObject || !objObject.type) {
								return;
							}
							
							if (!objFormsLayout || !objFormsLayout.cells || !objFormsLayout.cells(dhxPreviewCell)) {
								return;
							}
							
							try {
								//Obtiene la referencia a la pregunta en el preview
								var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
								if (!objiFrame.contentWindow.frmDevice) {
									return;
								}
								if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
									if (!objiFrame.contentWindow.frmDevice.selSurvey) {
										return;
									}
								} else {
									if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
										return;
									}
								}
								
								var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
								if (!objselSurvey || !objselSurvey.questions) {
									return;
								}
								
								var objQuestion = objselSurvey.questions[intObjectID];
								if (!objQuestion) {
									return;
								}
								
								//Actualiza la definición del tipo de captura del objeto en el preview así como el resto de las configuraciones relacionadas, y ejecuta el reDraw
								//para que se vean reflejados los cambios
								objQuestion.mcInputType = parseInt(objObject.mcInputType);
								objQuestion.reDraw();
							} catch(e) {
								console.log("Error updating mcInputType in preview: " + e);
							}
						}
					})),
					//JAPR 2015-08-15: Agregada la configuración para el tipo de teclado en preguntas numéricas
					new FieldCls($.extend(true, {}, objCommon, {name:"componentStyle", label:"<?=translate("Keypad type")?>", type:GFieldTypes.combo, options:opQuestionKeyboardType})),
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"maxChecked", label:"<?=translate("Max number of checked")?>"})),
					//JAPR 2015-08-11: Agregada la propiedad para el radio del mapa
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"mapRadio", label:"<?=translate("Map radio")?>", default:0})),
					//JAPR 2019-03-05: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"uploadMediaFiles", label:"<?=translate("Upload media files")?>"})),
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					new FieldCls($.extend(true, {}, objCommon, {name:"dataDestinationID", label:"<?=translate("Data destination")?>", type:GFieldTypes.combo, 
						options:arrDataDestinations, optionsOrder:arrDataDestinationsOrder, empty:true
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"nextSectionIfTrue", label:"<?=translate("Next section on save")?>", type:GFieldTypes.combo, empty:true, 
						options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
						optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"nextSectionIfFalse", label:"<?=translate("Next section on cancel")?>", type:GFieldTypes.combo, empty:true, 
						options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
						optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
					})),
					//JAPR
					new FieldCls($.extend(true, {}, objCommon, {name:"displayImageDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg})),
					new FieldCls($.extend(true, {}, objCommon, {name:"imageSketchDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg})),
					//JAPR 2016-07-28: Corregido un bug, esta configuración estaba como numérica pero debe permitir texto (#XYTODV)
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"canvasWidth", label:"<?=translate("Width")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"canvasHeight", label:"<?=translate("Height")?>"})),
					//JAPR 2019-02-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"sketchImageWidth", label:"<?=translate("Image Width")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"sketchImageHeight", label:"<?=translate("Image Height")?>"})),
					//JAPR
					new FieldCls($.extend(true, {}, objCommon, {name:"valuesSourceType", label:"<?=translate("Source")?>", type:GFieldTypes.combo, options:objValuesSourceType, 
						setValuesFn:function() {
							//Al cambiar esta configuración se deben cambiar las opciones de despliegue dependiendo de si la fuente de valores es o no un catálogo
							var intObjectType = this.parentType;
							var intObjectID = this.id;
							var objObject = getObject(intObjectType, intObjectID);
							if (!objObject || !objObject.type) {
								return;
							}
							
							var intQDisplayMode = objObject.displayMode;
							switch (objObject.type) {
								case Type.simplechoice:
									var objField = getFieldDefinition(this.tab, "displayMode");
									if (objField) {
										//OMMC 2016-04-21: Cambiado para cuando son preguntas Menú 
										if (objObject.valuesSourceType == TypeOfFill.tofCatalog) {
											if (objObject.noFlow) {
												objField.options = opMenuDisplayModeCatalog;
											} else {
												objField.options = opSCHCatQuestionDisplayMode;
											}
										} else {
											if (objObject.noFlow) {
												objField.options = opMenuDisplayMode;
											} else {
												objField.options = opQuestionDisplayMode;
											}
										}
										
										var blnDisplayChanged = false;
										if (!objField.options[intQDisplayMode]) {
											//Menú es compartida, así que ese será el default
											blnDisplayChanged = true;
											intQDisplayMode = DisplayMode.dspMenu;
											objField.setValue(undefined, undefined, intQDisplayMode);
											objField.refreshValue(intQDisplayMode);
										}
										
										//Encadena la función para hacer refresh de componentes independientemente de si cambió el tipo de despliegue, ya que por haber cambiado el
										//tipo de llenado es probable que ya no apliquen algunas configuraciones, así que se refresca todo
										objField.applyVisibilityRules();
										if (objField.setValuesFn) {
											objField.setValuesFn(blnDisplayChanged);
										}
									}
									break;
								case Type.multiplechoice:
									var objField = getFieldDefinition(this.tab, "displayMode");
									if (objField) {
										//JAPR 2016-04-22: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
										if (vObject.serviceVersion >= vObject.updateVersion.esvMChoiceCat && objObject.valuesSourceType == TypeOfFill.tofCatalog) {
											objField.options = opMCHCatQuestionDisplayMode;
										}
										else {
											objField.options = opQuestionDisplayMode;
										}
										//JAPR
									}
									break;
							}
							
							//JAPR 2016-12-20: Modificado para limpiar la información de catálogo de los campos asociados cuando se deja de utilizar un data source (#DBNNGH)
							//Independientemente del tipo de pregunta, si se dejó de utilizar un catálogo se debe aplicar limpieza a las propiedades asociadas
							if (objObject.valuesSourceType != TypeOfFill.tofCatalog) {
								var objField = getFieldDefinition(this.tab, "dataSourceID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataSourceMemberID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataMemberLatitudeID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataMemberLongitudeID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataMemberImageID");
								if (objField) {
									objField.setValue(undefined, undefined, 0);
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataMemberOrderID");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue(0);
								}
								var objField = getFieldDefinition(this.tab, "dataSourceFilter");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
								var objListField = getFieldDefinition(this.tab, "catMembersList");
								var objGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
								if (objListField && objGrid) {
									var arrCatMembersList = new Array();
									doUpdateProperty(objGrid, undefined, undefined, "catMembersList", 1, arrCatMembersList, true);
								}
								//OMMC 2019-03-25: Pregunta AR #4HNJD9
								var objField = getFieldDefinition(this.tab, "dataMemberZipFileID");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
								var objField = getFieldDefinition(this.tab, "dataMemberVersionID");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
								var objField = getFieldDefinition(this.tab, "dataMemberPlatformID");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
								var objField = getFieldDefinition(this.tab, "dataMemberTitleID");
								if (objField) {
									objField.setValue(undefined, undefined, '');
									objField.refreshValue('');
								}
							}
							//JAPR
						}, children:["displayMode"]
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"displayMode", label:"<?=translate("Display mode")?>", type:GFieldTypes.combo, options:objQuestionDisplayMode,
						setValuesFn:function(bDisplayChanged) {
							//El parámetro bDisplayChanged indicaría si se cambió o no el tipo de despliegue, sólo aplica cuando se invoca manualmente, ya que en el resto de los
							//casos vendría undefined lo que quiere decir que se cambió desde la propia configuración así que es seguro que si lo hizo
							if (bDisplayChanged === undefined) {
								bDisplayChanged = true;
							}
							
							//Al cambiar esta configuración se debe actualizar el preview con el nuevo tipo de despliegue aplicado
							var intQDisplayMode = this.getValue(true);
							
							//Obtiene la referencia a la instancia de pregunta en memoria
							var intObjectType = this.parentType;
							var intObjectID = this.id;
							var objObject = getObject(intObjectType, intObjectID);
							if (!objObject || !objObject.type) {
								return;
							}
							
							//Actualiza la consistencia de la pregunta cuando se trata de una que usa catálogo
							//JAPR 2016-04-25: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
							//Las preguntas que son de catálogo no tienen que hacer esta validación porque al estar limitadas a un sólo atributo, no tiene la configuración de lista
							//de atributos configurada, así que no se requiere ningún tipo de ajuste
							if (objObject.type != Type.multiplechoice && objObject.valuesSourceType == TypeOfFill.tofCatalog && bDisplayChanged) {
								switch (parseInt(intQDisplayMode)) {
									case DisplayMode.dspMenu:
										//Si se está cambiando de auto-complete a menú, entonces debe respetar la configuración previa de atributos seleccionados, lo cual implica
										//de todas maneras volver a grabar la lista para que el server haga la actualización de campos y catálogo correctas, además de actualizar en
										//la instancia al dataSourceMemberID. Si la lista de atributos hubiera estado vacía, el proceso no cambia, simplemente se limpiará toda referencia
										//a los atributos
										//Pudo haber llegado aquí además del cambio obvio de tipo de despliegue, debido a un cambio en el tipo de llenado de cualquier otro para ahora usar
										//catálogo, siempre y cuando el otro tipo de llenado no hubiera sido ni menú ni auto-complete, ya que en esos caso se hubiera forzado a entrar como
										//menú, si ese fue el caso entonces el proceso aunque innecesario, pasaría igual como si se hubiera cambiado de auto-complete a menú sin que alguna
										//vez hubieran estado seleccionados los atributos, por lo que limpiará las referencias
										var objField = getFieldDefinition(this.tab, "dataSourceMemberID");
										var objListField = getFieldDefinition(this.tab, "catMembersList");
										var objGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
										if (objField && objListField && objGrid) {
											var arrCatMembersList = objObject.catMembersList;
											var intDataSourceMemberID = 0;
											if (arrCatMembersList && $.isArray(arrCatMembersList)) {
												if (arrCatMembersList.length) {
													intDataSourceMemberID = parseInt(arrCatMembersList[arrCatMembersList.length-1]);
													if (!$.isNumeric(intDataSourceMemberID)) {
														intDataSourceMemberID = 0;
													}
												}
											}
											else {
												//En este caso simplemente crea una lista vacía de atributos
												arrCatMembersList = new Array();
											}
											
											//Actualiza en pantalla, en la instancia y en la BD la información de este valor
											objField.setValue(undefined, undefined, intDataSourceMemberID);
											objField.refreshValue(intDataSourceMemberID);
											doUpdateProperty(objGrid, undefined, undefined, "catMembersList", 1, arrCatMembersList, true);
										}
										break;
										
									case DisplayMode.dspAutocomplete:
										//Si se está cambiando de menú a auto-complete, entonces debe tomar el último valor de la lista de atributos
										//No pudo haber llegado a este punto cambiando de no usar catálogo a sí usarlo, ya que auto-complete es una opción válida en ambos así que
										//no habría forzado a esta parte (bDisplayChanged == true), sin embargo si puede ser que en la misma sesión una pregunta que no era de catálogo
										//cambió a tipo menú, luego a si ser de catálogo y de ahí a auto-complete, en cuyo caso catMembersList si estaría asignado pero vacío, el proceso
										//será el mismo sin embargo, sólo no habrá atributo en ese caso
										var objField = getFieldDefinition(this.tab, "dataSourceMemberID");
										var objGrid = objPropsTabs.tabs(this.tab).getAttachedObject();
										if (objField && objGrid) {
											var arrCatMembersList = objObject.catMembersList;
											var intDataSourceMemberID = 0;
											if (arrCatMembersList && $.isArray(arrCatMembersList)) {
												if (arrCatMembersList.length) {
													intDataSourceMemberID = parseInt(arrCatMembersList[arrCatMembersList.length-1]);
													if (!$.isNumeric(intDataSourceMemberID)) {
														intDataSourceMemberID = 0;
													}
												}
											}
											
											//Actualiza en pantalla, en la instancia y en la BD la información de este valor
											objField.setValue(undefined, undefined, intDataSourceMemberID);
											objField.refreshValue(intDataSourceMemberID);
											doUpdateProperty(objGrid, undefined, undefined, "dataSourceMemberID", 1, intDataSourceMemberID, true);
										}
										break;
								}
							}
							
							if (!objFormsLayout || !objFormsLayout.cells || !objFormsLayout.cells(dhxPreviewCell)) {
								return;
							}
							
							try {
								//Obtiene la referencia a la pregunta en el preview
								var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
								if (!objiFrame.contentWindow.frmDevice) {
									return;
								}
								if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
									if (!objiFrame.contentWindow.frmDevice.selSurvey) {
										return;
									}
								} else {
									if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
										return;
									}
								}
								
								var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
								if (!objselSurvey || !objselSurvey.questions) {
									return;
								}
								
								var objQuestion = objselSurvey.questions[intObjectID];
								if (!objQuestion) {
									return;
								}
								
								//Actualiza la definición del tipo de despliegue del objeto en el preview así como el resto de las configuraciones relacionadas, y ejecuta el reDraw
								//para que se vean reflejados los cambios (esto sólo debería funcionar si exclusivamente ha cambiado la configuración de despliegue, ya que actualizar los
								//catálogos requiere mayor esfuerzo)
								objQuestion.displayMode = parseInt(objObject.displayMode);
								//Actualiza otras configuraciones que son reelevantes para esta específicamente
								objQuestion.sourceQuestionID = parseInt(objObject.sourceQuestionID);
								objQuestion.sharedQuestionID = parseInt(objObject.sharedQuestionID);
								objQuestion.excludeFromQuestionID = parseInt(objObject.excludeFromQuestionID);
								objQuestion.reDraw();
							} catch(e) {
								console.log("Error updating displayMode in preview: " + e);
							}
						}
					})),
					new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"allowAdditionalAnswers", label:"<?=translate("Allow entering new answers?")?>"})),
					//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
					//new FieldCls($.extend(true, {}, objCommon, {name:"catalogID", label:"<?=translate("Catalog")?>", type:GFieldTypes.combo, options:arrCatalogs, empty:true,
					//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceID", label:"<?=translate("Catalog")?>", type:GFieldTypes.combo, options:arrCatalogs, optionsOrder:arrCatalogsOrder, empty:true,
						setValuesFn:fnSetChildrenValues, children:["catMembersList", "dataSourceMemberID", "dataMemberLatitudeID", "dataMemberLongitudeID", "dataMemberImageID", "dataMemberOrderID", "dataMemberZipFileID", "dataMemberVersionID", "dataMemberPlatformID", "dataMemberTitleID"]
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceMemberID", label:"<?=translate("Attribute")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberLatitudeID", label:"<?=translate("Attribute").": ".translate("Latitude")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberLongitudeID", label:"<?=translate("Attribute").": ".translate("Longitude")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					//JAPR 2016-03-17: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					//La propiedad filterProp funciona como un filtro, los objetos dentro de options o getOptions que cumplan la condición de tener dicha propiedad (con valor que se resuelva a true)
					//se incluirán para la colección de valores de este campo, mientras que los demás se ignorarán. Esto es útil en casos como este atributo, el cual llena sus options igual
					//que el resto de los atributos, pero con la particularidad que este sólo debe usar atributos que son imagen, así que se usa filterProp:'isImage' para filtrar la colección.
					//Eventualmente se podría usar un array en lugar de un string para filterProp, de tal manera que deba cumplir contener todas esas propiedades para ser válido
					<?if (getMDVersion() >= esvSimpleChoiceCatOpts) {?>
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberImageID", label:"<?=translate("Attribute").": ".translate("Image")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true, filterProp:'isImage'})),
					<?}?>
					//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
					//JAPR 2016-05-09: Modificada la pregunta simple choice Metro para configurar la dirección del ordenamiento y un HTML personalizado (#XRKG3R)
					<?if (getMDVersion() >= esvSChoiceMetro) {?>
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberOrderID", label:"<?=translate("Attribute").": ".translate("Order")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberOrderDir", label:"<?=translate("Order")?>", type:GFieldTypes.combo, options:opQuestionSorting, default:<?=ordbAsc?>})),
					new FieldCls($.extend(true, {}, objCommon, {name:"customLayout", label:"<?=translate("Form custom layout html")?>", empty:true, type:GFieldTypes.editorHTMLDialog, size:5000})),
					<?}?>
					//JAPR
					new FieldCls($.extend(true, {}, objCommon, {name:"dataSourceFilter", label:"<?=translate("Filter")?>", type:GFieldTypes.formula})),
					//new FieldCls($.extend(true, {}, objCommon, {name:"catMemberID", label:"<?=translate("Attribute")?>", type:GFieldTypes.combo, parent:"catalogID", empty:true})),
					//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
					//new FieldCls($.extend(true, {}, objCommon, {name:"catMembersList", label:"<?=translate("Attribute")?>", type:GFieldTypes.selList, parent:"catalogID", empty:true,
					new FieldCls($.extend(true, {}, objCommon, {name:"catMembersList", label:"<?=translate("Attribute")?>", type:GFieldTypes.selList, parent:"dataSourceID", empty:true,
						refreshValue:function() {
							//Este tipo de campo simplemente debe volver a asignar el valor de la celda para que se vuelva a pintar, ya que al ser un tipo personalizado que está
							//permanentemente fijo, no se porgramó dependiendo del valor asignado sino a partir del campo padre, así que cualquier cosa que se mande lo refrescará
							var blnForForm = (this.parentType == AdminObjectType.otySurvey);
							var objTabComponent = undefined;
							try {
								if (blnForForm) {
									var objTabComponent = objFormTabs.tabs(tabDetail).getAttachedObject();
								}
								else {
									var objTabComponent = objPropsTabs;
								}
							} catch(e) {}
							
							if (objTabComponent && objTabComponent.tabs && objTabComponent.tabs(this.tab)) {
								var objGrid = objTabComponent.tabs(this.tab).getAttachedObject();
								if (objGrid) {
									objGrid.cells(this.name, 0).setValue("");
								}
							}
						}
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"sourceQuestionID", label:"<?=translate("Other question")?>", type:GFieldTypes.combo, empty:true, 
						options:((objObject.getSourceQuestionItems)?(function() {return objObject.getSourceQuestionItems();}):{}),
						optionsOrder:((objObject.getSourceQuestionItemsOrder)?(function() {return objObject.getSourceQuestionItemsOrder();}):{})
					})),	//Obtain response items from the following multiple choice question
					new FieldCls($.extend(true, {}, objCommon, {name:"sharedQuestionID", label:"<?=translate("Copy question")?>", type:GFieldTypes.combo, empty:true,
						options:((objObject.getSharedQuestionItems)?(function() {return objObject.getSharedQuestionItems();}):{}),
						optionsOrder:((objObject.getSharedQuestionItemsOrder)?(function() {return objObject.getSharedQuestionItemsOrder();}):{})
					})),	//Share response items with the following question
					new FieldCls($.extend(true, {}, objCommon, {name:"excludeFromQuestionID", label:"<?=translate("Exclude question")?>", type:GFieldTypes.combo, empty:true,
						options:((objObject.getSourceExcludeQuestionItems)?(function() {return objObject.getSourceExcludeQuestionItems();}):{}),
						optionsOrder:((objObject.getSourceExcludeQuestionItemsOrder)?(function() {return objObject.getSourceExcludeQuestionItemsOrder();}):{})
					})),		//Exclude answer from the following simple or multiple choice question
					//JAPR 2016-05-12: Modificado el esquema de visibilidad de preguntas a partir de una condición (#NMW148)
					new FieldCls($.extend(true, {}, objCommon, {name:"showCondition", label:"<?=translate("Show condition")?>", type:GFieldTypes.formula})),
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberZipFileID", label:"<?=translate("Zip File")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberVersionID", label:"<?=translate("Version")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberPlatformID", label:"<?=translate("Platform")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true})),
					new FieldCls($.extend(true, {}, objCommon, {name:"dataMemberTitleID", label:"<?=translate("Title")?>", type:GFieldTypes.combo, parent:"dataSourceID", empty:true}))
					//JAPR 2019-07-15: Agregada la configuración para forzar el recalculo de los defaultValue de las preguntas dependientes (#QF6UEA)
					<?if (getMDVersion() >= esvRecalcDep) {?>
					, new FieldCls($.extend(true, {}, objCommon, objCheck, {name:"recalculateDependencies", label:"<?=translate("Recalculate dependencies")?>"}))
					<?}?>
				);
				
				//OMMC 2016-03-30: Agregado el recorrido de campos default para agregar comportamiento específico a una versión
				$.grep(objTabsColl["advTab"].fields, function(field, i){
					//OMMC 2016-04-22: Agregado el tipo de despliegue especial para preguntas tipo menu
					if (objObject.type == Type.simplechoice && objObject.noFlow && field.name == "displayMode" && vObject.serviceVersion >= vObject.updateVersion.esvNoFlow){
						//OMMC 2016-05-06: Corregido el despliegue de campos para cuando las preguntas menú son de catálogo
						if (objObject.valuesSourceType == TypeOfFill.tofCatalog) {
							field.options = opMenuDisplayModeCatalog;
						} else {
							field.options = opMenuDisplayMode;
						}
					}
					//OMMC 2016-04-22: Agregado el tipo de fuente para preguntas tipo menu
					if (objObject.type == Type.simplechoice && objObject.noFlow && field.name == "valuesSourceType" && vObject.serviceVersion >= vObject.updateVersion.esvNoFlow){
						field.options = opQuestionTypeOfFillingRealMenu;
					}
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					if (objObject.type == Type.simplechoice && objObject.displayMode == DisplayMode.dspAR && field.name == "dataSourceID" && vObject.serviceVersion >= vObject.updateVersion.esvQuestionAR){
						//AQUI: Falta sobreescribir las options de los DS, en teoría con crear un nuevo objeto filtrado basta, pero no es necesario sobreescribir propiedades del grid, ya lo había dejado preparado.
						//field.options = "";
					}
				});
				
				/****************************************************************************
					Inicia el apartado de rulesTab
				*****************************************************************************/
				var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"rulesTab"};
				objTabsColl["rulesTab"].fields =  new Array(
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"minValue", label:"<?=translate("Min")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"maxValue", label:"<?=translate("Max")?>"}))
					//new FieldCls($.extend(true, {}, objCommon, {name:"minValue", label:"<?=translate("Min")?>", type:GFieldTypes.formula})),
					//new FieldCls($.extend(true, {}, objCommon, {name:"maxValue", label:"<?=translate("Max")?>", type:GFieldTypes.formula}))
				);
				
				/****************************************************************************
					Inicia el apartado de valuesTab
				*****************************************************************************/
				var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"valuesTab"};
				var strDeleteClick = "javascript:doDeleteOption(\"" + objCommon.tab + "\");";
				objTabsColl["valuesTab"].fields =  new Array(
					//new FieldCls($.extend(true, {}, objCommon, {name:"open", label:"", type:GFieldTypes.image, image:"images/admin/openArrow.png", width:5, resize:false, click:"javascript:doShowOptionDetail(this);", readOnly:true, columnNum:intColumnNum++})),
					new FieldCls($.extend(true, {}, objCommon, {name:"detail", label:"", type:GFieldTypes.grid, width:25, readOnly:true, columnNum:intColumnNum++,
						definition:"valuesDetailTab"	//Apunta a la tab que define su detalle
					})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>", width:150, readOnly:true, columnNum:intColumnNum++})),
					//JAPR 2015-09-22: Agregada la alineación centrada del Score para que no se empalme (#7JPN3Q)
					//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"score", label:"<?=translate("Score")?>", width:100, readOnly:true, columnNum:intColumnNum++, align:"center"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Skip to section")?>", width:180, type:GFieldTypes.combo, empty:true, readOnly:true, columnNum:intColumnNum++,
						options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
						optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"showQuestionList", label:"<?=translate("Show Questions")?>", width:150, type:GFieldTypes.multiList, readOnly:true, columnNum:intColumnNum++,
						options:((selSurvey.getAllShowQuestionItems)?(function() {return selSurvey.getAllShowQuestionItems();}):{}),
						optionsOrder:((selSurvey.getAllShowQuestionItemsOrder)?(function() {return selSurvey.getAllShowQuestionItemsOrder();}):{})
					}))<?if ($this->FormType != formProdWithDev) {?>,
					new FieldCls($.extend(true, {}, objCommon, {name:"close", label:"", type:GFieldTypes.image, image:"images/admin/close.gif", width:25, resize:false, click:strDeleteClick, readOnly:true, columnNum:intColumnNum++}))
					<?}?>
				);
				
				/****************************************************************************
					Inicia el apartado de valuesDetailTab
				*****************************************************************************/
				var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"valuesDetailTab"};
				//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
				//Se usarán propiedades alternativas tal como están grabadas en la metadata
				objTabsColl["valuesDetailTab"].fields =  new Array(
					//new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"phoneNum", label:"<?=translate("Phone number")?>"})),
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Label")?>"})),
					//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
					new FieldCls($.extend(true, {}, objCommon, objInteger, {name:"score", label:"<?=translate("Score")?>"})),
					new FieldCls($.extend(true, {}, objCommon, {name:"showQuestionList", label:"<?=translate("Show Questions")?>", type:GFieldTypes.multiList,
						options:((objObject.getShowQuestionItems)?(function() {return objObject.getShowQuestionItems();}):{}),
						optionsOrder:((objObject.getShowQuestionItemsOrder)?(function() {return objObject.getShowQuestionItemsOrder();}):{})
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Skip to section")?>", type:GFieldTypes.combo, empty:true,
						options:((objObject.getNextSectionItems)?(function() {return objObject.getNextSectionItems();}):{}),
						optionsOrder:((objObject.getNextSectionItemsOrder)?(function() {return objObject.getNextSectionItemsOrder();}):{})
					})),
					new FieldCls($.extend(true, {}, objCommon, {name:"displayImageDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg}))
				);
				
				//JAPR 2016-04-22: Validado que si no se cumple con la versión correcta no agregue las cejillas de reglas (#OZGBL9)
				/****************************************************************************
					Inicia el apartado de rulesTab
				*****************************************************************************/
				if (objTabsColl["skipRulesTab"]) {
					//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					intColumnNum = 1;
					var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"skipRulesTab"};
					var strDeleteClick = "javascript:doDeleteSkipRule(\"" + objCommon.tab + "\");";
					objTabsColl["skipRulesTab"].fields =  new Array(
						new FieldCls($.extend(true, {}, objCommon, {name:"detail", label:"", type:GFieldTypes.grid, width:25, readOnly:true, columnNum:intColumnNum++,
							definition:"skipRulesDetailTab"	//Apunta a la tab que define su detalle
						})),
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>", width:150, readOnly:true, columnNum:intColumnNum++})),
						new FieldCls($.extend(true, {}, objCommon, {name:"skipCondition", label:"<?=translate("Formula")?>", width:250, readOnly:true, type:GFieldTypes.formula, columnNum:intColumnNum++})),
						new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Skip to section")?>", width:180, type:GFieldTypes.combo, empty:true, readOnly:true, columnNum:intColumnNum++,
							options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
							optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
						}))<?if ($this->FormType != formProdWithDev) {?>,
						new FieldCls($.extend(true, {}, objCommon, {name:"close", label:"", type:GFieldTypes.image, image:"images/admin/close.gif", width:25, resize:false, click:strDeleteClick, readOnly:true, columnNum:intColumnNum++}))
						<?}?>
					);
				}
				
				//JAPR 2016-04-22: Validado que si no se cumple con la versión correcta no agregue las cejillas de reglas (#OZGBL9)
				/****************************************************************************
					Inicia el apartado de rulesDetailTab
				*****************************************************************************/
				if (objTabsColl["skipRulesDetailTab"]) {
					var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"skipRulesDetailTab"};
					//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
					//Se usarán propiedades alternativas tal como están grabadas en la metadata
					objTabsColl["skipRulesDetailTab"].fields =  new Array(
						//new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"phoneNum", label:"<?=translate("Phone number")?>"})),
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>"})),
						new FieldCls($.extend(true, {}, objCommon, {name:"skipCondition", label:"<?=translate("Formula")?>", type:GFieldTypes.formula})),
						new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Skip to section")?>", type:GFieldTypes.combo, empty:true,
							options:((objObject.getNextSectionItems)?(function() {return objObject.getNextSectionItems();}):{}),
							optionsOrder:((objObject.getNextSectionItemsOrder)?(function() {return objObject.getNextSectionItemsOrder();}):{})
						})),
						new FieldCls($.extend(true, {}, objCommon, {name:"displayImageDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg}))
					);
				}
				
				//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				/****************************************************************************
					Inicia el apartado de itemsTab
				*****************************************************************************/
				if (objTabsColl["itemsTab"]) {
					//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					intColumnNum = 1;
					var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"itemsTab"};
					var strDeleteClick = "javascript:doDeleteOption(\"" + objCommon.tab + "\");";
					objTabsColl["itemsTab"].fields =  new Array(
						//new FieldCls($.extend(true, {}, objCommon, {name:"open", label:"", type:GFieldTypes.image, image:"images/admin/openArrow.png", width:5, resize:false, click:"javascript:doShowOptionDetail(this);", readOnly:true, columnNum:intColumnNum++})),
						new FieldCls($.extend(true, {}, objCommon, {name:"detail", label:"", type:GFieldTypes.grid, width:25, readOnly:true, columnNum:intColumnNum++,
							definition:"itemsDetailTab"	//Apunta a la tab que define su detalle
						})),
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Name")?>", width:150, readOnly:true, columnNum:intColumnNum++})),
						//JAPR 2015-09-22: Agregada la alineación centrada del Score para que no se empalme (#7JPN3Q)
						//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
						new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Source section")?>", width:180, type:GFieldTypes.combo, empty:true, readOnly:true, columnNum:intColumnNum++,
							options:((selSurvey.getAllSectionItems)?(function() {return selSurvey.getAllSectionItems();}):{}),
							optionsOrder:((selSurvey.getAllSectionItemsOrder)?(function() {return selSurvey.getAllSectionItemsOrder();}):{})
						}))<?if ($this->FormType != formProdWithDev) {?>,
						new FieldCls($.extend(true, {}, objCommon, {name:"close", label:"", type:GFieldTypes.image, image:"images/admin/close.gif", width:25, resize:false, click:strDeleteClick, readOnly:true, columnNum:intColumnNum++}))
						<?}?>
					);
				}
				
				/****************************************************************************
					Inicia el apartado de itemsDetailTab
				*****************************************************************************/
				if (objTabsColl["itemsDetailTab"]) {
					var objCommon = {id:intQuestionID, parentType:AdminObjectType.otyQuestion, tab:"itemsDetailTab"};
					//JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
					//Se usarán propiedades alternativas tal como están grabadas en la metadata
					objTabsColl["itemsDetailTab"].fields =  new Array(
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"name", label:"<?=translate("Label")?>"})),
						//JAPR 2016-07-28: Corregido un bug, se estaba permitiendo grabado un Score como texto (#XYTODV)
						new FieldCls($.extend(true, {}, objCommon, {name:"nextSection", label:"<?=translate("Source section")?>", type:GFieldTypes.combo, empty:true,
							options:((objObject.getNextSectionItems)?(function() {return objObject.getNextSectionItems();}):{}),
							optionsOrder:((objObject.getNextSectionItemsOrder)?(function() {return objObject.getNextSectionItemsOrder();}):{})
						})),
						new FieldCls($.extend(true, {}, objCommon, {name:"displayImageDes", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg}))
					);
				}
				//JAPR
				
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Marcará todos los campos como deshabilitados según el tipo de forma
				if ($this->FormType == formProdWithDev) {?>
				for (var strTabID in objTabsColl) {
					if (objTabsColl[strTabID].fields && objTabsColl[strTabID].fields.length) {
						for (var intFieldNum in objTabsColl[strTabID].fields) {
							if (objTabsColl[strTabID].fields[intFieldNum].disabled === undefined) {
								objTabsColl[strTabID].fields[intFieldNum].disabled = true;
							}
						}
					}
				}
				<?}
				//@JAPR
				?>
				
				generateFormDetail(getQuestionFieldsDefaulVisibility(sNodeId, objTabsColl), AdminObjectType.otyQuestion, intQuestionID);
			}
			
			/* Realiza el cambio de propiedades del objeto en el Previw según el elemento actualmente seleccionado
			//JAPR 2015-08-20: Agregado el parámetro bSkipValidation para forzar a que se realice el proceso aunque sea innecesario porque no ha cambiado localmente, esto se requiere
			debido a que procesos de validaciones de cambios de algunas opciones requieren ejecutar el grabado de configuraciones que encadenarán otros cambios en el server, pero localmente
			la información que se ve en pantalla o instancia no cambia, así que si no se evita esta validación nunca se actualizará en el server y quedará desfasado lo que se ve con lo que
			realmente aplicará en el server
			*/
			function doUpdateProperty(oGrid, iObjectType, iObjectID, rId, cInd, nValue, bSkipValidation) {
				console.log('doUpdateProperty iObjectType == ' + iObjectType + ', iObjectID == ' + iObjectID + ', rId == ' + rId + ', cInd == ' + cInd + ', nValue == ' + nValue);
				
				if (nValue == null) {
					debugger;
				}
				
				if (!oGrid) {
					return;
				}
				
				if (iObjectType === undefined) {
					iObjectType = oGrid.bitObjectType;
				}
				if (iObjectID === undefined) {
					iObjectID = oGrid.bitObjectID;
				}
				if (!iObjectType || !iObjectID) {
					return;
				}
				//JAPR 2015-06-29: Agregado el subgrid de propiedades de los valores
				var intChildObjectType = oGrid.bitChildObjectType;
				var intChildObjectID = oGrid.bitChildObjectID;
				
				//Verifica que realmente hubiera un cambio en el valor de esta propiedad antes de realizar el proceso
				var varOldValue = oGrid.getRowAttribute(rId, "value");
				//JAPR 2015-08-20: Agregado el parámetro bSkipValidation para forzar a que se realice el proceso aunque sea innecesario porque no ha cambiado localmente
				//JAPR 2015-11-04: Corregido un bug, al comparar valores numéricos con JSON.stringify contra un string del mismo número, se consideran diferentes así que grababa como
				//cambio aunque el valor fuera el mismo sólo por uno venir de un componente (ser string) y otro de una propiedad (ser integer)
				//Para evitar problemas, ambas propiedades se convertirán a String antes de comparar el valor
				if ($.isNumeric(varOldValue)) {
					varOldValue = String(varOldValue);
				}
				var strNewValue = nValue;
				if ($.isNumeric(varOldValue)) {
					strNewValue = String(strNewValue);
				}
				if (JSON.stringify(varOldValue) == JSON.stringify(strNewValue) && !bSkipValidation) {
					return;
				}
				
				oGrid.setRowAttribute(rId, "value", nValue);
				var objObject = getObject(iObjectType, iObjectID, intChildObjectType, intChildObjectID);
				if (objObject) {
					objObject[rId] = nValue;
				}
				
				//JAPR 2019-09-11: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
				//Agregado el evento onChange para permitir ejecutar acciones cuando un campo ha cambiado de valor (el evento se invocará antes del grabado pero no tiene manera de cancelarlo)
				var strTabName = oGrid.bitTabName;
				var blnForForm = (iObjectType == AdminObjectType.otySurvey);
				var objField = getFieldDefinition(strTabName, rId, blnForForm);
				if ( objField && objField.onChange ) {
					try {
						objField.onChange(oGrid.getRowAttribute(rId, "value"), nValue);
					}
					catch(e) {
						console.log("Error in event onchange for field " + rId + ": " + e);
					}
				}
				
				//JAPR 2015-08-10: Modificado para que actualice la información de la sección al regresar de algún cambio en el server
				var objCallbackFn = undefined;
				if (iObjectType == AdminObjectType.otySection) {
					objCallbackFn = function(oNewDefinition) {
						//JAPR 2015-11-13: Corregido un bug, los campos de Catálogo, Atributo y Filtro se estaban mostrando para tipos de sección que no aplican
						objObject.refreshView(oNewDefinition, true);
						//OMMC 2015-11-24: Debido a comportamientos erráticos de los componentes de jQuery, se optó por cambiar visualmente las preguntas MChoice a verticales cuando son de tipo menú o autocomplete.
						//Sólo ocurre en secciones tipo tabla
						if(objObject.type == SectionType.Inline && objObject.displayMode == SectionDisplayMode.sdspInline){
							for(var intQuestionID in objObject.questions){
								var actualQuestion = objObject.questions[intQuestionID];
								if(actualQuestion.type == Type.multiplechoice && (actualQuestion.displayMode == DisplayMode.dspAutocomplete || actualQuestion.displayMode == DisplayMode.dspMenu)){
									actualQuestion.displayMode = DisplayMode.dspVertical;
								}
							}
						}
					}
				}
				//JAPR
				
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (!gbFullSave && objObject && objObject.save) {
					var objParams = {};
					objParams[rId] = 1;
					//OMMC 2016-01-19: Agregado validaciones para preguntas: htmlCode, htmlHeaderDes, htmlFooterDes
					//JAPR 2016-05-10: Agrupados los casos en un switch
					switch (objObject.objectType) {
						case AdminObjectType.otyQuestion:
							switch (objObject.type) {
								case Type.simplechoice:
									if (rId == 'customLayout' && objObject.displayMode == DisplayMode.dspMetro) {
										objParams['HTMLEditorState'] = 1;
									}
									break;
								case Type.message:
									if (rId == 'htmlCode') {
										objParams['HTMLEditorState'] = 1;
									}
									break;
							}
							break;
						case AdminObjectType.otySection:
							if(rId == 'htmlHeaderDes') {
								objParams['HTMLHEditorState'] = 1;
							}
							else {
								if(rId == 'htmlFooterDes') {
									objParams['HTMLFEditorState'] = 1;
								}
							}
							break;
					}
					//OMMC
					
					setTimeout(objObject.save(objParams, objCallbackFn), 100);
				}
			}
			
			/* Realiza el cambio de posición del objeto indicado diretamente en el server
			*/
			function doReorderObject(iObjectType, iObjectID, iNewPos) {
				console.log('doReorderObject iObjectType == ' + iObjectType + ', iObjectID == ' + iObjectID + ', iNewPos == ' + iNewPos);
				
				if (!iObjectType || !iObjectID) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				var intOldPos = 0;
				var objObject = getObject(iObjectType, iObjectID);
				if (objObject) {
					intOldPos = objObject["number"];
					objObject["number"] = iNewPos;
				}
				
				//Si se trata de una pregunta, se sobreescribe la función que cancela el Drop para evitar que el iFrame del preview tenga uqe hacerlo
				if (iObjectType == AdminObjectType.otyQuestion) {
					doCancelDragDropOperation = function() {
						doRestoreDroppedItem(AdminObjectType.otyQuestion, iObjectID, intOldPos);
					};
				}
				
				//Si está activado el grabado automático durante la edición, se invoca al mismo en este punto sólo con la propiedad modificada
				if (!gbFullSave && objObject && objObject.save) {
					var objParams = {};
					objParams["number"] = 1;
					setTimeout(objObject.reorder(objParams), 100);
				}
			}
			
			/* Obtiene la referencia al objeto indicado por el tipo y id de los parámetros. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objObject = undefined;
				
				if (!selSurvey) {
					return objObject;
				}
				
				switch (iObjectType) {
					case AdminObjectType.otySurvey:
						objObject = selSurvey;
						break;
					case AdminObjectType.otySection:
						if (selSurvey.sections) {
							objObject = selSurvey.sections[iObjectID];
						}
						break;
					case AdminObjectType.otyQuestion:
						if (selSurvey.questions) {
							objObject = selSurvey.questions[iObjectID];
						}
						break;
				}
				
				if (iChildObjectType && iChildObjectID && objObject) {
					var objTmpObject = undefined;
					switch (iChildObjectType) {
						case AdminObjectType.otyOption:
							if (objObject.optionsByID) {
								objTmpObject = objObject.optionsByID[iChildObjectID];
							}
							break;
						//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
						case AdminObjectType.otySkipRule:
							if (objObject.skipRules) {
								objTmpObject = objObject.skipRules[iChildObjectID];
							}
							break;
						//JAPR
					}
					
					objObject = objTmpObject;
				}
				
				return objObject;
			}
			
			/* Esta función pinta todas las rows de los campos definidos en aTabsColl
			//JAPR 2015-06-18: Agregados los parámetros iObjectType, iObjectID para identificar el objeto actualmente desplegado en las propiedades
			//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
			*/
			function generateFormDetail(aTabsColl, iObjectType, iObjectID, oContainer) {
				console.log('generateFormDetail');
				
				//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
				if (oContainer === undefined) {
					oContainer = objPropsLayout.cells(dhxPropsDetailCell);
				}
				//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
				doRemovePopUps(oContainer);
				//JAPR
				oContainer.detachObject(true);
				
				//Si el contenedor tiene una propiedad cell, debe ser una celda de Layout o Tabbar (en ese caso es directamente el div con el contenido)
				//o algún componente que funciona de manera similar, en cualquier caso se le aplica el estilo de visibilidad a dicho div, ya que debido a 
				//que el Tabbar principal se creó con un estilo para ocultarlo, y como toda la ventana se construye a partir del mismo div base (excepto 
				//el preview que existe en un iframe), todos los tabbars internos se ven afectados por el estilo del div base, por lo que hay que cambiar 
				//el estilo a visible para los que si se desean con la parte de Tabs en pantalla, esto se hace sobre la celda que contendrá al tabbar y 
				//antes de pintarlo
				if (oContainer.cell) {
					$(oContainer.cell).addClass("visibleTabs");
				}
				objPropsTabs = oContainer.attachTabbar();
				//JAPR
				
				for (var strTabName in aTabsColl) {
					var objTab = aTabsColl[strTabName];
					var blnActive = (objTab.activeOnLoad == true);
					objPropsTabs.addTab(strTabName, objTab.name, null, null, blnActive);
					//Asigna la visibilidad por default de la cejilla
					//JAPR 2015-06-29: Modificada la visibilidad para usar el parámetro "self" para referirse a la propia tab y permitir ocultar campos de la misma
					if (!objTab.defaultVisibility || objTab.defaultVisibility["self"] === false) {
						objPropsTabs.tabs(strTabName).hide();
					}
					
					//Generar todos los fields del tab
					var objGrid = objPropsTabs.tabs(strTabName).attachGrid('gridbox');
					var strGridStyle = "font-family:Roboto Regular;font-size:13px;"
					objGrid.setStyle(strGridStyle, strGridStyle);
					doGenerateGrid(objGrid, objTab, iObjectType, iObjectID);
				}
			}
			
			/* Permite capturar un texto simple pero abrir un editor de fórmulas si se desea un texto mas complejo con ayuda de dicho editor
			*/
			function eXcell_edFormula(cell) {
				if (cell) {
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function() {
					this.val = this.getValue();
					var strRowId = this.cell.parentNode.idd;
					this.cell.innerHTML = "<img src='images/admin/editFormula.gif' style='width:18px; height:18px; float:left; cursor:pointer;'>"
						+ "<input autocomplete='off' class='dhx_combo_edit' style='height: 28px;'>";
					
					this.cell.childNodes[1].value = this.val;
					var objCell = this.cell;
					//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
					var intDataSourceID = 0;
					var intObjectType = this.grid.bitObjectType;
					var intObjectID = this.grid.bitObjectID;
					var objObject = getObject(intObjectType, intObjectID);
					//JAPR 2015-09-30: Habilitado el filtro dinámico de preguntas de catálogo para todos los tipos
					//Se habilitó el parámetro del DataSource para toda pregunta que sea configurada como captura de catálogo, de esa manera todas se comportarán en el editor como
					//la pregunta getData, cambiando la referencia a ella misma por atributos de catálogo (este casó sólo podría aplicar para la configuración de filtros dinámicos,
					//aunque ténicamente si se habilitada un defaultValue u otra expresión con variables en esta pregunta, generaría problemas porque la validación se basa en que
					//tenga un dataSourceID mas que en si es o no una expresión de filtro dinámico)
					//JAPR 2016-04-20: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					//En caso de ser una fórmula del tab de reglas de salto, NO se deben considerar los atributos como variables de filtros dinámicos pues son usados por el App
					//Corregido un bug, las preguntas múltiple choide de catálogo no estaban permitiendo definir filtro dinámico sobre ella misma (#VOPSK0)
					var strTabName = this.grid.bitTabName;
					if (objObject && objObject.dataSourceID && (
							(objObject.objectType == AdminObjectType.otyQuestion && (objObject.type == Type.simplechoice || objObject.type == Type.multiplechoice) && (objObject.displayMode == DisplayMode.dspGetData || objObject.valuesSourceType == TypeOfFill.tofCatalog) && strTabName != "skipRulesDetailTab") || 
							(objObject.objectType == AdminObjectType.otySection && objObject.valuesSourceType == TypeOfFill.tofCatalog)
							)) {
						intDataSourceID = objObject.dataSourceID;
					}
					
					//JAPR 2016-04-20: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
					//Si se trata de una pregunta simple choice de catálogo y se está editando la expresión en el tab de reglas de salto, si se puede permitir usar a la misma pregunta
					//dentro del editor, así que se limpian las variables que de otra manera impedirían que eso sucediera
					if (objObject && (objObject.objectType == AdminObjectType.otyQuestion && objObject.type == Type.simplechoice && objObject.valuesSourceType == TypeOfFill.tofCatalog && strTabName == "skipRulesDetailTab")) {
						intObjectType = 0;
						intObjectID = 0;
					}
					//JAPR
					
					this.cell.childNodes[0].onclick = function(e) {
						//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
						fnOpenFormulaEditor(objCell, objCell.childNodes[1].value, intDataSourceID, undefined, intObjectID, intObjectType);
						(e||event).cancelBubble = true;	//blocks onclick event
					}
					//Agrega el evento blur para actualizar el valor al perder el foco en el input
					var objGrid = this.grid;
					//Prepara el evento para recibir el <enter>, ya que internamente la celda manda al mismo evento editStop ya sea con <enter> (aceptar la captura) o <escape> (cancelar
					//la captura) y no da indicios de por cual de los dos exactamente se lanzó, así que se invocará al detach de la celda y ambos cancelarían el onblur, por lo que hay que
					//detectar desde el propio input cual de las teclas provocó el editStop y prevenir que en caso de <enter> se descarte el contenido
					this.cell.childNodes[1].onkeydown = function(e) {
						if (e && e.keyCode == 13) {
							this.bitPreventDisableBlur = true;
						}
					}
					this.cell.childNodes[1].onblur = function(e) {
						//El evento se controlará con este atributo, ya que la manera en que funciona el Grid con una celda editable (como este componente) es que lanza el evento editStop
						//el cual invoca a la función detach y ella sobreescribe la celda provocando el onBlur, para posteriormente hacer el setValue con el valor anterior a lo que se
						//estaba editando, pero para ese punto ya el onblur habría enviado el valor cancelado al server para grabar y visualmente se habría quedado el valor original,
						//así que el evento detach va a generar el atributo de esta validación en true para evitar el onblur en ese punto
						if (!this.bitDisableBlur || this.bitPreventDisableBlur) {
							doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, this.value);
						}
					}
					this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
				}
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("edFormula.setValue " + val);
					//JAPR 2015-11-12: Corregido un bug, este campo no contiene HTML así que debe usarse la versión de la función que sólo asigna texto (#WN65X7)
					//this.setCValue(val);
					this.setCTxtValue(val);
					//JAPR
				}
				this.getValue = function() {
					//JAPR 2015-11-12: Corregido un bug, este campo no contiene HTML así que debe usarse la versión de la función que sólo asigna texto (#WN65X7)
					return this.cell.innerText;
					//return this.cell.innerHTML;
					//JAPR
				}
				this.detach = function() {
					if (this.cell && this.cell.childNodes && this.cell.childNodes[1]) {
						this.cell.childNodes[1].bitDisableBlur = true;
					}
					
					this.setValue(this.cell.childNodes[1].value); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_edFormula.prototype = new eXcell;    // nests all other methods from base class
			
			/* Permite definir celdas de tipo DHTMLXEditor en el grid
			*/
			function eXcell_editor(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("editor.setValue " + val);
					
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellEditor_"+strRowId+'_'+this.cell.cellIndex;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					//JAPR 2015-09-02: Agregada la configuración del Height para el componente Editor
					var intHeight = objField.height;
					var strHeight = String(intHeight);
					strHeight = strHeight.replace('%', '').replace('px', '');
					if (!$.isNumeric(strHeight) || parseInt(strHeight) <= 0) {
						intHeight = '100px';
					}
					
					var strDisabledStyle = "";
					<?
					//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if ($this->FormType == formProdWithDev) {?>
					if (objField.disabled) {
						strDisabledStyle = ' style="display:none"';
					}
					<?}
					//@JAPR
					?>
					
					var buttonApplyHTML; 
					buttonApplyHTML = '<button type="button" onclick="javascript:applyEditor(\''+tabName+'\',\''+strRowId+'\')"'+strDisabledStyle+'><?=translate("Apply")?></button>';
					
					this.setCValue("<div id='"+strId+"' class='editor_compact' style='height:"+intHeight+";width:100%;overflow:visible;'>"+val+"</div>"+buttonApplyHTML,val);
					//JAPR
					var myEditor = new dhtmlXEditor({parent:strId, toolbar:true, iconsPath:"images/editor"});
					myEditor.setContent(val);
					myEditor.bitFieldName = strRowId;
					myEditor.bitEditorDivID = strId;
					<?
					//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if ($this->FormType == formProdWithDev) {?>
					if (objField.disabled) {
						myEditor.setReadonly(true);
					}
					<?}
					//@JAPR
					?>
					
					//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
					if ( !this.grid.bitChangingCellType ) {
						changeEditor(myEditor, objField.customButtons, tabName, strRowId);
					}
					//JAPR
					
					var objGrid = this.grid;
					myEditor.attachEvent("onAccess", function(eventName, evObj) {
						//Se comentó lo del evento blur por que esto ya se hará cuando se da click en el botón Aplicar
						/* 
						if (eventName == "blur") {
							//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
							var strContent = this.getContent();
							if ($.trim(strContent) == '' || $.trim(strContent.replace("<br>", "")) == '') {
								if (this.editor) {
									var objEditor = $(this.editor)[0];
									if (objEditor && objEditor.contentWindow && objEditor.contentWindow.focus) {
										var strContent = "<?=translate("You must enter a value for this field")?>";
										this.setContent(strContent);
										alert(strContent);
										objEditor.contentWindow.focus();
									}
								}
								return;
							}
							//JAPR
							
							//Al perder el foco debe actualizar el valor en el atributo de la row
							if (objGrid) {
								doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, strContent);
							}
						}
						*/
					});
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "editor", myEditor);
						//JAPRWarning: myEditor.tb es la DHTMLX Toolbar que se puede personalizar en este punto, obtener de this.grid.getUserData/getRowAttribute los botones personalizados
						//y la configuración de botones a ocultar como uyn array que debe modificat myEditor.tb
					}
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						strValue = myEditor.getContent();
					}
					return strValue;
				}
			}
			eXcell_editor.prototype = new eXcell;// nests all other methods from the base class
			
			function applyEditor(tabName, strRowId)
			{	
				//Obtiene la referencia al editor del campo que se está editando para obtener el contenido actualizado, asímismo se obtiene la referencia al grid para invocar al grabado
				var objGrid = undefined;
				if (objPropsTabs && objPropsTabs.tabs && objPropsTabs.tabs(tabName) && objPropsTabs.tabs(tabName).getAttachedObject) {
					var objGrid = objPropsTabs.tabs(tabName).getAttachedObject();
				}
				var myEditor = undefined;
				if (objGrid && objGrid.getUserData) {
					myEditor = objGrid.getUserData(strRowId, "editor");
				}
				
				//Código tomado de la función onAccess del editor, donde está que se grabé cuando pierde el focus
				//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
				var strContent = myEditor.getContent();
				if ($.trim(strContent) == '' || $.trim(strContent.replace("<br>", "")) == '') {
					if (myEditor.editor) {
						var objEditor = $(myEditor.editor)[0];
						if (objEditor && objEditor.contentWindow && objEditor.contentWindow.focus) {
							var strContent = "<?=translate("You must enter a value for this field")?>";
							myEditor.setContent(strContent);
							alert(strContent);
							objEditor.contentWindow.focus();
						}
					}
					return;
				}
				//JAPR
				
				//Al perder el foco debe actualizar el valor en el atributo de la row
				if (objGrid) {
					doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, strContent);
				}

				//Agregar el código necesario para que se refresque la pregunta en el preview
				//Este código se basa en la función 
				if (!objFormsLayout || !objFormsLayout.cells || !objFormsLayout.cells(dhxPreviewCell)) {
					return;
				}
				
				try {
					//Obtiene la referencia a la pregunta en el preview
					var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
					if (!objiFrame.contentWindow.frmDevice) {
						return;
					}
					if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
						if (!objiFrame.contentWindow.frmDevice.selSurvey) {
							return;
						}
					} else {
						if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
							return;
						}
					}
					
					var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
					if (!objselSurvey || !objselSurvey.questions) {
						return;
					}
					
					var objQuestion = objselSurvey.questions[objGrid.bitObjectID];
					if (!objQuestion) {
						return;
					}
					
					objQuestion.nameHTML = strContent;
					objQuestion.nameHTMLDes = strContent;
					objQuestion.reDraw();
				} catch(e) {
					console.log("Error updating name in preview: " + e);
				}
				
			}
			
			function eXcell_editorHTML(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){
					this.val = this.getValue();
					var strValue = this.val;
					var strRowId = this.cell.parentNode.idd;
					var strId = "editorHTML_"+strRowId+'_'+this.cell.cellIndex;
					//this.setCValue("<div id='"+strId+"' style='height:150px;width:100%;overflow:visible;'><textarea id='txt"+strId+"' name='txt"+strId+"'>"+val+"</textarea></div>",val);
					this.cell.innerHTML = "<div id='"+strId+"' style='height:150px;width:100%;overflow:visible;'><textarea id='txt"+strId+"' name='txt"+strId+"'></textarea></div>";
					
					var objGrid = this.grid;
					var objEditorHTML = undefined;
					$('#txt'+strId).redactor({
						focus: true,
						lang: 'es',
						//Definición de la altura del textarea
						minHeight: 150,
						maxHeight: 150,
						//Botones que se van a diujar
						buttons: ['html', 'bold', 'italic', 'image', 'file'],
						
						//Otros botones que se pueden dibujar
						//'formatting', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'
						//Botones que se pueden ocultar
						//buttonsHide: ['formatting', 'deleted', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'],
						
						//Plugins de subrayar, subir imágenes, tamaño de letra, tipo de leta y color de letra
						plugins:[
							'underline',
							'imagemanager',
							'fontsize',
							'fontfamily',
							'fontcolor'
						],
						//Incluir la ruta del archivo encargado de subir las imágenes al servidor
						imageUpload: 'images/image.php',
						
						initCallback: function()
						{
							objEditorHTML = this;
							this.code.set(strValue);
						},
						/*,
						blurCallback: function(e)
						{
							//alert('EVENTO BLUR:'+this.code.get());
							//alert("BLUR objGrid:"+objGrid);
							if (objGrid) {
								//alert("this.code.get:"+this.code.get());
								doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, this.code.get());
							}
						}*/
					});
					
					$('#'+strId).on('click', function(e) {
						debugger;
						(e||event).cancelBubble = true;
					})
					//this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick e
					
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					objEditorHTML.bitFieldName = strRowId;
					objEditorHTML.bitEditorDivID = strId;
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "editor", objEditorHTML);
					}
				}  //read-only cell doesn't have edit method
				//this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					//debugger;
					console.log ("editor.setValue " + val);
					
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						if (myEditor && myEditor.code) {
							strValue = myEditor.code.get();
						}
					}
					
					return strValue;
				}
				this.detach = function() {
					var strRowId = this.cell.parentNode.idd;
					if (this.grid) {
						myEditor = this.grid.getUserData(strRowId, "editor");
						strValue = myEditor.code.get();
					}
					
					this.setValue(strValue); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_editorHTML.prototype = new eXcell;// nests all other methods from the base class
			
			//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
			dhtmlXForm.prototype.items.editorHTMLForm = {
				//mandatory functions
				render: function(item, data){
					item._type = "editorHTMLForm";
					item._enabled = true;
					item.appendChild(document.createElement("div"));
					item.firstChild.appendChild(document.createElement("textarea"));
					//$(item.lastChild).css('background-color', 'red');
					$(item.firstChild).css('width', '655px');
					$(item.firstChild).css('height', '200px');
					//JAPR 2019-08-08: Eliminados los popups creados por diálogos (#BT0NMN)
					$(item).data('id', data.name);
					//JAPR
					$(item.firstChild.lastChild).attr('id', data.name);
					$(item.firstChild.lastChild).attr('name', data.name);
					item.firstChild.lastChild.innerHTML = data.value;
					item.isFromFormulaEditor = false;
					item.HTMLcodeCache = '';
					item.aState = 0;
					//JAPR 2016-06-28: Corregido un bug, al entrar a modo HTML con el Redactor, no se lanzan los eventos de blur o change, así que si en dicho estado se presionaba Ok
					//en la forma que lo contenía o se hacía cualquier evento que cerrara el redactos, sus cambios no se grababan (#IEJDXN)
					//Para corregir este problema, se almacenará en el objeto que representa los campos personalizados de la forma, una referencia al objeto redactos para poder actualizar
					//su texto justo antes de terminar de usar la forma
					if (!this.bitObjs) {
						this.bitObjs = new Object();
					}
					this.bitObjs[data.name] = undefined;
					var objCustomizedFieldsObj = this;
					
					//Se liga el ID del textarea del HTML al plugin
					$('#'+data.name).redactor({
						focus: true,
						lang: redactorLang,
						tabIndex: -1,
						//Definición de la altura del textarea
						minHeight: 250,
						maxHeight: 250,
						linebreaks: true,
						//Botones que se van a diujar
						buttons: data.redactorButtons,
						//JAPR 2016-06-15: Corregido un bug, debido a que el default de replaceDivs es true, se estaban eliminando todos los divs que el usuario introducía (#XWUYJ5)
						replaceDivs:false,
						//JAPR 2016-06-20: Esta configuración NO es parte del redactor, se agregó para que dentro de formulaEditor.js se pueda invocar al editor de fórmulas usando
						//la función local cuando se encuentre incrustado el componente dentro de surveyExt.inc.php (true), o se invoque a través del parent cuando se abre como un
						//php independiente (false) que es el comportamiento original cuando el EditorHTML se abre en un diálogo independiente de la página donde se invoca
						bitEmbedded:true,
						//OMMC 2015-10-14: Este par de callbacks revisan el código HTML periódicamente, si existe algun cambio, el código se refresca.
						initCallback: function(e){
							//JAPR 2016-06-28: Corregido un bug, al entrar a modo HTML con el Redactor, no se lanzan los eventos de blur o change, así que si en dicho estado se presionaba Ok
							//en la forma que lo contenía o se hacía cualquier evento que cerrara el redactos, sus cambios no se grababan (#IEJDXN)
							//Para corregir este problema, se almacenará en el objeto que representa los campos personalizados de la forma, una referencia al objeto redactos para poder actualizar
							//su texto justo antes de terminar de usar la forma
							if (objCustomizedFieldsObj && objCustomizedFieldsObj.bitObjs) {
								objCustomizedFieldsObj.bitObjs[data.name] = this;
							}
							//JAPR
							item.formEditorIcon = this.$toolbar[0].children[this.$toolbar[0].children.length-1];
							item.formEditorIcon.addEventListener("click", function(){
								item.isFromFormulaEditor = true;
							});
							
							var firstIcon = $(this.$toolbar[0].children[0]).find('a[rel=html]');
							//Encontró que el HTML está habilitado
							if(firstIcon.length > 0){
								//OMMC 2016-01-16: Se agrega el listener del boton del estado del editor HTML
								item.HTMLStatusIcon = this.$toolbar[0].children[0];
								item.HTMLStatusIcon.addEventListener("click", function(){
									//Agregados estados del editor HTML, 1 - HTML, 2 - Gráfico
									if(item.aState == 1){
										item.aState = 0;
									}else{
										item.aState = 1;
									}
								});
							}
							
							//Se revisa el estado, para mostrar el editor en la forma correcta cuando se inicializa.
							if(item.aState == 1){
								this.code.showCode();
							}
						},
						focusCallback: function(e)
						{
							if(item.isFromFormulaEditor){
								var HTMLcodeLive = this.code.get();
								if(item.HTMLcodeCache != HTMLcodeLive){
									this.code.set(HTMLcodeLive);
									item.isFromFormulaEditor = false;
								}
							}			    		
						},
						blurCallback: function(e)
						{
							item.HTMLcodeCache = this.code.get();
						},
						changeCallback: function(e)
						{
							item.HTMLcodeCache = this.code.get();
						},
						//JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
						/* Atrapa los errores de upload de imágenes, el parámetro contendrá la descripción del error en la propiedad message */
						imageUploadErrorCallback: function(e) {
							if ( e.error ) {
								alert(e.message || '');
							}
						},
						imageUploadCallback: function(image, json) {
						},
						//OMMC
						//Otros botones que se pueden dibujar
						/*'formatting', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'*/
						//Botones que se pueden ocultar
						/*buttonsHide: ['formatting', 'deleted', 'deleted', 'unorderedlist', 'orderedlist', 'outdent', 'indent', 'alignment', 'horizontalrule'],*/
						
						//OMMC 2015-10-07  Se deshabilita el editor de fórmulas.
						//Plugins de subrayar, subir imágenes, tamaño de letra, tipo de leta y color de letra
						plugins:[
							'underline',
							'imagemanage',
							'fontsize',
							'fontfamily',
							'fontcolor',
							'formulaEditor'
						],
						deniedTags: ['style', 'head', 'body', 'html'],
						//Incluir la ruta del archivo encargado de subir las imágenes al servidor
						imageEditable: false,
						imageResizable: false,
						dragImageUpload: false,
						dragFileUpload: false,
						imageLink: false,
						imageUpload: 'redactorImageManagement.php'
					});
					return this;
				}, //constructor
				destruct: function(item){
					/* your custom code is started here */
					/*debugger;
					if (item.formEditorIcon) {
						item.formEditorIcon.removeEventListener("click");
					}
					if (item.HTMLStatusIcon) {
						item.HTMLStatusIcon.removeEventListener("click");
					}*/
					//JAPR 2019-08-08: Eliminados los popups creados por diálogos (#BT0NMN)
					//Destruye la instancia del redactor que estuviera creada para remover los popups y otros componentes flotantes
					var strId = $(item).data('id');
					if ( strId ) {
						$('#' + strId).redactor('core.destroy');
					}
					//JAPR
					item.firstChild.lastChild.onclick = null;
					item.innerHTML = "";
				}, //destructor
				setValue: function(item, value){
					//JAPR 2016-06-28: Corregido un bug, al entrar a modo HTML con el Redactor, no se lanzan los eventos de blur o change, así que si en dicho estado se presionaba Ok
					//en la forma que lo contenía o se hacía cualquier evento que cerrara el redactos, sus cambios no se grababan (#IEJDXN)
					//En este punto el objeto item es una referencia al componente HTML que se generó en el campo personalizado de la forma DHTMLX, sin embargo como dicho objeto se
					//utilizó en el Plug-in del editor HTML, no hay una referencia directa a él así que se debe obtener mediante un selector
					$(item).find("textarea").val(value);
					//item.HTMLcodeCache = value;
					//item._value = value;
				}, //sets the value of the item
				getValue: function(item){
					//JAPR 2016-06-28: Corregido un bug, al entrar a modo HTML con el Redactor, no se lanzan los eventos de blur o change, así que si en dicho estado se presionaba Ok
					//en la forma que lo contenía o se hacía cualquier evento que cerrara el redactos, sus cambios no se grababan (#IEJDXN)
					//En este punto el objeto item es una referencia al componente HTML que se generó en el campo personalizado de la forma DHTMLX, sin embargo como dicho objeto se
					//utilizó en el Plug-in del editor HTML, no hay una referencia directa a él así que se debe obtener mediante un selector
					return $(item).find("textarea").val();
					//return item.HTMLcodeCache;
					//return item._value;
				}, //specifies the return value of the item
				enable: function(item){
					//item.lastChild.style.color = "black";
					item._enabled = true;
				}, // enables the item
				disable: function(item){
					//item.lastChild.style.color = "gray";
					item._enabled = false;
				} //disables the item
			}
			
			/* In order to validation can be applied to the item and its value could be set(gotten) by the methods setFormData(), getFormData() you should specify the following related methods */
			dhtmlXForm.prototype.getFormData_editorHTMLForm = function(name){
				return this.doWithItem(name, "getValue");
			};
			dhtmlXForm.prototype.setFormData_editorHTMLForm = function(name,value){
				return this.doWithItem(name, "setValue", value);
			};
			
			function eXcell_editorHTMLDialog(cell) {
				if (cell) {
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function() {
					//OMMC 2016-04-11: Agregada la validación para que al hacer click en elementos <a> no se ejecute el default y entre a edición.
					if($(event.target).is("a")){
						event.preventDefault();
					}
					this.val = this.getValue();
					var strRowId = this.cell.parentNode.idd;
					this.cell.innerHTML = "<img src='images/admin/editorhtml.gif' style='width:18px; height:18px; float:left; cursor:pointer;'>"
						+ "<input autocomplete='off' class='dhx_combo_edit' style='height: 28px;'>";
					//alert(this.cell.innerHTML);
					this.cell.childNodes[1].value = this.val;
					var objCell = this.cell;
					//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
					/*
					var intDataSourceID = 0;
					var intObjectType = this.grid.bitObjectType;
					var intObjectID = this.grid.bitObjectID;
					var objObject = getObject(intObjectType, intObjectID);
					
					if (objObject && objObject.dataSourceID && (
							(objObject.objectType == AdminObjectType.otyQuestion && objObject.type == Type.simplechoice && objObject.displayMode == DisplayMode.dspGetData) || 
							(objObject.objectType == AdminObjectType.otySection && objObject.valuesSourceType == TypeOfFill.tofCatalog)
							)) {
						intDataSourceID = objObject.dataSourceID;
					}
					*/
					//JAPR
					
					this.cell.childNodes[0].onclick = function(e) {
						var aElement = getObject(objCell.parentNode.grid.bitObjectType, objCell.parentNode.grid.bitObjectID);
						//JAPR 2015-08-02: Agregado el parámetro iDataSourceID para indicar al editor de fórmulas que incluya los atributos de dicho catálogo como variables
						//fnOpenFormulaEditor(objCell, objCell.childNodes[1].value, intDataSourceID);
						fnOpenEditorHTML(objCell, objCell.childNodes[1].value, aElement.typeExt);
						(e||event).cancelBubble = true;	//blocks onclick event
					}
					//Agrega el evento blur para actualizar el valor al perder el foco en el input
					//OMMC 2015-10-05: Se agrega validación para prevenir que pregunta HTML quede vacía.
					var objGrid = this.grid;
					var blnRequired = false;
					var intObjectType = objGrid.bitObjectType;
					var tabName = objGrid.getRowAttribute(strRowId, "tabName");
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);					
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					if (objField) {
						blnRequired = objField.required;
					}
					//Prepara el evento para recibir el <enter>, ya que internamente la celda manda al mismo evento editStop ya sea con <enter> (aceptar la captura) o <escape> (cancelar
					//la captura) y no da indicios de por cual de los dos exactamente se lanzó, así que se invocará al detach de la celda y ambos cancelarían el onblur, por lo que hay que
					//detectar desde el propio input cual de las teclas provocó el editStop y prevenir que en caso de <enter> se descarte el contenido
					//OMMC 2015-10-07 Cambio del del evento del <enter> ahora si se presiona en la celda del grid y el valor está vació se llenará con un valor default.
					this.cell.childNodes[1].onkeydown = function(e) {
						if (e && e.keyCode == 13) {
						if($.trim(this.value) == '' && blnRequired == true){
								var strContent = this.value;
								strContent = "<?=translate("You must enter a value for this field")?>";
								this.value = strContent;
								alert(strContent);
								$(this).focus();
								return;
							}
							//this.bitPreventDisableBlur = true;
						}
					}
					this.cell.childNodes[1].onblur = function(e) {
						//El evento se controlará con este atributo, ya que la manera en que funciona el Grid con una celda editable (como este componente) es que lanza el evento editStop
						//el cual invoca a la función detach y ella sobreescribe la celda provocando el onBlur, para posteriormente hacer el setValue con el valor anterior a lo que se
						//estaba editando, pero para ese punto ya el onblur habría enviado el valor cancelado al server para grabar y visualmente se habría quedado el valor original,
						//así que el evento detach va a generar el atributo de esta validación en true para evitar el onblur en ese punto
						if (!this.bitDisableBlur || this.bitPreventDisableBlur) {
							//JAPR 2015-09-01: Validado que se capture algo en el editor antes de perder el foco si el campo está marcado como requerido
							var strContent = this.value;
							//OMMC 2015-10-02: Validado para que los encabezados y pies de sección puedan ser nulos y no marque error (ISSUE: FZUWI5)
							if ($.trim(strContent) == '' && blnRequired == true) {
								var strContent = "<?=translate("You must enter a value for this field")?>";
								this.value = strContent;
								$(this).focus();
								alert(strContent);
								return;
							}
							//JAPR
							
							doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, this.value);
						}
					}
					this.cell.childNodes[1].onclick = function(e) {(e||event).cancelBubble = true;} //blocks onclick event
				}
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("edFormula.setValue " + val);
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					return this.cell.innerHTML;
				}
				this.detach = function() {
					if (this.cell && this.cell.childNodes && this.cell.childNodes[1]) {
						this.cell.childNodes[1].bitDisableBlur = true;
					}
					
					this.setValue(this.cell.childNodes[1].value); //sets the new value
					return this.val != this.getValue(); // compares the new and the old values
				}
			}
			eXcell_editorHTMLDialog.prototype = new eXcell;    // nests all other methods from base class
			
			
			<?
			$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
			$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			?>
			//Permite definir celdas para poder subir imágenes al servidor
			function eXcell_uploadImg(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.edit = function(){}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("uploadImg.setValue " + val);
					
					var strRowId = this.cell.parentNode.idd;
					//JAPR 2015-07-31: Corregido un bug, la propiedad displayImage causaba confusión entre rows de la colección y con la del question, así que se
					//lanzaba una forma equivocada, además al actualizar el IMG com había varios repetidos, cambiaba todos independientemente de cual se hubiera
					//configurado, ahora complementará el ID con el OptionID si es un Child para hacer diferencia
					var intObjectType = this.grid.getRowAttribute(strRowId, "objectType");
					var intObjectID = this.grid.bitObjectID;
					var intChildObjectType = this.grid.bitChildObjectType;
					var intChildObjectID = this.grid.bitChildObjectID;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var strObjectRowID = "_otp" + intObjectType + "oid" + intObjectID;
					var strSubObjRowID = "";
					if (intChildObjectType) {
						strSubObjRowID = "_ctp" + intChildObjectType + "cid" + intChildObjectID;
					}
					var strId = "cellUploadImg_"+tabName+"_"+strRowId+strObjectRowID+strSubObjRowID+'_'+this.cell.cellIndex;
					//JAPR
					if (val!="" && val!=undefined) {
						srcImg='<?=$pathImg?>'+val;
					}
					else {
						srcImg='<?=$pathImg?>images/admin/emptyimg.png';
					}
					var blnForForm = (intObjectType == AdminObjectType.otySurvey);
					var strColTabName = this.grid.bitCollectionTabName;
					var objField = getFieldDefinition(tabName, strRowId,blnForForm);
					
					if(objField)
					{
						parentTypeVal=objField.parentType;
					}
					else
					{
						parentTypeVal="-1";
					}
					var strPopupTimeID="";
					strFieldName="";
					var strDivEditorID="";
					styleDeleteImg='style="display:inline"';
					if(parentTypeVal=="-1")
					{
						//Entra aquí cuando el componente de imagen se está utilizando en un editor
						strPopupTimeID="&strPopupTimeID="+timePopup;
						tabName=this.grid.bitTabName;
						strFieldName="&fieldName="+this.grid.bitFieldName;
						strDivEditorID="&bitEditorDivID="+this.grid.bitEditorDivID;
						styleDeleteImg='style="display:none"';
					}
					
					var strDisabledStyle = "";
					var strDisabledStyleAttr = "";
					<?
					//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if ($this->FormType == formProdWithDev) {?>
					if (objField.disabled) {
						styleDeleteImg='style="display:none"';
						strDisabledStyle = ' style="display:none"';
						strDisabledStyleAttr = 'display:none;';
					}
					<?}
					//@JAPR
					?>
					
					var fieldHTML='<div id="divFile'+strId+'" style="border:none">'+
						'<form name="frmUploadFile'+strId+'" method="POST" action="uploadUserImage.php?objectType='+parentTypeVal+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+strPopupTimeID+strFieldName+strDivEditorID+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'<input type="file" id="file'+strId+'" name="file'+strId+'" value="" style="opacity:0;width:0px;height:0px;'+strDisabledStyleAttr+'" onchange="document.getElementById(\'txtsubmit'+strId+'\').click();">'+
						'<button type="button" onclick="document.getElementById(\'file'+strId+'\').click();"'+strDisabledStyle+'><?=translate("Select file")?></button>'+
						'<input type="submit" id ="txtsubmit'+strId+'" name="txtsubmit'+strId+'" value="<?=translate("Upload file")?>" style="display: none; visibility: hidden;">'+'<img name="deleteImg" id="deleteImg" src="images/admin/delete.png" '+ styleDeleteImg + ' onClick=\'removeImg("'+strId+'")\'>'+
						'<div>'+
						'<img name ="img'+strId+'" id="img'+strId+'" src="'+srcImg+'" style="display:inline;vertical-align:top;max-height:50px;">'+
						'<img name ="emptyimg'+strId+'" id="img'+strId+'" src="<?=$pathImg?>images/admin/emptyimg.png" style="display:inline;vertical-align:top;height:50px;">'+
						'</div>'+
						'</form>'+
						'</div>'+
						'<form id="frmDeleteFile'+strId+'" name="frmDeleteFile'+strId+'" method="POST" action="uploadUserImage.php?action=delete&objectType='+parentTypeVal+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
						'</form>'
					this.setCValue(fieldHTML);
					
					var objGrid = this.grid;
				}
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);
					var strValue = "";
					if (objField)
					{
						strValue = objField.getValue();
					}
					return strValue;
				}
			}
			function removeImg(strId)
			{
				//Forma para borrar imagen
				document.forms['frmDeleteFile'+strId].submit();
			}
			eXcell_uploadImg.prototype = new eXcell;// nests all other methods from the base class
			
			//JAPR 2015-07-15: Agregado el componente con la lista seleccionable ordenada
			/* Componente con dos listas, una de opciones disponibles y otra de opciones seleccionadas, esta última permite definir un orden en los elementos seleccionados mediante
			Drag & Drop de las opciones
			*/
			function eXcell_selList(cell) { //the eXcell name is defined here
				if (cell) {                // the default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.detach = function() {
					//this.setCValue("");
				}
				this.edit = function() {}  //read-only cell doesn't have edit method
				this.isDisabled = function(){ return false; } // the cell is read-only, so it's always in the disabled state
				this.setValue = function(val) {
					if (val === null) {
						return;
					}
					
					console.log ("selList.setValue " + val);
					
					var strRowId = this.cell.parentNode.idd;
					var strId = "cellSelList_"+strRowId+'_'+this.cell.cellIndex;
					this.setCValue("<div id='"+strId+"' style='height:220px;width:100%;overflow:visible;'>"+val+"</div>",val);
					var tabName = this.grid.getRowAttribute(strRowId, "tabName");
					var objField = getFieldDefinition(tabName, strRowId);					
					var objGrid = this.grid;
					
					var objFormData = [
						{type:"container", name:strId+"grdAvail", inputWidth:160, inputHeight:200, className:"selection_container"},
						{type:"newcolumn", offset:10},
						{type:"block", blockOffset:0, offsetTop:20, list:[
							{type:"button", name:strId+"btnAssoc", value:"", className:"assoc_button"},
							{type:"button", name:strId+"btnRemove", value:"", className:"remove_button"}
						]},
						{type:"newcolumn", offset:10},
						{type:"container", name:strId+"grdAssoc", inputWidth:160, inputHeight:200, className:"selection_container"},
						{type:"input", name:"SurveyID", value:intSurveyID, hidden:true}
					];
					
					objSelForm = new dhtmlXForm(strId, objFormData);
					$(objSelForm.cont).addClass('customGrid')
					
					//Contenido de la lista de usuarios disponibles
					var objAvailGrid = new dhtmlXGridObject(objSelForm.getContainer(strId+"grdAvail"));
					//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
					//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#TXI6BQ)
					objAvailGrid.dontSetSizes = true
					//JAPR
					objAvailGrid.setImagePath("<?=$strScriptPath?>/images/");
					objAvailGrid.setHeader("<?=translate("Available")?>");
					objAvailGrid.setInitWidthsP("100");
					objAvailGrid.setColAlign("left");
					objAvailGrid.setColTypes("ro");
					objAvailGrid.enableEditEvents(false);
					<?//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					//Sólo se habilitarán los eventos si el campo no se encuentra deshabilitado, adicionalmente si lo está, ocultará los botones para cambiar de lista los elementos?>
					if (!objField.disabled) {
						objAvailGrid.enableDragAndDrop(true);
						objAvailGrid.setColSorting("str");
					}
					<?//@JAPR?>
					objAvailGrid.enableMultiselect(true);
					objAvailGrid.init();
					objAvailGrid.sortRows(0,"str","asc");
					
					//Contenido de la lista de usuarios invitados
					var objAssocGrid = new dhtmlXGridObject(objSelForm.getContainer(strId+"grdAssoc"));
					//JAPR 2016-08-09: Corregido un bug, al escribir en el texto del filtro e invocar a la función del grid filterBy, se estaba haciendo un resize, así que se identificó
					//que con esta propiedad se evita que se recalcule el tamaño del grid en dicha función (#TXI6BQ)
					objAssocGrid.dontSetSizes = true
					//JAPR
					objAssocGrid.setImagePath("<?=$strScriptPath?>/images/");
					objAssocGrid.setHeader("<?=translate("Used")?>");
					objAssocGrid.setInitWidthsP("100");
					objAssocGrid.setColAlign("left");
					objAssocGrid.setColTypes("ro");
					objAssocGrid.enableEditEvents(false);
					<?//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					//Sólo se habilitarán los eventos si el campo no se encuentra deshabilitado, adicionalmente si lo está, ocultará los botones para cambiar de lista los elementos?>
					if (!objField.disabled) {
						objAssocGrid.enableDragAndDrop(true);
						objAssocGrid.setColSorting("str");
					}
					<?//@JAPR?>
					objAssocGrid.enableMultiselect(true);
					objAssocGrid.init();
					objAssocGrid.sortRows(0,"str","asc");
					
					//Llena la lista con las opciones disponibles (options) y las seleccionadas (value)
					var blnUseOrder = true;
					var cmbOptions = new Object();
					var cmbOptionsOrder = new Array();
					var arrSelected = new Array();
					if (objField && objField.getOptions) {
						var cmbOptions = objField.getOptions();
						var cmbOptionsOrder = objField._optionsOrder;
						var blnUseOrder = objField.useOrder;
						arrSelected = objField.getValue();
					}
					
					//Primero llena la lista de opciones disponibles
					for (var intIdx in cmbOptionsOrder) {
						var idOption = (blnUseOrder)?cmbOptionsOrder[intIdx]:intIdx;
						//Debe excluir a las que ya se encuentren seleccionadas
						if (arrSelected.indexOf(idOption) == -1) {
							var strName = cmbOptions[idOption].name;
							if (strName === undefined) {
								strName = cmbOptions[idOption];
							}
							
							var strItemId = "i" + idOption;
							objAvailGrid.addRow(strItemId, strName);
							objAvailGrid.setUserData(strItemId, "id", idOption);
						}
					}
					
					//Posteriormente llena las opciones seleccionadas, las cuales deben respetar el orden de selección del usuario
					//Se valida en caso de que los options no contengan a todos los elementos seleccionados, lo cual puede suceder si se cambia al padre teniendo ya configurado todo
					if ($.isArray(arrSelected)) {
						for (var intIdx in arrSelected) {
							var idOption = arrSelected[intIdx];
							if (!idOption) {
								continue;
							}
							
							if (!cmbOptions[idOption]) {
								continue;
							}
							
							var strName = cmbOptions[idOption].name;
							if (strName === undefined) {
								strName = cmbOptions[idOption];
							}
							
							var strItemId = "i" + idOption;
							objAssocGrid.addRow(strItemId, strName);
							objAssocGrid.setUserData(strItemId, "id", idOption);
						}
					}
					
					<?//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					//Sólo se habilitarán los eventos si el campo no se encuentra deshabilitado, adicionalmente si lo está, ocultará los botones para cambiar de lista los elementos?>
					if (objField.disabled) {
						objSelForm.hideItem(strId+"btnAssoc");
						objSelForm.hideItem(strId+"btnRemove");
					}
					else {
						objAvailGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
							console.log('objAvailGrid.onDrop Property');
							
							if (sObj == tObj) {
								return;
							}
							
							var arrIDs = doGetGridSelectedIDs(objAssocGrid);
							doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
						});
						
						objAvailGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
							this.moveRow(rId, "row_sibling", undefined, objAssocGrid);
							
							var arrIDs = doGetGridSelectedIDs(objAssocGrid);
							doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
						});
						
						//JAPR 2016-07-25: Corregido un bug, cuando se hacía click a ciertas parte de la lista, lanzaba el click de la última celda seleccionada la cual podría ser otra row
						//que no era la de esta lista, así que realizaba acciones indebidas, ahora se forzará a seleccionar la row de esta lista (#070KU7)
						objAvailGrid.attachEvent("onHeaderClick", function(ind,obj) {
							if (objGrid && strRowId) {
								var intRowId = objGrid.getRowIndex(strRowId);
								if ($.isNumeric(intRowId)) {
									objGrid.selectRow(intRowId);
								}
							}
							
							return true;
						});
						
						objAvailGrid.attachEvent("onBeforeSelect", function(ind,obj) {
							if (objGrid && strRowId) {
								var intRowId = objGrid.getRowIndex(strRowId);
								if ($.isNumeric(intRowId)) {
									objGrid.selectRow(intRowId);
								}
							}
							
							return true;
						});
						//JAPR
						
						objAssocGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
							console.log('objAssocGrid.onDrop Property');
							
							//JAPR 2015-07-18: Corregido un bug, el Drop en el mismo grid de asociación si se permite para cambiar el orden de los elementos
							//if (sObj == tObj) {
							//	return;
							//}
							
							var arrIDs = doGetGridSelectedIDs(this);
							doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
						});
						
						objAssocGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
							this.moveRow(rId, "row_sibling", undefined, objAvailGrid);
							
							var arrIDs = doGetGridSelectedIDs(this);
							doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
						});
						
						//JAPR 2016-07-25: Corregido un bug, cuando se hacía click a ciertas parte de la lista, lanzaba el click de la última celda seleccionada la cual podría ser otra row
						//que no era la de esta lista, así que realizaba acciones indebidas, ahora se forzará a seleccionar la row de esta lista (#070KU7)
						objAssocGrid.attachEvent("onHeaderClick", function(ind,obj) {
							if (objGrid && strRowId) {
								var intRowId = objGrid.getRowIndex(strRowId);
								if ($.isNumeric(intRowId)) {
									objGrid.selectRow(intRowId);
								}
							}
							
							return true;
						});
						
						objAssocGrid.attachEvent("onBeforeSelect", function(ind,obj) {
							if (objGrid && strRowId) {
								var intRowId = objGrid.getRowIndex(strRowId);
								if ($.isNumeric(intRowId)) {
									objGrid.selectRow(intRowId);
								}
							}
							
							return true;
						});
						//JAPR
						
						objSelForm.attachEvent("onButtonClick", function(name) {
							switch (name) {
								case strId+"btnAssoc":
									var strSelection = objAvailGrid.getSelectedRowId();
									if (strSelection) {
										var arrSelection = strSelection.split(",");
										if (arrSelection.length) {
											var blnMove = false;
											for (var intIdx in arrSelection) {
												var strItemId = arrSelection[intIdx];
												if (strItemId) {
													blnMove = true;
													objAvailGrid.moveRow(strItemId, "row_sibling", undefined, objAssocGrid);
												}
											}
											
											if (blnMove) {
												var arrIDs = doGetGridSelectedIDs(objAssocGrid);
												doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
											}
										}
									}
									break;
								
								case strId+"btnRemove":
									var strSelection = objAssocGrid.getSelectedRowId();
									if (strSelection) {
										var arrSelection = strSelection.split(",");
										if (arrSelection.length) {
											var blnMove = false;
											for (var intIdx in arrSelection) {
												var strItemId = arrSelection[intIdx];
												if (strItemId) {
													blnMove = true;
													objAssocGrid.moveRow(strItemId, "row_sibling", undefined, objAvailGrid);
												}
											}
											
											if (blnMove) {
												var arrIDs = doGetGridSelectedIDs(objAssocGrid);
												doUpdateProperty(objGrid, undefined, undefined, strRowId, 1, arrIDs);
											}
										}
									}
									break;
							}
						});
					}
					<?//@JAPR?>
					
					if (this.grid) {
						this.grid.setUserData(strRowId, "selGrid", objAssocGrid);
					}
				}
				
				this.getValue = function() {
					var strRowId = this.cell.parentNode.idd;
					var strValue = "";
					if (this.grid) {
						var oGrid = this.grid.getUserData(strRowId, "selGrid");
						if (oGrid) {
							var tabName = this.grid.getRowAttribute(strRowId, "tabName");
							var objField = getFieldDefinition(tabName, strRowId);					
							if (objField) {
								var intRows = oGrid.getRowsNum();
								var blnUseOrder = true;
								var cmbOptions = new Object();
								var cmbOptionsOrder = new Array();
								if (objField && objField.getOptions) {
									var cmbOptions = objField.getOptions();
									var cmbOptionsOrder = objField._optionsOrder;
									var blnUseOrder = objField.useOrder;
								}
								
								strAnd = "";
								for (var intIndex = 0; intIndex < intRows; intIndex++) {
									var strRowId = oGrid.getRowId(intIndex);
									if (strRowId) {
										var intObjectID = oGrid.getUserData(strRowId, "id");
										if (intObjectID && cmbOptions[intObjectID]) {
											var strName = cmbOptions[intObjectID].name;
											if (strName === undefined) {
												strName = cmbOptions[intObjectID];
											}
											
											strValue += strAnd + strName;
											strAnd = ", ";
										}
									}
								}
							}
						}
					}
					
					return strValue;
				}
			}
			eXcell_selList.prototype = new eXcell;// nests all other methods from the base class
			
			//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG			
			var errorMsg;
			errorMsg="";
			var blnError;
			blnError=false;
			
			function setUserImageForEditor(strId, tabName, pathImg, strRowId, iObjectType, iObjectID, iChildObjectType, iChildObjectID, sCollectionName, popupTimeID, fieldName, bitEditorDivID)
			{
			   <?
				$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
				$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			   ?>
			   
				//JAPR 2015-07-22: Validado el caso donde se reciba un mensaje de error
				if (blnError) {
					if(errorMsg!="")
					{
						alert(errorMsg);
						errorMsg="";
					}
					else
					{
						alert("<?=translate("There was an error uploading the image file, please contact Technical Support")?>: \n" + pathImg);
					}
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
					blnError=false;
					return;
				}
				//JAPR
				if (pathImg != "")
				{
				   //Ticket #IYR03T: Se agregó un parámetro extra de fecha para que no tome la imagen de caché
					//ya que si se subía una imagen con el mismo nombre de la anterior pero diferente contenido no la actualizaba
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>'+pathImg + "?dte=" + getFullDate(undefined, true));
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
				}
				else
				{
					//Se pone una imagen con fondo transparente cuando no hay una imagen configurada
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>images/admin/emptyimg.png');
				}
				
				var objImgPopUp = objArrPopUpsByTime[popupTimeID];
				if (objImgPopUp) {
					objImgPopUp.hide();	
				}
				delete objArrPopUpsByTime[popupTimeID];
				
				if (!bitEditorDivID) {
					return;
				}
				
				var iFrameEditor = $('#'+bitEditorDivID).find('iframe');
				var objImg = document.createElement("IMG");
				//Ticket #IYR03T: Se agregó un parámetro extra de fecha para que no tome la imagen de caché
				//ya que si se subía una imagen con el mismo nombre de la anterior pero diferente contenido no la actualizaba
				objImg.src = '<?=$pathImg?>'+pathImg + "?dte=" + getFullDate(undefined, true);
				
				//JAPR 2015-08-31: A este ID se le debe concatenar un valor que lo haga único
				//var dteNow = new Date();
				//var strDateID = dteNow.getFullYear() + (dteNow.getMonth()+1) + dteNow.getDate() + dteNow.getHours() + dteNow.getMinutes() + dteNow.getSeconds();
				//De hecho, no requiere ID, pero si lo requiriera entonces sería como arriba
				//objImg.id = strId + strDateID;
				//JAPR 2015-08-31: Para permitir que el evento permanezca entre sesiones, no se puede asignar una función sino directo en el HTML
				//objImg.onclick = new Function("setImageSize(this);");
				//objImg.onclick = "setImageSize(this);"
				iFrameEditor[0].contentWindow.document.body.appendChild(objImg);
				//$(objImg)[0].outerHTML.replace('src=', 'onclick="parent.setImageSize(this);" src=');
				if ($(objImg)[0] && $(objImg)[0].outerHTML) {
					//JAPR 2015-09-01: Validado que si no se recibió el tabName (esto sería cuando se está configurando una nueva pregunta, ya que ahí no se encuentra en el contexto de
					//un grid de propiedades) en lugar de 'undefined' genere un tabName que no exista para que no permita el grabado, utilizando una cadena que será reemplazada al grabar la
					//forma y que sea poco probable que un usuario utilice
					if (tabName === undefined || tabName == 'undefined') {
						tabName = 'bitamundefinedbit';
					}
					
					//$(objImg)[0].outerHTML = $(objImg)[0].outerHTML.replace('src=', 'oncontextmenu="var objEvent = (arguments[0] || event); if (objEvent) {objEvent.preventDefault();objEvent.stopPropagation();} return false;" onmouseup="var objEvent = (arguments[0] || event); if (!objEvent || objEvent.which != 3) {return;}; parent.setImageSize(this); objEvent.cancelBubble=true;" src=');
					$(objImg)[0].outerHTML = $(objImg)[0].outerHTML.replace('src=', 'ondblclick="if (parent.setImageSize) {parent.setImageSize(this, \''+tabName+'\', \''+strRowId+'\');}" src=');
				}
			}
			
			/* Permite hacer un resize del Tag Image indicado como parámetro asignándole el style width y height mediante JQuery */
			function setImageSize(oImgObj, tabName, strRowId) {
				if (!objWindowsImg) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindowsImg = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialogImg = objWindowsImg.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialogImg.setText("<?=translate("Size")?>");
				objDialogImg.denyPark();
				objDialogImg.denyResize();
				objDialogImg.setIconCss("without_icon");
				
				objDialogImg.attachEvent("onClose", function() {
					return true;
				});
				
				//JAPR 2015-08-31: Asignadas las propiedades de la imagen
				var strWidth = $(oImgObj).css('width');
				var strHeight = $(oImgObj).css('height');
				//JAPR
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"input", name:"width",  label:"<?=translate("Width")?>",  labelAlign:"left", value:strWidth, labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
					{type:"input", name:"height", label:"<?=translate("Height")?>", labelAlign:"left", value:strHeight, labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
					{type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]}
				];
				
				var objForm = objDialogImg.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("width");
				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialogImg();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									resizeImg(oImgObj, tabName, strRowId);
								}
							}, 100);
							break;
					}
				});
			}

			function resizeImg(oImgObj, tabName, strRowId) {
				console.log('resizeImg tabName == ' + tabName + ', strRowId == ' + strRowId);
				
				if (!objDialogImg) {
					return;
				}
				
				objDialogImg.progressOn();
				var objForm = objDialogImg.getAttachedObject();
				var objData = objForm.getFormData();
				objForm.lock();
				
				//JAPR 2015-08-31: Asignadas las propiedades de la imagen
				//$(oImgObj).attr('width', objData.width);
				//$(oImgObj).attr('height', objData.height);
				$(oImgObj).css('width', objData.width);
				$(oImgObj).css('height', objData.height);
				
				//Debido a la manera en que se graban los datos en las formas de creación de objetos, la cual es diferente al grabado en los grids de propiedades, según quien agregó
				//la imagen traerá un nombre de propiedad u otro, así que se hace el ajuste de las propiedades conocidas que usarían los componentes con este caso para ajustarlos a como
				//se esperan en el grid, ya que es el único lugar que grabaría con esta función
				switch (strRowId) {
					case "QuestionMessage":
						strRowId = "formattedTextDes";
						break;
				}
				
				//Obtiene la referencia al editor del campo que se está editando para obtener el contenido actualizado, asímismo se obtiene la referencia al grid para invocar al grabado
				var objGrid = undefined;
				if (objPropsTabs && objPropsTabs.tabs && objPropsTabs.tabs(tabName) && objPropsTabs.tabs(tabName).getAttachedObject) {
					var objGrid = objPropsTabs.tabs(tabName).getAttachedObject();
				}
				var myEditor = undefined;
				if (objGrid && objGrid.getUserData) {
					myEditor = objGrid.getUserData(strRowId, "editor");
				}
				var strContent = undefined;
				if (myEditor && myEditor.getContent) {
					strContent = myEditor.getContent();
				}
				if (strContent !== undefined) {
					//Actualiza el valor de este campo
					doUpdateProperty(objGrid, objGrid.bitObjectType, objGrid.bitObjectID, strRowId, 1, strContent);
				}
				
				//JAPR
				objDialogImg.progressOff();
				objForm.unlock();
				doUnloadDialogImg();
			}
			
			//JAPR 2015-07-19: Agregados los parámetros iObjectID, iChildObjectType, iChildObjectID, sCollectionName para identificar el elemento hijo si se tratara de un subgrid,
			//ya que en las opciones de respuesta el tabName representa un detalle pero no directo de objPropsTabs, así que se debe navegar un nivel mas abajo para obtener la referencia
			//al grid correcto, además tabName representa a un tab oculto que sólo se usa como definición de tantos tabs detalle como rows tenga el sCollectionName, que es desde donde
			//se obtendrá la verdadera referencia al SubGrid donde se modificó la propiedad. Esto es así exclusivamente cuando hay subGrids de collecciones (hasta un nivel) y porque este
			//componente tiene que invocar a Javascript desde una respuesta del server, si hubiera podido invocarlo en el cliente directamente, se hubiera pasado la referencia al grid
			//en lugra de hacer todo esto
			//En otras propiedades que son directas de un grid que no es de colección, basta con enviar los parámetros hasta iObjectType y el grid recuperado será el adecuado
			
			//Corrección Ticket #QND4OY: Sólo se aceptan imagenes JPG, GIF y PNG			
			function setUserImageForObject(strId, tabName, pathImg, strRowId, iObjectType, iObjectID, iChildObjectType, iChildObjectID, sCollectionName)
			{
			   <?
				$pos = strpos($_SESSION["PAHTTP_REFERER"],"build_page.php");
				$pathImg = substr($_SESSION["PAHTTP_REFERER"],0,$pos);
			   ?>
			   
				//JAPR 2015-07-22: Validado el caso donde se reciba un mensaje de error
				if (blnError) {
					if(errorMsg!="")
					{
						alert(errorMsg);
						errorMsg="";
					}
					else
					{
						alert("<?=translate("There was an error uploading the image file, please contact Technical Support")?>: \n" + pathImg);
					}
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
					blnError=false;
					return;
				}
				//JAPR
				if(pathImg!="")
				{
				   //Ticket #IYR03T: Se agregó un parámetro extra de fecha para que no tome la imagen de caché
					//ya que si se subía una imagen con el mismo nombre de la anterior pero diferente contenido no la actualizaba
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>'+pathImg + "?dte=" + getFullDate(undefined, true));
					//OMMC 2015-11-23: Limpiar el valor del input file ya que prevenía que se subiera la misma imagen 2 veces de manera sucesiva.
					//O que si se borraba la imagen y se deseaba subir una con el mismo nombre, no se podía.
					if($('#file'+strId).val() != ""){
						$('#file'+strId).val("");
					}
				}
				else
				{
					//Se pone una imagen con fondo transparente cuando no hay una imagen configurada
					$('img[name="img' + strId + '"]').attr('src', '<?=$pathImg?>images/admin/emptyimg.png');
				}
				propName=strRowId;
				if(iObjectType==AdminObjectType.otySurvey)
				{
					var objGrid = objFormTabs.tabs(tabDetail).getAttachedObject().tabs(tabName).getAttachedObject();
				}
				else
				{
					var objGrid = objPropsTabs.tabs(tabName).getAttachedObject();
					//Si se recibieron los parámetros de sCollectionName y los demás, quiere decir que esta propiedad es de un objeto detalle dentro de una colección, así que el grid
					//a modificar es el de dicho detalle, porque tabName representa sólo un grid dummy para generar a los demás (se asumirá por ahora que el subgrid nace a partir de la
					//columna 0)
					try {
						var objTab = objPropsTabs.tabs(sCollectionName);
						if (objTab) {
							var objCollGrid = objTab.getAttachedObject();
							if (objCollGrid && iChildObjectType && iChildObjectID) {
								if (objCollGrid.cells(iChildObjectID, 0)) {
									var objSubGrid = objCollGrid.cells(iChildObjectID, 0).getSubGrid();
									if (objSubGrid) {
										var objGrid = objSubGrid;
									}
								}
							}
						}
					} catch (e) {
						debugger;
					}
				}
				
				var blnForForm = (iObjectType == AdminObjectType.otySurvey);
				var objField = getFieldDefinition(tabName, propName,blnForForm);
				if (objField) {
					objField.setValue(null, null, pathImg);
					doUpdateProperty(objGrid, objField.parentType, objField.id, strRowId, 1, pathImg);
				}
			}
			
			/* Permite capturar una hora */
			function eXcell_time(cell){                        //excell name is defined here
				if (cell) {                                       //default pattern, just copy it
					this.cell = cell;
					this.grid = this.cell.parentNode.grid;
				}
				this.setValue = function(val) {
					this.setCValue(val);                                     
				}
				this.getValue = function() {
					return this.cell.innerHTML; // get value
				}
				this.edit = function() {
					this.val = this.getValue(); //save current value
					this.cell.innerHTML = "<input type='text' style='width:50px;'>"
					+ "<select style='width:50px;'><option value='AM'>AM"
					+ "<option value='PM'>PM</select>"; // editor's html
					this.cell.firstChild.value=parseInt(this.val); //set the first part of data
					if (this.val.indexOf("PM")!=-1) this.cell.childNodes[1].value="PM";
					  this.cell.childNodes[0].onclick=function(e){ (e||event).cancelBubble=true;} //blocks onclick event
					  this.cell.childNodes[1].onclick=function(e){ (e||event).cancelBubble=true;} //blocks onclick event
				}
				this.detach = function() {
					  this.setValue(this.cell.childNodes[0].value+" "+this.cell.childNodes[1].value); //sets the new value
					  return this.val!=this.getValue(); // compares the new and the old values
				}
			}
			eXcell_time.prototype = new eXcell;    // nests all other methods from base class
			
			/* Muestra la ventana de propiedades de la sección especificada, la cual debe estar previamente creada, en caso de no existir simplemente
			se generará la ventana de pregunta vacia para crear una nueva
			*/
			function doShowSectionProps(sNodeId) {
				console.log('doShowSectionProps ' + sNodeId);
				
				var intSectionID = sNodeId.substr(1);
				//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
				doRemovePopUps(objPropsLayout.cells(dhxPropsDetailCell));
				//JAPR
				objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
				//RV-Mar2015: Cuando se da click en un nodo de secciones en el árbol, aparte de mostrar las propiedades de las secciones
				//también se debe mostrar en el preview la sección seleccionada
				doSelectSection(intSectionID);
				generateSectionProps(sNodeId);
			}
			
			/* Muestra la ventana de propiedades de la pregunta especificada, la cual debe estar previamente creada, en caso de no existir simplemente
			se generará la ventana de pregunta vacia para crear una nueva
			*/
			function doShowQuestionProps(sNodeId) {
				console.log('doShowQuestionProps ' + sNodeId);
				
				if (!selSurvey) {
					return;
				}
				
				//Identifica el ID ya sea por medio del ID del árbol (que es el mismo del request) o mediante el ID numérico directo usando la instancia de la forma
				if (typeof sNodeId == "string") {
					var intQuestionID = sNodeId.substr(1);
					var objTree = objTreeLayout.cells(dhxTreeCell).getAttachedObject();
					var strSurveyQuestion = objTree.getParentId(sNodeId);
					var strSectionID = objTree.getParentId(strSurveyQuestion);
					var intSectionID = strSectionID.substr(1);
				}
				else {
					var intQuestionID = parseInt(sNodeId);
					var objQuestion = selSurvey.questions[intQuestionID];
					if (!objQuestion) {
						return;
					}
					var intSectionID = objQuestion.sectionID;
				}
				
				giQuestionID = intQuestionID; 
				
				//RV-Mar2015: Cuando se da click en un nodo de pregunta en el árbol, aparte de mostrar las propiedades de la pregunta
				//también se debe mostrar en el preview la sección correspondiente a la pregunta
				doSelectSection(intSectionID);
				generateQuestionProps(sNodeId);
			}
			
			/* Hace el cambio de sección en la vista previa, además de seleccionar las propiedades de la sección en el frame de propiedades, expandiendo las 
			preguntas de la sección en el frame de navegación
			*/
			//RV-Mar2015: Los parámetros ev, html ya no se utilizan por que ahora esta función no se manda llamar directamente desde un evento
			//si no dentro de la función doShowSectionProps que es la que está asociada al nodo de secciones en el árbol
			//JAPR 2015-07-31: Agregado el parámetro bChangeQuestionFocus para indicar cuando si se quiere modificar el foco de la pregunta, ya que en los clicks a la propia pregunta
			//desde el preview no es necesario hacer esto pues provocaría un efecto extraño al mover ligeramente el top de la página justo al inicio de la pregunta incluso si según el
			//scroll actual se veía abajo, así que eso sólo se requiere cuando se está haciendo un refresh
			function doSelectSection(id, bSelectFirst, bChangeQuestionFocus/*, ev, html*/) {
				console.log('doSelectSection ' + id + ', bSelectFirst == ' + bSelectFirst);
				
				if (!id || id <= 0) {
					//Si se utilizó el parámetro para auto-seleccionar la primer sección disponible, mueve el apuntador hacia la primera en el orden definido
					if (bSelectFirst && selSurvey && selSurvey.sectionsOrder && selSurvey.sections) {
						if (giSectionID) {
							//Si ya había una sección seleccionada entonces carga esa sección directamente
							id = giSectionID;
						}
						else {
							for (var intSectionNum in selSurvey.sectionsOrder) {
								var intSectionID = selSurvey.sectionsOrder[intSectionNum];
								var objSection = selSurvey.sections[intSectionID];
								if (objSection) {
									if (!giSectionID) {
										giSectionID = intSectionID;
										id = giSectionID;
										break;
									}
								}
							}
						}
					}
					else {
						return;
					}
				}
				
				var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
				try {
					if (!objiFrame.contentWindow.frmDevice) {
						return;
					}
					if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
						if (!objiFrame.contentWindow.frmDevice.selSurvey) {
							return;
						}
					} else {
						if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
							return;
						}
					}
					
					var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
					if (objselSurvey) {
						var objSection = objselSurvey.sections[id];
						if (!objSection) {
							return;
						}
						
						objselSurvey.changeSectionByID(objSection.id)
						//JAPR 2015-06-03: Validado para que sólo limpie el ID de pregunta si realmente se está cambiando la sección, de lo contrario se deja la última pregunta
						//que fue seleccionada como la activa
						if (giSectionID != objSection.id) {
							giQuestionID = 0;
						}
						
						//La variable global giSectionID se actualiza para que guarde el ID de la sección mostrada
						giSectionID = objSection.id;
						//JAPR 2015-06-09: Se cambia el título de la celda de preview al nombre de la sección
						//JAPR 2015-06-26: Agregados los campos de nombre en formato HTML para separar la etiqueta formateada de la original con texto plano //objSection.name
						//JAPR 2015-06-30: Este objeto es directo del preview, por lo tanto no tiene las propiedades ni funciones que el de diseño, se usa el nombre directo
						objFormsLayout.cells(dhxPreviewCell).setText(selSurvey.name + " - " + objSection.name);
						
						//Como se está haciendo click en una pregunta, hay que mover la pantalla hacia la posición donde se encuentra. Todas las preguntas de eForms
						//deben tener un Id así que se puede obtener su posición dentro del HTML y cambiar la posición del visor a dicha posición (se asume que la sección
						//ya está visible así que el HTML de la pregunta también lo está)
						if (bChangeQuestionFocus) {
							setTimeout(function () {
								doSelectQuestion(giQuestionID);
							}, 100);
						}
					}
				} catch (e) {
					alert('doSelectSection error: ' + e);
				}
			}
			
			/* Hace scroll de la página de sección hacia la posición de la pregunta indicada. Para este momento la sección visible ya debería corresponder con
			la sección donde se encuentra la pregunta
			//JAPRDescontinuada: Al remover el árbol esta función ya no es útil, ya que no se puede cambiar el foco a una pregunta directamente sino que se debe navegar dentro de la
			//sección hacia ella y hacer click, así que aunque se invocara correctamente, ya no aplicaría
			*/
			function doSelectQuestion(id) {
				console.log('doSelectQuestion ' + id);
				
				if (!id || id <= 0) {
					return;
				}
				
				var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
				try {
					if (!objiFrame.contentWindow.frmDevice) {
						return;
					}
					if (objiFrame.contentWindow.frmDevice.hasOwnProperty('selSurvey')) {
						if (!objiFrame.contentWindow.frmDevice.selSurvey) {
							return;
						}
					} else {
						if (!objiFrame.contentWindow.frmDevice.contentWindow.selSurvey) {
							return;
						}
					}
					
					var objselSurvey = objiFrame.contentWindow.frmDevice.selSurvey || objiFrame.contentWindow.frmDevice.contentWindow.selSurvey;
					if (objselSurvey) {
						var objQuestion = objselSurvey.questions[id];
						if (!objQuestion || !objQuestion.getFieldName) {
							return;
						}
						
						var strFieldName = objQuestion.getFieldName();
						if (strFieldName) {
							var objQuestionElement = (objiFrame.contentWindow.frmDevice.$ || objiFrame.contentWindow.frmDevice.contentWindow.$)('#' + strFieldName + "_area");
							if (objQuestionElement && objQuestionElement.length) {
								var objOffset = objQuestionElement.offset();
								if (objOffset && objOffset.top) {
									var intTop = objOffset.top;
								}
								else {
									var intTop = 0;
								}

								//MAPR 2017-10-25: Corrección al error de 'Illegal invocation' al agregar una pregunta (#MSTZX7)
								//(objiFrame.contentWindow.frmDevice.scrollTo || objiFrame.contentWindow.frmDevice.contentWindow.scrollTo)(0, intTop);
								if (objiFrame.contentWindow.frmDevice.scrollTo) objiFrame.contentWindow.frmDevice.scrollTo(0,intTop);
								if (objiFrame.contentWindow.frmDevice.contentWindow.scrollTo) objiFrame.contentWindow.frmDevice.contentWindow.scrollTo(0, intTop);
								//Selecciona la pregunta en la ventana del preview
								if (objiFrame.contentWindow.frmDevice.setSelectedQuestion || objiFrame.contentWindow.frmDevice.contentWindow.setSelectedQuestion) {
									(objiFrame.contentWindow.frmDevice.setSelectedQuestion || objiFrame.contentWindow.frmDevice.contentWindow.setSelectedQuestion)(objQuestion);
								}
								
								//La variable global giQuestionID se actualiza para que guarde el ID de la pregunta seleccionada
								giQuestionID = objQuestion.id; 
							}
						}
					}
				} catch (e) {
					alert('doSelectQuestion error: ' + e);
				}
			}
			
			/* Habilita/deshabilita el modo de preview, con el cual la celda de preview aplicará o no la lógica de flujo de captura (saltos, showquestions, visibilidad, etc.)
			*/
			function doSetPreviewMode(oCheckBox) {
				//se manda llamar al cambiar el checkbox de lockLeap
				if (oCheckBox) {
					var blnPreviewMode = oCheckBox.checked;
					//Habilita/deshabilita el modo de Preview del App de captura Web y forza a regenerarlo para aplicar los cambios, ya que los componentes previamente ocultos
					//no se mostrarían con sólo cambiar esta configuración
					if (objFormsLayout && objFormsLayout.cells(dhxPreviewCell)) {
						var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
						if (objiFrame && objiFrame.contentWindow.frmDevice) {
							if (objiFrame.contentWindow.frmDevice.hasOwnProperty('gbApplyFlowLogic')) {
								objiFrame.contentWindow.frmDevice.gbApplyFlowLogic = blnPreviewMode;
								doChangeDevice(objQuestionsToolbar.getListOptionSelected("view"), true);
							} else if (objiFrame.contentWindow.frmDevice.contentWindow) {
								objiFrame.contentWindow.frmDevice.contentWindow.gbApplyFlowLogic = blnPreviewMode;
								doChangeDevice(objQuestionsToolbar.getListOptionSelected("view"), true);
							}
						}
					}
				}
			
			}
			
			//JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
			function browserApp() {
				d = new Date();
				dteNow = '' + d.getFullYear() + d.getMonth() + d.getDay() + d.getHours() + d.getMinutes() + d.getSeconds();
				openWindow('GenerateSurvey.php?dte=' + dteNow + '&DefaultSurveyID=' + intSurveyID + '&login=1');
			}
			
			//JAPR 2017-02-09: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			/* Sincroniza los cambios de la forma de desarrollo hacia la forma de producción con desarrollo asociada */
			function doBuildProdForm() {
				var blnAnswer = confirm("<?=translate("The changes made in this form will be build in production overwriting the current form design")?>");
				if (blnAnswer) {
					$('#divSaving').show();
					strParams = "SelSurveyID=" + intSurveyID + "&Process=Build&RequestType=" + reqAjax;
					window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
						if (!loader || !loader.xmlDoc) {
							return;
						}
						
						if (loader.xmlDoc && loader.xmlDoc.status != 200) {
							alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
							return;
						}
						
						//Si llega a este punto es que se recibió una respuesta correctamente
						var response = loader.xmlDoc.responseText;
						try {
							var objResponse = JSON.parse(response);
						} catch(e) {
							alert(e + "\r\n" + response.substr(0, 1000));
							return;
						}
						
						if (objResponse.error) {
							alert(objResponse.error.desc);
							return;
						}
						else {
							if (objResponse.warning) {
								console.log(objResponse.warning.desc);
							}
						}
						
						alert("<?=translate("The form was generated successfully")?>");
					});
				}
			}
			
			//RV Agosto2016: Invoca proceso para copiar forma
			function doCopyForm(actionType) {
				if (actionType == undefined)
				{
					actionType = copyAction;
				}
				if (actionType == copyAction)
				{
					var blnAnswer = confirm("<?=translate("Do you want to copy the form")?>" + " " + selSurvey.name+" ?");
				}
				else
				{
					<?//@JAPR 2018-11-08: Corregido el escape de algunos caracteres en strings (#KGW0CU)?>
					var blnAnswer = confirm("<?=str_ireplace("{var1}", GetValidJScriptText($this->SurveyName), translate("Do you want to generate a development form for {var1}?"))?>");
				}
				
				if (blnAnswer) {
					window.parent.objFormsLayoutMenu.cells("b").progressOn();
					var strParams;
					strParams="SurveyID="+intSurveyID+"&actionType="+actionType;
					window.dhx4.ajax.post("processCopyForm.php", strParams, function(loader) {
						console.log('after processCopyForm.php');
						afterCopyForm(loader);
					});
				}
			}
			
			function afterCopyForm(loader)
			{
				console.log('funcion afterCopyForm');

				if (!loader || !loader.xmlDoc) {
					/*
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					*/
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					/*
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					*/
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				
				//JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert("<?=translate("There was an error while copying the form")?>: " +  "\r\n" + response.substr(0, 1000));
					window.parent.objFormsLayoutMenu.cells("b").progressOff();
					return;
				}
				
				if (objResponse.error) {
					alert("<?=translate("There was an error while copying the form")?>: " + objResponse.error.desc);
					window.parent.objFormsLayoutMenu.cells("b").progressOff();
					return;
				}
				else {
					if (objResponse.warning) {
						console.log("There was a warning while copying the form: " + objResponse.warning.desc);
					}
				}
				
				if (!objResponse.name) {
					objResponse.name = '';
				}
				
				alert("<?=translate("The form has been copied with the name")?>"+ " " + objResponse.name);
				//JAPR
				
				//Direccionar a colección de formas
				if(parent.doExecuteURL)
				{
					parent.doExecuteURL("Forms");
				}
			}
			
			function doChangeTab(sTabId) {
				console.log('doChangeTab ' + sTabId);
				
				var objEvent = this.event || window.event;
				if (objEvent) {
					var objTarget = objEvent.target || objEvent.srcElement;
					if (objTarget) {
						$("#divDesignHeader td").removeClass("linktdSelected");
						$(objTarget).addClass("linktdSelected");
					}
				}
				if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
					return;
				}

				if( sTabId == tabModel ) {
				//OMMC 2019-07-19: Corregido detalle con redactor, ya que los wizards de eBavel estaban sobreescribiendo jQuery
					require(['views/models/model', 'redactorEditor'], function( Page, aRedactor ) { 
						require([
							'css!redactor/redactor/redactor.css',
							'redactor/imagemanage',
							'redactor/underline',
							'redactor/fontsize',
							'redactor/fontfamily',
							'redactor/fontcolor',
							'redactor/formulaEditor',
							'redactor/lang/es',
							'redactor/lang/en'
						], function(){
							new Page().setElement( objFormTabs.tabs(sTabId).cell ).render(); 
						});
					});
				} else if( sTabId == tabDataDestinations ) {
					//@ears 2016-07-22 progressOn, al seleccionar Destino de Datos (issue GNFIZE)
					objFormTabs.tabs(sTabId).progressOn();					
					require(['views/datadestinations/page', 'redactorEditor'], function( Page, aRedactor ) { 
						require([
							'css!redactor/redactor/redactor.css',
							'redactor/imagemanage',
							'redactor/underline',
							'redactor/fontsize',
							'redactor/fontfamily',
							'redactor/fontcolor',
							'redactor/formulaEditor',
							'redactor/lang/es',
							'redactor/lang/en'
						], function(){
							objFormTabs.tabs(sTabId).progressOff();
							new Page().setElement( objFormTabs.tabs(sTabId).cell ).render();
						});	
					});
				}
				
				//@JAPR 2016-07-08: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize (#J2BJYD)
				//objFormTabs.tabs(sTabId).setActive();
			}
			
			/* Realiza el request para agregar una sección específica, presentando previamente un popUp para permitir elegir el tipo de sección
			*/
			function doSectionAction(id) {
				console.log('doSectionAction ' + id);
				
				if (objSectionPopUp) {
					objSectionPopUp.hide();
				}
				
				//Verifica el tipo de id recibido, si es numérico se asume que se desea cargar la sección con dicho id, de lo contrario se trata de una acción desde un componente
				if (!$.isNumeric(id)) {
					switch (id) {
						case "add":
							//Crea una nueva sección preguntando sólo el nombre
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							addNewSection();
							break;
						case "remove":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							if (!giSectionID) {
								alert("<?=str_ireplace("{var1}", strtolower(translate("Section")), translate("Please select a {var1} to remove"))?>");
								return;
							}
							else {
								var strObjectName = "";
								if (selSurvey && selSurvey.sections && selSurvey.sections[giSectionID]) {
									strObjectName = selSurvey.sections[giSectionID].getName();
								}
								var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove this %s?"), translate("Section"))?>" + " " + strObjectName);
								if (blnAnswer) {
									//doDeleteObject(AdminObjectType.otySection, giSectionID);
									doDeleteObject(AdminObjectType.otySection, giSectionID, undefined, undefined, reqAjax, function(oResponse) {
										console.log('doSectionAction remove function');
										
										//Remueve el elemento de la lista si todo estuvo ok
										if (!oResponse || !oResponse.deleted || !oResponse.newObject) {
											objFormTabs.tabs(tabDesign).progressOff();
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otySection) {
											objFormTabs.tabs(tabDesign).progressOff();
											return
										}
										
										//Debe actualizar primero la definición en memoria
										if (oResponse.definitions) {
											objFormsDefs = oResponse.definitions;
										}
										selSurvey = objFormsDefs.data.surveyList[intSurveyID];
										
										//Después de actualizar la definición de la forma, es necesario volver a extender los objetos actualizados
										addClassPrototype();
										
										//Como se está eliminando una sección, mueve el apuntador a la primera disponible
										giSectionID = 0;
										
										//Reconstruye la lista de secciones si hubo necesidad de remover alguna
										doDrawSections();
										
										//Posteriormente debe actualizar la definición en el preview
										var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
										if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
											return;
										}
										
										var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
										if (oResponse.definitionVersions) {
											objApp.getNewDefinitionsVersion(oResponse.definitionVersions);
										}
										if (oResponse.definitions) {
											objApp.getNewDefinitions(oResponse.definitions);
										}
										
										//Finalmente debe ejecutar el refresh del preview
										//Prepara la función para que redireccione al padre al terminar de refrescar el preview
										objApp.notifySurveyShown = function() {
											console.log('notifySurveyShown');
											
											doSelectSection(giSectionID, true);
											if (doSectionAction) {
												doSectionAction(giSectionID);
											}
											
											//JAPR 2015-10-14: Validado que en caso se ser la última sección la que se removió, quite el nombre de la sección del header (#UP6RRS)
											if (giSectionID <= 0) {
												if (selSurvey) {
													objFormsLayout.cells(dhxPreviewCell).setText(selSurvey.name);
												}
												objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?>");
											}
											else {
												if (giSectionID && objSectionsDataView) {
													objSectionsDataView.select(giSectionID);
												}
											}
											//JAPR
										}
										
										objApp.backToFormsList(true);
										if (objiFrame.contentWindow.frmDevice.hasOwnProperty('gblBuildingPage')) {
											objiFrame.contentWindow.frmDevice.gblBuildingPage = false;
										} else {
											objiFrame.contentWindow.frmDevice.contentWindow.gblBuildingPage = false;
										}
										objApp.showSurvey(intSurveyID);
										
										objFormTabs.tabs(tabDesign).progressOff();
									}, true);
								}
							}
							break;
					}
					return;
				}
				
				doShowSectionProps("s" + id);
			}
			
			/* Realiza el request para agregar una pregunta específica, presentando previamente un PopUp para permitir elegir el tipo de pregunta
			*/
			function doQuestionAction(id) {
				console.log('doQuestionAction ' + id);
				
				if (objQuestionPopUp && id != "add") {
					objQuestionPopUp.hide();
				}
				
				//Verifica el tipo de id recibido, si es numérico se asume que se desea cargar la sección con dicho id, de lo contrario se trata de una acción desde un componente
				if (!$.isNumeric(id)) {
					//Si se trata del comando Add y viene seguido de un id, quiere decir que se seleccionó la opción para crear un tipo de pregunta específico, así
					//que en lugar de invocar al Accordion para seleccionar el tipo, invoca directamente al diálogo de creación de pregunta basado en el tipo especificado
					var intQTypeID = 0;
					if (id.substr(0, 3) == "add") {
						intQTypeID = parseInt(id.substr(3));
						if (!$.isNumeric(intQTypeID)) {
							intQTypeID = 0;
						}
						
						id = "add";
					}
				
					switch (id) {
						case "add":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							if (intQTypeID) {
								//Crea una nueva pregunta solicitando la información mínima según el tipo especificado
								var objFieldsDef = getDefaultQuestionFields(intQTypeID);
								addNewQuestion(objFieldsDef, intQTypeID);
							}
							else {
								//Muestra el selector del tipo de pregunta
								if (gsLastQGroupSelected) {
									objNewQuestionsAcc.cells(gsLastQGroupSelected).open();
								}
								//objQuestionPopUp.show(id);
							}
							return;
							break;
						case "move":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							if (giQuestionID) {
								doShowMoveDialog();
							}
							return;
							break;
						case "refresh":
							//JAPR 2015-07-30: Validado que al actualizar la vista se regrese a donde se había quedado, ya sea una pregunta o una sección (#9AUDGB)
							var intObjectType = undefined;
							var intObjectID = undefined;
							if (giQuestionID > 0) {
								intObjectType = AdminObjectType.otyQuestion;
								intObjectID = giQuestionID;
							}
							else {
								if (giSectionID > 0) {
									intObjectType = AdminObjectType.otySection;
									intObjectID = giSectionID;
								}
							}
							
							doDeviceAction(id, intObjectType, intObjectID);
							//JAPR
							break;
						case "remove":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							if (!giQuestionID) {
								alert("<?=str_ireplace("{var1}", strtolower(translate("Question")), translate("Please select a {var1} to remove"))?>");
								return;
							}else {
								var strObjectName = "";
								if (!selSurvey && !selSurvey.questions && !selSurvey.questions[giQuestionID]) {
									return;
								}
								//OMMC 2015-12-08: Validación para revisar si la pregunta a eliminar es la pregunta selector cuando es una sección tabla. #4CDYL4
								//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
								if ( ((selSurvey.sections[giSectionID].sectionType == SectionType.Dynamic || selSurvey.sections[giSectionID].sectionType == SectionType.Inline) && selSurvey.sections[giSectionID].selectorQuestionID == giQuestionID) || 
										(selSurvey.sections[giSectionID].sectionType == SectionType.Multiple && (selSurvey.sections[giSectionID].xPosQuestionID == giQuestionID || selSurvey.sections[giSectionID].yPosQuestionID == giQuestionID || selSurvey.sections[giSectionID].itemNameQuestionID == giQuestionID)) ) {
									alert("<?=translate("It is not possible to delete this question, due to this section type.")?>");
									return;
								}
								//JAPR
								strObjectName = selSurvey.questions[giQuestionID].getName();
								var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove this %s?"), translate("Question"))?>" + " " + strObjectName);
								if (blnAnswer) {
									doDeleteObject(AdminObjectType.otyQuestion, giQuestionID, undefined, undefined, reqAjax, function(oResponse) {
										console.log('doQuestionAction remove function');
									
										//Remueve el elemento de la lista si todo estuvo ok
										if (!oResponse || !oResponse.deleted || !oResponse.newObject) {
											objFormTabs.tabs(tabDesign).progressOff();
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otyQuestion) {
											objFormTabs.tabs(tabDesign).progressOff();
											return
										}
										
										//Debe actualizar primero la definición en memoria
										if (oResponse.definitions) {
											objFormsDefs = oResponse.definitions;
										}
										selSurvey = objFormsDefs.data.surveyList[intSurveyID];
										
										//Después de actualizar la definición de la forma, es necesario volver a extender los objetos actualizados
										addClassPrototype();
										
										//Reconstruye la lista de secciones si hubo necesidad de remover alguna
										doDrawSections();
										
										//Posteriormente debe actualizar la definición en el preview
										var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
										if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
											return;
										}
										
										var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
										if (oResponse.definitionVersions) {
											objApp.getNewDefinitionsVersion(oResponse.definitionVersions);
										}
										if (oResponse.definitions) {
											objApp.getNewDefinitions(oResponse.definitions);
										}
										
										//Finalmente debe ejecutar el refresh del preview
										//Prepara la función para que redireccione al padre al terminar de refrescar el preview
										objApp.notifySurveyShown = function() {
											console.log('notifySurveyShown');
											
											doSelectSection(giSectionID, true);
											if (doSectionAction) {
												doSectionAction(giSectionID);
											}
											if (giSectionID && objSectionsDataView) {
												objSectionsDataView.select(giSectionID);
											}
										}
										
										objApp.backToFormsList(true);
										if (objiFrame.contentWindow.frmDevice.hasOwnProperty('gblBuildingPage')) {
											objiFrame.contentWindow.frmDevice.gblBuildingPage = false;
										} else {
											objiFrame.contentWindow.frmDevice.contentWindow.gblBuildingPage = false;
										}
										objApp.showSurvey(intSurveyID);
										
										objFormTabs.tabs(tabDesign).progressOff();
									}, true);
								}
							}
							break;
						case "moveQuestionUp":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							/*OMMC 2015-10-20: Incluida la funcionalidad de los botones UP y DOWN para mover las preguntas en el administrador.*/
							if (!giQuestionID) {
								alert("<?=str_ireplace("{var1}", strtolower(translate("Question")), translate("Please select a {var1} to move"))?>");
								return;
							}else{
								var actualQuestion = selSurvey.sections[giSectionID].questions[giQuestionID];
								if(actualQuestion.id == selSurvey.sections[giSectionID].questionsOrder[0]){
									alert("<?=translate('This is already the first question in this section');?>");
									return;
								}else{
									for(var i=0; i<selSurvey.sections[giSectionID].questionsOrder.length; i++){
										if(actualQuestion.id == selSurvey.sections[giSectionID].questionsOrder[i]){
											var beforeQuestionID = selSurvey.sections[giSectionID].questionsOrder[i-1];
										}
									}
									var questionPositionDestination = selSurvey.sections[giSectionID].questions[beforeQuestionID].number;
									doReorderObject(AdminObjectType.otyQuestion, giQuestionID, questionPositionDestination);
								}
							}
						break;						
						case "moveQuestionDown":
							<?
							//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
							if ($this->FormType == formProdWithDev) {?>
							break;
							<?}
							//@JAPR
							?>
							if (!giQuestionID) {
								alert("<?=str_ireplace("{var1}", strtolower(translate("Question")), translate("Please select a {var1} to move"))?>");
								return;
							}else{
								var actualQuestion = selSurvey.sections[giSectionID].questions[giQuestionID];
								if(actualQuestion.id == selSurvey.sections[giSectionID].questionsOrder[selSurvey.sections[giSectionID].questionsOrder.length-1]){
									alert("<?=translate('This is already the last question in this section');?>");
									return;
								}else{
									for(var i=0; i<selSurvey.sections[giSectionID].questionsOrder.length; i++){
										if(actualQuestion.id == selSurvey.sections[giSectionID].questionsOrder[i]){
											var afterQuestionID = selSurvey.sections[giSectionID].questionsOrder[i+1];
										}
									}
									var questionPositionDestination = selSurvey.sections[giSectionID].questions[afterQuestionID].number;
									doReorderObject(AdminObjectType.otyQuestion, giQuestionID, questionPositionDestination);
								}
							}
						break;
						
							
						default:
							//El caso cuando se selecciona un dispositivo específico o la orientación / tamaño
							doChangeDevice(id, true);
							break;
					}
					return;
				}
				
				doShowQuestionProps(id);
			}
			
			/* Muestra un diálogo modal que permitirá cambiar de sección la pregunta actualmente seleccionada */
			function doShowMoveDialog() {
				console.log('doShowMoveDialog');
				
				if (!selSurvey) {
					return;
				}
				
				var objQuestion = selSurvey.questions[giQuestionID];
				if (!objQuestion) {
					return;
				}
				var objCurrSection = selSurvey.sections[objQuestion.sectionID];
				if (!objCurrSection) {
					return;
				}
				
				//OMMC 2015-11-26: Agregada la validación para que no permita mover la pregunta selector a otra sección.
				//JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if ( objCurrSection.selectorQuestionID == objQuestion.id || objCurrSection.xPosQuestionID == objQuestion.id || objCurrSection.yPosQuestionID == objQuestion.id || objCurrSection.itemNameQuestionID == objQuestion.id ) {
					return;
				}
				//JAPR
				
				var intDialogWidth = 380;
				var intDialogHeight = 350;
				objDialog = objWindows.createWindow({
					id:"moveQuestion",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("Move question to another section")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});
				
				/* El diálogo para mover las preguntas de sección tiene 2 labels, una para indicar la pregunta a mover y la otra para indicar la sección actual, además tiene
				una combo para indicar la sección (sólo la lista de las aplicables) a la que se moverá la pregunta
				*/
				//Agrega las secciones válidas a la combo para seleccionar la nueva sección a donde se moverá la pregunta
				var objSectionsList = new Array();
				for (var intSectionNum in selSurvey.sectionsOrder) {
					var intSectionID = selSurvey.sectionsOrder[intSectionNum];
					var objSection = selSurvey.sections[intSectionID];
					if (objSection && intSectionID != objQuestion.sectionID && objSection.type != SectionType.Formatted) {
						objSectionsList.push({value:objSection.id, text:objSection.getName()});
					}
				}
				
				var objFormData = [
					{type:"settings", offsetLeft:20},
					{type:"input", name:"txtQuestion", label:"<?=translate("Question")?>", value:objQuestion.getName(), labelWidth:100, readonly:true},
					{type:"input", name:"txtSection", label:"<?=translate("Current section")?>", value:objCurrSection.getName(), labelWidth:100, readonly:true/*, className:"dhxform_txt_label2"*/},
					{type:"combo", name:"SectionID", label:"<?=translate("New section")?>", labelWidth:100, options:objSectionsList},
					{type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true},
					{type:"input", name:"QuestionID", value:objQuestion.id, hidden:true},
					{type:"input", name:"Design", value:1, hidden:true},
					{type:"input", name:"RequestType", value:reqAjax, hidden:true},
					{type:"input", name:"Process", value:"Edit", hidden:true},
					{type:"input", name:"ObjectType", value:AdminObjectType.otyQuestion, hidden:true}
				];
				
				var objMoveForm = objDialog.attachForm(objFormData);
				objMoveForm.adjustParentSize();
				objMoveForm.setItemFocus("SectionID");
				objMoveForm.attachEvent("onButtonClick", function(name) {
					console.log('objMoveForm.onButtonClick ' + name);
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objMoveForm.validate()) {
									doSendData(function(oResponse) {
										//Al terminar de grabar con éxito, debe agregar al objeto en memoria la nueva sección creada, además de regenerar la dataView de secciones
										//con la nueva sección como seleccionada y presentar la lista de propiedades para que permita ser modificada
										
										//Se recibirá como parámetro la respuesta del server en la cual debe venir la nueva definición (para este punto ya se validó que no hubiera
										//algún error en el request, así que debe ser una respuesta válida)
										if (!oResponse || !oResponse.newObject) {
											console.log("<?=translate("No response object was received during this process")?>");
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otyQuestion) {
											return
										}
										
										//Invoca al refresh del preview para realizar el cambio de sección de la pregunta indicada
										doDeviceAction("refresh", AdminObjectType.otySection, giSectionID);
										
										//Cambia la ventana de propiedades hacia la nueva sección
										//doShowSectionProps("s" + giSectionID);
									});
								}
							}, 100);
							break;
					}
				});
				
				var objCombo = objMoveForm.getCombo("SectionID");
				if (objCombo) {
					//JAPR 2015-09-21: Corregido un bug, esta combo no debe ser editable (#V8IYUH)
					objCombo.readonly(true);
					objCombo.attachEvent("onKeyPressed", function(keyCode) {
						switch (keyCode) {
							case 27:
								doUnloadDialog();
								break;
						}
					});
				}
			}
			
			//Descarga la instancia de la forma de la memoria
			function doUnloadDialog() {
				console.log('doUnloadDialog');
				if (!objWindows || !objDialog) {
					return;
				}
				
				var objForm = objDialog.getAttachedObject();
				//JAPR 2015-10-13: Agregado el cierre de los posibles PopUps que pudiera contener la forma 
				if (objForm) {
					objForm.forEachItem(function (name) {
						var strType = objForm.getItemType(name);
						if (strType == "editor") {
							var objEditor = objForm.getEditor(name);
							if (objEditor && objEditor.tb && objEditor.tb.objPopUpsColl) {
								for (var strPopUpName in objEditor.tb.objPopUpsColl) {
									var objPopUp = objEditor.tb.objPopUpsColl[strPopUpName];
									if (objPopUp) {
										if (objPopUp.bitEventHandler) {
											objPopUp.detachEvent(objPopUp.bitEventHandler);
										}
										//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
										//objPopUp.hide();
										objPopUp.unload();
										//JAPR
									}				
								}
							}
						}
					});
				}
				//JAPR
				
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialog = null;
			}
			function doUnloadDialogFn() {
				console.log('doUnloadDialogFn');
				if (!objWindowsFn || !objDialogFn) {
					return;
				}
				
				var objForm = objDialogFn.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindowsFn.unload) {
					objWindowsFn.unload();
					objWindowsFn = null;
					objWindowsFn = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogFn = null;
			}
			
			function doUnloadDialogImg() {
				console.log('doUnloadDialog');
				if (!objWindowsImg || !objDialogImg) {
					return;
				}
				
				var objForm = objDialogImg.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindowsImg.unload) {
					objWindowsImg.unload();
					objWindowsImg = null;
					objWindowsImg = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogImg = null;
			}
			
			function doUnloadDialogEd() {
				console.log('doUnloadDialogEd');
				if (!objWindowsEd || !objDialogEd) {
					return;
				}
				
				var objForm = objDialogEd.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
				
				if (objWindowsEd.unload) {
					objWindowsEd.unload();
					objWindowsEd = null;
					objWindowsEd = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				objDialogEd = null;
			}
			
			
			/* Realiza el request para actualizar las definiciones utilizadas por el Preview
			//JAPR 2015-06-12: Agregados los parámetros iSelType e iSelId para especificar que objeto deberá quedar dibujado al terminar de hacer el refresh
			*/
			function doDeviceAction(id, iSelType, iSelId) {
				console.log('doDeviceAction ' + id + ', iSelType == ' + iSelType + ', iSelId == ' + iSelId);
			
				switch(id) {
					case 'refresh':
						var strSelection = "";
						if ($.isNumeric(iSelType) && $.isNumeric(iSelId)) {
							strSelection = "&selItemType=" + iSelType + "&selItemID=" + iSelId;
						}
					
						//Invoca al método que actualiza las definiciones en la celda de Preview y vuelve a generar la forma
						objFormsLayout.cells(dhxPreviewCell).progressOn();
						frmRequests.action = "processRequest.php?Process=Refresh&SelSurveyID=" + intSurveyID + "&dte=<?=date('YmdHis')?>" + strSelection;
						frmRequests.target = "frameRequests";
						frmRequests.submit();
						break;
				}
			}
			
			/* Realiza el cambio de dispositivo y vuelve a generar la ventana del App de eForms con el cambio de resolución y device
			//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
			*/
			function doChangeDevice(sButtonId, bState) {
				console.log('doChangeDevice ' + sButtonId + ', bState == ' + bState);
				
				var strCurrDeviceName = "";
				if (objQuestionsToolbar) {
					strCurrDeviceName = objQuestionsToolbar.getUserData("view", "deviceSelected");
				}
				
				//Remueve la selección de las listOptions de dispositivos/orientación
				if (objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon) {
					$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr").css("background-color", "")
					//Sólo respeta la selección de dispositivo si se está cambiando la orientación
					if (objOrientationsColl[sButtonId]) {
						$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr:eq(" + objDevicesColl[strCurrDeviceName].pos +")").css("background-color", gc_ListOptionSelected)
					}
				}
				
				if (objOrientationsColl[sButtonId]) {
					doChangeOrientationDevice(sButtonId, bState);
					objQuestionsToolbar.setListOptionSelected("view", strCurrDeviceName);
					return;
				}
				
				if (!objDevicesColl[sButtonId]) {
					return;
				}
				
				//JAPR 2016-07-07: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
				//Identifica la orientación del dispositivo
				if (objQuestionsToolbar) {
					var blnPortrait = (objQuestionsToolbar.getUserData("view", "deviceOrientation") == "Portrait");
				}
				//JAPR
				
				var strCurrDeviceName = sButtonId;
				
				//Actualiza el estado del botón correspondiente en el botón de selección
				if (objQuestionsToolbar) {
					objQuestionsToolbar.setUserData("view", "deviceSelected", sButtonId);
					//Remueve la clase de todas las opciones
					if (objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon) {
						//Finalmente no sirvió esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, así que se perdía
						//la clase definida en este archivo, se optó por cambiar directamente el estilo
						//$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr").removeClass("tbOptionSelected");
						//$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr").css("background-color", "")
						if (objDevicesColl[sButtonId]) {
							//$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr:eq(" + objDevicesColl[sButtonId].pos +")").addClass("tbOptionSelected");
							$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr:eq(" + objDevicesColl[sButtonId].pos +")").css("background-color", gc_ListOptionSelected)
						}
						
						//Respeta la selección de la orientación si se está cambiando el dispositivo
						//JAPR 2016-07-07: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
						if (objQuestionsToolbar) {
							var blnPortrait = (objQuestionsToolbar.getUserData("view", "deviceOrientation") == "Portrait");
						}
						//JAPR
						$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr:eq(" + (Object.keys(objDevicesColl).length + 1 + ((blnPortrait)?0:1))  +")").css("background-color", gc_ListOptionSelected);
					}
				}
				
				//Ajusta el tamaño personalizado al del dispositivo seleccionado (o bien al personalizado manualmente si es que hay alguno) y vuelve a generar
				//el contenido de la forma
				var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
				try {
					if (!objiFrame.contentWindow.frmDevice) {
						return;
					}
					if (objiFrame.contentWindow.frmDevice.hasOwnProperty('objSettings')) {
						if (!objiFrame.contentWindow.frmDevice.objSettings) {
							return;
						}
					} else {
						if (!objiFrame.contentWindow.frmDevice.contentWindow.objSettings) {
							return;
						}
					}
					
					var objSettings = objiFrame.contentWindow.frmDevice.objSettings || objiFrame.contentWindow.frmDevice.contentWindow.objSettings;
					if (!objSettings.device) {
						return;
					}
					
					var strDeviceID = sButtonId;
					if (strDeviceID.toLowerCase().indexOf("android") == 0) {
						strDeviceID = "Android";
					}
					
					//Modifica la información del dispositivo a utilizar
					//Si se seleccionó uno de los dispositivos predefinidos, se asigna el tamaño en base a los default configurados
					if (objDevicesColl[sButtonId]) {
						if (blnPortrait) {
							objSettings.customWidth = objDevicesColl[sButtonId].width;
						}
						else {
							objSettings.customWidth = objDevicesColl[sButtonId].height;
						}
					}
					
					//Sin importar el dispositivo configurado, si existe un valor específico de tamaño, se debe utilizar el tamaño personalizado
					//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
					//Por ahora no existe forma de cambiar el tamaño personalizado
					var intWidth = "";
					var intHeight = "";
					
					if (intWidth.indexOf("px")) {
						intWidth = intWidth.replace("px", "");
					}
					
					if (intHeight.indexOf("px")) {
						intHeight = intHeight.replace("px", "");
					}
					
					//Si se especificó un tamaño fijo, prepara el objeto para definirlo y cambiar las dimensiones del frame contenedor
					var objCustomSize = undefined;
					intWidth = (isNaN(parseInt(intWidth)))?"":parseInt(intWidth);
					if (intWidth && intWidth > 0) {
						objSettings.customWidth = intWidth;
						if (!objCustomSize) {
							objCustomSize = new Object();
						}
						objCustomSize.width = intWidth;
					}
					intHeight = (isNaN(parseInt(intHeight)))?"":parseInt(intHeight);
					if (intHeight && intHeight > 0) {
						if (!objCustomSize) {
							objCustomSize = new Object();
						}
						objCustomSize.height = intHeight;
					}
					
					objSettings.device.platform = strDeviceID;
					if (strDeviceID == "Android") {
						if (sButtonId.toLowerCase().indexOf("tab")) {
							objSettings.device.version = 3;
						}
					}
					
					//Despliega la imagen del dispositivo seleccionado
					if (objiFrame.contentWindow.setDevice) {
						objiFrame.contentWindow.setDevice(strCurrDeviceName, ((blnPortrait)?'portrait':'landscape'), objCustomSize);
					}
					
					//Genera nuevamente la forma indicada en el previo
					var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
					if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
						return;
					}
					
					var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
					//Sobreescribe el evento al terminar de cargar el App ya que no es necesario cambiar la ventana de propiedades por cambio de dispositivo
					objApp.notifySurveyShown = function() {
						//JAPR 2015-07-31: Agregado el parámetro bChangeQuestionFocus para indicar cuando si se quiere modificar el foco de la pregunta, ya que en los clicks a la propia pregunta
						//desde el preview no es necesario hacer esto pues provocaría un efecto extraño al mover ligeramente el top de la página justo al inicio de la pregunta incluso si según el
						//scroll actual se veía abajo, así que eso sólo se requiere cuando se está haciendo un refresh
						doSelectSection(giSectionID, undefined, true);
					}
					
					objApp.backToFormsList(true);
					objApp.showSurvey(intSurveyID);
				} catch (e) {
					alert('doChangeDevice error: ' + e);
				}
			}
			
			/* Realiza el cambio en la orientación del dispositivo invirtiendo el tamaño máximo horizontal de la resolución por el vertical y refrescando la
			vista previa
			//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
			*/
			function doChangeOrientationDevice(sButtonId, bState) {
				console.log('doChangeOrientationDevice ' + sButtonId + ', bState == ' + bState);
				
				//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
				//Identifica el tipo de dispositivo seleccionado
				var strCurrDeviceName = objQuestionsToolbar.getUserData("view", "deviceSelected");
				var blnPortrait = (sButtonId == "Portrait");
				if (objQuestionsToolbar) {
					objQuestionsToolbar.setUserData("view", "deviceOrientation", sButtonId);
				}
				
				//Selecciona el ListOption de la orientación indicada
				if (objQuestionsToolbar) {
					$(objQuestionsToolbar.objPull[objQuestionsToolbar.idPrefix+"view"].polygon).find("tr:eq(" + (Object.keys(objDevicesColl).length + 1 + ((blnPortrait)?0:1))  +")").css("background-color", gc_ListOptionSelected);
				}
				
				//Ajusta el tamaño personalizado al del dispositivo seleccionado (o bien al personalizado manualmente si es que hay alguno) y vuelve a generar
				//el contenido de la forma
				var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
				try {
					if (!objiFrame.contentWindow.frmDevice) {
						return;
					}
					if (objiFrame.contentWindow.frmDevice.hasOwnProperty('objSettings')) {
						if (!objiFrame.contentWindow.frmDevice.objSettings) {
							return;
						}
					} else {
						if (!objiFrame.contentWindow.frmDevice.contentWindow.objSettings) {
							return;
						}
					}
					
					var objSettings = objiFrame.contentWindow.frmDevice.objSettings || objiFrame.contentWindow.frmDevice.contentWindow.objSettings;
					if (!objSettings.device) {
						return;
					}
					
					var strDeviceID = strCurrDeviceName;
					if (strDeviceID.toLowerCase().indexOf("android") == 0) {
						strDeviceID = "Android";
					}
					
					//Modifica la información del dispositivo a utilizar
					//Si se seleccionó uno de los dispositivos predefinidos, se asigna el tamaño en base a los default configurados
					if (objDevicesColl[strCurrDeviceName]) {
						if (blnPortrait) {
							objSettings.customWidth = objDevicesColl[strCurrDeviceName].width;
						}
						else {
							objSettings.customWidth = objDevicesColl[strCurrDeviceName].height;
						}
					}
					
					//Sin importar el dispositivo configurado, si existe un valor específico de tamaño, se debe utilizar el tamaño personalizado
					//JAPR 2016-07-08: Removido el Ribbon oculto, ahora todo lo manejará directamente la toolbar de preguntas (#J2BJYD)
					//Por ahora no existe forma de cambiar el tamaño personalizado
					var intWidth = "";
					var intHeight = "";
					/*if (blnPortrait) {
						var intWidth = objSrcObject.getValue("customWidth");
						var intHeight = objSrcObject.getValue("customHeight");
					}
					else {
						var intWidth = objSrcObject.getValue("customHeight");
						var intHeight = objSrcObject.getValue("customWidth");
					}
					*/
					
					if (intWidth.indexOf("px")) {
						intWidth = intWidth.replace("px", "");
					}
					
					if (intHeight.indexOf("px")) {
						intHeight = intHeight.replace("px", "");
					}
					
					//Si se especificó un tamaño fijo, prepara el objeto para definirlo y cambiar las dimensiones del frame contenedor
					var objCustomSize = undefined;
					intWidth = (isNaN(parseInt(intWidth)))?"":parseInt(intWidth);
					if (intWidth && intWidth > 0) {
						objSettings.customWidth = intWidth;
						if (!objCustomSize) {
							objCustomSize = new Object();
						}
						objCustomSize.width = intWidth;
					}
					intHeight = (isNaN(parseInt(intHeight)))?"":parseInt(intHeight);
					if (intHeight && intHeight > 0) {
						if (!objCustomSize) {
							objCustomSize = new Object();
						}
						objCustomSize.height = intHeight;
					}
					
					objSettings.device.platform = strDeviceID;
					if (strDeviceID == "Android") {
						if (strCurrDeviceName.toLowerCase().indexOf("tab")) {
							objSettings.device.version = 3;
						}
					}
					
					//Despliega la imagen del dispositivo seleccionado
					if (objiFrame.contentWindow.setDevice) {
						objiFrame.contentWindow.setDevice(strCurrDeviceName, ((blnPortrait)?'portrait':'landscape'), objCustomSize);
					}
					
					//Genera nuevamente la forma indicada en el previo
					var objiFrame = objFormsLayout.cells(dhxPreviewCell).getFrame();
					if (!objiFrame.contentWindow.frmDevice || !(objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp)) {
						return;
					}
					
					var objApp = objiFrame.contentWindow.frmDevice.objApp || objiFrame.contentWindow.frmDevice.contentWindow.objApp;
					//Sobreescribe el evento al terminar de cargar el App ya que no es necesario cambiar la ventana de propiedades por cambio de orientación
					objApp.notifySurveyShown = function() {
						//JAPR 2015-07-31: Agregado el parámetro bChangeQuestionFocus para indicar cuando si se quiere modificar el foco de la pregunta, ya que en los clicks a la propia pregunta
						//desde el preview no es necesario hacer esto pues provocaría un efecto extraño al mover ligeramente el top de la página justo al inicio de la pregunta incluso si según el
						//scroll actual se veía abajo, así que eso sólo se requiere cuando se está haciendo un refresh
						doSelectSection(giSectionID, undefined, true);
					}
					
					objApp.backToFormsList(true);
					objApp.showSurvey(intSurveyID);
				} catch (e) {
					alert('doChangeOrientationDevice error: ' + e);
				}
			}
			
			/* Agrega la clase correspondiente a cada objeto descargado de la definición de la forma */
			function addClassPrototype() {
				if (!selSurvey) {
					return;
				}
				
				$.extend(selSurvey, new SurveyCls());
				if (selSurvey.sections) {
					for (var intSectionID in selSurvey.sections) {
						$.extend(selSurvey.sections[intSectionID], new SectionCls());
						var objSection = selSurvey.sections[intSectionID];
						if (objSection.questions) {
							for (var intQuestionID in objSection.questions) {
								$.extend(objSection.questions[intQuestionID], new QuestionCls());
								//Agrega una nueva colección de options indexadas por el consecutiveID, ya que el preview las requiere indexadas por el name
								var objQuestion = objSection.questions[intQuestionID];
								if (objQuestion) {
									//OMMC 2015-11-24: Debido a comportamientos erráticos de los componentes de jQuery, se optó por cambiar visualmente las preguntas MChoice a verticales cuando son de tipo menú o autocomplete.
									if(objSection.type == SectionType.inline && objSection.displayMode == SectionDisplayMode.sdspInline){
										if(objQuestion.id != objSection.selectorQuestionID){
											if(objQuestion.type == Type.multiplechoice && (objQuestion.displayMode == DisplayMode.dspAutocomplete || objQuestion.displayMode == DisplayMode.dspMenu)){
												objQuestion.displayMode = DisplayMode.dspVertical;
											}
										}
									}
									
									
									objQuestion.optionsByID = new Object();
									objQuestion.optionsOrderByID = new Array();
									for (var intOptionNum in objQuestion.optionsOrder) {
										var strOptionName = objQuestion.optionsOrder[intOptionNum];
										$.extend(objQuestion.options[strOptionName], new OptionCls());
										var objOption = objQuestion.options[strOptionName];
										if (objOption) {
											objQuestion.optionsByID[objOption.id] = objOption;
											objQuestion.optionsOrderByID.push(objOption.id);
										}
									}
									
									//JAPR 2016-04-18: Modificada la pregunta simple choice para tener atributos imagen y opción de salto de sección
									for (var intSkipRuleID in objQuestion.skipRules) {
										$.extend(objQuestion.skipRules[intSkipRuleID], new SkipRuleCls());
									}
									//JAPR
									
									//Sobreescribe la instancia de selSurvey para mantener una referencia y permitir modificar sólo en un lugar
									if (selSurvey.questions && selSurvey.questions[objQuestion.id]) {
										selSurvey.questions[objQuestion.id] = objQuestion;
									}
								}
							}
						}
					}
				}
				
				/*
				if (selSurvey.questions) {
					for (var intQuestionID in selSurvey.questions) {
						$.extend(selSurvey.questions[intQuestionID], new QuestionCls());
						//Agrega una nueva colección de options indexadas por el consecutiveID, ya que el preview las requiere indexadas por el name
						var objQuestion = selSurvey.questions[intQuestionID];
						if (objQuestion) {
							objQuestion.optionsByID = new Object();
							objQuestion.optionsOrderByID = new Array();
							for (var intOptionNum in objQuestion.optionsOrder) {
								var strOptionName = objQuestion.optionsOrder[intOptionNum];
								$.extend(objQuestion.options[strOptionName], new OptionCls());
								var objOption = objQuestion.options[strOptionName];
								if (objOption) {
									objQuestion.optionsByID[objOption.id] = objOption;
									objQuestion.optionsOrderByID.push(objOption.id);
								}
							}
						}
					}
				}
				*/
			}
			
			/* Restable el objeto indicado a la posición original después de un fallido intento de Drag & Drop debido a algún tipo de error en el server
			Esta función nunca se llamaría si el proceso hubiera terminado correctamente, en ese caso simplemente se habría realizado un refresh con lo cual se
			vuelven a generar las diferentes partes de la ventana de diseño
			*/
			function doRestoreDroppedItem(iObjectType, iObjectID, iOldPos) {
				console.log("doRestoreDroppedItem iObjectType == " + iObjectType + ", iObjectID == " + iObjectID + ", iOldPos == " + iOldPos);
				
				//Remueve el progress que se activó al iniciar el proceso de Drag&Drop
				switch (iObjectType) {
					case AdminObjectType.otySection:
						objFormsLayout.cells(dhxLayOutCell).progressOff();
						break;
					case AdminObjectType.otyQuestion:
						objFormsLayout.cells(dhxLayOutCell).progressOff();
						break;
				}
				
				var objObject = getObject(iObjectType, iObjectID);
				if (!objObject) {
					return;
				}
				
				//Restaura la propiedad de posición original
				objObject.number = iOldPos;
				
				//Restaura el elemento en su componente correspondiente a la posición original
				//JAPR 2015-06-22: Agregado el reordenamiento de objetos
				//Se cancelará el Drop para que el component no lo haga, sino que sea el refresh del método de move el que finalmente regenere el
				//componente en caso de que termine todo ok, así que no es necesario realizar este Rollback
				/*
				switch (iObjectType) {
					case AdminObjectType.otySection:
						if (objSectionsDataView) {
							objSectionsDataView.move(iObjectID, iOldPos -1);
						}
						break;
				}
				*/
			}
			
			/* Procesa la respuesta de un request de reordenamiento de datos asíncrono y presenta el error en pantalla en caso de existir alguno
			Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error
			*/
			function doReorderConfirmation(loader) {
				console.log('doReorderConfirmation');
				
				$('#divSaving').hide();
				objFormsLayout.cells(dhxLayOutCell).progressOff();
				objFormsLayout.cells(dhxPreviewCell).progressOff();
				
				if (!loader || !loader.xmlDoc) {
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					if (doCancelDragDropOperation) {
						doCancelDragDropOperation();
					}
					return;
				}
				else {
					if (objResponse.warning) {
						console.log(objResponse.warning.desc);
					}
				}
				
				//La respuesta no tiene error, debió haber terminado de grabar correctamente
				doDeviceAction("refresh", AdminObjectType.otySection, giSectionID);
			}
			
			/* Procesa la respuesta de un request de actualización de datos asíncrona y presenta el error en pantalla en caso de existir alguno
			Esta función está pensada como un callback de un request Ajax que regresará un contenido en JSon con infomación adicional de error, sin embargo se puede recibir
			un callback adicional en el parámetro oCallbackFn
			El parámetro bRemoveProgress se usa generalmente sólo en requests Ajax y permite restablecer la ventana de diseño en caso de error, removiendo la progres bar del TabBar
			*/
			function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress) {
				console.log('doSaveConfirmation');
				
				$('#divSaving').hide();
				
				if (!loader || !loader.xmlDoc) {
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				//Si llega a este punto es que se recibió una respuesta correctamente
				var response = loader.xmlDoc.responseText;
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					if (bRemoveProgress) {
						objFormTabs.tabs(tabDesign).progressOff();
					}
					return;
				}
				else {
					//JAPR 2015-10-25: Agregado el proceso de confirmación de borrado cuando existen referencias
					if (objResponse.confirm && objResponse.confirm.desc) {
						//Si se pidió confirmación, pide al usuario autorización y si se acepta, repite el proceso de borrado pero esta vez ya sin validaciones
						var answer = confirm(objResponse.confirm.desc);
						if (answer) {
							if (goObjectToDelete) {
								goObjectToDelete.SkipValidation = 1;
							}
							
							setTimeout(function() {
								doDeleteObject(goObjectToDelete.objectType, goObjectToDelete.objectID, goObjectToDelete.selObjectType, goObjectToDelete.selObjectID, goObjectToDelete.requestType, goObjectToDelete.callbackFn, goObjectToDelete.removeProcess);
							}, 100);
							return;
						}
						
						//En este caso simplemente quita la barra de progreso para que todo regrese a la normalidad y ya no continua con el proceso
						if (bRemoveProgress) {
							objFormTabs.tabs(tabDesign).progressOff();
						}
						return;
					}
					else {
						if (objResponse.warning) {
							console.log(objResponse.warning.desc);
						}
					}
					//JAPR
				}
				
				//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
				//pueda ejecutar un código de callback
				if (objResponse.objectType) {
					if (objResponse.deleted) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse);
						}
						else {
							if (bRemoveProgress) {
								objFormTabs.tabs(tabDesign).progressOff();
							}
						}
					}
					return;
				}
				else {
					if (objResponse.newObject) {
						if (oCallbackFn && typeof oCallbackFn == "function") {
							oCallbackFn(objResponse.newObject);
						}
					}
				}
			}
			
			/* Manda los datos al server para ser procesados
			El parámetro oSuccess permite especificar la función que se debe invocar al terminar el proceso de grabado en caso de éxito
			*/
			function doSendData(oSuccess) {
				console.log('doSendData');
				if (!objDialog) {
					return;
				}
				
				objDialog.progressOn();
				var objForm = objDialog.getAttachedObject();
				var objData = objForm.getFormData();
				objForm.lock();
				objForm.send("processRequest.php", "post", function(loader, response) {
					if (!loader || !response) {
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					setTimeout(function () {
						try {
							var objResponse = JSON.parse(response);
						} catch(e) {
							alert(e + "\r\n" + response.substr(0, 1000));
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						
						if (objResponse.error) {
							alert(objResponse.error.desc);
							objDialog.progressOff();
							objForm.unlock();
							return;
						}
						else {
							if (objResponse.warning) {
								alert(objResponse.warning.desc);
							}
						}
						
						//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
						doUnloadDialog();
						if (oSuccess) {
							try {
								//Se enviará la respuesta obtenida, ya que ella traería la nueva definición del objeto a utilizar
								oSuccess(objResponse);
							} catch(e) {
								console.log("Error executing doSendData callback: " + e);
							}
						}
					}, 100);
				});
			}
			
			//Muestra el diálogo para crear una sección
			function addNewSection() {
				console.log('addNewSection');
				
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					width:intDialogWidth,
					height:intDialogHeight,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("New section")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					return true;
				});
				
				/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
				var objFormData = [
					{type:"settings"/*, offsetLeft:20*/},
					{type:"input", name:"SectionName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
					{type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]},
					{type:"input", name:"SectionID", value:-1, hidden:true},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true},
					{type:"input", name:"SectionType", value:SectionType.Standard, hidden:true},
					{type:"input", name:"Design", value:1, hidden:true},
					{type:"input", name:"RequestType", value:reqAjax, hidden:true},
					{type:"input", name:"Process", value:"Add", hidden:true},
					{type:"input", name:"ObjectType", value:AdminObjectType.otySection, hidden:true}
				];
				
				var objForm = objDialog.attachForm(objFormData);
				objForm.adjustParentSize();
				objForm.setItemFocus("SectionName");
				objForm.attachEvent("onBeforeValidate", function (id) {
					console.log('Before validating the section: id == ' + id);
				});
				
				objForm.attachEvent("onAfterValidate", function (status) {
					console.log('After validating the section: status == ' + status);
				});
				
				objForm.attachEvent("onValidateSuccess", function (name, value, result) {
					console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
				});
				
				objForm.attachEvent("onValidateError", function (name, value, result) {
					console.log('Error validating the section: name == ' + name + ', value == ' + value + ', result == ' + result);
				});
				
				objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
					if (!name || !inp || !ev || !ev.keyCode) {
						return;
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					console.log('objForm.onButtonClick ' + name);
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
							
						case "btnOk":
							setTimeout(function() {
								if (objForm.validate()) {
									doSendData(function(oResponse) {
										//Al terminar de grabar con éxito, debe agregar al objeto en memoria la nueva sección creada, además de regenerar la dataView de secciones
										//con la nueva sección como seleccionada y presentar la lista de propiedades para que permita ser modificada
										
										//Se recibirá como parámetro la respuesta del server en la cual debe venir la nueva definición (para este punto ya se validó que no hubiera
										//algún error en el request, así que debe ser una respuesta válida)
										if (!oResponse || !oResponse.newObject) {
											console.log("<?=translate("No response object was received during this process")?>");
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otySection) {
											return
										}
										
										//JAPR 2015-07-30: Corregido un bug, faltaba extender la clase del objeto recien agregado
										$.extend(objNewObject, new SectionCls());
										//JAPR
										
										//Agrega el nuevo elemento a la forma en memoria
										selSurvey.sectionsOrder.push(objNewObject.id);
										selSurvey.sections[objNewObject.id] = objNewObject;
										
										//Vuelve a generar la data view de secciones y la coloca como la seleccionada
										giSectionID = objNewObject.id;
										doDrawSections();
										
										//Invoca al refresh del preview para agregar la nueva sección
										doDeviceAction("refresh", AdminObjectType.otySection, giSectionID);
										
										//Cambia la ventana de propiedades hacia la nueva sección
										//doShowSectionProps("s" + giSectionID);
									});
								}
							}, 100);
							break;
					}
				});
			}
			
			/* Prepara el array de fields para la forma de creación de preguntas basado en el tipo de pregunta especificado */
			function getDefaultQuestionFields(iQTypeIDExt) {
				var blnAddMessage = true;
				var blnAddMandatory = true;
				var blnAddComments = true;
				var blnAddPhoto = true;
				var blnSavePhoto = false;
				var blnAllowGallery = false;
				var blnQuestionList = false;
				var intQTypeID = iQTypeIDExt;
				var intQDisplayMode = <?=dspEntry?>;
				//Campos capturables adicionales a los configurados mediante switches comunes
				var arrFields = new Array();
				//Campos ocultos que mandarán información según el tipo de pregunta
				var arrHiddenFields = new Array();
				var objFormData = new Array();
				//Configuración de teclas disponibles para eventos (originalmente usada para bloquear que en el evento keyup el <enter> funcionara como submit). Se definen
				//mediante el índice del campo (el name) que contiene un objeto donde el keyCode como índice indica si la tecla está o no disponible (default=true), ej:
				//objKeys = {"name":{13:false}};	//Indica que para el campo "name", la tecla <enter> (13) no está disponible, así que no lanzará el submit en el evento keyup
				var objKeys = new Object();
				//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
				var objButtonsMod = new Object();	//Modificador de posición de la botonera (es necesario para el caso de preguntas Mensaje y HTML, debido al redactor)
				//JAPR
				
				switch (iQTypeIDExt) {
					case QTypesExt.menuMenu:
					case QTypesExt.menuVert:
					case QTypesExt.menuHoriz:
					case QTypesExt.simpleMenu:
					case QTypesExt.simpleVert:
					case QTypesExt.simpleHoriz:
						switch (iQTypeIDExt) {
							case QTypesExt.menuMenu:
							case QTypesExt.simpleMenu:
								intQDisplayMode = DisplayMode.dspMenu;
								//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú
								if (iQTypeIDExt == QTypesExt.menuMenu) {
									blnAddMandatory = false;
									blnAddPhoto = false;
									blnAddComments = false;
									arrHiddenFields.push({type:"input", name:"NoFlow", value:1, hidden:true});
								}
								break;
							case QTypesExt.menuVert:
							case QTypesExt.simpleVert:
								intQDisplayMode = DisplayMode.dspVertical;
								//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú
								if (iQTypeIDExt == QTypesExt.menuVert) {
									blnAddMandatory = false;
									blnAddPhoto = false;
									blnAddComments = false;
									arrHiddenFields.push({type:"input", name:"NoFlow", value:1, hidden:true});
								}
								break;
							case QTypesExt.menuHoriz:
							case QTypesExt.simpleHoriz:
								intQDisplayMode = DisplayMode.dspHorizontal;
								//OMMC 2016-04-21: Agregada la propiedad para no alterar el flujo de captura cuando son preguntas Menú
								if (iQTypeIDExt == QTypesExt.menuHoriz) {
									blnAddMandatory = false;
									blnAddPhoto = false;
									blnAddComments = false;
									arrHiddenFields.push({type:"input", name:"NoFlow", value:1, hidden:true});
								}
								break;
						}
						
						intQTypeID = Type.simplechoice;
						//Se incluye un input para los valores iniciales de la pregunta
						arrFields.push({type:"input", name:"StrSelectOptions", label:"<?=translate("Values")?>", value:"", labelWidth:100, inputWidth:320, rows:5, /*required:true, validate:"NotEmpty",*/
							note:{text:"<?=addslashes(translate("Enter the list of values for single selection separated by <enter> character"))?>"}
						});
						arrHiddenFields.push({type:"input", name:"UseCatalog", value:0, hidden:true});
						objKeys["StrSelectOptions"] = {13:false};
						break;
						
					case QTypesExt.simpleAuto:
					case QTypesExt.simpleMap:
					case QTypesExt.simpleGetData:
					//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
					case QTypesExt.simpleMetro:
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					case QTypesExt.simpleAR:
						intQTypeID = Type.simplechoice;
						intQDisplayMode = DisplayMode.dspAutocomplete;
						//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
						//JAPR 2016-08-09: Corregido un bug, la condición de la pregunta Metro se había comparado contra el tipo de despliegue en lugar del tipo de pregunta
						if (iQTypeIDExt == QTypesExt.simpleGetData || iQTypeIDExt == QTypesExt.simpleMetro) {
							intQDisplayMode = DisplayMode.dspGetData;
							//JAPR 2016-08-09: Validado que la pregunta metro si pueda configurarse como requerida (#XRKG3R)
							if (iQTypeIDExt == QTypesExt.simpleGetData) {
								blnAddMandatory = false;
							}
							//JAPR
							blnAddPhoto = false;
							blnAddComments = false;
						} 
						
						//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
						arrFields.push({type:"combo", name:"DataSourceID", label:"<?=translate("Catalog")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogItems(true)});
						arrFields.push({type:"combo", name:"DataSourceMemberID", label:"<?=translate("Attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
						
						switch (iQTypeIDExt) {
							case QTypesExt.simpleMap:
								//JAPR 2015-07-28: Las preguntas AutoComplete se marcan como vertical, aunque otro tipo de despliegue no aplica, causaría problemas, especialmente AutoComplete
								intQDisplayMode = DisplayMode.dspVertical;
								//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
								//JAPR 2015-07-31: Corregido un bug, estaba mal el nombre de los campos (#31H112)
								arrFields.push({type:"combo", name:"DataMemberLatitudeID", label:"<?=translate("Latitude attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								arrFields.push({type:"combo", name:"DataMemberLongitudeID", label:"<?=translate("Longitude attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								//arrFields.push({type:"combo", name:"CatMemberLatitudeID", label:"<?=translate("Latitude attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								//arrFields.push({type:"combo", name:"CatMemberLongitudeID", label:"<?=translate("Longitude attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								arrHiddenFields.push({type:"input", name:"UseMap", value:1, hidden:true});
								break;
							//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
							case QTypesExt.simpleMetro:
								intQDisplayMode = DisplayMode.dspMetro;
								arrFields.push({type:"combo", name:"DataMemberImageID", label:"<?=translate("Attribute").": ".translate("Image")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true, {isImage: 1})});
								break;
							//OMMC 2019-03-25: Pregunta AR #4HNJD9
							case QTypesExt.simpleAR:
								blnAddMandatory = true;
								blnAddComments = true;
								blnAddPhoto = false;
								intQDisplayMode = DisplayMode.dspAR;
								arrFields.push({type:"combo", name:"DataMemberZipFileID", label:"<?=translate("Zip File")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								arrFields.push({type:"combo", name:"DataMemberVersionID", label:"<?=translate("Version")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								arrFields.push({type:"combo", name:"DataMemberPlatformID", label:"<?=translate("Platform")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
								arrFields.push({type:"combo", name:"DataMemberTitleID", label:"<?=translate("Title")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
							break;
						}
						arrHiddenFields.push({type:"input", name:"UseCatalog", value:1, hidden:true});
						arrHiddenFields.push({type:"input", name:"ValuesSourceType", value:1, hidden:true});
						break;
						
					case QTypesExt.multiMenu:
					case QTypesExt.multiVert:
					case QTypesExt.multiHoriz:
					case QTypesExt.multiRank:
					//JAPR 2016-04-11: Corregido un bug, en lo que se permiten catálogos en múltiple choice, este tipo debe permitir captura de valores directos (#UPZ4KE)
					case QTypesExt.multiAuto:
						switch (iQTypeIDExt) {
							case QTypesExt.multiMenu:
								intQDisplayMode = DisplayMode.dspMenu;
								break;
							case QTypesExt.multiVert:
								intQDisplayMode = DisplayMode.dspVertical;
								break;
							case QTypesExt.multiHoriz:
								intQDisplayMode = DisplayMode.dspHorizontal;
								break;
							case QTypesExt.multiRank:
								intQDisplayMode = DisplayMode.dspLabelnum;
								break;
							//JAPR 2016-04-11: Corregido un bug, en lo que se permiten catálogos en múltiple choice, este tipo debe permitir captura de valores directos (#UPZ4KE)
							case QTypesExt.multiAuto:
								intQDisplayMode = DisplayMode.dspAutocomplete;
								break;
						}
						
						intQTypeID = Type.multiplechoice;
						//Se incluye un input para los valores iniciales de la pregunta
						arrFields.push({type:"input", name:"StrSelectOptions", label:"<?=translate("Values")?>", value:"", labelWidth:100, inputWidth:320, rows:5, /*required:true, validate:"NotEmpty",*/
							note:{text:"<?=addslashes(translate("Enter the list of values for single selection separated by <enter> character"))?>"}
						});
						arrHiddenFields.push({type:"input", name:"UseCatalog", value:0, hidden:true});
						objKeys["StrSelectOptions"] = {13:false};
						break;
						
					//JAPR 2016-04-11: Corregido un bug, en lo que se permiten catálogos en múltiple choice, este tipo debe permitir captura de valores directos (#UPZ4KE)
					/*case QTypesExt.multiAuto:
						intQTypeID = Type.multiplechoice;
						intQDisplayMode = DisplayMode.dspAutocomplete;
						//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
						//JAPR 2015-07-31: Se remueve el catálogo de las preguntas multiple choice auto-complete porque no aplica
						//arrFields.push({type:"combo", name:"DataSourceID", label:"<?=translate("Catalog")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogItems(true)});
						//arrFields.push({type:"combo", name:"DataSourceMemberID", label:"<?=translate("Attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
						//JAPR
						//arrFields.push({type:"combo", name:"CatalogID", label:"<?=translate("Catalog")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogItems(true)});
						//arrFields.push({type:"combo", name:"CatMemberID", label:"<?=translate("Attribute")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboCatalogMemberItems(0, true)});
						arrHiddenFields.push({type:"input", name:"UseCatalog", value:1, hidden:true});
						break;*/
						
					case QTypesExt.photo:
						blnAddPhoto = false;
						break;
					case QTypesExt.image:
						//OMMC 2017-01-12: Faltó agregar el cambio de tipo de pregunta.
						intQTypeID = Type.photo;
						intQDisplayMode = DisplayMode.dspImage;
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						//OMMC 2017-01-18: Agregados los campos de tamaño y valor default en la creación de la pregunta.
						arrFields.push({type:"input", name:"imageHeight", label:"<?=translate("Height")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320});
						arrFields.push({type:"input", name:"imageWidth", label:"<?=translate("Width")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320});
						arrFields.push({type:"input", name:"DefaultValue", label:"<?=translate("Default value")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320, rows:5});
						arrFields.push({type:"button", name: "btnFormulaEd", value:"", className:"formulaButtonRight"});
						arrHiddenFields.push({type:"input", name:"ReadOnly", value:1, hidden:true});
						break;
					case QTypesExt.audio:
					case QTypesExt.video:
					case QTypesExt.document:
					case QTypesExt.sketch:
					//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case QTypesExt.sketchPlus:
						blnAddPhoto = false;
						
						//JAPR 2019-02-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						if ( iQTypeIDExt == QTypesExt.sketchPlus) {
							arrFields.push({type:"input", name:"sketchImageHeight", label:"<?=translate("Height")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320});
							arrFields.push({type:"input", name:"sketchImageWidth", label:"<?=translate("Width")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320});
						}
						//JAPR
						break;
					
					case QTypesExt.ocr:
						blnAddMandatory = false;
						arrFields.push({type:"checkbox", name:"SavePhoto", label: "<?=translate("Save photo")?>", checked:false, position:"label-right", offsetLeft:100});
						arrHiddenFields.push({type:"input", name:"canvasHeight", value:300, hidden:true});
						blnAddPhoto = false;
						break;
						
					case QTypesExt.sync:
					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					//Estos tipos de pregunta no tienen campos adicionales, así que se pueden comportar todos igual
					case QTypesExt.exit:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						break;
						
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					case QTypesExt.updateDest:
						arrFields.push({type:"combo", name:"DataDestinationID", label:"<?=translate("Data destination")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboDataDestinationsItems(true)});
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						break;
						
					case QTypesExt.skipsection:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						arrFields.push({type:"combo", name:"GoToQuestion", label:"<?=translate("Section")?>", labelWidth:100, inputWidth:320, options:QuestionCls.prototype.getComboSectionItems(true)});
						break;
						
					case QTypesExt.calc:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						arrFields.push({type:"input", name:"Formula", label:"<?=translate("Formula")?>", position:"label-top", value:"", labelWidth:100, inputWidth:420, rows:5});
						//JAPR 2015-08-01: Agregado el botón para ligar la fórmula con el editor (#4Y7EB7)
						arrFields.push({type:"button", name: "btnFormulaEd", value:"", className:"formulaButtonRight"});
						break;
						
					case QTypesExt.signature:
						blnAddPhoto = false;
						blnAddComments = false;
						break;
						
					case QTypesExt.message:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
						//arrFields.push({type:"input", name:"QuestionMessage", label:"<?=translate("Message")?>", position:"label-top", value:"", labelWidth:100, inputWidth:420, rows:10, required:true, validate:"NotEmpty"});
						//arrFields.push({type:"button", name: "btnHTMLEd", value:"", className:"editorHTMLButtonRight"});
						arrFields.push({type:"editorHTMLForm", name: "QuestionMessage", value:"", redactorButtons:['bold', 'italic', 'alignment', 'image', 'file']});
						objButtonsMod['offsetTop'] = 85;	//Esta es la cantidad de px que revisando en la consola se necesitaba recorrer la botonera para que se viera ok sólo con un redactor arriba
						//OMMC 2016-01-21: Antigua implementación de la pregunta mensaje (no eliminar código)
						//arrFields.push({type:"editor", name:"QuestionMessage", className:"editor_compact", label:"<?=translate("Message")?>", position:"label-top", value:"", labelWidth:300, inputWidth:420, inputHeight:300, toolbar:true, iconsPath:"images/editor", required:true, validate:"NotEmpty"});
						objKeys["QuestionMessage"] = {13:false};
						//JAPR 2015-09-01: Por instrucción de DAbdo, se oculta el nombre de pregunta para este tipo, sólo se configurará el HTML/Texto con Formato, y shortname
						blnAddMessage = false;
						break;
					
					//JAPR 2015-08-15: Agregado el ipo de pregunta HTML
					case QTypesExt.html:
						intQTypeID = Type.message;
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
						//arrFields.push({type:"input", name:"HTMLCode", label:"<?=translate("HTML Code")?>", position:"label-top", value:"", labelWidth:100, inputWidth:420, rows:10, required:true, validate:"NotEmpty"});
						//arrFields.push({type:"button", name: "btnHTMLEd", value:"", className:"editorHTMLButtonRight"});
						arrFields.push({type:"editorHTMLForm", name: "HTMLCode", value:"", redactorButtons:['html', 'bold', 'italic', 'alignment', 'image', 'file']});
						objButtonsMod['offsetTop'] = 85;	//Esta es la cantidad de px que revisando en la consola se necesitaba recorrer la botonera para que se viera ok sólo con un redactor arriba
						arrHiddenFields.push({type:"input", name:"EditorType", value:0, hidden:true});
						objKeys["HTMLCode"] = {13:false};
						//JAPR 2015-09-01: Por instrucción de DAbdo, se oculta el nombre de pregunta para este tipo, sólo se configurará el HTML/Texto con Formato, y shortname
						blnAddMessage = false;
						break;
					//JAPR
						
					case QTypesExt.callList:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						//JAPR 2015-09-28: Corregido un bug, validado que sólo se pueda grabar un número telefónico (#FZ43UJ)
						arrFields.push({type:"input", name:"StrSelectOptions", label:"<?=translate("Phone number")?>", value:"", labelWidth:100, inputWidth:320, required:true, validate:"NotEmpty,ValidTelephone"});
						break;
					
					case QTypesExt.barCode:
						blnAddComments = false;
						break;
					
					case QTypesExt.gps:
						blnAddMessage = false;
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						arrFields.push({type:"input", name:"AttributeName", label:"<?=translate("Short Name")?>", value:"", labelWidth:100, inputWidth:320, required:true, validate:"NotEmpty"});
						arrFields.push({type:"combo", name:"RegisterGPSAt", label:"<?=translate("REGISTERGPSDATAAT")?>", labelWidth:100, inputWidth:320, options:optionsRegisterGPSAtOptsNew});
						break;
					//OMMC 2016-04-20: Agregado el tipo de pregunta Mi localización.
					case QTypesExt.myLocation:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						arrHiddenFields.push({type:"input", name:"UseMap", value:1, hidden:true});
						break;
					
					case QTypesExt.openPassword:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						break;
					
					//JAPR 2015-08-22: Agregado el soporte para secciones Inline y dinámicas en v6
					case QTypesExt.section:
						blnAddMandatory = false;
						blnAddPhoto = false;
						blnAddComments = false;
						arrFields.push({type:"combo", name:"SourceSectionID", label:"<?=translate("Section")?>", labelWidth:100, inputWidth:320, options:selSurvey.getComboUnusedInlineSectionItems(true)});
						break;
					
					//Estos tipos de preguna se comportan igual que el default:
					case QTypesExt.openNumber:
					case QTypesExt.openAlpha:
					case QTypesExt.openDate:
					case QTypesExt.openTime:
					case QTypesExt.openText:
					default:
						//El tipo default es Open Text
						break;
				}
				
				objFormData.push({type:"settings"/*, offsetLeft:20*/});
				//La etiqueta de la pregunta (común a la mayoría de los componentes)
				if (blnAddMessage) {
					objFormData.push({type:"editor", name:"QuestionTextHTML", className:"editor_compact", label:"<?=translate("Message")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:320, inputHeight:80, required:true, validate:"NotEmpty", toolbar:true, iconsPath:"images/editor"});
					objKeys["QuestionText"] = {13:false};
				}
				
				//Componentes específicos por tipo de pregunta
				for (var intIndex in arrFields) {
					var objField = arrFields[intIndex];
					objFormData.push(objField);
				}
				
				//Switches comunes
				if (blnAddMandatory) {
					objFormData.push({type:"checkbox", name:"IsReqQuestion", label: "<?=translate("Required")?>", checked:false, position:"label-right", offsetLeft:100, labelLeft:50});
				}
				if (blnAddComments) {
					objFormData.push({type:"checkbox", name:"HasReqComment", label: "<?=translate("Comments")?>", checked:false, position:"label-right", offsetLeft:100});
				}
				if (blnAddPhoto) {
					objFormData.push({type:"checkbox", name:"HasReqPhoto", label: "<?=translate("Photo")?>", checked:false, position:"label-right", offsetLeft:100});
				}
				
				//Botones
				//JAPR 2016-06-20: Integrado el EditorHTML directo en la forma de creación en lugar de en un diálogo nuevo (#TY8HOB)
				objFormData.push($.extend({type:"block", blockOffset:0, offsetLeft:100, list:[
						{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
						{type:"newcolumn"},
						{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
					]
				}, objButtonsMod));
				
				//Campos ocultos con información fija para grabar la pregunta
				objFormData.splice(objFormData.length, 0, 
					{type:"input", name:"QuestionID", value:-1, hidden:true},
					{type:"input", name:"SectionID", value:giSectionID, hidden:true},
					{type:"input", name:"SurveyID", value:intSurveyID, hidden:true},
					{type:"input", name:"QTypeID", value:intQTypeID, hidden:true},
					{type:"input", name:"QDisplayMode", value:intQDisplayMode, hidden:true},
					{type:"input", name:"QComponentStyleID", value:0, hidden:true},
					{type:"input", name:"Design", value:1, hidden:true},
					{type:"input", name:"RequestType", value:reqAjax, hidden:true},
					{type:"input", name:"Process", value:"Add", hidden:true},
					{type:"input", name:"ObjectType", value:AdminObjectType.otyQuestion, hidden:true}
				);
				
				//Campos ocultos adicionales según el tipo de pregunta
				for (var intIndex in arrHiddenFields) {
					var objField = arrHiddenFields[intIndex];
					objFormData.push(objField);
				}
				
				var objFieldsDef = new Object();
				objFieldsDef.fields = objFormData;
				if (!$.isEmptyObject(objKeys)) {
					objFieldsDef.keys = objKeys;
				}
				return objFieldsDef;
			}
			
			//Muestra el diálogo para crear una pregunta
			function addNewQuestion(oFormData, qTypeID) {
				console.log('addNewQuestion');
				
				if (!giSectionID) {
					alert("<?=translate("Please select a section to create a new question")?>");
					return;
				}
				
				if (!objWindows) {
					//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
				
				if(!qTypeID){
					qTypeID = 0;
				}
				
				objDialog = objWindows.createWindow({
					id:"newObject",
					left:0,
					top:0,
					//width:intDialogWidth,
					//height:intDialogHeight,
					width:500,
					height:300,
					center:true,
					modal:true
				});
				
				objDialog.setText("<?=translate("New question")?>");
				objDialog.denyPark();
				objDialog.denyResize();
				objDialog.setIconCss("without_icon");
				
				//Al cerrar realizará el cambio de sección de la pregunta
				objDialog.attachEvent("onClose", function() {
					//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
					if ( this.dataObj && this.dataObj.getForm ) {
						var objForm = this.dataObj.getForm();
						if ( objForm ) {
							objForm.forEachItem(function (name) {
								var strType = objForm.getItemType(name);
								if (strType == "editor") {
									var objEditor = objForm.getEditor(name);
									if (objEditor && objEditor.tb && objEditor.tb.objPopUpsColl) {
										for (var strPopUpName in objEditor.tb.objPopUpsColl) {
											var objPopUp = objEditor.tb.objPopUpsColl[strPopUpName];
											if (objPopUp) {
												if (objPopUp.bitEventHandler) {
													objPopUp.detachEvent(objPopUp.bitEventHandler);
												}
												objPopUp.unload();
											}				
										}
									}
								}
							});
						}
					}
					//JAPR
					return true;
				});
				
				var objForm = objDialog.attachForm(oFormData.fields);
				objForm.adjustParentSize();
				objForm.setItemFocus("QuestionTextHTML");
				objDialog.centerOnScreen();
				
				fnFormKeyUp = function(inp, ev, name, value) {
					if (!name || !ev || !ev.keyCode) {
						return;
					}
					
					//Verifica si la tecla está o no permitida según la configuración del campo del evento
					if (oFormData.keys && oFormData.keys[name]) {
						var objKeys = oFormData.keys[name];
						if (objKeys[ev.keyCode] !== undefined) {
							if (!objKeys[ev.keyCode]) {
								ev.keyCode = 0;
								return;
							}
						}
					}
					
					switch (ev.keyCode) {
						case 13:
							//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
							break;
						case 27:
							setTimeout(function() {
								objForm.callEvent("onButtonClick", ["btnCancel"])
							}, 100);
							break;
					}
				}
				
				objForm.attachEvent("onKeyup", fnFormKeyUp);
				
				objForm.forEachItem(function (name) {
					var strType = objForm.getItemType(name);
					if (strType == "editor") {
						var objEditor = objForm.getEditor(name);
						if (objEditor) {
							objEditor.attachEvent("onAccess", function(eventName, evObj){
								if (eventName == "keyup") {
									fnFormKeyUp(undefined, evObj, name, undefined);
								}
							});
							var	customButtons = new Object();
							customButtons = {
								"backColor":{label:"<?=translate("BackColor")?>",type:"buttonColor",img:"higlighttext.gif"},
								"foreColor":{label:"<?=translate("ForeColor")?>",type:"buttonColor",img:"fontColor.gif"},
								"fontName":{label:"<?=translate("FontName")?>",type:"combo",img:"fontName.gif"},
								"fontSize":{label:"<?=translate("FontSize")?>",type:"combo",img:"fontSize.gif"},
								"formEdit":{label:"<?=translate("Edit formula")?>",type:"button",img:"editFormula.gif"},
								"uploadImg":{label:"<?=translate("Image")?>",type:"uploadImg",img:"insertImg.png"}
							};
							changeEditor(objEditor, customButtons, undefined, name, "dialogForm");
						}
					}
				});
				
				objForm.attachEvent("onButtonClick", function(name) {
					console.log('objForm.onButtonClick ' + name);
					switch (name) {
						case "btnCancel":
							setTimeout(function () {
								doUnloadDialog();
							}, 100);
							break;
						
						//JAPR 2015-08-01: Agregado el botón para ligar la fórmula con el editor (#4Y7EB7)
						//OMMC 2017-01-19: Agregado el campo de default value (#XJCO2G)
						case "btnFormulaEd":
							var objInput = this.getInput("Formula") || this.getInput("DefaultValue");
							if (objInput) {
								fnOpenFormulaEditor(objInput, objInput.value);
							}
							break;
						case "btnHTMLEd":
							if(qTypeID == QTypesExt.message){
								var objInput = this.getInput("QuestionMessage");
							}else{
								var objInput = this.getInput("HTMLCode");
							}
							
							if (objInput) {
								fnOpenEditorHTML(objInput, objInput.value, qTypeID);
							}
							break;
						case "btnOk":
							setTimeout(function() {
								//JAPR 2016-06-28: Corregido un bug, al entrar a modo HTML con el Redactor, no se lanzan los eventos de blur o change, así que si en dicho estado se presionaba Ok
								//en la forma que lo contenía o se hacía cualquier evento que cerrara el redactos, sus cambios no se grababan (#IEJDXN)
								//Para corregir este problema, se almacenará en el objeto que representa los campos personalizados de la forma, una referencia al objeto redactos para poder actualizar
								//su texto justo antes de terminar de usar la forma
								objForm.forEachItem(function (name) {
									var strType = objForm.getItemType(name);
									if (strType == "editorHTMLForm") {
										if (objForm && objForm.items && objForm.items.editorHTMLForm && objForm.items.editorHTMLForm.bitObjs) {
											var objRedactorObj = objForm.items.editorHTMLForm.bitObjs[name];
											if (objRedactorObj) {
												objRedactorObj.HTMLcodeCache = objRedactorObj.code.get();
												objRedactorObj.code.set(objRedactorObj.code.get());
											}
										}
									}
								});
								//JAPR
								
								if (objForm.validate()) {
									//JAPR 2015-09-01: Corregido un bug, las preguntas tipo Mensaje y HTML generarán un parámetro en undefined para la función setImageSize lo cual
									//impedirá que funcione mas adelante, así que se reemplaza el undefined por el tab correcto
									objForm.forEachItem(function (name) {
										var strType = objForm.getItemType(name);
										if (strType == "editor") {
											var objEditor = objForm.getEditor(name);
											if (objEditor) {
												var strContent = objEditor.getContent();
												strContent = strContent.replace(new RegExp(RegExp.quote("'bitamundefinedbit'"), "gi"), "'generalTab'");
												objEditor.setContent(strContent);
											}
										}
									});
									//JAPR
									
									doSendData(function(oResponse) {
										//Al terminar de grabar con éxito, debe agregar al objeto en memoria la nueva sección creada, además de regenerar la dataView de secciones
										//con la nueva sección como seleccionada y presentar la lista de propiedades para que permita ser modificada
										
										//Se recibirá como parámetro la respuesta del server en la cual debe venir la nueva definición (para este punto ya se validó que no hubiera
										//algún error en el request, así que debe ser una respuesta válida)
										if (!oResponse || !oResponse.newObject) {
											console.log("<?=translate("No response object was received during this process")?>");
											return;
										}
										
										var objNewObject = oResponse.newObject;
										//Identifica que sea realmente un objeto del tipo esperado
										if (!objNewObject.id || objNewObject.objectType != AdminObjectType.otyQuestion) {
											return
										}
										
										//JAPR 2015-07-30: Corregido un bug, faltaba extender la clase del objeto recien agregado
										$.extend(objNewObject, new QuestionCls());
										//JAPR
										
										//Agrega el nuevo elemento a la forma en memoria
										giSectionID = objNewObject.sectionID;
										selSurvey.sectionsOrder.push(objNewObject.id);
										selSurvey.questions[objNewObject.id] = objNewObject;
										var objSection = selSurvey.sections[giSectionID];
										if (objSection) {
											if (objSection.questions) {
												objSection.questions[objNewObject.id] = objNewObject;
											}
											if (objSection.questionsOrder) {
												objSection.questionsOrder.push(objNewObject.id);
											}
										}
										
										//Vuelve a generar la data view de secciones y la coloca como la seleccionada
										doDrawSections();
										
										//JAPR 2015-07-31: Corregida la redirección al crear una pregunta (#J74JKP)
										giQuestionID = objNewObject.id;
										//Invoca al refresh del preview para agregar la nueva pregunta
										doDeviceAction("refresh", AdminObjectType.otyQuestion, giQuestionID);
										
										//Cambia la ventana de propiedades hacia la nueva sección
										//doShowSectionProps("s" + giSectionID);
									});
								}
							}, 100);
							break;
					}
				});
				
				//Modifica el contenido de las combos con dependencias
				//JAPR 2015-07-18: Modificado para utilizar DataSources en lugar de catálogos originales
				var objCatalogCombo = objForm.getCombo("DataSourceID");
				var objAttributeCombo = objForm.getCombo("DataSourceMemberID");
				//JAPR 2015-07-31: Corregido un bug, estaba mal el nombre de los campos (#31H112)
				var objLatitudeCombo = objForm.getCombo("DataMemberLatitudeID");
				var objLongitudeCombo = objForm.getCombo("DataMemberLongitudeID");
				//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
				var objImageCombo = objForm.getCombo("DataMemberImageID");
				//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
				var objOrderCombo = objForm.getCombo("DataMemberOrderID");
				//JAPR
				//OMMC 2019-03-25: Pregunta AR #4HNJD9
				var objZipFileCombo = objForm.getCombo("DataMemberZipFileID");
				var objVersionCombo = objForm.getCombo("DataMemberVersionID");
				var objPlatformCombo = objForm.getCombo("DataMemberPlatformID");
				var objTitleCombo = objForm.getCombo("DataMemberTitleID");
				
				if (objCatalogCombo && (objAttributeCombo || objLatitudeCombo || objLongitudeCombo || objZipFileCombo || objVersionCombo || objPlatformCombo || objTitleCombo)) {
					//Función que permite modificar el contenido de la combo hija cuando cambia la combo padre (Catalogo -> Atributo)
					objCatalogCombo.attachEvent("onChange", function(value) {
						console.log("objCatalogCombo.onChange " + value);
						
						if (objAttributeCombo) {
							objAttributeCombo.clearAll();
							objAttributeCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							//JAPR 2016-08-09: Agregado el limpiado de la selección previa (#LQPS8F)
							objAttributeCombo.selectOption(0);
						}
						if (objLatitudeCombo) {
							objLatitudeCombo.clearAll();
							objLatitudeCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							//JAPR 2016-08-09: Agregado el limpiado de la selección previa (#LQPS8F)
							objLatitudeCombo.selectOption(0);
						}
						if (objLongitudeCombo) {
							objLongitudeCombo.clearAll();
							objLongitudeCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							//JAPR 2016-08-09: Agregado el limpiado de la selección previa (#LQPS8F)
							objLongitudeCombo.selectOption(0);
						}
						//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
						if (objImageCombo) {
							objImageCombo.clearAll();
							objImageCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true, {isImage: 1}));
							//JAPR 2016-08-09: Agregado el limpiado de la selección previa (#LQPS8F)
							objImageCombo.selectOption(0);
						}
						//JAPR 2016-05-05: Modificada la pregunta simple choice Metro para tener un orden en base a un atributo (#XRKG3R)
						if (objOrderCombo) {
							objOrderCombo.clearAll();
							objOrderCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							//JAPR 2016-08-09: Agregado el limpiado de la selección previa (#LQPS8F)
							objOrderCombo.selectOption(0);
						}
						//JAPR
						//OMMC 2019-03-25: Pregunta AR #4HNJD9
						if (objZipFileCombo) {
							objZipFileCombo.clearAll();
							objZipFileCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							objZipFileCombo.selectOption(0);
						}
						if (objVersionCombo) {
							objVersionCombo.clearAll();
							objVersionCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							objVersionCombo.selectOption(0);
						}
						if (objPlatformCombo) {
							objPlatformCombo.clearAll();
							objPlatformCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							objPlatformCombo.selectOption(0);
						}
						if (objTitleCombo) {
							objTitleCombo.clearAll();
							objTitleCombo.addOption(QuestionCls.prototype.getComboCatalogMemberItems(value, true));
							objTitleCombo.selectOption(0);
						}
					});
				}
			}
			
			function getQuestionTypeDescByTypeExt(type, displayMode) {
				var strType = '';
				switch (type) {
					case QTypesExt.openNumber:
					{
						strType = '<?=translate("Number")?>';
						break;
					}
					case QTypesExt.simpleMenu:
					case QTypesExt.simpleGetData:
					{
						if(displayMode==DisplayMode.dspGetData)
						{
							strType = '<?=translate("Get data")?>';
						}
						else
						{
							strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Menu")?>'+')';
						}
						break;
					}
					case QTypesExt.simpleVert:
					{
						strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Vertical")?>'+')';
						break;
					}
					case QTypesExt.simpleHoriz:
					{
						strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Horizontal")?>'+')';
						break;
					}
					case QTypesExt.simpleAuto:
					{
						strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Autocomplete")?>'+')';
						break;
					}
					case QTypesExt.simpleMap:
					{
						strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Map")?>'+')';
						break;
					}
					//JAPR 2016-04-29: Agregado el tipo de despliegue Metro para preguntas simple choice (#XRKG3R)
					case QTypesExt.simpleMetro:
						strType = '<?=translate("Simple choice")?>'+' ('+'<?=translate("Metro")?>'+')';
						break;
					//JAPR
					//OMMC 2019-03-25: Pregunta AR #4HNJD9
					case QTypesExt.simpleAR:
						strType = '<?=translate("AR")?>';
					break;
					case QTypesExt.multiMenu:
					{
						strType = '<?=translate("Multiple choice")?>'+' ('+'<?=translate("Menu")?>'+')';
						break;
					}				
					case QTypesExt.multiVert:
					{
						strType = '<?=translate("Multiple choice")?>'+' ('+'<?=translate("Vertical")?>'+')';
						break;
					}				
					case QTypesExt.multiHoriz:
					{
						strType = '<?=translate("Multiple choice")?>'+' ('+'<?=translate("Horizontal")?>'+')';
						break;
					}				
					case QTypesExt.multiAuto:
					{
						strType = '<?=translate("Multiple choice")?>'+' ('+'<?=translate("Autocomplete")?>'+')';
						break;
					}				
					case QTypesExt.multiRank:
					{
						strType = '<?=translate("Multiple choice")?>'+' ('+'<?=translate("Ranking")?>'+')';
						break;
					}				
					case QTypesExt.openDate:
					{
						strType = '<?=translate("Date")?>';
						break;
					}
					case QTypesExt.openText:
					{
						strType = '<?=translate("Text")?>';
						break;
					}
					case QTypesExt.openAlpha:
					{
						strType = '<?=translate("Alphanumeric")?>';
						break;
					}
					case QTypesExt.photo:
					{
						<?//@JAPR 2017-01-16: Corregio el tipo de pregunta desplegado en la ventana de propiedades?>
						if (displayMode == DisplayMode.dspImage) {
							strType = '<?=translate("Image")?>';
						}
						else {
							strType = '<?=translate("Photo")?>';
						}
						break;
					}
					case QTypesExt.signature:
					{
						strType = '<?=translate("Signature")?>';
						break;
					}
					case QTypesExt.message:
					{
						strType = '<?=translate("Message")?>';
						break;
					}
					case QTypesExt.html:
					{
						strType = '<?=translate("HTML")?>';
						break;
					}
					case QTypesExt.openTime:
					{
						strType = '<?=translate("Time")?>';
						break;
					}
					case QTypesExt.skipsection:
					{
						strType = '<?=translate("Skip to section")?>';
						break;
					}
					case QTypesExt.calc:
					{
						strType = '<?=translate("Calculated")?>';
						break;
					}
					case QTypesExt.document:
					{
						strType = '<?=translate("Document")?>';
						break;
					}
					case QTypesExt.ocr:
					{
						strType = '<?=translate("OCR")?>';
						break;
					}
					case QTypesExt.sync:
					{
						strType = '<?=translate("Synchronize")?>';
						break;
					}
					case QTypesExt.callList:
					{
						strType = '<?=translate("Call List")?>';
						break;
					}
					case QTypesExt.gps:
					{
						strType = '<?=translate("GPS")?>';
						break;
					}
					case QTypesExt.openPassword:
					{
						strType = '<?=translate("Password")?>';
						break;
					}
					case QTypesExt.audio:
					{
						strType = '<?=translate("Audio")?>';
						break;
					}
					case QTypesExt.video:
					{
						strType = '<?=translate("Video")?>';
						break;
					}
					case QTypesExt.sketch:
					//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case QTypesExt.sketchPlus:
					{
						strType = '<?=translate("Sketch")?>';
						break;
					}
					case QTypesExt.section:
					{
						strType = '<?=translate("Section")." ".strtolower(translate("Table"))?>';
						break;
					}
					case QTypesExt.barCode:
					{
						strType = '<?=translate("Barcode")?>';
						break;
					}
					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case QTypesExt.exit:
					{
						strType = '<?=translate("Exit")?>';
						break;
					}
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					case QTypesExt.updateDest:
					{
						strType = '<?=translate("Update data")?>';
						break;
					}
					//JAPR
					//OMMC 2016-04-20: Agregado el tipo de pregunta Mi localización
					case QTypesExt.myLocation:
					{
						strType = '<?=translate("Visual geolocation")?>';
						break;
					}
					case QTypesExt.menuMenu:
					{
						strType = '<?=translate("Menu")?>'+' ('+'<?=translate("Menu")?>'+')';
						break;
					}
					case QTypesExt.menuVert:
					{
						strType = '<?=translate("Menu")?>'+' ('+'<?=translate("Vertical")?>'+')';
						break;
					}
					case QTypesExt.menuHoriz:
					{
						strType = '<?=translate("Menu")?>'+' ('+'<?=translate("Horizontal")?>'+')';
						break;
					}
					//OMMC
					default:
					{
						strType = '<?=translate("Unknown")?>';
						break;
					}	
				}
				return strType;
			}
			
			function getQuestionTypeDesc(iType) {
				var strType = '';
				
				switch (iType) {
					case Type.number:
						strType = '<?=translate("Number")?>';
						break;
					case Type.simplechoice:
						strType = '<?=translate("Simple choice")?>';
						break;
					case Type.multiplechoice:
						strType = '<?=translate("Multiple choice")?>';
						break;
					case Type.date:
						strType = '<?=translate("Date")?>';
						break;
					case Type.text:
						strType = '<?=translate("Text")?>';
						break;
					case Type.alfanum:
						strType = '<?=translate("Alphanumeric")?>';
						break;
					case Type.photo:
						strType = '<?=translate("Photo")?>';
						break;
					case Type.action:
						strType = '<?=translate("Action")?>';
						break;
					case Type.signature:
						strType = '<?=translate("Signature")?>';
						break;
					case Type.message:
						strType = '<?=translate("Message")?>';
						break;
					case Type.time:
						strType = '<?=translate("Time")?>';
						break;
					case Type.skipsection:
						strType = '<?=translate("Skip to section")?>';
						break;
					case Type.calc:
						strType = '<?=translate("Calculated")?>';
						break;
					case Type.document:
						strType = '<?=translate("Document")?>';
						break;
					case Type.sync:
						strType = '<?=translate("Synchronize")?>';
						break;
					case Type.callList:
						strType = '<?=translate("Call List")?>';
						break;
					case Type.gps:
						strType = '<?=translate("GPS")?>';
						break;
					case Type.password:
						strType = '<?=translate("Password")?>';
						break;
					case Type.mapped:
						strType = '<?=translate("Mapped")?>';
						break;
					case Type.audio:
						strType = '<?=translate("Audio")?>';
						break;
					case Type.video:
						strType = '<?=translate("Video")?>';
						break;
					case Type.sketch:
					//JAPR 2019-02-06: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case Type.sketchPlus:
						strType = '<?=translate("Sketch")?>';
						break;
					case Type.section:
						strType = '<?=translate("inline")?>';
						break;
					case Type.barCode:
						strType = '<?=translate("Barcode")?>';
						break;
					case Type.ocr:
						strType = '<?=translate("OCR")?>';
						break;
					//JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case Type.exit:
						strType = '<?=translate("Exit")?>';
						break;
					//JAPR 2016-03-30: Agregado el tipo de pregunta Update Data
					case Type.updateDest:
						strType = '<?=translate("Update data")?>';
						break;
					//JAPR
					default:
						strType = '<?=translate("Unknown")?>';
						break;
				}
				
				return strType;
			}
			
			function getSectionTypeDesc(iType) {
				var strType = '';
				
				switch (iType) {
					case SectionType.Standard:
						strType = '<?=translate("Standard question & answer")?>';
						break;
					case SectionType.Dynamic:
						strType = '<?=translate("List-based")?>';
						break;
					case SectionType.Formatted:
						strType = '<?=translate("Formatted text")?>';
						break;
					case SectionType.Multiple:
						strType = '<?=translate("Master-Detail")?>';
						break;
					case SectionType.Inline:
						strType = '<?=translate("Inline")?>';
						break;
					case SectionType.Recap:
						strType = '<?=translate("Recap")?>';
						break;
					default:
						strType = '<?=translate("Unknown")?>';
						break;
				}
				
				return strType;
			}
			
			//JAPR 2015-09-28: Corregido un bug, validado que sólo se pueda grabar un número telefónico (#FZ43UJ)
			/* Regla de validación para números telefónicos al definir la pregunta tipo llamada, se considerarán válidas todas las combinaciones de números + los caracteres # y * sin
			importar cantidad o posición de los mismos
			*/
			function ValidTelephone(sText) {
				sText = String(sText);
				var arrResults = sText.match(/[\d#\*]+/g);
				if (!arrResults || !$.isArray(arrResults) || arrResults.length > 1) {
					return false;
				}
				
				if (arrResults[0].length != sText.length) {
					return false;
				}
				
				return true;
			}
			
			/* Imprime un objeto de una forma mas legible que Json en la consola. Si se indica el parámetro bIsFormObject, se asumirá que se trata de una
			colección de objetos genéricos que representan a una forma, así que se les dará nombres según el tipo y sólo se asumirá de un nivel la colección
			*/
			function PrintObject(oCollection, sTabSpcs, bIsFormObject) {
				if (!oCollection) {
					console.log('Empty');
					return;
				}
				if (!sTabSpcs) {
					sTabSpcs = '';
				}
				
				for (var strKey in oCollection) {
					var objObject = oCollection[strKey];
					if (bIsFormObject) {
						var intObjectType = objObject.type;
						var intObjectID = objObject.id;
						var strTypeStr = '';
						switch (intObjectType) {
							case AdminObjectType.otySurvey:
								strTypeStr = 'Survey';
								break;
							case AdminObjectType.otySection:
								strTypeStr = 'Section';
								break;
							case AdminObjectType.otyQuestion:
								strTypeStr = 'Question';
								break;
							case AdminObjectType.otyOption:
								var intParentType = objObject.parentType;
								strTypeStr = 'Option';
								switch (intParentType) {
									case AdminObjectType.otySection:
										strTypeStr = 'Section ' + strTypeStr;
										break;
									case AdminObjectType.otyQuestion:
									strTypeStr = 'Question ' + strTypeStr;
										break;
								}
								break;
							case AdminObjectType.otySurveyFilter:
								strTypeStr = 'Survey Filter';
								break;
							case AdminObjectType.otyQuestionFilter:
								strTypeStr = 'Question Filter';
								break;
							case AdminObjectType.otyShowQuestion:
								strTypeStr = 'Show Question';
								break;
						}
						
						console.log(strTypeStr + ': ' + intObjectID);
					}
					else {
						if (typeof objObject == "object") {
							console.log(sTabSpcs + strKey);
							PrintObject(objObject, sTabSpcs + '   ', bIsFormObject);
						}
						else {
							console.log(sTabSpcs + strKey + ' = ' + objObject);
						}
					}
				}
			}
			
			//JAPR 2015-07-31: Agregado el parámetro sSource para indicar el tipo de editor que se está cargando, ya que desde un diálogo tenía comportamientos
			//erráticos como que el popUp para seleccionar imagenes se cerraba al hacer click en el botón para iniciar la selección de imagen pero sólo la
			//primera vez
			/* Adapta el EditorHTML con una toolbar personalizable según la configuración indicada en customButtons
			Los valores del parámetro sSource son: 
			"dialogForm" = Se abre desde un diálogo (window) con una forma que lo incluye
			*/
			function changeEditor(objEditor, customButtons, tabName, sFieldName, sSource)
			{
				objEditor.tb.hideItem("applyH1");
				objEditor.tb.hideItem("applyH1");
				objEditor.tb.hideItem("applyH2");
				objEditor.tb.hideItem("applyH3");
				objEditor.tb.hideItem("applyH4");
				objEditor.tb.hideItem("separ01");
				objEditor.tb.hideItem("applyStrikethrough");
				objEditor.tb.hideItem("alignLeft");
				objEditor.tb.hideItem("alignCenter");
				objEditor.tb.hideItem("alignRight");
				objEditor.tb.hideItem("alignJustify");
				objEditor.tb.hideItem("separ03");
				objEditor.tb.hideItem("applySub");
				objEditor.tb.hideItem("applySuper");
				objEditor.tb.hideItem("separ04");
				objEditor.tb.hideItem("createNumList");
				objEditor.tb.hideItem("createBulList");
				objEditor.tb.hideItem("separ05");
				objEditor.tb.hideItem("increaseIndent");
				objEditor.tb.hideItem("decreaseIndent");
				objEditor.tb.hideItem("separ06");
				objEditor.tb.hideItem("clearFormatting");
				<?
				//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Oculta todos los botones default de la toolbar y no genera los botones personalizados
				if ($this->FormType == formProdWithDev) {?>
				var objField = getFieldDefinition(tabName, sFieldName);
				if (objField.disabled) {
					objEditor.tb.forEachItem(function(itemId) {
						objEditor.tb.hideItem(itemId);
					});				
					return;
				}
				<?}
				//@JAPR
				?>
				
				var buttonPos = objEditor.tb.getPosition("clearFormatting");
				buttonPos++;
				var myCP;
				objEditor.tb.objPopUpsColl = new Object();
				objEditor.tb.base.setAttribute("style", "width:325px");
				//JAPR 2019-08-08: Agregada la traducción del tooltip de los botones del editor (#BT0NMN)
				objEditor.tb.setItemToolTip("applyBold", "<?=translate("Bold")?>");
				objEditor.tb.setItemToolTip("applyItalic", "<?=translate("Italic")?>");
				objEditor.tb.setItemToolTip("applyUnderscore", "<?=translate("Underline")?>");
				//JAPR
				for (var strCustomButton in customButtons)
				{	
					var objEditorButton=customButtons[strCustomButton];
					
					switch (strCustomButton)
					{	
						case "fontName":
						{
							//Agrega la combo con los tipos de letra
							//objEditor.tb.addText("lblFontName", 40, "Font:");
							objEditor.tb.addText("fontName", 100, "");
							//JAPR 2019-08-08: Agregada la traducción del tooltip de los botones del editor (#BT0NMN)
							objEditor.tb.setItemToolTip(strCustomButton, objEditorButton.label);
							//JAPR
							var objComboDiv = objEditor.tb.objPull[objEditor.tb.idPrefix+"fontName"].obj;
							objComboDiv.style.margin = '2px';
							objEditor.tb.objPull[objEditor.tb.idPrefix+"fontName"].obj.innerHTML = "";
							var objCombo = new dhtmlXCombo(objComboDiv, "cbFontName", 125);
							//JAPR 2019-08-07: Ajustes a las fuentes utilizadas para estandarizar entre los editores y reducir complejidad en el diseño (#BT0NMN)
							objCombo.addOption("Arial", "Arial", "font-family:Arial");
							//objCombo.addOption("Helvetica", "Helvetica", "font-family:Helvetica");
							//objCombo.addOption("Georgia", "Georgia", "font-family:Georgia");
							objCombo.addOption("Lato", "Lato", "font-family:Lato");
							objCombo.addOption("Monospace", "Monospace", "font-family:Monospace");
							objCombo.addOption("Palatino Linotype", "Palatino Linotype", "font-family:Palatino Linotype");
							//objCombo.addOption("Times New Roman", "Times New Roman", "font-family:Times New Roman");
							//objCombo.addOption("Verdana", "Verdana", "font-family:Verdana");
							//JAPR
							objCombo.selectOption(0);
							objCombo.attachEvent("onChange", function() {
								objEditor._runCommand("fontName", objCombo.getSelectedText());
							});

							break;
						}
						case "fontSize":
						{
							objEditor.tb.addText("fontSize", 100, "");
							//JAPR 2019-08-08: Agregada la traducción del tooltip de los botones del editor (#BT0NMN)
							objEditor.tb.setItemToolTip(strCustomButton, objEditorButton.label);
							//JAPR
							var objComboDivFS = objEditor.tb.objPull[objEditor.tb.idPrefix+"fontSize"].obj;
							objComboDivFS.style.margin = '2px';
							objEditor.tb.objPull[objEditor.tb.idPrefix+"fontSize"].obj.innerHTML = "";
							var objComboFS = new dhtmlXCombo(objComboDivFS, "cbFontSize", 40);
							objComboFS.addOption("1", "1");
							objComboFS.addOption("2", "2");
							objComboFS.addOption("3", "3");
							objComboFS.addOption("4", "4");
							objComboFS.addOption("5", "5");
							objComboFS.addOption("6", "6");
							objComboFS.addOption("7", "7");
							objComboFS.selectOption(0);
							objComboFS.attachEvent("onChange", function() {
								objEditor._runCommand("fontSize", objComboFS.getSelectedText());
							});
							break;
						}
						default:
						{
							switch (objEditorButton.type)
							{
								case "buttonColor":
								{
									objEditor.tb.addButton(strCustomButton, buttonPos, "", objEditorButton.img, objEditorButton.img);
									//JAPR 2019-08-08: Agregada la traducción del tooltip de los botones del editor (#BT0NMN)
									objEditor.tb.setItemToolTip(strCustomButton, objEditorButton.label);
									//JAPR
									buttonPos++;
									var objColorPopUp = new dhtmlXPopup({ 
										toolbar: objEditor.tb,
										id: strCustomButton
									});
									objColorPopUp.bitButtonId = strCustomButton;
									objEditor.bitButtonId = strCustomButton;
									//OMMC 2015-10-14: Attach de evento para prevenir el cierre de los popups que contienen los color pickers de los editores HTML
									objEditor.tb.objPopUpsColl[strCustomButton] = objColorPopUp;
									var parentEventAttach = objColorPopUp.attachEvent("onBeforeHide", function(type, ev){
										if(type == "click" && !objColorPopUp.bitFirstClick){
											objColorPopUp.bitFirstClick = true;
											return false;
										}
									});
									
									//OMMC 2015-10-14: Funcionalidad agregada para cerrar (esconder) los popups que contienen los color pickers al dar click en el textarea (tanto en nueva pregunta como en el grid).
									objColorPopUp.bitEventHandler = parentEventAttach;
									objEditor.attachEvent("onAccess", function(eventName, evObj){
										if(eventName == "click"){
											if (evObj.target.tagName == "BODY"){
												for (var strPopUpName in objEditor.tb.objPopUpsColl) {
													var objPopUp = objEditor.tb.objPopUpsColl[strPopUpName];
													if (objPopUp) {
														objColorPopUp.bitFirstClick = undefined;
														objPopUp.hide();
													}
												}
											}
										}
									});
									
									objColorPopUp.attachEvent("onShow", function() {
										console.log(document.activeElement);
										for (var idEd in objEditor.tb.objPopUpsColl)
										{
											if(idEd!=this.bitButtonId)
											{
												objEditor.tb.objPopUpsColl[idEd].hide();	
											}
										}
										var objPopUp = objEditor.tb.objPopUpsColl[this.bitButtonId];
										myCP = objPopUp.attachColorPicker({
											color: "#2a87eb",
											custom_colors: ["#ff9f29","#a3ff2b","#57cdff","#fb26ff","#9f96ff"]
										});
										myCP.attachEvent("onCancel",function(color){
											// do not allow to hide colorpicker, hide popup instead
											objPopUp.hide();
											return false;
										});
										myCP.attachEvent("onSelect",function(color){
											objEditor._runCommand(objPopUp.bitButtonId, color);
											objPopUp.hide();
										});
										//JAPR 2019-08-08: Agregada la posibilidad de grabar los colores personalizados por el usuario
										if ( $.isArray(arrCustomizedColors) ) {
											myCP.memory.clean();
											myCP.setCustomColors(arrCustomizedColors.join(","));
										}
										myCP.attachEvent("onSaveColor",function(color){
											arrCustomizedColors = this.getCustomColors();
											doSaveCustomizedColors();
										});
										//JAPR
										
										//Traduce los textos del colorPicker
										setTimeout(function() {
											try {
												var objLabels = $('.dhxcp_inputs_cont td.dhxcp_label_hsl');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														var strLabel = "";
														switch (intCont % 3) {
															case 0:
																strLabel = "<?=translate("Hue")?>";
																break;
															case 1:
																strLabel = "<?=translate("Sat")?>";
																break;
															case 2:
																strLabel = "<?=translate("Lum")?>";
																break;
														}
														
														if (objLabels[intCont]) {
															objLabels[intCont].innerText = strLabel;
														}
													}
												}
												
												var objLabels = $('.dhxcp_inputs_cont td.dhxcp_label_rgb');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														var strLabel = "";
														switch (intCont % 3) {
															case 0:
																strLabel = "<?=translate("Red")?>";
																break;
															case 1:
																strLabel = "<?=translate("Green")?>";
																break;
															case 2:
																strLabel = "<?=translate("Blue")?>";
																break;
														}
														
														if (objLabels[intCont]) {
															objLabels[intCont].innerText = strLabel;
														}
													}
												}
												
												var objLabels = $('.dhxcp_g_memory_area button div.dhxcp_label_bm');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Save the color")?>";
													}
												}
												
												var objLabels = $('.dhxcp_buttons_area button.dhx_button_save');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Select")?>";
													}
												}
												
												var objLabels = $('.dhxcp_buttons_area button.dhx_button_cancel');
												if (objLabels && objLabels.length) {
													for (var intCont = 0; intCont < objLabels.length; intCont++) {
														objLabels[intCont].innerText = "<?=translate("Cancel")?>";
													}
												}
											} catch(e) {
												debugger;
											}
										}, 100);
									});
									break;
								}
								case "uploadImg":
								{
									//JAPR 2015-08-01: Debido a problemas con los PopUps en diálogos, se optó por integrar directamente el click del botón
									//para que haga el Upload de la imagen abriendo el diálogo correspondiente (#B94DKA)
									objEditor.tb.addButton(strCustomButton, buttonPos, "", objEditorButton.img, objEditorButton.img);
									//JAPR 2019-08-08: Agregada la traducción del tooltip de los botones del editor (#BT0NMN)
									objEditor.tb.setItemToolTip(strCustomButton, objEditorButton.label);
									//JAPR
									buttonPos++;
									/*
									var objImgPopUp = new dhtmlXPopup({ 
										toolbar: objEditor.tb,
										id: strCustomButton
									});
									objImgPopUp.bitButtonId = strCustomButton;
									*/
									var objButtonDiv = objEditor.tb.objPull[objEditor.tb.idPrefix+"uploadImg"].obj;
									var intObjectType = -1;
									var intObjectID = -1;
									var intChildObjectType = -1;
									var intChildObjectID = -1;
									var parentTypeVal = "-1";
									var strRowId = sFieldName;
									var strColTabName = "";
									var styleDeleteImg = 'style="display:inline"';
									var strObjectRowID = "_otp" + intObjectType + "oid" + intObjectID;
									var strSubObjRowID = "";
									var strPopupTimeID = "&strPopupTimeID=";
									var strFieldName="&fieldName="+sFieldName;
									var strDivEditorID = "&bitEditorDivID="+objEditor.base.id;
									if (intChildObjectType) {
										strSubObjRowID = "_ctp" + intChildObjectType + "cid" + intChildObjectID;
									}
									var strId = "cellUploadImg_"+tabName+"_"+strRowId+strObjectRowID+strSubObjRowID+'_'+0;
									var fieldHTML='<div style="opacity:0;width:0px;height:0px;"><div id="divFile'+strId+'" style="border:none">'+
										'<form name="frmUploadFile'+strId+'" method="POST" action="uploadUserImage.php?objectType='+parentTypeVal+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+strPopupTimeID+strFieldName+strDivEditorID+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
										'<input style="display:none !important" type="file" id="file'+strId+'" name="file'+strId+'" value="" style="opacity:0;width:0px;height:0px;" onchange="document.getElementById(\'txtsubmit'+strId+'\').click();">'+
										'<button style="display:none !important" type="button" onclick="document.getElementById(\'file'+strId+'\').click();"><?=translate("Select file")?></button>'+
										'<input style="display:none !important" style="display:none !important" type="submit" id ="txtsubmit'+strId+'" name="txtsubmit'+strId+'" value="<?=translate("Upload file")?>" style="display: none; visibility: hidden;">'+'<img style="display:none !important"name="deleteImg" id="deleteImg" src="images/admin/delete.png" '+ styleDeleteImg + ' onClick=\'removeImg("'+strId+'")\'>'+
										'<div>'+
										'<img name ="img'+strId+'" id="img'+strId+'" src="'+srcImg+'" style="display:none !important;vertical-align:top;max-height:50px;">'+
										'<img name ="emptyimg'+strId+'" id="img'+strId+'" src="<?=$pathImg?>images/admin/emptyimg.png" style="display:none !important;vertical-align:top;height:50px;">'+
										'</div>'+
										'</form>'+
										'</div>'+
										'<form id="frmDeleteFile'+strId+'" name="frmDeleteFile'+strId+'" method="POST" action="uploadUserImage.php?action=delete&objectType='+parentTypeVal+'&strId='+strId+'&tabName='+tabName+'&strRowId='+strRowId+'&objectID='+intObjectID+'&childType='+intChildObjectType+'&childID='+intChildObjectID+'&collTabName='+strColTabName+'" enctype="multipart/form-data" style="display:inline" target="frameUploadFile">'+
										'</form></div>'
									var objButton = $(objButtonDiv);
									objButton.on('click', function() {
										document.getElementById('file'+strId).click();
									});
									$(objButtonDiv).after(fieldHTML);
									//objButtonDiv.style.margin = '2px';
									//objEditor.tb.objPull[objEditor.tb.idPrefix+"uploadImg"].obj.innerHTML = "";
									/*var objCombo = new dhtmlXCombo(objButtonDiv, "cbFontName", 140);
									objCombo.attachEvent("onChange", function() {
										objEditor._runCommand("fontName", objCombo.getSelectedText());
									});*/
									
									
									//JAPR 2015-07-31: Agregado el parámetro sSource para indicar el tipo de editor que se está cargando
									//Si el PopUp se mostró en el componente dentro de una forma de un diálogo (para el texto HTML de las preguntas), entonces
									//agrega la marca para evitar el primer evento de hide debido a un click que reciba, ya que tenía un comportamiento errático
									//que forzaba a cerrarlo en el click al botón de seleccionar imagen
									/*
									if (sSource == "dialogForm") {
										objImgPopUp.bitSkipFirstHideEv = 1;
									}
									//JAPR
									objEditor.tb.objPopUpsColl[strCustomButton] = objImgPopUp;
									*/
									
									var myTime;
									myTime = new Date();
									var strMyTime = tabName + "_" + sFieldName + '_'+ myTime.getDate() + myTime.getMonth() + myTime.getFullYear() + myTime.getHours() + myTime.getMinutes() + myTime.getSeconds();
									//JAPR 2015-07-31: Agregado el tab y el field como parte del identificador de popUp y movido al Show para que no se agregue
									//al array sin necesidad y no dejar basura
                                    //objArrPopUpsByTime[strMyTime] = objImgPopUp;
									/*
									timePopup = strMyTime;
									objImgPopUp.bittimePopup=timePopup;
									objImgPopUp.bitTabName=tabName;
									objImgPopUp.bitFieldName=sFieldName;
									
									//JAPR 2015-07-31: Agregado el parámetro sSource para indicar el tipo de editor que se está cargando
									objImgPopUp.attachEvent("onBeforeHide", function(type, ev, id) {
										//Si se pidió cerrar el popUp por un click fuera de su área, se verifica si se tiene la marca para cancelar el primer
										//evento de este tipo ya que desde una forma en un diálogo tenía un comportamiento errático
										if (type == "click" && objImgPopUp.bitSkipFirstHideEv) {
											delete objImgPopUp.bitSkipFirstHideEv;
											return false;
										}
										
										delete objArrPopUpsByTime[this.bittimePopup];
										return true;
									});
									
									objImgPopUp.attachEvent("onShow", function() {
										//JAPR 2015-07-31: Agregado el tab y el field como parte del identificador de popUp y movido al Show para que no se agregue
										//al array sin necesidad y no dejar basura
										objArrPopUpsByTime[this.bittimePopup] = this;
										//JAPR
										//var objPopUp = objEditor.tb.objPopUpsColl[this.bitButtonId];
										var objPopUp = this;
										objGrid = objPopUp.attachGrid(500, 200);
										objGrid.setImagePath("<?=$strScriptPath?>/images/");          //the path to images required by grid
										objGrid.setHeader("<?=translate("Property")?>,<?=translate("Value")?>", null, ["background-color:cccccc;","background-color:cccccc;"]);
										objGrid.setInitWidthsP("20,80");
										objGrid.setColAlign("left,left");
										objGrid.bittimePopup=this.bittimePopup;
										objGrid.bitTabName=this.bitTabName;
										objGrid.bitFieldName = objEditor.bitFieldName;
										objGrid.bitEditorDivID = objEditor.bitEditorDivID;
										objGrid.setColTypes("ro,uploadImg");
										objGrid.enableEditEvents(true);
										objGrid.enableMarkedCells();
										objGrid.init();
										objGrid.addRow("rowImg","<?=translate("Image")?>,");
									});
									*/
									break;
								}
								case "input":
								{
									break;
								}
								default:
								{
									break;
								}
							}
							break;
						}
					}
				}
			}
			
			//JAPR 2019-08-07: Eliminados los popups creados por diálogos (#BT0NMN)
			/* Dado un objeto tabbar, remueve todas las posibles referencias de popups almacenadas entre los campos que se usaron para desplegar dentro de las tabs que contenían grids u
			otro tipo de componentes que los requerían, de tal maneraque no se quede basura en el DOM */
			function doRemovePopUps(oContainer) {
				if ( oContainer ) {
					var objTabbar = oContainer.getAttachedObject();
					if ( objTabbar && objTabbar.forEachTab ) {
						objTabbar.forEachTab(function(aTab) {
							var strTabId = aTab.getId();
							var objGrid = oContainer.getAttachedObject().tabs("generalTab").getAttachedObject();
							if ( objGrid && objGrid.getAllRowIds ) {
								var strPropertyNames = objGrid.getAllRowIds();
								var arrPropertyNames = String(strPropertyNames).split(",");
								if ( arrPropertyNames && arrPropertyNames.length ) {
									for (var intPropNum in arrPropertyNames) {
										var strPropertyName = arrPropertyNames[intPropNum];
										if ( strPropertyName ) {
											var objEditor = objGrid.getUserData(strPropertyName, "editor");
											if (objEditor && objEditor.tb && objEditor.tb.objPopUpsColl) {
												for (var strPopUpName in objEditor.tb.objPopUpsColl) {
													var objPopUp = objEditor.tb.objPopUpsColl[strPopUpName];
													if (objPopUp) {
														if (objPopUp.bitEventHandler) {
															objPopUp.detachEvent(objPopUp.bitEventHandler);
														}
														if ( objPopUp.unload ) {
															objPopUp.unload();
														}
													}				
												}
											}
										}
									}
								}
							}
						});
					}
				}
			}
			
			//JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
			/* Realiza el grabado de la lista de colores personalizados definida por el usuario */
			function doSaveCustomizedColors() {
				var strParams = "UserID={$intUserID}&Process=SaveCustomColors&RequestType=" + reqAjax;
				//En este caso el parámetro se genera también como array
				var strAnd = '&';
				for (var intIndex in arrCustomizedColors) {
					strParams += strAnd + "CustomizedColor[" + intIndex + "]=" + encodeURIComponent(arrCustomizedColors[intIndex]);
				}
				
				window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
					/*if (!loader || !loader.xmlDoc) {
						return;
					}
					
					if (loader.xmlDoc && loader.xmlDoc.status != 200) {
						alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
						return;
					}
					
					//Si llega a este punto es que se recibió una respuesta correctamente
					var response = loader.xmlDoc.responseText;
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						return;
					}
					else {
						if (objResponse.warning) {
							console.log(objResponse.warning.desc);
						}
					}*/
				});
			}
			//JAPR
		</script>
	</head>
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<!--<div id="objectId" style="width:100%;height:100%;background-color:blue;">abc</div>-->
		<div id="divInviteUsersParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divInviteUsers" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divInviteGroupsParent" style="display:none;height:100%;width:100%;overflow-y:auto;">
			<div id="divInviteGroups" style="height:100%;width:900px;overflow-y:auto;"></div>
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignHeader" class="linkToolbar" style="height:30px;width:100%">
				<table style="/*width:100%;*/height:100%;">
					<tbody>
						<tr>
						<td class="linkTD" tabName="form" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Design")?></td>
							<td class="linkTD" tabName="users" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Invite users")?></td>
							<td class="linkTD" tabName="usergroups" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Invite groups")?></td>
							<td class="linkTD" tabName="model" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Model")?></td>
							<td class="linkTD" tabName="datadestinations" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Data destinations")?></td>
							<td class="linkTD" tabName="details" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Form details")?></td>
							<!--<td class="linkTD" tabName="copy" onclick="doCopyForm()"><?=translate("Copy")?></td>-->
							<!--<td class="linkTD" tabName="lockLeap" ><input type="checkbox" id="blockFlow" value="" onclick="doSetPreviewMode(this)"/><?=translate("Leap Logic?")?></td>-->
							<td class="linkTD" tabName="onlineEntry" onclick="browserApp()"><?=translate("Online Entry")?></td>
							<td class="linkTD" tabName="save" style="text-decoration: none !important; cursor:auto;width:150px;">
								<div id="divSaving" class="dhx_cell_progress_img" style="display:none;position:relative !important;background-size:20px 20px;">&nbsp;</div>
								<div id="divSavingError" style="display:none;position:relative !important;background-size:20px 20px;cursor:pointer;" onclick="doShowErrors()">&nbsp;</div>
							</td>
						</tr>
				  </tbody>
				</table>
			</div>
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
	</body>
</html>
<?
	}
}

class BITAMSurveyExtCollection extends BITAMSurveyCollection
{
	use BITAMCollectionExt;
	public $ObjectType = otySurvey;
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SurveyExt";
	}
	
	function get_CustomButtons() {
		global $gbIsGeoControl;
		$arrButtons = array();
		//@JAPR 2015-06-29: Removido el botón de copy, ya que el copiado se hará directo en la forma
		//$arrButtons[] = array("id" => "copy", "label" => translate("Copy"), "image" => "copy.png", "url" => "", "onclick" => "copySurveys();");
		$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);	//*"label" => translate("Add"), "image" => "add.png", */
		//GCRUZ 2015-09-17. Agregar acceso a menus de formas desde la lista de formas
		$arrButtons[] = array("id" => "menu", 'url' => 'Menu', "label" => translate('Menus'), 'image' => 'menu.png');	//*"label" => translate("Add"), "image" => "add.png", */
		//@JAPR 2016-01-15: Agregado el enlace para captura Web para los Administradores de eForms (#JC04G0)
		$arrButtons[] = array("id" => "entry", "url" => "", "onclick" => "browserApp();", "label" => translate('Online Entry'), 'image' => 'onlineEntry.png');
		//GCRUZ 2015-05-06. Agregar enlace a descagra de app
		if (!$gbIsGeoControl)
			$arrButtons[] = array("id" => "downloadapp", "url" => "", "onclick" => "downloadApp();", "label" => translate('Download App'), 'image' => 'donloadappforms.png');
		return $arrButtons;
	}
	
	//GCRUZ 2015-09-17. Agregar botones custom al objeto de comportamientos
	function get_CustomButtonsBehaviour(){
		foreach (@$this->get_CustomButtons() as $aCustomButton)
		{
			$strButtonID = $aCustomButton['id'];
?>
		objCustomBehaviour["<?=$strButtonID?>"] = {customized:true};
<?
		}
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("Design", $aHTTPRequest->GET)) {
			$intSurveyID = 0;
			if (array_key_exists("SurveyID", $aHTTPRequest->GET)) {
				$intSurveyID = $aHTTPRequest->GET["SurveyID"];
			}
			
			if ($intSurveyID > 0) {
				$strCalledClass = static::class;
				//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
				$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, $intSurveyID);
				$anInstance->generateDesignWindow();
				die();
			}
		}
		
		return parent::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);
	}
	
	/* Agrega el código para permitir la creación flotante de formas así como el copiado */
	function generateAfterFormCode($aUser) {
?>
	<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>		
	<script language="JavaScript">
		var objWindows;
		var objDialog;
		var intDialogWidth = 380;
		var intDialogHeight = 350;
		var reqAjax = 1;
		
		//Descarga la instancia de la forma de la memoria
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData() {
			if (!objDialog) {
				return;
			}
			
			objDialog.progressOn();
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			objForm.lock();
			objForm.send("processRequest.php", "post", function(loader, response) {
				if (!loader || !response) {
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				setTimeout(function () {
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					else {
						if (objResponse.warning) {
							alert(objResponse.warning.desc);
						}
					}
					
					//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
					doUnloadDialog();
					var strURL = objResponse.url;
					if (strURL && parent.doExecuteURL) {
						parent.doExecuteURL("", undefined, undefined, strURL);
					}
				}, 100);
			});
		}
		
		//Muestra el diálogo para crear una nueva forma
		function addNewObject() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate("New survey")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			
			//Al cerrar realizará el cambio de sección de la pregunta
			objDialog.attachEvent("onClose", function() {
				return true;
			});
			
			/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"input", name:"SurveyName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
				{type:"block", blockOffset:0, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"SurveyID", value:-1, hidden:true},
				{type:"input", name:"Design", value:1, hidden:true},
				{type:"input", name:"RequestType", value:reqAjax, hidden:true},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otySurvey?>, hidden:true}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			objForm.setItemFocus("SurveyName");
			objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				switch (ev.keyCode) {
					case 13:
						//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
							if(objDialog.bitProcessing === undefined){
								objDialog.bitProcessing = true;
								setTimeout(function() {
									objForm.callEvent("onButtonClick", ["btnOk"]);
									objDialog.bitProcesing = undefined;
								}, 100);
							}
						break;
					case 27:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnCancel"])
						}, 100);
						break;
				}
			});
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData();
							}
						}, 100);
						break;
				}
			});
		}
		
		//JAPR 2016-01-15: Agregado el enlace para captura Web para los Administradores de eForms (#JC04G0)
		function browserApp() {
			d = new Date();
			dteNow = '' + d.getFullYear() + d.getMonth() + d.getDay() + d.getHours() + d.getMinutes() + d.getSeconds();
			openWindow('GenerateSurvey.php?dte=' + dteNow + '&login=1');
		}
		//GCRUZ 2015-05-06. Agregar enlace a descagra de app
		function downloadApp() {
			openWindow('http://www.kpiforms.com/p5i0iu-2cx/');
		}
	</script>
<?
	}
}
?>