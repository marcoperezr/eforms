<?php
/* Esta clase permite configurar el acceso a la metadata para el grabado de los estilos de componentes del App (Templates)
Es una clase pura de v6, esto significa que no está preparada para soportar el viejo framework de aplicaciones anteriores a v6, sino que es ya bajo el esquema de interfaz DHTMLX
donde cada ventana genera las formas de manera diferente, por tanto no contiene un nombre con terminación Ext para representar que no es una clase extendida basada de otra original
del viejo esquema ni usa un trait, sin embargo como se siguen invocando a las páginas de acuerdo al viejo Framework, seguirá la nomenclatura de algunas funciones y tendrá que implementarlas, 
como es el caso de generateForm
*/
require_once("repository.inc.php");
require_once("initialize.php");
require_once("object.trait.php");
require_once("eformsAppCustomStylesTpl.inc.php");

class BITAMEFormsAppCustomStyles extends BITAMObject
{
	public $TemplateID;					//ID de definición de templates para permitir reutilizar en diferentes usuarios como si fuera un catálogo sin tener que perder antiguas definiciones
	public $TemplateName;				//Cuando se carga la instancia para grabar un template, contiene el nombre asignado al mismo
	public $ArrayStyles;				//Lista de estilos indexados por todos los elementos de la tabla que identifican a un componente único
	public $ArrayStylesUpdated;			//Lista de estilos indexados por todos los elementos de la tabla que identifican a un componente único, usada exclusivamente en el proceso de grabado
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->TemplateID = -1;
		$this->TemplateName = '';
		$this->ArrayStyles = array();
		//Array exclusivo para el proceso de grabado, dado a que el grabado es incremental en base a los cambios realizados, no tiene caso recorrer un array potencialmente enorme
		//del total de las configuraciones buscando cual se marcó como modificada, ni generar un segundo array exclusivamente con llaves para recorrer el array principal, así que
		//dentro de updateFromArray se generará este array adicional con las combinaciones recibidas y que sean válidas (es decir, que si estén en el array principal) que habrían
		//cambiado, conteniendo el nuevo estilo a aplicar
		$this->ArrayStylesUpdated = array();
	}
	
	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	static function NewInstanceAll($aRepository, $aTemplateID = -1)
	{
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->TemplateID = $aTemplateID;
		$anInstance->resetDefaultSettings();
		
		$sql = "SELECT TemplateName 
			FROM SI_SV_TemplateStyleTpls 
			WHERE TemplateID = {$aTemplateID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF) {
			$anInstance->TemplateName = (string) @$aRS->fields["templatename"];
		}
		
		$arrCSSStyleInfo = array('dirty' => 0, 'style' => array());
		$arrDefaultCSSStyle = array(qelemLabCont => $arrCSSStyleInfo, qelemLabElem => $arrCSSStyleInfo, qelemQtnCont => $arrCSSStyleInfo, qelemQtnElem => $arrCSSStyleInfo);
		
		//Estilos de preguntas y secciones
		//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
		$sql = "SELECT TemplateID, SettingType, ObjectType, SectionType, QuestionType, DisplayType, ElementType, Style 
			FROM SI_SV_TemplateStyle 
			WHERE TemplateID = {$aTemplateID} 
			ORDER BY TemplateID, SettingType, ObjectType, SectionType, QuestionType, DisplayType, ElementType";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_TemplateStyle ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
			$settingType = (int) @$aRS->fields["settingtype"];
			$objectType = (int) @$aRS->fields["objecttype"];
			$sectionType = (int) @$aRS->fields["sectiontype"];
			$questionType = (int) @$aRS->fields["questiontype"];
			$displayType = (int) @$aRS->fields["displaytype"];
			$elementType = (int) @$aRS->fields["elementtype"];
			$style = trim((string) @$aRS->fields["style"]);
			if ($style == "") {
				$style = "{}";
			}
			
			//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
			if (!isset($anInstance->ArrayStyles[$settingType])) {
				$anInstance->ArrayStyles[$settingType] = array();
			}
			//@JAPR
			if (!isset($anInstance->ArrayStyles[$settingType][$objectType])) {
				$anInstance->ArrayStyles[$settingType][$objectType] = array();
			}
			if (!isset($anInstance->ArrayStyles[$settingType][$objectType][$sectionType])) {
				$anInstance->ArrayStyles[$settingType][$objectType][$sectionType] = array();
			}
			if (!isset($anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType])) {
				$anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType] = array();
			}
			if (!isset($anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType])) {
				$anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType] = array();
			}
			if (!isset($anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType])) {
				$anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType] = $arrCSSStyleInfo;
			}
			
			//Decodifica el String de estilo para mandarlo como un objeto de propiedades
			try {
				$arrJSON = json_decode($style);
			} catch (Exception $e) {
				$arrJSON = array();
			}
			
			$anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType]['style'] = $arrJSON;
			$anInstance->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType]['styleStr'] = $style;
			
			$aRS->MoveNext();
		}
		
		return($anInstance);
	}
	
	//Llena la instancia de settings con los valores default
	function resetDefaultSettings()
	{
		//Estructura del arreglo:
		//Primer nivel:  Tipo de objeto (sección, pregunta)
		//Segundo nivel: Tipo de sección
		//Tercer nivel:  Tipo de pregunta (cuando el tipo de objeto es pregunta)
		//Cuarto nivel:  Tipo de despliegue
		//				 Cada tipo de pregunta tendrá sus propios tipos de despliegue
		//				 Para las preguntas que no manejan tipo de despliegue, se manejará un arreglo
		//				 cuyo único elemento tendrá el índice cero.
		$this->ArrayStyles = array();
	}
	
	/* Obtiene el string JSON a utilizar para generar el objeto correspondiente en el cliente durante la configuración
	*/
	function getStyles($aUser)
	{
		if (is_null($this->ArrayStyles) || !is_array($this->ArrayStyles)) {
			$this->ArrayStyles = array();
		}
		//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
		if (!isset($this->ArrayStyles[tsstObject])) {
			$this->ArrayStyles[tsstObject] = array();
		}
		
		//Los estilos a descargar en el configurador por ahora son sólo estilos directos de objetos, no globales (eventualmente se podrían configurar los estilos globales, por ahora se
		//hace sólo mediante los botones para propagar estilos)
		$strJSONStyle = json_encode($this->ArrayStyles[tsstObject], JSON_FORCE_OBJECT);
		//@JAPR
		return $strJSONStyle;
	}
	
	function generateForm($aUser) {
		//@JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
		//Se tuvo que habilitar el DesignMode para la configuración de template, ya que getJSonDefinition en este modo no debe regresar la variable {@IMGFULLPATH} pero si cuando se invoca
		//desde una descarga de definiciones de algún app o browser
		global $gbDesignMode;
		$gbDesignMode = true;
		//@JAPR
		
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
		<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
        <!--<link type="text/css" rel="stylesheet" href="jquery.mobile-1.0/jquery.mobile-1.0.css">-->
		<link type="text/css" rel="stylesheet" href="jquery.mobile-1.4.5/jquery.mobile-1.4.5.css"/>
		<link type="text/css" rel="stylesheet" href="jquery.mobile-forms.css">
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<!-- solo se cambio de ui a jquery-ui-1.11.4 -->
		<link rel="stylesheet" href="jquery_signaturepad/jquery.signaturepad.css"/>
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.autocomplete.css">
		<link rel="stylesheet" href="js/jquery/jquery-ui-1.11.4/themes/base/jquery.ui.theme.css">
		<link rel="stylesheet" href="js/chosen/chosen.css" />
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
		<link type="text/css" rel="stylesheet" href="css/inline.css"/>
		<!--  -->
        <style type="text/css" media="screen">
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			
		   .ui-footer.ui-bar {
		       padding-left: 0px;
		       padding-right: 0px;
			}
            
		    h3 {
				color: white;
			}
			
			.allowVAlign{
				display: table-cell;
			}
			
			.itemFocus, .mhover:hover {
				border: 2px dotted blue !important; /*1px solid #ff0000 !important;*/
				background: #a1ceed !important;	/*#ffff00 !important;*/
			}
			
			.mhoverhand:hover {
				border: 2px dotted blue !important; /*1px solid #ff0000 !important;*/
				background: #a1ceed !important;	/*#ffff00 !important;*/
				cursor: pointer;
			}
			
			.mhoverhandinner:hover {
				border: 2px dotted blue !important; /*1px solid #ff0000 !important;*/
				background: #a1ceed !important;	/*#ffff00 !important;*/
				cursor: pointer;
			}
			
			.groupFontStyle, .dhxform_obj_dhx_terrace div.dhxform_txt_label2 {
				color: #4876ba !important;
				font-family: Roboto Regular !important;
				font-weight: bold !important;
				font-size: 12px !important;
			}
			
			.groupFontStyle2, .dhxform_obj_dhx_terrace .groupFontStyle2 div.dhxform_txt_label2 {
				color: #000000 !important;
				font-family: Roboto Regular !important;
				font-weight: bold !important;
				font-size: 12px !important;
			}
			
			.regFontStyle, .regFontStyle label, div.dhxcombo_dhx_terrace input.dhxcombo_input {
				font-family: Roboto Regular !important;
				font-size: 11px !important;
			}
			
			div.imgCheck {
				width:36px;
				height:36px;
				display:inline-block;
				cursor:pointer;
			}
			
			img.imgCheck {
				width:32px;
				height:32px;
				margin:2px;
			}
			
			.imgCheckFocus {
				background: #a1ceed !important;
			}
			
			.dhxform_obj_dhx_terrace div.dhxform_item_label_left {
				/*clear: both;*/
				padding-top: 1px;
				/*cursor: default;*/
			}
			
			/* Ajustes a los componentes DHTMLX específicos de esta ventana */
			.dhxform_base_nested.in_block .dhxform_base_nested.in_block {
				padding-left: 0px !important;
			}
			
			.dhx_cell_toolbar_def {
				padding: 0px !important;
			}
			
			.dhx_toolbar_btn.dhxtoolbar_btn_def .dhxtoolbar_input {
				height: 24px;
				border: 0px !important;
				margin-top: -4 !important;
			}
			
			div.dhx_toolbar_sep {
				border-left: 0px !important;
			}
			
			.dhxform_obj_dhx_terrace div.dhxform_img.btn2state_0 {
				background-image: url("images/admin/toggle_off.png");
				width: 42px;
				height: 20px;
			}
			.dhxform_obj_dhx_terrace div.dhxform_img.btn2state_1 {
				background-image: url("images/admin/toggle_on.png");
				width: 42px;
				height: 20px;
			}
			.dhxform_obj_dhx_terrace div.disabled div.dhxform_img.btn2state_0 {
				background-image: url("images/admin/toggle_off_dis.png");
				width: 42px;
				height: 20px;
			}
			.dhxform_obj_dhx_terrace div.disabled div.dhxform_img.btn2state_1 {
				background-image: url("images/admin/toggle_on_dis.png");
				width: 42px;
				height: 20px;
			}
			
			.dhxform_obj_dhx_terrace .boldImgs div.dhxform_img.btn2state_0 {
				background-image: url("images/admin/toggleB_off.png");
				width:18px;
				height:18px;
			}
			.dhxform_obj_dhx_terrace .boldImgs div.dhxform_img.btn2state_1 {
				background-image: url("images/admin/toggleB_on.png");
				width:18px;
				height:18px;
			}
			/*.dhxform_obj_dhx_terrace .boldImgs div.dhxform_label {
				display:none;
			}*/
			.dhxform_obj_dhx_terrace .boldImgs div.dhxform_control.dhxform_img_node .dhxform_textarea {
				width: 18px;
			}
			
			.dhxform_obj_dhx_terrace .italicImgs div.dhxform_img.btn2state_0 {
				background-image: url("images/admin/toggleI_off.png");
				width:18px;
				height:18px;
			}
			.dhxform_obj_dhx_terrace .italicImgs div.dhxform_img.btn2state_1 {
				background-image: url("images/admin/toggleI_on.png");
				width:18px;
				height:18px;
			}
			.dhxform_obj_dhx_terrace .italicImgs div.dhxform_label {
				display:none;
			}
			.dhxform_obj_dhx_terrace .italicImgs div.dhxform_control.dhxform_img_node .dhxform_textarea {
				width: 18px;
			}
			
			.dhxform_obj_dhx_terrace .underImgs div.dhxform_img.btn2state_0 {
				background-image: url("images/admin/toggleU_off.png");
				width:18px;
				height:18px;
			}
			.dhxform_obj_dhx_terrace .underImgs div.dhxform_img.btn2state_1 {
				background-image: url("images/admin/toggleU_on.png");
				width:18px;
				height:18px;
			}
			.dhxform_obj_dhx_terrace .underImgs div.dhxform_label {
				display:none;
			}
			.dhxform_obj_dhx_terrace .underImgs div.dhxform_control.dhxform_img_node .dhxform_textarea {
				width: 18px;
			}
		</style>
		
		<!--<script src="js/codebase/dhtmlx.js"></script>-->
		<script src="js/codebase/dhtmlxFmtd.js"></script>
		<script type="text/javascript" src="js/json2.min.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<script type="text/javascript" src="js/jquery-1.11.3.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>	    <!-- <script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script> -->
		<script type="text/javascript" src="js/jquery/jquery-ui-1.11.4/jquery-ui.js"></script>
		<!--JAPR 2016-03-02: Integrada nueva versión de JQ1.11.3/JQM1.45 (JIRH)-->
		<!--<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.min.js"></script>-->
		<!--<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>-->
		<!--<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>-->
		<!--<script type="text/javascript" src="jquery.mobile-1.0/jquery.mobile-1.0.js"></script>-->
		<script type="text/javascript" src="jquery.mobile-1.4.5/jquery.mobile-1.4.5.js"></script>
		<!--  -->
		<script type="text/javascript" data-main="js/_requireconfig.js" src="js/libs/require/require.js"></script>
		<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>		
<?
	//Contiene lo necesario para el manejo del grid
	//require_once("genericgrid.inc.php");
?>
		<script>
	
<?
		//getStyle retorna el arreglo ya con la función json_encode aplicada
		$strJSONStyle = $this->getStyles($aUser);
?>	
		var goStyle = JSON.parse("<?=addslashes($strJSONStyle)?>");
		
<?
		//Obtiene las clases a aplicar por default en el preview
		$strJSONStyle = json_encode($this->getJSonDefinition());
?>
		var objCSSStyleColl = JSON.parse("<?=addslashes($strJSONStyle)?>");
		
		//Convertir a objeto la cadena correspondiente al campo style en cada uno de
		//los elementos del arreglo
		//Recorrido por tipo de objeto
		var objPropsLayout;							//Layout principal de la ventana de configuración de layout
		var objPreviewLayout;						//Layout para el PopUp de preview
		var objPreviewDataView;						//Lista de ventanas de preview a configurar
		var objPropsForm;							//Forma donde se configuran todas las propiedades del Preview
		var objFormData;							//Campos que fueron usados para construir la forma
		var objPreviewToolbar;						//Botonera del preview para seleccionar el tipo de preview a configurar
		var objPreviewPopUp;						//PopUp selector de vista previa
		var objPropsTabbar;
		var objMappedStyles = {};					//Objeto con el mapeo del nombre de campo al estilo explícito que se afecta en el componente del DOM
		var gsLastTabSelected = ""					//Contiene el último tab al que se hizo click
		var hiddenFields = new Object();
		var hiddenBlocks = new Object();
		
		//Variables para la creación del selector
		var objDomSelector;							//Referencia al div selector
		var objLastElement;							//Elemento del preview sobre el que se hizo click por última vez (objeto del DOM con un evento setStyle)
		
		//Variables para identificar el último elemento seleccionado (algunos de estos valores se pueden deducir a partir de objLastElement)
		var giNotAssigned = -1;						//Significa que este elemento no aplica en la selección de componente actual
		var giTemplateID = <?=$this->TemplateID?>;	//ID del template a modificar
		var giSectionType = <?=sectAll?>;			//Tipo de sección
		var giObjectType = <?=otyQuestion?>;		//Tipo de objeto (Pregunta, Sección o Pantalla)
		var giQTypeID = <?=qtpOpenNumeric?>;		//Tipo de pregunta en caso de tener seleccionada una
		var giQDisplayMode = <?=dspEntry?>;			//Tipo de despliegue de la pregunta en caso de tener seleccionada una
		var giElementType = 0;						//Tipo específico de elemento a cambiar
		var giCElementType = 0;						//Tipo específico de elemento del contenedor (área) del elemento a cambiar (sólo aplica para preguntas)
		
		//Constantes
		var reqAjax = 1;
		var dhxPreviewCell = "a";
		var dhxPropsDetailCell = "b";
		var aFontNames = [
			{ value:"", text:"<?=translate("Default")?>"},
			{ value:"arial", text:"arial"},
			{ value:"arial black", text:"arial black"},
			{ value:"arial narrow", text:"arial narrow"},
			{ value:"arial unicode ms", text:"arial unicode ms"},
			{ value:"book antiqua", text:"book antiqua"},
			{ value:"bookman old style", text:"bookman old style"},
			{ value:"calibri", text:"calibri"},
			{ value:"candara", text:"candara"},
			{ value:"century gothic", text:"century gothic"},
			{ value:"comic sans ms", text:"comic sans ms"},
			{ value:"consolas", text:"consolas"},
			{ value:"constantia", text:"constantia"},
			{ value:"corbel", text:"corbel"},
			{ value:"courier new", text:"courier new"},
			{ value:"courierthai", text:"courierthai"},
			{ value:"garamond", text:"garamond"},
			{ value:"georgia", text:"georgia"},
			{ value:"haettenschweiler", text:"haettenschweiler"},
			{ value:"impact", text:"impact"},
			{ value:"lucida bright", text:"lucida bright"},
			{ value:"lucida console", text:"lucida console"},
			{ value:"lucida typewrite", text:"lucida typewrite"},
			{ value:"lucida sans unicode", text:"lucida sans unicode"},
			{ value:"map symbols", text:"map symbols"},
			{ value:"microsoft sans serif", text:"microsoft sans serif"},
			{ value:"monotype corsiva", text:"monotype corsiva"},
			{ value:"palatino linotype", text:"palatino linotype"},
			{ value:"segoe", text:"segoe"},
			{ value:"tahoma", text:"tahoma"},
			{ value:"thonburi", text:"thonburi"},
			{ value:"times new roman", text:"times new roman"},
			{ value:"trebuchet ms", text:"trebuchet ms"},
			{ value:"verdana", text:"verdana"}
		];
		
		var aFontSizes = [
			{ value:"", text:"<?=translate("Default")?>"},
			{ value:3, text:"3"},
			{ value:4, text:"4"},
			{ value:5, text:"5"},
			{ value:6, text:"6"},
			{ value:7, text:"7"},
			{ value:8, text:"8"},
			{ value:9, text:"9"},
			{ value:10, text:"10"},
			{ value:11, text:"11"},
			{ value:12, text:"12"},
			{ value:13, text:"13"},
			{ value:14, text:"14"},
			{ value:15, text:"15"},
			{ value:16, text:"16"},
			{ value:17, text:"17"},
			{ value:18, text:"18"},
			{ value:19, text:"19"},
			{ value:20, text:"20"},
			{ value:21, text:"21"},
			{ value:22, text:"22"},
			{ value:23, text:"23"},
			{ value:24, text:"24"},
			{ value:25, text:"25"},
			{ value:26, text:"26"},
			{ value:27, text:"27"},
			{ value:28, text:"28"},
			{ value:29, text:"29"},
			{ value:30, text:"30"},
			{ value:31, text:"31"},
			{ value:32, text:"32"},
			{ value:33, text:"33"},
			{ value:34, text:"34"},
			{ value:35, text:"35"},
			{ value:36, text:"36"},
			{ value:37, text:"37"},
			{ value:38, text:"38"},
			{ value:39, text:"39"},
			{ value:40, text:"40"}
		];
		
		var aHorizAlign = [
			{ value: "", text:"<?=translate("Default")?>"},
			{ value: "left", text: "<?=translate("Left")?>"},
			{ value: "center", text: "<?=translate("Center")?>"},
			{ value: "right", text: "<?=translate("Right")?>"}
		];
		
		var aVertAlign = [
			{ value: "", text:"<?=translate("Default")?>"},
			{ value: "top", text: "<?=translate("Top")?>"},
			{ value: "middle", text: "<?=translate("Center")?>"},
			{ value: "bottom", text: "<?=translate("Bottom")?>"}
		];
		
		var aBorderStyle = [
			{ value: "", text:"<?=translate("Default")?>"},
			{ value: "dotted", text: "<?=translate("Dotted")?>"},
			{ value: "dashed", text: "<?=translate("Dashed")?>"},
			{ value: "solid", text: "<?=translate("Solid")?>"},
			{ value: "double", text: "<?=translate("Double")?>"},
			{ value: "groove", text: "<?=translate("Groove")?>"},
			{ value: "ridge", text: "<?=translate("Ridge")?>"},
			{ value: "inset", text: "<?=translate("Inset")?>"},
			{ value: "outset", text: "<?=translate("Outset")?>"},
			{ value: "none", text: "<?=translate("None")?>"},
			{ value: "hidden", text: "<?=translate("Hidden")?>"}
		];
		
		var aBorderSizes = [
			{ value:"", text:"<?=translate("Default")?>"},
			{ value:1, text:"1"},
			{ value:2, text:"2"},
			{ value:3, text:"3"},
			{ value:4, text:"4"},
			{ value:5, text:"5"},
			{ value:6, text:"6"},
			{ value:7, text:"7"},
			{ value:8, text:"8"},
			{ value:9, text:"9"},
			{ value:10, text:"10"}
		];
		
		/* Inicializa los componentes y prepara eventos generales de la pantalla
		*/
		function doOnLoad() {
			console.log('doOnLoad');
			
			//if ($.mobile) {
				//$.mobile.autoInitializePage = false;
				//alert('disabling JQM enhancements');
				//$.mobile.ignoreContentEnabled = true;
			//}
			
			//Configura las etiquetas del DHTMLX ColorPicker
			if (dhtmlXColorPicker) {
				dhtmlXColorPicker.prototype.i18n["en"] = {
					labelHue: "<?=translate("Hue")?>",
					labelSat: "<?=translate("Sat")?>",
					labelLum: "<?=translate("Lum")?>",
					labelRed: "<?=translate("Red")?>",
					labelGreen: "<?=translate("Green")?>",
					labelBlue: "<?=translate("Blue")?>",
					btnAddColor: "<?=translate("Add color")?>",
					btnSelect: "<?=translate("OK")?>",
					btnCancel: "<?=translate("Cancel")?>"
				}
			}
			
			objPropsLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"2U"
			});
			
			//Genera la ventana de propiedades
			objPropsLayout.setSizes();
			//objPropsLayout.cells(dhxPreviewCell).setText("");
			objPropsLayout.cells(dhxPreviewCell).setText("<?=translate("Preview")?>");
			objPropsLayout.cells(dhxPreviewCell).showHeader();
			objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Login")?>");
			objPropsLayout.cells(dhxPropsDetailCell).showHeader();
			
			//Prepara la Toolbar para manipular las preguntas
			objPropsLayout.cells(dhxPreviewCell).attachToolbar({
				parent:"previewCell"
			});
			objPreviewToolbar = objPropsLayout.cells(dhxPreviewCell).getAttachedToolbar();
			objPreviewToolbar.setIconsPath("<?=$strScriptPath?>/images/admin/");
			var intPos = 0;
			objPreviewToolbar.addButton("change", intPos++, "<?=translate("Change")?>", "add.png");
			objPreviewToolbar.addButton("reset", intPos++, "<?=translate("Reset")?>", "remove.png");
			objPreviewToolbar.addSeparator("TemplateNameSep", intPos++);
			objPreviewToolbar.addText("TemplateNameLab", intPos++, "<?=translate("Template name")?>");
			objPreviewToolbar.addInput("TemplateName", intPos++, "<?=$this->TemplateName?>", 250);
			
			//Prepara el PopUp para seleccionar la vista previa a configurar
			objPreviewPopUp = new dhtmlXPopup({ 
				toolbar: objPreviewToolbar,
				id: "change"
			});
			
			objPreviewToolbar.attachEvent("onClick", function(id) {
				console.log('objPreviewToolbar.onClick ' + id);
				
				switch (id) {
					case "reset":
						var blnAnswer = confirm("<?=translate("This will clear all format applied to this template (this process cannot be reversed). Do you wish to continue?")?>");
						if (blnAnswer) {
							doSaveStyle(giElementType, {}, undefined, true);
						}
						break;
				}
			});
			
			objPreviewPopUp.attachEvent("onShow", function() {
				//Sólo se crea la primera vez que abre el PopUp
				if (objPreviewLayout) {
					return;
				}
				
				objPreviewLayout = objPreviewPopUp.attachLayout(450, 450, "1C");
				objPreviewLayout.cells(dhxPreviewCell).hideHeader();
				objPreviewDataView = objPreviewLayout.cells(dhxPreviewCell).attachDataView({
					container:"previewPopUpCell",
					drag:false, 
					select:true,
					type:{
						template:"<div style='font-weight:bold;text-align:center;'><img style='max-width:100px;max-height:85px;' src=#picture#><div>#name#</div></div>",
						width:100,
						border:1,
						height:110
					}
					//x_count: 1
				});
				
				var langg = '/';
				objPreviewDataView.parse([
					//{id:"PreviewLogin", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'loginPrev.png', name:"<?=translate("Login")?>"},
					{id:"PreviewSection", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'sectionPrev.png', name:"<?=translate("Section")?>"},
					{id:"PreviewOpen", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'openPrev.png', name:"<?=translate("Open")?>"},
					{id:"PreviewSimple", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'simplePrev.png', name:"<?=translate("Simple choice")?>"},
					{id:"PreviewMultiple", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'multiplePrev.png', name:"<?=translate("Multiple choice")?>"},
					{id:"PreviewMedia", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'mediaPrev.png', name:"<?=translate("Media")?>"},
					{id:"PreviewOther", picture:'<?=$strScriptPath?>/images/samples/previews/'+langg+'otherPrev.png', name:"<?=translate("Other")?>"}
				], "json");
				
				objPreviewDataView.attachEvent("onItemClick", function (id, ev, html){
					console.log('objPreviewDataView.onItemClick ' + id);
					
					objPreviewPopUp.hide();
					generatePreview(id);
				});
			});
			
			//Agrega el evento para procesos cuando termina de cargar cada frame del layout
			objPropsLayout.attachEvent("onContentLoaded", function(id) {
				console.log('objPropsLayout.onContentLoaded ' + id);
				objPropsLayout.cells(id).progressOff();
			});
			
			objPropsLayout.attachEvent("onPanelResizeFinish", function(names) {
				if (!objPropsLayout) {
					return;
				}
				
				//doOnResizeScreen();
				//return;
				var intWidth = parseInt(objPropsLayout.cells("a").getWidth() -3);
				if (!intWidth || intWidth < 0) {
					return;
				}
				
				$('.ui-footer').css('width', intWidth+	'px');
				/*if ($('#PreviewHTML .ui-content').height() < ($('#PreviewHTML .ui-footer').position().top || 0)) {
					$('#PreviewHTML .ui-content').height(($('#PreviewHTML .ui-footer').position().top || 0) - ($('#PreviewHTML .ui-content').position().top || 0))
				}*/
			});
			
			//Cuando se carga el template se muestran las propiedades para configurar el estilo de las preguntas abiertas numéricas
			var objDiv = document.getElementById("oty<?=otyQuestion?>sectp<?=sectNormal?>qtp<?=qtpOpenNumeric?>elem<?=qelemLabCont?>");
			//Invocada la generación del HTML de preview predeterminado
			generatePreview();
			objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Login")?>");
			objPropsLayout.cells(dhxPropsDetailCell).showHeader();
			
			//Aplica las clases default descargadas del template grabado en la metadata
			if (objCSSStyleColl && !$.isEmptyObject(objCSSStyleColl)) {
				for (var intStyleID in objCSSStyleColl) {
					var objCSSStyle = objCSSStyleColl[intStyleID];
					if (!objCSSStyle) {
						continue;
					}
					
					changeCSSStyle(objCSSStyle.name, objCSSStyle.style);
				}
			}
			
			//Asigna el primer elemento disponible como la selección inical de las propiedades
			objLastElement = $("#PreviewHTML #qfield1_lab");
			if (objLastElement && objLastElement.length) {
				setStyle(undefined, objLastElement[0]);
			}
			
			doOnResizeScreen();
			
			/*$(window).resize(function() {
				doOnResizeScreen();
			});
			*/
			//OMMC 2016-07-12: Comentado el saludo
			//$(window).on("resize", function(){   alert("hola");  });
			
			//Prepara eventos y realiza procesos después de construira la página
			setTimeout(function() {
				if (objPreviewToolbar) {
					var objInput = $(objPreviewToolbar.getInput("TemplateName"));
					if (objInput && objInput.length) {
						objInput.on('blur', function() {
							doSaveTemplateName(objInput.val());
						});
					}
				}
			}, 100);
		}
		
		function doOnResizeScreen() {
			if (!objPropsLayout) {
				return;
			}
			
			var intWidth = parseInt(objPropsLayout.cells("a").getWidth() -3);
			if (!intWidth || intWidth < 0) {
				return;
			}
			
			$('.ui-footer').css('width', intWidth+	'px');
			//if ($('#PreviewHTML .ui-content').height() < ($('#PreviewHTML .ui-footer').position().top || 0)) {
			$('#PreviewHTML .ui-content').height(($('#PreviewHTML .ui-footer').position().top || 0) - ($('#PreviewHTML .ui-content').position().top || 0));
			//}
		}
		
		/* Determina las propiedades de configuración visibles según el tipo de objeto al que se hizo click
		*/
		function getHiddenFields() {
			console.log("getHiddenFields");
			
			//Resetea el array de campos visibles para que por default ninguno lo sea, sólo los aplicables según la pregunta
			hiddenFields = new Object();
			hiddenBlocks = new Object();
			
			if (!objLastElement) {
				return;
			}
			
			//Obtiene el primer div Contenedor de la pregunta, si no hubiera alguno, significaría que no es una pregunta válida de eForms
			/*var objObjectDef = identifyElement(obj);
			
			//Si no es un objeto válido, no debe continuar
			if (!objObjectDef || $.isEmptyObject(objObjectDef)) {
				return;
			}*/
			
			//Se inicializan todos visibles
			/*Nomenclatura de los campos para hacer el POST/Grabar JSOn en Metadata: Component[|Element]_Property
			Donde:
				|: Delimitador entre Element y Component
				[]: Indica que esa porción es opcional, si viene, especificará el atributo "ele" a buscar del componente al que se hizo click ya que es una propiedad que se tiene que
					separar entre Área o Elemento dentro de ella, si no viene, significa que es directamente sobre el objeto al que se hizo click (se buscará su propio atributo "ele")
				Component: lbl|chk|rad|inp|etc (lbl == Etiqueta principal, chk == CheckBox, rad == Radio Button, inp == Input, etc.)
				Element: El # de "Elemento" específico a buscar según los enumerados qElem del archivo config.php (eje. lbl|1 quiere decir el Label del Área, sería innecesario especificar
					el componente lbl porque el Element == 1 es por si mismo la etiqueta del área, pero se hace por claridad y para que los nombres de campos no empiecen en número
					Un elemento -1 quiere decir el área contenedora, para los casos donde es dificil seleccionar dicha área porque se seleccionaría primero un componente específico
					como un label o input
				Property: El nombre de la propiedad a cambiar según CSS, se puede reemplazar "-" por "_" si causa conflicto como id (background-color, font-size, border-top, etc.)
			Límites:
				La suma del texto de Component + Element puede ser cualquier cantidad de caracteres, pero siempre debe llevar el caracter "_" antes de la definición de la propiedad css
				Las definiciones de campo que NO cumplan con esta condición mínima de tener un "_" y un prefijo antes de él (ejemplo: Layoout) se asumirán como configuraciones generales
					que no son específicas de un único componente, sino que cambian algún elemento mas genérico o global
			*/
			
			//El bloque de selección sólo estará desplegable para preguntas de selección sencilla y multiple con tipo de despliegue horizontal y vertical
			//Para todos los demás casos estará oculto
			hiddenBlocks["blkSelection"] = true;
			
			//Definir cuales campos estarán ocultos
			//Pregunta abierta númerica contenedor
			switch (giObjectType) {
				case <?=otySection?>:
					//console.log("********************** getHiddenFields Section  giElementType: "+ giElementType);
					//Cuando se refiere a las secciones, independientemente de la parte seleccionada, tiene los elementos base ya que la configuración es independiente del tipo de sección,
					//con las siguientes excepciones:
					//No tiene text-align (siempre está centrado el título)
					//No tiene apariencia/layout (no existe área para los componentes)
					//No tiene ancho (siempre es el 100%)
					//No tiene alto el body (siempre es el total generado)
					//No tiene alineación vertical ni horizontal (siempre está centrado)
					hiddenFields["obj_width"] = true;
					hiddenFields["obj_text-align"] = true;
					hiddenFields["obj_horizontal-align"] = true;
					hiddenFields["td|<?=qelemLabTD?>_vertical-align"] = true;
					hiddenFields["td|<?=qelemQtnTD?>_vertical-align"] = true;
					hiddenFields["table|<?=qelemQtnTab?>_width"] = true;
					hiddenFields["tr|<?=qelemQtnTR?>_height"] = true;
					hiddenFields["tr|<?=qelemQtnTR?>_background-color"] = true;
					hiddenFields["td|<?=qelemQtnTD?>_border-style"] = true;
					hiddenFields["td|<?=qelemQtnTD?>_border-width"] = true;
					hiddenFields["td|<?=qelemQtnTD?>_border-color"] = true;
					hiddenFields["table|<?=qelemLabTab?>_width"] = true;
					hiddenFields["tr|<?=qelemLabTR?>_height"] = true;
					hiddenFields["tr|<?=qelemLabTR?>_background-color"] = true;
					hiddenFields["td|<?=qelemLabTD?>_border-style"] = true;
					hiddenFields["td|<?=qelemLabTD?>_border-width"] = true;
					hiddenFields["td|<?=qelemLabTD?>_border-color"] = true;
					switch (giElementType) {
						case <?=selemHdrLab?>:
							hiddenFields["obj_height"] = true;
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemBdy?>:
							//console.log("********************** CASE BODY");
							hiddenFields["obj_height"] = true;
							hiddenBlocks["blkText"] = true;
							break;
						case <?=selemFtrBar?>:
							hiddenBlocks["blkText"] = true;
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemHdrBar?>:
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemLeftBtn?>:
						case <?=selemRigthBtn?>:
							hiddenFields["obj_height"] = true;
							hiddenBlocks["blkText"] = true;
							//Genera un error al intentar ocultar el bloque de Bordes
							//hiddenBlocks["blkBorder"] = true; 
							hiddenFields["obj_background-color"] = true;
							break;
						case <?=selemHdrTbl?>:
						case <?=selemTotalTbl?>:
							hiddenFields["obj_height"] = true;
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemLabelFirstCol?>:
							//console.log("********************** ETIQUETA PRIMERA COLUMNA");
							hiddenBlocks["blkFormat"] = true;
							hiddenBlocks["blkBorder"] = true;
							break;
						case <?=selemRowOdd?>:
						case <?=selemRowEven?>:
							//console.log("********************** FILA DE SECCION TABLA");
							hiddenFields["obj_height"] = true;
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemSummary?>:
							hiddenBlocks["blkText"] = true;
							hiddenFields["obj_height"] = true;
							hiddenFields["obj_background-image"] = true;
							break;
						case <?=selemSummaryFirstAns?>:
							hiddenBlocks["blkFormat"] = true;
							hiddenBlocks["blkBorder"] = true;
							break;
						case <?=selemSummaryAnswers?>:
							hiddenBlocks["blkFormat"] = true;
							hiddenBlocks["blkBorder"] = true;
							break;
						case <?=selemAddBtn?>:
							hiddenBlocks["blkBorder"] = true;
							break;
					}
					//RV Abr16: En la configuración de secciones no aplican los botones btnApplyAll y btnApplyGroup, por lo tanto se ocultan.
					hiddenFields["btnApplyAll"]=true;
					hiddenFields["btnApplyGroup"]=true;
					//El campo Posición en la pestaña de Apariencia no aplica para secciones
					hiddenFields["obj|<?=qelemArea?>_layout"]=true;
					break;
				
				case <?=otyQuestion?>:
				{
					hiddenFields["obj_background-image"] = true;
					//Todas las preguntas tienen los elementos base, aunque hay excepciones donde se ocultan
					switch (giElementType) {
						case <?=qelemLabTR?>:
						case <?=qelemQtnTR?>:
							//Área de pregunta y área de respuesta
							//No usa el width y height estándar sino uno personalizado para su contenedor
							hiddenFields["obj_width"] = true;
							hiddenFields["obj_height"] = true;
							hiddenFields["obj_background-color"] = true;
							hiddenFields["obj_horizontal-align"] = true;
							hiddenFields["obj_border-width"] = true;
							hiddenFields["obj_border-style"] = true;
							hiddenFields["obj_border-color"] = true;
							hiddenFields["td|<?=qelemLabTD?>_vertical-align"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_vertical-align"] = true;
							hiddenBlocks["blkText"] = true;
							
							switch (giElementType) {
								case <?=qelemLabTR?>:
									hiddenFields["table|<?=qelemQtnTab?>_width"] = true;
									hiddenFields["tr|<?=qelemQtnTR?>_height"] = true;
									hiddenFields["tr|<?=qelemQtnTR?>_background-color"] = true;
									hiddenFields["td|<?=qelemQtnTD?>_border-style"] = true;
									hiddenFields["td|<?=qelemQtnTD?>_border-width"] = true;
									hiddenFields["td|<?=qelemQtnTD?>_border-color"] = true;
									break;
								case <?=qelemQtnTR?>:
									hiddenFields["table|<?=qelemLabTab?>_width"] = true;
									hiddenFields["tr|<?=qelemLabTR?>_height"] = true;
									hiddenFields["tr|<?=qelemLabTR?>_background-color"] = true;
									hiddenFields["td|<?=qelemLabTD?>_border-style"] = true;
									hiddenFields["td|<?=qelemLabTD?>_border-width"] = true;
									hiddenFields["td|<?=qelemLabTD?>_border-color"] = true;
									break;
							}
							break;
						case <?=qelemQtnMetroMainAttr?>:
						case <?=qelemQtnMetroExtraAttr?>:
							//Los campos personalizados están ocultos por default
							hiddenFields["table|<?=qelemLabTab?>_width"] = true;
							hiddenFields["tr|<?=qelemLabTR?>_height"] = true;
							hiddenFields["tr|<?=qelemLabTR?>_background-color"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-style"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-width"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-color"] = true;
							hiddenFields["table|<?=qelemQtnTab?>_width"] = true;
							hiddenFields["tr|<?=qelemQtnTR?>_height"] = true;
							hiddenFields["tr|<?=qelemQtnTR?>_background-color"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-style"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-width"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-color"] = true;
						
							hiddenBlocks["blkFormat"] = true;
							hiddenBlocks["blkBorder"] = true;
							
							hiddenFields["btnApplyAll"]=true;
							hiddenFields["btnApplyGroup"]=true;
							
							break;
						default:
							//Los campos personalizados están ocultos por default
							hiddenFields["table|<?=qelemLabTab?>_width"] = true;
							hiddenFields["tr|<?=qelemLabTR?>_height"] = true;
							hiddenFields["tr|<?=qelemLabTR?>_background-color"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-style"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-width"] = true;
							hiddenFields["td|<?=qelemLabTD?>_border-color"] = true;
							hiddenFields["table|<?=qelemQtnTab?>_width"] = true;
							hiddenFields["tr|<?=qelemQtnTR?>_height"] = true;
							hiddenFields["tr|<?=qelemQtnTR?>_background-color"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-style"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-width"] = true;
							hiddenFields["td|<?=qelemQtnTD?>_border-color"] = true;
							switch (giElementType) {
								case <?=qelemLabElem?>:
									hiddenFields["td|<?=qelemQtnTD?>_vertical-align"] = true;
									break;
								case <?=qelemQtnElem?>:
									hiddenFields["td|<?=qelemLabTD?>_vertical-align"] = true;
									break;
								case <?=qelemQtnInput?>:
									hiddenFields["td|<?=qelemQtnTD?>_vertical-align"] = true;
									hiddenFields["td|<?=qelemLabTD?>_vertical-align"] = true;
									hiddenFields["obj|<?=qelemArea?>_layout"]=true;
									hiddenFields["obj_horizontal-align"] = true;
									hiddenFields["btnApplyAll"]=true;
									hiddenFields["btnApplyGroup"]=true;
									break;
							}
							break;
					}
					//RV Abr-16: Ocultar el bloque de Fuente para la respuesta en las preguntas tipo Media y algunas tipo "Otro"
					<?//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
					if(giElementType==<?=qelemQtnElem?> && (giQTypeID==<?=qtpPhoto?> || giQTypeID==<?=qtpAudio?> || 
					giQTypeID==<?=qtpVideo?> || giQTypeID==<?=qtpSketch?> || giQTypeID==<?=qtpSketchPlus?> || giQTypeID==<?=qtpDocument?> || giQTypeID==<?=qtpOCR?> ||
					giQTypeID==<?=qtpSkipSection?> || giQTypeID==<?=qtpMessage?> || giQTypeID==<?=qtpCallList?> || giQTypeID==<?=qtpSync?> || 
					giQTypeID==<?=qtpExit?> || giQTypeID==<?=qtpUpdateDest?>))
					{		//console.log("giElementType:"+giElementType+" - giQTypeID:"+giQTypeID);
							//console.log("Ocultar bloque de texto");
							hiddenBlocks["blkText"] = true;
					}
					


					//RV Jul-16: Mostrar el bloque de Selección para la respuesta en las preguntas tipo selección sencilla y múltiple
					if(giElementType==<?=qelemQtnElem?> && 
						((giQTypeID==<?=qtpSingle?> && giQDisplayMode==<?=dspVertical?>) || (giQTypeID==<?=qtpSingle?> && giQDisplayMode==<?=dspHorizontal?>) || 
						(giQTypeID==<?=qtpMulti?> && giQDisplayMode==<?=dspVertical?>) || (giQTypeID==<?=qtpMulti?> && giQDisplayMode==<?=dspHorizontal?>) ||
						(giQTypeID==<?=qtpMulti?> && giQDisplayMode==<?=dspLabelnum?>)
						))
					{		
							hiddenBlocks["blkSelection"] = false;
							//alert (giElementType+"-"+giQTypeID);
					}
					
					//RV Jul2016: Para las pregunta tipo metro al seleccionar una opción de respuesta no se desplegará el bloque para configurar texto
					//ya que este se despliega cuando se selecciona la etiqueta, por que se pueden configurar dos tipos de etiquetas de atributo (principal y extras)
					if(giElementType==<?=qelemQtnElem?> && giQTypeID==<?=qtpSingle?> && giQDisplayMode==<?=dspMetro?>)
					{		
							hiddenBlocks["blkText"] = true;
							hiddenBlocks["blkSelection"] = false;
							hiddenFields["td|<?=qelemQtnTD?>_vertical-align"] = true;
							hiddenFields["td|<?=qelemLabTD?>_vertical-align"] = true;
							hiddenFields["obj_horizontal-align"] = true;
							hiddenFields["obj_width"] = true;
							hiddenFields["obj_height"] = true;
							hiddenFields["btnApplyAll"]=true;
							hiddenFields["btnApplyGroup"]=true;
					}
				}
				
				default:
				{
					break;
				}
			}
		}
		
		/* Invocada al hacer click sobre algún elemento del Preview. Selecciona el elemento y llena la ventana de propiedades con la configuración del elemento
		*/
		function setStyle(e, obj) {
			//debugger;
			console.log('setStyle obj.id == ' + obj.id);
			
			var objObjectDef = identifyElement(obj);
			//Si no es un objeto válido, no debe continuar
			if (!objObjectDef || $.isEmptyObject(objObjectDef)) {
				return;
			}
			
			objLastElement = obj;
			giObjectType = objObjectDef['otype'];
			giQTypeID = objObjectDef['qtype'];
			
			console.log("giObjectType:"+giObjectType);
			console.log("giQTypeID:"+giQTypeID);
			
			if (giQTypeID === undefined) {
				giQTypeID = giNotAssigned;
			}
			giQDisplayMode = objObjectDef['disp'];
			if (giQDisplayMode === undefined) {
				giQDisplayMode = giNotAssigned;
			}
			giElementType = objObjectDef['ele'];
			giCElementType = objObjectDef['cele'];
			
			showElementName();
			
			//Crea el selector alrededor del objeto al que se dió click
			createSelector();
			
			//Actualiza la ventana de propiedades con la configuración de este objeto
			showStyleProps();
			
			//Detiene el evento hacia el objeto padre del que lo inició para que se pueda configurar sólo este componente
			if (e && e.stopPropagation) {
				e.stopPropagation();
			}
		}
		
		/* Muestra la ventana de propiedades a configurar según el tipo de objeto al que se hizo click
		*/
		function showStyleProps() {
			//debugger;
			console.log('showStyleProps');

			objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
			
			//Obtener la configuración de cuales campos estarán visibles
			getHiddenFields();
			objUnits = {type: "select", name: "units", label: "", className:"regFontStyle", options:[
				//{text: "<?=translate("")?>", value: ""},
				{text: "<?=translate("px")?>", value: "px"},
				{text: "<?=translate("%")?>", value: "% "},
				{text: "<?=translate("cm")?>", value: "cm"},
				{text: "<?=translate("mm")?>", value: "mm"},
				{text: "<?=translate("in")?>", value: "in"},
				{text: "<?=translate("pt")?>", value: "pt"},
				{text: "<?=translate("pc")?>", value: "pc"}
			]};
			
			var strApplyToGroupLabel = "Esto NO debería verse en esta configuración... reportar";
			switch (giObjectType) {
				case <?=otyQuestion?>:
					switch (giQTypeID) {
						case <?=qtpOpenNumeric?>:
						case <?=qtpOpenDate?>:
						case <?=qtpOpenString?>:
						case <?=qtpOpenAlpha?>:
						case <?=qtpOpenTime?>:
						case <?=qtpPassword?>:
							strApplyToGroupLabel = "<?=translate("Apply to all open questions")?>";
							break;
						
						case <?=qtpSingle?>:
							strApplyToGroupLabel = "<?=translate("Apply to all simple questions")?>";
							break;
						
						case <?=qtpMulti?>:
							strApplyToGroupLabel = "<?=translate("Apply to all multiple questions")?>";
							break;
						
						case <?=qtpPhoto?>:
						case <?=qtpAudio?>:
						case <?=qtpVideo?>:
						case <?=qtpSketch?>:
						<?//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
						case <?=qtpSketchPlus?>:
						case <?=qtpDocument?>:
							strApplyToGroupLabel = "<?=translate("Apply to all media questions")?>";
							break;
						
						default:
							strApplyToGroupLabel = "<?=translate("Apply to all other questions")?>";
							break;
					}
					break;
			}
			
			var objStyle = ((((((goStyle[giObjectType] || {})[giSectionType] || {})[giQTypeID] || {})[giQDisplayMode] || {})[giElementType] || {})['style'] || {});
			console.log('hiddenFields["btnApplyGroup"]:' + hiddenFields["btnApplyGroup"]);
			//debugger;
			var strImageDefault = "";
			if (goStyle && goStyle[giObjectType]) {
				if (goStyle[giObjectType][giSectionType]) {
					if (goStyle[giObjectType][giSectionType][giQTypeID]) {
						if (goStyle[giObjectType][giSectionType][giQTypeID][giQDisplayMode]) {
							if (goStyle[giObjectType][giSectionType][giQTypeID][giQDisplayMode][giElementType]) {
								if (goStyle[giObjectType][giSectionType][giQTypeID][giQDisplayMode][giElementType]["style"]) {
									if (goStyle[giObjectType][giSectionType][giQTypeID][giQDisplayMode][giElementType]["style"]["obj_background-image"]) {
										strImageDefault = goStyle[giObjectType][giSectionType][giQTypeID][giQDisplayMode][giElementType]["style"]["obj_background-image"];
									}
								}
							}
						}
					}
				}
			}
			objFormData = [
				{type: "settings", position: "label-left", labelWidth: "145", inputWidth: "auto"},
				//Fuente
				{type: "block", inputWidth: "auto", id: "blkText", hidden:hiddenBlocks["blkText"], list:[
					//@JAPRWarning: Ahora los campos se configurarán sólo para el elemento seleccionado
					{type: "select", name: "obj_font-family", label: "<?=translate("Type")?>", hidden:hiddenFields["obj_font-family"], className:"regFontStyle", options:aFontNames},
					{type: "select", name: "obj_font-size", label: "<?=translate("Size")?>", hidden:hiddenFields["obj_font-size"], className:"regFontStyle", options:aFontSizes},
					{type: "select", name: "obj_text-align", label: "<?=translate("Alignment")?>", hidden:hiddenFields["obj_text-align"], className:"regFontStyle", options:aHorizAlign},
					{type: "colorpicker", name: "obj_color", label: "<?=translate("Color")?>", hidden:hiddenFields["obj_color"], className:"regFontStyle"},
					{type: "block", width:"auto", list:[
						{type: "btn2state", name: "obj_font-weight", label:"<?=translate("Format")?>", hidden:hiddenFields["obj_font-weight"], className:"regFontStyle boldImgs", checked:((objStyle["obj_font-weight"])?true:false), userdata:{mappedValues:{false:"", true:"bold"}}},
						{type: "newcolumn"},
						{type: "btn2state", name: "obj_font-style", label:"", hidden:hiddenFields["obj_font-style"], className:"regFontStyle italicImgs", checked:((objStyle["obj_font-style"])?true:false), userdata:{mappedValues:{false:"", true:"italic"}}},
						{type: "newcolumn"},
						{type: "btn2state", name: "obj_text-decoration", label:"", hidden:hiddenFields["obj_text-decoration"], className:"regFontStyle underImgs", checked:((objStyle["obj_text-decoration"])?true:false), userdata:{mappedValues:{false:"", true:"underline"}}}
					]},
					{type: "block", width:"auto", list:[
						{type: "button", name: "btnApplyAll", offsetTop: 20, value: "<?=translate("Apply to all questions")?>", hidden:hiddenFields["btnApplyAll"]},
						{type: "newcolumn"},
						{type: "button", name: "btnApplyGroup", offsetTop: 20, value: strApplyToGroupLabel, hidden:hiddenFields["btnApplyGroup"]}
					]}
				]},
				//Apariencia
				{type: "block", inputWidth: "auto", id: "blkFormat", hidden:hiddenBlocks["blkFormat"], list:[
					{type: "template", name: "obj|<?=qelemArea?>_layout", label: "<?=translate("Position")?>", value: "", hidden:hiddenFields["obj|<?=qelemArea?>_layout"], className:"regFontStyle", format:doGenerateOptions, 
						userdata:{defaultValue: <?=qlayUpDown?>,
							options:{<?=qlayUpDown?>:{name:"<?=translate("Label Up")?>", img:"images/labelUp.png"}, 
								<?=qlayLeftRight?>:{name:"<?=translate("Label Left")?>", img:"images/labelLeft.png"}
							}
						}
					},
					{type: "block", width:"auto", hidden:hiddenFields["obj_width"], list:[
						{type: "input", name: "obj_width", inputWidth:42, label: "<?=translate("Width")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"obj_width_units"})
					]},
					{type: "block", width:"auto", hidden:hiddenFields["table|<?=qelemLabTab?>_width"], list:[
						{type: "input", name: "table|<?=qelemLabTab?>_width", inputWidth:42, label: "<?=translate("Width")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"table|<?=qelemLabTab?>_width_units"}),
					]},
					{type: "block", width:"auto", hidden:hiddenFields["table|<?=qelemQtnTab?>_width"], list:[
						{type: "input", name: "table|<?=qelemQtnTab?>_width", inputWidth:42, label: "<?=translate("Width")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"table|<?=qelemQtnTab?>_width_units"}),
					]},
					{type: "block", width:"auto", hidden:hiddenFields["obj_height"], list:[
						{type: "input", name: "obj_height", inputWidth:42, label: "<?=translate("Height")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"obj_height_units"})
					]},
					{type: "block", width:"auto", hidden:hiddenFields["tr|<?=qelemLabTR?>_height"], list:[
						{type: "input", name: "tr|<?=qelemLabTR?>_height", inputWidth:42, label: "<?=translate("Height")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"tr|<?=qelemLabTR?>_height_units"})
					]},
					{type: "block", width:"auto", hidden:hiddenFields["tr|<?=qelemQtnTR?>_height"], list:[
						{type: "input", name:"tr|<?=qelemQtnTR?>_height", inputWidth:42, label: "<?=translate("Height")?>", value: "", className:"regFontStyle"},
						{type: "newcolumn"},
						$.extend(true, {}, objUnits, {name:"tr|<?=qelemQtnTR?>_height_units"})
					]},
					//RV Mayo16: Al agregar un campo tipo imagen se tuvo que cambiar el archivo js/codebase/dhtmlxFmtd.js porque generaba error al intentar destruir un elemento que
					//aún no estaba creado, se agregó una validación para que sólo se hiciera en caso de existir
					{type: "block", width:"auto", hidden:hiddenFields["obj_background-image"], list:[
						{type: "image", name: "obj_background-image", label: "Image", offsetLeft: 0, imageWidth: 126, imageHeight: 126, value:strImageDefault, url: "loadimagedhtmlx.php", hidden:hiddenFields["obj_background-image"], className:"regFontStyle"},
						{type: "newcolumn"},
						{type: "button", name:"deleteImg", value: "<?=translate("Delete")?>"}
					]},
					{type: "colorpicker", name: "obj_background-color", label: "<?=translate("Color")?>", hidden:hiddenFields["obj_background-color"], className:"regFontStyle"},
					{type: "colorpicker", name: "tr|<?=qelemLabTR?>_background-color", label: "<?=translate("Color")?>", hidden:hiddenFields["tr|<?=qelemLabTR?>_background-color"], className:"regFontStyle"},
					{type: "colorpicker", name: "tr|<?=qelemQtnTR?>_background-color", label: "<?=translate("Color")?>", hidden:hiddenFields["tr|<?=qelemQtnTR?>_background-color"], className:"regFontStyle"},
					{type: "select", name: "td|<?=qelemLabTD?>_vertical-align", label: "<?=translate("Vertical alignment")?>", hidden:hiddenFields["td|<?=qelemLabTD?>_vertical-align"], className:"regFontStyle", options:aVertAlign},
					{type: "select", name:"td|<?=qelemQtnTD?>_vertical-align", label: "<?=translate("Vertical alignment")?>", hidden:hiddenFields["td|<?=qelemQtnTD?>_vertical-align"], className:"regFontStyle", options:aVertAlign},
					{type: "select", name: "obj_horizontal-align", label: "<?=translate("Horizontal alignment")?>", hidden:hiddenFields["obj_horizontal-align"], className:"regFontStyle", options:aHorizAlign},
					{type: "block", width:"auto", list:[
						{type: "button", name: "btnApplyAll", offsetTop: 20, value: "<?=translate("Apply to all questions")?>", hidden:hiddenFields["btnApplyAll"]},
						{type: "newcolumn"},
						//RV Abr2016: Se cambió la etiqueta del botón para que tomé el valor de strApplyToGroupLabel, ya que esta variable
						//tiene asignada etiqueta dependiendo el tipo de preguntas que se está desplegando
						{type: "button", name: "btnApplyGroup", offsetTop: 20, value: strApplyToGroupLabel, hidden:hiddenFields["btnApplyGroup"]}
					]}
				]},
				//Bordes
				{type: "block", inputWidth: "auto", id: "blkBorder", hidden:hiddenBlocks["blkBorder"], list:[
					{type: "block", inputWidth: "auto", hidden:hiddenFields["obj_border"], list:[
						{type: "template", name: "obj_border", label: "<?=translate("Position")?>", value: "", className:"regFontStyle", format:doGenerateOptions, 
							userdata:{defaultValue:"", callbackFn:"doChangeBorder", 
								options:{
									"<?=bortDefault?>":{name:"<?=translate("Default")?>", img:"images/admin/borderDef.png"}, 
									"<?=bortNone?>":{name:"<?=translate("None")?>", img:"images/admin/borderNone.png"},
									"<?=bortAll?>":{name:"<?=translate("All")?>", img:"images/admin/borderAll.png"}
								},
								mappedValues:{0:"", 1:"0_0_0_0", 2:"1_1_1_1"},
								clearFields:["obj_border-left", "obj_border-top", "obj_border-right", "obj_border-bottom"]
							}
						},
						{type: "newcolumn"},
						{type: "template", name: "obj_border-left", label: "", value: "", className:"regFontStyle", format:doGenerateOptions, 
							userdata:{defaultValue:"", callbackFn:"doChangeBorder", btn2State:1, 
								options:{
									"1":{name:"<?=translate("Left")?>", img:"images/admin/borderLeft.png"}
								},
								clearFields:["obj_border"]
							}
						},
						{type: "newcolumn"},
						{type: "template", name: "obj_border-top", label: "", value: "", className:"regFontStyle", format:doGenerateOptions, 
							userdata:{defaultValue:"", callbackFn:"doChangeBorder", btn2State:1, 
								options:{
									"1":{name:"<?=translate("Top")?>", img:"images/admin/borderTop.png"}
								},
								clearFields:["obj_border"]
							}
						},
						{type: "newcolumn"},
						{type: "template", name: "obj_border-right", label: "", value: "", className:"regFontStyle", format:doGenerateOptions, 
							userdata:{defaultValue:"", callbackFn:"doChangeBorder", btn2State:1, 
								options:{
									"1":{name:"<?=translate("Right")?>", img:"images/admin/borderRight.png"}
								},
								clearFields:["obj_border"]
							}
						},
						{type: "newcolumn"},
						{type: "template", name: "obj_border-bottom", label: "", value: "", className:"regFontStyle", format:doGenerateOptions, 
							userdata:{defaultValue:"", callbackFn:"doChangeBorder", btn2State:1, 
								options:{
									"1":{name:"<?=translate("Bottom")?>", img:"images/admin/borderBottom.png"}
								},
								clearFields:["obj_border"]
							}
						},
					]},
					{type: "select", name: "obj_border-style", label: "<?=translate("Style")?>", hidden:hiddenFields["obj_border-style"], className:"regFontStyle", options:aBorderStyle},
					{type: "select", name: "obj_border-width", label: "<?=translate("Size")?>", hidden:hiddenFields["obj_border-width"], className:"regFontStyle", options:aBorderSizes},
					{type: "colorpicker", name: "obj_border-color", label: "<?=translate("Color")?>", hidden:hiddenFields["obj_border-color"], className:"regFontStyle"},
					{type: "select", name: "td|<?=qelemLabTD?>_border-style", label: "<?=translate("Style")?>", hidden:hiddenFields["td|<?=qelemLabTD?>_border-style"], className:"regFontStyle", options:aBorderStyle},
					{type: "select", name: "td|<?=qelemLabTD?>_border-width", label: "<?=translate("Size")?>", hidden:hiddenFields["td|<?=qelemLabTD?>_border-width"], className:"regFontStyle", options:aBorderSizes},
					{type: "colorpicker", name: "td|<?=qelemLabTD?>_border-color", label: "<?=translate("Color")?>", hidden:hiddenFields["td|<?=qelemLabTD?>_border-color"], className:"regFontStyle"},
					{type: "select", name: "td|<?=qelemQtnTD?>_border-style", label: "<?=translate("Style")?>", hidden:hiddenFields["td|<?=qelemQtnTD?>_border-style"], className:"regFontStyle", options:aBorderStyle},
					{type: "select", name: "td|<?=qelemQtnTD?>_border-width", label: "<?=translate("Size")?>", hidden:hiddenFields["td|<?=qelemQtnTD?>_border-width"], className:"regFontStyle", options:aBorderSizes},
					{type: "colorpicker", name: "td|<?=qelemQtnTD?>_border-color", label: "<?=translate("Color")?>", hidden:hiddenFields["td|<?=qelemQtnTD?>_border-color"], className:"regFontStyle"},
					{type: "block", width:"auto", list:[
						{type: "button", name: "btnApplyAll", offsetTop: 20, value: "<?=translate("Apply to all questions")?>", hidden:hiddenFields["btnApplyAll"]},
						{type: "newcolumn"},
						//RV Abr2016: Se cambió la etiqueta del botón para que tomé el valor de strApplyToGroupLabel, ya que esta variable
						//tiene asignada etiqueta dependiendo el tipo de preguntas que se está desplegando
						{type: "button", name: "btnApplyGroup", offsetTop: 20, value: strApplyToGroupLabel, hidden:hiddenFields["btnApplyGroup"]}
					]}
				]},
				//Configurar Opción Seleccionada para preguntas de selección sencilla y múltiple
				{type: "block", inputWidth: "auto", id: "blkSelection", hidden:hiddenBlocks["blkSelection"], list:[
					/*	
					{type: "select", name: "obj_font-family", label: "<?=translate("Type")?>", hidden:hiddenFields["obj_font-family"], className:"regFontStyle", options:aFontNames},
					{type: "select", name: "obj_font-size", label: "<?=translate("Size")?>", hidden:hiddenFields["obj_font-size"], className:"regFontStyle", options:aFontSizes},
					{type: "select", name: "obj_text-align", label: "<?=translate("Alignment")?>", hidden:hiddenFields["obj_text-align"], className:"regFontStyle", options:aHorizAlign},
					*/
					{type: "colorpicker", name: "obj|<?=qelemQtnOpSelec?>_color", label: "<?=translate("Font Color")?>", hidden:hiddenFields["obj|<?=qelemQtnOpSelec?>_color"], className:"regFontStyle"},
					{type: "colorpicker", name: "obj|<?=qelemQtnOpSelec?>_background-color", label: "<?=translate("Background Color")?>", hidden:hiddenFields["obj|<?=qelemQtnOpSelec?>_background-color"], className:"regFontStyle"},
					/*
					{type: "block", width:"auto", list:[
						{type: "btn2state", name: "obj_font-weight", label:"<?=translate("Format")?>", hidden:hiddenFields["obj_font-weight"], className:"regFontStyle boldImgs", checked:((objStyle["obj_font-weight"])?true:false), userdata:{mappedValues:{false:"", true:"bold"}}},
						{type: "newcolumn"},
						{type: "btn2state", name: "obj_font-style", label:"", hidden:hiddenFields["obj_font-style"], className:"regFontStyle italicImgs", checked:((objStyle["obj_font-style"])?true:false), userdata:{mappedValues:{false:"", true:"italic"}}},
						{type: "newcolumn"},
						{type: "btn2state", name: "obj_text-decoration", label:"", hidden:hiddenFields["obj_text-decoration"], className:"regFontStyle underImgs", checked:((objStyle["obj_text-decoration"])?true:false), userdata:{mappedValues:{false:"", true:"underline"}}}
					]},
					*/
					{type: "block", width:"auto", list:[
						{type: "button", name: "btnApplyAll", offsetTop: 20, value: "<?=translate("Apply to all questions")?>", hidden:hiddenFields["btnApplyAll"]},
						{type: "newcolumn"},
						{type: "button", name: "btnApplyGroup", offsetTop: 20, value: strApplyToGroupLabel, hidden:hiddenFields["btnApplyGroup"]}
					]}
				]}				
			];
			console.log('hiddenFields["btnApplyGroup"] - 2:' + hiddenFields["btnApplyGroup"]);
			if (objPropsLayout.cells(dhxPropsDetailCell).cell) {
				$(objPropsLayout.cells(dhxPropsDetailCell).cell).addClass("visibleTabs");
			}
			
			//RV Ago2016: Ocurria un error cuando en un componente el Tab seleccionado no estaba visible en el siguiente componente al que se daba click
			//cómo quiera se ponía como el tab activo (a pesar de no estar visible) y eso generaba que no se mostraran los campos correctos en la forma
			var activeTab="";

			if(gsLastTabSelected)
			{
				//Si anteriormente se tenía un tab seleccionado, se verifica si está visible en este componente para ponerlo como activo
				if(gsLastTabSelected == "textTab" && (hiddenBlocks["blkText"]==undefined || hiddenBlocks["blkText"]==false))
				{
					activeTab = "textTab";
				}
				else if (gsLastTabSelected == "formatTab" && (hiddenBlocks["blkFormat"]==undefined || hiddenBlocks["blkFormat"]==false))
				{
					activeTab = "formatTab";
				}
				else if (gsLastTabSelected == "borderTab" && (hiddenBlocks["blkBorder"]==undefined || hiddenBlocks["blkBorder"]==false))
				{
					activeTab = "borderTab";
				}
				else if (gsLastTabSelected == "selectionTab" && (hiddenBlocks["blkSelection"]==undefined || hiddenBlocks["blkSelection"]==false))
				{
					activeTab = "selectionTab";
				}
			}

			//Si activeTab no está asignado quiere decir que gsLastTabSelected no tenía valor o que el tab activo anteriormente ya no está visible en el componente actual
			//entonces se pone como activo el primer tab visible del componente actual
			if(activeTab=="")
			{
				if(hiddenBlocks["blkText"]==undefined || hiddenBlocks["blkText"]==false)
				{
					activeTab="textTab";
				}
				else if(hiddenBlocks["blkFormat"]==undefined || hiddenBlocks["blkFormat"]==false)
				{
					activeTab="formatTab";
				}
				else if(hiddenBlocks["blkBorder"]==undefined || hiddenBlocks["blkBorder"]==false)
				{
					activeTab="borderTab";
				}
				else if(hiddenBlocks["blkSelection"]==undefined || hiddenBlocks["blkSelection"]==false)
				{
					activeTab="selectionTab";
				}
			}
			
			objPropsTabbar = objPropsLayout.cells(dhxPropsDetailCell).attachTabbar();
			//objPropsTabbar.addTab("templateTab","<?=translate("Template")?>",null,null,(gsLastTabSelected == "templateTab" || !gsLastTabSelected),false);
			//objPropsTabbar.addTab("sectionTab","<?=translate("Section")?>",null,null,gsLastTabSelected == "sectionTab",false);
			objPropsTabbar.addTab("textTab","<?=translate("Font")?>",null,null,activeTab=="textTab",false);
			objPropsTabbar.addTab("formatTab","<?=translate("Layout")?>",null,null,activeTab=="formatTab",false);
			objPropsTabbar.addTab("borderTab","<?=translate("Border")?>",null,null,activeTab=="borderTab",false);
			objPropsTabbar.addTab("selectionTab","<?=translate("Selection")?>",null,null,activeTab== "selectionTab",false);
			var tabIDS = objPropsTabbar.getAllTabs();
			//objPropsForm = objPropsTabbar.cells("templateTab").attachForm(objFormData);
			//objPropsTabbar.tabs("sectionTab").attachObject("blkSection");
			objPropsForm = objPropsTabbar.cells("textTab").attachForm(objFormData);
			//objPropsTabbar.tabs("textTab").attachObject("blkText");
			objPropsTabbar.tabs("formatTab").attachObject("blkFormat");
			objPropsTabbar.tabs("borderTab").attachObject("blkBorder");
			objPropsTabbar.tabs("selectionTab").attachObject("blkSelection");
			
			//Asigna la lista de campos que pertenecen a cada tab (es importante para los botones de Aplicar, ya que sólo se aplicarán las configuraciones de la tab seleccionada)
			objPropsTabbar.tabs("textTab").fieldsList = ["obj_font-family","obj_font-size","obj_text-align","obj_color","obj_font-weight","obj_font-style","obj_text-decoration"];
			objPropsTabbar.tabs("formatTab").fieldsList = ["obj|<?=qelemArea?>_layout","obj_width","table|<?=qelemLabTab?>_width","table|<?=qelemQtnTab?>_width",
				"obj_height","tr|<?=qelemLabTR?>_height","tr|<?=qelemQtnTR?>_height","obj_background-color","tr|<?=qelemLabTR?>_background-color",
				"tr|<?=qelemQtnTR?>_background-color","td|<?=qelemLabTD?>_vertical-align","td|<?=qelemQtnTD?>_vertical-align","obj_horizontal-align", "obj_background-image"];
			objPropsTabbar.tabs("borderTab").fieldsList = ["obj_border","obj_border-style","obj_border-width","obj_border-color","td|<?=qelemLabTD?>_border-style",
				"td|<?=qelemLabTD?>_border-width","td|<?=qelemLabTD?>_border-color","td|<?=qelemQtnTD?>_border-style","td|<?=qelemQtnTD?>_border-width","td|<?=qelemQtnTD?>_border-color"];
			objPropsTabbar.tabs("selectionTab").fieldsList = ["obj|<?=qelemQtnOpSelec?>_color","obj|<?=qelemQtnOpSelec?>_background-color"];
				
			
			//Oculta las tabs que se marcaran como inválidas para el objeto seleccionado
			if (hiddenBlocks["blkText"]) {
				objPropsTabbar.cells("textTab").hide();
			}
			if (hiddenBlocks["blkFormat"]) {
				objPropsTabbar.cells("formatTab").hide();
			}
			if (hiddenBlocks["blkBorder"]) {
				objPropsTabbar.cells("borderTab").hide();
			}
			if (hiddenBlocks["blkSelection"]) {
				objPropsTabbar.cells("selectionTab").hide();
			}
			
			//Evento para seleccionar una Tabbar
			objPropsTabbar.attachEvent("onSelect", function(id, lastId) {
				gsLastTabSelected = id;
				return true;
			});
			
			//Evento para click en botones
			objPropsForm.attachEvent("onButtonClick", function(name) {
				console.log('objPropsForm.onButtonClick ' + name);
				
				if(name=="deleteImg")
				{
					if (objPropsForm) {
						objPropsForm.setItemValue("obj_background-image", "");
						doChangeProperty("obj_background-image", "");
					}
				}
				else
				{
					doApplyStyleTo(name);
				}
			});
			
			//Evento para modificación de los inputs
			objPropsForm.attachEvent("onChange", function(name, value, state) {
				var varValue = value;
				if (value === null && state !== undefined) {
					//En este caso se trata de un botón que contiene estados, no valores
					varValue = state;
				}
				
				doChangeProperty(name, varValue);
			});
			
			objPropsForm.attachEvent("onImageUploadSuccess", function(name, value, extra){
				var bgImage = objPropsForm.getItemValue(name);
				//alert("name:"+name+", bgImage: " + bgImage + ", value:" + value);
				//debugger;
				doChangeProperty(name, bgImage);
			});
			
			objPropsForm.attachEvent("onImageUploadFail", function(name, extra){
				if ( extra ) {
					alert(extra);
				}
			});
			
			setFormFieldValues();
		}
		
		/* Envía una señal al servicio para que propague los cambios de la cejilla indicada a los elementos según el botón que se presionó, esto se hace enviando la configuración
		actual de los campos que si tengan un valor asignado, recorriendo aquellos visibles en la cejilla actualmente seleccionada de la ventana de propiedades
		*/
		function doApplyStyleTo(sBtnName) {
			console.log('doApplyStyleTo ' + sBtnName);
			
			if (!objPropsForm || !objPropsTabbar) {
				return;
			}
			
			//Identifica la cejilla actual y los campos a enviar
			var strCurrentTab = objPropsTabbar.getActiveTab();
			var objTabbar = objPropsTabbar.tabs(strCurrentTab);
			if (!objTabbar) {
				return;
			}
			
			var arrFields = objTabbar.fieldsList;
			var arrValidFields = new Array();;
			//Recorre los campos de la tabbar y prepara el objeto para mandar su estilo
			var objStyle = {};
			objPropsForm.forEachItem(function(name) {
				var strName = name;
				
				//Si tiene "_" en el nombre de campo, quiere decir que es una configuración que permite cambiar
				//la apariencia del preview, así que es válida para seleccionar al elemento
				var intPos = strName.indexOf("_");
				if (intPos == -1) {
					//Otros campos como columnas o agrupadores no son válidos para este proceso, sólo los que representan propiedades de estilo
					return;
				}
				
				//Sólo debe considerar los campos del tab seleccionado
				if (arrFields.indexOf(strName) == -1 || hiddenFields[strName]) {
					return;
				}
				arrValidFields.push(strName);
				
				//Si el nombre de campo tiene sufijo "_units", quiere decir que sólo es un campo adicional, así que graba el
				//campo real y sólo agrega las unidades al final
				var strValue = "";
				var blnPropField = true;
				var intPos = strName.indexOf("_units");
				if (intPos != -1) {
					blnPropField = false;
					//strName = strName.substr(0, intPos);
					//strValue = objPropsForm.getItemValue(strName) + value;
					//El campo de las unidades no se va a procesar, lo hará con el campo de estilo directo
					return;
				}
				else {
					//Si el nombre no tiene sufijo "_units", se verifica si existe un campo con este nombre que si lo
					//tenga y se complementa el valor con esas unidades
					var strValue = objPropsForm.getItemValue(strName);
					var typeField = objPropsForm.getItemType(strName)
					//Si el tipo de campo es 2state, debe hacer la conversión ya que el valor aunque regresa como integer, se preparó como boolean
					if (typeField == "btn2state") {
						strValue = (strValue)?true:false;
					}
					
					var objMappedValues = objPropsForm.getUserData(strName, "mappedValues");
					if (objMappedValues) {
						if (objMappedValues[strValue] !== undefined) {
							strValue = objMappedValues[strValue];
						}
					}
					
					//Casos excepción de campos que se manejan de forma especial
					if (strName.substr(-12) == "border-width") {
						//El grosor del borde siempre se maneja en px
						strValue += "px";
					}
					
					switch (strName) {
						case "obj_border":
							//El borde puede traer un valor == -1, lo que significa que no se eligió un borde general sino alguna de las 4 partes individuales, así que debe unirlas
							//Si por alguna razón no hubiera algún valor seleccionado en ellas, entonces omitirá esta configuración
							if (strValue == "-1") {
								var intBorderLeft = (objPropsForm.getItemValue("obj_border-left") != 0)?1:0;
								var intBorderTop = (objPropsForm.getItemValue("obj_border-top") != 0)?1:0;
								var intBorderRight = (objPropsForm.getItemValue("obj_border-right") != 0)?1:0;
								var intBorderBottom = (objPropsForm.getItemValue("obj_border-bottom") != 0)?1:0;
								
								if (intBorderLeft || intBorderTop || intBorderRight || intBorderBottom) {
									//Genera el nuevo string de borde con las selecciones actuales
									strValue = intBorderLeft+'_'+intBorderTop+'_'+intBorderRight+'_'+intBorderBottom;
								}
								else {
									strValue = "";
								}
							}
							break;
					}
					
					var strUnitsValues = objPropsForm.getItemValue(strName + "_units");
					//Si no se había especificado un valor, entonces no debe agregar las unidades para no grabar un estile donde solo existan estas sin el valor correspondiente
					if (strUnitsValues && strValue !== "") {
						//Hay unidades, se identifica y asigna su valor (en este caso estaba camiando el valor
						//de la configuración, así que unidades vacías no son válidas)
						strValue += strUnitsValues;
					}
				}
				
				//Si el valor es vacío (o considerable vacío) entonces lo agrega a la colección como cadena vacía para que literalmente se limpie al grabar
				var strEmptyValue = objPropsForm.getUserData(strName, "defaultValue");
				if (strValue === undefined || strValue === "" || (strEmptyValue !== "" && strValue == strEmptyValue)) {
					objStyle[strName] = "";
				}
				else {
					objStyle[strName] = strValue;
				}
			});
			
			//Inicia el proceso de grabado de los datos
			var objParams = {};
			objParams.Design = 1;
			objParams.RequestType = reqAjax;
			objParams.Process = "Edit";
			objParams.ObjectType = <?=otyAppCustStyles?>;
			
			//Agrega los parámetros para el grabado específico de un elemento, si se quisieran grabar todos, entonces deberia enviarse un array de elementos en lugar de uno solo
			objParams.TemplateID = giTemplateID;
			objParams.SectionType = giSectionType;
			objParams.ItemType = giObjectType;
			objParams.QTypeID = giQTypeID;
			objParams.QDisplayMode = giQDisplayMode;
			objParams.ElementType = giElementType;
			objParams.Style = JSON.stringify(objStyle);
			objParams.Fields = arrValidFields.join(",");
			
			//Los datos se mandan como una serie de parámetros con los elementos que se están modificando, por el momento se asume que es una única combinación, sin embargo
			//se podría enviar una serie de arrays con todos los estilos modificados si se implementa un grabado masivo de la ventana de configuración
			//Agrega el parámetro del botón que fue presionado
			var strParams = sBtnName + '=1';
			var strAnd = '&';
			for (var strProp in objParams) {
				strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
				strAnd = '&';
			}
			
			$('#divSaving').show();
			if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
				objPropsLayout.cells(dhxPropsDetailCell).progressOn();
			}
			window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
				$('#divSaving').hide();
				if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
					objPropsLayout.cells(dhxPropsDetailCell).progressOff();
				}
				
				doUpdateCSSContainer(loader);
			});
		}
		
		/* Genera el HTML de la selección de un checkbox basado en imagenes (componente template personalizado de la forma). Se basa en userdata.options para llenar los elementos
		Esta función se invoca cada vez que se llama a setItemValue, así que se debe volver a considerar "seleccionado" el elemento correspondiente al value recibido
		*/
		function doGenerateOptions(name, value) {
			//Es importante la validación de objPropsForm.getUserData, ya que los campos "template" se programaron para auto-generarse a partir de su propio userdata, sin embargo la
			//primera vez (mientras se crea la forma) no existe la referencia global así que no se pueden generar, tampoco existe si se acaba de destruir por haber seleccionado algún
			//componente del preview (pero en ese caso si existiría el anterior objPropsForm, así que sin el || habría entrado a esta función), sólo es válido entrar cuando la forma ya
			//está totalmente reconstruida, lo cual sucede hasta el llamado a setFormFieldValues
			if (!objPropsForm || !objPropsForm.getUserData) {
				return "";
			}
			
			//var bln2State = (objPropsForm.getUserData(name, "btn2State"))?true:false;
			
			var strFunction = objPropsForm.getUserData(name, "callbackFn")
			if (!strFunction) {
				strFunction = "doChangeLayout";
			}
			
			var objOptions = objPropsForm.getUserData(name, "options")
			if (!objOptions) {
				return "";
			}
			var arrKeys = Object.keys(objOptions);
			if (!arrKeys || !arrKeys.length) {
				return '';
			}
			
			value = parseInt(value);
			if (isNaN(value)) {
				value = 0;
			}
			
			var strHTML = "<div style=\"width:"+(arrKeys.length * 36)+"px;height:36px;\">";
			for (var intIndex in objOptions) {
				var objItem = objOptions[intIndex];
				if (!objItem) {
					continue;
				}
				
				var intKey = intIndex;
				if (objItem.id !== undefined) {
					intKey = objItem.id;
				}
				
				//Si el botón se configuró como de doble estado (y asumiendo que sólo tiene una opción) entonces cambia el valor del key según el valor actual del campo, ya que siempre
				//se habría devuelto el mismo key
				/*if (bln2State && arrKeys.length == 1) {
					var intValue = objPropsForm.getItemValue(name)
					if (intValue == intKey) {
						//Se resetea el valor a "", independientemente de que key se había usado, vacío siempre será el valor cuando no hay valor
						intKey = "";
					}
				}*/
				
				var strFocusClass = (intKey == value)?" imgCheckFocus":"";
				var strLabel = objItem.name;
				var strImg = objItem.img;
				strHTML += "<div id=\""+name+"_"+intKey+"\" class=\"imgCheck"+strFocusClass+"\" onmouseenter=\"doSelectElement({name:'"+name+"'});\" onmouseleave=\"doSelectElement({name:'"+name+"'}, true);\">"+
					"<img class=\"imgCheck\" src=\""+strImg+"\" onclick=\""+strFunction+"(event, this, '"+name+"', '"+intKey+"');\" title=\""+strLabel+"\"></div>";
			}
			strHTML += "</div>";
			
			return strHTML;
		}
		
		/* Asigna el valor del borde según la configuración actual, modificando los campos que se requieren para ello entre los 5 diferentes que existen
		*/
		function doSetBorderValue(name, value) {
			console.log('doSetBorderValue ' + name + ' => ' + value);
			
			if (!objPropsForm) {
				return;
			}
			
			//Independientemente de que va a asignar, debe generar los campos de bordes individuales sin selección antes de continuar
			objPropsForm.setItemValue(name+"-left", "0");
			objPropsForm.setItemValue(name+"-top", "0");
			objPropsForm.setItemValue(name+"-right", "0");
			objPropsForm.setItemValue(name+"-bottom", "0");
			
			//Las configuraciones default incluso la de todos los bordes activos, seleccionarán del campo principal de borde único, mientras que el resto de las combinaciones
			//seleccionarán los bordes individuales
			switch (value) {
				case "":		//Borde default
					objPropsForm.setItemValue(name, <?=bortDefault?>);
					break;
				case "0_0_0_0":	//Sin borde
					objPropsForm.setItemValue(name, <?=bortNone?>);
					break;
				case "1_1_1_1":	//todos los borders
					objPropsForm.setItemValue(name, <?=bortAll?>);
					break;
				default:
					//Primero debe reconstruir el campo del borde general pero con un valor tal que no seleccione ninguna opción
					objPropsForm.setItemValue(name, <?=bortNoSel?>);
					
					//Combinación de bordes individuales
					var arrBorders = String(value).split('_');
					if ($.isArray(arrBorders)) {
						var intBorder = arrBorders[0];
						if (intBorder == "1") {
							objPropsForm.setItemValue(name+"-left", "1");
						}
						var intBorder = arrBorders[1];
						if (intBorder == "1") {
							objPropsForm.setItemValue(name+"-top", "1");
						}
						var intBorder = arrBorders[2];
						if (intBorder == "1") {
							objPropsForm.setItemValue(name+"-right", "1");
						}
						var intBorder = arrBorders[3];
						if (intBorder == "1") {
							objPropsForm.setItemValue(name+"-bottom", "1");
						}
					}
					break;
			}
		}
		
		/* Evento para cambiar el valor de un borde según los border parciales seleccionados o bien la opción global elegida
		*/
		function doChangeBorder(e, obj, name, value, bSave) {
			console.log('doChangeBorder ' + name + ' => ' + value);
			
			if (!objPropsForm) {
				return;
			}
			
			if (bSave === undefined) {
				bSave = true;
			}
			
			var strBorder = "";
			//Dependiendo de la propiedad recibida, se va a concatenar el valor final a enviar para el borde y se cambiará la selección de las propiedades al valor mas adecuado posible
			switch (name) {
				case "obj_border":
					//En este caso se pudieron recibir 3 tipos de estados: vacio, ninguno o todos
					//Se limpiarán los bordes independientes
					objPropsForm.setItemValue("obj_border-left", "");
					objPropsForm.setItemValue("obj_border-top", "");
					objPropsForm.setItemValue("obj_border-right", "");
					objPropsForm.setItemValue("obj_border-bottom", "");
					
					//Se graba el estado final recibido
					if (bSave) {
						objPropsForm.setItemValue(name, value);
					}
					
					var objMappedValues = objPropsForm.getUserData(name, "mappedValues");
					if (objMappedValues) {
						if (objMappedValues[value] !== undefined) {
							strBorder = objMappedValues[value];
						}
					}
					break;
				default:
					//En este caso es un borde independiente, sólo se pudo recibir el estado de habilitado, así que primero se lee el valor actual del campo para saber el estado y se invierte
					//para dejar el estado final
					//Se limpiará la configuración global si y sólo si queda algún borde independiente seleccionado, si no, entonces la opción global de vacio (predeterminado) se elegirá
					//Primero asigna el valor del campo recién seleccionado (sólo si se está grabando)
					if (bSave) {
						var intValue = (objPropsForm.getItemValue(name) != 0)?1:0;
						intValue = (!intValue)?1:0;
						
						objPropsForm.setItemValue(name, intValue);
					}
					
					var intBorderLeft = (objPropsForm.getItemValue("obj_border-left") != 0)?1:0;
					var intBorderTop = (objPropsForm.getItemValue("obj_border-top") != 0)?1:0;
					var intBorderRight = (objPropsForm.getItemValue("obj_border-right") != 0)?1:0;
					var intBorderBottom = (objPropsForm.getItemValue("obj_border-bottom") != 0)?1:0;
					
					//Si quedaron bordes seleccionados, se resetean todas las configuraciones globales de bordes
					if (intBorderLeft || intBorderTop || intBorderRight || intBorderBottom) {
						//Se forzará a un valor que no se encuentre entre las opciones para que no muestre como seleccionado
						objPropsForm.setItemValue("obj_border", "<?=bortNoSel?>");
						
						//Genera el nuevo string de borde con las selecciones actuales
						strBorder = intBorderLeft+'_'+intBorderTop+'_'+intBorderRight+'_'+intBorderBottom;
					}
					else {
						///En este caso se desea que se genere sin bordes, ya que se eliminaron todos los individuales en lugar de haber seleccionado default
						strBorder = "<?=bortNone?>";
						objPropsForm.setItemValue("obj_border", strBorder);
					}
					break;
			}
			
			//Invoca al grabado de la metadata ya que los campos de forma tipo template no entran por el evento onChange como los demás por ser personalizados
			if (bSave) {
				doChangeProperty("obj_border", strBorder);
			}
		}
		
		/* Evento para seleccionar uno de los Layouts disponibles y grabar el cambio restableciendo los demás
		El parámetro bSave se utiliza para simplemente seleccionar la respuesta correcta en lugar de también grabar (false), si no se recibe se asume que se quiere grabar también
		*/
		function doChangeLayout(e, obj, name, value, bSave) {
			console.log('doChangeLayout ' + name + ' => ' + value);
			
			if (bSave === undefined) {
				bSave = true;
			}
			
			if (!obj || !name) {
				return;
			}
			
			var objParent = obj.parentElement;
			if (!objParent) {
				return;
			}
			
			var objMainDiv = $(objParent.parentElement);
			if (!objMainDiv || !objMainDiv.length) {
				return;
			}
			
			value = parseInt(value);
			if (isNaN(value)) {
				value = 0;
			}
			
			//objMainDiv.find("div").css('background-color', 'transparent');
			//$(objParent).css('background-color', '#a1ceed');
			objMainDiv.find("div").removeClass("imgCheckFocus");
			$(objParent).addClass("imgCheckFocus");
			
			if (bSave) {
				doChangeProperty(name, value);
			}
		}
		
		/* Obtiene un objeto referencia a estilo dado los parámetros que lo identifican (si no se especifica objData, se usará al objeto actualmente seleccionado */
		function getObjectStyle(iElementType, objData, bSetDirty) {
			console.log('getObjectStyle ' + name);
			
			var intObjectType = giObjectType;
			var intSectionType = giSectionType;
			var intQTypeID = giQTypeID;
			var intQDisplayMode = giQDisplayMode;
			var intElementType = iElementType;
			if (objData) {
				if (objData.giObjectType != undefined) {
					intObjectType = objData.giObjectType;
				}
				if (objData.giSectionType != undefined) {
					intSectionType = objData.giSectionType;
				}
				if (objData.giQTypeID != undefined) {
					intQTypeID = objData.giQTypeID;
				}
				if (objData.giQDisplayMode != undefined) {
					intQDisplayMode = objData.giQDisplayMode;
				}
				if (objData.giElementType != undefined) {
					intElementType = objData.giElementType;
				}
			}
			
			if (!goStyle[intObjectType]) {
				goStyle[intObjectType] = {};
			}
			if (!goStyle[intObjectType][intSectionType]) {
				goStyle[intObjectType][intSectionType] = {};
			}
			if (!goStyle[intObjectType][intSectionType][intQTypeID]) {
				goStyle[intObjectType][intSectionType][intQTypeID] = {};
			}
			if (!goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode]) {
				goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode] = {};
			}
			if (!goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType]) {
				goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType] = {}
				goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType]['dirty'] = 1;
				goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType]['style'] = {};
			}
			if (bSetDirty) {
				goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType]['dirty'] = 1;
			}
			var objStyle = goStyle[intObjectType][intSectionType][intQTypeID][intQDisplayMode][intElementType]['style'];
			
			return objStyle;
		}
		
		/* Función invocada cuando ha cambiado alguna configuración de estilo del elemento seleccionado o su contenedor o alguno especial generado para el objeto seleccionado
		Desencadena el grabado en el server y la actualización del preview
		*/
		function doChangeProperty(name, value) {
			console.log('doChangeProperty ' + name);
			
			var strName = name;
			var strValue = value;
			if (strName == "TemplateName") {
				doSaveTemplateName(strValue);
				return;
			}
			
			//oty indica el tipo de objeto (pregunta, sección, etc.)
			var oty = parseInt(objPropsForm.getUserData(strName, "oty"));
			
			//Si viene la propiedad mappedValues, quiere decir que el valor a grabar debe cambiar en función del objeto mappedValues (usado para componentes como CheckBoxes donde un
			//true/false no sirve como propiedad de CSS, sino que se debe enviar otro valor para el campo
			var objMappedValues = objPropsForm.getUserData(strName, "mappedValues");
			if (objMappedValues) {
				if (objMappedValues[strValue] !== undefined) {
					strValue = objMappedValues[strValue];
				}
			}
			
			//Si el nombre de campo tiene sufijo "_units", quiere decir que sólo es un campo adicional, así que graba el
			//campo real y sólo agrega las unidades al final
			var strFieldValue = "";
			var strUnitsValues = "";
			var intPos = strName.indexOf("_units");
			if (intPos != -1) {
				strName = strName.substr(0, intPos);
				strFieldValue = objPropsForm.getItemValue(strName);
				strValue = objPropsForm.getItemValue(strName) + value;
				strUnitsValues = value;
			}
			else {
				//Si el nombre no tiene sufijo "_units", se verifica si existe un campo con este nombre que si lo
				//tenga y se complementa el valor con esas unidades
				var strFieldValue = value;
				strUnitsValues = objPropsForm.getItemValue(strName + "_units");
				//Si no se había especificado un valor, entonces no debe agregar las unidades para no grabar un estile donde solo existan estas sin el valor correspondiente
				if (strUnitsValues && strFieldValue !== "") {
					//Hay unidades, se identifica y asigna su valor (en este caso estaba camiando el valor
					//de la configuración, así que unidades vacías no son válidas)
					strValue += strUnitsValues;
				}
			}
			
			//Casos excepción de campos que se manejan de forma especial
			if (strName.substr(-12) == "border-width") {
				//El grosor del borde siempre se maneja en px
				strValue += "px";
			}
			
			//Actualiza elementos del componente seleccionador de valor
			switch (objPropsForm.getItemType(strName)) {
				case "colorpicker":
					//El colorpicker debe cambiar el color de fondo del input donde se selecciona
					var objColorPicker = objPropsForm.getColorPicker(strName);
					if (objColorPicker && objColorPicker._nodes && objColorPicker._nodes[0] && objColorPicker._nodes[0].node) {
						var strNodeId = objColorPicker._nodes[0].node.id;
						if (strNodeId) {
							$('#'+strNodeId).css('background-color', strValue);
						}
					}
					break;
			}
			
			//Actualiza la vista de preview
			if (oty && oty==<?=otySection?>) {
				changeStylePreviewSection(strName, strValue) 
			}
			else {
				changeStylePreview(strName, strValue);
			}
			
			//Agregada la asignación automática de las áreas de etiqueta y pregunta al cambiar el estilo de despliegue
			if (strName == "obj|<?=qelemArea?>_layout" && strValue == "2") {
				//Tabla de la pregunta
				var intElementType = <?=qelemLabTab?>;
				var objStyle = getObjectStyle(intElementType, undefined, true);
				objStyle["table|<?=qelemLabTab?>_width"] = "50% ";
				doSaveStyle(intElementType, objStyle);
				//Actualiza el componente en la ventana de propiedades si era el seleccionado
				if (giElementType == <?=qelemLabTR?>) {
					objPropsForm.setItemValue("table|<?=qelemLabTab?>_width", "50");
					objPropsForm.setItemValue("table|<?=qelemLabTab?>_width_units", "% ");
				}
				
				//Tabla de la respuesta
				var intElementType = <?=qelemQtnTab?>;
				var objStyle = getObjectStyle(intElementType, undefined, true);
				objStyle["table|<?=qelemQtnTab?>_width"] = "50% ";
				doSaveStyle(intElementType, objStyle);
				//Actualiza el componente en la ventana de propiedades si era el seleccionado
				if (giElementType == <?=qelemQtnTR?>) {
					objPropsForm.setItemValue("table|<?=qelemQtnTab?>_width", "50");
					objPropsForm.setItemValue("table|<?=qelemQtnTab?>_width_units", "% ");
				}
				
				//Agregado el cambio de altura del área de la pregunta para igualar al del área de la respuesta
				var strHeightQuest = objPropsForm.getItemValue("tr|<?=qelemQtnTR?>_height");
				var strHeightQuestUnits = objPropsForm.getItemValue("tr|<?=qelemQtnTR?>_height_units");
				//Si hay un valor directo asignado, se usará el mismo para el área de la pregunta
				if (strHeightQuest == '') {
					//En este caso, el área de la pregunta si debe crecer al tamaño default del área de la respuesta (especialmente en caso como preguntas tipo Text, donde el TextArea
					//tiene un tamaño natural mayor a los inputs normales y se nota la diferencia respecto a la pregunta)
					var objJQMQuestionArea = $("#PreviewHTML [id$='_area'].qtp"+giQTypeID+".dsp"+giQDisplayMode+" tr.ele"+<?=qelemQtnTR?>);
					if (objJQMQuestionArea && objJQMQuestionArea.length) {
						//Esta medida por ser la natural, siempre corresponde a pixeles
						strHeightQuest = objJQMQuestionArea.outerHeight();
						strHeightQuestUnits = "px";
					}
				}
				
				if (strHeightQuest != '') {
					var intElementType = <?=qelemLabTR?>;
					var objStyle = getObjectStyle(intElementType, undefined, true);
					objStyle["tr|<?=qelemLabTR?>_height"] = strHeightQuest + strHeightQuestUnits;
					doSaveStyle(intElementType, objStyle);
					objPropsForm.setItemValue("tr|<?=qelemLabTR?>_height", strHeightQuest);
					objPropsForm.setItemValue("tr|<?=qelemLabTR?>_height_units", strHeightQuestUnits);
				}
			}
			
			//Agregado el recalculo de los widths de las áreas de etiqueta y pregunta para mantenerlas proporcionales (sólo en %)
			if (strName == "table|<?=qelemLabTab?>_width" || strName == "table|<?=qelemQtnTab?>_width") {
				//Sólo aplica si la unidad que está cambiando es porcentaje y si el tipo de despliegue es label left/right
				if (strFieldValue != '' && !isNaN(strFieldValue) && objPropsForm.getItemValue("obj|<?=qelemArea?>_layout") == "2" && strUnitsValues == "% ") {
					var intDiff = parseInt(100 - parseInt(strFieldValue));
					if (giElementType == <?=qelemLabTR?>) {
						//Ajusta la respuesta
						var intElementType = <?=qelemQtnTab?>;
					}
					else {
						//Ajusta la pregunta
						var intElementType = <?=qelemLabTab?>;
					}
					
					var strFieldName = "table|"+intElementType+"_width";
					var objStyle = getObjectStyle(intElementType, undefined, true);
					objStyle[strFieldName] = intDiff + "% ";
					doSaveStyle(intElementType, objStyle);
					objPropsForm.setItemValue(strFieldName, intDiff);
					objPropsForm.setItemValue(strFieldName + "_units", "% ");
				}
			}
		}
		
		/* Coloca los valores en los componentes que definen los estilos visuales dentro
		*/
		function setFormFieldValues() {
			var fieldValue;
			objPropsForm.forEachItem(function(name) {
				var strName = name;
				
				//Si el nombre de campo tiene sufijo "_units", quiere decir que sólo es un campo adicional, así que graba el
				//campo real y sólo agrega las unidades al final
				var blnPropField = true;
				var intPos = strName.indexOf("_units");
				if (intPos != -1) {
					//Este campo no se actualiza en este punto, se hace cuando cambia el campo con el contenido real,
					//de hecho este campo no deberia estar grabado directamente
					blnPropField = false;
					//return;
					//strName = strName.substr(0, intPos);
					//strValue = objPropsForm.getItemValue(strName) + value;
				}
				
				var typeField = objPropsForm.getItemType(strName)
				if (blnPropField) {
					var intElement = undefined;
					//Primero verifica si por excepción el campo se configura para un elemento en específico independientemente de a cual se dió click
					var objStyleProp = splitFieldName(strName);
					if (objStyleProp && objStyleProp['ele']) {
						intElement = parseInt(objStyleProp['ele']);
					}
					if (!intElement || isNaN(intElement)) {
						intElement = parseInt(objPropsForm.getUserData(strName, "ele"));
					}
					
					//Este es el caso normal, si no hubo una excepción a partir del nombre del campo, entonces usará el propio elemento del objeto seleccionado
					if (!intElement || isNaN(intElement)) {
						intElement = giElementType;
					}
					else {
						//En este caso el nombre de campo se refiere al contenedor del objeto, así que se ajusta el tipo de elemento para reflejarlo
						if (intElement == -1) {
							intElement = giCElementType;
						}
					}
					
					//var giSectionTypeLoc = giSectionType;
					var oty = parseInt(objPropsForm.getUserData(strName, "oty"));
					if (oty && oty ==<?=otySection?>) {
						var intObjectType = <?=otySection?>;
						//var giSectionTypeLoc = objPropsForm.getItemValue("sectionType");
						var intQTypeID = -1;
						var intQDisplayMode = -1;
					}
					else {
						var intObjectType = giObjectType;
						//var giSectionTypeLoc = giSectionType;
						var intQTypeID = giQTypeID;
						var intQDisplayMode = giQDisplayMode;
					}
					
					fieldValue = "";
					switch (typeField) {
						case "select":
						case "combo":
						case "input":
						case "colorpicker":
						case "image":
						{
							if (goStyle[intObjectType] != undefined) {
								if (goStyle[intObjectType][giSectionType] != undefined) {
									if (goStyle[intObjectType][giSectionType][intQTypeID] != undefined) {
										if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode] != undefined) {
											if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement] != undefined) {
												if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"] != undefined) {
													//Si se trata de un campo para configurar el contenedor de un objeto, el nombre de campo traerá un elemento dummy que se debe remover
													//cuando se intenta accesar en la colección de estilos
													var strFieldName = strName.replace("|-1", "");
													if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"][strFieldName] != undefined) {
														fieldValue = goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"][strFieldName];
													}													
												}												
											}
										}
									}
								}
							}
							
							if (fieldValue != "") {
								//Actualiza el campo de las unidades si es una configuración que lo requiere, para ello
								//debería existir un campo cuyo nombre tiene un sufijo "_units", si lo hay, de este valor
								//extrae las unidades en caso de haber existido alguna (serían los últimos 2 caracteres
								//siempre y cuando ellos no sean numéricos y el resultado del valor si lo sea)
								var strFieldValue = fieldValue;
								var strUnitsValues = objPropsForm.getItemValue(strName + "_units");
								if (strUnitsValues !== null || strName == "obj_border-width") {
									strUnitsValues = "";
									//Hay unidades, se identifica y asigna su valor (vacío también es válido)
									var intLength = strFieldValue.length - 2;
									if (intLength > 0) {
										strUnitsValues = strFieldValue.substr(-2);
										var strFieldValueTmp = strFieldValue.substr(0, intLength);
										if (!$.isNumeric(strUnitsValues) && $.isNumeric(strFieldValueTmp)) {
											strFieldValue = strFieldValue.substr(0, intLength);
										}
										else {
											//En este caso las unidades son numéricas o el valor no lo es, por tanto esta
											//combinación es inválida
											strUnitsValues = "";
										}
									}
									
									if (strUnitsValues) {
										objPropsForm.setItemValue(strName + "_units", strUnitsValues);
									}
								}
								
								//El valor de la imagen se asigna al crear el campo
								if(typeField!="image")
								{
									objPropsForm.setItemValue(strName, strFieldValue);
								}
								
								if (typeField == "colorpicker") {
									var objColorPicker = objPropsForm.getColorPicker(strName);
									if (objColorPicker && objColorPicker._nodes && objColorPicker._nodes[0] && objColorPicker._nodes[0].node) {
										var strNodeId = objColorPicker._nodes[0].node.id;
										if (strNodeId) {
											$('#'+strNodeId).css('background-color', fieldValue);
										}
									}
								}
							}
							
							if (typeField == "colorpicker") {
								var objColorPicker = objPropsForm.getColorPicker(strName);
								if (objColorPicker && objColorPicker._nodes && objColorPicker._nodes[0] && objColorPicker._nodes[0].node) {
									var strNodeId = objColorPicker._nodes[0].node.id;
									if (strNodeId) {
										$('#'+strNodeId).css('background-color', fieldValue);
									}
									
									//Agrega el evento para crear las opciones de transparencia y limpiar color
									objColorPicker.attachEvent("onShow", function(node) {
										if (!this.bitOptionsAdded) {
											if (this._globalNode) {
												var objButtonsArea = $(this._globalNode).find(".dhxcp_buttons_area")
												if (objButtonsArea && objButtonsArea.length) {
													var blnNoFill = true;
													var blnTransparent = false;
													if (node) {
														blnNoFill = $(node).val() == "";
														blnTransparent = String($(node).val()).toLowerCase() == "transparent";
													}
													
													//JAPR 2016-03-07: Integrada nueva versión de JQ1.11.3/JQM1.45
													//Al agregar el nuevo JQM ahora la página completa se inicializa con ui-mobile, lo cual entre otras cosas cambia el display de
													//todas las label para que sean block, por tanto labels normales deberán tener el estilo de display directo o podrían no verse
													//como se esperaba
													objButtonsArea.before('<label class="dhxcp_label_rgb" style="display:inline;"><input type="checkbox" value="" ' + ((blnNoFill)?'checked':'') + ' onclick="javascript:doSetNoFillColor(\''+name+'\');"><?=translate("No fill")?></label>');
													objButtonsArea.before('<label class="dhxcp_label_rgb" style="display:inline;"><input type="checkbox" value="" ' + ((blnTransparent)?'checked':'') + ' onclick="javascript:doSetTransColor(\''+name+'\');"><?=translate("Transparent")?></label>');
												}
												
												var objMemoryArea = $(this._globalNode).find(".dhxcp_g_memory_area")
												if (objMemoryArea && objMemoryArea.length) {
													objMemoryArea.hide();
												}
												
												this.bitOptionsAdded = 1;
											}
										}
										
										//Independientemente de si se agregaron o no las opciones, si no hay un valor asignado, coloca la Saturación al 100 para que se puedan elegir
										//directamente los colores mas vivos, ya que el default es 0 y se ven casi negros
										var strValue = objPropsForm.getItemValue(strName);
										if (strValue == "" && this._controllerNodes && this._controllerNodes.sat) {
											$(this._controllerNodes.sat).val(100);
											if (this._doOnChangeHSL) {
												this._doOnChangeHSL();
											}
										}
									});
								}
							}
							break;
						}
						
						case "template":
							//Asigna el valor de los componentes que no son nativos de las formas
							if (goStyle[intObjectType] != undefined) {
								if (goStyle[intObjectType][giSectionType] != undefined) {
									if (goStyle[intObjectType][giSectionType][intQTypeID] != undefined) {
										if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode] != undefined) {
											if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement] != undefined) {
												if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"] != undefined) {
													//Si se trata de un campo para configurar el contenedor de un objeto, el nombre de campo traerá un elemento dummy que se debe remover
													//cuando se intenta accesar en la colección de estilos
													var strFieldName = strName.replace("|-1", "");
													if (goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"][strFieldName] != undefined) {
														fieldValue = goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]["style"][strFieldName];
													}													
												}												
											}
										}
									}
								}
							}
							
							//Si es el campo del borde, ese recibe tratamiento especial
							if (strName.substr(-7) == "_border" || strName.substr(0, 11) == "obj_border-") {
								//Se omiten los campos extras de borde, porque el campo principal ya habría asignado el valor para ellos también
								if (strName == "obj_border") {
									doSetBorderValue(strName, fieldValue);
								}
								break;
							}
							
							//En este caso, los templates estan preparados para aceptar índices que empiezan desde 0, así que vacío es en realidad el valor adecuado para un default 0
							if (fieldValue == "") {
								fieldValue = "0";
							}
							
							//if (fieldValue != "") {
							//Obtiene la referencia a la respuesta actualmente seleccionada
							/*var intIndex = parseInt(fieldValue);
							if (isNaN(intIndex)) {
								intIndex = 0;
							}*/
							
							//Dados los nombres de campos que utilizan el caracter |, este selector no puede hacerse directo con #, se usará [id=""]
							//Por la manera en que se construyeron los campos template, primero se tienen que asignar los valores para que realmente se construya su HTML, así que no
							//se debe condicionar la asignación de este valor sino dejar que continue y él creará el HTML si aún no estaba generado
							//var objObject = $('[id="'+strName+'_'+intIndex+'"] img');
							//if (objObject && objObject.length) {
							objPropsForm.setItemValue(strName, fieldValue);
							//}
							break;
						
						default:
						{
							break;
						}
					}
				}
				
				//Agrega el evento de Mouse para mostrar el componente que será modificado, sólo para los campos
				//que permiten cambiar la apariencia
				var intPos = strName.indexOf("_");
				if (intPos != -1) {
					//Si tiene "_" en el nombre de campo, quiere decir que es una configuración que permite cambiar
					//la apariencia del preview, así que es válida para seleccionar al elemento
					var fnDoSelectElement = doSelectElement;
					if (oty == <?=otySection?>) {
						fnDoSelectElement = doSelectElementSection;
					}
					
					switch (typeField) {
						case "select":
							var objDOMElement = objPropsForm.getSelect(strName);
							if (objDOMElement) {
								$(objDOMElement).on("mouseenter", function() {
									fnDoSelectElement(objDOMElement);
								});
								$(objDOMElement).on("mouseleave", function() {
									fnDoSelectElement(objDOMElement, true);
								});
							}
							break;
						case "combo":
							var objDOMElement = objPropsForm.getCombo(strName);
							if (objDOMElement) {
								$(objDOMElement).on("mouseenter", function() {
									fnDoSelectElement(objDOMElement);
								});
								$(objDOMElement).on("mouseleave", function() {
									fnDoSelectElement(objDOMElement, true);
								});
							}
							break;
						case "input":
						case "colorpicker":
							var objDOMElement = objPropsForm.getInput(strName);
							if (objDOMElement) {
								$(objDOMElement).on("mouseenter", function() {
									fnDoSelectElement(objDOMElement);
								});
								$(objDOMElement).on("mouseleave", function() {
									fnDoSelectElement(objDOMElement, true);
								});
							}
							break;
						default:
							break;
					}
				}
			});
		}
		
		/* Aplica estilos especiales de colores sobre el componente y cierra el diálogo de HTMLX
		*/
		function doSetNoFillColor(name) {
			console.log('doSetNoFillColor ' + name);
			
			if (!objPropsForm) {
				return;
			}
			
			var objColorPicker = objPropsForm.getColorPicker(name);
			if (objColorPicker && objColorPicker._nodes && objColorPicker._nodes[0] && objColorPicker._nodes[0].node) {
				var strNodeId = objColorPicker._nodes[0].node.id;
				$('#'+strNodeId).css('background-color', "#ffffff");
				//$('#'+strNodeId).val("");
				objPropsForm.setItemValue(name, "")
				objColorPicker.hide();
				
				//Invoca directamente al grabado de la propiedad, ya que si pasa por doChangeProperty se volverá a pintar el input del colorPicker
				changeStylePreview(name, "");
				/*setTimeout(function() {
					$('#'+strNodeId).val("");
				}, 100);*/
			}
		}
		
		function doSetTransColor(name) {
			console.log('doSetTransColor ' + name);
			
			if (!objPropsForm) {
				return;
			}
			
			var objColorPicker = objPropsForm.getColorPicker(name);
			if (objColorPicker && objColorPicker._nodes && objColorPicker._nodes[0] && objColorPicker._nodes[0].node) {
				var strNodeId = objColorPicker._nodes[0].node.id;
				$('#'+strNodeId).css('background-color', "#ffffff");
				//$('#'+strNodeId).val("transparent");
				objPropsForm.setItemValue(name, "transparent")
				objColorPicker.hide();
				
				//Invoca directamente al grabado de la propiedad, ya que si pasa por doChangeProperty se volverá a pintar el input del colorPicker
				changeStylePreview(name, "transparent");
				/*setTimeout(function() {
					$('#'+strNodeId).val("transparent");
				}, 100);*/
			}
		}
		
		/* Habilita/deshabilita el resalte de los componentes que son seleccionados en los campos de propiedades
		*/
		function doSelectElementSection(oElement, bRemove) {
			if (!oElement || !objLastElement) {
				return;
			}
			
			var objStyleProp = splitFieldName(oElement.name);
			if (!objStyleProp || $.isEmptyObject(objStyleProp) || !objStyleProp['prop']) {
				return;
			}
			
			var intElement = undefined;
			if (objStyleProp['ele']) {
				intElement = parseInt(objStyleProp['ele']);
			}
			if (!intElement || isNaN(intElement)) {
				intElement = parseInt(objPropsForm.getUserData(oElement.name, "ele"));
			}
			var intObjectType = <?=otySection?>;
			var intMainElement = 0;
			switch (intElement) {
				case 1:
				case 2:
					intMainElement = 1;
					break;
				case 3:
					intMainElement = 3;
					break;
				case 4:
				default:
					intMainElement = 4;
					break;
			}
			
			var strStyleProp = objStyleProp['prop'];
			var objJQLastElement = $(".oty"+intObjectType+".stp"+giSectionType+".ele"+intMainElement);
			//JAPR 2016-03-01: Dado a que ahora el preview se pinta con las clases reales, ya no se puede usar una clase para el resalte, así que se optará por un style directo
			//JQuery no se puede usar, ya que no asigna la prioridad !important, así que se asignará el style directo con dicha prioridad temporalmente
			if (objJQLastElement && objJQLastElement.length) {
				if (bRemove) {
					//objJQLastElement.removeClass("itemFocus");
					objJQLastElement[0].style.setProperty("border", "");
					objJQLastElement[0].style.setProperty("background", "");
				}
				else {
					//objJQLastElement.addClass("itemFocus");
					objJQLastElement[0].style.setProperty("border", "2px dotted blue", "important");
					objJQLastElement[0].style.setProperty("background", "#a1ceed", "important");
				}
			}
		}
		
		/* Habilita/deshabilita el resalte de los componentes que son seleccionados en los campos de propiedades
		El parámetro oDOMElement sirve para indicar directamente el elemento al que se aplicará el estilo de resalte, sin tener que deducirlo en base a las clases del campo que se
		está configurando (oElement). Es especialmente útil cuando el resalte no se hace desde un campo de configuración sino algún evento o suceso externo
		*/
		function doSelectElement(oElement, bRemove, oDOMElement) {
			if ((!oElement || !objLastElement) && !oDOMElement) {
				return;
			}
			//debugger;
			if (oDOMElement) {
				objJQLastElement = $(oDOMElement);
			}
			else {
				//En este caso se deducirá el objeto dados los parámetros de la porpiedad configurada (oElement) y el último objeto del DOM al que se dió click (objLastElement)
				var objJQLastElement = $(objLastElement);
				var objObjectDef = identifyElement(objLastElement);
				//Si no es un objeto válido, no debe continuar
				if (!objObjectDef || $.isEmptyObject(objObjectDef)) {
					return;
				}
				
				var strName = oElement.name.replace("_units", "");
				var objStyleProp = splitFieldName(strName);
				if (!objStyleProp || $.isEmptyObject(objStyleProp) || !objStyleProp['prop']) {
					return;
				}
				
				var intElement = undefined;
				var intObjElement = parseInt(objJQLastElement.attr('ele'));
				var intContElement = objObjectDef['cele'];
				var strStyleProp = objStyleProp['prop'];
				if (objStyleProp['ele']) {
					var intElement = parseInt(objStyleProp['ele']);
					if (intObjElement != intElement) {
						if (intElement == -1) {
							var objNewElement = objJQLastElement.closest("[cont='1']");
						}
						else {
							var objNewElement = objJQLastElement.find("[ele=" + intElement + "]");
							if (!objNewElement || !objNewElement.length) {
								//var objNewElement = objJQLastElement.closest("[ele=" + intElement + "]");
								//if (!objNewElement || !objNewElement.length) {
									var objParent = objJQLastElement.closest("[id*='_area']");
									if (objParent && objParent.length) {
										var objNewElement = objParent.find("[ele=" + intElement + "]");
									}
								//}
							}
						}
						
						if (objNewElement && objNewElement.length) {
							objJQLastElement = objNewElement;
						}
					}
				}
			}
			
			//JAPR 2016-03-01: Dado a que ahora el preview se pinta con las clases reales, ya no se puede usar una clase para el resalte, así que se optará por un style directo
			//JQuery no se puede usar, ya que no asigna la prioridad !important, así que se asignará el style directo con dicha prioridad temporalmente
			if (objJQLastElement && objJQLastElement.length) {
				if (bRemove) {
					//objJQLastElement.removeClass("itemFocus");
					objJQLastElement[0].style.setProperty("border", "");
					objJQLastElement[0].style.setProperty("background", "");
					
					//RV Junio2016: Cuando se selecciona el THEAD se tiene que poner el color a nivel del TH para que se aplique,
					//ya que los TH tienen clase definida a ese nivel
					if(objJQLastElement[0].nodeName=="THEAD")
					{
						//Se navega desde el THEAD hacía su hijo 1 que es la TR y luego hacía su hijo 1 es el primer TH
						objJQLastElement[0].childNodes[1].childNodes[1].style.setProperty("background", "");
						//Se navega desde el THEAD hacía su hijo 1 que es la TR y luego hacía su hijo 3 es el segundo TH
						objJQLastElement[0].childNodes[1].childNodes[3].style.setProperty("background", "");
					}
				}
				else {
					//objJQLastElement.addClass("itemFocus");
					objJQLastElement[0].style.setProperty("border", "2px dotted blue", "important");
					objJQLastElement[0].style.setProperty("background", "#a1ceed", "important");
					
					//RV Junio2016: Cuando se selecciona el THEAD se tiene que poner el color a nivel del TH para que se aplique,
					//ya que los TH tienen clase definida a ese nivel
					if(objJQLastElement[0].nodeName=="THEAD")
					{
						//Se navega desde el THEAD hacía su hijo 1 que es la TR y luego hacía su hijo 1 es el primer TH
						objJQLastElement[0].childNodes[1].childNodes[1].style.setProperty("background", "#a1ceed", "important");
						//Se navega desde el THEAD hacía su hijo 1 que es la TR y luego hacía su hijo 3 es el segundo TH
						objJQLastElement[0].childNodes[1].childNodes[3].style.setProperty("background", "#a1ceed", "important");
					}
				}
			}
		}
		
		/* Dado un nombre de campo para configurar, separa la parte del definidor de Component, Element y Style para permitir usarlo de alguna otra manera
		*/
		function splitFieldName(sFieldName) {
			if (!sFieldName) {
				return {};
			}
			
			var objStyle = {};
			sFieldName = String(sFieldName);
			var intIndex = sFieldName.indexOf("_");
			if (intIndex >= 0) {
				var strElement = "";
				var strComponent = sFieldName.substr(0, intIndex);
				var strStyle = sFieldName.substr(intIndex +1);
				if (strComponent) {
					var intIndex = strComponent.indexOf("|");
					if (intIndex >= 0) {
						strElement = parseInt(strComponent.substr(intIndex +1));
						strComponent = sFieldName.substr(0, intIndex);
					}
				}
				else {
					strComponent = "";
				}
				
				if (strComponent) {
					objStyle['comp'] = strComponent;
				}
				if (!isNaN(strElement)) {
					objStyle['ele'] = strElement;
				}
				if (strStyle) {
					objStyle['prop'] = strStyle;
				}
			}
			
			return objStyle;
		}
		
		/* Dado un objeto del DOM (como el de un evento), obtiene los atributos tanto de él como de su contenedor para identificar el tipo de objeto al que hace referencia según
		la estructura de eForms de Secciones y Preguntas
		Si se va a utilizar esta función refiriendose al último elemento al que se dió click (es decir, que pasó por la función setStyle), entonces es preferible simplemente usar las
		variables globales que ya lo identifican en lugar de volver a usar esta función
		*/
		function identifyElement(oElement) {
			var objObjectDef = {};
			
			if (!oElement) {
				return objObjectDef;
			}
			
			//Obtiene el primer div Contenedor de la pregunta, si no hubiera alguno, significaría que no es una pregunta válida de eForms
			var objElement = $(oElement);
			if (!objElement || !objElement.length) {
				return objObjectDef;
			}
			
			var objParent = objElement.closest("[id*='_area']");
			if (!objParent || !objParent.length) {
				//En este caso no se trata de un componente de una pregunta, sin embargo es válido hacer click en algún componente siempre y cuando directamente tenga un atributo ele
				var intElement = parseInt(objElement.attr('ele'));
				if (!isNaN(intElement)) {
					objObjectDef['ele'] = parseInt(objElement.attr('ele'));
					//Otros atributos pudieran venir, si lo hacen entonces se asigna, de lo contrario el selector del preview debió asignarlos
					var intData = parseInt(objElement.attr('oty'));
					if (!isNaN(intData)) {
						objObjectDef['otype'] = intData;
					}
					var intData = parseInt(objElement.attr('qtp'));
					if (!isNaN(intData)) {
						objObjectDef['qtype'] = intData;
					}
					var intData = parseInt(objElement.attr('dsp'));
					if (!isNaN(intData)) {
						objObjectDef['disp'] = intData;
					}
					objObjectDef['cele'] = 0;
				}
				return objObjectDef;
			}
			
			objObjectDef['otype'] = parseInt(objParent.attr('oty'));
			objObjectDef['qtype'] = parseInt(objParent.attr('qtp'));
			objObjectDef['disp'] = parseInt(objParent.attr('dsp'));
			objObjectDef['ele'] = parseInt(objElement.attr('ele'));
			objObjectDef['cele'] = 0;
			//Identifica el tipo de contenedor del objeto, si ya es uno entonces no se agrega esta propiedad
			if (objObjectDef['ele'] != <?=qelemLabCont?> || objObjectDef['ele'] != <?=qelemQtnCont?>) {
				//Obtiene la referencia al primer contenedor de este objeto, ya que se determinó que él mismo no es un contenedor
				var objContainerElement = objElement.closest("[cont='1']");
				if (objContainerElement && objContainerElement.length) {
					//Según el esquema de HTML, debería haber un contenedor, pero si no entrara a este punto entonces no es un objeto válido
					objObjectDef['cele'] = parseInt($(objContainerElement).attr('ele'));
				}
			}
			
			return objObjectDef;
		}
		
		//JAPREliminar: Esta función se descontinuó, ahora no habrá diferencia en asignar un elemento u otro, se va a cambiar el identificador según el objeto al que se le dió click
		function changeStylePreviewSection(sFieldName, vValue) {
			console.log('changeStylePreviewSection ' + sFieldName + ' == ' + vValue);
			
			if (!goStyle) {
				return;
			}
			
			var objStyleProp = splitFieldName(sFieldName);
			if (!objStyleProp || $.isEmptyObject(objStyleProp) || !objStyleProp['prop']) {
				return;
			}
			
			var intElement = undefined;
			if (objStyleProp['ele']) {
				intElement = parseInt(objStyleProp['ele']);
			}
			if (!intElement || isNaN(intElement)) {
				intElement = parseInt(objPropsForm.getUserData(oElement.name, "ele"));
			}
			var intObjectType = <?=otySection?>;
			var intMainElement = 0;
			switch (intElement) {
				case 1:
				case 2:
					intMainElement = 1;
					break;
				case 3:
					intMainElement = 3;
					break;
				case 4:
				default:
					intMainElement = 4;
					break;
			}
			
			var strStyleProp = objStyleProp['prop'];
			var objJQLastElement = $(".oty"+intObjectType+".stp"+giSectionType+".ele"+intMainElement);
			if (objJQLastElement && objJQLastElement.length) {
				//JAPR 2016-02-29: Removida la asignación directa de Styles, ahora se heredarán en las clases descargadas durante la actualización
				//objJQLastElement.css(strStyleProp, vValue);
			}
			
			var objStyle = {};
			
			//Actualiza la combinación de estilo específica en memoria del objeto que fue modificado
			var intQTypeID = -1;
			var intQDisplayMode = -1;
			if (!goStyle[intObjectType]) {
				goStyle[intObjectType] = {};
			}
			if (!goStyle[intObjectType][giSectionType]) {
				goStyle[intObjectType][giSectionType] = {};
			}
			if (!goStyle[intObjectType][giSectionType][intQTypeID]) {
				goStyle[intObjectType][giSectionType][intQTypeID] = {};
			}
			if (!goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode]) {
				goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode] = {};
			}
			if (!goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]) {
				goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement] = {dirty:0, style:{}};
			}
			goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]['dirty'] = 1;
			goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]['style'][sFieldName] = vValue;
			var objStyle = goStyle[intObjectType][giSectionType][intQTypeID][intQDisplayMode][intElement]['style'];
			
			//Inicia el proceso de grabado en la metadata si existen elementos suficientes y se cambió algún estilo
			if (intElement && objStyle && !$.isEmptyObject(objStyle)) {
				var objData = {};
				objData.giObjectType = intObjectType;
				objData.giSectionType = giSectionType;
				objData.giQTypeID = intQTypeID;
				objData.giQDisplayMode = intQDisplayMode;
				
				doSaveStyle(intElement, objStyle, objData);
			}
		}
		
		/* Realiza el cambio de estilo en el componente selecionado según la propiedad configurada que se recibe como parámetro
		*/
		function changeStylePreview(sFieldName, vValue) {
			console.log('changeStylePreview ' + sFieldName + ' == ' + vValue);
			
			if (!objLastElement || !goStyle) {
				return;
			}
			
			var strFieldName = sFieldName;
			var objStyleProp = splitFieldName(sFieldName);
			if (!objStyleProp || $.isEmptyObject(objStyleProp) || !objStyleProp['prop']) {
				return;
			}
			
			var objJQLastElement = $(objLastElement);
			/*var objObjectDef = identifyElement(objLastElement);
			//Si no es un objeto válido, no debe continuar
			if (!objObjectDef || $.isEmptyObject(objObjectDef)) {
				return;
			}*/
			
			var intElement = undefined;
			var intObjElement = parseInt(objJQLastElement.attr('ele'));
			//var intContElement = objObjectDef['cele'];
			var intContElement = giCElementType;
			var strStyleProp = objStyleProp['prop'];
			if (objStyleProp['ele']) {
				var intElement = parseInt(objStyleProp['ele']);
				if (intObjElement != intElement) {
					if (intElement == -1) {
						var objNewElement = objJQLastElement.closest("[cont='1']");
					}
					else {
						var objNewElement = objJQLastElement.find("[ele=" + intElement + "]");
						if (!objNewElement || !objNewElement.length) {
							//var objNewElement = objJQLastElement.closest("[ele=" + intElement + "]");
							//if (!objNewElement || !objNewElement.length) {
								var objParent = objJQLastElement.closest("[id*='_area']");
								if (objParent && objParent.length) {
									var objNewElement = objParent.find("[ele=" + intElement + "]");
								}
							//}
						}
					}
					
					if (objNewElement && objNewElement.length) {
						objJQLastElement = objNewElement;
					}
				}
			}
			
			//JAPR 2016-02-29: Removida la asignación directa de Styles, ahora se heredarán en las clases descargadas durante la actualización
			//objJQLastElement.css(strStyleProp, vValue);
			//@JAPRWarning: Cuando se limpie una propiedad para regresar al default, al limpiar el css local aún quedaría el css heredado de las clases, así que se debe reconstruir
			//ese css directamente, lo mejor es invocar a un proceso de Ajax que descargue de nuevo el CSSStyle y vuelva a reconstruir el preview
			
			//Prepara las variables para el request de grabado, la mayoría fueron asignadas desde el momento que se hizo click al elemento a configurar, sólo estas específicamente
			//se deben asignar hasta este punto
			var intElementType = intObjElement;
			//Determina el elemento sobre el que se hizo click explícitamente, si se trata de un contenedor directo entonces usa ese element directamente, si se 
			//trata de una referencia al contenedor padre, se basa en el element actual para determinar su contenedor, en cualquier otro caso se asume que es el
			//element propio del objeto sobre el que se hizo click
			if (intElement && intObjElement != intElement) {
				if (intElement == -1) {
					//Se está cambiando un elemento padre del seleccionado
					intElementType = intContElement;
					//El campo se define con un identificador de elemento dummy para saber que se refiere al área contenedora, pero para grabar se debe remover así que se hace en este punto
					strFieldName = sFieldName.replace("|-1", "");
				}
				else {
					//Se está cambiando un elemento diferente al seleccionado pero no necesariamente al contenedor
					intElementType = intElement;
				}
			}
			var objStyle = {};
			
			//Actualiza la combinación de estilo específica en memoria del objeto que fue modificado
			switch (giObjectType) {
				case <?=otySection?>:
				case <?=otyQuestion?>:
					//Las preguntas actualizan el Array de estilo basado en el tipo de sección, tipo de pregunta y despliegue de la misma, así como componente específico en el que
					//se encuentra configurando, aunque existen algunos campos que son globales por lo que esos se actualizan directo si se recibieron
					var objStyle = getObjectStyle(intElementType, undefined, true);
					objStyle[strFieldName] = vValue;
					
					//Si el valor ya no se asignó, significa que se usará el default original, así que se remueve este elemento del array para que se borre de la metadata
					//JAPR 2016-03-08: Agregada la configuración de los campos para indicar los valores que se consideran "vacios" (usado originalmente para templates de campos personalizados)
					var strEmptyValue = objPropsForm.getUserData(sFieldName, "defaultValue");
					if (vValue === undefined || vValue === "" || (strEmptyValue !== "" && vValue == strEmptyValue)) {
						delete objStyle[strFieldName];
					}
					
					break;
				default:
					break;
			}
			
			//Inicia el proceso de grabado en la metadata si existen elementos suficientes y se cambió algún estilo
			//Si el valor ya no se asignó, significa que se usará el default original, así que se remueve este elemento del array para que se borre de la metadata
			if (intElementType && objStyle) {
				doSaveStyle(intElementType, objStyle);
			}
		}
		
		function doSaveTemplateName(value)
		{
			if (!value) {
				return;
			}
			
			var objParams = {};
			objParams.Design = 1;
			objParams.RequestType = reqAjax;
			objParams.Process = "Edit";
			objParams.ObjectType = <?=otyAppCustStyles?>;
			
			//Agrega los parámetros para el grabado específico de un elemento, si se quisieran grabar todos, entonces deberia enviarse un array de elementos en lugar de uno solo
			objParams.TemplateID = giTemplateID;
			objParams.TemplateName = value;	//objPropsForm.getItemValue("templateName");
			
			var strParams = '';
			var strAnd = '';
			for (var strProp in objParams) {
				strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
				strAnd = '&';
			}
			
			$('#divSaving').show();
			if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
				objPropsLayout.cells(dhxPropsDetailCell).progressOn();
			}
			window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
				$('#divSaving').hide();
				if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
					objPropsLayout.cells(dhxPropsDetailCell).progressOff();
				}
			});
		}
		
		/* Realiza el proceso de grabado enviando al server un paquete con todos los elementos que fueron modificados basado en el array de datos de estilos cargados goStyle
		Los parámetros iElementType y oStyle se utilizan para definir exactamente que elemento ha cambiado de valor, de tal forma que sólo se grabe esa porción del template completo
		de estilos, si no se recibieran se debería asumir que debe recorrer todos los estilos aplicables y grabar todos
		//JAPR 2016-03-08: Agregado el parámetro bClearAll, el cual sólo se usa si se desea resetear los estilos del template, ya que eliminará todo lo que está en la metadata. Se dejó
		en este método porque el regreso del mismo actualiza las clases en memoria. Si se recibe este parámetro, es irrelevante el estilo enviado (debería ser vacío preferentemente) pues
		todo lo que importa es el parámetro bClearAll
		*/
		function doSaveStyle(iElementType, oStyle, objData, bClearAll) {
			console.log('doSaveStyle iElementType:' + iElementType);
			
			//Si el valor ya no se asignó, significa que se usará el default original, así que se remueve este elemento del array para que se borre de la metadata
			if (!iElementType || !oStyle /*|| $.isEmptyObject(oStyle)*/) {
				return;
			}
			
			var objParams = {};
			objParams.Design = 1;
			objParams.RequestType = reqAjax;
			objParams.Process = "Edit";
			objParams.ObjectType = <?=otyAppCustStyles?>;
			if (bClearAll) {
				objParams.Clear = 1;
			}
			
			//Agrega los parámetros para el grabado específico de un elemento, si se quisieran grabar todos, entonces deberia enviarse un array de elementos en lugar de uno solo
			objParams.TemplateID = giTemplateID;
			var objInput = $(objPreviewToolbar.getInput("TemplateName"));
			if (objInput && objInput.length) {
				objInput.on('blur', function() {
					objParams.TemplateName = objInput.val();
				});
			}
			//objParams.TemplateName = objPropsForm.getItemValue("templateName");
			
			if (objData) {
				objParams.SectionType = objData.giSectionType;
				objParams.ItemType = objData.giObjectType;
				objParams.QTypeID = objData.giQTypeID
				objParams.QDisplayMode = objData.giQDisplayMode;
			}
			else
			{
				objParams.SectionType = giSectionType;
				objParams.ItemType = giObjectType;
				objParams.QTypeID = giQTypeID;
				objParams.QDisplayMode = giQDisplayMode;
			}
			objParams.ElementType = iElementType;
			objParams.Style = JSON.stringify(oStyle);
			
			//Los datos se mandan como una serie de parámetros con los elementos que se están modificando, por el momento se asume que es una única combinación, sin embargo
			//se podría enviar una serie de arrays con todos los estilos modificados si se implementa un grabado masivo de la ventana de configuración
			var strParams = '';
			var strAnd = '';
			for (var strProp in objParams) {
				strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
				strAnd = '&';
			}
			
			$('#divSaving').show();
			if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
				objPropsLayout.cells(dhxPropsDetailCell).progressOn();
			}
			window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
				$('#divSaving').hide();
				if (objPropsLayout && objPropsLayout.cells(dhxPropsDetailCell)) {
					objPropsLayout.cells(dhxPropsDetailCell).progressOff();
				}
				
				doUpdateCSSContainer(loader);
				
				//En caso de haber limpiado estilos, vuelve a seleccionar el último objeto al que se dió click para refrescar la ventana de configuraciones
				if (bClearAll) {
					goStyle = {};
					setStyle(undefined, objLastElement);
				}
				
				//Vuelve a seleccionar el objeto que se está configurando para que refleje los cambios de estilos en cuanto a tamaño
				createSelector();
			});
		}
		
		/* Cambia la vista del preview a la indicada en el parámetro sPrevId */
		function generatePreview(sPrevId)
		{	
			console.log('generatePreview');
			
			if (!sPrevId) {
				sPrevId = "PreviewOpen";
			}
			
			//Se envía false porque no se quiere eliminar el objeto en realidad, solo su contenido
			objPropsLayout.cells(dhxPreviewCell).detachObject(false);
			//JAPRWarning: Por el momento está fijo el HTML de las preguntas de sección estándar, pero aquí dependerá de la combinación de combos de la cejilla "Planilla"
			$('#PreviewHTML').html($('#'+sPrevId).html());
			objPropsLayout.cells(dhxPreviewCell).attachObject("PreviewHTML");
			
			//Selecciona el primer objeto a configurar según la ventana solicitada
			objLastElement = undefined;
			switch (sPrevId) {
				case "PreviewLogin":
					objLastElement = $("#PreviewHTML #li_settings");
					break;
				case "PreviewSection":
					objLastElement = $("#PreviewHTML #Section_2_header");
					break;
				case "PreviewOpen":
					objLastElement = $("#PreviewHTML #qfield1_lab");
					break;
				case "PreviewSimple":
					objLastElement = $("#PreviewHTML #qfield1_lab");
					break;
				case "PreviewMultiple":
					objLastElement = $("#PreviewHTML #qfield1_lab");
					break;
				case "PreviewMedia":
					objLastElement = $("#PreviewHTML #qfield1_lab");
					break;
				case "PreviewOther":
					objLastElement = $("#PreviewHTML #qfield1_lab");
					break;
			}
			if (objLastElement && objLastElement.length) {
				setStyle(undefined, objLastElement[0]);
			}
			
			doOnResizeScreen();
		}
		
		/* Coloca el nombre del objeto seleccionado en un área visible para que sea fácilmente identificado
		*/
		function showElementName() {
			console.log('showElementName');
			
			if (!objLastElement || !objPropsLayout || !objPropsLayout.cells(dhxPreviewCell)) {
				return;
			}
			
			var strName = "";
			switch (giObjectType) {
				case <?=otySection?>:
					strName += " <?=translate("Section")?> - ";
					
					var strElementType = "";
					switch (giElementType) {
						case <?=selemHdrBar?>:
							strElementType = "<?=translate("Header bar")?>";
							break;
						case <?=selemHdrLab?>:
							strElementType = "<?=translate("Header bar label")?>";
							break;
						case <?=selemPage?>:
						case <?=selemBdy?>:
							strElementType = "<?=translate("Body")?>";
							break;
						case <?=selemFtrBar?>:
							strElementType = "<?=translate("Footer bar")?>";
							break;
						case <?=selemLeftBtn?>:
							strElementType = "<?=translate("Left Button")?>";
							break;
						case <?=selemRigthBtn?>:
							strElementType = "<?=translate("Rigth Button")?>";
							break;
						case <?=selemHdrTbl?>:
							strElementType = "<?=translate("Section Table - Header Table")?>";
							break;
						case <?=selemTotalTbl?>:
							strElementType = "<?=translate("Section Table - Row Total")?>";
							break;
						case <?=selemLabelFirstCol?>:
							strElementType = "<?=translate("Section Table - Element Name")?>";
							break;
						case <?=selemRowOdd?>:
							strElementType = "<?=translate("Section Table - Odd Row")?>";
							break;
						case <?=selemRowEven?>:
							strElementType = "<?=translate("Section Table - Even Row")?>";
							break;
						case <?=selemSummary?>:
							strElementType = "<?=translate("Section Repeat multiple times - Summary")?>";
							break;
						case <?=selemSummaryFirstAns?>:
							strElementType = "<?=translate("Section Repeat multiple times - First Answer")?>";
							break;
						case <?=selemSummaryAnswers?>:
							strElementType = "<?=translate("Section Repeat multiple times - Answers")?>";
							break;
						case <?=selemAddBtn?>:
							strElementType = "<?=translate("Section Repeat multiple times - Button")?>";
							break;
					}
					
					strName += " > " + strElementType;
					break;
					
				case <?=otyQuestion?>:
					strName += " <?=translate("Question")?> - ";
					var strSectionType = "";
					var strQTypeID = "";
					switch (giQTypeID) {
						//-------------------------------------------------
						//Preguntas Abiertas
						//-------------------------------------------------
						case <?=qtpOpenNumeric?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Number")?>";
							break;
						case <?=qtpOpenDate?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Date")?>";
							break;
						case <?=qtpOpenString?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Text")?>";
							break;
						case <?=qtpOpenAlpha?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Alphanumeric")?>";
							break;
						case <?=qtpOpenTime?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Time")?>";
							break;
						case <?=qtpPassword?>:
							strSectionType = "<?=translate("Open")?>";
							strQTypeID = "<?=translate("Password")?>";
							break;
						//-------------------------------------------------
						//Preguntas Selección sencilla
						//-------------------------------------------------
						case <?=qtpSingle?>:
						{
							strSectionType = "<?=translate("Simple choice")?>";
							switch (giQDisplayMode) {
								//'dspVertical', 0
								case <?=dspVertical?>:
								{
									strQTypeID = "<?=translate("Vertical")?>";
									break;
								}
								//'dspHorizontal', 1
								case <?=dspHorizontal?>:
								{
									strQTypeID = "<?=translate("Horizontal")?>";
									break;
								}
								//'dspMenu', 2
								case <?=dspMenu?>:
								{
									strQTypeID = "<?=translate("Menu")?>";
									break;
								}
								//'dspAutocomplete', 5
								case <?=dspAutocomplete?>:
								{
									strQTypeID = "<?=translate("Autocomplete")?>";
									break;
								}
								//'dspMap', 8
								case <?=dspMap?>:
								{
									strQTypeID = "<?=translate("Map")?>";
									break;
								}
							}
							break;
						}
						//-------------------------------------------------
						//Preguntas Selección Múltiple
						//-------------------------------------------------
						case <?=qtpMulti?>:
						{
							strSectionType = "<?=translate("Multiple choice")?>";
							switch (giQDisplayMode) {
								//'dspVertical', 0
								case <?=dspVertical?>:
								{
									strQTypeID = "<?=translate("Vertical")?>";
									break;
								}
								//'dspHorizontal', 1
								case <?=dspHorizontal?>:
								{
									strQTypeID = "<?=translate("Horizontal")?>";
									break;
								}
								//'dspMenu', 2
								case <?=dspMenu?>:
								{
									strQTypeID = "<?=translate("Menu")?>";
									break;
								}
								//'dspAutocomplete', 5
								case <?=dspAutocomplete?>:
								{
									strQTypeID = "<?=translate("Autocomplete")?>";
									break;
								}
								//'dspLabelnum', 6
								case <?=dspLabelnum?>:
								{
									strQTypeID = "<?=translate("Ranking")?>";
									break;
								}
							}
							break;
						}
						//-------------------------------------------------
						//Preguntas Media
						//-------------------------------------------------
						case <?=qtpPhoto?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("Photo")?>";
							break;
						case <?=qtpAudio?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("Audio")?>";
							break;
						case <?=qtpVideo?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("Video")?>";
							break;
						case <?=qtpSketch?>:
						<?//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)?>
						case <?=qtpSketchPlus?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("Sketch")?>";
							break;
						case <?=qtpDocument?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("Document")?>";
							break;
						case <?=qtpOCR?>:
							strSectionType = "<?=translate("Media")?>";
							strQTypeID = "<?=translate("OCR")?>";
							break;
						//-------------------------------------------------
						//Preguntas Otras
						//-------------------------------------------------
						case <?=qtpSkipSection?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Skip to section")?>";
							break;
						case <?=qtpCalc?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Calculated")?>";
							break;
						case <?=qtpSignature?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Signature")?>";
							break;
						case <?=qtpMessage?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Message")?>";
							break;
						case <?=qtpCallList?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Call List")?>";
							break;
						case <?=qtpBarCode?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Barcode")?>";
							break;
						case <?=qtpSync?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Synchronize")?>";
							break;
						case <?=qtpSection?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Section")." - ".strtolower(translate("Table"))?>";
							break;
						case <?=qtpExit?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Exit")?>";
							break;
						case <?=qtpUpdateDest?>:
							strSectionType = "<?=translate("Other")?>";
							strQTypeID = "<?=translate("Update data")?>";
							break;
					}
					
					//console.log("***** giQTypeID:"+giQTypeID);
					//console.log("***** giQDisplayMode:"+giQDisplayMode);
					//console.log("***** strSectionType:"+strSectionType);
					//console.log("***** strQTypeID:"+strQTypeID);
					
					var strElementType = "";
					switch (giElementType) {
						case <?=qelemLabTR?>:
							strElementType = "<?=translate("Question area")?>";
							break;
						case <?=qelemLabElem?>:
							strElementType = "<?=translate("Question")?>";
							break;
						case <?=qelemQtnTR?>:
							strElementType = "<?=translate("Answer area")?>";
							break;
						case <?=qelemQtnElem?>:
							strElementType = "<?=translate("Answer")?>";
							break;
					}
					
					strName += strSectionType + " > " + strQTypeID + " > " + strElementType;
					break;
			}
			
			objPropsLayout.cells(dhxPreviewCell).setText("<?=translate("Preview")?>: " + strName);
		}
		
		/* Crea un div que funciona como selector del último elemento al que se hizo click, sólo puede haber un selector y si id siempre será "domSelector"
		Se creará el div selector en el padre y envolverá al componente que se le hizo el click, con una posición absoluta para no afectar otros elementos y para que haga scroll con la
		página, si el elemento al que se hizo click fuera parte de alguna pregunta y por tanto pudiera tener un contenedor, entonces el selector se creará en el padre de dicho contenedor,
		ya que en esos casos al elemento al que se hizo click debe configurarse a si mismo + su área contenedora, por tanto ambos estarán seleccionados
		Este selector no tiene funcionalidad, es exclusivamente para identificar a que elemento se le hizo click por última vez
		Dentro de un preview de una forma, los componentes principales de la pregunta tienen generalmente un contenedor, en ese caso el selector envolverá a dicho contenedor
		*/
		function createSelector() {
			//Primero debe eliminar el domSelector previamente creado
			$('#domSelector').remove();
			$('#dmsltl').remove();
			$('#dmsltc').remove();
			$('#dmsltr').remove();
			$('#dmslml').remove();
			$('#dmslmr').remove();
			$('#dmslbl').remove();
			$('#dmslbc').remove();
			$('#dmslbr').remove();
			
			if (!objLastElement) {
				return;
			}
			
			console.log('createSelector ' + objLastElement.id);
			
			var objJQLastElement = $(objLastElement);
			var objJQLastElementOrig = objJQLastElement;
			//Verifica si el elemento tiene un padre contenedor, eso sólo sería posible si se tratara de un elemento de pregunta, en caso de no tenerlo entonces utilizará a su padre
			//real inmediado
			var blnIsTableCont = false;
			var objParent = objJQLastElement.closest(".tableCont");
			if (objParent && objParent.length) {
				//Es un elemento que pertenece a una pregunta y tiene un contenedor, envolvemos al contenedor
				objJQLastElement = objParent;
				//objParent = objParent.parent();
				objParent = objParent.offsetParent();
				blnIsTableCont = true;
			}
			else {
				//Es un elemento que no pertenece a una pregunta, sólo lo envolvemos a él
				//objParent = objJQLastElement.parent();
				objParent = objJQLastElement.offsetParent();
				if (!objParent || !objParent.length) {
					return;
				}
			}
			
			//Obtiene el tamaño y posición del objeto a seleccionar para crear su selector
			var objPosition = objJQLastElement.position();
			if (!objPosition) {
				return;
			}
			
			var intTop = parseInt(objPosition.top);
			var intLeft = parseInt(objPosition.left);
			var intWidth = parseInt(objJQLastElement.innerWidth());
			var intHeight = parseInt(objJQLastElement.innerHeight());
			if (isNaN(intTop) || isNaN(intLeft) || isNaN(intWidth) || isNaN(intHeight)) {
				return;
			}
			
			//Ajusta la posición left dependiendo de si tiene o no un margen aplicado
			if (objJQLastElement.css('margin-left')) {
				var intLeftAdj = parseInt(objJQLastElement.css('margin-left'));
				if (!isNaN(intLeftAdj)) {
					intLeft += intLeftAdj;
				}
			}
			
			//Crea el domSelector nuevo colocandolo para envolver al objeto al que se seleccionó
			var strPositionDim = 'left:'+(intLeft-2)+';top:'+(intTop-2)+';width:'+(intWidth+5)+';height:'+(intHeight+2)+';';
			objParent.append('<div id="domSelector" style="position:absolute;background-color:transparent;border:1px dashed black;pointer-events:none;'+strPositionDim+'"></div>');
			var objDomSelector = $('#domSelector');
			if (!objDomSelector || !objDomSelector.length) {
				return;
			}
			
			//Crea los subelementos para dar la idea de selección dentro del selector previamente creado
			//JAPR 2016-03-14: Separado el selector del área del selector del objeto
			//Separa los cuadros para indicar la selección del objeto, mientras que el área será indicada con el recuadro punteado
			var intWidth = parseInt(objJQLastElementOrig.innerWidth());
			var intHeight = parseInt(objJQLastElementOrig.innerHeight());
			var objPosition = objJQLastElementOrig.position();
			var intTop = parseInt(objPosition.top);
			var intLeft = parseInt(objPosition.left);
			if (objJQLastElementOrig.css('margin-left')) {
				var intLeftAdj = parseInt(objJQLastElementOrig.css('margin-left'));
				if (!isNaN(intLeftAdj)) {
					intLeft += intLeftAdj;
				}
			}
			
			var intCenter = intLeft + parseInt(intWidth/2);
			var intMiddle = intTop + parseInt(intHeight/2);
			intWidth -= 2;
			intHeight -= 2;
			//Posiciones: TopLeft, TopCenter, TopRight, MiddleLeft, MiddleRight, BottomLeft, BottomCenter, BottomRight
			objParent.append('<div id="dmsltl" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft-2)+'px;top:'+(intTop-2)+'px;"></div>');
			objParent.append('<div id="dmsltc" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intCenter+'px;top:'+(intTop-2)+'px;"></div>');
			objParent.append('<div id="dmsltr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft+intWidth)+'px;top:'+(intTop-2)+'px;"></div>');
			objParent.append('<div id="dmslml" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft-2)+'px;top:'+intMiddle+'px;"></div>');
			objParent.append('<div id="dmslmr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft+intWidth)+'px;top:'+intMiddle+'px;"></div>');
			objParent.append('<div id="dmslbl" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft-2)+'px;top:'+(intTop+intHeight+2)+'px;"></div>');
			objParent.append('<div id="dmslbc" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intCenter+'px;top:'+(intTop+intHeight+2)+'px;"></div>');
			objParent.append('<div id="dmslbr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+(intLeft+intWidth)+'px;top:'+(intTop+intHeight+2)+'px;"></div>');
			/*var intWidth = parseInt(objDomSelector.innerWidth());
			var intHeight = parseInt(objDomSelector.innerHeight());
			var intCenter = parseInt(intWidth/2);
			var intMiddle = parseInt(intHeight/2);
			intWidth -= 2;
			intHeight -= 2;
			//Posiciones: TopLeft, TopCenter, TopRight, MiddleLeft, MiddleRight, BottomLeft, BottomCenter, BottomRight
			objDomSelector.append('<div id="dmsltl" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:-2px;top:-2px;"></div>');
			objDomSelector.append('<div id="dmsltc" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intCenter+'px;top:-2px;"></div>');
			objDomSelector.append('<div id="dmsltr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intWidth+'px;top:-2px;"></div>');
			objDomSelector.append('<div id="dmslml" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:-2px;top:'+intMiddle+'px;"></div>');
			objDomSelector.append('<div id="dmslmr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intWidth+'px;top:'+intMiddle+'px;"></div>');
			objDomSelector.append('<div id="dmslbl" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:-2px;top:'+intHeight+'px;"></div>');
			objDomSelector.append('<div id="dmslbc" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intCenter+'px;top:'+intHeight+'px;"></div>');
			objDomSelector.append('<div id="dmslbr" style="position:absolute;background-color:white;border:1px solid black;width:3px;height:3px;left:'+intWidth+'px;top:'+intHeight+'px;"></div>');
			*/
		}
		
		/* Dados los parámetros con el nombre de clase y el contenido que tendrá, crea un div por clase dentro de la página oculta "emptyPage", en el cual
		agregarla la definición de estilo para sobreescribir sólo las propiedades personalizadas de dicha clase. Si ya existía el div para dicha clase,
		entonces simplemente lo limpia y agrega la nueva definición que recibiera, de tal forma que se mantenga sólo el último estilo personalizado recibido
		//JAPR 2016-02-09: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		Agregado el parámetro sContainerName para permitir agregar estilos que no se removerán en la carga de estilos inyectada desde el servidor, sino bajo demanda en condiciones especiales,
		si no se especifica entonces se estará haciendo referencia al contenedor único para estilos inyectados desde el servidor
		*/
		function changeCSSStyle(sClassName, sClassValue, sContainerName) {
			console.log('Application.changeCSSStyle');
			
			if (!sClassName || !sClassValue) {
				return;
			}
			
			try {
				//Todos los divs de estilos se agregarán a la página vacia inicial, si no se pudiera obtener la referencia, entonces se agregan al body
				var objEmptyPage = $('#emptyPage');
				if (!objEmptyPage.length) {
					var objEmptyPage = $('body');
				}
				
				//Primero crea un div contenedor de todos los estilos que se van a definir, este div se puede limpiar cada vez que se van a redefinir los estilos
				var cssMainContainer = $('#css-modifier-container' + ((sContainerName)?sContainerName:''));
				if (cssMainContainer.length == 0) {
					var cssMainContainer = $('<div id="css-modifier-container'  + ((sContainerName)?sContainerName:'') + '"></div>');
					cssMainContainer.hide();
					cssMainContainer.appendTo(objEmptyPage);
				}
				
				//Luego crea un div contenedor de los estilos de la clase indicada, si ya existe (que no debería) simplemente reemplazará el estilo anterior
				classContainer = cssMainContainer.find('div[data-class="' + sClassName + '"]');
				if (classContainer.length == 0) {
					classContainer = $('<div data-class="' + sClassName + '"></div>');
					classContainer.appendTo(cssMainContainer);
				}
				
				classContainer.html('<style>' + sClassName + ' {' + sClassValue + '}</style>');
			} catch (e) {
				//Ignora este error, simplemente no se aplicarían los estilos
			}
		}
		
		/* Actualiza el contenedor con CSSs aplicados localmente dados los cambios recién subidos al server, ya que cuando se elimina alguna configuración, como localmente se queda el
		estile aplicado, hubiera heredado el style de las clases asignado al cargar la página, así que no se estaba limpiando correctamente, con esta función se permitirá actualizar
		también dicho default para reflejar este tipo de cambios
		*/
		function doUpdateCSSContainer(loader) {
			console.log('doUpdateCSSContainer');
			
			if (!loader || !loader.xmlDoc) {
				return;
			}
			
			if (loader.xmlDoc && loader.xmlDoc.status != 200) {
				alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
				return;
			}
			
			//Si llega a este punto es que se recibió una respuesta correctamente
			var response = loader.xmlDoc.responseText;
			try {
				var objResponse = JSON.parse(response);
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					return;
				}
				else {
					if (objResponse.warning) {
						console.log(objResponse.warning.desc);
					}
				}
				
				//En este punto objResponse debe tener la colección completa de estilos a aplicar, así que actualiza el contenedor de los mismos
				if (objResponse.newObject) {
					$('#css-modifier-container').html('')
					
					objCSSStyleColl = objResponse.newObject;
					if (objCSSStyleColl && !$.isEmptyObject(objCSSStyleColl)) {
						for (var intStyleID in objCSSStyleColl) {
							var objCSSStyle = objCSSStyleColl[intStyleID];
							if (!objCSSStyle) {
								continue;
							}
							
							changeCSSStyle(objCSSStyle.name, objCSSStyle.style);
						}
					}
				}
			} catch(e) {
				alert(e + "\r\n" + response.substr(0, 1000));
				return;
			}
		}
		
		function doSelectThis(oElement, oEvent) {
			console.log("doSelectThis " + oElement.id);
			//debugger;
			if (!oElement || !oEvent) {
				return;
			}
			
			//if (oElement == oEvent.target) {
				doSelectElement(undefined, false, oElement)
			//}
			oEvent.stopPropagation();
		}
		
		function doUnSelectThis(oElement, oEvent) {
			//console.log("doUnSelectThis " + oElement.id);
			
			if (!oElement || !oEvent) {
				return;
			}
			
			//if (oElement == oEvent.target) {
				doSelectElement(undefined, true, oElement)
			//}
			oEvent.stopPropagation();
		}
		
		function doUnSelectParent(oElement, oEvent) {
			//console.log("doUnSelectParent " + oElement.id);
			
			if (!oElement || !oEvent) {
				return;
			}
			
			if (oElement.parentElement) {
				$(oElement.parentElement).trigger('mouseleave');
			}
			oEvent.stopPropagation();
		}
		
		function doSelectParent(oElement, oEvent) {
			//console.log("doSelectParent " + oElement.id);
			
			if (!oElement || !oEvent) {
				return;
			}
			
			if (oElement.parentElement) {
				$(oElement.parentElement).trigger('mouseenter');
			}
			oEvent.stopPropagation();
		}
		</script>
	</head>
	
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<div id="emptyPage" name="emptyPage" style="display:none;"></div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
		<div id="PreviewHTML" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
		</div>
		
		<!-- include data-role="none" in the inputs to avoid JQM enhancement -->
		<!-- Login -->
		<div id="PreviewLogin" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="loginpage" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a ele3 ui-page-active" data-url="loginpage" style="min-height: 473px;">
<div id="loginpage_header" data-role="header" data-theme="a" ele="1" class="ele1 ui-header ui-bar-a" onclick="/*javascript: setStyle(event, this);*/" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars.png"></a><h1 ele="2" class="ele2 ui-title" onclick="/*javascript: setStyle(event, this);*/" role="heading" aria-level="1">Start session</h1></div>
					<div id="loginpage_body" data-role="content" ele="3" class="ele3 ui-content" onclick="/*javascript: setStyle(event, this);*/" role="main">
						<div id="kpiflogodiv">
							<img src="images/loginban.png" id="kpifimg" ele="1" class="1" onclick="javascript: setStyle(event, this);">
							</div>
						<ul style="padding-top:0px;" data-role="listview" data-inset="true" data-theme="a" class="ui-listview ui-listview-inset ui-corner-all ui-shadow ui-group-theme-a">
							<li class="loginli ui-li-static ui-body-inherit ui-first-child ui-last-child" id="li_settings" ele="2" onclick="javascript: setStyle(event, this);">
								<div ele="3" class="3" onclick="javascript: setStyle(event, this);">
									<label id="logtitle">Sign In</label>
								</div>
								<div class="logimput" id="myUserKPADiv">
									<label id="myUserKPAlabel" for="myUserKPA" ele="4" class="4" onclick="javascript: setStyle(event, this);">Username</label>
									<div class="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset"><input id="myUserKPA" name="myUserKPA_r" type="email" autocorrect="off" autocapitalize="off" autocomplete="off" value="demov6@kpiforms.com" ele="5" class="5" onclick="javascript: setStyle(event, this);"></div>
								</div>
								<div class="logimput" id="myPassKPADiv">
									<label for="myPassKPA" ele="6" class="6" onclick="javascript: setStyle(event, this);">Password</label>
									<div class="ui-input-text ui-body-inherit ui-corner-all ui-shadow-inset"><input id="myPassKPA" name="myPassKPA_r" type="password" autocorrect="off" autocapitalize="off" autocomplete="off" value="1234" ele="7" class="7" onclick="javascript: setStyle(event, this);"></div>
								</div>
								<div ele="8" class="8" onclick="javascript: setStyle(event, this);">
									<a class="loginbuttn ui-link ui-btn ui-shadow ui-corner-all" id="btnloginsurvey" data-role="button" role="button">Sign in</a>
								</div>
							</li>
						</ul>
					</div>
					<div id="loginpage_footer" data-role="footer" data-theme="a" ele="4" class="ele4 ui-footer ui-bar-a" onclick="/*javascript: setStyle(event, this);*/" role="contentinfo"><label ele="5" class="ele5" onclick="javascript: setStyle(event, this);">Powered by Bitam</label></div>
					</div>		
		</div>
		<!-- Login End -->
		
		<!-- Sección -->
		<div id="PreviewSection" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_2" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_2" style="min-height: 506px;">
<div id="Section_2_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a>            
							<a leftb="" id="Section_2_back" data-theme="a" style="float:none;top:10px;left:40px !important;" oty="2" ele="7" class="ele7 btnSectionLeft ui-btn-left" data-iconpos="left" data-role="none"
							onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);"
							>
							</a>
<h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="heading" aria-level="1">Sección</h1>
			<a id="section_2_next" data-theme="a" style="position:absolute;top:10px;right:10px;" oty="2" ele="8" class="ele8 btnSectionRight" data-iconpos="" data-icon="arrow-r" data-role="none"
			onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);"
			>
			</a>
</div>
		<div id="Section_2_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main">
		<br><span>Sección Tabla</span><br><br>
		<!-- Inicia Sección Tabla  -->
<table class="inlineTab ui-table" data-role="table" id="section_1_rec_0_table" data-mode="ct" style="table-layout:fixed;width:100%;">
				<!-- Agregar atributos, clases y eventos -->
				<thead class="oty2 stp0 ele9 stp-1" oty="2" ele="9" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);">
					<tr>
						<th style="overflow:hidden;" data-colstart="2">Nombre</th>
						<th  title="Valor" style="overflow:hidden;" qcol="6523" data-colstart="3">Valor</th>
					</tr>
				</thead>
					<tbody>
					<tr id="section_1_rec_0" class="oty2 stp0 stp-1 ele13" oty="2" ele="13" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);">
						<td class="selectorChk">
							<div class="overflowCell" >
							<!-- RV Jun2016 Se quitó el evento en la etiqueta y el estilo para la etiqueta se configurará al dar clic a la fila-->
							<!-- <label class="oty2 stp0 stp-1 ele10" id="qfield1_rec_0_option" oty="2" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);"> -->
							<label id="qfield1_rec_0_option" >
							Elemento 1
							</label>
							</div>
							<!-- RV Jun2016: No es necesario este div que está oculto ya que no se le especificará estilo -->
							<!-- 
							<div style="display:none;"><div id="qfield1_rec_0_area" data-role="fieldcontain" class="oty3 qtp2 dsp2 defaultastl ui-field-contain" style="display: none;">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4">
<label id="qfield1_rec_0_lab" class="ele5 defaultlstl" style="display:none;">1. Selector</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">

							<select class="ele10" name="qfield1_rec_0" id="qfield1_rec_0" data-native-menu="true" onchange="selSurvey.sections[1757].setActive(0);selSurvey.changeQuestion(1);selSurvey.questions[6522].doAction();" data-role="none" data-theme="a"><option value="Sí" selected="">Sí</option>
<option value="No">No</option>
</select>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
</div> 			
					-->			
						</td>
						<td qcol="6523">
						<!-- RV Jun2016: Se quita el contenedor correspondiente a la respuesta por que este no se configura en este apartado,
						hereda la configuración de cada tipo de pregunta -->
						<!-- <div class="overflowCell">
						<div id="qfield2_rec_0_area" data-role="fieldcontain" class="oty3 qtp1 dsp4 defaultastl ui-field-contain" style="display: inline-block;">

<table class="ele1 tableCont" style="display:none;" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4"><label id="qfield2_rec_0_lab" class="ele5 defaultlstl" for="qfield2_rec_0" style="display:none;">2. Valor</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" style=" width: 100% !important" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">
				<input class="ele10" name="qfield2_rec_0" style="width:98%;text-align:right; box-sizing:border-box;" id="qfield2_rec_0" type="text" numeric="decimal" data-theme="a" onkeyup="var strKeyCode = ((event.keyCode)?event.keyCode:event.charCode); if (String.fromCharCode(strKeyCode) == String.fromCharCode(13)) {selSurvey.setFocusToNextQuestion(event);}" onblur="selSurvey.questions[6523].updateValue();selSurvey.questions[6523].doAction();" onfocus="selSurvey.sections[1757].setActive(0);selSurvey.changeQuestion(2);" data-role="none" value="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
						</div> -->
						</td>
					</tr>
					<tr id="section_1_rec_1" class="oty2 stp0 stp-1 ele14" oty="2" ele="14" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);">
						<td class="selectorChk">
							<div class="overflowCell">
							<!-- RV Jun2016 Se quitó el evento en la etiqueta y el estilo para la etiqueta se configurará al dar clic a la fila-->
							<!-- <label class="oty2 stp0 stp-1 ele10" id="qfield1_rec_1_option" oty="2" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);"> -->
							<label id="qfield1_rec_1_option">
							Elemento 2</label>
							</div>
							<!-- RV Jun2016: No es necesario este div que está oculto ya que no se le especificará estilo -->
							<!-- 

 							<div style="display:none;"><div id="qfield1_rec_1_area" data-role="fieldcontain" class="oty3 qtp2 dsp2 defaultastl ui-field-contain" style="display: none;">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4">
<label id="qfield1_rec_1_lab" class="ele5 defaultlstl" style="display:none;">1. Selector</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">

							<select class="ele10" name="qfield1_rec_1" id="qfield1_rec_1" data-native-menu="true" onchange="selSurvey.sections[1757].setActive(1);selSurvey.changeQuestion(1);selSurvey.questions[6522].doAction();" data-role="none" data-theme="a"><option value="Sí" selected="">Sí</option>
<option value="No">No</option>
</select>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
</div> 				
						-->		
						</td>
						<td qcol="6523">
						<!-- RV Jun2016: Se quita el contenedor correspondiente a la respuesta por que este no se configura en este apartado,
						hereda la configuración de cada tipo de pregunta -->
						<!--
						<div class="overflowCell">
						<div id="qfield2_rec_1_area" data-role="fieldcontain" class="oty3 qtp1 dsp4 defaultastl ui-field-contain" style="display: inline-block;">

<table class="ele1 tableCont" style="display:none;" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4"><label id="qfield2_rec_1_lab" class="ele5 defaultlstl" for="qfield2_rec_1" style="display:none;">2. Valor</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" style=" width: 100% !important" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">
				<input class="ele10" name="qfield2_rec_1" style="width:98%;text-align:right; box-sizing:border-box;" id="qfield2_rec_1" type="text" numeric="decimal" data-theme="a" onkeyup="var strKeyCode = ((event.keyCode)?event.keyCode:event.charCode); if (String.fromCharCode(strKeyCode) == String.fromCharCode(13)) {selSurvey.setFocusToNextQuestion(event);}" onblur="selSurvey.questions[6523].updateValue();selSurvey.questions[6523].doAction();" onfocus="selSurvey.sections[1757].setActive(1);selSurvey.changeQuestion(2);" data-role="none" value="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
						</div>
						-->
						</td>
					</tr>
					<!-- Agregar atributos, clases y eventos -->
					<tr id="section_1_rec_totals" class="oty2 stp0 ele11 stp-1" oty="2" ele="11" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);">
						<td class="selectorChk">
							<div class="overflowCell"><label id="qfield1_rec_0_total">Total</label></div> 						</td>
					<td qcol="6523">
						<div id="qfield2_rec_0_total" class="overflowCell" style="text-align:right;">0</div>
					</td>
					</tr>
				</tbody>
			</table>

		<!-- Finaliza Sección Tabla  -->
		<br><span>Sección Múltiple Repeticiones</span><br><br>
		<!--- Inicio de Sección Maestro Detalle-->
<div id="section_1_rec_2_body" data-role="content" data-theme="a" class="oty2 stp3 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main"><div class="customizedHeaderContainer">
</div>		<div oty="2" ele="15" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" class="oty2 ele15 stp0 stp-1" id="section_1_summary" data-theme="a" >
<ul data-role="listview" id="section_1_summaryList" data-theme="a" data-dividertheme="a" data-counttheme="a" class="ui-listview ui-group-theme-a">
<li id="section_1_rec_0" class="ui-first-child">
					<a href="javascript: selSurvey.showSelectedRegMD(1874, 0);" class="ui-btn ui-btn-icon-right ui-icon-carat-r">
	                	<span style="display:none;position:absolute;">Test1</span>
<p oty="2" ele="16" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" class=" oty2 ele16 stp0 stp-1" style="font-weight:bold;font-size:12px;">1</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto A</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;left:34%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto B</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;left:63%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto C</p>
						</a>
					</li>
<li id="section_1_rec_1" class="ui-last-child">
					<a href="javascript: selSurvey.showSelectedRegMD(1874, 1);" class="ui-btn ui-btn-icon-right ui-icon-carat-r">
	                	<span style="display:none;position:absolute;">Test1</span>
<p oty="2" ele="16" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" class=" oty2 ele16 stp0 stp-1" style="font-weight:bold;font-size:12px;">2</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto D</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;left:34%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto E</p>
<p oty="2" ele="17" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" style="position:absolute;text-overflow:ellipsis;width:29%;left:63%;color:#5E87B0;" class=" oty2 ele17 stp0 stp-1 summaryList">Texto F</p>
						</a>
					</li>
</ul><br>
		</div>
<br>



				<br>
				<div data-type="horizontal" data-role="controlgroup" class="ele21 summaryButton ui-controlgroup ui-controlgroup-horizontal ui-corner-all"><div class="ui-controlgroup-controls ">
					<a onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" oty="2" ele="18" class="oty2 ele18 stp0 stp-1  ui-link ui-btn ui-shadow ui-corner-all ui-first-child" style="font-size:70%;width:31%;" data-role="button" role="button">Agregar</a>
					<a onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" oty="2" ele="18" class="oty2 ele18 stp0 stp-1  ui-link ui-btn ui-shadow ui-corner-all" style="width:32%;font-size:70%;" data-role="button" role="button">Remover</a>
					<a onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" oty="2" ele="18" class="oty2 ele18 stp0 stp-1  ui-link ui-btn ui-shadow ui-corner-all ui-last-child" style="width:32%;font-size:70%;" data-role="button" role="button">Terminar</a>
					</div></div><br><br>
<div class="customizedFooterContainer"></div>		</div>		
		<!--- Fin de Sección Maestro Detalle-->
		
<br>		</div>
		<br><br><br><div id="section_2_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position:fixed;bottom:0px;" oty="2" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
						<a id="section_2_footer_back" style="left:40px !important;float:none;" data-theme="a" data-iconpos="" oty="2" ele="7" class="ele7 btnSectionLeft ui-link">
						</a>
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a id="Section_2_footer_next" style="float:right;" data-theme="a" data-iconpos="" oty="2" ele="8" class="ele8 btnSectionRight ui-link">
						</a>
					</div>
				</div>
			</div>
		</div>
</div>
		</div>
		<!-- Sección End -->
		
		<!-- Preguntas abiertas -->
		<div id="PreviewOpen" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_3" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_3">
<div id="Section_3_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="/*javascript:setStyle(event, this);*/" onmouseenter="/*doUnSelectParent(this, event);doSelectThis(this, event);*/" onmouseleave="/*doSelectParent(this, event);doUnSelectThis(this, event);*/" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a>            <a leftb="" id="Section_3_back" data-theme="a" style="float:none;top:10px;left:40px !important;" class="ui-btn-left" data-iconpos="left" data-role="none"><img src="images/left.png"></a>
<h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="/*javascript:setStyle(event, this);*/" onmouseenter="/*doUnSelectParent(this, event);doSelectThis(this, event);*/" onmouseleave="/*doSelectParent(this, event);doUnSelectThis(this, event);*/" role="heading" aria-level="1">Preguntas abiertas</h1>
			<a id="section_3_next" data-theme="a" style="position:absolute;top:10px;right:10px;" data-iconpos="" data-icon="arrow-r" data-role="none"><img src="images/right.png"></a>
</div>
		<div id="Section_3_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="/*javascript:setStyle(event, this);*/" onmouseenter="/*doUnSelectParent(this, event);doSelectThis(this, event);*/" onmouseleave="/*doSelectParent(this, event);doUnSelectThis(this, event);*/" role="main">
<br><div id="qfield1_area" data-role="fieldcontain" class="oty3 qtp1 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="1" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield1_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">1. Numérica</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele10" name="qfield1" id="qfield1" type="text" numeric="decimal" data-theme="a" value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
<div id="qfield2_area" data-role="fieldcontain" class="oty3 qtp5 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="5" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield2_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Texto</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<textarea class="ele10 ui-input-text ui-shadow-inset ui-body-inherit ui-corner-all ui-textinput-autogrow" name="qfield2" id="qfield2" rows="8" cols="40" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" style="height: 52px;"></textarea>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
<div id="qfield3_area" data-role="fieldcontain" class="oty3 qtp6 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="6" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield3_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Alfanumérica</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele10" name="qfield3" id="qfield3" type="text" maxlength="255" data-theme="a" value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
<div id="qfield4_area" data-role="fieldcontain" class="oty3 qtp4 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="4" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield4_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">4. Fecha</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele10" name="qfield4" id="qfield4" type="date" data-theme="a" data-date="yyyy/MM/dd" data-date-format=" yyyy/MM/dd " value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
<div id="qfield5_area" data-role="fieldcontain" class="oty3 qtp12 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="12" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield5_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">5. Hora</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele10" name="qfield5" id="qfield5" type="time" data-theme="a" value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
<div id="qfield6_area" data-role="fieldcontain" class="oty3 qtp19 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="19" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield6_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">6. Contraseña</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele10" name="qfield6" id="qfield6" type="password" data-theme="a" value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
<br></div>
		</div>
		<br><br><br><div id="section_3_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position:fixed;bottom:0px;" oty="2" ele="5" onclick="/*javascript:setStyle(event, this);*/" onmouseenter="/*doUnSelectParent(this, event);doSelectThis(this, event);*/" onmouseleave="/*doSelectParent(this, event);doUnSelectThis(this, event);*/" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
						<a id="section_3_footer_back" style="left:40px !important;float:none;" data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/left.png">
						</a>
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a id="Section_3_footer_next" style="float:right;" data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/right.png">
						</a>
					</div>
				</div>
			</div>
		</div>
</div>
		</div>
		<!-- Preguntas abiertas End -->
		
		<!--
			<div id="Section_1_header" oty="<?=otySection?>" ele="<?=selemHdrBar?>" data-role="header" data-theme="h" style="position:relative;" class="ui-header ui-bar-h oty<?=otySection?> stp<?=sectNormal?> stp-1 ele<?=selemHdrBar?>" role="banner" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);" onclick="javascript: setStyle(event, this);">
				<a data-role="none" class="menu-trigger ui-btn-left" href="">
					<span class="pivot"></span>
					<img src="images/menubars2.png">
				</a>
				<a leftb="" id="Section_1_back" data-theme="b" style="float:none;top:4px;left:40px !important;" class="ui-btn-left" data-iconpos="left" data-role="none">
					<img src="images/left.png">
				</a>
				<h1 class="ui-title oty<?=otySection?> stp<?=sectNormal?> stp-1 ele<?=selemHdrLab?>" tabindex="0" role="heading" aria-level="1" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" onclick="javascript: setStyle(event, this);" oty="<?=otySection?>" ele="<?=selemHdrLab?>" >Seccion Abiertas</h1>
					<div class="ui-select" style="display: none;">
						<a href="#" role="button" id="Section_1_rec_0_selector-button" aria-haspopup="true" aria-owns="Section_1_rec_0_selector-menu" data-theme="c" class="ui-btn ui-btn-up-c ui-btn-icon-right ui-btn-corner-all ui-shadow">
							<span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
								<span class="ui-btn-text">Seccion Catalogo</span>
								<span class="ui-icon ui-icon-arrow-d ui-icon-shadow"></span>
							</span>
						</a>
						<select name="Section_1_rec_0_selector" id="Section_1_rec_0_selector" data-native-menu="false" data-theme="c" style="display:none" tabindex="-1">
							<option value="" data-placeholder="true">0. Checklist KPIForms v6 Dic 2015</option>
							<option value="1" rec="0">Seccion Uno</option>
							<option value="2" rec="0" selected="">Seccion Catalogo</option>
							<option value="3" rec="0">Seccion MC</option>
							<option value="4" rec="0">Seccion Dos</option>
							<option value="5" rec="0">Seccion tres</option>
							<option value="6" rec="0">Sección 1 Prueba Wixxo</option>
							<option value="7" rec="0">Seccion Cuatro</option>
							<option value="8" rec="0">Seccion Tabla sin datos</option>
							<option value="9" rec="0">Seccion Multimedia</option>
							<option value="10" rec="0">Seccion Multiple - 1</option>
							<option value="13" rec="0">Sección 2 Prueba Wixxo</option>
						</select>
					</div>
					<a id="Section_1_next" data-theme="b" style="position:absolute;top:4px;right:10px;" data-iconpos="" data-icon="arrow-r" data-role="none">
						<img src="images/right.png">
					</a>
			</div>
			<div id="Section_1_body" oty="<?=otySection?>" stp="<?=sectNormal?>" ele="<?=selemBdy?>" data-role="content" data-theme="c" class="ui-content ui-body-c oty<?=otySection?> stp<?=sectNormal?> stp-1 ele<?=selemBdy?>" role="main" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);" onclick="javascript: setStyle(event, this);">
				<br>
				<div id="qfield1_area" oty="<?=otyQuestion?>" qtp="<?=qtpOpenNumeric?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpOpenNumeric?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield1_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">1. Numeric</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<input name="qfield1" id="qfield1" ele="<?=qelemQtnElem?>" type="text" numeric="decimal" data-theme="c" value="" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ui-focus ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
				<div id="qfield2_area" oty="<?=otyQuestion?>" qtp="<?=qtpOpenAlpha?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpOpenAlpha?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield2_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Alfanumeric</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<input name="qfield2" id="qfield2" ele="<?=qelemQtnElem?>" type="text" maxlength="255" data-theme="c" value="" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
				<div id="qfield3_area" oty="<?=otyQuestion?>" qtp="<?=qtpOpenString?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpOpenString?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield3_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Text</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<textarea name="qfield3" id="qfield3" ele="<?=qelemQtnElem?>" rows="8" cols="40" style="width:97%;" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></textarea>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
				<div id="qfield4_area" oty="<?=otyQuestion?>" qtp="<?=qtpOpenDate?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpOpenDate?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield4_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">4. Date</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<input name="qfield4" id="qfield4" ele="<?=qelemQtnElem?>" type="date" data-theme="c" data-date="yyyy/MM/dd" data-date-format=" yyyy/MM/dd " value="" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
				<div id="qfield5_area" oty="<?=otyQuestion?>" qtp="<?=qtpOpenTime?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpOpenTime?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield5_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">5. Time</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<input name="qfield5" id="qfield5" ele="<?=qelemQtnElem?>" type="time" data-theme="c" value="" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
				<div id="qfield6_area" oty="<?=otyQuestion?>" qtp="<?=qtpPassword?>" dsp="<?=dspEntry?>" data-role="fieldcontain" class="ui-field-contain ui-body ui-br oty<?=otyQuestion?> qtp<?=qtpPassword?> dsp<?=dspEntry?> defaultastl" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">
					<table ele="<?=qelemLabTab?>" class="ele<?=qelemLabTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemLabTR?>" class="ele<?=qelemLabTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemLabTD?>" class="ele<?=qelemLabTD?> tdCont tdContLabAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemLabCont?>" cont="1" class="ele<?=qelemLabCont?>" style="width: 100%;">
										<label id="qfield6_lab" ele="<?=qelemLabElem?>" class="ui-input-text ele<?=qelemLabElem?> defaultlstl" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">6. Password</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<table ele="<?=qelemQtnTab?>" class="ele<?=qelemQtnTab?> tableCont" cellpadding="0" cellspacing="0" style="padding:0px;margin:0px;">
						<tbody>
							<tr ele="<?=qelemQtnTR?>" class="ele<?=qelemQtnTR?>" onclick="javascript: setStyle(event, this);">
								<td ele="<?=qelemQtnTD?>" class="ele<?=qelemQtnTD?> tdCont tdContAnsAdj" style="width:100%;" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
									<div ele="<?=qelemQtnCont?>" cont="1" class="ele<?=qelemQtnCont?>" style="width: 100%;">
										<input name="qfield6" id="qfield6" ele="<?=qelemQtnElem?>" type="password" data-theme="c" value="" class="ui-input-text ui-body-c ui-corner-all ui-shadow-inset ele<?=qelemQtnElem?>" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									</div>
								</td>
							</tr>
						</tbody>
					</table>
					<br>
				</div>
			</div>
			
			<div id="Section_1_footer" oty="<?=otySection?>" stp="<?=sectNormal?>" ele="<?=selemFtrBar?>" data-role="footer" data-theme="g" style="position: relative; bottom: 0px; visibility: visible;" class="ui-footer ui-bar-g oty<?=otySection?> stp<?=sectNormal?> stp-1 ele<?=selemFtrBar?>" role="contentinfo" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);" onclick="javascript: setStyle(event, this);">
				<div class="ui-bar ui-grid-a">
					<div class="ui-block-a">
						<a id="Section_1_footer_back" style="left:40px !important;float:none;" data-theme="b" data-iconpos="" class="ui-link">
							<img src="images/left.png">
						</a>
						<h1 style="margin-top:10px;">&nbsp;</h1>
					</div>
					<div class="ui-block-b">
						<div style="float:right;width: 220px;text-align: -webkit-right;">
							<a id="Section_1_footer_next" style="float:right;" data-theme="b" data-iconpos="" class="ui-link">
								<img src="images/right.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		-->
		
		<!-- Preguntas simple choice -->
		<div id="PreviewSimple" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_1" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_1">
<div id="Section_1_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a><h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="heading" aria-level="1">Preguntas Selección Sencilla</h1>
</div>
		<div id="Section_1_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main"><div class="customizedHeaderContainer">
</div><br><div id="qfield1_area" data-role="fieldcontain" class="oty3 qtp2 dsp2 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="2" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1">
<label id="qfield1_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">1. Menú</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
<br>
							<div class="ui-select"><div id="qfield1-button" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"><span class="ele10">Opción 1</span><select class="ele10" name="qfield1" id="qfield1" data-native-menu="true" data-theme="a" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
<option value="Opción 1" selected="">Opción 1</option>
<option value="Opción 2">Opción 2</option>
<option value="Opción 3">Opción 3</option>
</select></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield2_area" data-role="fieldcontain" class="oty3 qtp2 dsp0 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="0" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1">
<label id="qfield2_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Vertical</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
							<fieldset id="qfield2" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-corner-all ui-controlgroup-vertical"><div class="ui-controlgroup-controls ">
<div class="ui-radio"><label for="qfield2_radio_0" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-first-child ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 1</label><input class="ele10" type="radio" name="qfield2_radio" id="qfield2_radio_0" value="Opción 1" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
<div class="ui-radio"><label for="qfield2_radio_1" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 2</label><input class="ele10" type="radio" name="qfield2_radio" id="qfield2_radio_1" value="Opción 2" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
<div class="ui-radio"><label for="qfield2_radio_2" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-last-child ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 3</label><input class="ele10" type="radio" name="qfield2_radio" id="qfield2_radio_2" value="Opción 3" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
                            </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield3_area" data-role="fieldcontain" class="oty3 qtp2 dsp1 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="1" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1">
<label id="qfield3_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Horizontal</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
							<fieldset id="qfield3" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-controlgroup-horizontal ui-corner-all"><div class="ui-controlgroup-controls ">
<div class="ui-radio"><label for="qfield3_radio_0" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-first-child ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 1</label><input class="ele10" type="radio" name="qfield3_radio" id="qfield3_radio_0" value="Opción 1" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
<div class="ui-radio"><label for="qfield3_radio_1" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 2</label><input class="ele10" type="radio" name="qfield3_radio" id="qfield3_radio_1" value="Opción 2" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
<div class="ui-radio"><label for="qfield3_radio_2" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-last-child ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">Opción 3</label><input class="ele10" type="radio" name="qfield3_radio" id="qfield3_radio_2" value="Opción 3" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
									
                            </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield4_area" data-role="fieldcontain" class="oty3 qtp2 dsp5 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="5" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);" enhanced="true">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1">
<label id="qfield4_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">4. Autocompletado</label>
			</div>
</td>
				</tr>
			</tbody>
		</table>
		
		<!-- Actualizar código de la pregunta autocomplete por que se agregaron clases en la aplicación -->
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
							<select class="ele10" name="qfield4" id="qfield4" data-native-menu="true" data-theme="a" data-role="none" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" style="display: none;">
<option key="0" value=""></option>
<option value="Primera Opción">Primera Opción</option>
<option value="Segunda Opción">Segunda Opción</option>
<option value="Tercera Opción">Tercera Opción</option>
<!-- Se agregaron clases al autocomplete y se quito el estilo style="width: 96%; background: rgb(240, 240, 240);" por que así está en la forma de captura-->
</select><div class="ui-combobox inputautoCont"><div class="inputAutoContainer"><input class="inputauto ui-state-default ui-combobox-input ui-autocomplete-input ui-widget ui-widget-content ui-corner-left ui-input-text ele10" autocomplete="off" value="" qnum="4" qid="5043" qrec="5043_0" qaid="-1" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"></div><div class="divautoimg"><img class="autoimg" src="images/delAutoText.png"></div><a tabindex="-1" title="Show All Items" class="ui-corner-right ui-combobox-toggle" style="visibility: hidden;"></a></div>

<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield5_area" data-role="fieldcontain" class="oty3 qtp2 dsp8 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="8" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield5_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">5. Mapa</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<div style="display:none;">							<div class="ui-select"><div id="qfield5_attr_0-button" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"><span class="ele10">CHEDRAUI</span><select class="ele10" name="qfield5_attr_0" id="qfield5_attr_0" data-native-menu="true" data-theme="a" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
<option key="18" value="CHEDRAUI">CHEDRAUI</option>
<option key="9" value="INTERMEDIA SAN MATEO">INTERMEDIA SAN MATEO</option>
<option key="10" value="INTERMEDIA VALLE DE BRAVO">INTERMEDIA VALLE DE BRAVO</option>
<option key="14" value="INTERMEDIA ZINACANTEPEC II">INTERMEDIA ZINACANTEPEC II</option>
<option key="2" value="METEPEC II">METEPEC II</option>
<option key="6" value="PASE ATENCO">PASE ATENCO</option>
<option key="16" value="PASE ATLACOMULCO">PASE ATLACOMULCO</option>
<option key="15" value="PASE IXTLAHUACA">PASE IXTLAHUACA</option>
<option key="12" value="PASE METEPEC I ">PASE METEPEC I </option>
<option key="4" value="PASE SANTIAGO">PASE SANTIAGO</option>
<option key="13" value="PASE TENANCINGO">PASE TENANCINGO</option>
<option key="7" value="PASE TOLUCA">PASE TOLUCA</option>
<option key="11" value="PASE XONACATLAN">PASE XONACATLAN</option>
<option key="8" value="PASE ZINACANTEPEC III">PASE ZINACANTEPEC III</option>
<option key="1" value="PIRAMIDES">PIRAMIDES</option>
<option key="3" value="TOLUCA">TOLUCA</option>
<option key="5" value="ZINACANTEPEC I">ZINACANTEPEC I</option>
<option key="17" value="ZINACANTEPEC IV">ZINACANTEPEC IV</option>
							</select></div></div>
 						</div>
<div id="googlemapqfield5" class="singleChoiceMap" style="position: relative; overflow: hidden; transform: translateZ(0px); background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -37px; top: -92px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -37px; top: 164px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 219px; top: -92px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 219px; top: 164px;"></div></div></div></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -37px; top: -92px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -37px; top: 164px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 219px; top: -92px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 219px; top: 164px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div></div></div></div><div style="position: absolute; z-index: 0; transform: translateZ(0px); left: 0px; top: 0px;"><div style="overflow: hidden; width: 295px; height: 300px;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="transform: translateZ(0px); position: absolute; left: -37px; top: -92px;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i1!2i0!3i0!4i256!2m3!1e0!2sm!3i349016946!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=43571" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -37px; top: 164px;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i1!2i0!3i1!4i256!2m3!1e0!2sm!3i349016946!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=96773" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 219px; top: -92px;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i1!2i1!3i0!4i256!2m3!1e0!2sm!3i349016946!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=65155" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 219px; top: 164px;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i1!2i1!3i1!4i256!2m3!1e0!2sm!3i349016946!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=118357" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=10.04719,-50.066557&amp;z=1&amp;t=m&amp;hl=en&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 241px; height: 148px; position: absolute; left: 5px; top: 60px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 195px; bottom: 0px; width: 87px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2016</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@10.0471896,-50.0665575,1z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; display: none; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 22px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Map</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 30px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left-width: 0px; min-width: 40px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satellite</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 30px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div><div draggable="false" class="gm-style-cc" style="position: absolute; -webkit-user-select: none; height: 14px; line-height: 14px; right: 72px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><span>2000 km&nbsp;</span><div style="position: relative; display: inline-block; height: 8px; bottom: -1px; width: 70px;"><div style="width: 100%; height: 4px; position: absolute; left: 0px; top: 0px;"></div><div style="width: 4px; height: 8px; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"></div><div style="width: 4px; height: 8px; position: absolute; left: 0px; bottom: 0px; background-color: rgb(255, 255, 255);"></div><div style="position: absolute; height: 2px; left: 1px; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div><div style="position: absolute; width: 2px; height: 6px; left: 1px; top: 1px; background-color: rgb(102, 102, 102);"></div><div style="width: 2px; height: 6px; position: absolute; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div></div></div></div></div></div><br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>

<!-- RV Jul2016 Agregar Pregunta Metro -->
<!-- Inicio Pregunta Metro -->
<div id="qfield10_area" data-role="fieldcontain" class="oty3 qtp2 dsp10 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="2" dsp="10" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);" enhanced="true">

<table class="ele1 tableCont" style="" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield10_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">10. Metro</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
							<div id="qfield10" class="metrospan dhx_list">
								<div dhx_f_id="B001" class="dhx_list_item dhx_list_default_item" style="width:100%; height:autopx; padding:0px; margin:0px; overflow:hidden;">
								<div class="metroTable" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="metroImgCell" style="display:table-cell;"><img src="customerimages/fbm_bmd_0497/sample1.PNG">
									</div>
									<div class="metroDataCell">
										<label class="metroMainAttr" ele="18" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">B001</label>
										<label class="metroExtraAttr" ele="19" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">Opción 1</label>
								</div>
							</div>
						</div>
							<div dhx_f_id="B002" class="dhx_list_item dhx_list_default_item" style="width:100%; height:autopx; padding:0px; margin:0px; overflow:hidden;">
							<div class="metroTable" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
							<div class="metroImgCell" style="display:table-cell;"><img src="customerimages/fbm_bmd_0497/sample2.PNG"></div><div class="metroDataCell">
							<label class="metroMainAttr" ele="18" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">B002</label>
							<label class="metroExtraAttr" ele="19" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">Opción 2</label>
							</div></div></div>
							<div dhx_f_id="B003" class="dhx_list_item dhx_list_default_item" style="width:100%; height:autopx; padding:0px; margin:0px; overflow:hidden;">
							<div class="metroTable" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
							<div class="metroImgCell" style="display:table-cell;"><img src="customerimages/fbm_bmd_0497/sample3.PNG"></div><div class="metroDataCell">
							<label class="metroMainAttr" ele="18" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">B003</label>
							<label class="metroExtraAttr" ele="19" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">Opción 3</label></div></div></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<!-- Fin Pregunta Metro -->
<div class="customizedFooterContainer"></div>		</div>
		<br><br><br><div id="section_1_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position:fixed;bottom:0px;" oty="2" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/save.png">
						</a>
					</div>
				</div>
			</div>
		</div>
</div>		
		</div>
<!--<ul class="ui-autocomplete ui-front ui-menu ui-widget ui-widget-content" id="ui-id-12" tabindex="0" style="display: inline; top: 506.781px; left: 17px; width: 1303px;"><li class="ui-menu-item" id="ui-id-29" tabindex="-1"><a><strong></strong>O<strong></strong>p<strong></strong>c<strong></strong>i<strong></strong>ó<strong></strong>n<strong></strong> <strong></strong>u<strong></strong>n<strong></strong>o<strong></strong></a></li><li class="ui-menu-item" id="ui-id-30" tabindex="-1"><a><strong></strong>O<strong></strong>p<strong></strong>c<strong></strong>i<strong></strong>ó<strong></strong>n<strong></strong> <strong></strong>d<strong></strong>o<strong></strong>s<strong></strong></a></li><li class="ui-menu-item" id="ui-id-31" tabindex="-1"><a><strong></strong>O<strong></strong>p<strong></strong>c<strong></strong>i<strong></strong>ó<strong></strong>n<strong></strong> <strong></strong>t<strong></strong>r<strong></strong>e<strong></strong>s<strong></strong></a></li></ul>-->
		<!--
		<div id="PreviewSimple" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
			<div data-role="page" id="section_2" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" style="overflow:scroll;height:100px;" data-url="section_2">
			<div id="Section_2_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" onclick="javascript: setStyle(event, this);" role="banner">
									<a data-role="none" class="menu-trigger ui-btn-left" href="">
										<span class="pivot">
										</span><img src="images/menubars2.png"></a>            <a leftb="" id="Section_2_back" data-theme="a" style="float:none;top:10px;left:40px !important;" class="ui-btn-left" data-iconpos="left" data-role="none"><img src="images/left.png"></a>
			<h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" role="heading" aria-level="1">E-Simple Questions</h1>
					<div class="ui-select" style="display: none;"><a href="#" role="button" id="Section_2_rec_0_selector-button" aria-haspopup="true" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"><span>E-Simple Questions</span></a><select name="Section_2_rec_0_selector" id="Section_2_rec_0_selector" data-native-menu="false" data-theme="a" style="display:none" tabindex="-1">
			<option value="" data-placeholder="true">OMMC_styleDemo</option>
			<option value="1" rec="0">E-Open Questions</option>
			<option value="2" rec="0" selected="">E-Simple Questions</option>
			<option value="3" rec="0">E-Simple Questions Cat</option>
			<option value="4" rec="0">MD-Open Questions - 1</option>
			</select><div style="display: none;" id="Section_2_rec_0_selector-listbox-placeholder"></div></div>
						<a id="section_2_next" data-theme="a" style="position:absolute;top:10px;right:10px;" data-iconpos="" data-icon="arrow-r" data-role="none"><img src="images/right.png"></a>
			</div>
					<div id="Section_2_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" role="main">
			<br><div id="qfield9_area" data-role="fieldcontain" class="oty3 qtp2 dsp2 defaultastl ui-field-contain" style="display: inline-block;">

			<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
								<tbody>
									<tr class="ele2" ele="2" onclick="javascript: setStyle(event, this);">
										<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
			<div class="ele4" ele="4" cont="1">
			<label id="qfield9_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">9. SChoice Menu</label>
						</div>
			</td>
							</tr>
						</tbody>
					</table>
					<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
						<tbody>
							<tr class="ele7" ele="7" onclick="javascript: setStyle(event, this);">
								<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="ele9" ele="9" cont="1">
			<br>
										<div class="ui-select"><div id="qfield9-button" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"><span class="ele10">Valor 1</span><select class="ele10" name="qfield9" id="qfield9" data-native-menu="true" data-theme="a">
			<option value="Valor 1" selected="">Valor 1</option>
			<option value="Valor 2">Valor 2</option>
			<option value="Valor 3">Valor 3</option>
			<option value="Valor 4">Valor 4</option>
			</select></div></div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
			<br></div>
			<div id="qfield10_area" data-role="fieldcontain" class="oty3 qtp2 dsp0 defaultastl ui-field-contain" style="display: inline-block;">

			<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
								<tbody>
									<tr class="ele2" ele="2" onclick="javascript: setStyle(event, this);">
										<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
			<div class="ele4" ele="4" cont="1">
			<label id="qfield10_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">10. SChoice Vertical</label>
						</div>
			</td>
							</tr>
						</tbody>
					</table>
					<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
						<tbody>
							<tr class="ele7" ele="7" onclick="javascript: setStyle(event, this);">
								<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="ele9" ele="9" cont="1">
										<fieldset id="qfield10" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-corner-all ui-controlgroup-vertical"><div class="ui-controlgroup-controls ">
			<div class="ui-radio"><label for="qfield10_radio_0" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-first-child">Valor 1</label><input class="ele10" type="radio" name="qfield10_radio" id="qfield10_radio_0" value="Valor 1"></div>
												
			<div class="ui-radio"><label for="qfield10_radio_1" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off">Valor 2</label><input class="ele10" type="radio" name="qfield10_radio" id="qfield10_radio_1" value="Valor 2"></div>
												
			<div class="ui-radio"><label for="qfield10_radio_2" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-last-child">Valor 3</label><input class="ele10" type="radio" name="qfield10_radio" id="qfield10_radio_2" value="Valor 3"></div>
												
										</div></fieldset>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
			<br></div>
			<div id="qfield11_area" data-role="fieldcontain" class="oty3 qtp2 dsp1 defaultastl ui-field-contain" style="display: inline-block;">

			<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
								<tbody>
									<tr class="ele2" ele="2" onclick="javascript: setStyle(event, this);">
										<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
			<div class="ele4" ele="4" cont="1">
			<label id="qfield11_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">11. SChoice Horizontal</label>
						</div>
			</td>
							</tr>
						</tbody>
					</table>
					<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
						<tbody>
							<tr class="ele7" ele="7" onclick="javascript: setStyle(event, this);">
								<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="ele9" ele="9" cont="1">
										<fieldset id="qfield11" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-controlgroup-horizontal ui-corner-all"><div class="ui-controlgroup-controls ">
			<div class="ui-radio"><label for="qfield11_radio_0" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-first-child">Valor 1</label><input class="ele10" type="radio" name="qfield11_radio" id="qfield11_radio_0" value="Valor 1"></div>
												
			<div class="ui-radio"><label for="qfield11_radio_1" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off">Valor 2</label><input class="ele10" type="radio" name="qfield11_radio" id="qfield11_radio_1" value="Valor 2"></div>
												
			<div class="ui-radio"><label for="qfield11_radio_2" style="font-size:14;" class="ui-btn ui-corner-all ui-btn-inherit ui-radio-off ui-last-child">Valor 3</label><input class="ele10" type="radio" name="qfield11_radio" id="qfield11_radio_2" value="Valor 3"></div>
												
										</div></fieldset>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
			<br></div>
			<div id="qfield12_area" data-role="fieldcontain" class="oty3 qtp2 dsp5 defaultastl ui-field-contain" style="display: inline-block;" enhanced="true">

			<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
								<tbody>
									<tr class="ele2" ele="2" onclick="javascript: setStyle(event, this);">
										<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
			<div class="ele4" ele="4" cont="1"><label id="qfield12_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">12. SChoice Autocomplete</label>
							</div>
			</td>
							</tr>
						</tbody>
					</table>
					<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
						<tbody>
							<tr class="ele7" ele="7" onclick="javascript: setStyle(event, this);">
								<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="ele9" ele="9" cont="1">
									<div style="">							<select class="ele10" name="qfield12_attr_0" id="qfield12_attr_0" data-native-menu="true" data-theme="a" data-role="none" style="display: none;">
			<option key="0" value=""></option>
			<option key="1" value="1">1</option>
			<option key="2" value="2">2</option>
										</select><div class="ui-combobox"><input class="ui-state-default ui-combobox-input ui-autocomplete-input ui-widget ui-widget-content ui-corner-left ui-input-text" autocomplete="off" value="" qnum="12" qid="3704" qrec="3704_0" qaid="2082" style="width: 96%; background: rgb(240, 240, 240);"><a tabindex="-1" title="Show All Items" class="ui-corner-right ui-combobox-toggle" style="visibility: hidden;"></a></div>
									</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
			<br></div>
			<div id="qfield13_area" data-role="fieldcontain" class="oty3 qtp2 dsp2 defaultastl ui-field-contain" style="display: inline-block;">

			<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
								<tbody>
									<tr class="ele2" ele="2" onclick="javascript: setStyle(event, this);">
										<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
			<div class="ele4" ele="4" cont="1"><label id="qfield13_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript: setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">13. SChoice Map</label>
							</div>
			</td>
							</tr>
						</tbody>
					</table>
					<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
						<tbody>
							<tr class="ele7" ele="7" onclick="javascript: setStyle(event, this);">
								<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">
									<div class="ele9" ele="9" cont="1">
									<div style="display:none;">							<label for="qfield13_attr_0" style="width:100%;font-size:14;">Tienda</label>
										<div class="ui-select"><div id="qfield13_attr_0-button" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow"><span class="ele10">PASE ATENCO</span><select class="ele10" name="qfield13_attr_0" id="qfield13_attr_0" data-native-menu="true" data-theme="a"><option key="6" value="PASE ATENCO">PASE ATENCO</option>
			</select></div></div>
									</div>
			<div id="googlemapqfield13" class="singleChoiceMap" style="position: relative; overflow: hidden; transform: translateZ(0px) translateZ(0px); background-color: rgb(229, 227, 223);"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 163, -141);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 328px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 584px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 328px; top: 280px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 584px; top: 280px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 72px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 840px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 72px; top: 280px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 840px; top: 280px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -184px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: -184px; top: 280px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 1096px; top: 24px;"></div><div style="width: 256px; height: 256px; transform: translateZ(0px); position: absolute; left: 1096px; top: 280px;"></div></div></div></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 328px; top: 24px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 584px; top: 24px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 328px; top: 280px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 584px; top: 280px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 72px; top: 24px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 840px; top: 24px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 72px; top: 280px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 840px; top: 280px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -184px; top: 24px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: -184px; top: 280px;"><canvas draggable="false" height="256" width="256" style="-webkit-user-select: none; position: absolute; left: 0px; top: 0px; height: 256px; width: 256px;"></canvas></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 1096px; top: 24px;"></div><div style="width: 256px; height: 256px; overflow: hidden; transform: translateZ(0px); position: absolute; left: 1096px; top: 280px;"></div></div></div></div><div style="position: absolute; z-index: 0; transform: translateZ(0px); left: -178px; top: -2px;"><div style="overflow: hidden; width: 1347px; height: 300px;"><img src="http://maps.googleapis.com/maps/api/js/StaticMapService.GetMapImage?1m2&amp;1i39942&amp;2i14054&amp;2e1&amp;3u7&amp;4m2&amp;1u1347&amp;2u300&amp;5m5&amp;1e0&amp;5sen&amp;6sus&amp;10b1&amp;12b1&amp;token=117466" style="width: 1347px; height: 300px;"></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="transform: translateZ(0px); position: absolute; left: 328px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i30!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=13159" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 584px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i31!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=29862" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 584px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i31!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=107731" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 328px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i30!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=66361" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 72px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i29!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=8430" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 72px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i29!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=61632" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 840px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i32!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=71232" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 840px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i32!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=124434" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -184px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i28!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=44929" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: -184px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i28!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=98131" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 1096px; top: 24px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i33!3i55!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=34733" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="transform: translateZ(0px); position: absolute; left: 1096px; top: 280px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i7!2i33!3i56!4i256!2m3!1e0!2sm!3i339008132!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=87935" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 0px 0px 0px; transform: matrix(1, 0, 0, 1, 163, -141);"><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="transform: translateZ(0px); position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"><div style="cursor: default; position: absolute; width: 137px; height: 38px; left: -110px; top: 436px; z-index: 436;"><div style="position: absolute; left: 0px; top: 0px;"><div style="width: 0px; height: 0px; border-right-width: 10px; border-right-style: solid; border-right-color: transparent; border-left-width: 10px; border-left-style: solid; border-left-color: transparent; border-top-width: 24px; border-top-style: solid; border-top-color: rgba(0, 0, 0, 0.0980392); position: absolute; left: 59px; top: 38px;"></div><div style="position: absolute; left: 0px; top: 0px; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; width: 137px; height: 38px; background-color: rgba(0, 0, 0, 0.2);"></div><div style="border-top-width: 24px; position: absolute; left: 59px; top: 35px;"><div style="position: absolute; overflow: hidden; left: -6px; top: -1px; width: 16px; height: 30px;"><div style="position: absolute; left: 6px; transform: skewX(22.6deg); transform-origin: 0px 0px 0px; height: 24px; width: 10px; box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 6px; background-color: rgb(255, 255, 255);"></div></div><div style="position: absolute; overflow: hidden; top: -1px; left: 10px; width: 16px; height: 30px;"><div style="position: absolute; left: 0px; transform: skewX(-22.6deg); transform-origin: 10px 0px 0px; height: 24px; width: 10px; box-shadow: rgba(0, 0, 0, 0.6) 0px 1px 6px; background-color: rgb(255, 255, 255);"></div></div></div><div style="position: absolute; left: 1px; top: 1px; border-radius: 2px; width: 135px; height: 36px; background-color: rgb(255, 255, 255);"></div></div><div class="gm-style-iw" style="top: 9px; position: absolute; left: 15px; width: 107px;"><div style="display: inline-block; overflow: auto; max-height: 168px; max-width: 654px;"><div style="overflow: auto;">PASE ATENCO</div></div></div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 10px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 524px; top: 60px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016 Google, INEGI</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 158px; bottom: 0px; width: 151px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2016 Google, INEGI</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016 Google, INEGI</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px; display: none;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@21.8307685,266.3760502,7z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; display: none; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div draggable="false" class="gm-style-cc" style="position: absolute; -webkit-user-select: none; height: 14px; line-height: 14px; right: 72px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><span>50 km&nbsp;</span><div style="position: relative; display: inline-block; height: 8px; bottom: -1px; width: 44px;"><div style="width: 100%; height: 4px; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"></div><div style="width: 4px; height: 8px; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"></div><div style="width: 4px; height: 8px; position: absolute; left: 0px; bottom: 0px; background-color: rgb(255, 255, 255);"></div><div style="position: absolute; height: 2px; left: 1px; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div><div style="position: absolute; width: 2px; height: 6px; left: 1px; top: 1px; background-color: rgb(102, 102, 102);"></div><div style="width: 2px; height: 6px; position: absolute; bottom: 1px; right: 1px; background-color: rgb(102, 102, 102);"></div></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=21.830769,-93.62395&amp;z=7&amp;t=m&amp;hl=en&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div class="gmnoprint" style="margin: 10px; z-index: 0; position: absolute; cursor: pointer; left: 0px; top: 0px;"><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show street map" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-left-radius: 2px; border-top-left-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; min-width: 22px; font-weight: 500; background-color: rgb(255, 255, 255); background-clip: padding-box;">Map</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; left: 0px; top: 30px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show street map with terrain" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden; display: none;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Terrain</label></div></div></div><div class="gm-style-mtc" style="float: left;"><div draggable="false" title="Show satellite imagery" style="direction: ltr; overflow: hidden; text-align: center; position: relative; color: rgb(86, 86, 86); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 8px; border-bottom-right-radius: 2px; border-top-right-radius: 2px; -webkit-background-clip: padding-box; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-left-width: 0px; min-width: 40px; background-color: rgb(255, 255, 255); background-clip: padding-box;">Satellite</div><div style="z-index: -1; padding: 2px; border-bottom-left-radius: 2px; border-bottom-right-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; position: absolute; right: 0px; top: 30px; text-align: left; display: none; background-color: white;"><div draggable="false" title="Show imagery with street names" style="color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; -webkit-user-select: none; font-size: 11px; padding: 6px 8px 6px 6px; direction: ltr; text-align: left; white-space: nowrap; background-color: rgb(255, 255, 255);"><span role="checkbox" style="box-sizing: border-box; position: relative; line-height: 0; font-size: 0px; margin: 0px 5px 0px 0px; display: inline-block; border: 1px solid rgb(198, 198, 198); border-radius: 1px; width: 13px; height: 13px; vertical-align: middle; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: -2px; width: 13px; height: 11px; overflow: hidden;"><img src="http://maps.gstatic.com/mapfiles/mv/imgs8.png" draggable="false" style="position: absolute; left: -52px; top: -44px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 68px; height: 67px;"></div></span><label style="vertical-align: middle; cursor: pointer;">Labels</label></div></div></div></div></div></div>						</div>
								</td>
							</tr>
						</tbody>
					</table>
			<br></div>
					</div>
					<br><br><br><div id="section_2_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position: fixed; bottom: 0px; width: 100%;" oty="2" ele="5" onclick="javascript: setStyle(event, this);" role="contentinfo">
						<div class="ui-bar ui-grid-a">
							<div class="ui-block-a">
									<a id="section_2_footer_back" style="left:40px !important;float:none;" data-theme="a" data-iconpos="" class="ui-link"><img src="images/left.png"></a>
			<h1 style="margin-top:10px;">&nbsp;</h1>
							</div>
							<div class="ui-block-b">
								<div style="float:right;width: 220px;text-align: -webkit-right;">
									<a id="Section_2_footer_next" style="float:right;" data-theme="a" data-iconpos="" class="ui-link"><img src="images/right.png"></a>
								</div>
							</div>
						</div>
					</div>
			<div class="ui-screen-hidden ui-popup-screen ui-overlay-a" id="Section_2_rec_0_selector-listbox-screen"></div><div class="ui-popup-container ui-popup-hidden ui-popup-truncate" id="Section_2_rec_0_selector-listbox-popup"><div data-theme="a" data-overlay-theme="a" id="Section_2_rec_0_selector-listbox" class="ui-selectmenu ui-popup ui-body-a ui-overlay-shadow ui-corner-all"><div class="ui-header ui-bar-a"><h1 class="ui-title">OMMC_styleDemo</h1></div><ul class="ui-selectmenu-list ui-listview ui-group-theme-a" id="Section_2_rec_0_selector-menu" role="listbox" aria-labelledby="Section_2_rec_0_selector-button" data-theme="a"><li data-option-index="0" data-icon="false" data-placeholder="true" class="ui-screen-hidden" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn">OMMC_styleDemo</a></li><li data-option-index="1" data-icon="false" class="ui-first-child" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn">E-Open Questions</a></li><li data-option-index="2" data-icon="false" class="" role="option" aria-selected="true"><a href="#" tabindex="-1" class="ui-btn ui-btn-active">E-Simple Questions</a></li><li data-option-index="3" data-icon="false" class="" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn">E-Simple Questions Cat</a></li><li data-option-index="4" data-icon="false" class="ui-last-child" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn">MD-Open Questions - 1</a></li></ul></div></div><br><br><br></div>
		</div>
		-->
		<!-- Preguntas simple choice End -->
		<!-- Preguntas Multiple Choice-->
		<div id="PreviewMultiple" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_1" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_1">
<div id="Section_1_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a><h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="heading" aria-level="1">Preguntas de Selección Múltiple</h1>
</div>
		<div id="Section_1_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main"><div class="customizedHeaderContainer">
</div><br><div id="qfield1_area" data-role="fieldcontain" class="oty3 qtp3 dsp2 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="2" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield1_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">1. Menú</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<div class="ui-select ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"><a href="#qfield1-listbox" role="button" id="qfield1-button" aria-haspopup="true" class="ui-btn ui-icon-carat-d ui-btn-icon-right ui-btn-a ui-corner-all ui-shadow ui-li-has-count" data-rel="popup"><span class="ele10">Opción 1</span><span class="ui-li-count ui-body-inherit" style="display: none;">1</span></a><select class="ele10" name="qfield1" id="qfield1" data-native-menu="false" multiple="multiple" data-theme="a" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" tabindex="-1">
<option value="Opción 1">Opción 1</option>
<option value="Opción 2">Opción 2</option>
<option value="Opción 3">Opción 3</option>
</select><div style="display: none;" id="qfield1-listbox-placeholder"><!-- placeholder for qfield1-listbox --></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield2_area" data-role="fieldcontain" class="oty3 qtp3 dsp0 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="0" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield2_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Vertical</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<fieldset id="qfield2" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-corner-all ui-controlgroup-vertical"><div class="ui-controlgroup-controls ">
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield2_opt_0_label" for="qfield2_opt_0" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-first-child ele10">Opción 1</label><input type="checkbox" name="qfield2_opt_0" id="qfield2_opt_0" value="Opción 1" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield2_opt_0_labelnum" for="qfield2_opt_0_label"></label> 		            					
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield2_opt_1_label" for="qfield2_opt_1" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ele10">Opción 2</label><input type="checkbox" name="qfield2_opt_1" id="qfield2_opt_1" value="Opción 2" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield2_opt_1_labelnum" for="qfield2_opt_1_label"></label> 		            					
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield2_opt_2_label" for="qfield2_opt_2" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-last-child ele10">Opción 3</label><input type="checkbox" name="qfield2_opt_2" id="qfield2_opt_2" value="Opción 3" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield2_opt_2_labelnum" for="qfield2_opt_2_label"></label> 		            					
                        </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<!-----RV Jul16: Se agregan preguntas de selección múltiple con captura despliegue vertical, en estas sólo se configurará el input de captura, los demás estilos
se heredan de las preguntas de selección múltiple que no tienen captura -->
<div id="qfield6_area" data-role="fieldcontain" class="oty3 qtp3 dsp0 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="0">

<table class="ele1 tableCont" style="" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4"><label id="qfield6_lab" class="ele5 defaultlstl" for="qfield6" style="">6. Vertical con captura</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">
						<fieldset id="qfield6" class="ui-grid-a ui-controlgroup ui-corner-all ui-controlgroup-vertical" data-role="controlgroup" data-type="horizontal"><div class="ui-controlgroup-controls ">
												<div class="ui-block-a">
			            					<label for="qfield6_opt_0" style="width:100%;font-size:14px;" class="ele10">Opción 1</label>
					            				</div>
					            				<div class="ui-block-b" style="width:46%;">
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield6_opt_0" id="qfield6_opt_0" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 1" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
</div>
												<div class="ui-block-a">
			            					<label for="qfield6_opt_1" style="width:100%;font-size:14px;" class="ele10">Opción 2</label>
					            				</div>
					            				<div class="ui-block-b" style="width:46%;">
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield6_opt_1" id="qfield6_opt_1" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 2" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
</div>
												<div class="ui-block-a">
			            					<label for="qfield6_opt_2" style="width:100%;font-size:14px;" class="ele10">Opción 3</label>
					            				</div>
					            				<div class="ui-block-b" style="width:46%;">
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield6_opt_2" id="qfield6_opt_2" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 3" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
</div>
                        </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<!-----RV Jul16: Terminan preguntas de selección múltiple despliegue vertical con captura-->

<div id="qfield3_area" data-role="fieldcontain" class="oty3 qtp3 dsp1 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="1" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield3_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Horizontal</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<fieldset id="qfield3" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-controlgroup-horizontal ui-corner-all"><div class="ui-controlgroup-controls ">
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield3_opt_0_label" for="qfield3_opt_0" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-first-child ele10">Opción 1</label><input type="checkbox" name="qfield3_opt_0" id="qfield3_opt_0" value="Opción 1" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield3_opt_0_labelnum" for="qfield3_opt_0_label"></label> 		            					
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield3_opt_1_label" for="qfield3_opt_1" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ele10">Opción 2</label><input type="checkbox" name="qfield3_opt_1" id="qfield3_opt_1" value="Opción 2" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield3_opt_1_labelnum" for="qfield3_opt_1_label"></label> 		            					
										<div class="ui-checkbox"><label style="font-size:14;" id="qfield3_opt_2_label" for="qfield3_opt_2" class="ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-last-child ele10">Opción 3</label><input type="checkbox" name="qfield3_opt_2" id="qfield3_opt_2" value="Opción 3" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="" id="qfield3_opt_2_labelnum" for="qfield3_opt_2_label"></label> 		            					
                        </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>

<!-----RV Jul16: Se agregan preguntas de selección múltiple con captura despliegue horizontal, en estas sólo se configurará el input de captura, los demás estilos
se heredan de las preguntas de selección múltiple con despliegue horizontal que no tienen captura -->

<div id="qfield7_area" data-role="fieldcontain" class="oty3 qtp3 dsp1 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="1">

<table class="ele1 tableCont" style="" cellpadding="0" cellspacing="0">
					<tbody>
						<tr class="ele2">
							<td class="ele3 tdCont tdContLabAdj">
<div class="ele4"><label id="qfield7_lab" class="ele5 defaultlstl" for="qfield7" style="">7. Horizontal con captura</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0">
			<tbody>
				<tr class="ele7">
					<td class="ele8 tdCont tdContAnsAdj">
						<div class="ele9">
						<fieldset id="qfield7" class="ui-grid-a ui-controlgroup ui-controlgroup-horizontal ui-corner-all" data-role="controlgroup" data-type="horizontal"><div class="ui-controlgroup-controls ">
											<table style="float:left; border: 1px solid transparent; margin-left: 10px;">
												<tbody><tr>
													<td>
			            					<label for="qfield7_opt_0" style="width:100%;font-size:14px;" class="ele10">Opción 1</label>
													</td>
													<td>
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield7_opt_0" id="qfield7_opt_0" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 1" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
												</td></tr>
																							</tbody></table>
											<table style="float:left; border: 1px solid transparent; margin-left: 10px;">
												<tbody><tr>
													<td>
			            					<label for="qfield7_opt_1" style="width:100%;font-size:14px;" class="ele10">Opción 2</label>
													</td>
													<td>
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield7_opt_1" id="qfield7_opt_1" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 2" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
												</td></tr>
																							</tbody></table>
											<table style="float:left; border: 1px solid transparent; margin-left: 10px;">
												<tbody><tr>
													<td>
			            					<label for="qfield7_opt_2" style="width:100%;font-size:14px;" class="ele10">Opción 3</label>
													</td>
													<td>
						            	<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset"><input class="ele17" ele="17" name="qfield7_opt_2" id="qfield7_opt_2" type="text" data-inline="true" maxlength="255" data-theme="a" value="Texto 3" style="" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
												</td></tr>
																							</tbody></table>
                        </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<!-----RV Jul16: Terminan preguntas de selección múltiple despliegue horizontal con captura-->

<div id="qfield4_area" data-role="fieldcontain" class="oty3 qtp3 dsp5 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="5" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);" enhanced="true">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield4_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">4. Autocompletado</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
							<div class="side-by-side clearfix  ui-field-contain" data-role="fieldcontain">
								<select multiple="multiple" data-role="none" class="chzn-select" data-native-menu="true" style="width: 98%; display: none;" data-theme="a" id="qfield4" name="qfield4"> 
<option value="Primera Opción">Primera Opción</option>
<option value="Segunda Opción">Segunda Opción</option>
<option value="Tercera Opción">Tercera Opción</option>
</select><div class="chosen-container chosen-container-multi" style="width: 98%;" title="" id="qfield4_chosen"><ul class="chosen-choices ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"><li class="search-field"><input type="text" value="Select Some Options" class="default" autocomplete="off" style="width: 88px;"></li></ul><div class="chosen-drop"><ul class="chosen-results"><li class="active-result" data-option-array-index="0">Primera Opción</li><li class="active-result" data-option-array-index="1">Segunda Opción</li><li class="active-result" data-option-array-index="2">Tercera Opción</li></ul></div></div>
</div><br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield5_area" data-role="fieldcontain" class="oty3 qtp3 dsp6 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="3" dsp="6" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield5_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">5. Clasificación</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<fieldset id="qfield5" data-role="controlgroup" data-type="horizontal" class="ui-controlgroup ui-corner-all ui-controlgroup-vertical"><div class="ui-controlgroup-controls ">
										<div class="ui-checkbox"><label class="rankLabel ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-first-child ele10" style="font-size:14;" id="qfield5_opt_0_label" for="qfield5_opt_0">Opción 1</label><input type="checkbox" name="qfield5_opt_0" id="qfield5_opt_0" value="Opción 1" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="rankBullet" id="qfield5_opt_0_labelnum" for="qfield5_opt_0_label"></label> 		            					
										<div class="ui-checkbox"><label class="rankLabel ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ele10" style="font-size:14;" id="qfield5_opt_1_label" for="qfield5_opt_1">Opción 2</label><input type="checkbox" name="qfield5_opt_1" id="qfield5_opt_1" value="Opción 2" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="rankBullet" id="qfield5_opt_1_labelnum" for="qfield5_opt_1_label"></label> 		            					
										<div class="ui-checkbox"><label class="rankLabel ui-btn ui-corner-all ui-btn-inherit ui-checkbox-off ui-last-child ele10" style="font-size:14;" id="qfield5_opt_2_label" for="qfield5_opt_2">Opción 3</label><input type="checkbox" name="qfield5_opt_2" id="qfield5_opt_2" value="Opción 3" class="custom ele10" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
			            				<label class="rankBullet" id="qfield5_opt_2_labelnum" for="qfield5_opt_2_label"></label> 		            					
                        </div></fieldset>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div class="customizedFooterContainer"></div>		</div>
		<br><br><br><div id="section_1_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position: fixed; bottom: 0px; width: 100%;" oty="2" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/save.png">
						</a>
					</div>
				</div>
			</div>
		</div>
<div class="ui-screen-hidden ui-popup-screen ui-overlay-a" id="qfield1-listbox-screen"></div><div class="ui-popup-container ui-popup-hidden ui-popup-truncate" id="qfield1-listbox-popup"><div data-theme="a" data-overlay-theme="a" id="qfield1-listbox" class="ui-selectmenu ui-popup ui-body-a ui-overlay-shadow ui-corner-all"><div class="ui-header ui-bar-a"><h1 class="ui-title"></h1><a role="button" href="#" class="ui-btn ui-corner-all ui-btn-left ui-btn-icon-notext ui-icon-delete">Close</a></div><ul class="ui-selectmenu-list ui-listview ui-group-theme-a" id="qfield1-menu" role="listbox" aria-labelledby="qfield1-button" data-theme="a"><li data-option-index="0" data-icon="false" class="ui-first-child" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn ui-checkbox-off ui-btn-icon-right">Opción 1</a></li><li data-option-index="1" data-icon="false" class="" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn ui-checkbox-off ui-btn-icon-right">Opción 2</a></li><li data-option-index="2" data-icon="false" class="ui-last-child" role="option" aria-selected="false"><a href="#" tabindex="-1" class="ui-btn ui-checkbox-off ui-btn-icon-right">Opción 3</a></li></ul></div></div></div>
		</div>
		<!-- Preguntas "Media""-->
<div id="PreviewMedia" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_1" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_1">
<div id="Section_1_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a><h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="heading" aria-level="1">Preguntas Media</h1>
</div>
		<div id="Section_1_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main"><div class="customizedHeaderContainer">
</div><br><div id="qfield1_area" data-role="fieldcontain" class="oty3 qtp8 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="8" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield1_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">1. Foto</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div name="qfield1_photo" id="qfield1_photo" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/foto.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
			<img name="qfield1_img" id="qfield1_img" style="-webkit-border-radius:5px;padding:4px;border:1px solid #949494;display:none;height:80px;left:36%;" src="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield2_area" data-role="fieldcontain" class="oty3 qtp21 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="21" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield2_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Audio</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
                    <div name="qfield2_audio" id="qfield2_audio" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/audio.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield3_area" data-role="fieldcontain" class="oty3 qtp22 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="22" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield3_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Video</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
					<div name="qfield3_video" id="qfield3_video" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/video.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield4_area" data-role="fieldcontain" class="oty3 qtp23 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="23" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield4_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">4. Sketch</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
<a style="max-width:78px;width:90%;" class="ele10 ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);" ele="10" data-inline="true" data-mini="true" data-theme="a" id="qfield4fullscreenbutton" name="qfield4" data-role="button" role="button">
										<img width="28px" height="28px" src="images/sketch.png">
									</a>				<img name="qfield4_minisketch" id="qfield4_minisketch" style="-webkit-border-radius:5px;padding:4px;border:1px solid #949494;display:none;height:80px;left:36%;" src="">
<br>				<div name="qfield4_photo" id="qfield4_photo" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/foto.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
			<img name="qfield4_img" id="qfield4_img" style="-webkit-border-radius:5px;padding:4px;border:1px solid #949494;display:none;height:80px;left:36%;" src="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield5_area" data-role="fieldcontain" class="oty3 qtp15 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="15" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield5_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">5. Documento</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<div name="qfield5_document" id="qfield5_document" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/document.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield6_area" data-role="fieldcontain" class="oty3 qtp26 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="26" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield6_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">6. ROC</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
<div id="qfield6_diverror"></div>
<br>				<div name="qfield6_photo" id="qfield6_photo" data-role="fileuploader"><div class="qq-uploader">                        <div style="display:none;" class="qq-upload-drop-area"><span>Drop files here to upload</span></div>
                            <div class="qq-upload-button ui-btn ui-btn-up-c ui-btn-inline ui-btn-corner-all ui-shadow ele10" style="max-width: 78px; width: 90%; height: 52px; position: relative; overflow: hidden; direction: ltr;" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
                            <a data-inline="true" data-mini="true" data-theme="c">
                                <span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible; padding-left:32%">
                                    <span>
                                        <img width="28px" height="28px" src="images/foto.png"> 
                                    </span>
                                </span>
                            </a>
                        <input multiple="multiple" name="file" style="position: absolute; right: 0px; top: 0px; font-family: Arial; font-size: 118px; margin: 0px; padding: 0px; cursor: pointer; opacity: 0;"></div>
                        <ul class="qq-upload-list" style="display:none"></ul></div></div>
			<img name="qfield6_img" id="qfield6_img" style="-webkit-border-radius:5px;padding:4px;border:1px solid #949494;display:none;height:80px;left:36%;" src="">
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div class="customizedFooterContainer"></div>		</div>
		<br><br><br><div id="section_1_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position:fixed;bottom:0px;" oty="2" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/save.png">
						</a>
					</div>
				</div>
			</div>
		</div>
</div>
</div>		
		<!-- Preguntas "Otras""
		
		- En las plantillas de colores en las preguntas tipo "Otro" que no muestran etiqueta en la aplicación no debe poderse configurar estilos para el contenedor de la etiqueta ya que estas no se deben ver en la aplicación. Estos tipos de pregunta Otro que no muestran eiqueta son:
		 * Salto de Sección
		 * Mensaje
		 * HTML
		 *Sincronización
		 * Salir
		 * Actualizar Destino
		
		Se eliminaron los eventos en los contenedores de esos tipos de preguntas
		-->
		
		<div id="PreviewOther" style="display: none; width: 100%; height: 100%; overflow: scroll;" class="prevContent">
<div data-role="page" id="section_1" data-theme="a" tabindex="0" class="ui-page ui-page-theme-a oty2 stp0 stp-1 ele1 ui-page-active" data-url="section_1">
<div id="Section_1_header" data-role="header" data-theme="a" class="oty2 stp0 ele2 stp-1 ui-header ui-bar-a" oty="2" ele="2" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="banner">
						<a data-role="none" class="menu-trigger ui-btn-left" href="">
							<span class="pivot">
							</span><img src="images/menubars2.png"></a><h1 class="oty2 stp0 ele3 stp-1 ui-title" oty="2" ele="3" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="heading" aria-level="1">Otras</h1>
			<a id="section_1_next" data-theme="a" style="position:absolute;top:10px;right:10px;" data-iconpos="" data-icon="arrow-r" data-role="none" class="ui-btn-right"><img src="images/right.png"></a>
</div>
		<div id="Section_1_body" data-role="content" data-theme="a" class="oty2 stp0 ele4 stp-1 ui-content ui-body-a" oty="2" ele="4" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="main"><div class="customizedHeaderContainer">
</div><br><div id="qfield1_area" data-role="fieldcontain" class="oty3 qtp13 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="13" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield1_lab" class="ele5 defaultlstl" style="" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
					<div data-inline="true">
						<a name="qfield1_skipsection" id="qfield1_skipsection" style="max-width:78px;width:90%;" data-inline="true" data-mini="true" data-theme="a" data-role="button" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini ele10" role="button" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
							<img width="28px" height="28px" src="images/skipquestions.png">
						</a>
					</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield2_area" data-role="fieldcontain" class="oty3 qtp14 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="14" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield2_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">2. Calculada</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"><input class="ele10" name="qfield2" id="qfield2" type="text" numeric="decimal" data-theme="a" onkeyup="" value="1000" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield3_area" data-role="fieldcontain" class="oty3 qtp10 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="10" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield3_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">3. Firma</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9 ele10" ele="10" cont="1" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
				<div class="divwrapper">
<div id="qfield3" class="sigPad sigPadformsqfield3">
<div data-inline="true"><ul class="sigNav" style="display: block;"><li class="drawIt" style="display:inline"><a href="" class="ui-link current">Dibuje su firma</a></li><li class="clearButton" style="display: list-item;"><a href="" class="ui-link">Limpiar</a></li></ul><div class="sig sigWrapper current" style="width: 290px; overflow: hidden; display: block;"><div class="typed" style="display: none;"></div><canvas style="width:290px" height="150" pxundefined="" name="padsignatureqfield3" id="padsignatureqfield3" class="pad"></canvas><input type="hidden" name="output" class="output" id="outputqfield3" value=""></div><input type="hidden" id="qsignatureqfield3" name="qimageencodeqfield3" value=""></div></div></div><br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield4_area" data-role="fieldcontain" class="oty3 qtp11 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="11" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield4_lab" class="ele5 defaultlstl" style="display:none;" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div id="qfield4" data-inline="true">Mensaje</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield5_area" data-role="fieldcontain" class="oty3 qtp11 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="11" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield5_lab" class="ele5 defaultlstl" style="display:none;" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div id="qfield5" data-inline="true">HTML</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield6_area" data-role="fieldcontain" class="oty3 qtp17 dsp0 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="17" dsp="0" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield6_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">6. Llamada Telefónica</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
						<div data-inline="true">
							<a name="qfield6" id="qfield6" phonenum="8332133290" style="max-width:78px;width:90%;" data-inline="true" data-mini="true" data-theme="a" data-role="button" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini ele10" role="button" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"> 								<img width="28px" height="28px" src="images/phone.png">							</a>						</div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield7_area" data-role="fieldcontain" class="oty3 qtp25 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="25" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield7_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">7. Código de barras</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset ele10" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)"><input class="ele10" name="qfield7" id="qfield7" type="text" data-theme="a" value="" ele="10" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);"></div>
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield8_area" data-role="fieldcontain" class="oty3 qtp16 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="16" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield8_lab" class="ele5 defaultlstl" style="" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
					<div data-inline="true">						<a name="qfield8_sync&quot;" id="qfield8_sync&quot;" style="max-width:78px;width:90%;" data-inline="true" data-mini="true" data-theme="a" data-role="button" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini ele10" role="button" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">							<img width="28px" height="28px" src="images/sync.png">						</a>					</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield9_area" data-role="fieldcontain" class="oty3 qtp24 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="24" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield9_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">9. Sección tabla</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		
		<!-- Se quitan los eventos en el contedor de sección tabla ya que la sección tabla se configura en otra opción-->
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" >
					<td class="ele8 tdCont tdContAnsAdj" ele="8" >
						<div class="ele9 ele10" ele="10" cont="1" >
<br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield10_area" data-role="fieldcontain" class="oty3 qtp27 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="27" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield10_lab" class="ele5 defaultlstl" style="" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
					<div data-inline="true">
						<a name="qfield10_exit" id="qfield10_exit" style="max-width:78px;width:90%;" href="javascript: objApp.backToFormsList(true);" data-inline="true" data-mini="true" data-theme="a" data-role="button" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all ui-mini ele10" role="button" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">
							<img width="28px" height="28px" src="images/exit.png">
						</a>
					</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>
<div id="qfield11_area" data-role="fieldcontain" class="oty3 qtp28 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="28" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" >
							<td class="ele3 tdCont tdContLabAdj" ele="3" >
<div class="ele4" ele="4" cont="1"><label id="qfield11_lab" class="ele5 defaultlstl" style="" ele="5" ></label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
					<div data-inline="true">						<a name="qfield11_sync&quot;" id="qfield11_sync&quot;" style="max-width:78px;width:90%;" href="javascript: selSurvey.save(undefined, undefined, 5237);" data-inline="true" data-mini="true" data-theme="c" data-role="button" class="ui-link ui-btn ui-btn-c ui-btn-inline ui-shadow ui-corner-all ui-mini ele10" role="button" ele="10" onclick="setStyle(event, this)" onmouseenter="doSelectThis(this, event)" onmouseleave="doUnSelectThis(this, event)">								<img width="28px" height="28px" src="images/updateData.png">						</a>					</div>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>

<div id="qfield12_area" data-role="fieldcontain" class="oty3 qtp29 dsp4 defaultastl ui-field-contain" style="display: inline-block;" oty="3" qtp="29" dsp="4" onmouseenter="doUnSelectParent(this, event);" onmouseleave="doSelectParent(this, event);">

<table class="ele1 tableCont" style="" cellpadding="0" cellspacing="0" ele="1">
					<tbody>
						<tr class="ele2" ele="2" onclick="javascript:setStyle(event, this);">
							<td class="ele3 tdCont tdContLabAdj" ele="3" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
<div class="ele4" ele="4" cont="1"><label id="qfield12_lab" class="ele5 defaultlstl" style="" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this.parentElement, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this.parentElement, event);doUnSelectThis(this, event);">12. Geolocalización Visual</label>
				</div>
</td>
				</tr>
			</tbody>
		</table>
		<table class="ele6 tableCont" cellpadding="0" cellspacing="0" ele="6">
			<tbody>
				<tr class="ele7" ele="7" onclick="javascript:setStyle(event, this);">
					<td class="ele8 tdCont tdContAnsAdj" ele="8" onmouseenter="doSelectThis(this, event);" onmouseleave="doUnSelectThis(this, event);">
						<div class="ele9" ele="9" cont="1">
				<div class="myLocationContainer">
					<div style="position: relative; overflow: hidden; background-color: rgb(229, 227, 223);" id="googlemapqfield12" class="ele10 singleChoiceMap"><div class="gm-style" style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0;"><div style="position: absolute; left: 0px; top: 0px; overflow: hidden; width: 100%; height: 100%; z-index: 0; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default;"><div style="position: absolute; left: 0px; top: 0px; z-index: 1; width: 100%; transform-origin: 478px 75px 0px; transform: matrix(1, 0, 0, 1, 0, 0); will-change: transform;"><div style="position: absolute; left: 0px; top: 0px; z-index: 100; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; position: absolute; left: 573px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 317px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 317px; top: 113px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 573px; top: 113px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 61px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 61px; top: 113px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 829px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 829px; top: 113px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -195px; top: 113px;"></div><div style="width: 256px; height: 256px; position: absolute; left: -195px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1085px; top: -143px;"></div><div style="width: 256px; height: 256px; position: absolute; left: 1085px; top: 113px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 101; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 102; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 103; width: 100%;"><div style="position: absolute; left: 0px; top: 0px; z-index: -1;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 573px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 317px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 317px; top: 113px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 573px; top: 113px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 61px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 61px; top: 113px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 829px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 829px; top: 113px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -195px; top: 113px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: -195px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1085px; top: -143px;"></div><div style="width: 256px; height: 256px; overflow: hidden; position: absolute; left: 1085px; top: 113px;"></div></div></div></div><div style="position: absolute; left: 0px; top: 0px; z-index: 0;"><div aria-hidden="true" style="position: absolute; left: 0px; top: 0px; z-index: 1; visibility: inherit;"><div style="position: absolute; left: 317px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65535!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=111608" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 317px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65535!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=33739" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 573px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65536!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=90926" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 573px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65536!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=37724" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 61px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65534!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=54421" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -195px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65533!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=50436" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1085px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65538!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=21027" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: -195px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65533!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=128305" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 829px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65537!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=17042" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 829px; top: -143px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65537!3i65535!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=94911" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 61px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65534!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=107623" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div style="position: absolute; left: 1085px; top: 113px; transition: opacity 200ms ease-out;"><img src="http://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i65538!3i65536!4i256!2m3!1e0!2sm!3i359000000!3m9!2sen!3sUS!5e18!12m1!1e47!12m3!1e37!2m1!1ssmartmaps!4e0&amp;token=74229" draggable="false" alt="" style="position: absolute; left: 0px; top: 0px; width: 256px; height: 256px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="gm-style-pbc" style="position: absolute; left: 0px; top: 0px; z-index: 2; width: 100%; height: 100%; transition-duration: 0.3s; opacity: 0; display: none;"><p class="gm-style-pbt">Use two fingers to move the map</p></div><div style="position: absolute; left: 0px; top: 0px; z-index: 3; width: 100%; height: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 4; width: 100%; transform-origin: 478px 75px 0px; transform: matrix(1, 0, 0, 1, 0, 0);"><div style="position: absolute; left: 0px; top: 0px; z-index: 104; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 105; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 106; width: 100%;"></div><div style="position: absolute; left: 0px; top: 0px; z-index: 107; width: 100%;"></div></div></div><div style="margin-left: 5px; margin-right: 5px; z-index: 1000000; position: absolute; left: 0px; bottom: 0px;"><a target="_blank" href="https://maps.google.com/maps?ll=-0.000401,0.001023&amp;z=17&amp;t=m&amp;hl=en&amp;gl=US&amp;mapclient=apiv3" title="Click to see this area on Google Maps" style="position: static; overflow: visible; float: none; display: inline;"><div style="width: 66px; height: 26px; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/google4.png" draggable="false" style="position: absolute; left: 0px; top: 0px; width: 66px; height: 26px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div></a></div><div style="padding: 15px 21px; border: 1px solid rgb(171, 171, 171); font-family: Roboto, Arial, sans-serif; color: rgb(34, 34, 34); box-shadow: rgba(0, 0, 0, 0.2) 0px 4px 16px; z-index: 10000002; display: none; width: 256px; height: 148px; position: absolute; left: 519px; top: 60px; background-color: white;"><div style="padding: 0px 0px 10px; font-size: 16px;">Map Data</div><div style="font-size: 13px;">Map data ©2016</div><div style="width: 13px; height: 13px; overflow: hidden; position: absolute; opacity: 0.7; right: 12px; top: 12px; z-index: 10000; cursor: pointer;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/mapcnt6.png" draggable="false" style="position: absolute; left: -2px; top: -336px; width: 59px; height: 492px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div><div class="gmnoprint" style="z-index: 1000001; position: absolute; right: 72px; bottom: 0px; width: 87px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a style="color: rgb(68, 68, 68); text-decoration: none; cursor: pointer; display: none;">Map Data</a><span>Map data ©2016</span></div></div></div><div class="gmnoscreen" style="position: absolute; right: 0px; bottom: 0px;"><div style="font-family: Roboto, Arial, sans-serif; font-size: 11px; color: rgb(68, 68, 68); direction: ltr; text-align: right; background-color: rgb(245, 245, 245);">Map data ©2016</div></div><div class="gmnoprint gm-style-cc" draggable="false" style="z-index: 1000001; -webkit-user-select: none; height: 14px; line-height: 14px; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a href="https://www.google.com/intl/en_US/help/terms_maps.html" target="_blank" style="text-decoration: none; cursor: pointer; color: rgb(68, 68, 68);">Terms of Use</a></div></div><div style="width: 25px; height: 25px; overflow: hidden; display: none; margin: 10px 14px; position: absolute; top: 0px; right: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/sv5.png" draggable="false" class="gm-fullscreen-control" style="position: absolute; left: -52px; top: -86px; width: 164px; height: 112px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px;"></div><input class="googleMapSearch" id="search_qfield12" data-role="none" placeholder="Enter a query" autocomplete="off" style="z-index: 0; position: absolute; left: 0px; top: 0px;"><div draggable="false" class="gm-style-cc" style="-webkit-user-select: none; height: 14px; line-height: 14px; display: none; position: absolute; right: 0px; bottom: 0px;"><div style="opacity: 0.7; width: 100%; height: 100%; position: absolute;"><div style="width: 1px;"></div><div style="width: auto; height: 100%; margin-left: 1px; background-color: rgb(245, 245, 245);"></div></div><div style="position: relative; padding-right: 6px; padding-left: 6px; font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); white-space: nowrap; direction: ltr; text-align: right; vertical-align: middle; display: inline-block;"><a target="_new" title="Report errors in the road map or imagery to Google" href="https://www.google.com/maps/@-0.0004012,0.0010231,17z/data=!10m1!1e1!12b1?source=apiv3&amp;rapsrc=apiv3" style="font-family: Roboto, Arial, sans-serif; font-size: 10px; color: rgb(68, 68, 68); text-decoration: none; position: relative;">Report a map error</a></div></div><div class="gmnoprint gm-bundled-control gm-bundled-control-on-bottom" draggable="false" controlwidth="28" controlheight="93" style="margin: 10px; -webkit-user-select: none; position: absolute; bottom: 107px; right: 28px;"><div class="gmnoprint" controlwidth="28" controlheight="55" style="position: absolute; left: 0px; top: 38px;"><div draggable="false" style="-webkit-user-select: none; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; cursor: pointer; width: 28px; height: 55px; background-color: rgb(255, 255, 255);"><div title="Zoom in" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: 0px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div><div style="position: relative; overflow: hidden; width: 67%; height: 1px; left: 16%; top: 0px; background-color: rgb(230, 230, 230);"></div><div title="Zoom out" style="position: relative; width: 28px; height: 27px; left: 0px; top: 0px;"><div style="overflow: hidden; position: absolute; width: 15px; height: 15px; left: 7px; top: 6px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl.png" draggable="false" style="position: absolute; left: 0px; top: -15px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none; width: 120px; height: 54px;"></div></div></div></div><div class="gm-svpc" controlwidth="28" controlheight="28" style="box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; border-radius: 2px; width: 28px; height: 28px; cursor: url(&quot;http://maps.gstatic.com/mapfiles/openhand_8_8.cur&quot;) 8 8, default; position: absolute; left: 0px; top: 0px; background-color: rgb(255, 255, 255);"><div style="position: absolute; left: 1px; top: 1px;"></div><div style="position: absolute; left: 1px; top: 1px;"><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -26px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Pegman is on top of the Map" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -52px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div aria-label="Street View Pegman Control" style="width: 26px; height: 26px; overflow: hidden; position: absolute; left: 0px; top: 0px; visibility: hidden;"><img src="http://maps.gstatic.com/mapfiles/api-3/images/cb_scout5.png" draggable="false" style="position: absolute; left: -147px; top: -78px; width: 215px; height: 835px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div><div class="gmnoprint" controlwidth="28" controlheight="0" style="display: none; position: absolute;"><div title="Rotate map 90 degrees" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; cursor: pointer; display: none; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: 6px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div><div class="gm-tilt" style="width: 28px; height: 28px; overflow: hidden; position: absolute; border-radius: 2px; box-shadow: rgba(0, 0, 0, 0.298039) 0px 1px 4px -1px; top: 0px; cursor: pointer; background-color: rgb(255, 255, 255);"><img src="http://maps.gstatic.com/mapfiles/api-3/images/tmapctrl4.png" draggable="false" style="position: absolute; left: -141px; top: -13px; width: 170px; height: 54px; -webkit-user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"></div></div></div></div><div class="myLocationPointer"></div></div>
					
				</div><br>						</div>
					</td>
				</tr>
			</tbody>
		</table>
</div>

<div class="customizedFooterContainer"></div>		</div>
		<br><br><br><div id="section_1_footer" data-role="footer" data-theme="a" class="oty2 stp0 ele5 stp-1 ui-footer ui-bar-a" style="position: fixed; bottom: 0px; width: 100%;" oty="2" ele="5" onclick="javascript:setStyle(event, this);" onmouseenter="doUnSelectParent(this, event);doSelectThis(this, event);" onmouseleave="doSelectParent(this, event);doUnSelectThis(this, event);" role="contentinfo">
			<div class="ui-bar ui-grid-a">
				<div class="ui-block-a">
<h1 style="margin-top:10px;">&nbsp;</h1>
				</div>
				<div class="ui-block-b">
					<div style="float:right;width: 220px;text-align: -webkit-right;">
						<a id="Section_1_footer_next" style="float:right;" data-theme="a" data-iconpos="" class="ui-link">
							<img src="images/right.png">
						</a>
					</div>
				</div>
			</div>
		</div>
</div>		
		</div>
	</body>
</html>
<?
		die();
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		$intTemplateID = getParamValue('TemplateID', 'both', '(int)');
		if ($intTemplateID <= 0) {
			$intTemplateID = -1;
		}
		
		//Si vienen los parámetros btnApplyAll o btnApplyGroup, entonces los estilos recibidos se asignarán a la configuración global correspondiente según el tipo que lo estaba configurando
		if (array_key_exists("btnApplyAll", $aHTTPRequest->POST) || array_key_exists("btnApplyGroup", $aHTTPRequest->POST)) {
			$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstanceWithID($aRepository, (int)$intTemplateID);
			if (!is_null($anInstanceTPL)) {
				$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intTemplateID);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				if (!is_null($anInstance)) {
					$anInstanceTPL->setGlobalStyle($anInstance, $aHTTPRequest->POST);
					
					//Al terminar de grabar los datos, debe volver a cargar el objeto de estilo para que incluya la configuración global asignada
					$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intTemplateID);
					$aHTTPRequest->RedirectTo = $anInstance;
					return null;
				}
			}
		}
		//Si viene el parámetro Style en el POST, quiere decir que se está usando el request para grabar la configuración, no sólo para cargarla
		elseif (array_key_exists("Clear", $aHTTPRequest->POST)) {
			//Este proceso eliminará todas las definiciones de estilos aplicados para el template
			$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstanceWithID($aRepository, (int)$intTemplateID);
			if (!is_null($anInstanceTPL)) {
				$anInstanceTPL->clear();
			}
			
			$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intTemplateID);
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		elseif (array_key_exists("Style", $aHTTPRequest->POST)) {
			/*Inicia: Grabar el nombre del template*/
			$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstanceWithID($aRepository, (int)$intTemplateID);
			if (is_null($anInstanceTPL))
			{
				$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstance($aRepository);
			}
			$anInstanceTPL->updateFromArray($aHTTPRequest->POST);
			$aResultTPL = $anInstanceTPL->save();
			/*Termina: Grabar el nombre del template*/
			
			$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intTemplateID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else {
			//Cuando sólo se grabará nombre del template
			if (array_key_exists("TemplateName", $aHTTPRequest->POST))
			{
				/*Inicia: Grabar el nombre del template*/
				$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstanceWithID($aRepository, (int)$intTemplateID);
				if (is_null($anInstanceTPL))
				{
					$anInstanceTPL = BITAMEFormsAppCustomStylesTpl::NewInstance($aRepository);
				}
				$anInstanceTPL->updateFromArray($aHTTPRequest->POST);
				$aResultTPL = $anInstanceTPL->save();
				/*Termina: Grabar el nombre del template*/
			}
		}
		
		$anInstance = $strCalledClass::NewInstanceAll($aRepository, $intTemplateID);
		
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("TemplateID", $anArray)) {
			$this->TemplateID = $anArray["TemplateID"];
		}
		
		if (array_key_exists("TemplateName", $anArray)) {
			$this->TemplateName = $anArray["TemplateName"];
		}
		
		if (array_key_exists("Style", $anArray)) {
			$arrStyles = @$anArray["Style"];
			$arrSectionTypes = @$anArray["SectionType"];
			$arrObjectTypes = @$anArray["ItemType"];
			$arrQTypeIDs = @$anArray["QTypeID"];
			$arrQDisplayModes = @$anArray["QDisplayMode"];
			$arrElementTypes = @$anArray["ElementType"];
			if (!is_null($arrStyles) && !is_null($arrSectionTypes) && !is_null($arrObjectTypes) && !is_null($arrQTypeIDs) && !is_null($arrQDisplayModes) && !is_null($arrElementTypes)) {
				//Verifica si los parámetros recibidos son array o son valores puntuales, ya que internamente se manejará todo como un array (si el parámetro Style es array, se asumirá
				//que todos deben serlo, ya que la relación debe ser 1:1 entre los parámetros)
				if (!is_array($arrStyles)) {
					$arrStyles = array($arrStyles);
					$arrSectionTypes = array($arrSectionTypes);
					$arrObjectTypes = array($arrObjectTypes);
					$arrQTypeIDs = array($arrQTypeIDs);
					$arrQDisplayModes = array($arrQDisplayModes);
					$arrElementTypes = array($arrElementTypes);
				}
				
				try {
					$arrCSSStyleInfo = array('dirty' => 0, 'style' => array());
					$arrDefaultCSSStyle = array(qelemLabCont => $arrCSSStyleInfo, qelemLabElem => $arrCSSStyleInfo, qelemQtnCont => $arrCSSStyleInfo, qelemQtnElem => $arrCSSStyleInfo);
					foreach($arrStyles as $intStyleIdx => $strStyle) {
						$style = trim((string) $strStyle);
						//Si el estilo no viene asignado, no tiene caso actualizar en la metadata
						//Si el valor ya no se asignó, significa que se usará el default original, así que se remueve este elemento del array para que se borre de la metadata
						/*if ($style == "") {
							continue;
						}*/
						
						//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
						//Los estilos a descargar en el configurador por ahora son sólo estilos directos de objetos, no globales (eventualmente se podrían configurar los estilos globales, por ahora se
						//hace sólo mediante los botones para propagar estilos)
						$settingType = tsstObject;
						//@JAPR
						$sectionType = (int) @$arrSectionTypes[$intStyleIdx];
						$objectType = (int) @$arrObjectTypes[$intStyleIdx];
						$questionType = (int) @$arrQTypeIDs[$intStyleIdx];
						$displayType = (int) @$arrQDisplayModes[$intStyleIdx];
						$elementType = (int) @$arrElementTypes[$intStyleIdx];
						
						//Agrega el estilo para que el proceso de grabado lo actualice en la metadata
						//Para que se pueda descargar la definición correcta, adicionalmente se agregará al $this->ArrayStyles de tal forma que el response incluya el cambio
						//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
						if (!isset($this->ArrayStylesUpdated[$settingType])) {
							$this->ArrayStylesUpdated[$settingType] = array();
						}
						if (!isset($this->ArrayStylesUpdated[$settingType][$objectType])) {
							$this->ArrayStylesUpdated[$settingType][$objectType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType])) {
							$this->ArrayStyles[$settingType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType][$objectType])) {
							$this->ArrayStyles[$settingType][$objectType] = array();
						}
						//@JAPR
						
						if (!isset($this->ArrayStylesUpdated[$settingType][$objectType][$sectionType])) {
							$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType][$objectType][$sectionType])) {
							$this->ArrayStyles[$settingType][$objectType][$sectionType] = array();
						}
						
						if (!isset($this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType])) {
							$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType])) {
							$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType] = array();
						}
						
						if (!isset($this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType])) {
							$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType])) {
							$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType] = array();
						}
						
						if (!isset($this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType])) {
							$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType] = array();
						}
						if (!isset($this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType])) {
							$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType] = array();
						}
						
						$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType] = $arrCSSStyleInfo;
						$this->ArrayStylesUpdated[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType]['style'] = $style;
						$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType] = $arrCSSStyleInfo;
						//Decodifica el String de estilo para mandarlo como un objeto de propiedades
						try {
							$arrJSON = json_decode($style);
						} catch (Exception $e) {
							$arrJSON = array();
						}
						
						$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType]['style'] = $arrJSON;
						$this->ArrayStyles[$settingType][$objectType][$sectionType][$questionType][$displayType][$elementType]['styleStr'] = $style;
					}
				} catch (Exception $e) {
					//No reportará error en este caso, pero un error en este punto significaría un request mal formado, así que no grabará lo que el usuario pensaba que había cambiado
				}
			}
		}
			
		return $this;
	}
	
	/* Insert o actualiza los estilos preparados en el array de actualización
	*/
	function save() {
		try {
			//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
			foreach($this->ArrayStylesUpdated as $settingType => $arrSettingTypes) {
				foreach($arrSettingTypes as $objectType => $arrObjectTypes) {
					foreach($arrObjectTypes as $sectionType => $arrSectionTypes) {
						foreach($arrSectionTypes as $questionType => $arrQTypeIDs) {
							foreach($arrQTypeIDs as $displayType => $arrQDisplayModes) {
								foreach($arrQDisplayModes as $elementType => $arrElementStyle) {
									if ($elementType <= 0 || !isset($arrElementStyle['style'])) {
										continue;
									}
									
									$strStyle = trim((string) @$arrElementStyle['style']);
									//Si el valor ya no se asignó, significa que se usará el default original, así que se remueve este elemento del array para que se borre de la metadata
									if ($strStyle == "" || $strStyle == "{}") {
										$sql = "DELETE FROM SI_SV_TemplateStyle 
											WHERE TemplateID = {$this->TemplateID} AND ObjectType = {$objectType} AND 
												SectionType = {$sectionType} AND QuestionType = {$questionType} AND 
												DisplayType = {$displayType} AND ElementType = {$elementType}";
										$this->Repository->DataADOConnection->Execute($sql);
										continue;
									}
									
									$sql = "UPDATE SI_SV_TemplateStyle SET 
											Style = ".$this->Repository->DataADOConnection->Quote($strStyle)." 
										WHERE TemplateID = {$this->TemplateID} AND ObjectType = {$objectType} AND 
											SectionType = {$sectionType} AND QuestionType = {$questionType} AND 
											DisplayType = {$displayType} AND ElementType = {$elementType}";
									if (!$this->Repository->DataADOConnection->Execute($sql)) {
										continue;
									}
									
									$intAffectedRows = $this->Repository->DataADOConnection->_affectedrows();
									if ($intAffectedRows > 0) {
										continue;
									}
									
									//Si llega a este punto entonces se debe tratar de un estilo nuevo, así que lo inserta
									$sql = "INSERT INTO SI_SV_TemplateStyle (SettingType, TemplateID, ObjectType, SectionType, QuestionType, DisplayType, ElementType, Style) 
										VALUES (".tsstObject.", {$this->TemplateID}, {$objectType}, {$sectionType}, {$questionType}, {$displayType}, {$elementType}, ".
											$this->Repository->DataADOConnection->Quote($strStyle).")";
									if (!$this->Repository->DataADOConnection->Execute($sql)) {
										continue;
									}
								}
							}
						}
					}
				}
			}
			//@JAPR
		} catch (Exception $e) {
			//No se reportará este error por ahora
		}
	}
	
	function get_Title()
	{
		return translate("Color template definition");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=BITAMEFormsAppCustomStyles&TemplateID={$this->TemplateID}";
	}
	
 	//Basado en las configuraciones de la instancia actual (que debe ser una instancia obtenida mediante la función NewInstanceAll)
 	//genera el JSon de estilo que debe ser enviado al App, el cual se procesará para sobreescribir o generar dinámicamente las clases que se 
 	//requieren en el App para modificar la apariencia
	function getJSonDefinition($desig=false) {
		if ($desig) {	
			$strCalledClass = static::class;
			$arrDef = $strCalledClass::getJSonDefinitionDesign();
			return $arrDef;
		}
		
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		
		$intDefNumStyles = 0;
		$arrDef = array();
		$arrDefByName = array();
		$strStyle = '';
		
		//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
		foreach($this->ArrayStyles as $settingType => $arrSettingTypes) {
			foreach ($arrSettingTypes as $objectType => $arrObjectTypes) {
				foreach ($arrObjectTypes as $sectionType => $arrSectionTypes) {
					foreach ($arrSectionTypes as $questionType => $arrQuestionTypes) {
						foreach ($arrQuestionTypes as $displayType => $arrDisplayTypes) {
							foreach ($arrDisplayTypes as $elementType => $arrElementTypes) {
								$strStyle = (string) @$arrElementTypes['styleStr'];
								if($strStyle != '') {
									$arrTmpStyles = $this->genererateCSSSelector($settingType, $objectType, $sectionType, $questionType, $displayType, $elementType, $strStyle);
									if (count($arrTmpStyles) > 0) {
										foreach ($arrTmpStyles as $arrStyle) {
											$strName = (string) @$arrStyle['name'];
											if (trim($strName) == '') {
												continue;
											}
											
											//Si el estilo no se encontraba definido, entonces simplemente lo agrega, de lo contrario tiene que anexarlo al estilo previamente definido
											if (isset($arrDefByName[$strName])) {
												//Anexa al estilo preexistente la nueva variante (no se soportan casos donde se tenga que reemplazar por ahora)
												$intTmpStyleIdx = $arrDefByName[$strName];
												if (isset($arrDef[$intTmpStyleIdx])) {
													$strTmpStyle = (string) @$arrStyle['style'];
													$arrDef[$intTmpStyleIdx]['style'] .= $strTmpStyle;
												}
											}
											else {
												//No se encontraba, agrega el estilo
												$arrDefByName[$strName] = $intDefNumStyles;
												$arrDef[$intDefNumStyles++] = $arrStyle;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		//@JAPR
		
		return $arrDef;
	}

	//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
	/* Genera la(s) clase(s) y estilo(s) según el objeto y propiedades indicados en los parámetros, ya que puede ser necesario desde una simple clase hasta una colección completa,
	adicionalmente si la configuración está clasificada como global, entra a un proceso especial ya que se genera con selectores diferentes a los preparados por el HTML de eForms (en ese
	case son selectores mas directos hacia JQM)
	*/
	function genererateCSSSelector($SettingType, $ObjectType, $SectionType, $QuestionType, $DisplayType, $ElementType, $Style) {
		global $blnWebMode;
		//@JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
		//Se tuvo que habilitar el DesignMode para la configuración de template, ya que getJSonDefinition en este modo no debe regresar la variable {@IMGFULLPATH} pero si cuando se invoca
		//desde una descarga de definiciones de algún app o browser
		global $gbDesignMode;
		
		$strFullPathVar = "{@IMGFULLPATH}";
		if ($gbDesignMode) {
			$strFullPathVar = "customerimages/".$_SESSION["PABITAM_RepositoryName"];
		}
		//@JAPR
		
		$arrResult = array();
		$arrStyle = array();
		$arrException = $this->generateExceptionArray();
		$arrQTypes = $this->generateQArray(false);
		$styleString = json_decode($Style);
		$CSSString = '';
		
		//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
		//@JAPR 2016-03-11: Agregado el campo SettingType para identificar el tipo de configuración entre directa o global
		$strSpecificSectionTypeSel = ".stp".$SectionType;
		$strSpecificQDisplayModeSel = ".dsp".$DisplayType;
		$strSpecificQTypeIDSel = ".qtp".$QuestionType;
		if ($SettingType == tsstGlobal) {
			$strSpecificSectionTypeSel = "";
			$strSpecificQDisplayModeSel = "";
			//Si en un selector global no se asigna el tipo de pregunta, quiere decir que aplica para todas, así que se hace menos específico removiendo el tipo de pregunta del selector
			if ($QuestionType == -1) {
				$strSpecificQTypeIDSel = "";
			}
		}
		
		//@JAPR
		if ($ObjectType && $ObjectType == otySection) {
			//RV Jun2016: Cuando se da click en el encabezado de la sección tabla los estilos deben aplicarse todos sobre el Th
			//por que la clase inlineTab asiganda a la tabla especifica los estilos a ese nivel y si no se inyecta sobre el th no se ven 
			//reflejados
			if($ElementType==selemHdrTbl)
			{
				$arrStyle['name'] = ".inlineTab th";
			}
			else
			{
				//Los selectores de secciones no requieren tipo de pregunta ni de despliegue
				$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
			}
		}
		else if($ObjectType && $ObjectType == otyQuestion) {
			//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
			$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
		}
		else {
			$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
		}
		
	 	foreach($styleString as $CSSAttribute => $CSSValue) {
	 		$tmpIndex = strpos($CSSAttribute,"_");
	 		$tmpAttrString = substr($CSSAttribute, $tmpIndex + 1);
			if ($tmpIndex === false) {
				continue;
			}

			//Se comprara contra el array de excepciones
	 		if (array_key_exists($tmpAttrString, $arrException)) {
	 			$arrException[$tmpAttrString] = $CSSValue;
	 		}
			else {
				//No hay excepciones, así que se procesa con el selector y valores que trae la configuración
				
				//@JAPR 2016-03-10: Agregados casos especiales que no son propiamente excepciones pues no representan ni complejidad ni múltiples estilos, sino sólo cambios ligeros
				//dependiendo del tipo de objeto o alguna propiedad especial del mismo
				//switch ($ObjectType) {
					/*
					case otySection:
						//Para las secciones en el Header y el Footer no se usará background-color, sino background
						switch ($ElementType) {
							case selemHdrBar:
							case selemFtrBar:
								switch ($tmpAttrString) {
									case "background-color":
										$tmpAttrString = "background";
										break;
								}
								break;
						}
						break;
					*/
					/*case otyQuestion:
						switch ($tmpAttrString) {
							case "width":
								//Los inputs se envuelven en un div ui-input-text cuando los mejora JQM, así que esta configuración se aplica para ese div en lugar del componente directo
								$strSelector = '';
								switch ($QuestionType) {
									case qtpOpenNumeric:
									case qtpOpenDate:
									case qtpOpenAlpha:
									case qtpOpenTime:
									case qtpPassword:
										switch ($ElementType) {
											case qelemQtnElem:
												$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." div.ui-input-text";
												$arrStyle['name'] = $strSelector;
												break;
										}
										break;
									case qtpOpenString:
									default:
										//Utiliza directamente el elemento que se está configurando, así que deja que continue la generación normal
										break;
								}
								break;
						}
						break;
					*/
				//}
				
		 		if (is_numeric($CSSValue)) {
		 			$CSSValue = $CSSValue . "px";
		 		}
		 		$CSSString .= $tmpAttrString. ": " . $CSSValue . " !important; ";
	 		}	
	 	}

		//OMMC 2016-04-05: Cambiados los estilos para los elementos 10
		//Si estamos editando el qelemQtnElem de una pregunta sólo preguntamos por el tipo de pregunta y despliegue.
		//La base del selector siempre es la misma.
		//generateQArray retorna un array de tipos de pregunta contra el que se compara, no tiene caso preguntar por las abiertas.
		if ($ObjectType == otyQuestion && $ElementType == qelemQtnElem && in_array($QuestionType, $arrQTypes)) {

			//Se genera el string base
			$tmpName = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont;

			switch ($QuestionType) {
				case qtpSingle:
					switch ($DisplayType) {
						case dspMenu:
							$tmpName .= " .ui-select div"; 
						break;
						case dspVertical:
						case dspHorizontal:
							$tmpName .= " .ui-radio label"; 
						break;
						case dspAutocomplete:
							$tmpName .= " .ui-combobox input"; 
						break;
						default:
						break;
					}
				break;
				case qtpMulti: 
					switch ($DisplayType) {
						case dspMenu:
							$tmpName .= " .ui-select a"; 
						break;
						case dspVertical:
						case dspHorizontal:
						case dspLabelnum:						
							$tmpName .= " .ui-checkbox label"; 
						break;
						case dspAutocomplete:
							//Falta el complemento
							$tmpName .= " ul.chosen-choices";
						break;
						default:
							//Algún otro despliegue
						break;
					}
				break;
				case qtpPhoto:
				case qtpAudio:
				case qtpVideo:
				case qtpDocument:
				case qtpOCR:
				case qtpSketch:
				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				case qtpSketchPlus:
					//En modo web los botones de las preguntas multimedia se construyen de manera distinta a las de dispositivo.
					//Por lo tanto el selector debe de cambiar.
					$tmpName = $arrStyle['name'].",".$tmpName;
					if ($blnWebMode) {
						$tmpName .= " .qq-upload-button";
					} else {
						$tmpName .= " a";
					}
				break;
				case qtpSkipSection:
				case qtpCallList:
				case qtpSync:
				case qtpExit:
				case qtpUpdateDest:
					$tmpName .= " a";
				break;
				case qtpCalc:
				case qtpBarCode:
					$tmpName .= " .ui-input-text";
				break;
				case qtpSignature:
					//Falta el complemento
				break;
				case qtpMessage:
					$tmpName .= " .ele". qelemQtnElem;
				break;
				default:
					//Algún otro despliegue
				break;
			}

			//Se agrega el CSS adicional para los elementos 10 al array
			$arrStyle['name'] = $tmpName;
		}
		
	 	//Aquí sólo entrará en el caso que el estilo NO sea una excepción, en caso contrario tiene que ir al switch a generar tanto el name como el style para el caso indicado
	 	if ($CSSString != '') {
	 		$arrStyle['style'] = $CSSString;
			$arrResult[] = $arrStyle;
			
			//RV Jun2016: Si es la etiqueta de una pregunta de selección sencilla, los formatos de texto se deben heredar a las etiquetas
			//de los atributos del catálogo
			if($arrStyle['name']==".oty3.qtp2.dsp2 .ele5")
			{
				$arrStyle['name'] = ".oty3.qtp2.dsp2 .ele9 div label";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
			}
			
			//RV Jun2016: Los input de las multiple choice verticales heredarán los estilos de fuente configuradas para las etiquetas de las opciones
			if($arrStyle['name']==".oty3.qtp3.dsp0 .ele9 .ui-checkbox label")
			{	
				//Las opciones de las preguntas de selección multiple con captura heredarán los estilos de texto de las opciones de selección múltiple sin captura
				$arrStyle['name'] = ".oty3.qtp3.dsp0 .ele9 .ui-block-a label";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
				
				//RV Jul2016: Se comentó este código por que los input de las preguntas de selección múltiple con captura ya no heredarán los estilos
				//de texto de las opciones
				/*
				$arrStyle['name'] = ".oty3.qtp3.dsp0 .ele9 .ui-input-text input";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
				*/
			}
			
			//RV Jun2016: Los input de las multiple choice horizontales heredarán los estilos de fuente configuradas para las etiquetas de las opciones
			if($arrStyle['name']==".oty3.qtp3.dsp1 .ele9 .ui-checkbox label")
			{							
				//Las opciones de las preguntas de selección sencilla con captura heredarán los estilos de texto de las opciones de selección sencilla sin captura
				$arrStyle['name'] =".oty3.qtp3.dsp1 .ele9 label";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
				
				//RV Jul2016: Se comentó este código por que los input de las preguntas de selección múltiple con captura ya no heredarán los estilos
				//de texto de las opciones
				/*
				$arrStyle['name'] = ".oty3.qtp3.dsp1 .ele9 .ui-input-text input";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
				*/
			}

			//RV Jul2016: Si es pregunta de tipo selección sencilla ó múltiple y se está configurando la opción seleccionada (qelemQtnOpSelec=16) 
			//Entonces las propiedades del estilo de texto se asignan a la clase ui-btn-active que corresponde a la(s) opciones seleccionadas.
			if($ObjectType==otyQuestion && ($QuestionType==qtpSingle || $QuestionType==qtpMulti) && $ElementType == qelemQtnOpSelec)
			{
				if($DisplayType==dspVertical || $DisplayType==dspHorizontal || $DisplayType==dspLabelnum)
				{
					//Ejemplo: .oty3.qtp3.dsp6 label.ele10.ui-btn-active
					//Se tiene que incluir en el selector el componente label para que se vean reflejados los estilos de letra al momento
					//de seleccionar ya que si no se incluye le gana la clase asiganada a la etiqueta de la opción
					$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." label.ele10.ui-btn-active";
					$arrStyle['style'] = $CSSString;
					$arrResult[] = $arrStyle;
				}
				else if ($DisplayType==dspMetro)
				{
					$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .dhx_list_item.dhx_list_default_item_selected label";
					$arrStyle['style'] = $CSSString;
					$arrResult[] = $arrStyle;
				}
			}
			
			//RV Jul2016: Si es pregunta de tipo selección sencilla y autocomplete
			//entonces las propiedades del estilo de texto se heredan a la lista de opciones que se muestran cuando se escribe en el input
			if($ObjectType==otyQuestion && $QuestionType==qtpSingle && $DisplayType==dspAutocomplete && $ElementType == qelemQtnElem)
			{
				$arrStyle['name'] = ".ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content a";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
			}
			
			//RV Jul2016: Si es pregunta de tipo selección múltiple y ranking 
			//entonces las propiedades del estilo de texto se heredan a la clase rankBulletSelected que corresponde a los
			//números mostrados cuando se selecciona una opción
			if($ObjectType==otyQuestion && $QuestionType==qtpMulti && $DisplayType==dspLabelnum && $ElementType == qelemQtnElem)
			{
				$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .ele".qelemQtnCont." .rankBulletSelected";
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
				
				//RV Jul2016: El color del borde del número será el mismo color que el texto
				$borderColor="";
				$styleProps = explode(";",$CSSString);
				foreach($styleProps as $aProp)
				{
					if(strstr($aProp, "color")!==0)
					{	//Ejemplo de $aProp= color: #0b00e6 !important;
						$borderColor = str_replace("color:", "", $aProp);
						break;
					}
				}
				
				if($borderColor!="")
				{
					$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .ele".qelemQtnCont." .rankBulletSelected";
					$arrStyle['style'] = "border:2px solid ".$borderColor;
					$arrResult[] = $arrStyle;
					
					//Cuando la pregunta ranking está dentro de una tabla inline se maneja otra clase
					$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .ele".qelemQtnCont." .rankBulletSelectedInline";
					$arrStyle['style'] = "border:2px solid ".$borderColor;
					$arrResult[] = $arrStyle;
				}
			}
			
			//RV Jul2016: Si es pregunta de tipo selección sencilla y metro
			//entonces las propiedades del estilo de texto se aplican sobre las siguientes clases
			if($ObjectType==otyQuestion && $QuestionType==qtpSingle && $DisplayType==dspMetro && $ElementType == qelemQtnMetroMainAttr)
			{
				$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .metroMainAttr" ;
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
			}
			
			//RV Jul2016: Si es pregunta de tipo selección sencilla y metro
			//entonces las propiedades del estilo de texto se aplican sobre las siguientes clases
			if($ObjectType==otyQuestion && $QuestionType==qtpSingle && $DisplayType==dspMetro && $ElementType == qelemQtnMetroExtraAttr)
			{
				$arrStyle['name'] =".oty".otyQuestion.".qtp".$QuestionType.".dsp".$DisplayType." .metroExtraAttr" ;
				$arrStyle['style'] = $CSSString;
				$arrResult[] = $arrStyle;
			}
			
			if($ObjectType && $ObjectType == otySection)
			{
				switch($ElementType)
				{
					case selemTotalTbl:
						$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." label";
						$arrStyle['style'] = $CSSString;
						$arrResult[] = $arrStyle;
						break;
					//RV Jun2016: En sección tabla la etiquieta de la primera columna se configurará al seleccionar la fila
					//por eso se agrega este selector para considerar la etiqueta
					case selemRowOdd:
					case selemRowEven:
						$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." label";
						$arrStyle['style'] = $CSSString;
						$arrResult[] = $arrStyle;
						break;
					default:
						break;
				}
			}
	 	}
		
		//Se debe recorrer el array de excepciones para encontrar cual fue la que se generó.
		//Por la forma en la que se graban las propiedades, sólo se podrá encontrar una excepción a la vez y sólo podrá regresar un estilo.
		foreach($arrException as $arrProperty => $arrPropertyValue) {
			if (is_null($arrPropertyValue)) {
				continue;
			}
			
			if ($arrPropertyValue != '') {
				switch ($arrProperty) {
					//RV Mayo2016: Agregar excepción para el color de fondo por que en la sección debe aplicarse sobre el page y no sobre el body
					case 'background-color':
						switch ($ObjectType) {
							case otySection:
								//Para las secciones en el Header y el Footer no se usará background-color, sino background
								switch ($ElementType) {
									case selemHdrBar:
									case selemFtrBar:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;									
										$arrStyle['style'] = "background:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
									case  selemBdy:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemPage;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;

										//Al inyectar este estilo, se debe modificar el estilo del ui-body-a porque ese tiene un color default, así que si no se cambia no se verá la imagen
										$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemBdy.".ui-body-a";
										$arrStyle['name'] = $strSelector;
										$arrStyle['style'] = "background-color: transparent !important;";
										$arrResult[] = $arrStyle;
										
										break;
									case  selemHdrTbl:
										//$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." th";
										$arrStyle['name'] = ".inlineTab th";
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
									case selemRowOdd:
									case selemRowEven:
										$arrStyle['name'] = ".inlineTab .ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
									case  selemTotalTbl:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
									case  selemSummary:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
									case selemAddBtn:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										//El div que encierra los botones Add, Remove y Finish también se pondrá del mismo color de los botones
										$arrStyle['name'] = ".oty".$ObjectType." .ele".selemGroupBtn." .ui-controlgroup-controls";
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										
										break;
									default:
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
								}
								break;
							case otyQuestion:
								switch ($QuestionType) {
									
									case qtpMulti:
										switch ($DisplayType) {
											
											case dspMenu:
												//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
												if($ElementType==qelemQtnElem)
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." a";
												}
												else
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
												}
												$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												$arrResult[] = $arrStyle;
												break;
											
											case dspVertical:
											case dspHorizontal:
											case dspLabelnum:
												//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
												if($ElementType==qelemQtnElem)
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele9 label";
												}
												else
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
												}
												$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												$arrResult[] = $arrStyle;
												break;
											case dspAutocomplete:
												//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
												if($ElementType==qelemQtnElem)
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." ul";
													$arrStyle['style'] = "background-color:{$arrPropertyValue} !important; background-image:none !important;";
												}
												else
												{
													$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
													$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												}
												$arrResult[] = $arrStyle;
												break;
											default:
												//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
												$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
												$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												$arrResult[] = $arrStyle;
												break;
										}
										
										//RV Jul2016:
										//Para las preguntas de selección múltiple se tiene que agregar esta clase para que los elemenos seleccionados tengan el color default
										if($ElementType==qelemQtnElem && ($DisplayType==dspHorizontal || $DisplayType==dspVertical || $DisplayType==dspLabelnum))
										{
											$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType.".ui-btn-active";
											$arrStyle['style'] = "background-color:#3388cc !important;";
											$arrResult[] = $arrStyle;
										}
										//Si se configuro el elemento qelemQtnOpSelec entonces ese color de fondo se asigna a la clase ui-btn-active
										//que corresponde a la opción seleccionada
										if($ElementType==qelemQtnOpSelec && ($DisplayType==dspHorizontal || $DisplayType==dspVertical || $DisplayType==dspLabelnum))
										{
											$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnElem.".ui-btn-active";
											$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
											$arrResult[] = $arrStyle;
										}
										break;
									case qtpSingle:
										//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
										//Para preguntas simple choice autocomplete es necesario quitar la imagen establecida por default para que se despliegue
										//el color de fondo en el area de respuesta
										if($DisplayType==dspAutocomplete && $ElementType==qelemQtnElem)
										{
											//El background debe aplicar sobre el div contenedor para que también pinte el fondo de la imagen
											$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." .inputautoCont";
											$arrStyle['style'] = "background-color:{$arrPropertyValue} !important; background-image:none !important;";
										}
										else
										{
											$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										}
										$arrResult[] = $arrStyle;
										
										if($DisplayType==dspMetro && $ElementType==qelemQtnElem)
										{
											$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .dhx_list_item.dhx_list_default_item";
											$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
											$arrResult[] = $arrStyle;
										}
										
										//RV Jul2016:
										//Para las preguntas de selección sencilla autocompletado se tiene que agregar una clase para que el background-color
										//se aplique también en las opciones que se despliegan al escribir
										if($ElementType==qelemQtnElem && $DisplayType==dspAutocomplete)
										{
											$arrStyle['name'] = ".ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content";
											$arrStyle['style'] = "background-color:{$arrPropertyValue} !important; background:'' !important; background-image:none !important;";
											$arrResult[] = $arrStyle;
										}
										
										//RV Jul2016:
										//Para las preguntas de selección sencilla se tiene que agregar esta clase para que los elemenos seleccionados 
										//tengan el color default
										if($ElementType==qelemQtnElem && ($DisplayType==dspHorizontal || $DisplayType==dspVertical))
										{
											$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType.".ui-btn-active";
											$arrStyle['style'] = "background-color:#3388cc !important;";
											$arrResult[] = $arrStyle;
										}
										//Si se configuro el elemento qelemQtnOpSelec entonces ese color de fondo de asigna a la clase ui-btn-active
										//que corresponde a la opción seleccionada
										if($ElementType==qelemQtnOpSelec)
										{
											if($DisplayType==dspHorizontal || $DisplayType==dspVertical)
											{
												$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnElem.".ui-btn-active";
												$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												$arrResult[] = $arrStyle;
											}
											else if($DisplayType==dspMetro)
											{
												$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .dhx_list_item.dhx_list_default_item_selected";
												$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
												$arrResult[] = $arrStyle;
											}
										}
										break;
									default:
										//Los selectores de las preguntas podrían querer aplicar estilos a un elemento específico
										$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
										$arrStyle['style'] = "background-color:{$arrPropertyValue} !important;";
										$arrResult[] = $arrStyle;
										break;
								}
								break;
							default:
								break;
						}
						break;
					//RV Mayo2016: Agregar excepción para las preguntas tipo mapa por que la altura no se estaba aplicando correctamente.
					case 'height':
						if ( $QuestionType==qtpSingle && $DisplayType=dspMap) {
							$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." .singleChoiceMap";
							$arrStyle['name'] = $strSelector;
							$arrStyle['style'] = "height:{$arrPropertyValue};";
							$arrResult[] = $arrStyle;
						}
						else {
							$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
							$arrStyle['name'] = $strSelector;
							$arrStyle['style'] = "height:{$arrPropertyValue};";
							$arrResult[] = $arrStyle;
						}
						break;
					//RV Mayo2016: Agregar la excepción para background-image en el body de la sección
					case 'background-image':
						//@JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
						//Dado a que esto se descargará a Apps de dispositivos, se necesita utilizar la referencia a la variable {@IMGPATH}
						switch ($ElementType) {
							case selemBdy:
								//El body no se aplicará realmente sobre el body sino sobre el page, ya que el body crece según su contenido así que la imagen no sería consistente
								//en tamaño entre diferentes páginas de secciones
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemPage;
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-image:url('".$strFullPathVar."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								//$arrStyle['style'] = "background-image:url('customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								$arrResult[] = $arrStyle;
								
								//Al inyectar este estilo, se debe modificar el estilo del ui-body-a porque ese tiene un color default, así que si no se cambia no se verá la imagen
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemBdy.".ui-body-a";
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-color: transparent !important;";
								$arrResult[] = $arrStyle;
								break;
							
							case selemLeftBtn:
								//Los botones de next y de back se deben inyectar en pares para aplicar por igual al header y footer
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemHdrBar." .ele".selemLeftBtn;
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-image:url('".$strFullPathVar."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								$arrResult[] = $arrStyle;
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemFtrBar." .ele".selemLeftBtn;
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-image:url('".$strFullPathVar."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								$arrResult[] = $arrStyle;
								break;
							
							case selemRigthBtn:
								//Los botones de next y de back se deben inyectar en pares para aplicar por igual al header y footer
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemHdrBar." .ele".selemRigthBtn;
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-image:url('".$strFullPathVar."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								$arrResult[] = $arrStyle;
								$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".selemFtrBar." .ele".selemRigthBtn;
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "background-image:url('".$strFullPathVar."/".$arrPropertyValue."') !important; background-repeat: no-repeat !important; background-color: transparent !important; background-size: contain !important;";
								$arrResult[] = $arrStyle;
								break;
						}
						break;
					case 'border':
						//Si se recibe la propiedad border, se debe tratar como una combinación de bits 0|1 en la posición "left top right bottom" separados por "", de tal manera que
						//se pintarán los bordes recibidos de manera independiente. Si se recibieron todos (1 1 1 1) entonces simplemente se usará la propiedad border-width, si se 
						//recibieron separados (ej. 1 0 1 0) se usarán de manera independiente para asignar un width a cada parte del borde, si no se recibió alguno (0 0 0 0) entonces no
						//colocará borde (border-width: 0px), y si se recibió vacío entonces no realizará proceso alguno pues quiere decir que usará la configuración default de borde del componente 
						//en cuestión (border-width), por tanto teóricamente todos los bordes o recibir vacío debería ser lo mismo, pero en el primer caso se forza a agregar border-width,
						//mientras que en el segundo depende de que dicha propiedad se hubiera configurado independientemente
						if (trim($arrPropertyValue) != '') {
							$arrBorders = explode("_", $arrPropertyValue);
							//Debe haber un mínimo de 1 (Left), complementa los que falten
							if (!isset($arrBorders[1])) $arrBorders[1] = 0;	//Top
							if (!isset($arrBorders[2])) $arrBorders[2] = 0;	//Right
							if (!isset($arrBorders[3])) $arrBorders[3] = 0;	//Bottom
							
							$intTotal = 0;
							$arrBorderPos = array("left", "top", "right", "bottom");
							foreach($arrBorders as $intIndex => $strValue) {
								$intValue = (((int) $strValue) != 0)?1:0;
								$arrBorders[$intIndex] = $intValue;
								$intTotal += $intValue;
							}
							
							$intBorderWidth = (int) @$arrException['border-width'];
							//Si no se había especificado un width, usará por default 1px sólo para no dar la impresión de que esta configuración no funcionó, aunque en realidad
							//habría dependido de recibir el width por parte del usuario (hacer esto literalmente sobreescribiría cualquier default del usuario)
							if ($intBorderWidth <= 0) {
								$intBorderWidth = 1;
							}
							
							$intElementType = $ElementType;
							//En el caso de los TRs, no se debe asignar el borde en ellos sino en su TD, así que se cambia el element a utilizar
							switch ($intElementType) {
								case qelemLabTR:
									$intElementType = qelemLabTD;
									break;
								case qelemQtnTR:
									$intElementType = qelemQtnTD;
									break;
							}
							
							$strSelector = "";
							if ($ObjectType && $ObjectType == otySection) {
								//RV Jun2016: Aplicar borde en celdas cuando se trata de elementos de Sección Tabla
								switch ($ElementType) {
									case selemHdrTbl:
										//$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." th";
										$strSelector = ".inlineTab th";
										break;
									case selemRowOdd:
									case selemRowEven:
										$strSelector = "table.inlineTab .ele".$ElementType." td:not(.tdCont)";
										break;
									case selemTotalTbl:
										$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." td";
										break;
									default:
										$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										break;
								}
							}
							else {
								switch ($ElementType) {
									case qelemQtnElem:
										switch ($QuestionType) {
											case qtpOpenNumeric:
											case qtpOpenDate:
											case qtpOpenAlpha:
											case qtpOpenTime:
											case qtpPassword:
												//echo("<br>\r\n Entro a ui-input por $arrProperty");
												$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." div.ui-input-text";
												break;
											case qtpOpenString:
											default:
												//Utiliza directamente el elemento que se está configurando
												$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$intElementType;
												break;
										}
										break;
									default:
										//Utiliza directamente el elemento que se está configurando
										$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$intElementType;
										break;
								}
							}
							
							//RV Mayo2016: Cuando es pregunta Simple Choice el borde se tiene que aplicar al div que contiene el span
							switch ($QuestionType)
							{
								case qtpSingle:
								{
									switch ($intElementType)
									{
										case qelemQtnElem:
										{
											//RV Jul2016: Cuando es pregunta Simple Choice el selector para aplicar el borde depende del tipo de despliegue
											switch ($DisplayType)
											{
												case dspMenu:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select div';
													break;
												case dspVertical:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
													break;
												case dspHorizontal:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
													break;
												case dspAutocomplete:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .inputautoCont';
													break;
												case dspMetro:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .dhx_list_item.dhx_list_default_item";
													break;
													
												default:
													break;
											}
											break;
										}
										default:
											break;
									}
									break;
								}
								case qtpMulti:
								{
									switch ($intElementType)
									{
										case qelemQtnElem:
										{
											//RV Jul2016: Cuando es pregunta Multiple Choice el selector para aplicar el borde depende del tipo de despliegue
											switch ($DisplayType)
											{
												case dspMenu:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select a';
													break;
												case dspVertical:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspHorizontal:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspLabelnum:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspAutocomplete:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .chosen-container';
													break;
												default:
													break;
											}
											break;
										}
										default:
											break;
									}
									break;
								}
								
								default:
									break;
							}
							
							switch ($intTotal) {
								case 0:
									//No hay borde
									$arrStyle['name'] = $strSelector;
									$arrStyle['style'] = "border-width:0px !important;";
									$arrResult[] = $arrStyle;
									break;
							
								case 4:
									//Se pidieron todos los bordes, así que genera la propiedad de manera natural
									$arrStyle['name'] = $strSelector;
									$arrStyle['style'] = "border-width:{$intBorderWidth}px !important;";
									$arrResult[] = $arrStyle;
									break;
									
								default:
									//En este caso genera el borde para cada una de las partes que se recibieron
									foreach($arrBorders as $intIndex => $intValue) {
										if ($intValue) {
											$strPosition = (string) @$arrBorderPos[$intIndex];
											$arrStyle['name'] = $strSelector;
											$arrStyle['style'] = "border-{$strPosition}-width:{$intBorderWidth}px !important;";
											$arrResult[] = $arrStyle;
										}
										else {
											$strPosition = (string) @$arrBorderPos[$intIndex];
											$arrStyle['name'] = $strSelector;
											$arrStyle['style'] = "border-{$strPosition}-width:0px !important;";
											$arrResult[] = $arrStyle;
										}
									}
									break;
							}
							
							//Si no hay especificados ni estilo ni color de borde, agregará uno automáticamente para que se permita su despliegue (esto puede provocar que algunos estilos
							//default de JQM se sobreescriban, causando que ciertos componentes se vean mal, así que se tiene que validar elemento por elemento según se vayan encontrando
							//los casos en QA
							//Si estas propiedades si están asignadas, entonces se dejará que se generen de manera normal
							$strBorderStyle = trim((string) @$arrException['border-style']);
							if ($strBorderStyle == '') {
								$strBorderStyle = 'border-style: solid;';
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = $strBorderStyle;
								$arrResult[] = $arrStyle;
							}
							
							$strBorderColor = trim((string) @$arrException['border-color']);
							if ($strBorderColor == '') {
								$strBorderColor = 'border-color: #000000;';
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = $strBorderColor;
								$arrResult[] = $arrStyle;
							}
						}
						break;
						
					case 'border-width':
						//Border-width será ignorado completamente excepto en el caso que no exista la propiedad border, ya que si existe, se debe usar border como prioridad
						//Sólo no podría existir border, si se hubiera seleccionado la opción default para dicha configuración
						$strSelector = '';
						
						if (!isset($arrException['border'])) {
							if ($ObjectType && $ObjectType == otySection) {
								//RV Jun2016: Aplicar borde en celdas cuando se trata de elementos de Sección Tabla
								switch ($ElementType) {
									case selemHdrTbl:
										//$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." th";
										$strSelector = ".inlineTab th";
										break;
									case selemRowOdd:
									case selemRowEven:
										$strSelector = "table.inlineTab .ele".$ElementType." td:not(.tdCont)";
										break;
									case selemTotalTbl:
										$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." td";
										break;
									default:
										$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
										break;
								}
							}
							else {
								switch ($ElementType) {
									case qelemQtnElem:
										switch ($QuestionType) {
											case qtpOpenNumeric:
											case qtpOpenDate:
											case qtpOpenAlpha:
											case qtpOpenTime:
											case qtpPassword:
												$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." div.ui-input-text";
												break;
											case qtpSingle:
											{	
												//RV Jul2016: Cuando es pregunta Simple Choice el selector para aplicar el borde depende del tipo de despliegue
												switch ($DisplayType)
												{
													case dspMenu:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select div';
														break;
													case dspVertical:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
														break;
													case dspHorizontal:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
														break;
													case dspAutocomplete:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .inputautoCont';
														break;
													case dspMetro:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .dhx_list_item.dhx_list_default_item";
														break;
													default:
														break;
												}
												break;
											}
											case qtpMulti:
											{
												//RV Jul2016: Cuando es pregunta Multiple Choice el selector para aplicar el borde depende del tipo de despliegue
												switch ($DisplayType)
												{
													case dspMenu:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select a';
														break;
													case dspVertical:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
														break;
													case dspHorizontal:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
														break;
													case dspLabelnum:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
														break;
													case dspAutocomplete:
														$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .chosen-container';
														break;
													default:
														break;
												}
												break;
											}
											case qtpOpenString:
											default:
												//Utiliza directamente el elemento que se está configurando
												$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
												break;
										}
										break;
									default:
										//Utiliza directamente el elemento que se está configurando
										$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
										break;
								}
							}
							$arrStyle['name'] = $strSelector;
							$arrStyle['style'] = "{$arrProperty}:{$arrPropertyValue} !important;";
							$arrResult[] = $arrStyle;
						}
						break;
						
					case 'horizontal-align':
						//Sólo aplica a preguntas
						//Si el elemento es un input, se debe forzar el display a block, ya que el default inline-block en JQM 1.45 provoca que el margin no lo afecte
						$strAdditionalStyles = "";
						if ($ElementType == qelemQtnElem) {
							$strAdditionalStyles .= "display:block !important;";
						}
						
						//Los inputs se envuelven en un div ui-input-text cuando los mejora JQM, así que esta configuración se aplica para ese div en lugar del componente directo
						$strSelector = '';
						switch ($QuestionType) {
							case qtpOpenNumeric:
							case qtpOpenDate:
							case qtpOpenAlpha:
							case qtpOpenTime:
							case qtpPassword:
								switch ($ElementType) {
									case qelemQtnElem:
										$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." div.ui-input-text";
										break;
									default:
										//Utiliza directamente el elemento que se está configurando
										$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
										break;
								}
								break;
							case qtpOpenString:
							default:
								//Utiliza directamente el elemento que se está configurando
								$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
								break;
						}
						switch ($arrPropertyValue) {
							case "right":
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "margin-left:auto !important;{$strAdditionalStyles}";
								$arrResult[] = $arrStyle;
								break;
							case "center":
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "margin-left:auto !important;margin-right:auto !important;{$strAdditionalStyles}";
								$arrResult[] = $arrStyle;
								break;
							case "left":
								$arrStyle['name'] = $strSelector;
								$arrStyle['style'] = "margin-right:auto !important;{$strAdditionalStyles}";
								$arrResult[] = $arrStyle;
								break;
							default:
								break;
						}
						break;
						
					case 'layout':
						//Sólo aplica a preguntas
						$arrStyle = array();
						//@JAPR 2016-03-02: Modificado el layout para envolver a los divs contenedores en una tabla mas y permitir alinear verticalmente, ahora hay 2 tablas (una para
						//etiqueta y otra para respuesta) así que ya se puede aplicar floats para acomodarlas como se necesite en izquierda/derecha u otras posiciones, sin afectar a
						//la alineación vertical (ya que los floats la cancelaban) y sin el raro efecto de los display table y table-cell que forzaban a usar divs dummies y extraños
						//procesos de carga
						switch ($arrPropertyValue) {
							case qlayLeftRight:
								$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemLabTab;
								$arrStyle['style'] = "float:left;";
								$arrResult[] = $arrStyle;
								$arrStyle['name'] = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnTab;
								$arrStyle['style'] = "float:right;";
								$arrResult[] = $arrStyle;
								break;
						}
						break;
						
					case 'width':
						//width se agrega porque los inputs se envuelven en un div ui-input-text cuando los mejora JQM, así que esta configuración se aplica para ese div en lugar del
						//componente directo
					case 'obj_border-style':
					case 'obj_border-color':
					default:
						//border-style y border-color se agregan a la lista de excepciones para permitir controlar si se usa o no directamente, ya que al no haber un border-style pero si 
						//un border, es necesario asignar automáticamente un borde bajo ciertas circunstancias (algunos elementos lo traen por default, otros no), así que se coloca en este 
						//punto para que simplemente se agregue como todas las demás propiedades sin restricciones
						$strSelector = '';
						if ($ObjectType && $ObjectType == otySection) {
							//RV Jun2016: Aplicar borde en celdas cuando se trata de elementos de Sección Tabla
							switch ($ElementType) {
								case selemHdrTbl:
									//$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." th";
									$strSelector = ".inlineTab th";
									break;
								case selemRowOdd:
								case selemRowEven:
									$strSelector = "table.inlineTab .ele".$ElementType." td:not(.tdCont)";
									break;
								case selemTotalTbl:
									$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." td";
									break;
								default:
									$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType;
									break;
							}
						}
						else {
							switch ($ElementType) {
								case qelemQtnElem:
									switch ($QuestionType) {
										case qtpOpenNumeric:
										case qtpOpenDate:
										case qtpOpenAlpha:
										case qtpOpenTime:
										case qtpPassword:
											$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont." div.ui-input-text";
											break;
										//RV Jul2016: Cuando es pregunta Simple Choice el selector para aplicar el borde depende del tipo de despliegue
										case qtpSingle:
										{
											switch ($DisplayType)
											{
												case dspMenu:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select div';
													break;
												case dspVertical:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
													break;
												case dspHorizontal:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-radio label';
													break;
												case dspAutocomplete:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .inputautoCont';
													break;
												case dspMetro:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .dhx_list_item.dhx_list_default_item";
													break;
												default:
													break;
											}
											break;
										}
										case qtpMulti:
										{
											//RV Jul2016: Cuando es pregunta Multiple Choice el selector para aplicar el borde depende del tipo de despliegue
											switch ($DisplayType)
											{
												case dspMenu:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-select a';
													break;
												case dspVertical:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspHorizontal:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspLabelnum:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .ui-checkbox label';
													break;
												case dspAutocomplete:
													$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".qelemQtnCont.' .chosen-container';
													break;
												default:
													break;
											}
											break;
										}
										case qtpOpenString:
										default:
											//Utiliza directamente el elemento que se está configurando
											$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
											break;
									}
									break;
								default:
									//Utiliza directamente el elemento que se está configurando
									$strSelector = ".oty".$ObjectType.$strSpecificQTypeIDSel.$strSpecificQDisplayModeSel." .ele".$ElementType;
									break;
							}
						}
						$arrStyle['name'] = $strSelector;
						$arrStyle['style'] = "{$arrProperty}:{$arrPropertyValue} !important;";
						$arrResult[] = $arrStyle;
						
						//RV Jun2016: En la sección maestro detalle, el color de borde que se asigne al div del summary 
						//se heredará al color de la linea que divide las filas
						if($arrProperty=="border-color" && $ObjectType && $ObjectType == otySection && $ElementType==selemSummary)
						{	
							$strSelector = ".oty".$ObjectType.$strSpecificSectionTypeSel.".ele".$ElementType." ul li a";
							$arrStyle['name'] = $strSelector;
							$arrStyle['style'] = "border-bottom: 1px solid {$arrPropertyValue} !important;";
							$arrResult[] = $arrStyle;
						}
						break;
				}
			}
		}
	 	return $arrResult;
	}
	
	/* Genera un array de configuraciones que no se deben generar como clases directamente, ya que deben recibir un tratamiento especial ya sea porque implican diferentes estilos
	de una misma clase o diferentes clases para diferentes componentes, etc.
	*/
	function generateExceptionArray() {
		$arrException = array();
		$arrException['border'] = '';
		$arrException['border-color'] = '';
		$arrException['border-style'] = '';
		$arrException['border-width'] = '';
		$arrException['horizontal-align'] = '';
		$arrException['layout'] = '';
		$arrException['width'] = '';
		$arrException['height'] = '';
		$arrException['background-image'] = '';
		$arrException['background-color'] = '';
		
		return $arrException;
	}

	//OMMC 2016-05-04: Regresa un array de INT's representando la numeración de preguntas
	function generateQArray($blnOpen = true, $blnSingle = true, $blnMulti = true, $blnMedia = true, $blnOther = true) {

		$arrQuestions = array();

		if ($blnOpen) { array_push($arrQuestions, qtpOpenNumeric, qtpOpenString, qtpOpenAlpha, qtpOpenDate, qtpOpenTime, qtpPassword); }
		if ($blnSingle) { array_push($arrQuestions, qtpSingle); }
		if ($blnMulti) { array_push($arrQuestions, qtpMulti);	}
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ($blnMedia) { array_push($arrQuestions, qtpPhoto, qtpAudio, qtpVideo, qtpSketch, qtpDocument, qtpOCR, qtpSketchPlus); }		
		//@JAPR
		if ($blnOther) { array_push($arrQuestions, qtpSkipSection, qtpCalc, qtpSignature, qtpMessage, qtpCallList,  qtpBarCode, qtpGPS, qtpSync, qtpExit, qtpUpdateDest); }	

		return $arrQuestions;
	}
}
?>