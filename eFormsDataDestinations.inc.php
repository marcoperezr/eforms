<?php

require_once("survey.inc.php");

class BITAMeFormsDataDestinations extends BITAMObject 
{

	/**
	 * Id de Data Destination
	 * @var integer
	 */
	public $id;

	/**
	 * Id de la forma en eForms
	 * @var integer
	 */
	public $id_survey;

	/**
	 * Nombre de data destination definido por el usuario
	 * @var string
	 */
	public $name;

	/** @var boolean Indica si el envio de los datos al destino estan activos */
	public $active;

	/** @var string Condicion con los campos de eForms para enviar los datos al destino */
	public $condition;

	/** @var array Guarda la definicion del mapeo de preguntas de eForms a campos de eBavel */
	public $mapping;

	/** @var int Tipo de conexion, 1 => eBavel */
	public $type;

	/** @var string Nombre codigo de la forma de eBavel */
	public $eBavelForm;

	/** @var string Ruta del FTP */
	public $ftpServer;

	/** @var string Nombre de usuario para el FTP */
	public $ftpUsername;

	/** @var string Password para el ftp */
	public $ftpPassword;

	/** @var string Token de conexion para servicios de terceros */
	public $access_token;

	/** @var string Email para los servicios de terceros */
	public $email;

	/** @var string Guarda la informacion de la conexion, si esta disponible */
	public $connection;

	/** @var int Id de la conexion */
	public $id_connection = 0;

	/** @var int Tipo de formato a Exportar: 1=> CSV, 2 => Excel, 3 => PDF */
	public $fileformat = 0;

	/** @var string Ruta donde se guardara el archivo en el destino */
	public $path = '';

	/** @var int Tipo de formato a Exportar 3 => PDF, 4=> Dashboard, 5 => ARS */
	public $emailformat = ddeftNone;

	/** @var int dimension a la cual esta ligada la survey*/
	public $cla_dimension = 0;

	/** @var int dimension a la cual esta ligada la survey*/
	public $cla_dashboar = 0;

	/** @var int dimension a la cual esta ligada la survey*/
	public $cla_ars = 0;
	//@JRPP 2016-01-11: Variable definida para los parametros guardados de las conecciones de tipo HTTP
	public $Parameters = "";

	//GCRUZ 2016-01-15. Agregada propiedad de destino incremental
	public $isIncremental = false;
	//@JRPP 2016-01-28 Se agrega el campo AditionaleMAil para mandar un correo adicional a esta cuenta o cuentas de correo
	public $additionaleMail = "";
	//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
	public $httpRequest = ddrtPOST;
	//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
	public $httpSendSinglePOST = false;
	//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
	/** @var string Asunto del EMail personalizado (puede contener referencias de variables) */
	public $emailsubject;
	/** @var string Cuerpo del EMail personalizado (puede contener referencias de variables y ser HTML) */
	public $emailbody;
	//@JAPR
	//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
	public $additionaleMailComposite = "";
	
	public static $query = "SELECT id, id_survey, `name`, `active`, `condition`, `type`, `eBavelForm`, `id_connection`, `fileformat`, `path`, `real_path`";
	
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		return eval('return new '.$strCalledClass.'($aRepository);');
	}
	public static function getQuery () {
		$sQuery = self::$query;
		//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		//Modificada la manera de armar el query para no repetir tantas veces la misma porción, sino que dependiendo de la versión se agregarán nuevos campos
		if (getMDVersion()>=esvDataDestination) {
			$sQuery = $sQuery . ", `emailformat`, `cla_dimension` , cla_dashboar, cla_ars";
		}
		if (getMDVersion()>=esvParameterHTTP) {
			//@JRPP 2016-01-11: Variable definida para los parametros guardados de las conecciones de tipo HTTP
			$sQuery = $sQuery . ", parameters";
		}
		if (getMDVersion()>=esvDestinationIncremental) {
			//GCRUZ 2016-01-15. Agregada propiedad de destino incremental
			$sQuery = $sQuery . ", isIncremental";
		}
		if (getMDVersion()>=esvadditionaleMail) {
			//@JRPP 2016-01-28. Agregada propiedad de correo adicional
			$sQuery = $sQuery . ", additionaleMail";
		}
		if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
			//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
			$sQuery = $sQuery . ", httpSendSinglePOST";
		}
		//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		if (getMDVersion()>=esvDataDestEmailDetail) {
			$sQuery = $sQuery . ", emailsubject, emailbody";
		}
		//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
		if (getMDVersion()>=esvDataDestEmailComposite) {
			$sQuery = $sQuery . ", additionaleMailComposite";
		}
		
		//Independientemente de los campos a agregar, siempre se consulta la misma tabla
		$sQuery = $sQuery . " FROM si_sv_datadestinations";
		//@JAPR
		return $sQuery;
	}
	static function NewInstanceWithId( $aRepository, $id )
	{
		$strCalledClass = static::class;
		
		//$rs = $aRepository->DataADOConnection->Execute( self::$query . " WHERE id = " . $id );
		$rs = $aRepository->DataADOConnection->Execute( self::getQuery() . " WHERE id = " . $id );

		if( $rs && !$rs->EOF )
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $rs);	

			return $anInstance;
		}

		return false;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		
		$anInstance = $strCalledClass::NewInstance($aRepository);
		
		$anInstance->id = (int) $aRS->fields["id"];
		
		$anInstance->id_survey = $aRS->fields["id_survey"];
		
		$anInstance->name = $aRS->fields["name"];
		
		$anInstance->active = $aRS->fields["active"] == 1;
		
		$anInstance->condition = $aRS->fields["condition"];
		
		$anInstance->type = $aRS->fields["type"];
		
		$anInstance->eBavelForm = $aRS->fields["eBavelForm"];
		
		$anInstance->id_connection = $aRS->fields["id_connection"];
		
		$anInstance->fileformat = $aRS->fields["fileformat"];
		
		$anInstance->path = $aRS->fields["path"];
		
		$anInstance->realPath=$aRS->fields["real_path"];
		
		//@JRPP 2015-12-02 : Se agregan los campos a la clase 
		if (getMDVersion()>=esvDataDestination) {
			
			$anInstance->emailformat = @$aRS->fields["emailformat"];
			
			$anInstance->cla_dimension = @$aRS->fields["cla_dimension"];
			
			$anInstance->cla_dashboar = @$aRS->fields["cla_dashboar"];
			
			$anInstance->cla_ars = @$aRS->fields["cla_ars"];
			
		}
		if (getMDVersion()>=esvParameterHTTP) {
			//@JRPP 2016-01-11: Variable definida para los parametros guardados de las conecciones de tipo HTTP
			$anInstance->Parameters = @$aRS->fields["parameters"];
		}
		if (getMDVersion()>=esvDestinationIncremental) {
			//GCRUZ 2016-01-15. Agregada propiedad de destino incremental
			$isIncrementalValue = (isset($aRS->fields["isIncremental"]))? intval(@$aRS->fields["isIncremental"]) : 0;
			$anInstance->isIncremental =  ($isIncrementalValue == 0)? false : true;
		}
		if (getMDVersion()>=esvadditionaleMail) {
			//@JRPP 2016-01-28: Agregada propiedad de correo adicional
			$anInstance->additionaleMail =  @$aRS->fields["additionalemail"];
		}
		//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
		if (getMDVersion()>=esvDataDestEmailComposite) {
			$anInstance->additionaleMailComposite =  @$aRS->fields["additionaleMailComposite"];
		}
		if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
			//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
			$isHttpSendSinglePOST = (isset($aRS->fields["httpsendsinglepost"]))? intval(@$aRS->fields["httpsendsinglepost"]) : 0;
			$anInstance->httpSendSinglePOST =  ($isHttpSendSinglePOST == 0)? false : true;
		}
		//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		$anInstance->emailsubject = (string) @$aRS->fields["emailsubject"];
		$anInstance->emailbody = (string) @$aRS->fields["emailbody"];
		//@JAPR
		
		/** cargamos el mapeo */
		//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$strAdditionalFields = '';
		if (getMDVersion()>=esvSketchPlus) {
			$strAdditionalFields = ", additional_data";
		}
		$rs = $aRepository->DataADOConnection->Execute( "SELECT id_question, fieldformid, formula{$strAdditionalFields} FROM si_sv_datadestinationsmapping WHERE id_datadestinations = " . $anInstance->id );
		//@JAPR
		while( $rs && !$rs->EOF )
		{
			//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			$anInstance->mapping[] = array( 'questionid' => $rs->fields['id_question'], 'fieldformid' => $rs->fields['fieldformid'], 'formula' => $rs->fields["formula"], 'additionalData' => (int) @$rs->fields["additional_data"] );
			//@JAPR
			$rs->MoveNext();
		}
		
		/** Cargamos la conexion */
		//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
		$rs = $aRepository->DataADOConnection->Execute( "SELECT `name`, access_token, `user`, pass, ftp, `type`, type_request FROM si_sv_servicesconnection WHERE id = " . $anInstance->id_connection );
		
		if( $rs && !$rs->EOF )
		{
			$anInstance->connection = array(
					'name' => $rs->fields['name'],
					'access_token' => $rs->fields['access_token'],
					'user' => $rs->fields['user'],
					'pass' => $rs->fields['pass'],
					'ftp' => $rs->fields['ftp'],
					'type' => $rs->fields['type'],
					//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
					'type_request' => $rs->fields['type_request'],
					'id' => (int) $anInstance->id_connection
				);
			//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
			$anInstance->httpRequest = $rs->fields["type_request"];
			//@JAPR
		}
		
		return $anInstance;
	}

	function save( )
	{
		$bHavSrvConnection = getMDVersion() >= esveServicesConnection;

		/** 3 => Google Drive, 4 => FTP */
		if( ( $this->type == ddesGoogleDrive || $this->type == ddesFTP || $this->type == ddesDropBox || $this->type == ddesHTTP) && $bHavSrvConnection)
		{
			$sName = ($this->type == ddesGoogleDrive || $this->type == ddesDropBox ? $this->email : $this->ftpServer);
			if ($this->type == ddesFTP || $this->type == ddesHTTP && getMDVersion()>=esveServicesConnectionName) {
				if ($this->type == ddesFTP) {
					$FTPname = @$_REQUEST['ftpName'];
					$name = @$_REQUEST['ftpServer'];
					$user = @$_REQUEST['ftpUsername'];
					$pass = @$_REQUEST['ftpPassword'];

					$sql = 'SELECT id FROM si_sv_servicesconnection where servicename = '. $this->Repository->DataADOConnection->quote($FTPname);
					$rs = $this->Repository->DataADOConnection->Execute($sql);
					if ($rs && !$rs->EOF) {
						$accountID = $rs->fields['id'];
						$this->id_connection = $rs->fields['id'];
						$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $this->Repository->DataADOConnection->quote($name) . ', ' . 
												'`servicename` = '.  $this->Repository->DataADOConnection->quote($FTPname) . ', ' .
												'`ftp` = '.  $this->Repository->DataADOConnection->quote($name) . ', ' .
												'`pass` = '.  $this->Repository->DataADOConnection->quote($pass) . ', ' .
												'`user` = '.  $this->Repository->DataADOConnection->quote($user) . 
												' where id = '. $accountID;
						$this->Repository->DataADOConnection->Execute($sqlUpdate);
					} else {
						$sql= 'INSERT INTO `si_sv_servicesconnection` ( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, 
								`type_request`'.(getMDVersion()>=esveServicesConnectionName? ', `servicename`': ''). ')
							VALUES (' . $this->Repository->DataADOConnection->quote($name) . ',"",' . $this->Repository->DataADOConnection->quote($user) . ' ,' . 
								$this->Repository->DataADOConnection->quote($pass) . ' , ' . $this->Repository->DataADOConnection->quote($name) . ',5 ,NULL ,NULL , NULL ' . 
								(getMDVersion()>=esveServicesConnectionName? ', '. $this->Repository->DataADOConnection->quote($FTPname) : '') . ');';
						$rs = $this->Repository->DataADOConnection->Execute($sql);
						$accountID = $this->Repository->DataADOConnection->_insertid();
						$this->id_connection = $this->Repository->DataADOConnection->_insertid();
					}
				}
				else {
					$httpName = @$_REQUEST['httpName'];
					$name = @$_REQUEST['httpServer'];
					$user = @$_REQUEST['httpUsername'];
					$pass = @$_REQUEST['httpPassword'];
					$parameters = @$_REQUEST['Parameters'];
					//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
					$httpRequest = (int) @$_REQUEST['httpRequest'];

					$sql = 'SELECT id FROM si_sv_servicesconnection where servicename = '. $this->Repository->DataADOConnection->quote($httpName);
					$rs = $this->Repository->DataADOConnection->Execute($sql);
					if ($rs && !$rs->EOF) {
						$accountID = $rs->fields['id'];
						$this->id_connection = $rs->fields['id'];
						//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
						$sqlUpdate='UPDATE si_sv_servicesconnection set `name` = '.  $this->Repository->DataADOConnection->quote($name) . ', ' . 
												'`servicename` = '.  $this->Repository->DataADOConnection->quote($httpName) . ', ' .
												'`pass` = '.  $this->Repository->DataADOConnection->quote($pass) . ', ' .
												'`user` = '.  $this->Repository->DataADOConnection->quote($user) . ', '.
												'`type_request` = '.$httpRequest.
												' where id = '. $accountID;
						$this->Repository->DataADOConnection->Execute($sqlUpdate);
					} else {
						//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
						$sql= 'INSERT INTO `si_sv_servicesconnection` ( `name`, `access_token`, `user`, `pass`, `ftp`, `type`, `pass_parameter`, `user_parameter`, 
								`type_request`'.(getMDVersion()>=esveServicesConnectionName? ', `servicename`': ''). ')
							VALUES (' . $this->Repository->DataADOConnection->quote($name) . ',"",' . $this->Repository->DataADOConnection->quote($user) . ' ,' . 
								$this->Repository->DataADOConnection->quote($pass) . ' , ' . $this->Repository->DataADOConnection->quote($name) . ',6 ,NULL ,NULL , ' . $httpRequest . 
								(getMDVersion()>=esveServicesConnectionName? ', '. $this->Repository->DataADOConnection->quote($httpName) : '') . ');';
						$rs = $this->Repository->DataADOConnection->Execute($sql);
						$accountID = $this->Repository->DataADOConnection->_insertid();
						$this->id_connection = $this->Repository->DataADOConnection->_insertid();
					}
				}
			}
			else if( $sName )
			{
				/** Buscamos el ID de la conexion, si no existe la creamos */
				$rs = $this->Repository->DataADOConnection->Execute('SELECT id, name FROM si_sv_servicesconnection WHERE id = '. $this->Repository->DataADOConnection->Quote( $sName ) .' AND type = ' . $this->type);

				if ( $rs && !$rs->EOF )
				{
					$this->id_connection = $rs->fields['id'];
				} else
				{
					/** No existe la conexion, la creamos y obtenemos el ID */
					$this->Repository->DataADOConnection->Execute('INSERT INTO si_sv_servicesconnection (name, access_token, type, user, pass, ftp, pass_parameter, user_parameter,type_request) VALUES ('. $this->Repository->DataADOConnection->Quote( $sName ) .', '. $this->Repository->DataADOConnection->Quote( $this->access_token ) .', '. $this->type . ',' . $this->Repository->DataADOConnection->Quote( $this->ftpUsername ) . ',' . $this->Repository->DataADOConnection->Quote( $this->ftpPassword ) . ',' . $this->Repository->DataADOConnection->Quote( $this->ftpServer ). ',' . $this->Repository->DataADOConnection->Quote( $this->httpUsernameParameter ). ',' . $this->Repository->DataADOConnection->Quote( $this->httpPasswordParameter ). ',' . $this->Repository->DataADOConnection->Quote( $this->httpRequest ) .')');

					$this->id_connection = $this->Repository->DataADOConnection->_insertid();
				}
			}
		}

		if( $this->id < 1 || !is_numeric( $this->id ) )
		{
			//@JAPR 2016-08-11: Corregidos bugs con el armado de estos estatutos, al poner un If condicionado por cierta versión, se excluyó la posibilidad de cambiar requerimientos
			//de versión como sucedió cuando se decidieron bajar cambios a GeoControl, así que se estandariza el estatuto para que cada versión controle sólo sus propios campos en
			//lugar de agrupar a otros, además que $strIsIncrementalField sólo estaba en el if pero no en el else (#UGSLXZ)
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esveServicesConnection) {
				$strAdditionalFields .= ', id_connection, fileformat, path, real_path ';
				$strAdditionalValues .= ", " . $this->id_connection . "," . $this->fileformat . "," . $this->Repository->DataADOConnection->Quote( $this->path ) . "," . 
					$this->Repository->DataADOConnection->Quote( $this->realPath );
			}
			if (getMDVersion()>=esvParameterHTTP) {
				$strAdditionalFields .= ', parameters ';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote( $this->Parameters);
			}
			if (getMDVersion()>=esvDataDestination) {
				$strAdditionalFields .= ", emailformat, cla_dimension, cla_dashboar, cla_ars";
				$strAdditionalValues .= ", " . $this->emailformat . "," . $this->cla_dimension . "," . $this->cla_dashboar . "," . $this->cla_ars;
			}
			if (getMDVersion()>=esvadditionaleMail) {
				//@JRPP 2016-01-28: Agregada propiedad de correo adicional
				$strAdditionalFields .= ', additionaleMail ';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->additionaleMail);
			}
			if (getMDVersion()>=esvDestinationIncremental) {
				//GCRUZ 2016-01-15. Agregada propiedad de destino incremental
				$strAdditionalFields .= ', isIncremental ';
				$strAdditionalValues .= ', '.($this->isIncremental? '1' : '0');
			}
			if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
				//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
				$strAdditionalFields .= ', httpSendSinglePOST ';
				$strAdditionalValues .= ', '.($this->httpSendSinglePOST? '1' : '0');
			}
			//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
			if (getMDVersion()>=esvDataDestEmailDetail) {
				$strAdditionalFields .= ', emailsubject, emailbody ';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote( $this->emailsubject ).', '.
					$this->Repository->DataADOConnection->Quote( $this->emailbody );
			}
			//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
			if (getMDVersion()>=esvDataDestEmailComposite) {
				$strAdditionalFields .= ', additionaleMailComposite ';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->additionaleMailComposite);
			}
			//@JAPR
			$query = "INSERT INTO si_sv_datadestinations (`id_survey`, `name`, `createdDate`, `modifiedDate`, `active`, `condition`, `type`, `eBavelForm`".
					$strAdditionalFields." ) VALUES (". 
				$this->id_survey .", ". $this->Repository->DataADOConnection->Quote( $this->name ) .", ". $this->Repository->DataADOConnection->quote( date("Y-m-d H:i:s") ) .", ". 
				$this->Repository->DataADOConnection->quote( date("Y-m-d H:i:s") ) .", ". ( $this->active ? 1 : 0 ) .", ". $this->Repository->DataADOConnection->Quote( $this->condition ) .
				", ". $this->type .", ". $this->Repository->DataADOConnection->Quote( $this->eBavelForm ) . 
				$strAdditionalValues.")";
			//@JAPR
		} else {
			//@JAPR 2016-08-11: Corregidos bugs con el armado de estos estatutos, al poner un If condicionado por cierta versión, se excluyó la posibilidad de cambiar requerimientos
			//de versión como sucedió cuando se decidieron bajar cambios a GeoControl, así que se estandariza el estatuto para que cada versión controle sólo sus propios campos en
			//lugar de agrupar a otros, además que $strIsIncrementalUpdate sólo estaba en el if pero no en el else (#UGSLXZ)
			$strAdditionalValues = '';
			if (getMDVersion() >= esveServicesConnection) {
				$strAdditionalValues .= ", `id_connection` = " . $this->id_connection . 
					", `fileformat` = " . $this->fileformat . ", `path` = " . $this->Repository->DataADOConnection->Quote( $this->path ) . 
					", `real_path` = " . $this->Repository->DataADOConnection->Quote( $this->realPath );
			}
			if (getMDVersion()>=esvParameterHTTP) {
				$strAdditionalValues .= ", parameters = " . $this->Repository->DataADOConnection->Quote( $this->Parameters);
			}
			if (getMDVersion()>=esvDataDestination) {
				$strAdditionalValues .= ", emailformat = ".  $this->emailformat . 
					", cla_dimension = " . $this->cla_dimension . ", cla_dashboar = " . $this->cla_dashboar . ", cla_ars = " . $this->cla_ars;
			}
			if (getMDVersion()>=esvadditionaleMail) {
				//@JRPP 2016-01-28: Agregada propiedad de correo adicional
				$strAdditionalValues .= ', additionaleMail =' . $this->Repository->DataADOConnection->Quote($this->additionaleMail);
			}
			if (getMDVersion()>=esvDestinationIncremental) {
				//GCRUZ 2016-01-15. Agregada propiedad de destino incremental
				$strAdditionalValues .= ', isIncremental = '. (($this->isIncremental)? '1' : '0');
			}
			if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
				//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
				$strAdditionalValues .= ', httpSendSinglePOST = '. (($this->httpSendSinglePOST)? '1' : '0');
			}
			//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
			//@JAPR 2018-02-12: Corregido un bug, la validación de metadata se estaba haciendo contra la constante equivocada (#UTOK8K)
			if (getMDVersion()>=esvDataDestEmailDetail) {
				$strAdditionalValues .= ', emailsubject = '. $this->Repository->DataADOConnection->Quote( $this->emailsubject ).
					', emailbody = '. $this->Repository->DataADOConnection->Quote( $this->emailbody );
			}
			//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
			if (getMDVersion()>=esvDataDestEmailComposite) {
				$strAdditionalValues .= ', additionaleMailComposite =' .
				$this->Repository->DataADOConnection->Quote($this->additionaleMailComposite);
			}
			//@JAPR 2016-07-19: Corregido un bug, no estaba remapeando la forma de eBavel (#JKGL00)
			$query = "UPDATE si_sv_datadestinations SET `name`=". $this->Repository->DataADOConnection->Quote( $this->name ) .
				",modifiedDate=". $this->Repository->DataADOConnection->quote( date("Y-m-d H:i:s") ) .", `active`=". ( $this->active ? 1 : 0 ) .
				", `condition`=". $this->Repository->DataADOConnection->Quote( $this->condition ) . 
				", eBavelForm = " . $this->Repository->DataADOConnection->Quote( $this->eBavelForm ) .  
				$strAdditionalValues . 
				" WHERE id = " . $this->id;
			//@JAPR
		}

		$this->Repository->DataADOConnection->Execute($query);

		if( $this->id < 1 || !is_numeric( $this->id ) )
		{
			$this->id = $this->Repository->DataADOConnection->_insertid();
		}

		$this->Repository->DataADOConnection->Execute("DELETE FROM si_sv_datadestinationsmapping WHERE id_datadestinations = " . $this->id);

		/** Guardamos la definicion del mapeo si existen */
		if( $this->mapping && count( $this->mapping ) )
		{
			for( $i = 0; $i < count( $this->mapping ); $i++ )
			{
				//@JAPR 2016-07-18: Validado para permitir limpiar los mapeos hacia eBavel (#4LROGA)
				//Si se recibe un QuestionID == 0, significa que se decidió no mapear ese campo de eBavel, así que ya no lo inserta
				if ((int) @$this->mapping[$i]['questionid']) {
					//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					$strAdditionalFields = '';
					$strAdditionalValues = '';
					if (getMDVersion()>=esvSketchPlus) {
						$strAdditionalFields = ', additional_data';
						$strAdditionalValues = ', '.(int) @$this->mapping[$i]['additionalData'];
					}
					$sql = "INSERT INTO si_sv_datadestinationsmapping (id_datadestinations, id_question, fieldformid, formula {$strAdditionalFields}) 
						VALUES ( {$this->id}, {$this->mapping[$i]['questionid']}, ". 
							$this->Repository->DataADOConnection->Quote( $this->mapping[$i]['fieldformid'] ) .", ". 
							$this->Repository->DataADOConnection->Quote( $this->mapping[$i]['formula'] ) ." {$strAdditionalValues})";
					$this->Repository->DataADOConnection->Execute( $sql );
					//@JAPR
				}
				//@JAPR
			}
		}

	}


	//2018-05-11 @JMRC #J8QPV6: actualizacion de las condiciones de envio y el cuerpo de email en la copia de la forma
  function updateConditionIDs($oldSurveyId, $oldDataDestinationId = null)
  {
    $oldIDs = array();
    $newIDs = array();

    $query = "UPDATE si_sv_datadestinations SET {conditions} WHERE id = {$this->id}";
    $conditions = array();

    if ($this->condition != '' && !is_null($this->condition)) {
    	//2018-05-11 @JMRC #J8QPV6: obtener ids de los campos condicion y cuerpo de email
    	$oldIDs = $this->extractIDsFromField($this->condition);
    	if (!is_null($oldIDs)) {
    		//2018-05-11 @JMRC #J8QPV6: obtener los ids equivalentes en la forma copia y reemplazarlos
    		$newIDs = $this->getNewIDs($oldIDs, $oldSurveyId);
    		$this->condition = $this->replaceIDsInField($this->condition, $oldIDs, $newIDs);
    		$conditions[] = "`condition` = {$this->Repository->DataADOConnection->qstr($this->condition)}";
    	}
    }

    if ($this->type == ddeseBavel) {
    	//2018-05-29 @JMRC #J8QPV6: destino de datos eBavel agregado
    	$oldIDs['questions'] = $this->extractIDsFromTable();
    	if (!is_null($oldIDs['questions'])) {
    		$newIDs = $this->getNewIDs($oldIDs, $oldSurveyId);
    		//2018-05-29 @JMRC #J8QPV6: actualizar el mapeo de ebavel con la forma
    		for ($i = 0; $i < sizeof($oldIDs['questions']); $i++) {
    			$questionMapUpdateQuery = "UPDATE si_sv_datadestinationsmapping ".
    			"SET id_question = {$newIDs['questions'][$i]} ".
    			"WHERE id_datadestinations = {$this->id} AND id_question = {$oldIDs['questions'][$i]};";
    			$this->Repository->DataADOConnection->Execute($questionMapUpdateQuery);
        }
    	}
    } else {
    	//columnas para destino de datos email
    	if ($this->type == ddesEMail) {
    		if ($this->emailbody != '' && !is_null($this->emailbody)) {
	    		$oldIDs = $this->extractIDsFromField($this->emailbody);
	    		if (!is_null($oldIDs)) {
	    			$newIDs = $this->getNewIDs($oldIDs, $oldSurveyId);
	    			$this->emailbody = $this->replaceIDsInField($this->emailbody, $oldIDs, $newIDs);
	    			$conditions[] = "emailbody = {$this->Repository->DataADOConnection->qstr($this->emailbody)}";
	    		}
    		}

    		if ($this->emailsubject != '' && !is_null($this->emailsubject)) {
	    		$oldIDs = $this->extractIDsFromField($this->emailsubject);
	    		if (!is_null($oldIDs)) {
	    			$newIDs = $this->getNewIDs($oldIDs, $oldSurveyId);
	    			$this->emailsubject = $this->replaceIDsInField($this->emailsubject, $oldIDs, $newIDs);
	    			$conditions[] = "emailsubject = {$this->Repository->DataADOConnection->qstr($this->emailsubject)}";
	    		}
    		}
    	} else {
    		//columnas del resto de destinos de datos
    		//2018-06-07 @JMRC #J8QPV6: destino de datos ftp agregado
    		//2018-06-19 @JMRC #J8QPV6: destino de datos google drive agregado
    		//2018-06-19 @JMRC #J8QPV6: destino de datos http agregado
    		//2018-06-21 @JMRC #J8QPV6: destino de datos dropbox agregado
    		//obtener datos de conexion del destino de datos original
        $destinationsParamsQuery = "SELECT id_connection, `path`, real_path, parameters FROM si_sv_datadestinations WHERE id = {$oldDataDestinationId}";
        $destinationParamsResult = $this->Repository->DataADOConnection->Execute($destinationsParamsQuery);

        if ($destinationParamsResult && !$destinationParamsResult->EOF) {
	        //actualizar datos de conexion ftp en el destino de datos copia
	        $columnNames = array(
	        	'path',
	        	'real_path',
	        	'parameters',
	        	'id_connection'
	        );
	        foreach ($columnNames as $column) {
	        	if (!is_null($destinationParamsResult->fields[$column]) && $destinationParamsResult->fields[$column] != '') {
	        		$conditions[] = "`{$column}` = {$this->Repository->DataADOConnection->qstr($destinationParamsResult->fields[$column])}";
        		}
	        }
	      }
      }
    }
    //2018-06-29 @JMRC #SIPVYG: update del destino de datos actual
    if (sizeof($conditions) > 0) {
    	$query = str_replace('{conditions}', implode(', ', $conditions), $query);
    	$this->Repository->DataADOConnection->Execute($query);
    }
  }

	//2018-05-11 @JMRC #J8QPV6: extraccion de los IDs de seccion y de pregunta en una expresión
	private function extractIDsFromField($field)
	{
		//2018-05-11 @JMRC #J8QPV6: expresiones regulares para buscar los ID de preguntas ($Q123)
		//y secciones ($S123) en la condicion de envio
		$questionsRegExp = '/Q[0-9]+/';
		$sectionsRegExp = '/S[0-9]+/';
		$IDs = array();
		$results = array();

		preg_match_all($questionsRegExp, $field, $IDs['questions']);
		preg_match_all($sectionsRegExp, $field, $IDs['sections']);

		if (!empty($IDs['questions'][0])) {
			$results['questions'] = $IDs['questions'][0];
		}
		if (!empty($IDs['sections'][0])) {
			$results['sections'] = $IDs['sections'][0];
		}

		if (sizeof($results) > 0) {
			return $results;
		}
		return null;
	}

	//2018-05-29 @JMRC #J8QPV6: extraccion de IDs de preguntas, de la tabla de mapeo entre eBavel y eForms
	private function extractIDsFromTable()
	{
		$IDs = array();
		$questionsNumberQuery = "SELECT id_question, fieldformid FROM si_sv_datadestinationsmapping WHERE id_datadestinations = {$this->id};";
		$questionNumberResultSet = $this->Repository->DataADOConnection->Execute($questionsNumberQuery);
		if ($questionNumberResultSet && !$questionNumberResultSet->EOF) {
			while (!$questionNumberResultSet->EOF) {
				$IDs[] = $questionNumberResultSet->fields['id_question'];
				$questionNumberResultSet->MoveNext();
			}
			return $IDs;
		}
		return null;
	}

	//2018-05-11 @JMRC #J8QPV6: obtener los ids que son equivalentes a la expresion, para la forma copia
	private function getNewIDs($fieldIDs, $oldSurveyId)
	{
		$newFieldIDs = array();

		//2018-05-11 @JMRC #J8QPV6: evaluar si habia id de preguntas en la expresion
		if (array_key_exists('questions', $fieldIDs)) {
			$newFieldIDs['questions'] = array();
			foreach ($fieldIDs['questions'] as $qID) {
				//2018-05-11 @JMRC #J8QPV6: la funcion extractIDsFromField retorna valores: Q123
				$qID = str_replace('Q', '', $qID);

				//Si no existe la pregunta/mapeo a ebavel
				if (intval($qID) > 0) {
					//2018-05-11 @JMRC #J8QPV6: buscando la referencia de numero de pregunta en base a id pregunta de la forma original
					$questionNumberQuery = "SELECT SurveyID, QuestionID, QuestionNumber FROM si_sv_question ".
					"WHERE SurveyID = $oldSurveyId ".
					"AND QuestionID = $qID;";
					$questionNumberResultSet = $this->Repository->DataADOConnection->Execute($questionNumberQuery);
					//2018-05-11 @JMRC #J8QPV6: buscando la referencia de id pregunta en base a numero de pregunta para la forma copia
					$newQuestionIDQuery = "SELECT SurveyID, QuestionID, QuestionNumber FROM si_sv_question ".
					"WHERE QuestionNumber = {$questionNumberResultSet->fields['QuestionNumber']} ".
					"AND SurveyID = {$this->id_survey};";
					$newQuestionIDResultSet = $this->Repository->DataADOConnection->Execute($newQuestionIDQuery);
					//2018-05-11 @JMRC #J8QPV6: agregando el id de pregunta para la forma copia
					if ($newQuestionIDResultSet) {
						$newFieldIDs['questions'][] = $newQuestionIDResultSet->fields['QuestionID'];
					} else {
						$newFieldIDs['questions'][] = $qID;
					}
				} else {
					$newFieldIDs['questions'][] = $qID;
				}
			}
 		}

 		//2018-05-11 @JMRC #J8QPV6: evaluar si habia id de secciones en la expresion
 		if (array_key_exists('sections', $fieldIDs)) {
 			$newFieldIDs['sections'] = array();
 			foreach ($fieldIDs['sections'] as $sID) {
 				//2018-05-11 @JMRC #J8QPV6: la funcion extractIDsFromField retorna valores: S123
				$sID = str_replace('S', '', $sID);
				//Si no existe la pregunta/mapeo a ebavel
				if (intval($sID) > 0) {
					$sectionNumberQuery = "SELECT SurveyID, SectionID, SectionNumber FROM si_sv_section ".
					"WHERE SurveyID = $oldSurveyId ".
					"AND SectionID = $sID;";
					$sectionNumberResultSet = $this->Repository->DataADOConnection->Execute($sectionNumberQuery);
					$newSectionIDQuery = "SELECT SurveyID, SectionID, SectionNumber FROM si_sv_section ".
					"WHERE SectionNumber = {$sectionNumberResultSet->fields['SectionNumber']} ".
					"AND SurveyID = {$this->id_survey};";
					$newSectionIDResultSet = $this->Repository->DataADOConnection->Execute($newSectionIDQuery);
					if ($newSectionIDResultSet) {
						$newFieldIDs['sections'][] = $newSectionIDResultSet->fields['SectionID'];
					} else {
						$newFieldIDs['sections'][] = $sID;
					}
				} else {
					$newFieldIDs['sections'][] = $sID;
				}
 			}
 		}

 		if (sizeof($newFieldIDs) > 0) {
 			return $newFieldIDs;
 		}
 		return null;
	}

	//2018-05-11 @JMRC #J8QPV6: reemplazo de ids en una expresion dada
	private function replaceIDsInField($field, $oldIDs, $newIDs)
	{
		//2018-05-11 @JMRC #J8QPV6: se reemplazan los id de pregunta de la forma original por los ids de la forma copia
		foreach ($oldIDs as $key => $idsArray) {
			for ($i=0; $i < sizeof($idsArray); $i++) {
				$replace = "";
				if ($key == 'questions') {
					$replace = 'Q';
				} else {
					$replace = 'S';
				}
				$replace .= $newIDs[$key][$i];
				$field = str_replace($idsArray[$i], $replace, $field);
			}
		}
		return $field;
	}

	function remove()
	{
		//@JAPR 2016-05-31: Corregido un bug, faltaba eliminar las referencias a preguntas (#37X3EU)
		//Estandarizado el proceso de borrado a como se ejecuta en otras clases
		$sql = "UPDATE SI_SV_Question SET DataDestinationID = 0 WHERE DataDestinationID = ".$this->id;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM si_sv_datadestinationsmapping WHERE id_datadestinations = " . $this->id;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM si_sv_datadestinations WHERE id = " . $this->id;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}

	function addMapping( $mapping )
	{

		for($i = 0; $i < count( $mapping ); $i++)
		{
			if( $mapping[$i]['fieldformid'] && $mapping[$i]['idquestion'] )
			{
				//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				$this->mapping[] = array('fieldformid' => $mapping[$i]['fieldformid'], 'questionid' => $mapping[$i]['idquestion'], 
					'formula' => (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->id_survey, $mapping[$i]['formula'], 6, otyQuestion, 0, 0, false),
					'additionalData' => (int) @$mapping[$i]['additionalData']);
				//@JAPR
			}
		}

	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Data destination");
		}
		else
		{
			return $this->name;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=eFormsDataDestinations";
		}
		else
		{
			return "BITAM_PAGE=eFormsDataDestinations&id=".$this->id;
		}
	}

	function getJSonDefinition()
	{
		return array();
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("id", $aHTTPRequest->POST))
		{
			$ids = $aHTTPRequest->POST["id"];
			if (is_array($ids))
			{
				
				$aCollection = BITAMeFormsDataDestinationsCollection::NewInstance($aRepository, $ids);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
		}

	}

	static function saveConnection( $data, $aRepository )
	{
		
		$sName = ( $data['type'] == 3 ||  $data['type'] == 2 ? $data['email'] : $data['ftpServer'] );

		$id_connection = 0;

		/** Buscamos el ID de la conexion, si no existe la creamos */
		$rs = $aRepository->DataADOConnection->Execute('SELECT id, name FROM si_sv_servicesconnection WHERE name = '. $aRepository->DataADOConnection->Quote( $sName ) .' AND type = ' . $data['type']);

		if ( $rs && !$rs->EOF )
		{
			$id_connection = $rs->fields['id'];

			/** Si existe, actualizamos la informacion */
			$aRepository->DataADOConnection->Execute( 'UPDATE si_sv_servicesconnection SET `access_token`= '. $aRepository->DataADOConnection->Quote( @$data['access_token'] ) .', `user` = ' . $aRepository->DataADOConnection->Quote( @$data['ftpUsername'] ) . ', `pass` = ' . $aRepository->DataADOConnection->Quote( @$data['ftpPassword'] ) . ', `ftp` = ' . $aRepository->DataADOConnection->Quote( @$data['ftpServer'] ) .' WHERE id = ' . $id_connection );

		} else
		{
			/** No existe la conexion, la creamos y obtenemos el ID */
			$aRepository->DataADOConnection->Execute('INSERT INTO si_sv_servicesconnection (name, access_token, type, user, pass, ftp) VALUES ('. $aRepository->DataADOConnection->Quote( $sName ) .', '. $aRepository->DataADOConnection->Quote( @$data['access_token'] ) .', '. $data['type'] . ',' . $aRepository->DataADOConnection->Quote( @$data['ftpUsername'] ) . ',' . $aRepository->DataADOConnection->Quote( @$data['ftpPassword'] ) . ',' . $aRepository->DataADOConnection->Quote( @$data['ftpServer'] ) .')');

			$id_connection = $aRepository->DataADOConnection->_insertid();
		}

		return $id_connection;
	}
}

class BITAMeFormsDataDestinationsCollection extends BITAMCollection
{

	public $NoHeader = true;
	
	public $HasPath = false;

	static function NewInstance( $aRepository, $surveyID, $destinationIDs = array() )
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)){
			ECHOString("BITAMeFormsDataDestinationsCollection::NewInstance", 1, 1, "color:purple;");
			ECHOString("destinationIDs:".implode(",",$destinationIDs), 1, 1, "color:purple;");
		}
		
		global $gblShowErrorSurvey;
		
		$strWhereReprocess="";
		if(count($destinationIDs)>0)
		{
			$destinationFilter = implode(",",$destinationIDs);
			$strWhereReprocess = " AND id IN ({$destinationFilter})";
			if (getParamValue('DebugBreak', 'both', '(int)', true)){
				ECHOString("strWhereReprocess=".$strWhereReprocess, 1, 1, "color:purple;");
			}
		}
		$strCalledClass = static::class;

		$anInstance = new $strCalledClass($aRepository);

		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		if( is_array($surveyID) )
			//$sql = $strCalledClass::$query . ' WHERE id IN (' . implode(',', $surveyID) . ')';
			$sql = $strCalledClass::getQuery() . ' WHERE id IN (' . implode(',', $surveyID) . ')'.$strWhereReprocess;
		else
			//$sql = $strCalledClass::$query . ' WHERE id_survey = ' . $surveyID;
			$sql = $strCalledClass::getQuery() . ' WHERE id_survey = ' . $surveyID.$strWhereReprocess;
		//@JAPR 2018-12-07: Modificado el query para ordenar por id en lo que se programa una funcionalidad para ordenar los destinos al gusto del usuario (#FFWP5Q)
		$sql .= " ORDER BY id_survey, id";
		//@JAPR
		$aRS = $anInstance->Repository->DataADOConnection->Execute( $sql );
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)){
			ECHOString("Query para obtener destinos a procesar", 1, 1, "color:purple;");
			ECHOString($sql, 1, 1, "color:purple;");
		}
		

		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datadestinations ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." si_sv_datadestinations ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		while ($aRS && !$aRS->EOF)
		{			
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			
			$aRS->MoveNext();
		}

		return $anInstance;

	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;

		header("Content-Type:application/json; charset=UTF-8");		

		if( $action = getParamValue('action', 'both', '(string)', true) )
		{

			if( $action == 'getInfo' )
			{
				$r = self::getInfo( $aRepository );

			} else if ($action == 'getCollection')
			{
				$anInstance = $strCalledClass::NewInstance($aRepository, getParamValue('idSurvey', 'both', '(int)', true));

				$r = $anInstance->index( $aUser );	
			} else if ($action == 'getAppFormFields')
			{
				$r = self::getAppFormFields( $aRepository, getParamValue('id', 'both', '(string)', true));
			} else if( $action == 'save' )
			{
				//@JAPR 2018-06-11: Corregido un bug, no se estaba cargando la instancia previa del destino de datos, así que el realPath de Google Drive se perdía
				//porque quien programó esto siempre pensaba que se recibiría el nuevo id interno del folder, lo cual sólo sucede si el usuario selecciona nuevamente
				//el folder desde el árbol de carpetas, así que se cargará ahora la instancia original en lugar de siempre crear una nueva cuando se trate de
				//un destino de Google Drive para conservar el path interno (#17TWIB)
				$intType = getParamValue('type', 'both', '(int)', true);
				$intId = getParamValue('id', 'both', '(int)', true);
				if ( $intId > 0 && $intType == ddesGoogleDrive ) {
					$anInstance = BITAMeFormsDataDestinations::NewInstanceWithId($aRepository, $intId);
				}
				else {
					$anInstance = new BITAMeFormsDataDestinations( $aRepository );
				}
				//@JAPR

				$anInstance->id = getParamValue('id', 'both', '(int)', true);
				
				$anInstance->id_survey = getParamValue('idSurvey', 'both', '(int)', true);

				$anInstance->name  = getParamValue('name', 'both', '(string)', true);
				
				$anInstance->active  = getParamValue('active', 'both', '(int)', true) == 1;
				
				$anInstance->condition  = (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($aRepository, $anInstance->id_survey, getParamValue('condition', 'both', '(string)', true), 6, otyQuestion, 0, 0, false);

				$anInstance->type  = getParamValue('type', 'both', '(int)', true);
				
				$anInstance->fileformat  = getParamValue('fileformat', 'both', '(int)', true);

				$anInstance->eBavelForm  = getParamValue('eBavelAppForm', 'both', '(string)', true);
				
				$anInstance->path  = getParamValue('path', 'both', '(string)', true);

				//@JAPR 2018-06-11: Corregido un bug, no se estaba cargando la instancia previa del destino de datos, así que el realPath de Google Drive se perdía
				//porque quien programó esto siempre pensaba que se recibiría el nuevo id interno del folder, lo cual sólo sucede si el usuario selecciona nuevamente
				//el folder desde el árbol de carpetas, así que se cargará ahora la instancia original en lugar de siempre crear una nueva cuando se trate de
				//un destino de Google Drive para conservar el path interno (#17TWIB)
				//Sólo actualizará realPath si realmente fue recibido, lo cual significa que el usuario lo volvió a seleccionar del árbol, de lo contrario dejará
				//el valor previametne guardado siempre y cuando aún exista un path capturado
				$strPath = trim($anInstance->path);
				$strRealPath = getParamValue('realPath', 'both', '(string)', true);
				if ( ($strPath != '' && trim($strRealPath) != '') || $strPath == '' || $strPath == '/'  ) {
					$anInstance->realPath = $strRealPath;
				}
				//@JAPR
				
				//@JRPP 2015-12-02 : Se agregan los campos a la clase 
				if (getMDVersion()>=esvDataDestination) {

					$anInstance->emailformat  = getParamValue('emailformat', 'both', '(int)', true);
					
					$anInstance->cla_dimension  = getParamValue('cla_dimension', 'both', '(int)', true);

					$anInstance->cla_dashboar  = getParamValue('cla_dashboar', 'both', '(int)', true);

					$anInstance->cla_ars  = getParamValue('cla_ars', 'both', '(int)', true);
				}
				//@JRPP 2016-01-18. Agregada propiedad de destino incremental
				if (getMDVersion()>=esvDestinationIncremental) {
					$anInstance->isIncremental = getParamValue('isIncremental', 'both', '(int)', true);
				}
				//@JRPP 2016-01-28: Agregada propiedad de correo adicional
				if (getMDVersion()>=esvadditionaleMail) {
					$anInstance->additionaleMail = getParamValue('additionaleMail', 'both', '(string)', true);
				}
				if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
					//GCRUZ 2016-12-13. Envío de HTTP en un solo POST
					$anInstance->httpSendSinglePOST = getParamValue('httpSendSinglePOST', 'both', '(int)', true);
				}
				//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
				if (getMDVersion()>=esvDataDestEmailComposite) {
					$anInstance->additionaleMailComposite = (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($aRepository, $anInstance->id_survey, getParamValue('additionaleMailComposite', 'both', '(string)', true), 6, otyQuestion, 0, 0, false);
				}
				//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
				$anInstance->emailsubject  = (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($aRepository, $anInstance->id_survey, getParamValue('emailsubject', 'both', '(string)', true), 6, otyQuestion, 0, 0, false);
				$anInstance->emailbody  = (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($aRepository, $anInstance->id_survey, getParamValue('emailbody', 'both', '(string)', true), 6, otyQuestion, 0, 0, false);
				//@JAPR
				$anInstance->addMapping( getParamValue('mapping', 'both', '(array)', true) );
				//2015-10-21@JRPP: Estas propiedades no son inicializadas y en la linea 172 son usadas para armar un query
				$anInstance->httpUsernameParameter="";
				$anInstance->httpPasswordParameter="";
				$anInstance->httpRequest = ddrtPOST;
				//@JRPP 2016-01-11: Variable definida para los parametros guardados de las conecciones de tipo HTTP
				$anInstance->Parameters="";

				if( $anInstance->type == ddesDropBox ) 
				{
					//$anInstance->access_token = getParamValue('access_token', 'both', '(string)', true);
					
					$anInstance->email = getParamValue('dpxaccount', 'both', '(string)', true);

				}

				if( $anInstance->type == ddesGoogleDrive ) 
				{
					//$anInstance->access_token = getParamValue('access_token', 'both', '(string)', true);
					
					$anInstance->email = getParamValue('gaccount', 'both', '(string)', true);
				}

				/** Por cambios que hicieron en ultimo momento para mostrar las carpetas, ahora no se manda el email del usuario, solo se manda el id de la conexion */
				if( is_numeric( $anInstance->email ) ) {
					$anInstance->id_connection = $anInstance->email;
					$anInstance->email = null;
				}

				if( $anInstance->type == ddesFTP ) 
				{
					$anInstance->ftpServer = getParamValue('ftpServer', 'both', '(string)', true);
					
					$anInstance->ftpUsername = getParamValue('ftpUsername', 'both', '(string)', true);
					
					$anInstance->ftpPassword = getParamValue('ftpPassword', 'both', '(string)', true);

					if( $anInstance->ftpServer == '' )
					{
						$anInstance->ftpServer = getParamValue('ftpaccount', 'both', '(string)', true);
					}

				}
				if($anInstance->type == ddesHTTP){
					//$anInstance->httpaccount=getParamValue('httpaccount', 'both', '(string)', true);
					$anInstance->ftpServer=getParamValue('httpServer', 'both', '(string)', true);
					$anInstance->ftpUsername=getParamValue('httpUsername', 'both', '(string)', true);
					$anInstance->ftpPassword=getParamValue('httpPassword', 'both', '(string)', true);
					$anInstance->httpUsernameParameter=getParamValue('httpUsernameParameter', 'both', '(string)', true);
					$anInstance->httpPasswordParameter=getParamValue('httpPasswordParameter', 'both', '(string)', true);
					$anInstance->httpRequest=getParamValue('httpRequest', 'both', '(int)', true);
					//@JRPP 2016-01-11: Variable definida para los parametros guardados de las conecciones de tipo HTTP
					$anInstance->Parameters=getParamValue('Parameters', 'both', '(string)', true);

					if( $anInstance->ftpServer == '' )
					{
						$anInstance->ftpServer = getParamValue('httpaccount', 'both', '(string)', true);
					}
				}

				$anInstance->save( $aRepository );

				//@JAPR 2016-12-22: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
				$r['collection'][] = array( 'id' => $anInstance->id, 'name' => $anInstance->name, 'type' => $anInstance->type );
			} else if( $action == 'getInfoSrvConnection' )
			{
				if (getMDVersion()>=esveServicesConnectionName) {
					if (getParamValue('type', 'both', '(int)', true) == 5 ||getParamValue('type', 'both', '(int)', true) == 6 || getParamValue('type', 'both', '(int)', true) == 7) {
						$rs = $aRepository->DataADOConnection->Execute( 'SELECT id, `servicename` FROM si_sv_servicesconnection WHERE `type` = ' . getParamValue('type', 'both', '(int)', true) );
					} else {
						$rs = $aRepository->DataADOConnection->Execute( 'SELECT id, `name` FROM si_sv_servicesconnection WHERE `type` = ' . getParamValue('type', 'both', '(int)', true) );
					}
				}else {
					$rs = $aRepository->DataADOConnection->Execute( 'SELECT id, `name` FROM si_sv_servicesconnection WHERE `type` = ' . getParamValue('type', 'both', '(int)', true) );
				}
				//$r = array( array('text' => '', "value" => '') );
				$r=array();
				while($rs && !$rs->EOF)
				{
					if (getMDVersion()>=esveServicesConnectionName) {
						if (getParamValue('type', 'both', '(int)', true) == 5 ||getParamValue('type', 'both', '(int)', true) == 6 || getParamValue('type', 'both', '(int)', true) == 7) {
							$r[] = array('text' => $rs->fields['servicename'], "value" => $rs->fields['id']);
						}else {
							$r[] = array('text' => $rs->fields['name'], "value" => $rs->fields['id']);
						}
					}else {
						$r[] = array('text' => $rs->fields['name'], "value" => $rs->fields['id']);
					}
					$rs->MoveNext();
				}

			} else if( $action == 'saveConnection' )
			{
				$dataConnection = getParamValue('connection', 'both', '(array)', true);

				foreach ($dataConnection as $eachConnection)
				{
					$r = BITAMeFormsDataDestinations::saveConnection( $eachConnection, $aRepository ) > 0;
				}
			} else if ( $action == 'validateGoogle' )
			{
				/**
				 * Funciona de dos maneras distintas, si no se envia el parametro codeAuth abre la ventana de google
				 * para poder pedir permisos al usuario y obtener el codigo de autentificacion, la ventana utiliza el callback
				 * para volver entrar a estas lineas, ahora como parametro envia el codigo que es validado para obtener el json
				 * que contiene la propiedad refreshtoken que es guardada o actualizada en la base de datos.
				 *
				 * Al guardar cierra la ventana flotante llamando al codigo de javascript de la ventana principal mandando
				 * informacion del usuario del cual se dio permisos
				 */
				
				include_once('datadestinations/connectionCloud.class.php');

				$codeAuth = getParamValue('code', 'both', '(string)', true);

				if( $codeAuth ) {
					$aConnectionCloud = connectionCloud::createConnection('googleDrive', $aRepository);
					
					$aConnectionCloud->getCredentials( $codeAuth );

					header("Content-Type:text/html; charset=UTF-8");
					echo "<html><head></head><body><script>window.opener.fnSendCode({email: '{$aConnectionCloud->name}', id: {$aConnectionCloud->id}});</script></body> </html>";
					exit();

				} else {
					connectionCloud::createConnection('googleDrive', $aRepository, true);
				}				
			} else if ( $action == 'validateDropBox' )
			{
				include_once('datadestinations/connectionCloud.class.php');
				$codeAuth= getParamValue('code', 'both', '(string)', true);
				if ( $codeAuth ) {
					$aConnectionCloud = connectionCloud::createConnection('dropBox', $aRepository);
					$r=$aConnectionCloud->getCredentials($codeAuth);
				}else{
					$aConnectionCloud = connectionCloud::createConnection('dropBox', $aRepository,true);
				}

			}else if($action == 'getTreeFolder')
			{
				include_once('datadestinations/connectionCloud.class.php');
				$typeDataDestination  = getParamValue('type', 'both', '(int)', true);
				switch ($typeDataDestination){
					case '3':
						$nameCloud="googleDrive";
						$accountCloud=getParamValue('gaccount', 'both', '(string)', true);
						break;
					case '2':
						$nameCloud="dropBox";
						$accountCloud=getParamValue('dpxaccount', 'both', '(string)', true);
						break;
					case '5':
						$nameCloud="FTP";
						/*if(getParamValue('ftpServer', 'both', '(string)', true)!="" && getParamValue('ftpUsername', 'both', '(string)', true)!="" && getParamValue('ftpPassword', 'both', '(string)', true)!=""){
							$accountCloud= array('ftpServer' => getParamValue('ftpServer', 'both', '(string)', true),'ftpUsername' => getParamValue('ftpUsername', 'both', '(string)', true),'ftpPassword' => getParamValue('ftpPassword', 'both', '(string)', true) );
						}else{
							$accountCloud=getParamValue('ftpaccount', 'both', '(string)', true);
						}*/
						$accountCloud= array('ftpServer' => getParamValue('ftpServer', 'both', '(string)', true),'ftpUsername' => getParamValue('ftpUsername', 'both', '(string)', true),'ftpPassword' => getParamValue('ftpPassword', 'both', '(string)', true) );
						break;
				}
				$r = connectionCloud::getTreeFiles($nameCloud,$accountCloud,$aRepository,getParamValue('idFolder', 'both', '(string)', true),getParamValue('showFiles', 'both', '(string)', true));
			}else if ($action == 'downloadFile')
			{
				include_once('datadestinations/connectionCloud.class.php');
				$typeDataDestination  = getParamValue('type', 'both', '(int)', true);
				switch ($typeDataDestination){
					case '3':
						$nameCloud="googleDrive";
						$accountCloud=getParamValue('gaccount', 'both', '(string)', true);
						break;
					case '2':
						$nameCloud="dropBox";
						$accountCloud=getParamValue('dpxaccount', 'both', '(string)', true);
						break;
					case '5':
						$nameCloud="FTP";
						/*if(getParamValue('ftpServer', 'both', '(string)', true)!="" && getParamValue('ftpUsername', 'both', '(string)', true)!="" && getParamValue('ftpPassword', 'both', '(string)', true)!=""){
							$accountCloud= array('ftpServer' => getParamValue('ftpServer', 'both', '(string)', true),'ftpUsername' => getParamValue('ftpUsername', 'both', '(string)', true),'ftpPassword' => getParamValue('ftpPassword', 'both', '(string)', true) );
						}else{
							$accountCloud=getParamValue('ftpaccount', 'both', '(string)', true);
						}*/
						$accountCloud= array('ftpServer' => getParamValue('ftpServer', 'both', '(string)', true),'ftpUsername' => getParamValue('ftpUsername', 'both', '(string)', true),'ftpPassword' => getParamValue('ftpPassword', 'both', '(string)', true) );
						break;
				}
				$r = connectionCloud::downloadFile($nameCloud,$accountCloud,$aRepository,getParamValue('path', 'both', '(string)', true),getParamValue('file', 'both', '(string)', true));
			}else if ($action == 'downloadHttpAttributes')
			{
				include_once('datadestinations/connectionCloud.class.php');
				$typeDataDestination  = getParamValue('type', 'both', '(int)', true);
				switch($typeDataDestination){
					case '6':
						$nameCloud="HTTP";
						$accountCloud= array('httpServer' => getParamValue('httpServer', 'both', '(string)', true),'httpUsername' => getParamValue('httpUsername', 'both', '(string)', true),'httpPassword' => getParamValue('httpPassword', 'both', '(string)', true),'httpParameters' => getParamValue('httpParameters', 'both', '(string)', true), 'catalogId' => getParamValue('catalogId', 'both', '(int)', true) );
						break;
				}
				$r = connectionCloud::downloadAttributes($nameCloud,$accountCloud,$aRepository);
			}
		}

		echo json_encode( $r );

		exit();

		return ;
	}

	function index($aUser)
	{
		$res = array( 'collection' => array() );

		for( $i = 0; $i < count( $this->Collection ); $i++ )
		{
			$eachInstance = $this->Collection[$i];
			
			//@JAPR 2016-12-22: Validado que al terminar de eliminar ciertos objetos se puedan realizar acciones como redireccionar a otra página (#37X3EU)
			$res['collection'][] = array( 'id' => $eachInstance->id, 'name' => $eachInstance->name, 'type' => $eachInstance->type );

		}

		return $res;
	}

	static function getAppFormFields( $aRepository, $sSectionName )
	{
		$res = array('fieldsSelect' => array());
		/** Obtenemos la lista de aplicaciones->formas->campos */
		//@JAPR 2016-07-19: Validado que no se puedan seleccionar ciertos tipos de campos de eBavel como mapeables (#HJ3L7N)
		//@JAPR 2016-12-08: Validado de nuevo que no se puedan seleccionar mas campos de eBavel como mapeables (#5Z5VWX)
		$sql = "SELECT A.id, A.label, A.required, A.tagname 
			FROM si_forms_fieldsforms A, si_forms_sections B 
			WHERE B.id = ". $aRepository->ADOConnection->Quote( $sSectionName ) ." AND B.id_section = A.section_id AND 
				A.id NOT LIKE ". $aRepository->ADOConnection->Quote( '%_' . $sSectionName ) ." 
			ORDER BY sortableKey ASC";
		$rs = $aRepository->ADOConnection->Execute($sql);
		//@JAPR 2016-12-28: Validado de nuevo que no se puedan seleccionar mas campos de eBavel como mapeables (#5Z5VWX)
		$arrExcludedFields = array('widget_secheader', 'widget_calculatedfield');
		//@JAPR
		$arrExcludedFields = array_flip($arrExcludedFields);
		while( $rs && !$rs->EOF )
		{
			$strTagName = strtolower((string) @$rs->fields['tagname']);
			if (!isset($arrExcludedFields[$strTagName])) {
				$res['fieldsSelect'][] = array( 'value' => $rs->fields['id'], 'text' => $rs->fields['label'], 'required' => $rs->fields['required'] == '1' );
			}
			$rs->MoveNext();
		}
		//@JAPR

		return $res;
	}
	
	/* Obtiene las colecciones de elementos que se usarán como posibles valores a seleccionar en la interfaz Wizard de la configuración de un Destino de datos (por lo menos tipo eBavel)
	directamente desde la metadata, y la formatea para que sea aplicable en los componentes de la ventana
	*/
	static function getInfo( $aRepository )
	{
		include_once('question.inc.php');

		$aQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, getParamValue("idSurvey", 'both', '(int)', true));

		$lastSection = '';

		$res = array('questionSelect' => '<option></option>');
		//@JAPR 2016-07-18: Validado para permitir limpiar los mapeos hacia eBavel (#4LROGA)
		//Agrega la opción para no seleccionar mapeo
		$res['questionSelect'] .= '<optgroup label="'. translate("None") .'">';
		$res['questionSelect'] .= '<option value="'. sfidNoMapping .'">('. translate("None") .')</option>';
		$res['questionSelect'] .= '</optgroup>';
		//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$res['questions'] = array();
		//@JAPR
		
		for($i = 0; $i < count($aQuestionCollection->Collection); $i++)
		{
			//Excluye los tipos de preguntas que no son mapeables por no tener un valor
			//@JAPR 2016-04-12: Corregido un bug, no estaba excluyendo todos los tipos de pregunta sin respuesta y estaba incluyendo a los tipos Llamada
			if( !in_array($aQuestionCollection->Collection[$i]->QTypeID, array(qtpShowValue,qtpAction,qtpMessage,qtpSkipSection,qtpSync,qtpPassword,qtpMapped,qtpSection,qtpOCR,qtpExit,qtpUpdateDest))  )
			{
				
				if( $lastSection != $aQuestionCollection->Collection[$i]->SectionName )
				{
					if($lastSection != '')
					{
						$res['questionSelect'] .= '</optgroup>';
					}

					$res['questionSelect'] .= '<optgroup label="'. $aQuestionCollection->Collection[$i]->SectionName .'">';
				}

				//@JAPR 2019-03-07: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				//Agregado el atributo con el tipo de pregunta para poder identificar a las Sketch+ entre las demás
				$res['questionSelect'] .= '<option value="'. $aQuestionCollection->Collection[$i]->QuestionID .'">'. $aQuestionCollection->Collection[$i]->QuestionText .'</option>';
				$res['questions'][$aQuestionCollection->Collection[$i]->QuestionID] = array("id" => $aQuestionCollection->Collection[$i]->QuestionID, "name" => $aQuestionCollection->Collection[$i]->QuestionText, "qtypeid" => $aQuestionCollection->Collection[$i]->QTypeID);
				//@JAPR
				$lastSection = $aQuestionCollection->Collection[$i]->SectionName;
			}
		}

		/**
		 * Calidad levanto un issue #6R5Q77
		 * para eliminar estas constantes
		 */
		//@JAPRWarning: Constantes sfid en config.php
		$arrConstant = array(
			"GPS" => array( 
				-5  => translate('GPS location'),
				-23 => translate('GPS country'),
				-24 => translate('GPS state'),
				-25 => translate('GPS city'),
				-26 => translate('GPS address'),
				-27 => translate('GPS zip code'),
				-28 => translate('GPS full address'),
				-36 => translate('GPS accuracy')
				),
			"Entry" => array(
				-20 => translate('Entry id'),
				-21 => translate('User id'),		
				-40 => translate('Entry score'),
				-3 =>  translate('End entry date in app'),
				-10 => translate('End entry date in server'),
				-1  => translate('Start entry date in app'),
				//-8  => translate('Start entry date in server'),
				-6  => translate('Synchronization date'),
				-4  => translate('End entry time in app'),
				-11  => translate('End entry time in server'),
				-2  => translate('Start entry time in app'),
				//-9  => translate('Start entry time in server'),
				-7  => translate('Synchronization time'),
				-22 => translate('User'),
				//@JAPR 2017-02-01: Agregado el mapeo del UUID (#TDYU15)
				-sfidUUID => translate('UUID')
				),
			'Constant' => array(
				-39 => translate('Numeric constant value'),
				-38 => translate('String constant value')
				)
		);

		foreach( $arrConstant as $keyGroup => $eachConstant )
		{
			$res['questionSelect'] .= '<optgroup label="'. translate($keyGroup) .'">';

			foreach ($eachConstant as $key => $value)
			{
				$res['questionSelect'] .= '<option value="'. $key .'">'. $value .'</option>';
			}

			$res['questionSelect'] .= '</optgroup>';
		}

		//$res['questionSelect'] .= '<optgroup label="'. translate('Constant') .'">';
		
		//$res['questionSelect'] .= '<option value="-41">'. translate('Attribute') .'</option><option value="-31">'. translate('Action category') .'</option><option value="-25">'. translate('GPS city') .'</option><option value="-20">'. translate('Report id') .'</option><option value="-21">'. translate('User id') .'</option><option value="-27">'. translate('GPS zip code') .'</option><option value="-29">'.translate('Action description') .'</option><option value="-26">'. translate('GPS address') .'</option><option value="-28">'. translate('GPS full address') .'</option><option value="-40">'. translate('Entry score') .'</option><option value="-24">'. translate('GPS state') .'</option><option value="-33">'. translate('Action status') .'</option><option value="-32">'. translate('Action due date') .'</option><option value="-3">'. translate('End entry date in app') .'</option><option value="-10">'. translate('End entry date in server') .'</option><option value="-1">'. translate('Start entry date in app"') .'</option><option value="-8">'. translate('Start entry date in server') .'</option><option value="-6">'. translate('Synchronization date') .'</option><option value="-34">'. translate('Action source') .'</option><option value="-4">'. translate('End entry time in app') .'</option><option value="-11">'. translate('End entry time in server') .'</option><option value="-2">'. translate('Start entry time in app') .'</option><option value="-9">'. translate('Start entry time in server') .'</option><option value="-7">'. translate('Synchronization time') .'</option><option value="-35">'. translate('Action source id') .'</option><option value="-19">'. translate('Action source entry id') .'</option><option value="-39">'. translate('Numeric constant value') .'</option><option value="-23">'. translate('GPS country') .'</option><option value="-36">'. translate('GPS accuracy') .'</option><option value="-30">'. translate('Action responsible') .'</option><option value="-38">'. translate('String constant value') .'</option><option value="-37">'. translate('Action title') .'</option><option value="-5">'. translate('GPS location') .'</option><option value="-22">'. translate('User') .'</option>';

		$anInstance = BITAMeFormsDataDestinations::NewInstanceWithId( $aRepository, getParamValue("id", 'both', '(int)', true));

		if( $anInstance )
		{
			//@JAPR 2018-01-16: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
			//Modificada la manera de generar el array de valores para no repetir tantas veces la misma porción, sino que dependiendo de la versión se agregarán nuevos campos
			$res['info'] = array('name' => $anInstance->name, 'active' => $anInstance->active, 'condition' => (string) BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($aRepository, $anInstance->id_survey, $anInstance->condition, 6, false), 'type' => $anInstance->type, 'eBavelForm' => $anInstance->eBavelForm, 'mapping' => $anInstance->mapping, 'connection' => $anInstance->connection, 'fileformat' => $anInstance->fileformat, 'path' => $anInstance->path);
			$arrAdditionalData = array();
			if (getMDVersion()>=esvDataDestination) {
				$arrNewFields = array( 'emailformat' => $anInstance->emailformat, 'cla_dimension' => $anInstance->cla_dimension, 'cla_dashboar' => $anInstance->cla_dashboar, 'cla_ars' => $anInstance->cla_ars );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			if (getMDVersion()>=esvParameterHTTP) {
				$arrNewFields = array( 'parameters' => $anInstance->Parameters );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			if (getMDVersion()>=esvDestinationIncremental) {
				$arrNewFields = array( 'isIncremental' => $anInstance->isIncremental );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			if (getMDVersion()>=esvadditionaleMail) {
				$arrNewFields = array( 'additionaleMail' => $anInstance->additionaleMail );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			if (getMDVersion()>=esvDataDestHTTPSinglePOST) {
				$arrNewFields = array( 'httpSendSinglePOST' => $anInstance->httpSendSinglePOST );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			if (getMDVersion()>=esvDataDestEmailDetail) {
				//2018-06-05 @JMRC #J8QPV6, se invoca la funcion para reemplazar ID´s absolutos por relativos en los campos
				//emailsubject e emailbody
				$arrNewFields = array(
					'emailsubject' => (string) BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($aRepository, $anInstance->id_survey, $anInstance->emailsubject, 6, false),
					'emailbody' => (string) BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($aRepository, $anInstance->id_survey, $anInstance->emailbody, 6, false)
				);
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
			if (getMDVersion()>=esvDataDestEmailComposite) {
				$arrNewFields = array( 'additionaleMailComposite' => $anInstance->additionaleMailComposite );
				$arrAdditionalData = array_merge($arrAdditionalData, $arrNewFields);
			}
			$res['info'] = array_merge($res['info'], $arrAdditionalData);
			
			//@JAPR 2016-07-20: Corregido el valor del tipo de request ya que no estaba seleccionandolo al editar (#N5DUR8)
			//Agregado el valor faltante con el tipo de request realizado
			$res['info']['httpRequest'] = $anInstance->httpRequest;
			//@JAPR
			
			for($i = 0; $i < count( $res['info']['mapping'] ); $i++)
			{
				$res['info']['mapping'][$i]['formula'] = (string) BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($aRepository, $anInstance->id_survey, $res['info']['mapping'][$i]['formula'], 6, false);
			}
		}		

		$apps = self::appFormsList( $aRepository );

		$res['appFormsList'] = $apps['appFormsList'];

		return $res;
	}

	static function appFormsList( $aRepository )
	{
		header("Content-Type:application/json; charset=UTF-8");
		
		$res = array('appFormsList' => '<option></option>');
		//@JAPR 2016-07-19: Agregado el ordenamiento de Aplicaciones y Formas de eBavel en la lista de selección de mapeos
		$rs = $aRepository->ADOConnection->Execute('SELECT A.codeName, A.applicationName, B.id, B.sectionName FROM si_forms_apps A, si_forms_sections B WHERE A.id_app = B.appId AND B.editType != 10 ORDER BY A.applicationName, A.codeName, B.sectionName, B.id');
		//@JAPR
		$lastApp = null;
		
		while( $rs && !$rs->EOF )
		{
			if( $lastApp != $rs->fields['codeName'] )
			{
				if( $lastApp != null )
				{
					$res['appFormsList'] .= '</optgroup>';
				}

				$res['appFormsList'] .= '<optgroup label="'. $rs->fields['applicationName'] .'">';
			}

			$res['appFormsList'] .= '<option value="'. $rs->fields['id'] .'">'. $rs->fields['sectionName'] .'</option>';

			$lastApp = $rs->fields['codeName'];

			$rs->MoveNext();
		}

		return $res;
	}

	function generateDetailForm($aUser, $isAssociateDissociateMode = false) {

		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		
		$strScriptPath = $path_parts["dirname"];

		$myFormName = get_class($this);

		

	
		exit();

	}

	function get_CustomButtons()
	{
		return array();
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=" . static::class;
	}

	function get_Title()
	{	
		return translate("Models");
	}

	function get_PathArray()
	{
		$pathArray = array();
		
		return $pathArray;
	}

}


?>
