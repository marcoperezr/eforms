<?php
/* Este archivo contendrá métodos que regresan arrays de datos de los objetos de eBavel para uso dentro de eForms
No pretende simular funcionalidad ni obtener la totalidad de los detalles del eBavel, sino simplemente leer información básica para permitir los
mapeos entre los productos o bien consultar cierta información que se necesida de eBavel. No se pueden utilizar directamente las clases del eBavel
porque están preparadas para su propio ambiente y en general manejan una estructura muy diferente a la de eForms (no siguen el mismo estándar de
framework de EktosBS, así que tentativamente este archivo será la única forma de acceder a los datos de eBavel en lo que se pudiera generar una
versión estandarizada inter-producto
*/
/*
function GetEBavelAppIDFromSectionID($aRepository, $aSectionID = null) {
	if (is_null($aSectionID) || !is_null($aSectionID)) {
		return 0;
	}
}
*/

//@JAPR 2014-08-07: Agregados los datos heredados desde eBavel para secciones Recap
/* Dado un array con las propiedades de un campo de eBavel (como los retornados por la función GetEBavelFieldsForms) se generará un nombre de
variable para que el usuario pueda configurar sus filtros dinámicos fácilmente, la sintaxis es similar a la retornada por la función
BITAMCatalogMember->getVariableName
*/
function GetEBavelFieldVariable($aField) {
	if (!is_array($aField)) {
		return '';
	}
	
	$strFieldVarName = trim((string) @$aField['label']);
	$intPos = strpos($strFieldVarName, ' ');
	if ($intPos !== false) {
		//@JAPR 2014-08-28: Corregido un bug, se estaba dejando el espacio en blanco, ahora preferentemente agregará una segunda palabra cuando
		//se detecte un espacio en blanco, si es que hay un segundo espacio
		$intPos2 = strpos($strFieldVarName, ' ', $intPos +1);
		if ($intPos2 !== false) {
			$strFieldVarName = str_replace(' ', '', substr($strFieldVarName, 0, $intPos2));
		}
		else {
			$strFieldVarNamePrev = $strFieldVarName;
			$strFieldVarName = substr($strFieldVarName, 0, $intPos);
			//En este caso, agregará los siguientes 10 caracteres a partir del espacio
			$strFieldVarName .= trim(substr($strFieldVarNamePrev, $intPos +1, 10));
		}
		//@JAPR
	}
	else {
		$strFieldVarName = substr($strFieldVarName, 0, 10);
	}
	
	if ($strFieldVarName == '') {
		$strFieldVarName = 'ID'.(int) @$aField['key'];
	}
	
	$strFieldVarName = "@Fld".str_replace("'", '', $strFieldVarName);
	
	return $strFieldVarName;
}

//@JAPR 2014-08-18: Agregada una función genérica (no basada en un catálogo) para extraer valores de una forma de eBavel según los parámetros
/* Esta función podría reemplazar lo realizado previamente en los archivos catalogFilter.inc.php y eBavelCatalogValue.inc.php, ya que ambos
archivos se basan en un catálogo pero al final el acceso a eBavel es mediante una forma, de hecho el archivo geteBavelFieldValues.php utiliza
directamente esta función pues ahí nació el requerimiento de pedir datos a una forma sin necesidad de un catálogo
Los parámetros $aFormID y $aFieldsArray se basan en los IDs numéricos de la metadata de eBavel, aunque internamente se deben utilizar los
IDs string para invocar a los métodos de los objetos de eBavel, pero esa lógica se delega a esta función. $aFieldsArray puede ser un ID de
campo directamente en lugar de un array de varios IDs
El parámetro $aFieldsFilters contiene un array indexado por el FieldID (numérico) y conteniendo un valor a filtrar (eventualmente se podría
recibir un array para aplicar filtro de tipo IN)
El parámetro $iReturnType especificará el formato en que se deben regresar los archivos:
	ebdtSectionFilters - String para Filtros, esto es una lista de rows separada por _SV_AT_SV_, donde el primer elemento (separado del resto por _SV_ID_SV_)
		es el ID numérico del campo en cuestión, y luego viene una lista separada por _SV_VAL_SV_ de todos los valores de dicho campo
	ebdtJSonForApps - JSon requerido por el App, en formato de tabla incluyendo un Header con la definición de cada campo. La tabla de valores se compone
		de los rows de datos, donde cada columna es uno de los campos solicitados. Las rows no tienen índice pues es un array, pero las
		columnas si porque es una array de objetos, así que al objeto Row se le puede pedir el valor por medio del ID numérico de campo
	
Si la función falla, regresará un null para no tener problemas por los diferentes tipos de respuesta esperados según el parámetro $iReturnType
*/
function GetEBavelFieldValues($aRepository, $aFormID, $aFieldsArray, $aFieldsFilters = null, $iReturnType = ebdtSectionFilters) {
	$aFormID = (int) @$aFormID;
	if ($aFormID <= 0) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nNo eBavel form was specified");
		}
		return null;
	}
	
	if (!is_array($aFieldsArray)) {
		$aFieldsArray = array((int) @$aFieldsArray);
	}
	if (count($aFieldsArray) == 0) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nNo eBavel fields were specified");
		}
		return null;
	}
	
	if (is_null($aFieldsFilters) || !(is_array($aFieldsFilters) || is_string($aFieldsFilters))) {
		$aFieldsFilters = array();
	}
	
	$strResult = "";
	try {
		//Obtiene la ruta del eBavel a utilizar
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel path configured");
			}
			return null;
		}
		
		//Verica si el código de eBavel está disponible
		$strPath = '';
		$streBavelPath = '';
		$arrURL = parse_url($objSetting->SettingValue);
		$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
		$strPath = str_replace('/', '\\', $strPath);
		$strPath = str_replace('\\\\', '\\', $strPath);
		if (file_exists($strPath))
		{
			@require_once($strPath);
			$streBavelPath = str_ireplace('service.php', '', $strPath);
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if (!function_exists('NotifyError')) {
				echo("<br>\r\nNo NotifyError function");
			}
			else {
				echo("<br>\r\nNotifyError function exists");
			}
		}
		
		if (!class_exists('Tables'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			return null;
		}
		if (!class_exists('DBForm'))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel code loaded");
			}
			return null;
		}
		
		//@JAPR 2014-05-20: Corregido un bug, para que eBavel use seguridad, necesita tener esta variable de sesión asignada
		//@JAPR 2016-07-06: Corregido un bug, para el acceso al API simplemente se sobrescriben las variables, condicionarlo de esta manera evitaba que se pudiera usar el
		//login de eForms independiente (sin pasar por KPIOnline) ya que ese no sobreescribe estas variables, así que quedaban mezcladas (#S36ANP)
		//if (is_null(@$_SESSION["BITAM_UserName"]) || trim($_SESSION["BITAM_UserName"]) == '') {
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//@session_register("BITAM_UserName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		//}
		//@JAPR
		
		$fieldValues = array();
		$arreBavelFormColl = @GetEBavelSections($aRepository, false, null, array($aFormID));
		if (is_null($arreBavelFormColl) || !is_array($arreBavelFormColl) || count($arreBavelFormColl) == 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel section loaded");
			}
			return null;
		}
		
		$objForm = $arreBavelFormColl[0];
		$strFormID = $objForm['id'];
		
		$arreBavelFields = @GetEBavelFieldsForms($aRepository, true, null, array($aFormID));
		if (is_null($arreBavelFields) || !is_array($arreBavelFields) || count($arreBavelFields) == 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel fields loaded");
			}
			return null;
		}
		
		//Convierte los IDs numéricos recibidos en su ID string interno (campo id), ya que así es como se maneja en los objetos de eBavel
		$arrFieldIDs = array();
		foreach ($aFieldsArray as $inteBavelFieldID) {
			if (isset($arreBavelFields[$inteBavelFieldID])) {
				$arrFieldIDs[$inteBavelFieldID] = $arreBavelFields[$inteBavelFieldID]['id'];
			}
		}
		$intNumFields = count($arrFieldIDs);
		
		//Inicia la invocación de los métodos de eBavel para extraer los datos
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		//El parámetro aFieldsIDs es un array con los IDs de campos de eBavel que se quieren recuperar
		//@JAPR 2014-11-13: Agregado el mapeo de atributos Geolocalización de eBavel separados en Latitude y Longitude
		//A la fecha de esta implementación, no se había necesitado esta función sino sólo en 2 lugares, y en ninguno de ellos se hacía
		//referencia a atributos que se mapeaban a un campo Geolocation de eBavel, así que no se modificó esta función, sin embargo sería bueno
		//revisar la implementación buscando los comentarios con el mismo texto que el de esta fecha, por si se intentasen utilizar campos de
		//eBavel que son tipo Geolocation directamente. Los puntos donde se había utilizado esta funcionalidad en este fecha eran:
		//-Filtros de secciones Recap, las cuales no estaban habilitadas
		//-Filtros de markers de preguntas simple-choice tipo Mapa, los cuales si estaban habilitados pero no habían usado campos tipo Geolocation
		$objQuery = DBForm::name($strFormID)->fields($arrFieldIDs);

		//Agregado el GROUP BY por el campo que se está pidiendo
		//@$objQuery->groupBy($arrFieldIDs);
		//$objQuery->onlyFields(false);
		//Si sólo se pedirá el RecordSet, hay que validar si se incluyen o no los campos de sistema
		//if ($bGetRecordset) {
		//	$objQuery->onlyFields(!$bIncludeSystemFields);
		//}
		
		//Genera el filtro con los campos que recibieron algún valor en los parámetros si es que se recibió un array, de lo contrario lo
		//procesa como si fuera un String
		if (is_array($aFieldsFilters)) {
			$strAnd = '';
			foreach ($arreBavelFields as $inteBavelFieldID => $objeBavelField)
			{
				if (isset($aFieldsFilters[$inteBavelFieldID]) && $aFieldsFilters[$inteBavelFieldID] != 'sv_ALL_sv') {
					$strAttributeFieldName = $objeBavelField['id'];
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					$objQuery->where($objeBavelField['id'], '=', $aFieldsFilters[$inteBavelFieldID], $strAnd);
					$strAnd = 'AND';
				}
			}
		}
		else {
			if ($aFieldsFilters != '') {
				@$objQuery->whereRaw($aFieldsFilters);
			}
		}
		
		//Obtiene los datos con los filtros aplicados hasta este punto
		$sql = '';
		if (getParamValue('DebugBreak', 'both', '(int)', true) && method_exists($objQuery, 'compileQuery')) {
			//@JAPR 2014-08-19: Agregada esta asignación por compatibilidad con eBavel a partir de cambios de esta fecha
			//@JAPR 2018-02-12: Corregido un bug, no estaba extendiendo las clases utilizadas por los destinos de datos de preguntas tipo actualizar destino en eForms (#77WD5J)
			//Para permitir compatibilidad del código entre eBavel 5 y eBavel 6, se buscará una nueva variable global definida en el archivo de eBavel
			//service.php la cual indica la versión del servicio de eBavel, si se detecta que es eBavel 6, se necesitará generar un objeto BITAMRepository
			//de eBavel ya que es diferente al de eForms, además de asignar dicho objeto a la clase Metadata de eBavel, pues de lo contrario algunos
			//procesos como el llamado a getModel fallarán cuando intente cargar otros elementos de eBavel
			global $eBavelServiceVersion;
			global $theUser;
			if ( isset($eBavelServiceVersion) && ((int)$eBavelServiceVersion) >= 6 ) {
				$aneBavelRepository = \eBavel\BITAMRepository::OpenRepository(trim($_SESSION["PABITAM_RepositoryName"]), $theUser->UserName, BITAMDecryptPassword($theUser->Password));
				Metadata::setRepository($aneBavelRepository);
			}
			else {
				if (class_exists('Metadata')) {
					@Metadata::setRepository($aRepository);
				}
			}
			//@JAPR
			
			$sql = (string) @$objQuery->compileQuery($aRepository->ADOConnection);
			echo("<br>\r\nQuery: {$sql}<br>\r\n");
		}
		
		//Obtiene los valores de eBavel
		$aRS = @$objQuery->get($aRepository->ADOConnection);
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		if (is_null($aRS) || $aRS === false)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nThere was an error while extracting eBavel data: ");
			}
			return null;
		}
		
		//Procesa los valores obtenidos
		if (property_exists($aRS, 'EOF')) {
			switch ($iReturnType) {
				case ebdtJSonForApps:
				case ebdtJSonArrayForApps:
					//En este caso se va a generar un JSon preparado para utilizarse en las Apps de eForms
					$strResult = array();
					$arrValuesByFieldEmpty = array();
					foreach ($arrFieldIDs as $inteBavelFieldID => $strFieldIDToGet) {
						$arrValuesByFieldEmpty[$inteBavelFieldID] = null;
					}
					
					//Se iniciará el número de row en 1 para forzar a que el app lo reciba como un object y así poder aplicar $.extend para permitir
					//sobreescribir los datos que va cambiando el App durante la captura
					$intIndex = 1;
					while (!$aRS->EOF) {
						$values = array();
						$arrValuesByField = $arrValuesByFieldEmpty;
						foreach ($arrFieldIDs as $inteBavelFieldID => $strFieldIDToGet) {
							$value = @$aRS->fields[$strFieldIDToGet];
							if (!is_null($value)) {
								//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
								$value = $value;
							}
							$arrValuesByField[$inteBavelFieldID] = $value;
						}
						
						$strResult[(string) $intIndex++] = $arrValuesByField;
						$aRS->MoveNext();
					}
					
					if ($iReturnType == ebdtJSonForApps) {
						$strResult = json_encode($strResult);
					}
					break;
				
				case ebdtSectionFilters:
				default:
					//El comportamiento default es generar el string aplicable a los filtros de secciones del Admin
					$arrValuesByField = array();
					foreach ($arrFieldIDs as $inteBavelFieldID => $strFieldIDToGet) {
						$arrValuesByField[$inteBavelFieldID] = array();
					}
					
					while (!$aRS->EOF) {
						$values = array();
						foreach ($arrFieldIDs as $inteBavelFieldID => $strFieldIDToGet) {
							$value = @$aRS->fields[$strFieldIDToGet];
							$arrValuesByField[$inteBavelFieldID][] = $value;
						}
						$aRS->MoveNext();
					}
					
					foreach ($arrValuesByField as $inteBavelFieldID => $values) {
						if($strResult!="")
						{
							$strResult.="_SV_AT_SV_";
						}
						
						$strResult.=$inteBavelFieldID."_SV_ID_SV_";
						$strResult.=implode("_SV_VAL_SV_", $values);
					}
					break;
			}
		}
	} catch (Exception $e) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nThere was an error while extracting eBavel data: ".$e->getMessage());
		}
		return null;
	}
	
	return $strResult;
}

//Regresa la lista de Aplicaciones del eBavel dados los parámetros
function GetEBavelApplications($aRepository, $bUseKeysAsIndex = true, $arrIDs = null) {
	$arrObjects = array();
	
	if (is_null($aRepository)) {
		return $arrObjects;
	}
	if (!is_null($arrIDs) && !is_array()) {
		$arrIDs = array($arrIDs);
	}
	$bUseKeysAsIndex = (bool) @($bUseKeysAsIndex == true);
	
	$strWhere = '';
	if (!is_null($arrIDs)) {
		$strIDs = implode(', ', $arrIDs);
		if (trim($strIDs) != '') {
			$strWhere = ' AND t1.id_app IN ('.$strIDs.') ';
		}
	}
	$sql = "SELECT t1.id_app, t1.applicationName, t1.codeName 
		FROM SI_FORMS_APPS t1 
		WHERE t1.id_app > 1 $strWhere 
		ORDER BY t1.applicationName";
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS) {
        while(!$aRS->EOF) {
            $intItemKey = (int) @$aRS->fields["id_app"];
            $arrItem = array();
            $arrItem['key'] = $intItemKey;
            $arrItem['id_app'] = $intItemKey;
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            $arrItem['applicationName'] = (string) @$aRS->fields["applicationname"];
            $arrItem['codeName'] = (string) @$aRS->fields["codename"];
            if ($bUseKeysAsIndex) {
            	$arrObjects[$intItemKey] = $arrItem;
            }
            else {
            	$arrObjects[] = $arrItem;
            }
            $aRS->MoveNext();
        }
    }
	
	return $arrObjects;
}

//Regresa la lista de Formas del eBavel dados los parámetros
//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
//Agregado el parámetro $bUseStrID para indicar que el parámetro $arrIDs en lugar de corresponder con id_section corresponderá con el campo id, el cual es un ID tipo alfanumérico usado en v6
function GetEBavelSections($aRepository, $bUseKeysAsIndex = true, $appId = null, $arrIDs = null, $bUseStrID = false) {
	$arrObjects = array();
	
	if (is_null($aRepository)) {
		return $arrObjects;
	}
	if (!is_null($arrIDs) && !is_array($arrIDs)) {
		$arrIDs = array($arrIDs);
	}
	$bUseKeysAsIndex = (bool) @($bUseKeysAsIndex == true);
	
	$strWhere = '';
	$strAnd = 'WHERE ';
	if (!is_null($appId)) {
		$strWhere .= $strAnd.' t1.appId = '.$appId.' ';
		$strAnd = 'AND ';
	}
	if (!is_null($arrIDs)) {
		//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
		if ($bUseStrID) {
			foreach ($arrIDs as $intIndex => $strFormID) {
				$arrIDs[$intIndex] = $aRepository->ADOConnection->Quote($strFormID);
			}
			$strIDs = implode(', ', $arrIDs);
			if (trim($strIDs) != '') {
				$strWhere .= $strAnd.' t1.id IN ('.$strIDs.') ';
				$strAnd = 'AND ';
			}
		}
		else {
			$strIDs = implode(', ', $arrIDs);
			if (trim($strIDs) != '') {
				$strWhere .= $strAnd.' t1.id_section IN ('.$strIDs.') ';
				$strAnd = 'AND ';
			}
		}
		//@JAPR
	}
	$sql = "SELECT t1.appId, t1.id_section, t1.id, t1.name, t1.sectionName 
		FROM SI_FORMS_SECTIONS t1 
		$strWhere 
		ORDER BY t1.appId, t1.sectionName";
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS) {
        while(!$aRS->EOF) {
            $intItemKey = (int) @$aRS->fields["id_section"];
            $arrItem = array();
            $arrItem['key'] = $intItemKey;
            $arrItem['id_section'] = $intItemKey;
            $arrItem['appId'] = (int) @$aRS->fields["appid"];
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
            $arrItem['id'] = (string) @$aRS->fields["id"];
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            $arrItem['sectionName'] = (string) @$aRS->fields["sectionname"];
            $arrItem['name'] = (string) @$aRS->fields["name"];
            if ($bUseKeysAsIndex) {
            	$arrObjects[$intItemKey] = $arrItem;
            }
            else {
            	$arrObjects[] = $arrItem;
            }
            $aRS->MoveNext();
        }
    }
	
	return $arrObjects;
}
/*@AAL 19/03/2015: Función Modificada para que regresa la lista de Vistas de eBavel*/
function GetEBavelViews($aRepository, $bUseKeysAsIndex = true, $appId = null, $arrIDs = null) {
	$arrObjects = array();
	
	if (is_null($aRepository)) {
		return $arrObjects;
	}
	if (!is_null($arrIDs) && !is_array($arrIDs)) {
		$arrIDs = array($arrIDs);
	}
	$bUseKeysAsIndex = (bool) @($bUseKeysAsIndex == true);
	   
    $strWhere = '';
	$strAnd = '';
	if (!is_null($appId)) {
		$strWhere .= 'AND t1.appId = '.$appId.' ';
		$strAnd = 'AND ';
	}
	if (!is_null($arrIDs)) {
		$strIDs = implode(', ', $arrIDs);
		if (trim($strIDs) != '') {
			$strWhere .= $strAnd.' t1.id_view IN ('.$strIDs.') ';
			$strAnd = 'AND ';
		}
	}
    
   /*@AAL 19/03/2015: Adicionalmente se obtiene el Section_ID empleado en las clases BITAMeBavelCatalogValue */   
	$sql = "SELECT t1.appId, t1.id_view, t1.id, t2.id AS id_Section, t1.name, t1.label FROM SI_FORMS_VIEWS t1, SI_FORMS_SECTIONS t2 
            WHERE t1.section_id = t2.id_section $strWhere ORDER BY t1.appId, t1.label";
       
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS) {
        while(!$aRS->EOF) {
            $intItemKey = (int) @$aRS->fields["id_view"];
            $arrItem = array();
            $arrItem['key'] = $intItemKey;
            $arrItem['id_view'] = $intItemKey;
            $arrItem['appId'] = (int) @$aRS->fields["appid"];
			//@JAPR 2014-05-13: Agregados los catálogos tipo eBavel
            $arrItem['id'] = (string) @$aRS->fields["id"];
            $arrItem['id_Section'] = (string) @$aRS->fields["id_Section"];
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
            $arrItem['label'] = (string) @$aRS->fields["label"];
            $arrItem['name'] = (string) @$aRS->fields["name"];
            if ($bUseKeysAsIndex) {
            	$arrObjects[$intItemKey] = $arrItem;
            }
            else {
            	$arrObjects[] = $arrItem;
            }
            $aRS->MoveNext();
        }
    }
	
	return $arrObjects;
}
//@AAL

/* Regresa la lista de Campos del eBavel dados los parámetros
El parámetro $arrFilters permitirá agregar cualquier filtro personalizado a la consulta. Consiste de pares "FieldName" -> FilterValue, y
opcionalmente se puede incluir un FilterCondition, si no se incluye se asume que se trata de un IN | = del contenido de FilterValue. Se pueden
definir filtros adicionales usando índices o literales en el array de filtros como sigue:
	0 ==> FieldName
	1 ==> FieldValue
	2 ==> FilterCondition
ejemplo:
		$arrFilter = array ('CLA_USUARIO', 34, '<>');
	es equivalente a:
		$arrFilter = array ('FieldName => 'CLA_USUARIO', 'FieldValue => 34, 'FilterCondition' => '<>');
	o a:
		$arrFilter = array ('CLA_USUARIO', 'FieldValue => 34, 2 => '<>');	(el primer índice toma el valor de 0, así que se puede omitir)
*/
function GetEBavelFieldsForms($aRepository, $bUseKeysAsIndex = true, $appId = null, $arrIDs = null, $arrFilters = null) {
	$arrObjects = array();
	
	if (is_null($aRepository)) {
		return $arrObjects;
	}
	if (!is_null($arrIDs) && !is_array($arrIDs)) {
		$arrIDs = array($arrIDs);
	}
	$bUseKeysAsIndex = (bool) @($bUseKeysAsIndex == true);
	
	//@JAPR 2013-04-18: Corregido un bug, no se estaba calculando el query aplicando diferentes joins, además se cambió para hacer un join permanente
	//a la tabla de formas y así poder extraer el ApplicationId
	$strWhere = 'WHERE t1.section_id = t2.id_section ';
	$strAnd = 'AND ';
	$strFrom = '';
	if (!is_null($appId)) {
		$strWhere .= $strAnd.' t2.appId = '.$appId.' ';
		$strAnd = 'AND ';
	}
	if (!is_null($arrIDs)) {
		$strIDs = implode(', ', $arrIDs);
		if (trim($strIDs) != '') {
			$strWhere .= $strAnd.' t1.section_id IN ('.$strIDs.') ';
			$strAnd = 'AND ';
		}
	}
	
	$strFilter = '';
	if (!is_null($arrFilters) && is_array($arrFilters)) {
		foreach ($arrFilters as $aFilterDef) {
			if (is_array($aFilterDef) && count($aFilterDef) > 0) {
				$strFieldName = (isset($aFilterDef['FieldName']))?$aFilterDef['FieldName']:((isset($aFilterDef[0]))?$aFilterDef[0]:'');
				$strFieldValue = (isset($aFilterDef['FieldValue']))?$aFilterDef['FieldValue']:((isset($aFilterDef[1]))?$aFilterDef[1]:'');
				$strFilterCond = (isset($aFilterDef['FilterCondition']))?$aFilterDef['FilterCondition']:((isset($aFilterDef[2]))?$aFilterDef[2]:'');
				if (trim($strFilterCond) == '') {
					if (strpos($strFieldValue, ',') !== false) {
						$strFilterCond = ' IN ';
					}
					else {
						$strFilterCond = ' = ';
					}
				}
				
				if ($strFilterCond != '' && $strFieldName != '') {
					if (trim($strFilterCond) == 'IN') {
						$strFilterCond .= '(';
						$strFieldValue .= ')';
					}
					$strFilter .= $strAnd.$strFieldName.$strFilterCond.$strFieldValue;
					$strAnd = 'AND ';
				}
			}
		}
	}
	/*@AAL 19/03/2015: Modificado para no seleccionar los campos calculados, puesto que el valor de estos se calculan al mstrarse en la vista*/
	//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
	//Agregada la lectura del campo keyfield
	$sql = "SELECT t2.appId, t1.section_id, t1.id_fieldform, t1.id, t1.label, t1.tagname, t1.keyfield 
		FROM SI_FORMS_FIELDSFORMS t1, SI_FORMS_SECTIONS t2
		$strWhere $strFilter AND t1.tagname <> 'widget_calculatedfield' 
        ORDER BY t2.appId, t1.section_id, t1.label, t1.id_fieldform";
            
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS) {
        while(!$aRS->EOF) {
            $intItemKey = (int) @$aRS->fields["id_fieldform"];
            $arrItem = array();
            $arrItem['key'] = $intItemKey;
            $arrItem['id_fieldform'] = $intItemKey;
            $arrItem['appId'] = (int) @$aRS->fields["appid"];
            $arrItem['section_id'] = (int) @$aRS->fields["section_id"];
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            $arrItem['id'] = (string) @$aRS->fields["id"];
            $arrItem['label'] = (string) @$aRS->fields["label"];
            $arrItem['tagname'] = (string) @$aRS->fields["tagname"];
			//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
            $arrItem['keyfield'] = (int) @$aRS->fields["keyfield"];
			//@JAPR
            if ($bUseKeysAsIndex) {
            	$arrObjects[$intItemKey] = $arrItem;
            }
            else {
            	$arrObjects[] = $arrItem;
            }
            $aRS->MoveNext();
        }
    }
	
	return $arrObjects;
}
/*@AAL 19/03/2015: Función Modificada para obtener la lista de FieldList de las Vistas de eBavel*/
function GetEBavelFieldsViews($aRepository, $bUseKeysAsIndex = true, $appId = null, $arrIDs = null, $arrFilters = null) {
	$arrObjects = array();
	
	if (is_null($aRepository)) {
		return $arrObjects;
	}
	if (!is_null($arrIDs) && !is_array($arrIDs)) {
		$arrIDs = array($arrIDs);
	}
	$bUseKeysAsIndex = (bool) @($bUseKeysAsIndex == true);
	   
    $strWhere = '';
	$strAnd = '';
	if (!is_null($appId)) {
		$strWhere = ' t2.appId = ' . $appId;
		$strAnd = ' AND ';
	}
	if (!is_null($arrIDs)) {
		$strIDs = implode(', ', $arrIDs);
		if (trim($strIDs) != '')
			$strWhere .= $strAnd . ' t1.view_id IN ('.$strIDs.') ';
	}
	
	$strFilter = '';
	if (!is_null($arrFilters) && is_array($arrFilters)) {
		foreach ($arrFilters as $aFilterDef) {
			if (is_array($aFilterDef) && count($aFilterDef) > 0) {
				$strFieldName = (isset($aFilterDef['FieldName']))?$aFilterDef['FieldName']:((isset($aFilterDef[0]))?$aFilterDef[0]:'');
				$strFieldValue = (isset($aFilterDef['FieldValue']))?$aFilterDef['FieldValue']:((isset($aFilterDef[1]))?$aFilterDef[1]:'');
				$strFilterCond = (isset($aFilterDef['FilterCondition']))?$aFilterDef['FilterCondition']:((isset($aFilterDef[2]))?$aFilterDef[2]:'');
				if (trim($strFilterCond) == '') {
					if (strpos($strFieldValue, ',') !== false) {
						$strFilterCond = ' IN ';
					}
					else {
						$strFilterCond = ' = ';
					}
				}
				
				if ($strFilterCond != '' && $strFieldName != '') {
					if (trim($strFilterCond) == 'IN') {
						$strFilterCond .= '(';
						$strFieldValue .= ')';
					}
					$strFilter .= $strAnd.$strFieldName.$strFilterCond.$strFieldValue;
					$strAnd = 'AND ';
				}
			}
		}
	}  
	
	/*@AAL 19/03/2015: Modificada para obtener el tagName de los campos de la forma(FIELDSFORMS) relacionados a los campos de las vistas(FIELDSLISTS) */
    $sql = "SELECT t2.appId, t1.view_id, t1.id_fieldlist, t1.id, t1.label, t3.tagname FROM SI_FORMS_FIELDSLISTS t1
            INNER JOIN SI_FORMS_VIEWS t2 ON t1.view_id = t2.id_view
            INNER JOIN SI_FORMS_FIELDSFORMS t3 ON t1.id = t3.id
            WHERE {$strWhere} ORDER BY t2.appId, t1.view_id, t1.label, t1.id_fieldlist";
    
    $aRS = $aRepository->ADOConnection->Execute($sql);
    if ($aRS) {
        while(!$aRS->EOF) {
            $intItemKey = (int) @$aRS->fields["id_fieldlist"];
            $arrItem = array();
            $arrItem['key'] = $intItemKey;
            $arrItem['id_fieldlist'] = $intItemKey;
            $arrItem['appId'] = (int) @$aRS->fields["appid"];
            $arrItem['view_id'] = (int) @$aRS->fields["view_id"];
	    	//@JAPR 2013-12-13: Corregido un bug, de cierta fecha en adelante, ahora eBavel ya no graba como utf-8 sus strings
            $arrItem['id'] = (string) @$aRS->fields["id"];
            $arrItem['label'] = (string) @$aRS->fields["label"];
            $arrItem['tagname'] = (string) @$aRS->fields["tagname"];
            if ($bUseKeysAsIndex) {
            	$arrObjects[$intItemKey] = $arrItem;
            }
            else {
            	$arrObjects[] = $arrItem;
            }
            $aRS->MoveNext();
        }
    }

	return $arrObjects;
}
//@AAL

/* Dada una colección obtenida con un método Get de este mismo archivo, regresa un array con índices usando el campo key y con valores usando el
campo especificado en $aFieldName, la cual puede ser utilizada para FormFields de las ventanas de eForms. Si no se especifica $aFieldName entonces
se regresará el elemento completo del array de datos
Adicionalmente se puede especificar el campo $aKeyFieldName para indicar que propiedad de los objetos se debe usar como el campo key, si se recibe
cadena vacia, significaría que se quiere regresar la colección con índices numéricos
También se puede indicar opcionalmente, si esta
colección se encontrará agrupada, en cuyo caso se regresa un array multidimensional donde la 1er dimensión es el grupo (especificado por la
propiedad del campo $aGroupFieldName) y la 2da dimensión ya es la colección de objetos del tipo pedido para dicho grupo. Sólo se permite agrupar
hasta un nivel
//@JAPR 2014-05-20: Corregido un bug, cuando el parámetro $aKeyFieldName si se envía, ya no podían regresarse índices numéricos en el valor de None
así que en algunos casos fallaba, para corregirlo se agregó el parámetro $aDefaultNoneID para indicar que valor usar como índice de dicho elemento
*/
function GetEBavelCollectionByField($anEBavelCollection, $aFieldName, $aKeyFieldName = "key", $includeNone = false, $aGroupFieldName = '', $aDefaultNoneID = '') {
	$arrItems = array();
	$aGroupFieldName = trim($aGroupFieldName);
	if (is_null($aKeyFieldName)) {
		$aKeyFieldName = 'key';
	}
	if ($includeNone) {
		if ($aGroupFieldName == '') {
			if ($aKeyFieldName == '') {
				$arrItems[0] = '('.translate('None').')';
			}
			else {
				$arrItems[$aDefaultNoneID] = '('.translate('None').')';
			}
		}
		else {
			$arrItems = array($aDefaultNoneID => array($aDefaultNoneID => '('.translate('None').')'));
		}
	}
	
	try {
		foreach ($anEBavelCollection as $anItem) {
			//@JAPR 2013-04-18: Modificada para permitir regresar el objeto completo en lugar de solo una descripción
			if ($aFieldName != '') {
				$objData = $anItem[$aFieldName];
			}
			else {
				$objData = $anItem;
			}
			
			if ($aGroupFieldName == '') {
				if ($aKeyFieldName == '') {
					$arrItems[] = $objData;
				}
				else {
					$arrItems[$anItem[$aKeyFieldName]] = $objData;
				}
			}
			else {
				$strGroupValue = $anItem[$aGroupFieldName];
				if (!isset($arrItems[$strGroupValue])) {
					$arrItems[$strGroupValue] = array();
					//@JAPR 2013-07-04: Corregido un bug, faltaba agregar el elemento vacio a cada array agrupado
					if ($includeNone) {
						if ($aKeyFieldName == '') {
							$arrItems[$strGroupValue][0] = '('.translate('None').')';
						}
						else {
							$arrItems[$strGroupValue][$aDefaultNoneID] = '('.translate('None').')';
						}
					}
					//@JAPR
				}
				if ($aKeyFieldName == '') {
					$arrItems[$strGroupValue][] = $objData;
				}
				else {
					$arrItems[$strGroupValue][$anItem[$aKeyFieldName]] = $objData;
				}
			}
		}
	} catch (Exception $e) {
		//Ignora el error, simplemente regresa un array vacio
		return array();
	}
	
	return $arrItems;
}

//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
function convertEBavelToEFormsFieldType($aneBavelTagName) {
	$aneBavelTagName = (string) $aneBavelTagName;
	
	$intQTypeID = qtpInvalid;
	switch (strtolower($aneBavelTagName)) {
		//Estos tipos de campos no son soportados o tienen funcionalidad especial en eBavel que no se debe intentar mapear en eForms
		case 'widget_secheader':
			//@JAPR 2013-05-02: Agregado el tipo de pregunta inválido, porque algunos tipos negativos son dummies para soporte de eBavel
			$intQTypeID = qtpInvalid;
			break;
		
		case 'widget_checkbox_multiple-horizontal':
		case 'widget_checkbox_multiple-vertical':
			$intQTypeID = qtpMulti;
			break;
		
		case 'widget_selectlist':
		case 'widget_radiobutton_single-horizontal':
		case 'widget_radiobutton_single-vertical':
		//Este es un caso especial, este tipo de dato define una Simple Choice pero de tipo catálogo, así que debería permitir elegir el
		//catálogo correspondiente, aunque eso es algo que en eForms se tiene que configurar manualmente por ahora, así que se regresará como
		//si fuera Simple Choice normal
		case 'widget_selectionhierarchy':
			$intQTypeID = qtpSingle;
			break;
		
		case 'widget_file':
			$intQTypeID = qtpDocument;
			break;
			
		case 'widget_field_date':
			$intQTypeID = qtpOpenDate;
			break;
		
		case 'widget_field_time':
			$intQTypeID = qtpOpenTime;
			break;
			
		case 'widget_field_numeric':
			$intQTypeID = qtpOpenNumeric;
			break;
		
		case 'widget_textarea':
			$intQTypeID = qtpOpenString;
			break;
		
		case 'widget_field_email':
			$intQTypeID = qtpEMail;
			break;
		
		case 'widget_geolocation':
			$intQTypeID = qtpLocation;
			break;
		
		case 'widget_qr':
		case 'widget_photo':
			$intQTypeID = qtpPhoto;
			break;
		
		case 'widget_signature':
			$intQTypeID = qtpSignature;
			break;
			
		case 'widget_field_alphanumeric':
		default:
			//Cualquier tipo de campo de eBavel mas reciente será tratado como alfanumérico por eForms
			$intQTypeID = qtpOpenAlpha;
			break;
	}
	
	return $intQTypeID;
}

//@MABH 2014-06-18
function GetEBavelFieldForm ($aRepository, $opts) {
	$fieldformID = @$opts['fieldformID'];
	$sql = "SELECT * FROM SI_FORMS_FIELDSFORMS WHERE id_fieldform = ".$fieldformID;
	$aRS = $aRepository->ADOConnection->Execute($sql);
	$fieldFormObj = array();
	if ($aRS) {
		foreach(@$aRS->fields as $key => $value) {
			$fieldFormObj[$key] = $value;
		}
	}
	return fieldFormObj;
}
//@MABH

/*@AAL 19/03/2015: Modificada para regresar un array multidimensional de los campos de eBavel mapeados según el tipo de pregunta de eForms*/
function GetMappedEBavelFieldsForms($aRepository, $bUseKeysAsIndex = true, $appId = null, $arrIDs = null, $arrFilters = null, $includeNone = false, $aFieldName = 'label') {
	
    try{
    	//@AAL Obtiene un array de Fields dependiendo si se trata de una Forma o una Vista
        if($aFieldName == "Forms")
            $arreBavelFields = GetEBavelCollectionByField(@GetEBavelFieldsForms($aRepository, $bUseKeysAsIndex, $appId, $arrIDs, $arrFilters), '', 'key', false, 'tagname');
        elseif($aFieldName == "Views")
            $arreBavelFields = GetEBavelCollectionByField(@GetEBavelFieldsViews($aRepository, $bUseKeysAsIndex, $appId, $arrIDs, $arrFilters), '', 'key', false, 'tagname');
        else
            $arreBavelFields = GetEBavelCollectionByField(@GetEBavelFieldsForms($aRepository, $bUseKeysAsIndex, $appId, $arrIDs, $arrFilters), $aFieldName, 'key', false, 'tagname');
            
    } catch(Exception $e){
    	$arreBavelFields = array();
    }
    
	//Mapea los campos de eBavel utilizando QTypes de eForms
	$arrMappedFieldsForms = array();
	if ($includeNone) {
		$arrMappedFieldsForms = array('0' => array(0 => translate('None')));
	}
	
	foreach ($arreBavelFields as $strTagName => $aFieldsColl) {
		//No se puede usar array_combine porque no se desea reorganizar los índices numéricos de los keys de cada campo en $aFieldsColl
		$intQTypeID = convertEBavelToEFormsFieldType($strTagName);
		
		//Si es un tipo de pregunta inválido continua con los siguientes campos
		//@JAPR 2013-05-02: Agregado el tipo de pregunta inválido, porque algunos tipos negativos son dummies para soporte de eBavel
		//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
		//El tipo de campo qtpLocation se usó originalmente para identificar a campos tipo Geolocalización antes de que existieran las preguntas tipo
		//GPS en eForms, así que por compatibilidad hacia atrás se respetará dicho valor, sin embargo el tipo de campo qtpGPS creado precisamente
		//para las preguntas tipo GPS de eForms, se debe tratar igual que qtpLocation cuando es regresado por esta y otras funciones
		if ($intQTypeID == qtpInvalid) {
			continue;
		}
		
		//Crea el array de este tipo y agrega los campos
		if (!isset($arrMappedFieldsForms[$intQTypeID])) {
			if ($includeNone) {
				$arrMappedFieldsForms[$intQTypeID] = array(0 => translate('None'));
			}
			else {
				$arrMappedFieldsForms[$intQTypeID] = array();
			}
		}
		
		foreach ($aFieldsColl as $intKey => $aField) {
			$arrMappedFieldsForms[$intQTypeID][$intKey] = $aField;
			//@JAPR 2016-09-06: Agregado el tipo de dato ajustado a eForms para no tener que calcularlo cada vez que se utilice para una comparación
			$arrMappedFieldsForms[$intQTypeID][$intKey]['qtypeid'] = $intQTypeID;
			//@JAPR
		}
	}
	
	return $arrMappedFieldsForms;
}
?>