<?php

class BITAMFormField
{
	public $Name;
	public $Title;
	public $Type;
	public $Size;
	public $VisualComponent;
	public $FormatMask;
	public $Value;
	public $Options;
	public $OptionsSize;
	public $IsDisplayOnly;
	public $IsVisible;
	public $Parent;
	public $Children;
	public $OnChange;
	public $BeforeMessage;
	public $AfterMessage;
	public $FullRow;
	public $InTitle;
	public $CloseCell;
	public $SubmitOnChange;
	public $BorderRight;
	public $CodeBeforeField;
	public $IsLink;
	public $CustomAnchor;
	public $BackgroundColor;
	public $FixedWidth;
	public $InsertRowsBeforeField;
	public $IsSingleDimensionalOptionsArray;
	//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (sólo para BITAMCollection)
	public $Image;
	//@JAPR 2014-06-17: Agregada la propiedad para agregar estilos personalizados a los campos
	public $CustomizedClass;
	//@JAPR
	public $ToolTip;
	public $TextAlign;
	public $DefaultNumberZero;
	//@JAPR 2014-08-04: Agregado el Responsive Design
	public $MultiDeviceNames;					//Array utilizado originalmente en campos tipo LargeStringHTML para agregar un componente por cada "sufijo" especificado aquí, que en la práctica eran los diferentes tipos de componentes
	public $MultiDeviceLabels;					//Array con las etiquetas correspondientes a los fufijos de MultiDeviceNames (los sufijos son el complemente del nombre interno de los campos, las etiquetas son sólo el nombre visible al usuario)
	//HTML Designer
	public $UseHTMLDes;
	public $CompToAdd;
	public $CompToAddDes;

	function __construct()
	{
		$this->Name = "";
		$this->Title = "";
		$this->Type = "";
		$this->Size = 0;
		$this->VisualComponent = "";
		$this->FormatMask = "";
		$this->Value = null;
		$this->Options = array();
		$this->OptionsSize = 4;
		$this->IsDisplayOnly = false;
		$this->IsVisible = true;
		$this->Parent = null;
		$this->Children = array();
		$this->OnChange = "";
		$this->BeforeMessage = "";
		$this->AfterMessage = "";
		$this->FullRow = true;
		$this->InTitle = false;
		$this->CloseCell = true;
		$this->SubmitOnChange = false;
		$this->BorderRight = false;
		$this->CodeBeforeField = "";
		$this->IsLink = true;
		$this->CustomAnchor = '';
		$this->BackgroundColor = null;
		$this->FixedWidth = null;
		$this->InsertRowsBeforeField = array();
		$this->IsSingleDimensionalOptionsArray = true;
		$this->Image = '';
		$this->ToolTip = '';
		$this->TextAlign = '';
		$this->DefaultNumberZero = true;
		//@JAPR 2014-06-17: Agregada la propiedad para agregar estilos personalizados a los campos
		$this->CustomizedClass = '';
		//@JAPR 2014-08-04: Agregado el Responsive Design
		$this->MultiDeviceNames = array();
		$this->MultiDeviceLabels = array();
		//HTML Designer
		$this->UseHTMLDes = 0;
		$this->CompToAdd = "'LBL,ADD','IMG,ADD','BTN,ADD'";
		$this->CompToAddDes = "ML[157]+'_AWSep_comp_agregar.gif',ML[584]+'_AWSep_rep_etiqueta.gif',ML[361]+'_AWSep_rep_imagen.gif', ML['Button']+'_AWSep_insertButton.png'";
	}
	
	static function NewFormField()
	{
		return new BITAMFormField();
	}
}
?>