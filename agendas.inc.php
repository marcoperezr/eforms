<?php

require_once "dataSource.inc.php";

class BITAMAgendas extends BITAMObject 
{

	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		return eval('return new '.$strCalledClass.'($aRepository);');
	}

	static function NewInstanceWithId( $aRepository, $id )
	{
		$strCalledClass = static::class;
		
		$rs = $aRepository->DataADOConnection->Execute( self::$query . " WHERE id = " . $id );

		if( $rs && !$rs->EOF )
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $rs);	

			return $anInstance;
		}

		return false;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		
		$anInstance = $strCalledClass::NewInstance($aRepository);
		
		//$anInstance->id = (int) $aRS->fields["id"];
		
		

		return $anInstance;
	}

	function save( )
	{
		$bHavSrvConnection = getMDVersion() >= esveServicesConnection;

		

	}

	function remove()
	{
		//$this->Repository->DataADOConnection->Execute("DELETE FROM si_sv_datadestinations WHERE id = " . $this->id);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Data destination");
		}
		else
		{
			return $this->name;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Agendas";
		}
		else
		{
			return "BITAM_PAGE=Agendas&id=".$this->id;
		}
	}

	function getJSonDefinition()
	{
		return array();
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("id", $aHTTPRequest->POST))
		{
			$ids = $aHTTPRequest->POST["id"];
			if (is_array($ids))
			{
				
				$aCollection = BITAMeFormsDataDestinationsCollection::NewInstance($aRepository, $ids);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
		}

	}
}

class BITAMCheckpoint extends BITAMObject 
{
	/**
	 * Id del registro
	 * @var integer
	 */
	public $id;

	/**
	 * Nombre de la ruta de visita
	 * @var string
	 */
	public $name;

	/**
	 * Llave del catalogo de donde se sacaran los puntos de visita
	 * @var integer
	 */
	public $catalog_id;

	/**
	 * Lista de atributos y sus tipos
	 * @var array
	 */
	public $attributes = [];

	public static $query = "SELECT `id`, `name`, `catalog_id` FROM si_sv_checkpoint";

	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		return eval('return new '.$strCalledClass.'($aRepository);');
	}

	static function NewInstanceWithId( $aRepository, $id )
	{
		$strCalledClass = static::class;
		
		$rs = $aRepository->DataADOConnection->Execute( self::$query . " WHERE id = " . $id );

		if( $rs && !$rs->EOF )
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $rs);	

			return $anInstance;
		}

		return false;
	}

	static function NewInstanceFromRS( $aRepository, $rs )
	{
		$anInstance = self::NewInstance( $aRepository );

		$anInstance->id = $rs->fields['id'];
		
		$anInstance->name = $rs->fields['name'];
		
		$anInstance->catalog_id = $rs->fields['catalog_id'];

		/** Cargamos los atributos */
		$rs = $aRepository->DataADOConnection->Execute('SELECT `attr_id`, `type` FROM `si_sv_checkpointattr` WHERE `checkpoint_id` =' . $anInstance->id);

		while ($rs && !$rs->EOF)
		{
			$anInstance->attributes[] = [ 'id' => $rs->fields['attr_id'], 'type' => $rs->fields['type']  ];
			$rs->MoveNext();
		}

		return $anInstance;
	}

	public function save()
	{
		$name = $this->Repository->DataADOConnection->Quote( $this->name );

		if( $this->isNewObject() )
		{
			$sSQL = "INSERT INTO si_sv_checkpoint (`name`, `catalog_id`) VALUES ( {$name}, {$this->catalog_id} )";
		} else 
		{
			$sSQL = "UPDATE si_sv_checkpoint SET `name` = {$name}, `catalog_id` = {$this->catalog_id} WHERE id = " . $this->id;
		}

		$this->Repository->DataADOConnection->Execute( $sSQL );

		if( $this->isNewObject() ) $this->id = $this->Repository->DataADOConnection->_insertid();

		/** Borramos toda la lista de atributos de este checkpoint para volver a insertarla */
		$this->Repository->DataADOConnection->Execute( 'DELETE FROM si_sv_checkpointattr WHERE checkpoint_id = ' . $this->id );

		/** Recorremos la lista de atributos a insertar */
		for( $i = 0; $i < count($this->attributes); $i++ )
		{
			$this->Repository->DataADOConnection->Execute( "INSERT INTO si_sv_checkpointattr (checkpoint_id, attr_id, type) VALUES ( {$this->id}, {$this->attributes[$i]['id']}, {$this->attributes[$i]['type']} )" );
		}
	}

	public function getAttrsKey()
	{
		$res = [];

		for($i=0; $i < count( $this->attributes ); $i++)
		{
			if( $this->attributes[$i]['type'] == 1 )
				$res[] = $this->attributes[$i];
		}

		return $res;
	}

	public function isNewObject()
	{
		return !is_numeric( $this->id ) && $this->id <= 0;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("id", $aHTTPRequest->POST))
		{
			$ids = $aHTTPRequest->POST["id"];
			if (is_array($ids))
			{
				
				$aCollection = BITAMCheckpointCollection::NewInstance($aRepository, $ids);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					if( $anInstanceToRemove->canRemove($aUser) )
						$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
		}
	}

	public function remove()
	{
		$this->Repository->DataADOConnection->Execute( 'DELETE FROM si_sv_checkpointattr WHERE checkpoint_id = ' . $this->id );

		$this->Repository->DataADOConnection->Execute( 'DELETE FROM si_sv_checkpoint WHERE id = ' . $this->id );
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Checkpoint";
		}
		else
		{
			return "BITAM_PAGE=Checkpoint&id=".$this->id;
		}
	}

	public function canRemove( $anUser )
	{
		/** Revisamos si este checkpoint esta siento usado en alguna agenda scheduler */
		$rs = $this->Repository->DataADOConnection->Execute("SELECT count(*) as nCount FROM si_sv_surveyagendascheduler WHERE checkpointID = " . $this->id);

		if( $rs && @$rs->fields['nCount'] )
		{
			return false;
		}

		$rs = $this->Repository->DataADOConnection->Execute("SELECT count(*) as nCount FROM si_sv_surveyagenda WHERE CaptureStartDate >= CURDATE() AND checkpointID = " . $this->id);

		if( $rs && @$rs->fields['nCount'] )
		{
			return false;
		}	

		return true;
	}

	function getJSonDefinition()
	{
		return array();
	}
}

class BITAMCheckpointCollection extends BITAMCollection
{
	static function NewInstance($aRepository, $arrIds = null)
	{
		global $gblShowErrorSurvey;

		$strCalledClass = static::class;
		
		$anInstance = new $strCalledClass($aRepository);

		$sql = BITAMCheckpoint::$query .  ( $arrIds ? " WHERE id IN ( ". implode(",", $arrIds) ." ) " : "" ) ;
		//@JAPR 2016-06-07: Corregido el ordenamiento de los puntos de visita (#VE6X65)
		$sql .= " ORDER BY `name`";
		//@JAPR
		$rs = $aRepository->DataADOConnection->Execute( $sql );

		if ($rs === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_checkpoint ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." si_sv_checkpoint ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		while( $rs && !$rs->EOF )
		{
			$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
			
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $rs);

			$rs->MoveNext();
		}

		return $anInstance;
	}

	function get_CustomButtons()
	{
		return array();
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=" . static::class;
	}

	function get_Title()
	{	
		return translate("Check point");
	}

	function get_PathArray()
	{
		$pathArray = array();
		
		return $pathArray;
	}
}


class BITAMAgendasCollection extends BITAMCollection
{

	public $NoHeader = true;
	
	public $HasPath = false;

	static function NewInstance( $aRepository, $surveyID )
	{
		global $gblShowErrorSurvey;

		$strCalledClass = static::class;

		$anInstance = new $strCalledClass($aRepository);

		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);

		if( is_array($surveyID) )
			$sql = $strCalledClass::$query . ' WHERE id IN (' . implode(',', $surveyID) . ')';
		else
			$sql = $strCalledClass::$query . ' WHERE id_survey = ' . $surveyID;

		$aRS = $anInstance->Repository->DataADOConnection->Execute( $sql );

		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." si_sv_datadestinations ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." si_sv_datadestinations ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}

		while ($aRS && !$aRS->EOF)
		{			
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			
			$aRS->MoveNext();
		}

		return $anInstance;

	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;

		if( $action = getParamValue('action', 'both', '(string)', true) )
		{
			header("Content-Type:application/json; charset=UTF-8");

			$res = ['collection' => []];

			if( $action == 'getcollection' )
			{
				$type = getParamValue('type', 'both', '(int)', true);

				if( $type == 1 )
				{
					$anCollection = BITAMCheckpointCollection::NewInstance( $aRepository );

					for( $i = 0; $i < count( $anCollection->Collection ); $i++ )
					{
						$res['collection'][] = [ 
							"name" => $anCollection->Collection[$i]->name
							, "id" => $anCollection->Collection[$i]->id
							, "canRemove" => $anCollection->Collection[$i]->canRemove( $aUser )
						];
					}
				}

			} else if( $action == "getAttr" )
			{	
				/** Muestra una lista de atributos */
				$res = [];

				if( getParamValue('attrKeys', 'both', '(boolean)', true) )
				{
					$anCheckpoint = BITAMCheckpoint::NewInstanceWithId( $aRepository, getParamValue('catalogid', 'both', '(int)', true) );

					$arrKeys = $anCheckpoint->getAttrsKey();

					$arrAttrsKey = [];

					foreach ($arrKeys as $each)
					{
						$arrAttrsKey[] = $each['id'];
					}

					$arrAttr = BITAMDataSourceMemberCollection::NewInstance($aRepository, $anCheckpoint->catalog_id, $arrAttrsKey);

				} else
				{
					$arrAttr = BITAMDataSourceMemberCollection::NewInstance($aRepository, getParamValue('catalogid', 'both', '(int)', true));
				}

				for($i=0; $i < count( $arrAttr->Collection ); $i++)
				{
					$res[] = ["value" => $arrAttr->Collection[$i]->MemberID, "text" => $arrAttr->Collection[$i]->MemberName];
				}

			} else if( $action == "saveCheckpoint" )
			{

				/** Revisamos si el catalogo ya existe en algun checkpoint */
				$rs = $aRepository->DataADOConnection->Execute('SELECT count(*) as nCount FROM si_sv_checkpoint WHERE id <> '. ( getParamValue('id', 'both', '(int)', true) ? getParamValue('id', 'both', '(int)', true) : 0 ) .' AND catalog_id = ' . getParamValue('catalog', 'both', '(int)', true));
			
				if( $rs && @$rs->fields['nCount'] )
				{
					$res = false;
				} else 
				{
					$anCheckPoint = self::saveCheckpoint( $aRepository );

					$res['collection'][] = ["id" => $anCheckPoint->id, "name" => $anCheckPoint->name];	
				}				
			
			} else if( $action == "getCheckpoint" )
			{
				if( $anInstance = BITAMCheckpoint::NewInstanceWithId( $aRepository, getParamValue('id', 'both', '(int)', true) ) )
				{
					$res = ['id' => $anInstance->id, 'name' => $anInstance->name, 'catalog_id' => $anInstance->catalog_id, 'attributes' => $anInstance->attributes];
				}
			} else if( $action == 'getValueAttrs' )
			{
				
				$res = self::getValueAttrs( $aRepository );

			}

			echo json_encode( $res );

		} else 
		{
			$puntoVisita = translate("Check point");
			$planificador = translate("Scheduler");
			$monitorVisitas = translate("Visits monitor");
			/** inicializamos la interfaz */
			echo '<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/><link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/><link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/><link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>';
			echo '<script src="js/codebase/dhtmlxFmtd.js"></script>';
			echo '<script src="js/libs/require/require.js"></script>
<script>

	var bLayout = window && window.parent && window.parent.objFormsLayoutMenu && window.parent.objFormsLayoutMenu.cells;

	requirejs.config({baseUrl: "js"});

	require(["_requireconfig"], function() {

		if( bLayout ) window.parent.objFormsLayoutMenu.cells("b").progressOn();

		require(["js/views/agendas/agendas.php?", "jquery"], function( Page ) {

			if( bLayout ) window.parent.objFormsLayoutMenu.cells("b").progressOff();

			var page = new Page().setElement($("body"));

			page.render();

			page.displayBtnAdd( false );
			page.displayBtnRemove( false );

			page.options.picture = "images/agendas/checkpoints.png";
			page._addItem( {id: 1, name:"'. $puntoVisita . '", description: ""} );
			page.options.picture = "images/agendas/scheduler.png";
			page._addItem( {id: 2, name:"' . $planificador . '", description: ""} );
			page.options.picture = "images/agendas/monitor.png";
			page._addItem( {id: 3, name:"' . $monitorVisitas . '", description: ""} );
		});
	});
</script>';
		}		

		exit();

		return ;
	}

	static function getValueAttrs( $aRepository )
	{
		if( $anInstance = BITAMCheckpoint::NewInstanceWithId( $aRepository, getParamValue('catalogid', 'both', '(int)', true) ) )
		{

			$arrAttrsKey = [ getParamValue('attrKey', 'both', '(int)', true) ];
			
			$arrFilters = getParamValue('filters', 'both', '(array)', true);

			//$arrAttributes = BITAMDataSource::GetCatalogAttributes($aRepository, $anInstance->catalog_id, 0, $arrAttrsKey);
			$arrAttributes = BITAMDataSourceMemberCollection::NewInstance($aRepository, $anInstance->catalog_id, $arrAttrsKey);

			$objDataSourceMember = $arrAttributes->Collection[0];

			$aDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $anInstance->catalog_id);

			//$aDataSource


			/*$anObjectMember["DataSourceID"]= $objDataSourceMember->DataSourceID;
			$anObjectMember["sourceMemberID"]= $objDataSourceMember->MemberID;
			$anObjectMember["MemberName"]= $objDataSourceMember->MemberName;
			$anObjectMember["FieldName"]
*/
			//Filtro
			//[21=>["Tamaulipas", "San Luis Potosi"], 20 => ['Mexico']];

			//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
			$aCollection = $aDataSource->getAttributeValues([[
				"DataSourceID" => $objDataSourceMember->DataSourceID
				, "sourceMemberID" => $objDataSourceMember->MemberID
				, "MemberName" => $objDataSourceMember->MemberName
				, "FieldName" => $objDataSourceMember->FieldName
			]], -1, ( count( $arrFilters ) ? $arrFilters : '' ), 1, false, false, false, null, false);
			//@JAPR
			$nCount = count( $aCollection );

			$res = [];

			//$res[] = ["text" => translate('All'), "value" => '', 'order' => 0];

			for($i = 0; $i < $nCount; $i++)
			{
				$res[] = ["text" => $aCollection[$i]["value"], "value" =>$aCollection[$i]["value"]];
			}

			return $res;
			
		}

		return [];
	}

	static function saveCheckpoint( $aRepository )
	{
		$anCheckPoint = BITAMCheckpoint::NewInstance( $aRepository );

		if( ($id = getParamValue('id', 'both', '(int)', true)) )
			$anCheckPoint->id = $id;			

		$anCheckPoint->name = getParamValue('name', 'both', '(string)', true);

		$anCheckPoint->catalog_id = getParamValue('catalog', 'both', '(int)', true);

		$arrDescription = getParamValue('description', 'both', '(array)', true);

		for($i=0; $i < count( $arrDescription ); $i++)
		{
			$anCheckPoint->attributes[] = [ 'id' => $arrDescription[$i], 'type' => 2  ];
		}

		$arrKeys = getParamValue('keys', 'both', '(array)', true);

		for($i=0; $i < count( $arrKeys ); $i++)
		{
			$anCheckPoint->attributes[] = [ 'id' => $arrKeys[$i], 'type' => 1  ];
		}

		$anCheckPoint->attributes[] = [ 'id' => getParamValue('latitude', 'both', '(int)', true), 'type' => 3  ];

		$anCheckPoint->attributes[] = [ 'id' => getParamValue('longitude', 'both', '(int)', true), 'type' => 4  ];

		$anCheckPoint->save();

		return $anCheckPoint;
	}

	function generateDetailForm($aUser, $isAssociateDissociateMode = false) {

		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		
		$strScriptPath = $path_parts["dirname"];

		$myFormName = get_class($this);

		echo "here";

	
		exit();

	}

	function get_CustomButtons()
	{
		return array();
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=" . static::class;
	}

	function get_Title()
	{	
		return translate("Agendas");
	}

	function get_PathArray()
	{
		$pathArray = array();
		
		return $pathArray;
	}

}


?>