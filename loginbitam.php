<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
$goto = "";

if (array_key_exists('params', $_GET))
{
	require_once("initialize.php");
	require_once("utils.inc.php");
	require_once("config.php");
	//@JAPR 2016-08-23: Corregido un bug por SBrande, había faltado agregar la referencia a la clase BITAMAppUser para poder usar la funcionalidad del seguridad de Admin (#KY40NF)
	require_once("appuser.inc.php");
	
	//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
	@setAppVersion();
	//@JAPR
	
	$anArrayOfParams = explode("\r\n", BITAMDecode($_GET['params']));

	if (count($anArrayOfParams) > 2)
	{
		$UserName = strtoupper(trim($anArrayOfParams[0]));
		$Password = trim($anArrayOfParams[1]);
		$goto = trim($anArrayOfParams[2]);
		$url_parts = parse_url($goto);
		$query_string = array_key_exists("query", $url_parts) ? $url_parts["query"] : "";
		$query_array = array();
		parse_str($query_string, $query_array);

		global $BITAMRepositories;		
		$theRepositoryName = $query_array["RepositoryName"];
		//@JAPR 2014-07-17: Validación para que al cargar desde un enlace externo que sólo contiene el RepositoryName en los params (lo cual es necesario para que pase por la validación del
		//If siguiente) NO se redireccione al main sin el parámetro adecuado de a que página debe enviarse, ya que en las versiones recientes ahora los usuarios invitados ya no tienen acceso
		//a la colección de encuestas, que era la ventana a la que se redireccionaba por default en caso de no venir la información adecuada de redirección (en el servicio de IUSACELL se hizo
		//muy evidente este problema). Si se envia el parámetro NoRedirect, entonces build_page.php ignorará el parámetro query_string y cargará como si entraran desde el login
		$blnNoRedirect = (int) @$query_array["NoRedirect"];
		//@JAPR
		if (!array_key_exists($theRepositoryName, $BITAMRepositories))
		{
			header("Location: index.php?error=1");
			exit();
		}
		$theRepository = $BITAMRepositories[$theRepositoryName];
		if (!$theRepository->open())
		{
			header("Location: index.php?error=2");
			exit();
		}	
		
		$theUserName = $UserName;
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
		if (is_null($theUser))
		{
			header("Location: index.php?error=3");
			exit();
		}
		
		//@JAPR 2014-11-19: Agregada la dimensión Agenda
		@AutoUpdateRequiredMetadata($theRepository);
		//@JAPR
		
		if (!is_null($theUser))
		{	
			//@JAPR 2009-02-10: Corregido el login desde enlaces extrnos para usuarios Bitam, falta integrar el cambio para usuarios LDAP pero desconozco como llegan los
			//parámetros para esos casos
			if (BITAMDecryptPassword($theUser->Password) != $Password)
			{
				setcookie("PABITAM_RepositoryName", $theRepositoryName);
			    setcookie("PABITAM_UserName", $theUserName);
				header("Location: index.php?error=3");
			}
			else
			{	
				$_SESSION["PAArtusLinker"] = 1;
				$_SESSION["PABITAM_RepositoryName"]=$theRepositoryName;
				$_SESSION["PABITAM_UserName"]=$theUserName;
				$_SESSION["PABITAM_Password"]=$Password;
				$_SESSION["PAstrGoTo"]=$goto;
				$_SESSION["PAdisplayAllFrames"]=1;
				$_SESSION["PABITAM_QueryString"] = $query_string;
				//@JAPR 2014-07-17: Validación para que al cargar desde un enlace externo que sólo contiene el RepositoryName en los params, utilice la carga como en un login normal
				$_SESSION["PABITAM_NoRedirect"] = $blnNoRedirect;
				//@JAPR

				if(isset($query_array["DisplayAllFrames"]))
				{
					if(((int)$query_array["DisplayAllFrames"])==0)
					{
						$_SESSION["PAdisplayAllFrames"]=0;
					}
				}
			}
			
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			//Carga la seguridad del usuario logeado para mantenerla en memoria por si aplica en diferentes ventanas y/o procesos
			//Por el momento dado a que esta seguridad no puede ser modificada desde la interfaz, es factible sólo cargarla una vez y dejarla en la sesión permanentemente, sólo que se indexará
			//utilizando el ID del usuario para que en caso de mezcla de sesiones que no sea por medio de un login, al no encontrar la seguridad por lo menos intentará cargarla de nuevo
			//@JAPR 2016-08-23: Corregido el registro de seguridad por usuario, no estaba considerando al repositorio así que mismos usuarios de diferentes BDs habrían tenido conflicto (#KY40NF)
			//@JAPR 2016-11-04: Modificado para que en este punto que es un registro de login (no se repite constantemente), siempre se cargue la seguridad independientemente de si ya
			//había o no alguna cargada
			//if (!isset($_SESSION["PABITAM_Security"]) || !isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]]) || !isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$theUser->UserID])) {
				if (!isset($_SESSION["PABITAM_Security"])) {
					$_SESSION["PABITAM_Security"] = array();
				}
				
				if (!isset($_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]])) {
					$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]] = array();
				}
				
				$arrSecurity = BITAMAppUser::GetUserSecurity($theRepository, $theUser->UserID);
				if (is_array($arrSecurity)) {
					$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$theUser->UserID] = $arrSecurity;
				}
			//}
			//@JAPR
		}
	}
}

$pageName="index.php";
if(isset($query_array["externalPage"]) && $query_array["externalPage"]==1)
{
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("SVExternalPage");
	$_SESSION["SVExternalPage"] = 1;

	$pageName = $goto;
}
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?
if ($goto != "")
{
?>
<meta http-equiv="refresh" content="0;url=<?=$pageName?>">
<?
}
?>
</head>
<body>
</body>
</html>