<?php

require_once('artusmodelGeoFence.class.php');

function geoFenceCubeGeneration($theRepository, $theAppUser)
{
	//Validar si ya est� creado el cubo
	//Revisar que est� creado el campo
	$aSQL = "ALTER TABLE si_sv_gblsurveymodel ADD COLUMN GblGeoFenceModelID INTEGER NULL";
	$theRepository->DataADOConnection->Execute($aSQL);
	$aSQL = "ALTER TABLE si_sv_gblsurveymodel ADD COLUMN GblGeoFenceModelVersion DOUBLE NULL";
	$theRepository->DataADOConnection->Execute($aSQL);
	$aSQL = "SELECT GblGeoFenceModelID, GblGeoFenceModelVersion FROM si_sv_gblsurveymodel";
	$rsModelID = $theRepository->DataADOConnection->Execute($aSQL);
	$bCreateModel = true;
	$intModelID = -1;
	$dblVersion = floatval(esvGeoFenceModel);
	if ($rsModelID && $rsModelID->_numOfRows != 0)
	{
		$bCreateModel = false;
		$intModelID = intval($rsModelID->fields['gblgeofencemodelid']);
		if ($intModelID == 0)
			$intModelID = -1;
		$dblVersion = floatval($rsModelID->fields['gblgeofencemodelversion']);
		if ($dblVersion < floatval(esvGeoFenceModel_1_0001))
			$bCreateModel = true;
	}
	geofenceCubeGenerationLogString('ModelID:'.$intModelID);
	geofenceCubeGenerationLogString('ModelCurrentVersion:'.$dblVersion);
	geofenceCubeGenerationLogString('ModelVersion:'.esvGeoFenceModel);

	//Variable de sesi�n requerida por model manager
	if (!array_key_exists("BITAM_UserID", @$_SESSION))
		$_SESSION["BITAM_UserID"] = 1;

	
	if ($bCreateModel)
	{
		geofenceCubeGenerationLogString('Create Model!');
		$theArtusModelScheduler = ARTUSModelGeoFence::NewInstance($theRepository);
		if ($theArtusModelScheduler)
			$theArtusModelScheduler->createIfNotExistsGeoFenceModel($bCreateModel, $intModelID, $dblVersion);
		$aSQL = "UPDATE SI_SV_GblSurveyModel SET GblGeoFenceModelID = ".$theArtusModelScheduler->cla_concepto.", GblGeoFenceModelVersion = ".esvGeoFenceModel;
		$theRepository->DataADOConnection->Execute($aSQL);
	}

	$aResponse = array();
	$aResponse['error'] = false;
	$aResponse['errmsg'] = "AgendaCubeGeneration:Success";

	return $aResponse;
}

function geofenceCubeGenerationLogString($msg)
{
	//Si no hay nombre de archivo entonces utiliza el default. Si hay nombre pero no contiene un path entonces usa el path default
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$sFilePath = GeteFormsLogPath();
	error_log(date('Y-m-d H:i:s').' :: '.$msg.PHP_EOL, 3, $sFilePath.'geofenceCubeGeneration.log');
	//@JAPR
}

?>