<?php
	function grantService(){
		$redirectURL = "browser_error.php";
		$browserInfo = get_browser(null, true);

		$accessGranted = false;
		$browserVersion = $browserInfo["version"];
		switch(strtolower(trim($browserInfo["browser"]))){
			case 'chrome':
				if($browserVersion >= '32.0'){
					$accessGranted = true;
				}else{
					$accessGranted = true;
				}
			break;
			case 'firefox':
				if($browserVersion >= '30.0'){
					$accessGranted = false;
				}else{
					$accessGranted = false;
				}
			break;
			case 'ie':
				if($browserVersion >= '8.0'){
					$accessGranted = false;
				}else{
					$accessGranted = false;
				}
			break;
			case 'safari':
				if($browserVersion >= '6.1'){
					$accessGranted = false;
				}else{
					$accessGranted = false;
				}
			break;
			case 'opera':
				if($browserVersion >= '12.15'){
					$accessGranted = true;
				}else{
					$accessGranted = true;
				}
			break;
			case 'edge':
				if($browserVersion >= '12.0'){
					$accessGranted = true;
				}else{
					$accessGranted = true;
				}
			break; 
			default:
				if($_SERVER['REMOTE_ADDR'] == '10.0.0.1'){
					$accessGranted = true;
				}else{
					$accessGranted = false;	
				}
		}
		if($accessGranted === false){
			$oldStyleCheck = false;
			if (strtolower(trim($browserInfo["browser"])) == 'default browser') {
				if (stripos(strtolower((string) @$_SERVER['HTTP_USER_AGENT']), "chrome") !== false) {
					$oldStyleCheck = true;
				}
			}
			if (!$oldStyleCheck) {
				header('Location: '.$redirectURL);
			}
		}
	}
?>