<?php 

require_once("repository.inc.php");
require_once("user.inc.php");
require_once("usergroupuser.inc.php");
require_once("usergroupindicatorgroup.inc.php");
//require_once("usergroupdashboard.inc.php");
//require_once("usergroupdimhide.inc.php");

class BITAMUserGroup extends BITAMObject
{
	public $UserGroupID;
	public $UserGroupName;
	public $UserGroupOwner;
	//@AAL 18/072015 Agrgado para saber el cla_usurio de losusuarios asociados a este grupo
	public $UsersAssct;
	public $DefaultSurvey;
	public $TemplateID;
	//@AAL 19/07/2015: es true cuando se desasociarón o desmarcaron todos los usuarios del grupo(en el cliente), en caso contrario false
	public $DissociateAll; 
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->UserGroupID = -1;
		$this->UserGroupName = "";
		$this->UsersAssct = array();
		$this->DefaultSurvey = 0;
		$this->TemplateID = -1;
		$this->DissociateAll = false;
		/*
		// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
		$UserNameOwner = $_SESSION["BITAM_UserName"];
		// Si no existe en la sesion, lo tomamos de la cookie			
		if (is_null($UserNameOwner))
			$UserNameOwner = $_COOKIE["BITAM_UserName"];
		// Si aun asi es nulo, lo inicializamos en cero
		if (is_null($UserNameOwner))
			$UserNameOwner = "SUPERVISOR";		*/
		
		$this->UserGroupOwner = "SUPERVISOR";//$UserNameOwner;	
	}

	static function NewUserGroup($aRepository)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}

	static function NewUserGroupWithID($aRepository, $aUserGroupID)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		$anInstance = null;
		if (((int) $aUserGroupID) < 0)
		{
			return $anInstance;
		}

		$sql = 
		"SELECT t1.CLA_ROL AS UserGroupID, t1.NOM_ROL AS UserGroupName, t1.DefaultSurvey As DefaultSurvey, t1.TemplateID As TemplateID, t2.NOM_CORTO As Owner ".
		"FROM 	SI_ROL t1, SI_USUARIO t2 ". 
		"WHERE 	t2.CLA_USUARIO = t1.CLA_OWNER and t1.CLA_ROL = ".((int) $aUserGroupID);
		
//		print '<BR>'.$sql.'<BR>';
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_ROL table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewUserGroupFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	//@AAL 17/07/2015: Agregado parámetro true, para indicar si obtener o no los usuarios asociados al grupo
	static function NewUserGroupFromRS($aRepository, $aRS, $isCollection = false)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;

		$anInstance = $strCalledClass::NewUserGroup($aRepository);
		$anInstance->UserGroupID = (int) $aRS->fields["usergroupid"];
		$anInstance->UserGroupName = rtrim($aRS->fields["usergroupname"]);
		$anInstance->UserGroupOwner = rtrim($aRS->fields["owner"]);
		$anInstance->DefaultSurvey = (int) @$aRS->fields["defaultsurvey"];
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		if ($anInstance->TemplateID <= 0) {
			$anInstance->TemplateID = -1;
		}

		if (!$isCollection) {
			$sql = "SELECT CLA_USUARIO FROM SI_ROL_USUARIO WHERE CLA_ROL = {$anInstance->UserGroupID} ORDER BY CLA_USUARIO";
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while($aRS && !$aRS->EOF)
			{
				$anInstance->UsersAssct[] =  $aRS->fields["cla_usuario"];
				$aRS->MoveNext();
			}
		}
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (array_key_exists("UserGroupID", $aHTTPRequest->POST))
		{
			$aUserGroupID = $aHTTPRequest->POST["UserGroupID"];
			if (is_array($aUserGroupID))
			{
				$aCollection = BITAMUserGroupCollection::NewUserGroupCollection($aRepository, $aUserGroupID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
					if (!($anInstanceToRemove->Message == ""))
					{
						return $anInstanceToRemove;
					}
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewUserGroupWithID($aRepository, (int) $aUserGroupID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewUserGroup($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("UserGroupID", $aHTTPRequest->GET))
		{
			$aUserGroupID = $aHTTPRequest->GET["UserGroupID"];
			$anInstance = $strCalledClass::NewUserGroupWithID($aRepository, (int) $aUserGroupID);
			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewUserGroup($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewUserGroup($aRepository);
		}
		//$trace = @debug_backtrace();
		$anInstance->generateDesignWindow();
		die();
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("UserGroupID", $anArray))
		{
			$this->UserGroupID = (int) $anArray["UserGroupID"];
		}
 		if (array_key_exists("UserGroupName", $anArray))
		{
			$this->UserGroupName = stripslashes($anArray["UserGroupName"]);
		}
 		if (array_key_exists("UserGroupOwner", $anArray))
		{
			$this->UserGroupOwner = stripslashes($anArray["UserGroupOwner"]);
		}
		if (array_key_exists("UsersAssct", $anArray))
		{
			//Pasamos los elementos separados por , al Array UsersAssct.
			if($anArray["UsersAssct"] != ""){
				$this->UsersAssct = explode(",", $anArray["UsersAssct"]);
			}
		}
		//@AAL 19/07/2015: Empleado para saber si se han desmarcado todos los usuarios del grupo durante la edición
		if (array_key_exists("DissociateAll", $anArray)) 
		{
			$this->DissociateAll = (bool) $anArray["DissociateAll"];
		}
		if (array_key_exists("DefaultSurvey", $anArray)) //20-sep-2011 Conchita, agregado supervisor
		{
			$this->DefaultSurvey = (int)@$anArray["DefaultSurvey"];
		}
		
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
		if (array_key_exists("TemplateID", $anArray))
		{
			$this->TemplateID = $anArray["TemplateID"];
		}

		return $this;
	}

	function save()
	{
		CheckMetadataDuplicated($this->Repository->ADOConnection, 'SI_ROL', 'NOM_ROL', $this->UserGroupName, 'CLA_ROL', $this->UserGroupID, '');
		
	 	if ($this->isNewObject())
		{
/*
			$sql =  "SELECT MAX(CLA_ROL) AS UserGroupID".
						" FROM SI_ROL";

			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("Error accessing SI_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			
			// Obtenemos el siguiente grupo de indicadores
			if (is_null($aRS->fields["usergroupid"]))
				$this->UserGroupID = 1;
			else
				$this->UserGroupID = $aRS->fields["usergroupid"] + 1;
*/
			$this->UserGroupID = BITAMGetNextID($this->Repository->ADOConnection, 'CLA_ROL', 'SI_ROL', '');
			//@AAL 18/07/2015 Cometado por que mandaba error antes de insetar
			// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
			/*$UserIDOwner = $_SESSION["BITAM_UserID"];
			// Si no existe en la sesion, lo tomamos de la cookie			
			if (is_null($UserIDOwner))
				$UserIDOwner = $_COOKIE["BITAM_UserID"];
			// Si aun asi es nulo, lo inicializamos en cero
			if (is_null($UserIDOwner))*/
				$UserIDOwner = "0";
			//@AAL 2015-07-21: En eFormsV6 y siguientes se Agrega al grupo una Forma y un template a elegir
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ',DefaultSurvey';
				$strAdditionalValues .= ','.$this->DefaultSurvey;
			}
			if (getMDVersion() >= esveFormsv6) {
				$strAdditionalFields .= ',TemplateID';
				$strAdditionalValues .= ','.$this->TemplateID;
			}
			$sql = "INSERT INTO SI_ROL (".
			            "CLA_ROL".
			            ",NOM_ROL".
			            ",CLA_OWNER".
			            $strAdditionalFields.
						") VALUES (".
						$this->UserGroupID.
						",".$this->Repository->ADOConnection->Quote($this->UserGroupName).
						",".$UserIDOwner.
						$strAdditionalValues.
						")";
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			//@AAL 17/07/2015: Si hay usuarios asociados a este grupo, los insertamos a la tabla SI_ROL_USUARIO
			if (count($this->UsersAssct) > 0) {
				$Values = "";
				//Preparamos los usuarios a insertar para asociarlos al nuevo grupo
				for ($i = 0; $i < count($this->UsersAssct); $i++) {
					$Values .= "({$this->UserGroupID}, {$this->UsersAssct[$i]}),";
				}
				$Values = substr($Values, 0, strlen($Values)-1);
				$sql = "INSERT INTO SI_ROL_USUARIO (CLA_ROL, CLA_USUARIO) VALUES" . $Values;
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die("Error accessing SI_ROL_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				}
			}
		}
		else
		{
			$sql = "UPDATE SI_ROL SET ".
						"NOM_ROL = ".$this->Repository->ADOConnection->Quote($this->UserGroupName).
						", DefaultSurvey = ".$this->DefaultSurvey.
						", TemplateID = ".$this->TemplateID.
					" WHERE CLA_ROL = ".$this->UserGroupID;

			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}

			//@AAL 18/07/2015 Eliminamos e insertamos nuevamente los usuarios asociados a este grupo
			if (count($this->UsersAssct) > 0) {
				$sql = "DELETE FROM SI_ROL_USUARIO WHERE CLA_ROL = {$this->UserGroupID}";
				$this->Repository->ADOConnection->Execute($sql);
				//@AAL 19/07/2015 si DissociateAll es true quiere decir que ya no insertaremos nuevamente los usuarios al grupo, solo se borraron de la tabla SI_ROL_USUARIO
				if (!$this->DissociateAll) {
					$Values = "";
					//Preparamos los usuarios a insertar para asociarlos al nuevo grupo
					//GCRUZ 2015-08-28 Validar Usuarios duplicados. Bug GN1Q7C
					$insertedUsers = array();
					for ($i = 0; $i < count($this->UsersAssct); $i++) {
						if (array_search($this->UsersAssct[$i], $insertedUsers) === false)
						{
						$Values .= "({$this->UserGroupID}, {$this->UsersAssct[$i]}),";
							$insertedUsers[] = $this->UsersAssct[$i];
						}
					}
					$Values = substr($Values, 0, strlen($Values)-1);
					$sql = "INSERT INTO SI_ROL_USUARIO (CLA_ROL, CLA_USUARIO) VALUES" . $Values;
					if ($this->Repository->ADOConnection->Execute($sql) === false)
					{
						die("Error accessing SI_ROL_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
					}
				}
				else
				{
					$this->DissociateAll = false;
				}
			}
		}
		return $this;
	}

	function IsSystemRol($UserGroupID) {
		global $gbIsGeoControl;
		
		//@JAPR 2015-10-07: Validado que GeoControl no permita eliminar los grupos básicos asociados a los tipos de rol creados por el sistema externo de GeoControl
		if ($gbIsGeoControl) {
			return ((int) $UserGroupID) <= 5;
		}
		else {
			// @EA 2011-02-09 Regresa true en caso de que sea un ROL de tipo sistema ( Administrador, Invitado, Ventas, etc )		
			if (InModeSAAS()) {
				return strpos(',1,2,4,5,6,7,8,9,10,', ','.$UserGroupID.',') === false ? false : true;
			}
		}
		//@JAPR
		
		return false;
	}
	
	function remove()
	{		
		/* ====================================================================================
			Garantizamos la integridad del repositorio
			SI_ROL_USUARIO  -> usuarios
			SI_ROL_IND      -> grupos de indicadores
			SI_ROL_CONFIG   -> seguridad de cubo 
			SI_MENSAJES_ROL -> mensajes
			SI_ADO_SEGROL   -> seguridad de mensajes
			SI_FAVORITOS_ROL-> publicacion de favoritos
			SI_ESC_GPO		-> publicacion de escenarios
		   ==================================================================================== */
		
		if ($this->IsSystemRol($this->UserGroupID))
		{
			$this->Message = translate("This user group was used to define SAAS system");
			//die($this->Message);
			return $this;
		}		
		
		if (ExistsIndicatorGroupInRol($this->Repository->ADOConnection, $this->UserGroupID))
		{
			$this->Message = translate("This user group was used to define security, reset before please");
			//die($this->Message);
			return $this;
		}
		
		// usuarios
		$sql = "DELETE FROM SI_ROL_USUARIO WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		// grupos de indicadores
		$sql = "DELETE FROM SI_ROL_IND WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_IND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		// seguridad de cubo
		$sql = "DELETE FROM SI_ROL_CONFIG WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_CONFIG table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
	
		// mensajes
		$sql = "DELETE FROM SI_MENSAJES_ROL WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_MENSAJES_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}		
		
		// seguridad de mensajes
		$sql = "DELETE FROM SI_ADO_SEGROL WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ADO_SEGROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		// publicacion de favoritos
		$sql = "DELETE FROM SI_FAVORITOS_ROL WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_FAVORITOS_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		// publicacion de escenarios
		$sql = "DELETE FROM SI_ESC_GPO WHERE CLA_GPO = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ESC_GPO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}		
		
		// finalmente los grupos de indicadores (roles)
		$sql = "DELETE FROM SI_ROL WHERE CLA_ROL = ".$this->UserGroupID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->UserGroupID < 0);
	}

	function get_Image()
	{
		//return "<img src=\"images/groups.png\" />";
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New User Group");
		}
		else
		{
			return $this->UserGroupName;
		}
	}

	function get_QueryString()
	{
		if ($this->UserGroupID < 0)
		{
			return "BITAM_PAGE=UserGroup";
		}
		else
		{
			return "BITAM_PAGE=UserGroup&UserGroupID=".$this->UserGroupID;
		}
	}

	function get_Parent()
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return $strCalledClass::NewUserGroupCollection($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'UserGroupID';
	}

	function get_FormFieldName() {
		return 'UserGroupName';
	}
	
	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMUserGroupUserCollection::NewUserGroupUserCollection($this->Repository, $this->UserGroupID);
//			$myChildren[] = BITAMUserGroupDashboardCollection::NewUserGroupDashboardCollection($this->Repository, $this->UserGroupID);
			$myChildren[] = BITAMUserGroupIndicatorGroupCollection::NewUserGroupIndicatorGroupCollection($this->Repository, $this->UserGroupID);
//			$myChildren[] = BITAMUserGroupDimHideCollection::NewUserGroupDimHideCollection($this->Repository, $this->UserGroupID);
		}		
		return $myChildren;
	}

	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		
		$arrDef = array();
		$arrDef['UserGroupID'] = $this->UserGroupID;
		$arrDef['UserGroupName'] = $this->UserGroupName;
		$arrDef['UsersAssct'] = $this->UsersAssct;
		$arrDef['DissociateAll'] = $this->DissociateAll;
		$arrDef['DefaultSurvey'] = $this->DefaultSurvey;
		$arrDef['TemplateID'] = $this->TemplateID;
			
		return $arrDef;
	}

	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserGroupName";
		$aField->Title = translate("User Group Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserGroupOwner";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$aField->IsDisplayOnly = true;
			$myFields[] = $aField;		
		}
		
		return $myFields;
	}
	
	function canRemove($aUser) {
		global $gbIsGeoControl;
		
		//@JAPR 2015-10-07: Validado que GeoControl no permita eliminar los grupos básicos asociados a los tipos de rol creados por el sistema externo de GeoControl
		if ($gbIsGeoControl) {
			return ((int) $this->UserGroupID) > 5;
		}
		else {
			// @EA 2011-02-09 Regresa true en caso de que sea un ROL de tipo sistema ( Administrador, Invitado, Ventas, etc )		
			if (InModeSAAS()) {
				return strpos(',1,2,4,5,6,7,8,9,10,', ','.$this->UserGroupID.',') === false ? true : false;
			}
		}
		
		return true;
		//@JAPR
	}
	
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}
	
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}		
}

class BITAMUserGroupCollection extends BITAMCollection
{
	function __construct($aRepository, $anArrayOfUserGroupIDs)
	{
		BITAMCollection::__construct($aRepository);
		
		$filter = "";
		if (!is_null($anArrayOfUserGroupIDs))
		{
			foreach ($anArrayOfUserGroupIDs as $aUserGroupID)
			{
				if ($filter != "")
				{
					$filter .= ", ";
				}
				$filter .= (int) $aUserGroupID; 
			}
			if ($filter != "")
			{
				$filter = "and t1.CLA_ROL IN (".$filter.")";
			}
		}
		
		if ($filter == "")
		{
			$filter = "and t1.CLA_ROL > -1";
		}		

		//GCRUZ 2015-10-02. Ordenar por nombre de grupo y no por cla_rol.
		$sql = 
		"SELECT t1.CLA_ROL AS UserGroupID, t1.NOM_ROL AS UserGroupName, t1.DefaultSurvey As DefaultSurvey, t1.TemplateID As TemplateID, t2.NOM_CORTO As Owner ".
		"FROM 	SI_ROL t1, SI_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER ".$filter.
		" and t1.NOM_ROL <> 'Dummy' ".
		" ORDER BY t1.NOM_ROL";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_ROL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$strCalledClass = static::class;
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		while (!$aRS->EOF)
		{
			//@AAL 17/07/2015: Agregado parámetro true, para indicar si obtener o no los usuarios asociados al grupo
			$this->Collection[] = $strCalledClass::NewUserGroupFromRS($this->Repository, $aRS, true);
			$aRS->MoveNext();
		}
	}

	static function NewUserGroupCollection($aRepository, $anArrayOfUserGroupIDs = null)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $anArrayOfUserGroupIDs);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return $strCalledClass::NewUserGroupCollection($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Image()
	{
		//return '<img src="images/gruposusuarios.gif">';
	}
	
	function get_Title()
	{
		return translate("User Groups");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=UserGroupCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=UserGroup";
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'UserGroupID';
	}

	function get_FormFieldName() {
		return 'UserGroupName';
	}
	
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserGroupName";
		$aField->Title = translate("User Group Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserGroupOwner";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;
			$myFields[] = $aField;		
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		/*$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
			return true;
		else */
			return true;
	}
	
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}
	
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}
}

class BITAMUserGroupCollectionLimited extends BITAMCollection
{
	public $UserID;
	
	function __construct($aRepository, $UserID)
	{
		BITAMCollection::__construct($aRepository);
		$this->UserID = $UserID;
	}
	
	static function NewInstance($aRepository, $UserID)
	{
		$anInstance = new BITAMUserGroupCollectionLimited($aRepository, $UserID);
		
		$filter = "";
		//@JAPR 2015-09-25: Corregido un bug, faltaba el campo defaultSurvey
		$sql = 
		"SELECT t1.CLA_ROL AS UserGroupID, t1.NOM_ROL AS UserGroupName, t1.DefaultSurvey As DefaultSurvey, t1.TemplateID As TemplateID, t2.NOM_CORTO As Owner ".
		"FROM 	SI_ROL t1, SI_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER ".
		"		and t1.CLA_ROL in ".
		"		( select CLA_ROL from SI_ROL_USUARIO where CLA_USUARIO = ".$UserID." )".
		" ORDER BY 2";
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_ROL table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroup::NewUserGroupFromRS($aRepository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewUserGroupCollectionLimited($aRepository, $UserID)
	{
		return new BITAMUserGroupCollectionLimited($aRepository, $UserID);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		return BITAMUserGroupCollectionLimited::NewUserGroupCollection($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Image()
	{
		return '<img src="images/gruposusuarios.gif">';
	}
	
	function get_Title()
	{
		return translate("User Groups");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=UserGroupCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=UserGroup";
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'UserGroupID';
	}
	
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserGroupName";
		$aField->Title = translate("User Group Name");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;
		
		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserGroupOwner";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;
			$myFields[] = $aField;		
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}
	
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		return $this->canRemove($aUser);
	}
}

?>