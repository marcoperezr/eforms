<?php
global $widthByDisp;
$myFormName = get_class($this);
?>
&nbsp;
<br>
<div id="formattedTextArea" width="100%">
	<!-- @AAL 25/02/2015: Agregado para enviar el SurveyID y el SectionID a traves de un elemento oculto.
     estos valores son empleados al abrir un nuevo Editor de texto en una Sección -->
	<input type="hidden" id="SurveyAndSectOrQuestIDs" value="<?=$this->SurveyID . ':' . $this->SectionID . ':SectionID'?>">
	<table class="object_table" width="100%" cellpadding="0" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td>
				<strong><?=translate("Formatted text")?></strong>
			</td>
		</tr>
		<tr>
			<td class="object_field_value_3" colspan="3">
<?
		//@JAPR 2014-07-30: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
				<div id="FormattedTextMenu" class="tabbed" style="height:40px;">
					<ul class="menu">
						<li id = "defaultFormattedText" name="Default" class="menuOption selectedTab" onclick="changeTab(this,'FormattedText')">
							<a>Default</a>
				        </li>					
						<li name="iPod" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>iPod</a>
				        </li>					
						<li name="iPad" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>iPad</a>
				        </li>					
						<li name="iPadMini" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>iPad Mini</a>
				        </li>					
						<li name="iPhone" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>iPhone</a>
				        </li>					
						<li name="Cel" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>Android Cel</a>
				        </li>					
						<li name="Tablet" class="menuOption" onclick="changeTab(this,'FormattedText')">
							<a>Android Tablet</a>
				        </li>					
					</ul>
				</div>
<?
		}
		//@JAPR
		
		
		//Sep-2014
		if($this->EditorType==0)
		{
			$styleDefaultDiv = '';
			$styleDefaultDesDiv = 'style="display: none;width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
		}
		else
		{
			$styleDefaultDiv = 'style="display: none;"';
			$styleDefaultDesDiv = 'style="width: '.$widthByDisp["Default"].'; height: 200px;overflow-x: auto;overflow-y: auto;"';
		}
?>
				<div id="FormattedTextTabs">
					<div id="FormattedTextDefaultDiv" <?=$styleDefaultDiv?>>
						<textarea id="FormattedText" name="FormattedText" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedText?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedText');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
<?
		//@JAPR 2014-07-30: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					<div id="FormattedTextiPodDiv" style="display: none;">
						<textarea id="FormattedTextiPod" name="FormattedTextiPod" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextiPod?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextiPod');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="FormattedTextiPadDiv" style="display: none;">
						<textarea id="FormattedTextiPad" name="FormattedTextiPad" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextiPad?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextiPad');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="FormattedTextiPadMiniDiv" style="display: none;">
						<textarea id="FormattedTextiPadMini" name="FormattedTextiPadMini" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextiPadMini?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextiPadMini');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="FormattedTextiPhoneDiv" style="display: none;">
						<textarea id="FormattedTextiPhone" name="FormattedTextiPhone" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextiPhone?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextiPhone');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="FormattedTextCelDiv" style="display: none;">
						<textarea id="FormattedTextCel" name="FormattedTextCel" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextCel?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextCel');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
					<div id="FormattedTextTabletDiv" style="display: none;">
						<textarea id="FormattedTextTablet" name="FormattedTextTablet" cols="150" rows="15" onkeydown="javascript: return TextArea_onkeydown(this);">
							<?=$this->FormattedTextTablet?>
						</textarea>   <br><a href="#" title="toogle TinyMCE" onclick="toogleEditorMode('FormattedTextTablet');"><img src='toogletinymce.png' alt='Toggle TinyMCE'></a>
					</div>
<?
		}
?>
			</div>
<?
		//@JAPR
					//Sep-2014
					//Nuevos divs para el nuevo editor
					
					if (getMDVersion() >= esvEditorHTML)
					{
?>
							
				<span id="FormattedTextSpanDesigner">
					<table id="FormattedText_tbl" style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC; border-collapse:collapse;">
					<tbody>
						<tr height=25px>
						<td margin="0" style="background: #F0F0EE;border: 0; border-bottom:1px solid #CCC;padding:0px">
						<div id="FormattedText_toolbargroup" tabindex="-1">
						<span>
							<table id="FormattedText_toolbar1" cellpadding="0" cellspacing="0" tabindex="-1">
								<tbody>
								<tr>
									<td style="position: relative;padding:0px">
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['FormattedText'],visibleDivArray['FormattedText'],'LBL,ADD',0,0);" title="Insert Text">
											<img src="./images/insertText.png" style="display:inline;cursor:pointer;">
										</a>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['FormattedText'],visibleDivArray['FormattedText'],'BTN,ADD',0,0);" title="Insert Button">
											<img src="./images/insertButton.png" style="display:inline;cursor:pointer;">
										</a>
										<a href="javascript:;" onmousedown="return false;" onclick="currentEvent=null; mayAddElement(visibleDivArray['FormattedText'],visibleDivArray['FormattedText'],'IMG,ADD',0,0);" title="Insert Image">
											<img src="./images/insertImage.png" style="display:inline;cursor:pointer;">
										</a>
									</td>
								</tr>
								</tbody>
							</table>
						</span>
						</div>
						</td>
						</tr>
						<tr>
						<td style="border-top:1px solid #CCC; border-bottom:1px solid #CCC; border-left:1px solid #CCC;border-right:1px solid #CCC;padding:0px">
					<div id="FormattedTextDefaultDesDiv" <?=$styleDefaultDesDiv?>>
						<textarea id="FormattedTextDes" name="FormattedTextDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextDes?></textarea>
						<textarea id="FormattedTextDesHTML" name="FormattedTextDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedText?></textarea>
					</div>

					<?
						//@JAPR 2014-07-30: Agregado el Responsive Design
						//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
						//el componente default
						if (getMDVersion() >= esvResponsiveDesign) 
						{
?>
					<div id="FormattedTextiPodDesDiv"  style="display: none; width: <?=$widthByDisp["iPod"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextiPodDes" name="FormattedTextiPodDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPodDes?></textarea>
						<textarea id="FormattedTextiPodDesHTML" name="FormattedTextiPodDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPodDes?></textarea>
					</div>
					<div id="FormattedTextiPadDesDiv" style="display: none;width: <?=$widthByDisp["iPad"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextiPadDes" name="FormattedTextiPadDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPadDes?></textarea>
						<textarea id="FormattedTextiPadDesHTML" name="FormattedTextiPadDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPadDes?></textarea>
					</div>
					<div id="FormattedTextiPadMiniDesDiv" style="display: none;width: <?=$widthByDisp["iPadMini"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextiPadMiniDes" name="FormattedTextiPadMiniDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPadMiniDes?></textarea>
						<textarea id="FormattedTextiPadMiniDesHTML" name="FormattedTextiPadMiniDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPadMiniDes?></textarea>
					</div>
					<div id="FormattedTextiPhoneDesDiv" style="display: none;width: <?=$widthByDisp["iPhone"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextiPhoneDes" name="FormattedTextiPhoneDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPhoneDes?></textarea>
						<textarea id="FormattedTextiPhoneDesHTML" name="FormattedTextiPhoneDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextiPhoneDes?></textarea>
					</div>
					<div id="FormattedTextCelDesDiv" style="display: none;width: <?=$widthByDisp["Cel"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextCelDes" name="FormattedTextCelDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextCelDes?></textarea>
						<textarea id="FormattedTextCelDesHTML" name="FormattedTextCelDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextCelDes?></textarea>
					</div>
					<div id="FormattedTextTabletDesDiv" style="display: none;width: <?=$widthByDisp["Tablet"]?>; height: 200px;overflow-x: hidden;overflow-y: auto;">
						<textarea id="FormattedTextTabletDes" name="FormattedTextTabletDes" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextTabletDes?></textarea>
						<textarea id="FormattedTextTabletDesHTML" name="FormattedTextTabletDesHTML" cols="150" rows="15" onkeydown="" style="display: none;"><?=$this->FormattedTextTabletDes?></textarea>
					</div>
<?
						}
?>
						</td>
						</tr>
					</tbody>
					</table>
				</span>
<?
					}
?>

				
				<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
				<script type="text/javascript">
				visibleDivArray['FormattedText'] = 'FormattedTextDefaultDesHDiv';
				function changeTab(oEvt, sPropName) {
					
					if (!oEvt || !sPropName) {
						return;
					}
					
					//Sep-2014
					//debugger;
					objEditorType=<?=$myFormName?>_SaveForm.EditorType;
					if(objEditorType)
					{
						editorTypeValue = objEditorType.options[objEditorType.selectedIndex].value;
					}
					else
					{
						editorTypeValue = 0;
					}
					
					if(editorTypeValue==0)
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'Div';
					}
					else
					{
						var strTabId = sPropName+oEvt.getAttribute('name')+'DesDiv';
						visibleDivArray[sPropName] =  sPropName+oEvt.getAttribute('name')+'DesHDiv';
					}
					
					//alert("Editor: "+editorTypeValue+", strTabId:"+strTabId+", sPropName="+sPropName+", visibleDiv ="+visibleDiv);
					
									
					document.getElementById(sPropName+'DefaultDiv').style.display = 'none';
					var objDiv = document.getElementById(sPropName+'iPodDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					//Sep-2014
					var objDiv = document.getElementById(sPropName+'DefaultDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPodDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPadMiniDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'iPhoneDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'CelDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					var objDiv = document.getElementById(sPropName+'TabletDesDiv');
					if (objDiv) {
						objDiv.style.display = 'none';
					}
					
					var objDiv = document.getElementById(strTabId);
					if (objDiv) {
						objDiv.style.display = 'block';
					}
					try {
						var objDiv = document.getElementById(sPropName+'Menu');
						if (objDiv) {
							var arrLi = objDiv.getElementsByTagName('li');
							if (arrLi) {
								for (var intIdx in arrLi) {
									arrLi[intIdx].className = "menuOption";
								}
							}
						}
						
						if (oEvt) {
							oEvt.className = "menuOption selectedTab"						
						}
					} catch(e) {}
				}
				
				var tinyMCEmode = true; //Conchita 16-nov-2011 cambio para toggle tinymce
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "FormattedText,ajaxfilemanager",
				      	theme : "advanced",
						file_browser_callback : "ajaxfilemanager",
						entity_encoding : "raw",
						theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",

   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					
					/*
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "HTMLHeader,ajaxfilemanager",
				      	theme : "advanced",
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",

   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					*/
					
					/*
					tinyMCE.init({
						// General options
						mode : "exact",
						readonly : 0,
				        elements : "HTMLFooter,ajaxfilemanager",
				      	theme : "advanced",
						file_browser_callback : "ajaxfilemanager",
						handle_event_callback : "formattedtextchanged",
						entity_encoding : "raw",
						theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
						theme_advanced_buttons2: "",
						theme_advanced_buttons3: "",
						theme_advanced_buttons4: "",		
						theme_advanced_toolbar_location : "top",
						theme_advanced_toolbar_align : "left",
						theme_advanced_statusbar_location : "none",
						theme_advanced_resizing : true,
						//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
						theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
        										"Arial=arial,helvetica,sans-serif;"+
        										"Arial Black=arial black,avant garde;"+
        										"Book Antiqua=book antiqua,palatino;"+
								                "Comic Sans MS=comic sans ms,sans-serif;"+
								                "Courier New=courier new,courier;"+
								                "Georgia=georgia,palatino;"+
								                "Helvetica=helvetica;"+
								                "Impact=impact,chicago;"+
								                "Symbol=symbol;"+
								                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
								                "Terminal=terminal,monaco;"+
								                "Times New Roman=times new roman,times;"+
								                "Trebuchet MS=trebuchet ms,geneva;"+
								                "Verdana=verdana,geneva;"+
								                "Webdings=webdings;"+
								                "Wingdings=wingdings,zapf dingbats",

   						relative_urls : false,
						content_css : "",
						convert_urls : false,
						
						// Drop lists for link/image/media/template dialogs
						external_link_list_url : "lists/link_list.js",
						save_callback : "myCustomSaveContent",
						oninit: "myInitTinyMCE",
						cleanup: false,
						verify_html : false,
						forced_root_block : false
					});
					*/
					
					var arrResponsiveDesignObjects = new Array();
<?
		//@JAPR 2014-07-30: Agregado el Responsive Design
		//Si se utilizará Responsive Design, agrega el menú para seleccionar el tipo de dispositivo, de lo contrario sólo permanecerá visible
		//el componente default
		if (getMDVersion() >= esvResponsiveDesign) {
?>
					arrResponsiveDesignObjects.push('FormattedTextiPod');
					arrResponsiveDesignObjects.push('FormattedTextiPad');
					arrResponsiveDesignObjects.push('FormattedTextiPadMini');
					arrResponsiveDesignObjects.push('FormattedTextiPhone');
					arrResponsiveDesignObjects.push('FormattedTextCel');
					arrResponsiveDesignObjects.push('FormattedTextTablet');
<?
		}
		//@JAPR
?>
					
					for (var intIdx in arrResponsiveDesignObjects) {
						tinyMCE.init({
							// General options
							mode : "exact",
							readonly : 0,
					        elements : arrResponsiveDesignObjects[intIdx] + ",ajaxfilemanager",
					      	theme : "advanced",
							file_browser_callback : "ajaxfilemanager",
							entity_encoding : "raw",
							theme_advanced_buttons1 : "bold,italic,forecolor,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect,|,link,unlink,image",
							theme_advanced_buttons2: "",
							theme_advanced_buttons3: "",
							theme_advanced_buttons4: "",		
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "none",
							theme_advanced_resizing : true,
							//Conchita 10-nov-2011 Agregado tipo de letra Normal Text
							theme_advanced_fonts :  "NORMAL TEXT=helvetica;"+ "Andale Mono=andale mono,times;"+
	        										"Arial=arial,helvetica,sans-serif;"+
	        										"Arial Black=arial black,avant garde;"+
	        										"Book Antiqua=book antiqua,palatino;"+
									                "Comic Sans MS=comic sans ms,sans-serif;"+
									                "Courier New=courier new,courier;"+
									                "Georgia=georgia,palatino;"+
									                "Helvetica=helvetica;"+
									                "Impact=impact,chicago;"+
									                "Symbol=symbol;"+
									                "Tahoma=tahoma,arial,helvetica,sans-serif;"+
									                "Terminal=terminal,monaco;"+
									                "Times New Roman=times new roman,times;"+
									                "Trebuchet MS=trebuchet ms,geneva;"+
									                "Verdana=verdana,geneva;"+
									                "Webdings=webdings;"+
									                "Wingdings=wingdings,zapf dingbats",
	
	   						relative_urls : false,
							content_css : "",
							convert_urls : false,
							
							// Drop lists for link/image/media/template dialogs
							external_link_list_url : "lists/link_list.js",
							save_callback : "myCustomSaveContent",
							oninit: "myInitTinyMCE",
							cleanup: false,
							verify_html : false,
							forced_root_block : false
						});
					}
					
					function myInitTinyMCE() {
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["FormattedText"]) {
							var htmlSrc = tinyMCE.editors["FormattedText"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								setTimeout(function () {toogleEditorMode('FormattedText');}, 300);
							}
						}
						
						/*
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["HTMLHeader"]) {
							var htmlSrc = tinyMCE.editors["HTMLHeader"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								setTimeout(function () {toogleEditorMode('HTMLHeader');}, 300);
							}
						}
						
						if (tinyMCE && tinyMCE.editors && tinyMCE.editors["HTMLFooter"]) {
							var htmlSrc = tinyMCE.editors["HTMLFooter"].getContent()
							
							if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
								setTimeout(function () {toogleEditorMode('HTMLFooter');}, 300);
							}
						}
						*/
						
						for (var intIdx in arrResponsiveDesignObjects) {
							var strTinyMCEObjectName = arrResponsiveDesignObjects[intIdx];
							if (tinyMCE && tinyMCE.editors && tinyMCE.editors[strTinyMCEObjectName]) {
								var htmlSrc = tinyMCE.editors[strTinyMCEObjectName].getContent()
								
								if (htmlSrc.toLowerCase().indexOf('script') >= 0) {
									setTimeout(function () {toogleEditorMode(strTinyMCEObjectName);}, 300);
								}
							}
						}
					}
					
					function myCustomSaveContent(element_id, html, body) {
					        // Do some custom HTML cleanup
					        if (html) {
					        	html = html.replace(/\/\/ <!\[cdata\[/gi, '').replace(/\/\/ \]\]>/gi,'');
					        }
					
					        return html;
					}
					
					function formattedtextchanged(event) {
						
						var htmlSrc = tinyMCE.activeEditor.getContent();
			            
						if(event.type == "keydown") {
							
						}
						
						/*
						var rexp = [a-zA-z0-9\t\n ./<>?;:"'`!@#$%^&*()[]{}_+=-|\\];
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      		*/
			         
					}
					
					function ajaxfilemanager(field_name, url, type, win) 
					{
						var ajaxfilemanagerurl = "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
						var view = 'detail';
						switch (type) 
						{
							case "image":
								view = 'thumbnail';
								break;
							case "media":
								break;
							case "flash": 
								break;
							case "file":
								break;
							default:
								return false;
						}
    					
						tinyMCE.activeEditor.windowManager.open({
							url: "../../../../jscripts/tiny_mce/plugins/ajaxfilemanager/ajaxfilemanager.php?view=" + view,
							width: 900,
        					height: 600,
							inline : "yes",
							close_previous : "no"
						},{
							window : win,
							input : field_name
						}); 
					}
					
					//Inicio conchita 16-nov-2011 toggle tinymce
			        function toogleEditorMode(sEditorID) { 
			        	var x;
			        	var tmp;
			        	
			            try { 
			                if(tinyMCEmode) { 
			                	//x=removeHTML();
			               		tinyMCE.execCommand('mceRemoveControl', false, sEditorID); 
			                   	//document.getElementById(sEditorID).value=x; 
			                    tinyMCEmode = false; 
			                } else {   tinyMCE.execCommand('mceAddControl', false, sEditorID);
			                    //tinyMCE.addMCEControl(document.getElementById(''), sEditorID); 
			                    tinyMCEmode = true; 
			                } 
			            } catch(e) { 
			            	alert('An error occur, try again. Error: '+ e);
			                //error handling 
			            } 
			        }
					
			        function removeHTML() {
			            var htmlSrc = tinyMCE.activeEditor.getContent();
			            var rexp = /<[^<>]+>/g;
			          
			            var cleanSrc = htmlSrc.replace(rexp,"");
			      
			            return cleanSrc;
			        }
					//Fin Conchita toggle tinymce 16-nov-2011	
				</script>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="object_field_value_3">
				&nbsp;
			</td>
		</tr>
		<tr>
			<td colspan="3" class="object_field_value_3">
				<?=translate('Options:')?>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="object_field_value_3">
				<input id="SendThumbnails" type="checkbox" name="SendThumbnails" value="1" style="border:0;background:transparent;">
				<?=translate('Send images to mobile devices (thumbnails)')?>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="object_field_value_3">
				<input id="EnableHyperlinks" type="checkbox" name="EnableHyperlinks" value="1" style="border:0;background:transparent;">
				<?=translate('Enable hyperlinks on mobile devices')?>
			</td>
		</tr>
	</tbody>
	</table>
</div>
<script language="Javascript">
Start();

function Start()
{
	var sendthumbnails = <?=$this->SendThumbnails?>;
	if (sendthumbnails == 1)
	{
		<?=$myFormName?>_SaveForm.SendThumbnails.checked = true;
		<?=$myFormName?>_SaveForm.SendThumbnails.defaultChecked = true;
	}
	else
	{
		<?=$myFormName?>_SaveForm.SendThumbnails.checked = false;
		<?=$myFormName?>_SaveForm.SendThumbnails.defaultChecked = false;
	}
	
	var enablehyperlinks = <?=$this->EnableHyperlinks?>;
	if (enablehyperlinks == 1)
	{
		<?=$myFormName?>_SaveForm.EnableHyperlinks.checked = true;
		<?=$myFormName?>_SaveForm.EnableHyperlinks.defaultChecked = true;
	}
	else
	{
		<?=$myFormName?>_SaveForm.EnableHyperlinks.checked = false;
		<?=$myFormName?>_SaveForm.EnableHyperlinks.defaultChecked = false;
	}
	
	//Sep-2014
	//debugger;
<?
		if (getMDVersion() >= esvEditorHTML) {
?>
	aHTMLDivObjects['FormattedTextDefaultDesHDiv'] = new reportPart('FormattedTextDefaultDesDiv', '', defaultWidth, defaultHeight, 'FormattedTextDefaultDesHDiv', 'FormattedTextDes');
	aHTMLDivObjects['FormattedTextiPodDesHDiv'] = new reportPart('FormattedTextiPodDesDiv', '', iPodWidth, iPodHeight, 'FormattedTextiPodDesHDiv', 'FormattedTextiPodDes');
	aHTMLDivObjects['FormattedTextiPadDesHDiv'] = new reportPart('FormattedTextiPadDesDiv', '', iPadWidth, iPadHeight, 'FormattedTextiPadDesHDiv', 'FormattedTextiPadDes');
	aHTMLDivObjects['FormattedTextiPadMiniDesHDiv'] = new reportPart('FormattedTextiPadMiniDesDiv', '', iPadMiniWidth, iPadMiniHeight, 'FormattedTextiPadMiniDesHDiv', 'FormattedTextiPadMiniDes');
	aHTMLDivObjects['FormattedTextiPhoneDesHDiv'] = new reportPart('FormattedTextiPhoneDesDiv', '', iPhoneWidth, iPhoneHeight, 'FormattedTextiPhoneDesHDiv', 'FormattedTextiPhoneDes');
	aHTMLDivObjects['FormattedTextCelDesHDiv'] = new reportPart('FormattedTextCelDesDiv', '', celAndroidWidth, celAndroidHeight, 'FormattedTextCelDesHDiv', 'FormattedTextCelDes');
	aHTMLDivObjects['FormattedTextTabletDesHDiv'] = new reportPart('FormattedTextTabletDesDiv', '', tabletAndroidWidth, tabletAndroidHeight, 'FormattedTextTabletDesHDiv', 'FormattedTextTabletDes');
<?
		}
?>
}
</script>