<?php 

require_once("repository.inc.php");
require_once("prop.inc.php");
require_once("indicatorprop.inc.php");
require_once("cube.inc.php");

class BITAMIndicatorCollection extends BITAMCollection
{
	function __construct($aRepository, $anArrayOfIndicatorIDs)
	{
		BITAMCollection::__construct($aRepository);

		$filter = "";
		if (!is_null($anArrayOfIndicatorIDs))
		{
			foreach ($anArrayOfIndicatorIDs as $aIndicatorID)
			{
				if ($filter != "")
				{
					$filter .= ", ";
				}
				$filter .= (int) $aIndicatorID; 
			}
			if ($filter != "")
			{
				$filter = "and t1.CLA_INDICADOR IN (".$filter.")";
			}
		}
		
		if ($filter == "")
		{
			$filter = "and t1.CLA_INDICADOR > -1";
		}		

		$sql = 
		"SELECT t3.CLA_CONCEPTO AS CubeID".
		"		,t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		"		,t3.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t1.CLA_OWNER AS OwnerID ".
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".
		"		,t1.IN_AGGREGATION as in_aggregation ".
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_CONCEPTO t3 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER AND t1.TIPO_INDICADOR in (1,3,5) ".
		"		AND t3.CLA_CONCEPTO = t1.CLA_CONCEPTO ".$filter." ".
		"ORDER BY 4, 3";
		// 
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$this->Collection[] = BITAMIndicator::NewIndicatorFromRS($this->Repository, $aRS);
			$aRS->MoveNext();
		}
	}

	static function NewIndicatorCollection($aRepository, $anArrayOfIndicatorIDs = null)
	{
		return new BITAMIndicatorCollection($aRepository, $anArrayOfIndicatorIDs);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		return BITAMIndicatorCollection::NewIndicatorCollection($aRepository);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Image()
	{
		return "<img src=images/indicadores.gif>";
	}
	
	function get_Title()
	{
		return translate("Indicators");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=IndicatorCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Indicator";
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorName";
		$aField->Title = translate("Indicator Name");
		$aField->Type = "String";
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CubeName";
		$aField->Title = translate("Model");
		$aField->Type = "String";
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "formato";
		$aField->Title = translate("Format");
		$aField->Type = "String";
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField;		
		


		if (!InModeSAAS())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OwnerName";
			$aField->Title = translate("Owner");
			$aField->Type = "String";
			$aField->Size = 15;		
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
}

class BITAMIndicator extends BITAMObject
{
	public $IndicatorID;
	public $IndicatorName;
	public $CubeID;
	public $CubeIDString;	
	public $CubeName;	
	public $formula_usr;
	public $formula_bd;
	public $nom_tabla;
	public $llaves;
	public $tipo_agrupacion;
	public $formato;
	public $tipo_indicador;
	public $cla_usuario;
	public $reexpresados;
	public $descripcion;
	public $filtro;
	public $paretto;
	public $tablas;
	public $donde;
	public $dimensiones;
	public $sdimensiones;	
	public $adimensiones;		
	public $no_ejecutivo;
	public $es_atributo;
	public $nivel_jerarquia;
	public $es_divisor;
	public $dim_depend;
	public $begin_target;
	public $end_target;
	public $resolveinquery;
	public $OwnerName;
	public $decremental;
	public $period_key;
	public $nactual;
	public $only_last_period;
	public $is_advanced;
	public $is_custom;	
	public $snom_anterior;
	public $oCube;
	public $filter_sql;
	public $niveles;
// @EA 2011-02-17
	public $in_aggregation = 0;
	public $in_aggregation_original = 0;	
//	
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->IndicatorID = -1;
		$this->IndicatorName = "";
		
		$CubeID = -1;
		if (array_key_exists("CubeID", $_GET))
		{
			$CubeID = $_GET["CubeID"];
		}
		if ($CubeID == -1)
		{
			if (array_key_exists("CubeID", $_POST))
			{
				$CubeID = $_POST["CubeID"];
			}
		}
		
		$this->CubeID = $CubeID;		
		
		if ($CubeID > 0)
		{
			$this->oCube = BITAMCube::NewCubeWithCubeID($aRepository, $CubeID);
			$this->CubeIDString = $this->CubeID;
		}

		$this->filter_sql = '';
		$this->niveles = '';
		
		$this->CubeName = "";
		$this->cla_usuario = -1;
		$this->formula_usr = "";
		$this->formula_bd = "";
		$this->nom_tabla = "";
		$this->llaves = "";
		$this->tipo_agrupacion = "NORMAL";
		$this->formato = "#,##0";
		$this->tipo_indicador = 1;
		$this->reexpresados = 0;
		$this->descripcion = "";
		$this->filtro = 0;
		$this->paretto = 80;
		$this->tablas = "";
		$this->donde = "";
		$this->dimensiones = array();
		$this->sdimensiones = "";		
		$this->no_ejecutivo = 0;
		$this->es_atributo = 0;
		$this->nivel_jerarquia = 0;
		$this->es_divisor = 0;
		$this->dim_depend = "";		
		$this->begin_target = 0;
		$this->end_target = 0;
		$this->resolveinquery = 0;
		$this->decremental = 0;
		$this->period_key = -1;
		$this->nactual = 0;
		$this->only_last_period = 0;
		$this->is_advanced = 0;
		$this->is_custom = 0;
		$this->snom_anterior = '';
		
		$this->readDimensions($aRepository);
		
		// Obtenemos el nombre del usuario logeado, primero lo tomamos de la sesion
		//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesi�n de eForms
		$UserNameOwner = @$_SESSION["PABITAM_UserName"];
		// Si no existe en la sesion, lo tomamos de la cookie			
		if (is_null($UserNameOwner))
			$UserNameOwner = @$_COOKIE["BITAM_UserName"];
		// Si aun asi es nulo, lo inicializamos en cero
		if (is_null($UserNameOwner))
			$UserNameOwner = "SUPERVISOR";		

		$this->OwnerName = $UserNameOwner;
/*		
		$sql = 
		"select REF_CONFIGURA AS pathArtusWEB ".
		"from	SI_CONFIGURA ".
		"where  CLA_CONFIGURA = 916 and CLA_USUARIO = -2";
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$gsURLWEBHTML = rtrim($aRS->fields['pathartusweb']);
		}
		else
			$gsURLWEBHTML = GetLocalURLWEBG6();
*/			
		unset ($aRS);
		
	}

	static function NewIndicator($aRepository)
	{
		return new BITAMIndicator($aRepository);
	}

	static function NewIndicatorWithID($aRepository, $aIndicatorID)
	{
		$anInstance = null;
		if (((int) $aIndicatorID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		"SELECT t1.CLA_CONCEPTO AS CubeID".
		"		,t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		"		,t3.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".		
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".
		"		,t1.IN_AGGREGATION as in_aggregation ".
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_CONCEPTO t3 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER ".
		"		AND t3.CLA_CONCEPTO = t1.CLA_CONCEPTO AND t1.TIPO_INDICADOR in (1,3,5) ".
		"		AND t1.CLA_INDICADOR = ".((int) $aIndicatorID);
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMIndicator::NewIndicatorFromRS($aRepository, $aRS);
		}
		
		return $anInstance;
	}

	static function NewIndicatorFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMIndicator::NewIndicator($aRepository);
		$anInstance->CubeID = (int) $aRS->fields["cubeid"];
		$anInstance->CubeIDString = (int) $aRS->fields["cubeid"];
		$anInstance->IndicatorID = (int) $aRS->fields["indicatorid"];
		$anInstance->IndicatorName = rtrim($aRS->fields["indicatorname"]);
		
		$anInstance->snom_anterior = $anInstance->IndicatorName;
		
		$anInstance->CubeName = $aRS->fields["cubename"];
		$anInstance->formula_usr = rtrim($aRS->fields["formula_usr"]);
		$anInstance->formula_bd = rtrim($aRS->fields["formula_bd"]);
		$anInstance->nom_tabla = rtrim($aRS->fields["nom_tabla"]);
		$anInstance->llaves = rtrim($aRS->fields["llaves"]);
		$anInstance->tipo_agrupacion = rtrim($aRS->fields["tipo_agrupacion"]);
		
		if ($anInstance->tipo_agrupacion == 'CUSTOM')
			$anInstance->is_custom = true;
		else
			$anInstance->is_custom = false;
		
		$anInstance->formato = rtrim($aRS->fields["formato"]);
		$anInstance->tipo_indicador = (int) $aRS->fields["tipo_indicador"];
		
		$anInstance->is_advanced = false;
		if ($anInstance->tipo_indicador == 5)
			$anInstance->is_advanced = true;
		
		$anInstance->reexpresados = (int) $aRS->fields["reexpresados"];
		$anInstance->descripcion = rtrim($aRS->fields["descripcion"]);
		$anInstance->filtro = (int) $aRS->fields["filtro"];
		$anInstance->paretto = (int) $aRS->fields["paretto"];
		$anInstance->tablas = rtrim($aRS->fields["tablas"]);
		$anInstance->donde = rtrim($aRS->fields["donde"]);
		
		if (!is_null($aRS->fields["dimensiones"]))
			$anInstance->sdimensiones = rtrim($aRS->fields["dimensiones"]);
			
		$anInstance->readDimensions();
		
		$anInstance->no_ejecutivo = (int) $aRS->fields["no_ejecutivo"] == 0 ? 1 : 0;
		$anInstance->es_atributo = (int) $aRS->fields["es_atributo"];
		$anInstance->nivel_jerarquia = (int) $aRS->fields["nivel_jerarquia"];
		$anInstance->es_divisor = (int) $aRS->fields["es_divisor"];
		$anInstance->dim_depend = rtrim($aRS->fields["dim_depend"]);
		$anInstance->OwnerName = $aRS->fields["ownername"];
//
		$sql = 'select CLA_PROP from SI_PROPxIND where CLA_INDICADOR = '.$anInstance->IndicatorID.'  AND INICIO_TARGET = 1';
		$rst = $aRepository->ADOConnection->Execute($sql);
		if ($rst)
		{
			if (!$rst->EOF)
				$anInstance->begin_target = $rst->fields["cla_prop"];
		}
		$rst = null;
		
		$sql = 'select CLA_PROP from SI_PROPxIND where CLA_INDICADOR = '.$anInstance->IndicatorID.'  AND TARGET = 1';
		$rst = $aRepository->ADOConnection->Execute($sql);
		if ($rst)
		{
			if (!$rst->EOF)
				$anInstance->end_target = $rst->fields["cla_prop"];
		}
		$rst = null;
		
		if (!is_null($aRS->fields['decremental']))
			$anInstance->decremental = $aRS->fields['decremental'];
			
		if (!is_null($aRS->fields['period_key']))
			$anInstance->period_key = $aRS->fields['period_key'];
			
		if (!is_null($aRS->fields['nactual']))
			$anInstance->nactual = $aRS->fields['nactual'];
			
		if (!is_null($aRS->fields['only_last_period']))
			$anInstance->only_last_period = $aRS->fields['only_last_period'];
			
		if ($anInstance->CubeID > 0)
			$anInstance->oCube = BITAMCube::NewInstanceWithID($aRepository, $anInstance->CubeID);
			
		$anInstance->filter_sql = rtrim($aRS->fields['filter_sql']);
		$anInstance->niveles = rtrim($aRS->fields['niveles']);
		
// @EA 2011-02-17
		$anInstance->in_aggregation = (int) $aRS->fields['in_aggregation'];
		$anInstance->in_aggregation_original = $anInstance->in_aggregation;
//		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("IndicatorID", $aHTTPRequest->POST))
		{
			$aIndicatorID = $aHTTPRequest->POST["IndicatorID"];
			if (is_array($aIndicatorID))
			{
				$aCollection = BITAMIndicatorCollection::NewIndicatorCollection($aRepository, $aIndicatorID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMIndicator::NewIndicatorWithID($aRepository, (int) $aIndicatorID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMIndicator::NewIndicator($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("IndicatorID", $aHTTPRequest->GET))
		{
			$aIndicatorID = $aHTTPRequest->GET["IndicatorID"];
			$anInstance = BITAMIndicator::NewIndicatorWithID($aRepository, (int) $aIndicatorID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMIndicator::NewIndicator($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMIndicator::NewIndicator($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("CubeID", $anArray))
		{
			$this->CubeID = (int) $anArray["CubeID"];
		}		
 		if (array_key_exists("CubeIDString", $anArray))
		{
			$this->CubeIDString = (int) $anArray["CubeIDString"];
		}		
 		if (array_key_exists("IndicatorID", $anArray))
		{
			$this->IndicatorID = (int) $anArray["IndicatorID"];
		}
 		if (array_key_exists("IndicatorName", $anArray))
		{
			$this->IndicatorName = stripslashes($anArray["IndicatorName"]);
		}
// @EA 2011-02-17		
 		if (array_key_exists("in_aggregation", $anArray))
		{
			$this->in_aggregation = (int) $anArray["in_aggregation"];
		}
 		if (array_key_exists("in_aggregation_original", $anArray))
		{
			$this->in_aggregation_original = (int) $anArray["in_aggregation_original"];
		}
//		
 		if (array_key_exists("snom_anterior", $anArray))
		{
			$this->snom_anterior = stripslashes($anArray["snom_anterior"]);
		}
		if (array_key_exists("formula_usr", $anArray))
		{
			$this->formula_usr = stripslashes($anArray["formula_usr"]);
		}
		if (array_key_exists("filter_sql", $anArray))
		{
			$this->filter_sql = stripslashes($anArray["filter_sql"]);
		}
		if (array_key_exists("niveles", $anArray))
		{
			$this->niveles = stripslashes($anArray["niveles"]);
		}
		if (array_key_exists("formula_bd", $anArray))
		{
			$this->formula_bd = stripslashes($anArray["formula_bd"]);
		}
		if (array_key_exists("nom_tabla", $anArray))
		{
			$this->nom_tabla = stripslashes($anArray["nom_tabla"]);
		}
		if (array_key_exists("llaves", $anArray))
		{
			$this->llaves = stripslashes($anArray["llaves"]);
		}
		if (array_key_exists("tipo_agrupacion", $anArray))
		{
			$this->tipo_agrupacion = stripslashes($anArray["tipo_agrupacion"]);
		}
 		if (array_key_exists("formato", $anArray))
		{
			$this->formato = stripslashes($anArray["formato"]);
		}
 		if (array_key_exists("tipo_indicador", $anArray))
		{
			$this->tipo_indicador = (int) $anArray["tipo_indicador"];
		}
 		if (array_key_exists("reexpresados", $anArray))
		{
			$this->reexpresados = (int) $anArray["reexpresados"];
		}
 		if (array_key_exists("descripcion", $anArray))
		{
			$this->descripcion = stripslashes($anArray["descripcion"]);
		}
 		if (array_key_exists("filtro", $anArray))
		{
			$this->filtro = (int) $anArray["filtro"];
		}
 		if (array_key_exists("paretto", $anArray))
		{
			$this->paretto = (int) $anArray["paretto"];
		}
 		if (array_key_exists("tablas", $anArray))
		{
			$this->tablas = stripslashes($anArray["tablas"]);
		}
 		if (array_key_exists("donde", $anArray))
		{
			$this->donde = stripslashes($anArray["donde"]);
		}
 		if (array_key_exists("dimensiones", $anArray))
		{
			$this->dimensiones = $anArray["dimensiones"];
		}
		else
		{
			if (array_key_exists("dimensiones_EmptySelection", $anArray))
			{
				if ((bool) $anArray["dimensiones_EmptySelection"])
				{
					$this->dimensiones = array();
				}
			}
		}
		
 		if (array_key_exists("no_ejecutivo", $anArray))
		{
			$this->no_ejecutivo = (int) $anArray["no_ejecutivo"] == 1 ? 0 : 1;
		}
 		if (array_key_exists("es_atributo", $anArray))
		{
			$this->es_atributo = (int) $anArray["es_atributo"];
		}
 		if (array_key_exists("nivel_jerarquia", $anArray))
		{
			$this->nivel_jerarquia = (int) $anArray["nivel_jerarquia"];
		}
 		if (array_key_exists("es_divisor", $anArray))
		{
			$this->es_divisor = (int) $anArray["es_divisor"];
		}
 		if (array_key_exists("dim_depend", $anArray))
		{
			$this->dim_depend = stripslashes($anArray["dim_depend"]);
		}
 		if (array_key_exists("OwnerName", $anArray))
		{
			$this->OwnerName = stripslashes($anArray["OwnerName"]);
		}		
//
 		if (array_key_exists("begin_target", $anArray))
		{
			$this->begin_target = (int) $anArray["begin_target"];
		}
 		if (array_key_exists("end_target", $anArray))
		{
			$this->end_target = (int) $anArray["end_target"];
		}
 		if (array_key_exists("decremental", $anArray))
		{
			$this->decremental = (int) $anArray["decremental"];
		}
 		if (array_key_exists("period_key", $anArray))
		{
			$this->period_key = (int) $anArray["period_key"];
		}
 		if (array_key_exists("nactual", $anArray))
		{
			$this->nactual = (int) $anArray["nactual"];
		}
 		if (array_key_exists("only_last_period", $anArray))
		{
			$this->only_last_period = (int) $anArray["only_last_period"];
		}
 		if (array_key_exists("is_advanced", $anArray))
		{
			$this->is_advanced = (int) $anArray["is_advanced"];
			if ($this->is_advanced == 1)
				$this->tipo_indicador = 5;
		}
 		if (array_key_exists("is_custom", $anArray))
		{
			$this->is_custom = (int) $anArray["is_custom"];
		}
//		
		
		return $this;
	}
	
	function ChangeFormulaLastPeriod($sformula_bd, $bApply)
	{
		$sql = 'select NOM_FISICO as nom_fisico from SI_CPTO_ATRIB where CLA_CONCEPTO = '.$this->CubeID.' and ESTATUS = 1 order by ORDEN';
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		$saAtributos = array();
		$ncont = 0;
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$saAtributos[$ncont] = trim($rst->fields['nom_fisico']);
				$ncont++;
				$rst->MoveNext();
			}
		}
		
	    $sTablaPeriodo = trim($this->oCube->dim_periodo);
	    $sTablaCubo = trim($this->oCube->nom_tabla);
	    $sFieldFechaCubo = trim($this->oCube->fecha);
	    $sAlias = '';
		$sformula_ok = '';
		
	    if (strtolower($sTablaPeriodo) == strtolower($sTablaCubo))
	    	$sAlias = 't1';
	    else
	    	$sAlias = 't2';
	    	
	    
	    for ($ncont = 0; $ncont < count($saAtributos); $ncont++)
	    {
	    	$sIndicator = 't1.'.$saAtributos[$ncont].' ';
	    	$npos2 = stripos($sformula_bd, $sIndicator);
	    	if (!($npos2 === false))
	    	{
				$stoken_case = 'case when ' . $sAlias . '.' . $sFieldFechaCubo . ' = @DATE_LAST_DATA then ' . $sIndicator . ' else 0 end';
	            if ($bApply)
	                $sformula_ok = str_ireplace($sIndicator, $stoken_case, $sformula_bd);
	            else
	                $sformula_ok = str_ireplace($stoken_case, $sIndicator, $sformula_bd);
	    
	            return $sformula_ok;
	    	}
	    	else
	    	{
		    	$sIndicator = 't1.'.$saAtributos[$ncont];
		    	$npos2 = stripos($sformula_bd, $sIndicator.')');
		    	if (!($npos2 === false))
		    	{
					$stoken_case = 'case when ' . $sAlias . '.' . $sFieldFechaCubo . ' = @DATE_LAST_DATA then ' . $sIndicator . ' else 0 end';
		            if ($bApply)
		                $sformula_ok = str_ireplace($sIndicator.')', $stoken_case.')', $sformula_bd);
		            else
		                $sformula_ok = str_ireplace($stoken_case.')', $sIndicator.')', $sformula_bd);
		    
		            return $sformula_ok;
		    	}
	    	}
	    }
		
		unset ($rst);
		unset ($saAtributos);
		
		return $sformula_bd;
	}

	function save()
	{
		$this->IndicatorName = trim($this->IndicatorName);
		CheckMetadataDuplicated($this->Repository->ADOConnection, 'SI_INDICADOR', 'NOM_INDICADOR', $this->IndicatorName, 'CLA_INDICADOR', $this->IndicatorID, 'CLA_CONCEPTO = '.$this->CubeID);
		
		$bError = false;
		// Nos aseguramos que tengamos actualizada la propiedad
		//echo("<li>".$this->formula_usr."</li>");
		//echo("<li>".$this->formula_bd."</li>");
		if ($this->is_advanced)
		{
			$aIndicators = $this->fill_Indicators_for_advanced();
			$this->formula_bd = $this->get_FormulaBD_for_advanced($bError, $aIndicators);
			$this->filter_sql = '';
			$this->niveles = '';
			$this->tipo_indicador = 5;			
		}
		elseif ($this->is_custom == 0)
		{
			$aIndicators = $this->fill_Indicators();
			$this->formula_bd = $this->get_FormulaBD($bError, $aIndicators);
			$this->tipo_agrupacion = 'NORMAL';
			$this->tipo_indicador = 1;
			
			if ($this->es_atributo == 1)
				$this->formula_usr = '{'.$this->IndicatorName.'}';
		}
		else
		{
			$aIndicators = $this->fill_Indicators();
			$this->tipo_agrupacion = 'CUSTOM';
			$this->tipo_indicador = 1;
		}
		
// @EA 2011-02-18		
		if ($this->in_aggregation)
		{
			$this->filter_sql = '';
			$this->niveles = '';
			$this->period_key = -1;
			$this->nactual = 0;
			$this->only_last_period = 0;
		}
//
		
		$this->sdimensiones = $this->readDimensionsAfter();
		
		$lsformula_bd = $this->formula_bd;
	    if ($this->only_last_period > 0)
	    {
	        if (stripos($lsformula_bd, '@DATE_LAST_DATA') === false)
	        {
	            $lsformula_bd = $this->ChangeFormulaLastPeriod($lsformula_bd, true);
	            if (stripos($lsformula_bd, '@DATE_LAST_DATA') === false)
	            {
	                $this->only_last_period = 0;
	            }
	            $this->tipo_agrupacion = 'CUSTOM';
	        }
	    }
	    else
	    {
	        if (!(stripos($lsformula_bd, '@DATE_LAST_DATA') === false))
	        {
	            $lsformula_bd = $this->ChangeFormulaLastPeriod($lsformula_bd, false);
	        }
	    }
	    
		$this->formula_bd = $lsformula_bd;
		
		if ($this->tipo_indicador == 5)				
		{
			$this->period_key = -1;
			$this->nactual = 0;
			$this->filter_sql = '';
			$this->niveles = '';
			$this->only_last_period = 0;
		}
		
	 	if ($this->isNewObject())
		{
/*			
			$sql =  "SELECT MAX(CLA_INDICADOR) As IndicatorID".
						" FROM SI_INDICADOR";

			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}

			if (is_null($aRS->fields["indicatorid"]))
				$this->IndicatorID = 1;
			else
				$this->IndicatorID = $aRS->fields["indicatorid"] + 1;
*/
			$this->IndicatorID = BITAMGetNextID($this->Repository->ADOConnection, 'CLA_INDICADOR', 'SI_INDICADOR', '');

			// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
			$UserIDOwner = $_SESSION["BITAM_UserID"];
			// Si no existe en la sesion, lo tomamos de la cookie			
			if (is_null($UserIDOwner))
				$UserIDOwner = $_COOKIE["BITAM_UserID"];
			// Si aun asi es nulo, lo inicializamos en cero
			if (is_null($UserIDOwner))
				$UserIDOwner = "0";
								
			$sql = 
			"INSERT INTO SI_INDICADOR (".
			"CLA_CONCEPTO".
			",CLA_INDICADOR".
			",NOM_INDICADOR".
			",FORMULA_USR ".
			",FORMULA_BD ".
			",NOM_TABLA ". 
			",LLAVES ". 
			",TIPO_AGRUPACION ". 
			",FORMATO ". 
			",TIPO_INDICADOR ". 
			",CLA_USUARIO ". 
			",REEXPRESADOS ". 
			",DESCRIPCION ". 
			",FILTRO ". 
			",PARETTO ". 
			",TABLAS ". 
			",DONDE ". 
			",DIMENSIONES ". 
			",NO_EJECUTIVO ". 
			",ES_ATRIBUTO ". 
			",NIVEL_JERARQUIA ". 
			",ES_DIVISOR ". 
			",DIM_DEPEND ". 
			",CLA_OWNER ".
			",DECREMENTAL".
			",CLA_PERIODO".
			",ACTUAL".
			",ONLY_LAST_PERIOD".
			",FILTER_SQL".
			",NIVELES, CLA_PADRE, IN_AGGREGATION".
			") VALUES (".
			$this->CubeID.
			",".$this->IndicatorID.
			",".$this->Repository->ADOConnection->Quote($this->IndicatorName).
			",".$this->Repository->ADOConnection->Quote($this->formula_usr).
			",".$this->Repository->ADOConnection->Quote($this->formula_bd).
			",".$this->Repository->ADOConnection->Quote($this->nom_tabla).
			",".$this->Repository->ADOConnection->Quote($this->llaves).
			",".$this->Repository->ADOConnection->Quote($this->tipo_agrupacion).
			",".$this->Repository->ADOConnection->Quote($this->formato).
			",".$this->tipo_indicador.
			",".$UserIDOwner.
			",0". // $this->reexpresados.
			",".$this->Repository->ADOConnection->Quote($this->descripcion).
			",".$this->filtro.
			",".$this->paretto.
			",".$this->Repository->ADOConnection->Quote($this->tablas).
			",".$this->Repository->ADOConnection->Quote($this->donde).
			",".$this->Repository->ADOConnection->Quote($this->sdimensiones).
			",".$this->no_ejecutivo.
			",0". // $this->es_atributo.
			",1". // $this->nivel_jerarquia.
			",".$this->es_divisor.
			",".$this->Repository->ADOConnection->Quote($this->dim_depend).
			",".$UserIDOwner.
			",".$this->decremental.
			",".$this->period_key.
			",".$this->nactual.
			",".$this->only_last_period.
			",".$this->Repository->ADOConnection->Quote($this->filter_sql).
			",".$this->Repository->ADOConnection->Quote($this->niveles).",-1".
			",".$this->in_aggregation.
			")";
			
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			
			$smodel_options = $this->oCube->model_options;
			$cla_gpo_ind = (int) BITAMGetSiguiente($smodel_options);
			
			if ($cla_gpo_ind == 0)
			{
				$sql = 'select LLAVES as model_options from SI_CONCEPTO where CLA_CONCEPTO = '.$this->CubeID;
				
				$rst = $this->Repository->ADOConnection->Execute($sql);
				
				if ($rst and !$rst->EOF)
				{
					$smodel_options = $rst->fields['model_options'];
					$cla_gpo_ind = (int) BITAMGetSiguiente($smodel_options);
				}
			}
			
			if ($cla_gpo_ind > 0)
			{
				$sql = 
				"insert into SI_GPO_IND_DET(CLA_GPO_IND, CLA_INDICADOR) values (".$cla_gpo_ind.",".$this->IndicatorID.")";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					savelogfile("Error accessing SI_GPO_IND_DET table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql, true);
				}
			}
			
// @EA 2011-02-18
			if ($this->in_aggregation)
				$this->AddColumnInAggregations($this->IndicatorID);
//
			
		}
		else
		{
			$sql = "UPDATE SI_INDICADOR SET ".
						"NOM_INDICADOR = ".$this->Repository->ADOConnection->Quote($this->IndicatorName).
						",FORMULA_USR = ".$this->Repository->ADOConnection->Quote($this->formula_usr).
						",FORMULA_BD = ".($this->es_atributo == 1 ? "FORMULA_BD" : $this->Repository->ADOConnection->Quote($this->formula_bd)).
						",NOM_TABLA = ".$this->Repository->ADOConnection->Quote($this->nom_tabla).
						",LLAVES = ".$this->Repository->ADOConnection->Quote($this->llaves).
						",TIPO_AGRUPACION = ".$this->Repository->ADOConnection->Quote($this->tipo_agrupacion).
						",FORMATO = ".$this->Repository->ADOConnection->Quote($this->formato).
						",TIPO_INDICADOR = ".$this->tipo_indicador.
						",REEXPRESADOS = ".$this->reexpresados.
						",DESCRIPCION = ".$this->Repository->ADOConnection->Quote($this->descripcion).
						",FILTRO = ".$this->filtro.
						",PARETTO = ".$this->paretto.
						",TABLAS = ".$this->Repository->ADOConnection->Quote($this->tablas).
						",DONDE = ".$this->Repository->ADOConnection->Quote($this->donde).
						",DIMENSIONES = ".$this->Repository->ADOConnection->Quote($this->sdimensiones).
						",NO_EJECUTIVO = ".$this->no_ejecutivo.
						",ES_ATRIBUTO = ".$this->es_atributo.
						",NIVEL_JERARQUIA = ".$this->nivel_jerarquia.
						",ES_DIVISOR = ".$this->es_divisor.
						",DIM_DEPEND = ".$this->Repository->ADOConnection->Quote($this->dim_depend).
						",DECREMENTAL = ".$this->decremental.
						",CLA_PERIODO = ".$this->period_key.
						",ACTUAL = ".$this->nactual.
						",ONLY_LAST_PERIOD = ".$this->only_last_period.
						",FILTER_SQL = ".$this->Repository->ADOConnection->Quote($this->filter_sql).
						",NIVELES = ".$this->Repository->ADOConnection->Quote($this->niveles).
						",IN_AGGREGATION = ".$this->in_aggregation.
				   " WHERE CLA_INDICADOR = ".$this->IndicatorID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			
			$sql = 'update SI_PROPxIND set TARGET = 0, INICIO_TARGET = 0 WHERE CLA_INDICADOR = '.$this->IndicatorID;
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
			}
			
			if ($this->begin_target > 0)
			{
				$sql = 'update SI_PROPxIND set INICIO_TARGET = 1 WHERE CLA_INDICADOR = '.$this->IndicatorID.' AND CLA_PROP = '.$this->begin_target;
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die("Error accessing SI_PROPxIND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				}
			}
			
			if ($this->end_target > 0)
			{
				$sql = 'update SI_PROPxIND set TARGET = 1 WHERE CLA_INDICADOR = '.$this->IndicatorID.' AND CLA_PROP = '.$this->end_target;
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die("Error accessing SI_PROPxIND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
				}
			}
			
			if (!($this->is_advanced))
				$this->ActualizaFormulas($this->CubeID, $this->IndicatorID, $this->snom_anterior, $aIndicators);
				
// @EA 2011-02-18
			if ($this->in_aggregation != $this->in_aggregation_original)
			{
				if ($this->in_aggregation)
					$this->AddColumnInAggregations($this->IndicatorID);
				else
					$this->DropColumnInAggregations($this->IndicatorID);
			}
//
		}
		
		ExportCubeToPHP($this->Repository, $this->CubeID, true, true);
		
		return $this;
	}

	function remove()
	{		
		/* ====================================================================================
			Garantizamos la integridad del repositorio
		   ==================================================================================== */
// @EA 2011-02-18		
		if ($this->in_aggregation)
			$this->DropColumnInAggregations($this->IndicatorID);
//			
		
		$sql = "DELETE FROM SI_PROPxIND WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_PROPxIND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		$sql = "DELETE FROM SI_OPxIND WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_OPxIND table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_IND_LINEA WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_IND_LINEA table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_IND_REPORTE WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_IND_REPORTE table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_GPO_IND_DET WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_GPO_IND_DET table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_COMENTARIO WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_COMENTARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_COMENTARIO_FULL WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_COMENTARIO_FULL table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		$sql = "delete from SI_INDL_NUSER where CONSECUTIVO in ".
    	"(SELECT CLA_INDLINEA FROM SI_INDxLINEA WHERE CLA_INDICADOR = ".$this->IndicatorID.")";
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_INDL_NUSER table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}

		$sql = "DELETE FROM SI_INDxLINEA WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_INDxLINEA table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		// Finalmente los grupos de indicadores		
		$sql = "DELETE FROM SI_INDICADOR WHERE CLA_INDICADOR = ".$this->IndicatorID;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		ExportCubeToPHP($this->Repository, $this->CubeID, true, true);
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->IndicatorID < 0);
	}

	function get_Image()
	{
		return "<img src=images/indicadores.gif>";
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New Indicator");
		}
		else
		{
			return $this->IndicatorName;
		}
	}

	function get_QueryString()
	{
		if ($this->IndicatorID < 0)
		{
			return "BITAM_PAGE=Indicator";
		}
		else
		{
			return "BITAM_PAGE=Indicator&IndicatorID=".$this->IndicatorID;
		}
	}

	function get_Parent()
	{
		return $this->Repository;
		//return BITAMIndicatorCollection::NewIndicatorCollection($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorID';
	}

	
	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMIndicatorPropCollection::NewIndicatorPropCollection($this->Repository, $this->IndicatorID);
		}
		
		return $myChildren;
	}	
	
	function readDimensionsAfter()
	{
		$nMaxID = 0;
		foreach ($this->adimensiones as $key => $value )
		{
			$nMaxID = (int) $key;
		}
		$sDims = str_repeat('0', $nMaxID);
		
//		print '<br>$sDims ='.$sDims.'<br>';

		foreach ($this->dimensiones as $key => $value )
		{
            $sLeft = '';
            if ($value > 1)
                $sLeft = substr($sDims, 0, $value - 1);

            $sRight = '';
            if ($value < $nMaxID)
                $sRight = substr($sDims, $value);

            $sDims = $sLeft.'1'.$sRight;
		}

		return $sDims;
	}
	
	function readDimensions($aRepository = null)
	{
		$aDimensions = array();
		$aDimensionsConsecutives = array();
		
		$sql = 'SELECT t1.CONSECUTIVO as consecutivo, t1.NOM_LOGICO as nom_logico '.
               'FROM   SI_CPTO_LLAVE t1, SI_DESCRIP_ENC t2 '.
               'WHERE t1.CLA_CONCEPTO = '.$this->CubeID.' '.
               ' and t1.ESTATUS <> 2 and t2.CLA_DESCRIP = t1.CLA_DESCRIP '.
               'ORDER BY t1.CONSECUTIVO';

        if (is_null($aRepository))
			$rst = $this->Repository->ADOConnection->Execute($sql);
		else
			$rst = $aRepository->ADOConnection->Execute($sql);
			
		$this->adimensiones = array();
		
//		$this->adimensiones[0] = '';
		
		$top = 0;
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$this->adimensiones[$rst->fields['consecutivo']] = $rst->fields['nom_logico'];
				$top++;
				$aDimensionsConsecutives[$top] = (int) $rst->fields['consecutivo'];
				
				$rst->MoveNext();
			}
		}
		
		if ($this->sdimensiones != '')
		{
			$nlen = strlen($this->sdimensiones);
			for ($i = 1; $i <= $top; $i++)			
			{
				if ($nlen >= $aDimensionsConsecutives[$i])
				{
					if ((int)(substr($this->sdimensiones, $aDimensionsConsecutives[$i]-1, 1)) == 1)
						$aDimensions[$aDimensionsConsecutives[$i]] = $aDimensionsConsecutives[$i];
				}
				else
					$aDimensions[$aDimensionsConsecutives[$i]] = $aDimensionsConsecutives[$i];
					
			}
		}
		else
		{
			$top = count($aDimensionsConsecutives);
			for ($i = 1; $i <= $top; $i++)			
			{
				$aDimensions[$aDimensionsConsecutives[$i]] = $aDimensionsConsecutives[$i];
			}
		}
		
		$this->dimensiones = $aDimensions; 
	}
	
	function fill_Indicators()
	{
// Llenamos un arreglo con los nombres de indicadores + formulas de base de datos
		$sql = 
		"select NOM_INDICADOR AS IndicatorName ".
		"		,FORMULA_BD AS FormulaBD ".
		"from   SI_INDICADOR ".
		"where  CLA_CONCEPTO = ".$this->CubeID.
		"		and CLA_INDICADOR <> ".$this->IndicatorID.
		"		and TIPO_INDICADOR IN ( 1, 3 ) ".
		"order by NOM_INDICADOR";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$aIndicators = array();
		while (!$aRS->EOF)
		{		
			$aIndicators["{".$aRS->fields["indicatorname"]."}"] = $aRS->fields["formulabd"];
			$aRS->MoveNext();
		}
		return $aIndicators;
	}
	
	function fill_Indicators_for_advanced()
	{
// Llenamos un arreglo con los nombres de indicadores + formulas de base de datos
		$sql = 
		"select t1.CLA_INDICADOR as indicator_id, t1.NOM_INDICADOR AS indicator_name, ".
		"		t2.NOM_CONCEPTO as cube_name ".
		"from   SI_INDICADOR t1, SI_CONCEPTO t2 ".
		"where  t2.TIPO_CUBO in ( 3, 4 ) ".
		"		and TIPO_INDICADOR IN ( 1, 3 ) ".
		"		and t2.CLA_CONCEPTO = t1.CLA_CONCEPTO ".
		"order by t2.NOM_CONCEPTO, t1.NOM_INDICADOR";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$aIndicators = array();
		while (!$aRS->EOF)
		{		
			$aIndicators["{".$aRS->fields["cube_name"]."}.{".$aRS->fields["indicator_name"]."}"] = "{".$aRS->fields["indicator_id"]."}";
			$aRS->MoveNext();
		}
		return $aIndicators;
	}
	
	function get_FormulaBD_for_advanced($bError, &$aIndicators)
	{
// @EA 2005-06-30 Obtiene la formula de base de datos correspondiente al indicador capturado	
		$nposini = 0;
		$nposfin = 0;
		$sindicador_usr = "";
		$sindicator_bd = "";
		$bError = false;
// Obtenemos la formula de usuario original		
		$sformula_usr = $this->formula_usr;
		$sformula_bd = $this->formula_usr;
// Buscamos la primera ocurrencia		
		$nposini = strpos($sformula_usr, "{");
		while (!($nposini === false))
		{
// Buscamos donde termina la ocurrencia			
			$nposfin = strpos($sformula_usr, "}", $nposini+1);
			if ( $nposfin === false )
			{
				break;
			}
			else
			{
				$nposfin = strpos($sformula_usr, "}", $nposfin+1);
				if ( $nposfin === false )
				{
					break;
				}
// Obtenemos el nombre del indicador sin llaves {}				
				$sindicador_usr = substr($sformula_usr, $nposini, $nposfin-$nposini+1 );
// Obtenemos la formula de base de datos del indicador correspondiente

				if (array_key_exists($sindicador_usr, $aIndicators))
				{
					$sindicator_bd = $aIndicators[$sindicador_usr];
					
					//echo("<li>".$sindicator_bd."</li>");
	
					$sformula_bd = str_replace($sindicador_usr, $sindicator_bd, $sformula_bd);
					//echo("<li>".$sformula_bd."</li>");
				}
				$nposini = strpos($sformula_usr, "{", $nposfin+1);
				if ($nposini === false) 
					break;
			}
		}
		return $sformula_bd;
	}
	
	function get_FormulaBD($bError, &$aIndicators)
	{
// @EA 2005-06-30 Obtiene la formula de base de datos correspondiente al indicador capturado	
		$nposini = 0;
		$nposfin = 0;
		$sindicador_usr = "";
		$sindicator_bd = "";
		$bError = false;
// Obtenemos la formula de usuario original		
		$sformula_usr = $this->formula_usr;
		$sformula_bd = $this->formula_usr;
// Buscamos la primera ocurrencia		
		$nposini = strpos($sformula_usr, "{");
		while (!($nposini === false))
		{
// Buscamos donde termina la ocurrencia			
			$nposfin = strpos($sformula_usr, "}", $nposini+1);
			if ( $nposfin === false )
			{
				break;
			}
			else
			{
// Obtenemos el nombre del indicador sin llaves {}				
				$sindicador_usr = substr($sformula_usr, $nposini, $nposfin-$nposini+1 );
// Obtenemos la formula de base de datos del indicador correspondiente
				//echo("<li>".$sindicador_usr."</li>");
				$sindicator_bd = $aIndicators[$sindicador_usr];
				//echo("<li>".$sindicator_bd."</li>");
// Lo reemplazamos en la formula de usuario original
				if ($this->IsNecesaryParentesis($sindicator_bd, $bError))
				{
					if ($bError)
					{
						return $sformula_bd;
					}
					else
						$sindicator_bd = "(".$sindicator_bd.")";
				}
				$sformula_bd = str_replace($sindicador_usr, $sindicator_bd, $sformula_usr);
				$sformula_usr = $sformula_bd;
				//echo("<li>".$sformula_bd."</li>");
				$nposini = strpos($sformula_usr, "{");
				if ($nposini === false) 
					break;
			}
		}
		return $sformula_bd;
	}
	

	function IsNecesaryParentesis($sExpresion, &$bError)
	{	
//Recibe una expresion con un anidamiento correcto de parentesis y
//verifica si es necesario agregar parentesis externos para poder utilizar
//dicha expresion en una mayor		
		return (strpos($sExpresion, '+') !== false or strpos($sExpresion, '-') !== false or strpos($sExpresion, '/') !== false or strpos($sExpresion, '*') !== false);
	}

	function ReplaceFirstParentesis($sExpresion, &$bError)
	{
//Elimina el anidamiento de parentesis mas interno en la Expresion dada
//sustituyendola por un caracter especial		
		$iLastParentesis = 0;
		$iFirstParentesis = 0;
		$i = 0;
		$sParentesis = "";
		$sStringBefore = "";
		$schar = "";
		$xChar = "";
	    $bError = false;
	    $xChar = "�";
	
	    $iLastParentesis = strpos($sExpresion, ")");
	    If (!($iLastParentesis >= 0))
	    {
	    	$bError = true;
	    	return "";
	    }
	    
	    $i = $iLastParentesis - 1;
	    $bSalir = false;
	    while ($i >= 0 and !$bSalir )
	    {
	        $schar = substr($sExpresion, $i, 1);
	        if ($schar == "(") 
	        	$bSalir = true;
	        else
	        	$i = $i - 1;
	    }        	
	    if ($i = -1) 
	    {
	    	$bError = true;
	    	return "";
	    }
	    
	    $iFirstParentesis = $i;
	    $sParentesis = substr($sExpresion, $iFirstParentesis, $iLastParentesis - $iFirstParentesis + 1);
	    
	    $sStringBefore = substr($sExpresion, 0, $iFirstParentesis);
	    $sExpresion = str_replace($sParentesis, $xChar, $sExpresion);
	    return $sStringBefore.$sExpresion;
	}

	
	function ActualizaFormulas($ncube_key, $nindicator_key, $snom_anterior, &$aIndicators)
	{
// @EA 2009-09-15 Actualiza todos los indicadores que estan relacionados con el actual

		$tmp_formula_usr = $this->formula_usr;
		
		$aIndicators['{'.$this->IndicatorName.'}'] = $this->formula_bd;
		$aIndicators['{'.$snom_anterior.'}'] = $this->formula_bd;
		
	    $strCadena = '{'.$snom_anterior.'}';
	    $strLlaves = $nindicator_key;
	    $strProcesados_keys = ','.$nindicator_key.',';
	    $strValor = '';
	    $lngClave = 0;
	    $blnIndicInicial = true;
	    
	    $aIndicatorsUpdates = array();
	    
	    $aIndicatorsUpdates[] = $snom_anterior;
	    
	    while ($strCadena != '')
	    {
	    	$strValor = BITAMGetSiguiente($strCadena, chr(31));
	    	$lngClave = BITAMGetSiguiente($strLlaves);
	    	
			$sql = 
			'select CLA_INDICADOR, FORMULA_USR, NOM_INDICADOR, NOM_TABLA ' .
	        'from   SI_INDICADOR ' .
	        'where  CLA_CONCEPTO = ' . $ncube_key . ' and ' .
	        '       CLA_INDICADOR <> ' . $lngClave . ' and ' .
	        '       TIPO_AGRUPACION = \'NORMAL\' and ' .
	        '       ( TIPO_INDICADOR <> 5 and TIPO_INDICADOR <> 6 ) and ' .
	        ' FORMULA_USR like \'%' . BITAMReemplazaComillas($strValor) . '%\'';
	        
			$rst = $this->Repository->ADOConnection->Execute($sql);
			
			if ($rst)
			{
				while (!$rst->EOF)
				{
					$indicator_update = $rst->fields["cla_indicador"];
					$indicator_name = $rst->fields["nom_indicador"];
					
					if (strpos($strProcesados_keys, ','.$indicator_update.',') === false)
					{
						$aIndicatorsUpdates[] = $indicator_name;						

						$strProcesados_keys = $strProcesados_keys.$indicator_update.',';
						
						if ($strCadena == '')
							$strCadena = '{'.$indicator_name.'}';
						else
							$strCadena = $strCadena.chr(31).'{'.$indicator_name.'}';
						
						if ($strLlaves == '')
							$strLlaves = $indicator_update;
						else
							$strLlaves = $strLlaves.','.$indicator_update;
						
						$formula_usr_update = $rst->fields["formula_usr"];
						
						if ($blnIndicInicial)	
						{
							$formula_usr_update = str_ireplace('{'.$snom_anterior.'}', '{'.$this->IndicatorName.'}', $formula_usr_update);
						}
						
						$this->formula_usr = $formula_usr_update;
						
						$berror = false;
						
						$formula_bd_update = $this->get_FormulaBD($berror, $aIndicators);
						
						$aIndicators['{'.$indicator_name.'}'] = $formula_bd_update;
						
						$formula_usr_update = BITAMReemplazaComillas($formula_usr_update);
						
						$formula_bd_update = BITAMReemplazaComillas($formula_bd_update);
						
						$sql = 'update SI_INDICADOR set FORMULA_USR = \''.$formula_usr_update.'\', FORMULA_BD = \''.$formula_bd_update.'\' where CLA_INDICADOR = '.$indicator_update;
						
						$this->Repository->ADOConnection->Execute($sql);
					}
										
					$rst->MoveNext();
				}
			}
			$blnIndicInicial = false;
	    }
		$this->formula_usr = $tmp_formula_usr;
		
		$blnIndicInicial = true;
		for ($ncont = 0; $ncont < count($aIndicatorsUpdates); $ncont++)
		{
			$this->ActualizaAlarmas($ncube_key, $aIndicatorsUpdates[$ncont],$blnIndicInicial, $aIndicators);
			$this->ActualizaOperandos($ncube_key, $aIndicatorsUpdates[$ncont],$blnIndicInicial, $aIndicators);
			$blnIndicInicial = false;
		}
		
		$sql = 'select NOM_CONCEPTO from SI_CONCEPTO where CLA_CONCEPTO = '.$ncube_key;
		
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			if (!$rst->EOF)
			{
				$snom_cubo = $rst->fields["nom_concepto"];
				$this->ActualizaIndicadoresAvanzados($snom_cubo, $snom_anterior);
			}
		}
		
	}
	
	function ActualizaOperandos($ncube_key, $snom_indicador, $blnIndicInicial, &$aIndicators)
	{
// @EA 2009-09-15 Actualiza todos los operandos que estan relacionados con el actual
		$tmp_formula_usr = $this->formula_usr;
		
		$sql = 
		'select CLA_INDICADOR, FORMULA_USR, CLA_OP ' .
        'from   SI_OPxIND ' .
        'where  CLA_INDICADOR IN (SELECT CLA_INDICADOR FROM SI_INDICADOR '.
        '						  WHERE CLA_CONCEPTO = '.$ncube_key.') and AVANZADO = 0 and '.
        ' FORMULA_USR like \'%{' . BITAMReemplazaComillas($snom_indicador) . '}%\'';
        
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$indicator_update = $rst->fields["cla_indicador"];
				$op_update = $rst->fields["cla_op"];
				$formula_usr_update = $rst->fields["formula_usr"];				
				
				if ($blnIndicInicial)	
				{
					$formula_usr_update = str_ireplace('{'.$snom_indicador.'}', '{'.$this->IndicatorName.'}', $formula_usr_update);
				}
				
				$this->formula_usr = $formula_usr_update;
				
				if (strpos($formula_usr_update, '}.{') === false)
				{
					$berror = false;			
					$formula_bd_update = $this->get_FormulaBD($berror, $aIndicators);
					
					$formula_usr_update = BITAMReemplazaComillas($formula_usr_update);
					
					$formula_bd_update = BITAMReemplazaComillas($formula_bd_update);
					
					$sql = 'update SI_OPxIND set FORMULA_USR = \''.$formula_usr_update.'\', FORMULA_BD = \''.$formula_bd_update.'\' where CLA_INDICADOR = '.$indicator_update.' and CLA_OP ='.$op_update;
					
					$this->Repository->ADOConnection->Execute($sql);
				}
				
				$rst->MoveNext();
			}
		}
		$this->formula_usr = $tmp_formula_usr;
	}
	
	function ActualizaAlarmas($ncube_key, $snom_indicador, $blnIndicInicial, &$aIndicators)
	{
// @EA 2009-09-15 Actualiza todas los alarmas que estan relacionados con el actual
		$tmp_formula_usr = $this->formula_usr;
		
		$sql = 
		'select t1.CLA_INDICADOR, t1.VALOR, t1.CLA_PROP ' .
        'from   SI_PROPxIND t1, SI_INDICADOR t2 ' .
        'where  t2.CLA_CONCEPTO = ' . $ncube_key . ' and ' .
        '       ( t2.TIPO_INDICADOR <> 5 and t2.TIPO_INDICADOR <> 6 ) and ' .
        '		( t1.CLA_INDICADOR_PROP = -1 or t1.CLA_INDICADOR_PROP is null ) and '.
        ' t1.VALOR like \'%{' . BITAMReemplazaComillas($snom_indicador) . '}%\'';
        
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$indicator_update = $rst->fields["cla_indicador"];
				$prop_update = $rst->fields["cla_prop"];
				$formula_usr_update = $rst->fields["valor"];				
				
				if ($blnIndicInicial)	
				{
					$formula_usr_update = str_ireplace('{'.$snom_indicador.'}', '{'.$this->IndicatorName.'}', $formula_usr_update);
				}
				
				$this->formula_usr = $formula_usr_update;
	
				$berror = false;			
				$formula_bd_update = $this->get_FormulaBD($berror, $aIndicators);
				
				$formula_usr_update = BITAMReemplazaComillas($formula_usr_update);
				
				$formula_bd_update = BITAMReemplazaComillas($formula_bd_update);
				
				$sql = 'update SI_PROPxIND set VALOR = \''.$formula_usr_update.'\', VALOR_BD = \''.$formula_bd_update.'\' where CLA_INDICADOR = '.$indicator_update.' and CLA_PROP ='.$prop_update;
				
				$this->Repository->ADOConnection->Execute($sql);
									
				$rst->MoveNext();
			}
		}
		$this->formula_usr = $tmp_formula_usr;
	}
	
	function ActualizaIndicadoresAvanzados($snom_cubo, $snom_indicador)
	{
// @EA 2009-09-15 Actualiza todas los indicadores avanzados que estan relacionados con el actual
		$snom_indicador_ant = '{'.$snom_cubo.'}.{'.$snom_indicador.'}';
		$snom_indicador_new = '{'.$snom_cubo.'}.{'.$this->IndicatorName.'}';

		$sql = 
		'select CLA_INDICADOR, FORMULA_USR ' .
        'from SI_INDICADOR ' .
        'where (TIPO_INDICADOR = 5 or TIPO_INDICADOR = 6) and '.
        ' FORMULA_USR like \'%' . BITAMReemplazaComillas($snom_indicador_ant) . '%\'';
        
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				$indicator_update = $rst->fields["cla_indicador"];
				$formula_usr_update = $rst->fields["formula_usr"];
				
				$formula_usr_update = str_ireplace($snom_indicador_ant, $snom_indicador_new, $formula_usr_update);
				
				$formula_usr_update = BITAMReemplazaComillas($formula_usr_update);
				
				$sql = 'update SI_INDICADOR set FORMULA_USR = \''.$formula_usr_update.'\' where CLA_INDICADOR = '.$indicator_update;
				
				$this->Repository->ADOConnection->Execute($sql);
									
				$rst->MoveNext();
			}
		}
		
// @EA 2010-07-28		
		$sql = 
		'select CLA_INDICADOR_PROP as cla_indicador, VALOR as formula_usr '.
		'from	SI_PROPxIND '.
		'where	NOT CLA_INDICADOR_PROP is NULL and '.
		'		VALOR like '.BITAMReemplazaComillas('%'.$snom_indicador_ant.'%', true);
		
		$rst = $this->Repository->ADOConnection->Execute($sql);
		
		$aIndicators = array();
		
		if ($rst)
		{
			while (!$rst->EOF)
			{
				 $aIndicators[$rst->fields['cla_indicador']] = $rst->fields['formula_usr'];
				 $rst->MoveNext();
			}
		}
		
		foreach ($aIndicators as $IndicatorKey => $strFormulaUsr)
		{
			$formula_usr_update = str_ireplace($snom_indicador_ant, $snom_indicador_new, $strFormulaUsr);
			
			$sql = 
			'update	SI_PROPxIND set VALOR = '.BITAMReemplazaComillas($formula_usr_update, true).' '.
			'where	CLA_INDICADOR_PROP = '.$IndicatorKey;
			
			$this->Repository->ADOConnection->Execute($sql);
			
			savelogfile('RenameIndicator -> SI_INDICADOR -> IndicatorKey = '.$IndicatorKey.' -> LastFormula = '.$strFormulaUsr.' -> ActualFormula = '.$formula_usr_update);			
		}
		
		unset ($aIndicators);
//
		
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		
		$this->generateBeforeFormCode(null);
	
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorName";
		$aField->Title = translate("Indicator Name");
		$aField->Type = "String";
		$aField->Size = 100;
		$myFields[$aField->Name] = $aField;
/*		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorID";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 10;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
*/		

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "filter_sql";
		$aField->Type = "String";
		$aField->Size = 500;
		$aField->IsVisible = false;
		
		global $gsURLWEBHTML;
		
		$gsURLWEBHTML = GetLocalURLWEBG6();
		
		if ($gsURLWEBHTML != '' and !$this->es_atributo and !is_null($this->oCube))
		{
			if ($this->oCube->CubeType == ROLAP and $this->tipo_indicador != 5)
			{
				$aField->Title = translate("Data Filter");
				$aField->AfterMessage = 
				"<img id=indFilterImg
				name=indFilterImg 
				style=\"display:none; cursor:pointer\"
				src=\"images/indfilter.gif\"
		 		onclick=\"javascript: selectFilter();\"><input type=\"text\"
				style=\"background-color:transparent; border:none; width:30px;
				font-family:verdana; font-size:13px;\" readonly name=\"txtIndFilter\"
				id=\"txtIndFilter\" value=\"\">";
			}
		}
		$myFields[$aField->Name] = $aField;

		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "niveles";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 500;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CubeIDString";
		$aField->Title = "";
		$aField->Type = "String";
		$aField->Size = 10;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		if (!$this->isNewObject())		
		{
			
			$sql = 'select t1.CLA_PROP, t1.NOM_PROP from SI_PROPIEDAD t1, SI_PROPxIND t2 where '.
					't2.CLA_PROP = t1.CLA_PROP and t2.CLA_INDICADOR = '.$this->IndicatorID.'  group by t1.CLA_PROP, t1.NOM_PROP order by t1.NOM_PROP';
			$rst = $this->Repository->ADOConnection->Execute($sql);
					
			$aTargets1 = array();
			
			if ($rst)
			{
				while (!$rst->EOF)
				{
					$aTargets1[$rst->fields['cla_prop']] = $rst->fields['nom_prop'];
					$rst->MoveNext();
				}
			}
			
			if (count($aTargets1) > 0)
			{
				$aTargets1[0] = '(none)';
			
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "begin_target";
				$aField->Title = translate("Begin/End Target");
				$aField->Type = "Object";
				$aField->Options = $aTargets1;
				$aField->CloseCell = false;			
				$myFields[$aField->Name] = $aField;				
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "end_target";
				$aField->Type = "Object";
				$aField->Options = $aTargets1;			
				$aField->CloseCell = false;						
				$myFields[$aField->Name] = $aField;
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "decremental";
				$aField->Type = "Boolean";
				$aField->VisualComponent = "Checkbox";
				$aField->AfterMessage = translate("Decrease");
				$aField->CloseCell = true;									
				$myFields[$aField->Name] = $aField;
			}
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "formato";
		$aField->Title = translate("Format");
		$aField->Type = "String";
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "formula_bd";
		$aField->Type = "String";
		$aField->Size = 500;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		$aIndicatorFilter = array();
		$aIndicatorFilter[0] = "All";
		$aIndicatorFilter[1] = "Different to 0";
		$aIndicatorFilter[2] = "Higher than 0";
		$aIndicatorFilter[3] = "Smaller than 0";
		$aIndicatorFilter[4] = "Paretto";
		$aIndicatorFilter[5] = "Different to 100%";
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "filtro";
		$aField->Title = translate("Indicator filter");
		$aField->Type = "Object";
		$aField->Options = $aIndicatorFilter;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "paretto";
		$aField->Title = translate("80/20 rule");
		$aField->Type = "Integer";
		$aField->FormatMask = "0";		
		$aField->AfterMessage = "%";				
		
		$myFields[$aField->Name] = $aField;		
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "resolveinquery";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Resolve in Query");
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "no_ejecutivo";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Show in Desktop");
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "es_divisor";
		$aField->Title = "";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		$aField->AfterMessage = translate("Apply Divisor");
		$myFields[$aField->Name] = $aField;
		
		if (!is_null($this->oCube))
		{
			if ($this->oCube->CubeType == ROLAP)				
			{
				if ($this->oCube->DBID == SQL_SERVER OR $this->oCube->DBID == MYSQL OR $this->oCube->DBID == ORACLE)
				{
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "only_last_period";
					$aField->Title = "";
					$aField->Type = "Boolean";
					$aField->VisualComponent = "Checkbox";
					$aField->AfterMessage = translate("Data from  latest period");
					$aField->OnChange = 'UpdateOnlyLastPeriod()';
					$myFields[$aField->Name] = $aField;
				}
			}
		}
		
		if (!$this->es_atributo)
		{
			if (!is_null($this->oCube))
			{
				if ($this->oCube->CubeType == ROLAP)				
				{
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "is_advanced";
					$aField->Type = "Boolean";
					$aField->VisualComponent = "Checkbox";
					$aField->AfterMessage = translate("Advanced");
					$aField->OnChange = 'UpdateIsAdvanced()';
					$myFields[$aField->Name] = $aField;		
					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "is_custom";
					$aField->Type = "Boolean";
					$aField->VisualComponent = "Checkbox";
					$aField->AfterMessage = translate("Custom SQL");
					$aField->OnChange = 'UpdateIsCustom()';
					$myFields[$aField->Name] = $aField;		
					
// @EA 2011-02-17					
					$aField = BITAMFormField::NewFormField();
					$aField->Name = "in_aggregation";
					$aField->Type = "Boolean";
					$aField->VisualComponent = "Checkbox";
					$aField->AfterMessage = translate("Save in Aggregations");
					$aField->OnChange = 'UpdateInAggregation()';
					$myFields[$aField->Name] = $aField;		
//
				}
			}
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "descripcion";
		$aField->Title = translate("Description");
		$aField->Type = "LargeString";
		$aField->Size = 250;
		$myFields[$aField->Name] = $aField;

		//echo("<li>".$this->CubeID."&".$this->IndicatorID."</li>");
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "formula_usr";
		$aField->Title = translate("Formula");
		$aField->Type = "LargeString";
		$aField->IsDisplayOnly = true;
//		$FormName = "BITAMCubeIndicator";
		
		// Solo se permiten editar formulas si son indicadores compuestos
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		if (!$this->es_atributo and $this->canEdit($aUser))
		{
			
		// text-align:center;vertical-align:top;width:64px\
/*
			$aField->AfterMessage = 
			"<input type=\"button\" name=\"pbbuild\" value=\"".translate("Build")."...\" style=\"text-align:center;vertical-align:top;width:64px\" ".
			"onclick=\"javascript: ".$FormName."_SaveForm.".$aField->Name.".value = showModalDialog('build_indicator.php?CubeID=".$this->CubeID."&IndicatorID=".$this->IndicatorID."', new Array('', ".$FormName."_SaveForm.".$aField->Name.".value), 'help: no; status: no; scroll: no; resizable: yes;')\">"; 
*/			

			$aField->AfterMessage = 
			"<input type=\"button\" name=\"pbbuild\" value=\"".translate("Build")."...\" style=\"text-align:center;vertical-align:top;width:64px\" ".
			"onclick=\"javascript:addIndicatorToFormula(".$this->CubeID.",".$this->IndicatorID.");\">";

		}
		// padding-top:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\
		$myFields[$aField->Name] = $aField;
		
		if (!is_null($this->oCube))
		{
			if ($this->oCube->CubeType == ROLAP)				
			{
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "nactual";
				$aField->Title = "Current";
				$aField->Type = "Integer";
				$aField->FormatMask = "0";
				$aField->CloseCell = false;		
				$myFields[$aField->Name] = $aField;		
				
				$sql = 'select CLA_PERIODO as cla_periodo, NOM_PERIODO as nom_periodo from SI_PERIODO where CLA_PERIODO > 0 and NOT CLA_PERIODO IS NULL ORDER BY CLA_PERIODO';
				$rst = $this->Repository->ADOConnection->Execute($sql);
				$aPeriodsR = array();
				$aPeriodsR[-1] = '(None)';
				if ($rst)
				{
					while (!$rst->EOF)
					{
						$aPeriodsR[$rst->fields['cla_periodo']] = $rst->fields['nom_periodo'];
						$rst->MoveNext();
					}
				}
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "period_key";
				$aField->Type = "Object";
				$aField->Options = $aPeriodsR;
				$aField->CloseCell = true;		
				$myFields[$aField->Name] = $aField;
				
		        $aField = BITAMFormField::NewFormField();
		        $aField->Name = "dimensiones";
		        $aField->Title = translate("Visible dimensions");
		        $aField->Type = "Array";
		        $aField->Options = $this->adimensiones;
		        $aField->FullRow = false;
		        $myFields[$aField->Name] = $aField;
			}
		}

/*		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OwnerName";
		$aField->Title = translate("Owner");
		$aField->Type = "String";
		$aField->Size = 15;
		$aField->IsDisplayOnly = true;
		$myFields[$aField->Name] = $aField;		
*/		
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
		{
			return true;
		}
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}

	function AddColumnInAggregations($IndicatorID)
	{
// @EA 2011-02-18
		$aggregation_tables = $this->GetTablesWithAggregations();
		
		foreach ($aggregation_tables as $ag_table)
		{
			$sql = 'alter table '.$ag_table.' add COUNT_'.$IndicatorID.' float NULL';
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
				savelogfile("AddColumnInAggregations : ".$this->Repository->DataADOConnection->ErrorMsg().". Executing: ".$sql, true);
		}
	}
	
	function DropColumnInAggregations($IndicatorID)
	{
// @EA 2011-02-18
		$aggregation_tables = $this->GetTablesWithAggregations();
		
		foreach ($aggregation_tables as $ag_table)
		{
			$sql = 'alter table '.$ag_table.' drop column COUNT_'.$IndicatorID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
				savelogfile("DropColumnInAggregations : ".$this->Repository->DataADOConnection->ErrorMsg().". Executing: ".$sql, true);
		}
	}
	
	function GetTablesWithAggregations()
	{
// @EA 2011-02-18		

		$sql = 
		"SELECT CLA_PERIODO AS periodid ".
		"FROM 	SI_CPTO_LLAVE ".
		"WHERE	CLA_CONCEPTO = ".$this->CubeID.' and NOT CLA_PERIODO IS NULL and CLA_PERIODO >= 0';
		
		$aPeriods = array();

		$aRS = $this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS)
			while (!$aRS->EOF)
			{
				$aPeriods[] = (int) $aRS->fields['periodid'];
				$aRS->MoveNext();
			}
		unset($aRS);

		$sql = 
		"SELECT CONSECUTIVO AS agregationid ".
		"FROM 	SI_AG_NEW ".
		"WHERE	CLA_CONCEPTO = ".$this->CubeID;

		$aTables = array();
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);

		if ($aRS)
		{
			while (!$aRS->EOF)
			{
				foreach ($aPeriods as $PeriodID)
				{
					$aTables[] = 'AN_'.$this->CubeID.'_'.$PeriodID.'_'.$aRS->fields['agregationid'];
				}
				$aRS->MoveNext();
			}
			
			foreach ($aPeriods as $PeriodID)
			{
				$aTables[] = 'AN_'.$this->CubeID.'_'.$PeriodID.'_0';
			}
		}
		
		return $aTables;
	}

	function FormulaUsrImg()
	{
		//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (s�lo para BITAMCollection)
		$htmlString = $this->formula_usr;
		//Ya no es necesario concatenar la imagen al Name, porque eso se pone en el id del Tag
		return $htmlString;
		//@JAPR
// @EA 2011-02-17		
		if ($this->in_aggregation == 1)
//
			$htmlString = "<img src=images/datasource.gif title = '".$htmlString."'>".$htmlString;		
		elseif ($this->nactual != 0)
			$htmlString = "<img src=images/date.gif title = '".$htmlString."'>".$htmlString;
		elseif ($this->filter_sql != '')
			$htmlString = "<img src=images/indfilter.gif title = '".$htmlString."'>".$htmlString;
		elseif ($this->tipo_indicador == 5)
			$htmlString = "<img src=images/genBudgetCalInd.gif title = '".$htmlString."'>".$htmlString;
		else
			$htmlString = "<img src=images/EditFormula.gif title = '".$htmlString."'>".$htmlString;		
			
		return $htmlString;
	}
	
	//@JAPR 2010-06-25: Agregada la imagen para los Elementos de una Row (s�lo para BITAMCollection)
	function FormulaImg()
	{
		$htmlString = '';
		
//		if ($this->es_atributo == 1)
// @EA 2011-02-17		
		if ($this->in_aggregation == 1)
//
			$htmlString = "<img src=images/datasource.gif title = '".htmlentities($this->formula_usr)."'>".$htmlString;		
		elseif ($this->nactual != 0)
			$htmlString = "<img src=images/date.gif title = '".htmlentities($this->formula_usr)."'>".$htmlString;
		elseif ($this->filter_sql != '')
			$htmlString = "<img src=images/indfilter.gif title = '".htmlentities($this->formula_usr)."'>".$htmlString;
		elseif ($this->tipo_indicador == 5)
			$htmlString = "<img src=images/genBudgetCalInd.gif title = '".htmlentities($this->formula_usr)."'>".$htmlString;
		else
			$htmlString = "<img src=images/EditFormula.gif title = '".htmlentities($this->formula_usr)."'>".$htmlString;		
			
		return $htmlString;
	}
	//@JAPR
	
	function generateBeforeFormCode($aUser)
	{
		
		global $gsURLWEBHTML;
		
		$gsURLWEBHTML = GetLocalURLWEBG6();
		
			// Is the user using HTTPS?
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://';
			
			// Complete the URL
			$url .= $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
			
			$path = $url."/ApplyIndFilter.php";
			
	//		$pathFilterPage = "http://stratego1/artus/eis/";
			$pathFilterPage = $gsURLWEBHTML;
			
			$pathFilterPage = str_ireplace('localhost', $_SERVER['HTTP_HOST'], $pathFilterPage);
	//		$pathFilterPage = "http://edgardesktop/artus/genvi/";
	//		$pathFilterPage = "http://demodmf/artus/genvi/";
			
	//		print '<BR>$pathFilterPage = '.$pathFilterPage;
			$theRepositoryName = '';
			
			global $workingMode;
			
			if ($workingMode == wrkmBITAM)
			{
				
				if (isset($_SESSION["repository"]))
					$theRepositoryName = str_ireplace('projects/', '', $_SESSION["repository"]);
				else
					$theRepositoryName = $_SESSION["BITAM_RepositoryName"];
	
				savelogfile('Get_ProyectNames = ['.$theRepositoryName.']');
					
				require_once("../conf_rep/include/generador.inc.php");
				
				$theRepositoryNameG6 = Get_ProyectNames($theRepositoryName,false,true,'../');
				
				savelogfile('Get_ProyectNamesG6 = ['.$theRepositoryNameG6.']');
				
				if ($theRepositoryNameG6 != '')
					$theRepositoryName = $theRepositoryNameG6;
			}
			else
			{
				$theRepositoryName = $_SESSION["BITAM_RepositoryName"];

				savelogfile('BITAM_RepositoryName = ['.$theRepositoryName.']');
			}
			
/*			
			if ($theRepositoryName == '')
				$theRepositoryName = $_SESSION["BITAM_RepositoryName"];
*/				
//			$theRepositoryName = $_SESSION["BITAM_RepositoryName"];
				
			$thePassword = $_SESSION["BITAM_Password"];
			$thePassword = BITAMEncryptPassword($thePassword);
			
			// Obtenemos el id del usuario logeado, primero lo tomamos de la cookie
			$theUser = "SUPERVISOR";

			if (array_key_exists("BITAM_UserName", $_COOKIE))
			{
				$theUser = $_COOKIE["BITAM_UserName"];		
				// Si no existe en la sesion, lo tomamos de la cookie			
				//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesi�n de eForms
				if (is_null($theUser))
					$theUser = @$_SESSION["PABITAM_UserName"];
			}
?>	

	<form action="<?=$pathFilterPage?>loadstage.php" name="frmFilter" method="post">
			<input name="txtRepositorio" type="hidden" value="<?=$theRepositoryName?>">
			<input name="txtUser" type="hidden" value="<?=$theUser?>">
			<input name="txtPassword" type="hidden" value="<?=htmlspecialchars($thePassword,ENT_QUOTES)?>">
			<input name="txtNumEsc" type="hidden" value="-3">
			<input name="txtParametros" type="hidden">
	</form>	
	
	<form id="frmData" name="frmData" >
		<input type="hidden" id="txtLevel" name="txtLevel" value="">
		<input type="hidden" id="txtFilter" name="txtFilter" value="">
	</form>
	

<script language="javascript">

 		//Evento del click para invocar a la forma del Filtro
		function selectFilter()
		{
			//cargar el filtro anterior, asignar los valores a
			//frmData.txtLevel.value y frmData.txtFilter.value q se mandan como par�metros a la p�gina del 
			// filtro para q considere el filtro indicado anteriormente
			loadSavedFilter();
				
			//var anIndicatorID=BITAMBudget_SaveForm.IndicatorID.options(BITAMBudget_SaveForm.IndicatorID.selectedIndex).value;
			

			var nCubeID= document.getElementsByName('CubeIDString')[0].value;
//			var nCubeID= 1;
			var sLevel= document.getElementsByName('niveles')[0].value;
			
			sLevel = sLevel.replace('|', ',')
			
			var sFilter= document.getElementsByName('filter_sql')[0].value;
			
//			alert(nIndicatorID);
//			alert(sLevel);
//			alert(sFilter);
			
//			var nIndicatorID= 45;
//			var sLevel= '|1';			
//			var sFilter= '[AREAS].[areaorig_01].[NORTH]';									
			
//			alert(BITAMUserGroupIndicatorGroup_SaveForm.IndicatorID.value);

//			alert('<?=$path?>');
//			alert('<?=$url?>');

			frmFilter.txtParametros.value = 'TPParamsTPFILTER,<?=$path?>|AWCUBE:' + nCubeID + '|' + 4 + '|' + sLevel + '|' + sFilter;
			
			x = new Date();
			y  = "mywindow";
			y = y + x.getHours() + x.getMinutes() + x.getSeconds();		
	
			window.open("<?=$pathFilterPage?>Loading.htm", y, "menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=no",1);		
			frmFilter.target = y;
			frmFilter.submit();
		}
		
		
		function loadSavedFilter()
		{
/*			
			var filterString = BITAMUserGroupIndicatorGroup_SaveForm.SecurityFilter.value;
			
			var anArray= new Array;
			anArray = filterString.split('|');
			if (trim(filterString) == "") 
			{
				//BITAMBudget_SaveForm.txtIndFilter.value = "No";
				frmData.txtLevel.value = "";
				frmData.txtFilter.value = "";
			}
			else
			{  
				if (trim(anArray[3])=="AND")
				{ 
					//BITAMBudget_SaveForm.txtIndFilter.value = "No"
					frmData.txtLevel.value = ""
					frmData.txtFilter.value = ""
				}
				else
				{
					//BITAMBudget_SaveForm.txtIndFilter.value = "Yes";
					frmData.txtLevel.value = anArray[2]; //Esta es la lista de consecutivos separada por ","
					frmData.txtFilter.value = anArray[3]; //Esta es la cadena del filtro tal como la regresa WebHTML
				}
			}
*/			
		}
	
		function changeFilter(argFilterString)
		{

			var anArray=new Array;
			anArray = argFilterString.split('|');
			
			var sLevel= document.getElementsByName('niveles')[0];
			var sFilter= document.getElementsByName('filter_sql')[0];
			
//			alert(argFilterString);
			
			if (Trim(argFilterString) == "") 
			{
				sLevel.value = "";
				sFilter.value = "";
			}
			else
			{  
				if (Trim(anArray[3])=="AND")
				{ 
					sLevel.value = "";
					sFilter.value= "";
				}
				else
				{
					anArray[2] = anArray[2].replace( '0,', '|' );
					anArray[2] = anArray[2].replace( ',', '|' );
					if (Trim(anArray[3].substring(0, 4)) == 'AND')
					{
						anArray[3] = anArray[3].substring(4);
					}
					sLevel.value = anArray[2]; 
					sFilter.value = anArray[3]; 
				}
			}
		}
		
		function Trim(TRIM_VALUE){
			if(TRIM_VALUE.length < 1){
				return"";
			}
			TRIM_VALUE = RTrim(TRIM_VALUE);
			TRIM_VALUE = LTrim(TRIM_VALUE);
			if(TRIM_VALUE==""){
				return "";
			}
			else{
				return TRIM_VALUE;
			}
		} //End Function

		function RTrim(VALUE){
			var w_space = String.fromCharCode(32);
			var v_length = VALUE.length;
			var strTemp = "";
			if(v_length < 0){
				return"";
			}
			var iTemp = v_length -1;
			
			while(iTemp > -1){
				if(VALUE.charAt(iTemp) == w_space){
				}
				else{
					strTemp = VALUE.substring(0,iTemp +1);
					break;
				}
				iTemp = iTemp-1;
				} //End While
			return strTemp;
		} //End Function
		
		function LTrim(VALUE){
			var w_space = String.fromCharCode(32);
			if(v_length < 1){
				return"";
			}
			var v_length = VALUE.length;
			var strTemp = "";
		
			var iTemp = 0;
		
			while(iTemp < v_length){
				if(VALUE.charAt(iTemp) == w_space){
				}
				else{
					strTemp = VALUE.substring(iTemp,v_length);
					break;
				}
					iTemp = iTemp + 1;
			} //End While
			return strTemp;
		} //End Function
</script>		
<?		
		
	}

}
?>