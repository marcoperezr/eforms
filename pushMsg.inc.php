<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("paginator.inc.php");
require_once("appUSer.inc.php");
require_once("usergroup.inc.php");
require_once("utils.inc.php");
require_once('settingsvariable.inc.php');
require_once("ApnsPHP/Autoload.php");
$svStartDate=null;
$svEndDate=null;
$svUserID=null;
$svIpp=null;

global $arrUsers; 	
$arrUsers = array();	

global $arrUsersGroups;
$arrUsersGroups = array();

class BITAMPushMsg extends BITAMObject 
{
	//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	/* El formato de los elementos del array $arrAPNSPEMConnections es:
	[PEMFileName como índice] => array(
		'apns' => Stream con la conexión viva hacia el servicio de push de Apple
		'start' => date('Y-m-d H:i:s') Hora de inicio de la creación de la conexión, si se excede del tiempo de vigencia y se invoca al método ValidateAPNSTokenExpiration, se forzará a reconectarse
	)
	*/
	static $arrAPNSPEMConnections = array();		//Array con información de las conexiones APNS hacia archivos .pem de Apple para manterlas vivas durante la ejecución del Agente de notificaciones Push
	static $intAPNSTimeOut = 60;					//Tiempo en minutos máximo que se deberá mantener viva la conexión APNS hacia el servicio de push de Apple
	static $APNSError = '';							//¿Código? de error de la creación de la conexión APNS al servicio de push de Apple
	static $APNSErrorString = '';					//Texto de error de la creación de la conexión APNS al servicio de push de Apple
	//@JAPR
	static $arrAPNSiOSServer = null;
	
	public $MessageID;
	public $DestinationType;
	public $DestinationID;
	public $DestinationName;
	public $CreationUserID;
	public $CreationDateID;
	public $Title;
	public $PushMessage;
	public $State;
	public $StateOld;
	public $DeviceIDs;

	//GCRUZ 2016-05-04. Guardar la colección de usuarios y grupos
	public $UserCollection;
	public $GroupCollection;
	
	//ears 2016-10-25 Detalle del mensaje
	public $MessageDetail;
	//ears 2016-10-27 manejo del estatus del mensaje
	public $NomState;	
	//ears 2016-11-10 para registrar notificaciones
	public $arrDestinationsD;
	//ears 2016-12-23 detalle de grupos de usuario o usuarios
	public $MessageUserGroupID;
		
		
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->MessageID = -1;
		$this->DestinationType = -1;
		$this->DestinationID = -1;
		$this->DestinationName = "";
		$this->CreationUserID = -1;
		$this->CreationDateID = "";
		$this->Title = '';
		$this->PushMessage = "";
		$this->State = 0;
		$this->StateOld = 0;
		$this->DeviceIDs = array();
		$this->MsgConfirmRemove = "Do you wish to delete this entry?";

		$this->UserCollection = null;
		$this->GroupCollection = null;
		$this->MessageDetail = array(); //ears 2016-10-25 detalle del mensaje
		$this->NomState = '';
		$this->MessageUserGroupID = array(); //ears 2016-12-23 detalle de grupos de usuario o usuarios
		$this->DestinationNameUserGroup = array(); //ears 2016-12-23 a nivel de detalle
		
	}

	static function NewInstance($aRepository)
	{
		return new BITAMPushMsg($aRepository);
	}

	static function NewInstanceWithID($aRepository, $aMessageID, $bGetValues = true)
	{
		$anInstance = null;
		if ((int)$aMessageID < 0)
		{
			return $anInstance;
		}
		$anInstance = BITAMPushMsg::NewInstance($aRepository);
		
		$sql = "SELECT A.MessageID, A.CreationDateID, A.CreationUserID, A.DestinationType, A.DestinationID, A.Title, A.PushMessage, A.State ";
		$where = '';
		$sql .= " FROM SI_SV_PushMsg A";
		//Ligamos las Tablas para lo de Email
		$where.=" A.MessageID = {$aMessageID}";
		if($where!="")
		{
			$sql .= " WHERE ".$where;
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMPushMsg::NewInstanceFromRS($aRepository, $aRS, $bGetValues);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS, $bGetValues = false, $aUserCollection = null, $aGroupCollection = null)
	{
		$anInstance = BITAMPushMsg::NewInstance($aRepository);
		if (!is_null($aUserCollection))
			$anInstance->UserCollection = $aUserCollection;
		if (!is_null($aGroupCollection))
			$anInstance->GroupCollection = $aGroupCollection;
		$anInstance->MessageID = (int) @$aRS->fields["messageid"];
		$anInstance->CreationDateID = (string) @$aRS->fields["creationdateid"];
		$anInstance->CreationUserID = (int) @$aRS->fields["creationuserid"];
		$anInstance->DestinationType = (int) @$aRS->fields["destinationtype"];
		$anInstance->DestinationID = (int) @$aRS->fields["destinationid"];
		$anInstance->Title = (string) @$aRS->fields["title"];
		$anInstance->PushMessage = (string) @$aRS->fields["pushmessage"];
		$anInstance->State = (int) @$aRS->fields["state"];
		$anInstance->StateOld = $anInstance->State;
		//ears 2016-10-27 manejo del status del mensaje
		$aNomState = $anInstance->State;
		
		switch ($aNomState) {
			case pushMsgPending:
				$anInstance->NomState = translate('Pending');			
				break;
			case pushMsgSent:
				$anInstance->NomState = translate('Sent');						
				break;
			case pushMsgError:
				$anInstance->NomState = translate('Error');						
				break;				
			default:	
				$anInstance->NomState = translate('Unrecognized type');
				break;	
		}							
        
		if ($bGetValues) {
			$anInstance->getDevices();
			//ears 2016-10-25 detalle del mensaje
			$anInstance->getDetail($aRepository);			
		}

		//ears 2016-12-23 detalle de grupos de usuario o usuarios
		$anInstance->getMessageUserGroupID($aRepository);													
		$anInstance->getDestinationName($aRepository);
				
		return $anInstance;
	}
	
	//ears 2016-12-23 detalle de grupos de usuario o usuarios
	function getMessageUserGroupID($aRepository) {
		//ears 2016-12-26 detalle de grupos de usuario o usuarios
		if (getMDVersion() >= esvPushMsgUserGroups) {
		   $this->MessageUserGroupID = array();
		   $sql = "SELECT A.MessageID,A. UserGroupID, B.DestinationType FROM SI_SV_PushMsgUserGroups A LEFT JOIN SI_SV_PushMsg B ON A.MessageID = B.MessageID 
				  WHERE A.MessageID = {$this->MessageID}";			   
		   $aRS = $this->Repository->DataADOConnection->Execute($sql);

			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_PushMsgUserGroups, SI_SV_PushMsg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
		   $i = 0;	
		   while (!$aRS->EOF) {
			$this->MessageUserGroupID[$i]['MessageID'] = (int) @$aRS->fields["MessageID"];   
			$this->MessageUserGroupID[$i]['UserGroupID'] = (int) @$aRS->fields["UserGroupID"];   		
			$this->MessageUserGroupID[$i]['DestinationType'] = (int) @$aRS->fields["DestinationType"];		   		
			$i++;
			$aRS->MoveNext();
		   }  		
		}   
	}
	
	//ears 2016-10-25 detalle del mensaje	
	function getDetail($aRepository) {	
		//ears 2016-12-26 detalle de usuarios
		if (getMDVersion() >= esvRealPushMsg) {
		   $this->MessageDetail = array();
		   $sql = "SELECT MessageID, UserID, State, SI_SV_PushMsgDet.ErrorDesc, {$aRepository->DataADOConnection->databaseName}.SI_USUARIO.NOM_LARGO 
			FROM SI_SV_PushMsgDet Left JOIN {$aRepository->DataADOConnection->databaseName}.SI_USUARIO on
			 SI_SV_PushMsgDet.UserID = {$aRepository->DataADOConnection->databaseName}.SI_USUARIO.CLA_USUARIO
			WHERE MessageID = {$this->MessageID}";			   
		   $aRS = $this->Repository->DataADOConnection->Execute($sql);

			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_SV_PushMsgDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
		   $i = 0;	
		   while (!$aRS->EOF) {
			$this->MessageDetail[$i]['UserID'] = (int) @$aRS->fields["UserID"];
			$this->MessageDetail[$i]['NOM_LARGO'] = (string) @$aRS->fields["NOM_LARGO"];
			$intPendingPushMsg = (int) @$aRS->fields["State"];
			$this->MessageDetail[$i]['ErrorDesc'] = (string) @$aRS->fields["ErrorDesc"];
			
			switch ($intPendingPushMsg) {		
				case pushMsgPending:
					$this->MessageDetail[$i]['State'] = translate('Pending');
					break;
				case pushMsgSent:
					$this->MessageDetail[$i]['State'] = translate('Sent');	
					break;
				case pushMsgError:
					$this->MessageDetail[$i]['State'] = translate('Error');		
					break;
				case pushMsgReprocess:
					$this->MessageDetail[$i]['State'] = translate('Reprocess');		
					break;				
				case pushMsgRead:
					$this->MessageDetail[$i]['State'] = translate('Read');
					break;
				case pushMsgDeleted:				
					$this->MessageDetail[$i]['State'] = translate('Deleted');
					break;
			}
			
			$i++;
			$aRS->MoveNext();
		   }  		
		}   
	}
		
	//Obtiene la colección de dispositivos a los que se envió este mensaje
	function getDevices() {
		$this->DeviceIDs = array();
		
		$sql = "SELECT DeviceID FROM SI_SV_PushMessageDevices WHERE MessageID = {$this->MessageID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_PushMessageDevices ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF) {
			$intDeviceID = (int) @$aRS->fields["deviceid"];
			if ($intDeviceID > 0) {
				$this->DeviceIDs[] = $intDeviceID;
			}
			$aRS->MoveNext();
		}
	}

	function getDestinationName($aRepository)
	{
		//ears 2016-12-26 detalle de grupos de usuario o usuarios
		if (getMDVersion() >= esvPushMsgUserGroups) {
			$this->DestinationNameUserGroup = array();
			switch($this->DestinationType)
			{
				case pushMsgUser:
					if (is_null($this->UserCollection))
					{
						$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
						$this->UserCollection = $aUserCollection->Collection;
					}
					$i = 0;
					foreach ($this->UserCollection  as $aUser)
					{	
						if ($this->DestinationID == 0) {  //ears 2016-12-26 a partir de que ya no se registró DestinationID 
							foreach ($this->MessageUserGroupID as $objPushMsg) {							
								//@JAPR 2017-09-29: Corregido un bug, se estaban cargando los usuarios por medio del UserID, siendo que debía ser el CLA_USUARIO (#4WSLS1)
								if ($aUser->CLA_USUARIO == @$objPushMsg['UserGroupID']) {
								//if ($aUser->UserID == @$objPushMsg['UserGroupID']) {
									$this->DestinationNameUserGroup[$i]['UserGroupID'] = @$objPushMsg['UserGroupID'];
									$this->DestinationNameUserGroup[$i]['DestinationName'] = $aUser->UserName;
									$this->DestinationNameUserGroup[$i]['DestinationType'] = pushMsgUser;
									$i++;
								}									
							}	
						}
						else{
							if ($aUser->UserID == $this->DestinationID) {
								$this->DestinationNameUserGroup[$i]['UserGroupID'] = $this->DestinationID;
								$this->DestinationNameUserGroup[$i]['DestinationName'] = $aUser->UserName;
								$this->DestinationNameUserGroup[$i]['DestinationType'] = pushMsgUser;
								$i++;
							}																
						}
					}
					break;
					
				case pushMsgGroup:
					if (is_null($this->GroupCollection))
					{
						$aGroupCollection = new BITAMUserGroupCollection($aRepository, null);
						$this->GroupCollection = $aGroupCollection->Collection;
					}
					
					$i = 0;
					foreach ($this->GroupCollection as $aGroup)
					{ 	
						if ($this->DestinationID == 0){ //ears 2016-12-26 a partir de que ya no se registró DestinationID 
							foreach ($this->MessageUserGroupID as $objPushMsg) {
								if ($aGroup->UserGroupID == @$objPushMsg['UserGroupID']){
									$this->DestinationNameUserGroup[$i]['UserGroupID'] = @$objPushMsg['UserGroupID'];
									$this->DestinationNameUserGroup[$i]['DestinationName'] = $aGroup->UserGroupName;
									$this->DestinationNameUserGroup[$i]['DestinationType'] = pushMsgGroup;
									$i++;
								}								
							}	
						}
						else{
							if ($aGroup->UserGroupID == $this->DestinationID){
								$this->DestinationNameUserGroup[$i]['UserGroupID'] = $this->DestinationID;
								$this->DestinationNameUserGroup[$i]['DestinationName'] = $aGroup->UserGroupName;
								$this->DestinationNameUserGroup[$i]['DestinationType'] = pushMsgGroup;
								$i++;
							}															
						}
					}
					break;
				default:
					break;
			}
		}
		else {
			switch($this->DestinationType)
			{
				case pushMsgUser:
					if (is_null($this->UserCollection))
					{
						$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
						$this->UserCollection = $aUserCollection->Collection;
					}
					foreach ($this->UserCollection as $aUser)
					{
						if ($aUser->UserID == $this->DestinationID)
							$this->DestinationName = $aUser->UserName;
					}
					break;
				case pushMsgGroup:
					if (is_null($this->GroupCollection))
					{
						$aGroupCollection = new BITAMUserGroupCollection($aRepository, null);
						$this->GroupCollection = $aGroupCollection->Collection;
					}
					foreach ($this->GroupCollection as $aGroup)
					{ 
						if ($aGroup->UserGroupID == $this->DestinationID)
							$this->DestinationName = $aGroup->UserGroupName;
					}
					break;
				default:
					break;
			}			
		}	
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $svStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$svStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $svEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$svEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $svUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$svUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $svIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$svIpp = $aHTTPRequest->GET["ipp"];
		}

		if (array_key_exists("MessageID", $aHTTPRequest->POST))
		{
			//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
			$arrDates = array();
			$aMessageID = $aHTTPRequest->POST["MessageID"];
			if (is_array($aMessageID))
			{
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$svStartDate = $aHTTPRequest->POST["startDate"];
				}
				
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$svEndDate = $aHTTPRequest->POST["endDate"];
				}
				
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$svUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$svIpp = $aHTTPRequest->POST["ipp"];
				}
				
				$aCollection = BITAMPushMsgCollection::NewInstanceForRemove($aRepository, $aMessageID, $svStartDate, $svEndDate, $svUserID, $svIpp);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$strDateID = trim((string) @$anInstanceToRemove->DateID);
					if ($strDateID != '') {
						$strDateID = (string) @substr($strDateID, 0, 10);
						$arrDates[$strDateID] = $strDateID;
					}
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMPushMsg::NewInstanceWithID($aRepository, (int) $aMessageID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMPushMsg::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMPushMsg::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("MessageID", $aHTTPRequest->GET))
		{
			$aMessageID = (int) @$aHTTPRequest->GET["MessageID"];
			$anInstance = BITAMPushMsg::NewInstanceWithID($aRepository, $aMessageID);
			if (is_null($anInstance))
			{
				$anInstance = BITAMPushMsg::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMPushMsg::NewInstance($aRepository);
		} 

		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = (int)$anArray["UserID"];
		}
		
		if (array_key_exists("UserIDs", $anArray))
		{
			$this->UserIDs = $anArray["UserIDs"];
		}
		
		if (array_key_exists("DeviceIDs", $anArray))
		{
			$this->DeviceIDs = $anArray["DeviceIDs"];
		}
		
		if (array_key_exists("CreationUserID", $anArray))
		{
			$this->CreationUserID = (int)$anArray["CreationUserID"];
		}
		
		if (array_key_exists("CreationDateID", $anArray))
		{
			$this->CreationDateID = (string)$anArray["CreationDateID"];
		}
		
		if (array_key_exists("Title", $anArray))
		{
			$this->Title = (string)$anArray["Title"];
		}
		
		if (array_key_exists("PushMessage", $anArray))
		{
			$this->PushMessage = (string)$anArray["PushMessage"];
		}
		
		if (array_key_exists("State", $anArray))
		{
			$this->State = (int)$anArray["State"];
		}
	}
	
	function save()
	{
		$blnSendPush = false;
		$blnNewObject = false;
		$currDate = date("Y-m-d H:i:s");
		
	 	if ($this->isNewObject())
		{
			$blnNewObject = true;
			$blnSendPush = true;
			$this->CreationDateID = $currDate;
			$sql = "INSERT INTO SI_SV_PushMsg (".
				" CreationDateID".
				", CreationUserID".
				", DestinationType".
				", DestinationID".
				", State".
				", Title".
				", PushMessage".
				") VALUES (".
				$this->Repository->DataADOConnection->DBTimeStamp($this->CreationDateID).
				",".$this->CreationUserID.
				",".$this->DestinationType.
				",".$this->DestinationID.
				",".$this->State.
				",".$this->Repository->DataADOConnection->Quote($this->Title).
				",".$this->Repository->DataADOConnection->Quote($this->PushMessage).
				")";

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			$this->MessageID = @$this->Repository->DataADOConnection->Insert_ID();
        } else {
        	$blnSendPush = ($this->State && !$this->StateOld);
            $sql = "UPDATE SI_SV_PushMsg SET ".
					"Title = ".$this->Repository->DataADOConnection->Quote($this->Title).
					", PushMessage = ".$this->Repository->DataADOConnection->Quote($this->PushMessage).
					", State = ".$this->State.
				" WHERE MessageID = ".$this->MessageID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die( translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
		
		//@ears 2016-11-10 desde aquí se insertará el detalle
		
        //Envía la notificación si se activó y se está grabando con un estado de no enviado
		if (getMDVersion() >= esvPushMsgUserGroups) {
			if ($this->DestinationType == pushMsgUser) {
				//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
				//No es requerido grabar el RegID a nivel de mensaje de push, ya que éste puede ir dirigido a múltiples usuarios y cada uno tendría un RegID diferente, así que el RegID
				//se asignará hasta que se está realizando el envío a cada dispositivo específico para cada usuario
				/*
				@JAPR 2016-10-14: Modificado el esquema de Push para envíar mensajes al último dispositivo utilizado por el usuario solamente (#L9T2IU)
				Ahora es necesario insertar el RegID del usuario para el último dispositivo que tiene asignado, ya que sólo así se van a recuperar los mensajes Push pendientes que es 
				seguro que se le enviaron a su dispositivo actual
				$aUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $this->DestinationID);
				$aUser->getMobileDataV6();
				Este array sólo debe traer un valor, sin embargo si por alguna razón vinieran varios, sólo tomará el primero que también debería ser el mas reciente
				foreach ($aUser->MobileData as $strRegID => $arrMobileData) {
					break;
				}
				*/
				
				//@ears 2016-11-10 desde aquí inserción de detalle, control de usuarios a los cuales se envía notificación
				foreach ($this->arrDestinationsD as $aDestination) {
					$aDestinationID = intval($aDestination);															
					$aUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $aDestinationID);
										
					if (!is_null($aUser)) {
						//@ears 2016-11-10 detalle usuarios/grupos
						$sql = "INSERT INTO SI_SV_PushMsgUserGroups (MessageID, UserGroupID) 
							VALUES ({$this->MessageID}, {$aDestinationID})";
						if ($this->Repository->DataADOConnection->Execute($sql) === false) {
							die( translate("Error accessing")." SI_SV_PushMsgUserGroups ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						
						//@ears 2016-11-10 detalle usuarios detallados
						$sql = "INSERT INTO SI_SV_PushMsgDet (MessageID, UserID, State) 
							VALUES ({$this->MessageID}, {$aUser->CLA_USUARIO}, ".pushMsgPending.")";
						if ($this->Repository->DataADOConnection->Execute($sql) === false) {
							die( translate("Error accessing")." SI_SV_PushMsgDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}	
				//$this->sendMessage();
				//@JAPR
			}
			elseif ($this->DestinationType == pushMsgGroup) {
				//@ears 2016-11-10 desde aquí inserción de detalle, control de grupos/usuarios a los cuales se envía notificación
				foreach ($this->arrDestinationsD as $aDestination) {
					$aDestinationID = intval($aDestination);
					$aUserGroup = BITAMUserGroup::NewUserGroupWithID($this->Repository, $aDestinationID);

					if (!is_null($aUserGroup)) {
						//@ears 2016-11-10 detalle usuarios/grupos
						$sql = "INSERT INTO SI_SV_PushMsgUserGroups (MessageID, UserGroupID)  
							VALUES ({$this->MessageID}, {$aDestinationID})";
						if ($this->Repository->DataADOConnection->Execute($sql) === false) {
							die( translate("Error accessing")." SI_SV_PushMsgUserGroups ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
											
						foreach ($aUserGroup->UsersAssct as $aCLA_USUARIO) {
							//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
							//No es requerido grabar el RegID a nivel de mensaje de push, ya que éste puede ir dirigido a múltiples usuarios y cada uno tendría un RegID diferente, así que el RegID
							//se asignará hasta que se está realizando el envío a cada dispositivo específico para cada usuario
							/*
							//@JAPR 2016-10-14: Modificado el esquema de Push para envíar mensajes al último dispositivo utilizado por el usuario solamente (#L9T2IU)
							//Ahora es necesario insertar el RegID del usuario para el último dispositivo que tiene asignado, ya que sólo así se van a recuperar los mensajes Push pendientes que es 
							//seguro que se le enviaron a su dispositivo actual
							$aUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $aCLA_USUARIO);
							$aUser->getMobileDataV6();
							//Este array sólo debe traer un valor, sin embargo si por alguna razón vinieran varios, sólo tomará el primero que también debería ser el mas reciente
							foreach ($aUser->MobileData as $strRegID => $arrMobileData) {
 								break;
							}
							*/														
							//@ears 2016-11-10 detalle usuarios detallados, evitar repetir usuarios en caso de que el mismo usuario aparezca en dos gpos distintos
							$blnUser = false;
							$pushInstance = BITAMPushMsg::NewInstanceWithID($this->Repository, $this->MessageID);
							foreach ($pushInstance->MessageDetail as $objPushMsg) {																		
								$blnUser = ($objPushMsg['UserID'] == $aCLA_USUARIO)? true:false;
								if ($blnUser) {
									break;
								}
							}
							
							if (!$blnUser) {
								$sql = "INSERT INTO SI_SV_PushMsgDet (MessageID, UserID, State) 
									VALUES ({$this->MessageID}, {$aCLA_USUARIO}, ".pushMsgPending.")";
								if ($this->Repository->DataADOConnection->Execute($sql) === false) {
									die( translate("Error accessing")." SI_SV_PushMsgDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
							}	
							//$this->sendMessage($aUser);
							//@JAPR
						}
					}
				}	 
			}
		}
		
		//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
		//Si se trata de una nueva notificación o se está reprogramando el envío, se debe agregar una tarea de notificación a la tabla que el agente consulta constamente
		if ($blnNewObject) {
			require_once('eSurveyServiceMod.inc.php');
			$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
			$aBITAMConnection = connectToKPIRepository();
			
			if (is_object($aBITAMConnection)) {
				$sql = "INSERT INTO SI_SV_PushTasks (CreationDateID, Repository, TaskStatus, FormsPath) 
					VALUES (".
						$aBITAMConnection->ADOConnection->DBTimeStamp($currDate).", ".
						$aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"]).", ".
						pushMsgPending.", ".
						$aBITAMConnection->ADOConnection->Quote($streFormsService).")";
				@$aBITAMConnection->ADOConnection->Execute($sql);
			}
		}
		//@JAPR
	}
	
	//Envía nuevamente el mensaje a todos los dispositivos del destinatario
	function reSend() {
		$this->State = pushMsgReprocess;
		$this->StateOld = pushMsgError;
		$this->save();
	}
	
	//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	/* Actualiza la tabla de detalle de error del envío de esta notificación. Adicionalmente se puede especificar un código de error */
	function updatePushMsgDetailStatus($aUserID, $aRegID, $sErrDesc = '', $aState = pushMsgError) {
		$aUserID = (int) $aUserID;
		if ($aUserID <= 0) {
			return;
		}
		
		$sql = "UPDATE SI_SV_PushMsgDet SET State = {$aState} 
				, ErrorDesc = ".$this->Repository->DataADOConnection->Quote($sErrDesc)." 
				, RegID = ".$this->Repository->DataADOConnection->Quote($aRegID)." 
			WHERE MessageID = {$this->MessageID} AND UserID = {$aUserID}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$this->Repository->DataADOConnection->Execute($sql);
	}
	
	/* Basado en la definición del mensaje y el Status de cada usuario involucrado, realiza el envío de la notificación a cada usuario que aún esté pendiente utilizando el RegID del
	último dispositivo utilizado por cada uno de ellos
	*/ 
	function sendNotifications() {
		$strErrorDesc = "";
		$strErrorAnd = "";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("starting sendNotifications for PushMsg {$this->MessageID}", 1, 1, "color:purple;");
		}
		
		//Primero debe determinar la cuenta maestra de este repositorio, para con ella determinar si existe o no una aplicación personalizada que la deba utilizar, ya que por cada
		//aplicación personalizada que tenga asociado este repositorio se deberá realizar la notificación push a los usuarios correspondientes
		$strMasterAccount = '';
		$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
		$arreFormsApps = array();
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		if (!is_null($aBITAMConnection)) {
			$sql = "SELECT b.EMail, a.CustomerType 
	    		FROM saas_databases a, saas_users b 
	    		WHERE a.UserID = b.UserID AND a.Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepositoryName);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$strMasterAccount = (string) @$aRS->fields["email"];
			}
			
			//Determina si existen aplicaciones personalizadas que apuntan a este repositorio, si es así, se enviará la notificación sólo a dichas aplicaciones, en caso contrario se
			//enviará a la primer aplicación que el usuario tenga disponible
			if ($strMasterAccount) {
				$sql = "SELECT A.appName 
					FROM saas_eforms_apps A 
					WHERE A.masterAccount = ".$aBITAMConnection->ADOConnection->Quote($strMasterAccount);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sql}");
				}
				$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
				if ($aRS && !$aRS->EOF) {
					while (!$aRS->EOF) {
						$strAppName = (string) @$aRS->fields["appname"];
						if (trim($strAppName) != '') {
							$arreFormsApps[] = $strAppName;
						}
						$aRS->MoveNext();
					}
				}
			}
		}
		
		//Si no se hubiera logrado identificar alguno de los componentes necesarios para obtener las Apps personalizadas, se asumirá que no existen tales Apps y se intentará el envío
		//a la primer App disponible para el usuario, por lo que simplemente agrega un elemento vacío para permitir que inicie el ciclo push con una única iteración
		if (!count($arreFormsApps)) {
			$arreFormsApps[] = '';
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n arreFormsApps:");
			PrintMultiArray($arreFormsApps);
		}
		
		//@ears 2016-12-13 adecuación en el envío derivados de los cambios en donde se tiene un detalle de usuarios
		//Recorre todas las Apps personalizadas y envía la notificación hacia cada una para los usuarios correspondientes según el último RegID utilizado por cada uno
		if (getMDVersion() >= esvRealPushMsg) {
			foreach ($arreFormsApps as $strAppName) {
				//Envía la notificación al usuario indicado						
				//@ears 2016-12-13 	
				//$strErrorDesc = '';	
				$anInstance = BITAMPushMsg::NewInstanceWithID($this->Repository, (int) $this->MessageID);
				foreach ($anInstance->MessageDetail as $objPushMsg) {																		
					/*$blnUser = ($objPushMsg['UserID'] == $aCLA_USUARIO)? true:false;*/
					$strError = '';
					$anUserID = (int) @$objPushMsg['UserID'];
					//@JAPR 2017-09-29: Corregido un bug, se estaban cargando los usuarios por medio del UserID, siendo que debía ser el CLA_USUARIO (#4WSLS1)
					if (getMDVersion() >= esvRealPushMsg) {
						$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $anUserID);
					}
					else {
						$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $anUserID);
					}
					//@JAPR
					if (!is_null($theAppUser)) {
						$strError = $this->sendMessage($theAppUser, $strAppName);
						if ($strError) {
							$strErrorDesc .= $strErrorAnd.$strError;
							$strErrorAnd = "<br>\r\n";
						}
						
						$intPushMsgStatusD = ($strError != '')?pushMsgError:pushMsgSent;
						$sql = "UPDATE SI_SV_PushMsgDet SET State = {$intPushMsgStatusD} 
							WHERE MessageID = {$this->MessageID} AND UserID = {$anUserID}";
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\n sql: {$sql}");
						}
						$this->Repository->DataADOConnection->Execute($sql);															
					}													
												
				}	
			}	
				/*
				switch($this->DestinationType)
				{
					case pushMsgUser:						
						//$this->sendMessage($aUser);
						//@JAPR												
						foreach ($this->arrDestinationsD as $aDestination) {
							$aDestinationID = intval($aDestination);															
							$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $aDestinationID);
												
							if (!is_null($theAppUser)) {
								$strError = $this->sendMessage($theAppUser, $strAppName);
								if ($strError) {
									$strErrorDesc .= $strErrorAnd.$strError;
									$strErrorAnd = "<br>\r\n";
								}								
							}
														
							$intPushMsgStatusD = ($strErrorDesc != '')? pushMsgError : pushMsgSent;
							$sql = "UPDATE SI_SV_PushMsgDet SET State = {$intPushMsgStatusD} 
								WHERE MessageID = {$this->MessageID} AND UserID = {$aDestinationID}";
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo("<br>\r\n sql: {$sql}");
							}
							$this->Repository->DataADOConnection->Execute($sql);							
						}
						break;
					
					case pushMsgGroup:
						//Envía la notificación a todos los usuarios del grupo
						//@ears 2016-12-13																							
						foreach ($this->arrDestinationsD as $aDestination) {
							$aDestinationID = intval($aDestination);
							$aUserGroup = BITAMUserGroup::NewUserGroupWithID($this->Repository, $aDestinationID);

							if (!is_null($aUserGroup)) {													
								foreach ($aUserGroup->UsersAssct as $aCLA_USUARIO) {
									//@JAPR 2016-10-20: Corregido un bug, por cada usuario se estaba procesando el mensaje push sólo para el usuario con ID del DestinationID, así que no mandaba
									//a ninguno prácticamente, sólo al que coincidiera con el ID del grupo de usuario (#L9T2IU)
									$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $aCLA_USUARIO);
									//@JAPR
									if (!is_null($theAppUser)) {
										$strError = $this->sendMessage($theAppUser, $strAppName);
										if ($strError) {
											$strErrorDesc .= $strErrorAnd.$strError;
											$strErrorAnd = "<br>\r\n";
										}
									}
									//$this->sendMessage($aUser);
									//@JAPR
																		
									$intPushMsgStatusD = ($strErrorDesc != '')? pushMsgError : pushMsgSent;
									$sql = "UPDATE SI_SV_PushMsgDet SET State = {$intPushMsgStatusD} 
										WHERE MessageID = {$this->MessageID} AND UserID = {$aCLA_USUARIO}";
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										echo("<br>\r\n sql: {$sql}");
									}
									$this->Repository->DataADOConnection->Execute($sql);							
													
								}
							}		
						}	
						break;
					default:
						break;
				}
				*/			
			
		}	
		else	
		{			
			foreach ($arreFormsApps as $strAppName) {
				switch($this->DestinationType)
				{
					case pushMsgUser:
						//Envía la notificación al usuario indicado
						$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $this->DestinationID);
						if (!is_null($theAppUser)) {
							$strError = $this->sendMessage($theAppUser, $strAppName);
							if ($strError) {
								$strErrorDesc .= $strErrorAnd.$strError;
								$strErrorAnd = "<br>\r\n";
							}
						}
						break;
					
					case pushMsgGroup:
						//Envía la notificación a todos los usuarios del grupo
						$aUserGroup = BITAMUserGroup::NewUserGroupWithID($this->Repository, $this->DestinationID);
						if (!is_null($aUserGroup)) {
							foreach ($aUserGroup->UsersAssct as $aCLA_USUARIO) {
								//@JAPR 2016-10-20: Corregido un bug, por cada usuario se estaba procesando el mensaje push sólo para el usuario con ID del DestinationID, así que no mandaba
								//a ninguno prácticamente, sólo al que coincidiera con el ID del grupo de usuario (#L9T2IU)
								$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $aCLA_USUARIO);
								//@JAPR
								if (!is_null($theAppUser)) {
									$strError = $this->sendMessage($theAppUser, $strAppName);
									if ($strError) {
										$strErrorDesc .= $strErrorAnd.$strError;
										$strErrorAnd = "<br>\r\n";
									}
								}
							}
						}
						break;
					default:
						break;
				}
			}
		}		
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("sendNotifications finished", 1, 1, "color:purple;");
		}
		
		//Actualiza el status del mensaje para indicar que ya fue procesado, dependiendo de si hubo o no errores asignará un status de ok o de fallo, esto para evitar que se vuelva
		//a considerar a este mensaje para el proceso del agente, aunque individualmente cada usuario tiene un status específico sobre si recibió o no el mensaje
		$intPushMsgStatus = ($strErrorDesc != '')?pushMsgError:pushMsgSent;
		$sql = "UPDATE SI_SV_PushMsg SET State = {$intPushMsgStatus} 
			WHERE MessageID = {$this->MessageID}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$this->Repository->DataADOConnection->Execute($sql);
		
		return $strErrorDesc;				
			
	}
	//@JAPR
	
	/**
	 * @param $http2ch          the curl connection
	 * @param $http2_server     the Apple server url
	 * @param $apple_cert       the path to the certificate
	 * @param $app_bundle_id    the app bundle id
	 * @param $message          the payload to send (JSON)
	 * @param $token            the token of the device
	 * @return mixed            the status code (see https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/APNsProviderAPI.html#//apple_ref/doc/uid/TP40008194-CH101-SW18)
	 */
	function sendHTTP2Push(/*$http2ch, */$http2_server, $apple_cert, $app_bundle_id, $message, $token) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Entering sendHTTP2Push method ", 1, 1, "color:blue;");	
			ECHOString("Server: {$http2_server}", 1, 1, "color:green;");
			ECHOString("Certificate: {$apple_cert}", 1, 1, "color:green;");
			ECHOString("Bundle ID: {$app_bundle_id}", 1, 1, "color:green;");
			ECHOString("Payload: {$message}", 1, 1, "color:green;");
			ECHOString("Token: {$token}", 1, 1, "color:green;");
		}
		
		$strErrorDesc = "";
		$http2ch = curl_init();
		curl_setopt($http2ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
		$milliseconds = round(microtime(true) * 1000);

		// url (endpoint)
		$url = "{$http2_server}/3/device/{$token}";

		// certificate
		$cert = realpath($apple_cert);
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Cert: {$cert}", 1, 1, "color:green;");
		}

		// headers
		$headers = array(
			"apns-topic: {$app_bundle_id}"/*,
			"User-Agent: My Sender"*/
		);

		// other curl options
		curl_setopt_array($http2ch, array(
			CURLOPT_URL => "{$url}",
			CURLOPT_PORT => 443,
			CURLOPT_HTTPHEADER => $headers,
			CURLOPT_POST => TRUE,
			CURLOPT_POSTFIELDS => $message,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSLCERT => $cert,
			CURLOPT_HEADER => 1
		));

		// go...
		$result = curl_exec($http2ch);
		if ($result === FALSE) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("The push msg produced an error", 1, 1, "color:red;");
			}
			$strErrorDesc = curl_error($http2ch);
			curl_close($http2ch);
			return "Error: ".$strErrorDesc;
			//throw new Exception('Curl failed with error: ' . curl_error($http2ch));
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("HTTP Result: '{$result}'", 1, 1, "color:green;");
			}
		}

		// get respnse
		$arrRequestInfo = curl_getinfo($http2ch/*, CURLINFO_HTTP_CODE*/);
		if ( $arrRequestInfo && is_array($arrRequestInfo) ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("The push msg response:", 1, 1, "color:blue;");
				PrintMultiArray($arrRequestInfo);
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("No request info", 1, 1, "color:blue;");
			}
		}

		$duration = round(microtime(true) * 1000) - $milliseconds;
		curl_close($http2ch);
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Finishing sendHTTP2Push", 1, 1, "color:blue;");	
		}

	//    echo $duration;

		return "";
	}

	//OMMC 2018-02-26: Nuevo servicio de APNS para mensajes en iOS
	function SendAPNSMessage($sendType, $pemFile, $messagePayload) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\n SendAPNSMessage executing via: {$sendType}", 1, 1, "color:purple;");
		}
		$apns = null;
		$pemFile = (string) @$pemFile;
		self::$APNSError = '';
		self::$APNSErrorString = '';
		
		switch($sendType) {
			case "server":
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("<br>\r\n Creating APNSServer {$pemFile}", 1, 1, "color:purple;");
				}
				if (!self::$arrAPNSiOSServer[$pemFile]) {
					self::$arrAPNSiOSServer[$pemFile] = new ApnsPHP_Push_Server(ApnsPHP_Abstract::ENVIRONMENT_SANDBOX, $pemFile);
				}

				if (self::$arrAPNSiOSServer[$pemFile]) {
					self::$arrAPNSiOSServer[$pemFile]->setRootCertificationAuthority('PEMFiles/entrust_root_certification_authority.pem');
					//Default es 3
					self::$arrAPNSiOSServer[$pemFile]->setProcesses(3);
					self::$arrAPNSiOSServer[$pemFile]->start();
					
					while(self::$arrAPNSiOSServer[$pemFile]->run()){
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("<br>\r\n Error stream report:", 1, 1, "color:red;");
							$aErrorQueue = self::$arrAPNSiOSServer[$pemFile]->getErrors();
							if ($aErrorQueue) { var_dump($aErrorQueue); }
						}
						$aMessage = BITAMPushMsg::messageInstance($messagePayload['regID'], $messagePayload['notification']);
						if ($aMessage) { 
							self::$arrAPNSiOSServer[$pemFile]->add($aMessage); 
						} else {
							self::$APNSError = '1';
							self::$APNSErrorString = 'Error adding the message to the link stream.';
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\n Error adding message to link stream.", 1, 1, "color:red;");
							}
						}
						usleep(200000);
					}
				} else {
					self::$APNSError = '-1';
					self::$APNSErrorString = 'Error creating APNS Server';
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\n APNS server could not be created for: {$pemFile}", 1, 1, "color:red;");
					}
				}
			break;
			default:
			case "link":
				try {
					$apns = new ApnsPHP_Push(ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,	$pemFile);
				} catch(Exception $e) {
					self::$APNSError = '-1';
					self::$APNSErrorString = 'Error creating APNS Connection' . $e;
					//Error al momento de crear la instancia del mensaje
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\n Error while creating apns instance: $e", 1, 1, "color:red;");
					}
				}
				if ($apns) {
					$apns->setRootCertificationAuthority('PEMFiles/entrust_root_certification_authority.pem');
					//OMMC 2018-03-27: Agregado el passphrase para validar los PEMs al momomento del request, estaba fallando para ambiente de producción con apps de la tienda.
					$apns->setProviderCertificatePassphrase(PEMPassphrase);
				} else {
					self::$APNSError = '-1';
					self::$APNSErrorString = 'Error creating APNS Connection';
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\n Error creating APNS connection for: {$pemFile}, bForceNew == {$bForceNew}", 1, 1, "color:red;");
					}
				}
				$apns->connect();
				$aMessage = BITAMPushMsg::messageInstance($messagePayload['regID'], $messagePayload['notification']);
				if ($aMessage) { 
					$apns->add($aMessage); 
				} else {
					self::$APNSError = '1';
					self::$APNSErrorString = 'Error adding the message to the link stream.';
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("<br>\r\n Error adding message to link stream.", 1, 1, "color:red;");
					}
				}
				$apns->send();
				BITAMPushMsg::CloseAPNSLink($apns);
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("<br>\r\n Error stream report:", 1, 1, "color:red;");
					$aErrorQueue = $apns->getErrors();
					if ($aErrorQueue) { var_dump($aErrorQueue); }
				}
			break;
		}
	}
	
	function CloseAPNSLink($apns) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CloseAPNSLink", 1, 1, "color:purple;");
		}
		
		if (is_null($apns)) {
			return;
		}
		
		try {
			$apns->disconnect();
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("CloseAPNSLink connection closed", 1, 1, "color:purple;");
			}
		}
		catch (Exception $e) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Failed to close APNS Link because: {$e}", 1, 1, "color:red;");
			}
		}
	}
	
	/*
		$aRegID			(string)		RegID del usuario al que se envía el mensaje
		$notification	(object)		Objeto de notificación, contiene elementos como el badge, título, mensaje, alerta, etc.
	*/
	function messageInstance($aRegID, $notification) {
		$aMessage = null;
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("<br>\r\n Generating message instance for: {$aRegID}", 1, 1, "color:purple;");
		}
		
		try {
			$aMessage = new ApnsPHP_Message($aRegID);
			//Se crea un string aleatorio para identificar el mensaje y recuperalo en caso de ser necesario
			$aMessage->setCustomIdentifier("");
			$aMessage->setBadge($notification["badge"]);
			$aMessage->setText($notification["body"]);
			$aMessage->setSound();
			//En caso de querer mandar propiedades extras
			/*
				propertyName	(string)		Nombre de la propiedad que se va a enviar.
				property		(mixed)			Objeto, array, int, string, etc. que sería enviado.
			*/
			//$message->setCustomProperty(propertyName, property);
		} catch(Exception $e) {
			//Error al momento de crear la instancia del mensaje
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("<br>\r\n Error while creating message instance: $e", 1, 1, "color:red;");
			}
		}
		return $aMessage;
	}

	//@JAPR 2018-02-26: Esta función es la que se programó originalmente y es usada hasta que se habilite el soporte de sendHTTP2Push
	function sendMessage($theAppUser = null, $sAppName = '') {
		global $gbIsPushAgent;
		$objSetting = NULL;
		$strErrorDesc = '';
		$strPushRegID = '';
		try {
		    if (is_null($theAppUser) && $this->DestinationType == pushMsgUser) {
				//@JAPR 2017-09-29: Corregido un bug, se estaban cargando los usuarios por medio del UserID, siendo que debía ser el CLA_USUARIO (#4WSLS1)
				if (getMDVersion() >= esvRealPushMsg) {
					$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($this->Repository, $this->DestinationID);
				}
				else {
					$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $this->DestinationID);
				}
				//@JAPR
			}
			
		    if (is_null($theAppUser)) {
		    	return;
		    }
		    
		    if (getMDVersion() >= esvAllMobile) {
		    	$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository,'ALLMOBILE');
			}
			if (getMDVersion() >= esvAllMobile && !is_null($objSetting) && $objSetting->SettingValue) {
			    //MAPR 2018-02-15: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
				$theAppUser->getAllMobileData($sAppName);	
			} else {
				//Obtiene el RegID de este usuario según el último dispositivo que ha utilizado para conectarse a eForms
			    $theAppUser->getMobileDataV6($sAppName);
			}
			//Server legacy token
			$apiKey = "AIzaSyAhJkvrJmJjEknzgxFNH2aKimN8enjnMkg";
			//New token
			//$apiKey = "AIzaSyC4t7DC1pPInDgprpagYe9zw1RZ6CtRi0A";
			//Old token
			//$apiKey = "AIzaSyB2flGIjUeX0c_pBhBTxSZ2GiDeaSo6dio";    //api key de google que se genera por APLICACIÓN
			$arrAndroidPhones = array();
			$arrIOsPhones = array();
			echo('<br> $theAppUser:');
			PrintMultiArray($theAppUser);
			foreach ($theAppUser->MobileData as $strRegID => $arrMobileData) {
		    	$strType = trim((string) @$arrMobileData['type']);
				$strUUID = trim((string) @$arrMobileData['uuid']);
				$strUserAgent = trim((string) @$arrMobileData['userAgent']);
		    	if ($strRegID == '') {
					//@JAPR 2018-02-23: Se comentó este mensaje de error para que no por un registro de RegID inválido se marcara toda la tarea como inválida,
					//especialmente al enviar notificaciones con la configuración de enviar a todos los dispositivos históricos del usuario (#)
					//$strErrorDesc .= "<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						//ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
						ECHOString("<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'", 1, 1, "color:red;");
					}
		    		continue;
		    	}
		    	
				$strPushRegID = $strRegID;
				
		    	if ($strType == 'IOS') {
		    		$arrIOsPhones[] = $strRegID;
		    	}
		    	else {
		    		$arrAndroidPhones[] = $strRegID;
		    	}
		    }
		    
		    //Mandar mensaje a los dispositivos Android configurados
			//Comentar el false para habilitar el procesamiento de mensajes a Android, descomentar para depurar solo los de IOS
			if (count($arrAndroidPhones) > 0 /*&& false*/) {
				foreach ($arrAndroidPhones as $regID)
				{
					//Cambiada la URL
					$url = 'https://fcm.googleapis.com/fcm/send';
					//$url = 'https://gcm-http.googleapis.com/gcm/send';
					//$url = 'https://android.googleapis.com/gcm/send';
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					//Cambiado de "registration_ids" a "to"
					$fields = array(
						'to' => $regID,
						'data' => array( "message" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
					);
					$headers = array(
						'Authorization: key=' . $apiKey,
						'Content-Type: application/json'
					);
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Sending android push message", 1, 1, "color:blue;");
						ECHOString("URL: {$url}", 1, 1, "color:blue;");
						ECHOString("Fields: ", 1, 1, "color:blue;");
						PrintMultiArray($fields);
						ECHOString("Headers:", 1, 1, "color:blue;");
						PrintMultiArray($headers);
					}
					
					// Open connection
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 1);
					curl_setopt($ch, CURLOPT_NOBODY, 0);
					curl_setopt($ch, CURLOPT_VERBOSE, 0);
					
					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					
					// Execute post
					$result = curl_exec($ch);
					if ($result === false){
						$strErrorDescLoc = curl_error($ch);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Error sending Android push msg '{$strErrorDescLoc}'", 1, 1, "color:red;");
						}
						$strErrorDesc .= $strErrorDescLoc;
					}
					else {
						/*if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("No error sending Android push, the response was: '{$result}'", 1, 1, "color:blue;");
						}*/
					}
					
					// Close connection
					curl_close($ch);
				}
			}
			
			/*$pemFiles = array(
					0 => 'ck.pem',
					1 => 'ckformsstore.pem',
					2 => 'ckmovilidad.pem',
					1 => 'ck_forms.pem',
					3 => 'FormsV6PushCK.pem',
					4 => 'GeocontrolPushCK.pem',
					5 => 'formsV602Push.pem',
					6 => 'petrotechPush.pem'
					);*/
			//$pemFiles = array(5 => 'formsV602Push.pem');
			//@JAPR 2018-02-23: Corregido un bug, los PEMs se estaban asignando como la manera original exclusivamente de un archivo y con la variable $arrMobileData
			//que en este punto no tiene contexto, siendo que lo correcto es que se asigne por cada dispositivo específico así que se movió al ciclo siguiente
			//donde podrá variar para cada dispositivo que deberá recibir el mensaje de push
			/*$pemFiles = (string) @$arrMobileData['pemFile'];
			if (is_null($pemFiles)) {
				$pemFiles = array();
			}
			if (!is_array($pemFiles)) {
				$pemFiles = array($pemFiles);
			}*/
			//@JAPR
			
			//Mandar mensaje a los dispositivos IOs configurados
			//Descomentar el false para habilitar el procesamiento de mensajes a IOS, descomentar para depurar solo los de Android
			if (count($arrIOsPhones) > 0 /*&& false*/)
			{
				$intContadorMsgs = 0;
				foreach ($arrIOsPhones as $strRegID)
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString(str_repeat("*", 80), 1, 1, "color:purple;");
						ECHOString("Processing {$strRegID}", 1, 1, "color:purple;");
					}
					
					$arrMobileData = @$theAppUser->MobileData[$strRegID];
					$strPEMFile = (string) @$arrMobileData['pemFile'];
					//if (is_null($pemFiles)) {
						$pemFiles = array($strPEMFile);
					//}
					if (!is_array($pemFiles)) {
						$pemFiles = array($pemFiles);
					}
					
					//hay q ejecutar por cada pem este codigo
					//si es q existe ese pem
					foreach ($pemFiles as $pem) {
						if(file_exists($pem)) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("With PEM File: {$pem}", 1, 1, "color:purple;");
							}
							$message = array("body" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline', 'badge' => BITAMPushMsgCollection::GetPushMessagesBadgeNumberByUser($this->Repository, $theAppUser->CLA_USUARIO, $strRegID) + 1, 'sound' => 'default');
							$load = array("regID" => $strRegID, "notification" => $message);
							
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Sending ios push message", 1, 1, "color:blue;");
								ECHOString("Payload: ", 1, 1, "color:blue;");
								PrintMultiArray($load);
							}
							//OMMC 2018-03-18: Modificado a que soporte 2 tipos de envío.
							//Ya sea que se vaya a crear un servidor que soporte todos los mensajes para un mismo PEM o que se envíe de manera independiente por un link. Descomentar y comentar la línea de $sendType en caso de ser necesario.

							$sendType = "link";
							//$sendType = "server";
							BITAMPushMsg::SendAPNSMessage($sendType, $pem, $load);
						} else {
							$strErrorDesc .= "<br>\r\n PEM file not found: {$pem}";
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\n PEM file not found: {$pem}", 1, 1, "color:red;");
							}
						}
						
						$intContadorMsgs++;
					}
				}
			}
		} catch (Exception $e) {
			$strErrorDesc .= $e->getMessage();
		}

		//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
		//Ahora existe una tabla de detalle de envío de notificaciones por usuario donde se actualizará el Status de la notificación
		if ($strErrorDesc != '') {
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, $strErrorDesc);
			
			if ($gbIsPushAgent) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
				}
			}
			else {
				die("(".__METHOD__.") ".$strErrorDesc);
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("PushMsg->sendMessage ok", 1, 1, "color:blue;");
			}
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, '', pushMsgSent);
		}
		
		return $strErrorDesc;
	}

	//@JAPR 2018-02-26: Esta función es la que se programó originalmente y es usada hasta que se habilite el soporte de sendHTTP2Push
	function sendMessageOld($theAppUser = null, $sAppName = '') {
		global $gbIsPushAgent;
		$objSetting = NULL;
		$strErrorDesc = '';
		$strPushRegID = '';
		try {
		    if (is_null($theAppUser) && $this->DestinationType == pushMsgUser) {
				$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $this->DestinationID);
			}
			
		    if (is_null($theAppUser)) {
		    	return;
		    }
		    
		    if (getMDVersion() >= esvAllMobile) {
		    	$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository,'ALLMOBILE');
			}
			if (getMDVersion() >= esvAllMobile && !is_null($objSetting) && $objSetting->SettingValue) {
			    //MAPR 2018-02-15: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
				$theAppUser->getAllMobileData($sAppName);	
			} else {
				//Obtiene el RegID de este usuario según el último dispositivo que ha utilizado para conectarse a eForms
			    $theAppUser->getMobileDataV6($sAppName);
			}
			//Server legacy token
			$apiKey = "AIzaSyAhJkvrJmJjEknzgxFNH2aKimN8enjnMkg";
			//New token
			//$apiKey = "AIzaSyC4t7DC1pPInDgprpagYe9zw1RZ6CtRi0A";
			//Old token
			//$apiKey = "AIzaSyB2flGIjUeX0c_pBhBTxSZ2GiDeaSo6dio";    //api key de google que se genera por APLICACIÓN
			$arrAndroidPhones = array();
			$arrIOsPhones = array();
			echo('<br> $theAppUser:');
			PrintMultiArray($theAppUser);
			foreach ($theAppUser->MobileData as $strRegID => $arrMobileData) {
		    	$strType = trim((string) @$arrMobileData['type']);
				$strUUID = trim((string) @$arrMobileData['uuid']);
				$strUserAgent = trim((string) @$arrMobileData['userAgent']);
		    	if ($strRegID == '') {
					//@JAPR 2018-02-23: Se comentó este mensaje de error para que no por un registro de RegID inválido se marcara toda la tarea como inválida,
					//especialmente al enviar notificaciones con la configuración de enviar a todos los dispositivos históricos del usuario (#)
					//$strErrorDesc .= "<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						//ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
						ECHOString("<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'", 1, 1, "color:red;");
					}
		    		continue;
		    	}
		    	
				$strPushRegID = $strRegID;
				
		    	if ($strType == 'IOS') {
		    		$arrIOsPhones[] = $strRegID;
		    	}
		    	else {
		    		$arrAndroidPhones[] = $strRegID;
		    	}
		    }
		    
		    //Mandar mensaje a los dispositivos Android configurados
			//Comentar el false para habilitar el procesamiento de mensajes a Android, descomentar para depurar solo los de IOS
			if (count($arrAndroidPhones) > 0 /*&& false*/) {
				foreach ($arrAndroidPhones as $regID)
				{
					//Cambiada la URL
					$url = 'https://fcm.googleapis.com/fcm/send';
					//$url = 'https://gcm-http.googleapis.com/gcm/send';
					//$url = 'https://android.googleapis.com/gcm/send';
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					//Cambiado de "registration_ids" a "to"
					$fields = array(
						'to' => $regID,
						'data' => array( "message" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
					);
					$headers = array(
						'Authorization: key=' . $apiKey,
						'Content-Type: application/json'
					);
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Sending android push message", 1, 1, "color:blue;");
						ECHOString("URL: {$url}", 1, 1, "color:blue;");
						ECHOString("Fields: ", 1, 1, "color:blue;");
						PrintMultiArray($fields);
						ECHOString("Headers:", 1, 1, "color:blue;");
						PrintMultiArray($headers);
					}
					
					// Open connection
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 1);
					curl_setopt($ch, CURLOPT_NOBODY, 0);
					curl_setopt($ch, CURLOPT_VERBOSE, 0);
					
					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					
					// Execute post
					$result = curl_exec($ch);
					if ($result === false){
						$strErrorDescLoc = curl_error($ch);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Error sending Android push msg '{$strErrorDescLoc}'", 1, 1, "color:red;");
						}
						$strErrorDesc .= $strErrorDescLoc;
					}
					else {
						/*if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("No error sending Android push, the response was: '{$result}'", 1, 1, "color:blue;");
						}*/
					}
					
					// Close connection
					curl_close($ch);
				}
			}
			
			/*$pemFiles = array(
					0 => 'ck.pem',
					1 => 'ckformsstore.pem',
					2 => 'ckmovilidad.pem',
					1 => 'ck_forms.pem',
					3 => 'FormsV6PushCK.pem',
					4 => 'GeocontrolPushCK.pem',
					5 => 'formsV602Push.pem',
					6 => 'petrotechPush.pem'
					);*/
			//$pemFiles = array(5 => 'formsV602Push.pem');
			//@JAPR 2018-02-23: Corregido un bug, los PEMs se estaban asignando como la manera original exclusivamente de un archivo y con la variable $arrMobileData
			//que en este punto no tiene contexto, siendo que lo correcto es que se asigne por cada dispositivo específico así que se movió al ciclo siguiente
			//donde podrá variar para cada dispositivo que deberá recibir el mensaje de push
			/*$pemFiles = (string) @$arrMobileData['pemFile'];
			if (is_null($pemFiles)) {
				$pemFiles = array();
			}
			if (!is_array($pemFiles)) {
				$pemFiles = array($pemFiles);
			}*/
			//@JAPR
			
			//Mandar mensaje a los dispositivos IOs configurados
			//Comentar el false para habilitar el procesamiento de mensajes a IOS, descomentar para depurar solo los de Android
			if (count($arrIOsPhones) > 0 && false/*&& false*/)
			{
				$intContadorMsgs = 0;
				foreach ($arrIOsPhones as $strRegID)
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString(str_repeat("*", 80), 1, 1, "color:purple;");
						ECHOString("Processing {$strRegID}", 1, 1, "color:purple;");
					}
					
					$arrMobileData = @$theAppUser->MobileData[$strRegID];
					$strPEMFile = (string) @$arrMobileData['pemFile'];
					//if (is_null($pemFiles)) {
						$pemFiles = array($strPEMFile);
					//}
					if (!is_array($pemFiles)) {
						$pemFiles = array($pemFiles);
					}
					
					//hay q ejecutar por cada pem este codigo
					//si es q existe ese pem
					foreach ($pemFiles as $pem)
					{
						if(file_exists($pem))
						{
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("With PEM File: {$pem}", 1, 1, "color:purple;");
							}
							//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
							//Modificado el acceso al API de Apple para envío de Push, ahora se mantendrá viva la conexión hasta un máximo de 1 hora, ya que si se abre y cierra muchas
							//veces el server de Apple puede considerar un ataque de Spam y bloquear el acceso, así que la clase mantendrá en variable estáticas el array de las conexiones
							//creadas por cada archivo .pem para poder reutilizarlas, anexando la fecha y hora en que fue creada para que sólo dure como máximo 1 hora abierta, ya que la
							//documentación indicaba que el token otorgado para la conexión podría expirar y en ese caso habría que volver a generar la conexión
							//https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/Introduction.html
							// Create a stream to the server
							//Se usará esta función para validar si la conexión existente ya había expirado, en caso de no ser así se podrá usar la conexión existente, y si no hubiera
							//existido la conexión de cualquier manera este método la habría creado así que es válido. Es irrelevante el resultado porque no interesa si la conexión estaba
							//o no vigente, lo que interesa es el hecho de que exista una conexión que si lo esté simplemente
							BITAMPushMsg::ValidateAPNSTokenExpiration($pem);
							//En este punto un CreateAPNSConnection posterior a un ValidateAPNSTokenExpiration no está creando una conexión, está reutilizando aquella creada por el otro
							//método, lo cual además asegura que sea una conexión vigente para que no provoque algún error por expiración del token
							$apns = BITAMPushMsg::CreateAPNSConnection($pem);
							/*
							$streamContext = stream_context_create();
							stream_context_set_option($streamContext, 'ssl', 'local_cert', $pem);//'ck.pem'
							stream_context_set_option($streamContext, 'ssl', 'passphrase', 'b1t4m1nc');
							$apns = @stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
							*/
							if (!$apns) {
								$error = (string) @BITAMPushMsg::$APNSError;
								$errorString = (string) @BITAMPushMsg::$APNSErrorString;
								$strErrorDesc .= "<br>\r\n Failed to connect: {$error} {$errorString}";
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("Failed to connect: {$error} {$errorString}", 1, 1, "color:red;");
								}
								BITAMPushMsg::CloseAPNSConnection($pem);
								continue;
							}
							else {
								//$strErrorDesc = '';
							}

							// Now we need to create JSON which can be sent to APNS
							//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
							$load = array('aps' => array(
								'alert' => array( "body" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
								'badge' => BITAMPushMsgCollection::GetPushMessagesBadgeNumberByUser($this->Repository, $theAppUser->CLA_USUARIO, $strRegID) + 1,
								'sound' => 'default')
							);
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Sending ios push message", 1, 1, "color:blue;");
								ECHOString("Payload: ", 1, 1, "color:blue;");
								PrintMultiArray($load);
							}
							
							$payload = json_encode($load);
							
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Stream package: Packed RegID = ".pack('H*', str_replace(' ', '', $strRegID)), 1, 1, "color:blue;");
								ECHOString("Stream package: Payload length = ".strlen($payload), 1, 1, "color:blue;");
								ECHOString("Stream package: Chr Payload length = ".chr(strlen($payload)), 1, 1, "color:blue;");
							}
							// The payload needs to be packed before it can be sent
							//@JAPR 2018-02-23: Corrección al envío de mensajes push de Apple (#)
							//Este es el legacy format como se menciona aquí:
							//"0" "0" "tokenlength" "devicetoken(el pack)" "0" "payloadlength" "payload(el json_encode de $load)"
							//https://developer.apple.com/library/content/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/LegacyNotificationFormat.html
							$apnsMessage = chr(0) . chr(0) . chr(32);
							$apnsMessage .= pack('H*', str_replace(' ', '', $strRegID));
							$apnsMessage .= chr(0) . chr(strlen($payload)) . $payload;
							/*//Se cambiará al "Enhanced Notification Format" mencionado en la misma URL de arriba
							//"1" "identifier 4bytes" "expiry 4 bytes" "0" "tokenlength" "devicetoken(el pack)" "0" "payloadlength" "payload(el json_encode de $load)"
							$apnsMessage = chr(1) . pack('i*', $intContadorMsgs) . chr(0) . chr(0) . chr(32);
							$apnsMessage .= pack('H*', str_replace(' ', '', $strRegID));
							$apnsMessage .= chr(0) . chr(strlen($payload)) . $payload;*/
							
							// Write the payload to the APNS
							//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
							//Validado que si falla el evento de write, se verifique si era porque el token de la conexión apns había caducado y se vuelva a intentar antes de generar
							//un error definitivo y marcar el envío de mensaje como un fallo, dado a que los tokens provistos por apple tienen una vigencia que no es controlada por
							//el producto, o bien el certificado pudo haber expirado, en ambos casos se recomienda volver a crear la conexión y reintentar
							$blnAPNSRetry = true;
							$blnAPNSRetried = false;
							do {
								$blnAPNSSucceed = false;
								$blnRequestFailed = false;
								try {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("Writing push message in apns stream: {$apnsMessage}", 1, 1, "color:blue;");
									}
									//@JAPR 2018-02-22: Validación de escritura en IOS (#)
									$intBytes = fwrite($apns, $apnsMessage);
									if ( $intBytes === false || $intBytes === 0 ) {
										ECHOString("Writing to the APNS failed", 1, 1, "color:red;");
										$blnRequestFailed = true;
									}
									else {
										ECHOString("Writing to the APNS succeeded: {$intBytes} bytes written", 1, 1, "color:green;");
										$blnAPNSSucceed = true;
									}
								}
								catch (Exception $e) {
									$strErrorDesc .= "There was an error writing to the apns stream: ".$e->getMessage();
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("There was an error writing to the apns stream: ".$e->getMessage(), 1, 1, "color:red;");
									}
								}
								
								//En caso de error en el envío del mensaje, debe cerrar la conexión y reintentar si es que se valida que ya ha pasado mucho tiempo abierta, pero si
								//este proceso ya se había intentado o no se ha cumplido el tiempo entonces simplemente marcará error
								if (!$blnAPNSSucceed) {
									if ($blnAPNSRetry) {
										$blnAPNSRetry = false;
										//Valida si ya expiró el tiempo de vide del token para intentar una nueva conexión apns
										//@JAPR 2018-02-23: Agregado el parámetro $bForceClose para que en caso de no haber recibido respuesta se force a cerrar 
										//la conexión en un intento de mandar nuevamente el mensaje (#)
										$blnTryAgain = BITAMPushMsg::ValidateAPNSTokenExpiration($pem, $blnRequestFailed);
										if ($blnTryAgain) {
											//Se crea de nuevo la conexión apns para intentar enviar el mensaje
											$apns = BITAMPushMsg::CreateAPNSConnection($pem);
										}
										else {
											//En este caso no se ha concluido el timeout de la conexión, por lo tanto cualquier error debe permanecer como tal y se forza a salir del ciclo
											$blnAPNSRetried = true;
										}
									}
									else {
										$blnAPNSRetried = true;
										$strErrorDesc .= "Writing to the APNS failed";
									}
								}
								
							} while(!$blnAPNSSucceed && !$blnAPNSRetried);
							
							// Close the connection
							//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
							//La conexión se mantendrá viva, cerrándose sólo hasta que el Agente termine su ejecución
							/*BITAMPushMsg::CloseAPNSConnection($pem);
							fclose($apns);*/
							//@JAPR
							//GCRUZ 2016-05-16. Se asume que si ya llegó a esta instancia, el mensaje ya fué enviado correctamente
							//OMMC 2016-09-09: Comentado el break para que se mande a todos los .pem (la manera correcta sería mandar cierta info del repositorio indicando el .pem requerido)
							//break;
						}
						else {
							$strErrorDesc .= "<br>\r\n PEM file not found: {$pem}";
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\n PEM file not found: {$pem}", 1, 1, "color:red;");
							}
						}
						
						$intContadorMsgs++;
					}
				}
			}
		} catch (Exception $e) {
			$strErrorDesc .= $e->getMessage();
		}

		//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
		//Ahora existe una tabla de detalle de envío de notificaciones por usuario donde se actualizará el Status de la notificación
		if ($strErrorDesc != '') {
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, $strErrorDesc);
			
			if ($gbIsPushAgent) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
				}
			}
			else {
				die("(".__METHOD__.") ".$strErrorDesc);
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("PushMsg->sendMessage ok", 1, 1, "color:blue;");
			}
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, '', pushMsgSent);
		}
		
		return $strErrorDesc;
	}
	
	//Realiza la invocación del mensaje hacia el dispositivo móvil, si logra hacerlo sin error, actualiza el Status del mensaje
	//@JAPR 2018-02-26: Esta función es la que se usaría en caso de implementar adecuadamente el soporte para la función sendHTTP2Push, ya que con la versión actual
	//de curl no era posible realizar el tipo de request mediante CURL_HTTP_VERSION_2_0
	function sendMessageHTTP2($theAppUser = null, $sAppName = '') {
		global $gbIsPushAgent;
		$objSetting = NULL;
		$strErrorDesc = '';
		$strPushRegID = '';
		try {
		    if (is_null($theAppUser) && $this->DestinationType == pushMsgUser) {
				$theAppUser = BITAMAppUser::NewInstanceWithID($this->Repository, $this->DestinationID);
			}
			
		    if (is_null($theAppUser)) {
		    	return;
		    }
		    
		    if (getMDVersion() >= esvAllMobile) {
		    	$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository,'ALLMOBILE');
			}
			if (getMDVersion() >= esvAllMobile && !is_null($objSetting) && $objSetting->SettingValue) {
			    //MAPR 2018-02-15: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
				$theAppUser->getAllMobileData($sAppName);	
			} else {
				//Obtiene el RegID de este usuario según el último dispositivo que ha utilizado para conectarse a eForms
			    $theAppUser->getMobileDataV6($sAppName);
			}
			//Server legacy token
			$apiKey = "AIzaSyAhJkvrJmJjEknzgxFNH2aKimN8enjnMkg";
			//New token
			//$apiKey = "AIzaSyC4t7DC1pPInDgprpagYe9zw1RZ6CtRi0A";
			//Old token
			//$apiKey = "AIzaSyB2flGIjUeX0c_pBhBTxSZ2GiDeaSo6dio";    //api key de google que se genera por APLICACIÓN
			$arrAndroidPhones = array();
			$arrIOsPhones = array();
			echo('<br> $theAppUser:');
			PrintMultiArray($theAppUser);
			foreach ($theAppUser->MobileData as $strRegID => $arrMobileData) {
		    	$strType = trim((string) @$arrMobileData['type']);
				$strUUID = trim((string) @$arrMobileData['uuid']);
				$strUserAgent = trim((string) @$arrMobileData['userAgent']);
		    	if ($strRegID == '') {
					//@JAPR 2018-02-23: Se comentó este mensaje de error para que no por un registro de RegID inválido se marcara toda la tarea como inválida,
					//especialmente al enviar notificaciones con la configuración de enviar a todos los dispositivos históricos del usuario (#)
					//$strErrorDesc .= "<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'";
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						//ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
						ECHOString("<br>\r\n Invalid mobile data, no RegID registered for this device: uuid = '{$strUUID}', userAgent = '{$strUserAgent}'", 1, 1, "color:red;");
					}
		    		continue;
		    	}
		    	
				$strPushRegID = $strRegID;
				
		    	if ($strType == 'IOS') {
		    		$arrIOsPhones[] = $strRegID;
		    	}
		    	else {
		    		$arrAndroidPhones[] = $strRegID;
		    	}
		    }
		    
		    //Mandar mensaje a los dispositivos Android configurados
			//Comentar el false para habilitar el procesamiento de mensajes a Android, descomentar para depurar solo los de IOS
			if (count($arrAndroidPhones) > 0 /*&& false*/) {
				foreach ($arrAndroidPhones as $regID)
				{
					//Cambiada la URL
					$url = 'https://fcm.googleapis.com/fcm/send';
					//$url = 'https://gcm-http.googleapis.com/gcm/send';
					//$url = 'https://android.googleapis.com/gcm/send';
					//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
					//Cambiado de "registration_ids" a "to"
					$fields = array(
						'to' => $regID,
						'data' => array( "message" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
					);
					$headers = array(
						'Authorization: key=' . $apiKey,
						'Content-Type: application/json'
					);
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Sending android push message", 1, 1, "color:blue;");
						ECHOString("URL: {$url}", 1, 1, "color:blue;");
						ECHOString("Fields: ", 1, 1, "color:blue;");
						PrintMultiArray($fields);
						ECHOString("Headers:", 1, 1, "color:blue;");
						PrintMultiArray($headers);
					}
					
					// Open connection
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
					curl_setopt($ch, CURLOPT_TIMEOUT, 30);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_HEADER, 1);
					curl_setopt($ch, CURLOPT_NOBODY, 0);
					curl_setopt($ch, CURLOPT_VERBOSE, 0);
					
					// Set the url, number of POST vars, POST data
					curl_setopt( $ch, CURLOPT_URL, $url );
					curl_setopt( $ch, CURLOPT_POST, true );
					curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($fields));
					curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					
					// Execute post
					$result = curl_exec($ch);
					if ($result === false){
						$strErrorDesc .= curl_error($ch);
					}
					
					// Close connection
					curl_close($ch);
				}
			}
			
			/*$pemFiles = array(
					0 => 'ck.pem',
					1 => 'ckformsstore.pem',
					2 => 'ckmovilidad.pem',
					1 => 'ck_forms.pem',
					3 => 'FormsV6PushCK.pem',
					4 => 'GeocontrolPushCK.pem',
					5 => 'formsV602Push.pem',
					6 => 'petrotechPush.pem'
					);*/
			//$pemFiles = array(5 => 'formsV602Push.pem');
			//@JAPR 2018-02-23: Corregido un bug, los PEMs se estaban asignando como la manera original exclusivamente de un archivo y con la variable $arrMobileData
			//que en este punto no tiene contexto, siendo que lo correcto es que se asigne por cada dispositivo específico así que se movió al ciclo siguiente
			//donde podrá variar para cada dispositivo que deberá recibir el mensaje de push
			/*$pemFiles = (string) @$arrMobileData['pemFile'];
			if (is_null($pemFiles)) {
				$pemFiles = array();
			}
			if (!is_array($pemFiles)) {
				$pemFiles = array($pemFiles);
			}*/
			//@JAPR
			
			//Mandar mensaje a los dispositivos IOs configurados
			//Comentar el false para habilitar el procesamiento de mensajes a IOS, descomentar para depurar solo los de Android
			if (count($arrIOsPhones) > 0 && false/*&& false*/)
			{
				$intContadorMsgs = 0;
				foreach ($arrIOsPhones as $strRegID)
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString(str_repeat("*", 80), 1, 1, "color:purple;");
						ECHOString("Processing {$strRegID}", 1, 1, "color:purple;");
					}
					
					$arrMobileData = @$theAppUser->MobileData[$strRegID];
					$strPEMFile = (string) @$arrMobileData['pemFile'];
					//if (is_null($pemFiles)) {
						$pemFiles = array($strPEMFile);
					//}
					if (!is_array($pemFiles)) {
						$pemFiles = array($pemFiles);
					}
					
					//hay q ejecutar por cada pem este codigo
					//si es q existe ese pem
					foreach ($pemFiles as $pem)
					{
						if(file_exists($pem))
						{
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("With PEM File: {$pem}", 1, 1, "color:purple;");
							}

							// Now we need to create JSON which can be sent to APNS
							//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
							$load = array('aps' => array(
								'alert' => array( "body" => $this->PushMessage, "title" => ($this->Title != '')?$this->Title:'KPIOnline'),
								'badge' => BITAMPushMsgCollection::GetPushMessagesBadgeNumberByUser($this->Repository, $theAppUser->CLA_USUARIO, $strRegID) + 1,
								'sound' => 'default')
							);
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Sending ios push message", 1, 1, "color:blue;");
								ECHOString("Payload: ", 1, 1, "color:blue;");
								PrintMultiArray($load);
							}
							
							$payload = json_encode($load);
							
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("Stream package: Packed RegID = ".pack('H*', str_replace(' ', '', $strRegID)), 1, 1, "color:blue;");
								ECHOString("Stream package: Payload length = ".strlen($payload), 1, 1, "color:blue;");
								ECHOString("Stream package: Chr Payload length = ".chr(strlen($payload)), 1, 1, "color:blue;");
							}
							
							$retText = $this->sendHTTP2Push('https://api.development.push.apple.com', $pem, 'com.bitam.rac', $payload, $strRegID);
							if ( $retText ) {
								$strErrorDesc .= "There was an error writing to the apns stream: ".$retText;
							}
						}
						else {
							$strErrorDesc .= "<br>\r\n PEM file not found: {$pem}";
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("<br>\r\n PEM file not found: {$pem}", 1, 1, "color:red;");
							}
						}
						
						$intContadorMsgs++;
					}
				}
			}
		} catch (Exception $e) {
			$strErrorDesc .= $e->getMessage();
		}

		//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
		//Ahora existe una tabla de detalle de envío de notificaciones por usuario donde se actualizará el Status de la notificación
		if ($strErrorDesc != '') {
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, $strErrorDesc);
			
			if ($gbIsPushAgent) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("PushMsg->sendMessage error: {$strErrorDesc}", 1, 1, "color:red;");
				}
			}
			else {
				die("(".__METHOD__.") ".$strErrorDesc);
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("PushMsg->sendMessage ok", 1, 1, "color:blue;");
			}
			$this->updatePushMsgDetailStatus($theAppUser->CLA_USUARIO, $strPushRegID, '', pushMsgSent);
		}
		
		return $strErrorDesc;
	}
	
	/* Crea una nueva conexión APNS utilizando el nombre de archivo .pem existente, si ya existía la conexión entonces simplemente la reutiliza. El parámetro $bForceNew indicará que
	independientemente de la existencia de la conexión, deberá cerrar la anterior (en caso de existir) y volver a abrir otra, esto es útil cuando se llega al tiempo de desconexión previsto
	de una hora para evitar que la posible expiración del token generado cause problemas
	*/
	static function CreateAPNSConnection($sPEMFileName, $bForceNew = false) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CreateAPNSConnection {$sPEMFileName}, bForceNew == {$bForceNew}", 1, 1, "color:purple;");
		}
		
		$sPEMFileName = (string) @$sPEMFileName;
		self::$APNSError = '';
		self::$APNSErrorString = '';
		
		if ($bForceNew) {
			BITAMPushMsg::CloseAPNSConnection($sPEMFileName);
		}
		
		//Primero verifica si ya existe el objeto en el array estático, si es así simplemente lo regresa, esto no aplica si se va a forzar una nueva conexión, en cuyo caso
		//se habría eliminado en el paso previo
		$apns = @self::$arrAPNSPEMConnections[$sPEMFileName]['apns'];
		if (!is_null($apns)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("CreateAPNSConnection returning existing connection", 1, 1, "color:purple;");
			}
			return $apns;
		}
		
		//En este caso debe crear la conexión y devolver la instancia
		$streamContext = stream_context_create();
		stream_context_set_option($streamContext, 'ssl', 'local_cert', $sPEMFileName);
		stream_context_set_option($streamContext, 'ssl', 'passphrase', 'b1t4m1nc');
		$apns = stream_socket_client('ssl://gateway.push.apple.com:2195', self::$APNSError, self::$APNSErrorString, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
		//$apns = @stream_socket_client('ssl://gateway.push.apple.com:2195', self::$APNSError, self::$APNSErrorString, 60, STREAM_CLIENT_ASYNC_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
		if ( $apns ) {
			stream_set_blocking($apns, false);
			$accepted = stream_socket_enable_crypto($apns, true, STREAM_CRYPTO_METHOD_TLS_CLIENT);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Crypto accepted: '{$accepted}'", 1, 1, "color:purple;");
			}
		}
		self::$arrAPNSPEMConnections[$sPEMFileName] = array('apns' => $apns, 'start' => date('Y-m-d H:i:s'));
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CreateAPNSConnection new connection created, error = ".self::$APNSError.", errorString = ".self::$APNSErrorString, 1, 1, "color:purple;");
			if ( $apns === false ) {
				ECHOString("CreateAPNSConnection failed, connection returned false", 1, 1, "color:red;");
			}
		}
		
		return $apns;
	}
	
	/* Cierra la conexión APNS para el archivo .pem indicado y remueve el elemento del array donde se mantenía viva para liberar el recurso */
	static function CloseAPNSConnection($sPEMFileName) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CloseAPNSConnection {$sPEMFileName}", 1, 1, "color:purple;");
		}
		
		$apns = @self::$arrAPNSPEMConnections[$sPEMFileName]['apns'];
		if (is_null($apns)) {
			return;
		}
		
		try {
			fclose($apns);
		}
		catch (Exception $e) {
		}
		self::$arrAPNSPEMConnections[$sPEMFileName] = null;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CloseAPNSConnection connection closed", 1, 1, "color:purple;");
		}
	}
	
	/* Método de liberacipon de recursos de las conexiones APNS existentes, útil únicamente cuando el Agente de notificaciones Push debe terminar su ejecución "permanente" después de
	cumplir con algunas de las múltiples condiciones para tal fin
	*/
	static function CloseAllAPNSConnections() {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("CloseAllAPNSConnections", 1, 1, "color:purple;");
		}
		
		foreach (self::$arrAPNSPEMConnections as $sPEMFileName => $arrAPNSData) {
			BITAMPushMsg::CloseAPNSConnection($sPEMFileName);
		}
	}
	
	/* Verifica si la conexión del archivo .pem especificado ha estado abierta por mas tiempo del configurado para mantenerla viva, en caso de ser así, cierra la conexión y crea una
	nueva para el mismo archivo regresando un true, pero si aún no se había llegado al tiempo límite entonces simplemente regresa false, con lo cual indicará que cualquier problema que
	hubiera ocurrido es probable que no se deba a la expiración del token, por lo que se debería reportar como error de envío. Si se regresó true se está sugiriendo que el proceso
	intente enviar de nuevo el mensaje Push pidiendo previamente la conexión APNS con el método CreateAPNSConnection pero sin forzar a una nueva conexión, ya que este método ya se
	encargó de volver a crearla, por lo anterior es posible simplemente invocar a este método para crear la primera conexión APNS ya que al no encontrarla precisamente la creará para
	poder regresar true, puesto que el valor de false indicaría que no ha expirado la sesión de una conexión existente y en este caso no había conexión
	//@JAPR 2018-02-23: Agregado el parámetro $bForceClose para que en caso de no haber recibido respuesta se force a cerrar la conexión en un intento de mandar
	nuevamente el mensaje (#)
	*/
	static function ValidateAPNSTokenExpiration($sPEMFileName, $bForceClose = false) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("ValidateAPNSTokenExpiration {$sPEMFileName}, bForceClose == '{$bForceClose}'", 1, 1, "color:purple;");
		}
		
		$arrAPNSData = @self::$arrAPNSPEMConnections[$sPEMFileName];
		if (is_null($arrAPNSData) || !is_array($arrAPNSData) || !$arrAPNSData['apns']) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("ValidateAPNSTokenExpiration there is no connection to validate, creating a new connection", 1, 1, "color:purple;");
			}
			$apns = BITAMPushMsg::CreateAPNSConnection($sPEMFileName);
			return true;
		}
		
		//Llegado a este punto ya se tiene una conexión preexistentes, se verifica si ha llegado al tiempo de expiración
		$strStartDate = $arrAPNSData['start'];
		$dteStartDate = new DateTime($strStartDate);
		$strCurrentDate = date('Y-m-d H:i:s');
		$dteCurrentDate = new DateTime($strCurrentDate);
		$interval = $dteCurrentDate->diff($dteStartDate);
		$intDaysDiff = (int) $interval->format('%d');
		$intHoursDiff = (int) $interval->format('%h');
		$intMinutesDiff = (int) $interval->format('%i');
		$intMinutesDiff += $intHoursDiff * 60;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("ValidateAPNSTokenExpiration validating connection:", 1, 1, "color:purple;");
			ECHOString("dteStartDate = {$strStartDate}, dteCurrentDate = {$strCurrentDate}, intDaysDiff = {$intDaysDiff}, intHoursDiff = {$intHoursDiff}, intMinutesDiff = {$intMinutesDiff}", 1, 1, "color:purple;");
		}
		//Si ya había expirado el tiempo de la sesión, resetea la conexión y regresa true
		if ($intDaysDiff > 0 || $intMinutesDiff > BITAMPushMsg::$intAPNSTimeOut || $bForceClose) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("ValidateAPNSTokenExpiration connection exceed it's lifetime, creating a new connection:", 1, 1, "color:purple;");
			}
			$apns = BITAMPushMsg::CreateAPNSConnection($sPEMFileName, true);
			return true;
		}
		
		//En este caso la sesión sigue siendo válida, quien invoca a la función tendrá que reportar error porque no es debido a la expiración del token
		return false;
	}
	
	function remove()
	{
		$sql = "DELETE FROM SI_SV_PushMessageDevices WHERE MessageID = {$this->MessageID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_PushMsg WHERE MessageID = {$this->MessageID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
    
	function isNewObject()
	{
		return ($this->MessageID < 0);
	}

	function get_Title()
	{
		return translate("Mobile notification");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		
		$strParameters = "";
		
		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}
		
		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}
		
		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}
		
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=PushMessage".$strParameters;;
		}
		else
		{
			return "BITAM_PAGE=PushMessage"."&MessageID=".$this->MessageID.$strParameters;
		}
	}

	function get_Parent()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		return BITAMPushMsgCollection::NewInstance($this->Repository, $svStartDate, $svEndDate, $svUserID, $svIpp);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'MessageID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		$myFields = array();
		$arrayUsers = array();
		$arrayUsersOnly = array();
		if ($this->isNewObject()) {
			$arrayUsers[0] = "(".translate("Select user").")";
			$arrayUsers[-1] = "(".translate("Customized").")";
		}
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)@$aRS->fields["cla_usuario"];
			$nom_largo = (string) @$aRS->fields["nom_largo"];
			$arrayUsers[$cla_usuario] = $nom_largo;
			$arrayUsersOnly[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		if ($this->UserID < 0) {
			$this->UserID = 0;
		}
		if (!isset($arrayUsers[$this->UserID])) {
			$this->UserID = -1;
			$arrayUsers[-1] = "(".translate("Deleted user").")";
		}
		
		$strUserButtons = '<span id="spnUserButtons" name="spnUserButtons" style="display:none;"><img title="'.translate("Marcar todos").'" src="images/selectall.png" onclick="checkAllItems(\'UserIDs[]\')" style="cursor:pointer;"><img title="'.translate("Desmarcar todos").'" src="images/unselect.png" onclick="uncheckAllItems(\'UserIDs[]\')" style="cursor:pointer;"></span>';
		$userField = BITAMFormField::NewFormField();
		$userField->Name = "UserID";
		$userField->Title = translate("User Name");
		$userField->ToolTip = translate("User Name");
		$userField->Type = "Object";
		$userField->VisualComponent = "Combobox";
		$userField->Options = $arrayUsers;
		$userField->OnChange = "checkCustomizedUsers()";
		$userField->Size = 255;
		if (!$this->isNewObject()) {
			$userField->IsDisplayOnly = true;
		}
		$myFields[$userField->Name] = $userField;
		
		if ($this->isNewObject()) {
			$surveyField = BITAMFormField::NewFormField();
			$surveyField->Name = "UserIDs";
			$surveyField->Title = translate("Users");
			$surveyField->Type = "Array";
			$surveyField->Options = $arrayUsersOnly;
			$surveyField->AfterMessage = $strUserButtons;
			$myFields[$surveyField->Name] = $surveyField;
		}
		
		$arrDevicesByUser = BITAMAppUser::GetMobileDevicesByUser($this->Repository);
		if (is_null($arrDevicesByUser) || !is_array($arrDevicesByUser)) {
			$arrDevicesByUser = array();
		}
		$arrDevicesByUser[0] = array();
		$arrDevicesByUser[-1] = array();
		
		$strDeviceButtons = '<span id="spnDeviceButtons" name="spnDeviceButtons" style="display:none;"><img title="'.translate("Marcar todos").'" src="images/selectall.png" onclick="checkAllItems(\'DeviceIDs[]\')" style="cursor:pointer;"><img title="'.translate("Desmarcar todos").'" src="images/unselect.png" onclick="uncheckAllItems(\'DeviceIDs[]\')" style="cursor:pointer;"></span>';
		$devicesField = BITAMFormField::NewFormField();
		$devicesField->Name = "DeviceIDs";
		$devicesField->Title = translate("Mobile devices");
		$devicesField->Type = "Array";
		$devicesField->Options = $arrDevicesByUser;
		$devicesField->AfterMessage = $strDeviceButtons;
		$myFields[$devicesField->Name] = $devicesField;
		$devicesField->Parent = $userField;
		$userField->Children[] = $devicesField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Title";
		$aField->Title = translate("Title");
		$aField->ToolTip = translate("Title");
		$aField->Type = "String";
		$aField->Size = 100;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PushMessage";
		$aField->Title = translate("Message");
		$aField->ToolTip = translate("Message");
		$aField->Type = "LargeString";
		$aField->Size = 500;
		$myFields[$aField->Name] = $aField;
		
		if (!$this->isNewObject()) {
			$arrayStatus = array(0 => translate('Not sent'), 1 => translate('Sent'));
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "State";
			$aField->Title = translate("Status");
			$aField->ToolTip = translate("Status");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrayStatus;
			$aField->Size = 255;
			if ($this->State > 0) {
				$aField->IsDisplayOnly = true;
			}
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
	{
?>
 		<script language="JavaScript">
		function verifyFieldsAndSave(target)
 		{
 			strBlanksField = "";
 			
			if(Trim(BITAMPushMsg_SaveForm.Title.value)=="")
			{
				strBlanksField += '\n- ' + '<?=sprintf(translate("%s must be filled in"),translate("Title"))?>';
			}
 			
			selectedIdx = parseInt(BITAMPushMsg_SaveForm.UserID.selectedIndex);
			if(selectedIdx<0 || BITAMPushMsg_SaveForm.UserID.value == 0)
			{
				strBlanksField+= '\n'+'<?=translate("You must have selected a valid user.")?>';
			}
			
			if(strBlanksField!="")
  			{
				alert('<?=translate("Please verify")?>:' + strBlanksField );
  			}
  			else {
  				BITAMPushMsg_Ok(target);
  			}
 		}
 		
 		function checkCustomizedUsers()
 		{
 			if (cancel==true || true)
 			{
 				/*
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					BITAMSurveyAgenda_FilterText = new Array();
	 			}
	 			*/
 			}
 			//cancel=false;
	 		var rowUserIDs = document.getElementById("Row_UserIDs");
	 		if (rowUserIDs) {
	 			rowUserIDs.style.display = (BITAMPushMsg_SaveForm.UserID.value != -1)?"none":((bIsIE)?"inline":"table-row");
	 		}
	 		
	 		var intUserID = 0;
	 		var spanUserButtons = document.getElementById("spnUserButtons");
	 		if (spanUserButtons) {
	 			intUserID = BITAMPushMsg_SaveForm.UserID.value;
				spanUserButtons.style.display = (intUserID != -1)?'none':'inline';
			}
	 		
	 		var rowDeviceIDs = document.getElementById("Row_DeviceIDs");
	 		if (rowDeviceIDs) {
	 			rowDeviceIDs.style.display = (intUserID > 0)?((bIsIE)?"inline":"table-row"):"none";
		 		var spanDeviceButtons = document.getElementById("spnDeviceButtons");
		 		if (spanDeviceButtons) {
					spanDeviceButtons.style.display = rowDeviceIDs.style.display;
				}
	 			
	 			var arrObjs = document.getElementsByName("DeviceIDs[]");
	 			if (arrObjs && arrObjs.length) {
	 				for (var i in arrObjs) {
	 					arrObjs[i].checked = true;
	 				}
	 			}
	 		}
 		}
 		
 		function checkAllItems(sElementName) {
 			var arrObjs = document.getElementsByName(sElementName);
 			if (arrObjs && arrObjs.length) {
 				for (var i in arrObjs) {
 					arrObjs[i].checked = true;
 				}
 			}
 		}
 		
 		function uncheckAllItems(sElementName) {
 			var arrObjs = document.getElementsByName(sElementName);
 			if (arrObjs && arrObjs.length) {
 				for (var i in arrObjs) {
 					arrObjs[i].checked = false;
 				}
 			}
 		}
 		</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return (!$this->isNewObject() && !$this->State);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return (!$this->isNewObject() && !$this->State);
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideBackButton($aUser)
	{
		return true;
	}

	function getImages($questionID)
	{
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateAfterFormCode($aUser)
	{
?>
	 	<script language="JavaScript">
		objOkSelfButton = document.getElementById("BITAMPushMsg_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMPushMsg_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
<?
		if($this->isNewObject())
 		{
?>
			objOkNewButton = document.getElementById("BITAMPushMsg_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
		</script>
<?
	}
	
}

class BITAMPushMsgCollection extends BITAMCollection
{
	public $StartDate;
	public $EndDate;
	public $UserID;
	public $ipp;
	public $Pages;
	
	function __construct($aRepository, $aUserID)
	{
		BITAMCollection::__construct($aRepository);

		$this->UserID = $aUserID;
		$this->MsgConfirmRemove = "Do you wish to delete the selected entries?";
		$this->MsgNoSelectedRemove = "Please select the entries you wish to delete.";
        $this->MsgConfirmSendPDF = "Do you wish to Send PDF of the selected entries?";
		$this->MsgNoSelectedSendPDF = "Please select the entries you wish to Send PDF.";
        $this->MsgCancelSendPDF = "Send PDF of selected entries cancelled.";
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->ipp = 100;
		$this->Pages = null;
	}
	
	//@JAPR 2016-10-15: Agregado el parámetro $anArrayOfStates para permitir filtrar por diferentes estados de mensajes (#L9T2IU)
	static function NewInstance($aRepository, $startDate = '', $endDate = '', $FilterDate = '', $GroupID = -1, $UserID = -1, $searchString = '', $ipp=100, $bGetValues = false, $anArrayOfStates = null)
	{
		$anInstance = new BITAMPushMsgCollection($aRepository, $UserID);
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $UserID;
		$svIpp = $ipp;

		$where = '';
		if(trim($FilterDate) != ""){
			switch ($FilterDate) {
				case 'T':
					$CurrentDate = date("Y-m-d");
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y-%m-%d')";
				break;
				case 'W':
					$CurrentDate = date("W");
					$CurrentYear = date("Y");
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%v')" . " AND " . $aRepository->DataADOConnection->Quote($CurrentYear) . " = DATE_FORMAT(A.CreationDateID, '%Y')";
				break;
				case 'M':
					$CurrentDate = date("Y-m");
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y-%m')";
				break;
				case 'Y':
					$CurrentDate = date("Y");
					$where.= " AND " . $aRepository->DataADOConnection->Quote($CurrentDate) . " = DATE_FORMAT(A.CreationDateID, '%Y')";
				break;
				default:
				break;
			}
		}		
		
		if(trim($startDate) != "" && trim($endDate) != "")
		{
			$endDate = $endDate . " 23:59:59";
			$where.= "AND A.CreationDateID >= " . $aRepository->DataADOConnection->DBTimeStamp($startDate) . " AND A.CreationDateID <= " . $aRepository->DataADOConnection->DBTimeStamp($endDate);
		}
					
		$sql = "SELECT A.MessageID, A.CreationDateID, A.CreationUserID, A.DestinationType, A.DestinationID, A.Title, A.PushMessage, A.State ";
		if($UserID !=-1 && $UserID !=0)
		{
			if($where !="")
			{
				$where.=" AND ";
			}
			/*$where.=" A.DestinationType = ".pushMsgUser." AND A.DestinationID = ".$UserID;*/			
			if (getMDVersion() >= esvPushMsgUserGroups) {
				if ($UserID != ""){
					$where.=" A.DestinationType = ".pushMsgUser. " AND (CASE WHEN A.DestinationID = 0 THEN A.MessageID IN (SELECT MessageID FROM SI_SV_PushMsgDet WHERE UserID IN ({$UserID})) ELSE ". (($UserID != "") ? " A.DestinationID IN ({$UserID})" : "") . " END)";		 					
				}
				else {
					$where.=" A.DestinationType = ".pushMsgUser;	
				}
			}
			else {
				$where.=" A.DestinationType = ".pushMsgUser. (($UserID != "") ? " AND A.DestinationID IN ({$UserID})" : "");
			}	
			
		}
		
		if($GroupID!=-1 && $GroupID!=0)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			/*$where.=" A.DestinationType = ".pushMsgGroup." AND A.DestinationID = ".$GroupID;*/			
			if (getMDVersion() >= esvPushMsgUserGroups) {
				if ($GroupID != "") {
					$where.=" A.DestinationType = ".pushMsgGroup. " AND (CASE WHEN A.DestinationID = 0 THEN A.MessageID IN (SELECT MessageID FROM SI_SV_PushMsgUserGroups WHERE UserGroupID IN ({$GroupID})) ELSE ". (($GroupID != "") ? " A.DestinationID IN ({$GroupID})" : ""). " END)";	
				}
				else {
					$where.=" A.DestinationType = ".pushMsgGroup;
				}
			}
			else{
				$where.=" A.DestinationType = ".pushMsgGroup.(($GroupID != "") ? " AND A.DestinationID IN ({$GroupID})" : "");
			}	
						
		}
		
		//@JAPR 2016-10-15: Agregado el parámetro $anArrayOfStates para permitir filtrar por diferentes estados de mensajes (#L9T2IU)
		if (is_null($anArrayOfStates)) {
			$anArrayOfStates = array();
		}
		elseif (!is_array($anArrayOfStates)) {
			$anArrayOfStates = array((int) $anArrayOfStates);
		}
			
		$filter = "";
		switch (count($anArrayOfStates))
		{
			case 0:
				break;
			case 1:
				$filter = " AND A.State = ".((int) $anArrayOfStates[0]);
				break;
			default:
				$filter = " AND A.State IN (".implode(', ', $anArrayOfStates).")";
				break;
		}
		
		if ($filter) {
			$where .= $filter;
		}
		//@JAPR
		
		$sql .= " FROM SI_SV_PushMsg A ";
						
		//Ligamos las Tablas para lo de Email
		if($where!="")
		{
			$sql .= " WHERE 1=1 ".$where;
		}
		$sql .= " ORDER BY A.CreationDateID DESC";  //@ears 2016-11-09 ordenamiento descendente para que se muestren primero las notificaciones mas recientes					
					
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n BITAMPushMsgCollection::NewInstance query: {$sql}");
		}
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
				
		$bFoundUser = false;
		$bFoundGroup = false;
		//@JAPR 2016-10-15: Movido este código para no estar cargando la colección múltiples veces, ya que no hay diferencia en la referencia asignada a cada objeto generado en este punto (#L9T2IU)
		//@JAPRWarning: No debería ser necesario asignar la referencia a estos objetos a nivel del objeto BITAMPushMsg, ya que sólo se utiliza para obtener el nombre del mensaje, en todo
		//caso lo correcto hubiera sido grabar un nombre de mensaje de manera automática o bien usar esta colección en este punto para asignarlo al generar la instancia
		$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
		$aGroupCollection = new BITAMUserGroupCollection($aRepository, null);
		//@JAPR
		while (!$aRS->EOF)
		{
			//@JAPR 2013-05-24: Validado que no trate de obtener todos los valores en la colección de reportes para ciertas cuentas (temporal)
			$aMessage = BITAMPushMsg::NewInstanceFromRS($aRepository, $aRS, $bGetValues, $aUserCollection->Collection, $aGroupCollection->Collection);
			//GCRUZ 2016-05-04. Buscar el searchString primero en los usuarios, y luego en los grupos
			if ($searchString != '')
			{
				if (getMDVersion() >= esvPushMsgUserGroups) {		
					if ($aMessage->DestinationID == 0){
						foreach ($aMessage->DestinationNameUserGroup as $objPushMsg) {
							if (strpos(@$objPushMsg['DestinationName'], $searchString) !== false)
							{
								if (@$objPushMsg['DestinationType'] == pushMsgUser && !$bFoundGroup)
								{
									$bFoundUser = true;
									$anInstance->Collection[] = $aMessage;
								}
								elseif (@$objPushMsg['DestinationType'] == pushMsgGroup && !$bFoundUser)
								{
									$bFoundGroup = true;
									$anInstance->Collection[] = $aMessage;
								}
							}
						}	
					}
					else {
						if (strpos($aMessage->DestinationName, $searchString) !== false)
						{
							if ($aMessage->DestinationType == pushMsgUser && !$bFoundGroup)
							{
								$bFoundUser = true;
								$anInstance->Collection[] = $aMessage;
							}
							elseif ($aMessage->DestinationType == pushMsgGroup && !$bFoundUser)
							{
								$bFoundGroup = true;
								$anInstance->Collection[] = $aMessage;
							}
						}																
					}	
				}
				else {							
					if (strpos($aMessage->DestinationName, $searchString) !== false)
					{
						if ($aMessage->DestinationType == pushMsgUser && !$bFoundGroup)
						{
							$bFoundUser = true;
							$anInstance->Collection[] = $aMessage;
						}
						elseif ($aMessage->DestinationType == pushMsgGroup && !$bFoundUser)
						{
							$bFoundGroup = true;
							$anInstance->Collection[] = $aMessage;
						}
					}										
				}	
			}
			else
				$anInstance->Collection[] = $aMessage;
			$aRS->MoveNext();
		}		
		
		if ($startDate!="")
		{ 
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($UserID!=-2)
		{
			$anInstance->UserID = $UserID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}
		
		return $anInstance;
	}

	static function NewInstanceToLog($aRepository, $searchString = "", $startDate = "", $endDate = "", $GroupID = "", $UserID = "",  $FilterDate = "")
	{
		$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
		$strScriptPath = $path_parts["dirname"];
		global $arrUsers; 
		global $arrUsersGroups; 
		
		if(trim($FilterDate) == "" && $startDate == "" && $endDate == ""){
			//EVEGA (issue:MR9XFH)Se definio que por default muestre lo del dia
			//El filtro de fecha por default NO se aplicará cuando se mande llamar la función desde	processDataDestination.php 
			//para el reproceso de destinos ya que se envia como parametro los IDs especificos que se desean procesar y no es necesario aplicar el filtro de fecha
			$FilterDate='T';
		}
		
		$aPushMsgCollection = BITAMPushMsgCollection::NewInstance($aRepository, $startDate, $endDate, $FilterDate, $GroupID, $UserID, $searchString);
		$aUserCollection = BITAMAppUserCollection::NewInstance($aRepository);
		$aUserGroupsColl = BITAMUserGroupCollection::NewUserGroupCollection($aRepository);
		
		/*
		if (trim($FilterDate) == "W") {
			print('pushmsgcollection');
			PrintMultiArray($aPushMsgCollection->Collection);
			die();
		}
		*/
		
		//@ears 2016-11-11 considerar solo usuarios con RegID valido 
		require_once('eSurveyServiceMod.inc.php');					
		$aBITAMConnection = connectToKPIRepository();
		$sql = "SELECT DISTINCT UserID, LastDateID, RegID FROM SI_SV_UserDevices WHERE IsActive = 1 AND DeviceID >0 AND RegID<>'' ORDER BY UserID, LastDateID DESC";
		$RSS = $aBITAMConnection->ADOConnection->Execute($sql);
		
		if ($RSS === false) 
		{
			die( translate("Error accessing")." SI_SV_UserDevices ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		while (!$RSS->EOF){
			$arrUsers[$i]['UserID'] = $RSS->fields["UserID"]; 			
			$i++;    
			$RSS->MoveNext();
		} 				
		
		//@ears 2016-11-11 usuarios de los grupos
		$sql="SELECT t1.CLA_ROL,t1.NOM_ROL,t3.CLA_ROL,t3.CLA_USUARIO,t2.NOM_LARGO 
		FROM {$aRepository->DataADOConnection->databaseName}.SI_ROL t1, {$aRepository->DataADOConnection->databaseName}.SI_ROL_USUARIO t3, 
			 {$aRepository->DataADOConnection->databaseName}.SI_USUARIO t2
		WHERE 	
			t1.CLA_ROL = t3.CLA_ROL AND
			t3.cla_usuario = t2.cla_usuario";			
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_ROL, SI_ROL_USUARIO, SI_USUARIO".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	  	
		$i = 0;
		while (!$aRS->EOF) {
			foreach($aUserGroupsColl as $aGroup)
			{	   					
				if ($aGroup->UserGroupID == @$aRS->fields['CLA_ROL']) {
					$arrUsersGroups[$i]['UserGroupID']=@$aRS->fields['CLA_ROL'];  
					$arrUsersGroups[$i]['NOM_ROL']=@$aRS->fields['NOM_ROL'];   			
					$arrUsersGroups[$i]['UserID']=@$aRS->fields['CLA_USUARIO'];   
					$arrUsersGroups[$i]['NOM_LARGO']=@$aRS->fields['NOM_LARGO'];   			
				}				
			}	
			$i++;
			$aRS->MoveNext();			
	    }  			
		
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
		<script src="js/codebase/dhtmlxFmtd.js"></script>
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<!-- Inician librerias de filtro de Ebavel>-->
		 <link type="text/css" rel="stylesheet" href="css/eBavel.css"/>
		
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery-ui-1.10.1.custom.min.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery.jsperanto.js"></script>
		<script type="text/javascript" src="js/eBavel/bitamApp.js"></script>
		<script type="text/javascript" src="js/eBavel/bitamstorage.js"></script>
		<script type="text/javascript" src="js/eBavel/databases.js"></script>
		<script type="text/javascript" src="js/eBavel/section.class.js"></script>
		<script type="text/javascript" src="js/eBavel/jquery.widgets.js"></script>
		<script type="text/javascript" src="js/eBavel/date.js"></script>
		<script type="text/javascript" src="js/jsondefinition/bitamAppFormsJsonDefinition.js"></script>
		<script type="text/javascript" src="js/jsondefinition/agendasJsonDefinition.js"></script>
		<script type="text/javascript" src="js/jquery.widgets.js"></script>
		<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
	
		<!-- Termina librerias de filtro de Ebavel style="height: 100%; margin: 0px;" -->
		</head>

		<body style="width: 100%; height: 100%; margin: 0px; overflow: hidden;" onload="doOnload()">
		<div id="boxFilter" style="float:left; width: 200px; height: 100%; overflow-y: scroll; overflow-x: hidden; margin-top: 5px; background:white;border-right: 1px solid lightgray"></div>
		<style type="text/css">
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				text-transform: none !important;
			}
			/*div.dhx_toolbar_dhx_terrace div.dhxtoolbar_float_left {
  				float: right !important;
			}*/
			/* Estilos para ocultar el icono de las ventanas modales (todas las skins utilizadas) */
			.dhxwins_vp_dhx_skyblue div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			.dhxwins_vp_dhx_web div.dhxwin_hdr div.dhxwin_icon.without_icon {
				display: none;
			}
			/* Estilo para los botones de búsqueda asociados a grids */
			.search_button input {
				border:1px solid #e1e1e1 !important;
			}
			.selection_container div.dhxform_container {
				border:1px solid #e1e1e1 !important;
				//height:90% !important;
				////overflow-y:auto;
			}
			.selection_container div.objbox {
				//overflow-y:auto;
			}
			.selection_list fieldset.dhxform_fs {
				border:0px solid #e1e1e1 !important;
				//height:95% !important;
			}
			div.dhxform_item_label_left.assoc_button div.dhxform_btn_txt {
				background-image:url(images/add_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			div.dhxform_item_label_left.remove_button div.dhxform_btn_txt {
				background-image:url(images/del_field.png);
				background-repeat:no-repeat;
				background-position: 0px 3px;
				padding-left: 22px;
				margin: 0px 15px 0px 12px;
			}
			/* it's important to set width/height to 100% for full-screen init */
			html, body {
				width: 100%;
				height: 100%;
				margin: 0px;
				overflow: hidden;
			}
			span.tabFormsOpts {
				font-size:12px;
			}
			div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
				 text-transform: none !important;
				 padding-left: 20px;
			}
			div.gridbox_dhx_terrace.gridbox table.obj tr td {
				padding-left: 20px;
			}
			/*
			
			
			/* Estilos para ocultar las tabs pero permitir ver sus celdas */
			.hiddenTabs div.dhx_cell_tabbar {
				position:none !important;	//Este estilo no existe, pero efectivamente bloquea el aplicado a la clase que sobreescribe
			}
			.hiddenTabs div.dhxtabbar_tabs {
				display:none !important;
			}
			.hiddenTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			.visibleTabs div.dhx_cell_tabbar {
				position:absolute !important;	//Utilizado para pintar tabs que estarían dentro de la tab principal oculta
			}
			.visibleTabs div.dhxtabbar_tabs {
				display:inline !important;
			}
			.visibleTabs div.dhx_cell_cont_tabbar {
				border: 0px !important;
			}
			/* Estilo para los enlaces de las opciones principales (tabs ocultos) de la ventana de diseño */
			.linkToolbar {
				background-color: #264B83;
			}
			.linktd {
				cursor: pointer;
				font-family: Roboto Regular;
				font-size: 13px;
				//font-weight: bold;
				//text-align: center;
				//text-decoration: underline;
				color: white;	//#333333;
				padding-left: 24px;
				padding-right: 24px;
				//width: 150px;
				white-space: nowrap;
			}
			.linktdSelected {
				text-decoration: underline;
			}
			/* Estilo para mantener seleccionado el botón de selección de la toolbar de preguntas */
			//Finalmente no sirvió esto, porque el evento MouseOver del list option cambia de clase eliminando la anterior aplicada, así que se perdía
			//la clase definida en este archivo, se optó por cambiar directamente el estilo
			.tbOptionSelected {
				  background-color: #fff3a1 !important;
			}
			/* Agrega el border a las celdas del grid */
			table.obj tr td {
				//border-left-width: 1px !important;
				border: 0px !important;
				padding-left: 20px !important;
				padding-right: 0px !important;
			}
			.dhx_textarea {
				margin-left: 0px;
			}
			/* Estilos para los subgrids de propiedades de las tabs de colecciones */
			.dhx_sub_row {
				//overflow: visible !important;
				border: 1px solid #808080 !important;
			}
			.dhx_sub_row table.obj tr td {
				border-left-width:1px solid #808080 !important;
			}
			.dhxdataview_placeholder div.dhx_dataview_item {
				border: 0px !important;
			}
			/* Estilos de los Layouts */
			.dhx_cell_hdr {
				background-color: #f5c862 !important;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>

		<script language="JavaScript">
		var Content;
		var ContentA;
		var ContentB;
		var objLogGrid;
		//Diálogos
		var objWindows;
		var objDialog;

		var Users = [];
		var Groups = [];
	
		//@ears 2016-11-11 considerar solo usuarios y gpos de usuarios con RegID valido 
		var UserGroupRegID = [];
		var GroupRegID = [];
<?
		foreach($arrUsers as $objUsers) {
?>
			UserGroupRegID.push("<?=@$objUsers['UserID']?>");
<?
		}
		
		foreach($arrUsersGroups as $objUsersGroups){
			echo("\t\t\t"."GroupRegID.push({usergroupid:{$objUsersGroups['UserGroupID']}, groupname:'".htmlspecialchars($objUsersGroups['NOM_ROL'], ENT_QUOTES)."', userid:{$objUsersGroups['UserID']}, username:'".htmlspecialchars($objUsersGroups['NOM_LARGO'], ENT_QUOTES)."'});\r\n");
		}
			
		//para usuarios		
		foreach($aUserCollection as $aUser)
		{			
?>
			var aUserName = ("<?=$aUser->UserName?>");
			for(i=0; i<UserGroupRegID.length; i++) {
				if (UserGroupRegID[i] == aUserName){
					<?//JAPR 2017-09-29: Corregido un bug, se estaban cargando los usuarios por medio del UserID, siendo que debía ser el CLA_USUARIO (#4WSLS1)?>
					<?echo("\t\t\t"."Users.push({value:{$aUser->CLA_USUARIO}, text:'".htmlspecialchars($aUser->UserName, ENT_QUOTES)."'});\r\n");?>		
					<?//@JAPR?>
				}
			}
<?
		}	
?>		

		//para grupos de usuarios		
		for(j=0; j<GroupRegID.length; j++){
			var blnGp = false;
			for(x=0; x<Groups.length; x++){
				if (GroupRegID[j].groupname == Groups[x].text){		
					blnGp = true;
					break;
				}	
			}
			
			if (!blnGp){	
				var blnUser = false;
				for(i=0; i<UserGroupRegID.length; i++) {
					if (GroupRegID[j].username == UserGroupRegID[i]){		
						blnGp = true;
						break;
					}
				}	
				if (blnGp){
					Groups.push({value:GroupRegID[j].usergroupid, text:GroupRegID[j].groupname});				
				}	
			}
		}
 
		console.log(Groups);
		console.log(Users);
		
		delete Array.prototype.contains;
		bitamApp.getAppFormsDefinition();
		$.extend($, { t: function (word) {
				return $.jsperanto.translate(word);
			}
		});

		function doOnload () {
			var FilterDate = ("<?= (isset($_POST['FilterDate']) ? $_POST['FilterDate'] : "") ?>");
			if (FilterDate == "T" || FilterDate == "W" || FilterDate == "M" || FilterDate == "Y") {
				var Btns = $(".dhxtoolbar_text");
				for(var i = 0; i < Btns.length; i++){
					if(Btns[i].innerText == Toolbar.getItemText(FilterDate)){
						$(".dhxtoolbar_text").parent()[i].setAttribute("class", "dhx_toolbar_btn dhxtoolbar_btn_over");
						break;
					}
				}
			}
		}

		function applyFilterAttrib (filter, oldTags, descrTag, allForms)
		{
			if (filter) {
				
				for (var fkey in filter) {

					//Search for Input String
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}

					//Search for Date
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
						//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
						//Modificada la validación, ya que cuando se presionaba el botón de Search sin texto escrito, el filter venía con un elemento Date que era un array cuyo índice
						//0 era undefined, lo cual es correcto en el contexto normal de la ventana si se está limpiando el filtro de fecha con el botón para tal fin, pero eso implicaba
						//que debió haber existido una fecha previamente, en cuyo caso los botones de filtrado automático de fechas no habrían estado asignados, pero con este cambio
						//para realizar un refresh después de eliminar capturas, se invoca al click del filtro de fecha (independientemente de si había o no una) y en ese caso pudiera si
						//haber botones de filtro de fecha automáticos que habría que respetar, así que se validarán si no había fecha asignada en el filtro
						else {
							Toolbar.forEachItem(function(itemId){
								if(Toolbar.getItemState(itemId)){
									frmAttribFilter.FilterDate.value = itemId;
								}
							});
						}												
						//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
						//Modificada la validación, ya que cuando se presionaba el botón de Search sin texto escrito, el filter venía con un elemento Date que era un array cuyo índice
						//0 era undefined, lo cual es correcto en el contexto normal de la ventana si se está limpiando el filtro de fecha con el botón para tal fin, pero eso implicaba
						//que debió haber existido una fecha previamente, en cuyo caso los botones de filtrado automático de fechas no habrían estado asignados, pero con este cambio
						//para realizar un refresh después de eliminar capturas, se invoca al click del filtro de fecha (independientemente de si había o no una) y en ese caso pudiera si
						//haber botones de filtro de fecha automáticos que habría que respetar, así que se validarán si no había fecha asignada en el filtro																
					}else if(!filter['Date'] || ($.isArray(filter['Date']) && !filter['Date'][0])){ //if(!filter['Date']){											
						//EVEGA Buscamos en si uno de los botones de periodo estan activados
						Toolbar.forEachItem(function(itemId){
						    if(Toolbar.getItemState(itemId)){
						    	frmAttribFilter.FilterDate.value = itemId;
						    }
						});
					}
					//Search for Group of Users
					if (fkey == 'Groups') {
						var filterGroups = filter['Groups'];
						var Groups = new Array();
						for (var ukey in filterGroups) {
							Groups.push(filterGroups[ukey]);
						}
						frmAttribFilter.GroupID.value = Groups.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldGroupsTags.value = oldTags;
					}	
					//Search for Users
					if (fkey == 'Users') {
						var filterUsers = filter['Users'];
						var Users  = new Array();
						for (var ukey in filterUsers) {
							Users.push(filterUsers[ukey]);
						}
						frmAttribFilter.userID.value = Users.join(',');
						if(descrTag == 'Groups')
							frmAttribFilter.oldUserTags.value = oldTags;
					}
				}
			} 
			else {
				/*var objCatalogID = document.getElementById("CatalogID");
				frmAttribFilter.catalogID.value = objCatalogID.value;*/

				var objStartDate = document.getElementById("CaptureStartDate");
				frmAttribFilter.startDate.value = objStartDate.value;

				var objEndDate = document.getElementById("CaptureEndDate");
				frmAttribFilter.endDate.value = objEndDate.value;
				
				var objUserID = document.getElementById("UserID");
				frmAttribFilter.userID.value = objUserID.value;
				
				var objIpp = document.getElementById("ipp");
				frmAttribFilter.ipp.value = objIpp.value;
			}

			frmAttribFilter.submit();
		}
		
<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//@JAPR 2016-08-24: Implementada la eliminación múltiple de reportes
?>
		//Si se encontraba en estado de selección, realiza el borrado de los elementos seleccionados, en caso contrario inicia el estado de selección de elementos
		function doRemoveItems() {
			if (!Toolbar || !objLogGrid) {
				return;
			}
			
			//Si aún no se había presionado el botón de eliminar, entonces cambia la imagen y deshabilita el click en el Grid para permitir selección múltiple
			if (!Toolbar.bitRemoveState) {
				objRowsSelected = new Object();
				var selRows = objLogGrid.getSelectedRowId();
				if (selRows) {
					var arrSelRows = selRows.split(",");
					for (iRow in arrSelRows) {
						var selRow = arrSelRows[iRow];
						if (selRow) {
							objRowsSelected[selRow] = 1;
						}
					}
				}
				
				Toolbar.bitRemoveState = 1;
				//@ears 2016-10-07 solo en versiones anteriores seguirá apareciendo texto
				//como no se permitirá eliminación de notificaciones solo se conserva el texto como Editar
				//solo para mantener el estadar de uso para la selección múltiple de registros
				if (eFormsMDVersion < 6.03) {
					Toolbar.setItemText('remove', '<?= translate("Remove") ?>');
					//Toolbar.setItemText('edit', '<?= translate("Edit") ?>');
				}
				else {
					Toolbar.setItemToolTip("remove","<?= translate("Remove") ?>");
					//Toolbar.setItemToolTip("edit","<?= translate("Edit") ?>");
				}	
				Toolbar.setItemImage('remove', 'remove.png');
				//Toolbar.setItemImage('edit', 'edit.png');
				Toolbar.showItem('removeCancel');
				objLogGrid.enableMultiselect(true);
				return;
			}
			
			//Los reportes no tienen la referencia directa del ID, así que se debe enviar la llave que contiene la combinación Forma + Usuario + Fecha-Hora para que se use al eliminar
			var selRows = objLogGrid.getSelectedRowId();
			if (selRows == null) {
				return;
			}
			
			var arrSelRows = selRows.split(",");
			for (iRow in arrSelRows) {
				var selRow = arrSelRows[iRow];
				if (selRow) {
					selRow += "_" + objLogGrid.getRowAttribute(selRow, "UserID");
					arrSelRows[iRow] = selRow;
				}
			}

			/*@ears 2016-10-10 comentadas las siguientes líneas no se debe permitir eliminar notificaciones*/			
			/*
			var blnAnswer = confirm("<?=sprintf(translate("Do you want to remove all selected %s?"), strtolower(translate("Entries")))?>");
			if (!blnAnswer) {
				return;
			}
			
			var objParams = {
				RequestType: <?=reqAjax?>,
				Process: "Delete",
				ObjectType: <?=otyPushMsg?>,
				ReportRowID: arrSelRows
			};
			*/
			
			/*@ears 2016-10-10 comentada doSendDataAjax no debe permitir eliminar notificaciones, las líneas de código que estaban activas son las que tienen 3 slash*/
			///doSendDataAjax(objParams, function(oResponse) {
				//Invoca al widget de boxfilter para forzar al refresh de la ventana utilizando los mismos valores seleccionados tanto en el filtro como en los botones de fechas
			///	fnRefreshPage();
				//objReportLayout.cells(dhxGridDataCell).progressOff();
				//Cierra el diálogo de este reporte al terminar el borrado
				//setTimeout(function () {
					//Refresca la ventana de reportes aplicando los mismos filtros que actualmente tenía seleccionados
					//Elimina la row de este reporte
					/*if (parent.objLogGrid && parent.objLogGrid.deleteRow && '') {
						parent.objLogGrid.deleteRow('');
					}
					
					if (parent.doUnloadDialog) {
						parent.doUnloadDialog();
					}*/
				//}, 100);
			///});
		}
		
		//Cancela el estado de selección de elementos
		function doCancelRemoveItems() {
			if (!Toolbar || !objLogGrid) {
				return;
			}
			
			Toolbar.bitRemoveState = 0;
			objLogGrid.clearSelection();
			objLogGrid.enableMultiselect(false);
			//@ears 2016-10-07 solo en versiones anteriores seguirá apareciendo texto
			if (eFormsMDVersion < 6.03) {
				Toolbar.setItemText('remove', '<?= translate("Edit") ?>');
			}
			else {
				Toolbar.setItemToolTip("remove","<?= translate("Edit") ?>");
			}							
			Toolbar.setItemImage('remove', 'edit.png');
			Toolbar.hideItem('removeCancel');
		}
		
		function doSendDataAjax(oFields, oCallbackFn, bRemoveProgress) {
			console.log("doSendDataAjax");
			
			if (bRemoveProgress === undefined) {
				bRemoveProgress = true;				
			}
			
			//Prepara los parámetros con los datos a grabar
			var objFieldsData = oFields;
			if (!objFieldsData || !$.isPlainObject(objFieldsData) || $.isEmptyObject(objFieldsData)) {
				return;
			}
			
			var objParams = $.extend({}, objFieldsData);
			if (!objParams) {
				return;
			}
			
			var strParams = '';
			var strAnd = '';
			for (var strProp in objParams) {
				//JAPR 2016-08-24: Agregado el soporte para parámetros array
				var objParamArray = objParams[strProp];
				if ($.isArray(objParamArray)) {
					for (var intIndex in objParamArray) {
						strParams += strAnd + strProp + "["+intIndex+"]=" + encodeURIComponent(objParamArray[intIndex]);
						strAnd = '&';
					}
				}
				else {
					strParams += strAnd + strProp + "=" + encodeURIComponent(objParams[strProp]);
					strAnd = '&';
				}
			}
			
			//objReportLayout.cells(dhxGridDataCell).progressOn();
			window.dhx4.ajax.post("processRequest.php", strParams, function(loader) {
				doSaveConfirmation(loader, oCallbackFn, bRemoveProgress);
			});
		}		
				
		/* Procesa la respuesta de una invocación al servidor */
		function doSaveConfirmation(loader, oCallbackFn, bRemoveProgress) {
			console.log('doSaveConfirmation');
			
			if (!loader || !loader.xmlDoc) {
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			if (loader.xmlDoc && loader.xmlDoc.status != 200) {
				alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			//Si llega a este punto es que se recibió una respuesta correctamente
			var response = loader.xmlDoc.responseText;
			try {
				var objResponse = JSON.parse(response);
			} catch(e) {
				alert(e + "\r\n" + response.substr(0, 1000));
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			
			if (objResponse.error) {
				alert(objResponse.error.desc);
				if (bRemoveProgress) {
					//objReportLayout.cells(dhxGridDataCell).progressOff();
				}
				return;
			}
			else {
				if (objResponse.warning) {
					console.log(objResponse.warning.desc);
				}
			}
			
			//JAPR 2015-07-17: Agregado el borrado de objetos directamente desde Ajax, los cuales no regresan código desde el server sino sólo una referencia para que se
			//pueda ejecutar un código de callback
			if (objResponse.objectType) {
				if (objResponse.deleted) {
					if (oCallbackFn && typeof oCallbackFn == "function") {
						oCallbackFn(objResponse);
					}
					else {
						if (bRemoveProgress) {
							//objReportLayout.cells(dhxGridDataCell).progressOff();
						}
					}
				}
				return;
			}
			else {
				if (objResponse.newObject) {
					if (oCallbackFn && typeof oCallbackFn == "function") {
						oCallbackFn(objResponse.newObject);
					}
				}
				return;
			}
			
			if (bRemoveProgress) {
				//objReportLayout.cells(dhxGridDataCell).progressOff();
			}
		}
				
		function addDescriptionTag (tagName, tagDescription, element) {
			var realTagName = tagName;
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = 1;
			} 
			else
				theTag.elements[elementID].num = (theTag.elements[elementID].num+1);
		}
		
		function addDescriptionTagByCount (tagName, tagDescription, element) {
			var realTagName = tagName;
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = element.count;
			}
		}

		var descriptionTags = {};
		
		var searchString = '';
		<? if (isset($_POST["searchString"]) && $_POST["searchString"] != '') { ?>
			searchString = '<?= $_POST["searchString"] ?>';
		<? } ?>
		
		var filterDate = '';
		<? if ((isset($_POST["startDate"]) && $_POST["startDate"] != '') && (isset($_POST["endDate"]) && $_POST["endDate"] != '')) { ?>
			filterDate = '<?= $_POST["startDate"] ?>@<?= $_POST["endDate"] ?>';
		<? } 
		foreach ($aPushMsgCollection->Collection as $aMessage)
		{
			//@ears 2016-12-26 verificar versión 
			//ears 2016-12-26 detalle de grupos de usuario o usuarios
			if (getMDVersion() >= esvPushMsgUserGroups) {
				foreach ($aMessage->DestinationNameUserGroup as $objPushMsg) {
					if (@$objPushMsg['DestinationType'] == pushMsgUser)
					{
		?>
						addDescriptionTag('Users','Users' , {id: "<?= (int) @$objPushMsg['UserGroupID'] ?>",   name:"<?= @$objPushMsg['DestinationName'] ?>" });
		<?
					}
					elseif (@$objPushMsg['DestinationType'] == pushMsgGroup)
					{
		?>
						addDescriptionTag('Groups','Groups',{id: <?= (int) @$objPushMsg['UserGroupID'] ?>,  name:"<?= (isset($objPushMsg['DestinationName']) && @$objPushMsg['DestinationName']!='')? @$objPushMsg['DestinationName']:"(".translate('None').")" ?>"});
		<?				
					}			
				}		
			}
			else {
				if ($aMessage->DestinationType == pushMsgUser)
				{
	?>
					addDescriptionTag('Users','Users' , {id: "<?= $aMessage->DestinationID ?>",   name:"<?= $aMessage->DestinationName ?>" });
	<?
				}
				elseif ($aMessage->DestinationType == pushMsgGroup)
				{
	?>
					addDescriptionTag('Groups','Groups',{id: <?= $aMessage->DestinationID ?>,  name:"<?= (isset($aMessage->DestinationName) && $aMessage->DestinationName!='')? $aMessage->DestinationName:"(".translate('None').")" ?>"});
	<?				
				}	
			}
		}
?>
		var oldGroupsTags = JSON.parse(<?= json_encode((isset($_POST['oldGroupsTags']) && $_POST['oldGroupsTags'] != '') ? $_POST['oldGroupsTags'] : '{}') ?>);
		var oldUserTags = JSON.parse(<?= json_encode((isset($_POST['oldUserTags']) && $_POST['oldUserTags'] != '') ? $_POST['oldUserTags'] : '{}') ?>);
		
		var checkedElemenTags = {};

		/*@AAL 24/04/2015: agregado para identificar si se trata de una agenda o AgendaScheduler y mostrar el filtro fecha,
		la cual solo debe de mostrarse en agendas, la validación se muestra em el archivo jquery.widgets.js*/
		//GCRUZ 2016-05-04. Esta opción debe de existir siempre e ir en true si se quiere mostrar el filtro de fecha en el boxfilter
		var isAgenda = true;

		function initBoxFilter () {
			var checkedValues = ("<?= (isset($_POST['GroupID']) ? $_POST['GroupID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Groups'] = checkedValues.split(',');
				$.each(checkedElemenTags['Groups'], function (index, value) {
					value = ('' + value);
				});
			}

			var checkedValues = ("<?= (isset($_POST['userID']) ? $_POST['userID'] : "") ?>");
			if (checkedValues) {
				checkedElemenTags['Users'] = checkedValues.split(',');
				$.each(checkedElemenTags['Users'], function (index, value) {
					value = ('' + value);
				});
			}

			if (!$.isEmptyObject(oldGroupsTags) && descriptionTags['Groups'] && descriptionTags['Groups'].elements) {
				$.extend(descriptionTags['Groups'].elements, oldGroupsTags);
			}
			if (!$.isEmptyObject(oldUserTags) && descriptionTags['Users'] && descriptionTags['Users'].elements) {
				//GCRUZ 2015-09-08. Duplicidad de grupos en bitácora. Issue: 75O07A
				//$.extend(descriptionTags['Users'].elements, oldUserTags);
			}

			//GCRUZ 2015-10-15. Indicar un orden específico a los elementos del filtro
			for (var dkey in descriptionTags)
			{
				if (!descriptionTags[dkey].order) {
					descriptionTags[dkey].order = [];
					var thisTagOrder = {};
					for (var elkey in descriptionTags[dkey].elements)
					{
						thisTagOrder[descriptionTags[dkey].elements[elkey].id] = elkey;
					}
					var arrTagsOrder = Object.keys(thisTagOrder);
					arrTagsOrder.sort();
					var i=0;
					for (var descOrder in arrTagsOrder)
					{
						descriptionTags[dkey].order[i] = thisTagOrder[arrTagsOrder[descOrder]];
						i++;
					}
				}
			}

			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate
			};
			//console.log(options);
			
			//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
			//JAPR 2016-08-25: Dado a que se necesita permitir invocar al submit de este widget (submitFilter) pero desde fuera, y el submit lo que hace es invocar a una función
			//de fuera que se pasa como parámetro o aquí mismo se obtiene su referencia global (applyFilterAttrib), a la cual le manda como primeros 3 parámetros el conjunto de
			//elementos marcados del Widget, para no repetir el código que analiza la selección del Widget, se verificará si en options se recibió una función "setRefreshFn" la cual
			//recibirá como parámetro precisamente a la función submitFilter para permitir invocarla desde fuera, ya sea especificando un objeto que simule al event de uno de los
			//componentes del widget, o no mandando nada con lo cual se simulará que se presionó el botón de Search, y el único uso de esta función será simular un refresh completo
			//de la ventana tal como lo haría el widget con su estado actual
			options.setRefreshFn = function(oWidgetSubmitFn) {
				if (oWidgetSubmitFn) {
					fnRefreshPage = function() {
						oWidgetSubmitFn();
					}
				}
			};
			//JAPR
						
			if ($.isEmptyObject($('#boxFilter').data())){
				$('#boxFilter').boxfilter(options);
				if(options.filterDate != '')
					$('.tagsFecha .clear.ico').show();
			}
			else 
				$('#boxFilter').boxfilter('update');
		}
		
		try { 
			initBoxFilter(); 
		} catch (e) { 
			alert('Search filter error: '+e);
		}

		var Content = new dhtmlXLayoutObject({
	            parent: document.body,
    			pattern: "2U"
	        });
		var toolBarItems=[];
		var eFormsMDVersion = <?=getMDVersion()?>;		
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>
		//@ears 2016-10-07 solo para versiones anteriores seguirá apareciendo el texto en los botones
		if (eFormsMDVersion < 6.03) {	
			toolBarItems.push({id: "T", type: "buttonTwoState", text: "<?=translate('Today')?>", img: "date_day.png"});
			toolBarItems.push({id: "W", type: "buttonTwoState", text: "<?=translate('This Week')?>", img: "date_week.png"});
			toolBarItems.push({id: "M", type: "buttonTwoState", text: "<?=translate('This Month')?>", img: "date_month.png"});
			toolBarItems.push({id: "Y", type: "buttonTwoState", text: "<?=translate('This Year')?>", img: "date_year.png"});
			toolBarItems.push({id: "remove", type: "button", text: "<?=translate('Edit')?>", img: "edit.png"});
			toolBarItems.push({id: "removeCancel", type: "button", text: "<?=translate('Cancel')?>", img: "edit.png"});		
			//@ears 2016-10-04 
			toolBarItems.push({id: "NA", type: "button", text: "<?=translate('Button')?>", img: ""});
			toolBarItems.push({id: "NW", type: "button", text: "<?=translate('Push')?>", img: "notification16x16.png"});
		}
		else {
			toolBarItems.push({id: "T", type: "buttonTwoState", img: "date_day.png"});
			toolBarItems.push({id: "W", type: "buttonTwoState", img: "date_week.png"});
			toolBarItems.push({id: "M", type: "buttonTwoState", img: "date_month.png"});
			toolBarItems.push({id: "Y", type: "buttonTwoState", img: "date_year.png"});
			toolBarItems.push({id: "remove", type: "button", img: "edit.png"});
			toolBarItems.push({id: "removeCancel", type: "button", img: "edit.png"});		
			//@ears 2016-10-04 
			toolBarItems.push({id: "NA", type: "button", img: ""});
			toolBarItems.push({id: "NW", type: "button", img: "notification16x16.png"});			
		}
			
		Toolbar = Content.attachToolbar({
			icons_path: "images/",
			items:toolBarItems
		});
		
		//EVEGA verificamos si se esta filtrando por tiempo seteamos el buttonTwoState en true
		<?if(trim($FilterDate) != ""){?>
		Toolbar.setItemState("<?=$FilterDate?>", true);
		<?}?>		
		
		if (eFormsMDVersion >= 6.03) {	
			Toolbar.setItemToolTip("T", "<?=translate('Today')?>");
			Toolbar.setItemToolTip("W", "<?=translate('This Week')?>");
			Toolbar.setItemToolTip("M", "<?=translate('This Month')?>");
			Toolbar.setItemToolTip("Y", "<?=translate('This Year')?>");
			Toolbar.setItemToolTip("remove", "<?=translate('Edit')?>");
			Toolbar.setItemToolTip("removeCancel", "<?=translate('Cancel')?>");
			Toolbar.setItemToolTip("NW", "<?=translate('Push')?>");
		}
		
		//Divide los botones del toolbar
		<?
		//@JAPR 2016-08-24: Rediseñada la toolbar para utilizar únicamente botones con imagenes sin texto
		//Ya no se tiene el dato de quien programó ni quien pidió que la toolbar estuviera sólo sobre el área del grid, sin embargo debido a la gran cantidad de opciones que
		//se han agregado, se decidió (LRoux y CGil) cambiar para usar sólo imagenes y que todo estuviera alineado hasta la izquierda, así que se removerán los trucos usados para
		//dar el efecto anterior
		?>
		/*Toolbar.addSpacer('NW');
		
		//Ajusta los botones de filtrado por fecha
		$( ".dhxtoolbar_float_right" ).attr("style", "position: absolute; margin: 0px 210px 0px;");
		*/
		Toolbar.addSpacer('NA');
		Toolbar.hideItem('NA');
		Toolbar.hideItem('removeCancel');
		//@JAPR
		
		//EVEGA Se agrego el escucha del evento para los buttonTwoState (filtros de tiempo)
		Toolbar.attachEvent("onStateChange", function(id) {
			Toolbar.forEachItem(function(itemId){
			    // your code here
			    if(id!=itemId && Toolbar.getItemState(itemId)){
			    	Toolbar.setItemState(itemId, false);
			    }
			});

			FilterToDate(id);
		});
		
		Toolbar.attachEvent("onClick", function(id) {						
			switch (id) {
				case 'NW':     //UeserID = Email
					newPushMsg();
					break;
				//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
				case "remove":
				{
					doRemoveItems();
					break;
				}
				case "removeCancel":
				{
					doCancelRemoveItems();
					break;
				}
				//JAPR								
				default:
					break;
			}
		});
		
		//Add Style to Label Forms Log
		//$( ".dhx_toolbar_text" ).attr("style", "color: #285768; font: bold 20px arial,sans-serif;");

		$("#Search").attr("placeholder", "<?=translate('Search')?>");
		$("li.titleFecha").text("<?=translate('Date')?>");
		$("#CustomDateRange").text("<?=translate('Custom date range')?>");
		$("#From").text("<?=translate('From')?>");
		$("#To").text("<?=translate('To')?>");

		$("li.titleGroups").text("<?=translate('Groups')?>");
		$("li.titleUsers").text("<?=translate('Users')?>");
		$("li.titleForms").text("<?=translate('Forms')?>");
		
		var BoxFilter = Content.cells("a").attachLayout("1C");
		BoxFilter.cells("a").hideHeader();
		BoxFilter.cells("a").setWidth(200);
		Content.cells("a").fixSize(true, true); //Bloque el Rezise del Content a);
		BoxFilter.cells("a").attachObject("boxFilter");
		
		//JAPR 2015-08-08: Corregido un bug, el diálogo para mostrar un reporte utilizaba una variable con el mismo nombre que esta para el grid, así que se sobrescribía la instancia
		//y causaba que al ver un reporte, la lista de logs ya no funcionara correctamente y no pudiera mostrar los reportes siguientes sino una ventana en blanco
        var objLogGrid = Content.cells("b").attachGrid();
        Content.cells("b").hideHeader();
        Content.cells("b").fixSize(true, true); //Bloque el Rezise del Content b)
        
		//ears 2016-10-27 ya no deben aparecer las columnas de Destino y Usuario
		/*
		objLogGrid.setHeader("<?=translate('Date')?>,<?=translate('Title')?>,<?=translate('Destination')?>,<?=translate('User/Group')?>, <?=translate('Message')?>, <?=translate('Status')?>",null,["text-align:left;","text-align:left;", "text-align:left;", "text-align:left;", "text-align:left", "text-align:left"]);		
		objLogGrid.setColAlign("left,left,left,left,left,left");
		objLogGrid.setColTypes("ro,ro,ro,ro,ro,ro");
		objLogGrid.setColSorting("date,str,str,str,str,str");
		objLogGrid.setInitWidths("150,100,100,*,*,100");
		objLogGrid.enableResizing("false,false,false,false,false,false");
		*/				
		//objLogGrid.setHeader("<?=translate('Date')?>,<?=translate('Title')?>, <?=translate('Message')?>, <?=translate('Status')?>",null,["text-align:left;","text-align:left;", "text-align:left;", "text-align:left"]);		
		objLogGrid.setHeader("<?=translate('Date')?>, <?=translate('Status')?>, <?=translate('Title')?>, <?=translate('Message')?>",null,["text-align:left;","text-align:left;", "text-align:left;", "text-align:left"]);
		objLogGrid.setColAlign("left,left,left,left");
		objLogGrid.setColTypes("ro,ro,ro,ro");
		objLogGrid.setColSorting("date,str,str,str");
		objLogGrid.setInitWidths("150,120,400,500");
		objLogGrid.enableResizing("false,false,false,false");
		
		//objLogGrid.enableAutoHeight(true);
		//objLogGrid.enableAutoWidth(true);
		//objDialog.denyResize();
		//objLogGrid.objBox.style.overflowX = "scroll";
        //objLogGrid.objBox.style.overflowY = "auto";
		objLogGrid.setStyle("background-color:rgb(254,204,98);font-family:Arial;font-weight:bold;font-size:10pt;", "background-color:rgb(240,240,240);color:rgb(51,51,51);font-family:Arial;font-size:9pt;", "", "background-color:rgb(60,102,136);color:rgb(240,240,240);font-family:Arial;font-size:9pt;");		
		//objLogGrid.attachEvent("onRowSelect",OnRowSelected);
		objLogGrid.attachEvent("onBeforeSelect", OnBeforeSelected);
		objLogGrid.attachEvent("onRowSelect", OnRowSelected);		
		objLogGrid.init();
		
		
<?
		foreach($aPushMsgCollection->Collection as $aMessage)
		{
			$RowID = $aMessage->MessageID;
			//@JAPR 2016-09-01: Corregido un bug, al haber un <enter> como parte de los mensajes de push, se rompía la cadena del código y generaba un error de script (#WJUKIP)
			//OMMC 2016-10-03: Agregado el escape de comillas para los textos
?>
			var rowValues = [];
			rowValues.push("<?=$aMessage->CreationDateID?>");
			rowValues.push("<?=$aMessage->NomState?>");
			/*
<?
			//ears 2016-10-27 ya no deben aparecer las columnas de Destino y Usuario			
			if ($aMessage->DestinationType == pushMsgUser)
			{				
?>
				rowValues.push("<?=translate('User')?>");
<?
			
			}
			elseif ($aMessage->DestinationType == pushMsgGroup)
			{
				
?>
				rowValues.push("<?=translate('Group')?>");
<?
			
			}
			else
			{
			
?>
				rowValues.push("<?=translate('Unrecognized type')?>");
<?
			
			}
			
?>
			*/
			//@JAPR 2016-09-01: Corregido un bug, al haber un <enter> como parte de los mensajes de push, se rompía la cadena del código y generaba un error de script (#WJUKIP)
			//OMMC 2016-10-03: Agregado el escape de comillas para los textos			
			//rowValues.push("<?=$aMessage->DestinationName?>");
			rowValues.push("<?=str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($aMessage->Title))?>");
			rowValues.push("<?=str_ireplace(array("\r\n", "\r", "\n"), "<br>", addslashes($aMessage->PushMessage))?>");
			//rowValues.push("<?=$aMessage->State?>");
			var strRowID = "<?= $RowID . '_' . $aMessage->MessageID ?>";
			//JAPR 2015-08-08: Corregido un bug, no estaba cargando la instancia de reporte correcto pues no estaba considerando al usuario (#4PIE8N)
			objLogGrid.addRow(strRowID, rowValues);
			objLogGrid.setRowAttribute(strRowID, "UserID", <?=(int) @$aMessage->UserID?>);
<?
		}
?>
		
		
		//Descarga la instancia del diálogo abierto
		function doUnloadDialog() {
			console.log('doUnloadDialog');
			if (!objWindows || !objDialog) {
				return;
			}
			
			if (objWindows.unload) {
				objWindows.unload();
				objWindows = null;
			}
			objDialog = null;
		}
		
		//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
		function OnBeforeSelected(new_row, old_row, new_col_index) {
			if (!Toolbar || !Toolbar.bitRemoveState || Toolbar.bitDisableEvents || !objRowsSelected) {
				return true;
			}
			
			//Si se está haciendo click nuevamente a un elemento ya seleccionado, se debe limpiar la selección y remover del objeto de selecciones a la row a la que se hizo click
			Toolbar.bitDisableEvents = true;
			if (objRowsSelected[new_row]) {
				delete objRowsSelected[new_row];
				objLogGrid.clearSelection();
				for (selRow in objRowsSelected) {
					objLogGrid.selectRowById(selRow, true);
				}
			}
			else {
				//Se agrega el elemento a al objeto de selecciones y se forza a su selección, en este caso se conservan las selecciones previas así que no hay necesidad de volver
				//a seleccionar cada row como cuando se está removiendo una selección
				objRowsSelected[new_row] = 1;
				objLogGrid.selectRowById(new_row, true);
			}
			Toolbar.bitDisableEvents = false;
			
			//Cancela el evento para que no llegue a OnRowSelect, ya que en este punto se seleccionarán o removerán las selecciones de rows cuando está el modo de remover activo
			return false;
		}

		/*
		function OnRowSelected(RowId, ColumId) {
			//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
			//Si está habilitado el estado para remover, el click no debe cargar el detalle del reporte sino sólo permitir la selección múltiple
			if (Toolbar && Toolbar.bitRemoveState) {
				return;
			}
			//JAPR
		}	
		*/
		
		//EARS 2016-10-24 Visualización del detalle del mensaje
		function OnRowSelected(RowId, ColumId) {
			//JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
			//Si está habilitado el estado para remover, el click no debe cargar el detalle del reporte sino sólo permitir la selección múltiple
			if (Toolbar && Toolbar.bitRemoveState) {
				return;
			}
			//JAPR
			
		<?
			if (getMDVersion() >= esvRealPushMsg) {		
		?>
			
				if (ColumId != 4 && ColumId != 5) {				
					var MessageID = RowId.split('_')[0];
					//var UserID = RowId.split('_')[2];
					//var GroupID = RowId.split('_')[3];
				
					//alert(MessageID);
					//alert(UserID);
					//alert(GroupID);
					
					objWindows = new dhtmlXWindows();
					objDialog = objWindows.createWindow(
						{
							id:"w1",
							left:50,
							top:50,
							width:1000,
							height:600,
							center:true,
							modal:true
							
						});
					objDialog.setText("<?=translate('Push Messages')?>");
					$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
					//objDialog.denyResize();
					objDialog.centerOnScreen();
					
					objDialog.attachEvent("onClose", function() {
						//Despues de cerrar la ventana de Dialogo
						setTimeout(function () {
							doUnloadDialog()
						}, 100);
						return true;
					});

					objDialog.attachURL("ShowDataToFormsPush.php", null, {MessageID:MessageID, RowID:RowId});
				}			
		<?
			}
		?>	
			
		}
		
		var UserGp = [];
		var strValueA = '';
		
		function newPushMsg()
		{  
			//GCRUZ 2016-02-25. Preguntar por formas al generar template de agendas
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objWindows = new dhtmlXWindows();
			objDialog = objWindows.createWindow(
				{
					id:"w1",
					left:50,
					top:50,
					width:1000,
					height:500,
					center:true,
					modal:true
					
				});
			objDialog.setText("<?=translate('New Push Message')?>");
			$(".dhxwin_hdr").attr("style", "z-index: 0; background-color: wheat;");
			//objDialog.denyResize();
			objDialog.denyPark();
			objDialog.attachEvent("onClose", function() {
				//Despues de cerrar la ventana de Dialogo
				setTimeout(function () {
					doUnloadDialog()
				}, 100);
				return true;
			});

			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"block", blockOffset:0, offsetLeft:0, list:[
					{type: "label", label: "<?=translate("Destination Type")?>", labelWidth:150},
					{type: "newcolumn"},
					{type:"radio", name:"DestinationType", label:"<?=translate("User")?>", labelAlign:"right", labelWidth:50, value:'1', checked:true},
					{type: "newcolumn"},
					{type:"radio", name:"DestinationType", label:"<?=translate("Group")?>", labelAlign:"right", labelWidth:50, value:'2'}
				]},
				{type:"block", name:'blockUsers', blockOffset:0, offsetLeft:0, list:[
					{type:"fieldset", name:"fldsAvailableUsers", label:"<?=translate("Available users")?>", width:260, className:"selection_list", list:[
						{type:"input", offsetTop:0, name:"txtSearchAvailableUsers", label:"<?=translate("Search")?>", value:"", labelWidth:50, inputWidth:160, className:"search_button"},
						{type:"container", name:"grdAvailableUsers", inputWidth:210, inputHeight:180, className:"selection_container"}
					]},
					{type:"newcolumn", offset:10},
					{type:"block", blockOffset:0, offsetTop:50, list:[
						{type:"button", name: "btnSelectUser", value:"", className:"assoc_button"},
						{type:"button", name: "btnRemoveUser", value:"", className:"remove_button"}
					]},
					{type:"newcolumn", offset:10},
					{type:"fieldset",  name:"fldsSelectedUsers", label:"<?=translate("Selected users")?>", width:260, className:"selection_list", list:[
						{type:"input", offsetTop:0, name:"txtSearchSelectedUsers", label:"<?=translate("Search")?>", value:"", labelWidth:50, inputWidth:160, className:"search_button"},
						{type:"container", name:"grdSelectedUsers", inputWidth:210, inputHeight:180, className:"selection_container"}
					]}
				]},
				{type:"block", name:'blockGroups', blockOffset:0, offsetLeft:0, list:[
					{type:"fieldset",  name:"fldsAvailableGroups", label:"<?=translate("Available groups")?>", width:250, className:"selection_list", list:[
						{type:"input", offsetTop:0, name:"txtSearchAvailableGroups", label:"<?=translate("Search")?>", value:"", labelWidth:50, inputWidth:150, className:"search_button"},
						{type:"container", name:"grdAvailableGroups", inputWidth:200, inputHeight:180, className:"selection_container"}
					]},
					{type:"newcolumn", offset:10},
					{type:"block", blockOffset:0, offsetTop:50, list:[
						{type:"button", name: "btnSelectGroup", value:"", className:"assoc_button"},
						{type:"button", name: "btnRemoveGroup", value:"", className:"remove_button"}
					]},
					{type:"newcolumn", offset:10},
					{type:"fieldset",  name:"fldsSelectedGroups", label:"<?=translate("Selected groups")?>", width:250, className:"selection_list", list:[
						{type:"input", offsetTop:0, name:"txtSearchSelectedGroups", label:"<?=translate("Search")?>", value:"", labelWidth:50, inputWidth:150, className:"search_button"},
						{type:"container", name:"grdSelectedGroups", inputWidth:200, inputHeight:180, className:"selection_container"}
					]}
				]},
				//{type:"combo", comboType: "checkbox", name:"DestinationUser", label:"<?=translate("User")?>", labelAlign:"left", options:Users, labelWidth:150, inputWidth:250},
				{type:"input", name:"DestinationUser", value:"", hidden:true},
				//{type:"combo", comboType: "checkbox", name:"DestinationGroup", label:"<?=translate("Group")?>", labelAlign:"left", options:Groups, labelWidth:150, inputWidth:250},
				{type:"input", name:"DestinationGroup", value:"", hidden:true},
				//JAPR 2016-12-16: Agregado el maxLength y validación de requerido a los textos y títulos de los mensajes (#WJUKIP)
				{type:"input",name:"Title",label:"<?=translate('Title')?>",value:"", labelAlign:"left", labelWidth:100, inputWidth:480, maxLength:100, validate:"NotEmpty"},
				{type:"input",name:"Message",label:"<?=translate('Message')?>",value:"", rows:5, labelAlign:"left", labelWidth:100, inputWidth:480, maxLength:500, validate:"NotEmpty"},
				{type:"block", blockOffset:50, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otyPushMsg?>, hidden:true}
			];

			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			var dialogDimension = objDialog.getDimension();
			objDialog.setDimension(dialogDimension[0]+30, dialogDimension[1]+10);
			objForm.setItemFocus("TemplateForms");
			//objForm.hideItem('DestinationGroup');
			objForm.hideItem('blockGroups');
			objForm.getInput('Message').style.resize = 'auto';
			objDialog.centerOnScreen();

			//Contenido de la lista de usuarios disponibles
			var objAvailableUsersGrid = new dhtmlXGridObject(objForm.getContainer("grdAvailableUsers"));
			objAvailableUsersGrid.setImagePath("<?=$strScriptPath?>/images/");
			objAvailableUsersGrid.setHeader("<?=translate("Users")?>");
			objAvailableUsersGrid.setInitWidthsP("100");
			objAvailableUsersGrid.setColAlign("left");
			objAvailableUsersGrid.setColTypes("ro");
			objAvailableUsersGrid.enableEditEvents(false);
			objAvailableUsersGrid.enableDragAndDrop(true);
			objAvailableUsersGrid.setColSorting("str");
			objAvailableUsersGrid.enableMultiselect(true);
			objAvailableUsersGrid.init();
			objAvailableUsersGrid.sortRows(0,"str","asc");
			
			//Contenido de la lista de usuarios seleccionados
			var objSelectedUsersGrid = new dhtmlXGridObject(objForm.getContainer("grdSelectedUsers"));
			objSelectedUsersGrid.setImagePath("<?=$strScriptPath?>/images/");
			objSelectedUsersGrid.setHeader("<?=translate("Users")?>");
			objSelectedUsersGrid.setInitWidthsP("100");
			objSelectedUsersGrid.setColAlign("left");
			objSelectedUsersGrid.setColTypes("ro");
			objSelectedUsersGrid.enableEditEvents(false);
			objSelectedUsersGrid.enableDragAndDrop(true);
			objSelectedUsersGrid.setColSorting("str");
			objSelectedUsersGrid.enableMultiselect(true);
			objSelectedUsersGrid.init();
			objSelectedUsersGrid.sortRows(0,"str","asc");

			for (var i=0; i<Users.length; i++) {
				objAvailableUsersGrid.addRow(Users[i].value, Users[i].text);
				objAvailableUsersGrid.setUserData(Users[i].value, "id", Users[i].value);
			}

			//Contenido de la lista de grupos disponibles
			var objAvailableGroupsGrid = new dhtmlXGridObject(objForm.getContainer("grdAvailableGroups"));
			objAvailableGroupsGrid.setImagePath("<?=$strScriptPath?>/images/");
			objAvailableGroupsGrid.setHeader("<?=translate("Groups")?>");
			objAvailableGroupsGrid.setInitWidthsP("100");
			objAvailableGroupsGrid.setColAlign("left");
			objAvailableGroupsGrid.setColTypes("ro");
			objAvailableGroupsGrid.enableEditEvents(false);
			objAvailableGroupsGrid.enableDragAndDrop(true);
			objAvailableGroupsGrid.setColSorting("str");
			objAvailableGroupsGrid.enableMultiselect(true);
			objAvailableGroupsGrid.init();
			objAvailableGroupsGrid.sortRows(0,"str","asc");
			
			//Contenido de la lista de grupos seleccionados
			var objSelectedGroupsGrid = new dhtmlXGridObject(objForm.getContainer("grdSelectedGroups"));
			objSelectedGroupsGrid.setImagePath("<?=$strScriptPath?>/images/");
			objSelectedGroupsGrid.setHeader("<?=translate("Groups")?>");
			objSelectedGroupsGrid.setInitWidthsP("100");
			objSelectedGroupsGrid.setColAlign("left");
			objSelectedGroupsGrid.setColTypes("ro");
			objSelectedGroupsGrid.enableEditEvents(false);
			objSelectedGroupsGrid.enableDragAndDrop(true);
			objSelectedGroupsGrid.setColSorting("str");
			objSelectedGroupsGrid.enableMultiselect(true);
			objSelectedGroupsGrid.init();
			objSelectedGroupsGrid.sortRows(0,"str","asc");

			for (var i=0; i<Groups.length; i++) {
				objAvailableGroupsGrid.addRow(Groups[i].value, Groups[i].text);
				objAvailableGroupsGrid.setUserData(Groups[i].value, "id", Groups[i].value);
			}

			objAvailableUsersGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objAvailableUsersGrid.onDrop Forms');
				objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());
			});

			objAvailableUsersGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objAvailableUsersGrid.onRowDblClicked Forms');
				this.moveRow(rId, "row_sibling", undefined, objSelectedUsersGrid);
				objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());								
				
				//se usará en el evento onKeyup para el manejo de la captura de búsqueda cuando esta es eliminada
				//var UserGp = [];
				UserGp = objSelectedUsersGrid.getAllRowIds();	
				UserGpSelect = UserGp.concat(',');				
			});

			objSelectedUsersGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objSelectedUsersGrid.onDrop Forms');
				objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());
			});

			objSelectedUsersGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objSelectedUsersGrid.onRowDblClicked Forms');
				this.moveRow(rId, "row_sibling", undefined, objAvailableUsersGrid);
				objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());
			});

			objAvailableGroupsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objAvailableGroupsGrid.onDrop Forms');
				objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
			});

			objAvailableGroupsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objAvailableGroupsGrid.onRowDblClicked Forms');
				this.moveRow(rId, "row_sibling", undefined, objSelectedGroupsGrid);
				objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
				
				//se usará en el evento onKeyup para el manejo de la captura de búsqueda cuando esta es eliminada
				//var UserGp = [];
				UserGp = objSelectedGroupsGrid.getAllRowIds();	
				UserGpSelect = UserGp.concat(',');								
			});

			objSelectedGroupsGrid.attachEvent("onDrop", function(sId,tId,dId,sObj,tObj,sCol,tCol) {
				console.log('objSelectedGroupsGrid.onDrop Forms');
				objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
			});

			objSelectedGroupsGrid.attachEvent("onRowDblClicked", function(rId,cInd) {
				console.log('objSelectedGroupsGrid.onRowDblClicked Forms');
				this.moveRow(rId, "row_sibling", undefined, objAvailableGroupsGrid);
				objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
			});

			objForm.attachEvent("onKeyUp",function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				var objGrid = undefined;
				var objGridS = undefined;
				switch (name) {
					case "txtSearchAvailableUsers":
						objGrid = objAvailableUsersGrid;
						objGridS = objSelectedUsersGrid;
						break;
					case "txtSearchSelectedUsers":
						objGrid = objSelectedUsersGrid;
						objGridS = objAvailableUsersGrid;
						break;
					case "txtSearchAvailableGroups":
						objGrid = objAvailableGroupsGrid;
						objGridS = objSelectedGroupsGrid;
						break;
					case "txtSearchSelectedGroups":
						objGrid = objSelectedGroupsGrid;
						objGridS = objAvailableGroupsGrid;
						break;
				}
				
				if (!objGrid) {
					return;
				}
				
				//Verifica si  ya se había aplicado esta búsqueda (debido por ejemplo a teclas que no se consideran caracteres y que por tanto no alterarían el contenido)
				var strValue = inp.value;
				var strLastSearch = objForm.getUserData(name, "lastSearch");
				if (strLastSearch == strValue) {
					return;
				}
				
				objForm.setUserData(name, "lastSearch", strValue);							
				objGrid.filterBy(0, strValue,true);
				
				//ears 2016-11-04 con filterBy( y su tercer parámetro en false) se produce la restauración de todos los elementos de la lista de usuarios o grupos según sea el caso, incluyendo los ya seleccionados
				//de esta forma se trata de filtrar solo los usuarios no seleccionados, todo esto cuando se utiliza una cadena de búsqueda.	
				if (ev.keyCode == 8 || ev.keyCode == 46){  //retroceso o suprimir
					if (strLastSearch.length > 0) {
						if (name == 'txtSearchAvailableUsers' || name == 'txtSearchAvailableGroups'){
							objGrid.clearAll();												 
							var UsersGps = [];
							if (name == 'txtSearchAvailableUsers'){							
								var i=0;
								while (i < Users.length) {
									var blnUser = false;
									UsersGps = objGridS.getAllRowIds();	
									UsersGpsSelected = UsersGps.concat(',');											
									while (UsersGpsSelected.length > 0) {		
											var sUser = UsersGpsSelected.substr(0,(UsersGpsSelected.indexOf(',')))
											UsersGpsSelected = UsersGpsSelected.substr((UsersGpsSelected.indexOf(',')+1))
											if (Users[i].value == sUser) {
												blnUser = true;
											}		 
									}									
									if (!blnUser) {	
										objGrid.addRow(Users[i].value, Users[i].text);
										objGrid.setUserData(Users[i].value, "id", Users[i].value);
									}
									i++;
								}
							}
							
							if (name == 'txtSearchAvailableGroups'){							
								var i=0;
								while (i < Groups.length) {	
									var blnUser = false;
									UsersGps = objGridS.getAllRowIds();	
									UsersGpsSelected = UsersGps.concat(',');											

									while (UsersGpsSelected.length > 0) {		
											var sUser = UsersGpsSelected.substr(0,(UsersGpsSelected.indexOf(',')))
											UsersGpsSelected = UsersGpsSelected.substr((UsersGpsSelected.indexOf(',')+1))
											if (Groups[i].value == sUser) {
												blnUser = true;
											}		
									}									
									if (!blnUser) {	
										objGrid.addRow(Groups[i].value, Groups[i].text);
										objGrid.setUserData(Groups[i].value, "id", Groups[i].value);										
									}
									i++;									
								}
							}
									
						}
						
						//@ears 2016-11-09 lo mismo se revisa para la lista de usuarios o gpos selccionados para pasar a disponibles nuevamente, se revisa su filtro de búsqueda ya que no restauraba
						//debidamente						
						//usuarios o grupos seleccionados
						if (name == 'txtSearchSelectedUsers' || name == 'txtSearchSelectedGroups'){	
							if (name == 'txtSearchSelectedUsers'){	
								var sUsersAvailable = [];
								objGrid.clearAll();
								var sUsersS = UserGpSelect;								
								while(sUsersS.length > 0) { //todos los seleccionados
									var sUserS = sUsersS.substr(0,(sUsersS.indexOf(',')))
									sUsersS = sUsersS.substr((sUsersS.indexOf(',')+1))	
									var blnUser = false;
																		
									sUsersAvailable = objGridS.getAllRowIds();
									sUsersA = sUsersAvailable.concat(',');								

									while (sUsersA.length > 0) { //disponibles
										var sUserA = sUsersA.substr(0,(sUsersA.indexOf(',')))
										sUsersA = sUsersA.substr((sUsersA.indexOf(',')+1))											
										if (sUserA == sUserS) {
											blnUser = true;
										}
									}
									if (!blnUser) {
										var j=0;
										while (j < Users.length) {
											if (Users[j].value == sUserS) {																								
												objGrid.addRow(Users[j].value, Users[j].text);												
											}												
											j++;
										}	
									}																		
								}																																													
							}							
							if (name == 'txtSearchSelectedGroups'){									 												
								var sGpsAvailable = [];
								objGrid.clearAll();
								var sGpsS = UserGpSelect;								
								while(sGpsS.length > 0) { //todos los seleccionados
									var sGpS = sGpsS.substr(0,(sGpsS.indexOf(',')))
									sGpsS = sGpsS.substr((sGpsS.indexOf(',')+1))	
									var blnUser = false;
																		
									sGpsAvailable = objGridS.getAllRowIds();
									sGpsA = sGpsAvailable.concat(',');								

									while (sGpsA.length > 0) { //disponibles
										var sGpA = sGpsA.substr(0,(sGpsA.indexOf(',')))
										sGpsA = sGpsA.substr((sGpsA.indexOf(',')+1))											
										if (sGpA == sGpS) {
											blnUser = true;
										}
									}
									if (!blnUser) {
										var j=0;
										while (j < Groups.length) {
											if (Groups[j].value == sGpS) {																								
												objGrid.addRow(Groups[j].value, Groups[j].text);												
											}												
											j++;
										}	
									}																		
								}																																													
							}																				
						}	
					}
					objGrid.filterBy(0, strValue,true);	
				}		

				strValueA = '';
				if (name == 'txtSearchAvailableUsers' || name == 'txtSearchAvailableGroups') {
					strValueA = inp.value; 
					//capto la entrada del filtro de búsqueda para usuarios disponibles
				}									
			});
			

			objForm.attachEvent("onChange", function (name, value, state){
				console.log('objForm.onChange:: name='+name+' value='+value+' state='+state);
				if (name == 'DestinationType')
				{
					if (value == '1')
					{
						//objForm.hideItem('DestinationGroup');
						//objForm.showItem('DestinationUser');
						objForm.hideItem('blockGroups');
						objForm.showItem('blockUsers');
					}
					if (value == '2')
					{
						//objForm.hideItem('DestinationUser');
						//objForm.showItem('DestinationGroup');
						objForm.hideItem('blockUsers');
						objForm.showItem('blockGroups');
					}
				}
			});

			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							//JAPR 2016-12-16: Agregado el maxLength y validación de requerido a los textos y títulos de los mensajes (#WJUKIP)
							if (objForm.validate()) {
								doSaveMessage();
							}
						}, 100);
						break;
					case "btnSelectUser":
						var strSelection = objAvailableUsersGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objAvailableUsersGrid.moveRow(strId, "row_sibling", undefined, objSelectedUsersGrid);
										
										//ears 2016-11-09 se usará en el evento onKeyup para el manejo de la captura de búsqueda cuando esta es eliminada
										UserGp = objSelectedUsersGrid.getAllRowIds();	
										UserGpSelect = UserGp.concat(',');								
										
									}
								}
								 
								if (blnMove) {
									objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());
									objSelectedUsersGrid.filterBy(0,strValueA,true); 									
								}
							}
						}
						break;
					case "btnRemoveUser":
						var strSelection = objSelectedUsersGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objSelectedUsersGrid.moveRow(strId, "row_sibling", undefined, objAvailableUsersGrid);										
									}
								}
								
								if (blnMove) {
									objForm.setItemValue("DestinationUser", objSelectedUsersGrid.getAllRowIds());
									objAvailableUsersGrid.filterBy(0,strValueA,true); 
									//@ears 2016-11-09 para filtrado una vez que se pasa de seleccionado a disponible
								}
							} 
						}
						break;
					case "btnSelectGroup":
						var strSelection = objAvailableGroupsGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objAvailableGroupsGrid.moveRow(strId, "row_sibling", undefined, objSelectedGroupsGrid);
										
										//@ears 2016-11-09 se usará en el evento onKeyup para el manejo de la captura de búsqueda cuando esta es eliminada
										UserGp = objSelectedGroupsGrid.getAllRowIds();	
										UserGpSelect = UserGp.concat(',');																		
									}
								}
								
								if (blnMove) {
									objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
								}
							}
						}
						break;
					case "btnRemoveGroup":
						var strSelection = objSelectedGroupsGrid.getSelectedRowId();
						if (strSelection) {
							var arrSelection = strSelection.split(",");
							if (arrSelection.length) {
								var blnMove = false;
								for (var intIdx in arrSelection) {
									var strId = arrSelection[intIdx];
									if (strId) {
										blnMove = true;
										objSelectedGroupsGrid.moveRow(strId, "row_sibling", undefined, objAvailableGroupsGrid);										
									}
								}
								
								if (blnMove) {
									objForm.setItemValue("DestinationGroup", objSelectedGroupsGrid.getAllRowIds());
									objAvailableUsersGrid.filterBy(0,strValueA,true); 
									//@ears 2016-11-09 para filtrado una vez que se pasa de seleccionado a disponible
								}
							}
						}
						break;
				}
			});

			objForm.adjustParentSize();
			var dialogDimension = objDialog.getDimension();
			objDialog.setDimension(dialogDimension[0]+30, dialogDimension[1]+10);
		}

		function doSaveMessage()
		{
			if (!objDialog) {
				return;
			}
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
//			var Combx = objForm.getCombo('DestinationUser');
//			var Values = Combx.getChecked();
//			objForm.setItemValue("DestinationUser", Values.join()); //Si Values es [] se envia ""
//			var Combx = objForm.getCombo('DestinationGroup');
//			var Values = Combx.getChecked();
//			objForm.setItemValue("DestinationGroup", Values.join()); //Si Values es [] se envia ""

			objDialog.progressOn();
			objForm.lock();
			objForm.send("processRequest.php", "post", function(loader, response) {
				if (!loader || !response) {
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				setTimeout(function () {
					console.log(response);
					//JAPR 2017-01-18: Corregido un bug, se había agregado un espacio al inicio de la respuesta, así que ya no coincidía con la validación y mostraba un alert sin haber error (#F42URH)
					if ($.trim(String(response).toUpperCase()) != 'OK') {
						alert(response);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
					doUnloadDialog();
					parent.doExecuteURL("", undefined, undefined, 'main.php?BITAM_SECTION=PushMsgCollection');
				}, 100);
			});
		}
		
		function str_custom(a,b,order){
	        return (a.toLowerCase() > b.toLowerCase() ? 1: -1) * (order == "asc" ? 1 : -1);
	    }

		function FilterToDate(FiterDate)
 		{
 			//EVEGA Obtenemos el widget del BoxFilter para vericar si tiene filtros activados
 			if (!$.isEmptyObject($('#boxFilter').data())){
 				var boxFilterWidget=$('#boxFilter').data().bitamBoxfilter;
 				var searchString = boxFilterWidget.element.find('input[name|="search"]').val();
 				var filter = boxFilterWidget.filter;
 				if(searchString != ''){
					filter['SearchString'] = searchString;
				}
				/*if ($('[data-fieldname="'+'date'+'"]').val() != '') {
					filter['Date'] = $('[data-fieldname="'+'date'+'"]').val();
				}*/
				for (var fkey in filter) {
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}
					//Search for Date
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
					}
					//Search for Group of Users
					if (fkey == 'Groups') {
						var filterGroups = filter['Groups'];
						var Groups = new Array();
						for (var ukey in filterGroups) {
							Groups.push(filterGroups[ukey]);
						}
						frmAttribFilter.GroupID.value = Groups.join(',');
						/*if(descrTag == 'Groups')
							frmAttribFilter.oldGroupsTags.value = oldTags;*/
					}	
					//Search for Users
					if (fkey == 'Users') {
						var filterUsers = filter['Users'];
						var Users = new Array();
						for (var ukey in filterUsers) {
							Users.push(filterUsers[ukey]);
						}
						frmAttribFilter.userID.value = Users.join(',');
						/*if(descrTag == 'Groups')
							frmAttribFilter.oldUserTags.value = oldTags;*/
					}
				}

				//EVEGA obtenemos los tags del box filter para mandarlo como oldTags
				var oldTags=JSON.stringify(boxFilterWidget.descriptionsTags);
 			}
 			if(FiterDate!=null){
 				frmAttribFilter.FilterDate.value = FiterDate;
 			}
			frmAttribFilter.submit();
 		}

		
		</script>
		<form name="frmAttribFilter" action="main.php?BITAM_SECTION=PushMsgCollection" method="POST" accept-charset="utf-8">
			<input type="hidden" name="BITAM_SECTION" value="PushMsgCollection">
			<input type="hidden" name="GroupID" id="GroupID" value="">
			
			<input type="hidden" name="FilterDate" id="FilterDate" value="">
			<input type="hidden" name="startDate" id="startDate" value="">
			<input type="hidden" name="endDate" id="endDate" value="">
			<input type="hidden" name="userID" id="userID" value="">

			<input type="hidden" name="searchString" id="searchString" value="">
			<input type="hidden" name="oldGroupsTags" id="oldGroupsTags" value="">
			<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
		</form>

		</body>
		</HTML>
<?
		die();
	}

	static function NewInstanceForRemove($aRepository, $anArrayOfMessageIDs=null, $startDate="", $endDate="", $userID=-2, $ipp=100, $bGetValues = false)
	{
		$anInstance = new BITAMPushMsgCollection($aRepository, $userID);
		
		//Obtener los datos de la dimension Usuario
		$aSurvey = BITAMSurvey::NewInstance($aRepository);
		if (is_null($aSurvey)) {
			return null;
		}
		$aSurvey->readGblSurveyModel();
		if ($aSurvey->GblUserDimID <= 0) {
			return null;
		}
		
		$fieldEmailKey = "RIDIM_".$aSurvey->GblUserDimID."KEY";
		$fieldEmailDesc = "DSC_".$aSurvey->GblUserDimID;
		
		$whereFilter = "";
		if (!is_null($anArrayOfMessageIDs))
		{
			switch (count($anArrayOfMessageIDs))
			{
				case 0:
					break;
				case 1:
					$whereFilter = " AND A.MessageID = ".((int)$anArrayOfMessageIDs[0]);
					break;
				default:
					foreach ($anArrayOfMessageIDs as $aMessageID)
					{
						if ($whereFilter != "")
						{
							$whereFilter .= ", ";
						}
						
						$whereFilter .= (int)$aMessageID; 
					}
					
					if ($whereFilter != "")
					{
						$whereFilter = " AND A.MessageID IN (".$whereFilter.")";
					}
					break;
			}
		}
		
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;
		$svStartDate = $startDate;
		$svEndDate = $endDate;
		$svUserID = $userID;
		$svIpp = $ipp;
		
		$sql = "SELECT A.MessageID, A.UserID, C.{$fieldEmailDesc} AS UserName, A.CreationUserID, A.CreationDateID, A.Title, A.PushMessage, A.State ";
		$where = '';
		$sql .= " FROM SI_SV_PushMsg A, RIDIM_{$aSurvey->GblUserDimID} C";
		//Ligamos las Tablas para lo de Email
		$where.=($where!=""?" AND ":"")."A.UserID = C.{$fieldEmailKey}";
		$sql .= " WHERE ".$where.$whereFilter;
		$sql .= " ORDER BY A.CreationDateID DESC, C.{$fieldEmailDesc}";  //@ears 2016-11-09 ordenamiento descendente para que se muestren primero las notificaciones mas recientes
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_PushMsg ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMPushMsg::NewInstanceFromRS($aRepository, $aRS, $bGetValues);
			$aRS->MoveNext();
		}
		
		if ($startDate!="")
		{
			$anInstance->StartDate = $startDate;
		}
		
		if ($endDate!="")
		{
			$anInstance->EndDate = $endDate;
		}
		
		if ($userID!=-2)
		{
			$anInstance->UserID = $userID;
		}
		
		if ($ipp!=100)
		{
			$anInstance->ipp = $ipp;
		}
		
		return $anInstance;
	}
	
	//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	/* Dada la clave de un usuario, obtiene la cantidad de mensajes que deben ser indicados en el Badge del App para indicar los mensajes pendientes por leer, los cuales se consideran
	aquellos enviados no marcados como leídos y no eliminados (a la fecha de implementación no había mecanismo para marcar un mensaje como no leído, pero si se implementase, simplemente
	bastaría con restaurar el state a enviado para ser considerado por este proceso como no leído)
	*/
	static function GetPushMessagesBadgeNumberByUser($aRepository, $aUserID, $aRegID) {
		$intPendingPushMsg = 0;
		$aUserID = (int) @$aUserID;
		
		$strRegIDFilter = "";
		if ($aRegID) {
			$strRegIDFilter = " AND RegID = ".$aRepository->DataADOConnection->Quote($aRegID);
		}
		$sql = "SELECT COUNT(*) AS BadgeNum 
			FROM SI_SV_PushMsgDet 
			WHERE UserID = {$aUserID} AND State IN (".pushMsgSent.") 
			{$strRegIDFilter}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$sql}");
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intPendingPushMsg = (int) @$aRS->fields["badgenum"];
		}
		
		return $intPendingPushMsg;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$searchString = "";
		if (array_key_exists("searchString", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["searchString"] != "")
				$searchString = @$aHTTPRequest->POST["searchString"];
		}
		
		$startDate = "";
		if (array_key_exists("startDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["startDate"])!="" && substr($aHTTPRequest->POST["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->POST["startDate"]);
			}
		}
		elseif (array_key_exists("startDate", $aHTTPRequest->GET)) {
			$startDate = trim($aHTTPRequest->GET["startDate"]);
		}
		
		$endDate = "";
		if (array_key_exists("endDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["endDate"])!="" && substr($aHTTPRequest->POST["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->POST["endDate"]);
			}
		}
		elseif (array_key_exists("endDate", $aHTTPRequest->GET)) {
			$endDate = trim($aHTTPRequest->GET["endDate"]);
		}
		
		$UserID = "";
		if (array_key_exists("userID", $aHTTPRequest->GET))
		{
			$UserID = $aHTTPRequest->GET["userID"];
		}
		elseif (array_key_exists("userID", $aHTTPRequest->POST)) {
			$UserID = $aHTTPRequest->POST["userID"];
		}

		//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/Año
		$dateTwoState = "";
		if (array_key_exists("dateTwoState", $aHTTPRequest->GET))
		{
			$dateTwoState = $aHTTPRequest->GET["dateTwoState"];
		}
		elseif (array_key_exists("dateTwoState", $aHTTPRequest->POST)) {
			$dateTwoState = $aHTTPRequest->POST["dateTwoState"];
		}
		switch($dateTwoState)
		{
			case 'T':
				$startDate = date('Y-m-d').' 00:00:00';
				$endDate = date('Y-m-d').' 23:59:59';
				break;
			case 'W':
				$weekDay = intval(date('w'));
				$startDate = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
				$endDate = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
				break;
			case 'M':
				$tDay = date('t');
				$startDate = date('Y-m-').'01 00:00:00';
				$endDate = date('Y-m-').$tDay.' 23:59:59';
				break;
			case 'Y':
				$startDate = date('Y-').'01-01 00:00:00';
				$endDate = date('Y-').'12-31 23:59:59';
				break;
		}

		$GroupID = "";
		if (array_key_exists("GroupID", $aHTTPRequest->POST)) {
			$GroupID = $aHTTPRequest->POST["GroupID"];
		}
		elseif (array_key_exists("GroupID", $aHTTPRequest->GET)) {
			$GroupID = $aHTTPRequest->GET["GroupID"];
		}
		
		$FilterDate = "";
		if (array_key_exists("FilterDate", $aHTTPRequest->POST))
		{
			$FilterDate = $aHTTPRequest->POST["FilterDate"];
		}
		return BITAMPushMsgCollection::NewInstanceToLog($aRepository, $searchString, $startDate, $endDate, $GroupID, $UserID, $FilterDate);
	}

	function get_Parent()
	{
		return $this->Repository;
	}

	function get_Title()
	{
		return translate("Mobile notifications");
	}

	function get_QueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_SECTION=PushMessageCollection".$strParameters;
	}

	function get_ChildQueryString()
	{
		return "BITAM_SECTION=PushMessage";
	}

	function get_AddRemoveQueryString()
	{
		global $svStartDate;
		global $svEndDate;
		global $svUserID;
		global $svIpp;

		$strParameters = "";

		if(!is_null($svStartDate))
		{
			$strParameters.="&startDate=".$svStartDate;
		}

		if(!is_null($svEndDate))
		{
			$strParameters.="&endDate=".$svEndDate;
		}
		
		if(!is_null($svUserID))
		{
			$strParameters.="&userID=".$svUserID;
		}

		if(!is_null($svIpp))
		{
			$strParameters.="&ipp=".$svIpp;
		}

		return "BITAM_PAGE=PushMessage".$strParameters;
	}

	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'MessageID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");

		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CreationDateID";
		$aField->Title = translate("Date");
		$aField->ToolTip = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Title";
		$aField->Title = translate("Title");
		$aField->ToolTip = translate("Title");
		$aField->Type = "String";
		$aField->IsLink = false;
		$myFields[$aField->Name] = $aField;
		
		$arrayUsers = array();
		if ($this->isNewObject()) {
			$arrayUsers[0] = translate("Select a user");
		}
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)@$aRS->fields["cla_usuario"];
			$nom_largo = (string) @$aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		if ($this->UserID < 0) {
			$this->UserID = 0;
		}
		if (!isset($arrayUsers[$this->UserID])) {
			$this->UserID = -1;
			$arrayUsers[-1] = "(".translate("Deleted user").")";
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "UserID";
		$aField->Title = translate("User Name");
		$aField->ToolTip = translate("User Name");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayUsers;
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "PushMessage";
		$aField->Title = translate("Message");
		$aField->ToolTip = translate("Message");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$arrayStatus = array(0 => translate('Not sent'), 1 => translate('Sent'));
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "State";
		$aField->Title = translate("Status");
		$aField->ToolTip = translate("Status");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arrayStatus;
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function hideRemoveButton($aUser)
	{
		return false;
	}
	
	function generateBeforeFormCode($aUser)
	{
?>		<script language="JavaScript">
 		
		function applyFilterAttrib()
		{
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = objStartDate.value;

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = objEndDate.value;
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = objUserID.value;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = objIpp.value;

			frmAttribFilter.submit();
		}
		
		function clearFilter()
		{
<?
			if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100)
			{
?>
			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
?>
		}
		
		var ctrlEditAudit;
		ctrlEditAudit = -1;
		
		function editAudit()
		{
			ctrlEditAudit = ctrlEditAudit*(-1);
			var obDivTitle = document.getElementById("collection_title_div");
			var objBtnBack = document.getElementById("btnBack");
			var objBtnEditLog = document.getElementById("btnEditLog");
			var objBtnDelEntries = document.getElementById("btnDelEntries");
			var arrayTrCollection = document.getElementsByName("tr_collection_checkbox");
			var i;
			
			if(ctrlEditAudit==1)
			{
				objBtnBack.style.display = 'inline';
				objBtnEditLog.style.display = 'none';
                objBtnDelEntries.style.display = 'inline';
				//obDivTitle.innerHTML = 'Edit Log<hr>';
				
    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
      				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = ((bIsIE)?"inline":"table-cell");
    				}
    			}
			}
			else
			{
				objBtnBack.style.display = 'none';
				objBtnEditLog.style.display = 'inline';
				objBtnDelEntries.style.display = 'none';
				//obDivTitle.innerHTML = 'Log: Data Captures<hr>';

    			if(arrayTrCollection!=undefined && arrayTrCollection!=null)
    			{
    				for(i=0; i<arrayTrCollection.length; i++)
    				{
    					objTD = arrayTrCollection[i].firstChild;
    					objTD.style.display = 'none';
    				}
    			}		
			}
		}
		</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
?>
	
	<form name="frmAttribFilter" action="main.php" method="GET" target="body">
		<input type="hidden" name="BITAM_SECTION" value="PushMessageCollection">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">
	</form>
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}

	function displayCheckBox($aUser)
	{
		return true;
	}
	
	function afterTitleCode($aUser)
	{
		$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		$numSurveys = count($surveyCollection->Collection);
		
		$numRows = ceil(0/3);
		$countCell = 0;
		$maxCell = 4;
		//@JAPR 2012-04-04: Modificado el combo de encuestas para que aparezca mas grande y no corte nombres de encuestas largos
		$numColSpanForm = 4; // 2;
		//@JAPR
		$totalColSpanTable = 1 + ($maxCell*2);
		
		if($this->UserID!=-2 || $this->StartDate!="" || $this->EndDate!="" || $this->ipp!=100)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = "Reapply";
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = "Apply";
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply Filters").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">Clear</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		$btnEditAudit='<div class="collection_buttons" style="border:0px; background:transparent"><button id="btnEditLog" style="display:none" onclick="javascript:editAudit();"><img src="images/edit.png" alt="'.translate("Edit log").'" title="'.translate("Edit log").'" displayMe="1" />'.translate("Edit log").'</button></div>';
		
		$arrayButtons[0]=$btnEditAudit;
		$btnDelEntries = '<span id="btnDelEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:BITAMPushMsgCollection_Remove();"><img src="images/delete.png">&nbsp;'.translate("Delete entries").'</span>';
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons)+1;
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo=
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

?>
	<span id="btnBack" style="display:none" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editAudit();"><img src="images/home.png">&nbsp;<?="Back"?></span>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		$arrayUsers[-2] = translate("All");
		$arrayUsers[-1] = "NA";
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$sql.=" AND B.CLA_USUARIO > 0 ";
			//@JAPR
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			
			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		$maxRows = $countButtons;

		$selected = "";
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?="Filters"?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser=>$nameUser)
		{
			$selected="";

			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
		$countCell=1;
		
		$loadCat = true;
		$strAtribFilter = "";
		
		if(true)
		{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td <?=$classBorderBottom?>>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						elseif ($maxRows == ($numRows+1)) {
							$classBorderTop = ' class="styleBorderFilterBottom"';
							$classBorderRightTop = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		}
?>
	</table>
	<br>
<?
	}
	
	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
<?
	}
}
?>
