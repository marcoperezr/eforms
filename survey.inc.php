<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("appuser.inc.php");
require_once("user.inc.php");
require_once("section.inc.php");
require_once("question.inc.php");
require_once("catalog.inc.php");
require_once("surveyfilter.inc.php");
require_once("surveyscheduler.inc.php");

class BITAMSurvey extends BITAMObject
{
	//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
	static $KeyField = "SurveyKey";		//SVeForms_####.SurveyKey: Key auto generado que representa una captura de la forma (es el equivalente al ID de Reporte, el anterior FactKeyDimVal)

	public $ForceNew;
	public $SurveyID;
	public $SurveyName;
	public $Status;
	public $ModelID;
	public $ClosingMsg;
	public $CountIndID;
	public $AnsweredQuestionsIndID;
	public $DurationIndID;
	//Latitude y longitude
	public $LatitudeIndID;
	public $LongitudeIndID;
	public $UserDimID;
	//@JAPRDescontinuada: Al día 2014-09-24 no se encontró ningún proceso que la invocara, excepto en section.inc.php donde realmente se usa en otra
	//función que está descontinuada
	public $SectionDimID;
	public $CaptureStartTime;
	public $CaptureEndTime;
	public $HelpMsg;
	public $TempWeek;
	public $TempMonth;
	public $TempTwoMonths;
	public $TempQuarter;
	//@JAPR 2014-02-17: Removida la configuración del catálogo de la encuesta
	public $CatalogID;		//@JAPRDescontinuada, ya no se requiere un catálogo asociado a la encuesta
	public $OldCatalogID;	//@JAPRDescontinuada, ya no se requiere un catálogo asociado a la encuesta
	
	public $CaptureStartDate;
	public $CaptureEndDate;
	public $WhenToCaptureType;
	public $WhenToCaptureValue;
	public $StartDay;
	public $Duration;
	public $AllowMultipleSurveys;
	public $UserIDs;
	public $UsrRoles;
	public $StrUserRol;
	//@JAPR 2015-07-26: Removidas todas las referencias a las tablas enc_ pues tenía años que no se usaba, SurveyType no indica nada en absoluto a la fecha de
	//esta limpieza, eventualmente se podría llegar a usar para algún fin pero por ahora todas son consideradas svtNormal
	public $SurveyType;
	public $HasSignature;
	//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
	public $SyncWhenSaving;
	//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
	public $HideNavButtons;
	//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
	public $HideSectionNames;
	//@JAPR
	
	//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
	//Número de modificación de cualquiera de las partes de la estructura de la Agenda (excepto catálogos). No necesariamente cambia cuando se modifica LastDateID, ya que este valor no depende de los catálogos mientras que LastDateID si lo hace
	public $VersionNum;
	//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	public $EnableReschedule;			//Indica si se permitirá que al grabar esta encuesta la agenda a la que pertenece será recalendarizada
	public $RescheduleCatalogID;		//Catalogo para Recalendarizar la encuesta
	public $RescheduleCatMemberID;		//Atributo para Recalendarizar la encuesta
	public $RescheduleStatus;			//Texto del Status que se asociará al Recalendarizar (no debe ser el ID porque las Apps no tienen acceso a ellos)
	
	//@LROUX 2011-10-18: Si esta opción esta en 1, el botón de save en la encuesta esta en todas las pantallas intermedias, si esta en 0, se muestra solo al final.
	public $AllowPartialSave;
	
	public $GblSurveyModelID;
	public $GblSurveyDimID;					//Contiene los nombres de las encuestas definidas en eForms
        
	//Se agregaron las dimensiones "Survey Global ID" y "Survey Single Record ID"
	/* Nomenclatura:
		KEY == Llave subrrogada de la dimensión correspondiente
		_ == Separador, se usa para combinar 2 o mas identificadores de esta lista o separar texto descriptivo de identificadores (ej: KEY_SurveyDimID quiere decir que se debe buscar a que se hace referencia con KEY y a que se hace referencia con SurveyDimID, y entonces interpretarlo como el KEY de ese SurveyDimID)
		- == Literalmente un guión en el contenido del campo
		SurveyDimID == Dimensión que contiene el listado de todas las encuestas ($this->GblSurveyDimID)
		FactKeyDimID == Dimensión que contiene el consecutivo de las capturas de una encuesta (se crea en el modelo de encuesta, no en el modelo Global de encuestas, $this->FactKeyDimID)
		FactKey == Llave subrrogada única autoincremental de una tabla de Modelo, en este caso se hace referencia a la tabla de Modelo de la encuesta en cuestión
	*/
	public $GblSurveyGlobalDimID;			//Contiene un registro de la captura de encuesta. Contenido: KEY_SurveyDimID - KEY_FactKeyDimID
	public $GblSurveySingleRecordDimID;		//Contiene un registro por cada elemento dinámico o Maestro-Detalle (si lo hay) o un sólo registro para una captura estándar de encuesta. Contenido: KEY_SurveyDimID - KEY_FactKeyDimID - FactKey
    
	public $GblUserDimID;
	public $GblStartDateDimID;
	public $GblEndDateDimID;
	public $GblStartTimeDimID;
	public $GblEndTimeDimID;
	public $GblCountIndID;
	public $GblAnsweredQuestions;
	public $GblDurationIndID;
	public $GblLatitudeIndID;
	public $GblLongitudeIndID;
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	public $GblAccuracyIndID;
	public $GblAccuracyDimID;
	public $GblLatitudeDimID;
	public $GblLongitudeDimID;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización
	public $GblSyncDateDimID;
	public $GblSyncTimeDimID;
	//@JAPR 2014-11-18: Agregada la dimensión Agenda
	public $GblAgendaDimID;
	//@JAPR
	/*@MABH20121205*/
	public $GblServerStartDateDimID;		//Fecha inicial de la captura inicial según el servidor
	public $GblServerEndDateDimID;			//Fecha final de la captura inicial según el servidor
	public $GblServerStartTimeDimID;		//Hora inicial de la captura inicial según el servidor
	public $GblServerEndTimeDimID;			//Hora final de la captura inicial según el servidor
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	public $GblServerStartDateEditDimID;	//Fecha inicial de la última edición local de outbox según el servidor
	public $GblServerEndDateEditDimID;		//Fecha final de la última edición local de outbox según el servidor
	public $GblServerStartTimeEditDimID;	//Hora inicial de la última edición local de outbox según el servidor
	public $GblServerEndTimeEditDimID;		//Hora final de la última edición local de outbox según el servidor
	//@MABH20121205
	//@JAPR 2015-07-09 Agregado el soporte de Formas v6 con un grabado completamente diferente
	//Se agregarán nuevas dimensiones de Latitud y Longitud, ya que las existentes eran dimensiones atributo así que no se podían registrar en una tabla separada
	//para las nuevas tablas de datos de eForms (NO confundir con las dimensiones-atributo originales, se les agregará "Ind" de "Independiente" antes de DimID)
	//@JAPR 2015-07-24: Finalmente NO se agregaron dimensiones independientes para Latitude, Longitude ni Accuracy de la captura, porque la idea original de tener toas las tablas normalizadas
	//ya no aplicó debido a limitantes en la cantidad de joins, adicionalmente EArteaga utilizó dimensiones-atributo para estos valores en los modelos mapeados a partir de las nuevas tablas
	//de eForms v6, por lo que haber creado una nueva tercia de dimensiones habría entrado en conflicto con las dimensiones-atributos existentes del cubo global, así que se optó por
	//seguir utilizando las ya existentes
	//public $GblLatitudeIndDimID;		//Dimensión independiente con el valor de la Latitude en el cubo global de Surveys
	//public $GblLongitudeIndDimID;		//Dimensión independiente con el valor de la Longitude en el cubo global de Surveys
	//public $GblAccuracyIndDimID;		//Dimensión independiente con el valor de la Accuracy en el cubo global de Surveys
	//El resto de las dimensiones siguientes son estrictamente nuevas en las formas de V6
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	//A partir de V6 el día de esta implementación, se modificó el sentido de estas dimensiones para que contengan las fechas de edición locales de los Outbox, en caso de agregar
	//fechas de edición directas en browser mas adelante, se tendría que agregar un nuevo conjunto de dimensiones para tal fin. En los pocos casos donde un repositorio viniera de
	//versiones anteriores a V6, habría que considerar que en el cubo global esta dimensión tendría un antes y un después o mejor dicho según la versión de la forma significarían
	//edición en browser (v5) o en outbox local (v6)
	public $GblStartDateEditDimID;		//Fecha inicial de la última edición local de outbox (x- vía Web -x) de una captura previamente grabada
	public $GblStartTimeEditDimID;		//Hora inicial de la última edición local de outbox (x- vía Web -x) de una captura previamente grabada
	public $GblEndDateEditDimID;		//Fecha final de la última edición local de outbox (x- vía Web -x) de una captura previamente grabada
	public $GblEndTimeEditDimID;		//Hora final de la última edición local de outbox (x- vía Web -x) de una captura previamente grabada
	public $GblSyncEndDateDimID;		//Fecha final de la sincronización de la captura
	public $GblSyncEndTimeDimID;		//Hora en formato 00:00:00 correspondiente con la hora final de la sincronización de la captura
	public $GblSyncLatitudeDimID;		//Latitude de la sincronización de la captura
	public $GblSyncLongitudeDimID;		//Longitude de la sincronización de la captura
	public $GblSyncAccuracyDimID;		//Accuracy de los datos de GPS de la sincronización de la captura
	public $GblCountryDimID;			//País obtenido directamente de Google Maps según los datos de GPS de la captura cuando esta se realizó en el dispositivo
	public $GblStateDimID;				//Estado obtenido directamente de Google Maps según los datos de GPS de la captura cuando esta se realizó en el dispositivo
	public $GblCityDimID;				//Ciudad obtenida directamente de Google Maps según los datos de GPS de la captura cuando esta se realizó en el dispositivo
	public $GblZipCodeDimID;			//Código postal obtenido directamente de Google Maps según los datos de GPS de la captura cuando esta se realizó en el dispositivo
	public $GblAddressDimID;			//Dirección obtenida directamente de Google Maps según los datos de GPS de la captura cuando esta se realizó en el dispositivo
	public $GblLastCheckDate;			//Fecha y hora de la última revisión de integridad realizada, no se realizará una nueva sino hasta el día siguiente (una fecha distinta)
	//@JAPR
	//GCRUZ 2016-05-30. Tiempo y distancia de traslado de la captura
	public $GblTransferTimeIndID;
	public $GblTransferDistanceIndID;
	
	public $EmailDimID;
	public $StatusDimID;
	public $SchedulerDimID;
	public $FactKeyDimID;
	public $LatitudeAttribID;
	public $LongitudeAttribID;
	public $ActionIndInlineID;
	//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
	public $ElementQuestionID;			//Id de la pregunta que se usará como la descripción del "Element" de las Acciones
	public $eBavelAppID;				//Id de la aplicación de eBavel asociada a esta encuesta para crear las Acciones
	public $eBavelFormID;				//Id de la forma de eBavel sobre la que se crearán grabarán los datos de la encuesta
	//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	public $eBavelAppIDOld;				//Id de la aplicación de eBavel al cargar la encuesta (si este cambia, el método cleareBavelFields limpiará el mapeo de campos)
	public $eBavelFormIDOld;			//Id de la forma de eBavel al cargar la encuesta (si este cambia, el método cleareBavelFields limpiará el mapeo de campos)
	//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	public $eBavelFieldsData;			//Variable temporal (no se graba en la metadata) con los pares de datos "SurveyFieldMapName=eBavelFormFieldID" separados por "|" utilizados durante el grabado para mapear los campos de eBavel
	public $eBavelFieldsDataOld;		//Igual que la variable de arriba pero para detectar si hubo o no cambios en el mapeo
	//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
	public $eBavelActionFormID;			//Id de la forma de eBavel sobre la que se crearán las Acciones
	public $eBavelActionFieldsData;		//Variable temporal (no se graba en la metadata) con los pares de datos "SurveyFieldMapName=eBavelFormFieldID" separados por "|" utilizados durante el grabado para mapear los campos de eBavel
	public $eBavelActionFieldsDataOld;	//Igual que la variable de arriba pero para detectar si hubo o no cambios en el mapeo
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	public $AccuracyAttribID;			//Valor de la exactitud de la localización GPS medida en metros (Atributo-dimension e indicador respectivamente)
	public $AccuracyIndID;
	//@JAPR 2014-11-18: Agregada la dimensión Agenda
	public $AgendaDimID;				//Id de la dimensión Agenda. Es la misma que GblAgendaDimID, pero separandola se podría provocar que se haga la actualización de la dimensióne específicamente en el cubo de la encuesta aunque en el cubo global ya exista
	//@JAPR
	
	public $StartDateDimID;
	public $EndDateDimID;
	public $StartTimeDimID;
	public $EndTimeDimID;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización
	public $SyncDateDimID;				//Fecha de inicio de la sincronización de una captura desde móvil
	public $SyncTimeDimID;				//Hora de inicio de la sincronización de una captura desde móvil
	//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
	public $UseStdSectionSingleRec;		//Indica si al grabar las capturas se deberá generar un registro especial para grabar todas las preguntas de secciones estándar sin preguntas de otras
			//secciones como dinámicas, maestro-detalle o preguntas categoría de dimensión (true - default para encuestas nuevas v4+), o si no habrá tal registro y las preguntas de secciones estándar se grabarán como siempre
			//repitiendo su valor en cada registro que se inserte (false - default para encuestas pre-existentes migradas). NO se debe cambiar este switch una vez que ya se tengan capturas realizadas, además debe ser una encuesta
			//ya completamente migrada a v4 pues depende de la correcta asignación de los campos que identifica la sección a la que pertenecen los registros para permitir ver datos correctos
	//@JAPR
	public $UseSourceFields;			//Indica si están disponibles los campos EntrySectionID y EntryCatDimID
	public $UseRecVersionField;			//Indica si está disponible el campo eFormsVersionNum
	public $UseDynamicPageField;		//Indica si está disponible el campo DynamicPageDSC
	public $SpecialFieldsRead;			//Indica si ya se identificaron los campos especiales para no repetir los SELECTs en la misma llamada al server
	//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $UseDynamicOptionField;		//Indica si está disponible el campo DynamicOptionDSC (y por consiguiente DynamicValue pues se agregan juntos)
	//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
	//Se dejó como una propiedad, pero realmente no es configurable, siempre grabará en NULL a partir de este momento, sólo es propiedad para poder desactivar fácilmente esta opción en caso de algún error
	public $UseNULLForEmptyNumbers;	//Indica que se deberá grabar un valor de NULL para preguntas numéricas que realmente no hubieran sido respondidas
	//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	/* Esta opción sólo aplica durante la creación de la encuesta, o bien, si la encuesta no tiene datos, de haber al menos una captura se validará
	para NO permitir cambiarla ya que los datos grabados no tendrían sentido una vez hecho el cambio. Originalmente las preguntas de catálogo apuntaban
	a la dimensión del catálogo tanto en la Paralela como en la tabla de hechos, de tal suerte que el menos cambio en el catálogo provocaba que el
	historico se alterara o incluso que ya no se pudiera recuperar el datos si se habían borrado dichos elementos. Con esta nueva opción ahora la
	dimensión de la pregunta contendrá el Key (según el caso pues para dinámicas depende de si se generan registros por página o por CheckBox) del 
	elemento del atributo correspondiente pero ya según como se grabó la descripción en la tabla de la dimensión independienet (no según el registro
	en la tabla del catálogo), de esta forma cada dimensión independiente que represente a los atributos puede tener un key diferente y los cambios
	en el catálogo no afectarían en absoluto al histórico (los valores se recuperarían de las nuevas dimensiones y no del catálogo durante la edición)
	*/
	//Esto NO será una opción configurable, sólo nuevos catálogos y nuevas encuestas nacerán con esta opción activada, solamente los catálogos preexistentes se actualizarán al ser usados por una encuesta nueva
	public $DissociateCatDimens;		//Indica si se manejarán dimensiones separadas para los atributos de los catálogos en esta encuesta
	//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	public $FactTable;
	//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
	public $OtherLabel;					//Etiqueta que se usará en lugar de la palabra "Other" (opción de AllowAdditionalAnswers == true)
	public $CommentLabel;				//Etiqueta que se usará en lugar de la palabra "Comment"
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	public $AdditionalEMails;			//Puede ser una o varias cuentas de EMail adicionales las que se enviará el reporte (además de la propia cuenta de quien captura + su AltEMail), ya sea directas separadas por ";" o bien através de variables que hagan referencia a las preguntas incluyendo de catalogo con referencia a atributos ({@Q#(x)})
	//@JAPR 2014-03-18: Agregado el icono para las encuestas
	public $SurveyImageText;			//Imagen asociada a la encuesta para verla como icono en la lista de encuestas del App de captura
	//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
	public $SurveyMenuImage;			//Imagen asociada a el boton de menu la encuesta
	//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
	public $DisableEntry;				//Si se activa esta opción, la encuesta no tendrá un botón de Save/Save Draft, no podrá ser editada desde los reportes, no podrá generar Schedulers tipo EMail y sus preguntas tipo Sincronización no tendrán funcionalidad ligada, ya que no se permite que graben datos sino sólo visualizarlas en el App
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	/*@MABH20121205*/
	public $ServerStartDateDimID;
	public $ServerEndDateDimID;
	public $ServerStartTimeDimID;
	public $ServerEndTimeDimID;
	//@MABH20121205
	
	public $IsAgendable;
	public $RadioForAgenda;
	//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
	public $SchedulerID;
	public $SurveyDesc;
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	//Propiedades con nombres de campos y/o tablas que apuntan a la nueva estructura de grabado de datos de las formas
	//public $KeyField;				//SVeForms_####.SurveyKey: Key auto generado que representa una captura de la forma (es el equivalente al ID de Reporte, el anterior FactKeyDimVal)
	public $SurveyTable;			//SVeForms_####: Nombre de la tabla que almacena el registro único que existe por cada captura (si la versión es previa a v6, entonces es simplemente SVSurvey_####)
	public $SurveyStdTable;			//SVeForms_####_SectionStd: Nombre de la tabla que almacena el registro único que existe para las secciones estándar
	//@JAPR
	//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
	public $incrementalFile;
	//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
	public $Width;					//El tamaño horizontal del elemento en la lista de formas del App definida en pixeles
	public $Height;					//El tamaño vertical del elemento en la lista de formas del App definida en pixeles
	public $TextPosition;			//El tipo de alineación del texto respecto a la imagen de la forma
	public $TextSize;				//Dependiendo del tipo de alineación, es el ancho/alto del área del texto en pixeles o porcentaje (la imagen usará el restante del área total de la forma definida en Width) definida en pixeles
	public $CustomLayout;			//El template HTML a utilizar como contenido de cada item en la lista de formas del App en caso de haberse personalizado. Si está vacío entonces se generará la vista default según otras configuraciones
	//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
    public $TemplateID;				//ID de la plantilla de personalización de interface aplicada a esta forma
	//@JAPR
	//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
	public $ShowQNumbers; 			//Muestra o no los números de las preguntas de la survey.
	//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
	public $PlannedDuration;
	//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
	public $EnableNavHistory;		//Indica si se habilitará la pila de secciones para que al retroceder vuelva por el camino utilizado para llegar hasta el punto donde se encuentra, en lugar de regresar a la sección inmediata anterior según la numeración
	//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
	public $AutoFitImage;			//Indica si la imagen de la forma en el menú de formas del App se va a autoajustar al tamaño del parea especificada sin mantener la escala original
	public $CopiedSurveyID;			//Si la forma es generada mediante el proceso de copiado de forma entonces esta variable guarda el id de la forma que se copio
	public $FormType;				//0: Producción, 1: Desarrollo, 2:Producción que tiene copias en desarrollo
	public $ActionType;				//0: Creación manual, 1: Copiado de la forma, 2: Generar forma de desarrollo
	//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
	public $SourceID;				//ID del objeto original al que apunta esta instancia para mantener la sincronización entre formas de desarrollo/producción
	//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
	public $QuestionIDForEntryDesc;	//ID de la pregunta de la forma que se usará para definir la descripción de las capturas
	public $ReloadSurveyFromSection;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->ForceNew = false;
		$this->SurveyID = -1;
		$this->SurveyName= "";
		$this->Status = svstInDevelopment;
		$this->ModelID = -1;
		$this->SurveyTable = "";
		$this->ClosingMsg = "";
		$this->CountIndID = -1;
		$this->AnsweredQuestionsIndID = -1;
		$this->DurationIndID = -1;
		//Latitude y Longitude
		$this->LatitudeIndID = -1;
		$this->LongitudeIndID = -1;
		$this->UserDimID = -1;
		$this->SectionDimID = -1;
		$this->CaptureStartTime= "";
		$this->CaptureEndTime= "";
		$this->HelpMsg="";
		$this->CatalogID = 0;
		$this->OldCatalogID = 0;
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		$this->AccuracyAttribID = -1;
		$this->AccuracyIndID = -1;

		//En teoria no se van a ocupar
		$this->AllowMultipleSurveys = 1;
		$this->CaptureStartDate= "";
		$this->CaptureEndDate= "";
		$this->WhenToCaptureType=0;
		$this->WhenToCaptureValue="";
		$this->StartDay = 0;
		$this->Duration = 0;
		$this->TempWeek = array();
		$this->TempMonth = 0;
		$this->TempTwoMonths = 0;
		$this->TempQuarter = 0;

		$this->UserIDs = array();
		$this->UsrRoles = array();
		$this->StrUserRol="";
		$this->SurveyType = svtNormal;
		$this->HasSignature = 0;
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
		$this->SyncWhenSaving = 0;
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		$this->HideNavButtons = 0;
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		$this->HideSectionNames = 0;
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		$this->VersionNum = 0;
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		$this->EnableReschedule = 0;
		$this->RescheduleCatalogID = 0;
		$this->RescheduleCatMemberID = 0;
		$this->RescheduleStatus = '';
		//@LROUX 2011-10-18: Si esta opción esta en 1, el botón de save en la encuesta esta en todas las pantallas intermedias, si esta en 0, se muestra solo al final.
		$this->AllowPartialSave = 0;
		$this->GblSurveyModelID = -1;
		$this->GblSurveyDimID = -1;
                
		//Se agregaron las dimension "Survey Global ID" y "Survey Single Record ID"
		$this->GblSurveyGlobalDimID = -1;
		$this->GblSurveySingleRecordDimID = -1;
		
		$this->GblUserDimID = -1;
		$this->GblStartDateDimID = -1;
		$this->GblEndDateDimID = -1;
		$this->GblStartTimeDimID = -1;
		$this->GblEndTimeDimID = -1;
		$this->GblCountIndID = -1;
		$this->GblAnsweredQuestionsIndID = -1;
		$this->GblDurationIndID = -1;
		$this->GblLatitudeIndID = -1;
		$this->GblLongitudeIndID = -1;
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización
		$this->GblSyncDateDimID = -1;
		$this->GblSyncTimeDimID = -1;
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		$this->GblAccuracyIndID = -1;
		$this->GblAccuracyDimID = -1;
		$this->GblLatitudeDimID = -1;
		$this->GblLongitudeDimID = -1;
		//@JAPR 2014-11-18: Agregada la dimensión Agenda
		$this->GblAgendaDimID = -1;
		//@JAPR
		/*@MABH20121205*/
		$this->GblServerStartDateDimID = -1;
		$this->GblServerEndDateDimID = -1;
		$this->GblServerStartTimeDimID = -1;
		$this->GblServerEndTimeDimID = -1;
		//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
		$this->GblServerStartDateEditDimID = -1;
		$this->GblServerEndDateEditDimID = -1;
		$this->GblServerStartTimeEditDimID = -1;
		$this->GblServerEndTimeEditDimID = -1;
		//@MABH20121205
		//@JAPR 2015-07-09 Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Se agregarán nuevas dimensiones de Latitud y Longitud, ya que las existentes eran dimensiones atributo así que no se podían registrar en una tabla separada
		//para las nuevas tablas de datos de eForms (NO confundir con las dimensiones-atributo originales, se les agregará "Ind" de "Independiente" antes de DimID)
		//@JAPR 2015-07-24: Finalmente NO se agregaron dimensiones independientes para Latitude, Longitude ni Accuracy de la captura, porque la idea original de tener toas las tablas normalizadas
		//ya no aplicó debido a limitantes en la cantidad de joins, adicionalmente EArteaga utilizó dimensiones-atributo para estos valores en los modelos mapeados a partir de las nuevas tablas
		//de eForms v6, por lo que haber creado una nueva tercia de dimensiones habría entrado en conflicto con las dimensiones-atributos existentes del cubo global, así que se optó por
		//seguir utilizando las ya existentes
		//$this->GblLatitudeIndDimID = -1;
		//$this->GblLongitudeIndDimID = -1;
		//$this->GblAccuracyIndDimID = -1;
		//El resto de las dimensiones siguientes son estrictamente nuevas en las formas de V6
		$this->GblStartDateEditDimID = -1;
		$this->GblStartTimeEditDimID = -1;
		$this->GblEndDateEditDimID = -1;
		$this->GblEndTimeEditDimID = -1;
		$this->GblSyncLatitudeDimID = -1;
		$this->GblSyncLongitudeDimID = -1;
		$this->GblSyncAccuracyDimID = -1;
		$this->GblCountryDimID = -1;
		$this->GblStateDimID = -1;
		$this->GblCityDimID = -1;
		$this->GblZipCodeDimID = -1;
		$this->GblAddressDimID = -1;
		$this->GblLastCheckDate = '';
		//@JAPR
		//GCRUZ 2016-05-30. Tiempo y distancia de traslado de la captura
		$this->GblTransferTimeIndID = -1;
		$this->GblTransferDistanceIndID = -1;
		
		$this->EmailDimID = -1;
		$this->StatusDimID = -1;
		$this->SchedulerDimID = -1;
		$this->FactKeyDimID = -1;
		$this->LatitudeAttribID = -1;
		$this->LongitudeAttribID = -1;
		$this->ActionIndInlineID = -1;
		//@JAPR 2014-11-18: Agregada la dimensión Agenda
		$this->AgendaDimID = -1;
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
		$this->ElementQuestionID = 0;
		$this->eBavelAppID = 0;
		$this->eBavelFormID = 0;
		//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$this->eBavelAppIDOld = 0;
		$this->eBavelFormIDOld = 0;
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$this->eBavelFieldsData = '';
		$this->eBavelFieldsDataOld = '';
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		$this->eBavelActionFormID = 0;
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		//@JAPR
		
		$this->StartDateDimID = -1;
		$this->EndDateDimID = -1;
		$this->StartTimeDimID = -1;
		$this->EndTimeDimID = -1;
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización
		$this->SyncDateDimID = -1;
		$this->SyncTimeDimID = -1;
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		$this->UseStdSectionSingleRec = 1;
		$this->UseSourceFields = false;
		$this->UseRecVersionField = false;
		$this->UseDynamicPageField = false;
		$this->SpecialFieldsRead = false;
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->UseDynamicOptionField = false;
		//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
		$this->UseNULLForEmptyNumbers = true;
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->DissociateCatDimens = 1;
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$this->OtherLabel = '';
		$this->CommentLabel = '';
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		$this->AdditionalEMails = '';
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		$this->SurveyImageText = '';
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		$this->SurveyMenuImage;
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		$this->DisableEntry = 0;
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$this->CreationAdminVersion = 0;
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = 0;
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$this->FactTable = '';
		//@JAPR
		/*@MABH20121205*/
		$this->ServerStartDateDimID = -1;
		$this->ServerEndDateDimID = -1;
		$this->ServerStartTimeDimID = -1;
		$this->ServerEndTimeDimID = -1;
		//@MABH20121205
	
		$this->IsAgendable = 0;
		$this->RadioForAgenda = 0;
		//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
		$this->SchedulerID = 0;
		$this->SurveyDesc = '';

		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		$this->incrementalFile = '';
		//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
		$this->Width = 0;
		$this->Height = 0;
		$this->TextPosition = svtxpDefault;
		$this->TextSize = '';
		$this->CustomLayout = '';
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		$this->ShowQNumbers = -1;
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		$this->TemplateID = -1;
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		$this->PlannedDuration = 0;
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		//JAPR 2016-09-07: Corregido un bug, no estaba utilizando correctamente el default de la configuración (#WWIJLY)
		$this->EnableNavHistory = -1;
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		$this->AutoFitImage = 0;
		$this->CopiedSurveyID = 0;
		$this->FormType = formProduction; 	//La forma al crearse tendrá como tipo "Producción"
		$this->ActionType = createAction;
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$this->SourceID = 0;
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		$this->QuestionIDForEntryDesc = 0;
		$this->ReloadSurveyFromSection = 0;
	}

	static function NewInstance($aRepository)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}

	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	//Agregado el parámetro $bFromCollection para indicar que se está cargando la instancia desde la colección, si es así, no realiza la carga
	//de algunos elementos adicionales ni otros procesos que podrían resultar muy tardados
	static function NewInstanceWithID($aRepository, $aSurveyID, $bFromCollection = false)
	{
		//@JAPR 2013-06-05: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance = null;
		
		if (((int)  $aSurveyID) < 0)
		{
			return $anInstance;
		}
		
		$anInstance =& BITAMGlobalFormsInstance::GetSurveyInstanceWithID($aSurveyID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

    	//@LROUX 2011-10-18: Se agrega AllowPartialSave.
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', t1.SyncWhenSaving';
		}
		/*@MABH20121205*/
		
		if (getMDVersion() >= esvServerDateTime) {
			$strAdditionalFields .= ", ServerStartDateDimID, ServerEndDateDimID, ServerStartTimeDimID, ServerEndTimeDimID";
		}
		//@MABH20121205
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$strAdditionalFields .= ', t1.UseStdSectionSingleRec';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', t1.DissociateCatDimens';
		}
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strAdditionalFields .= ', t1.OtherLabel, t1.CommentLabel';
		}
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$strAdditionalFields .= ', t1.eBavelActionFormID';
		}
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$strAdditionalFields .= ', t1.AccuracyAttribID, t1.AccuracyIndID';
		}
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		if (getMDVersion() >= esvReportEMailsByCatalog) {
			$strAdditionalFields .= ', t1.AdditionalEMails';
		}
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', t1.SurveyImageText';
		}
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if  (getMDVersion() >= esvMenuImageConfig) {
			$strAdditionalFields .= ', t1.SurveyMenuImage';
		}
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		if (getMDVersion() >= esvHideNavigationButtons) {
			$strAdditionalFields .= ', t1.HideNavButtons, t1.HideSectionNames';
		}
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.DisableEntry';
		}
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion';
		}
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.IsAgendable';
			$strAdditionalFields .= ', t1.RadioForAgenda';
		}
		//@JAPR 2014-11-18: Agregada la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID) {
			$strAdditionalFields .= ', t1.AgendaDimID';
		}
		//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.SchedulerID, t1.SurveyDesc';
		}
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		if (getMDVersion() >= esvDestinationIncremental) {
			$strAdditionalFields .= ', t1.incrementalFile';
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', t1.Width, t1.Height, t1.TextPosition, t1.TextSize, t1.CustomLayout';
		}
		//@JAPR
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if(getMDVersion() >= esvShowQNumbers){
			$strAdditionalFields .= ', t1.ShowQNumbers';
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$strAdditionalFields .= ', t1.TemplateID';
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		if (getMDVersion() >= esvFormsDuration) {
			$strAdditionalFields .= ', t1.PlannedDuration';
		}
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$strAdditionalFields .= ', t1.EnableNavHistory';
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$strAdditionalFields .= ', t1.AutoFitImage';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID, t1.FormType';
		}
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (getMDVersion() >= esvEntryDescription) {
			$strAdditionalFields .= ', t1.QuestionIDForEntryDesc';
		}
		if ( getMDVersion() >= esvReloadSurveyFromSection ) {
			$strAdditionalFields .= ', t1.ReloadSurveyFromSection';
		}
		//@JAPR
		$sql = "SELECT t1.SurveyID, t1.SurveyName, t1.Status, t1.ModelID, t1.ClosingMsg, t1.CountIndID, t1.AnsweredQuestionsIndID, t1.DurationIndID, t1.LatitudeIndID,t1.LongitudeIndID, t1.UserDimID, 
				t1.SectionDimID, t1.CaptureStartTime, t1.CaptureEndTime, t1.HelpMsg, t1.CatalogID, t1.SurveyType, t1.HasSignature, t1.AllowPartialSave, t1.EmailDimID, t1.StatusDimID, t1.SchedulerDimID,
				t1.StartDateDimID, t1.EndDateDimID, t1.StartTimeDimID, t1.EndTimeDimID, t1.SyncDateDimID, t1.SyncTimeDimID, 
				t1.FactKeyDimID, t1.LatitudeAttribID, t1.LongitudeAttribID, t1.ActionIndInlineID, t1.VersionNum, t1.ElementQuestionID, t1.eBavelAppID, t1.eBavelFormID, t1.EnableReschedule,  
				t1.RescheduleCatalogID, t1.RescheduleCatMemberID, t1.RescheduleStatus $strAdditionalFields 
			FROM SI_SV_Survey t1 WHERE t1.SurveyID = ".((int)$aSurveyID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
		    if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		}
		if (!$aRS->EOF)
		{
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS, $bFromCollection);
		}
		return $anInstance;
	}

	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	//Agregado el parámetro $bFromCollection para indicar que se está cargando la instancia desde la colección, si es así, no realiza la carga
	//de algunos elementos adicionales ni otros procesos que podrían resultar muy tardados
	static function NewInstanceFromRS($aRepository, $aRS, $bFromCollection = false)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
		//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
		$anInstance->SurveyName = trim((string) $aRS->fields["surveyname"]);
		//@JAPR 2016-07-27: Validado que si no se grabó el nombre de forma, se genere uno automáticamente
		if ($anInstance->SurveyName == '') {
			$anInstance->SurveyName = "(NoNameSurvey)";
		}
		//@JAPR
		$anInstance->SurveyType = (int) $aRS->fields["surveytype"];
		$anInstance->Status = (int)$aRS->fields["status"];
		$anInstance->ModelID = (int)$aRS->fields["modelid"];
		$anInstance->SurveyTable = "SVSurvey_".$anInstance->ModelID;
		$anInstance->ClosingMsg = $aRS->fields["closingmsg"];
		$anInstance->CountIndID = (int)$aRS->fields["countindid"];
		$anInstance->AnsweredQuestionsIndID = (int) $aRS->fields["answeredquestionsindid"];
		$anInstance->DurationIndID = (int)$aRS->fields["durationindid"];
		//Latitude y Longitude
		$anInstance->LatitudeIndID = (int)$aRS->fields["latitudeindid"];
		$anInstance->LongitudeIndID = (int)$aRS->fields["longitudeindid"];
		$anInstance->ActionIndInlineID = (int)$aRS->fields["actionindinlineid"];
		$anInstance->UserDimID = (int)$aRS->fields["userdimid"];
		$anInstance->SectionDimID = (int)$aRS->fields["sectiondimid"];
		$anInstance->CaptureStartTime = $aRS->fields["capturestarttime"];
		$anInstance->CaptureEndTime = $aRS->fields["captureendtime"];
		$anInstance->HelpMsg = $aRS->fields["helpmsg"];
		$anInstance->CatalogID = (int)$aRS->fields["catalogid"];
		$anInstance->OldCatalogID = (int)$aRS->fields["catalogid"];
		$anInstance->HasSignature = (int)$aRS->fields["hassignature"];
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$anInstance->AccuracyAttribID = (int) @$aRS->fields["accuracyattribid"];
			$anInstance->AccuracyIndID = (int) @$aRS->fields["accuracyindid"];
		}
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
		if (getMDVersion() >= esvExtendedActionsData) {
			$anInstance->SyncWhenSaving = (int) @$aRS->fields["syncwhensaving"];
		}
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		$anInstance->HideNavButtons = (int) @$aRS->fields["hidenavbuttons"];
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		$anInstance->HideSectionNames = (int) @$aRS->fields["hidesectionnames"];
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//@JAPR 2013-03-11: Modificado para que esta opción solo aplique en encuestas nuevas
		//if (getMDVersion() >= esveFormsStdQSingleRec) {
		$anInstance->UseStdSectionSingleRec = (int) @$aRS->fields["usestdsectionsinglerec"];
		//}
		//@JAPR 2013-03-19: Variable para forzar al uso de registro único (sólo activar este código si se quiere un servicio eSurveyV4QA para 
		//actualización a esta funcionalidad)
	    //global $ActivateSingleRecordConfigForUpdate;
	    //$anInstance->UseStdSectionSingleRec = (int) @$ActivateSingleRecordConfigForUpdate;
	    
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		$anInstance->VersionNum = (int) $aRS->fields["versionnum"];
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		$anInstance->EnableReschedule = (int) $aRS->fields["enablereschedule"];
		$anInstance->RescheduleCatalogID = (int) $aRS->fields["reschedulecatalogid"];
		$anInstance->RescheduleCatMemberID = (int) $aRS->fields["reschedulecatmemberid"];
		$anInstance->RescheduleStatus = (string) $aRS->fields["reschedulestatus"];
		//@LROUX 2011-10-18: Si esta opción esta en 1, el botón de save en la encuesta esta en todas las pantallas intermedias, si esta en 0, se muestra solo al final.
		$anInstance->AllowPartialSave = (int)$aRS->fields["allowpartialsave"];
		$anInstance->EmailDimID = (int)$aRS->fields["emaildimid"];
		$anInstance->StatusDimID = (int)$aRS->fields["statusdimid"];
		$anInstance->SchedulerDimID = (int)$aRS->fields["schedulerdimid"];
		$anInstance->FactKeyDimID = (int)$aRS->fields["factkeydimid"];
		
		/*Se agregaron las 4 dimensiones StartDate, EndDate, StartTime, EndTime*/
		$anInstance->StartDateDimID = (int)$aRS->fields["startdatedimid"];
		$anInstance->EndDateDimID = (int)$aRS->fields["enddatedimid"];
		$anInstance->StartTimeDimID = (int)$aRS->fields["starttimedimid"];
		$anInstance->EndTimeDimID = (int)$aRS->fields["endtimedimid"];
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
		$anInstance->SyncDateDimID = (int) @$aRS->fields["syncdatedimid"];
		$anInstance->SyncTimeDimID = (int) @$aRS->fields["synctimedimid"];
		//@JAPR
		/*@MABH20121205*/
		if (getMDVersion() >= esvServerDateTime) {
			$anInstance->ServerStartDateDimID = (int) @$aRS->fields["serverstartdatedimid"];
			$anInstance->ServerEndDateDimID = (int) @$aRS->fields["serverenddatedimid"];
			$anInstance->ServerStartTimeDimID = (int) @$aRS->fields["serverstarttimedimid"];
			$anInstance->ServerEndTimeDimID = (int) @$aRS->fields["serverendtimedimid"];
		}
		//@MABH20121205
		
		$anInstance->LatitudeAttribID = (int)$aRS->fields["latitudeattribid"];
		$anInstance->LongitudeAttribID = (int)$aRS->fields["longitudeattribid"];
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action
		$anInstance->ElementQuestionID = (int) $aRS->fields["elementquestionid"];
		$anInstance->eBavelAppID = (int) $aRS->fields["ebavelappid"];
		$anInstance->eBavelFormID = (int) $aRS->fields["ebavelformid"];
		//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$anInstance->eBavelAppIDOld = $anInstance->eBavelAppID;
		$anInstance->eBavelFormIDOld = $anInstance->eBavelFormID;
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		$anInstance->eBavelActionFormID = (int) @$aRS->fields["ebavelactionformid"];
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$anInstance->DissociateCatDimens = (int) @$aRS->fields["dissociatecatdimens"];
		//@JAPR 2013-02-05: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$anInstance->FactTable = "RIFACT_".$anInstance->ModelID;
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		$anInstance->OtherLabel = (string) @$aRS->fields["otherlabel"];
		$anInstance->CommentLabel = (string) @$aRS->fields["commentlabel"];
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		$anInstance->AdditionalEMails = (string) @$aRS->fields["additionalemails"];
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		$anInstance->SurveyImageText = (string) @$aRS->fields["surveyimagetext"];
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		$anInstance->SurveyMenuImage = (string) @$aRS->fields["surveymenuimage"];
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		$anInstance->DisableEntry = (int) @$aRS->fields["disableentry"];
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR 2014-11-18: Agregada la dimensión Agenda
		$anInstance->AgendaDimID = (int) @$aRS->fields["agendadimid"];
		//@JAPR
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		$anInstance->incrementalFile = (string) @$aRS->fields["incrementalFile"];
		
		//@AAL 27/05/2015 Modificado: en versiones de eForms 6.0 las dimensiones no aplican
		$anInstance->SurveyTable = ($anInstance->CreationAdminVersion >= esveFormsv6) ? "SVeForms_" . $anInstance->SurveyID : "SVSurvey_" . $anInstance->ModelID;
		
		//Se lee el contenido de la tabla SI_SV_GblSurveyModel
		$anInstance->readGblSurveyModel();
		
		//@JAPR 2013-04-29: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		//@JAPR 2013-05-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6) {
			if (getMDVersion() >= esveBavelMapping) {
				$anInstance->readeBavelFieldsMapping();
			}
			//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$anInstance->readeBavelActionFieldsMapping();
			}
		}

		//@AAL 27/05/2015 Modificado, solo en versiones de Eforms menores a 6 se utilizaran las dimensiones
		//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
		//Se bloqueará este proceso en este punto para que sólo se ejecute cuando se grabe alguna forma y bajo demanda, no mientras se cargan las formas
		/*
		if ($anInstance->CreationAdminVersion < esveFormsv6){
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			if (!$bFromCollection && getMDVersion() >= esvAccuracyValues) {
				//Si está cargando en forma independiente, realiza una revisión de la instancia para verificar si le falta o no crear algunas
				//columnas o dimensiones/indicadores adicionales que se van agregando con el paso de las versiones
				@$anInstance->checkIntegrity();
			}
		}
		*/
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->IsAgendable = (int) @$aRS->fields["isagendable"];
			$anInstance->RadioForAgenda = (int) @$aRS->fields["radioforagenda"];
		}
		//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$anInstance->SchedulerID = (int) @$aRS->fields["schedulerid"];
			$anInstance->SurveyDesc = (string) @$aRS->fields["surveydesc"];
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		$anInstance->Width = (int) @$aRS->fields["width"];
		$anInstance->Height = (int) @$aRS->fields["height"];
		$anInstance->TextPosition = (int) @$aRS->fields["textposition"];
		$anInstance->TextSize = (string) @$aRS->fields["textsize"];
		$anInstance->CustomLayout = (string) @$aRS->fields["customlayout"];
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if(getMDVersion() >= esvShowQNumbers){
			$anInstance->ShowQNumbers = (int) @$aRS->fields["showqnumbers"];
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		$anInstance->PlannedDuration = (float) @$aRS->fields["plannedduration"];
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		$anInstance->EnableNavHistory = (int) @$aRS->fields["enablenavhistory"];
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		$anInstance->AutoFitImage = (int) @$aRS->fields["autofitimage"];
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		$anInstance->SourceID = (int) @$aRS->fields["sourceid"];
		$anInstance->FormType = (int) @$aRS->fields["formtype"];
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		$anInstance->QuestionIDForEntryDesc = (int) @$aRS->fields["questionidforentrydesc"];
		$anInstance->ReloadSurveyFromSection = (int) @$aRS->fields["reloadsurveyfromsection"];
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$anInstance->setTableAndFieldsProperties();
		//@JAPR
		
		BITAMGlobalFormsInstance::AddSurveyInstanceWithID($anInstance->SurveyID, $anInstance);
		
		return $anInstance;
	}
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	/* Asigna las propiedades que contienen los nombres de tablas y campos a utilizar durante el proceso de generación de las tablas de datos o de grabado
	de las capturas realizadas para las formas de v6. Si la forma fuera previa a v4 y la propiedad hubiera existido previamente, entonces le deja el mismo
	nombre que originalmente se utilizaba */
	function setTableAndFieldsProperties() {
		if ($this->CreationAdminVersion >= esveFormsv6) {
			//$this->KeyField = "SurveyKey";
			$this->SurveyTable = "SVeForms_{$this->SurveyID}";
			$this->SurveyStdTable = "SVeForms_{$this->SurveyID}_SectionStd";
		}
		else {
			$this->SurveyTable = "SVSurvey_{$this->ModelID}";
		}
	}
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	/*	Realiza una revisión de integridad de la encuesta, verificando que las nuevas dimensiones e indicadores (según la versión de la metadata)
	se encuentren correctamente creadas, de no ser así, las crea en este intante. Estos procesos anteriormente se han realizado vía scripts
	de PhP que se tienen que ejecutar directamente, pero ha sucedido que en ocasiones durante las actualizaciones no los ejecutaban, así que
	ahora simplemente se realizará la revisión durante la carga de las encuestas
	//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
	A partir de la V6 los viejos métodos de creación de dimensiones e indicadores se removerán, ya que había duplicidad (había uno para crear cuando se agregaba el cubo global de surveys
	y otro para crear cuando se copiaba una forma), todos quedarán colgados de este único método sea o no una nueva forma o incluso si sólo se está cargando una instancia, eso para permitir
	corregir errores de consistencia en caso de que se dañe algo o se quisiera reparar mediante el producto (se limpia el campo de ID de dimensión correspondiente y eForms lo crearía)
	*/
	function checkIntegrity() {
		global $generateXLS;
		global $gblEFormsNAKey;
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		global $gblModMngrErrorMessage;
		global $intCreatedBy;
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Check integrity {$this->SurveyID}");
		}
		
		if ($this->CreationAdminVersion < esveFormsv6) {	
			return;
		}
		
		//Primero verificaría si ya existe el Modelo definido, si no es así entonces lo creará
		$this->readGblSurveyModel();
		if ($this->GblLastCheckDate != '' && unformatDate($this->GblLastCheckDate, "") == unformatDate(today(), "")) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Integrity was checked this day already");
			}
			return;
		}
		
		$strOriginalWD = getcwd();
		//Agrega el API necesaria del Model Manager
		//@JAPR 2015-07-24: Esto ya no es necesario porque se invocará a una función que realiza este proceso internamente, así se centralizará la creación de dimensiones
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		//Procedemos a crear el modelo
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/attribdimension.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicator.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		//Bloquea durante todo el proceso la generación del template XLS del Model Manager para que en caso de necesitar cambiar algo, no lo esté generando con cada cambio
		$generateXLS = false;
		
		//*****************************************************************************************************************************************************************
		//Modelo
		if ($this->GblSurveyModelID > 0) {
			//Si el modelo ya estaba asignado, sólo verifica que si exista
			$anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
			if (is_null($anInstanceModel)) {
				chdir($strOriginalWD);
				$gblEFormsErrorMessage = "(".__METHOD__.") (SurveyID: {$this->SurveyID})".translate("There was an error loading the surveys model, please contact Technical Support")." (ModelID: {$this->GblSurveyModelID})";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Model exists already");
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Creating new surveys model");
			}
			
			//Debe crear el modelo global de surveys
			$modelName = "Surveys";
			$anInstanceModel = BITAMModel::NewModel($this->Repository);
			$anInstanceModel->ModelName = $modelName;
			$anInstanceModel->readPeriods();
			//@JAPR 2013-03-19: Agregada la propiedad para impedir que el Model Manager manipule al modelo creado para eForms
			@$anInstanceModel->ri_definition = $intCreatedBy;
			
			//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModel->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating global survey model").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			//@JAPR
			
			//Ahora al crear el modelo creará el registro, y por cada dimensión se hará el UPDATE separado para permitir su actualización/reparación
			$this->GblSurveyModelID = $anInstanceModel->ModelID;
			$sql = "INSERT INTO SI_SV_GblSurveyModel (GblSurveyModelID) VALUES ({$this->GblSurveyModelID})";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}
		
		//*****************************************************************************************************************************************************************
		// Dimensiones y elementos Pre-existentes a la versión v6
		//*****************************************************************************************************************************************************************
		//Crear la dimensión Survey (utiliza llave)
		$strPropertyName = "GblSurveyDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Surveys";
			$sKeyValue = -99999;	//Por consistencia con la V5. Asignando un valor !== false como este se creará la dimensión con llave
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SurveyDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
		//Insertamos las formas existentes en el repositorio, se hará siempre para mantener la integridad de la dimensión, la cual se debe llenar al agregar/renombrar formas
		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->GblSurveyModelID, -1, $this->{$strPropertyName});
		if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
			BITAMSurvey::insertSurveysDimensionValues($this->Repository, $anInstanceModelDim);
		}
		
		//Crear la dimensión Survey Survey Global ID (No utilizada en v6 igual que en versiones anteriores, ya que sólo existe un único registro de todas maneras, sigue representando
		//el ID de una captura)
		$strPropertyName = "GblSurveyGlobalDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Survey Global ID";
			$sKeyValue = false;
			//En versiones previas esta dimensión no tenía valor de NA, pero eso estaba mal, se agregará uno con -1 para compatibilidad hacia atrás
			$intNAValue = -1;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SurveyGlobalDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Survey Single Record ID (No utilizada en v6 en absoluto, ya que no existen múltiples registros por captura en la tabla de la forma y porque no hay modelo
		//equivalente donde se guarden los datos, así que simplemente repetirá el valor de la dimensión GblSurveyGlobalDimID que es lo mas parecido)
		$strPropertyName = "GblSurveySingleRecordDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Survey Single Record ID";
			$sKeyValue = false;
			//En versiones previas esta dimensión no tenía valor de NA, pero eso estaba mal, se agregará uno con -1 para compatibilidad hacia atrás
			$intNAValue = -1;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SurveySingleRecordDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension User (esta dimensión es global a todos los productos, o al menos podría serlo, debe ser una representación casi fiel de SI_USUARIO en cuando al que el Key
		//corresponde con SI_USUARIO, sin embargo como dimensión tiene un valor de No Aplica que dado el origen de la tabla es -1)
		//equivalente donde se guarden los datos, así que simplemente repetirá el valor de la dimensión GblSurveyGlobalDimID que es lo mas parecido)
		$strPropertyName = "GblUserDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "User";
			$sKeyValue = false;
			//Esta dimensión es especial, su valor de NA no es el mismo que el estándar de eForms por ser un espejo de otra tabla
			$intNAValue = -1;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "UserDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
		//Insertamos los usuarios de kpionline inscritos al repositorio, se hará siempre porque era un error común tener problemas de integridad en esta dimensión
		$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->GblSurveyModelID, -1, $this->{$strPropertyName});
		if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
			BITAMSurvey::insertUserDimensionValues($this->Repository, $anInstanceModelDim);
		}
		
	    //Crea la dimension Start Date
		$strPropertyName = "GblStartDateDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Start Date";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "StartDateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension End Date
		$strPropertyName = "GblEndDateDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "End Date";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "EndDateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Start Time
		$strPropertyName = "GblStartTimeDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Start Time";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "StartTimeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension End Time
		$strPropertyName = "GblEndTimeDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "End Time";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "EndTimeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Server Start Date
		$strPropertyName = "GblServerStartDateDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Server Start Date";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerStartDateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Server End Date
		$strPropertyName = "GblServerEndDateDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Server End Date";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerEndDateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Server Start Time
		$strPropertyName = "GblServerStartTimeDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Server Start Time";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerStartTimeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Server End Time
		$strPropertyName = "GblServerEndTimeDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Server End Time";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerEndTimeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
		//Crea el indicador COUNT
		$strPropertyName = "GblCountIndID";
		if ($this->{$strPropertyName} <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing indicator detected: {$strPropertyName}");
			}
			
			$anInstanceIndicator = BITAMIndicator::NewIndicator($this->Repository);
			$anInstanceIndicator->CubeID = $this->GblSurveyModelID;
			$anInstanceIndicator->IndicatorName = 'COUNT';
			$anInstanceIndicator->formula_bd = "COUNT(DISTINCT t1.RIDIM_".$this->GblSurveyGlobalDimID."KEY)";
			$anInstanceIndicator->formato = "#,##0";
			$anInstanceIndicator->is_custom = 1;
			$anInstanceIndicator->no_ejecutivo = 0;
			$objMMAnswer = @$anInstanceIndicator->save();
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating COUNT indicator").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
			$sql = "UPDATE SI_SV_GblSurveyModel SET CountIndID = ".$this->{$strPropertyName}." 
				WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}
		
		//Crea el indicador Duration
		$strPropertyName = "GblDurationIndID";
		if ($this->{$strPropertyName} <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing indicator detected: {$strPropertyName}");
			}
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
			$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
			$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
			$anInstanceIndicator->IndicatorName = 'Duration';
			$anInstanceIndicator->formato = "#,##0";
			$anInstanceIndicator->agrupador = "SUM";
			$anInstanceIndicator->sql_source = "SUM";
			$objMMAnswer = @$anInstanceIndicator->save();
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating duration indicator").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
			$sql = "UPDATE SI_SV_GblSurveyModel SET DurationIndID = ".$this->{$strPropertyName}." 
				WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}
		
		//Crea el indicador AnsweredQuestions
		$strPropertyName = "GblAnsweredQuestionsIndID";
		if ($this->{$strPropertyName} <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing indicator detected: {$strPropertyName}");
			}
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
			$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
			$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
			$anInstanceIndicator->IndicatorName = "AnsweredQuestions";
			$anInstanceIndicator->formato = '#,##0';
			$anInstanceIndicator->agrupador = 'SUM';
			$anInstanceIndicator->sql_source = 'SUM';
			$objMMAnswer = @$anInstanceIndicator->save();
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating answered questions indicator").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
			$sql = "UPDATE SI_SV_GblSurveyModel SET AnsweredQuestionsIndID = ".$this->{$strPropertyName}." 
				WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}
		
		//Crea el indicador Latitude
		$strPropertyName = "GblLatitudeIndID";
		if ($this->{$strPropertyName} <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing indicator detected: {$strPropertyName}");
			}
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
			$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
			$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
			$anInstanceIndicator->IndicatorName = "Latitude";
			$anInstanceIndicator->formato = '#,##0.000000';
			$anInstanceIndicator->agrupador = 'AVG';
			$anInstanceIndicator->sql_source = 'AVG';
			$objMMAnswer = @$anInstanceIndicator->save();
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating latitude indicator").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
			$sql = "UPDATE SI_SV_GblSurveyModel SET LatitudeIndID = ".$this->{$strPropertyName}." 
				WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}

		//Crear el indicador Longitude
		$strPropertyName = "GblLongitudeIndID";
		if ($this->{$strPropertyName} <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing indicator detected: {$strPropertyName}");
			}
			
			$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
			$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
			$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
			$anInstanceIndicator->IndicatorName = "Longitude";
			$anInstanceIndicator->formato = '#,##0.000000';
			$anInstanceIndicator->agrupador = 'AVG';
			$anInstanceIndicator->sql_source = 'AVG';
			$objMMAnswer = @$anInstanceIndicator->save();
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating longitude indicator").": ".$strMMError.". ";
				$gblEFormsErrorMessage = $gblModMngrErrorMessage;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			
			$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
			$sql = "UPDATE SI_SV_GblSurveyModel SET LongitudeIndID = ".$this->{$strPropertyName}." 
				WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
			if (!$this->Repository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
		}
		
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		$cla_descrip_parent = $this->GblSurveyGlobalDimID;
		$cla_concepto = $this->GblSurveyModelID;
		if (getMDVersion() >= esvAccuracyValues && $cla_descrip_parent > 0) {
			//Crea el atributo Latitud (este no se puede crear con la función que crea dimensiones porque es un atributo de otra dimensión y es un proceso ligeramente diferente)
			$strPropertyName = "GblLatitudeDimID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing attribute detected: {$strPropertyName}");
				}
				
				$cla_descrip = -1;
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
				$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
				$anInstanceAttrib->Dimension->DimensionName = "Latitude";
				$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
				$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
				$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
				$objMMAnswer = @$anInstanceAttrib->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating Latitude attribute").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceAttrib->Dimension->DimensionClaDescrip;
				$sql = "UPDATE SI_SV_GblSurveyModel SET LatitudeDimID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}
			
			//Crea el atributo Longitud
			$strPropertyName = "GblLongitudeDimID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing attribute detected: {$strPropertyName}");
				}
				
				$cla_descrip = -1;
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
				$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
				$anInstanceAttrib->Dimension->DimensionName = "Longitude";
				$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
				$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
				$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
				$objMMAnswer = @$anInstanceAttrib->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating Longitude attribute").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceAttrib->Dimension->DimensionClaDescrip;
				$sql = "UPDATE SI_SV_GblSurveyModel SET LongitudeDimID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}
			
			//Crea el atributo Accuracy
			$strPropertyName = "GblAccuracyDimID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing attribute detected: {$strPropertyName}");
				}
				
				$cla_descrip = -1;
				//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
				//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
				$anInstanceAttrib = BITAMAttribDimension::NewAttribDimension($this->Repository, $cla_concepto, $cla_descrip_parent);
				$anInstanceAttrib->Dimension->DimensionName = "Accuracy";
				$anInstanceAttrib->Dimension->ParentDimensionID = $cla_descrip_parent;
				$anInstanceAttrib->Dimension->DimensionClaDescrip = $cla_descrip;
				$anInstanceAttrib->Dimension->DimensionID = $cla_descrip;
				$objMMAnswer = @$anInstanceAttrib->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating Accuracy attribute").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceAttrib->Dimension->DimensionClaDescrip;
				$sql = "UPDATE SI_SV_GblSurveyModel SET AccuracyAttribID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}
			
			//Crea el indicador Accuracy
			$strPropertyName = "GblAccuracyIndID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing indicator detected: {$strPropertyName}");
				}
				
				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
				$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
				$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
				$anInstanceIndicator->IndicatorName = "Accuracy";
				$anInstanceIndicator->formato = '#,##0';
				$anInstanceIndicator->agrupador = 'AVG';
				$anInstanceIndicator->sql_source = 'AVG';
				$objMMAnswer = @$anInstanceIndicator->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating Accuracy indicator").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
				$sql = "UPDATE SI_SV_GblSurveyModel SET AccuracyIndID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}
		}
		
	    //Crea la dimension Sync Date
		$strPropertyName = "GblSyncDateDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Sync Date";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncDateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
	    //Crea la dimension Sync Time
		$strPropertyName = "GblSyncTimeDimID";
		if ($this->{$strPropertyName} <= 0 ) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Missing dimension detected: {$strPropertyName}");
			}
			
			$dimensionName = "Sync Time";
			$sKeyValue = false;
			$intNAValue = $gblEFormsNAKey;
			$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncTimeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
			if ($cla_descrip <= 0) {
				$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
				if ($gblShowErrorSurvey) {
					die ($gblEFormsErrorMessage);
				}
				else {
					return $gblEFormsErrorMessage;
				}
			}
			$this->{$strPropertyName} = $cla_descrip;
		}
		
		//Crea la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID) {
			$strPropertyName = "GblAgendaDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Agenda";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "AgendaDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
		}
		//@JAPR

		//*****************************************************************************************************************************************************************
		// Dimensiones y elementos Exclusivos de v6+
		//*****************************************************************************************************************************************************************
		if (getMDVersion() >= esveFormsv6) {
			//Crea la dimensión de la Fecha inicial de edición
			$strPropertyName = "GblStartDateEditDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Start Date Edit";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "StartDateEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora inicial de edición
			$strPropertyName = "GblStartTimeEditDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Start Time Edit";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "StartTimeEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Fecha final de edición
			$strPropertyName = "GblEndDateEditDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "End Date Edit";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "EndDateEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora final de edición
			$strPropertyName = "GblEndTimeEditDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "End Time Edit";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "EndTimeEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}

			//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
			if (getMDVersion() >= esvServerEditDates) {
				//Crea la dimension Fecha inicial de edición según el server
				$strPropertyName = "GblServerStartDateEditDimID";
				if ($this->{$strPropertyName} <= 0 ) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Missing dimension detected: {$strPropertyName}");
					}
					
					$dimensionName = "Server Start Date Edit";
					$sKeyValue = false;
					$intNAValue = $gblEFormsNAKey;
					$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerStartDateEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
					if ($cla_descrip <= 0) {
						$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
						if ($gblShowErrorSurvey) {
							die ($gblEFormsErrorMessage);
						}
						else {
							return $gblEFormsErrorMessage;
						}
					}
					$this->{$strPropertyName} = $cla_descrip;
				}
				
				//Crea la dimension Fecha final de edición según el server
				$strPropertyName = "GblServerEndDateEditDimID";
				if ($this->{$strPropertyName} <= 0 ) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Missing dimension detected: {$strPropertyName}");
					}
					
					$dimensionName = "Server End Date Edit";
					$sKeyValue = false;
					$intNAValue = $gblEFormsNAKey;
					$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerEndDateEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
					if ($cla_descrip <= 0) {
						$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
						if ($gblShowErrorSurvey) {
							die ($gblEFormsErrorMessage);
						}
						else {
							return $gblEFormsErrorMessage;
						}
					}
					$this->{$strPropertyName} = $cla_descrip;
				}
				
				//Crea la dimension Hora inicial de edición según el server
				$strPropertyName = "GblServerStartTimeEditDimID";
				if ($this->{$strPropertyName} <= 0 ) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Missing dimension detected: {$strPropertyName}");
					}
					
					$dimensionName = "Server Start Time Edit";
					$sKeyValue = false;
					$intNAValue = $gblEFormsNAKey;
					$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerStartTimeEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
					if ($cla_descrip <= 0) {
						$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
						if ($gblShowErrorSurvey) {
							die ($gblEFormsErrorMessage);
						}
						else {
							return $gblEFormsErrorMessage;
						}
					}
					$this->{$strPropertyName} = $cla_descrip;
				}
				
				//Crea la dimension Hora final de edición según el server
				$strPropertyName = "GblServerEndTimeEditDimID";
				if ($this->{$strPropertyName} <= 0 ) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Missing dimension detected: {$strPropertyName}");
					}
					
					$dimensionName = "Server End Time Edit";
					$sKeyValue = false;
					$intNAValue = $gblEFormsNAKey;
					$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ServerEndTimeEditDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
					if ($cla_descrip <= 0) {
						$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
						if ($gblShowErrorSurvey) {
							die ($gblEFormsErrorMessage);
						}
						else {
							return $gblEFormsErrorMessage;
						}
					}
					$this->{$strPropertyName} = $cla_descrip;
				}
			}
			//@JAPR
			
			//Crea la dimensión de la Fecha final de sincronización
			$strPropertyName = "GblSyncEndDateDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "End Date Sync";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncDateEndDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora final de sincronización
			$strPropertyName = "GblSyncEndTimeDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "End Time Sync";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncTimeEndDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora final de sincronización
			$strPropertyName = "GblSyncLatitudeDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Sync latitude";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncLatitudeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora final de sincronización
			$strPropertyName = "GblSyncLongitudeDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Sync longitude";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncLongitudeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Hora final de sincronización
			$strPropertyName = "GblSyncAccuracyDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Sync accuracy";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "SyncAccuracyDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión del País de la captura basado en el GPS
			$strPropertyName = "GblCountryDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Country";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "CountryDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión del Estado de la captura basado en el GPS
			$strPropertyName = "GblStateDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "State";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "StateDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Ciudad de la captura basada en el GPS
			$strPropertyName = "GblCityDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "City";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "CityDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión del Código postal de la captura basado en el GPS
			$strPropertyName = "GblZipCodeDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "ZipCode";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "ZipCodeDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}
			
			//Crea la dimensión de la Dirección de la captura basada en el GPS
			$strPropertyName = "GblAddressDimID";
			if ($this->{$strPropertyName} <= 0 ) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing dimension detected: {$strPropertyName}");
				}
				
				$dimensionName = "Address";
				$sKeyValue = false;
				$intNAValue = $gblEFormsNAKey;
				$cla_descrip = BITAMSurvey::getDimClaDescripFromGblModel($this->Repository, "AddressDimID", $dimensionName, true, null, $sKeyValue, $intNAValue, $this, $strPropertyName);
				if ($cla_descrip <= 0) {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".str_ireplace("{var1}", $strPropertyName, translate("Error creating {var1} dimension")).": ".$gblEFormsErrorMessage.". ";
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				$this->{$strPropertyName} = $cla_descrip;
			}

			//GCRUZ 2016-05-30. Tiempo y distancia de traslado de la captura
			//Crear el indicador TransferTime
			$strPropertyName = "GblTransferTimeIndID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing indicator detected: {$strPropertyName}");
				}
				
				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
				$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
				$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
				$anInstanceIndicator->IndicatorName = "TransferTime";
				$anInstanceIndicator->formato = '#,##0.000000';
				$anInstanceIndicator->agrupador = 'SUM';
				$anInstanceIndicator->sql_source = 'SUM';
				$objMMAnswer = @$anInstanceIndicator->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating longitude indicator").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
				$sql = "UPDATE SI_SV_GblSurveyModel SET TransferTimeIndID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}

			//Crear el indicador TransferDistance
			$strPropertyName = "GblTransferDistanceIndID";
			if ($this->{$strPropertyName} <= 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Missing indicator detected: {$strPropertyName}");
				}
				
				$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($this->Repository);
				$anInstanceIndicator->ModelID = $this->GblSurveyModelID;
				$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);
				$anInstanceIndicator->IndicatorName = "TransferDistance";
				$anInstanceIndicator->formato = '#,##0.000000';
				$anInstanceIndicator->agrupador = 'SUM';
				$anInstanceIndicator->sql_source = 'SUM';
				$objMMAnswer = @$anInstanceIndicator->save();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "<br>\r\n(".__METHOD__.") ".translate("Error creating longitude indicator").": ".$strMMError.". ";
					$gblEFormsErrorMessage = $gblModMngrErrorMessage;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
				
				$this->{$strPropertyName} = $anInstanceIndicator->IndicatorID;
				$sql = "UPDATE SI_SV_GblSurveyModel SET TransferDistanceIndID = ".$this->{$strPropertyName}." 
					WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
				if (!$this->Repository->DataADOConnection->Execute($sql)) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
					if ($gblShowErrorSurvey) {
						die ($gblEFormsErrorMessage);
					}
					else {
						return $gblEFormsErrorMessage;
					}
				}
			}


		}
		//@JAPR
		
		//Actualiza la fecha de última revisión de integridad para no repetir dos veces este proceso durante el mismo día
		$this->GblLastCheckDate = today();
		$sql = "UPDATE SI_SV_GblSurveyModel SET LastCheckDate = ".$this->Repository->DataADOConnection->DBTimeStamp($this->GblLastCheckDate)." 
			WHERE GblSurveyModelID = {$this->GblSurveyModelID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		return true;
	}
	//@JAPR
	
	//@JAPR 2013-04-29: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	//Lee el mapeo de campos de eForms hacia eBavel y los almacena como un string de pares NombreCampo=Id que se puede usar en la ventana para su configuración
	function readeBavelFieldsMapping() {
		require_once("surveyFieldMap.inc.php");
		
		$this->eBavelFieldsData = '';
		$this->eBavelFieldsDataOld = '';
		
		$objSurveyFieldMap = BITAMSurveyFieldMap::NewInstanceWithSurvey($this->Repository, $this->SurveyID);
		if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$strAnd = '';
		$streBavelFieldsData = '';
		foreach($objSurveyFieldMap->ArrayVariables as $intSurveyFieldID => $strVariableName) {
			$streBavelFieldsData .= $strAnd.$strVariableName.'='.((int) @$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID]);
			$strAnd = '|';
		}
		
		$this->eBavelFieldsData = $streBavelFieldsData;
		$this->eBavelFieldsDataOld = $this->eBavelFieldsData;
	}
	
	//Graba el string de pares NombreCampo=Id en la tabla de mapeos de campos de eForms hacia eBavel sólo si hubo cambios
	function saveeBavelFieldsMapping() {
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		if (trim((string) $this->eBavelFieldsData) == trim((string) $this->eBavelFieldsDataOld)) {
			return;
		}
		
		require_once("surveyFieldMap.inc.php");
		
		//El siguiente objeto, esté o no algo grabado en la Metadata, contendrá la lista de los campos configurables por encuesta indexados por
		//el ID, con dicha lista se podrán actualizar los campos prviamente grabados o se agregarán las nuevas configuraciones
		$objSurveyFieldMap = BITAMSurveyFieldMap::NewInstanceWithSurvey($this->Repository, $this->SurveyID);
		if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$arrFieldsByName = array_flip($objSurveyFieldMap->ArrayVariables);
		
		//Recorre el String recibido actualizando, insertando o eliminando según el caso los campos recibidos
		$arrFieldsData = explode('|', $this->eBavelFieldsData);
		$arrProcessedFields = array();
		
		foreach ($arrFieldsData as $aFieldData) {
			if (trim($aFieldData) == '') {
				continue;
			}
			
			$arrFieldInfo = explode('=', $aFieldData);
			if (count($arrFieldInfo) < 2) {
				continue;
			}
			
			$streBavelFieldName = $arrFieldInfo[0];
			$intSurveyFieldID = (int) @$arrFieldsByName[$streBavelFieldName];
			if ($intSurveyFieldID == 0) {
				continue;
			}
			
			//Es un mapeo de campo válido, identifica si debe agregar o actualizar la información
			if (!isset($objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID])) {
				//Es un elemento nuevo, pero como el array ya debe venir con los defaults conocidos, se ignorará pues debe ser un error
				//No se intentará eliminar, simplemente al cargar la instancia nunca será reconocído como un tipo de mapeo válido en esta versión
			}
			else {
				//Es un elemento ya existente, se actualizará/insertará dependiendo de si ya estaba o no en la Metadata, pero se actualiza su
				//mapeo. El grabado se hace en base a la propiedad y no al array de valores, por lo tanto se agrega/modifica dicha propiedad
				$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID] = $arrFieldInfo[1];
				$objSurveyFieldMap->$streBavelFieldName = $arrFieldInfo[1];
				$arrProcessedFields[$streBavelFieldName] = $streBavelFieldName;
			}
		}
		
		//Invoca al método que graba los mapeos
		@$objSurveyFieldMap->save();
	}
	
	//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
	//Lee el mapeo de campos de eForms hacia eBavel y los almacena como un string de pares NombreCampo=Id que se puede usar en la ventana para su configuración
	function readeBavelActionFieldsMapping() {
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		require_once("surveyActionMap.inc.php");
		
		$this->eBavelActionFieldsData = '';
		$this->eBavelActionFieldsDataOld = '';
		
		$objSurveyFieldMap = BITAMSurveyActionMap::NewInstanceWithSurvey($this->Repository, $this->SurveyID);
		if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$strAnd = '';
		$streBavelFieldsData = '';
		foreach($objSurveyFieldMap->ArrayVariables as $intSurveyFieldID => $strVariableName) {
			$streBavelFieldsData .= $strAnd.$strVariableName.'='.((int) @$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID]);
			$strAnd = '|';
		}
		
		$this->eBavelActionFieldsData = $streBavelFieldsData;
		$this->eBavelActionFieldsDataOld = $this->eBavelActionFieldsData;
	}
	
	//Graba el string de pares NombreCampo=Id en la tabla de mapeos de campos de eForms hacia eBavel sólo si hubo cambios
	function saveeBavelActionFieldsMapping() {
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//@JAPRDescontinuada en v6
		return;
		//@JAPR
		if (trim((string) $this->eBavelActionFieldsData) == trim((string) $this->eBavelActionFieldsDataOld)) {
			return;
		}
		
		require_once("surveyActionMap.inc.php");
		
		//El siguiente objeto, esté o no algo grabado en la Metadata, contendrá la lista de los campos configurables por encuesta indexados por
		//el ID, con dicha lista se podrán actualizar los campos prviamente grabados o se agregarán las nuevas configuraciones
		$objSurveyFieldMap = BITAMSurveyActionMap::NewInstanceWithSurvey($this->Repository, $this->SurveyID);
		if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
			return;
		}
		
		$arrFieldsByName = array_flip($objSurveyFieldMap->ArrayVariables);
		
		//Recorre el String recibido actualizando, insertando o eliminando según el caso los campos recibidos
		$arrFieldsData = explode('|', $this->eBavelActionFieldsData);
		$arrProcessedFields = array();
		
		foreach ($arrFieldsData as $aFieldData) {
			if (trim($aFieldData) == '') {
				continue;
			}
			
			$arrFieldInfo = explode('=', $aFieldData);
			if (count($arrFieldInfo) < 2) {
				continue;
			}
			
			$streBavelFieldName = $arrFieldInfo[0];
			$intSurveyFieldID = (int) @$arrFieldsByName[$streBavelFieldName];
			if ($intSurveyFieldID == 0) {
				continue;
			}
			
			//Es un mapeo de campo válido, identifica si debe agregar o actualizar la información
			if (!isset($objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID])) {
				//Es un elemento nuevo, pero como el array ya debe venir con los defaults conocidos, se ignorará pues debe ser un error
				//No se intentará eliminar, simplemente al cargar la instancia nunca será reconocído como un tipo de mapeo válido en esta versión
			}
			else {
				//Es un elemento ya existente, se actualizará/insertará dependiendo de si ya estaba o no en la Metadata, pero se actualiza su
				//mapeo. El grabado se hace en base a la propiedad y no al array de valores, por lo tanto se agrega/modifica dicha propiedad
				$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID] = $arrFieldInfo[1];
				$objSurveyFieldMap->$streBavelFieldName = $arrFieldInfo[1];
				$arrProcessedFields[$streBavelFieldName] = $streBavelFieldName;
			}
		}
		
		//Invoca al método que graba los mapeos
		@$objSurveyFieldMap->save();
	}
	//@JAPR
	
	//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
	/* Verifica si la instancia de la forma tiene o no asociado un Scheduler automático, si no lo tiene entonces lo genera y lo liga a esta forma
	A partir de esta versión la publicación de formas se hará únicamente desde la ventana de diseño de la forma, así que no hay necesidad de permitir generar
	múltiples schedulers, si no había uno asociado en este momento, se asumirá que se venía de la versión previa en la que había múltiples Schedulers pero 
	esos serán ignorados por la nueva versión durante la descarga de definiciones para las formas nuevas
	*/
	function saveFormScheduler() {
		//No es necesario volver a crear el Scheduler si ya estaba asignado
		if ($this->SchedulerID > 0) {
			return;
		}
		
		require_once("surveyScheduler.inc.php");
		$objSurveyScheduler = BITAMSurveyScheduler::NewInstance($this->Repository);
		if (is_null($objSurveyScheduler)) {
			return;
		}
		
		$objSurveyScheduler->SurveyID = $this->SurveyID;
		$objSurveyScheduler->SchedulerName = $this->SurveyName;
		$objSurveyScheduler->CaptureVia = 0;
		$objSurveyScheduler->save();
		$this->SchedulerID = (int) $objSurveyScheduler->SchedulerID;
		if ($this->SchedulerID > 0) {
			$sql = "UPDATE SI_SV_Survey SET SchedulerID = ".$this->SchedulerID." 
				WHERE SurveyID = ".$this->SurveyID;
			$this->Repository->DataADOConnection->Execute($sql);
			
			//@JAPR 2015-08-01: Agrega al usuario que creó la forma por default. Como este método se invocaría sólo durante la creación de la forma (que es cuando aún no tiene un
			//scheduler asociado de manera automática), es seguro insertar en este punto directamente (#N8PRAP)
			$sql = "INSERT INTO SI_SV_SurveySchedulerUser (SchedulerID, UserID) 
						VALUES (".$this->SchedulerID.",".(int) @$_SESSION["PABITAM_UserID"].")";
			$this->Repository->DataADOConnection->Execute($sql);
			//@JAPR
		}
	}
	
	function readGblSurveyModel() {
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2016-12-30: Agregadas las fechas de edición según el servidor (#44O0VU)
		//Para este caso exclusivamente, dado a que afecta al cubo global de Surveys y eso podría afectar al grabado de las capturas, se agregará un ALTER TABLE forzado para modificar
		//automáticamente la tabla de cubo global en caso de que no existieran los nuevos campos, de esa manera la integridad del proceso se mantendría al no generar un error de grabado
		//si ya se había actualizado aparentemente la metadata pero no se hubiera ejecutado correctamente el script
		if (getMDVersion() >= esvServerEditDates) {
			$sql = "ALTER TABLE SI_SV_GblSurveyModel ADD ServerStartDateEditDimID INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_GblSurveyModel ADD ServerEndDateEditDimID INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_GblSurveyModel ADD ServerStartTimeEditDimID INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
			$sql = "ALTER TABLE SI_SV_GblSurveyModel ADD ServerEndTimeEditDimID INTEGER NULL";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		//@JAPR
		
		$strAdditionalFields = "";
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
		$sql = "SELECT GblSurveyModelID, SurveyDimID, SurveyGlobalDimID, SurveySingleRecordDimID, UserDimID, StartDateDimID, EndDateDimID, StartTimeDimID, EndTimeDimID, 
				CountIndID, AnsweredQuestionsIndID, DurationIndID, LatitudeIndID, LongitudeIndID, SyncDateDimID, SyncTimeDimID";
		if (getMDVersion() >= esvServerDateTime) {
			$strAdditionalFields .= ", ServerStartDateDimID, ServerEndDateDimID, ServerStartTimeDimID, ServerEndTimeDimID";
		}
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$strAdditionalFields .= ", AccuracyAttribID, AccuracyIndID, LatitudeDimID, LongitudeDimID";
		}
		//@JAPR 2014-11-19: Agregada la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID) {
			$strAdditionalFields .= ", AgendaDimID";
		}
		//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
		if (getMDVersion() >= esveFormsv6) {
			//@JAPR 2015-07-24: Finalmente NO se agregaron dimensiones independientes para Latitude, Longitude ni Accuracy de la captura, porque la idea original de tener toas las tablas normalizadas
			//ya no aplicó debido a limitantes en la cantidad de joins, adicionalmente EArteaga utilizó dimensiones-atributo para estos valores en los modelos mapeados a partir de las nuevas tablas
			//de eForms v6, por lo que haber creado una nueva tercia de dimensiones habría entrado en conflicto con las dimensiones-atributos existentes del cubo global, así que se optó por
			//seguir utilizando las ya existentes	//LatitudeIndDimID, LongitudeIndDimID, AccuracyIndDimID, 
			$strAdditionalFields .= ", StartDateEditDimID, EndDateEditDimID, StartTimeEditDimID, EndTimeEditDimID, SyncDateEndDimID, SyncTimeEndDimID, 
				CountryDimID, StateDimID, CityDimID, ZipCodeDimID, AddressDimID, SyncLatitudeDimID, SyncLongitudeDimID, SyncAccuracyDimID, LastCheckDate";
		}
		//GCRUZ 2016-05-30. Tiempo y distancia de traslado de la captura
		if (getMDVersion() >= esvCaptureTransfer) {
			$strAdditionalFields .= ", TransferTimeIndID, TransferDistanceIndID";
		}
		//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
		if (getMDVersion() >= esvServerEditDates) {
			$strAdditionalFields .= ", ServerStartDateEditDimID, ServerEndDateEditDimID, ServerStartTimeEditDimID, ServerEndTimeEditDimID";
		}
		//@JAPR
		$sql .= $strAdditionalFields ." FROM SI_SV_GblSurveyModel";
		//@JAPREliminar, sólo para pruebas forzando la creación de un nuevo modelo
		//$sql .= " WHERE GblSurveyModelID <> 29002";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		if (!$aRS->EOF) {
			$this->GblSurveyModelID = (int)$aRS->fields["gblsurveymodelid"];
			$this->GblSurveyDimID = (int)$aRS->fields["surveydimid"];

			//se agregaron las dimensiones "Survey Global ID" y "Survey Single Record ID"
			$this->GblSurveyGlobalDimID = (int)$aRS->fields["surveyglobaldimid"];
			$this->GblSurveySingleRecordDimID = (int)$aRS->fields["surveysinglerecorddimid"];

			$this->GblUserDimID = (int)$aRS->fields["userdimid"];
			$this->GblStartDateDimID = (int)$aRS->fields["startdatedimid"];
			$this->GblEndDateDimID = (int)$aRS->fields["enddatedimid"];
			$this->GblStartTimeDimID = (int)$aRS->fields["starttimedimid"];
			$this->GblEndTimeDimID = (int)$aRS->fields["endtimedimid"];
			/*@MABH20121205*/
			if (getMDVersion() >= esvServerDateTime) {
				$this->GblServerStartDateDimID = (int) @$aRS->fields["serverstartdatedimid"];
				$this->GblServerEndDateDimID = (int) @$aRS->fields["serverenddatedimid"];
				$this->GblServerStartTimeDimID = (int) @$aRS->fields["serverstarttimedimid"];
				$this->GblServerEndTimeDimID = (int) @$aRS->fields["serverendtimedimid"];
			}
			//@MABH20121205
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				$this->GblAccuracyIndID = (int) @$aRS->fields["accuracyindid"];
				$this->GblAccuracyDimID = (int) @$aRS->fields["accuracyattribid"];
				$this->GblLatitudeDimID = (int) @$aRS->fields["latitudedimid"];
				$this->GblLongitudeDimID = (int) @$aRS->fields["longitudedimid"];
			}
			//@JAPR
			$this->GblCountIndID = (int)$aRS->fields["countindid"];
			$this->GblAnsweredQuestionsIndID = (int)$aRS->fields["answeredquestionsindid"];
			$this->GblDurationIndID = (int)$aRS->fields["durationindid"];
			$this->GblLatitudeIndID = (int)$aRS->fields["latitudeindid"];
			$this->GblLongitudeIndID = (int)$aRS->fields["longitudeindid"];
			//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización
			$this->GblSyncDateDimID = (int) @$aRS->fields["syncdatedimid"];
			$this->GblSyncTimeDimID = (int) @$aRS->fields["synctimedimid"];
			//@JAPR 2014-11-18: Agregada la dimensión Agenda
			$this->GblAgendaDimID = (int) @$aRS->fields["agendadimid"];
			//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
			$this->GblStartDateEditDimID = (int) @$aRS->fields[strtolower("StartDateEditDimID")];
			$this->GblStartTimeEditDimID = (int) @$aRS->fields[strtolower("StartTimeEditDimID")];
			$this->GblEndDateEditDimID = (int) @$aRS->fields[strtolower("EndDateEditDimID")];
			$this->GblEndTimeEditDimID = (int) @$aRS->fields[strtolower("EndTimeEditDimID")];
			//@JAPR 2015-07-24: Finalmente NO se agregaron dimensiones independientes para Latitude, Longitude ni Accuracy de la captura, porque la idea original de tener toas las tablas normalizadas
			//ya no aplicó debido a limitantes en la cantidad de joins, adicionalmente EArteaga utilizó dimensiones-atributo para estos valores en los modelos mapeados a partir de las nuevas tablas
			//de eForms v6, por lo que haber creado una nueva tercia de dimensiones habría entrado en conflicto con las dimensiones-atributos existentes del cubo global, así que se optó por
			//seguir utilizando las ya existentes
			//$this->GblLatitudeIndDimID = (int) @$aRS->fields[strtolower("LatitudeIndDimID")];
			//$this->GblLongitudeIndDimID = (int) @$aRS->fields[strtolower("LongitudeIndDimID")];
			//$this->GblAccuracyIndDimID = (int) @$aRS->fields[strtolower("AccuracyIndDimID")];
			//@JAPR
			$this->GblSyncEndDateDimID = (int) @$aRS->fields[strtolower("SyncDateEndDimID")];
			$this->GblSyncEndTimeDimID = (int) @$aRS->fields[strtolower("SyncTimeEndDimID")];
			$this->GblSyncLatitudeDimID = (int) @$aRS->fields[strtolower("SyncLatitudeDimID")];
			$this->GblSyncLongitudeDimID = (int) @$aRS->fields[strtolower("SyncLongitudeDimID")];
			$this->GblSyncAccuracyDimID = (int) @$aRS->fields[strtolower("SyncAccuracyDimID")];
			$this->GblCountryDimID = (int) @$aRS->fields[strtolower("CountryDimID")];
			$this->GblStateDimID = (int) @$aRS->fields[strtolower("StateDimID")];
			$this->GblCityDimID = (int) @$aRS->fields[strtolower("CityDimID")];
			$this->GblZipCodeDimID = (int) @$aRS->fields[strtolower("ZipCodeDimID")];
			$this->GblAddressDimID = (int) @$aRS->fields[strtolower("AddressDimID")];
			$this->GblLastCheckDate = trim((string) @$aRS->fields[strtolower("LastCheckDate")]);
			//@JAPR
			//GCRUZ 2016-05-30. Tiempo y distancia de traslado de la captura
			$this->GblTransferTimeIndID = (int) @$aRS->fields[strtolower("TransferTimeIndID")];
			$this->GblTransferDistanceIndID = (int) @$aRS->fields[strtolower("TransferDistanceIndID")];;
			//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
			if (getMDVersion() >= esvServerEditDates) {
				$this->GblServerStartDateEditDimID = (int) @$aRS->fields["serverstartdateeditdimid"];
				$this->GblServerEndDateEditDimID = (int) @$aRS->fields["serverenddateeditdimid"];
				$this->GblServerStartTimeEditDimID = (int) @$aRS->fields["serverstarttimeeditdimid"];
				$this->GblServerEndTimeEditDimID = (int) @$aRS->fields["serverendtimeeditdimid"];
			}
			//@JAPR
		}
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("SurveyID", $aHTTPRequest->POST))
		{
			$aSurveyID = $aHTTPRequest->POST["SurveyID"];
			
			if (is_array($aSurveyID))
			{
				
				$aCollection = BITAMSurveyCollection::NewInstance($aRepository, $aSurveyID);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aSurveyID);
				if (is_null($anInstance))
				{
					//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("SurveyID", $aHTTPRequest->GET))
		{
			$forceToStatusInDev = false;
			if(isset($aHTTPRequest->GET["ForceStatus"]))
			{
				$forceToStatusInDev = true;
			}
			
			$deleteDataCaptured = false;
			if(isset($aHTTPRequest->GET["DeleteData"]))
			{
				$deleteDataCaptured = true;
			}

			$aSurveyID = $aHTTPRequest->GET["SurveyID"];

			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aSurveyID);

			if (is_null($anInstance))
			{
				//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
			else 
			{
				if($forceToStatusInDev)
				{
					$anInstance->forceToChangeToStatusInDev();
					return $anInstance;
				}
				
				if($deleteDataCaptured)
				{
					$anInstance->deleteDataCapturedInSurvey();
					return $anInstance;
				}
			}
		}
		else
		{
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		return $anInstance;
	}

	//@JAPR 2012-07-26: Agregado el salto de sección a sección
	static function GetLastSectionID($aRepository, $aSurveyID)
	{
		$intSectionID = -1;
		
		$sql = "SELECT SectionID 
			FROM SI_SV_Section 
			WHERE SurveyID = ".$aSurveyID." AND SectionNumber IN (
				SELECT MAX(SectionNumber) 
				FROM SI_SV_Section 
				WHERE SurveyID = ".$aSurveyID.")";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$intSectionID = (int)$aRS->fields["sectionid"];
		}
		
		return $intSectionID;
	}
	//@JAPR
	
	function updateFromArray($anArray)
	{
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		global $garrUpdatedPropsWithVars;
		if (!isset($garrUpdatedPropsWithVars)) {
			$garrUpdatedPropsWithVars = array();
		}
		if (!isset($garrUpdatedPropsWithVars["Surveys"])) {
			$garrUpdatedPropsWithVars["Surveys"] = array();
		}
		//@JAPR
		
		if (array_key_exists("SurveyID", $anArray))
		{
			$this->SurveyID = (int)$anArray["SurveyID"];
		}
		
	 	if (array_key_exists("SurveyName", $anArray))
		{
			//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
			$this->SurveyName = trim($anArray["SurveyName"]);
			//JAPR
		}
		
		if (array_key_exists("SurveyType", $anArray))
		{
			$this->SurveyType = (int)$anArray["SurveyType"];
		}
		
	 	if (array_key_exists("ClosingMsg", $anArray))
		{
			$this->ClosingMsg = rtrim($anArray["ClosingMsg"]);
		}

	 	if (array_key_exists("HelpMsg", $anArray))
		{
			$this->HelpMsg = rtrim($anArray["HelpMsg"]);
		}
		
	 	if (array_key_exists("Status", $anArray))
		{
			$this->Status = (int)$anArray["Status"];
		}
		
		if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = (int)$anArray["CatalogID"];
		}
		
		if (array_key_exists("HasSignature", $anArray))
		{
			$this->HasSignature = (int)$anArray["HasSignature"];
		}
		
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta
		if (array_key_exists("SyncWhenSaving", $anArray))
		{
			$this->SyncWhenSaving = (int)$anArray["SyncWhenSaving"];
		}
		//@JAPR
		
    	//@LROUX 2011-10-18: Se agrega AllowPartialSave.
		if (array_key_exists("AllowPartialSave", $anArray))
		{
			$this->AllowPartialSave = (int)$anArray["AllowPartialSave"];
		}

		//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		if (array_key_exists("ElementQuestionID", $anArray))
		{
			$this->ElementQuestionID = (int)$anArray["ElementQuestionID"];
		}
		
		if (array_key_exists("eBavelAppID", $anArray))
		{
			$this->eBavelAppID = (int) $anArray["eBavelAppID"];
		}

		if (array_key_exists("eBavelFormID", $anArray))
		{
			$this->eBavelFormID = (int)$anArray["eBavelFormID"];
		}
		
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (array_key_exists("eBavelFieldsData", $anArray))
		{
			$this->eBavelFieldsData = $anArray["eBavelFieldsData"];
		}
		
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		if (array_key_exists("eBavelActionFormID", $anArray))
		{
			$this->eBavelActionFormID = (int)$anArray["eBavelActionFormID"];
		}
		
		if (array_key_exists("eBavelActionFieldsData", $anArray))
		{
			$this->eBavelActionFieldsData = $anArray["eBavelActionFieldsData"];
		}
		
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if (array_key_exists("EnableReschedule", $anArray))
		{
			$this->EnableReschedule = (int)$anArray["EnableReschedule"];
		}
		
		if (array_key_exists("RescheduleCatalogID", $anArray))
		{
			$this->RescheduleCatalogID = (int) $anArray["RescheduleCatalogID"];
		}
		
		if (array_key_exists("RescheduleCatMemberID", $anArray))
		{
			$this->RescheduleCatMemberID = (int) $anArray["RescheduleCatMemberID"];
		}
		
		if (array_key_exists("RescheduleStatus", $anArray))
		{
			$this->RescheduleStatus = (string) $anArray["RescheduleStatus"];
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (array_key_exists("UseStdSectionSingleRec", $anArray))
		{
			$this->UseStdSectionSingleRec = (int)$anArray["UseStdSectionSingleRec"];
		}
		
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if(array_key_exists("OtherLabel", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->OtherLabel = (string) $anArray["OtherLabel"];
		}
		
		if(array_key_exists("CommentLabel", $anArray))
		{
			//@JAPR 2012-04-25: Corregido un bug, siempre se estaba asignando como verdadero esta opción
			$this->CommentLabel = (string) $anArray["CommentLabel"];
		}
		
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		if(array_key_exists("AdditionalEMails", $anArray))
		{
			$this->AdditionalEMails = (string) $anArray["AdditionalEMails"];
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->AdditionalEMails = $this->TranslateVariableQuestionNumbersByIDs($this->AdditionalEMails, optyAdditionalEMails);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["Surveys"]["AdditionalEMails"] = 1;
			//@JAPR
		}
		
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if(array_key_exists("SurveyImageText", $anArray))
		{
			$this->SurveyImageText = (string) $anArray["SurveyImageText"];
		}
		
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if(array_key_exists("SurveyMenuImage", $anArray))
		{
			$this->SurveyMenuImage = (string) $anArray["SurveyMenuImage"];
		}
		
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		if(array_key_exists("HideNavButtons", $anArray))
		{
			$this->HideNavButtons = (int) $anArray["HideNavButtons"];
		}
		
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		if(array_key_exists("HideSectionNames", $anArray))
		{
			$this->HideSectionNames = (int) $anArray["HideSectionNames"];
		}
		
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if(array_key_exists("DisableEntry", $anArray))
		{
			$this->DisableEntry = (int) $anArray["DisableEntry"];
		}
		//@JAPR
		
		if(array_key_exists("IsAgendable", $anArray))
		{
			$this->IsAgendable = (int) $anArray["IsAgendable"];
		}
		if(array_key_exists("RadioForAgenda", $anArray))
		{
			$this->RadioForAgenda = (int) $anArray["RadioForAgenda"];
		}
		
		//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
		if (array_key_exists("SurveyDesc", $anArray))
		{
			$this->SurveyDesc = (string) $anArray["SurveyDesc"];
		}
		//@JAPR
		
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		if (array_key_exists("incrementalFile", $anArray))
		{
			$this->incrementalFile = (string) $anArray["incrementalFile"];
		}
		
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (array_key_exists("Width", $anArray))
		{
			$this->Width = (int) @$anArray["Width"];
		}
		if (array_key_exists("Height", $anArray))
		{
			$this->Height = (int) @$anArray["Height"];
		}
		if (array_key_exists("TextPosition", $anArray))
		{
			$this->TextPosition = (int) @$anArray["TextPosition"];
		}
		if (array_key_exists("TextSize", $anArray))
		{
			$this->TextSize = (string) trim($anArray["TextSize"]);
		}
		if (array_key_exists("CustomLayout", $anArray))
		{
			$this->CustomLayout = (string) $anArray["CustomLayout"];
		}
		//@JAPR
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (array_key_exists("ShowQNumbers", $anArray))
		{
			$this->ShowQNumbers = (int) $anArray["ShowQNumbers"];
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (array_key_exists("TemplateID", $anArray))
		{
			$this->TemplateID = (string) $anArray["TemplateID"];
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		if (array_key_exists("PlannedDuration", $anArray))
		{
			$this->PlannedDuration = (float) $anArray["PlannedDuration"];
		}
		
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (array_key_exists("EnableNavHistory", $anArray))
		{
			$this->EnableNavHistory = (int) $anArray["EnableNavHistory"];
		}
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (array_key_exists("QuestionIDForEntryDesc", $anArray)) {
			$this->QuestionIDForEntryDesc = (int) $anArray["QuestionIDForEntryDesc"];
		}
		if ( array_key_exists( "ReloadSurveyFromSection", $anArray ) ) {
			$this->ReloadSurveyFromSection = (int) $anArray[ "ReloadSurveyFromSection" ];
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (array_key_exists("AutoFitImage", $anArray))
		{
			$this->AutoFitImage = (int) $anArray["AutoFitImage"];
		}
		//@JAPR
		
		return $this;
	}
	
	function forceToChangeToStatusInDev()
	{
		//Cambiar a estado In Development
		$sql = "UPDATE SI_SV_Survey SET Status = ".svstInDevelopment." WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Cambiar el status para que se vea reflejado en el objeto
		$this->Status = svstInDevelopment;
	}
	
	function deleteDataCapturedInSurvey()
	{
		//Comenzamos con borrar las tablas de hechos global, tabla de hechos de la encuesta, tabla de hechos paralela
		$GblFactTable = "RIFACT_".$this->GblSurveyModelID;

		$surveyDimTableName = "RIDIM_".$this->GblSurveyDimID;
		$fieldSurrogateKey = $surveyDimTableName."KEY";
		$fieldKey = "KEY_".$this->GblSurveyDimID;
		$fieldDesc = "DSC_".$this->GblSurveyDimID;

		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$surveyDimTableName." WHERE ".$fieldKey." = ".$this->SurveyID;

		$aRS = $this->Repository->DataADOConnection->Execute($sql);

		if ($aRS === false && $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$surveyDimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Ya tenemos el valor de la llave subrogada de la dimension Survey
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];

		//Eliminamos los elementos de la encuesta que se encuentran almacenados en el cubo global de encuestas
		$sql = "DELETE FROM ".$GblFactTable." WHERE ".$fieldSurrogateKey." = ".$valueKey;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$GblFactTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Removemos la informacion de la dimension Survey Global ID para dicha encuesta
		$tableDim = "RIDIM_".$this->GblSurveyGlobalDimID;
		$fieldDesc = "DSC_".$this->GblSurveyGlobalDimID;
		$strSearch = $valueKey."-%";

		$sql = "DELETE FROM ".$tableDim." WHERE ".$fieldDesc." LIKE ".$this->Repository->DataADOConnection->Quote($strSearch);
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Removemos la informacion de la dimension Survey Single Record ID para dicha encuesta
		$tableDim = "RIDIM_".$this->GblSurveySingleRecordDimID;
		$fieldDesc = "DSC_".$this->GblSurveySingleRecordDimID;
		$strSearch = $valueKey."-%";

		$sql = "DELETE FROM ".$tableDim." WHERE ".$fieldDesc." LIKE ".$this->Repository->DataADOConnection->Quote($strSearch);
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos todo el contenido de la tabla de hechos de la encuesta
		$factTable = "RIFACT_".$this->ModelID;
		
		$sql = "DELETE FROM ".$factTable;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos todo el contenido de la tabla paralela
		$sql = "DELETE FROM ".$this->SurveyTable;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos todo el contenido de la tabla paralela de la matrix
		$tableMatrix = "SVSurveyMatrixData_".$this->ModelID;
		
		$sql = "DELETE FROM ".$tableMatrix;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableMatrix." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos todo el contenido de la tabla paralela de Category Dimension
		$tableCatDimVal = "SVSurveyCatDimVal_".$this->ModelID;
		
		$sql = "DELETE FROM ".$tableCatDimVal;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableCatDimVal." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos los contenidos de las tablas SI_SV_SurveyAnswerImage, SI_SV_SurveyAnswerComment, SI_SV_SurveyQuestionActions, SI_SV_SurveySignatureImg, SVSurveyActions
		
		//Eliminamos el contenido de la tabla SI_SV_SurveyAnswerImage para la encuesta señalada
		//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$sql = "DELETE FROM SI_SV_SurveyAnswerImageSrc WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImageSrc ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Eliminamos el contenido de la tabla SI_SV_SurveyAnswerComment para la encuesta señalada
		$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos el contenido de la tabla SI_SV_SurveyQuestionActions para la encuesta señalada
		$sql = "DELETE FROM SI_SV_SurveyQuestionActions WHERE SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos el contenido de la tabla SI_SV_SurveySignatureImg para la encuesta señalada
		$sql = "DELETE FROM SI_SV_SurveySignatureImg WHERE SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySignatureImg ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos el contenido de la tabla SVSurveyActions para la encuesta señalada
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$this->SurveyID;
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Eliminamos el contenido de la dimension Encuesta ID excepto el valor de -1 que es el valor de No Aplica
		$tableDimEncuestaID = "RIDIM_".$this->FactKeyDimID;
		$fieldEncuestaIDKey = $tableDimEncuestaID."KEY";
		
		//Eliminamos el contenido de la tabla de dimension Encuesta ID
		$sql = "DELETE FROM ".$tableDimEncuestaID." WHERE ".$fieldEncuestaIDKey." <> -1";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEncuestaID." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
		$anArrayTypes = array();
		$anArrayTypes[0] = 3;
		$questionCollection = BITAMQuestionCollection::NewInstanceWithExceptType($this->Repository, $this->SurveyID, $anArrayTypes);

		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			switch ($aQuestion->QTypeID) 
			{
				//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
				//@JAPR 2012-09-25: Agregado el tipo de pregunta Hora
				case qtpOpenNumeric:
				case qtpCalc:
				case qtpOpenTime:
				case qtpOpenDate:
				case qtpOpenString:
				case qtpOpenAlpha:
				//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$tableDimension = "RIDIM_".$aQuestion->IndDimID;
					$fieldDimKey = $tableDimension."KEY";
					
					//Eliminamos el contenido de la tabla de dimension de la pregunta excepto el valor de No Aplica
					$sql = "DELETE FROM ".$tableDimension." WHERE ".$fieldDimKey." <> 1";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimension." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					break;
			
				default:
					//En este caso solo aplica el caso 2
					//es decir, preguntas de seleccion
					//simple pero en dichas preguntas no se debe eliminar nada
					break;
			}
			
			//Preguntamos si la pregunta tiene foto opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
			{
				$tableDimensionImg = "RIDIM_".$aQuestion->ImgDimID;
				$fieldDimImgKey = $tableDimensionImg."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Imagen de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionImg." WHERE ".$fieldDimImgKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionImg." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Preguntamos si la pregunta tiene documento opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqDocument>0 && !is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
			{
				$tableDimensionDoc = "RIDIM_".$aQuestion->DocDimID;
				$fieldDimImgKey = $tableDimensionDoc."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Documento de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionDoc." WHERE ".$fieldDimDocKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionDoc." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q pertenezcan a secciones dinamicas y esten dentro de ellas
		$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($this->Repository, $this->SurveyID);
		
		foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			if($aQuestion->MCInputType>0)
			{
				$tableDimension = "RIDIM_".$aQuestion->IndDimID;
				$fieldDimKey = $tableDimension."KEY";
				
				//Eliminamos el contenido de la tabla de dimension de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimension." WHERE ".$fieldDimKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimension." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Preguntamos si la pregunta tiene foto opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
			{
				$tableDimensionImg = "RIDIM_".$aQuestion->ImgDimID;
				$fieldDimImgKey = $tableDimensionImg."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Imagen de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionImg." WHERE ".$fieldDimImgKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionImg." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Preguntamos si la pregunta tiene documento opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqDocument>0 && !is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
			{
				$tableDimensionImg = "RIDIM_".$aQuestion->DocDimID;
				$fieldDimDocKey = $tableDimensionDoc."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Imagen de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionDoc." WHERE ".$fieldDimDocKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionDoc." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}

		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q no pertenezcan a dimensiones dinamicas o bien esten fuera de ellas
		$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($this->Repository, $this->SurveyID);
		
		//Recorremos las preguntas de seleccion multiple para obtener sus valores multiples
		foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion)
		{
			if($aQuestion->MCInputType>0)
			{
				$QIndDimIDs = $aQuestion->QIndDimIds;
			
				//Obtenemos las instancias de las dimensiones de las opciones
				foreach ($QIndDimIDs as $qKey=>$qDimID) 
				{
					$tableDimension = "RIDIM_".$qDimID;
					$fieldDimKey = $tableDimension."KEY";
					
					//Eliminamos el contenido de la tabla de dimension de la pregunta excepto el valor de No Aplica
					$sql = "DELETE FROM ".$tableDimension." WHERE ".$fieldDimKey." <> 1";
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimension." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}

			//Preguntamos si la pregunta tiene foto opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqPhoto>0 && !is_null($aQuestion->ImgDimID) && $aQuestion->ImgDimID>0)
			{
				$tableDimensionImg = "RIDIM_".$aQuestion->ImgDimID;
				$fieldDimImgKey = $tableDimensionImg."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Imagen de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionImg." WHERE ".$fieldDimImgKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionImg." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//Preguntamos si la pregunta tiene Documento opcional o requerida y que tenga 
			//valor de dimension valido
			if($aQuestion->HasReqDocument>0 && !is_null($aQuestion->DocDimID) && $aQuestion->DocDimID>0)
			{
				$tableDimensionDoc = "RIDIM_".$aQuestion->DocDimID;
				$fieldDimDocKey = $tableDimensionDoc."KEY";
				
				//Eliminamos el contenido de la tabla de dimension Imagen de la pregunta excepto el valor de No Aplica
				$sql = "DELETE FROM ".$tableDimensionDoc." WHERE ".$fieldDimDocKey." <> 1";
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimensionDoc." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		
		//Se proceden a eliminar las imagenes de las fotos, obtenemos la ruta de las fotos
		$DocsDir = getcwd()."\\"."surveyimages";
		$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		$ProjectDir = $RepositoryDir."\\"."survey_".$this->SurveyID;
  
		$this->executeRMDir($ProjectDir);
		
		//Se proceden a eliminar los documentos, obtenemos la ruta de los documentos
		$DocsDir = getcwd()."\\"."surveydocuments";
		$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		$ProjectDir = $RepositoryDir."\\"."survey_".$this->SurveyID;
  
		$this->executeRMDir($ProjectDir);
		
		//Se proceden a eliminar las imagenes de las firmas, obtenemos la ruta de las firmas
		$DocsDir = getcwd()."\\"."surveysignature";
		$RepositoryDir = $DocsDir."\\".trim($_SESSION["PABITAM_RepositoryName"]);
		$ProjectDir = $RepositoryDir."\\"."survey_".$this->SurveyID;
  
		$this->executeRMDir($ProjectDir);
	}
	
	function executeRMDir($dir, $delCurrentDir=false) 
	{ 
		if (is_dir($dir))
		{
			$objects = scandir($dir);
			foreach ($objects as $object)
			{
				if ($object != "." && $object != "..")
				{
					if (filetype($dir."/".$object) == "dir")
					{
						$this->executeRMDir($dir."/".$object, true);
					}
					else 
					{
						unlink($dir."/".$object);
					}
				}
			}

			reset($objects);
			
			if($delCurrentDir==true)
			{
				rmdir($dir);
			}
		}
	}
	
	//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
	//Obtiene la lista de IDs de catálogos que son usados por esta encuesta de alguna forma
	//A la fecha de implementación, aunque se usaran para secciones, tenía que haber por lo menos una pregunta de dicho catálogo para ser válido,
	//sin embargo se hará el UNION incluyendo a las secciones directamente
	function getUsedCatalogsIDs()
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2014-12-19: Corregido un bug, sólo estaba considerando secciones dinámicas
		$sql = "SELECT DISTINCT A.CatalogID ".
			"FROM SI_SV_Section A ".
			"WHERE (A.SectionType = ".sectDynamic." OR A.SectionType = ".sectInline.") AND A.CatalogID > 0 AND A.SurveyID = {$this->SurveyID}".
			" UNION ".
			"SELECT DISTINCT A.CatalogID ".
			"FROM SI_SV_Question A ".
			"WHERE A.CatalogID > 0 AND A.QTypeID <> ".qtpOpenNumeric." AND A.SurveyID = {$this->SurveyID}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section, SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section, SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$arrCatalogIDs = array();
		while (!$aRS->EOF) {
			$arrCatalogIDs[] = $aRS->fields["catalogid"];
			$aRS->MoveNext();
		};
		
		return $arrCatalogIDs;
	}
	
	//@JAPR 2013-01-14: Agregada la opción para que las preguntas estándar se graben en un registro único
	//Esta función identifica si se encuentran o no (o si son usables) los campos especiales de la tabla SVSurvey_#### de manera que algunos
	//procesos puedan aprovecharlos (el grabado, reportes, exportación, etc.) para consultar mas eficientemente los datos
	//No tiene resultado, pero asignará propiedades de la instancia que de otra forma vendrían desactivadas (equivalen a algunas variables globales
	//que se usaban inicialmente para dicho fin, la idea es remover eventualmente las variables globales y usar sólo la instancia de survey)
	//Sólo se debe invocar una vez durante el proceso de manera que no se repita el query múltiples veces (se valida internamente)
	function getSpecialFieldsStatus() {
		if ($this->SpecialFieldsRead) {
			return;
		}
		
		//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
		//Verifica si existen los campos para almacenar el originen del registro de la tabla de datos
		$sql = "SELECT EntrySectionID, EntryCatDimID FROM ".$this->SurveyTable." WHERE 1 = 2";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$this->UseSourceFields = true;
		}
		//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
		$sql = "SELECT eFormsVersionNum FROM ".$this->SurveyTable." WHERE 1 = 2";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$this->UseRecVersionField = true;
		}
		//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
		$sql = "SELECT DynamicPageDSC FROM ".$this->SurveyTable." WHERE 1 = 2";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$this->UseDynamicPageField = true;
		}
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$sql = "SELECT DynamicOptionDSC FROM ".$this->SurveyTable." WHERE 1 = 2";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$this->UseDynamicOptionField = true;
		}
		
		$this->SpecialFieldsRead = true;
	}
	//@JAPR
	
	//Obtiene la lista de usuarios asociados a la encuesta
	function getAllScheduledUsers()
	{
		$arrUserIDs = array();
		//Obtener el listado de usuarios que tienen acceso a esta encuesta en forma directa
		$sql = "SELECT DISTINCT B.UserID ".
			"FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerUser B ".
			"WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$this->SurveyID.
			" ORDER BY 1";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS)
		{
			while(!$aRS->EOF)
			{
				$intUserID = (int)$aRS->fields["userid"];
				$arrUserIDs[$intUserID] = $intUserID;
				$aRS->MoveNext();
			}
		}
		
		$arrUserGroups = array();
		//Obtener el listado de usuarios que tienen acceso a esta encuesta por medio de sus roles
		$sql = "SELECT DISTINCT B.RolID ".
			"FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerRol B ".
			"WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$this->SurveyID.
			" ORDER BY 1";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS)
		{
			while(!$aRS->EOF)
			{
				$intUserGroupID = (int)$aRS->fields["rolid"];
				$arrUserGroups[$intUserGroupID] = $intUserGroupID;
				$aRS->MoveNext();
			}
		}
		
		//Ya con los grupos de usuario, hay que accesar al Repositorio BITAM para obtener la lista de usuarios que contiene cada uno
		if (count($arrUserGroups) > 0)
		{
			$sql = "SELECT A.CLA_USUARIO AS UserID ".
				"FROM SI_ROL_USUARIO A ".
				"WHERE A.CLA_ROL IN (".implode(',', $arrUserGroups).") ".
				"ORDER BY 1";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS)
			{
				while(!$aRS->EOF)
				{
					$intUserID = (int)$aRS->fields["userid"];
					$arrUserIDs[$intUserID] = $intUserID;
					$aRS->MoveNext();
				}
			}
		}
		
		return $arrUserIDs;
	}
    
	//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
	/* Esta función se encargará de buscar todas las posibles variables de preguntas que actualmente existan en la definición de la encuesta ($aSurveyID),
	basandose en la versión del Administrador ($anAdminVersionNum) que hizo el grabado de la propiedad que se pide reemplazar ($sText), ya que 
	previo a la versión esvExtendedModifInfo siempre se grababan números de pregunta en lugar de IDs, así que debe comprobar si es o no necesario
	hacer un cambio en el texto en base a dicha versión.
	El parámetro $bServerVariables realmente sólo aplicaría si se hiciera algún cambio en el método de reemplazo, ya que las variables del server
	tienen la particularidad que no se han encerrado entre { }, por lo que es mas sencillo el reemplazo ya que se puede hacer directo asumiendo que
	siempre seguirían la sintaxis @Q# directamente, a diferencia de las variables del App que pueden contener espacios y aún así funcionar
	adecuadamente, sin embargo para efectos de este proceso, se tomará como premisa que todas las variables siguen el patrón @Q# sin espacios, ya que
	se complicaría mas hacer un parse que considerara espacios intermedios
	El proceso de reemplazo primero cambiará de Números de pregunta a una variable ligeramente diferente (@QN#) almacenando en un array los casos
	identificados, esto para que al final se reemplace por las variables intermedias y así evitar reemplazos dobles cuando IDs y números de preguntas
	tengan valores similares
	Esta función se debería invocar con los parámetros completos sólo previa al grabado de un objeto, si se va a invocar en forma limitada (sin
	identificar al objeto y propiedad sobre quien se usa) serviría sólo como forma de comprobación o preview del resultado final, ya que no realizaría
	el respaldo del dato anterior
	*/
	static function ReplaceVariableQuestionNumbersByIDs($aRepository, $aSurveyID, $sText, $anAdminVersionNum, $iObjectType = 0, $iObjectID = 0, $iPropertyID = 0, $bServerVariables = false) {
		if (getMDVersion() < esvExtendedModifInfo) {
			return $sText;
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		$blnQuestionVars = false;
		$blnSectionVars = false;
		$intOffSet = 0;
		$strResult = $sText;
		$aSurveyID = (int) @$aSurveyID;
		$anAdminVersionNum = (float) @$anAdminVersionNum;
		
		//@JAPR 2017-02-17: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		$blnPartialIDsReplaceFix = ($anAdminVersionNum >= esvPartialIDsReplaceFix);
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		if ($bServerVariables) {
			//Las variables del server se caracterizan por no estar delimitadas entre { }
			$blnQuestionVars = stripos($sText, '@Q', $intOffSet);
			if ($blnQuestionVars === false) {
				$blnQuestionVars = stripos($sText, '$Q', $intOffSet);
			}
		}
		else {
			//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
			$blnQuestionVars = stripos($sText, '{@Q', $intOffSet);
			if ($blnQuestionVars === false) {
				$blnQuestionVars = stripos($sText, '{$Q', $intOffSet);
			}
		}
		
		if ($bServerVariables) {
			//Las variables del server se caracterizan por no estar delimitadas entre { }
			$blnSectionVars = stripos($sText, '@S', $intOffSet);
			if ($blnSectionVars === false) {
				$blnSectionVars = stripos($sText, '$S', $intOffSet);
			}
		}
		else {
			//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
			$blnSectionVars = stripos($sText, '{@S', $intOffSet);
			if ($blnSectionVars === false) {
				$blnSectionVars = stripos($sText, '{$S', $intOffSet);
			}
		}
		//@JAPR
		
		//Si no se encuentran variables de preguntas, la versión de la propiedad no es de una que todavía hubiera grabado con números de preguntas,
		//o bien no hay cómo poder realizar el reemplazo, entonces se deja el texto original
		//@JAPR 2014-09-18: Realmente no se debe validar contra la versión, si está grabando (se invoca en UpdateFromArray) entonces debió haber
		//sido configurada utilizando números de preguntas, por tanto en esta versión siempre debería convertir a IDs
		if (($blnQuestionVars === false && $blnSectionVars === false) || is_null($aRepository) || $aSurveyID <= 0) {	//|| $anAdminVersionNum >= esvExtendedModifInfo
			return $sText;
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
		if ($blnQuestionVars !== false) {
			$objQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objQuestionCollection)) {
				$objQuestionCollection = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $aSurveyID);
			}
			
			//Primero se tiene que ordenar la lista de variables en base al número de pregunta, ya que se debe hacer el replace de tal manera que
			//primero se reemplacen los números mas grandes (la colección de preguntas ya se regresa en forma ordenada, así que simplemente se recorre
			//en orden inverso)
			//Este array contendrá las instancias de las preguntas que si fueron encontradas y que se tendrán que reemplazar en el string
			//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
			//Se tienen que considerar las variables tanto del viejo formato como del nuevo, así que se recorre por los dos caracteres identificadores
			$arrIdentifiers = array('@', '$');
			foreach ($arrIdentifiers as $strVarIdentifier) {
				$arrReplacedVars = array();
				$intNumQuestions = count($objQuestionCollection->Collection) -1;
				//foreach ($objQuestionCollection->Collection as $aQuestion) {
				for ($intIndex = $intNumQuestions; $intIndex >= 0; $intIndex--) {
					$aQuestion = @$objQuestionCollection->Collection[$intIndex];
					if (!$aQuestion) {
						continue;
					}
					
					//Las variables del server se caracterizan por no estar delimitadas entre { }
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					$strNumVar = (($bServerVariables)?'':'{').$strVarIdentifier."Q".$aQuestion->QuestionNumber.(($bServerVariables)?'':'.');
					$intPos = stripos($strResult, $strNumVar, $intOffSet);
					if ($intPos === false) {
						continue;
					}
					
					//En este caso la variable se encuentra por lo menos una vez en el texto especificado, así que se procederá a reemplazarla primero
					//con un nombre de variable temporal y posteriormente en un ciclo mas adelante se utilizará el ID de la pregunta, por ahora se agrega
					//la instancia de pregunta al array de reemplazos y se cambia el texto de la propiedad
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."QN".$aQuestion->QuestionNumber.(($bServerVariables)?'':'.');
					$arrReplacedVars[$aQuestion->QuestionID] = $aQuestion;
					$strResult = str_ireplace($strNumVar, $strTempVar, $strResult);
				}
				
				if (count($arrReplacedVars) > 0) {
					//@JAPR 2017-12-20: Corregido un bug, haber hecho este ordenamiento provocaba que se hicieran reemplazos parciales, en el caso de Idealease donde la pregunta #1 tenía
					//el ID mas grande, si al mismo tiempo colocaban otra pregunta cuyo número empezara con 1 también (ejemplo: 10, 11, 19, 104, 130, etc.) y cuyo ID fuera > que el de la
					//pregunta #1, entonces al haber hecho este ordenamiento de IDs sobre el array ya procesado por # de pregunta de manera descendente (y por tanto ya preparado para evitar
					//reemplazos parciales) provocaba que la pregunta #1 que tenía ID mas grande se reemplzara al inicio, pero dado a que su expresión de búsqueda era un subconjunto de
					//las expresiones de las preguntas con # mayor pero con ID menor, terminaba reemplazando por ejemplo {$QN130 con el contenido de {$QN1 así que en lugar de un ID como
					//{$Q450} quedaba un ID como {$Q56030}, donde el "30" era la parte de la expresión mas grande pero que se intentaba reemplazar al final porque primero reemplazó la
					//expresión mas pequeña que si lo contenía, así que finalmente si hacía un reemplazo parcial.
					//Debido a que el código opuesto que traduce de IDs a número cometió el mismo error, se deberá actualizar LastVersion = esvPartialIDsReplaceFix antes de evitar
					//realizar estos reemplazos, de tal manera que el código opuesto sólo realice este ordenamiento equivocado también si no se había grabado aún con dicha versión para
					//mantener la consistencia de las traducciones de IDs por # de pregunta (#)
					
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					//A partir de esta actualización esvPartialIDsReplaceFix ya NO se reordenará para evitar que se realice el grabado de manera incorrecta con reemplazos parciales
					//Reordena el array basado en los IDs de las preguntas de forma ascendente
					$arrQuestionIDs = array_keys($arrReplacedVars);
					/*natcasesort($arrQuestionIDs);
					$arrQuestionIDs = array_reverse($arrQuestionIDs);*/
					//@JAPR
					
					//Realiza el reemplazo final de las variables numéricas por las variables basadas en IDs
					foreach ($arrQuestionIDs as $intQuestionID) {
						$aQuestion = @$arrReplacedVars[$intQuestionID];
						if ($intQuestionID <= 0 || is_null($aQuestion)) {
							continue;
						}
						
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."QN".$aQuestion->QuestionNumber.(($bServerVariables)?'':'.');
						$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."Q".$aQuestion->QuestionID.(($bServerVariables)?'':'.');
						$strResult = str_ireplace($strTempVar, $strIDVar, $strResult);
					}
				}
			}
			//@JAPR
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
		if ($blnSectionVars !== false) {
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
			//@JAPR 2017-02-21: Agregado el método para caché de secciones, parámetro $bAddCache. Se hizo de esta manera porque no hubo tiempo de verificar si agregar cache siempre habría o no
			//afectado algún proceso, por tanto sólo se usará bajo demanda específicamente en el proceso que lo va a utilizar
			$objSectionCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID, null, null, true);
			if (is_null($objSectionCollection)) {
				$objSectionCollection = BITAMSectionCollection::NewInstanceEmpty($aRepository, $aSurveyID);
			}
			
			//Primero se tiene que ordenar la lista de variables en base al número de sección, ya que se debe hacer el replace de tal manera que
			//primero se reemplacen los números mas grandes (la colección de secciones ya se regresa en forma ordenada, así que simplemente se recorre
			//en orden inverso)
			//Este array contendrá las instancias de las secciones que si fueron encontradas y que se tendrán que reemplazar en el string
			//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
			//Se tienen que considerar las variables tanto del viejo formato como del nuevo, así que se recorre por los dos caracteres identificadores
			$arrIdentifiers = array('@', '$');
			foreach ($arrIdentifiers as $strVarIdentifier) {
				$arrReplacedVars = array();
				$intNumSections = count($objSectionCollection->Collection) -1;
				//foreach ($objSectionCollection->Collection as $aSection) {
				for ($intIndex = $intNumSections; $intIndex >= 0; $intIndex--) {
					$aSection = @$objSectionCollection->Collection[$intIndex];
					if (!$aSection) {
						continue;
					}
					
					//Las variables del server se caracterizan por no estar delimitadas entre { }
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					$strNumVar = (($bServerVariables)?'':'{').$strVarIdentifier."S".$aSection->SectionNumber.(($bServerVariables)?'':'.');
					$intPos = stripos($strResult, $strNumVar, $intOffSet);
					if ($intPos === false) {
						continue;
					}
					
					//En este caso la variable se encuentra por lo menos una vez en el texto especificado, así que se procederá a reemplazarla primero
					//con un nombre de variable temporal y posteriormente en un ciclo mas adelante se utilizará el ID de la sección, por ahora se agrega
					//la instancia de sección al array de reemplazos y se cambia el texto de la propiedad
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."SN".$aSection->SectionNumber.(($bServerVariables)?'':'.');
					$arrReplacedVars[$aSection->SectionID] = $aSection;
					$strResult = str_ireplace($strNumVar, $strTempVar, $strResult);
				}
				
				if (count($arrReplacedVars) > 0) {
					//@JAPR 2017-12-20: Corregido un bug, haber hecho este ordenamiento provocaba que se hicieran reemplazos parciales, en el caso de Idealease donde la pregunta #1 tenía
					//el ID mas grande, si al mismo tiempo colocaban otra pregunta cuyo número empezara con 1 también (ejemplo: 10, 11, 19, 104, 130, etc.) y cuyo ID fuera > que el de la
					//pregunta #1, entonces al haber hecho este ordenamiento de IDs sobre el array ya procesado por # de pregunta de manera descendente (y por tanto ya preparado para evitar
					//reemplazos parciales) provocaba que la pregunta #1 que tenía ID mas grande se reemplzara al inicio, pero dado a que su expresión de búsqueda era un subconjunto de
					//las expresiones de las preguntas con # mayor pero con ID menor, terminaba reemplazando por ejemplo {$QN130 con el contenido de {$QN1 así que en lugar de un ID como
					//{$Q450} quedaba un ID como {$Q56030}, donde el "30" era la parte de la expresión mas grande pero que se intentaba reemplazar al final porque primero reemplazó la
					//expresión mas pequeña que si lo contenía, así que finalmente si hacía un reemplazo parcial.
					//Debido a que el código opuesto que traduce de IDs a número cometió el mismo error, se deberá actualizar LastVersion = esvPartialIDsReplaceFix antes de evitar
					//realizar estos reemplazos, de tal manera que el código opuesto sólo realice este ordenamiento equivocado también si no se había grabado aún con dicha versión para
					//mantener la consistencia de las traducciones de IDs por # de pregunta (#)
					
					//Reordena el array basado en los IDs de las secciones de forma ascendente
					$arrSectionIDs = array_keys($arrReplacedVars);
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					//A partir de esta actualización esvPartialIDsReplaceFix ya NO se reordenará para evitar que se realice el grabado de manera incorrecta con reemplazos parciales
					//Reordena el array basado en los IDs de las preguntas de forma ascendente
					/*natcasesort($arrSectionIDs);
					$arrSectionIDs = array_reverse($arrSectionIDs);*/
					//@JAPR
					
					//Realiza el reemplazo final de las variables numéricas por las variables basadas en IDs
					foreach ($arrSectionIDs as $intSectionID) {
						$aSection = @$arrReplacedVars[$intSectionID];
						if ($intSectionID <= 0 || is_null($aSection)) {
							continue;
						}
						
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."SN".$aSection->SectionNumber.(($bServerVariables)?'':'.');
						$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."S".$aSection->SectionID.(($bServerVariables)?'':'.');
						$strResult = str_ireplace($strTempVar, $strIDVar, $strResult);
					}
				}
			}
			//@JAPR
		}
		
		//@JAPRDescontinuada: Este proceso se usó originalmente en v5 durante el cambio de números de IDs, pero es obsoleto para v6 así que para no estar
		//generando basura se va a remover
		/*
		//Antes de continuar se tiene que almacenar el historial de cambios realizados, para esto se tiene una tabla donde se almacenan las fórmulas
		//anteriores y la fecha en que se realizó el cambio, así en caso de error se puede restaurar el valor original. La PK impedirá que se pudiera
		//sobreescribir el valor original en un grabado posterior si por alguna razón hay un error en el proceso y se tiene que repetir la actualización
		//Sólo se realiza este proceso si se recibieron las propiedades para identificar al objeto
		$iObjectType = (int) @$iObjectType;
		$iObjectID = (int) @$iObjectID;
		$iPropertyID = (int) @$iPropertyID;
		if ($iObjectType > 0 && $iObjectID > 0 && $iPropertyID > 0) {
			$sql = "CREATE TABLE SI_SV_SurveyFormulaReplace (
				SurveyID INT NOT NULL,
				ObjectType INT NOT NULL,
				ObjectID INT NOT NULL,
				PropertyID INT NOT NULL,
				OldValue TEXT NULL,
				LastDateID datetime NULL,
				LastVersion FLOAT NULL,
				PRIMARY KEY (SurveyID, ObjectType, ObjectID, PropertyID))";
			$aRepository->DataADOConnection->Execute($sql);
			
			$currentDate = date("Y-m-d H:i:s");
			$sql = "INSERT INTO SI_SV_SurveyFormulaReplace (SurveyID, ObjectType, ObjectID, PropertyID, OldValue, LastDateID, LastVersion) 
				VALUES ({$aSurveyID}, {$iObjectType}, {$iObjectID}, {$iPropertyID}, ".
				$aRepository->DataADOConnection->Quote($sText).",".
				$aRepository->DataADOConnection->DBTimeStamp($currentDate).",".
				ESURVEY_SERVICE_VERSION.")";
			$aRepository->DataADOConnection->Execute($sql);
		}
		*/
		
		return $strResult;
	}
	
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		//@JAPR 2017-02-21: Corregido un bug, ya no se debe usar nunca mas $blnServerVariables pues a partir de v6 siempre se usará el formato nuevo de variables
		if (getMDVersion() < esveFormsv6) {
			switch ((int) @$iPropertyID) {
				case optyAdditionalEMails:
					$blnServerVariables = true;
					break;
			}
		}
		//@JAPR
		
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, otySurvey, $this->SurveyID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función opuesta a ReplaceVariableQuestionNumbersByIDs y mucho mas simple, ya que esta implica que ya se tienen grabadas las propiedades usando
	los IDs para las variables (lo cual es precisamente lo correcto) y simplemente presentará el mismo string usando números de preguntas, que es
	como se debe hacer a nivel del usuario ya que ese es un valor que puede identificar fácilmente, por tanto no hay necesidad de respaldar el valor
	anterior ya que esta función se utilizaría siempre sólo como un traductor y no necesariamente implicará que se actualizará alguna propiedad
	La validación de versión en este caso es inversa también, ya que se asume que si fue grabada posterior a la versión que implementó este cambio,
	entonces si es necesario hacer esta conversión, de lo contrario ya estaría grabada con números de preguntas
	*/
	static function ReplaceVariableQuestionIDsByNumbers($aRepository, $aSurveyID, $sText, $anAdminVersionNum, $bServerVariables = false) {
		if (getMDVersion() < esvExtendedModifInfo) {
			return $sText;
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		$blnQuestionVars = false;
		$blnSectionVars = false;
		$intOffSet = 0;
		$strResult = $sText;
		$aSurveyID = (int) @$aSurveyID;
		$anAdminVersionNum = (float) @$anAdminVersionNum;
		
		//@JAPR 2017-02-17: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		$blnPartialIDsReplaceFix = ($anAdminVersionNum >= esvPartialIDsReplaceFix);
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		if ($bServerVariables) {
			//Las variables del server se caracterizan por no estar delimitadas entre { }
			$blnQuestionVars = stripos($sText, '@Q', $intOffSet);
			if ($blnQuestionVars === false) {
				$blnQuestionVars = stripos($sText, '$Q', $intOffSet);
			}
		}
		else {
			//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
			$blnQuestionVars = stripos($sText, '{@Q', $intOffSet);
			if ($blnQuestionVars === false) {
				$blnQuestionVars = stripos($sText, '{$Q', $intOffSet);
			}
		}
		
		if ($bServerVariables) {
			//Las variables del server se caracterizan por no estar delimitadas entre { }
			$blnSectionVars = stripos($sText, '@S', $intOffSet);
			if ($blnSectionVars === false) {
				$blnSectionVars = stripos($sText, '$S', $intOffSet);
			}
		}
		else {
			//En este caso es una variable de App, asi que es obligatorio estar delimitadas entre { }
			$blnSectionVars = stripos($sText, '{@S', $intOffSet);
			if ($blnSectionVars === false) {
				$blnSectionVars = stripos($sText, '{$S', $intOffSet);
			}
		}
		//@JAPR
		
		//Si no se encuentran variables de preguntas, la versión de la propiedad es de una que todavía hubiera grabado con números de preguntas,
		//o bien no hay cómo poder realizar el reemplazo, entonces se deja el texto original
		if (($blnQuestionVars === false && $blnSectionVars === false) || is_null($aRepository) || $aSurveyID <= 0 || $anAdminVersionNum < esvExtendedModifInfo) {
			return $sText;
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
		if ($blnQuestionVars !== false) {
			$objQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objQuestionCollection)) {
				$objQuestionCollection = BITAMQuestionCollection::NewInstanceEmpty($aRepository, $aSurveyID);
			}
			
			//Primero se tiene que ordenar la lista de variables en base al ID de pregunta, ya que se debe hacer el replace de tal manera que
			//primero se reemplacen los números mas grandes
			//Este array contendrá las instancias de las preguntas que si fueron encontradas y que se tendrán que reemplazar en el string
			//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
			//Se tienen que considerar las variables tanto del viejo formato como del nuevo, así que se recorre por los dos caracteres identificadores
			$arrIdentifiers = array('@', '$');
			foreach ($arrIdentifiers as $strVarIdentifier) {
				$arrReplacedVars = array();
				foreach ($objQuestionCollection->Collection as $aQuestion) {
					//@JAPR 2017-12-20: Corregido un bug, NO haber hecho este ordenamiento (ya que a diferencia de la función opuesta a esta la cual procea por posición, esta procesa por
					//ID pero la función que carga los objetos los regresa por posición) provocaba que se hicieran reemplazos parciales, en el caso de Idealease donde la pregunta #1 tenía
					//un ID mas grande, si al mismo tiempo colocaban otra pregunta cuyo ID empezara con el mismo prefijo también (ejemplo si el ID fuera 1 entonces otros casos que habría 
					//provocado error serían: 10, 11, 19, 104, 130, etc.) y cuyo ID fuera < que el de la pregunta #1, entonces al no haber hecho este ordenamiento de IDs previo a comprobar 
					//cada variable provocaba que se incluyera siempre la pregunta cuyo ID era un subconjunto de otros IDs y que debido al ordenamiento de mas adelante que ya era ahora si 
					//por ID, se terminaba reemplazando de manera parcial, esto generalmente es dificil que se de con formas posteriores a la primera, donde los IDs ya están en el orden de
					//las decenas y para volver a tener el mismo ID como prefijo habría que haber tenido tantas preguntas que alcanzaran el orden de las centenas, y así sucesivamente, sin
					//embargo por un error también debido a una ordenación en aquel caso SI realizada indebidamente en la función opuesta a esta, internamente si quedaban Ids con prefijos
					//iguales (por ejemplo {$Q563.value} siendo esa la pregunta #1, y {$Q56330.value} siendo esa la pregunta #130 que se reemplazó mal con el mismo ID de la pregunta #1 pero 
					//sólo parcialmente para el primer 1 del 130), pero en ese caso muy específico si era deseable que este mismo error entrara en efecto para restaurar el valor correctamente
					//siguiendo exactamente el proceso opuesto al error que lo generó, así que no se podrá simplemente agregar aquí el ordenamiento real ni empezar a hacer los reemplazos
					//completos hasta el "." a menos que este seguro que la expresión fue grabada correctamente con el código ya libre del error original, así que se validará a partir
					//del LastVersion del objeto el cual se tendrá que forzar a grabar junto con la actualización simultánea de todas las otras configuraciones con expresiones de variables
					//cuando se grabe el objeto por cualquier motivo
					
					//Las variables del server se caracterizan por no estar delimitadas entre { }
					//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
					//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
					$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."Q".$aQuestion->QuestionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
					$intPos = stripos($strResult, $strIDVar, $intOffSet);
					if ($intPos === false) {
						continue;
					}
					
					$arrReplacedVars[$aQuestion->QuestionID] = $aQuestion;
				}
				
				if (count($arrReplacedVars) > 0) {
					//Reordena el array basado en los IDs de las preguntas de forma ascendente
					$arrQuestionIDs = array_keys($arrReplacedVars);
					natcasesort($arrQuestionIDs);
					$arrQuestionIDs = array_reverse($arrQuestionIDs);
					
					//Inicia el reemplazo por las variables temporales
					foreach ($arrQuestionIDs as $intQuestionID) {
						$aQuestion = @$arrReplacedVars[$intQuestionID];
						if ($intQuestionID <= 0 || is_null($aQuestion)) {
							continue;
						}
						
						//Las variables del server se caracterizan por no estar delimitadas entre { }
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."Q".$aQuestion->QuestionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						
						//En este caso la variable se encuentra por lo menos una vez en el texto especificado, así que se procederá a reemplazarla primero
						//con un nombre de variable temporal y posteriormente en un ciclo mas adelante se utilizará el número de la pregunta
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."QN".$aQuestion->QuestionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strResult = str_ireplace($strIDVar, $strTempVar, $strResult);
					}
					
					//Realiza el reemplazo final de las variables de IDs por las variables basadas numéricas
					foreach ($arrQuestionIDs as $intQuestionID) {
						$aQuestion = @$arrReplacedVars[$intQuestionID];
						if ($intQuestionID <= 0 || is_null($aQuestion)) {
							continue;
						}
						
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."QN".$aQuestion->QuestionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strNumVar = (($bServerVariables)?'':'{').$strVarIdentifier."Q".$aQuestion->QuestionNumber.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strResult = str_ireplace($strTempVar, $strNumVar, $strResult);
					}
				}
			}
			//@JAPR
		}
		
		//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
		//@JAPR 2014-11-10: Corregido un bug, esta variable traía una posición así que se debe preguntar como boolean
		if ($blnSectionVars !== false) {
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "."
			//@JAPR 2017-02-21: Agregado el método para caché de secciones, parámetro $bAddCache. Se hizo de esta manera porque no hubo tiempo de verificar si agregar cache siempre habría o no
			//afectado algún proceso, por tanto sólo se usará bajo demanda específicamente en el proceso que lo va a utilizar
			$objSectionCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID, null, null, true);
			if (is_null($objSectionCollection)) {
				$objSectionCollection = BITAMSectionCollection::NewInstanceEmpty($aRepository, $aSurveyID);
			}
			
			//Primero se tiene que ordenar la lista de variables en base al ID de la sección, ya que se debe hacer el replace de tal manera que
			//primero se reemplacen los números mas grandes
			//Este array contendrá las instancias de las secciones que si fueron encontradas y que se tendrán que reemplazar en el string
			//@JAPR 2014-10-16: Agregado el reemplazo de las variables de sección y el nuevo formato de variables
			//Se tienen que considerar las variables tanto del viejo formato como del nuevo, así que se recorre por los dos caracteres identificadores
			$arrIdentifiers = array('@', '$');
			foreach ($arrIdentifiers as $strVarIdentifier) {
				$arrReplacedVars = array();
				foreach ($objSectionCollection->Collection as $aSection) {
					//@JAPR 2017-12-20: Corregido un bug, NO haber hecho este ordenamiento (ya que a diferencia de la función opuesta a esta la cual procea por posición, esta procesa por
					//ID pero la función que carga los objetos los regresa por posición) provocaba que se hicieran reemplazos parciales, en el caso de Idealease donde la pregunta #1 tenía
					//un ID mas grande, si al mismo tiempo colocaban otra pregunta cuyo ID empezara con el mismo prefijo también (ejemplo si el ID fuera 1 entonces otros casos que habría 
					//provocado error serían: 10, 11, 19, 104, 130, etc.) y cuyo ID fuera < que el de la pregunta #1, entonces al no haber hecho este ordenamiento de IDs previo a comprobar 
					//cada variable provocaba que se incluyera siempre la pregunta cuyo ID era un subconjunto de otros IDs y que debido al ordenamiento de mas adelante que ya era ahora si 
					//por ID, se terminaba reemplazando de manera parcial, esto generalmente es dificil que se de con formas posteriores a la primera, donde los IDs ya están en el orden de
					//las decenas y para volver a tener el mismo ID como prefijo habría que haber tenido tantas preguntas que alcanzaran el orden de las centenas, y así sucesivamente, sin
					//embargo por un error también debido a una ordenación en aquel caso SI realizada indebidamente en la función opuesta a esta, internamente si quedaban Ids con prefijos
					//iguales (por ejemplo {$Q563.value} siendo esa la pregunta #1, y {$Q56330.value} siendo esa la pregunta #130 que se reemplazó mal con el mismo ID de la pregunta #1 pero 
					//sólo parcialmente para el primer 1 del 130), pero en ese caso muy específico si era deseable que este mismo error entrara en efecto para restaurar el valor correctamente
					//siguiendo exactamente el proceso opuesto al error que lo generó, así que no se podrá simplemente agregar aquí el ordenamiento real ni empezar a hacer los reemplazos
					//completos hasta el "." a menos que este seguro que la expresión fue grabada correctamente con el código ya libre del error original, así que se validará a partir
					//del LastVersion del objeto el cual se tendrá que forzar a grabar junto con la actualización simultánea de todas las otras configuraciones con expresiones de variables
					//cuando se grabe el objeto por cualquier motivo
					
					//Las variables del server se caracterizan por no estar delimitadas entre { }
					$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."S".$aSection->SectionID;
					$intPos = stripos($strResult, $strIDVar, $intOffSet);
					if ($intPos === false) {
						continue;
					}
					
					$arrReplacedVars[$aSection->SectionID] = $aSection;
				}
				
				if (count($arrReplacedVars) > 0) {
					//Reordena el array basado en los IDs de las secciones de forma ascendente
					$arrSectionIDs = array_keys($arrReplacedVars);
					natcasesort($arrSectionIDs);
					$arrSectionIDs = array_reverse($arrSectionIDs);
					
					//Inicia el reemplazo por las variables temporales
					foreach ($arrSectionIDs as $intSectionID) {
						$aSection = @$arrReplacedVars[$intSectionID];
						if ($intSectionID <= 0 || is_null($aSection)) {
							continue;
						}
						
						//Las variables del server se caracterizan por no estar delimitadas entre { }
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strIDVar = (($bServerVariables)?'':'{').$strVarIdentifier."S".$aSection->SectionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						
						//En este caso la variable se encuentra por lo menos una vez en el texto especificado, así que se procederá a reemplazarla primero
						//con un nombre de variable temporal y posteriormente en un ciclo mas adelante se utilizará el número de la sección
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."SN".$aSection->SectionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strResult = str_ireplace($strIDVar, $strTempVar, $strResult);
					}
					
					//Realiza el reemplazo final de las variables de IDs por las variables basadas numéricas
					foreach ($arrSectionIDs as $intSectionID) {
						$aSection = @$arrReplacedVars[$intSectionID];
						if ($intSectionID <= 0 || is_null($aSection)) {
							continue;
						}
						
						//@JAPR 2017-02-20: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
						//Ahora buscará hasta el "." si la versión del código que grabó es compatible con la corrección, de lo contrario buscará con error como antes
						$strTempVar = (($bServerVariables)?'':'{').$strVarIdentifier."SN".$aSection->SectionID.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strNumVar = (($bServerVariables)?'':'{').$strVarIdentifier."S".$aSection->SectionNumber.(($bServerVariables || !$blnPartialIDsReplaceFix)?'':'.');
						$strResult = str_ireplace($strTempVar, $strNumVar, $strResult);
					}
				}
			}
			//@JAPR
		}
		
		return $strResult;
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, $this->LastModAdminVersion, $bServerVariables);
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	/* Esta función actualizará las propiedades que pudieran tener variables en su interior y que no fueron asignadas por la función UpdateFromArray, forzando a traducirlas primero 
	a números de pregunta para que en caso de que el objeto no hubiera sido grabado aún con la versión que ya corregía los IDs se puedan recuperar correctamente, posteriormente las
	volverá a traducir a IDs pero ahora aplicando ya la corrección para no realizar reemplazos parciales. Esta función debe utilizarse siempre antesde un Save del objeto para que 
	durante el primer grabado sea cual fuera la razón para hacerlo, se puedan traducir correctamente las propiedades al momento de asignar el numero de versión de este servicio que ya
	asumirá que graba correctamente este tipo de propiededes, por tanto su uso es:
	1- Esperar a un UpdateFromArray (opcional, si no se invocó, simplemente sobreescribirá todas las propiedades, como por ejemplo en el proceso de copiado)
	2- Ejecutar esta función (si hubo un UpdateFromArray, algunas propiedades podrían ya estar asignadas en el array de reemplazos, así que esas se ignoran)
	3- Ejecutar al Save (esto sobreescribirá al objeto y sus propiedades que utilizan variables
	*/
	function fixAllPropertiesWithVarsNotSet() {
		global $garrUpdatedPropsWithVars;
		
		if (!isset($garrUpdatedPropsWithVars) || !is_array($garrUpdatedPropsWithVars) || !isset($garrUpdatedPropsWithVars["Surveys"])) {
			$garrUpdatedPropsWithVars = array();
			$garrUpdatedPropsWithVars["Surveys"] = array();
		}
		
		//Si ya se había aplicado un grabado a este objeto por lo menos con la versión de este fix, entonces ya no tiene sentido continuar con el proceso pues no habra diferencia
		if ($this->LastModAdminVersion >= esvPartialIDsReplaceFix) {
			return;
		}
		
		//Actualiza las propiedades no asignadas en el array de propiedades modificadas
		//Para que el proceso funcione correctamente, es indispensable que se engañe a la instancia sobreescribiendo temporalmente LastModAdminVersion a la versión actual pero sólo
		//cuando ya se va a reemplazar por IDs, cuando se va a reemplazar por números si puede dejar la versión actual para que auto-corrija el error con el que había grabado usando IDs
		//previamente
		$dblLastModAdminVersion = $this->LastModAdminVersion;
		$blnUpdatedProperties = false;
		//Primero traducirá de IDs a números todas las propiedades no grabadas manualmente para corregir los IDs mal grabados
		if (!isset($garrUpdatedPropsWithVars["Surveys"]["AdditionalEMails"])) {
			$this->AdditionalEMails = $this->TranslateVariableQuestionIDsByNumbers($this->AdditionalEMails);
			$blnUpdatedProperties = true;
		}
		
		if (!$blnUpdatedProperties) {
			return;
		}
		
		//Finalmente actualizará las propiedades no grabadas manualmente a IDs ya aplicando la corrección sobreescribiendo la versión del objeto
		//Como esta función se supone que se usará justo antes de un save, es correcto dejar la propiedad LastModAdminVersion actualizada, ya que de todas formas se modificaría en el save
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		if (!isset($garrUpdatedPropsWithVars["Surveys"]["AdditionalEMails"])) {
			$this->AdditionalEMails = $this->TranslateVariableQuestionNumbersByIDs($this->AdditionalEMails, optyAdditionalEMails);
		}
	}
	//@JAPR
	
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	function save($bJustMobileTables = false, $bCopy=false) {
		//@JAPR 2013-06-18: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		$gblModMngrErrorMessage = '';
		//@JAPR
		
		$currentDate = date("Y-m-d H:i:s");
		$strCaptureStartTime = "";
		$strCaptureEndTime = "";
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Al realizar el grabado del objeto (pero no copiado) se deben actualizar a la par todas las configuraciones que pudieran tener variables para arreglar los IDs mal grabados
		//Se debe realizar antes de la asignación de LastModAdminVersion o de lo contrario ya no tendría efecto pues ya consideraría que está grabado con la versión corregida
		if (!$bCopy) {
			$this->fixAllPropertiesWithVarsNotSet();
		}
		//@JAPR
		
		$isNewObj = $this->isNewObject();
		if(trim($this->CaptureStartTime)=="")
		{
			$strCaptureStartTime = "NULL";
			$strCaptureEndTime = "NULL";
		}
		else 
		{
			$strCaptureStartTime = $this->Repository->DataADOConnection->Quote($this->CaptureStartTime);
			$strCaptureEndTime = $this->Repository->DataADOConnection->Quote($this->CaptureEndTime);
		}
		
		//@JAPR 2012-06-01: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//Se remueve la referencia al catálogo si no se usará la encuesta para recalendarizar
		if (!$this->EnableReschedule)
		{
			$this->RescheduleCatalogID = 0;
			$this->RescheduleCatMemberID = 0;
			$this->RescheduleStatus = '';
		}
		
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		
		$strOriginalWD = getcwd();
	 	if ($this->isNewObject()) {
			$this->CreationAdminVersion = $this->LastModAdminVersion;
			
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(SurveyID)", "0")." + 1 AS SurveyID".
						" FROM SI_SV_Survey";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->SurveyID = (int) $aRS->fields["surveyid"];
			//@LROUX 2011-10-18: Se agrega AllowPartialSave	
			//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas (campo VersionNum)
			//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campo ElementQuestionID, eBavelAppID, eBavelFormID)
			//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
			//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalFields .= ', SyncWhenSaving';
				$strAdditionalValues .= ', '.$this->SyncWhenSaving;
			}
			//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
			if (getMDVersion() >= esveFormsStdQSingleRec) {
				$strAdditionalFields .= ', UseStdSectionSingleRec';
				$strAdditionalValues .= ', '.$this->UseStdSectionSingleRec;
			}
			//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			if (getMDVersion() >= esvCatalogDissociation) {
				$strAdditionalFields .= ', DissociateCatDimens';
				$strAdditionalValues .= ', '.$this->DissociateCatDimens;
			}
			//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
			if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
				$strAdditionalFields .= ',OtherLabel,CommentLabel';
				$strAdditionalValues .= ','.$this->Repository->DataADOConnection->Quote($this->OtherLabel).','.
					$this->Repository->DataADOConnection->Quote($this->CommentLabel);
			}
			//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalFields .= ', eBavelActionFormID';
				$strAdditionalValues .= ', '.$this->eBavelActionFormID;
			}
			//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
			if (getMDVersion() >= esvReportEMailsByCatalog) {
				$strAdditionalFields .= ', AdditionalEMails';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->AdditionalEMails);
			}
			//@JAPR 2014-03-18: Agregado el icono para las encuestas
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalFields .= ', SurveyImageText';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->SurveyImageText);
			}
			//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
			if (getMDVersion() >= esvMenuImageConfig) {
				$strAdditionalFields .= ', SurveyMenuImage';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->SurveyMenuImage);
			}
			//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
			//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
			if (getMDVersion() >= esvHideNavigationButtons) {
				$strAdditionalFields .= ', HideNavButtons, HideSectionNames';
				$strAdditionalValues .= ', '.$this->HideNavButtons.', '.$this->HideSectionNames;
			}
			//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
			if (getMDVersion() >= esvSurveySectDisableOpts) {
				$strAdditionalFields .= ', DisableEntry';
				$strAdditionalValues .= ', '.$this->DisableEntry;
			}
			//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalFields .= ', CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$this->LastModAdminVersion.', '.$this->LastModAdminVersion;
			}
			//@JAPR
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalFields .= ', IsAgendable, RadioForAgenda';
				$strAdditionalValues .= ', '.$this->IsAgendable.', '.$this->RadioForAgenda;
			}
			//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
			if (getMDVersion() >= esvAdminWYSIWYG) {
				$strAdditionalFields .= ', SurveyDesc';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->SurveyDesc);
			}
			//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
			if (getMDVersion() >= esvDestinationIncremental) {
				$strAdditionalFields .= ', incrementalFile';
				$strAdditionalValues .= ', '.$this->Repository->DataADOConnection->Quote($this->incrementalFile);
			}
			//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
			if (getMDVersion() >= esvFormsLayouts) {
				$strAdditionalFields .= ', Width, Height, TextPosition, TextSize, CustomLayout';
				$strAdditionalValues .= ', '.$this->Width.', '.$this->Height.', '.$this->TextPosition.', '.
					$this->Repository->DataADOConnection->Quote($this->TextSize).', '.
					$this->Repository->DataADOConnection->Quote($this->CustomLayout);
			}
			//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
			if (getMDVersion() >= esvShowQNumbers) {
				$strAdditionalFields .= ', ShowQNumbers';
				$strAdditionalValues .= ', '.$this->ShowQNumbers;
			}
			//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
			if (getMDVersion() >= esvTemplateStyles) {
				$strAdditionalFields .= ', TemplateID';
				$strAdditionalValues .= ', '.$this->TemplateID;
			}
			//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
			if (getMDVersion() >= esvFormsDuration) {
				$strAdditionalFields .= ', PlannedDuration';
				$strAdditionalValues .= ', '.$this->PlannedDuration;
			}
			//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
			if (getMDVersion() >= esvNavigationHistory) {
				$strAdditionalFields .= ', EnableNavHistory';
				$strAdditionalValues .= ', '.$this->EnableNavHistory;
			}
			//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
			if (getMDVersion() >= esvAutoFitFormsImage) {
				$strAdditionalFields .= ', AutoFitImage';
				$strAdditionalValues .= ', '.$this->AutoFitImage;
			}
			//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
			if (getMDVersion() >= esvEntryDescription) {
				$strAdditionalFields .= ', QuestionIDForEntryDesc';
				$strAdditionalValues .= ', '.$this->QuestionIDForEntryDesc;
			}
			if ( getMDVersion() >= esvReloadSurveyFromSection ) {
				$strAdditionalFields .= ', ReloadSurveyFromSection';
				$strAdditionalValues .= ', '.$this->ReloadSurveyFromSection;
			}
			//RV Feb2016: Agregar el campo que indica cual es el ID de la forma a partir de la cual se copio esta forma (cuando sea el caso)
			if (getMDVersion() >= esvCopyForms) {
				$strAdditionalFields .= ', SourceID, FormType';
				if ($this->ActionType == devAction) {
					//Sólo si se trata de una generación de forma de desarrollo es como interesa mantener el apuntador a la forma original y el tipo de forma asignado
					$strAdditionalValues .= ','.$this->CopiedSurveyID.','.$this->FormType;
				}
				else {
					//Cualquier otro tipo de forma deberá mantener sólo el tipo de forma asignado, pero sin apuntadores
					$strAdditionalValues .= ', 0, '.formProduction;
				}
			}
			
			$sql = "INSERT INTO SI_SV_Survey (".
						" SurveyID".
						", SurveyName".
						", Status".
						", ClosingMsg".
						", CreationUserID".
						", CreationDateID".
						", LastUserID".
						", LastDateID".
						", CaptureStartTime".
						", CaptureEndTime".
						", HelpMsg".
						", CatalogID".
						", SurveyType".
						", HasSignature".
						", AllowPartialSave".
						", VersionNum".
						", ElementQuestionID".
						", eBavelAppID".
						", eBavelFormID".
						", EnableReschedule".
						", RescheduleCatalogID".
						", RescheduleCatMemberID".
						", RescheduleStatus".
						$strAdditionalFields.
						") VALUES (".
						$this->SurveyID.
						",".$this->Repository->DataADOConnection->Quote($this->SurveyName).
						",".$this->Status.
						",".$this->Repository->DataADOConnection->Quote($this->ClosingMsg).
						",".$_SESSION["PABITAM_UserID"].
						",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						",".$_SESSION["PABITAM_UserID"].
						",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						",".$strCaptureStartTime.
						",".$strCaptureEndTime.
						",".$this->Repository->DataADOConnection->Quote($this->HelpMsg).
						",".$this->CatalogID.
						",".$this->SurveyType.
						",".$this->HasSignature.
						",".$this->AllowPartialSave.
						",1".
						",".$this->ElementQuestionID.
						",".$this->eBavelAppID.
						",".$this->eBavelFormID.
						",".$this->EnableReschedule.
						",".$this->RescheduleCatalogID.
						",".$this->RescheduleCatMemberID.
						",".$this->Repository->DataADOConnection->Quote($this->RescheduleStatus).
						$strAdditionalValues.
					")";

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//@JAPR 2017-02-07: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			if (getMDVersion() >= esvCopyForms) {
				if ($this->ActionType == devAction && $this->CopiedSurveyID)
				{
					//Si se está generando una forma de desarrollo entonces a la forma original se le cambia el status para que indique que tiene una forma en desarrollo
					$sql = "UPDATE SI_SV_Survey SET SourceID = {$this->SurveyID}, FormType = ".formProdWithDev." WHERE SurveyID = ".$this->CopiedSurveyID;
					if ($this->Repository->DataADOConnection->Execute($sql) === false)
					{
						if($gblShowErrorSurvey) {
							die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
						else {
							return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
						}
					}
				}
			}
			//@JAPR
		}
		else {
			$strCaptureStartTime = "";
			$strCaptureEndTime = "";
			
			if(trim($this->CaptureStartTime)=="")
			{
				$strCaptureStartTime = "NULL";
				$strCaptureEndTime = "NULL";
			}
			else 
			{
				$strCaptureStartTime = $this->Repository->DataADOConnection->Quote($this->CaptureStartTime);
				$strCaptureEndTime = $this->Repository->DataADOConnection->Quote($this->CaptureEndTime);
			}

			// @LROUX 2011-10-18: Se agrega AllowPartialSave					
			//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas (campo VersionNum)
			//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campo ElementQuestionID, eBavelAppID, eBavelFormID)
			//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
			//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedActionsData) {
				$strAdditionalValues .= ', SyncWhenSaving = '.$this->SyncWhenSaving;
			}
			//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
			if (getMDVersion() >= esveFormsStdQSingleRec) {
				$strAdditionalValues .= ', UseStdSectionSingleRec = '.$this->UseStdSectionSingleRec;
			}
			//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
			if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
				$strAdditionalValues .= ', OtherLabel = '.$this->Repository->DataADOConnection->Quote($this->OtherLabel).
					', CommentLabel = '.$this->Repository->DataADOConnection->Quote($this->CommentLabel);
			}
			//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				$strAdditionalValues .= ', eBavelActionFormID = '.$this->eBavelActionFormID;
			}
			//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
			if (getMDVersion() >= esvReportEMailsByCatalog) {
				$strAdditionalValues .= ', AdditionalEMails = '.$this->Repository->DataADOConnection->Quote($this->AdditionalEMails);
			}
			//@JAPR 2014-03-18: Agregado el icono para las encuestas
			if (getMDVersion() >= esvPasswordLangAndRedesign) {
				$strAdditionalValues .= ', SurveyImageText = '.$this->Repository->DataADOConnection->Quote($this->SurveyImageText);
			}
			//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
			if (getMDVersion() >= esvMenuImageConfig) {
				$strAdditionalValues .= ', SurveyMenuImage = '.$this->Repository->DataADOConnection->Quote($this->SurveyMenuImage);
			}
			//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
			//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
			if (getMDVersion() >= esvHideNavigationButtons) {
				$strAdditionalValues .= ', HideNavButtons = '.$this->HideNavButtons.
					', HideSectionNames = '.$this->HideSectionNames;
			}
			//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
			if (getMDVersion() >= esvSurveySectDisableOpts) {
				$strAdditionalValues .= ', DisableEntry = '.$this->DisableEntry;
			}
			//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
			if (getMDVersion() >= esvExtendedModifInfo) {
				$strAdditionalValues .= ', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			if (getMDVersion() >= esvAgendaRedesign) {
				$strAdditionalValues .= ', IsAgendable = '.$this->IsAgendable;
				$strAdditionalValues .= ', RadioForAgenda = '.$this->RadioForAgenda;
			}
			//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
			if (getMDVersion() >= esvAdminWYSIWYG) {
				$strAdditionalValues .= ', SurveyDesc = '.$this->Repository->DataADOConnection->Quote($this->SurveyDesc);
			}
			//@JAPR
			//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
			if (getMDVersion() >= esvDestinationIncremental) {
				$strAdditionalValues .= ', incrementalFile = '.$this->Repository->DataADOConnection->Quote($this->incrementalFile);
			}
			//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
			if (getMDVersion() >= esvFormsLayouts) {
				$strAdditionalValues .= ', Width = '.$this->Width.
					', Height = '.$this->Height.
					', TextPosition = '.$this->TextPosition.
					', TextSize = '.$this->Repository->DataADOConnection->Quote($this->TextSize).
					', CustomLayout = '.$this->Repository->DataADOConnection->Quote($this->CustomLayout);
			}
			//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
			if (getMDVersion() >= esvShowQNumbers) {
				$strAdditionalValues .= ', ShowQNumbers = '.$this->ShowQNumbers;
			}
			//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
			if (getMDVersion() >= esvTemplateStyles) {
				$strAdditionalValues .= ', TemplateID = '.$this->TemplateID;
			}
			//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
			if (getMDVersion() >= esvFormsDuration) {
				$strAdditionalValues .= ', PlannedDuration = '.$this->PlannedDuration;
			}
			//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
			if (getMDVersion() >= esvNavigationHistory) {
				$strAdditionalValues .= ', EnableNavHistory = '.$this->EnableNavHistory;
			}
			//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
			if (getMDVersion() >= esvAutoFitFormsImage) {
				$strAdditionalValues .= ', AutoFitImage = '.$this->AutoFitImage;
			}
			//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
			if (getMDVersion() >= esvEntryDescription) {
				$strAdditionalValues .= ', QuestionIDForEntryDesc = '.$this->QuestionIDForEntryDesc;
			}
			if ( getMDVersion() >= esvReloadSurveyFromSection ) {
				$strAdditionalValues .= ', ReloadSurveyFromSection = '.$this->ReloadSurveyFromSection;
			}
			$sql = "UPDATE SI_SV_Survey SET ".
					"SurveyName = ".$this->Repository->DataADOConnection->Quote($this->SurveyName).
					", Status = ".$this->Status.
					", ClosingMsg = ".$this->Repository->DataADOConnection->Quote($this->ClosingMsg).
					", LastUserID = ".$_SESSION["PABITAM_UserID"].
					", LastDateID = ".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					", CaptureStartTime = ".$strCaptureStartTime.
					", CaptureEndTime = ".$strCaptureEndTime.
					", HelpMsg = ".$this->Repository->DataADOConnection->Quote($this->HelpMsg).
					", CatalogID = ".$this->CatalogID.
					", SurveyType = ".$this->SurveyType.
					", HasSignature = ".$this->HasSignature.
					", AllowPartialSave = ".$this->AllowPartialSave.
					", VersionNum = ".($this->VersionNum +1).
					", ElementQuestionID = ".$this->ElementQuestionID.
					", eBavelAppID = ".$this->eBavelAppID.
					", eBavelFormID = ".$this->eBavelFormID.
					", EnableReschedule = ".$this->EnableReschedule.
					", RescheduleCatalogID = ".$this->RescheduleCatalogID.
					", RescheduleCatMemberID = ".$this->RescheduleCatMemberID.
					", RescheduleStatus = ".$this->Repository->DataADOConnection->Quote($this->RescheduleStatus).
					$strAdditionalValues.
				" WHERE SurveyID = ".$this->SurveyID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
			if (getMDVersion() >= esvCopyForms) {
				if ($this->FormType == formProdWithDev && $this->ActionType == createAction && $this->SourceID > 0) {
					//Si se está actualizando la forma de producción con desarrollo, siendo la única posibilidad a la fecha de implementación que eso sucediera debido a un cambio en el
					//nombre de la forma, entonces se debe actualizar la posible forma de desarrollo que se hubiera creado para esta forma de producción
					$strAdditionalValues = '';
					$sql = "UPDATE SI_SV_Survey SET ".
						"SurveyName = ".$this->Repository->DataADOConnection->Quote($this->SurveyName.' - '.translate("Development")).
						", LastUserID = ".$_SESSION["PABITAM_UserID"].
						", LastDateID = ".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						", VersionNum = VersionNum + 1".
						$strAdditionalValues.
					" WHERE SurveyID = ".$this->SourceID;
					$this->Repository->DataADOConnection->Execute($sql);
				}
			}
			//@JAPR
			
			//Verificamos si el catalogo fue modificado, si es asi, entonces se tienen q eliminar todos los filtros anteriores
			//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
			//Las encuestas ya no tienen un catálogo asociado realmente, además esta tabla cambió su objetivo original (nunca implementado) para
			//contener filtros de todos los posibles catálogos utilizados en sus preguntas/secciones
			/*
			if($this->OldCatalogID!=$this->CatalogID)
			{
				$sql = "DELETE FROM SI_SV_SurveyFilter WHERE SurveyID = ".$this->SurveyID." AND CatalogID = ".$this->OldCatalogID;
				if ($this->Repository->DataADOConnection->Execute($sql) === false)
				{
					if($gblShowErrorSurvey) {
						die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
					else {
						return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
			*/
			//@JAPR
			
			//@AAL 27/05/2015: En versiones de EformsV6 los modelos ya no aplican
			if($this->CreationAdminVersion < esveFormsv6){
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
				
				//Procedemos a actualizar el modelo
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/model.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				global $generateXLS;
				$generateXLS = false;
				$anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
				$anInstanceModel->ModelName = $this->SurveyName;
				$anInstanceModel->readPeriods();
				//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
				$objMMAnswer = @$anInstanceModel->save();
				chdir($strOriginalWD);
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error adding survey's model").": ".$strMMError.". ";
				}
				//@JAPR
				
				//Se actualiza la dimension "Surveys" en la descripcion de la encuesta
				$this->updateSurveyDimName();
			}
		}
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if(!$bCopy && getMDVersion() < esveFormsv6)
		{
			//@JAPR 2013-04-29: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
			//@JAPR 2013-05-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
			if (getMDVersion() >= esveBavelMapping) {
				if ($this->eBavelFieldsData != $this->eBavelFieldsDataOld) {
					$this->saveeBavelFieldsMapping();
				}
			}
			//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
			if (getMDVersion() >= esveBavelSuppActions) {
				if ($this->eBavelActionFieldsData != $this->eBavelActionFieldsDataOld) {
					$this->saveeBavelActionFieldsMapping();
				}
			}
		}
		//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			//RV Sep2016: Si el grabado de la forma proviene de la generación de una copia
			//se inicializa el id del scheduler para que se cree un scheduler nuevo para la copia de la forma
			if($bCopy)
			{
				$this->SchedulerID = 0;
			}
			$this->saveFormScheduler();
		}
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		if ($gblModMngrErrorMessage != '') {
			if($gblShowErrorSurvey) {
				die($gblModMngrErrorMessage);
			}
			else {
				return($gblModMngrErrorMessage);
			}
		}
		
		//@AAL 27/05/2015 Agregado el Rediseño de tablas paralelas para EformsV6 y siguientes.
		$this->createDataTablesAndFields($isNewObj);

		//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
		//Cada vez que se modifique la forma se actualizará su definición en caso de tratarse de una forma de v6
		$this->checkIntegrity();
		//@JAPR
		
		return $this;
	}

	/*@AAL 27/05/2015 Agregado para EformsV6 y siguientes, Se crea la tabla paralela en la cual se almacenaran
	las respuestas de las encuestas tal y como se capturaron. El nombre de la tabla cambia a SVeForms_####. Donde #### es el SurveyID.*/
	//El parámetro $bIsNew nos indicará si se estaba creando o no el objeto, ya que siempre que se invoca ya tiene activado un QuestionID, así que no hay forma de identificar si se estaba
	//creando o no realmente con la propiedad isNewObject en este punto
	function createDataTablesAndFields($bIsNew = false) {
		if ($this->CreationAdminVersion < esveFormsv6) {	
			return;
		}
		
		//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
		$this->setTableAndFieldsProperties();
		//@JAPR
		
		$strTableName = $this->SurveyTable;
		//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
		//Agregar Key como sufijo y cambiar a INTEGER a todos los campos que se quisieran normalizar apuntando a la dimensión global del cubo de Surveys a partir de StartDate
		$defaultType = "VARCHAR(255)";
		$dateType = "VARCHAR(20)";
		//@JAPR 2015-08-30: Agregado el grabado de la Agenda que realizó la captura
		//GCRUZ 2016-05-18. Agregados campos (TransferTime, TransferDistance) para el tiempo y distancia de traslado para una captura
		//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
		$sql = "CREATE TABLE {$strTableName} (
			".BITAMSurvey::$KeyField." INTEGER NOT NULL AUTO_INCREMENT, 
			UserID INTEGER NOT NULL, 
			UserEMail {$defaultType} NULL, 
			DateID DATETIME NOT NULL, 
			DateKey INTEGER NOT NULL, 
			StartDate {$dateType} NULL, 
			StartTime {$dateType} NULL, 
			EndDate {$dateType} NULL, 
			EndTime {$dateType} NULL, 
			ServerStartDate {$dateType} NULL, 
			ServerStartTime {$dateType} NULL, 
			ServerEndDate {$dateType} NULL, 
			ServerEndTime {$dateType} NULL, 
			StartDateEdit {$dateType} NULL, 
			EndDateEdit {$dateType} NULL, 
			StartTimeEdit {$dateType} NULL, 
			EndTimeEdit {$dateType} NULL, 
			ServerStartDateEdit {$dateType} NULL, 
			ServerEndDateEdit {$dateType} NULL, 
			ServerStartTimeEdit {$dateType} NULL, 
			ServerEndTimeEdit {$dateType} NULL, 
			SyncDate {$dateType} NULL, 
			SyncTime {$dateType} NULL, 
	 	    SyncEndDate {$dateType} NULL, 
			SyncEndTime {$dateType} NULL, 
			eFormsVersionNum DOUBLE NOT NULL, 
			Latitude DOUBLE NULL, 
			Longitude DOUBLE NULL, 
			Accuracy INTEGER NULL, 
			Country {$defaultType} NULL, 
			State {$defaultType} NULL, 
			City {$defaultType} NULL, 
			ZipCode {$defaultType} NULL, 
			Address {$defaultType} NULL, 
			SyncLatitude DOUBLE NULL, 
			SyncLongitude DOUBLE NULL,
			SyncAccuracy INTEGER NULL,
			AgendaID INTEGER NULL,
			TransferTime DOUBLE NULL,
			TransferDistance DOUBLE NULL,
			PRIMARY KEY (`".BITAMSurvey::$KeyField."`)
		) AUTO_INCREMENT = 1";
		$this->Repository->DataADOConnection->Execute($sql);
		
		//Reconstruye las columnas que pudieran haber sido eliminadas
		$sql = "ALTER TABLE {$strTableName} ADD UserEMail {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD StartDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD StartTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD EndDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD EndTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerStartDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerStartTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerEndDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerEndTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD StartDateEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD EndDateEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD StartTimeEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD EndTimeEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
		$sql = "ALTER TABLE {$strTableName} ADD ServerStartDateEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerEndDateEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerStartTimeEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ServerEndTimeEdit {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		$sql = "ALTER TABLE {$strTableName} ADD SyncDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncEndDate {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncEndTime {$dateType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD eFormsVersionNum DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD Latitude DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD Longitude DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD Accuracy INTEGER NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD Country {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD State {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD City {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD ZipCode {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD Address {$defaultType} NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncLatitude DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncLongitude DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD SyncAccuracy INTEGER NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR 2015-08-30: Agregado el grabado de la Agenda que realizó la captura
		$sql = "ALTER TABLE {$strTableName} ADD AgendaID INTEGER NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		//GCRUZ 2016-05-18. Agregados campos (TransferTime, TransferDistance) para el tiempo y distancia de traslado para una captura
		$sql = "ALTER TABLE {$strTableName} ADD TransferTime DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE {$strTableName} ADD TransferDistance DOUBLE NULL";
		$this->Repository->DataADOConnection->Execute($sql);
		
		//@AAL 06/07/2015 Acontinuacion se creará la tabla SVeForms_####_SectionStd empleada para secciones Estándar
		$strTableName = $this->SurveyStdTable;
		$sql = "CREATE TABLE {$strTableName} (
			SurveyKey INTEGER NOT NULL, 
			SectionKey INTEGER NOT NULL AUTO_INCREMENT, 
			PRIMARY KEY (`SectionKey`)
		)";
		$this->Repository->DataADOConnection->Execute($sql);
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	//Crea la dimensión indicada en el cubo global reutilizando la que ya se debió haber creado en la primer encuesta capturada, o bien durante el
	//proceso de migración ejecutando el script correspondiente (no debería entrar aquí nunca por primera vez, aunque tampoco habrá error
	//si así fuera, pues simplemente se detecta que ya existe e ignora la creación)
	//Originalmente usada para crear las dimensiones SyncDateDimId y SyncTimeDimID
	//Los parámetros $aNewDimName y $aNewDimField indican el nombre de la dimensión y el campo en la tabla SI_SV_GblSurveyModel donde se grabará
	//el Id de la dimensión creada respectivamente, si dicho campo no existe entonces cancela la creación, lo mismo si existe con un ID > 0, sólo
	//si no estuviera asignado es como crearía la dimensión
	//La función regresa el ID de la dimensión creada (o bien la ya existente) o -1 si ocurrió algún error
	//@JAPR 2012-09-05: Agregado el cubo global de Scores
	//El parámetro $aModelField indica el campo de la tabla de definición del que se obtendrá el ID de modelo a usar, si no se especifica
	//se asume que se tratará del modelo Global de encuestas
	//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
	/* Se modificó esta función para no terminar la ejecución en caso de error si está habilitada la variable del proceso de sincronización que forza a sólo regresar los errores sin die
	Se habilitó el parámetro $surveyInstance para que en lugar de hacer un query para extraer la información se puede reutilizar dicha instancia para preguntar por la propiedad, asumiendo
	que previamente ya había leído la metadata con la definición del modelo global de surveys
	El parámetro $sKeyValue indica si se debe o no agregar llave a la dimensión creada con el valor de la llave default (si es que se pide agregarla). Si es false no se inserta, de lo
	contrario si se generará con llave, además que en caso de si agregar un valor de NA también en esta llamada, este será el valor de la llave para el valor de NA
	El parámetro $iNAValue que indica si se debe o no intentar insertar un registro con el valor de No Aplica (default para cuando no se captura). Si es false no se inserta, de lo contrario
	se insertará con el ID recibido
	El parámetro $sPropName sólo aplica si se especificó una $surveyInstance y se usa para dinámicamente obtener el valor de la propiedad con dicho nombre que representa el ID de la
	dimensión que se va a crear ($dimensionName) en caso de que la propiedad no tenga un valor > 0
	*/
	static function createNewGblSurveyDimension($aRepository, $aNewDimName, $aNewDimField, $aModelField = null, $sKeyValue = false, $iNAValue = false, $surveyInstance = null, $sPropName = "") {
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		global $generateXLS;
		global $intCreatedBy;
		$generateXLS = false;
		
		if (is_null($aRepository) || is_null($aNewDimField) || trim($aNewDimField) == '' || is_null($aNewDimName) || trim($aNewDimName) == '') {
			return 0;
		}
		
		if (is_null($aModelField) || trim($aModelField) == '') {
			$aModelField = 'GblSurveyModelID';
		}
		
		//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
		$intClaDescrip = -1;
		$intGblSurveyModelID = -1;
		if (!is_null($surveyInstance) && property_exists($surveyInstance, $sPropName)) {
			$intGblSurveyModelID = $surveyInstance->GblSurveyModelID;
			$intClaDescrip = $surveyInstance->{$sPropName};
		}
		else {
			//@JAPR 2012-09-05: Agregado el cubo global de Scores
			//Modificado para controla en que cubo se quiere crear la dimensión
			$sql = "SELECT ".$aModelField.", {$aNewDimField} 
				FROM SI_SV_GblSurveyModel";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if (!$aRS) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return 0;
			}
			
			//Si no hay registro quiere decir que no se ha creado aún el cubo global, por tanto NO se puede proceder con este método y se debe entrar
			//directamente a checkIntegrity (si hubiera entrado aquí por la creación de la primer encuesta, inmediatamente al terminar cualquier
			//método que hubiera provocado que llegara aquí se invocaría a checkIntegrity así que no hay problema)
			if ($aRS->EOF) {
				return 0;
			}
			
			//@JAPR 2012-09-05: Agregado el cubo global de Scores
			//Modificado para controla en que cubo se quiere crear la dimensión
			$intGblSurveyModelID = (int) @$aRS->fields[strtolower($aModelField)];
			$intClaDescrip = (int) @$aRS->fields[strtolower($aNewDimField)];
		}
		
		//Si no hay modelo NO se puede proceder con este método y se debe entrar
		//directamente a checkIntegrity (si hubiera entrado aquí por la creación de la primer encuesta, inmediatamente al terminar cualquier
		//método que hubiera provocado que llegara aquí se invocaría a checkIntegrity así que no hay problema)
		if ($intGblSurveyModelID <= 0) {
			return 0;
		}
		
		//Ya existía la dimensión, así que no hay necesidad de crearla
		if ($intClaDescrip > 0) {
			return $intClaDescrip;
		}
		
		//Inicia la creación de la dimensión
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, siempre se reutiliza desde la tabla global del modelo de Surveys
		$dimensionName = $aNewDimName;
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $intGblSurveyModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = $intCreatedBy;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = (($sKeyValue !== false)?1:0);
		
		//Verificamos si hay una dimension existente con ese nombre
		//si ya existe entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$intClaDescrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
		if ($intClaDescrip != -1) {
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $intClaDescrip;
			//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError != '') {
				$gblEFormsErrorMessage = $strMMError;
				//@JAPRWarning
				//Si ocurre un error en este punto cuando si había una dimensión, no significaría que no pudo crearla necesariamente, puede ser que la dimensión ya estaba asociada al
				//modelo y este sería un error complejo de identificar por ahora, así que se dejará pasar y considerará a la dimensión agregada correctamente
				//$intClaDescrip = 0;
			}
		}
		else {
			//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
			$objMMAnswer = @$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			$strMMError = identifyModelManagerError($objMMAnswer);
			if ($strMMError == '') {
				//Insertamos el valor de No Aplica
				if ($iNAValue !== false) {
					BITAMSurvey::insertNoApplyValue($aRepository, $anInstanceModelDim, $iNAValue, true, $sKeyValue);
				}
				
				//Obtenemos el Id de la dimension creada para almacenarlo en la tabla SI_SV_GblSurveyModel
				$intClaDescrip = $anInstanceModelDim->Dimension->DimensionClaDescrip;
			}
			else {
				$gblEFormsErrorMessage = $strMMError;
				return 0;
			}
		}
		
		//Actualiza el ID de dimensión para este campo en la tabla de definición del modelo de encuestas
		//@JAPR 2012-09-05: Agregado el cubo global de Scores
		//Modificado para controla en que cubo se quiere crear la dimensión
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		if ($intClaDescrip > 0) {
			$sql = "UPDATE SI_SV_GblSurveyModel SET {$aNewDimField} = {$intClaDescrip} 
				WHERE {$aModelField} = {$intGblSurveyModelID}";		//GblSurveyModelID
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			}
		}
		
		//Termina la creación de la dimensión
		return $intClaDescrip;
	}
	//@JAPR
	
	function updateSurveyDimName() {
	    $surveyDimTableName = "RIDIM_".$this->GblSurveyDimID;
	    $fieldSurrogateKey = $surveyDimTableName."KEY";
	    $fieldKey = "KEY_".$this->GblSurveyDimID;
	    $fieldDesc = "DSC_".$this->GblSurveyDimID;
		
	    $sql = "UPDATE ".$surveyDimTableName." SET ".$fieldDesc." = ".$this->Repository->DataADOConnection->Quote($this->SurveyName)." 
			WHERE ".$fieldKey." = ".$this->SurveyID;
	    if (!$this->Repository->DataADOConnection->Execute($sql)) {
			die("(".__METHOD__.") ".translate("Error accessing")." ".$surveyDimTableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	    }
	}
	
	//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
	//Se convirtió a static porque no se utilizaba mas como instancia al eliminar el código de la creación del modelo original
	/* Inserta un valor de NA a una tabla de dimensión especificada con configuración default, excepto si se especifica alguna configuración distinta mediante los parámetros
	El parámetro $withKey representa el valor del campo Clave adicional (NO el campo Subrrogado auto-incremental de una dimensión)
	El parámetro $isSubrogateKey forza a que el parámetro $withKey se considere precisamente el campo subrrogado auto-incremental, siendo esa la única manera de insertar dicho ID con
	un valor diferente al default $gblEFormsNAKey
	El parámetro $sKeyFieldVal se utiliza sólo si $!isSubrogateKey para especificar el valor del campo $fieldKey si es que lo soporta. Si se manda Null no se insertará dicho valor
	El parámetro $sNAValue permite especificar un valor de NA diferente al default
	*/
	static function insertNoApplyValue($aRepository, $anInstanceModelDim, $withKey = null, $isSubrogateKey = false, $sKeyFieldVal = null, $sNAValue = null) {
		global $gblEFormsNA;
		global $gblEFormsNAKey;
		
		//Procedemos a almacenar el valor de No Aplica en el Catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		$fieldKey = $anInstanceModelDim->Dimension->FieldKey;
		$fieldSubrogateKey = $tableName."KEY";
		
		$strAdditionalFields = '';
		$strAdditionalValues = '';
		if (!is_null($sKeyFieldVal) && $fieldKey != $fieldDescription) {
			//Si se especificó un valor para el campo llave y si existe dicho campo en la dimensión (no es igual que el campo descripción), entonces agrega dicho campo con el valor
			//indicado, este campo por definición es Varchar también, así que aplica un Quote
			$strAdditionalFields .= ", ".$fieldKey;
			$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($sKeyFieldVal);
		}
		
		if (is_null($sNAValue)) {
			$sNAValue = $gblEFormsNA;
		}
		
		if ($isSubrogateKey) {
			if (is_null($withKey)) {
				$withKey = $gblEFormsNAKey;
			}
			
			//Si se especificó como llave subrrogada se inserta el valor directo que se especificó, o bien el default automático
			$sql = "INSERT INTO ".$tableName." (".$fieldSubrogateKey.", ".$fieldDescription.$strAdditionalFields.") 
				VALUES (".$withKey.", ".$aRepository->DataADOConnection->Quote($sNAValue).$strAdditionalValues.")";
		}
		else {
			//Si no se especificó como llave subrrogada se insertará la descripción con el primer key que se le genere automáticamente
			$sql = "INSERT INTO ".$tableName." (".$fieldDescription.$strAdditionalFields.") 
				VALUES (".$aRepository->DataADOConnection->Quote($sNAValue).$strAdditionalValues.")";
		}
		
		if (!$aRepository->DataADOConnection->Execute($sql)) {
			//die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	static function insertNewNoApplyValue($aRepository, $anInstanceModelDim, $withKey=-99999, $isSubrogateKey=false)
	{
		global $gblEFormsNA;
		//Procedemos a almacenar el valor de No Aplica en el Catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		$fieldKey = $anInstanceModelDim->Dimension->FieldKey;
		$fieldSubrogateKey = $tableName."KEY";
		
		if($isSubrogateKey==false)
		{
			if ($fieldKey!=$fieldDescription)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES (".$withKey.", ".$aRepository->DataADOConnection->Quote($gblEFormsNA).")";
			}
			else 
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$aRepository->DataADOConnection->Quote($gblEFormsNA).")";
			}
		}
		else 
		{
			$sql = "INSERT INTO ".$tableName." (".$fieldSubrogateKey.", ".$fieldDescription.") VALUES (".$withKey.", ".$aRepository->DataADOConnection->Quote($gblEFormsNA).")";
		}

		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
	/* Actualiza la información de última modificación de la encuesta, incluyendo la versión del Administrador de eForms que hizo el grabado, esto
	con el propósito de realizar conversiones de números de preguntas por IDs en las propiedades que los utilicen
	*/
	static function updateLastUserAndDateInSurvey($aRepository, $aSurveyID)
	{
		$currentDate = date("Y-m-d H:i:s");
		
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		$strAdditionalValues = "";
		//@JAPR 2014-09-25: Corregido un bug, no se debe actualizar LastVersion, porque de eso dependen las propiedades grabadas con variables de
		//preguntas, y en esta función sólo está actualizando estos campos sin afectar a los que contienen a esas variables
		//if (getMDVersion() >= esvExtendedModifInfo) {
		//	$strAdditionalValues .= ', LastVersion = '.ESURVEY_SERVICE_VERSION;
		//}
		//@JAPR
		$sql = "UPDATE SI_SV_Survey SET LastUserID = ".$_SESSION["PABITAM_UserID"].
				", LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
				", VersionNum = ".$aRepository->DataADOConnection->IfNull("VersionNum", "0")." +1".
				$strAdditionalValues.
				" WHERE SurveyID = ".$aSurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	static function getDimClaDescrip($aRepository, $dimensionName) {
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		global $intCreatedBy;
		
		$cla_descrip = -1;
		$sql = "SELECT CLA_DESCRIP 
			FROM SI_DESCRIP_ENC 
			WHERE CREATED_BY = ".$intCreatedBy." AND NOM_LOGICO = ".$aRepository->ADOConnection->Quote($dimensionName);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if (!$aRS) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			if ($gblShowErrorSurvey) {
				die($gblEFormsErrorMessage);
			}
			else {
				return $cla_descrip;
			}
		}
		
		if (!$aRS->EOF) {
			$cla_descrip = (int)$aRS->fields["cla_descrip"];
		}
		
		return $cla_descrip;
	}
	
	//El parámetro $bCreateDimension indica que si no se encontraba registrada una dimensión con esta característica en el cubo global, entonces
	//se cree en ese instante, por lo que ya no tendría que hacer la búsqueda por nombre en este método ya que el método de creación lo hará
	//internamente y se regresará el ID de la dimensión existente/creada
	//@JAPR 2012-09-05: Agregado el cubo global de Scores
	//El parámetro $aModelField indica el campo de la tabla de definición del que se obtendrá el ID de modelo a usar, si no se especifica
	//se asume que se tratará del modelo Global de encuestas
	//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
	/* Se modificó esta función para no terminar la ejecución en caso de error si está habilitada la variable del proceso de sincronización que forza a sólo regresar los errores sin die
	Se habilitó el parámetro $surveyInstance para que en lugar de hacer un query para extraer la información se puede reutilizar dicha instancia para preguntar por la propiedad, asumiendo
	que previamente ya había leído la metadata con la definición del modelo global de surveys
	El parámetro $sKeyValue indica si se debe o no agregar llave a la dimensión creada con el valor de la llave default (si es que se pide agregarla). Si es false no se inserta, de lo
	contrario si se generará con llave, además que en caso de si agregar un valor de NA también en esta llamada, este será el valor de la llave para el valor de NA
	El parámetro $iNAValue que indica si se debe o no intentar insertar un registro con el valor de No Aplica (default para cuando no se captura). Si es false no se inserta, de lo contrario
	se insertará con el ID recibido
	El parámetro $sPropName sólo aplica si se especificó una $surveyInstance y se usa para dinámicamente obtener el valor de la propiedad con dicho nombre que representa el ID de la
	dimensión que se va a crear ($dimensionName) en caso de que la propiedad no tenga un valor > 0
	*/
	static function getDimClaDescripFromGblModel($aRepository, $fieldName, $dimensionName, $bCreateDimension = false, $aModelField = null, $sKeyValue = false, $iNAValue = false, $surveyInstance = null, $sPropName = "") {
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		
		if (is_null($aModelField) || trim($aModelField) == '') {
			$aModelField = 'GblSurveyModelID';
		}
		
		$cla_descrip = -1;
		//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
		if (!is_null($surveyInstance) && property_exists($surveyInstance, $sPropName)) {
			$cla_descrip = (int) @$surveyInstance->{$sPropName};
		}
		else {
			$sql = "SELECT ".$fieldName." AS cla_descrip FROM SI_SV_GblSurveyModel";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_GblSurveyModel ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die($gblEFormsErrorMessage);
				}
				else {
					return -1;
				}
			}
			
			if (!$aRS->EOF) {
				if ($aRS->fields["cla_descrip"]!=null) {
					$cla_descrip = (int)$aRS->fields["cla_descrip"];
				}
			}
		}
		
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
		//Si no hay ya asignada una dimensión en el cubo global y se pidió crear, la intenta crear y regresa el ID de la dimensión
		if ($cla_descrip <= 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Creating new dimension?: {$bCreateDimension}");
			}
			if ($bCreateDimension) {
				//Invoca a la creación de la dimensión en el modelo global de encuestas, ya que no hay proceso que lo reconstruya al crear encuestas
				$cla_descrip = (int) @BITAMSurvey::createNewGblSurveyDimension($aRepository, $dimensionName, $fieldName, $aModelField, $sKeyValue, $iNAValue, $surveyInstance, $sPropName);
				if ($cla_descrip <= 0) {
					$cla_descrip = -1;
				}
			}
			else {
				$cla_descrip = -1;
			}
		}
		//@JAPR
		
		if ($cla_descrip == -1) {
			/*
			//Si no encontró valor en el campo de la tabla SI_SV_GblSurveyModel, entonces buscará por nombre lógico
			$cla_descrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
			*/
			//Si no se encontró el valor, ya debió intentar crear la dimensión correspondiente, así que no regresa ningún ID de alguna dimensión similar y quien invoca debe generar
			//un error de Not Found o decidir si crear algo diferente a lo que este método hubiera hecho
		}
		else {
			//Si encontro valor en el campo de la tabla SI_SV_GblSurveyModel se validará que ese cla_descrip sí exista
			//en la tabla si_descrip_enc, si existe se retornará esa clave si no entonces se buscará por nombre lógico
			$sql = "SELECT CLA_DESCRIP FROM SI_DESCRIP_ENC WHERE CLA_DESCRIP = ".$cla_descrip;
			$aRS = $aRepository->ADOConnection->Execute($sql);
			if (!$aRS) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				if ($gblShowErrorSurvey) {
					die($gblEFormsErrorMessage);
				}
				else {
					return -1;
				}
			}
			
			if ($aRS->EOF) {
				/*
				//Si no existe ese cla_descrip se buscará por nombre lógico
				$cla_descrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
				*/
				//Si no existe la dimensión entonces se debe generar un error dado que no podrá grabar los datos en ella
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("There was an error loading the surveys model, please contact Technical Support")." (DimensionID: {$cla_descrip} => {$dimensionName})";
				if ($gblShowErrorSurvey) {
					die($gblEFormsErrorMessage);
				}
				else {
					return -1;
				}
			}
		}
		
		return $cla_descrip;
	}
	
	static function insertStatusDimValues($aRepository, $anInstanceModelDim)
	{
		//Procedemos a almacenar los elementos en la tabla del catalogo Status
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $tableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//Se verifica que la tabla no tenga valores diferentes al de no aplica
		$sql = "SELECT ".$fieldDescription." FROM ".$tableName." WHERE ".$fieldKey." <> 1";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrayValues = array();
		$arrayValues[2] = "Not Started";
		$arrayValues[3] = "Not Finished";
		$arrayValues[4] = "Finished";

		//Si no existen valores entonces se procede a insertarlos
		if($aRS->EOF)
		{
			foreach ($arrayValues as $dimensionKey=>$dimensionValue)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES ( ".$dimensionKey.", ".$aRepository->DataADOConnection->Quote($dimensionValue).")";
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	static function insertEmailDimensionValues($aRepository, $anInstanceModelDim, $dimEmails) 
	{
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		foreach($dimEmails as $email)
		{
			$sql = "SELECT COUNT(".$fieldDescription.") AS nEmails FROM ".$tableName." WHERE ".$fieldDescription."=".$aRepository->DataADOConnection->Quote($email);
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if($aRS->fields["nEmails"] == 0)
			{
				$sql = "INSERT INTO ".$tableName." (".$fieldDescription.") VALUES (".$aRepository->DataADOConnection->Quote($email).")";
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	
	function insertSchedulerDimensionValue($aRepository, $anInstanceModelDim, $SchedulerID, $SchedulerName) 
	{
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $anInstanceModelDim->Dimension->FieldKey;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES (".$aRepository->DataADOConnection->Quote($SchedulerID).", ".$aRepository->DataADOConnection->Quote($SchedulerName).")";
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	function deleteSchedulerDimensionValue($aRepository, $anInstanceModelDim, $SchedulerID) 
	{
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $anInstanceModelDim->Dimension->FieldKey;
		
		$sql = "DELETE FROM ".$tableName." WHERE ".$fieldKey."=".$SchedulerID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	/* Inserta los usuarios en la dimensión usuario, excluyendo al SUPERVISOR y los que ya existieran previamente
	A partir de esta versión se optimizó el proceso para hacer una actualización basado en el ID, ya que la dimensión usuario debería ser consistente con SI_USUARIO, cualquier otro uso
	diferente sería un error del producto que así lo hiciere, ya que la dimensión existe desde las versiones originales de eForms y adicionalmente ahora se ha condicionado a que debe ser
	creada explícitamente por eForms (excepto en el caso que se tratara de una migración de versión anterior, pero incluso en ese caso, como la usaba eForms ya debería tener este
	formato así que sigue siendo válido)
	*/
	static function insertUserDimensionValues($aRepository, $anInstanceModelDim) {
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		
		//Procedemos a almacenar los elementos en la tabla del catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//@JAPR 2018-06-28: Corregido un bug, estaba reemplazando la cuenta de correo por el nombre de usuario en la dimensión usuario (#HM41DE)
		//A la fecha de implementación eForms no se utiliza en un ambiente fuera de KPIOnline, aunque así fuera, la dimensión usuario debe ser consistente con los
		//joins para los cuales se utiliza, así que se dejará fija a la cuenta de correo que es lo que sirve como identificador en KPIOnline
		//Obtenemos todos los usuarios de la tabla si_usuario pero exceptuamos el usuario supervisor
		/*if ($_SESSION["PAFBM_Mode"] == 1) {*/
		$fieldUserName = "CUENTA_CORREO";
		/*}
		else {
			$fieldUserName = "NOM_LARGO";
		}*/
		
		//@JAPR 2015-07-25: Para optimización del proceso, se asumirá que tanto el BMD como el DWH existen en el mismo Server, así que se hará un UPDATE FROM e INSERT SELECT
		//directo desde la tabla de usuarios hacia la dimensión usuario para mantener la integridad de esta dimensión que constantemente quedaba desfasada respecto a los usuarios existentes
		$sql = "INSERT INTO {$tableName} ({$fieldKey}, {$fieldDescription}) 
			SELECT CLA_USUARIO, {$fieldUserName} 
			FROM {$aRepository->DataADOConnection->databaseName}.SI_USUARIO 
			WHERE CLA_USUARIO > 0 AND CLA_USUARIO NOT IN (
				SELECT DISTINCT {$fieldKey} 
				FROM {$tableName} 
				WHERE {$fieldKey} > 0)";
		$aRepository->DataADOConnection->Execute($sql);
		
		//Actualiza aquellos valores que hubieran existido con errores de integridad
		$sql = "UPDATE {$tableName} A 
				INNER JOIN {$aRepository->DataADOConnection->databaseName}.SI_USUARIO B ON A.{$fieldKey} = B.CLA_USUARIO 
			SET A.{$fieldDescription} = B.{$fieldUserName}";
		$aRepository->DataADOConnection->Execute($sql);
		//@JAPR
	}
	
	/* Inserta las formas en la dimensión Survey, excluyendo a los que ya existieran previamente
	A partir de esta versión se optimizó el proceso para hacer una actualización basado en el ID, ya que la dimensión survey debería ser consistente con SI_SV_Survey, cualquier otro uso
	diferente sería un error del producto que así lo hiciere, ya que la dimensión existe desde las versiones originales de eForms y adicionalmente ahora se ha condicionado a que debe ser
	creada explícitamente por eForms (excepto en el caso que se tratara de una migración de versión anterior, pero incluso en ese caso, como la usaba eForms ya debería tener este
	formato así que sigue siendo válido)
	*/
	static function insertSurveysDimensionValues($aRepository, $anInstanceModelDim) {
		global $gblShowErrorSurvey;
		global $gblEFormsErrorMessage;
		
		//Procedemos a almacenar los elementos en la tabla del catalogo
		$tableName = $anInstanceModelDim->Dimension->TableName;
		$fieldSurrogateKey = $tableName."KEY";
		$fieldKey = $anInstanceModelDim->Dimension->FieldKey;
		$fieldDescription = $anInstanceModelDim->Dimension->FieldDescription;
		
		//@JAPR 2015-07-25: Para optimización del proceso, se asumirá que tanto el BMD como el DWH existen en el mismo Server, así que se hará un UPDATE FROM e INSERT SELECT
		//directo desde la tabla de usuarios hacia la dimensión usuario para mantener la integridad de esta dimensión que constantemente quedaba desfasada respecto a los usuarios existentes
		$sql = "INSERT INTO {$tableName} ({$fieldKey}, {$fieldDescription}) 
			SELECT SurveyID, SurveyName 
			FROM SI_SV_Survey 
			WHERE SurveyID > 0 AND SurveyID NOT IN (
				SELECT DISTINCT {$fieldKey} 
				FROM {$tableName} 
				WHERE NOT $fieldKey IS NULL AND {$fieldKey} > 0)";
		$aRepository->DataADOConnection->Execute($sql);
		
		//Actualiza aquellos valores que hubieran existido con errores de integridad
		$sql = "UPDATE {$tableName} A 
				INNER JOIN SI_SV_Survey B ON A.{$fieldKey} = B.SurveyID 
			SET A.{$fieldDescription} = B.SurveyName";
		$aRepository->DataADOConnection->Execute($sql);
		/*
		//Obtenemos todas las encuestas creadas
		$sql = "SELECT SurveyID, SurveyName FROM SI_SV_Survey ORDER BY SurveyID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF) {
			$surveyid = (int)$aRS->fields["surveyid"];
			$surveyname = $aRS->fields["surveyname"];
			
			$sql = "INSERT INTO ".$tableName." (".$fieldKey.", ".$fieldDescription.") VALUES (".$surveyid.", ".$aRepository->DataADOConnection->Quote($surveyname).")";
			if ($aRepository->DataADOConnection->Execute($sql) === false) {
				die("(".__METHOD__.") ".translate("Error accessing")." $tableName ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$aRS->MoveNext();
		}
		*/
	}
	
	//@JAPRDescontinuada en v6
	function createSurveyTable()
	{
		//Se crea la tabla en la cual se almacenaran las respuestas de las encuestas tal y como se
		//capturaron en la encuesta
		$tableName = $this->SurveyTable;
		
		$sql = "CREATE TABLE ".$tableName." ( DateID DATETIME, 
				HourID VARCHAR(64), 
				UserID INTEGER, 
				SyncDate VARCHAR(64), 
				SyncTime VARCHAR(64), 
				StartTime VARCHAR(64), 
				EndTime VARCHAR(64), 
				LastDateID DATETIME,
				LastHourID VARCHAR(64), 
				LastStartTime VARCHAR(64), 
				LastEndTime VARCHAR(64), 
				FactKey BIGINT, 
				FactKeyDimVal BIGINT, 
				FolioID BIGINT";
		/*@MABH20121205*/
		$strAdditionalFields = "";
		if (getMDVersion() >= esvServerDateTime) {
			$strAdditionalFields .= ", ServerStartDate VARCHAR(64), ServerEndDate VARCHAR(64), ServerStartTime VARCHAR(64), ServerEndTime VARCHAR(64)";
		}
		//@MABH20121205
		//@JAPR 2013-01-12: Faltaba agregar las nuevas columnas de registros únicos
		if (getMDVersion() >= esveFormsV41Func) {
			$strAdditionalFields .= ", EntrySectionID INTEGER NULL, EntryCatDimID INTEGER NULL";
		}
		//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$strAdditionalFields .= ", eFormsVersionNum VARCHAR(10) NULL, DynamicPageDSC VARCHAR(255) NULL";
		}
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
			$strAdditionalFields .= ", DynamicOptionDSC VARCHAR(255) NULL, DynamicValue LONGTEXT NULL";
		}
		//@JAPR 2014-02-17: Agregado el Primary Key y el índice principal para optimizar el grabado en la tabla paralela
		$sql .= $strAdditionalFields.", 
				PRIMARY KEY (`FactKey`), 
				KEY `FactKeyDimValFactKey` (`FactKeyDimVal`,`FactKey`) 
			)";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	function remove($bJustMobileTables = false)
	{
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		$gblModMngrErrorMessage = '';
		
		//@JAPR 2015-07-10: Agregado el borrado en cadena para permitir eliminar todas las tablas del nuevo diseño adecuadamente
		$sectionCollection = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
		foreach ($sectionCollection->Collection as $anInstanceToRemove) {
			$anInstanceToRemove->remove();
		}
		
		//@JAPR 2012-03-13: Agregadas las Agendas
		$sql = "DELETE FROM SI_SV_SurveyAgendaDet WHERE SurveyID = ".$this->SurveyID;
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		//Se eliminan las referencias de imagenes por pregunta
		//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$sql = "DELETE FROM SI_SV_SurveyAnswerImageSrc WHERE SurveyID = " . $this->SurveyID;
		//@JAPR 2012-06-06: Validado que si no falla el DELETE, no genere un error durante el borrado
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImageSrc ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		$sql = "DELETE FROM SI_SV_SurveyAnswerImage WHERE SurveyID = " . $this->SurveyID;
		//@JAPR 2012-06-06: Validado que si no falla el DELETE, no genere un error durante el borrado
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan los comentarios por pregunta
		$sql = "DELETE FROM SI_SV_SurveyAnswerComment WHERE SurveyID = " . $this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerComment ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan los documentos por pregunta
		$sql = "DELETE FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = " . $this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan las acciones por pregunta
		$sql = "DELETE FROM SI_SV_SurveyQuestionActions WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan las acciones por opcion de pregunta
		$sql = "DELETE FROM SVSurveyActions WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		//Se eliminan los mapeos a eBavel de las opciones de las pregunta de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_SurveyAnswerActionFieldsMap WHERE QuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2015-12-08: Agregado el borrado de la tabla de Destinos de datos
		$sql = "DELETE FROM SI_SV_DataDestinationsMapping 
			WHERE id_datadestinations IN (
				SELECT id 
				FROM SI_SV_DataDestinations 
				WHERE id_survey = {$this->SurveyID})";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_DataDestinations WHERE id_survey = ".$this->SurveyID;
		$this->Repository->DataADOConnection->Execute($sql);
		
		//@JAPR 2015-12-08: Agregado el borrado de la tabla de Mapeos de modelos
		$sql = "DELETE FROM SI_SV_ModelArtus_Mapping 
			WHERE id_modelartus IN (
				SELECT id_modelartus 
				FROM SI_SV_ModelArtus 
				WHERE id_survey = {$this->SurveyID})";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_ModelArtus_Questions 
			WHERE id_modelartus IN (
				SELECT id_modelartus 
				FROM SI_SV_ModelArtus 
				WHERE id_survey = {$this->SurveyID})";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_ModelArtus WHERE id_survey = ".$this->SurveyID;
		$this->Repository->DataADOConnection->Execute($sql);
		//@JAPR
		
		//Se eliminan las opciones de las preguntas de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_QAnswers WHERE QuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan las opciones de columna de las preguntas de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_QAnswersCol WHERE QuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswersCol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan las acciones de las opciones de las preguntas de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_QOptionActions WHERE QuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QOptionActions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Se eliminan los showquestions de las opciones de las preguntas de la encuesta si es q tienen
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE QuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2013-02-14: Corregido un bug, no estaba eliminando la referencia del ShowQuestions cuando esta es la pregunta a mostrar (#27519)
		//Este DELETE no es realmente necesario a menos que hubiera basura en la encuesta y se hubieran grabado ShowQuestions con question IDs
		//ya no existentes al momento del DELETE anterior
		$sql = "DELETE FROM SI_SV_ShowQuestions WHERE ShowQuestionID IN ( SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." )";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ShowQuestions ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2013-01-29: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$arrCatalogIDs = array();
		if ($this->DissociateCatDimens) {
			//Si se usan catálogos desasociados entonces se deben remover las dimensiones de preguntas de catálogo manualmente, ya que estas
			//dimensiones son globales y en cuanto se remueva el modelo se eliminarán también de forma global si ya no son usadas por algún
			//otro modelo, por tanto se usará un método especial que sólo las desasocia del modelo actual sin tocar a la definición global
			$objQuestionCollection = BITAMQuestionCollection::NewInstanceIncludeTheseTypes($this->Repository, $this->SurveyID, array(qtpSingle));
			if (!is_null($objQuestionCollection)) {
				foreach ($objQuestionCollection as $aQuestion) {
					//Toda pregunta de catálogo dentro de una encuesta que está configurada con catálogos desasociados, por ende tendrá 
					//dimensiones independientes asociadas y eso significa que para dicho catálogo se deben remover todas las dimensiones
					//que existan asociadas a la encuesta y que le pertenezcan, pero primero se remueven las propias preguntas
					if ($aQuestion->UseCatalog && $aQuestion->CatalogID > 0) {
						$arrCatalogIDs[$aQuestion->CatalogID] = $aQuestion->CatalogID;
						$aQuestion->remove();
					}
				}
			}
		}
		
		//@JAPR 2013-07-10: Agregado el mapeo de una forma de eBavel para acciones
		$sql = "DELETE FROM SI_SV_SurveyQuestionActionFieldsMap WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2012-06-06: Validado que si no falla el DELETE, no genere un error durante el borrado
		$sql = "DELETE FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveyFilter WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_Section WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//@JAPR 2012-06-06: Validado que si no falla el DELETE, no genere un error durante el borrado
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveyScheduler WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveySchedulerUser WHERE SchedulerID IN (SELECT SchedulerID FROM SI_SV_SurveyScheduler WHERE SurveyID = ".$this->SurveyID.")";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerUser ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveySchedulerRol WHERE SchedulerID IN (SELECT SchedulerID FROM SI_SV_SurveyScheduler WHERE SurveyID = ".$this->SurveyID.")";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveySchedulerRol ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@AAL 27/05/2015 Se valida por que en versiones de EformsV6 ya no se usan los Modelos
		if ($this->CreationAdminVersion < esveFormsv6) {
			//Se elimina la tabla parelela a la tabla de hechos
			$sql = "DROP TABLE ".$this->SurveyTable;
			//@JAPR 2012-06-06: Validado que si no existe la tabla, no genere un error durante el borrado
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$this->SurveyTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			//se elimina la tabla de datos de las preguntas tipo Matrix
			$sql = "DROP TABLE SVSurveyMatrixData_".$this->ModelID;
			//@JAPR 2012-06-06: Validado que si no existe la tabla, no genere un error durante el borrado
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyMatrixData_".$this->ModelID." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//se elimina la tabla de datos de las preguntas tipo Category Dimension
			$sql = "DROP TABLE SVSurveyCatDimVal_".$this->ModelID;
			//@JAPR 2012-06-06: Validado que si no existe la tabla, no genere un error durante el borrado
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SVSurveyCatDimVal_".$this->ModelID." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
			//para asignarlos a las variables de session que se ocupa en el model manager
			//@JAPR 2015-02-09: Agregado soporte para php 5.6
			//session_register("BITAM_UserID");
			//session_register("BITAM_UserName");
			//session_register("BITAM_RepositoryName");
			$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
			$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
			$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
			
			//Procedemos a actualizar el modelo
			require_once("../model_manager/filescreator.inc.php");
			require_once("../model_manager/model.inc.php");
			require_once("../model_manager/modeldata.inc.php");
			
			//@JAPR 2012-06-06: Validado que si no existe el elemento, no genere un error durante el borrado
			$anInstanceModel = BITAMModel::NewModelWithModelID($this->Repository, $this->ModelID);
			if (!is_null($anInstanceModel))
			{
				//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
				$objMMAnswer = @$anInstanceModel->remove();
				$strMMError = identifyModelManagerError($objMMAnswer);
				if ($strMMError != '') {
					$gblModMngrErrorMessage .= "\r\n(".__METHOD__.") ".translate("Error deleting survey model").": ".$strMMError.". ";
					die($gblModMngrErrorMessage);
				}
			}
			//@JAPR

			//Corroborar si tambien se debe eliminar la informacion desde el cubo global de encuestas
			$this->removeGblSurveyModelInfoBySurvey();
		}
		else {
			//@AAL En versiones de EformsV6 y siguientes el Borrado de las tablas paralelas correspondientes  
			//a las Formas o encuestas cambia, puesto que la estructura de las tablas es diferente.
			if ($this->SurveyStdTable) {
				$sql = "DROP TABLE {$this->SurveyStdTable}";
				$this->Repository->DataADOConnection->Execute($sql);
			}
			$sql = "DROP TABLE {$this->SurveyTable}";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		
		//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
		//Para concluir hace el borrado lógico de los logs para que no se vean en la bitácora
		if (getMDVersion() >= esvDeleteReports) {
			$sql = "UPDATE SI_SV_SurveyLog SET Deleted = 1 
				WHERE SurveyID = {$this->SurveyID}";
			$this->Repository->DataADOConnection->Execute($sql);
		}
		
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			if ($this->SourceID) {
				switch ($this->FormType) {
					case formProdWithDev:
						//Desasocia la forma de desarrollo si esta era la de producción para que la forma de desarrollo quede como independiente, esto mismo ocurre para todos los
						//elementos de la forma de desarrollo
						$sql = "UPDATE SI_SV_Survey SET SourceID = 0, FormType = ".formProduction." 
							WHERE SurveyID = {$this->SourceID}";
						$this->Repository->DataADOConnection->Execute($sql);
						
						//Ahora remueve la liga de todos los apuntadores en los objetos de la forma de desarrollo
						$sql = "UPDATE SI_SV_Section SET SourceID = 0 
							WHERE SurveyID = {$this->SourceID}";
						$this->Repository->DataADOConnection->Execute($sql);
						
						$sql = "UPDATE SI_SV_Question SET SourceID = 0 
							WHERE SurveyID = {$this->SourceID}";
						$this->Repository->DataADOConnection->Execute($sql);
						
						$sql = "UPDATE SI_SV_QAnswers SET SourceID = 0 
							WHERE QuestionID IN (
								SELECT QuestionID 
								FROM SI_SV_Question 
								WHERE SurveyID = {$this->SourceID}
							)";
						$this->Repository->DataADOConnection->Execute($sql);
						break;
					case formDevelopment:
						//Desasocia la forma de desarrollo de la de producción si esta era de desarrollo, para que se libere la forma de producción correspondiente
						$sql = "UPDATE SI_SV_Survey SET SourceID = 0, FormType = ".formProduction." 
							WHERE SurveyID = {$this->SourceID}";
						$this->Repository->DataADOConnection->Execute($sql);
						break;
				}
			}
		}
		//@JAPR
		
		//@JAPR 2012-06-06: Movido el borrado de la encuesta al último paso para asegurar que cualquier error en pasos previos por lo menos
		//permitirá volver a intentar el borrado de la encuesta, aunque para este momento ya todos los pasos previos deberían estar
		//validados para no detener la ejecución
		$sql = "DELETE FROM SI_SV_Survey WHERE SurveyID = ".$this->SurveyID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		//@JAPR
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		if ($gblModMngrErrorMessage != '') {
			die($gblModMngrErrorMessage);
		}
		//@JAPR
		
		return $this;
	}
	
	function removeGblSurveyModelInfoBySurvey()
	{
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		
		//Obtenemos el modelo del cubo global de encuestas
		//@JAPR 2013-03-22: Agregado el control de errores del Model Manager
		$anInstanceModel = @BITAMModel::NewModelWithModelID($this->Repository, $this->GblSurveyModelID);

		//Obtenemos el SurveyKey
		$surveyKey = -1;
		$aSurveyInstanceDim = @BITAMModelDimension::NewModelDimensionWithDimensionID($this->Repository, $this->GblSurveyModelID, -1, $this->GblSurveyDimID);
		if (!is_null($aSurveyInstanceDim)) {
			$surveyDimField = $aSurveyInstanceDim->Dimension->TableName."KEY";
			$surveyKey = $this->getSurveyDimKey($aSurveyInstanceDim);
		}
		
		if($surveyKey!=-1 && !is_null($anInstanceModel) && !is_null($aSurveyInstanceDim))
		{
			//Remover la información de la tabla de hechos que haga referencia a ese SurveyKey
			$factTable = $anInstanceModel->nom_tabla;
			
			$sql = "DELETE FROM ".$factTable." WHERE ".$surveyDimField." = ".$surveyKey;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Removemos la informacion de Survey Global ID para dicha encuesta
			$tableDim = "RIDIM_".$this->GblSurveyGlobalDimID;
			$fieldDesc = "DSC_".$this->GblSurveyGlobalDimID;
			$strSearch = $surveyKey."-%";
			
			$sql = "DELETE FROM ".$tableDim." WHERE ".$fieldDesc." LIKE ".$this->Repository->DataADOConnection->Quote($strSearch);
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Removemos la informacion de Survey Single Record ID para dicha encuesta
			$tableDim = "RIDIM_".$this->GblSurveySingleRecordDimID;
			$fieldDesc = "DSC_".$this->GblSurveySingleRecordDimID;
			$strSearch = $surveyKey."-%";
			
			$sql = "DELETE FROM ".$tableDim." WHERE ".$fieldDesc." LIKE ".$this->Repository->DataADOConnection->Quote($strSearch);
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDim." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//Remover ese SurveyKey desde el catalogo de Surveys
			$tableName = $aSurveyInstanceDim->Dimension->TableName;
			$fieldSurrogateKey = $tableName."KEY";
			
			$sql = "DELETE FROM ".$tableName." WHERE ".$fieldSurrogateKey." = ".$surveyKey;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	function getSurveyDimKey($aSurveyInstanceDim)
	{
		$valueKey = -1;
		$tableName = $aSurveyInstanceDim->Dimension->TableName;
		$fieldSurrogateKey = $tableName."KEY";
		$fieldKey = $aSurveyInstanceDim->Dimension->FieldKey;
		
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$this->SurveyID;
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Considerando que es un solo valor el que se esta procesando
		if (!$aRS->EOF)
		{	
			$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
		}
		
		return $valueKey;
	}

	function isNewObject()
	{
		return ($this->SurveyID < 0 || $this->ForceNew);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Survey");
		}
		else
		{
			return $this->SurveyName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Survey";
		}
		else
		{
			return "BITAM_PAGE=Survey&SurveyID=".$this->SurveyID;
		}
	}

	function get_Parent()
	{
		return BITAMSurveyCollection::NewInstance($this->Repository, null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SurveyID';
	}

	function get_FormFieldName() {
		return 'SurveyName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$isNewObj = $this->isNewObject();
		$myFields = array();

		$isReadOnly = false;
		if(!$this->isNewObject())
		{
			$isReadOnly = true;
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyName";
		$aField->Title = translate("Survey Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$statusOptions = array();
			
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Status";
		$aField->Title = translate("Status");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";

		if(!$isNewObj)
		{
			//Si su estado es activo, solamente se permitirá modificar su estado
			//a desarrollo si no hay registros capturados
			if($this->Status==svstActive)
			{
				$isInUse = BITAMSurvey::isInUse($this->Repository, $this->SurveyID);
				
				if($isInUse==true)
				{
					$statusOptions[svstActive] = translate("Active");
					$statusOptions[svstInactive] = translate("Inactive");
				}
				else 
				{
					$statusOptions[svstInDevelopment] = translate("In Development");
					$statusOptions[svstActive] = translate("Active");
					$statusOptions[svstInactive] = translate("Inactive");
				}
			}
			else if ($this->Status==svstInactive)
			{
				$statusOptions[svstInactive] = translate("Inactive");
				$aField->IsDisplayOnly = true;
			}
			else 
			{
				$statusOptions[svstInDevelopment] = translate("In Development");
				$statusOptions[svstActive] = translate("Active");
				$statusOptions[svstInactive] = translate("Inactive");
			}
		}
		else 
		{
			$statusOptions[svstInDevelopment] = translate("In Development");
			$statusOptions[svstActive] = translate("Active");
			$statusOptions[svstInactive] = translate("Inactive");
		}

		$aField->Options = $statusOptions;
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$aField->InTitle = true;
		$aField->IsVisible = false;
		//@JAPR
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ClosingMsg";
		$aField->Title = translate("Closing Message");
		$aField->Type = "String";
		$aField->Size = 255;
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$aField->InTitle = true;
		$aField->IsVisible = false;
		//@JAPR
		$myFields[$aField->Name] = $aField;
		/* Conchita 9 de nov ocultar Answer Guide
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "HelpMsg";
		$aField->Title = translate("Answer Guide");
		$aField->Type = "LargeString";
		$aField->Size = 500;
		$myFields[$aField->Name] = $aField;
		*/
		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "CatalogID";
		$catalogField->Title = translate("Catalog");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
		$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository, true);
		//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
		$catalogField->InTitle = true;
		$catalogField->IsVisible = false;
		//@JAPR
		$myFields[$catalogField->Name] = $catalogField;
		
		$blnHasActionQuestions = false;
		if (!$isNewObj)
		{
			$sql = "SELECT COUNT(*) AS NumQuestions FROM SI_SV_Question WHERE SurveyID = ".$this->SurveyID." AND QTypeID = ".qtpAction;
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if ($aRS)
			{
				if (!$aRS->EOF)
				{
					if ((int) $aRS->fields['numquestions'] > 0)
					{
						$blnHasActionQuestions = true;
					}
				}
			}
		}
		
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		//Ahora se habilitarán los campos de mapeo con eBavel no solo si hay acciones sino en caso de que sea la versión que ya soporta grabar
		//en las formas de eBavel directamente
		$blnForceShowEBavel = false;
		if (getMDVersion() >= esveBavelMapping) {
			$blnForceShowEBavel = true;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "ElementQuestionID";
		$aField->Title = translate("Action element");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		if ($blnHasActionQuestions)
		{
			$aField->Options = BITAMQuestion::GetQuestionsForActionElement($this->Repository, $this->SurveyID, true);
		}
		else 
		{
			$aField->Options = array(0 => translate('None'));
		}
		if ($isNewObj || !$blnHasActionQuestions)
		{
			$aField->InTitle = true;
			$aField->IsVisible = false;
		}
		$myFields[$aField->Name] = $aField;
		
		//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
		$arreBavelApps = array(0 => '('.translate('None').')');
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if ($blnHasActionQuestions || $blnForceShowEBavel)
		{
			require_once('eBavelIntegration.inc.php');
			$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true);
		}
		
		$aFieldApps = BITAMFormField::NewFormField();
		$aFieldApps->Name = "eBavelAppID";
		$aFieldApps->Title = translate("eBavel application");
		$aFieldApps->Type = "Object";
		$aFieldApps->VisualComponent = "Combobox";
		$aFieldApps->Options = $arreBavelApps;
		$aFieldApps->OnChange = "cleareBavelFields();cleareBavelActionFields();";
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (($isNewObj || !$blnHasActionQuestions) && !$blnForceShowEBavel)
		{
			$aFieldApps->InTitle = true;
			$aFieldApps->IsVisible = false;
		}
		$myFields[$aFieldApps->Name] = $aFieldApps;

		//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "eBavelAppIDOld";
		$aField->Title = translate("eBavel application");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $arreBavelApps;
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		*/
		//@JAPR
		
		//Obtiene la lista de formas de eBavel creadas en el repositorio
		$arreBavelForms = array('' => array('' => '('.translate('None').')'));
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if ($blnHasActionQuestions || $blnForceShowEBavel)
		{
			require_once('eBavelIntegration.inc.php');
			$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId');
		}
		
		$aFieldForms = BITAMFormField::NewFormField();
		$aFieldForms->Name = "eBavelFormID";
		$aFieldForms->Title = translate("eBavel data form");
		$aFieldForms->Type = "Object";
		$aFieldForms->VisualComponent = "Combobox";
		$aFieldForms->Options = $arreBavelForms;
		$aFieldForms->OnChange = "cleareBavelFields();";
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (($isNewObj || !$blnHasActionQuestions) && !$blnForceShowEBavel)
		{
			$aFieldForms->InTitle = true;
			$aFieldForms->IsVisible = false;
		}
		//@JAPR 2013-04-29: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		//@JAPR 2013-05-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		//2014.11.21 JCEM #I4LL44 Se modifica style del boton de mapeo
		if (getMDVersion() >= esveBavelMapping) {
			//$aFieldForms->AfterMessage = "<span id=\"mapeBavelFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);background-repeat:no-repeat;background-position:100% 100%;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelFields();\">".translate("Map fields")."</span>";
			$aFieldForms->AfterMessage = "<span id=\"mapeBavelFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;
				font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" 
				onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelFields();\">".
				"<span style=\"background-image: url('btngenerar.gif');background-size: 99% 99%;background-repeat: no-repeat;padding: 3px 16px 5px 13px;\">".translate("Map fields")."</span>";
		}
		//@JAPR
		$myFields[$aFieldForms->Name] = $aFieldForms;
		$aFieldForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldForms;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "eBavelFieldsData";
		$aField->Title = "eBavel fields mapping";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		$aFieldActionForms = BITAMFormField::NewFormField();
		$aFieldActionForms->Name = "eBavelActionFormID";
		$aFieldActionForms->Title = translate("eBavel action data form");
		$aFieldActionForms->Type = "Object";
		$aFieldActionForms->VisualComponent = "Combobox";
		$aFieldActionForms->Options = $arreBavelForms;
		$aFieldActionForms->OnChange = "cleareBavelActionFields();";
		if (getMDVersion() >= esveBavelSuppActions) {
			//$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);background-repeat:no-repeat;background-position:100% 100%;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".translate("Map action fields")."</span>";
			$aFieldActionForms->AfterMessage = "<span id=\"mapeBavelActionFields\" unselectable=\"on\" style=\"padding:3px;text-align:center;width:97px;height:22px;cursor:pointer;
				font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" 
				onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openAssigneBavelActionFields();\">".
				"<img src=\"btngenerar.gif\" style=\"position:relative; left:0px; top:2px; width:150px; height:16px;\"/ displayMe=1>".
				"<span style=\"position:relative; left:-140px; top:-3px\">".
				translate("Map action fields")."</span></span>";
		}
		else {
			$aFieldActionForms->InTitle = true;
			$aFieldActionForms->IsVisible = false;
		}
		//@JAPR 2013-07-11: Validado temporalmente que no se habiliten estas opciones
		$aFieldActionForms->AfterMessage = '';
		$aFieldActionForms->InTitle = true;
		$aFieldActionForms->IsVisible = false;
		//@JAPR
		$myFields[$aFieldActionForms->Name] = $aFieldActionForms;
		$aFieldActionForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldActionForms;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "eBavelActionFieldsData";
		$aField->Title = "eBavel action fields mapping";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$signtureOptions = array();
		$signtureOptions[0] = translate("No");
		$signtureOptions[1] = translate("Yes");
			
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "HasSignature";
		$aField->Title = translate("Has signature?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $signtureOptions;
		$myFields[$aField->Name] = $aField;

    // @LROUX 2011-10-18: Se agrega AllowPartialSave					
		$partialSaveOptions = array();
		$partialSaveOptions[0] = translate("No");
		$partialSaveOptions[1] = translate("Yes");
			
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AllowPartialSave";
		$aField->Title = translate("Allow partial Save?");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $partialSaveOptions;
		$myFields[$aField->Name] = $aField;

		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
		if (getMDVersion() >= esvExtendedActionsData) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "SyncWhenSaving";
			$aField->Title = translate("Synchronize when saving?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $partialSaveOptions;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		if (getMDVersion() >= esvHideNavigationButtons) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "HideNavButtons";
			$aField->Title = translate("Hide navigation buttons?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $partialSaveOptions;
			$myFields[$aField->Name] = $aField;
			
			//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "HideSectionNames";
			$aField->Title = translate("Hide section names?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $partialSaveOptions;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DisableEntry";
			$aField->Title = translate("Disable entry?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $partialSaveOptions;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "EnableReschedule";
		$aField->Title = "Enable agenda reschedule";
		$aField->Type = "Boolean";
		$aField->VisualComponent = "Checkbox";
		//$aField->AfterMessage = translate("Enable agenda reschedule");
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$aField->InTitle = true;
		$aField->IsVisible = false;
		//@JAPR
		$aField->OnChange = 'showHideChkValidation()';
		$myFields[] = $aField;
		
		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "RescheduleCatalogID";
		$catalogField->Title = translate("Select catalog for reschedule status");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
		$catalogField->OnChange="updateFields();updateMemberValues()";
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$catalogField->InTitle = true;
		$catalogField->IsVisible = false;
		//@JAPR
		$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository, true);
		$myFields[$catalogField->Name] = $catalogField;
		
	    require_once('surveyAgenda.inc.php');
		$catmemberField = BITAMFormField::NewFormField();
		$catmemberField->Name = "RescheduleCatMemberID";
		$catmemberField->Title = translate("Select catalog attribute for reschedule status");
		$catmemberField->Type = "Object";
		$catmemberField->VisualComponent = "Combobox";
		$catmemberField->Options = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository);
		$catmemberField->OnChange = "updateMemberValues()";
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$catmemberField->InTitle = true;
		$catmemberField->IsVisible = false;
		//@JAPR
		$myFields[$catmemberField->Name] = $catmemberField;

		$catmemberField->Parent = $catalogField;
		$catalogField->Children[] = $catmemberField;
        
		$defaultFilter = array();
		$defaultFilter[$this->RescheduleCatMemberID] = array('' => '');
		if ($this->RescheduleStatus != '' && $this->RescheduleCatMemberID > 0)
		{
			$defaultFilter[$this->RescheduleCatMemberID] = BITAMSurveyAgenda::getCatMemberValuesByAttributeID($this->Repository, $this->RescheduleCatMemberID);
		}
		
		$catValuesField = BITAMFormField::NewFormField();
		$catValuesField->Name = "RescheduleStatus";
		$catValuesField->Title = translate("Reschedule status");
		$catValuesField->Type = "Object";
		$catValuesField->VisualComponent = "Combobox";
		$catValuesField->Options = $defaultFilter;
		//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
		$catValuesField->InTitle = true;
		$catValuesField->IsVisible = false;
		//@JAPR
		$myFields[$catValuesField->Name] = $catValuesField;
		
		$catValuesField->Parent = $catmemberField;
		$catmemberField->Children[] = $catValuesField;
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UseStdSectionSingleRec";
			$aField->Title = translate("Store data from standard section questions in a single record");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $partialSaveOptions;
			//@JAPR 2013-03-11: Modificado para que esta opción solo aplique en encuestas nuevas
			$aField->IsDisplayOnly = true;				//Esta opción NO es configurable
			//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
			$aField->InTitle = true;
			$aField->IsVisible = false;
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$arrDissociateDimsOptions = array();
			$arrDissociateDimsOptions[0] = translate("No");
			$arrDissociateDimsOptions[1] = translate("Yes");
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DissociateCatDimens";
			$aField->Title = translate("Use catalog dissociation?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrDissociateDimsOptions;
			$aField->IsDisplayOnly = true;				//Esta opción NO es configurable
			//@JAPR 2014-02-06: Removidas algunas configuraciones por solicitud de DAbdo
			$aField->InTitle = true;
			$aField->IsVisible = false;
			//@JAPR
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "OtherLabel";
			$aField->Title = translate("Label for other");
			$aField->Type = "String";
			$aField->Size = 50;
			$myFields[$aField->Name] = $aField;
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "CommentLabel";
			$aField->Title = translate("Label for comment");
			$aField->Type = "String";
			$aField->Size = 50;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		//@JAPR 2014-09-18: Activada esta funcionalidad para todas las versiones posteriores
		if (getMDVersion() >= esvReportEMailsByCatalog) {
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
			$this->AdditionalEMails = $this->TranslateVariableQuestionIDsByNumbers($this->AdditionalEMails, true);
			//@JAPR
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "AdditionalEMails";
			$aField->Title = translate("Additional emails  for capture report delivery");
			$aField->Type = "String";
			$aField->Size = 255;
			/*@AAL 25/02/2015: Agregado Button para abrir la ventana del editor de fórmulas*/
			$aField->AfterMessage = '<input type="button" title="Show formula editor" value="Formula editor" style="width: 100px; border: 0px; cursor: pointer; color: rgb(213, 213, 188); background-image: url(http://kpionline8.bitam.com/artus/gen8/ESurveyV5/btnGenerar.gif); background-size: 100% 100%; background-repeat: no-repeat;" onmouseover="this.style.color=\'#98fb98\'" onmouseout="this.style.color=\'#d5d5bc\'" onclick="this.style.color=\'#d5d5bc\';ShowWindowsDialogEditor(\'AdditionalEMails\');">';
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$IsAgendableOptions = array();
			$IsAgendableOptions[0] = translate("No");
			$IsAgendableOptions[1] = translate("Yes");
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "IsAgendable";
			$aField->Title = translate("Is agendable?");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $IsAgendableOptions;
			$myFields[$aField->Name] = $aField;

			$aField = BITAMFormField::NewFormField();
			$aField->Name = "RadioForAgenda";
			$aField->Title = translate("Agenda radio");
			$aField->Type = "Integer";
			$myFields[$aField->Name] = $aField;
		}

		return $myFields;
	}

	function generateBeforeFormCode($aUser)
 	{
 		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
 		function verifyFieldsAndSave(target)
 		{
			var strAlert = '';
			
 			if(Trim(<?=$myFormName?>_SaveForm.SurveyName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Survey Name"))?>';
 			}

 			if(Trim(<?=$myFormName?>_SaveForm.SurveyName.value)=='Surveys')
 			{	
 				strAlert+= '\n'+'<?=translate("\"Surveys\" is a reserved word. Please use another survey name.")?>';
 			}
 			
 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				<?=$myFormName?>_Ok(target);
 			}
 		}
 		
 		function forceChangeStatus()
		{
			frmForceStatusChange.submit();
		}
		
 		function deleteDataCaptured()
		{
			var answer = confirm('Do you want to remove data captured in this survey?');

			if (answer)
			{
				frmDeleteDataCaptured.submit();
			}
		}
		
 		function showHideChkValidation()
 		{
	 		var objEnableReschedule = <?=$myFormName?>_SaveForm.EnableReschedule;
	 		if (objEnableReschedule != null && objEnableReschedule != undefined)
	 		{
		 		var rowRescheduleCatalogID = document.getElementById("Row_RescheduleCatalogID");
		 		if (rowRescheduleCatalogID != undefined) {
		 			rowRescheduleCatalogID.style.display = (objEnableReschedule[1].checked)?((bIsIE)?"inline":"table-row"):"none";
		 		}
		 		var rowRescheduleCatMemberID = document.getElementById("Row_RescheduleCatMemberID");
		 		if (rowRescheduleCatMemberID != undefined) {
			 		rowRescheduleCatMemberID.style.display = (objEnableReschedule[1].checked)?((bIsIE)?"inline":"table-row"):"none";
		 		}
		 		var rowRescheduleStatus = document.getElementById("Row_RescheduleStatus");
		 		if (rowRescheduleStatus != undefined) {
		 			rowRescheduleStatus.style.display = (objEnableReschedule[1].checked)?((bIsIE)?"inline":"table-row"):"none";
		 		}
	 		}
 		}
		
 		function updateFields()
 		{
 			if (cancel==true || true)
 			{
 				<?=$myFormName?>_RescheduleCatMemberID_Update();
 				
 				var catMember = <?=$myFormName?>_SaveForm.RescheduleCatMemberID;
 				
 				if (initialSelectionCatMember!=null)
 				{
 					for(i=0;i<catMember.options.length;i++)
 					{
 						if (catMember.options[i].value==initialSelectionCatMember)
 						{	
 							catMember.selectedIndex=i;
 							break;
 						}
 					}
 				}
 			}
 			cancel=false;
 		}
 		
 		function updateMemberValues()
 		{
 			if (cancel==true || true)
 			{
 				var intUserID = 0;
 				var cmbUserID = <?=$myFormName?>_SaveForm.UserID;
 				if (cmbUserID != null && cmbUserID.selectedIndex >= 0)
 				{
 					intUserID = cmbUserID.options[cmbUserID.selectedIndex].value;
 				}
 				//Verifica si ya se habían cargado los valores de este atributo, si no es así hace una petición Ajax para obtenerlos
				var myParent = <?=$myFormName?>_SaveForm.RescheduleCatMemberID;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
	        		var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
	        	if (myParentSelection != null)
	        	{
	        		var myNewValues = <?=$myFormName?>_RescheduleStatus[myParentSelection];
	        	}
	        	if (myNewValues == null)
	        	{
	        		//No se habían cargado los valores de este atributo
   	        		var myNewValues = new Array();
   	        		
					var xmlObj = getXMLObject();
					
					if (xmlObj == null)
					{
						alert("XML Object not supported");
					}
					else
					{
						var objCmbCatalog = <?=$myFormName?>_SaveForm.CatalogID;
						var intCatalogID =objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
						
					 	xmlObj.open("POST", "getDistinctAttribValues.php?CatalogID="+intCatalogID+"&CatMemberID="+myParentSelection+"&userID="+intUserID, false);
						
					 	xmlObj.send(null);
					 	
					 	strResult = Trim(unescape(xmlObj.responseText));
					 	if (strResult.indexOf('<?=SV_SRV_ERROR?>') > 0)
					 	{
					 		var errorData = strResult.split('<?=SV_SRV_ERROR?>');
					 		if (errorData.length > 1)
					 		{
								alert("Error retrieving attribute's values: (" + errorData[0] + ') ' + errorData[1]);
					 		}
					 		else
					 		{
								alert("Error retrieving attribute's values: " + strResult);
					 		}
				 			//cancel=false;
							return(new Array);
					 	}
					 	else
					 	{
					 		var attribValues = strResult.split("_SV_VAL_");
					 		var numVal = attribValues.length;
					 		for (i = 0; i < numVal; i++)
					 		{
					 			arrValData = attribValues[i].split("<?=SV_DELIMITER?>");
					 			if (arrValData.length > 1)
					 			{
					 				myNewValues[i] = new Array(arrValData[0], arrValData[1]);
					 			}
					 			else
					 			{
					 				myNewValues[i] = new Array(i, arrValData[0]);
					 			}
					 		}
							//myNewValues = attribValues;
		   	        		<?=$myFormName?>_RescheduleStatus[myParentSelection] = myNewValues;
					 	}
					}
	        	}
	        	else
	        	{
	        		//Reutiliza los valores previamente cargados
   	        		var myNewValues = <?=$myFormName?>_RescheduleStatus[myParentSelection];
		        }
				
 				<?=$myFormName?>_RescheduleStatus_Update();
 				
 				var rescheduleStatus = <?=$myFormName?>_SaveForm.RescheduleStatus;
 				
 				if (initialSelectionCatMember!=null)
 				{
 					for(i=0;i<rescheduleStatus.options.length;i++)
 					{
 						if (rescheduleStatus.options[i].value==initialSelectionCatMember)
 						{	
 							rescheduleStatus.selectedIndex=i;
 							break;
 						}
 					}
 				}
 			}
 			//cancel=false;
 		}
 		
 		initialSelectionCatMember = null;
 		initialSelectionRescheduleStatus = null;

 		function setInitialValues()
		{
            var catMember = <?=$myFormName?>_SaveForm.RescheduleCatMemberID;
            initialSelectionCatMember = null;
            if (catMember.selectedIndex >= 0)
            {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
            }
                    
            var rescheduleStatus = <?=$myFormName?>_SaveForm.RescheduleStatus;
            initialSelectionRescheduleStatus = null;
            if (rescheduleStatus.selectedIndex >= 0)
            {
	        	initialSelectionRescheduleStatus = rescheduleStatus.options[rescheduleStatus.selectedIndex].value;
            }
		}
		
		function cleareBavelFields() {
			var objField = <?=$myFormName?>_SaveForm.eBavelFieldsData;
			if (objField != null) {
				objField.value = '';
			}
		}
		
		function cleareBavelActionFields() {
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFieldsData;
			if (objField != null) {
				objField.value = '';
			}
		}
		
		function openAssigneBavelFields()
		{
			var inteBavelAppID = 0;
			var inteBavelFormID = 0;
			var objField = <?=$myFormName?>_SaveForm.eBavelAppID;
			if (objField != null) {
				inteBavelAppID = objField.value;
			}
			var objField = <?=$myFormName?>_SaveForm.eBavelFormID;
			if (objField != null) {
				inteBavelFormID = objField.value;
			}
			
			openWindowDialog('main.php?BITAM_PAGE=SurveyFieldMap&SurveyID=<?=$this->SurveyID?>&eBavelAppID='+inteBavelAppID+'&eBavelFormID='+inteBavelFormID, [window], [], 600, 600, openAssigneBavelFieldsDone, 'openAssigneBavelFieldsDone');
		}
		
		function openAssigneBavelFieldsDone(sValue, sParams)
		{
			var strInfo = sValue;
			//Actualiza la cadena de los campos mapeados de eBavel
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.eBavelFieldsData.value = strInfo;
				} catch (e) {
				}
			}
		}
		
		function openAssigneBavelActionFields()
		{
			var inteBavelAppID = 0;
			var inteBavelFormID = 0;
			var objField = <?=$myFormName?>_SaveForm.eBavelAppID;
			if (objField != null) {
				inteBavelAppID = objField.value;
			}
			var objField = <?=$myFormName?>_SaveForm.eBavelActionFormID;
			if (objField != null) {
				inteBavelFormID = objField.value;
			}
			
			openWindowDialog('main.php?BITAM_PAGE=SurveyActionMap&SurveyID=<?=$this->SurveyID?>&eBavelAppID='+inteBavelAppID+'&eBavelFormID='+inteBavelFormID, [window], [], 600, 600, openAssigneBavelActionFieldsDone, 'openAssigneBavelActionFieldsDone');
		}
		
		function openAssigneBavelActionFieldsDone(sValue, sParams)
		{
			var strInfo = sValue;
			//Actualiza la cadena de los campos mapeados de eBavel
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.eBavelActionFieldsData.value = strInfo;
				} catch (e) {
				}
			}
		}
 	</script>

 	<script language="javascript" src="js/EditorHTML/dialogs.js"></script>
 	<script language="JavaScript">
	
		/*@AAL 25/02/2015: Abre la ventana de dialogo donde se mostrará el editor de fórmulas, recibe como 
		parámetro el nombre del elemento HTML (NameElement) de donde se tomará y escribirá la fórmula.*/
		function ShowWindowsDialogEditor(NameElement)
		{
			var SurveyID = '<?= $this->SurveyID ?>';
			var aText = document.getElementsByName(NameElement);
			var Parameters = 'SurveyID=' + SurveyID + '&Type=S&aTextEditor=' + encodeURIComponent(aText[0].value);
			openWindowDialog('formulaEditor.php?' + Parameters, [window], [NameElement], 620, 460, ResultEditFormulas, 'ResultEditFormulas');
		}

		/*@AAL 25/02/2015: Esta función recibe como parámetro sValue que corresponde al texto devuelto del editor de fórmulas 
		y sParams corresponde al nombre del elemento HTML donde se asignará la nueva fórmula o texto*/
		function ResultEditFormulas(sValue, sParams)
		{
			document.getElementsByName(sParams[0])[0].value = sValue;
		}
		//@AAL

	</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
		$myFormName = get_class($this);
?>
	<script language="JavaScript">
	 	setInitialValues();
		
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false;
		}		
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");

		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		if(objOkParentButton)
		{
			objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		}
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("<?=$myFormName?>_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
	</script>

	<form name="frmForceStatusChange" action="main.php?BITAM_PAGE=Survey&SurveyID=<?=$this->SurveyID?>&ForceStatus=1" method="post" target="body">
	</form>
	<form name="frmDeleteDataCaptured" action="main.php?BITAM_PAGE=Survey&SurveyID=<?=$this->SurveyID?>&DeleteData=1" method="post" target="body">
	</form>
<?
	}
	
	//@JAPR 2014-03-18: Agregado el icono para las encuestas
	function generateInsideFormCode($aUser)
	{
		include("formattedTextSurvey.php");
	}
	//@JAPR
	
	function insideEdit()
	{
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esveBavelMapping) {
?>
		var qimage = document.getElementById("surveyImageTextArea");
		var qimagedisable = document.getElementById("disableSurveyImage");
		if (qimagedisable) {
			qimagedisable.style.zIndex = 0;
			qimagedisable.style.display = "none";
		}
<?
		}
		
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if (getMDVersion() >= esvMenuImageConfig) {
?>
		var qmenuimage = document.getElementById("surveyMenuImageArea");
		var qmenuimagedisable = document.getElementById("disableSurveyMenuImage");
		if (qmenuimagedisable) {
			qmenuimagedisable.style.zIndex = 0;
			qmenuimagedisable.style.display = "none";
		}
<?
		}
		
		//@JAPR 2013-05-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (getMDVersion() >= esveBavelMapping) {
?>
		objSpanChange = document.getElementById("mapeBavelFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssigneBavelFields();");
<?
		}

		//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		//@JAPR 2013-07-11: Validado temporalmente que no se habiliten estas opciones
		if (getMDVersion() >= esveBavelSuppActions && false) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground.gif)';
		objSpanChange.style.cursor = 'pointer';
		objSpanChange.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChange.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChange.onclick = new Function("openAssigneBavelActionFields();");
<?
		}
	}
	
	function insideCancel()
	{
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esveBavelMapping) {
?>
		var qimage = document.getElementById("surveyImageTextArea");
		var qimagedisable = document.getElementById("disableSurveyImage");
		if (qimagedisable) {
			qimagedisable.style.zIndex = 100;
			qimagedisable.style.display = "block";
		}
<?
		}
		
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if (getMDVersion() >= esvMenuImageConfig) {
?>
		var qmenuimage = document.getElementById("surveyMenuImageArea");
		var qmenuimagedisable = document.getElementById("disableSurveyMenuImage");
		if (qmenuimagedisable) {
			qmenuimagedisable.style.zIndex = 0;
			qmenuimagedisable.style.display = "block";
		}
<?
		}
		
		//@JAPR 2013-05-06: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		if (getMDVersion() >= esveBavelMapping) {
?>
		objSpanChange = document.getElementById("mapeBavelFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
		}
		
		//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		//@JAPR 2013-07-11: Validado temporalmente que no se habiliten estas opciones
		if (getMDVersion() >= esveBavelSuppActions && false) {
?>
		objSpanChange = document.getElementById("mapeBavelActionFields");
		objSpanChange.style.backgroundImage = 'url(images\btnBackground_off.gif)';
		objSpanChange.style.cursor = 'default';
		objSpanChange.onmouseover = "";
		objSpanChange.onmouseout = "";
		objSpanChange.onclick = "";
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			if($this->Status!=svstInactive)
			{
				return true;
			}
			else 
			{
				return false;
			}
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
 	function addButtons($aUser)
	{
		global $gbIsGeoControl;
		
		//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
		if (!$this->isNewObject() && !$gbIsGeoControl)
		{
			if($this->Status==svstActive)
			{
				$isInUse = BITAMSurvey::isInUse($this->Repository, $this->SurveyID);
				
				if($isInUse==true)
				{
	?>
		<!--<a href="#" class="alinkescfav" onclick="forceChangeStatus();"><img src="images/reopen.gif" displayMe=1> <?=translate("Force to change to status: In Development")?> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		<button id="btnForceChangeStatus" class="alinkescfav" onclick="forceChangeStatus();"><img src="images/reopen.gif" alt="<?=translate('Force to change to status: In Development')?>" title="<?=translate('Force to change to status: In Development')?>" displayMe="1" /> <?=translate("Force to change to status: In Development")?></button>
	<?
				}
			}
			else if($this->Status==svstInDevelopment)
			{
	?>
		<!--<a href="#" class="alinkescfav" onclick="deleteDataCaptured();"><img src="images/delete.png" displayMe=1> <?=translate("Delete Data Captured in the Survey")?> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		<button id="btnDeleteDataCaptured" class="alinkescfav" onclick="deleteDataCaptured();"><img src="images/delete.png" alt="<?=translate('Delete Data Captured in the Survey')?>" title="<?=translate('Delete Data Captured in the Survey')?>" displayMe="1" /> <?=translate("Delete Data Captured in the Survey")?></button>
	<?
			}
		}
	
	}

	function get_Children($aUser)
	{
		$myChildren = array();

		if (!$this->isNewObject())
		{
			$myChildren[] = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID);
			
			//@JAPR 2014-06-05: Agregado el refresh de catálogos dinámico
			//Las encuestas ya no tienen un catálogo asociado realmente, además esta tabla cambió su objetivo original (nunca implementado) para
			//contener filtros de todos los posibles catálogos utilizados en sus preguntas/secciones
			//if($this->CatalogID>0)
			//{
			$myChildren[] = BITAMSurveyFilterCollection::NewInstance($this->Repository, $this->SurveyID);
			//}
			//@JAPR
		}

		return $myChildren;
	}
	
	//@JAPR 2016-08-25: Implementada la eliminación múltiple de reportes
	/* Dados los datos que conforman la llave única de una captura (Forma + Usuario + Fecha-Hora), obtiene el ID de la captura correspondiente. En caso de no encontrar la llave de
	la captura o en caso de error, regresará un ID de 0 para que el proceso que invocó a la función no continue
	*/
	static function GetEntryIDFromDateAndUser($aRepository, $aSurveyID, $aUserID, $aDateID) {
		$intReportID = 0;
		$aSurveyID = (int) $aSurveyID;
		$aUserID = (int) $aUserID;
		$aDateID = trim((string) $aDateID);
		if ($aSurveyID <= 0 || $aUserID <= 0 || $aDateID == '') {
			return $intReportID;
		}
		
		$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		if (is_null($objSurvey)) {
			return $intReportID;
		}
		
		$strTableName = $objSurvey->SurveyTable;
		$strKeyField = BITAMSurvey::$KeyField;
		
		$sql = "SELECT {$strKeyField} 
			FROM {$strTableName} 
			WHERE UserID = {$aUserID} AND DateID = ".$aRepository->DataADOConnection->DBTimeStamp($aDateID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intReportID = (int) @$aRS->fields[strtolower($strKeyField)];
		}
		
		return $intReportID;
	}
	
	static function getDateAndUserFromEntryID($aRepository, $surveyInstance, $entryID)
	{
		$arrayData = array();
		
		$tableName = $surveyInstance->SurveyTable;
		$factTable = "RIFACT_".$surveyInstance->ModelID;
		$factKeyDimValTable = "RIDIM_".$surveyInstance->FactKeyDimID;
		$factKeyDimValField = "RIDIM_".$surveyInstance->FactKeyDimID."KEY";
		$latitudeIndField = "IND_".$surveyInstance->LatitudeIndID;
		$longitudeIndField = "IND_".$surveyInstance->LongitudeIndID;
		//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
		$accuracyIndField = '';
		if (getMDVersion() >= esvAccuracyValues) {
			if ($surveyInstance->AccuracyIndID > 0) {
				$accuracyIndField = ", IND_".$surveyInstance->AccuracyIndID." AS AccuracyInd";
			}
		}
		//@JAPR
		
		$sql = "SELECT A.DateID, A.HourID, A.UserID, A.EndTime, B.".$latitudeIndField." AS LatitudeInd, B.".$longitudeIndField." AS LongitudeInd {$accuracyIndField} 
				FROM ".$tableName." A, ".$factTable." B 
				WHERE A.FactKeyDimVal = ".$entryID." AND A.FactKeyDimVal = B.".$factKeyDimValField." ORDER BY A.FactKey";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		
		if(!$aRS->EOF)
		{
			$arrayData["DateID"] = substr($aRS->fields["dateid"], 0, 10);
			$arrayData["HourID"] = $aRS->fields["hourid"];
			$arrayData["UserID"] = $aRS->fields["userid"];
			$arrayData["EndTime"] = $aRS->fields["endtime"];
			$arrayData["Latitude"] = @$aRS->fields["latitudeind"];
			$arrayData["Longitude"] = @$aRS->fields["longitudeind"];
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			$arrayData["Accuracy"] = (int) @$aRS->fields["accuracyind"];
		}
		
		return $arrayData;
	}
	
	//@JAPREliminar: Descontinuada pues no había referencias
	static function getSurveyDataFromDateAndUser($aRepository, $surveyInstance, $questionCollection, $dateID, $hourID, $userID, &$entryID)
	{
		$answersCollection = array();
		$positionsFields = array();
		
		$tableName = $surveyInstance->SurveyTable;
		
		$sql = "SELECT FactKeyDimVal".(count($questionCollection->Collection)>0?", ":"");
		
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			$fieldName = strtolower($aQuestion->SurveyField);
			$positionsFields[$fieldName] = $questionKey;
			
			$sql.=$aQuestion->SurveyField;
			
			if($questionKey != (count($questionCollection->Collection)-1))
			{
				$sql.=",";
			}
		}
		
		//Modificamos la variable $dateID para que incluya la hora, minutos y segundos
		$dateID = substr($dateID, 0, 10)." ".substr($hourID, 0, 8);

		$sql.=" FROM ".$tableName." WHERE DateID = ".$aRepository->DataADOConnection->DBTimeStamp($dateID);
		$sql.=" AND HourID = ".$aRepository->DataADOConnection->Quote($hourID);
		$sql.=" AND UserID = ".$userID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$entryID = (int)$aRS->fields["factkeydimval"];
			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				$arrayValues = array();
				$fieldName = strtolower($aQuestion->SurveyField);

				//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
				if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
				{
					$anValue = (float)$aRS->fields[$fieldName];
					$arrayValues[0] = $anValue;
					
					$position = $positionsFields[$fieldName];
					$answersCollection[$position] = $arrayValues;
				}
				else 
				{
					if($aQuestion->QTypeID!=qtpMulti)
					{
						$anValue = $aRS->fields[$fieldName];
						$arrayValues[0] = $anValue;
						
						$position = $positionsFields[$fieldName];
						$answersCollection[$position] = $arrayValues;
					}
					else 
					{
						$anValue = $aRS->fields[$fieldName];
						if(trim($anValue)!="")
						{
							$arrayValues = explode(";", $anValue);
						}
						
						$position = $positionsFields[$fieldName];
						$answersCollection[$position] = $arrayValues;
					}
				}
			}
		}
		
		return $answersCollection;
	}
	
	//@JAPR 2011-06-28: Agregada la carga de las imagenes por respuesta
	//Regresa un array multidimensional con la lista de folios-preguntas que contienen alguna imagen, indicando la referencia a la respuesta
	//específica que la contiene (en el caso de preguntas tipo selección, en las abiertas siempre son los mismos índices)
	//El método recibe la lista de folios sobre los que deberá revisar si hay o no imagenes asociadas
	function getAnswerWithPictures($aSheetIDs)
	{
		$arrAWP = array();
		$arrValidQuestions = array();
		
		$strFolios = implode(',',$aSheetIDs);
		//Si no hay folios regresa un arreglo vacio
		if (trim($strFolios) == '')
		{
			return array();
		}
		
		//Primero obtiene la lista de preguntas válidas para el reporteador y que puedan contener imagenes
		$aQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		foreach ($aQuestionCollection as $aQuestion)
		{
			if (($aQuestion->HasReqPhoto==1 || $aQuestion->HasReqPhoto==2)&& $aQuestion->IsFieldSurveyID==1)
			{
				$arrValidQuestions[$aQuestion->QuestionID] = $aQuestion;
			}
		}
		
		//Sólo verifica sobre las preguntas válidas
		$strValidQuestions = '';
		foreach ($arrValidQuestions as $aQuestionID => $aQuestion)
		{
			$strValidQuestions .= ','.$aQuestionID;
		}
		
		//Si no hay preguntas válidas sale
		if (trim($strValidQuestions == ''))
		{
			return array();
		}
		else 
		{
			$strValidQuestions = substr($strValidQuestions, 1);
		}

		//Luego verifica si hay imagenes en las preguntas de los folios especificados, si no las hay entonces ya no continua
		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerImage 
				WHERE SurveyID = ".$this->SurveyID." AND FactKey IN (".$strFolios.") AND QuestionID IN (".$strValidQuestions.")";

		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
			return array();
		}
		
		//En este caso por lo menos existe una imagen, así que identifica a que pregunta de que folio pertenece y para que valor(es)
		//Genera un array con las combinaciones por folio, pregunta y respuestas
		$arrCombFQA = array();
		while (!$aRS->EOF)
		{
			//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
			$intFolio = (int) $aRS->fields["factkey"];
			$intQuestionID = (int) $aRS->fields["questionid"];
			$arrCombFQA[$intFolio][$intQuestionID] = array(0, 0);
			//@JAPR
			$aRS->MoveNext();
		}

		return $arrCombFQA;
	}
	//@JAPR
	
	//El método recibe la lista de folios sobre los que deberá revisar si hay documentos asociados
	function getAnswerWithDocuments($aSheetIDs)
	{
		$arrAWP = array();
		$arrValidQuestions = array();
		
		$strFolios = implode(',',$aSheetIDs);
		//Si no hay folios regresa un arreglo vacio
		if (trim($strFolios) == '')
		{
			return array();
		}
		
		//Primero obtiene la lista de preguntas válidas para el reporteador y que puedan contener imagenes
		$aQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		foreach ($aQuestionCollection as $aQuestion)
		{
			if (($aQuestion->HasReqDocument==1 || $aQuestion->HasReqDocument==2)&& $aQuestion->IsFieldSurveyID==1)
			{
				$arrValidQuestions[$aQuestion->QuestionID] = $aQuestion;
			}
		}
		
		//Sólo verifica sobre las preguntas válidas
		$strValidQuestions = '';
		foreach ($arrValidQuestions as $aQuestionID => $aQuestion)
		{
			$strValidQuestions .= ','.$aQuestionID;
		}
		
		//Si no hay preguntas válidas sale
		if (trim($strValidQuestions == ''))
		{
			return array();
		}
		else 
		{
			$strValidQuestions = substr($strValidQuestions, 1);
		}

		//Luego verifica si hay imagenes en las preguntas de los folios especificados, si no las hay entonces ya no continua
		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		$sql = "SELECT FactKey, SurveyID, QuestionID FROM SI_SV_SurveyAnswerDocument 
				WHERE SurveyID = ".$this->SurveyID." AND FactKey IN (".$strFolios.") AND QuestionID IN (".$strValidQuestions.")";

		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
			return array();
		}
		
		//En este caso por lo menos existe una imagen, así que identifica a que pregunta de que folio pertenece y para que valor(es)
		//Genera un array con las combinaciones por folio, pregunta y respuestas
		$arrCombFQA = array();
		while (!$aRS->EOF)
		{
			//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
			$intFolio = (int) $aRS->fields["factkey"];
			$intQuestionID = (int) $aRS->fields["questionid"];
			$arrCombFQA[$intFolio][$intQuestionID] = array(0, 0);
			//@JAPR
			$aRS->MoveNext();
		}

		return $arrCombFQA;
	}
	
	//@JAPR 2011-06-28: Agregada la carga de los comentarios por respuesta
	//Regresa un array multidimensional con la lista de folios-preguntas que contienen algun comentario, indicando la referencia a la respuesta
	//específica que la contiene (en el caso de preguntas tipo selección, en las abiertas siempre son los mismos índices)
	//El método recibe la lista de folios sobre los que deberá revisar si hay o no comentarios asociados
	function getAnswerWithComments($aSheetIDs)
	{
		$arrAWP = array();
		$arrValidQuestions = array();
		
		$strFolios = implode(',',$aSheetIDs);
		//Si no hay folios regresa un arreglo vacio
		if (trim($strFolios) == '')
		{
			return array();
		}
		
		//Primero obtiene la lista de preguntas válidas para el reporteador y que puedan contener comentarios
		$aQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
		foreach ($aQuestionCollection as $aQuestion)
		{
			if (($aQuestion->HasReqComment==1 || $aQuestion->HasReqComment==2) && $aQuestion->IsFieldSurveyID==1)
			{
				$arrValidQuestions[$aQuestion->QuestionID] = $aQuestion;
			}
		}
		
		//Sólo verifica sobre las preguntas válidas
		$strValidQuestions = '';
		foreach ($arrValidQuestions as $aQuestionID => $aQuestion)
		{
			$strValidQuestions .= ','.$aQuestionID;
		}
		
		//Si no hay preguntas válidas sale
		if (trim($strValidQuestions == ''))
		{
			return array();
		}
		else 
		{
			$strValidQuestions = substr($strValidQuestions, 1);
		}

		//Luego verifica si hay comentarios en las preguntas de los folios especificados, si no las hay entonces ya no continua
		//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
		$sql = "SELECT FactKey, SurveyID, QuestionID, StrComment FROM SI_SV_SurveyAnswerComment 
				WHERE SurveyID = ".$this->SurveyID." AND FactKey IN (".$strFolios.") AND QuestionID IN (".$strValidQuestions.")";
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
			return array();
		}
		
		//En este caso por lo menos existe un comentario, así que identifica a que pregunta de que folio pertenece y para que valor(es)
		//Genera un array con las combinaciones por folio, pregunta y respuestas
		$arrCombFQA = array();
		while (!$aRS->EOF)
		{
			//Verificamos si el comentario se encuentra vacio entonces que no se muestre la imagen de Comment
			$strComment = trim($aRS->fields["strcomment"]);
			
			if($strComment!="")
			{
				//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sóla tabla de encuestas, ahora la llave es FactKey
				$intFolio = (int) $aRS->fields["factkey"];
				$intQuestionID = (int) $aRS->fields["questionid"];
				$arrCombFQA[$intFolio][$intQuestionID] =array(0, 0);
				//@JAPR
			}
			$aRS->MoveNext();
		}

		return $arrCombFQA;
	}
	//@JAPR
	
	//@JAPRDescontinuada: Se utilizaba sólo en la edición de encuestas V3 pero incluso ahi estaba en un If al que ya nunca entraba
	static function getAppliedSurveys($aRepository, $surveyID, $userID)
	{
		$arrayAppliedSurveys = array();

		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
		$tableName = $surveyInstance->SurveyTable;
		
		$questionCollection = BITAMQuestion::getIdentifierQuestion($aRepository, $surveyID);
		
		$selectElements = "";
		
		if(!is_null($questionCollection) && count($questionCollection->Collection)>0)
		{
			foreach($questionCollection->Collection as $questionInstance)
			{
				//@JAPR 2013-05-09: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				$blnValid = true;
				switch ($questionInstance->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValid = false;
						break;
				}
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				$selectElements.=", ".$questionInstance->SurveyField;
			}
		}
		
		$sql = "SELECT DateID, HourID, UserID".$selectElements." FROM ".$tableName." WHERE UserID = ".$userID." ORDER BY DateID, HourID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		
		while (!$aRS->EOF)
		{
			$arrayAppliedSurveys[$i] = array();
			$arrayAppliedSurveys[$i]["DateID"] = substr($aRS->fields["dateid"], 0, 10);
			$arrayAppliedSurveys[$i]["HourID"] = $aRS->fields["hourid"];
			$arrayAppliedSurveys[$i]["UserID"] = (int)$aRS->fields["userid"];
			
			$strdate = $arrayAppliedSurveys[$i]["DateID"]." ".$arrayAppliedSurveys[$i]["HourID"];
			$strdateToTime = strtotime($strdate);
			$formattedDate = date("F j, Y, g:i a", $strdateToTime);
			
			$arrayAppliedSurveys[$i]["FormattedDate"] = $formattedDate;
			$arrayAppliedSurveys[$i]["Message"] = "Survey captured on ".$formattedDate;
			
			$identifier = "";
			if(!is_null($questionCollection) && count($questionCollection->Collection)>0)
			{
				foreach($questionCollection->Collection as $questionInstance)
				{
					//@JAPR 2013-05-09: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
					$blnValid = true;
					switch ($questionInstance->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
						case qtpSignature:
						case qtpShowValue:
						case qtpMessage:
						case qtpSkipSection:
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							$blnValid = false;
							break;
					}
					if (!$blnValid) {
						continue;
					}
					//@JAPR
					
					$fieldName = strtolower($questionInstance->SurveyField);
					if($identifier!="")
					{
						$identifier.=" - ";
					}
					$identifier.=$aRS->fields[$fieldName];
				}
				
				$arrayAppliedSurveys[$i]["Message"] = $identifier." captured on ".$formattedDate;
			}

			$i++;
			$aRS->MoveNext();
		}
		
		return $arrayAppliedSurveys;
	}
	
	static function hasSurveyDataInThisSchedEmail($aRepository, $schedulerID, $email)
	{
		$arrayData = array();
		
		$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($aRepository, $schedulerID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $schedulerInstance->SurveyID);

		$tableName = $surveyInstance->SurveyTable;
		$factTable = "RIFACT_".$surveyInstance->ModelID;
		
		$emailKey = 0;
		$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
		$fieldDimEMailKey = $tableDimEmail."KEY";
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($email);
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}
		else 
		{
			return null;
		}

		$schedulerKey = 0;
		$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerKey = $tableDimScheduler."KEY";
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
		}
		else 
		{
			return null;
		}
		
		if($emailKey<=0 || $schedulerKey<=0)
		{
			return null;
		}

		$sql = "SELECT A.DateID, A.HourID, A.FactKeyDimVal FROM ".$tableName." A, ".$factTable." B 
				WHERE A.FactKey = B.FactKey AND B.".$fieldDimEMailKey." = ".$emailKey." AND B.".$fieldDimSchedulerKey." = ".$schedulerKey;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		
		if(!$aRS->EOF)
		{
			$arrayData["DateID"] = substr($aRS->fields["dateid"], 0, 10);
			$arrayData["HourID"] = $aRS->fields["hourid"];
			$arrayData["EntryID"] = $aRS->fields["factkeydimval"];
		}
		
		return $arrayData;
	}
	
	static function hasSurveyDataInThisSchedEmailKey($aRepository, $schedulerID, $email, $emailKeyValue)
	{
		$arrayData = array();
		
		$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($aRepository, $schedulerID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $schedulerInstance->SurveyID);

		$tableName = $surveyInstance->SurveyTable;
		$factTable = "RIFACT_".$surveyInstance->ModelID;
		
		$emailKey = 0;
		$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
		$fieldDimEMailKey = $tableDimEmail."KEY";
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($email);
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}
		else 
		{
			return null;
		}

		$schedulerKey = 0;
		$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerKey = $tableDimScheduler."KEY";
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
		}
		else 
		{
			return null;
		}
		
		if($emailKey<=0 || $schedulerKey<=0)
		{
			return null;
		}
		
		//Obtenemos una instancia del catalogo definido en el scheduler para poder
		//acceder al campo de esa dimension-catalogo y realizar la consulta buscando
		//por el parametro $emailkey
		$catalogInstance = BITAMCatalog::NewInstanceWithID($aRepository, $schedulerInstance->CatalogID);
		$fieldDimCat = "RIDIM_".$catalogInstance->ParentID."KEY";

		if($surveyInstance->DissociateCatDimens==0)
		{
			$sql = "SELECT A.DateID, A.HourID, A.FactKeyDimVal FROM ".$tableName." A, ".$factTable." B 
					WHERE A.FactKey = B.FactKey AND B.".$fieldDimEMailKey." = ".$emailKey." 
					AND B.".$fieldDimSchedulerKey." = ".$schedulerKey." AND B.".$fieldDimCat."=".$emailKeyValue;
		}
		else
		{
			//Buscar primera pregunta simple choice que tenga el mismo catálogo del scheduler
			//Obtener su QuestionID
			//Buscar en tabla paralela por el campo Dim_Questionid ya que en las encuestas con 
			//$surveyInstance->DissociateCatDimens=1 el campo $fieldDimCat ya no existe en la tabla de hechos
			//Pendiente soportar disasociación de catálogos 
			
			$questions = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);
			
			foreach ($questions as $question)
			{
				if($question->QTypeID==qtpSingle && $question->CatalogID==$schedulerInstance->CatalogID)
				{
					break;
				}
			}
			
			if(!is_null($question))
			{
				$sql = "SELECT A.DateID, A.HourID, A.FactKeyDimVal FROM ".$tableName." A, ".$factTable." B 
						WHERE A.FactKey = B.FactKey AND B.".$fieldDimEMailKey." = ".$emailKey." 
						AND B.".$fieldDimSchedulerKey." = ".$schedulerKey." AND A.".$question->SurveyField."=".$emailKeyValue;
			}
			else
			{
				return null;
			}
		}

		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$i=0;

		if(!$aRS->EOF)
		{
			$arrayData["DateID"] = substr($aRS->fields["dateid"], 0, 10);
			$arrayData["HourID"] = $aRS->fields["hourid"];
			$arrayData["EntryID"] = $aRS->fields["factkeydimval"];
		}
		
		return $arrayData;
	}
	
	static function getAppliedSurveysByEmail($aRepository, $schedulerID, $email)
	{
		$arrayAppliedSurveys = array();
		
		$schedulerInstance = BITAMSurveyScheduler::NewInstanceWithID($aRepository, $schedulerID);
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $schedulerInstance->SurveyID);

		$tableName = $surveyInstance->SurveyTable;
		$factTable = "RIFACT_".$surveyInstance->ModelID;
		
		$questionCollection = BITAMQuestion::getIdentifierQuestion($aRepository, $surveyInstance->SurveyID);
		
		$selectElements = "";
		
		if(!is_null($questionCollection) && count($questionCollection->Collection)>0)
		{
			foreach($questionCollection->Collection as $questionInstance)
			{
				//@JAPR 2013-05-09: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
				$blnValid = true;
				switch ($questionInstance->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpAudio:
					case qtpVideo:
					case qtpSignature:
					case qtpShowValue:
					case qtpMessage:
					case qtpSkipSection:
					case qtpSync:
					//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
					case qtpPassword:
					//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
					case qtpSection:
					//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
					case qtpExit:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
						$blnValid = false;
						break;
				}
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				$selectElements.=", A.".$questionInstance->SurveyField;
			}
		}
		
		$emailKey = 0;
		$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
		$fieldDimEMailKey = $tableDimEmail."KEY";
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($email);
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}
		else 
		{
			return $arrayAppliedSurveys;
		}

		$schedulerKey = 0;
		$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerKey = $tableDimScheduler."KEY";
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
		}
		else 
		{
			return $arrayAppliedSurveys;
		}
		
		if($emailKey<=0 || $schedulerKey<=0)
		{
			return $arrayAppliedSurveys;
		}

		$sql = "SELECT A.DateID, A.HourID, A.UserID ".$selectElements." FROM ".$tableName." A, ".$factTable." B 
				WHERE A.FactKey = B.FactKey AND B.".$fieldDimEMailKey." = ".$emailKey." AND B.".$fieldDimSchedulerKey." = ".$schedulerKey." 
				ORDER BY A.DateID, A.HourID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName.", ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$i=0;
		
		while (!$aRS->EOF)
		{
			$arrayAppliedSurveys[$i] = array();
			$arrayAppliedSurveys[$i]["DateID"] = substr($aRS->fields["dateid"], 0, 10);
			$arrayAppliedSurveys[$i]["HourID"] = $aRS->fields["hourid"];
			$arrayAppliedSurveys[$i]["UserID"] = (int)$aRS->fields["userid"];
			
			$strdate = $arrayAppliedSurveys[$i]["DateID"]." ".$arrayAppliedSurveys[$i]["HourID"];
			$strdateToTime = strtotime($strdate);
			$formattedDate = date("F j, Y, g:i a", $strdateToTime);
			
			$arrayAppliedSurveys[$i]["FormattedDate"] = $formattedDate;
			$arrayAppliedSurveys[$i]["Message"] = "Survey captured on ".$formattedDate;
			
			$identifier = "";
			if(!is_null($questionCollection) && count($questionCollection->Collection)>0)
			{
				foreach($questionCollection->Collection as $questionInstance)
				{
					//@JAPR 2013-05-09: Corregido un bug, estos tipos de pregunta no tienen respuesta así que se deben omitir
					$blnValid = true;
					switch ($questionInstance->QTypeID) {
						case qtpPhoto:
						case qtpDocument:
						case qtpAudio:
						case qtpVideo:
						case qtpSignature:
						case qtpShowValue:
						case qtpMessage:
						case qtpSkipSection:
						case qtpSync:
						//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
						case qtpPassword:
						//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
						case qtpSection:
						//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
						case qtpExit:
						//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
						case qtpUpdateDest:
							$blnValid = false;
							break;
					}
					if (!$blnValid) {
						continue;
					}
					//@JAPR
					
					$fieldName = strtolower($questionInstance->SurveyField);
					if($identifier!="")
					{
						$identifier.=" - ";
					}
					$identifier.=$aRS->fields[$fieldName];
				}
				
				$arrayAppliedSurveys[$i]["Message"] = $identifier." captured on ".$formattedDate;
			}

			$i++;
			$aRS->MoveNext();
		}
		
		return $arrayAppliedSurveys;
	}
	
	//@JAPREliminar: Descontinuada pues no había referencias
	static function getSurveyDataFromDateSchedEmail($aRepository, $surveyInstance, $questionCollection, $dateID, $hourID, $schedulerID, $email, &$entryID)
	{
		$answersCollection = array();
		$positionsFields = array();
		
		$tableName = $surveyInstance->SurveyTable;
		$factTable = "RIFACT_".$surveyInstance->ModelID;
		
		$emailKey = 0;
		$tableDimEmail = "RIDIM_".$surveyInstance->EmailDimID;
		$fieldDimEMailKey = $tableDimEmail."KEY";
		$fieldDimEMailDsc = "DSC_".$surveyInstance->EmailDimID;
		
		$sqlEmail = "SELECT ".$fieldDimEMailKey." FROM ".$tableDimEmail." WHERE ".$fieldDimEMailDsc." = ".$aRepository->DataADOConnection->Quote($email);
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlEmail);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimEmail." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlEmail);
		}
		
		if(!$aRS->EOF)
		{
			$emailKey = (int)$aRS->fields[strtolower($fieldDimEMailKey)];
		}

		$schedulerKey = 0;
		$tableDimScheduler= "RIDIM_".$surveyInstance->SchedulerDimID;
		$fieldDimSchedulerKey = $tableDimScheduler."KEY";
		$fieldDimSchedulerClave = "KEY_".$surveyInstance->SchedulerDimID;
		
		$sqlScheduler = "SELECT ".$fieldDimSchedulerKey." FROM ".$tableDimScheduler." WHERE ".$fieldDimSchedulerClave." = ".$schedulerID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sqlScheduler);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableDimScheduler." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlScheduler);
		}
		
		if(!$aRS->EOF)
		{
			$schedulerKey = (int)$aRS->fields[strtolower($fieldDimSchedulerKey)];
		}
		
		$sql = "SELECT A.FactKeyDimVal".(count($questionCollection->Collection)>0?", ":"");
		
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			$fieldName = strtolower($aQuestion->SurveyField);
			$positionsFields[$fieldName] = $questionKey;
			
			$sql.="A.".$aQuestion->SurveyField;
			
			if($questionKey != (count($questionCollection->Collection)-1))
			{
				$sql.=",";
			}
		}
		
		//Modificamos la variable $dateID para que incluya la hora, minutos y segundos
		$dateID = substr($dateID, 0, 10)." ".substr($hourID, 0, 8);

		$sql.=" FROM ".$tableName." A, ".$factTable." B WHERE A.DateID = ".$aRepository->DataADOConnection->DBTimeStamp($dateID);
		$sql.=" AND A.HourID = ".$aRepository->DataADOConnection->Quote($hourID);
		$sql.=" AND A.FactKey = B.FactKey AND B.".$fieldDimEMailKey." = ".$emailKey." AND B.".$fieldDimSchedulerKey." = ".$schedulerKey;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$entryID = (int)$aRS->fields["factkeydimval"];

			foreach ($questionCollection->Collection as $questionKey => $aQuestion)
			{
				$arrayValues = array();
				$fieldName = strtolower($aQuestion->SurveyField);

				//@JAPR 2012-09-25: Agregado el tipo de pregunta Calculo
				if($aQuestion->QTypeID==qtpOpenNumeric || $aQuestion->QTypeID==qtpCalc)
				{
					$anValue = (float)$aRS->fields[$fieldName];
					$arrayValues[0] = $anValue;
					
					$position = $positionsFields[$fieldName];
					$answersCollection[$position] = $arrayValues;
				}
				else 
				{
					if($aQuestion->QTypeID!=qtpMulti)
					{
						$anValue = $aRS->fields[$fieldName];
						$arrayValues[0] = $anValue;
						
						$position = $positionsFields[$fieldName];
						$answersCollection[$position] = $arrayValues;
					}
					else 
					{
						$anValue = $aRS->fields[$fieldName];
						if(trim($anValue)!="")
						{
							$arrayValues = explode(";", $anValue);
						}
						
						$position = $positionsFields[$fieldName];
						$answersCollection[$position] = $arrayValues;
					}
				}
			}
		}
		
		return $answersCollection;
	}
	
	static function canApplySurvey($aRepository, $surveyID, $artusUserID)
	{
		//Obtenemos  los roles de los usuarios
		$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $artusUserID);
		
		//Obtener el listado de schedulers disponibles para el usuario logueado x user
		$sql = "SELECT DISTINCT A.SchedulerID FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerUser B
				WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$surveyID." AND B.UserID = ".$artusUserID;
		
		$recordset = $aRepository->DataADOConnection->Execute($sql);
			
		if ($recordset === false) 
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	
		$anArrayOfSchedulerIDs = array();
		
		if (!$recordset->EOF)
		{
			return true;
		}
		else 
		{
			if(count($roles)>0)
			{
				//Obtener el listado de schedulers disponibles para el usuario logueado x rol
				$sql = "SELECT DISTINCT A.SchedulerID FROM SI_SV_SurveyScheduler A, SI_SV_SurveySchedulerRol B
						WHERE A.SchedulerID = B.SchedulerID AND A.SurveyID = ".$surveyID." AND B.RolID IN (".implode(",", $roles).")";
				
				$recordset = $aRepository->DataADOConnection->Execute($sql);
	
				if ($recordset === false) 
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyScheduler ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
	
				if (!$recordset->EOF)
				{
					return true;
				}
			}
			else 
			{
				return false;
			}
		}
	}
	
	//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
	//Agregado el parámetro $includeNone
	static function getSurveys($aRepository, $arrayStatusSurvey = null, $justAgendables = null, $includeNone = false)
	{
		$where = "";
		if(!is_null($arrayStatusSurvey) && is_array($arrayStatusSurvey) && count($arrayStatusSurvey)>0)
		{
			$where = " WHERE Status IN (".implode(",", $arrayStatusSurvey).")";
		}
		if (!is_null($justAgendables) && $justAgendables) {
			$where .= (($where != "") ? " AND": " WHERE");
			$where .= " IsAgendable > 0";
		}
		
		//@JAPR 2015-07-30: Validado que no aparezcan formas de versiones previas
		$strAdditionalFields = "";
		if (getMDVersion() >= esvExtendedModifInfo) {
			$strAdditionalFields .= ', CreationVersion';
		}
		
		//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
		$sql = "SELECT SurveyID, SurveyName $strAdditionalFields 
			FROM SI_SV_Survey ".$where." 
			ORDER BY LTRIM(RTRIM(SurveyName))";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arraySurveys = array();
		//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
		if ($includeNone) {
			$arraySurveys[0] = translate("None");
		}
		//@JAPR
		while (!$aRS->EOF) {
			$surveyID = $aRS->fields["surveyid"];
			//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
			$surveyName = trim((string) $aRS->fields["surveyname"]);
			//@JAPR 2016-07-27: Validado que si no se grabó el nombre de forma, se genere uno automáticamente
			if ($surveyName == '') {
				$surveyName = "(NoNameSurvey)";
			}
			//@JAPR 2015-07-30: Validado que no aparezcan formas de versiones previas
			$dblCreationAdminVersion = (float) @$aRS->fields["creationversion"];
			if ($dblCreationAdminVersion >= esveFormsv6) {
				$arraySurveys[$surveyID] = $surveyName;
			}
			//@JAPR
			
			$aRS->MoveNext();
		}
		
		return $arraySurveys;
	}
	
	//Copiar definición de encuesta básica
	static function copySurvey($aRepository, $aSurveyID, $showMessage=true)
	{
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		global $gblModMngrErrorMessage;
		
		$gblModMngrErrorMessage = '';
		//@JAPR
		
		$sql =  "SELECT ".$aRepository->DataADOConnection->IfNull("MAX(SurveyID)", "0")." + 1 AS SurveyID FROM SI_SV_Survey";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$temporalID = (int)$aRS->fields["surveyid"];
		$maxSurveyID = $temporalID;
		
		//Definición de la encuesta que se copiará (SI_SV_Survey)
        //@LROUX 2011-10-18: Se agrega AllowPartialSave.
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campo ElementQuestionID, eBavelAppID, eBavelFormID)
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', t1.SyncWhenSaving';
		}
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$strAdditionalFields .= ', t1.UseStdSectionSingleRec';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', t1.DissociateCatDimens';
		}
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strAdditionalFields .= ', t1.OtherLabel, t1.CommentLabel';
		}
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$strAdditionalFields .= ', t1.eBavelActionFormID';
		}
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		if (getMDVersion() >= esvReportEMailsByCatalog) {
			$strAdditionalFields .= ', t1.AdditionalEMails';
		}
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', t1.SurveyImageText';
		}
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if (getMDVersion() >= esvMenuImageConfig) {
			$strAdditionalFields .= ', t1.SurveyMenuImage';
		}
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		if (getMDVersion() >= esvHideNavigationButtons) {
			$strAdditionalFields .= ', t1.HideNavButtons, t1.HideSectionNames';
		}
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.DisableEntry';
		}
		//@JAPR 2014-09-18: Agregados los datos extendidos de modificación
		//Este valor se debe heredar durante la copia en lugar de asignarse a la versión actual, ya que esto controla los reemplazos de IDs de preguntas
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
			$strAdditionalFields .= ', t1.CreationUserID, t1.LastUserID, t1.CreationDateID, t1.LastDateID, t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.IsAgendable, t1.RadioForAgenda';
		}
		//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.SurveyDesc';
		}
		//@JAPR
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		if (getMDVersion() >= esvDestinationIncremental) {
			$strAdditionalFields .= ', t1.incrementalFile';
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', t1.Width, t1.Height, t1.TextPosition, t1.TextSize, t1.CustomLayout';
		}
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			$strAdditionalFields .= ', ShowQNumbers';
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$strAdditionalFields .= ', t1.TemplateID';
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		if (getMDVersion() >= esvFormsDuration) {
			$strAdditionalFields .= ', t1.PlannedDuration';
		}
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$strAdditionalFields .= ', t1.EnableNavHistory';
		}
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (getMDVersion() >= esvEntryDescription) {
			$strAdditionalFields .= ', t1.QuestionIDForEntryDesc';
		}
		if ( getMDVersion() >= esvReloadSurveyFromSection ) {
			$strAdditionalFields .= ', t1.ReloadSurveyFromSection';
		}
		$sql = "SELECT 
					t1.SurveyID,
					t1.SurveyName,
					t1.Status,
					t1.ClosingMsg,
					t1.UserDimID,
					t1.CaptureStartTime, 
					t1.CaptureEndTime, 
					t1.HelpMsg, 
					t1.CatalogID, 
					t1.SurveyType, 
					t1.HasSignature,
          			t1.AllowPartialSave, 
          			t1.ElementQuestionID, 
          			t1.eBavelAppID, 
          			t1.eBavelFormID, 
          			t1.EnableReschedule, 
          			t1.RescheduleCatalogID, 
          			t1.RescheduleCatMemberID, 
          			t1.RescheduleStatus 
          			$strAdditionalFields 
				FROM SI_SV_Survey t1 
				WHERE t1.SurveyID = ".((int) $aSurveyID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if ($aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("The SurveyID ".$aSurveyID."doesnŽt exist"));
		}
			
		$description = $aRS->fields["surveyname"];
		
		if($showMessage)
		{
			echo("<script language='JavaScript'>");
			echo("strInfo='(".date("H:i:s").") - ".translate ("Copying")." ".translate("Survey").": ".$description."';\n");
			echo("newRow = infoCopySurveyTable.insertRow(0);");
			echo("newCell = newRow.insertCell();");
			echo("newCell.innerHTML=\"<span class='infoGenBudget'>\"+strInfo+\"</span>\";");
			echo(str_repeat(" ",4096));
			echo("</script>");
			ob_flush();
			flush();
		}
		
		//Establecer la descripción de la nueva copia del survey
		$i=1;
		$descripFlag = true;
		
		while($descripFlag)
		{
			if($i==1)
			{
				$strDescrip = "Copy Of ".$aRS->fields["surveyname"];
			}
			else
			{
				$strDescrip = "Copy (".$i.") Of ".$aRS->fields["surveyname"];
			}
		
			$sqlDesc = "SELECT SurveyName FROM SI_SV_Survey WHERE SurveyName = ".$aRepository->DataADOConnection->Quote($strDescrip);
			$aRSDesc = $aRepository->DataADOConnection->Execute($sqlDesc);
			if ($aRSDesc === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlDesc);
			}
			
			if ($aRSDesc->EOF)
			{
				$descripFlag = false;
			}
			
			$i++;
		}
		
		$strCaptureStartTime = "";
		$strCaptureEndTime = "";
		
		if(trim($aRS->fields["capturestarttime"])=="")
		{
			$strCaptureStartTime = "NULL";
			$strCaptureEndTime = "NULL";
		}
		else 
		{
			$strCaptureStartTime = $aRepository->DataADOConnection->Quote($aRS->fields["capturestarttime"]);
			$strCaptureEndTime = $aRepository->DataADOConnection->Quote($aRS->fields["captureendtime"]);
		}
		
		$currentDate = date("Y-m-d H:i:s");
		
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		if (getMDVersion() >= esvCatalogDissociation) {
			if ($aSurveyInstance->DissociateCatDimens) {
				//Si la encuesta utilizará los catálogos con dimensiones independientes, hay que asegurarse que estén configurados correctamente
				//antes de proceder o de lo contrario no se podrán reutilizar
				$strCatalogList = '';
				$sql = "SELECT DISTINCT A.CatalogID 
					FROM SI_SV_Question A
					WHERE A.SurveyID = $aSurveyID AND A.CatalogID > 0 AND A.QTypeID = ".qtpSingle;
				//@JAPR 2013-04-18: Corregido un bug, estaba reutilizando el mismo recordset con los datos de la encuesta así que se perdían en la copia
				$aRSTemp = $aRepository->DataADOConnection->Execute($sql);
				if ($aRSTemp === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRSTemp->EOF)  {
					$intCatalogID = (int) @$aRSTemp->fields["catalogid"];
					$strCatalogList .= ','.$intCatalogID;
					$aRSTemp->MoveNext();
				}
				
				if ($strCatalogList != '') {
					$strCatalogList = substr($strCatalogList, 1);
					$objCatalogColl = BITAMCatalogCollection::NewInstance($aRepository, $strCatalogList);
					foreach ($objCatalogColl as $objCatalog) {
						$objCatalog->generateDissociatedDimensions();
					}
				}
			}
		}
		
		//@JAPR 2015-01-29: Agregado el registro de la forma origen de la copia
		$sql = "ALTER TABLE SI_SV_Survey ADD SourceObjectID INTEGER NULL";
		$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_Section ADD SourceObjectID INTEGER NULL";
		$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_Question ADD SourceObjectID INTEGER NULL";
		$aRepository->DataADOConnection->Execute($sql);
		
		//Introducir registro de encuesta
	    //@LROUX 2011-10-18: Se agrega AllowPartialSave.
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas (campo VersionNum)
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campo ElementQuestionID, eBavelAppID, eBavelFormID)
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
		//@JAPR 2012-06-06: Corregido un bug, el campo RescheduleStatus no tenía el Quote y el resto de los campos si están vacios se agregaban mal al insert
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
		$strAdditionalFields = '';
		$strAdditionalValues = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', SyncWhenSaving';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["syncwhensaving"];
		}
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$strAdditionalFields .= ', UseStdSectionSingleRec';
			//@JAPR 2013-03-11: Modificado para que esta opción solo aplique en encuestas nuevas
			$strAdditionalValues .= ', 1';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', DissociateCatDimens';
			//@JAPR 2013-01-21: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
			//Las encuestas copiadas automáticamente nacerán usando los catálogos desasociados, esto para permitir la migración fácil al nuevo
			//esquema sin tener que reconstruir la encuesta desde 0, aunque los datos no son transferibles de todas formas
			$strAdditionalValues .= ', 1';
		}
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strAdditionalFields .= ', OtherLabel, CommentLabel';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["otherlabel"]).
				', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["commentlabel"]);
		}
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$strAdditionalFields .= ', eBavelActionFormID';
			//@JAPR 2013-07-17: Corregido un bug, se había dejado una referencia a $this
			$strAdditionalValues .= ', '.(int) @$aRS->fields["ebavelactionformid"];
		}
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		if (getMDVersion() >= esvReportEMailsByCatalog) {
			$strAdditionalFields .= ', AdditionalEMails';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["additionalemails"]);
		}
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', SurveyImageText';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["surveyimagetext"]);
		}
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if (getMDVersion() >= esvMenuImageConfig) {
			$strAdditionalFields .= ', SurveyMenuImage';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["surveymenuimage"]);
		}
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		if (getMDVersion() >= esvHideNavigationButtons) {
			$strAdditionalFields .= ', HideNavButtons, HideSectionNames';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["hidenavbuttons"].', '.(int) @$aRS->fields["hidesectionnames"];
		}
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', DisableEntry';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["disableentry"];
		}
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		$dteLastDateID = null;
		$intLastUserID = null;
		$intLastVersion = null;
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
			//Las fechas y usuarios de creación y modificación se deben validar con cuiado, ya que no todos los objetos las tienen definidas
			//@JAPR 2014-09-29: Se determinó que los datos de creación deben actualizarse al copiar, mas no así los datos de última modificación
			//$intCreationUserID = @$aRS->fields["creationuserid"];
			//if (!is_null($intCreationUserID) && (int) $intCreationUserID > 0) {
			$strAdditionalFields .= ', CreationUserID';
			$strAdditionalValues .= ', '.(int) @$_SESSION["PABITAM_UserID"];
			//}
			$intLastUserID = @$aRS->fields["lastuserid"];
			if (!is_null($intLastUserID) && (int) $intLastUserID > 0) {
				$strAdditionalFields .= ', LastUserID';
				$strAdditionalValues .= ', '.(int) $intLastUserID;
			}
			//$dteCreationDateID = @$aRS->fields["creationdateid"];
			//if (!is_null($dteCreationDateID) && trim((string) $dteCreationDateID) != '') {
			$strAdditionalFields .= ', CreationDateID';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->DBTimeStamp($currentDate);
			//}
			$dteLastDateID = @$aRS->fields["lastdateid"];
			if (!is_null($dteLastDateID) && trim((string) $dteLastDateID) != '') {
				$strAdditionalFields .= ', LastDateID';
				$strAdditionalValues .= ', '.$aRepository->DataADOConnection->DBTimeStamp($dteLastDateID);
			}
			
			$intLastVersion = (float) @$aRS->fields["lastversion"];
			$strAdditionalFields .= ', CreationVersion, LastVersion';
			$strAdditionalValues .= ', '.ESURVEY_SERVICE_VERSION.', '.$intLastVersion;
		}
		//@JAPR
		if (getMDVersion() >= esvAgendaRedesign) {
        	$strAdditionalFields .= ', IsAgendable, RadioForAgenda';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["isagendable"].', '.(int) @$aRS->fields["radioforagenda"];
        }
		//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', SurveyDesc';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["surveydesc"]);
		}
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		if (getMDVersion() >= esvDestinationIncremental) {
			$strAdditionalFields .= ', incrementalFile';
			$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["incrementalFile"]);
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', Width, Height, TextPosition, TextSize, CustomLayout';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["width"].', '.(int) @$aRS->fields["height"].', '.(int) @$aRS->fields["textposition"].', '.
				$this->Repository->DataADOConnection->Quote((string) @$aRS->fields["textsize"]).', '.
				$this->Repository->DataADOConnection->Quote((string) @$aRS->fields["customlayout"]);
		}
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			$strAdditionalFields .= ', ShowQNumbers';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["showqnumbers"];
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$strAdditionalFields .= ', TemplateID';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["templateid"];
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		if (getMDVersion() >= esvFormsDuration) {
			$strAdditionalFields .= ', PlannedDuration';
			$strAdditionalValues .= ', '.(float) @$aRS->fields["plannedduration"];
		}
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$strAdditionalFields .= ', EnableNavHistory';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["enablenavhistory"];
		}
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (getMDVersion() >= esvEntryDescription) {
			$strAdditionalFields .= ', QuestionIDForEntryDesc';
			$strAdditionalValues .= ', '.(int) @$aRS->fields["questionidforentrydesc"];
		}
		if ( getMDVersion() >= esvReloadSurveyFromSection ) {
			$strAdditionalFields .= ', ReloadSurveyFromSection';
			$strAdditionalValues .=  ', '.(int) @$aRS->fields[ "reloadsurveyfromsection" ];
		}
		//@JAPR 2015-01-29: Agregado el registro de la forma origen de la copia
		$sql = "INSERT INTO SI_SV_Survey (".
					"SurveyID".
					", SurveyName".
					", Status".
					", ClosingMsg".
		            //", CreationUserID".
		            //", CreationDateID".
		            //", LastUserID".
		            //", LastDateID".
		            ", CaptureStartTime".
		            ", CaptureEndTime".
					", HelpMsg".
					", CatalogID".
					", SurveyType".
					", HasSignature".
					", AllowPartialSave".
					", VersionNum".
					", ElementQuestionID".
					", eBavelAppID".
					", eBavelFormID".
					", EnableReschedule".
					", RescheduleCatalogID".
					", RescheduleCatMemberID".
					", RescheduleStatus".
					", SourceObjectID".
					$strAdditionalFields.
					") VALUES (".
					$maxSurveyID.
					",".$aRepository->DataADOConnection->Quote($strDescrip).
					",".(int) $aRS->fields["status"].
					",".$aRepository->DataADOConnection->Quote($aRS->fields["closingmsg"]).
					//",".$_SESSION["PABITAM_UserID"].
					//",".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
					//",".$_SESSION["PABITAM_UserID"].
					//",".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
					",".$strCaptureStartTime.
					",".$strCaptureEndTime.
					",".$aRepository->DataADOConnection->Quote($aRS->fields["helpmsg"]).
					",".(int) $aRS->fields["catalogid"].
					",".(int) $aRS->fields["surveytype"].
					",".(int) $aRS->fields["hassignature"].
					",".(int) $aRS->fields["allowpartialsave"].
					",1".
					",".(int) $aRS->fields["elementquestionid"].
					",".(int) $aRS->fields["ebavelappid"].
					",".(int) $aRS->fields["ebavelformid"].
					",".(int) $aRS->fields["enablereschedule"].
					",".(int) $aRS->fields["reschedulecatalogid"].
					",".(int) $aRS->fields["reschedulecatmemberid"].
					",".$aRepository->DataADOConnection->Quote((string) $aRS->fields["reschedulestatus"]).
					",".(int) $aSurveyID.
					$strAdditionalValues.
					")";
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2015-07-09 Agregado el soporte de Formas v6 con un grabado completamente diferente
		//Se remueven todas las referencias a modelos, dimensiones e indicadores, no se permitirá copiar una forma de versión previa para convertirla en versión nueva
		$arraySections = BITAMSurvey::copySections($aRepository, $aSurveyID, $maxSurveyID, $showMessage);
		//@JAPR 2014-10-17: Corregido un bug, con la introducción del grabado de variables de preguntas y secciones usando IDs, al terminar el
		//proceso de clonado en la copia ya no funcionaban las fórmulas pues utilizaban los IDs de la encuesta original, así que hay que remapearlos
		$arrayQuestions = BITAMSurvey::copyQuestions($aRepository, $aSurveyID, $maxSurveyID, $aNewModelID, $arraySections, $showMessage);
		@BITAMSurvey::TranslateObjecIDsInCopy($aRepository, $aSurveyID, $maxSurveyID, $arraySections, $arrayQuestions);
		//@JAPR
		
		//@JAPR 2012-06-07: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
		//Al terminar de generar la copia regresa el número de versión a 1, ya que el proceso de copia reutiliza métodos que van cambiando el
		//numero de versión por lo que no reflejaría realmente que se acaba de hacer la copia en este momento
		//@JAPR 2014-09-29: Corregido un bug, se estaba actualizando usando el ID de Modelo en lugar del de encuesta
		//@JAPR 2014-09-29: Se determinó que los datos de creación deben actualizarse al copiar, mas no así los datos de última modificación
		//Debido a la manera de grabar las preguntas y otros datos, las propiedades de última modificación se ajustan en lugar de heredarse, así que
		//se tienen que volver a dejar como estaba originalente al terminar todo el proceso
		$strAdditionalFields = '';
		if (!is_null($intLastUserID) && (int) $intLastUserID > 0) {
			$strAdditionalFields .= ', LastUserID = '.(int) $intLastUserID;
		}
		if (!is_null($dteLastDateID) && trim((string) $dteLastDateID) != '') {
			$strAdditionalFields .= ', LastDateID = '.$aRepository->DataADOConnection->DBTimeStamp($dteLastDateID);
		}
		$sql = "UPDATE SI_SV_Survey SET 
				VersionNum = 1 
				, LastVersion = ".(float) @$intLastVersion.
				$strAdditionalFields." 
			WHERE SurveyID = ".$maxSurveyID;
		$aRepository->DataADOConnection->Execute($sql);
        //@JAPR
        
        if($showMessage)
		{
			echo("<script language='JavaScript'>");
			echo("strInfo='(".date("H:i:s").") - ".translate("Survey").": ".$strDescrip." ".translate("has been created")."';\n");
			echo("newRow = infoCopySurveyTable.insertRow(0);");
			echo("newCell = newRow.insertCell();");
			echo("newCell.innerHTML=\"<span class='infoGenBudget'>\"+strInfo+\"</span>\";");
			echo(str_repeat(" ",4096));
			echo("</script>");
			ob_flush();
			flush();
		}
		
		//@JAPR 2013-03-19: Agregado el control de errores del Model Manager
		if ($gblModMngrErrorMessage != '') {
			die($gblModMngrErrorMessage);
		}
		//@JAPR
	}
	
	//@JAPR 2014-10-17: Corregido un bug, con la introducción del grabado de variables de preguntas y secciones usando IDs, al terminar el
	//proceso de clonado en la copia ya no funcionaban las fórmulas pues utilizaban los IDs de la encuesta original, así que hay que remapearlos
	/* Dados los IDs de encuesta anteriores y nueva, asi como los arrays de todas las secciones y preguntas (con IDs anteriores y nuevos), recorre
	todas las propiedades que pudieran involucrar variables de preguntas y/o sección para reemplazar los IDs de la forma original por los
	IDs de esos mismos objetos en la forma recien copiada
	Este proceso se puede ejecutar múltiples ocasiones ya que teoricamente los IDs son únicos así que no hay manera en preguntas de diferentes
	formas pudieran haber compartido el ID. Las variables se considerarán grabadas aún con número de preguntas/secciones si la versión de último
	administrador en modificar los objetos es menor que la que soportó este grabado con IDs, así que esos no sufrirán cambios
	*/
	static function TranslateObjecIDsInCopy($aRepository, $aSurveyID, $aNewSurveyID, $arraySections, $arrayQuestions) {
		//Si la versión actual de la metadata no es siquiera la que soporta IDs en las variables, entonces no hay necesidad de este proceso
		if (getMDVersion() < esvExtendedModifInfo) {	
			return;
		}
		
		//Como este cambio es el mas reciente en la fecha de esta corrección, prácticamente todos los campos previos a esa versión tendrían que 
		//existir, así que sólo se validarán los campos de versiones posteriores
		if (is_null($arraySections) || !is_array($arraySections)) {
			$arraySections = array();
		}
		if (is_null($arrayQuestions) || !is_array($arrayQuestions)) {
			$arrayQuestions = array();
		}
		
		//Primero convierte los IDs de la forma
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t1.LastVersion, t1.AdditionalEMails $strAdditionalFields 
			FROM SI_SV_Survey t1 
			WHERE t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
			//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
			if ($anAdminVersionNum >= esvExtendedModifInfo) {
				$arrFieldsToUpdate = array();
				$strFieldName = 'AdditionalEMails';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
				$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				
				if (count($arrFieldsToUpdate) > 0) {
					//Realiza la actualizacion de los campos que si requirieron cambio de IDs
					$strAdditionalFields = '';
					$strAnd = '';
					foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
						$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
						$strAnd = ', ';
					}
					
					$sql = "UPDATE SI_SV_Survey SET {$strAdditionalFields} 
						WHERE SurveyID = {$aNewSurveyID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
			}
		}
		
		//Convierte los IDs de las secciones
		$strAdditionalFields = '';
		//@JAPR 2017-02-13: Corregido un bug, no estaba convirtiendo los IDs de los filtros dinámicos (#N5ZGDX)
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', t1.DataSourceFilter';
		}
		//@JAPR
		$sql= "SELECT t1.SurveyID, t1.LastVersion, t1.SectionID, t1.FormattedText, t1.ShowCondition, t1.HTMLHeader, t1.HTMLFooter $strAdditionalFields 
			FROM SI_SV_Section t1 
			WHERE t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intSectionID = (int) $aRS->fields["sectionid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intSectionID > 0) {
					$strFieldName = 'FormattedText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ShowCondition';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'HTMLHeader';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'HTMLFooter';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if (getMDVersion() >= esveFormsv6) {
						$strFieldName = 'DataSourceFilter';
						$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
						//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
						$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
						if ($blnNeedReplace !== false) {
							$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
						}
					}
					//@JAPR
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_Section SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$intSectionID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//Convierte los IDs de las preguntas
		$strAdditionalFields = '';
		//@JAPR 2017-02-13: Corregido un bug, no estaba convirtiendo los IDs de los filtros dinámicos (#N5ZGDX)
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.DataSourceFilter';
		}
		//@JAPR
		$sql= "SELECT t1.SurveyID, t1.LastVersion, t1.QuestionID, t1.QuestionText, t1.ActionText, t1.ActionTitle, t1.DefaultValue, t1.QuestionMessage, 
				t1.Formula, t1.ShowCondition $strAdditionalFields 
			FROM SI_SV_Question t1 
			WHERE t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intQuestionID = (int) $aRS->fields["questionid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intQuestionID > 0) {
					$strFieldName = 'QuestionText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ActionText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ActionTitle';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'DefaultValue';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'QuestionMessage';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'Formula';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ShowCondition';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
					if (getMDVersion() >= esvAdminWYSIWYG) {
						$strFieldName = 'DataSourceFilter';
						$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
						//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
						$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
						if ($blnNeedReplace !== false) {
							$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
						}
					}
					//@JAPR
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_Question SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND QuestionID = {$intQuestionID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//Convierte los IDs de las opciones de respuesta
		$strAdditionalFields = '';
		$sql= "SELECT t2.SurveyID, t1.LastVersion, t1.ConsecutiveID, t1.HTMLText, t1.DefaultValue, t1.ActionTitle, t1.ActionText $strAdditionalFields 
			FROM SI_SV_QAnswers t1, SI_SV_Question t2 
			WHERE t1.QuestionID = t2.QuestionID AND t2.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intConsecutiveID = (int) $aRS->fields["consecutiveid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intConsecutiveID > 0) {
					$strFieldName = 'HTMLText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'DefaultValue';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ActionTitle';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'ActionText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_QAnswers SET {$strAdditionalFields} 
							WHERE ConsecutiveID = {$intConsecutiveID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//Convierte los IDs de los Filtros de Formas
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t2.LastVersion, t1.FilterID, t1.FilterText $strAdditionalFields 
			FROM SI_SV_SurveyFilter t1, SI_SV_Survey t2 
			WHERE t1.SurveyID = t2.SurveyID AND t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intFilterID = (int) $aRS->fields["filterid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intFilterID > 0) {
					$strFieldName = 'FilterText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_SurveyFilter SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND FilterID = {$intFilterID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//Convierte los IDs de los Filtros de Secciones
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t2.LastVersion, t1.FilterID, t1.FilterText $strAdditionalFields 
			FROM SI_SV_SectionFilter t1, SI_SV_Section t2 
			WHERE t1.SurveyID = t2.SurveyID AND t1.SectionID = t2.SectionID AND t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intFilterID = (int) $aRS->fields["filterid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intFilterID > 0) {
					$strFieldName = 'FilterText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_SectionFilter SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND FilterID = {$intFilterID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//Convierte los IDs de los Filtros de Preguntas
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t2.LastVersion, t1.FilterID, t1.FilterText $strAdditionalFields 
			FROM SI_SV_QuestionFilter t1, SI_SV_Section t2 
			WHERE t1.SurveyID = t2.SurveyID AND t1.QuestionID = t2.QuestionID AND t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intFilterID = (int) $aRS->fields["filterid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intFilterID > 0) {
					$strFieldName = 'FilterText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_QuestionFilter SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND FilterID = {$intFilterID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2017-02-21: Corregido un bug, había faltado copiar las reglas de salto de las preguntas de selección (#DSN1B5)
		//Convierte los IDs de las opciones de respuesta
		$strAdditionalFields = '';
		if (getMDVersion() >= esvPartialIDsReplaceFix) {
			$strAdditionalFields .= ', t1.LastVersion';
		}
		$sql= "SELECT t2.SurveyID, t1.SkipRuleID, t1.SkipCondition $strAdditionalFields 
			FROM SI_SV_SkipRules t1, SI_SV_Question t2 
			WHERE t1.QuestionID = t2.QuestionID AND t2.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intSkipRuleID = (int) $aRS->fields["skipruleid"];
				$anAdminVersionNum = @$aRS->fields["lastversion"];
				//A partir de esta corrección se crearon estos campos de auditoría para este objeto, así que se usa un default adecuado para que ingrese cuando no venían asignados
				if (is_null($anAdminVersionNum)) {
					$anAdminVersionNum = esvCopyForms;
				}
				
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intSkipRuleID > 0) {
					$strFieldName = 'SkipCondition';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_SkipRules SET {$strAdditionalFields} 
							WHERE SkipRuleID = {$intSkipRuleID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
		
		//Convierte los IDs de las configuraciones por dispositivo
		$strAdditionalFields = '';
		$strEmptyFilters = "AND (A.FormattedText <> ".$aRepository->DataADOConnection->Quote('')." OR 
			A.HTMLHeader <> ".$aRepository->DataADOConnection->Quote('')." OR 
			A.HTMLFooter <> ".$aRepository->DataADOConnection->Quote('').")";
		$sql= "SELECT A.SurveyID, B.LastVersion, ".otySurvey." AS ObjectType, A.ObjectID, A.DeviceID, A.FormattedText, A.HTMLHeader, A.HTMLFooter 
			FROM SI_SV_SurveyHTML A 
			INNER JOIN SI_SV_Survey B ON (A.ObjectType = ".otySurvey." and A.SurveyID = B.SurveyID) 
			WHERE A.SurveyID = {$aNewSurveyID} ".$strEmptyFilters." 
			UNION 
			SELECT A.SurveyID, B.LastVersion, ".otySection." AS ObjectType, A.ObjectID, A.DeviceID, A.FormattedText, A.HTMLHeader, A.HTMLFooter 
			FROM SI_SV_SurveyHTML A 
			INNER JOIN SI_SV_Section B ON (A.ObjectType = ".otySection." and A.SurveyID = B.SurveyID and A.ObjectID = B.SectionID) 
			WHERE A.SurveyID = {$aNewSurveyID} ".$strEmptyFilters." 
			UNION 
			SELECT A.SurveyID, B.LastVersion, ".otyQuestion." AS ObjectType, A.ObjectID, A.DeviceID, A.FormattedText, A.HTMLHeader, A.HTMLFooter 
			FROM SI_SV_SurveyHTML A 
			INNER JOIN SI_SV_Question B on (A.ObjectType = ".otyQuestion." AND A.SurveyID = B.SurveyID AND A.ObjectID = B.QuestionID) 
			WHERE A.SurveyID = {$aNewSurveyID} ".$strEmptyFilters." 
			UNION 
			SELECT A.SurveyID, B.LastVersion, ".otyAnswer." AS ObjectType, A.ObjectID, A.DeviceID, A.FormattedText, A.HTMLHeader, A.HTMLFooter 
			FROM SI_SV_SurveyHTML A 
			INNER JOIN SI_SV_QAnswers B ON (A.ObjectType = ".otyAnswer." AND A.ObjectID = B.ConsecutiveID) 
			INNER JOIN SI_SV_Question C ON (B.QuestionID = C.QuestionID AND C.SurveyID = A.SurveyID) 
			WHERE A.SurveyID = {$aNewSurveyID} ".$strEmptyFilters;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intObjectType = (int) $aRS->fields["objecttype"];
				$intObjectID = (int) $aRS->fields["objectid"];
				$intDeviceID = (int) $aRS->fields["deviceid"];
				$anAdminVersionNum = (float) @$aRS->fields["lastversion"];
				//Sólo si el registro fue grabado a partir de la versión con IDs en las variables es como realiza el proceso
				if ($anAdminVersionNum >= esvExtendedModifInfo && $intObjectType > 0 && $intObjectID > 0) {
					$strFieldName = 'FormattedText';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'HTMLHeader';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					$strFieldName = 'HTMLFooter';
					$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
					//@JAPR 2017-02-17: Corregido un bug, se estaba enviando el parámetro de server variables, el cual ya no se usa en v6 y provocaba reemplazos parciales
					$blnNeedReplace = ReplaceOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions, $anAdminVersionNum);
					if ($blnNeedReplace !== false) {
						$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
					}
					
					if (count($arrFieldsToUpdate) > 0) {
						//Realiza la actualizacion de los campos que si requirieron cambio de IDs
						$strAdditionalFields = '';
						$strAnd = '';
						foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
							$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
							$strAnd = ', ';
						}
						
						$sql = "UPDATE SI_SV_SurveyHTML SET {$strAdditionalFields} 
							WHERE SurveyID = {$aNewSurveyID} AND ObjectType = {$intObjectType} AND ObjectID = {$intObjectID} AND 
								DeviceID = {$intDeviceID}";
						$aRepository->DataADOConnection->Execute($sql);
					}
				}
				$aRS->MoveNext();
			}
		}
	}
	
	/* Similar a TranslateObjecIDsInCopy pero en este caso utilizada exclusivamente para propiedades que pudieran contener HTML con Javascript, buscando sólo secuencias que son seguras
	como acceso a IDs de preguntas o Secciones del tipo selSurvey.questions[ID] o selSurvey.sections[ID], el usuario deberá ser responsable de sólo programar utilizando este tipo de
	secuencias soportadas, o de lo contrario no se prodrá sincronizar correctamente su Javascript entre desarrollo - producción y ser funcional en ambos ambientes
	*/
	static function TranslateJavaScriptObjecIDsInCopy($aRepository, $aSurveyID, $aNewSurveyID, $arraySections, $arrayQuestions) {
		if (is_null($arraySections) || !is_array($arraySections)) {
			$arraySections = array();
		}
		if (is_null($arrayQuestions) || !is_array($arrayQuestions)) {
			$arrayQuestions = array();
		}
		
		//Convierte los campos de las secciones
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t1.SectionID, t1.FormattedText, t1.ShowCondition, t1.HTMLHeader, t1.HTMLFooter $strAdditionalFields 
			FROM SI_SV_Section t1 
			WHERE t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intSectionID = (int) $aRS->fields["sectionid"];
				
				$strFieldName = 'FormattedText';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'ShowCondition';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'HTMLHeader';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'HTMLFooter';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				
				if (count($arrFieldsToUpdate) > 0) {
					//Realiza la actualizacion de los campos que si requirieron cambio de IDs
					$strAdditionalFields = '';
					$strAnd = '';
					foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
						$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
						$strAnd = ', ';
					}
					
					$sql = "UPDATE SI_SV_Section SET {$strAdditionalFields} 
						WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$intSectionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
				
				$aRS->MoveNext();
			}
		}
		
		//Convierte los campos de las preguntas
		$strAdditionalFields = '';
		$sql= "SELECT t1.SurveyID, t1.QuestionID, t1.DefaultValue, t1.QuestionMessage, t1.Formula, t1.ShowCondition $strAdditionalFields 
			FROM SI_SV_Question t1 
			WHERE t1.SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			while (!$aRS->EOF) {
				$arrFieldsToUpdate = array();
				$intQuestionID = (int) $aRS->fields["questionid"];
				
				$strFieldName = 'DefaultValue';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'QuestionMessage';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'Formula';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				$strFieldName = 'ShowCondition';
				$strFieldValue = (string) @$aRS->fields[strtolower($strFieldName)];
				$blnNeedReplace = ReplaceJavaScriptOldVariableIDsAfterCopy($strFieldValue, $arraySections, $arrayQuestions);
				if ($blnNeedReplace !== false) {
					$arrFieldsToUpdate[$strFieldName] = $blnNeedReplace;
				}
				
				if (count($arrFieldsToUpdate) > 0) {
					//Realiza la actualizacion de los campos que si requirieron cambio de IDs
					$strAdditionalFields = '';
					$strAnd = '';
					foreach ($arrFieldsToUpdate as $strFieldName => $strFieldValue) {
						$strAdditionalFields .= $strAnd.$strFieldName.' = '.$aRepository->DataADOConnection->Quote($strFieldValue);
						$strAnd = ', ';
					}
					
					$sql = "UPDATE SI_SV_Question SET {$strAdditionalFields} 
						WHERE SurveyID = {$aNewSurveyID} AND QuestionID = {$intQuestionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
				
				$aRS->MoveNext();
			}
		}
	}
	//@JAPR
	
	/* Copia todas las preguntas de la forma, para lo cual carga la instancia anterior y hace ajustes para que se grabe como una nueva pregunta
	en la forma que se está copiando
	Regresará un array con los IDs anteriores y nuevos de cada pregunta que se copió para que se puedan llevar a cabo procesos adicionales con ellas,
	los cuales sólo se pueden completar hasta que todas las preguntas han sido clonadas (básicamente el reemplazo de formulas que se graban con
	IDs en las tablas, ya que se puede usar cualquier pregunta y hasta terminar de clonarlas tendremos los IDs de todas)
	*/
	static function copyQuestions($aRepository, $aSurveyID, $aNewSurveyID, $aNewModelID, $arraySections, $showMessage = true)
	{
		$currentDate = date("Y-m-d H:i:s");
		//Obtener la coleccion de preguntas para obtener la informacion desde un objeto
		$aQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		$aNewSurveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aNewSurveyID);
		
		//En un arreglo almacenamos los IDs nuevos de los questions
		$arrayNewQuestionIDs = array();
		$arrayOldQuestionIDs = array();
		//$arrayMapQuestionIDs = array();
		
		foreach ($aQuestionCollection as $aQuestion)
		{
			$questionKey = $aQuestion->QuestionID;
			$oldSectionID = $aQuestion->SectionID;
			
			//@JAPR 2015-01-28: Modificado para permitir que durante una copia de forma las configuraciones que pudieran contener variables
			//se modifiquen para utilizar IDs en lugar de números (#Z9SE00)
			//Al terminar el proceso de copiado, se intercambiarán los IDs de las preguntas/secciones de la forma original, por los
			//correspondientes en la forma nueva, así que este proceso debe incrementar la versión con que se considera grabada la pregunta
			//a aquella que ya soportaba el uso de IDs en las fórmulas
			//Se tiene que lanzar este proceso antes de modificar la instancia para que la conversión se haga en la forma original, ya que el
			//caché de objetos provocaría que la colección de preguntas que internamente se va a cargar en este método, se cargue sólo hasta
			//las preguntas que incluyan la primera que use variables, así que varios reemplazos quedarían sin hacerse correctamente
			$aQuestion->translateObjectNumbersByIDsInCopy();
			//@JAPR
			
			//@JAPR 2012-06-06: Modificado el método de clonado de preguntas para que reutilice el código de BITAMQuestion->Save
			//Sobreescribe propiedades de la instancia de Pregunta en base a los nuevos IDs generados para la encuesta copiada
			$aQuestion->SurveyID = $aNewSurveyID;
			$oldGoToSection = $aQuestion->GoToQuestion;
			$aQuestion->SectionID = $arraySections[$oldSectionID];
			$aQuestion->GoToQuestion = (int) @$arraySections[$oldGoToSection];		//El nombre está mal, es por compatibilidad con el nombre en V2, realmente es una Sección, no una Pregunta
			//JAPR 2014-10-09: Agregadas las preguntas tipo sección (Inline)
			$oldSourceSectionID = $aQuestion->SourceSectionID;
			$aQuestion->SourceSectionID = (int) @$arraySections[$oldSourceSectionID];
			//@JAPR
			//Para que se genere como nueva pregunta, hay que forzar al Save a que lo considere como tal (o se puede simplemente quitar el QuestionID)
			$aQuestion->ForceNew = true;
			
			//Se limpian las dimensiones de las respuestas a preguntas que son múltiple dimensión, ya que
			//si se quedan asignadas al entrar al método Save no se volverían a crear las dimensiones
			//correspondientes sino que pensaría que ya las tiene, y aunque la dimensión si existe en forma
			//global gracias a la encuesta original, no estaría asociada al cubo de la encuesta copia
			if ($aQuestion->IsMultiDimension == 1)
			{
				$aQuestion->QIndDimIds = array();
				$aQuestion->QIndicatorIds = array();
			}
			
			//Se debe actualizar el ModelID así como los nombres de tablas que hacer referencia al mismo, o de lo contrario se intentarán crear
			//las columnas en la tabla equivocada
			$aQuestion->ModelID = $aNewSurveyInstance->ModelID;
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//$aQuestion->SurveyTable = "SVSurvey_".$aQuestion->ModelID;
			$aQuestion->SurveyTable = $aNewSurveyInstance->SurveyTable;
			//@JAPR
			$aQuestion->SurveyMatrixTable = "SVSurveyMatrixData_".$aQuestion->ModelID;
			
			//@JAPR 2013-12-05: Agregada la opción para compartir las opciones de respuesta con otras preguntas
			if ($aQuestion->SharedQuestionID > 0) {
				//Si la pregunta es compartida, entonces actualiza el ID de la pregunta origen hacia el nuevo ID de su copia en esta encuesta
				//para que el Save pueda grabar correctamente la relación de las opciones de respuesta (no se puede hacer posterior al Save como
				//con las opciones SourceQuestionID o SourceSimpleQuestionID, ya que dichas opciones no generan opciones de respuesta como el
				//compartir, así que en este caso debe ser antes del grabado de la copia, aunque como sólo se puede compartir con una pregunta
				//previa, pues es prácticamente un hecho que para este momento su copia ya estaría creada)
				$aQuestion->SharedQuestionID = (int) @$arrayNewQuestionIDs[$aQuestion->SharedQuestionID];
			}
			//@JAPR
			
			//Graba la pregunta como nueva en esta encuesta, conservando todas las propiedades de configuración y generando todas las Dimensiones
			//e indicadores (como se está forzando como nueva pregunta, no importa que traiga los IDs de dimensiones e indicadores de la pregunta
			//original, ya que en el caso de nuevas preguntas todo se crea en base a la configuración de la misma)
			$aQuestion->Save(false, -1, -1, $questionKey);
			
			//Actualiza el Array con los QuestionID a los de la copia
			$arrayNewQuestionIDs[$questionKey] = $aQuestion->QuestionID;
			$arrayOldQuestionIDs[$aQuestion->QuestionID] = $questionKey;
			
			//@JAPR 2015-01-29: Agregado el registro de la forma origen de la copia
			$sql = "UPDATE SI_SV_Question SET SourceObjectID = {$questionKey} 
				WHERE SurveyID = {$aNewSurveyID} AND QuestionID = {$aQuestion->QuestionID}";
			$aRepository->DataADOConnection->Execute($sql);
			
			//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las preguntas
			//En este punto que ya tiene creada la sección copia, realiza la copia de propiedades adicionales que dependen del nuevo ID en la copia
			if (getMDVersion() >= esvInlineSection) {
				if ($aQuestion->IsSelector) {
					//Si es la pregunta selector, entonces quiere decir que es una sección Inline y debe asignarse a dicha sección la nueva referencia
					//a esta pregunta para que funcione correctamente
					$sql = "UPDATE SI_SV_Section SET SelectorQuestionID = {$aQuestion->QuestionID} 
						WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$aQuestion->SectionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
			}
			
			//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			if (getMDVersion() >= esvSketchPlus) {
				if ($aQuestion->IsCoordX) {
					//Si es la pregunta de coordenada, entonces quiere decir que es una sección Maestro-detalle y debe asignarse a dicha sección la nueva referencia
					//a esta pregunta para que funcione correctamente
					$sql = "UPDATE SI_SV_Section SET XPosQuestionID = {$aQuestion->QuestionID} 
						WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$aQuestion->SectionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
				if ($aQuestion->IsCoordY) {
					//Si es la pregunta de coordenada, entonces quiere decir que es una sección Maestro-detalle y debe asignarse a dicha sección la nueva referencia
					//a esta pregunta para que funcione correctamente
					$sql = "UPDATE SI_SV_Section SET YPosQuestionID = {$aQuestion->QuestionID} 
						WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$aQuestion->SectionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
				if ($aQuestion->IsItemName) {
					//Si es la pregunta de nombre de elemento, entonces quiere decir que es una sección Maestro-detalle y debe asignarse a dicha sección la nueva referencia
					//a esta pregunta para que funcione correctamente
					$sql = "UPDATE SI_SV_Section SET ItemNameQuestionID = {$aQuestion->QuestionID} 
						WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$aQuestion->SectionID}";
					$aRepository->DataADOConnection->Execute($sql);
				}
			}
			//@JAPR
		}
		
		//Terminada la generación de todas las preguntas en la encuesta copia, empieza a agregar los detalles de las Respuestas y sus opciones
		//para mostrar otras preguntas (como se grabó como si se tratara de una nueva pregunta, algunas cosas ya se crearon en automático como
		//nuevas, pero los ShowQuestions y el SourceQuestionID entre otras no, así que hay que actualizarlos manualmente)
		foreach ($aQuestionCollection as $aQuestion)
		{
			$questionKey = (int) @$arrayOldQuestionIDs[$aQuestion->QuestionID];
			if ($aQuestion->SourceQuestionID > 0)
			{
				$intSourceQuestionID = (int) @$arrayNewQuestionIDs[$aQuestion->SourceQuestionID];
				
				$sql = "UPDATE SI_SV_Question SET SourceQuestionID = ".$intSourceQuestionID.
					" WHERE QuestionID = ".$aQuestion->QuestionID;
				$aRepository->DataADOConnection->Execute($sql);
			}
			
			if(($aQuestion->QTypeID==qtpSingle && $aQuestion->UseCatalog==0 && $aQuestion->QDisplayMode!=dspMatrix))
			{
				//Actualiza los saltos de las posibles respuestas
				$aQuestionOptionsColl = BITAMQuestionOptionCollection::NewInstance($aRepository, $aQuestion->QuestionID);
				if (!is_null($aQuestionOptionsColl))
				{
					foreach ($aQuestionOptionsColl as $aQuestionOption)
					{
						if ($aQuestionOption->NextQuestion > 0)
						{
							$intNextSection = $arraySections[$aQuestionOption->NextQuestion];
							$sql = "UPDATE SI_SV_QAnswers SET NextSection = ".$intNextSection.
								" WHERE ConsecutiveID = ".$aQuestionOption->QuestionOptionID." AND QuestionID = ".$aQuestionOption->QuestionID;
							$aRepository->DataADOConnection->Execute($sql);
						}
					}
				}

				//Se cargan las ShowQuestions de la pregunta origen, posteriormente se recorre cada una actualizando los IDs de preguntas
				$aShowQuestionColl = BITAMShowQuestionCollection::NewInstanceByQuestionID($aRepository, $questionKey);
				if (!is_null($aShowQuestionColl) && count($aShowQuestionColl->Collection) > 0)
				{
					//Obtiene la colección de Respuestas posibles de esta pregunta
					if (!is_null($aQuestionOptionsColl) && count($aQuestionOptionsColl->Collection) > 0)
					{
						foreach ($aShowQuestionColl as $aShowQuestion)
						{
							//Identifica el ConsecutiveID de la respuesta nueva comparando contra el texto, ya que como se generó como nueva
							//pregunta pero no había ningún array donde se grabara eso, no tenemos forma de obtenerlo de otra manera
							$strOptionText = '';
							foreach ($aQuestion->SelectOptions as $intIdx => $displayText)
							{
								if ((int) @$aQuestion->QConsecutiveIDs[$intIdx] == $aShowQuestion->ConsecutiveID)
								{
									$strOptionText = strtolower($displayText);
									break;
								}
							}
							
							$intConsecutiveID = 0;
							if ($strOptionText != '')
							{
								foreach ($aQuestionOptionsColl as $aQuestionOption)
								{
									if ($strOptionText == strtolower($aQuestionOption->QuestionOptionName))
									{
										$intConsecutiveID = $aQuestionOption->QuestionOptionID;
										break;
									}
								}
							}
							
							$intShowQuestionID = (int) @$arrayNewQuestionIDs[$aShowQuestion->ShowQuestionID];
							if ($intShowQuestionID > 0 && $intConsecutiveID > 0)
							{
								//Se elimina el ShowID para que se cree como nuevo
								$aShowQuestion->ShowID = 0;
								$aShowQuestion->ConsecutiveID = $intConsecutiveID;
								$aShowQuestion->QuestionID = $aQuestion->QuestionID;
								$aShowQuestion->ShowQuestionID = $intShowQuestionID;
								$aShowQuestion->Save();
							}
						}
					}
				}
			}
		}
		
		//@JAPR 2014-10-17: Corregido un bug, con la introducción del grabado de variables de preguntas y secciones usando IDs, al terminar el
		//proceso de clonado en la copia ya no funcionaban las fórmulas pues utilizaban los IDs de la encuesta original, así que hay que remapearlos
		return $arrayNewQuestionIDs;
	}

	static function copySections($aRepository, $aSurveyID, $aNewSurveyID, $showMessage = true)
	{
		$currentDate = date("Y-m-d H:i:s");
		$arraySections = array();
		
		//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las secciones (de SectionType a EnableHyperlinks)
		//@JAPR 2014-09-18: Agregados los datos extendidos de modificación
		$strAdditionalFields = "";
		//Este valor se debe heredar durante la copia en lugar de asignarse a la versión actual, ya que esto controla los reemplazos de IDs de preguntas
		if (getMDVersion() >= esvExtendedModifInfo) {
			$strAdditionalFields .= ', t1.CreationUserID, t1.LastUserID, t1.CreationDateID, t1.LastDateID, t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR 2014-09-25: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
		if (getMDVersion() >= esvMultiDynamicAndStandarization) {
			$strAdditionalFields .= ', t1.NextSectionID ';
		}
		if (getMDVersion() >= esvAutoRedrawFmtd) {
			$strAdditionalFields .= ', t1.AutoRedraw';
		}
		if (getMDVersion() >= esvShowSectionCondition) {
			$strAdditionalFields .= ', t1.ShowCondition ';
		}
		if (getMDVersion() >= esvInlineSection) {
			$strAdditionalFields .= ', t1.DisplayMode, t1.SwitchBehaviour ';
		}
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			$strAdditionalFields .= ', t1.HTMLFooter, t1.HTMLHeader ';
		}
		if (getMDVersion() >= esvShowInOnePageMD) {
			$strAdditionalFields .= ', t1.SummaryInMDSection ';
		}
		if (getMDVersion() >= esvFixedInlineSections) {
			$strAdditionalFields .= ', t1.OptionsText ';
		}
		if (getMDVersion() >= esvSectionRecap) {
			$strAdditionalFields .= ', t1.SectionFormID ';
		}
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.ShowInNavMenu ';
		}
		//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
		//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
		if (getMDVersion() >= esvInlineMerge) {
			$strAdditionalFields .= ', t1.ShowSelector, t1.ShowAsQuestion ';
		}
		//@JAPR
		$sql= "SELECT t1.SurveyID, t1.SectionID, t1.SectionName, t1.SectionNumber, t1.SectionType, t1.CatalogID, t1.CatMemberID, t1.FormattedText, 
				t1.SendThumbnails, t1.EnableHyperlinks $strAdditionalFields 
			FROM SI_SV_Section t1 
			WHERE t1.SurveyID = ".$aSurveyID." 
			ORDER BY t1.SectionNumber";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
		//Este array contiene indexado por el ID de la sección actual, a que sección debería brincar basado en el ID de la sección original (sólo
		//se asigna si existe brinco)
		$arrNextSectionIDs = array();
		//@JAPR
		while (!$aRS->EOF) 
		{
			//@JAPR 2015-01-29: Agregado el registro de la forma origen de la copia
			$intSectionID = (int) $aRS->fields["sectionid"];
			//@JAPR 2014-09-26: Modificado para obtener el siguiente ID justo antes del INSERT en lugar de incrementarlo localmente, esto reduce
			//un poco el riesgo de colisión entre procesos independientes que pudieran estar agregando secciones
			$sql =  "SELECT ".
					$aRepository->DataADOConnection->IfNull("MAX(SectionID)", "0")." + 1 AS SectionID".
					" FROM SI_SV_Section";
			$aRSTemp = $aRepository->DataADOConnection->Execute($sql); 
			if ($aRSTemp === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$temporalID = (int) @$aRSTemp->fields["sectionid"];
			$maxSectionID = $temporalID;
			
			//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las secciones (de SectionType a EnableHyperlinks)
			//@JAPR 2014-09-18: Agregados los datos extendidos de modificación
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvExtendedModifInfo) {
				//Las fechas y usuarios de creación y modificación se deben validar con cuiado, ya que no todos los objetos las tienen definidas
				//@JAPR 2014-09-29: Se determinó que los datos de creación deben actualizarse al copiar, mas no así los datos de última modificación
				//$intCreationUserID = @$aRS->fields["creationuserid"];
				//if (!is_null($intCreationUserID) && (int) $intCreationUserID > 0) {
				$strAdditionalFields .= ', CreationUserID';
				$strAdditionalValues .= ', '.(int) @$_SESSION["PABITAM_UserID"];
				//}
				$intLastUserID = @$aRS->fields["lastuserid"];
				if (!is_null($intLastUserID) && (int) $intLastUserID > 0) {
					$strAdditionalFields .= ', LastUserID';
					$strAdditionalValues .= ', '.(int) $intLastUserID;
				}
				//$dteCreationDateID = @$aRS->fields["creationdateid"];
				//if (!is_null($dteCreationDateID) && trim((string) $dteCreationDateID) != '') {
				$strAdditionalFields .= ', CreationDateID';
				$strAdditionalValues .= ', '.$aRepository->DataADOConnection->DBTimeStamp($currentDate);
				//}
				$dteLastDateID = @$aRS->fields["lastdateid"];
				if (!is_null($dteLastDateID) && trim((string) $dteLastDateID) != '') {
					$strAdditionalFields .= ', LastDateID';
					$strAdditionalValues .= ', '.$aRepository->DataADOConnection->DBTimeStamp($dteLastDateID);
				}
				
				$strAdditionalFields .= ', CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.ESURVEY_SERVICE_VERSION.', '.(float) @$aRS->fields["lastversion"];
			}
			//@JAPR 2014-09-25: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
			if (getMDVersion() >= esvMultiDynamicAndStandarization) {
				//Este valor se deja como 0 inicialmente, porque los IDs de secciones nuevos aún no se tienen en este punto, mas abajo se actualizará
				$strAdditionalFields .= ', NextSectionID';
				$strAdditionalValues .= ', 0';
				//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
				$intNextSectionIDOld = (int) @$aRS->fields["nextsectionid"];
				if ($intNextSectionIDOld > 0) {
					$arrNextSectionIDs[$maxSectionID] = $intNextSectionIDOld;
				}
				//@JAPR
			}
			if (getMDVersion() >= esvAutoRedrawFmtd) {
				$strAdditionalFields .= ', AutoRedraw';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["autoredraw"]);
			}
			if (getMDVersion() >= esvShowSectionCondition) {
				$strAdditionalFields .= ', ShowCondition ';
				$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["showcondition"]);
			}
			if (getMDVersion() >= esvInlineSection) {
				$strAdditionalFields .= ', DisplayMode, SwitchBehaviour ';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["displaymode"]).
					', '.((int) @$aRS->fields["switchbehaviour"]);
			}
			if (getMDVersion() >= esvSectionFmtdHeadFoot) {
				$strAdditionalFields .= ', HTMLFooter, HTMLHeader ';
				$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["htmlfooter"]).
					', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["htmlheader"]);
			}
			if (getMDVersion() >= esvShowInOnePageMD) {
				$strAdditionalFields .= ', SummaryInMDSection ';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["summaryinmdsection"]);
			}
			if (getMDVersion() >= esvFixedInlineSections) {
				$strAdditionalFields .= ', OptionsText ';
				$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote((string) @$aRS->fields["optionstext"]);
			}
			if (getMDVersion() >= esvSectionRecap) {
				$strAdditionalFields .= ', SectionFormID ';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["sectionformid"]);
			}
			if (getMDVersion() >= esvSurveySectDisableOpts) {
				$strAdditionalFields .= ', ShowInNavMenu ';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["showinnavmenu"]);
			}
			//@JAPR 2014-09-25: Agregada la configuración para que no se muestre el Checkbox selector de las secciones Inline
			//@JAPR 2014-09-25: Agregada la configuración para que las secciones Inline puedan fusionarse en una sección Inline o estandar previa
			if (getMDVersion() >= esvInlineMerge) {
				$strAdditionalFields .= ', ShowSelector, ShowAsQuestion ';
				$strAdditionalValues .= ', '.((int) @$aRS->fields["showselector"]).
					', '.((int) @$aRS->fields["showasquestion"]);
			}
			//@JAPR 2015-01-29: Agregado el registro de la forma origen de la copia
			$sql = "INSERT INTO SI_SV_Section (".
				" SurveyID".
				", SectionID".
				", SectionName".
				", SectionNumber".
				", SectionType".
				", CatalogID".
				", CatMemberID".
				", FormattedText".
				", SendThumbnails".
				", EnableHyperlinks".
				", SourceObjectID".
				$strAdditionalFields.
				") VALUES (".
				$aNewSurveyID.
				",".$maxSectionID.
				",".$aRepository->DataADOConnection->Quote($aRS->fields["sectionname"]).
				",".$aRS->fields["sectionnumber"].
				",".(int) $aRS->fields["sectiontype"].
				",".(int) $aRS->fields["catalogid"].
				",".(int) $aRS->fields["catmemberid"].
				",".$aRepository->DataADOConnection->Quote((string) $aRS->fields["formattedtext"]).
				",".(int) $aRS->fields["sendthumbnails"].
				",".(int) $aRS->fields["enablehyperlinks"].
				",".(int) $intSectionID.
				$strAdditionalValues.
				")";
			if($aRepository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$oldSectionID = (int)$aRS->fields["sectionid"];
			$arraySections[$oldSectionID] = $maxSectionID;
			
			//@JAPR 2013-03-12: Corregido un bug, si la sección es dinámica, al copiarla debe generar las dimensiones de los atributos que usará
			//@JAPR 2014-09-23: Corregido un bug, al copiar las encuestas que generen dimensiones, deberá de crear dichas dimensiones nuevamente
			$intSectionType = (int) @$aRS->fields["sectiontype"];
			switch ($intSectionType) {
				case sectDynamic:
					$objSection = @BITAMSection::NewInstanceWithID($aRepository, $maxSectionID);
					if (!is_null($objSection)) {
						$objSection->getChildCatMemberID();
						$objSection->createDissociatedCatalogDimension();
					}
					break;
					
				case sectInline:
					$objSection = @BITAMSection::NewInstanceWithID($aRepository, $maxSectionID);
					if (!is_null($objSection)) {
						$objSection->getChildCatMemberID();
						$objSection->createDissociatedCatalogDimension();
						//La pregunta selector no se necesita clonan en este punto, ya que al clonar a las propias preguntas de la sección va
						//implícita junto a su dimensión
						//$objSection->createSelectorQuestion();
						$objSection->createSectionDimension();
					}
					break;
			}
			
			//@JAPR 2012-06-06: Corregido un bug, no se estaban copiando todas las propiedades de las secciones (de SectionType a EnableHyperlinks)
			//En este punto que ya tiene creada la sección copia, realiza la copia de propiedades adicionales que dependen del nuevo ID en la copia
			if (getMDVersion() >= esvResponsiveDesign) {
				$sql = "INSERT INTO SI_SV_SurveyHTML (SurveyID, ObjectType, ObjectID, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText) 
					SELECT {$aNewSurveyID}, ObjectType, {$maxSectionID}, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText 
					FROM SI_SV_SurveyHTML 
					WHERE SurveyID = {$aSurveyID} AND ObjectType = ".otySection." AND ObjectID = {$oldSectionID}";
				$aRepository->DataADOConnection->Execute($sql);
			}
			//@JAPR
			
			//@JAPR 2014-09-26: Modificado para obtener el siguiente ID justo antes del INSERT en lugar de incrementarlo localmente, esto reduce
			//un poco el riesgo de colisión entre procesos independientes que pudieran estar agregando secciones
			//$maxSectionID++;
			//@JAPR
			$aRS->MoveNext();
		}
		
		//@JAPR 2014-09-26: Corregido un bug, no se estaban respetando muchas de las configuraciones durante el copiado de encuestas
		foreach ($arrNextSectionIDs as $intSectionID => $intNextSectionIDOld) {
			if (!isset($arraySections[$intNextSectionIDOld])) {
				//Si ya no pudo identificar la sección anterior, entonces no debe asignar este valor aunque se pierda el salto configurado
				continue;
			}
			
			$intNextSectionID = (int) @$arraySections[$intNextSectionIDOld];
			$sql = "UPDATE SI_SV_Section SET NextSectionID = {$intNextSectionID} 
				WHERE SurveyID = {$aNewSurveyID} AND SectionID = {$intSectionID}";
			$aRepository->DataADOConnection->Execute($sql);
		}
		//@JAPR
		
		return $arraySections;
	}
	
	static function createNewIndicator($strName, $agregation, $format, $aRepository, $aNewSurveyID, $aNewModelID, $showMessage = true) {
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		//require_once("../model_manager/utilsEMonitoring.inc.php");
		global $generateXLS;
		$generateXLS = false;
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($aRepository);
		$anInstanceIndicator->ModelID = $aNewModelID;
		$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($aRepository, $aNewModelID);
		$anInstanceIndicator->IndicatorName = $strName;
		$anInstanceIndicator->formato = $format;
		$anInstanceIndicator->agrupador = $agregation;
		$anInstanceIndicator->sql_source = $agregation;

		$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
		$aIndicatorIndID = $anInstanceIndicator->IndicatorID;
		
		$sql = "UPDATE SI_SV_Survey SET ".$strName."IndID = ".$aIndicatorIndID." WHERE SurveyID = ".$aNewSurveyID;
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $aIndicatorIndID;
	}
	
	function createNewIndicatorInLine($aRepository, $aNewSurveyID, $aNewModelID, $showMessage = true)
	{
		//Realizamos instancia de la encuesta para obtener unos datos de ella
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aNewSurveyID);
		
		//Obtener el maximo consecutivo de tabla si_ind_linea
		$sql = "SELECT ".$aRepository->ADOConnection->IfNull("MAX(CONSECUTIVO)", "0")." + 1 AS CONSECUTIVO FROM SI_IND_LINEA";
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_IND_LINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$consecutivo = (int) $aRS->fields["consecutivo"];
		$cla_indicador = 0;
		$cla_conexion = 1;
		$nombre = "Actions";
		$valores = "SurveyDimVal, Action";
		$tablas = "SVSurveyActions";
		$condiciones = "SurveyID = ".$aNewSurveyID;
		$agrupamiento = "";
		$orden = "FactKey";
		$titulos = "Survey ID|Actions|";
		$posiciones = "1,15,";
		
		$sql = "INSERT INTO SI_IND_LINEA ( 
				CONSECUTIVO
				, CLA_INDICADOR
				, CLA_CONEXION
				, NOMBRE
				, VALORES
				, TABLAS
				, CONDICIONES
				, AGRUPAMIENTO
				, ORDEN
				, TITULOS
				, POSICIONES 
				) VALUES ( 
				".$consecutivo."
				, ".$cla_indicador."
				, ".$cla_conexion."
				, ".$aRepository->ADOConnection->Quote($nombre)." 
				, ".$aRepository->ADOConnection->Quote($valores)." 
				, ".$aRepository->ADOConnection->Quote($tablas)." 
				, ".$aRepository->ADOConnection->Quote($condiciones)." 
				, ".$aRepository->ADOConnection->Quote($agrupamiento)." 
				, ".$aRepository->ADOConnection->Quote($orden)." 
				, ".$aRepository->ADOConnection->Quote($titulos)." 
				, ".$aRepository->ADOConnection->Quote($posiciones)." 
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_IND_LINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//En esta parte obtenemos el consecutivo de la dimension Encuesta ID, ya que tenemos solamente el cla_descrip pero para asociar la dimension
		//al indicador en linea se requiere el consecutivo
		//@JAPR 2012-05-10: Corregido un bug, se estaba intentando usar la propiedad FactKeyDimID pero en estos métodos createNew usados durante
		//el copiado de encuestas, la instancia que se carga inicialmente se queda en global instance, y como se ejecutan UPDATEs que modifican
		//su contenido, termina usandose una instancia que no tiene los últimos cambios y por tanto no realizaba todo lo que debía
		
		//Para obtener el fieldName se tiene que consultar el FactKeyDimID de la nueva encuesta
		$sql = "SELECT FactKeyDimID FROM SI_SV_Survey WHERE SurveyID = ".$aNewSurveyID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$factKeyDimID = (int)$aRS->fields["factkeydimid"];
		
		//@JAPR 2012-05-10: Se cambió la referencia de la variable para obtener el FactKeyDimID
		$sql = "SELECT CONSECUTIVO FROM SI_CPTO_LLAVE WHERE CLA_CONCEPTO = ".$surveyInstance->ModelID." AND CLA_DESCRIP = ".$factKeyDimID;	//$surveyInstance->FactKeyDimID
		//@JAPR
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if (!$aRS || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_CPTO_LLAVE ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$consecutivoClaDescrip = (int)$aRS->fields["consecutivo"];
		$expresion = "SurveyDimVal IN (?)";
		
		//Procedemos a insertar relacion dimension con indicador en linea
		$sql = "INSERT INTO SI_INDL_LLAVE ( 
				IND_LINEA
				, LLAVE
				, EXPRESION
				) VALUES ( 
				".$consecutivo."
				, ".$consecutivoClaDescrip."
				, ".$aRepository->ADOConnection->Quote($expresion)." 
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDL_LLAVE ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Y por cada uno de los indicadores normales que se tengan, se hara la asociacion de indicadores en linea junto a ellos,
		//para este caso, tenemos 4 indicadores creados en el cubo de la encuesta los cuales son:
		//El indicador COUNT, el indicador AnsweredQuestions, Duration, Latitude y Longitude
		
		//Insertar indicador COUNT
		$sql = "INSERT INTO SI_INDxLINEA ( 
				CLA_INDLINEA
				, CLA_INDICADOR
				) VALUES ( 
				".$consecutivo."
				, ".$surveyInstance->CountIndID."
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//Insertar indicador AnsweredQuestions
		$sql = "INSERT INTO SI_INDxLINEA ( 
				CLA_INDLINEA
				, CLA_INDICADOR
				) VALUES ( 
				".$consecutivo."
				, ".$surveyInstance->AnsweredQuestionsIndID."
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Insertar indicador Duration
		$sql = "INSERT INTO SI_INDxLINEA ( 
				CLA_INDLINEA
				, CLA_INDICADOR
				) VALUES ( 
				".$consecutivo."
				, ".$surveyInstance->DurationIndID."
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Insertar indicador Latitude
		$sql = "INSERT INTO SI_INDxLINEA ( 
				CLA_INDLINEA
				, CLA_INDICADOR
				) VALUES ( 
				".$consecutivo."
				, ".$surveyInstance->LatitudeIndID."
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Insertar indicador Longitude
		$sql = "INSERT INTO SI_INDxLINEA ( 
				CLA_INDLINEA
				, CLA_INDICADOR
				) VALUES ( 
				".$consecutivo."
				, ".$surveyInstance->LongitudeIndID."
				)";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID > 0) {
			//Insertar indicador Accuracy
			$sql = "INSERT INTO SI_INDxLINEA ( 
					CLA_INDLINEA
					, CLA_INDICADOR
					) VALUES ( 
					".$consecutivo."
					, ".$surveyInstance->AccuracyIndID."
					)";
			if ($aRepository->ADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_INDxLINEA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		//@JAPR
		
		//Procedemos a exportar los indicadores en linea de acuerdo a las funciones indicadas por Edgar Arteaga
		$strOriginalWD = getcwd();

		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		
		//Llamamos a la funcion ExportOnlineIndicators
		ExportOnlineIndicators($aRepository);
		
		chdir($strOriginalWD);
		
		//Obtenemos el Id del indicador creado y lo almacenamos en la tabla SI_SV_Survey
		$actionIndInlineID = $consecutivo;
		
		$sql = "UPDATE SI_SV_Survey SET ActionIndInlineID = ".$actionIndInlineID." WHERE SurveyID = ".$aNewSurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	static function createNewIndicatorDuration($aRepository, $aNewSurveyID, $aNewModelID, $showMessage = true)
	{
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		//require_once("../model_manager/utilsEMonitoring.inc.php");
		global $generateXLS;
		$generateXLS = false;
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicator($aRepository);
		$anInstanceIndicator->ModelID = $aNewModelID;
		$anInstanceIndicator->oModel = BITAMModel::NewModelWithModelID($aRepository, $aNewModelID);
		$anInstanceIndicator->IndicatorName = 'Duration';
		$anInstanceIndicator->formato = "#,##0";
		$anInstanceIndicator->agrupador = "SUM";
		$anInstanceIndicator->sql_source = "SUM";

		$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
		$aDurationIndID = $anInstanceIndicator->IndicatorID;
		
		$sql = "UPDATE SI_SV_Survey SET DurationIndID = ".$aDurationIndID." WHERE SurveyID = ".$aNewSurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	static function createNewIndicatorCount($aRepository, $aNewSurveyID, $aNewModelID, $showMessage = true)
	{
		$strOriginalWD = getcwd();
		
		//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
		//para asignarlos a las variables de session que se ocupa en el model manager
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

		require_once("../model_manager/filescreator.inc.php");
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/indicator.inc.php");
		
		//Para obtener el fieldName se tiene que consultar el FactKeyDimID de la nueva encuesta
		$sql = "SELECT FactKeyDimID FROM SI_SV_Survey WHERE SurveyID = ".$aNewSurveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);

		if ($aRS === false || $aRS->EOF)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$factKeyDimID = (int)$aRS->fields["factkeydimid"];
		}

		$fieldName = "RIDIM_".$factKeyDimID."KEY";
		
		$anInstanceIndicator = BITAMIndicator::NewIndicator($aRepository);
		$anInstanceIndicator->CubeID = $aNewModelID;
		$anInstanceIndicator->IndicatorName = 'COUNT';
		$anInstanceIndicator->formula_bd = "COUNT(DISTINCT t1.".$fieldName.")";
		$anInstanceIndicator->formato = "#,##0";
		$anInstanceIndicator->is_custom = 1;
		$anInstanceIndicator->no_ejecutivo = 0;
		$anInstanceIndicator->save();
		
		chdir($strOriginalWD);
		
		//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
		$aCountIndID = $anInstanceIndicator->IndicatorID;
		
		$sql = "UPDATE SI_SV_Survey SET CountIndID = ".$aCountIndID." WHERE SurveyID = ".$aNewSurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	static function getCaptureTimes($aRepository, $aSurveyID)
	{
		$captureTimes = array();
		
		$strCaptureStartTime = "";
		$strCaptureEndTime = "";
		
		//Tiempos de Captura
		$sql = "SELECT CaptureStartTime, CaptureEndTime FROM SI_SV_Survey WHERE SurveyID = ".$aSurveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_EKT_Configuration ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$strCaptureStartTime = trim($aRS->fields["capturestarttime"]);
			
			if(trim($strCaptureStartTime)=="")
			{
				$strCaptureEndTime = "";
			}
			else 
			{
				$strCaptureEndTime = trim($aRS->fields["captureendtime"]);
			}
		}

		$captureTimes["CaptureStartTime"]=$strCaptureStartTime;
		$captureTimes["CaptureEndTime"]=$strCaptureEndTime;
		
		return $captureTimes;
	}
	
	static function isEnabledCaptureNow($aRepository, $aSurveyID)
	{
		$isEnabledCapture = true;
		
		$captureTimes = BITAMSurvey::getCaptureTimes($aRepository, $aSurveyID);
		
		//Si los tiempos de captura estan en blanco entonces se permite la captura libre de horario
		if(trim($captureTimes["CaptureStartTime"])=="" || trim($captureTimes["CaptureEndTime"])=="")
		{
			$isEnabledCapture = true;
		}
		else 
		{
			//Obtener la fecha actual
			$arrayTime = getdate();
			$currentHours = (int)$arrayTime["hours"];
			$currentMinutes = (int)$arrayTime["minutes"];
			
			//Tiempo en timestamp de la hora actual
			$currentTS = strtotime($currentHours.':'.$currentMinutes);
			//Tiempo en timestamp de la hora de inicio
			$beginTS = strtotime($captureTimes["CaptureStartTime"]);
			//Tiempo en timestamp de la hora de fin		
			$endTS = strtotime($captureTimes["CaptureEndTime"]);
			
			if($currentTS>=$beginTS && $currentTS<=$endTS)
			{
				$isEnabledCapture = true;
			}
			else 
			{
				$isEnabledCapture = false;
			}
		}
		
		return $isEnabledCapture;
	}
	
	static function isInUse($aRepository, $aSurveyID)
	{
		$isInUse = false;
		
		$aSurveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		
		$factTable = "RIFACT_".$aSurveyInstance->ModelID;
		$surveyTable = $aSurveyInstance->SurveyTable;
		
		//Verificar si hay registros en la tabla de hechos				
		$sql = "SELECT FactKey FROM ".$factTable;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$isInUse = true;
		}
		
		//Verificar si hay registros en la tabla paralela de encuesta				
		$sql = "SELECT DateID FROM ".$surveyTable;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." ".$surveyTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$isInUse = true;
		}
		
		return $isInUse;
	}
	
	//@JAPR 2015-11-26: Modificada la función para unificar ambos queries y permitir ordenar la lista correctamente
	/* Obtiene la lista de formas a las que tiene acceso un usuario basado en los Schedulers a los que se encuentra asociados, tanto directamente como por medio de los grupos de usuario
	a los que pertenezca
	Esta función fue rehabilitada para v6+, ya que en realidad se había dejado de utilizar desde hacía mucho tiempo, por lo mismo se renombró para estandarizar como estática
	Está función NO sigue el estándar de otras implementaciones en las cuales se regresa un objeto colección, y en caso de error hace un die o regresa una descripción/false según una
	variable global, simplemente regresa un array con la lista de elementos encontrados, o bien un array vacío en caso de error
	El parámetro $bApplySecurity permite indicar si se deberá o no aplicar la seguridad de acceso a las formas vía Schedulers, si no lo hace se regresarán todas las formas existentes
	*/
	static function GetSurveyList($aRepository, $userID, $bApplySecurity = true)
	{
		$activeStatus = array(0=>0, 1=>1);
		$arraySurveys = array();
		
		//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
		//En este caso como el ORDER BY es en base a posición, el SELECT ya debe contener el TRIM que para esta misma corrección se estaba aplicando al ORDER BY en otros queries
		if ($bApplySecurity) {
			//En este caso hay que aplicar seguridad de acceso tanto por Roles como vía Schedulers
			$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $userID);
			
			//Obtener el listado de encuestas disponibles para el usuario logueado x user
			//@JAPR 2017-01-10: Validado que no aparezcan formas de versiones previas
			$sql = "SELECT DISTINCT A.SurveyID, LTRIM(RTRIM(A.SurveyName)) AS SurveyName, A.CreationVersion 
				FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C 
				WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = ".$userID." 
					AND A.Status IN (".implode(",", $activeStatus).") ";
			//@JAPR 2015-11-26: Modificada la función para unificar ambos queries y permitir ordenar la lista correctamente
			if (count($roles)>0) {
				//Obtener el listado de encuestas disponibles para el usuario logueado x rol
				//@JAPR 2017-01-10: Validado que no aparezcan formas de versiones previas
				$sql .= "UNION 
					SELECT DISTINCT A.SurveyID, LTRIM(RTRIM(A.SurveyName)) AS SurveyName, A.CreationVersion 
					FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C 
					WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (".implode(",", $roles).") 
						AND A.Status IN (".implode(",", $activeStatus).")";
			}
		}
		else {
			//En este caso se desea la lista completa de formas
			//@JAPR 2017-01-10: Validado que no aparezcan formas de versiones previas
			$sql = "SELECT DISTINCT A.SurveyID, LTRIM(RTRIM(A.SurveyName)) AS SurveyName, A.CreationVersion 
				FROM SI_SV_Survey A 
				WHERE A.Status IN (".implode(",", $activeStatus).") ";
		}
		//@JAPR 2015-11-30: Corregido un bug, al haber un UNION no debe existir ORDER BY en base a nombres de columnas (#AZIWFC)
		$sql .= " ORDER BY 2, 1";
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			return $arraySurveys;
		}
		
		while (!$aRS->EOF) {
			$surveyID = (int) @$aRS->fields["surveyid"];
			//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
			$surveyName = trim((string) @$aRS->fields["surveyname"]);
			//@JAPR 2016-07-27: Validado que si no se grabó el nombre de forma, se genere uno automáticamente
			if ($surveyName == '') {
				$surveyName = "(NoNameSurvey)";
			}
			//@JAPR 2017-01-10: Validado que no aparezcan formas de versiones previas
			$dblCreationAdminVersion = (float) @$aRS->fields["creationversion"];
			if ($dblCreationAdminVersion >= esveFormsv6) {
				$arraySurveys[$surveyID] = $surveyName;
			}
			//@JAPR
			
			$aRS->MoveNext();
		}
		
		return $arraySurveys;
	}
	
	static function existAttribNameInThisSurvey($aRepository, $surveyID, $attributeName)
	{
		//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
		global $garrReservedDimNames;
		//@JAPR
		
		//Realizar una instancia de Survey para obtener el CLA_CONCEPTO del modelo
		$anInstanceSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
		$cla_concepto = $anInstanceSurvey->ModelID;
		
		$sql = "SELECT A.CLA_DESCRIP FROM SI_DESCRIP_ENC A, SI_CPTO_LLAVE B 
				WHERE A.CLA_DESCRIP = B.CLA_DESCRIP AND B.CLA_CONCEPTO = ".$cla_concepto." 
				AND A.NOM_LOGICO = ".$aRepository->ADOConnection->Quote($attributeName);
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_DESCRIP_ENC, SI_CPTO_LLAVE ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if($aRS->EOF)
		{
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			if (isset($garrReservedDimNames) && isset($garrReservedDimNames[strtolower($attributeName)])) {
				return ((int) array_search(strtolower($attributeName), array_keys($garrReservedDimNames))) +1;
			}
			else {
				return false;
			}
			//@JAPR
		}
		else 
		{
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			if (isset($garrReservedDimNames) && isset($garrReservedDimNames[strtolower($attributeName)])) {
				return ((int) array_search(strtolower($attributeName), array_keys($garrReservedDimNames))) +1;
			}
			//@JAPR
			
			return true;
		}
	}
	
	static function existCatalogQuestionInThisSurvey($aRepository, $surveyID, $catalogID)
	{
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$surveyID." AND UseCatalog = 1 AND CatalogID = ".$catalogID." AND QTypeID <> ".qtpOpenNumeric;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if($aRS->EOF)
		{
			return false;
		}
		else 
		{
			return true;
		}
	}
    
	//@JAPRDescontinuada en v6
	//A partir de v6 esta función quedó descontinuada, se dejará la referencia por si en algún proceso de compatibilidad se pensara reutilizar el servicio para definir formas viejas
	//La actualización de formas la realizará el DataSource directamente
	static function updateAllSurveyLastUserAndDateFromCatalog($aRepository, $catalogID)
	{
		$currentDate = date("Y-m-d H:i:s");
		
		//Obtengo los SurveyIDs de todas las encuestas que esten relacionadas con este catalogo
		$sql = "SELECT SurveyID FROM SI_SV_Survey WHERE CatalogID = ".$catalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$arraySurveyIDs = array();
			
			while(!$aRS->EOF)
			{
				$surveyID = (int)$aRS->fields["surveyid"];
				
				$arraySurveyIDs[$surveyID] = $surveyID;
				
				$aRS->MoveNext();
			}
			
			//Obtenemos los SurveyIDs de todas las preguntas que esten relacionados con este catalogo
			$sql = "SELECT SurveyID FROM SI_SV_Question WHERE QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$catalogID." GROUP BY SurveyID";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else 
			{
				while(!$aRS->EOF)
				{
					$surveyID = (int)$aRS->fields["surveyid"];
					
					$arraySurveyIDs[$surveyID] = $surveyID;
					
					$aRS->MoveNext();
				}
				
				foreach ($arraySurveyIDs as $aSurveyID)
				{
					$sql = "UPDATE SI_SV_Survey SET LastUserID = ".$_SESSION["PABITAM_UserID"].
							", LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
							//@JAPR 2014-03-25: Corregido un bug, no estaba actualizando la versión de las encuestas que usan este catalogo
							", VersionNum = ".$aRepository->DataADOConnection->IfNull("VersionNum", "0")." +1".
							//@JAPR
							" WHERE SurveyID = ".$aSurveyID;
					if ($aRepository->DataADOConnection->Execute($sql) === false)
					{
						//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
		}
	}
	
	/* Actualiza la versión de las formas que utilizan el DataSource que se indica
	Se debe invocar cada vez que cambie algún elemento o configuración de un DataSource
	*/
	static function updateAllSurveyLastUserAndDateFromDataSource($aRepository, $aDataSourceID) {
		$currentDate = date("Y-m-d H:i:s");
		
		//Obtengo los SurveyIDs de todas las encuestas que esten relacionadas con este catalogo
		$arraySurveyIDs = array();
		$sql = "SELECT DISTINCT SurveyID 
			FROM SI_SV_SECTION 
			WHERE ValuesSourceType = ".tofCatalog." AND DataSourceID = {$aDataSourceID} 
			UNION 
			SELECT DISTINCT SurveyID 
			FROM SI_SV_Question 
			WHERE ValuesSourceType = ".tofCatalog." AND DataSourceID = {$aDataSourceID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$surveyID = (int) @$aRS->fields["surveyid"];
				if ($surveyID > 0) {
					$arraySurveyIDs[$surveyID] = $surveyID;
				}
				$aRS->MoveNext();
			}
		}
		
		if (count($arraySurveyIDs) > 0) {
			foreach ($arraySurveyIDs as $aSurveyID) {
				$sql = "UPDATE SI_SV_Survey SET LastUserID = ".$_SESSION["PABITAM_UserID"].
						", LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate).
						", VersionNum = ".$aRepository->DataADOConnection->IfNull("VersionNum", "0")." +1".
					" WHERE SurveyID = ".$aSurveyID;
				if ($aRepository->DataADOConnection->Execute($sql) === false) {
				}
			}
		}
	}
	
	//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
	/* Dado el ID de la encuesta, genera los Arrays globales necesarios para el proceso de agregaciones incrementales basado en la definición de
	la encuesta. Este método se debe invocar tanto en el momento del grabado (incluyendo edición) como en el momento del borrado de las capturas
	de encuestas
	*/
	static function PrepareIncrementalAggregationsArrays($aRepository, $aSurveyID, $bClearArrays = false) {
		global $arrDateValues;
		global $arrDimKeyValues;
		global $arrDimDescValues;
		global $arrIndicValues;
		global $arrAllDimensions;
		global $arrIndicFields;
		global $strIndicFieldsArrayCode;
		global $gbEnableIncrementalAggregations;
		global $gbTestIncrementalAggregationsData;
		global $intComboID;
		global $arrDefDimValues;
		
		if (!isset($arrDateValues) || $bClearArrays) {
			$arrDateValues = array();
		}
		if (!isset($arrDimKeyValues) || $bClearArrays) {
			$arrDimKeyValues = array();
		}
		if (!isset($arrDimDescValues) || $bClearArrays) {
			$arrDimDescValues = array();
		}
		if (!isset($arrIndicValues) || $bClearArrays) {
			$arrIndicValues = array();
		}
		if (!isset($arrAllDimensions) || $bClearArrays) {
			$arrAllDimensions = array();
		}
		if (!isset($arrIndicFields) || $bClearArrays) {
			$arrIndicFields = array();
		}
		if (!isset($intComboID) || $bClearArrays) {
			$intComboID = 0;
		}
		if (!isset($strIndicFieldsArrayCode) || $bClearArrays) {
			$strIndicFieldsArrayCode = '';
		}
		//@JAPR 2014-01-10: Corregido un bug, este array no se debe limpiar con la variable $bClearArrays
		if (!isset($arrDefDimValues)) {
			$arrDefDimValues = array();
		}
		//@JAPR
		
		$strOriginalWD = getcwd();
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("BITAM_UserID");
		//session_register("BITAM_UserName");
		//session_register("BITAM_RepositoryName");
		$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
		$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
		$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		require_once("../model_manager/model.inc.php");
		require_once("../model_manager/modeldata.inc.php");
		require_once("../model_manager/modeldimension.inc.php");
		require_once("../model_manager/indicatorkpi.inc.php");
		
		//Se tiene que generar un array con los nombre de los indicadores de la encuesta que son dependientes de las preguntas, para permitir
		//llenarlo mientras se procesa cada registro de la encuesta
		$strIndicFieldsArrayCode = '';
		$strIndicatorList = '';
		
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$sectionCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
		$arrSectionsByID = array();
		$arrHasMultiChoiceQuestions = array();
		
		$dynamicSectionID = 0;
		$dynamicSectionCatID = 0;
		foreach ($sectionCollection as $aSection) {
			$arrSectionsByID[$aSection->SectionID] = $aSection;
			$arrHasMultiChoiceQuestions[$aSection->SectionID] = false;
			if ($aSection->SectionType == sectDynamic) {
				$dynamicSectionID = $aSection->SectionID;
				$dynamicSectionCatID = $aSection->CatalogID;
			}
		}
		
		$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		$arrQuestionsByID = array();
		foreach ($questionCollection as $aQuestion) {
			$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion;
			
			//Agrega la bandera de preguntas múltiples en secciones dinámicas
			if ($aQuestion->QTypeID == qtpMulti) {
				$objSection = @$arrSectionsByID[$aQuestion->SectionID];
				if (!is_null($objSection)) {
					$arrHasMultiChoiceQuestions[$objSection->SectionID] = true;
				}
			}
		}
		
		//Agrega las dimensiones default de la encuesta
		if ($surveyInstance->SyncDateDimID > 0) {
			$aSyncDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncDateDimID);
			$arrAllDimensions[] = $aSyncDateInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);
		}
		
		if ($surveyInstance->SyncTimeDimID > 0) {
			$aSyncTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SyncTimeDimID);
			$arrAllDimensions[] = $aSyncTimeInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);
		}
		
		$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->UserDimID);
		$arrAllDimensions[] = $userDimension->Dimension->DimensionID;
		chdir($strOriginalWD);

		//@JAPR 2014-11-19: Agregada la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID && $surveyInstance->AgendaDimID > 0) {
			$AgendaDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->AgendaDimID);
			if (!is_null($AgendaDimension)) {
				$arrAllDimensions[] = $AgendaDimension->Dimension->DimensionID;
			}
			chdir($strOriginalWD);
		}
		//@JAPR
		$EMailDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EmailDimID);
		$arrAllDimensions[] = $EMailDimension->Dimension->DimensionID;
		chdir($strOriginalWD);

		$StatusDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StatusDimID);
		$arrAllDimensions[] = $StatusDimension->Dimension->DimensionID;
		chdir($strOriginalWD);

		$SchedulerDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->SchedulerDimID);
		$arrAllDimensions[] = $SchedulerDimension->Dimension->DimensionID;
		chdir($strOriginalWD);

		$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartDateDimID);
		$arrAllDimensions[] = $aStartDateInstanceDim->Dimension->DimensionID;
		chdir($strOriginalWD);

		$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndDateDimID);
		$arrAllDimensions[] = $anEndDateInstanceDim->Dimension->DimensionID;
		chdir($strOriginalWD);

		$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->StartTimeDimID);
		$arrAllDimensions[] = $aStartTimeInstanceDim->Dimension->DimensionID;
		chdir($strOriginalWD);

		$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->EndTimeDimID);
		$arrAllDimensions[] = $anEndTimeInstanceDim->Dimension->DimensionID;
		chdir($strOriginalWD);
		
		if(getAppVersion() >= esvServerDateTime && 
				$surveyInstance->ServerStartDateDimID > 0 && $surveyInstance->ServerEndDateDimID > 0 && $surveyInstance->ServerStartTimeDimID > 0 && $surveyInstance->ServerEndTimeDimID > 0) {
			$aServerStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartDateDimID);
			$arrAllDimensions[] = $aServerStartDateInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);

			$aServerEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndDateDimID);
			$arrAllDimensions[] = $aServerEndDateInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);

			$aServerStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerStartTimeDimID);
			$arrAllDimensions[] = $aServerStartTimeInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);

			$aServerEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->ServerEndTimeDimID);
			$arrAllDimensions[] = $aServerEndTimeInstanceDim->Dimension->DimensionID;
			chdir($strOriginalWD);
		}
		
		//Agrega los indicadores Default de la encuesta
		//Obtenemos instancia del indicador AnsweredQuestions
		$answeredQuestionsIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AnsweredQuestionsIndID);
		$indicatorFieldName = (string) @$answeredQuestionsIndicator->field_name.$answeredQuestionsIndicator->IndicatorID;
		$strIndicatorList .= ", '".$indicatorFieldName."' => ".((false)?'NULL':'0');
		$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
		chdir($strOriginalWD);
		
		//Obtenemos instancia del indicador Duration
		$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->DurationIndID);
		$indicatorFieldName = (string) @$durationIndicator->field_name.$durationIndicator->IndicatorID;
		$strIndicatorList .= ", '".$indicatorFieldName."' => ".((false)?'NULL':'0');
		$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
		chdir($strOriginalWD);
		
		//Obtenemos instancia del indicador Latitude y Longitude
		$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LatitudeIndID);
		$indicatorFieldName = (string) @$latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
		$strIndicatorList .= ", '".$indicatorFieldName."' => ".((false)?'NULL':'0');
		$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
		
		$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LongitudeIndID);
		$indicatorFieldName = (string) @$longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;
		$strIndicatorList .= ", '".$indicatorFieldName."' => ".((false)?'NULL':'0');
		$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
		chdir($strOriginalWD);
		
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues && $surveyInstance->AccuracyIndID > 0) {
			$accuracyIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->AccuracyIndID);
			if (!is_null($accuracyIndicator)) {
				$indicatorFieldName = (string) @$accuracyIndicator->field_name.$accuracyIndicator->IndicatorID;
				$strIndicatorList .= ", '".$indicatorFieldName."' => ".((false)?'NULL':'0');
				$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
			}
			chdir($strOriginalWD);
		}
		//@JAPR
		
		//Agrega los indicadores y dimensiones basadas en preguntas según su tipo
		//Se genera instancia de coleccion de preguntas dado un SurveyID pero se ignoraran los datos de tipo 3 Seleccion Multiple
		$blnDynamicFound = false;
		$arrMaxAttribUsedByCatalog = array();
		$anArrayTypes = array();
		$anArrayTypes[0] = qtpMulti;
		$dspMatrix = dspMatrix;
		$questionCollection = BITAMQuestionCollection::NewInstanceWithExceptTypeQDisplayMode($aRepository, $aSurveyID, $anArrayTypes, $dspMatrix);
		foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
			$arrayValues = array();
			
			//Las preguntas de Foto y Firma no graban una respuesta, sólo la imagen, por lo tanto se salta esta pregunta completamente
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($aQuestion->QTypeID) {
				case qtpPhoto:
				case qtpDocument:
				case qtpSignature:
				case qtpSkipSection:
				case qtpMessage:
				case qtpSync:
				case qtpPassword:
				case qtpAudio:
				case qtpVideo:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			elseif ($aQuestion->QTypeID == qtpGPS) {
				//Se deben agregar tanto la dimensión base como los 3 atributos asociados
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				if (!is_null($anInstanceDimension)) {
					$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
				}
				chdir($strOriginalWD);
				
				//Atributo Latitude
				if ($aQuestion->LatitudeAttribID > 0) {
					$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($aRepository, $aQuestion->ModelID, $aQuestion->IndDimID, -1, $aQuestion->LatitudeAttribID);
					if (!is_null($anInstanceAttrib)) {
						$arrAllDimensions[] = $anInstanceAttrib->Dimension->DimensionID;
					}
					chdir($strOriginalWD);
				}
				
				//Atributo Longitude
				if ($aQuestion->LongitudeAttribID > 0) {
					$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($aRepository, $aQuestion->ModelID, $aQuestion->IndDimID, -1, $aQuestion->LongitudeAttribID);
					if (!is_null($anInstanceAttrib)) {
						$arrAllDimensions[] = $anInstanceAttrib->Dimension->DimensionID;
					}
					chdir($strOriginalWD);
				}
				
				//Atributo Accuracy
				if ($aQuestion->AccuracyAttribID > 0) {
					$anInstanceAttrib = BITAMAttribDimension::NewAttribDimensionWithDimensionID($aRepository, $aQuestion->ModelID, $aQuestion->IndDimID, -1, $aQuestion->AccuracyAttribID);
					if (!is_null($anInstanceAttrib)) {
						$arrAllDimensions[] = $anInstanceAttrib->Dimension->DimensionID;
					}
					chdir($strOriginalWD);
				}
			}
			//@JAPR
			//@JAPR 2012-05-17: Agregadas las preguntas tipo Action
			//La pregunta tipo Action no generan indicadores, por lo tanto se omite
			else if ($aQuestion->QTypeID == qtpAction) {
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				if (!is_null($anInstanceDimension)) {
					$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
				}
				chdir($strOriginalWD);
			}
			else if ($aQuestion->QTypeID == qtpOpenNumeric || $aQuestion->QTypeID == qtpCalc) {
				if ($aQuestion->IsIndicator == 1) {
					$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
					if (!is_null($anInstanceIndicator)) {
						$indicatorFieldName = (string) @$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
						$strIndicatorList .= ", '".$indicatorFieldName."' => ".(($surveyInstance->UseNULLForEmptyNumbers)?'NULL':'0');
						$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
					}
					chdir($strOriginalWD);
				}
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
				if (!is_null($anInstanceDimension)) {
					$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
				}
				chdir($strOriginalWD);
			}
			else {
				if ($aQuestion->QTypeID == qtpSingle || $aQuestion->QTypeID == qtpCallList) {
					//@JAPR 2013-01-23: Corregido un bug, finalmente si era correcto que repitiera todas las preguntas en todos los registros, excepto en el
					//caso de preguntas numéricas ya que tentativamente esas son las únicas que generan indicadores y por tanto son las que al acumular afectaría
					//al resultado si viniera repetido, por tanto se cambió la validación para solo omitir dichas preguntas
					if (($aQuestion->QTypeID == qtpSingle) && $aQuestion->UseCatalog == 0 && $aQuestion->IndicatorID > 0) {
						$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
						if (!is_null($anInstanceIndicator)) {
							$indicatorFieldName = (string) @$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
							$strIndicatorList .= ", '".$indicatorFieldName."' => ".(($surveyInstance->UseNULLForEmptyNumbers)?'NULL':'0');
							$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
						}
						chdir($strOriginalWD);
					}
				}
				
				//En el caso de las preguntas Simple choice de catálogo, sólo puede procesar directamente la dimensión si no se tiene activada
				//la opción para desasociar los catálogos, ya que de lo contrario hay múltiples dimensiones para el catálogo hasta el máximo de
				//atributos usado. Cualquier otro tipo de pregunta tiene sólo una dimensión así que se agrega directa
				if ($surveyInstance->DissociateCatDimens && $aQuestion->QTypeID == qtpSingle && $aQuestion->UseCatalog == 1) {
					//En este caso debe agregar tantas dimensiones como atributos sean usados de este catálogo, como el grabado se hace siguiendo
					//la secuencia de las preguntas, pero pudiera haber múltiples preguntas del mismo catálogo en posiciones no consecutivas,
					//con lo cual pudieran mezclarse con otras preguntas que tienen dimensiones, se optará por procesar todas las dimensiones del
					//mismo catálogo al final, manteniendo sólo el orden en que aparecieron los catálogos.
					//Además de lo anterior, cuando el catálogo es igual al de la sección dinámica, se debe concatenar al valor de la última
					//pregunta de dicho catálogo, los valores de la página y opción dinámica que corresponde con el registro que se está procesando.
					//Cuando la sección que graba el registro no corresponde con la sección dinámica del catálogo, o bien no se contestó el total
					//de atributos configurados en preguntas, se debe llenar el resto de los atributos con el valor de NA
					if (!isset($arrMaxAttribUsedByCatalog[$aQuestion->CatalogID])) {
						$arrMaxAttribUsedByCatalog[$aQuestion->CatalogID] = $aQuestion->CatMemberID;
					}
					
					//@JAPR 2013-11-13: Corregido un bug, se estaba usando $aSection en lugar de $aQuestion para cargar la instancia de sección
					$aSection = @$arrSectionsByID[$aQuestion->SectionID];
					if (!is_null($aSection) && $aQuestion->CatalogID == $dynamicSectionCatID) {
						//Es una sección dinámica, se verifica contra el último atributo usado que es el de ella misma o el de su pregunta 
						//múltiple choice si tiene alguna
						//Sólo realiza esta comprobación la primera vez, sin importar cuantas preguntas del mismo catálogo existan
						$aSection = @$arrSectionsByID[$dynamicSectionID];
						if (!$blnDynamicFound && !is_null($aSection)) {
							if (@$arrHasMultiChoiceQuestions[$aSection->SectionID]) {
								$arrMaxAttribUsedByCatalog[$aQuestion->CatalogID] = $aSection->ChildCatMemberID;
							}
							else {
								$arrMaxAttribUsedByCatalog[$aQuestion->CatalogID] = $aSection->CatMemberID;
							}
						}
						$blnDynamicFound = true;
					}
					else {
						//Es una sección Estándar o Maestro-detalle, así que simplemente se actualiza al último atributo usado por cualquiera de
						//las preguntas de dicha sección
						$arrMaxAttribUsedByCatalog[$aQuestion->CatalogID] = $aQuestion->CatMemberID;
					}
				}
				else {
					$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
					if (!is_null($anInstanceDimension)) {
						$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
					}
					chdir($strOriginalWD);
				}
			}
		}
		
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q pertenezcan a secciones dinamicas y esten dentro de ellas
		$uniqueMultiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceUniqueDim($aRepository, $aSurveyID);
		foreach ($uniqueMultiChoiceQuestionCollection->Collection as $questionKey => $aQuestion) {
			$arrayValues = array();
			
			//Esto aplica para las preguntas multiple choice con numeros o textos
			//entonces lo procesamos como dimensiones unicas
			if($aQuestion->MCInputType == mpcNumeric && $aQuestion->IsIndicatorMC == 1) {
				$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $aQuestion->IndicatorID);
				if (!is_null($anInstanceIndicator)) {
					$indicatorFieldName = (string) @$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
					$strIndicatorList .= ", '".$indicatorFieldName."' => ".(($surveyInstance->UseNULLForEmptyNumbers)?'NULL':'0');
					$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
				}
				chdir($strOriginalWD);
			}
			
			$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $aQuestion->IndDimID);
			if (!is_null($anInstanceDimension)) {
				$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
			}
			chdir($strOriginalWD);
		}
		
		//Generamos una coleccion de solo las preguntas que sean de selección múltiple 
		//y q no pertenezcan a dimensiones dinamicas o bien esten fuera de ellas
		$multiChoiceQuestionCollection = BITAMQuestionCollection::NewInstanceMultiChoiceDims($aRepository, $aSurveyID);
		foreach ($multiChoiceQuestionCollection->Collection as $questionKey => $aQuestion) {
			//Obtenemos las instancias de las dimensiones de las opciones
			foreach ($aQuestion->QIndDimIds as $qKey => $qDimID) {
				if ($aQuestion->MCInputType == mpcCheckBox ) {
					$tmpInd = (int)$aQuestion->QIndicatorIds[$qKey];
					if($tmpInd != 0) {
						$anInstanceIndicator = @BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $tmpInd);
						if (!is_null($anInstanceIndicator)) {
							$indicatorFieldName = (string) @$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
							$strIndicatorList .= ", '".$indicatorFieldName."' => ".(($surveyInstance->UseNULLForEmptyNumbers)?'NULL':'0');
							$arrIndicFields[$indicatorFieldName] = $indicatorFieldName;
						}
						chdir($strOriginalWD);
					}
				}
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $aQuestion->ModelID, -1, $qDimID);
				if (!is_null($anInstanceDimension)) {
					$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
				}
				chdir($strOriginalWD);
			}
		}
		
		//Agrega al final todas las dimensiones de las preguntas de catálogo
		foreach ($arrMaxAttribUsedByCatalog as $intCatalogID => $intCatMemberID) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
			if (is_null($objCatalog)) {
				continue;
			}
			
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $intCatalogID);
			if (is_null($objCatalogMembersColl)) {
				continue;
			}
			
			//Identifica las dimensiones de los atributos usados para este catálogo
			$arrAttributeIDs = array();
			foreach ($objCatalogMembersColl->Collection as $intIndex => $objCatalogMember) {
				$intDimID = (int) @$objCatalogMember->IndDimID;
				if ($intDimID <= 0) {
					break;
				}
				
				$anInstanceDimension = @BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $intDimID);
				if (!is_null($anInstanceDimension)) {
					$arrAllDimensions[] = $anInstanceDimension->Dimension->DimensionID;
				}
				chdir($strOriginalWD);
				
				//Cuando se llegó al último atributo usado debe salir del ciclo pero primero agrega su dimensión
				if ($objCatalogMember->MemberID == $intCatMemberID) {
					break;
				}
			}
		}
		
		//Agrega la dimensión con el ID único de la encuesta al final, ya que el ID se obtiene justo antes del INSERT así que es lo último que
		//se va a calcular durante el grabado
		$FactKeyDimValInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->ModelID, -1, $surveyInstance->FactKeyDimID);
		$arrAllDimensions[] = $FactKeyDimValInstanceDim->Dimension->DimensionID;
		chdir($strOriginalWD);
		
		if ($strIndicatorList != '') {
			$strIndicatorList = substr($strIndicatorList, 2);
			$strIndicFieldsArrayCode = '$arrRowIndicValues = @array('.$strIndicatorList.');';
		}
	}
	//@JAPR
	
	//@JAPR 2016-01-08: Validado que antes de eliminar una forma se verifique si está o no siendo usada por una Agenda/Scheduler de Agenda, para que
	//en ese caso no se permita eliminarla (#AU6V5R)
	/* Función de validación del tipo SearchVariablePatternInMetadata que regresará aquellas agendas o Schedulers de Agendas que siendo aún válidos (es decir, en
	el caso de Agendas, aquellas que aún no son del día actual o posterior, y en el caso de Schedulers de Agendas cualquiera existentes) contienen alguna referencia
	a cualquiera de las formas que se indican en el parámetro de IDs, ya que esas son agendas/schedulers que quedarían inválidos o mal definidos si se elimnan
	las formas en cuestión
	*/
	static function GetAgendasAndSchedulersWithSurveyIDs($aRepository, $anArrayOfSurveyIDs) {
	}
	
	/*  Esta función recibe un ID de forma de desarrollo para cargar su definición completa y hacer los ajustes necesarios para sobrescribir la forma de producción correspondiente,
	eliminando las secciones, preguntas, etc. que ya no existen, agregando las nuevas y actualizando las que aún permanecen, respectivamente en ese orden para no afectar de alguna
	manera irreparable a la forma. El proceso se realizará cargando las instancias correspondientes para el borrado de tal manera que los métodos ya probados entren en funcionamiento,
	mientras que para la creación se forzará a usar la instancia del objeto de desarrollo ajustando los IDs de tal manera que se realice sobre la forma de producción, finalmente la
	actualización también se realizará sobre el objeto de desarrollo ajustando los IDs. Al final del proceso se requiere un ajuste masivo de IDs, por lo que el proceso conservará
	memoria de todos los IDs que acaba de agregar/cambiar para poder hacer los reemplazos correspondientes de las propiedades que estuvieran apuntando a los IDs originales de desarrollo
	*/
	static function BuildSurvey($aRepository, $aSurveyID) {
		global $blnSaveLogStringFile;
		
		$strCurrentDate = date('Y-m-d H:i:s');
		$strTimeStamp = str_replace(array('-', ':', ' '), '', $strCurrentDate);
		$strFileName = "FormBuild_{$aSurveyID}_{$strTimeStamp}.log";
		ob_start();
		$blnSaveLogStringFile = true;
		$intSurveyID = 0;
		$intProductionFormVersionNum = 0;
		try {
			$strErrorDesc = '';
			ECHOString("BuildSurvey {$aSurveyID}", 1, 0, "color:purple;");
			
			//Primero carga las instancias de las formas involucradas para confirmar que se trata de una forma de desarrollo que tiene a una forma de producción correctamente asociada
			//La forma de desarrollo desde la que se copiaran las definiciones
			$objDevSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
			if (is_null($objDevSurvey)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." # {$aSurveyID})");
			}
			
			if ($objDevSurvey->FormType != formDevelopment || $objDevSurvey->SourceID <= 0) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." # {$aSurveyID})");
			}
			
			//La forma de producción a la que se copiarán las definiciones
			$intSurveyID = $objDevSurvey->SourceID;
			$objProdSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
			if (is_null($objProdSurvey)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." # {$intSurveyID})");
			}
			
			//Ambas formas deben estar correctamente ligadas para continuar
			if ($objProdSurvey->FormType != formProdWithDev || $objProdSurvey->SourceID != $aSurveyID) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." # {$intSurveyID})");
			}
			//Es importante almacenar la versión de producción porque con cada cambio realizado se estará actualizando dicho valor, así que se debe forzar a utilizar este valor
			//al terminar el proceso de sincronización para que no se incremente desproporcionalmente sino en una únidad exclusivamente (NO se validará si hubo o no realmente cambios,
			//eso conllevaría a demasiadas validaciones, simplemente se asumirá que al terminar el proceso la forma debe considerarse modificada una vez, en caso de error se restaurará
			//la versión preasignada para que por lo menos quienes ya habían descargado definiciones continuen sin necesitar actualizarlas a una que pudo quedar dañada)
			$intProductionFormVersionNum = $objProdSurvey->VersionNum;
			
			//Carga todos los elementos de ambas formas
			//Forma de desarrollo
			$objDevSectionCollection = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objDevSectionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Sections")." # {$aSurveyID})");
			}
			$objDevQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (is_null($objDevQuestionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Questions")." # {$aSurveyID})");
			}
			
			//Forma de producción
			$objProdSectionCollection = BITAMSectionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_null($objProdSectionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Sections")." # {$intSurveyID})");
			}
			$objProdQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_null($objProdQuestionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Questions")." # {$intSurveyID})");
			}
			
			//Genera arrays con las referencias de todos los objetos actuales de la forma de desarrollo para poder utilizandos mas adelante durante la actualización de datos
			$arrDevSectionsByID = array();
			$arrDevQuestionsByID = array();
			$arrDevQOptionsByID = array();
			$arrDevQOptionsByQuestionID = array();
			//Genera los arrays de mapeos entre ambas formas, el índice corresponde al ID en desarrollo y valor al ID en producción. En el Array de desarrollo a producción están todos los
			//objetos, pero en el de producción a desarrollo sólo aquellos que aún existen en desarrollo
			$arrMappedDevSections = array();
			$arrMappedDevQuestions = array();
			$arrMappedDevQuestionOptions = array();
			$arrMappedProdSections = array();
			$arrMappedProdQuestions = array();
			$arrMappedProdQuestionOptions = array();
			foreach ($objDevSectionCollection->Collection as $objSection) {
				$arrDevSectionsByID[$objSection->SectionID] = $objSection;
				if ($objSection->SourceID > 0) {
					$arrMappedDevSections[$objSection->SectionID] = $objSection->SourceID;
					$arrMappedProdSections[$objSection->SourceID] = $objSection->SectionID;
				}
			}
			
			foreach ($objDevQuestionCollection->Collection as $objQuestion) {
				$arrDevQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
				$arrDevQOptionsByQuestionID[$objQuestion->QuestionID] = array();
				if ($objQuestion->SourceID > 0) {
					$arrMappedDevQuestions[$objQuestion->QuestionID] = $objQuestion->SourceID;
					$arrMappedProdQuestions[$objQuestion->SourceID] = $objQuestion->QuestionID;
				}
				
				//Carga las opciones de respuesta si es un tipo de pregunta que las requiere
				if ($objQuestion->QTypeID == qtpSingle || $objQuestion->QTypeID == qtpMulti || $objQuestion->QTypeID == qtpCallList || $objQuestion->QTypeID == qtpUpdateDest) {
					//Las opciones de respuesta se cargan pregunta a pregunta solamente
					$objDevQuestionOptionsCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $objQuestion->QuestionID);
					if (is_null($objDevQuestionOptionsCollection)) {
						throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Options")." # {$aSurveyID} - {$objQuestion->QuestionID})");
					}
					
					foreach ($objDevQuestionOptionsCollection as $objQuestionOption) {
						$arrDevQOptionsByID[$objQuestionOption->QuestionOptionID] = $objQuestionOption;
						$arrDevQOptionsByQuestionID[$objQuestion->QuestionID][$objQuestionOption->QuestionOptionID] = $objQuestionOption;
						if ($objQuestionOption->SourceID > 0) {
							$arrMappedDevQuestionOptions[$objQuestionOption->QuestionOptionID] = $objQuestionOption->SourceID;
							$arrMappedProdQuestionOptions[$objQuestionOption->SourceID] = $objQuestionOption->QuestionOptionID;
						}
					}
				}
			}
			
			//Verifica si hay elementos que ya no se encuentren en la forma de desarrollo para eliminarlos directamente de la forma de producción
			//El proceso se realiza primero por secciones, ya que al eliminar una sección automáticamente eliminaría todas las preguntas que le pertenecen, así que no hay necesidad
			//posteriormente de eliminarlas una a una, pero contabiliza las secciones eliminadas para evitar recorrer dichas preguntas
			//Adicionalment generará un array con las referencias a los objetos de producción para utilizarlos posteriormente al actualizar sus datos
			$arrProdSectionsByID = array();
			$arrProdQuestionsByID = array();
			$arrProdQOptionsByID = array();
			$arrProdQOptionsByQuestionID = array();
			$arrRemovedProdSections = array();
			$arrRemovedProdSectionsObjs = array();
			$arrRemovedProdQuestions = array();
			$arrRemovedProdQuestionsObjs = array();
			$arrRemovedProdQuestionOptions = array();
			$arrRemovedProdQuestionOptionsObjs = array();
			foreach ($objProdSectionCollection->Collection as $objSection) {
				//Si la sección ya no existe en desarrollo, deberá eliminarla de producción
				if (!isset($arrMappedProdSections[$objSection->SectionID])) {
					//Eliminará todos los posibles hijos, así que se marca para ignorarlos cuando se estén recorriendo mas adelante
					$arrRemovedProdSections[$objSection->SectionID] = $objSection->SectionID;
					$arrRemovedProdSectionsObjs[$objSection->SectionID] = $objSection;
					//$objSection->remove();
				}
				else {
					//Es una sección que aún existe, así que la agrega al array de producción
					$arrProdSectionsByID[$objSection->SectionID] = $objSection;
				}
			}
			
			foreach ($objProdQuestionCollection as $objQuestion) {
				//Sólo son válidas las preguntas que no sean hijas de las secciones eliminadas en este mismo proceso
				if (isset($arrRemovedProdSections[$objQuestion->SectionID])) {
					//Agrega la pregunta a la lista de preguntas eliminadas aunque no se hubiera eliminado de manera directa sino a través de la sección a la que pertenecía
					$arrRemovedProdQuestions[$objQuestion->QuestionID] = $objQuestion->QuestionID;
					//$arrRemovedProdQuestionsObjs[$objQuestion->QuestionID] = $objQuestion;
					continue;
				}
				
				//Si la pregunta ya no existe en desarrollo, deberá eliminarla de producción
				if (!isset($arrMappedProdQuestions[$objQuestion->QuestionID])) {
					//Eliminará todos los posibles hijos, así que se marca para ignorarlos cuando se estén recorriendo mas adelante
					$arrRemovedProdQuestions[$objQuestion->QuestionID] = $objQuestion->QuestionID;
					$arrRemovedProdQuestionsObjs[$objQuestion->QuestionID] = $objQuestion;
					//$objQuestion->remove();
					
					//No tiene caso continuar con las opciones de respuesta pues también ya habrían sido eliminadas
					continue;
				}
				else {
					//Es una pregunta que aún existe, así que la agrega al array de producción
					$arrProdQuestionsByID[$objQuestion->QuestionID] = $objQuestion;
					$arrProdQOptionsByQuestionID[$objQuestion->QuestionID] = array();
				}
				
				//Carga las opciones de respuesta si es un tipo de pregunta que las requiere
				if ($objQuestion->QTypeID == qtpSingle || $objQuestion->QTypeID == qtpMulti || $objQuestion->QTypeID == qtpCallList || $objQuestion->QTypeID == qtpUpdateDest) {
					//Las opciones de respuesta se cargan pregunta a pregunta solamente
					$objProdQuestionOptionsCollection = BITAMQuestionOptionCollection::NewInstance($aRepository, $objQuestion->QuestionID);
					if (is_null($objProdQuestionOptionsCollection)) {
						throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Options")." # {$intSurveyID} - {$objQuestion->QuestionID})");
					}
					
					foreach ($objProdQuestionOptionsCollection as $objQuestionOption) {
						//Si la opción de respuesta ya no existe en desarrollo, deberá eliminarla de producción
						if (!isset($arrMappedProdQuestionOptions[$objQuestionOption->QuestionOptionID])) {
							$arrRemovedProdQuestionOptions[$objQuestionOption->QuestionOptionID] = $objQuestionOption->QuestionOptionID;
							$arrRemovedProdQuestionOptions[$objQuestionOption->QuestionOptionID] = $objQuestionOption;
							//$objQuestionOption->remove();
						}
						else {
							//Es una opción de respuesta que aún existe, así que la agrega al array de producción
							$arrProdQOptionsByID[$objQuestionOption->QuestionOptionID] = $objQuestionOption;
							$arrProdQOptionsByQuestionID[$objQuestion->QuestionID][$objQuestionOption->QuestionOptionID] = $objQuestionOption;
						}
					}
				}
			}
			
			//************************************************************************************************************************************************
			//En este punto ya se tienen marcados todos los objetos que ya no existen en desarrollo, además de tener generados todos los arrays para iniciar
			//el proceso completo de construcción de la forma de producción
			ECHOString("Printing data arrays", 1, 0, "color:purple;");
			ECHOString("arrRemovedProdSections", 2, 0, "color:green;");
			PrintMultiArray($arrRemovedProdSections);
			ECHOString("arrRemovedProdQuestions", 2, 0, "color:green;");
			PrintMultiArray($arrRemovedProdQuestions);
			ECHOString("arrRemovedProdQuestionOptions", 2, 0, "color:green;");
			PrintMultiArray($arrRemovedProdQuestionOptions);
			
			ECHOString("arrMappedDevSections", 2, 0, "color:green;");
			PrintMultiArray($arrMappedDevSections);
			ECHOString("arrMappedDevQuestions", 2, 0, "color:green;");
			PrintMultiArray($arrMappedDevQuestions);
			ECHOString("arrMappedDevQuestionOptions", 2, 0, "color:green;");
			PrintMultiArray($arrMappedDevQuestionOptions);
			
			ECHOString("arrMappedProdSections", 2, 0, "color:green;");
			PrintMultiArray($arrMappedProdSections);
			ECHOString("arrMappedProdQuestions", 2, 0, "color:green;");
			PrintMultiArray($arrMappedProdQuestions);
			ECHOString("arrMappedProdQuestionOptions", 2, 0, "color:green;");
			PrintMultiArray($arrMappedProdQuestionOptions);
			
			//************************************************************************************************************************************************
			//FASE #1: Eliminación
			//Inicia el borrado de los objetos eliminados en desarrollo, este cambio no tiene consecuencias excepto la posibilidad de que los objetos restantes tuvieran referencias
			//a los eliminados, lo cual se solucionará con la fase de actualización
			ECHOString("Deleting objects from production form", 3, 0, "color:purple;");
			
			//Primero elimina las secciones, lo cual automáticamente eliminará todas las preguntas que pertenecen a ellas
			ECHOString("Deleting sections", 2, 0, "color:purple;");
			foreach ($arrRemovedProdSectionsObjs as $intSectionID => $objSection) {
				ECHOString("Deleting section {$intSectionID}", 1, 0);
				$objSection->remove();
			}
			
			//Posteriormente elimina las preguntas, lo cual automáticamente eliminará todas las opciones de respuesta que le pertenecen a ellas
			ECHOString("Deleting questions", 2, 0, "color:purple;");
			foreach ($arrRemovedProdQuestionsObjs as $intQuestionID => $objQuestion) {
				ECHOString("Deleting question {$intQuestionID}", 1, 0);
				$objQuestion->remove();
			}
			
			//Finalmente si aún quedaran opciones de respuesta eliminadas individualmente, las elimina para concluir con este paso
			ECHOString("Deleting question options", 2, 0, "color:purple;");
			foreach ($arrRemovedProdQuestionOptionsObjs as $intQuestionOptionID => $objQuestionOption) {
				ECHOString("Deleting question option {$intQuestionOptionID}", 1, 0);
				$objQuestionOption->remove();
			}
			
			//************************************************************************************************************************************************
			//FASE #2: Inserción
			//Inicia la inserción de nuevos objetos creados en desarrollo, este cambio tal como el previo no tiene efectos excepto los mismos de la fase previa, y serán corregidos con 
			//la fase de actualización
			//La fase consiste en recorrer todos los objetos de desarrollo que no tengan una contraparte en producción, lo cual significaría que o son nuevos o es un error de integridad,
			//cualquiera que sea el caso el objeto se insertará en producción con la misma definición que en desarrollo utilizando literalmente la misma instancia pero forzando al cambio
			//de forma y reasignación de nombres de tablas y demás. Como finalmente es un tipo de copiado, se utilizará el parámetro $bCopy para que funcione igual que en el copiado de
			//formas creando objetos que son necesrios o pasos extras
			
			ECHOString("Inserting objects to production form", 3, 0, "color:purple;");
			//Estos IDs se indexarán por los IDs de desarrollo, para permitir que al procesar las actualizaciones usando precisamente a dichos objetos, se puedan omitir al identificarlos
			//como recién insertados. El valor del array será el ID que se le asignó en producción
			$arrInsertedProdSections = array();
			//$arrInsertedProdSectionsObjs = array();
			$arrInsertedProdQuestions = array();
			//$arrInsertedProdQuestionsObjs = array();
			$arrInsertedProdQuestionOptions = array();
			//$arrInsertedProdQuestionOptionsObjs = array();
			
			//Primero inserta todas las secciones independientemente, ya que las preguntas no son requeridas para continuar insertando el resto de las secciones
			ECHOString("Inserting sections", 2, 0, "color:purple;");
			foreach ($arrDevSectionsByID as $intSectionID => $objSection) {
				if ($objSection->SourceID > 0 && isset($arrProdSectionsByID[$objSection->SourceID])) {
					//En este caso es una sección que ya existe en producción, se ignora por ahora
					ECHOString("Skipping existing section {$intSectionID}", 1, 0);
					continue;
				}
				
				ECHOString("Inserting section {$intSectionID}", 1, 0);
				//Empieza por ajustar los parámetros para asegurar que la copia se creará en producción
				$objSection->CopiedSectionID = $intSectionID;
				$objSection->SurveyID = $intSurveyID;
				$objSection->ForceNew = true;
				
				//Graba la copia en producción y genera el mapeo del nuevo ID
				$objSection->save(false, true);
				//Se debe limpiar el forzado de objeto nuevo para que cuando entre a actualización no lo inserte otra vez
				$objSection->ForceNew = false;
				
				//Después de insertar el objeto, se debe actualizar el SourceID en la forma de desarrollo para apuntar al objeto correspondiente en produccióna
				$sql = "UPDATE SI_SV_Section SET SourceID = {$objSection->SectionID} 
					WHERE SectionID = {$objSection->CopiedSectionID}";
				ECHOString("Linking section {$objSection->CopiedSectionID} to {$objSection->SectionID}: {$sql}", 2, 0, "color:blue;");
				if (!$aRepository->DataADOConnection->Execute($sql)) {
					throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//En este punto la instancia ya no es de la forma de desarrollo sino la de producción, pero falta ajustar los apuntadores
				$arrMappedDevSections[$objSection->CopiedSectionID] = $objSection->SectionID;
				$arrMappedProdSections[$objSection->SectionID] = $objSection->CopiedSectionID;
				$arrInsertedProdSections[$objSection->CopiedSectionID] = $objSection->SectionID;
			}
			
			//Continua insertando todas las preguntas sin sus opciones de respuesta, tal como con las secciones no es requerido hacerlo en el mismo paso
			ECHOString("Inserting questions", 2, 0, "color:purple;");
			foreach ($arrDevQuestionsByID as $intQuestionID => $objQuestion) {
				if ($objQuestion->SourceID > 0 && isset($arrProdQuestionsByID[$objQuestion->SourceID])) {
					//En este caso es una pregunta que ya existe en producción, se ignora por ahora
					ECHOString("Skipping existing question {$intQuestionID}", 1, 0);
					continue;
				}
				
				ECHOString("Inserting question {$intQuestionID}", 1, 0);
				//Obtiene la instancia de la sección a la que pertenece, fuera una sección pre-existente en producción o que se acaba de agregar, para este punto ya debería existir
				$intSectionID = (int) @$arrMappedDevSections[$objQuestion->SectionID];
				if ($intSectionID <= 0) {
					continue;
				}
				
				$objQuestion->CopiedQuestionID = $intQuestionID;
				$objQuestion->SurveyID = $intSurveyID;
				$objQuestion->SectionID = $intSectionID;
				$objQuestion->ForceNew = true;
				
				//Las opciones se grabarán con el save de BITAMQuestionOption
				$objQuestion->StrSelectOptions = "";
				
				//Graba la copia en producción y genera el mapeo del nuevo ID
				//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Agregado el parámetro $bUpdateDep para indicar cuando se desea actualizar las dependencias. Originalmente usado sólo para el proceso de Generación de formas de producción
				$objQuestion->save(false, -1, -1, 0, true, false);
				//@JAPR
				//Se debe limpiar el forzado de objeto nuevo para que cuando entre a actualización no lo inserte otra vez
				$objQuestion->ForceNew = false;
				
				//Después de insertar el objeto, se debe actualizar el SourceID en la forma de desarrollo para apuntar al objeto correspondiente en produccióna
				$sql = "UPDATE SI_SV_Question SET SourceID = {$objQuestion->QuestionID} 
					WHERE QuestionID = {$objQuestion->CopiedQuestionID}";
				ECHOString("Linking question {$objQuestion->CopiedQuestionID} to {$objQuestion->QuestionID}: {$sql}", 2, 0, "color:blue;");
				if (!$aRepository->DataADOConnection->Execute($sql)) {
					throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//En este punto la instancia ya no es de la forma de desarrollo sino la de producción, pero falta ajustar los apuntadores
				$arrMappedDevQuestions[$objQuestion->CopiedQuestionID] = $objQuestion->QuestionID;
				$arrMappedProdQuestions[$objQuestion->QuestionID] = $objQuestion->CopiedQuestionID;
				$arrInsertedProdQuestions[$objQuestion->CopiedQuestionID] = $objQuestion->QuestionID;
				
				//Genera la asociación de atributos de la pregunta en caso de tener alguno
				$objQuestion->saveCatMembersIDs();
			}
			
			//Finalmente inserta todas las opciones de respuesta
			ECHOString("Inserting question options", 2, 0, "color:purple;");
			foreach ($arrDevQOptionsByID as $intQuestionOptionID => $objQuestionOption) {
				if ($objQuestionOption->SourceID > 0 && isset($arrProdQOptionsByID[$objQuestionOption->SourceID])) {
					//En este caso es una opción de pregunta que ya existe en producción, se ignora por ahora
					ECHOString("Skipping existing question option {$intQuestionOptionID}", 1, 0);
					continue;
				}
				
				ECHOString("Inserting question option {$intQuestionOptionID}", 1, 0);
				//Obtiene la instancia de la pregunta a la que pertenece, fuera una pregunta pre-existente en producción o que se acaba de agregar, para este punto ya debería existir
				$intQuestionID = (int) @$arrMappedDevQuestions[$objQuestionOption->QuestionID];
				if ($intQuestionID <= 0) {
					continue;
				}
				
				$objQuestionOption->CopiedOptionID = $intQuestionOptionID;
				$objQuestionOption->SurveyID = $intSurveyID;
				$objQuestionOption->QuestionID = $intQuestionID;
				$objQuestionOption->ForceNew = true;
				$objQuestionOption->bCopy = true;
				
				//Graba la copia en producción y genera el mapeo del nuevo ID
				$objQuestionOption->save();
				//Se debe limpiar el forzado de objeto nuevo para que cuando entre a actualización no lo inserte otra vez
				$objQuestionOption->ForceNew = false;
				
				//Después de insertar el objeto, se debe actualizar el SourceID en la forma de desarrollo para apuntar al objeto correspondiente en produccióna
				$sql = "UPDATE SI_SV_QAnswers SET SourceID = {$objQuestionOption->QuestionOptionID} 
					WHERE ConsecutiveID = {$objQuestionOption->CopiedOptionID}";
				ECHOString("Linking question option {$objQuestionOption->CopiedOptionID} to {$objQuestionOption->QuestionOptionID}: {$sql}", 2, 0, "color:blue;");
				if (!$aRepository->DataADOConnection->Execute($sql)) {
					throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				//En este punto la instancia ya no es de la forma de desarrollo sino la de producción, pero falta ajustar los apuntadores
				$arrMappedDevQuestionOptions[$objQuestionOption->CopiedOptionID] = $objQuestionOption->QuestionOptionID;
				$arrMappedProdQuestionOptions[$objQuestionOption->QuestionOptionID] = $objQuestionOption->CopiedOptionID;
				$arrInsertedProdQuestionOptions[$objQuestionOption->CopiedOptionID] = $objQuestionOption->QuestionOptionID;
			}
			
			ECHOString("Printing data arrays after deleting and inserting objects", 1, 0, "color:purple;");
			ECHOString("arrInsertedProdSections", 2, 0, "color:green;");
			PrintMultiArray($arrInsertedProdSections);
			ECHOString("arrInsertedProdQuestions", 2, 0, "color:green;");
			PrintMultiArray($arrInsertedProdQuestions);
			ECHOString("arrInsertedProdQuestionOptions", 2, 0, "color:green;");
			PrintMultiArray($arrInsertedProdQuestionOptions);
			
			//************************************************************************************************************************************************
			//FASE #3: Actualización
			//En este punto ya se tienen eliminados los objetos que ya no se necesita e insertados los nuevos objetos, ahora sólo falta actualizar los objetos preexistentes, para lo
			//cual se utilizará la instancia de desarrollo cambiando únicamente los IDs a los que se apunta para que se haga la actualización directo en producción. En ninguno de los
			//casos se soporta en esta versión cambio de tipo de pregunta hasta la fecha de implementación, así que es seguro asumir que los objetos que contienen opciones de respuesta
			//siguen teniendo dicha relación a pesar de esta actualización, por lo que no es necesario un proceso de limpieza adicional (aunque en caso de haber sido necesario, se habría
			//detectado igual desde la Fase #1)
			//Los objetos que serán utilizados en este punto, aún NO pertenecen a producción, excepto si acaban de ser insertados en cuyo caso serán ingorados pues ya estarán esencialmente
			//igual a los de desarrollo, por lo que se conserva la validación inicial en cada ciclo pero invertida, ya que el array de objetos no se modifica durante el proceso, sólo
			//el array de mapeos
			//Por simplicidad se integrarán la Fase #3 y la Fase #5 en este punto, puesto que para este momento ya se tienen creados todos los objetos nuevos con su correspondiente
			//mapeo, así que se puede evitar un segundo recorrido cambiando las referencias directamente en este UPDATE
			ECHOString("Updating objects to production form", 3, 0, "color:purple;");
			
			//Actualiza las secciones
			ECHOString("Updating sections", 2, 0, "color:purple;");
			foreach ($arrDevSectionsByID as $intSectionID => $objSection) {
				$blnIsNewObject = false;
				if ($objSection->SourceID <= 0 || !isset($arrProdSectionsByID[$objSection->SourceID])) {
					//En este caso es una sección nueva por lo que no es necesario ajustar los IDs del padre, ya que para haberla creado se tuvieron que haber asignado y ya
					//se autogeneró el ID propio del objeto, sólo se ajustarán las referencias
					ECHOString("Updating new section {$intSectionID}", 1, 0);
					//continue;
					$blnIsNewObject = true;
				}
				else {
					ECHOString("Updating section {$intSectionID}", 1, 0);
				}
				
				//Empieza por ajustar las referencias a los objetos padre que son las únicas necesarias para hacer join durante la actualización, adicionalmente ajusta todas las
				//referencias directas de IDs antes de iniciar la actualización
				//Section: Requiere join entre SurveyID y SectionID
				if (!$blnIsNewObject) {
					$objSection->SurveyID = $intSurveyID;
					$objSection->SectionID = (int) @$arrMappedDevSections[$intSectionID];
					
					//Debe limpiar la información de catálogo para que se autogenere uno nuevo en producción
					//@JAPR 2017-02-13: Corregido un bug, para el proceso de copiado es correcto eliminar la referencia al catálogo, incluso para el proceso de inserción de objetos,
					//pero para el proceso de actualización no se debe hacer o se perderá la referencia anterior al catálogo y eso dejará basura cuando genere uno nuevo desligando el anterior
					//Si es una sección pre-existente, entonces estas referencias apuntan a desarrollo por lo que hay que cambiarlas al valor correspondiente de producción o el proceso
					//se ejecutará de manera equivocada
					//$objSection->CatalogID = 0;
					//$objSection->CatMemberID = 0;
					//$objSection->CatalogIDOld = 0;
					$objProdSection = $arrProdSectionsByID[$objSection->SourceID];
					if (!is_null($objProdSection)) {
						$objSection->CatalogID = $objProdSection->CatalogID;
						$objSection->CatMemberID = $objProdSection->CatMemberID;
						$objSection->CatalogIDOld = $objProdSection->CatalogIDOld;
					}
					//@JAPR
				}
				else {
					//Si la sección fuera nueva, entonces estos mismos valores ya habrían asignado para este punto el catálogo correcto de producción, por lo que no hacemos cambios
					//a los IDs ya que no estaríamos actualizando nada de catálogos en este punto, sólo ajustando referencias de IDs del diseño básico
				}
				
				//Si por alguna razón no se hubiera logrado el mapeo aquí debería marcar un error
				if ($objSection->SectionID <= 0) {
					continue;
				}
				
				//Ajusta referencias directas
				if ($objSection->NextSectionID > 0) {
					//@JAPR 2017-02-13: Corregido un bug, se estaba usando el array equivocado para obtener la referencia del ID en producción (#N5ZGDX)
					$objSection->NextSectionID = (int) @$arrMappedDevSections[$objSection->NextSectionID];
				}
				
				if ($objSection->SelectorQuestionID > 0) {
					$objSection->SelectorQuestionID = (int) @$arrMappedDevQuestions[$objSection->SelectorQuestionID];
				}
				
				//@JAPR 2019-03-11: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if ($objSection->XPosQuestionID > 0) {
					$objSection->XPosQuestionID = (int) @$arrMappedDevQuestions[$objSection->XPosQuestionID];
				}
				if ($objSection->YPosQuestionID > 0) {
					$objSection->YPosQuestionID = (int) @$arrMappedDevQuestions[$objSection->YPosQuestionID];
				}
				if ($objSection->ItemNameQuestionID > 0) {
					$objSection->ItemNameQuestionID = (int) @$arrMappedDevQuestions[$objSection->ItemNameQuestionID];
				}
				//@JAPR
				
				//Actualiza la copia en producción
				$objSection->save();
			}
			
			//Actualiza las preguntas
			ECHOString("Updating questions", 2, 0, "color:purple;");
			foreach ($arrDevQuestionsByID as $intQuestionID => $objQuestion) {
				$blnIsNewObject = false;
				if ($objQuestion->SourceID <= 0 || !isset($arrProdQuestionsByID[$objQuestion->SourceID])) {
					//En este caso es una pregunta nueva por lo que no es necesario ajustar los IDs del padre, ya que para haberla creado se tuvieron que haber asignado y ya
					//se autogeneró el ID propio del objeto, sólo se ajustarán las referencias
					ECHOString("Updating new question {$intQuestionID}", 1, 0);
					//continue;
					$blnIsNewObject = true;
				}
				else {
					ECHOString("Updating question {$intQuestionID}", 1, 0);
				}
				
				//Empieza por ajustar las referencias a los objetos padre que son las únicas necesarias para hacer join durante la actualización, adicionalmente ajusta todas las
				//referencias directas de IDs antes de iniciar la actualización
				//Question: Requiere join entre SurveyID y QuestionID
				if (!$blnIsNewObject) {
					$objQuestion->SurveyID = $intSurveyID;
					$objQuestion->QuestionID = (int) @$arrMappedDevQuestions[$intQuestionID];
					
					//Debe limpiar la información de catálogo para que se autogenere uno nuevo en producción
					//@JAPR 2017-02-13: Corregido un bug, para el proceso de copiado es correcto eliminar la referencia al catálogo, incluso para el proceso de inserción de objetos,
					//pero para el proceso de actualización no se debe hacer o se perderá la referencia anterior al catálogo y eso dejará basura cuando genere uno nuevo desligando el anterior
					//Si es una pregunta pre-existente, entonces estas referencias apuntan a desarrollo por lo que hay que cambiarlas al valor correspondiente de producción o el proceso
					//se ejecutará de manera equivocada
					//$objQuestion->CatalogID = 0;
					//$objQuestion->CatMemberID = 0;
					//$objQuestion->CatalogIDOld = 0;
					//$objQuestion->CatMemberLatitudeID = 0;
					//$objQuestion->CatMemberLongitudeID = 0;
					//$objQuestion->CatMemberImageID = 0;
					//$objQuestion->CatMemberOrderID = 0;
					$objProdQuestion = $arrProdQuestionsByID[$objQuestion->SourceID];
					if (!is_null($objProdQuestion)) {
						$objQuestion->CatalogID = $objProdQuestion->CatalogID;
						$objQuestion->CatMemberID = $objProdQuestion->CatMemberID;
						$objQuestion->CatalogIDOld = $objProdQuestion->CatalogIDOld;
						$objQuestion->CatMemberLatitudeID = $objProdQuestion->CatMemberLatitudeID;
						$objQuestion->CatMemberLongitudeID = $objProdQuestion->CatMemberLongitudeID;
						$objQuestion->CatMemberImageID = $objProdQuestion->CatMemberImageID;
						$objQuestion->CatMemberOrderID = $objProdQuestion->CatMemberOrderID;
					}
					//@JAPR
				}
				else {
					//Si la pregunta fuera nueva, entonces estos mismos valores ya habrían asignado para este punto el catálogo correcto de producción, por lo que no hacemos cambios
					//a los IDs ya que no estaríamos actualizando nada de catálogos en este punto, sólo ajustando referencias de IDs del diseño básico
				}
				
				//Si por alguna razón no se hubiera logrado el mapeo aquí debería marcar un error
				if ($objQuestion->QuestionID <= 0) {
					continue;
				}
				
				//Ajusta referencias directas
				if ($objQuestion->SourceQuestionID > 0) {
					$objQuestion->SourceQuestionID = (int) @$arrMappedDevQuestions[$objQuestion->SourceQuestionID];
				}
				
				if ($objQuestion->SharedQuestionID > 0) {
					$objQuestion->SharedQuestionID = (int) @$arrMappedDevQuestions[$objQuestion->SharedQuestionID];
				}
				
				if ($objQuestion->SourceSimpleQuestionID > 0) {
					$objQuestion->SourceSimpleQuestionID = (int) @$arrMappedDevQuestions[$objQuestion->SourceSimpleQuestionID];
				}
				
				if ($objQuestion->GoToQuestion > 0) {
					//@JAPR 2017-02-13: Corregido un bug, se estaba usando el array equivocado para obtener la referencia del ID en producción (#N5ZGDX)
					$objQuestion->GoToQuestion = (int) @$arrMappedDevSections[$objQuestion->GoToQuestion];
				}
				
				if ($objQuestion->SourceSectionID > 0) {
					//@JAPR 2017-02-13: Corregido un bug, se estaba usando el array equivocado para obtener la referencia del ID en producción (#N5ZGDX)
					$objQuestion->SourceSectionID = (int) @$arrMappedDevSections[$objQuestion->SourceSectionID];
				}
				
				//Actualiza la copia en producción
				//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Agregado el parámetro $bUpdateDep para indicar cuando se desea actualizar las dependencias. Originalmente usado sólo para el proceso de Generación de formas de producción
				$objQuestion->save(false, -1, -1, 0, false, false);
				//@JAPR
				
				//Después de ajustar los IDs en las preguntas es necesario actualizar unas tablas de dependencia que se utilizan en
				//preguntas que copian su respuesta de otras, preguntas calculadas y condiciones de visibilidad
				//@JAPR 2017-02-13: No tiene caso realizar este proceso en este punto, ya que los defaults, fórmulas y demás expresiones de la instancia aún utilizan los IDs originales,
				//así que aunque se invocara al método, sólo obtendría IDs que no podría convertir a números de objeto en esta forma y no realizaría nada en realidad
				/*if($objQuestion->HasQuestionDep) {
					$objQuestion->setQuestionsToEval();
				}
				$objQuestion->setFormulaChildren();
				$objQuestion->setShowConditionChildren();
				*/
				//@JAPR
				
				//Actualizar el campo mostrar preguntas
				//@JAPR 2017-02-14: Corregido un bug, no estaba grabando correctamente los Show Questions (#N5ZGDX)
				//En este caso, se tratará de reutilizar la mayor cantidad de showQuestions que hubiera, así que se carga tanto la colección de desarrollo como la de producción para
				//actualizar utilizando la instancia de producción mientras aún existan instancias previamente creadas, agregando una nueva en cada caso después de haber terminado de
				//actualizar los showQuestions de desarrollo, eliminando los sobrantes
				$objDevShowQuestionsColl = BITAMShowQuestionCollection::NewInstanceByQuestionID($aRepository, $intQuestionID);
				$objProdShowQuestionsColl = BITAMShowQuestionCollection::NewInstanceByQuestionID($aRepository, $objQuestion->QuestionID);
				$intCountShowQuestions = count($objProdShowQuestionsColl->Collection);
				$intIdxShowQuestion = 0;
				if (!is_null($objDevShowQuestionsColl) && !is_null($objProdShowQuestionsColl)) {
					//Primero insertará o actualizará los show questions existentes
					foreach ($objDevShowQuestionsColl as $objShowQuestion) {	
						ECHOString("Updating show question {$objShowQuestion->ShowID} => {$objShowQuestion->ConsecutiveID}", 1, 0);
						
						//Primero obtiene el ID de la opción de respuesta a utilizar
						$intQuestionOptionID = (int) @$arrMappedDevQuestionOptions[$objShowQuestion->ConsecutiveID];
						if ($intQuestionOptionID < 0) {
							continue;
						}
						
						//Crea una nueva instancia o reutiliza una pre-existente de producción para esta pregunta, en ambos casos el QuestionID ya estaría correctamente asignado, sólo
						//hace falta cambiar el ID del Mapeo. Se aprovechará del hecho que la PRIMARY KEY o constraints no impiden duplicar la combinación de QuestionID + ShowQuestionID
						//para no tener que reutilizar exactamente la misma instancia para redefinir la combinación, simplemente se grabará con la primera disponible y las no usadas se
						//eliminarán, así que la integridad quedará correcta al terminar
						if ($intIdxShowQuestion < $intCountShowQuestions) {
							$objNewShowQuestion = $objProdShowQuestionsColl->Collection[$intIdxShowQuestion++];
						}
						else {
							$objNewShowQuestion = BITAMShowQuestion::NewInstance($aRepository, $intQuestionOptionID);
						}
					
						$objNewShowQuestion->ShowQuestionID = (int) @$arrMappedDevQuestions[$objShowQuestion->ShowQuestionID];
						$objNewShowQuestion->save();
					}
					
					//Si al finalizar hubieran quedado Show Questions de producción sin utilizar, se deben remover para no dejar basura
					while ($intIdxShowQuestion < $intCountShowQuestions) {
						$objNewShowQuestion = $objProdShowQuestionsColl->Collection[$intIdxShowQuestion++];
						if (!is_null($objNewShowQuestion)) {
							$objNewShowQuestion->remove();
						}
					}
				}
				
				//@JAPR 2017-02-13: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
				//Actualizar las referencias de preguntas OCR
				if ($objQuestion->QTypeID == qtpOCR) {
					$arrQuestionList = array();
					foreach ($objQuestion->QuestionList as $intOpenQuestionID) {
						$intProdOpenQuestionID = (int) @$arrMappedDevQuestions[$intOpenQuestionID];
						if ($intProdOpenQuestionID > 0) {
							$arrQuestionList[] = $intProdOpenQuestionID;
						}
					}
					$objQuestion->QuestionList = $arrQuestionList;
					$objQuestion->saveOCRQuestionIDs();
				}
				//@JAPR
			}
			
			//Actualiza las opciones de preguntas
			ECHOString("Updating question options", 2, 0, "color:purple;");
			foreach ($arrDevQOptionsByID as $intQuestionOptionID => $objQuestionOption) {
				$blnIsNewObject = false;
				if ($objQuestionOption->SourceID <= 0 || !isset($arrProdQOptionsByID[$objQuestionOption->SourceID])) {
					//En este caso es una opción de pregunta nueva por lo que no es necesario ajustar los IDs del padre, ya que para haberla creado se tuvieron que haber asignado y ya
					//se autogeneró el ID propio del objeto, sólo se ajustarán las referencias
					ECHOString("Updating new question option {$intQuestionOptionID}", 1, 0);
					//continue;
					$blnIsNewObject = true;
				}
				else {
					ECHOString("Updating question option {$intQuestionOptionID}", 1, 0);
				}
				
				//Empieza por ajustar las referencias a los objetos padre que son las únicas necesarias para hacer join durante la actualización, adicionalmente ajusta todas las
				//referencias directas de IDs antes de iniciar la actualización
				//Question Option: Requiere join entre QuestionOptionID y QuestionID
				if (!$blnIsNewObject) {
					$objQuestionOption->SurveyID = $intSurveyID;
					$objQuestionOption->QuestionID = (int) @$arrMappedDevQuestions[$objQuestionOption->QuestionID];
					$objQuestionOption->QuestionOptionID = (int) @$arrMappedDevQuestionOptions[$intQuestionOptionID];
				}
				
				//Si por alguna razón no se hubiera logrado el mapeo aquí debería marcar un error
				if ($objQuestionOption->QuestionID <= 0) {
					continue;
				}
				
				//Ajusta referencias directas
				if ($objQuestionOption->NextQuestion > 0) {	
					//@JAPR 2017-02-13: Corregido un bug, se estaba usando el array equivocado para obtener la referencia del ID en producción (#N5ZGDX)
					$objQuestionOption->NextQuestion = (int) @$arrMappedDevSections[$objQuestionOption->NextQuestion];
				}
				
				//Actualiza la copia en producción
				$objQuestionOption->save();
			}
			
			//@JAPR 2017-02-13: Agregada la actualización de las configuraciones de la forma
			//Actualiza las configuraciones de la forma
			//Para este punto la forma de producción ya se encuentra generada, así que se utiliza la instancia de desarrollo para heredar de manera automática todas las configuraciones
			//hacia la de producción, sólo se cambia el ID y los campos claves que la ligaban a producción para simular que es una forma de producción con desarrollo
			ECHOString("Updating survey", 2, 0, "color:purple;");
			$objDevSurvey->createAction = createAction;
			$objDevSurvey->FormType = formProdWithDev;
			$objDevSurvey->SourceID = $aSurveyID;
			$objDevSurvey->SurveyID = $intSurveyID;
			$objDevSurvey->SurveyName = $objProdSurvey->SurveyName;
			$objDevSurvey->ForceNew = false;
			// MAPR 2017-07-26: Un error en el copiado de la forma, no permite pasar EntryDescription como actualización de Desarrollo a Producción (#S818ZS)
			$objDevSurvey->QuestionIDForEntryDesc = (int) @$arrMappedDevQuestions[$objDevSurvey->QuestionIDForEntryDesc];
			$objDevSurvey->save(false, false);
			
			//************************************************************************************************************************************************
			//FASE #4: Actualización de fórmulas (objetos nuevos o pre-existentes)
			//Dado a que la inserción de nuevos objetos y la actualización de objetos pre-existentes se realizó utilizando la instancia de desarrollo pero ajustada a la forma de 
			//producción, todas las referencias quedaron apuntando realmente a IDs de desarrollo, así que es necesario realizar un proceso que las traduzca a IDs de producción, después
			//del cual se tendrán que volver a cargar las referencias pero limpiando primero el Caché de objetos, ya que esta traducción de IDs se realiza a nivel de Metadata y no en los
			//objetos cargados en memoria
			ECHOString("Updating object formulas in production form", 3, 0, "color:purple;");
			
			//Limpia los objetos en memoria
			BITAMGlobalFormsInstance::ResetCollections();
			
			//Ajustar los IDs de secciones y preguntas en los campos donde se utiliza la sintaxis de fórmulas
			@BITAMSurvey::TranslateObjecIDsInCopy($aRepository, $aSurveyID, $intSurveyID, $arrMappedDevSections, $arrMappedDevQuestions);

			//FASE #4.1: Actualización de HTMLs personalizados
			@BITAMSurvey::TranslateJavaScriptObjecIDsInCopy($aRepository, $aSurveyID, $intSurveyID, $arrMappedDevSections, $arrMappedDevQuestions);
			
			//************************************************************************************************************************************************
			//FASE #5: Actualización de referencias
			//Todo este proceso se implementó directamente en la Fase #3 por simplicidad, incluso para los objetos nuevos, así que no queda nada por hacer al respecto en este punto a
			//nivel de la definición, sin embargo existen tablas adicionales que deben ser ajustadas pues contienen referencias que utilizan los IDs originales así que se deben traducir
			//a los IDs en la forma de producción (por ejemplo las dependencias de preguntas en fórmulas, HTMLs personalizados, etc.)
			ECHOString("Updating object references in production form", 3, 0, "color:purple;");
			
			//Vuelve a cargar las colecciones de producción, pero esta vez las almacena en un array diferente. Para este momento todos los objetos regresados en las colecciones son
			//válidos en la forma de producción y ya tienen sus referencias en metadata correctamente ajustadas
			$objProdSectionCollection = BITAMSectionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_null($objProdSectionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Sections")." # {$intSurveyID})");
			}
			
			$objProdQuestionCollection = BITAMQuestionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_null($objProdQuestionCollection)) {
				throw new Exception(translate("There was an error while building the production form")." (".translate("Survey")." - ".translate("Questions")." # {$intSurveyID})");
			}
			
			//@JAPR 2017-02-13: Corregido un bug, no estaba actualizando los filtros dinámicos
			ECHOString("Updating section references", 2, 0, "color:purple;");
			foreach ($objProdSectionCollection as $objSection) {
				$intSectionID = $objSection->SectionID;
				ECHOString("Updating section {$intSectionID} references", 1, 0);
				
				$objSection->saveDynamicFilter();
			}
			//@JAPR
			
			ECHOString("Updating question references", 2, 0, "color:purple;");
			foreach ($objProdQuestionCollection as $objQuestion) {
				$intQuestionID = $objQuestion->QuestionID;
				ECHOString("Updating question {$intQuestionID} references", 1, 0);
				
				if ($objQuestion->HasQuestionDep) {
					$objQuestion->updateQuestionsToEval();
				}
				$objQuestion->updateFormulaChildren();
				$objQuestion->updateShowConditionChildren();
				
				//@JAPR 2017-02-13: Corregido un bug, no estaba actualizando los filtros dinámicos
				$objQuestion->saveDynamicFilter();
			}
			
			//************************************************************************************************************************************************
			//FASE #6: Ajustar el orden de los objetos
			//Al final de todos los movimientos, se debe volver a actualizar cada una de las secciones, preguntas y opciones de respuesta ya que por el orden de modificación de la
			//forma de producción y debido a que los métodos que agregan van automáticamente incrementando las posiciones, es casi un hecho que la numeración debió haber quedado desfasada
			//en producción o incluso con huecos, sin embargo la actualización se hace mediante estatutos SQL ya que para este punto la forma en desarrollo y la forma en producción
			//deberían tener la misma cantidad de elementos en cada tipo y siempre en una relación 1:1 con el apuntador en desarrollo, si no fuera así sería un error
			ECHOString("Updating objects order in production form", 3, 0, "color:purple;");
			
			$sql = "UPDATE SI_SV_Section Prod 
							INNER JOIN SI_SV_Section Dev ON Prod.SectionID = Dev.SourceID 
						SET Prod.SectionNumber = Dev.SectionNumber 
						WHERE Dev.SurveyID = {$aSurveyID}";
			ECHOString("Updating Sections numbers: {$sql}", 2, 0, "color:blue;");
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "UPDATE SI_SV_Question Prod 
							INNER JOIN SI_SV_Question Dev ON Prod.QuestionID = Dev.SourceID 
						SET Prod.QuestionNumber = Dev.QuestionNumber 
						WHERE Dev.SurveyID = {$aSurveyID}";
			ECHOString("Updating Questions numbers: {$sql}", 2, 0, "color:blue;");
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$sql = "UPDATE SI_SV_QAnswers Prod 
							INNER JOIN SI_SV_QAnswers Dev ON Prod.ConsecutiveID = Dev.SourceID 
						SET Prod.SortOrder = Dev.SortOrder 
						WHERE Dev.QuestionID IN (
							SELECT QuestionID 
							FROM SI_SV_Question 
							WHERE SurveyID = {$aSurveyID}
						)";
			ECHOString("Updating Question Options numbers: {$sql}", 2, 0, "color:blue;");
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				throw new Exception("(".__METHOD__.") ".translate("Error accessing")." SI_SV_QAnswers ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//************************************************************************************************************************************************
			//Antes de dar por finalizado el proceso, debe incrementar en una unidad exclusivamente la versión de la forma, sin embargo debido a que se usan la instancias para
			//modificar el diseño, se lanzaron múltiples actualizaciones de la versión por lo que realmente en este punto restaurar a la versión original + 1 solamente, ya que
			//la versión seguramente quedó muchas unidades desfasada de la original
			//@JAPRWarning: El único problema en esta situación sería si alguien que ya tenía la forma en su App comienza a sincronizar capturas, ya que al actualizar definiciones
			//podría descargar una versión no concluída de la forma que estuviera siendo sincronizada en ese preciso instante, si esto llega a suceder, su número de versión sería
			//demasiado grande comparado con el que finalmente quedará, así que no podrá descargar la versión corregida de la forma sin primero eliminar definiciones locales, sin embargo
			//sincronizar mientras están generando el App de producción NO es válido así que no se debería hacer de cualquier manera, este proceso sólo se debe hacer cuando estamos
			//seguros que no hay nada siendo recibido o descargado desde/hacia dispositivos
			if ($intProductionFormVersionNum > 0 && $intSurveyID > 0) {
				$intProductionFormVersionNum++;
				$sql = "UPDATE SI_SV_Survey SET VersionNum = {$intProductionFormVersionNum} 
					WHERE SurveyID = {$intSurveyID}";
				ECHOString("Updating form VersionNum: {$sql}", 2, 0, "color:blue;");
				$aRepository->DataADOConnection->Execute($sql);
			}
			
			//Termina de generar la forma de producción
			ECHOString("Process finished ok", 2, 2, "color:purple;");
		}
		catch (Exception $e) {
			$strErrorDesc = $e->getMessage();
			
			//En caso de error tiene que volver a restaura el número de versión de la forma de producción para evitar problemas con quienes ya tenían un avance en capturas al sincronizar
			//definiciones, no se podrá seguramente grabar nada de lo que capture, pero por lo menos podrán seguir mandando sus datos en lo que se corrige la situación
			if ($intProductionFormVersionNum > 0 && $intSurveyID > 0) {
				$sql = "UPDATE SI_SV_Survey SET VersionNum = {$intProductionFormVersionNum} 
					WHERE SurveyID = {$intSurveyID}";
				ECHOString("Restoring form VersionNum: {$sql}", 2, 0, "color:blue;");
				$aRepository->DataADOConnection->Execute($sql);
			}
			
			ECHOString("Process finished with error: {$strErrorDesc}", 2, 2, "color:red;");
		}
		$strBuffer = trim(ob_get_contents());
		ob_end_clean();
		
		//Si se pidió depuración del proceso, se vuelve a mandar la salida a la pantalla (esto provocará que falle la respuesta JSON, como lo indica el nombre, es sólo depuración)
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			if ($strBuffer) {
				echo($strBuffer);
			}
		}
		
		//Si hubo un error durante el proceso, dejará grabado el archivo log de último procesamiento para que pueda ser analizado
		if ($strErrorDesc && $strBuffer) {
			logString("Building Survey {$aSurveyID}", $strFileName);
			logString($strBuffer, $strFileName, false);
		}
		$blnSaveLogStringFile = false;
		
		return $strErrorDesc;
	}
	//@JAPR
	
	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}
		
		global $appVersion;
		//@JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
		global $gbDesignMode;
		//@JAPR
		
		$arrDef = array();
		
		$arrDef['id'] = $this->SurveyID;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['name'] = $this->SurveyName;
		$arrDef['catalogID'] = $this->CatalogID;
		//$arrDef['lastDateID'] = $this->LastDateID;
		$arrDef['hasSignature'] = $this->HasSignature;
		$arrDef['allowPartialSave'] = $this->AllowPartialSave;
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas
		$arrDef['versionNum'] = $this->VersionNum;
		//@JAPR 2012-05-16: Agregadas las preguntas tipo Action
		$arrDef['elementQuestionID'] = $this->ElementQuestionID;
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		$arrDef['enableReschedule'] = $this->EnableReschedule;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['rescheduleStatus'] = $this->RescheduleStatus;
		$arrDef['syncWhenSaving'] = $this->SyncWhenSaving; //conchita 2013-03-26 faltaba agregar esta propiedad
		//@JAPR 2014-12-01: Corregido un bug, todas estas opciones una vez asignadas no se sobreescribían pues sólo se regresaban si estaban
		//activas
		if (getMDVersion() >= esvHideNavigationButtons) {
			//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
			$arrDef['hideNavButtons'] = $this->HideNavButtons;
			//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
			$arrDef['hideSectionNames'] = $this->HideSectionNames;
		}
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
			$arrDef['disableEntry'] = $this->DisableEntry;
		}
		//@JAPR 2015-06-25: Rediseñado el Admin con DHTMLX
		//OMMC 2016-04-26: Agregado el trim en la descripción, por alguna razón se estaba quedando un espacio en balanco a pesar
		//que se borraban las descripciones de las formas, hacía que se dibujara de manera innecesaria en el App.
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$arrDef['descrip'] = trim($this->SurveyDesc);
			$arrDef['otherLabel'] = $this->OtherLabel;
			$arrDef['commentLabel'] = $this->CommentLabel;
			$strEM = $this->TranslateVariableQuestionIDsByNumbers($this->AdditionalEMails);
			$arrDef['additionalEMails'] = $strEM;
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$arrDef['width'] = $this->Width;
			$arrDef['height'] = $this->Height;
			$arrDef['textPos'] = $this->TextPosition;
			$arrDef['textSize'] = trim($this->TextSize);
			$arrDef['customLayout'] = $this->CustomLayout;
		}
		//@JAPR
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			if($this->ShowQNumbers != -1 || $this->ShowQNumbers != "-1"){
				$arrDef['showQNumbers'] = $this->ShowQNumbers;	
			}
		}
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$arrDef['enableNavHistory'] = $this->EnableNavHistory;
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$arrDef['autoFitImage'] = $this->AutoFitImage;
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		$arrDef['plannedDuration'] = $this->PlannedDuration;
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$arrDef['templateID'] = $this->TemplateID;
			if ($this->TemplateID > 0) {
				$objSetting = @BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ENABLECOLORTEMPLATES');
				if (!is_null($objSetting) && ((int) @$objSetting->SettingValue) == 1) {
					//Agregado el parámetro $surveyID para indicar la forma de la cual se deberá identificar si tiene o no configuración de template personalizada
					$objAppCustomization = BITAMEFormsAppCustomStylesTpl::GetCurrentColorScheme($this->Repository, -1, $this->SurveyID);
					if (!is_null($objAppCustomization)) {
						$arrCSSStyle = $objAppCustomization->getJSonDefinition();
						if (!is_null($arrCSSStyle) && is_array($arrCSSStyle) && count($arrCSSStyle) > 0) {
							$arrDef['cssStyle'] = $arrCSSStyle;
						}
						else {
							$arrDef['cssStyle'] = array();
						}
					}
				}
			}
		}
		//@JAPR
		
		//@MABH20130221
		$arrDef['hasScheduler'] = BITAMSurvey::canApplySurvey($this->Repository, $this->SurveyID, $_SESSION["PABITAM_UserID"]);
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		$strDisplayImage = trim($this->SurveyImageText);
		//@JAPR 2015-11-19: Corregido un bug, independientemente de si hay o no imagen, se debe regresar la propiedad, ya que no regresarla provocaba que el diseñador la tomara como
		//undefined. Dado que la propiedad se asigna inmediatamente con $strDisplayImage antes de que esta cambie, este es el mejor punto para asignarla independientemente si viene
		//o no vacía (Raquel validó en el diseñador de todas maneras que undefined se considerara vacío, así que el error se corrigió ya por las dos vías)
		//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
		//Se usarán propiedades alternativas tal como están grabadas en la metadata
		if ($gbDesignMode) {
			$arrDef['displayImageDes'] = $strDisplayImage;
		}
		//@JAPR
		if ($strDisplayImage != '') {
			$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
			}
			elseif (!$blnWebMode) {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
				$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayImage, true, false);
			}
			else {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				//JAPR 2015-08-04: Modificada para utilizar DHTMLX
				if ($this->CreationAdminVersion >= esveFormsv6 && $appVersion < esveFormsDHTMLX) {
					$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
				}
				//@JAPR
			}
			//@JAPR
		}
		//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
		$arrDef['displayImage'] = $strDisplayImage;
		
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		$strDisplayMenuImage = trim($this->SurveyMenuImage);
		//@JAPR 2015-11-19: Corregido un bug, independientemente de si hay o no imagen, se debe regresar la propiedad, ya que no regresarla provocaba que el diseñador la tomara como
		//undefined. Dado que la propiedad se asigna inmediatamente con $strDisplayImage antes de que esta cambie, este es el mejor punto para asignarla independientemente si viene
		//o no vacía (Raquel validó en el diseñador de todas maneras que undefined se considerara vacío, así que el error se corrigió ya por las dos vías)
		//@JAPR 2015-07-31: Corregidas las imagenes, se usará una propiedad alterna para reutilizar las definiciones descargadas ajustas a preview (#6X0EUZ)
		//Se usarán propiedades alternativas tal como están grabadas en la metadata
		if ($gbDesignMode) {
			$arrDef['displayMenuImageDes'] = $strDisplayMenuImage;
		}
		//@JAPR
		if ($strDisplayMenuImage != '') {
			$strDisplayMenuImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayMenuImage));
			//@JAPR 2013-03-11: Corregido un bug, la validación de webMode no se hace con la pura existencia del parámetro sino con su valor
			//@JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
			if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
				$strDisplayMenuImage = (string) @BITAMSection::convertImagesToBase64($strDisplayMenuImage);	
			}
			elseif (!$blnWebMode) {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
				$strDisplayMenuImage = (string) @BITAMSection::convertImagesToRelativePath($this->Repository, $strDisplayMenuImage, true, false);
			}
			else {
				//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
				//JAPR 2015-08-04: Modificada para utilizar DHTMLX
				if ($this->CreationAdminVersion >= esveFormsv6 && $appVersion < esveFormsDHTMLX) {
					$strDisplayMenuImage = '<img src="'.$strDisplayMenuImage.'" />';
				}
				//@JAPR
			}
			//@JAPR
		}
		//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
		$arrDef['displayMenuImage'] = $strDisplayMenuImage;

		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		$arrDef['incrementalFile'] = trim($this->incrementalFile);
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		//Configuraciones que sólo se agregarán al App en modo de Preview del diseñador de formas
		if ($gbDesignMode) {
			$arrDef['formType'] = $this->FormType;
		}
		
		//@JAPR 2012-11-24: Modificado para permitir que las preguntas numéricas de catálogo sean evaluadas al cambiar a la pregunta padre
		//en cualquier tipo de sección
		$arrAllQuestions = array();	//Este elemento NO se regresa entre las definiciones
		
        //@JAPR 2014-06-17: Agregado el soporte de filtrado de secciones Inline a partir de una pregunta previa del mismo catálogo
        $arrSectionsByCatalog = array();
        $arrSectionFound = array();
        $arrFatherQuestionID = array();
        $arrFatherQuestionSectionID = array();
		
        //Obtenemos coleccion de secciones
        $arrDef['sections'] = array();
        $arrDef['sectionsOrder'] = array();
        $arrDef['questionsOrder'] = array();
		//@JAPR 2015-06-19: Rediseñado el Admin con DHTMLX
		//Por optimización se generará para modo diseño el Array de preguntas en este punto, lo cual no es necesario ya que el App lo hace en addClassPrototype
		if ($gbDesignMode) {
			$arrDef['questions'] = array();
		}
		//@JAPR
		//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Asignada la variable para que se grabe en caché de secciones esta petición, así podrá ser reutilizada mas abajo por getEFormsDefinitions
		$sectionCollection = BITAMSectionCollection::NewInstance($this->Repository, $this->SurveyID, null, null, true);
		//@JAPR
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-07-30: Agregado el Responsive Design
			if (getMDVersion() >= esvResponsiveDesign) {
				//Carga las propiedades dinámicas equivalentes al responsive Design pero sólo si se recibió el tipo de dispositivo que está
				//haciendo la petición
				$intDeviceID = identifyDeviceType();
				if ($intDeviceID > dvcWeb) {
					BITAMSection::GetResponsiveDesignProps($this->Repository, $sectionCollection, $this->SurveyID);
				}
			}
		}
		//@JAPR
        
        foreach ($sectionCollection->Collection as $sectionElement) {
        	$arrDef['sections'][$sectionElement->SectionID] = $sectionElement->getJSonDefinition();
        	//@JAPR 2012-08-23: Agregada la referencia a la pregunta que genera los valores dinámicos de la sección (nuevo en v4)
        	if ($sectionElement->SectionType == sectDynamic) {
        		//@JAPR 2012-11-21: Corregido un bug, sólo asignaba la pregunta padre si pertenecía a la sección inmediata anterior
        		//Recorre todas las secciones desde la primera hasta la actual para identificar a la pregunta padre
        		$intFatherQuestionID = 0;
        		$intFatherQuestionSectionID = 0;
        		$intNumSections = count($arrDef['sectionsOrder']);
        		for ($intCont = 0; $intCont < $intNumSections; $intCont++) {
        			$aPreviousSection = @$sectionCollection->Collection[$intCont];
	        		if (!is_null($aPreviousSection)) {
	        			//Si ya llegó a la sección dinámica, entonces sale pues el padre ya debió estar definido en este punto
	        			if ($aPreviousSection->SectionID == $sectionElement->SectionID) {
	        				break;
	        			}
	        			
		        		//Obtiene la última pregunta de la sección previa que utilice el mismo catálogo que la sección dinámica, ya que hasta v3 sólo
		        		//podía haber una sección de este tipo por encuesta y esta era la condición
			        	foreach ($arrDef['sections'][$aPreviousSection->SectionID]['questions'] as $intQuestionID => $aQuestionDef) {
			        		if ($aQuestionDef['catalogID'] == $sectionElement->CatalogID) {
			        			$intFatherQuestionID = $intQuestionID;
			        			$intFatherQuestionSectionID = $aPreviousSection->SectionID;
			        		}
			        	}
	        		}
        		}
        		
        		if ($intFatherQuestionID > 0 && $intFatherQuestionSectionID > 0) {
		        	$arrDef['sections'][$sectionElement->SectionID]['fatherQuestionID'] = $intFatherQuestionID;
		        	
	        		if (isset($arrDef['sections'][$intFatherQuestionSectionID]['questions'][$intFatherQuestionID])) {
	        			$arrDef['sections'][$intFatherQuestionSectionID]['questions'][$intFatherQuestionID]['childSectionID'] = $sectionElement->SectionID;
	        		}
        		}
        	}
	        //@JAPR 2014-06-17: Agregado el soporte de filtrado de secciones Inline a partir de una pregunta previa del mismo catálogo
        	elseif ($sectionElement->SectionType == sectInline) {
        		if ($sectionElement->CatalogID > 0) {
        			if (!isset($arrSectionsByCatalog[$sectionElement->CatalogID])) {
			        	$arrSectionsByCatalog[$sectionElement->CatalogID] = array();
        			}
        			$arrSectionsByCatalog[$sectionElement->CatalogID][$sectionElement->SectionID] = $sectionElement->SectionID;
			        $arrSectionFound[$sectionElement->SectionID] = false;
			        $arrFatherQuestionID[$sectionElement->SectionID] = 0;
			        $arrFatherQuestionSectionID[$sectionElement->SectionID] = 0;
        		}
        	}
        	//@JAPR
        	
	        $arrDef['sectionsOrder'][] = $sectionElement->SectionID;
	        $arrDef['questionsOrder'] = array_merge($arrDef['questionsOrder'], $arrDef['sections'][$sectionElement->SectionID]['questionsOrder']);
			//@JAPR 2012-11-24: Modificado para permitir que las preguntas numéricas de catálogo sean evaluadas al cambiar a la pregunta padre
			//en cualquier tipo de sección
			foreach ($arrDef['sections'][$sectionElement->SectionID]['questions'] as $intQuestionID => $aQuestionDef) {
				$arrAllQuestions[$intQuestionID] = $aQuestionDef;
				//@JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
				//Por optimización se generará para modo diseño el Array de preguntas en este punto, lo cual no es necesario ya que el App lo hace en addClassPrototype
				if ($gbDesignMode) {
					$arrDef['questions'][$intQuestionID] = $aQuestionDef;
				}
				//@JAPR
			}
        }
        
        //@JAPR 2014-06-17: Agregado el soporte de filtrado de secciones Inline a partir de una pregunta previa del mismo catálogo
        //Basado en el mismo principio que el ciclo anterior, recorre las secciones identificando las preguntas que serán las padres de las secciones
        //inline apoyado en los Arrays, al final sobreescribirá las propiedades requeridas en las preguntas y en la sección correspondientes
        foreach ($sectionCollection->Collection as $sectionElement) {
        	//Al llegar a una sección Inline la marca como ya procesada, lo que significaría que ya debe asignar su padre si es que ya estaba 
        	//identificado, de lo contrario ya no tendrá que hacerlo con ninguna pregunta posterior del mismo catálogo pues no la debería afectar 
        	//(ni tampoco la sección afectaría a dicha pregunta)
        	if ($sectionElement->SectionType == sectInline) {
        		if (isset($arrSectionFound[$sectionElement->SectionID])) {
        			$arrSectionFound[$sectionElement->SectionID] = true;
        			$intFatherQuestionID = (int) @$arrFatherQuestionID[$sectionElement->SectionID];
        			$intFatherQuestionSectionID = (int) @$arrFatherQuestionSectionID[$sectionElement->SectionID];
	        		if ($intFatherQuestionID > 0 && $intFatherQuestionSectionID > 0) {
			        	$arrDef['sections'][$sectionElement->SectionID]['fatherQuestionID'] = $intFatherQuestionID;
			        	
		        		if (isset($arrDef['sections'][$intFatherQuestionSectionID]['questions'][$intFatherQuestionID])) {
		        			$arrDef['sections'][$intFatherQuestionSectionID]['questions'][$intFatherQuestionID]['childSectionID'] = $sectionElement->SectionID;
		        		}
	        		}
        		}
        	}
        	
        	//Las preguntas padre sólo pueden venir de una sección estándar previa, así que sólo llena los arrays en esos casos
        	if ($sectionElement->SectionType != sectNormal) {
        		continue;
        	}
        	
        	foreach ($arrDef['sections'][$sectionElement->SectionID]['questions'] as $intQuestionID => $aQuestionDef) {
        		$intCatalogID = (int) @$aQuestionDef['catalogID'];
        		if ($intCatalogID <= 0) {
        			continue;
        		}
	        	if (!isset($arrSectionsByCatalog[$intCatalogID])) {
	        		continue;
	        	}
	        	
    			$intFatherQuestionID = $intQuestionID;
    			$intFatherQuestionSectionID = $sectionElement->SectionID;
	        	$arrSectionIDs = @$arrSectionsByCatalog[$intCatalogID];
	        	foreach ($arrSectionIDs as $intSectionID) {
	        		if ($arrSectionFound[$intSectionID]) {
	        			continue;
	        		}
	        		
			        $arrFatherQuestionID[$intSectionID] = $intFatherQuestionID;
			        $arrFatherQuestionSectionID[$intSectionID] = $intFatherQuestionSectionID;
	        	}
        	}
        }
		
		//@JAPR 2012-11-24: Modificado para permitir que las preguntas numéricas de catálogo sean evaluadas al cambiar a la pregunta padre
		//en cualquier tipo de sección
		$arrAttributesByCatalog = array();
        foreach ($sectionCollection->Collection as $sectionElement) {
			//Recorre la lista de preguntas de la sección para modificar la fórmula default de las preguntas abiertas numéricas que sean de catálogo
			//para que apunten hacia la pregunta simple choice que las debería modificar. En este proceso se dejará la fórmula de la última pregunta
			//simple choice ya que las secciones se están recorriendo en orden y todas ellas tienen referencia a la misma pregunta abierta, y esto
			//es correcto porque en el App al cambiar cualquiera de los padres de todas maneras cambia a las preguntas simple choice hijas en cadena
			foreach ($arrDef['sections'][$sectionElement->SectionID]['questionsOrder'] as $intPos => $intQuestionID) {
				$aQuestionDef = @$arrAllQuestions[$intQuestionID];
				if (is_null($aQuestionDef)) {
					continue;
				}
				
				if ($aQuestionDef['type'] == qtpSingle && $aQuestionDef['useCatalog'] && $aQuestionDef['catalogID'] > 0) {
					//Es una pregunta simple choice de catálogo, verificamos si tiene asignadas hijas de fórmulas que sean abiertas
					foreach ($aQuestionDef['formulaChildren'] as $intChildQuestionID) {
						$aChildQuestionDef = @$arrAllQuestions[$intChildQuestionID];
						if (!is_null($aChildQuestionDef)) {
							switch ($aChildQuestionDef['type']) {
								case qtpOpenNumeric:
									if ($aChildQuestionDef['useCatalog'] && $aChildQuestionDef['catalogID'] == $aQuestionDef['catalogID']) {
										$intCatalogID = $aChildQuestionDef['catalogID'];
										$intCatMemberOrder = 0;
										$intCatMemberID = (int) @$aChildQuestionDef['catMemberID'];
										if ($intCatMemberID > 0) {
											require_once('catalog.inc.php');
											
											//Debe verificar el número de atributo al que corresponde el de esta pregunta
											if (!isset($arrAttributesByCatalog[$intCatalogID])) {
												$arrAttributesByCatalog[$intCatalogID] = BITAMCatalog::GetCatalogAttributes($this->Repository, $intCatalogID);
											}
											$intCatMemberOrder = (int) @$arrAttributesByCatalog[$intCatalogID][$intCatMemberID]['order'];
										}
										
										if ($intCatMemberOrder > 0) {
											//Es una pregunta válida, modifica su fórmula, la cual debió haber estado vacia porque este tipo de
											//preguntas no permite capturar fórmula al día de esta modificación
											$arrDef['sections'][$aChildQuestionDef['sectionID']]['questions'][$aChildQuestionDef['id']]['formula'] = "{@Q".$aQuestionDef['number']."(".$intCatMemberOrder.")}";
										}
									}
									break;
							}
						}
					}
				}
			}
        }
        
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (getMDVersion() >= esvEntryDescription) {
			$strEntryDescription = '';
			$aQuestionDef = @$arrAllQuestions[$this->QuestionIDForEntryDesc];
			if ($aQuestionDef && $aQuestionDef['number']) {
				$strEntryDescription = '{$Q'.$aQuestionDef['number'].'.value}';
				//Asigna la propiedad en la pregunta que le indica que será una pregunta que modifique la descripción de la captura
				$intQuestionID = (int) @$aQuestionDef['id'];
				$intSectionID = (int) @$aQuestionDef['sectionID'];
				if ($intQuestionID && $intSectionID) {
					if ($gbDesignMode) {
						if (isset($arrDef['questions'][$intQuestionID])) {
							$arrDef['questions'][$intQuestionID]['evaluateEntryDesc'] = true;
						}
					}
					if (isset($arrDef['sections'][$intSectionID]['questions'][$intQuestionID])) {
						$arrDef['sections'][$intSectionID]['questions'][$intQuestionID]['evaluateEntryDesc'] = true;
					}
				}
			}
			$arrDef['entryDescriptionExpr'] = $strEntryDescription;
			if ($gbDesignMode) {
				$arrDef['questionIDForEntryDesc'] = $this->QuestionIDForEntryDesc;
			}
		}
		
		if (getMDVersion() >= esvReloadSurveyFromSection) {
			$arrDef['reloadSurveyFromSection'] = $this->ReloadSurveyFromSection;
		}
		//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
		//Colección de filtros dinámicos por catálogo
        $objSurveyFilterCollection = BITAMSurveyFilterCollection::NewInstance($this->Repository, $this->SurveyID);
		//@JAPR 2015-09-18: Corregido un bug, se debe enviar siempre la colección aunque sea vacía, ya que en caso de remover el filtro no lo actualizaría en el App
		//si ya se encontraba cargada la forma en memoria (tanto en browser como en dispositivo) (#BJOHPQ)
		$arrDef['dynamicCatFilters'] = array();
        if (count($objSurveyFilterCollection->Collection) > 0) {
	        foreach ($objSurveyFilterCollection->Collection as $objSurveyFilter) {
	        	$arrDef['dynamicCatFilters'][$objSurveyFilter->CatalogID] = $objSurveyFilter->getJSonDefinition();
	        }
        }
		//@JAPR 2015-10-05: Agregado un filtro dinámico automático en base al radio para las preguntas tipo Mapa
		//OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA 
		//Se comenta la parte del radio ya que ahora habrá un radio por default en la configuración
		foreach ($arrAllQuestions as $intQuestionID => $aQuestionDef) {
			//$intMapRadio = (int) @$aQuestionDef['mapRadio'];
			if (is_null($aQuestionDef) || (int) @$aQuestionDef['type'] != qtpSingle || !((int) @$aQuestionDef['useMap'])/* || $intMapRadio <= 0*/) {
				continue;
			}
			
			$intCatalogID = (int) @$aQuestionDef['catalogID'];
			$intDataSourceID = (int) @$aQuestionDef['dataSourceID'];
			//$intCatMemberLatitudeID = (int) @$aQuestionDef['dataMemberLatitudeID'];
			//$intCatMemberLongitudeID = (int) @$aQuestionDef['dataMemberLongitudeID'];
			if ($intCatalogID <= 0 || $intDataSourceID <= 0 /*|| $intCatMemberLatitudeID <= 0 || $intCatMemberLongitudeID <= 0*/) {
				continue;
			}

			if (isset($arrDef['dynamicCatFilters'][$intCatalogID])) {
				continue;
			}

			//$strDynamicFilter = "Geolocalization(@DMID{$intCatMemberLatitudeID}, @DMID{$intCatMemberLongitudeID}, {@gps(Latitude)}, {@gps(Longitude)}) < {$intMapRadio}";
			
			$objSurveyFilter = BITAMSurveyFilter::NewInstance($this->Repository, $this->SurveyID);
			if (!is_null($objSurveyFilter)) {
				//$objSurveyFilter->FilterText = $strDynamicFilter;
				$objSurveyFilter->FilterText = "1=1";
				$objSurveyFilter->CatalogID = $intCatalogID;
				$objSurveyFilter->DataSourceID = $intDataSourceID;
				$arrDef['dynamicCatFilters'][$intCatalogID] = $objSurveyFilter->getJSonDefinition();
			}
		}
		
		//@JAPR 2016-01-12: Modificado para ejecutar los filtros dinámicos dentro de la misma sección al contestar las preguntas de la que depende (#Z8FCWT)
		//Recorre todos los filtros dinámicos de la forma, identificando el catálogo asociado a cada uno, posteriormente recorre todas las preguntas para
		//identificar cuales son de tipo catálogo y que compartan el catálogo de este filtro, porque significará que es el filtro que le pertenece, una vez
		//identificado eso, obtiene la referencia a las preguntas que afectan al filtro y verifica si son o no parte de la misma sección, si lo son, entonces
		//agrega a la pregunta del filtro como dependencia de las preguntas que forman su filtro dinámico
		$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
		$charToErase = array("{","}","@","Q","$");
		$replaceWith = array("","","","","");
		foreach ($arrDef['dynamicCatFilters'] as $intCatalogID => $arrDynamicCatFilter) {
			$strFilterText = (string) @$arrDynamicCatFilter['filterText'];
			if (trim($strFilterText) == '') {
				continue;
			}
			
			//El filtro dinámico se debe procesar en base a los IDs de las preguntas, así que hace la conversión
			$strFilterText = $this->TranslateVariableQuestionNumbersByIDs($strFilterText);
			
			//Busca las preguntas del catálogo de este filtro dinámico para identificar sus componentes, a la fecha de implementación sólo podía haber una
			//pregunta por filtro dinámico, pero se dejará la posibilidad de tener múltiples preguntas en el futuro
			foreach ($arrAllQuestions as $intQuestionID => $aQuestionDef) {
				//@JAPR 2016-05-31: Corregido un bug, no se habían considerado las preguntas múltiple choice de catálogo para esta funcionalidad, así que no estaban asignando la
				//propiedad filterChildren a las preguntas de las que dependían los filtros dinámicos de las múltiple choice y por tanto no se encadenaban (#VOPSK0)
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Check Question for Dynamic filter, Question's catalog: ".(int) @$aQuestionDef['catalogID'].", Filter's catalog: {$intCatalogID}");
				}
				
				if (is_null($aQuestionDef) || ((int) @$aQuestionDef['type'] != qtpSingle && (int) @$aQuestionDef['type'] != qtpMulti) || ((int) @$aQuestionDef['catalogID']) != $intCatalogID) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Check Question for Dynamic filter: Invalid question");
					}
					continue;
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Check Question for Dynamic filter: Valid question: {$intQuestionID} => ".(int) @$aQuestionDef['type']);
				}
				
				//Sólo se pueden agregar las preguntas de la misma sección, ya que las de otras secciones habrían forzado a un cambio de sección que
				//automáticamente habría ejecutado los filtros dinámicos, así que no se necesitan agregar a esta colección
				$intSectionID = (int) @$aQuestionDef['sectionID'];
				$intQuestionNum = (int) @$aQuestionDef['number'];
				//@JAPR 2016-06-01: Corregido un bug, no se estaba considerando el caso de las preguntas que si bien no son de la misma sección, pertenecen a una sección
				//tipo tabla que está dentro de una pregunta tipo tabla, la cual si está en la misma sección que la pregunta que la modifica (#Z8FCWT)
				$intContainerSectionID = 0;
				$intContainerQuestionNum = 0;
				$objSectionDef = @$arrDef['sections'][$intSectionID];
				if (!is_null($objSectionDef)) {
					if ($objSectionDef['type'] == sectInline && $objSectionDef['displayMode'] == sdspInline) {
						//Es una sección inline tabla, así que debe verificar si está o no contenida en alguna pregunta tipo tabla, si es así, es la sección de la pregunta tabla la
						//que se usará para determinar si hay encadenamiento de refresh de filtros dinámicos
						foreach ($arrAllQuestions as $intTableQuestionID => $aTableQuestionDef) {
							$intSourceSectionID = (int) @$aTableQuestionDef['sourceSectionID'];
							if (@$aTableQuestionDef['type'] == qtpSection && $intSourceSectionID > 0 && $intSourceSectionID == $intSectionID) {
								$intContainerQuestionNum = (int) @$aTableQuestionDef['number'];
								$intContainerSectionID = $aTableQuestionDef['sectionID'];
								break;
							}
						}
					}
				}
				//@JAPR
				
				//Identifica las preguntas que son componentes que modifican el filtro dinámico que se está procesando para agregarlas al array
				if (preg_match_all($pattern, $strFilterText, $arrayEvalQuestionsAdv)) {
					if (isset($arrayEvalQuestionsAdv[0]) && count($arrayEvalQuestionsAdv[0]) > 0) {
						$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
						array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
						$arrayQuestions = array_unique($arrayQuestionsAdv);
						$arrayQuestionIDs = str_ireplace($charToErase,$replaceWith,$arrayQuestions);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Check Question for Dynamic filter: Matched questions: ");
							PrintMultiArray($arrayQuestionIDs);
						}
						
						foreach ($arrayQuestionIDs as $intOpQuestionID) {
							$anOpQuestionDef = @$arrAllQuestions[$intOpQuestionID];
							//@JAPR 2016-06-01: Corregido un bug, no se estaba considerando el caso de las preguntas que si bien no son de la misma sección, pertenecen a una sección
							//tipo tabla que está dentro de una pregunta tipo tabla, la cual si está en la misma sección que la pregunta que la modifica (#Z8FCWT)
							$blnValid = false;
							$intSectionIDToUse = 0;
							if (!is_null($anOpQuestionDef)) {
								$blnValid = ($intSectionID == ((int) @$anOpQuestionDef['sectionID']) && $intOpQuestionID != $intQuestionID);
								
								//Verifica si la pregunta es de una sección tabla que se encuentre dentro de una pregunta tabla de la misma sección
								if ($blnValid) {
									$intSectionIDToUse = $intSectionID;
								}
								else {
									$blnValid = ($intContainerSectionID == (int) @$anOpQuestionDef['sectionID'] && $intContainerQuestionNum > (int) @$anOpQuestionDef['number']);
									if ($blnValid) {
										$intSectionIDToUse = $intContainerSectionID;
									}
								}
							}
							
							//Sólo si es una pregunta válida de la misma sección y previa a la del filtro entonces la agrega al array de dependencias
							if ($blnValid) {
								if (!isset($arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['filterChildren'])) {
									$arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['filterChildren'] = array();
								}
								$arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['filterChildren'][] = $intQuestionID;
							}
							//@JAPR
						}
					}
				}
			}
			
			//@JAPR 2016-07-15: Modificado para ejecutar los filtros dinámicos dentro de la misma sección al contestar las preguntas de la que depende (#B59I3K)
			//Busca las secciones del catálogo de este filtro dinámico para identificar sus componentes, a la fecha de implementación sólo podía haber una
			//sección por filtro dinámico, pero se dejará la posibilidad de tener múltiples secciones en el futuro
			foreach ($arrDef['sections'] as $intSectionID => $objSectionDef) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Check Section for Dynamic filter, Section's catalog: ".(int) @$objSectionDef['catalogID'].", Filter's catalog: {$intCatalogID}");
				}
				
				if (is_null($objSectionDef) || (int) @$objSectionDef['type'] != sectInline || (int) @$objSectionDef['displayMode'] != sdspInline || ((int) @$objSectionDef['catalogID']) != $intCatalogID || !((int) @$objSectionDef['showAsQuestion'])) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Check Section for Dynamic filter: Invalid section");
					}
					continue;
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Check Section for Dynamic filter: Valid section: {$intSectionID} => ".(int) @$aQuestionDef['type']);
				}
				
				//Sólo se pueden agregar las preguntas de la misma sección, ya que las de otras secciones habrían forzado a un cambio de sección que
				//automáticamente habría ejecutado los filtros dinámicos, así que no se necesitan agregar a esta colección
				//$intSectionID = (int) @$aQuestionDef['sectionID'];
				//$intQuestionNum = (int) @$aQuestionDef['number'];
				//@JAPR 2016-06-01: Corregido un bug, no se estaba considerando el caso de las preguntas que si bien no son de la misma sección, pertenecen a una sección
				//tipo tabla que está dentro de una pregunta tipo tabla, la cual si está en la misma sección que la pregunta que la modifica (#Z8FCWT)
				$intContainerSectionID = 0;
				$intContainerQuestionNum = 0;
				//$objSectionDef = @$arrDef['sections'][$intSectionID];
				if (!is_null($objSectionDef)) {
					if ($objSectionDef['type'] == sectInline && $objSectionDef['displayMode'] == sdspInline) {
						//Es una sección inline tabla, así que debe verificar si está o no contenida en alguna pregunta tipo tabla, si es así, es la sección de la pregunta tabla la
						//que se usará para determinar si hay encadenamiento de refresh de filtros dinámicos
						foreach ($arrAllQuestions as $intTableQuestionID => $aTableQuestionDef) {
							$intSourceSectionID = (int) @$aTableQuestionDef['sourceSectionID'];
							if (@$aTableQuestionDef['type'] == qtpSection && $intSourceSectionID > 0 && $intSourceSectionID == $intSectionID) {
								$intContainerQuestionNum = (int) @$aTableQuestionDef['number'];
								$intContainerSectionID = $aTableQuestionDef['sectionID'];
								break;
							}
						}
					}
				}
				//@JAPR
				
				//Identifica las preguntas que son componentes que modifican el filtro dinámico que se está procesando para agregarlas al array
				if (preg_match_all($pattern, $strFilterText, $arrayEvalQuestionsAdv)) {
					if (isset($arrayEvalQuestionsAdv[0]) && count($arrayEvalQuestionsAdv[0]) > 0) {
						$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
						array_walk($arrayQuestionsAdv, 'replaceNewVariableParams');
						$arrayQuestions = array_unique($arrayQuestionsAdv);
						$arrayQuestionIDs = str_ireplace($charToErase,$replaceWith,$arrayQuestions);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Check Question for Dynamic filter: Matched questions: ");
							PrintMultiArray($arrayQuestionIDs);
						}
						
						foreach ($arrayQuestionIDs as $intOpQuestionID) {
							$anOpQuestionDef = @$arrAllQuestions[$intOpQuestionID];
							//@JAPR 2016-06-01: Corregido un bug, no se estaba considerando el caso de las preguntas que si bien no son de la misma sección, pertenecen a una sección
							//tipo tabla que está dentro de una pregunta tipo tabla, la cual si está en la misma sección que la pregunta que la modifica (#Z8FCWT)
							$blnValid = false;
							$intSectionIDToUse = 0;
							if (!is_null($anOpQuestionDef)) {
								//En este caso para ser dependencia válida, la sección contenedora de la sección que corresponde a este filtro, es la que debe ser igual a la sección
								//de esta pregunta donde se asignará la dependencia
								//$blnValid = ($intSectionID == ((int) @$anOpQuestionDef['sectionID']) && $intOpQuestionID != $intSectionID);
								
								//Verifica si la pregunta es de una sección tabla que se encuentre dentro de una pregunta tabla de la misma sección
								if ($blnValid) {
									$intSectionIDToUse = $intSectionID;
								}
								else {
									$blnValid = ($intContainerSectionID == (int) @$anOpQuestionDef['sectionID'] && $intContainerQuestionNum > (int) @$anOpQuestionDef['number']);
									if ($blnValid) {
										$intSectionIDToUse = $intContainerSectionID;
									}
								}
							}
							
							//Sólo si es una pregunta válida de la misma sección y previa a la del filtro entonces la agrega al array de dependencias
							if ($blnValid) {
								if (!isset($arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['sectionFilterChildren'])) {
									$arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['sectionFilterChildren'] = array();
								}
								$arrDef['sections'][$intSectionIDToUse]['questions'][$intOpQuestionID]['sectionFilterChildren'][] = $intSectionID;
							}
							//@JAPR
						}
					}
				}
			}
			//@JAPR
		}
		//@JAPR
        
        if (getMDVersion() >= esvRecapDynamicData && $appVersion >= esvRecapDynamicData) {
        	//Colección de los filtros dinámicos de secciones tipo Recap, o bien, simplemente cada sección Recap aún sin filtro generará uno
        	//vacio para que el proceso descargue todos sus datos tal como existan en eBavel, ya que al final se ejecuta en el mismo punto
	        $arrDef['sectionDataFilters'] = array();
        	foreach ($sectionCollection->Collection as $sectionElement) {
        		$aSectionDef = @$arrDef['sections'][$sectionElement->SectionID];
        		if (is_null($aSectionDef) || $sectionElement->SectionType != sectRecap || $sectionElement->SectionFormID <= 0) {
        			continue;
        		}
        		
        		//Obtiene los filtros dinámicos si es que existe alguno
	        	$objSectionFilterCollection = BITAMSectionFilterCollection::NewInstance($this->Repository, $sectionElement->SectionID);
	        	if (is_null($objSectionFilterCollection) || count($objSectionFilterCollection->Collection) == 0) {
	        		//En este caso no hay filtros dinámicos, pero se verifica si la sección aún así debe descargar datos de eBavel, si es así
	        		//entonces crea una instancia de filtro dinámico pero sin filtro, simplemente para que descargue todos los datos
	        		$objSectionFilter = BITAMSectionFilter::NewInstance($this->Repository, $sectionElement->SectionID);
	        		if (!is_null($objSectionFilter)) {
	        			$arrDef['sectionDataFilters'][$objSectionFilter->SectionID] = $objSectionFilter->getJSonDefinition();
	        		}
	        	}
	        	else {
	        		//En este caso se utiliza directamente el filtro dinámico
			        foreach ($objSectionFilterCollection->Collection as $objSectionFilter) {
			        	$arrDef['sectionDataFilters'][$objSectionFilter->SectionID] = $objSectionFilter->getJSonDefinition();
			        }
	        	}
        	}
        }
		//@JAPR
		
        if (getMDVersion() >= esvAgendaRedesign) {
        	$arrDef['isAgendable'] = $this->IsAgendable;
        	$arrDef['radioForAgenda'] = $this->RadioForAgenda;
			
			$arrDef['cancelAgendaQuestionIDs'] = array();
			foreach ($arrAllQuestions as $qID => $aQuestion) {
				//var_dump($aQuestion);
				//exit();
				foreach ($aQuestion['options'] as $optionKey => $qOption) {
					//var_dump($qOption);
					//exit();
					if (isset($qOption['cancelAgenda']) && $qOption['cancelAgenda']) {
						if (!isset($arrDef['cancelAgendaQuestionIDs'][$qID])) {
							$arrDef['cancelAgendaQuestionIDs'][$qID] = array();
						}
						array_push($arrDef['cancelAgendaQuestionIDs'][$qID], $optionKey);
					}
				}
			}
			if (count($arrDef['cancelAgendaQuestionIDs']) == 0) {
				unset($arrDef['cancelAgendaQuestionIDs']);
			}
        }
		return $arrDef;
	}
}

class BITAMSurveyCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfSurveyIDs = null, $exceptInactive=true)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$where = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$aSurveyID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.SurveyID IN (".$where.")";
					}
					break;
			}
		}
		
		if($exceptInactive==true)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			else 
			{
				$where.=" WHERE ";
			}
			
			$where.=" t1.Status <> ".svstInactive;
		}

        //@LROUX 2011-10-18: Se agrega AllowPartialSave.
		//@JAPR 2012-03-28: Agregada la versión de definición de las encuestas (campo VersionNum)
		//@JAPR 2012-05-15: Agregadas las preguntas tipo Action (Campo ElementQuestionID, eBavelAppID, eBavelFormID)
		//@JAPR 2012-05-29: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas (campos EnableReschedule, RescheduleCatalogID, RescheduleCatMemberID y RescheduleStatus)
		//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
		//@JAPR 2012-11-02: Agregada la opción para sincronizar al momento de grabar la encuesta (Campo SyncWhenSaving)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvExtendedActionsData) {
			$strAdditionalFields .= ', t1.SyncWhenSaving';
		}
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		if (getMDVersion() >= esveFormsStdQSingleRec) {
			$strAdditionalFields .= ', t1.UseStdSectionSingleRec';
		}
		//@JAPR 2013-01-18: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		if (getMDVersion() >= esvCatalogDissociation) {
			$strAdditionalFields .= ', t1.DissociateCatDimens';
		}
		//@JAPR 2013-05-17: Agregada la opción para renombrar etiquetas de ciertas opciones
		if (getMDVersion() == esvCustomOtherComentLabelv403 || getMDVersion() >= esvCustomOtherComentLabelv404) {
			$strAdditionalFields .= ', t1.OtherLabel, t1.CommentLabel';
		}
		//@JAPR 2013-06-28: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$strAdditionalFields .= ', t1.eBavelActionFormID';
		}
		//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
		if (getMDVersion() >= esvAccuracyValues) {
			$strAdditionalFields .= ', t1.AccuracyAttribID, t1.AccuracyIndID';
		}
		//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
		if (getMDVersion() >= esvReportEMailsByCatalog) {
			$strAdditionalFields .= ', t1.AdditionalEMails';
		}
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', t1.SurveyImageText';
		}
		//@GCRUZ 2015-08-21: Agregado el icono para el menu de la encuesta
		if (getMDVersion() >= esvMenuImageConfig) {
			$strAdditionalFields .= ', t1.SurveyMenuImage';
		}
		//@JAPR 2014-05-05: Agregada la opción para ocultar los botones de navegación
		//@JAPR 2014-05-07: Agregada la opción para ocultar los nombres de sección
		if (getMDVersion() >= esvHideNavigationButtons) {
			$strAdditionalFields .= ', t1.HideNavButtons, t1.HideSectionNames';
		}
		//@JAPR 2014-08-15: Agregada la opción para deshabilitar la captura de encuestas
		if (getMDVersion() >= esvSurveySectDisableOpts) {
			$strAdditionalFields .= ', t1.DisableEntry';
		}
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		if (getMDVersion() >= esvExtendedModifInfo) {
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			$strAdditionalFields .= ', t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', t1.IsAgendable';
			$strAdditionalFields .= ', t1.RadioForAgenda';
		}
		//@JAPR 2014-11-18: Agregada la dimensión Agenda
		if (getMDVersion() >= esvAgendaDimID) {
			$strAdditionalFields .= ', t1.AgendaDimID';
		}
		//@JAPR 2015-06-22: Rediseñado el Admin con DHTMLX
		if (getMDVersion() >= esvAdminWYSIWYG) {
			$strAdditionalFields .= ', t1.SchedulerID, t1.SurveyDesc';
		}
		//@JAPR
		//GCRUZ 2016-01-15 Agregada configuración para nombre de archivo incremental
		if (getMDVersion() >= esvDestinationIncremental) {
			$strAdditionalFields .= ', t1.incrementalFile';
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', t1.Width, t1.Height, t1.TextPosition, t1.TextSize, t1.CustomLayout';
		}
		//@JAPR
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			$strAdditionalFields .= ', t1.ShowQNumbers';
		}
		//JAPR 2016-03-22: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$strAdditionalFields .= ', t1.TemplateID';
		}
		//@JAPR 2016-05-27: Agregada la duración planeada de la captura, usada para el cubo de Agendas
		if (getMDVersion() >= esvFormsDuration) {
			$strAdditionalFields .= ', t1.PlannedDuration';
		}
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$strAdditionalFields .= ', t1.EnableNavHistory';
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$strAdditionalFields .= ', t1.AutoFitImage';
		}
		//@JAPR 2017-02-08: Agregado el proceso de generación de forma de desarrollo para sincronizar a forma de producción (#N5ZGDX)
		if (getMDVersion() >= esvCopyForms) {
			$strAdditionalFields .= ', t1.SourceID, t1.FormType';
		}
		//@JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		if (getMDVersion() >= esvEntryDescription) {
			$strAdditionalFields .= ', t1.QuestionIDForEntryDesc';
		}
		if ( getMDVersion() >= esvReloadSurveyFromSection ) {
			$strAdditionalFields .= ', t1.ReloadSurveyFromSection';
		}
		//@JAPR 2016-07-25: Corregido un bug, se estaba permitiendo capturar nombres con demasiados espacios en blanco que la interfaz no presentaba, se elimiarán (#070KU7)
		$sql = "SELECT t1.SurveyID, t1.SurveyName, t1.Status, t1.ModelID, t1.ClosingMsg, t1.AnsweredQuestionsIndID, t1.CountIndID, t1.DurationIndID, t1.LatitudeIndID, t1.LongitudeIndID, t1.UserDimID, 
				t1.SectionDimID, t1.CaptureStartTime, t1.CaptureEndTime, t1.HelpMsg, t1.CatalogID, t1.SurveyType, t1.HasSignature, t1.AllowPartialSave, t1.EmailDimID, t1.StatusDimID, t1.SchedulerDimID, 
				t1.StartDateDimID, t1.EndDateDimID, t1.StartTimeDimID, t1.EndTimeDimID, t1.SyncDateDimID, t1.SyncTimeDimID, 
				t1.FactKeyDimID, t1.LatitudeAttribID, t1.LongitudeAttribID, t1.ActionIndInlineID, t1.VersionNum, t1.ElementQuestionID, t1.eBavelAppID, t1.eBavelFormID, t1.EnableReschedule, 
				t1.RescheduleCatalogID, t1.RescheduleCatMemberID, t1.RescheduleStatus $strAdditionalFields 
			FROM SI_SV_Survey t1 ".$where." 
			ORDER BY LTRIM(RTRIM(t1.SurveyName))";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
			//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			//@JAPR 2015-07-30: Validado que no aparezcan formas de versiones previas
			$dblCreationAdminVersion = (float) @$aRS->fields["creationversion"];
			if ($dblCreationAdminVersion >= esveFormsv6) {
				$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS, true);
			}
			//@JAPR
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Obtiene la colección de formas indicada en el array, cargando todos los elementos que dependen de ella a todos los niveles inferiores de manera masiva para
	optimizar el proceso y posteriormente agregarlos al caché para utilizarlos en las llamadas a los métodos de carga individuales, evitando así llamadas excesivas
	en cascada especialmente durante la obtención de los getJSonDefinition que se usan al obtener las definiciones para las Apps.
	Este proceso sólo se usó originalmente durante la obtención de definiciones para tener aislado el punto de uso a dicho lugar y no afectar en absoluto al resto
	de los procesos de eForms como los del Agente o el Administrador, eventualmente se podría extender a todo eForms */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null, $exceptInactive=true) {
		//La carga inicial de la colección de formas se hará mediante la función original de esta colección, la cual ya deja en caché a las formas
		$anInstance = self::NewInstance($aRepository, $anArrayOfSurveyIDs, $exceptInactive);
		
		if ( !is_null($anInstance) ) {
			//Las cargas subsecuentes de las colecciones de dependencias a todos los niveles se hará enviando la lista de formas obtenida, ya que son las formas
			//válidas que si se encontraron en la Metadata. Estas llamadas dejarán los elementos en los respectivos cachés tanto los originales ya existentes que
			//se asignan en los métodos NewInstanceFromRS, como los cachés indexados por colecciones de las llamadas a los distintos métodos de las colecciones
			//usadas en el proceso de generación de definiciones (getJSonDefinition, los cuales no se encontraban en caché)
			$arrSurveyIDs = array();
			foreach ($anInstance->Collection as $objSurvey) {
				$arrSurveyIDs[] = $objSurvey->SurveyID;
				//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
				//Marca a las formas como procesadas con carga masiva para que cualquier objeto que necesite cargar alguna información posteriormente pueda determinar si puede o no utilizar
				//los resultados de carga masiva correspondientes
				//Se agrega en este punto porque algunos arrays u objetos se cargarán antes de sus padres para que al cargar las instancias de los padres los métodos que los cargarían
				//de manera individual simplemente obtengan las referencias correspondientes (ej. los DataSourceMemberIDs de las Preguntas de selección)
				BITAMGlobalFormsInstance::AddSurveyFullyLoadedFromDB($objSurvey->SurveyID);
				//@JAPR
			}
			
			$sectionCollection = BITAMSectionCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			$sectionFilterCollection = BITAMSectionFilterCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			//Las opciones de respuesta se deben cargar primero, ya que al cargar cada pregunta se van a cargar sus propias opciones de respuesta, sin embargo
			//no se generarán elementos en caché para preguntas que no tienen opciones de respuesta, lo cual provocaría que al cargar las preguntas se tuvieran que
			//lanzar de nuevo las consultas para cargar las opciones de respuesta al no ser encontradas en caché, por tanto se tendrá que agregar un nuevo parámetro
			//durante la carga de las preguntas para indicar que el caché es obligatorio y si no se encuentra una colección de opciones de respuesta quiere decir
			//que no deberá intentar cargarla nuevamente desde la base de datos
			$questionOptionsCollection = BITAMQuestionOptionCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			$catMembersListCollection = BITAMQuestionCollection::LoadCatMembersListFromDBFull($aRepository, $arrSurveyIDs);
			$showConditionChildrenCollection = BITAMQuestionCollection::LoadShowConditionChildrenFromDBFull($aRepository, $arrSurveyIDs);
			$formulaChildrenCollection = BITAMQuestionCollection::LoadFormulaChildrenFromDBFull($aRepository, $arrSurveyIDs);
			$questionsToEvalCollection = BITAMQuestionCollection::LoadQuestionToEvalFromDBFull($aRepository, $arrSurveyIDs);
			//@JAPR
			$questionCollection = BITAMQuestionCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			$questionFilterCollection = BITAMQuestionFilterCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			$skipRulesColl = BITAMSkipRuleCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			$showQuestionsColl = BITAMShowQuestionCollection::NewInstanceFromDBFull($aRepository, $arrSurveyIDs);
			//@JAPR
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("copySurveysAction", $aHTTPRequest->GET) && array_key_exists("strSurveyIDs", $aHTTPRequest->GET))
		{	
			$strSurveyIDs = $aHTTPRequest->GET["strSurveyIDs"];
			
			ini_set("display_errors","0");
			BITAMSurveyCollection::copySurveys($aRepository, $strSurveyIDs);
			ini_set("display_errors","1");
		}

		//@JAPR 2015-04-15: Rediseñado el Admin con DHTMLX
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	static function copySurveys($aRepository, $strSurveyIDs)
	{
		$arraySurveyIDs = explode("|", $strSurveyIDs);
		
		$numSurveys = count($arraySurveyIDs);
		
		for($i=0; $i<$numSurveys; $i++)
		{
			BITAMSurvey::copySurvey($aRepository, $arraySurveyIDs[$i], false);
		}
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Surveys");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=SurveyCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Survey";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SurveyID';
	}

	function get_FormFieldName() {
		return 'SurveyName';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyName";
		$aField->Title = translate("Survey Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
 	function addButtons($aUser)
	{
		global $gbIsGeoControl;
		
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			if (count($this->Collection)>0)
			{
?>
			<!--<a href="#" id="copySurveyBtn" class="alinkescfav" onclick="copySurveys();"><img src="images/copy.gif" displayMe="1">&nbsp;<?=translate("Copy")?>&nbsp;&nbsp;</a>-->
			<button id="copySurveyBtn" class="alinkescfav" onclick="copySurveys();"><img src="images/copy.gif" alt="<?=translate("Copy")?>" title="<?=translate("Copy")?>" displayMe="1" /> <?=translate("Copy")?></button>
<?
			}
		}

 		if (!$gbIsGeoControl) {
?>
			<!--<a href="#" id="browserApp" class="alinkescfav" onclick="browserApp();"><img src="images/icon-72.png" width=16px height=16px displayMe="1">&nbsp;<?=translate("Data entry via Web")?>&nbsp;&nbsp;</a>-->
			<button id="browserApp" class="alinkescfav" onclick="browserApp();"><img src="images/dataEntry.gif" alt="<?=translate("Online Entry")?>" title="<?=translate("Online Entry")?>" displayMe="1" /> <?=translate("Online Entry")?></button>
<?
 		}
	}
	
	function generateBeforeFormCode($aUser)
 	{
 		//@JAPR 2014-06-11: Corregido un bug, la parte del script estaba condicionada a los Admins, así que para los normales se veía código
 		//en la pantalla en lugar de realmente descargar un script
?>
 		<script language="JavaScript">
<?
		//@JAPR
		if($_SESSION["PABITAM_UserRole"]==1)
		{
?>
 		function copySurveys()
		{
			var cont = 0;
			var elements = BITAMSurveyCollection_SelectForm.elements;
			var strSurveyIDs="";
			var numElements = elements.length;
			for (var i = 0; i < numElements; i++)
			{
				if (elements[i].name == "SurveyID[]" && elements[i].checked==true)
				{
					if(strSurveyIDs!="")
					{
						strSurveyIDs+="|";
					}
					strSurveyIDs+=elements[i].value;
					cont++;
				}
			}
			
			if (cont == 0)
			{
				 alert('<?= sprintf(translate("Please select which %s to copy"), translate("Surveys"))?>');
				 return;
			}
			
 			var answer = confirm('<?= translate("Do you want to copy all selected surveys?")?>');
	
			if(answer)
			{
				//Código para poder debuguear la copia de Surveys
				//document.getElementById("iFrameCopySurveys").style.display='inline';
				
				frmCopySurveys.strSurveyIDs.value = strSurveyIDs;
				frmCopySurveys.submit();
				
				/*
				var strFeaturesWindow = "dialogHeight:250px; dialogWidth:375px; center:yes; help:no; resizable:yes; status:no;"
				window.showModalDialog('dialog.html', "copySurveys.php?strSurveyIDs="+strSurveyIDs, strFeaturesWindow);
				document.location.reload();
				*/
			}
		}
<?
		}
		
?>
		function browserApp() {
<?
		require_once('settingsvariable.inc.php');
		$strAltEformsService = '';
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($this->Repository, 'ALTEFORMSSERVICE');
		if (!is_null($objSetting) && trim($objSetting->SettingValue != ''))
		{
			$strAltEformsService = $objSetting->SettingValue;
			if (substr($strAltEformsService, -1) != "/") {
				$strAltEformsService .= "/";
			}
			
			if (strtolower(substr($strAltEformsService, 0, 4)) != 'http') {
				$strAltEformsService = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$strAltEformsService;
			}
		}
?>			
			d = new Date();
			dteNow = '' + d.getFullYear() + d.getMonth() + d.getDay() + d.getHours() + d.getMinutes() + d.getSeconds();
			openWindow('GenerateSurvey.php?dte=' + dteNow);
		}
 		</script>
		
		<!--iframe para poder debuggear en el Zend la copia de encuestas-->
		<!--
		<iframe id="iFrameCopySurveys" name="iFrameCopySurveys" style="display:none" width="100%" height="300px"></iframe>
		<form name="frmCopySurveys" action="copySurveys.php" method="get" target="iFrameCopySurveys">
			<input type="hidden" id="strSurveyIDs" name="strSurveyIDs" value="">
		</form>
		-->
		<form name="frmCopySurveys" action="main.php" method="GET" target="body">
			<input type="hidden" name="BITAM_SECTION" value="SurveyCollection">
			<input type="hidden" name="copySurveysAction" id="copySurveysAction"> 
			<input type="hidden" id="strSurveyIDs" name="strSurveyIDs" value="">
		</form>
<?
 	}
}
?>