<?php 

require_once("repository.inc.php");
require_once("indicatorgroup.inc.php");
//require_once("indicator.inc.php");

class BITAMIndicatorGroupIndicatorCollection extends BITAMCollection
{
	public $IndicatorGroupID;
	
	function __construct($aRepository, $aCubeID)
	{
		BITAMCollection::__construct($aRepository);
		$this->IndicatorGroupID = $aCubeID;
	}

	static function NewIndicatorGroupIndicatorCollection($aRepository, $aCubeID, $anArrayOfIndicatorIDs = null)
	{
		$anInstance = new BITAMIndicatorGroupIndicatorCollection($aRepository, $aCubeID);

		$filter = "";
		if (!is_null($anArrayOfIndicatorIDs))
		{
			switch (count($anArrayOfIndicatorIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.CLA_INDICADOR = ".((int) $anArrayOfIndicatorIDs[0]);
					break;
				default:
					foreach ($anArrayOfIndicatorIDs as $aIndicatorID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aIndicatorID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.CLA_INDICADOR IN (".$filter.")";
					}
					break;
			}
		}

		$sql = 
		"SELECT t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		" 		,t4.CLA_CONCEPTO AS CubeID ".
		"		,t4.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t1.CLA_OWNER AS OwnerID".		
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".		
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".		
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".	
		"		,t1.IN_AGGREGATION as in_aggregation ".
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_GPO_IND_DET t3, SI_CONCEPTO t4 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER AND t1.TIPO_INDICADOR in (1,3,5) ".
		"  		AND t3.CLA_INDICADOR = t1.CLA_INDICADOR ".
		"  		AND t4.CLA_CONCEPTO = t1.CLA_CONCEPTO ".
		"  		AND  t3.CLA_GPO_IND = ".$aCubeID." ".$filter.
		" ORDER BY 3, 2";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicatorFromRS($anInstance->Repository, $aCubeID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewIndicatorGroupIndicatorCollectionForAssociation($aRepository, $aCubeID)
	{
		$anInstance = new BITAMIndicatorGroupIndicatorCollection($aRepository, $aCubeID);

		$sql = 
		"SELECT t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		"		,t4.CLA_CONCEPTO AS CubeID ".
		"		,t4.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t1.CLA_OWNER AS OwnerID ".
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".		
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".
		"		,t1.IN_AGGREGATION as in_aggregation ".		
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_CONCEPTO t4 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER AND t1.TIPO_INDICADOR in (1,3,5) ".
		"		and t4.CLA_CONCEPTO = t1.CLA_CONCEPTO ".
		"ORDER BY 3, 2";

		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicatorFromRS($anInstance->Repository, $aCubeID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewIndicatorGroupIndicatorCollectionForSaving($aRepository, $aCubeID, $anArrayOfIndicatorIDs)
	{
		$anInstance = new BITAMIndicatorGroupIndicatorCollection($aRepository, $aCubeID);

		$filter = "";
		switch (count($anArrayOfIndicatorIDs))
		{
			case 0:
				break;
			case 1:
				$filter = "AND t1.CLA_INDICADOR = ".((int) $anArrayOfIndicatorIDs[0]);
				break;
			default:
				foreach ($anArrayOfIndicatorIDs as $aIndicatorID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aIndicatorID; 
				}
				if ($filter != "")
				{
					$filter = "AND t1.CLA_INDICADOR IN (".$filter.")";
				}
				break;
		}

		$sql = 		
		"SELECT t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		"		,t4.CLA_CONCEPTO AS CubeID ".
		"		,t4.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t1.CLA_USUARIO AS OwnerID ".
		"		,t2.NOM_CORTO AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".		
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".		
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".		
		"		,t1.IN_AGGREGATION as in_aggregation ".
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_CONCEPTO t4 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER AND t1.TIPO_INDICADOR in (1,3,5) ".
		"		and t4.CLA_CONCEPTO = t1.CLA_CONCEPTO ".
		$filter.
		"  AND  t1.CLA_INDICADOR NOT IN ".
		"         (SELECT t3.CLA_INDICADOR ".
		"            FROM SI_GPO_IND_DET t3 ".
		"            WHERE  t3.CLA_GPO_IND = ".$aCubeID.") ".
		" ORDER BY 3, 2";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicatorFromRS($anInstance->Repository, $aCubeID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("IndicatorGroupID", $aHTTPRequest->GET))
		{
			$action = array_key_exists("action", $aHTTPRequest->GET) ? $aHTTPRequest->GET["action"] : "";
			$aCubeID = $aHTTPRequest->GET["IndicatorGroupID"];
			if (array_key_exists("IndicatorID", $aHTTPRequest->POST))
			{
				$aIndicatorID = $aHTTPRequest->POST["IndicatorID"];
				if (is_array($aIndicatorID))
				{
					$anArrayOfIndicatorIDs = $aIndicatorID;
					switch ($action)
					{
						case 'associate':
							$filter = "";
							switch (count($anArrayOfIndicatorIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_INDICADOR <> ".((int) $anArrayOfIndicatorIDs[0]);
									break;
								default:
									foreach ($anArrayOfIndicatorIDs as $aIndicatorID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aIndicatorID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_INDICADOR NOT IN (".$filter.")";
									}
									break;
							}
							$sql = "DELETE FROM SI_GPO_IND_DET WHERE CLA_GPO_IND = ".((int) $aCubeID).$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die("Error accessing SI_GPO_IND_DET table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
							}

							$aCollection = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollectionForSaving($aRepository, $aCubeID, $anArrayOfIndicatorIDs);
							foreach ($aCollection as $anInstanceToAssociate)
							{
								$anInstanceToAssociate->saveAssociation();
							}
							$aHTTPRequest->RedirectTo = $aCollection;
							
							ExportCubeToPHP($aRepository, -1, false, true);
							
							break;
						case 'dissociate':
							$filter = "";
							switch (count($anArrayOfIndicatorIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_INDICADOR = ".((int) $anArrayOfIndicatorIDs[0]);
									break;
								default:
									foreach ($anArrayOfIndicatorIDs as $aIndicatorID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aIndicatorID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_INDICADOR IN (".$filter.")";
									}
									break;
							}
							if ($filter != "")
							{
								$sql = "DELETE FROM SI_GPO_IND_DET WHERE CLA_GPO_IND = ".((int) $aCubeID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die("Error accessing SI_GPO_IND_DET table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
								}
							}
							$aCollection = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollection($aRepository, $aCubeID);
							$aHTTPRequest->RedirectTo = $aCollection;
							
							ExportCubeToPHP($aRepository, -1, false, true);
							
							break;
					}
					return null;
				}
			}
			$anInstance = null;
			switch ($action)
			{
				case 'associate':
				case 'dissociate':
					$aHTTPRequest->RedirectTo = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollection($aRepository, $aCubeID);
					break;
				default:
					$anInstance = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollectionForAssociation($aRepository, $aCubeID);
					break;
			}
			return $anInstance;
		}
		return null;
	}
	
	function get_AssociatedIDs()
	{
		$anArray = array();

		$sql = 
		"SELECT t1.CLA_INDICADOR AS IndicatorID, ".
		"		t1.NOM_INDICADOR AS IndicatorName ".
		"FROM 	SI_INDICADOR t1, SI_GPO_IND_DET t2 ".
		"WHERE	t2.CLA_INDICADOR = t1.CLA_INDICADOR ".
		"  AND  t2.CLA_GPO_IND = ".((int) $this->IndicatorGroupID).
		" ORDER BY 2";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anArray[(int) $aRS->fields["indicatorid"]] = strtoupper(rtrim($aRS->fields["indicatorname"]));
			$aRS->MoveNext();
		}
		
		return $anArray;
	}
	
	function get_Parent()
	{
		return BITAMIndicatorGroup::NewIndicatorGroupWithID($this->Repository, $this->IndicatorGroupID);
	}
	
	function get_Image()
	{
		return '<img src="images/indicadores.gif">';
	}
	
	function get_Title()
	{
		return translate("Indicators");
	}

	function get_QueryString()
	{
		return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=IndicatorGroupIndicatorCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=IndicatorGroupIndicator&IndicatorGroupID=".$this->IndicatorGroupID;
	}
	
	function get_AssociateDissociateQueryString()
	{
		return "BITAM_SECTION=IndicatorGroupIndicatorCollection&IndicatorGroupID=".$this->IndicatorGroupID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorID';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "IndicatorName";
		$aField->Title = translate("Indicator Name");
		$aField->Type = "String";
		$aField->Size = 15;
		$myFields[] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormulaUsrImg";
		$aField->Title = translate("Formula");
		$aField->Type = "String";
		$aField->Size = 15;
		$aField->Image = "FormulaImg";
		$myFields[] = $aField;		

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CubeName";
		$aField->Title = translate("Model");
		$aField->Type = "String";
		$aField->Size = 60;
		$myFields[] = $aField;

/*		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OwnerName";
		$aField->Title = translate("Owner");
		$aField->Type = "String";
		$aField->Size = 15;
		$myFields[] = $aField;		
*/		

		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
}

class BITAMIndicatorGroupIndicator extends BITAMObject
{
	public $IndicatorGroupID;
	public $Indicator;
	
	function __construct($aRepository, $aCubeID)
	{
		BITAMObject::__construct($aRepository);
		$this->IndicatorGroupID = (int) $aCubeID;
		$this->Indicator = BITAMIndicator::NewIndicator($aRepository);
	}

	static function NewIndicatorGroupIndicator($aRepository, $aCubeID)
	{
		return new BITAMIndicatorGroupIndicator($aRepository, $aCubeID);
	}

	static function NewIndicatorCubeWithIndicatorID($aRepository, $aCubeID, $aIndicatorID)
	{
		
		$anInstance = null;
		if (((int) $aCubeID) < 0)
		{
			return $anInstance;
		}
		if (((int) $aIndicatorID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		"SELECT t1.CLA_INDICADOR AS IndicatorID ".
		"		,t1.NOM_INDICADOR AS IndicatorName ".
		"		,t4.CLA_CONCEPTO AS CubeID ".
		"		,t4.NOM_CONCEPTO AS CubeName ".
		"		,t1.FORMULA_USR AS formula_usr ".
		"		,t1.FORMULA_BD AS formula_bd ".
		"		,t1.NOM_TABLA AS nom_tabla ". 
		"		,t1.LLAVES AS llaves ". 
		"		,t1.TIPO_AGRUPACION AS tipo_agrupacion ". 
		"		,t1.FORMATO AS formato ". 
		"		,t1.TIPO_INDICADOR AS tipo_indicador ". 
		"		,t1.CLA_USUARIO AS cla_usuario ". 
		"		,t1.REEXPRESADOS AS reexpresados ". 
		"		,t1.DESCRIPCION AS descripcion ". 
		"		,t1.FILTRO AS filtro ". 
		"		,t1.PARETTO AS paretto ". 
		"		,t1.TABLAS AS tablas ". 
		"		,t1.DONDE AS donde ". 
		"		,t1.DIMENSIONES AS dimensiones ". 
		"		,t1.NO_EJECUTIVO AS no_ejecutivo ". 
		"		,t1.ES_ATRIBUTO AS es_atributo ". 
		"		,t1.NIVEL_JERARQUIA AS nivel_jerarquia ". 
		"		,t1.ES_DIVISOR AS es_divisor ". 
		"		,t1.DIM_DEPEND AS dim_depend ". 
		"		,t1.CLA_OWNER	AS OwnerID ".
		"		,t2.NOM_CORTO	AS OwnerName ".
		"		,t1.DECREMENTAL AS decremental ".
		"		,t1.CLA_PERIODO as period_key ".
		"		,t1.ACTUAL as nactual ".
		"		,t1.ONLY_LAST_PERIOD as only_last_period ".
		"		,t1.FILTER_SQL as filter_sql ".
		"		,t1.NIVELES as niveles ".		
		"		,t1.IN_AGGREGATION as in_aggregation ".
		"FROM 	SI_INDICADOR t1, SI_USUARIO t2, SI_GPO_IND_DET t3, SI_CONCEPTO t4 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_OWNER AND t1.TIPO_INDICADOR in (1,3,5) ".
		"  AND  t3.CLA_INDICADOR = t1.CLA_INDICADOR ".
		"  AND  t4.CLA_CONCEPTO = t1.CLA_CONCEPTO ".
		"  AND  t3.CLA_GPO_IND = ".((int) $aCubeID).
		"  AND  t1.CLA_INDICADOR = ".((int) $aIndicatorID);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_INDICADOR table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicatorFromRS($aRepository, $aCubeID, $aRS);
		}
		return $anInstance;
	}

	static function NewIndicatorGroupIndicatorFromRS($aRepository, $aCubeID, $aRS)
	{
		$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicator($aRepository, $aCubeID);
		$anInstance->Indicator = BITAMIndicator::NewIndicatorFromRS($aRepository, $aRS);
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aIndicator)
	{
		if (array_key_exists("IndicatorGroupID", $aHTTPRequest->GET))
		{
			$aCubeID = (int) $aHTTPRequest->GET["IndicatorGroupID"];
			if (array_key_exists("IndicatorID", $aHTTPRequest->POST))
			{
				$aIndicatorID = $aHTTPRequest->POST["IndicatorID"];
				if (is_array($aIndicatorID))
				{
					$aCollection = BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollection($aRepository, $aCubeID, $aIndicatorID);
					foreach ($aCollection as $anInstanceToRemove)
					{
						$anInstanceToRemove->remove();
					}
					$aHTTPRequest->RedirectTo = $aCollection;
				}
				else
				{
					$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorCubeWithIndicatorID($aRepository, $aCubeID, $aIndicatorID);
					if (is_null($anInstance))
					{
						$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicator($aRepository, $aCubeID);
					}
					$anInstance->updateFromArray($aHTTPRequest->GET);
					$anInstance->updateFromArray($aHTTPRequest->POST);
					$anInstance->save();
					if (array_key_exists("go", $_POST))
					{
						$go = $_POST["go"];
					}
					else
					{
						$go = "self";
					}
					if ($go == "parent")
					{
						$anInstance = $anInstance->get_Parent();
					}
					else
					{
						if ($go == "new")
						{
							$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicator($aRepository, $aCubeID);
						}
					}
					$aHTTPRequest->RedirectTo = $anInstance;
				}
				return null;
			}
			$anInstance = null;
			if (array_key_exists("IndicatorID", $aHTTPRequest->GET))
			{
				$aIndicatorID = $aHTTPRequest->GET["IndicatorID"];
				$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorCubeWithIndicatorID($aRepository, $aCubeID, $aIndicatorID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicator($aRepository, $aCubeID);
				}
			}
			else
			{
				$anInstance = BITAMIndicatorGroupIndicator::NewIndicatorGroupIndicator($aRepository, $aCubeID);
			}
			return $anInstance;
		}
		return null;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("IndicatorGroupID", $anArray))
		{
			$this->IndicatorGroupID = (int) $anArray["IndicatorGroupID"];
		}
		$this->Indicator->updateFromArray($anArray);
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{
			$this->Indicator->save();
			$this->saveAssociation();
		}
		else
		{
			$this->Indicator->save();
		}
		return $this;
	}

	function saveAssociation()
	{
		$sql = "INSERT INTO SI_GPO_IND_DET (".
					"CLA_GPO_IND".
					",CLA_INDICADOR".
					") VALUES (".
					((int) $this->IndicatorGroupID).
					",".((int) $this->Indicator->IndicatorID).
					")";
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_GPO_IND_DET table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		return $this;
	}

	function remove()
	{		
		$sql = "DELETE FROM SI_GPO_IND_DET WHERE CLA_GPO_IND = ".((int) $this->IndicatorGroupID)." AND CLA_INDICADOR = ".((int) $this->Indicator->IndicatorID);
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_GPO_IND_DET table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		$this->Indicator->remove();
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->Indicator->IndicatorID < 0);
	}

	function get_Image()
	{
		return "<img src=images/indicadores.gif>";
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New Indicator");
		}
		else
		{
			return $this->Indicator->IndicatorName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=IndicatorGroupIndicator&IndicatorGroupID=".$this->IndicatorGroupID;
		}
		else
		{
			return "BITAM_PAGE=IndicatorGroupIndicator&IndicatorGroupID=".$this->IndicatorGroupID."&IndicatorID=".$this->Indicator->IndicatorID;
		}
	}

	function get_Parent()
	{
		return BITAMIndicatorGroupIndicatorCollection::NewIndicatorGroupIndicatorCollection($this->Repository, $this->IndicatorGroupID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'IndicatorID';
	}

	function get_FieldValueOf($aFieldName)
	{
		if ($aFieldName == 'IndicatorGroupID')
    	{
    		$aFieldValue = $this->get_FieldValueOf($aFieldName);
    	}
    	else
    	{
    		$aFieldValue = $this->Indicator->get_FieldValueOf($aFieldName);
    	}
		return $aFieldValue;
	}
	
	function get_Children($aUser)
	{
		return $this->Indicator->get_Children($aUser);
	}		

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		return $this->Indicator->get_FormFields($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->Indicator->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}	

}

?>