<?php
    //@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
    require_once("checkCurrentSession.inc.php");
    
    //Cargar diccionario del lenguage a utilizar
    if (array_key_exists("PAuserLanguage", $_SESSION))
    {
        InitializeLocale($_SESSION["PAuserLanguageID"]);
        LoadLanguageWithName($_SESSION["PAuserLanguage"]);
    }
    else 
    {   //English
        InitializeLocale(2);    
        LoadLanguageWithName("EN");
    }
    //@JAPR

    /*@AAL 25/02/2015: Se importan los archivos necesarios para obtener los datos que seran mostrados en el editor de fórmulas */
    require_once("survey.inc.php");
    require_once("question.inc.php");
    require_once("section.inc.php");
    require_once("dataSource.inc.php");
    require_once("config.php");
    require_once("utils.inc.php");

	//GCRUZ 2016-05-24
	require_once('dataSourceFilter.inc.php');

//	$filter = BITAMDataSourceFilter::generateResultFilterByUser($theRepository, 187, 64);
//	echo "<pre>";
//	var_dump($filter);
//	exit;


    /*@AAL 25/02/2015: Extraemos los parámetros que se enviaron desde el archivo principal */
    $SurveyID = $_GET["SurveyID"];
    $SectionID = (isset($_GET["SectionID"]) ? $_GET["SectionID"] : NULL);
    $QuestionID = (isset($_GET["QuestionID"]) ? $_GET["QuestionID"] : NULL);
    //@JAPR 2015-08-02: Agregada la lista de atributos para permitir formar filtros desde el catálogo
    global $DataSourceID;
    $DataSourceID = getParamValue('DataSourceID', 'both', '(int)');
    //OMMC 2015/09/22: Se agregan parámetros para filtros de catálogo
    $sourceID = getParamValue('sourceID', 'both', '(int)');
    $sourceType = getParamValue('sourceType', 'both', '(int)');
    switch ($sourceType) {
        case otyQuestion:
            $QuestionID = $sourceID;
            break;
        case otySection:
            $SectionID = $sourceID;
            break;
    }
    //@JAPR
    $Type = getParamValue('Type', 'both', '(string)'); // Puede ser "S" o "Q" es decir, Sección o pregunta.
    $aText = getParamValue('aTextEditor', 'both', '(string)'); //Contenido del campo de texto de donde se mando a llamar a el editor de formulas
    $objDataSourceMembersColl = null;
    $objDataSource = BITAMDataSource::NewInstanceWithID($theRepository, $DataSourceID);
    if (!is_null($objDataSource)) {
        $objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($theRepository, $DataSourceID);
        if (is_null($objDataSourceMembersColl)) {
            $objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstanceEmpty($theRepository, $DataSourceID);
        }
    }

    /*@AAL 25/02/2015: Se inicia la carga de los datos y se almacenan en las propiedades de en un objeto HTML (Select)*/
    $HtmlSelect = "";
    $ArrNames = array();
    $ArrIDNames = array();
    //$aSectionCollection = BITAMSectionCollection::NewInstance($theRepository, $SurveyID);
    $HtmlSelect .= "<select id='SelectMenu' onchange='javascript: SelectOnchangeOrClick(this)' size='23' class='sel_sq'>";
    
    //OMMC 2015/09/10: Se dejan de mostrar los atributos de los catálogos al momento de generar filtros
    //@JAPR 2015-08-02: Agregada la lista de atributos para permitir formar filtros desde el catálogo
    //Primero llenará la parte del catálogo si es que se indicó alguno
    
    /*if (!is_null($objDataSource)) {
        $DataSourceName = (strlen($objDataSource->DataSourceName) >= 28) ? substr($objDataSource->DataSourceName, 0, 28) . "..." : trim($objDataSource->DataSourceName);
        $HtmlSelect .= "<option id='@D' value='' style='background-color: #FECC62;' title='" . $objDataSource->DataSourceName . "' style='font-family: Arial;'>" . $DataSourceName . "</option>";
        foreach ($objDataSourceMembersColl as $objDataSourceMember) {
            $DataSourceName = (strlen($objDataSource->DataSourceName) >= 28) ? substr($objDataSource->DataSourceName, 0, 28) . "..." : trim($objDataSource->DataSourceName);
            $Qname = $objDataSourceMember->getVariableName();
            if (!is_null($ArrNames)){
                if (array_key_exists($Qname, $ArrNames)) 
                    $Qname = getNumberOfName($Qname, $ArrNames); //Por si existen nombres repetidos, cunado se corta un nombre a  caracteres.
            }
            
            $Attrs = array();
            $ArrNames[$Qname] = $objDataSourceMember->getAttribIDVariableName();
            /*Se crea el value que se le asignara al option en formato JSON:
              {IdNumeric: "$Qn", Name: "$QShortName", Qtype: 2, Attributes: Array[n]: ["attrName1", "attrName2", "attrName3", ... ,"attrName_n"]*/
            /*$QuestionAttr = array('IdNumeric' => $Qname, 'Name' => $Qname, 'Stype' => 0, 'Qtype' => -1, 'Attributes' => $Attrs);
            $AttributeName = (strlen($objDataSourceMember->MemberName) >= 20) ? substr($objDataSourceMember->MemberName, 0, 20) . '...': trim($objDataSourceMember->MemberName);
            $HtmlSelect .= "<option id='@DM' value='" . json_encode($QuestionAttr) . "' title='" . $objDataSourceMember->MemberName . "' style='font-family: Arial;'>&nbsp;&nbsp;&nbsp;&nbsp" . $objDataSourceMember->MemberOrder . ".- " . $AttributeName . "</option>";
        }
    }*/
    //@JAPR

    //OMMC 2015/09/23 Inclusion de nombres cortos para los atributos de catálogos recortado de la funcionalidad de arriba
/*    
	$arrCatMembersbyDataSourceMemeberID = array();
    if (!is_null($objDataSource)) {
        if($sourceType == otySection){
            $objObject = BITAMSection::NewInstanceWithID($theRepository, $SectionID);
        }else{
            $objObject = BITAMSection::NewInstance($theRepository, $QuestionID);
        }
        //las preguntas están bien
        if ($objObject->CatalogID > 0){
            $aCatalogMemberCollection = BITAMCatalogMemberCollection::NewInstance($theRepository, $objObject->CatalogID);
            if(!is_null($aCatalogMemberCollection)){
                foreach ($aCatalogMemberCollection as $objCatalogMember) {
                    $arrCatMembersbyDataSourceMemeberID[] = $objCatalogMember->getVariableName();
                }
            }
        }
    }
*/
	//GCRUZ 2016-05-24
	$aUserID = getParamValue('UserID', 'both', '(int)');
	$aCatalogAttributes = BITAMDataSource::GetCatalogAttributes($theRepository, $DataSourceID);
//	echo "<pre>";
//	var_dump($objDataSource);
//	var_dump($objDataSourceMembersColl);
//	var_dump($aCatalogAttributes);
//	exit;
	$Attrs = array();
	$AttrIDs = array();
	$Attrs_vars = array();
	foreach ($objDataSourceMembersColl->Collection as $objCatalogMember)
	{
		$AttrIDs[] = $objCatalogMember->MemberID;
		$Attrs[] =  $objCatalogMember->MemberName;
		//OMMC 2015-11-05: Se codifican a UTF8 los atributos de los catálogos. 
		$AttrVars[] = utf8_encode($objCatalogMember->getVariableName());
		$ArrIDNames[$objCatalogMember->MemberID] = $objCatalogMember->MemberName;
	}
	$SectionAttr = array('sourceType' => otyDataSource, 'sourceID' => $objDataSource->DataSourceID, 'IdNumeric' => '$S' . $objDataSource->DataSourceID, 'Name' => $objDataSource->DataSourceName, 'Stype' => 5, 'Attribute_vars' => $AttrVars, 'Attributes' => $Attrs, 'Attribute_ids' => $AttrIDs);
	$HtmlSelect .= "<option id='$" . "S' value='" . json_encode($SectionAttr) . "' class='tag' title='" . $objDataSource->DataSourceName . "'>" . $objDataSource->DataSourceName . "</option>";

if (false)
{
    foreach ($aSectionCollection as $objSection) {
        /*Si hay secciones con catalogos, registramos sus atributos en las propiedad value del elemento Select*/
        //OMMC 2016-02-09: Agregados los DataSouceMemberID para los atributos de catálogo
        $Attrs = array();
        $AttrIDs = array();
        if ($objSection->CatalogID != 0){
            $aCatalogMemberCollection = BITAMCatalogMemberCollection::NewInstance($theRepository, $objSection->CatalogID);
            if (!is_null($aCatalogMemberCollection)) {
                foreach ($aCatalogMemberCollection as $objCatalogMember) {                      
                    $Attrs[] =  $objCatalogMember->MemberName;
                    $AttrIDs[] = $objCatalogMember->DataSourceMemberID;
                    $ArrIDNames[$objCatalogMember->DataSourceMemberID] = $objCatalogMember->MemberName;
                }
            }
        }
        $Sname = str_replace(" ", "", $objSection->SectionName);
        //OMMC 2015-12-11: Agregado para que no partiera caracteres especiales cuando son multibytes.
        $Sname = (mb_strlen($Sname) > 5) ? '$S' . mb_substr($Sname, 0, 10) : '$S' . $Sname;
        if (!is_null($ArrNames)){
            if (array_key_exists($Sname, $ArrNames)) 
                $Sname = getNumberOfName($Sname, $ArrNames); //Por si existen nombres repetidos esta función agrega al mismo nombre el siguiente consecutivo (1, 2, 3 ..., n)
        }
        $ArrNames[$Sname] = '$S' . $objSection->SectionNumber;
        /*Creación del objeto JSON con formato:
          {IdNumeric: "$Sn", Name: "$SShortName", Stype: 2, Attributes: Array[n]: ["attrName1", "attrName2", "attrName3", ... ,"attrName_n"]*/
        $SectionAttr = array('sourceType' => otySection, 'sourceID' => $objSection->SectionID, 'IdNumeric' => '$S' . $objSection->SectionNumber, 'Name' => $Sname, 'Stype' => $objSection->SectionType, 'Attribute_vars' => $arrCatMembersbyDataSourceMemeberID, 'Attributes' => $Attrs, 'Attribute_ids' => $AttrIDs);
        //OMMC 2015-12-11: Agregado para que no partiera caracteres especiales cuando son multibytes.
        $SectionName = (mb_strlen($objSection->SectionName) >= 28) ? mb_substr($objSection->SectionName, 0, 28) . "..." : trim($objSection->SectionName);
        //$SectionName = substr($objSection->SectionName, 0, 13);
        //$SectionName = $objSection->SectionName;
        //Options correspondientes a las Secciones
        //OMMC 2015/09/22: Agregado sourceType='".otySection."' y sourceID='".$objSection->SectionID."' para filtros de catálogo
        $HtmlSelect .= "<option id='$" . "S' value='" . json_encode($SectionAttr) . "' class='tag' title='" . $objSection->SectionName . "'>" . $SectionName . "</option>";
        //Options correspondientes a la preguntas y atributos de aquellas que tengan catalogos
        $aQuestionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $objSection->SectionID);
        if(!is_null($aQuestionCollection))
            $HtmlSelect .= getHtmlOptionsOfQuestion($theRepository, $objSection, $aQuestionCollection, $ArrNames, $Type, $QuestionID, $ArrIDNames);
    }
}

    $HtmlSelect .= "</select>";

    //OMMC 2015/08/28 Inclusión de funciones y variables de GPS
    $functionSelect = "";
    $functionSelect .= "<option fnTag='1' class='tag'>" . translate("Global variables") . "</option>\n";
    $functionSelect .= "<option value=\"'@LOGINEMAIL'\" class='op'>" . translate("User") . "</option>\n";
/*
    $functionSelect .= "<option value='{FormName}' class='op'>" . translate("Form name") . "</option>\n";
    $functionSelect .= "<option value='{Score}' class='op'>" . translate("Score") . "</option>\n";
    $functionSelect .= "<option fnTag='1' class='tag'>". translate("GPS") ."</option>\n";
    $functionSelect .= "<option value='{accuracy}' class='op'>" . translate("GPS accuracy") . "</option>\n";
    $functionSelect .= "<option value='{latitude}' class='op'>" . translate("GPS latitude") . "</option>\n";
    $functionSelect .= "<option value='{longitude}' class='op'>" . translate("GPS longitude") . "</option>\n";
    $functionSelect .= "<option value='{country}' class='op'>" . translate("GPS country") . "</option>\n";
    $functionSelect .= "<option value='{state}' class='op'>" . translate("GPS state") . "</option>\n";
    $functionSelect .= "<option value='{city}' class='op'>" . translate("GPS city") . "</option>\n";
    $functionSelect .= "<option value='{zipcode}' class='op'>" . translate("GPS zip code") . "</option>\n";
    $functionSelect .= "<option value='{address}' class='op'>" . translate("GPS address") . "</option>\n";
    //OMMC 2015-11-23: Las funciones sólo se dibujarán si no hay un DataSource de por medio
    if($DataSourceID == 0){
        $functionSelect .= "<option fnTag='1' class='tag'>" . translate("Functions") . "</option>\n";
        $functionSelect .= "<option value=\"Dist({latitude},{longitude},'LatD','LonD')\" class='op'>" . translate("Distance") . "(LatO, LonO, LatD, LonD)</option>\n";
        $functionSelect .= "<option value=\"If(exp, ifTrue, ifFalse)\" class='op'>IF(Conditional, true, false)</option>\n";
        $functionSelect .= "<option value=\"Concat('','')\" class='op'>" . translate("Concat(String1, String2)") . "</option>\n";
        $functionSelect .= "<option value=\"Left('', 0)\" class='op'>" . translate("Left(String, index)") . "</option>\n";
        $functionSelect .= "<option value=\"Mid('', 0, 0)\" class='op'>" . translate("Mid(String, Start, End)") . "</option>\n";
        $functionSelect .= "<option value=\"Right('', 0)\" class='op'>" . translate("Right(String, index)") . "</option>\n";
        $functionSelect .= "<option value=\"Day('YYYY-MM-DD')\" class='op'>" . translate("Day(Date)") . "</option>\n";
        $functionSelect .= "<option value=\"Month('YYYY-MM-DD')\" class='op'>" . translate("Month(Date)") . "</option>\n";
        $functionSelect .= "<option value=\"Year('YYYY-MM-DD')\" class='op'>" . translate("Year(Date)") . "</option>\n";
        $functionSelect .= "<option value=\"CurrentTime()\" class='op'>" . translate("CurrentTime()") . "</option>\n";
        $functionSelect .= "<option value=\"CurrentDate()\" class='op'>" . translate("CurrentDate()") . "</option>\n";
        $functionSelect .= "<option value=\"CurrentDay()\" class='op'>" . translate("CurrentDay()") . "</option>\n";
        $functionSelect .= "<option value=\"CurrentMonth()\" class='op'>" . translate("CurrentMonth()") . "</option>\n";
        $functionSelect .= "<option value=\"CurrentYear()\" class='op'>" . translate("CurrentYear()") . "</option>\n";
        $functionSelect .= "<option value=\"DateAdd('', 0 ,'YYYY-MM-DD')\" class='op'>" . translate("DateAdd(Type, index, Date)") . "</option>\n";
        $functionSelect .= "<option value=\"DateDiff('YYYY-MM-DD','YYYY-MM-DD','')\" class='op'>" . translate("DateDiff(EndDate, StartDate, Type)") . "</option>\n";
    }
*/
	if (getMDVersion() >= esvUserParams) {
		//GCRUZ 2016-07-19. Nuevas fuciones. Parámetros de usuario.
		$functionSelect .= "<option value=\"'@USERPARAM1'\" class='op'>" . translate("User Parameter 1") . "</option>\n";
		$functionSelect .= "<option value=\"'@USERPARAM2'\" class='op'>" . translate("User Parameter 2") . "</option>\n";
		$functionSelect .= "<option value=\"'@USERPARAM3'\" class='op'>" . translate("User Parameter 3") . "</option>\n";
	}

    
    /*@AAL 25/02/2015: Establecemos el patrón para buscar todas la preguntas y secciones con formato (@|$)[Q|S]Number
    Si estas, contienen @ simplemente no seran reemplazadas*/
    $Pattrns = "/([$][S|Q][\d]+)/";
    preg_match_all($Pattrns, $aText, $matches, PREG_PATTERN_ORDER);
    /*@AAL 25/02/2015: Reemplazamos las preguntas/secciones con formato de número $(Q|S)Number al formato $(Q|S)ShortName 
      Para mostrarlo en el editor de fórmulas*/
    for($i = 0; $i < count($matches[0]); $i++)
    {
        $key = array_search($matches[0][$i], $ArrNames);
        if (!$key) $key = $matches[0][$i];
        $aText = preg_replace("/(\\" . $matches[0][$i] . "\.)/", $key . ".", $aText);
    }

    //Por si existiesen en el editor de formulas las variables objSettings.['user'] o selSurvey.['name'], éstas las reeemplazamos por ForName o UserName
    $aText = str_replace("{objSettings.['user']}", "{UserName}", $aText);
    $aText = str_replace("{selSurvey.['name']}", "{FormName}", $aText);
    $aText = str_replace("{\$Score}", "{Score}", $aText);
    
    //Por si existiesen en el editor de formulas las variables @gps(), éstas las reemplazamos de esta froma
    $aText = str_replace("{\$gps.accuracy}", "{accuracy}", $aText);
    $aText = str_replace("{\$gps.latitude}", "{latitude}", $aText);
    $aText = str_replace("{\$gps.longitude}", "{longitude}", $aText);
    $aText = str_replace("{\$gps.country}", "{country}", $aText);
    $aText = str_replace("{\$gps.state}", "{state}", $aText);
    $aText = str_replace("{\$gps.city}", "{city}", $aText);
    $aText = str_replace("{\$gps.zipcode}", "{zipcode}", $aText);
    $aText = str_replace("{\$gps.address}", "{address}", $aText);
    
    //OMMC 2016-02-10: Agregados los reemplazos de attrdid(#)
    //Busca la cadena de: attrdid(4342)
    $Patterns = "/(attrdid\([\"\w]+\))/i";
    //Busca la cadena de: (1234)
    $Patterns2 = "/\((.*?)\)/i";
    preg_match_all($Patterns, $aText, $matches, PREG_PATTERN_ORDER);
    //Buscamos entre todos las coincidencias con: attrdid(#)
    for($i=0; $i < count($matches[0]); $i++){
        //Si se encuentra algo en attrdid(#) de la manera (#), que siempre ocurrirá.
        if(preg_match($Patterns2, $matches[0][$i], $match)){
            //Entonces se reemplazará el valor del datasourcememberid por el nombre del atributo
            //de modo tal que.. {$S(nombredeseccion).attrdid(23423423)} sea {$Sseccion3.Calle}
            if(array_key_exists(str_replace(["(",")"], "", $match[0]), $ArrIDNames)){
                $aText = str_replace($matches[0][$i], $ArrIDNames[str_replace(["(",")"], "", $match[0])], $aText);    
            }
        }
    }

    
    /*@AAL 25/02/2015: Establecer el patrón para buscar todos los atributos con formato Attr("atributeName") | Attr('atributeName') | Attr(atributeName)*/
    $Pattrns = "/(attr\(['\"\w]+\))/i";
    //Patron es para obtener el valor de Attr("atributeName") osea atributeName.
    $Pattrns2 = "/(attr\(|['\")])/i";
    preg_match_all($Pattrns, $aText, $matches, PREG_PATTERN_ORDER);
    /*@AAL 25/02/2015: Se procede a realizar el reemplazo correspondiente de los Attr("atributeName") 
    por su atributeName, para que en el editor se muestre como $(S|Q)ShorName.atributeName*/
    for($i = 0; $i < count($matches[0]); $i++)
    {
        $Attr = preg_replace($Pattrns2, "", $matches[0][$i]);
        $aText = str_replace($matches[0][$i], $Attr, $aText);
    }
    
    /*@AAL 25/02/2015: Esta función arma el HTML correspondiente a los options de cada pregunta y además se obtienen
    los atributos del catalogo si es que tiene cada preguta y se registran en el value de cada option en formato JSON*/
    function getHtmlOptionsOfQuestion($theRepository, $objSection, $aQuestionCollection, &$ArrNames, $Type, $IdQuestion, &$ArrIDNames)
    {
        global $DataSourceID;
        $HtmlOptions =  "";
        foreach ($aQuestionCollection as $objQuestion) {
            if ($objSection->SectionID == $objQuestion->SectionID) {
                $aCatalogMemberCollection = BITAMCatalogMemberCollection::NewInstance($theRepository, $objQuestion->CatalogID);
                if (!is_null($aCatalogMemberCollection)) {
                    //OMMC 2016-02-09: Agregados los DataSouceMemberID para los atributos de catálogo
                    $Attrs = array();
                    $AttrIDs = array();
                    $AttrVars = array();
                    foreach ($aCatalogMemberCollection as $objCatalogMember) {
                        $AttrIDs[] = $objCatalogMember->DataSourceMemberID;
                        $Attrs[] =  $objCatalogMember->MemberName;
                        //OMMC 2015-11-05: Se codifican a UTF8 los atributos de los catálogos. 
                        $AttrVars[] = utf8_encode($objCatalogMember->getVariableName());
                        $ArrIDNames[$objCatalogMember->DataSourceMemberID] = $objCatalogMember->MemberName;
                        //$AttrVars[] = $objCatalogMember->getVariableName();
                    }
                    $Qname = str_replace(" ", "", $objQuestion->QuestionText);
                    //OMMC 2015-12-11: Agregado para que no partiera caracteres especiales cuando son multibytes.
                    $Qname = (mb_strlen($Qname) > 5) ? '$Q' . mb_substr($Qname, 0, 10) : '$Q' . $Qname;
                    if (!is_null($ArrNames)){
                        if (array_key_exists($Qname, $ArrNames)) 
                            $Qname = getNumberOfName($Qname, $ArrNames); //Por si existen nombres repetidos, cunado se corta un nombre a  caracteres.
                    }
                    $ArrNames[$Qname] = '$Q' . $objQuestion->QuestionNumber;
                    /*Se crea el value que se le asignara al option en formato JSON:
                      {IdNumeric: "$Qn", Name: "$QShortName", Qtype: 2, Attributes: Array[n]: ["attrName1", "attrName2", "attrName3", ... ,"attrName_n"]*/
                    $QuestionAttr = array('sourceType' => otyQuestion, 'sourceID' => $objQuestion->QuestionID, 'IdNumeric' => '$Q' . $objQuestion->QuestionNumber, 'Name' => $Qname, 'Stype' => $objSection->SectionType, 'Qtype' => $objQuestion->QTypeID, 'ValuesSourceType' => $objQuestion->ValuesSourceType, 'QDisplayMode' => $objQuestion->QDisplayMode, 'UseMap' => $objQuestion->UseMap, 'EditorType' => $objQuestion->EditorType, 'Attribute_vars' => $AttrVars, 'Attributes' => $Attrs, 'Attribute_ids' => $AttrIDs);
                    //OMMC 2015-12-11: Agregado para que no partiera caracteres especiales cuando son multibytes.
                    $AttributeName = (mb_strlen($objQuestion->QuestionText) >= 20) ? mb_substr($objQuestion->QuestionText, 0, 20) . '...': trim($objQuestion->QuestionText);
                    //OMMC 2015/09/22: Agregado sourceType='".otyQuestion."' y sourceID='".$objQuestion->QuestionID ."' para obtener la ID de las preguntas
                    if($Type == "S" || $IdQuestion == -1) //agregar cualquier otra situación
                        $HtmlOptions .= "<option id='$" . "Q' value='" . json_encode($QuestionAttr) . "' title='" . $objQuestion->QuestionText . "' class='op'>&nbsp;&nbsp;&nbsp;&nbsp" . $objQuestion->QuestionNumber . ".- " . $AttributeName . "</option>";
                    else{ //Solo si se trata de una pregunta 'Q' que no sea nueva
                        if ($objQuestion->QuestionID != $IdQuestion)
                            $HtmlOptions .= "<option id='$" . "Q' value='" . json_encode($QuestionAttr) . "' title='" . $objQuestion->QuestionText . "' class='op'>&nbsp;&nbsp;&nbsp;&nbsp" . $objQuestion->QuestionNumber . ".- " . $AttributeName . "</option>";
                        //Sólo si estoy en la misma pregunta en la cual deseo hacer el filtro
                        elseif ($objQuestion->QuestionID == $IdQuestion && $DataSourceID != NULL)
                            $HtmlOptions .= "<option id='$" . "Q' value='" . json_encode($QuestionAttr) . "' title='" . $objQuestion->QuestionText . "' class='op'>&nbsp;&nbsp;&nbsp;&nbsp" . $objQuestion->QuestionNumber . ".- " . $AttributeName . "</option>"; /*Se borra del arreglo la misma pregunta donde estoy*/
                        else
                            unset($ArrNames[$Qname]);
                    }
                }
            }
        }
        return $HtmlOptions;
    }


    /*@AAL 25/02/2015: Puesto que los nombres de las secciones o preguntas se cortan a 10 caracteres es posible que pueda repetirse algún nombre.
     Para evitarlo esta siguiente función verifica si existe alguno, si lo hay, agrega al mismo nombre el siguiente consecutivo (1, 2, 3 ..., n)*/
    function getNumberOfName($strName, $ArrNames) 
    {
        //OMMC 2015-12-11: Agregado para que no partiera caracteres especiales cuando son multibytes.
        $Len = mb_strlen($strName); $k = 0;
        foreach ($ArrNames as $key => $value) {
            //Se considera como repetido el nombre sin importar mayúsculas o minúsculas
            if (strcasecmp($strName, mb_substr($key, 0, $Len)) == 0) {
                //if ($strName == substr($key, 0, $Len)) {
                $i = mb_substr($key, $Len);
                if($i) {
                    if((int)$i >= $k)
                        $k = (int)$i + 1;
                }
                else $k = 1;
            }
        }
        if($k == 0) $k = '';
            return $strName . $k;
    }
    //@AAL
?>
<!-- @AAL 25/02/2015: Se inicia a crear el HTML y los elementos que mostrara la página, pero antes
    en el encabezado se definen los estilos y el código JavaScrip emplados por los elemetos HTML -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Formula Editor</title>
        <style>
            .formulaEditorDiv legend{font-size: 13px; margin-bottom: 3px; font-weight: bold;}
            .div_sq{width: 250px; height: 210px; padding-top: 10px; float: left;}
            .div_fun{width: 250px; height: 245px; padding-left: 10px; padding-top: 10px; float: left;}
            .div_right_top{width: 500px; height: 270px;}
            .div_right_bot{ width: 475px; height: 200px; margin-left: 25px;}
            .div_att{width: 200px; height: 210px; padding-left: 25px; padding-top: 10px; float: left;}
            .div_cal{width: 230px; height: 210px; padding-left: 25px; padding-top: 10px; float: left; }
            .div_tex{width: 100%; }
            .div_button{width: 100%; height: 40px; float:left; }
            select.sel_sq {-webkit-appearance: none; -moz-appearance: none; appearance: none; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; -moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888; border: 1px solid transparent; color: #555; font-size: 13px; padding: 3px 3px; text-overflow: ellipsis; white-space: nowrap; width: 100%; height: 90%; background-color: #F0F0F0; cursor: pointer; }
            select.sel_fun {-webkit-appearance: none; -moz-appearance: none; appearance: none; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; -moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888; border: 1px solid transparent; color: #555; font-size: 13px; padding: 3px 3px; text-overflow: ellipsis; white-space: nowrap; width: 100%; height: 90%; background-color: #F0F0F0; cursor: pointer; }
            select.sel_att {-webkit-appearance: none; -moz-appearance: none; appearance: none; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; -moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888; border: 1px solid transparent; color: #555; font-size: 13px; padding: 3px 3px; text-overflow: ellipsis; white-space: nowrap; width: 100%; height: 90%; background-color: #F0F0F0; cursor: pointer; }
            .sel_sq .tag{color: #000000; background-color: #FFB72F; vertical-align: middle; font-weight: bold !important; padding-left: 4px; padding-top: 2px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; } 
            .sel_sq .tag:hover{background-color: #F39F00;}
            .sel_att .tag{color: #000000; background-color: #FFB72F; vertical-align: middle; font-weight: bold !important; padding-left: 4px; padding-top: 2px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; } 
            .sel_att .tag:hover{background-color: #F39F00; }
            .sel_fun .tag{color: #FFFFFF; background-color: #0B43B9; vertical-align: middle; font-weight: bold; padding-left: 4px; padding-top: 2px; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; }
            .sel_fun .tag:hover{background-color: #05276E; }
            .op:hover{background-color: #BABABA; font-weight: bold; color: #FFFFFF; -webkit-border-radius: 3px; -moz-border-radius: 3px; -border-radius: 3px; }
            .calc_border{height: 195px; width: 220px; -webkit-border-radius: 5px; -moz-border-radius: 5px; -border-radius: 5px; border: 1px solid #605E5E; }
            .calc_row{width: 230px; height: 30px; } 
            .calc_button{width: 45px; height: 30px; float: left; font-weight: bold; -webkit-border-radius: 5px; -moz-border-radius: 5px; -border-radius: 5px; border: 1px solid #E7E7E7; /*-moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888;*/ vertical-align: middle; text-align: center; line-height: 30px; margin-left: 6px; margin-top: 6px; background-color: #E7E7E7; color: #383838; } 
            .calc_button:hover{background-color: #8E8E8E; color: #FFFFFF; border: 1px solid #8E8E8E; cursor: pointer; font-weight: bold; }
            .div_tex textarea{width: 440px; height: 130px; resize: none; font-family: monospace; -webkit-border-radius: 5px; -moz-border-radius: 5px; -border-radius: 5px; border: 1px solid #E7E7E7; -moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888; }
            .formulaButton{background: #605E5E; color: #fff; font-family: Sans-serif; font-size: 14px; line-height: 22px; margin: 12px 12px; text-align: center; border: 0; transition: all 0.3s ease 0s; -webkit-border-radius: 5px; -moz-border-radius: 5px; -border-radius: 5px; cursor: pointer; -moz-box-shadow: 3px 3px 8px #888888; -webkit-box-shadow: 3px 3px 8px #888888; box-shadow: 3px 3px 8px #888888; }
            .formulaButtonAdd{height: 22px; width: 150px; margin-left: 63%; }
            .formulaButtonSave{height: 22px; width: 150px; margin-left: 61%; margin-top: 20px; }
            .formulaButton:hover{background: #272525 } 
        </style>

    <!-- @AAL 25/02/2015: Se inicia el apartado de texto javascript para validar los datos a mostrar en 
        cada uno de los elementos HTML -->
        <script language="javascript" src="js/EditorHTML/dialogs.js"></script>
        <script src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
        <script src="js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript">
            var sParamReturn = '';
            //OMMC 2015/09/23: Parámetros para match de filtros
            var dataSourceParamID = <?=($DataSourceID>0)?1:0?>;
            var sourceParamID = <?=$sourceID;?>;
            var MDVersion = parseFloat(<?=getMDVersion()?>);

            /*@AAL 25/02/2015: Manejo de los eventos relacionados a los elementos HTML (Textarea, Select, Button, etc.)*/
            function getCursorPosition (el) {
                    var Pos = 0;
                    if('selectionStart' in el) {
                        Pos = el.selectionStart;
                    } else if('selection' in document) {
                        el.focus();
                        var Sel = document.selection.createRange();
                        var SelLength = document.selection.createRange().text.length;
                        Sel.moveStart('character', -el.value.length);
                        Pos = Sel.text.length - SelLength;
                }
                return Pos;
            }

            function setCursorPosition(el, st, end) {
                if (el.setSelectionRange) {
                    el.focus();
                    el.setSelectionRange(st, end);
                } 
                else if (el.createTextRange) {
                        range = el.createTextRange();
                        range.collapse(true);
                        range.moveEnd('character', end);
                        range.moveStart('character', st);
                        range.select();
                }
            }

            /*@AAL 25/02/2015: Evento disparado cuando se presiona una tecla o se hace click a algún botón de la calculadora*/
            function EditTexKeyPress(Chr, e)
            {
                if (Chr) {
                    var aText = document.getElementById("EditFormulas");
                    var index = getCursorPosition(aText);
                    if (Chr == 'Del') {
                        aText.value = aText.value.substr(0, index - 1) + aText.value.substr(index);
                        index -= 1;
                    } 
                    else{
                        aText.value = aText.value.substr(0, index) + Chr + aText.value.substr(index);
                        index += Chr.length;
                    }
                    setCursorPosition(aText, index, index);
                }
                else {
                    var Tecla = (document.all) ? e.keyCode : e.which;
                    if (Tecla == 8) 
                        return true; 
                    var Regex = /([\/'".+\-*=<>$&?(){}\s\w]+)/i;
                    var Chr = String.fromCharCode(Tecla);
                    //@JAPR 2015-07-16: Se habilitaron todos los caracteres nuevamente
                    //return Regex.test(Chr);
                    return true;
                }
            }
            /*@AAL 25/02/2015: Función que se ejecuta al hacer doble click en las preguntas, se agrega por default: {$QShortName.value}*/
            function AddToEditorDefault(Obj) {
                Options = JSON.parse(Obj.value);
                if (Options.Qtype) {
                    //JAPR 2015-08-02: Agregada la lista de atributos para permitir formar filtros desde el catálogo
                    if (Options.Qtype == -1) {
                        //No son preguntas sino DataSourceMembers del DataSource seleccionado, sólo requieren value
                        EditTexKeyPress('{' + Options.Name + '}');
                    }
                    else {
                        EditTexKeyPress('{' + Options.Name + '.value}');
                    }
                    //JAPR
                }
            }
            
            //OMMC 2015/08/27 Se ejecuta al seleccionar una variable o función
            function SelectOnchangeFunction(Obj){
                var selectFunctionObject = document.getElementById("SelectFunction");
                //la clase para evitar las etiquetas del listado "fntag"
            }
            
            /*@AAL 25/02/2015: Se ejecuta al cambiar de sección/preguta en el elemento select("SelectMenu")*/
            function SelectOnchangeOrClick(Obj)
            {
                var SelectProperty = document.getElementById("SelectProperty");
                SelectProperty.options.length = 0;
                if(Obj.value == ""){
                    return;
                }
                Options = JSON.parse(Obj.value);
                var Len = Options.Attributes.length;
                if (Len) {
                    //OMMC 2015/09/10: Cambio de formato en los atributos de catálogo de filtros para preguntas getData
                    /*OMMC 2015/09/02: Cambió el formato en el que aparecen los atributos de las secciones:
                        Anterior: {$S#.atributo}
                        Actual: {$S#.attr(atributo)}*/
                    if(true || (dataSourceParamID == 1 && sourceParamID == Options.sourceID)){
                        SelectProperty.options.add(new Option('<?=translate("Catalog Attributes")?>', 'Catalog Attributes'));
                        /*SelectProperty.options[0].style.background = "#F5C862";
                        SelectProperty.options[0].style.color = "#33333B";
                        SelectProperty.options[0].style.textAlign  = "center";*/
                        SelectProperty.options[0].disabled = true;
                        SelectProperty.options[0].setAttribute("class", "tag");
                        SelectProperty.options[0].setAttribute("fnTag", "1");
                        //OMMC 2015/09/22: Se previene con una regex que los atributos de los catálogos tengan espacios
                        for(var i = 0; i < Len; i++){
                            SelectProperty.options.add(new Option(Options.Attributes[i], Options.Attribute_vars[i]));
                            SelectProperty.options[i+1].setAttribute("class", "op");
                        }
                    }else{
                        SelectProperty.options.add(new Option('<?=translate("Attributes")?>', 'Attributes'));
                        /*SelectProperty.options[0].style.background = "#F5C862";
                        SelectProperty.options[0].style.color = "#33333B";
                        SelectProperty.options[0].style.textAlign  = "center";
                        SelectProperty.options[0].disabled = true;*/
                        SelectProperty.options[0].setAttribute("class", "tag");
                        SelectProperty.options[0].setAttribute("fnTag", "1");
                        for(var i = 0; i < Len; i++){
                            //OMMC 2016-02-09: Agregado el attrdid como atributo al option
                            //OMMC 2016-02-24: Validado con la MetaData
                            if(MDVersion >= parseFloat(<?=esvATTRDIDSyntax?>)){
                                SelectProperty.options.add(new Option(Options.Attributes[i], Options.Name + "." + Options.Attributes[i]));
                                //SelectProperty.options.add(new Option(Options.Attributes[i], Options.Name + ".attrid(" + Options.Attributes[i] + ")"));
                            }else{
                                SelectProperty.options.add(new Option(Options.Attributes[i], Options.Name + ".attr(" + Options.Attributes[i] + ")"));
                            }
                            SelectProperty.options[i+1].setAttribute("class", "op");
                            SelectProperty.options[i+1].setAttribute("attrDID", Options.Attribute_ids[i]);
                        }
                    }
                }
                //OMMC 2015-10-28: Propiedades de preguntas rediseñado
                //Propiedades de cualquier pregunta: Comment, Score, Value    
                /* 
                SelectProperty.options.add(new Option("<?=translate("comment")?>", Options.IdNumeric + '.' + "comment"));
                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                ----------------------------------------------------------------------------------------------------------
                SelectProperty.options.add(new Option("<?=translate("score")?>", Options.IdNumeric + '.' + "score"));
                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                ----------------------------------------------------------------------------------------------------------
                SelectProperty.options.add(new Option("<?=translate("value")?>", Options.IdNumeric + '.' + "value"));
                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                ----------------------------------------------------------------------------------------------------------
                SelectProperty.options.add(new Option('<?=translate("Non Options")?>', 'No Options'));
                SelectProperty.options[SelectProperty.options.length-1].disabled = true;
                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                */
                switch(Options.Stype){
                    // 0 - Secciones estándar, 1 - Secciones Dinámicas, 3 - Maestro Detalle, 4 - Inline
                    case 0:
                    case 1:
                    case 3:
                    case 4:
                        SelectProperty.options.add(new Option('<?=translate("Properties")?>', 'Properties'));
                        Len = SelectProperty.options.length - 1;
                        SelectProperty.options[Len].setAttribute("class", "tag");
                        SelectProperty.options[Len].setAttribute("fnTag", "1");
                        switch(Options.Qtype){
                            //-1 mail, 0 open, 1 numeric, 2 SChoice, 3 MChoice, 4 date, 5 text, 6 alfanumeric, 12 openTime, 25 barcode
                            //Todos esos tipos llevan valor y comentario
                            case -1: case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 12: case 25:
                                //Si son SChoice o MChoice que NO tengan un catálogo. Tendrán comment, score y value.
                                if((Options.Qtype == 2 && Options.ValuesSourceType == 0) || (Options.Qtype == 3 && Options.ValuesSourceType == 0)){
                                    SelectProperty.options.add(new Option("<?=translate("comment")?>", Options.IdNumeric + '.' + "comment"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                    SelectProperty.options.add(new Option("<?=translate("score")?>", Options.IdNumeric + '.' + "score"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op"); 
                                    SelectProperty.options.add(new Option("<?=translate("value")?>", Options.IdNumeric + '.' + "value"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                //Pregunta tipo Map. (Es una SChoice pero su propiedad UseMap es igual a 1)
                                }else if(Options.Qtype == 2 && Options.UseMap == 1){
                                    SelectProperty.options.add(new Option("<?=translate("comment")?>", Options.IdNumeric + '.' + "comment"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                //Pregunta tipo getData, las cuales sólo deben tener los atributos del catálogo. (Es una SChoice oculta con un catálogo con un displayMode igual a 9)
                                }else if(Options.Qtype == 2 && Options.QDisplayMode == 9){
                                    SelectProperty.options.add(new Option('<?=translate("Non Options")?>', 'No Options'));
                                    SelectProperty.options[SelectProperty.options.length-1].disabled = true;
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                //El resto de las preguntas
                                }else{
                                    SelectProperty.options.add(new Option("<?=translate("comment")?>", Options.IdNumeric + '.' + "comment"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op"); 
                                    SelectProperty.options.add(new Option("<?=translate("value")?>", Options.IdNumeric + '.' + "value"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                }
                            break;
                            //14 calc, 17 phone, 18 geolocalization
                            //Sólo tienen valor
                            case 14: case 17: case 18:
                                SelectProperty.options.add(new Option("<?=translate("value")?>", Options.IdNumeric + '.' + "value"));
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                            break;
                            //8 photo, 15 document, 21 audio, 22 video
                            //Sólo llevan comentario.
                            case 8: case 15: case 21: case 22:
                                SelectProperty.options.add(new Option("<?=translate("comment")?>", Options.IdNumeric + '.' + "comment"));
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                            break;
                            //9 action
                            //Sólo llevan el número de días. 
                            case 9:
                                SelectProperty.options.add(new Option("<?=translate("days")?>", Options.IdNumeric + '.' + "days"));
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                            break;
                            //7 showValue, 10 signature, 11 message/html, 13 skip, 16 sync, 19 password, 23 sketch, 24 section
                            //Las que no deben llevar opciones
                            case 7: case 10: case 11: case 13: case 16: case 19: case 23: case 24:
                                SelectProperty.options.add(new Option('<?=translate("Non Options")?>', 'No Options'));
                                SelectProperty.options[SelectProperty.options.length-1].disabled = true;
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                            break;
                            //Quiere decir que el click ocurrió en una sección.
                            default:
                                SelectProperty.options.add(new Option("<?=translate("name")?>", Options.IdNumeric + '.' + "name"));
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                SelectProperty.options.add(new Option("<?=translate("number")?>", Options.IdNumeric + '.' + "number"));
                                SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                               /*Se muestran éstas opciones sólo si las secciones donde nos encontramos son:
                                     SectDynamic         SectMaestDetalle       SectInLine   */
                                if (Options.Stype == 1 || Options.Stype == 3 || Options.Stype == 4) {
                                    SelectProperty.options.add(new Option("<?=translate("pagename")?>", Options.IdNumeric + '.' + "pagename"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                    SelectProperty.options.add(new Option("<?=translate("pagenumber")?>", Options.IdNumeric + '.' + "pagenumber"));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                    SelectProperty.options.add(new Option("<?=translate("score")?>", Options.IdNumeric + '.' + "score")); 
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                } 
                            break;
                        }
                        /*@OMMC 2015/09/01: Se muestran las funciones de max, avg, count, min y sum cuando sean preguntas con secciones: SectDynamic         SectMaestDetalle       SectInLine*/
                        //OMMC 2015-10-27: No todas las preguntas deben llevar estas funciones. SÓLO las preguntas numéricas y calculadas cuando están en una sección que no es estandar.
                        if (Options.Stype == 1 || Options.Stype == 3 || Options.Stype == 4) {
                            if(Options.Qtype == 1 || Options.Qtype == 14){
                                var functs = ['avg()', 'count()', 'max()', 'min()', 'sum()'];
                                for (var j = 0; j < functs.length; j++) {
                                    SelectProperty.options.add(new Option(functs[j], Options.IdNumeric + '.' + functs[j]));
                                    SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                                }
                            }
                        }
                    break;
                    //Algún otro caso
                    default:
                        SelectProperty.options.add(new Option('<?=translate("Properties")?>', 'Properties'));
                        Len = SelectProperty.options.length - 1;
                        SelectProperty.options[Len].setAttribute("class", "tag");
                        SelectProperty.options[Len].setAttribute("fnTag", "1");
                        SelectProperty.options.add(new Option('<?=translate("Non Options")?>', 'No Options'));
                        SelectProperty.options[SelectProperty.options.length-1].disabled = true;
                        SelectProperty.options[SelectProperty.options.length-1].setAttribute("class", "op");
                    break;
                }
            }    
            //OMMC: Fin del rediseño de propiedades de preguntas

            
            //@OMMC 2015/08/28 Quita la selección del panel de variables/funciones
            function clearFunctions(){
                if(document.getElementById("SelectFunction").selectedIndex != -1){
                    document.getElementById("SelectFunction").options[document.getElementById("SelectFunction").selectedIndex].selected = false;
                }
            }

            //@OMMC 2015/08/27 Se editó esta función para poder agregar las las variables y funciones
            /*@AAL 25/02/2015: Esta función se ejecuta al hacer doble clik en el elemento Select ("SelectProperty")
            o al hacer click en el botón Add To Editor*/
            function AddToEditor()
            {
                //hay una funcion seleccionada
                if(document.getElementById("SelectFunction").selectedIndex != -1){
                    //@OMMC 2015/08/27 Funcionalidad agregada
                    var SelectVarOrFn = document.getElementById("SelectFunction");
                    var Index = SelectVarOrFn.selectedIndex;
                    //Si se selecciona una variable o función que NO es una etiqueta
                    if(SelectVarOrFn.options[Index].getAttribute("fnTag") != 1){
                            if(Index != -1){
                            var value = "";
                            value = SelectVarOrFn.options[Index].value;
                            EditTexKeyPress(value);
                            SelectVarOrFn.options[Index].selected = false;
                        }else{
                            alert("Debe seleccionar una funci&oacute;n"); //Debe seleccionar una función
                        }
                    }
                }else{ //Se desea seleccionar una propiedad
                    var SelectProperty = document.getElementById("SelectProperty");
                    var Index = SelectProperty.selectedIndex;
                    if (Index != -1){
                        //OMMC 2016-02-09: Cuando se daba click en una etiqueta del cuadro de propiedades, se estaba agregando
                        if(SelectProperty.options[Index].getAttribute("fnTag") == 1){
                            return;
                        }

                        var SelectMenu = document.getElementById("SelectMenu");
                        var IndexAux = SelectMenu.selectedIndex;
                        
                        if (IndexAux != -1) {
                            Options = JSON.parse(SelectMenu.value);
                            var value = "";
                            //Este caso es solo para funciones Con Cadenas y Fechas. Estas no se agregan con {}
                            if(SelectProperty.options[Index].value.indexOf('(') != -1 && SelectProperty.options[Index].value.indexOf('.') == -1){
                                value = SelectProperty.options[Index].value;
                                EditTexKeyPress(value);
                                //Se elimina la selección de una variable/función si es que hay selección
                                if(document.getElementById("SelectFunction").selectedIndex != -1){
                                    document.getElementById("SelectFunction").options[document.getElementById("SelectFunction").selectedIndex].selected = false;
                                }
                            }
                            else {//Para el caso de ForName y UserName. Estas no estan Separadas por .
                                if(SelectProperty.options[Index].value.split('.')[1] == undefined)
                                    value = SelectProperty.options[Index].value;
                                else if(SelectProperty.options[Index].className == 'advFn')
                                    value = Options.Name + '.attr(' + SelectProperty.options[Index].value.split(').')[1];
                                else //Para cualquier otro caso, tomamos la parte despues del .
                                    value = Options.Name + '.' + SelectProperty.options[Index].value.split('.')[1];
                                //Agregar al Editor de formulas.
                                EditTexKeyPress('{' + value + '}');
                                //Se elimina la selección de una variable/función si es que hay selección
                                if(document.getElementById("SelectFunction").selectedIndex != -1){
                                    document.getElementById("SelectFunction").options[document.getElementById("SelectFunction").selectedIndex].selected = false;
                                }
                            }
                        }
                        else
                            alert('<?=translate("You must select a section or question")?>'); //Debe seleccionar una sección o pregunta
                    }
                    else
                        alert('<?=translate("You must select a section or question")?>');
                }
            }

            //OMMC 2015-10-01: Agregada función para validar regex de funciones gps
            RegExp.quote = function(str) {
                return str.replace(/([.?*+^$[\]\\(){}|-])/g, "\\$1");
            };
     
            /*@AAL 25/02/2015: Se ejecuta al enviar la fórmula o el texto tecleado en el editor, al hacer click
            sobre el botón Send & Close. Principalmente se valida que antes de enviar el texto este sea convertido
            nuevamente al formato de número {$(Q|S)Number.{attr(atributeName) | function() | properties}*/
            function ResultEditFormulas()
            {
                var allowClose = true;
                $('#closeButton').attr('allowClose', allowClose);
                sParamReturn = document.getElementById("EditFormulas").value;
                //Se inicia el proceso de validación y transformación de nombres de variables a número
                if (sParamReturn.trim() != "") {
                    var Matches = new Array();
                    var Results;
                    //Extraer del texarea (Editor) los atributos usados (con formato ".NameAtributo}") y posteriormente corroborar si son validos
                    if(MDVersion >= parseFloat(<?=esvATTRDIDSyntax?>)){
                        var Regex = /\.(.*?)\}/g; // /\.[\w]+[(\w)]*}/g; /(Attr\(['\"\w]+\))/ig; /.[Attr]\w+/g; /\.[\w]+[()]*}/g;
                    }else{
                        var Regex = /\.[\w]+[()]*}/g;
                    }
                    while ((Results = Regex.exec(sParamReturn)) != null)
                        Matches.push(Results[0]);
                    Matches.sort();
                    /* @AAL 25/02/2015: Como el value de cada Option estan los datos en formato $(Q|S)Number, hay que reemplazar los que están en el Editor con formato $(Q|S)ShortName y a la vez reemplazar los atributos 
                    por el formato Attr('attrName')*/
                    var ObjSelect = document.getElementById("SelectMenu");
                    for (var i = 0; i < ObjSelect.length; i++) {
                        //JAPR 2015-08-01: Corregido un bug, si no hay valor no debe hacer el parse porque tronaría, en ese caso se omite este reemplazo
                        if (ObjSelect.options[i].value == "") {
                            continue;
                        }
                        
                        Options = JSON.parse(ObjSelect.options[i].value);
                        // Escapamos los simbolos \. \$ .replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
                        //OMMC 2015-10-01: Modificado para prevenir errores en el editor de fórmulas cuando los nombres de las pregutnas tienen variables GPS, UPDATE: Se remueve el doble regex.
                        //var find = (Options.Name + '.').replace(/([$.])/g, '\\$1');
                        var find = (Options.Name + '.');
                        var Pattrns = new RegExp(RegExp.quote(find), 'gi');
                        //OMMC 2016-02-09: Corregido. No se deben reemplazar los atributos de secciones o preguntas que no correspondan.
                        if(sParamReturn.match(Pattrns)){
                            if (Options.Attributes.length) {
                                for (var j = 0; j < Options.Attributes.length; j++) {
                                    Index = Matches.indexOf('.' + Options.Attributes[j] + '}');
                                    if (Index != -1) {
                                        LastIndex = Matches.lastIndexOf('.' + Options.Attributes[j] + '}');
                                        /*Se remueve de Matches los attrNames validos incluyendo si hay repetidos de tal forma que 
                                        si Matches queda vacio, indicará que se teclearon attrNames validos*/
                                        Matches.splice(Index, (LastIndex - Index)+1); 
                                        if(MDVersion >= parseFloat(<?=esvATTRDIDSyntax?>)){
                                            sParamReturn =  sParamReturn.replace("." + Options.Attributes[j], ".attrdid(" + Options.Attribute_ids[j] + ")");
                                        }else{
                                            sParamReturn =  sParamReturn.replace("." + Options.Attributes[j], ".attr('" + Options.Attributes[j] + "')");
                                        }
                                    }
                                }
                            }
                        }
                        sParamReturn = sParamReturn.replace(Pattrns, Options.IdNumeric + '.'); 
                    }
                    //Por si existiesen en el editor de formulas las variables ForName o UserName, las reemplazamos por su valor correspondiente.
                    sParamReturn = sParamReturn.replace("{UserName}", "{objSettings.['user']}");
                    sParamReturn = sParamReturn.replace("{FormName}", "{selSurvey.['name']}");
                    sParamReturn = sParamReturn.replace("{Score}", "{$Score}");
                    
                    //Por si existiesen en el editor de formulas variables de GPS, se reemplazan con lo siguiente.
                    sParamReturn = sParamReturn.replace("{location}", "{$gps.location}");
                    sParamReturn = sParamReturn.replace("{accuracy}", "{$gps.accuracy}");
                    sParamReturn = sParamReturn.replace("{latitude}", "{$gps.latitude}");
                    sParamReturn = sParamReturn.replace("{longitude}", "{$gps.longitude}");
                    sParamReturn = sParamReturn.replace("{country}", "{$gps.country}");
                    sParamReturn = sParamReturn.replace("{state}", "{$gps.state}");
                    sParamReturn = sParamReturn.replace("{city}", "{$gps.city}");
                    sParamReturn = sParamReturn.replace("{zipcode}", "{$gps.zipcode}");
                    sParamReturn = sParamReturn.replace("{address}", "{$gps.address}");

                    //Puesto que Matches también contiene palabras reservadas, es necesario eliminarlas
                    var Reservadas = ['.avg()}', '.days}', '.count()}', '.comment}', '.name}', '.number}', '.max()}', '.min()}', '.pagename}', 'pagenumber}', '.sum()}', '.score}', '.value}'];
                    for (var k = 0; k < Matches.length; k++) {
                        if(Reservadas.indexOf(Matches[k]) != -1){
                            Matches.splice(k, 1);
                            k -= 1;
                        }
                        else
                            Matches[k] = Matches[k].substring(1, Matches[k].length-1)
                    }
                    //Si todavía hay palabras en Maches éstas son desconocidas y se reportan como invalidas
                    if (Matches.length){
                        allowClose = false;
                        $('#closeButton').attr('allowClose', allowClose);
                        //alert('The Attributes {' + Matches.valueOf() + '} are not valid');
                        alert('<?=translate("The Attributes {var1} are not valid")?>'.replace("{var1}", Matches.valueOf()));
                        
                    }
                    /*@AAL 25/02/2015: Ahora se realiza un nuevo Match en el Editor por si quedaron nombres de preguntas o secciones 
                    sin reemplazarse por el formato $(Q|S)Number*/
                    /*OMMC 2015-10-09: Se comenta esta porción del código, si se encuentan coincidencias con el formato $(Q|S)Number, simplemente no se reemplazarán cuando pasen del editor de fórmulas al grid.*/
                    /*Regex = /([$][S|Q][a-zA-Z]+\w*)(?!\d+)/g;
                    while ((Results = Regex.exec(sParamReturn)) != null)
                        Matches.push(Results[0].substring(1, Results[0].length-1));
                    //Si todavía existen palabras con formato $(Q|S)ShortName, se reporta como nombres de preguntas/secciones invalidos
                    if (Matches.length){
                        //alert('The Questions/Sections {' + Matches.valueOf() + '} are not valid');
                        alert('<?=translate("The Questions/Sections {var1} are not valid")?>'.replace("{var1}", Matches.valueOf()));
                        return;    
                    }*/
                } 
                else
                    sParamReturn = ""; //Contiene el texto resultante del editor si es que tiene.
                //Se regresa la fórmula con el formato: $(Q|S)Number.{property | function() | Attr('attrName')} ...
                try {
                    if (window.setDialogReturnValue)
                        setDialogReturnValue(sParamReturn);
                } catch (e) {
                    alert(e.message);
                }
                
                window.close();
            }

            /*@AAL 25/02/2015: Limpia el contenido del editor de fórmulas (elemento Textarea)*/
            function ClearAll()
            {
                document.getElementById("EditFormulas").value = "";
                document.getElementById("EditFormulas").focus();
            }

            /*@AAL 25/02/2015: Función que evita maximizar o redimensionar la ventana. Simplemente la regresa a su namaño normal*/
            function Resize () { window.resizeTo(625, 525); }
            
            /*@AAL 25/02/2015: al cargar la página esta función asigna el foco a la primera opción del elemento select ("SelectMenu"), así como el foco*/
            function OnLoadSelectMenu()
            {
                window.resizeTo(625, 525);
                var ObjSelect = document.getElementById("SelectMenu");
                ObjSelect.selectedIndex = 0;
                SelectOnchangeOrClick(ObjSelect);           
                ObjSelect.focus();
            }
            //OMMC 2015-10-07: Se agrega la función KeyPressValidation sobre el body
            //OMMC 2015-10-14: Cambiado porque FireFox no aceptaba el evento 
            $( document ).ready(function() {
                $("body").on("keyup", function(e){
                    var code = e.charCode || e.keyCode;
                    if(code == 27){
                        parent.objDialogFn.close();
                    }
                });
            });
        </script>
    </head>

    <!-- @AAL 25/02/2015: Se inicia el apartado del cuerpo HTML (BODY) asi como los elementos que contendra la página -->
    <body onLoad="OnLoadSelectMenu()" onresize="Resize()">
        <div class="formulaEditorDiv" style="width: 745px; height: 480px; background-color: #FFFFFF; font-family: Helvetica">       
            <!--Div de la izquierda-->
            <div style="float: left; width: 260px; height: 480px">
                <div class="div_sq">
                    <legend><?=translate("Catalogs")?></legend>
                    <? echo $HtmlSelect; ?>    
                </div>
                <div class="div_fun">
                    <legend><?=translate("Variables and functions")?></legend>
                    <select id='SelectFunction' ondblclick="AddToEditor()" size="23" class="sel_fun">
                        <? echo $functionSelect; ?>
                    </select>
                </div>
            </div>
            <!--Div de la derecha-->
            <div style="float: left; width: 475px; height: 480px">
                <div class="div_right_top">
                    <div class="div_att">
                        <legend><?=translate("Properties")?></legend>
                        <select id="SelectProperty" size="23" ondblclick="AddToEditor()" onfocus="clearFunctions()" class="sel_att" ></select>
                    </div>
                    <div class="div_cal">
                        <legend><?=translate("Calculator")?></legend>
                        <div class="calc_border">
                        <div class="calc_row" style="height: 35px;">
                             <div class="calc_button" onClick="EditTexKeyPress('(')" class="Calc"> ( </div>
                             <div class="calc_button" onClick="EditTexKeyPress(')')" class="Calc"> ) </div>
                             <div class="calc_button" onClick="ClearAll()" class="Calc"> <?=translate("AC")?> </div>
                             <div class="calc_button" onClick="EditTexKeyPress('Del')" class="Calc"> &larr; </div>
                        </div>

                        <div class="calc_row" style="height: 35px;">
                            <div class="calc_button" onClick="EditTexKeyPress('7')" class="Calc"> 7 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('8')" class="Calc"> 8 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('9')" class="Calc"> 9 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('/')" class="Calc"> / </div>
                        </div>

                        <div class="calc_row" style="height: 35px;">
                            <div class="calc_button" onClick="EditTexKeyPress('4')" class="Calc"> 4 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('5')" class="Calc"> 5 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('6')" class="Calc"> 6 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('*')" class="Calc"> x </div>
                        </div>

                        <div class="calc_row" style="height: 35px;">
                            <div class="calc_button" onClick="EditTexKeyPress('1')" class="Calc"> 1 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('2')" class="Calc"> 2 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('3')" class="Calc"> 3 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('-')" class="Calc"> - </div>
                        </div>

                        <div class="calc_row" style="height: 35px;">
                            <div class="calc_button" onClick="EditTexKeyPress('0')" class="Calc"> 0 </div>
                            <div class="calc_button" onClick="EditTexKeyPress('.')" class="Calc"> . </div>
                            <div class="calc_button" onClick="EditTexKeyPress('=')" class="Calc"> = </div>
                            <div class="calc_button" onClick="EditTexKeyPress('+')" class="Calc"> + </div>
                        </div>
                        </div>
                    </div>
                    <div class="div_button">
                        <button onClick="AddToEditor()" class="formulaButton formulaButtonAdd"> <?=translate("Add To Formula")?> </button>
                    </div>
                </div>
                <div class="div_right_bot">
                    <div class="div_tex">
                        <legend style="text-align: right;"><?=translate("Editor")?></legend>
                        <textarea id="EditFormulas" spellcheck="false" onKeyPress="return EditTexKeyPress(undefined, event)"><?echo $aText;?></textarea>
                    </div>
                     <div class="div_button">
                        <button id="closeButton" onClick="ResultEditFormulas()" class="formulaButton formulaButtonSave"> <?=translate("Save")?> </button> 
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>