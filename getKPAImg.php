<?php
session_start();

if (array_key_exists('pmUs', $_GET) && array_key_exists('pmPs', $_GET))
{
	require_once("utils.inc.php");
	
	$kpiUser = $_GET["pmUs"];
	$kpiPass = $_GET["pmPs"];
	
	//Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
	$strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
		die ($strPrjAndUser);
	}

	$arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
	
	$strRepositoryName = $arrayPrjUsr[0];
	$strUserName = $arrayPrjUsr[1];
	
	session_register("PABITAM_RepositoryName");
	$_SESSION["PABITAM_RepositoryName"] = $strRepositoryName;
	
	//Generamos la ruta de la cual se van a obtener los archivos
	$pathImgDir = getcwd()."\\"."documents"."\\".trim($_SESSION["PABITAM_RepositoryName"])."\\"."img";
	$pathImgInternal = "/documents/".trim($_SESSION["PABITAM_RepositoryName"])."/img";
		
	if (!file_exists($pathImgDir))
	{
		die("Error, can not access images directory");
	}

	$arrayImgFiles = listFiles($pathImgDir, $pathImgInternal);
	
	generateStrResult($arrayImgFiles);
}
else
{
	die("Error, user and password parameters don't exist");
}

function generateStrResult($arrayImgFiles)
{
	header('Content-Type: text/plain; charset=ISO-8859-1');
	
	if(count($arrayImgFiles)>0)
	{
		$first = true;
		
		foreach ($arrayImgFiles as $each)
		{
			if (!$first)
			{
				echo("\036");
			}
		
			echo($each);
		
			$first = false;
		}
	}
	else
	{
		echo("Error, there are no images");
	} 
}

function listFiles($path, $pathInternal)
{
	//Se obtiene el servidor donde se esta ejecutando KPA
	$protocol_string = "http://";
	$host_string = $_SERVER["SERVER_NAME"];
	$port = (int)$_SERVER["SERVER_PORT"];
	
	$port_string = "";
	
	if($port!=80)
	{
		$port_string = ":".$port;
	}

	$pathParts = pathinfo($_SERVER['REQUEST_URI']);

	$path_string = $pathParts['dirname'];

	$urlPrefix = $protocol_string.$host_string.$port_string.$path_string;

	$arrayFiles = array();
	
	if (is_dir($path))
	{
		$objects = scandir($path);
		foreach ($objects as $object)
		{
			if ($object != "." && $object != "..")
			{
				if (filetype($path."/".$object) != "dir")
				{
					$arrayFiles[] = $urlPrefix.$pathInternal."/".$object;
				}
			}
		}

		reset($objects);
	}
	
	return $arrayFiles;
}

function getKPAProjectAndUser($kpiUser, $kpiPassword)
{
	require_once('../fbm/conns.inc.php');

	if (strlen($kpiUser) == 0)
	{
		return 'Error, user email empty';
	}

	$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);

	if (!$mysql_Connection)
	{
		return 'Error, connection error! '.mysql_error();
	}

	$db_selected = mysql_select_db($masterdbname, $mysql_Connection);

	if (!$db_selected)
	{
		return 'Error, could not select '.$masterdbname.'!'."\n".mysql_error();
	}

	//Obtener el UserID de la tabla de SAAS_Users
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
	$aSQL = sprintf("SELECT userID, Email, AccPassword, ActiveAcc FROM saas_users WHERE Email = '%s'", mysql_real_escape_string($kpiUser, $mysql_Connection));

	$result = mysql_query($aSQL, $mysql_Connection);

	if (!$result || mysql_num_rows($result) == 0)
	{
		return 'Error, data not found: '.$aSQL."\n".mysql_error();
	}

	//Obtener resultado de la consulta
	$row = mysql_fetch_assoc($result);
	$intUserID = $row["userID"];
	$strUserEmail = $row["Email"];
	$intUserActive = intval($row["ActiveAcc"]);
	$strEncryptedPass = $row["AccPassword"];

	//Verificar que la cuenta se encuentre activa
	if ($intUserActive == 2)
	{
		return 'Error, the user account is not active'."\n".mysql_error();
	}

	//Verificar que el password coincida
	$strUserPass = BITAMDecryptPassword($strEncryptedPass);
	if ($kpiPassword !== $strUserPass)
	{
		return 'Error, incorrect user or password'."\n".mysql_error();
	}

	//Obtener el nombre del repositorio de KPA al cual tiene acceso
	$aSQL = sprintf("SELECT A.Repository FROM saas_dbxusr B, saas_databases A
					WHERE B.UserID = %d AND B.PManagerLogin = 1
					AND B.DatabaseID = A.DatabaseID AND A.Status = 1", $intUserID);

	$result = mysql_query($aSQL, $mysql_Connection);

	if (!$result || mysql_num_rows($result) == 0)
	{
		//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
		return translate("Either the user/password combination is incorrect or the account has been blocked");
		//@JAPR
	}

	//Obtener el nombre del repositorio
	$row = mysql_fetch_assoc($result);
	$strRepository = $row["Repository"];

	//Obtener el nombre corto de la cuenta de KPI
	//@JAPR 2016-12-28: Corregido un bug, al ser un like, estaba permitiendo entrar con cuentas ligeramente diferentes siempre y cuando tuvieran un patrón LIKE válido (#IF5J19)
	$aSQL = sprintf("SELECT NOM_CORTO FROM $strRepository.SI_USUARIO WHERE CUENTA_CORREO = '%s'", mysql_real_escape_string($strUserEmail, $mysql_Connection));

	$result = mysql_query($aSQL, $mysql_Connection);

	if (!$result || mysql_num_rows($result) == 0)
	{
		//@JAPR 2019-09-23: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
		return translate("Either the user/password combination is incorrect or the account has been blocked");
		//@JAPR
	}

	//Se asigna el nombre corto del usuario
	$row = mysql_fetch_assoc($result);
	$strUserNomCorto = $row["NOM_CORTO"];

	unset($row);
	unset($mysql_Connection);

	return $strRepository.'_KPASep_'.$strUserNomCorto;
}
?>