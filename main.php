<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();
//@JAPR 2015-02-05: Agregado soporte para php 5.6
if (isset($_SESSION["PABITAM_RepositoryName"]) && isset($_SESSION["PABITAM_UserName"]))
{
	require_once("repository.inc.php");
	require_once('config.php');
	require_once('cubeClasses.php');
	
	global $BITAMRepositories;

	$url="build_page.php";
	
	$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
	if (!array_key_exists($theRepositoryName, $BITAMRepositories))
	{
		header("Location: index.php?error=1");
		exit();
	}
	
	$theRepository = $BITAMRepositories[$theRepositoryName];
	if (!$theRepository->open())
	{
		header("Location: index.php?error=2");
		exit();
	}	
	
	$theUserName = $_SESSION["PABITAM_UserName"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
	
	if (is_null($theUser))
	{
		header("Location: index.php?error=3");
		exit();
	}
	
	//Cargar diccionario del lenguage a utilizar
	if (array_key_exists("PAuserLanguage", $_SESSION))
	{
		InitializeLocale($_SESSION["PAuserLanguageID"]);
		LoadLanguageWithName($_SESSION["PAuserLanguage"]);
	}
	else 
	{	//English
		InitializeLocale(2);
		LoadLanguageWithName("EN");
	}
	
	if(array_key_exists("LoadMainFrames", $_GET))
	{
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$url."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	
	$theHTTPRequest = BITAMHTTPRequest::NewHTTPRequest();
	
	if (array_key_exists("HTTP_REFERER", $_SERVER))
	{
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("PAHTTP_REFERER");
		$_SESSION["PAHTTP_REFERER"]=$_SERVER["HTTP_REFERER"];
	}
	//OMMC 2015-10-12: Se agrega codificación UTF-8 a la cabecera.
	header("Content-Type:text/html; charset=UTF-8");
	$theObject = $theRepository->PerformHTTPRequest($theHTTPRequest, $theUser); 
	if (!is_null($theHTTPRequest->RedirectTo))
	{
		header("Location: main.php?".$theHTTPRequest->RedirectTo->get_QueryString());
		exit();		
	}
	
	if(!is_null($theObject))
	{
		$theTitle = $theObject->get_Title();
	}
	else 
	{
		echo('Error: Cannot call to a member function get_Title() because $theObject is a non-object');
	}
	
	//@JAPR 2015-03-19: Corregido un bug, en ambientes donde el default_charset de PhP no era utf8, se requería agregar este header para forzarlo, ya que el meta
	//con el charset en utf8 no estaba funcionando
	//header("Content-Type:text/html; charset=UTF-8");
	
	//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
	//Si se definió una propiedad NoHeader, se evita agregar este archivo para permitir que una clase Ext pueda generar la página con DHTMLX
	$blnIncludeHeader = true;
	if (!is_null($theObject)) {
		if (property_exists($theObject, 'NoHeader')) {
			$blnIncludeHeader = false;
		}
	}
	//@JAPR 2015-10-02: Removidas las referencias a header.php sin necesidad de la propiedad aunque se vuelva incompatible con la versión anterior, si se requiere rehabilitar para soporte
	//de versiones previas, se tendrá que agregar la propiedad correspondiente a cada clase Extendida para ignorarlo en v6
	if ($blnIncludeHeader) {
		//include("header.php");
	}
	//@JAPR
	
	$showHeader=1;
	if (array_key_exists("showHeader", $_GET))
	{
		$showHeader=(int)$_GET["showHeader"];
	}
	
	$hideFrame=1;
	if (array_key_exists("hideFrame", $_GET))
	{
		$hideFrame=(int)$_GET["hideFrame"];
	}
	
	if($_SESSION["PAFBM_Mode"] != 1)
	{
		$currentUser = $_SESSION["PABITAM_UserName"];
	}
	else 
	{
		$currentUser = $theUser->FullName;
	}

	if(!is_null($theObject))
	{
		$theObject->generateForm($theUser);
	}
	else
	{
		echo('Error: Cannot call to a member function generateForm() because $theObject is a non-object');
	}
	
	//@JAPR 2015-10-02: Removidas las referencias a header.php sin necesidad de la propiedad aunque se vuelva incompatible con la versión anterior, si se requiere rehabilitar para soporte
	//de versiones previas, se tendrá que agregar la propiedad correspondiente a cada clase Extendida para ignorarlo en v6
	//include("footer.php");
	//@JAPR
}
else
{
	if ((array_key_exists("BITAM_SECTION", $_GET) && $_GET["BITAM_SECTION"]=='EKTRepositoryCollection')
		|| (array_key_exists("BITAM_PAGE", $_GET) && $_GET["BITAM_PAGE"]=='EKTRepository') )
	{
		require_once("repository.inc.php");	
		$theHTTPRequest = BITAMHTTPRequest::NewHTTPRequest();
		$theRepository = BITAMRepository::NewInstance("Default");
		$theUser=null;
		$theObject = $theRepository->PerformHTTPRequest($theHTTPRequest, $theUser); 
		if (!is_null($theHTTPRequest->RedirectTo))
		{
			header("Location: main.php?".$theHTTPRequest->RedirectTo->get_QueryString());
			exit();		
		}
		$theTitle = $theObject->get_Title();
		include("header.php");
		$theObject->generateForm($theUser);
		include("footer.php");
	}
	else
	{
		//Identificar si nos encontramos en modo FBM
		$fbmMode = (int)$_COOKIE["PAFBM_Mode"];
		
		if($fbmMode!=1)
		{
			if (array_key_exists("QUERY_STRING", $_SERVER))
			{
				$goto = base64_encode($_SERVER["PHP_SELF"]."?".$_SERVER["QUERY_STRING"]);
			}
			else
			{
				$goto = base64_encode($_SERVER["PHP_SELF"]);
			}
			header("Location: index.php?error=4&goto=".$goto);
		}
		else 
		{
			//@JAPR 2015-10-06: Validado que si el request es Ajax, entonces sólo regrese un texto descriptivo de error en lugar de redireccionar
			if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == strtolower('XMLHttpRequest')) {
				if ($gbIsGeoControl) {
					die(translate("Los datos de su sesión se perdieron o esta sobrepasó el tiempo de inactividad. Por favor reinicie el proceso"));
				}
				else {
					die(translate("Your session data was lost or has timed out due to inactivity. Please restart the process"));
				}
			}
			else {
				header("Location: loadKPILogin.php");
			}
			//@JAPR
		}
		
		exit();
	}
}
?>