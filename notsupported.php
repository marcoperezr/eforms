<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <!-- Change this if you want to allow scaling -->
        <!--<meta name="viewport" content="width=default-width; user-scalable=no" />-->
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        <title>eSurvey</title>

        <!-- iPad/iPhone specific css below, add after your main css >
		<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="ipad.css" type="text/css" />		
		<link rel="stylesheet" media="only screen and (max-device-width: 480px)" href="iphone.css" type="text/css" />		
		-->
        
        <!-- If you application is targeting iOS BEFORE 4.0 you MUST put json2.js from http://www.JSON.org/json2.js into your www directory and include it here -->
        <link rel="stylesheet" href="jquery.mobile-1.0a4/jquery.mobile-1.0a4.css"/>
        <style type="text/css" title="currentStyle">
            @import "datatable/css/demo_page.css";
            @import "datatable/css/demo_table.css";
        </style>
        <link type="text/css" href="jquery.mobile.datebox.css" rel="stylesheet" />
        <!--<link rel="stylesheet" href="spinningwheel/spinningwheel.css" type="text/css" media="all" />-->
        <script type="text/javascript" src="date.js"></script>
        <script type="text/javascript" src="json2.js"></script>
        <script type="text/javascript" src="jquery-1.5.min.js"></script>
        <script type="text/javascript" src="jquery.mobile-1.0a4/jquery.mobile-1.0a4.js"></script>
        <script type="text/javascript" src="jquery.mobile.datebox.js"></script>
        <!--<script type="text/javascript" src="spinningwheel/spinningwheel-min.js?v=1.4"></script>-->
        <script type="text/javascript" src="datatable/js/jquery.dataTables.js"></script>
        <!--<script type="text/javascript" charset="utf-8" src="phonegap.0.9.4.min.js"></script>-->
        <script type="text/javascript" src="js/utils.js"></script>
        <script type="text/javascript" charset="utf-8">
		function onBodyLoad()
		{
			
		}
		
		function showDeviceInfo () {
			
		}
        </script>
    </head>
    <body onload="onBodyLoad()">

        <!-- validacion de usuario y pass con kpi -->
        <div data-role="page" id="loginpage" data-theme="c">
            <div data-role="header" data-id="headerPage" data-theme="a" data-backbtn="false">
                <div data-role="navbar">
                    <ul>
                         <li><a style="font-size: 150%;" href="index.php" data-ajax="false" data-theme="a">eSurvey Login</a></li> 
                    	 <!--<li><a style="font-size: 150%;" href="index.php" data-ajax="false" data-iconpos="bottom" data-icon="meetings" data-theme="a"><?= $deviceIs ?></a></li> -->
                    </ul>
                </div>
            </div>
            <div id="contlogin" data-role="content">
      			<span>Unsupported Browser</span>
            </div>
            <br><br>
            <div data-role="footer" data-id="footerPage" style="position: absolute; bottom: 0;" data-theme="a">
            	
                <div data-role="navbar">
                    <ul>
                        <li><a href="javascript: openWindow('http://kpionline.bitam.com/english');">Powered By BITAM</a></li>
                        <li><a href="#" >About</a></li>
                        <li><a href="#" >Preferences</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>