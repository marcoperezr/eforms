<?php

// Advisor Dashboards Group class
// 26 - Sept - 2008
//

class AdvisorDashboardGroup
{
	public static $registered = array();
	
	public $cla_gpo_esc = -1;
	public $cla_usuario = -1;
	public $nom_gpo_esc = "";
	public $tipo_gpo_esc = -1;
	public $Dashboards = array();
	
	const mainSQL = "SELECT A.CLA_GPO_ESC, A.NOM_GPO_ESC, A.CLA_USUARIO, A.TIPO_GPO_ESC
        		 FROM   SI_GPO_ESCENARIO A";
	
	public function register()
	{
		array_push(AdvisorDashboardGroup::$registered, $this);
	}
	
	public static function withKeys($aCol)
	{
		if (is_null($aCol))
			return (AdvisorDashboardGroup::$registered);

		$res = array();

		foreach (AdvisorDashboardGroup::$registered as $each)
		{
			if (in_array($each->cla_gpo_esc, $aCol))
				$res[] = $each;
		}
		
		return ($res);
	}

	public static function withKey($aKey)
	{
		foreach (AdvisorDashboardGroup::$registered as $each)
			if ($each->cla_gpo_esc == $aKey)
				return ($each);

		return (null);
	}
	
	public static function withKeyFromDB($sDashboardGroupID, $aConnection)
	{
		$aSQL = AdvisorDashboardGroup::mainSQL;

		if (is_null($sDashboardGroupID)||$sDashboardGroupID=="")
		{
			return NULL;
		}
		else
		{
			$aSQL .= " where A.CLA_GPO_ESC = ".$sDashboardGroupID;
		}

		$aSQL .= " order by A.NOM_GPO_ESC";

		$recordSet = $aConnection->Execute($aSQL);

//		$result = array();

		if($recordSet)
		{
			$aDashboardGroup = AdvisorDashboardGroup::withKey($recordSet->fields["CLA_GPO_ESC"]);

			if (is_null($aDashboardGroup))
			{
				$aDashboardGroup = new AdvisorDashboardGroup();
				$aDashboardGroup->fillFromRowDB($recordSet->fields);
				$aDashboardGroup->register();
			}
		}
		else
		{
			return NULL;
		}

		$recordSet->close();
		
		return($aDashboardGroup);
	}
	
	// Cargar los Grupos de Escenarios y Escenarios propietarios de la colección de usuarios
	public static function loadUserDashboardGroups($aUserIDsCollection, $aConnection)
	{
		$aSQL = "SELECT B.CLA_GPO_ESC, B.NOM_GPO_ESC, B.CLA_USUARIO, B.TIPO_GPO_ESC, "."\n".
				"A.CLA_ESCENARIO, A.NOM_ESCENARIO, A.CLA_USUARIO, A.CLA_GPO_ESC "."\n".
        		"FROM   SI_ESCENARIO A, SI_GPO_ESCENARIO B "."\n".
        		"WHERE  B.CLA_GPO_ESC = A.CLA_GPO_ESC AND "."\n".
                "B.CLA_USUARIO = A.CLA_USUARIO";
		
		if (!is_null($aUserIDsCollection))
		{
			$aSQL .= " AND A.CLA_USUARIO in (";

			$first = true;
			foreach ($aUserIDsCollection as $eachKey)
			{
				if ($first)
					$first = false;
				else
					$aSQL .= ",";

				$aSQL .= $eachKey;
			}

			$aSQL .= ")";
		}
		
		$aSQL .= "\n"."ORDER BY B.NOM_GPO_ESC, A.NOM_ESCENARIO";
		
		$recordSet = $aConnection->Execute($aSQL);
		
		$title = "Loading User Dashboards:";
		$msg = $aSQL;
		//AdvisorConfiguration::logString($msg, true, $_SESSION['ADVISOR_ServiceName'], $title);
		
		$resultDashboardGroups = array();
		
		while (!$recordSet->EOF)
		{
			$aDashboardGroup = AdvisorDashboardGroup::withKey($recordSet->fields[0]);

			if (is_null($aDashboardGroup))
			{
				//$aDashboardGroup = AdvisorDashboardGroup::withKeyFromDB($recordSet->fields["cla_gpo_esc"], $aConnection);
				$aDashboardGroup = new AdvisorDashboardGroup();
				$aDashboardGroup->fillFromRowDB(array_slice($recordSet->fields,0, 4));
				$resultDashboardGroups[] = $aDashboardGroup;
				$aDashboardGroup->register();
			}

			$aDashboard = AdvisorDashboard::withKey($recordSet->fields[4]);

			if (is_null($aDashboard))
			{
				//$aDashboard = AdvisoDashboard::withKeyFromDB($recordSet->fields["cla_escenario"], $aConnection);
				$aDashboard = new AdvisorDashboard();
				$aDashboard->fillFromRowDB(array_slice($recordSet->fields, 4));
				$aDashboard->register();
				$aDashboardGroup->addDashboard($aDashboard);
				
				//$resultUsers[] = $aUser;
			}


			//$aDashboard->assignDashboardGroup($aDashboardGroup->cla_gpo_esc);

			$recordSet->MoveNext();
		}

		$recordSet->close();
		
		return $resultDashboardGroups;
		
	}
	
	// Cargar los Escenarios publicados a la colección de usuarios así como a los publicados por rol
	// asignarla a un nuevo grupo de escenarios "publicados" cla_gpo_esc=0
	public static function loadUserPubDashboardGroups($aUserIDsCollection, $aRoleIDsCollection, $aConnection)
	{
		$aSQL = "SELECT B.CLA_ESCENARIO, B.NOM_ESCENARIO, B.CLA_USUARIO, B.CLA_GPO_ESC
				FROM SI_ESC_PUB A, SI_ESCENARIO B
				WHERE A.CLA_ESCENARIO = B.CLA_ESCENARIO";
		
		if (!is_null($aUserIDsCollection))
		{
			$aSQL .= " AND A.CLA_SUSCRIPTOR in (";

			$first = true;
			foreach ($aUserIDsCollection as $eachKey)
			{
				if ($first)
					$first = false;
				else
					$aSQL .= ",";

				$aSQL .= $eachKey;
			}

			$aSQL .= ")";
		}
		
		$aSQL .= "UNION
				SELECT b.CLA_ESCENARIO, b.NOM_ESCENARIO, B.CLA_USUARIO, B.CLA_GPO_ESC
 				FROM SI_ESC_GPO A, SI_ESCENARIO B
 				WHERE A.CLA_ESCENARIO = B.CLA_ESCENARIO";
		
		if (!is_null($aRoleIDsCollection))
		{
			$aSQL .= " AND A.CLA_GPO in (";

			$first = true;
			foreach ($aRoleIDsCollection as $eachKey)
			{
				if ($first)
					$first = false;
				else
					$aSQL .= ",";

				$aSQL .= $eachKey;
			}

			$aSQL .= ")";
		}
		
		$aSQL .= " ORDER BY B.NOM_ESCENARIO";
		
		$recordSet = $aConnection->Execute($aSQL);
		
		$resultDashboardGroup = new AdvisorDashboardGroup();
		$resultDashboardGroup->cla_gpo_esc = 0;
		$resultDashboardGroup->nom_gpo_esc = translate("Public Dashboard");
		$resultDashboardGroup->register();
		
		while (!$recordSet->EOF)
		{
			$aDashboard = AdvisorDashboard::withKey($recordSet->fields["cla_escenario"]);

			if (is_null($aDashboard))
			{
				//$aDashboard = AdvisorDashboard::withKeyFromDB($recordSet->fields["cla_escenario"], $aConnection);
				$aDashboard = new AdvisorDashboard();
				$aDashboard->fillFromRowDB($recordSet->fields);
				$resultDashboardGroup->addDashboard($aDashboard);
			}
			//$aDashboard->assignDashboardGroup($aDashboardGroup->cla_gpo_esc);

			$recordSet->MoveNext();
		}

		$recordSet->close();
		
		return $resultDashboardGroup;
		
	}
	
	public static function getAllUserPubDashboardGroups($aUserIDsCollection, $aRoleIDsCollection, $aConnection)
	{
		if(is_array($aUserIDsCollection))
			$countUserIDs = count($aUserIDsCollection);
		else
			$countUserIDs = 0;
		if(is_array($aRoleIDsCollection))
			$countRoleIDs = count($aRoleIDsCollection);
		else
			$countRoleIDs = 0;
		$aSQL = "SELECT ESC0.CLA_ESCENARIO, ESC0.NOM_ESCENARIO, ESC0.CLA_USUARIO, ESC0.CLA_GPO_ESC FROM SI_ESCENARIO ESC0 "."\n";
		
		for ($i = 0; $i < $countUserIDs-1; $i++)
		{
			$aSQL .= "JOIN SI_ESCENARIO ESC". ($i+1) ." ON ESC". $i .".CLA_ESCENARIO = ESC". ($i+1) .".CLA_ESCENARIO "."\n";
		}
		for ($j = $i; $j < $countUserIDs+$countRoleIDs-1; $j++)
		{
			$aSQL .= "JOIN SI_ESCENARIO ESC". ($j+1) ." ON ESC". $j .".CLA_ESCENARIO = ESC". ($j+1) .".CLA_ESCENARIO "."\n";
		}
		
		//@JAPR 2015-10-06: Agregada la validación de escenarios visibles a partir del Dummy insertado y configurado en SI_CONFIGURA (#XMK1CP)
		$intFirstUserDashboardID = 0;
		$sql = "SELECT REF_CONFIGURA 
			FROM SI_CONFIGURA 
			WHERE CLA_CONFIGURA = 703";
		$aRS = $aConnection->Execute($sql);
		if ($aRS && !$aRS->EOF) {
			$intFirstUserDashboardID = (int) @$aRS->fields[0];	//"ref_configura"
			if ($intFirstUserDashboardID <= 0) {
				$intFirstUserDashboardID = 0;
			}
		}
		
		//Debido a que en eForms no se ha utilizado nunca un array de IDs de usuarios con mas de un elemento, es seguro a la fecha de implementación sólo aplicar esta condición
		//para el único SELECT que debería haber, sin embargo si se llegase a recibir un array de usuarios, debería replicarse la misma condición a todos los JOINs creados en el SELECT
		if ($intFirstUserDashboardID > 0) {
			$aSQL .="WHERE ESC0.CLA_ESCENARIO > {$intFirstUserDashboardID}"."\n";
			$first = false;
		}
		else {
			$aSQL .="WHERE "."\n";
			$first = true;
		}
		//@JAPR
		$whereCount = 0;
		if ($countUserIDs) foreach ($aUserIDsCollection as $aUserID)
		{
			if ($first)
				$first = false;
			else
				$aSQL .= "\n"."AND ";
			
			$aSQL .=" (ESC". $whereCount .".CLA_USUARIO = ". $aUserID ." OR "."\n".
			 "ESC". $whereCount .".CLA_ESCENARIO IN (SELECT CLA_ESCENARIO FROM SI_ESC_PUB WHERE CLA_SUSCRIPTOR = ". $aUserID .") OR "."\n".
			 "ESC". $whereCount .".CLA_ESCENARIO IN (SELECT CLA_ESCENARIO FROM SI_ESC_GPO WHERE CLA_GPO IN (SELECT CLA_ROL FROM SI_ROL_USUARIO WHERE CLA_USUARIO = ". $aUserID .")))";
			$whereCount++;
		}
		if ($countRoleIDs) foreach ($aRoleIDsCollection as $aRoleID)
		{
			if ($first)
				$first = false;
			else
				$aSQL .= "\n"."AND ";
			$aSQL .=" (ESC". $whereCount .".CLA_ESCENARIO IN (SELECT CLA_ESCENARIO FROM SI_ESC_GPO WHERE CLA_GPO = ". $aRoleID ." ))";
			$whereCount++;
		}

		$aSQL .= "\n"." ORDER BY ESC0.NOM_ESCENARIO";
		$recordSet = $aConnection->Execute($aSQL);
		
		$title = "Loading Users Public Dashboards: ";
		//$msg .= $aSQL;
		//AdvisorConfiguration::logString($msg, true, $_SESSION['ADVISOR_ServiceName'], $title);
		
		$resultDashboardGroup = new AdvisorDashboardGroup();
		$resultDashboardGroup->cla_gpo_esc = 1;
		$resultDashboardGroup->nom_gpo_esc = translate("Public Dashboard");
		$resultDashboardGroup->register();
		
		while (!$recordSet->EOF)
		{
			$aDashboard = AdvisorDashboard::withKey($recordSet->fields[0]);
			
			if (is_null($aDashboard))
			{
				//$aDashboard = AdvisorDashboard::withKeyFromDB($recordSet->fields["cla_escenario"], $aConnection);
				$aDashboard = new AdvisorDashboard();
				$aDashboard->fillFromRowDB($recordSet->fields);
				$aDashboard->register();
				$resultDashboardGroup->addDashboard($aDashboard);
			}
			//$aDashboard->assignDashboardGroup($aDashboardGroup->cla_gpo_esc);

			$recordSet->MoveNext();
		}

		$recordSet->close();
		
		return $resultDashboardGroup;
	}
	
	public static function fromSQLFromDB($aSQL, $aConnection)
	{
		$recordSet = $aConnection->Execute($aSQL);

		$aCol = array();

		while (!$recordSet->EOF)
		{
			$anObject = new AdvisorDashboardGroup();
			$anObject->fillFromRowDB($recordSet->fields);
			$anObject->register();

			$aCol[] = $anObject;

			$recordSet->MoveNext();
		}

		$recordSet->close();

		return($aCol);
	}

	private function fillFromRowDB($aRow)
	{
		$this->cla_gpo_esc = $aRow[0];
		$this->nom_gpo_esc = rtrim($aRow[1]);
		$this->cla_usuario = $aRow[2];
		$this->tipo_gpo_esc = $aRow[3];
	}
	
	public function addDashboard($aDashboard)
	{
		array_push($this->Dashboards, $aDashboard);
	}
}


?>