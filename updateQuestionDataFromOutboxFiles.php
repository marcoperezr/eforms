<?php
/* Este archivo se utilizará para actualizar las tablas de captura de datos de las encuestas a partir de los datos almacenados en los archivos .dat
sin necesitar de reprocesar por completo el archivo ni forzar a una edición (ya que la edición realiza cambios a la info que llega en los catálogos
por lo que no se podría usar el mismo archivo .dat de la carga original para simular una edición)

	Originalmente fué creado para el caso específico de Rich en el que una pregunta Multiple-choice tenía opciones de respuesta triplicadas y eso
provocó que en ninguna de las capturas de aprox. 5 meses se grabaran datos para esa pregunta, por lo tanto se necesitó alguna manera de recorrer
todos los archivos .dat para extraer el valor de esa pregunta exclusivamente, para posteriormente actualizarlo en las tablas paralela, de dimensión
y de hecho correspondientes sin alternar en absoluto al resto de las preguntas ni la cantidad de registros generados

	Eventualmente este archivo se podrá ir expandiendo para soportar todos los tipos de preguntas e incluso reprocesar en base a secciones o una
serie adicional de condiciones, al momento de implementación el archivo estaba preparado para utilizar arrays que indican la encuesta y pregunta
específica que debía reprocesarse, así como el rango de fechas del cual se tenían que reprocesar los archivos sin necesidad de especificar filtros
adicionales ya que el error fué general

	En la sección 'SETTINGS' se dejará el historial de los casos que se han corregido usando este método (favor de no eliminarlo y seguir la misma
estructura), siempre conteniendo el caso que se usará en la siguiente/última ejecución. El archivo debe ejecutarse en el mismo servicio que se
encuentra la cuenta maestra correspondiente, además debe ejecutarse como todos los archivos de actualización, directamente en el browser previo
login al Administrador de eForms con la cuenta maestra en la que se encuetre el problema

	El método siempre (a menos que explícitamente se agregue soporte para ello posteriormente) asumirá que se desea grabar los datos ajustados a
la última versión soportada por el servicio en que se programa, por lo tanto no hay necesidad de recibir el AppVersion pues cualquier captura
que no cumpliera con la versión actual será actualizada automáticamente para la pregunta que será sobreescrita
*/
$theRepositoryName = '';

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

@ob_start();
logString(str_repeat('*', 80));
logString('Starting eForms repair process');

// *********************************************************************************************************************************************
// *********************************************************************************************************************************************
// ----------------------------------------------------------------- SETTINGS -----------------------------------------------------------------
/* Parámetros para indicarle al proceso la encuesta, pregunta(s), fecha(s) y demás filtro(s) que debe aplicar al momento de reprocesar los
archivos .dat. Los filtros se combinarán para procesar sólo aquellos outbox que cumplan con todos ellos. No configurar un filtro es equivalente
a procesar cualquier archivo sin considerar a dicho criterio
	Se utilizará la configuración de la sesión activa para identificar el repositorio a utilizar
*/
// *********************************************************************************************************************************************
// *********************************************************************************************************************************************
$arrSettings = array();
$arrSettings['StartDate'] = '20130813105220';			//Fecha de inicio del rango de reprocesamiento de capturas (formato numérico)
$arrSettings['EndDate'] = '20130813110536';				//Fecha de fin del rango de reprocesamiento de capturas (formato numérico)
$arrSettings['Surveys'] = array(413 => 'EFLORES Encuesta del proceso Reupload para Rich');	//Array indexado por los IDs de encuestas que se quieran reprocesar
$arrSettings['Users'] = array("eflores@bitam.com" => "eflores@bitam.com");						//Array indexado por las cuentas de usuarios que se quieran reprocesar
$arrSettings['Questions'] = array(5039);					//Array de IDs de las preguntas que deberán ser grabadas nuevamente (esto está sujeto a que dicho tipo de pregunta y para la sección correspondiente ya tenga un método programado para su grabado)

/* //Rich problema con Multiple Choice que repitió 3 respuestas como "."
$arrSettings['StartDate'] = '20130307164505';			//Fecha de inicio del rango de reprocesamiento de capturas (formato numérico)
$arrSettings['EndDate'] = '20130728075624';				//Fecha de fin del rango de reprocesamiento de capturas (formato numérico)
$arrSettings['Surveys'] = array(2 => 'Visita Tecnica');	//Array indexado por los IDs de encuestas que se quieran reprocesar
$arrSettings['Users'] = array();						//Array indexado por las cuentas de usuarios que se quieran reprocesar
$arrSettings['Questions'] = array(97);					//Array de IDs de las preguntas que deberán ser grabadas nuevamente (esto está sujeto a que dicho tipo de pregunta y para la sección correspondiente ya tenga un método programado para su grabado)
*/

$dteStartDate = null;
if (isset($arrSettings['StartDate'])) {
	$dteStartDate = $arrSettings['StartDate'];
	$arrSettings['StartDate'] = formatNumericDate($dteStartDate).' '.formatNumericTime(substr($dteStartDate, 8));
}
$dteEndDate = null;
if (isset($arrSettings['EndDate'])) {
	$dteEndDate = $arrSettings['EndDate'];
	$arrSettings['EndDate'] = formatNumericDate($dteEndDate).' '.formatNumericTime(substr($dteEndDate, 8));
}

logString('Settings:');
PrintMultiArray($arrSettings);
if (!is_null($dteStartDate)) {
	$arrSettings['StartDate'] = $dteStartDate;
}
if (!is_null($dteEndDate)) {
	$arrSettings['EndDate'] = $dteEndDate;
}

// *********************************************************************************************************************************************
// *********************************************************************************************************************************************
// --------------------------------------------------------------- INITIAL SETUP ---------------------------------------------------------------
// *********************************************************************************************************************************************
// *********************************************************************************************************************************************

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

//Parámetro de seguridad, si no se especifica entonces no se lanzará el proceso para evitar que se reprocese con la última configuración guardada
$blnAreYouSure = getParamValue('imsure', 'both', "(int)", true);

if (!$blnAreYouSure) {
	logString('Please check the last parameters inside the "SETTINGS" section before invoking this process. If everything is ok, add the parameter imsure=1 to start the process');
	die();
}

@setAppVersion();

//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
global $arrMasterDetSections;
global $arrMasterDetSectionsIDs;
//@JAPR 2014-05-28: Agregado el tipo de sección Inline
global $arrInlineSections;
global $arrInlineSectionsIDs;
//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $dieOnError;
$dieOnError = true;
//@JAPR 2013-07-11: Movidas estas configuraciones al archivo eFormsAgentConfig.inc.php para permitir incluirlo tanto en el servicio como en el propio
//agente y que en ambos casos tome las configuraciones sin tener que mantenerlas en varios lugares
//@JAPR 2013-06-21: Agregado el agente de eForms
/*
global $blnIsAgentEnabled;
$blnIsAgentEnabled = true;
//Esta es una lista de bases de datos que se deben excluir del proceso del agente, de tal forma que trabajarán como antes grabando la información
//inmediatamente cuando es sincronizada al servidor. El formato es $arrDBsExcludedFromAgent['FBM_BMD_####'] = 1;
global $arrDBsExcludedFromAgent;
$arrDBsExcludedFromAgent = array();
//Esta es una lista de bases de datos que son las únicas que van a usar el agente. Si no contiene elementos entonces se asume que todas las bases
//de datos usarán el agente El formato es $arrDBsExcludedFromAgent['FBM_BMD_####'] = 1;
//Las bases de datos que no se encuentren en esta lista, funcionarán tal como si estuviera en el array $arrDBsExcludedFromAgent
global $arrDBsEnabledForAgent;
$arrDBsEnabledForAgent = array();
*/
//@JAPR 2012-05-21: Validado que las acciones sólo se generen una vez cuando existen secciones Maestro - Detalle o dinámicas (las preguntas que
//se encuentra dentro de ellas pueden grabar múltiples acciones, pero las que están fuera de ellas sólo pueden grabar una acción)
global $arrProcessedActionQuestions;
$arrProcessedActionQuestions = array();
global $arrQuestionOptions;
$arrQuestionOptions = array();
//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
global $usePhotoPaths;
//Por default no se usan photo paths, esto es, se recibe directamente el B64, sólo con la v4.01000 o posterior se pueden recibir rutas de
//imagenes por lo que dentro de eSurveyServiceMod.inc.php se cambiará esta bandera a true para variar como funciona SavePhoto
$usePhotoPaths = true;
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $useRecoveryPaths;
//Esta bandera indica que en lugar de usar la carpeta SyncData, se utilizaría SyncErrors (sólo aplica cuando $usePhotoPaths ==true)
$useRecoveryPaths = false;
//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
global $eBavelAppCodeName;
global $arrayPDF;
global $arrActionItems;
$arrActionItems = array();
global $arrNumericQAnswersColl;
$arrNumericQAnswersColl = array();
//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
global $blnEBavelSupLoaded;
$blnEBavelSupLoaded = false;
global $arrEBavelSupervisors;
$arrEBavelSupervisors = array();
//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
global $blnUseMaxLongDummies;
$blnUseMaxLongDummies = false;
//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
global $blnWebMode;
//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
global $arrEditedFactKeys;
$arrEditedFactKeys = array();
//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
global $strLastUpdateDate;
global $strLastUpdateEndDate;
global $strLastUpdateTime;
global $strLastUpdateEndTime;
$strLastUpdateDate = '';
$strLastUpdateEndDate = '';
$strLastUpdateTime = '';
$strLastUpdateEndTime = '';
//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
global $strPDFFileNamePrefix;
$strPDFFileNamePrefix = '';
global $intPDFFilaNamePrefixCont;
$intPDFFilaNamePrefixCont = 0;
//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
global $arrDynamicCatalogAttributes;
//@JAPR 2014-05-29: Agregado el tipo de sección Inline
global $arrInlineCatalogAttributes;
//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
global $gstrOutboxFilePath;
$gstrOutboxFilePath = '';
global $gstrOutboxFileName;
$gstrOutboxFileName = '';

require_once('processSurveyFunctions.php');

global $blnSaveLogStringFile;
$blnSaveLogStringFile = false;
global $gblShowErrorSurvey;
$gblShowErrorSurvey = false;		//Para que no haga un die en caso de errores dentro de los diferences métodos estáticos de carga, sino que regrese un texto con error para permitir continuar con el proceso
global $gblEFormsNA;

$appVersion = getParamValue('appVersion', 'both', '(string)', true);
if (is_null($appVersion))
{
	$appVersion = @ESURVEY_SERVICE_VERSION;
}
$blnDecodeUTF8 = getParamValue('utf8decode', 'both', "(int)", true);

/* Variantes específicas por captura:
	- $blnWebMode debe ser asignada dinámicamente si se quiere procesar fotos, ya que los archivos .dat están mezclados y no todos se suben desde
web o desde móvil, por tanto una única variable no funciona en esos casos
	- $blnUseMaxLongDummies debe ser asignada dinámicamente en base a los posibles IDs usados por la captura, ya que es un cambio que evolucionó
con el tiempo así que algunas capturas pudieron haber usado IDs longs mientras que otras negativos para indicar a los responsables (aplica sólo
en el grabado de acciones y es dependiente de la versión del app que sincronizó)
*/

// *********************************************************************************************************************************************
// *********************************************************************************************************************************************
// ------------------------------------------------------------------ PROCESS ------------------------------------------------------------------
// *********************************************************************************************************************************************
// *********************************************************************************************************************************************
//Primero limpia el caché de archivos para que no ocurran errores de identificación o cosas así
clearstatcache();

$strOutBoxPath = getcwd();
$strOutBoxPath .= "\\syncdata\\".$theRepositoryName;

//Obtiene todos los archivos de outbox que se encuentren en la carpeta syncData y empieza a aplicar los filtros especificados para generar un array
//de los archivos que finalmente tendrá que procesar
$arrOutboxToProcess = array();
$arrOutboxToSkip = array();
$arrOutboxFailed = array();
$arrOutboxUnidentified = array();

logString("Outbox path: {$strOutBoxPath}");

$handler = @opendir($strOutBoxPath);
if ($handler === false) {
	logString('Error opening the path handler');
	die();
}

$arrQuestionIDs = @$arrSettings['Questions'];
if (is_null($arrQuestionIDs) || !is_array($arrQuestionIDs)) {
	$arrQuestionIDs = array();
}
if (count($arrQuestionIDs) == 0) {
	logString('You must specify the questions to restore before running this process in the form: $arrSettings["Questions"] = array(intQuestionID)');
	die();
}

// open directory and walk through the filenames
$errorLogPrefix = "surveyanswers_";
while ($file = @readdir($handler)) {
	// if file isn't this directory or its parent, add it to the results
	if ($file != "." && $file != "..") {
		//Tiene que identificar el archivo como un outbox, como no se tiene un nombre de usuario, tratará de hacer match del segundo parámetro
		//separado por "_" hasta encontrar la primer "@" y buscará ese nombre como un usuario del repositorio, de no encontrarse entonces no
		//se procesará el archivo, esto se hace on la función unformatFileName así que en este punto con que se trate de un outbox basta
		if(strpos($file, $errorLogPrefix) !== false) {
			$arrFileData = unformatFileNameWOUser($file, $aRepository);
			$blnValid = true;
			$strUserName = '';
			if ($arrFileData === false || !is_array($arrFileData) || count($arrFileData) == 0) {
				$blnValid = false;
			}
			else {
				//Verifica que se tenga un nombre de usuario
				$strUserName = (string) @$arrFileData['userName'];
				if (trim($strUserName) == '') {
					$blnValid = false;
				}
			}
			
			if ($blnValid) {
				//Una vez que se logra identificar el usuario, se aplican los criterios para considerar como válido este outbox
				$intSurveyID = (int) @$arrFileData['surveyID'];
				$dteStartDate = (string) @$arrFileData['date'];
				$dteStartHour = (string) @$arrFileData['hour'];
				$dteEntryDate = $dteStartDate.$dteStartHour;
				
				$blnValid = true;
				if (count($arrSettings['Users']) > 0 && !isset($arrSettings['Users'][$strUserName])) {
					$blnValid = false;
				}
				if (count($arrSettings['Surveys']) > 0 && !isset($arrSettings['Surveys'][$intSurveyID])) {
					$blnValid = false;
				}
				if (isset($arrSettings['StartDate']) && $dteEntryDate < $arrSettings['StartDate']) {
					$blnValid = false;
				}
				if (isset($arrSettings['EndDate']) && $dteEntryDate > $arrSettings['EndDate']) {
					$blnValid = false;
				}
				
				if ($blnValid) {
					$arrOutboxToProcess[] = $file;
				}
				else {
					$arrOutboxToSkip[] = $file;
				}
			}
			else {
				$arrOutboxUnidentified[] = $file;
			}
		}
	}
}

// tidy up: close the handler
@closedir($handler);
// done!

logString('Total outbox files read: '.count($arrOutboxToProcess) + count($arrOutboxToSkip) + count($arrOutboxUnidentified));
logString('Number of files to process: '.count($arrOutboxToProcess));
logString('Number of unidentified files: '.count($arrOutboxUnidentified));
require_once('json.class.php');

//Prepara los arrays de datos de las encuestas y preguntas utilizadas para no estar cargando varias veces la misma información
$arrSurveys = array();
$arrSurveys = array();
$arrSectionsBySurvey = array();
$arrQuestionsBySurvey = array();
$arrayInstancesMultiDim = array();
$arrayInstancesMultiChoiceInds = array();

//Inicia el procesamiento de cada archivo de outbox que si cumplió con los criterios
foreach ($arrOutboxToProcess as $file) {
	try {
		//Primero carga el contenido del Outbox, ya que si ocurre un error en ese punto no se podrá hacer nada mas. El contenido viene en utf8
		//así que se deben hacer las conversiones correspondientes según el tipo de pregunta
    	logString("Processing outbox file: ".$file);
		$strCompletePath = $strOutBoxPath."\\".$file;
		$strJSON = @file_get_contents($strCompletePath);
		if ($strJSON === false)
		{
	    	logString("The data file name specified was not found: ".$strCompletePath);
	    	continue;
		}
		
		$json = new Services_JSON();
		$jsonData = $json->decode($strJSON);
		$jsonData = object_to_array($jsonData);
    	logString("Outbox data: ");
		PrintMultiArray($jsonData);
		@ob_flush();
		
		$arrAnswers = @$jsonData['answers'];
		if (is_null($arrAnswers) || !is_array($arrAnswers)) {
			//En este caso al no haber datos, no tiene caso siquiera continuar con el proceso
	    	logString("No data found in the outbox file");
	    	continue;
		}
		
		//Carga la encuesta, las preguntas correspondientes y el resto de los objetos que se van a requerir para procesar el outbox
		$arrFileData = unformatFileNameWOUser($file, $aRepository);
		$intSurveyID = (int) @$arrFileData['surveyID'];
		$strUserName = (string) @$arrFileData['userName'];
		$dteFmtStartDate = (string) @$arrFileData['fmtdate'];
		$dteFmtStartHour = (string) @$arrFileData['fmthour'];
		$dteFmtEntryDate = $dteFmtStartDate.' '.$dteFmtStartHour;
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$objUser = @BITAMeFormsUser::NewInstanceWithEMail($aRepository, $strUserName);
		if (is_null($objUser) || !is_object($objUser)) {
	    	logString("Error loading the user ({$strUserName}), Error: ".(string) $objUser);
	    	continue;
		}
		
		$objSurvey = @$arrSurveys[$intSurveyID];
		if (is_null($objSurvey)) {
			$objSurvey = BITAMSurvey::NewInstanceWithID($aRepository, $intSurveyID);
			$arrSurveys[$intSurveyID] = $objSurvey;
		}
		if (is_null($objSurvey) || !is_object($objSurvey)) {
	    	logString("Error loading the survey ({$intSurveyID}), Error: ".(string) $objSurvey);
			continue;
		}
		
		$objSectionColl = @$arrSectionsBySurvey[$intSurveyID];
		if (is_null($objSectionColl)) {
			$objSectionColl = BITAMSectionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_object($objSectionColl)) {
				$objSectionCollByID = array();
				foreach ($objSectionColl->Collection as $objSection) {
					$objSectionCollByID[$objSection->SectionID] = $objSection;
				}
				$objSectionColl = $objSectionCollByID;
				$arrSectionsBySurvey[$intSurveyID] = $objSectionColl;
			}
		}
		
		if (is_null($objSectionColl) || !is_array($objSectionColl)) {
	    	logString("Error loading the survey sections ({$intSurveyID}): ".(string) $objSectionColl);
			continue;
		}

		$objQuestionColl = @$arrQuestionsBySurvey[$intSurveyID];
		if (is_null($objQuestionColl)) {
			$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $intSurveyID);
			if (is_object($objQuestionColl)) {
				$objQuestionCollByID = array();
				foreach ($objQuestionColl->Collection as $objQuestion) {
					$objQuestionCollByID[$objQuestion->QuestionID] = $objQuestion;
				}
				$objQuestionColl = $objQuestionCollByID;
				$arrQuestionsBySurvey[$intSurveyID] = $objQuestionColl;
			}
		}
		if (is_null($objQuestionColl) || !is_array($objQuestionColl)) {
	    	logString("Error loading the survey questions ({$intSurveyID}): ".(string) $objQuestionColl);
			continue;
		}
		
		$tableDimFactKey = "RIDIM_".$objSurvey->FactKeyDimID;
		$fieldDimFactKeyKey = $tableDimFactKey."KEY";
		
		//Identifica el ID de reporte de esta captura para actualizar sólo sobre los registros que le pertenezcan
		$intFactKeyDimVal = 0;
		$sql = "SELECT DISTINCT A.FactKeyDimVal 
			FROM {$objSurvey->SurveyTable} A 
			WHERE A.UserID = {$objUser->UserID} AND A.DateID = ".$aRepository->DataADOConnection->DBTimeStamp($dteFmtEntryDate);
    	logString($sql);
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
	    if ($aRS && !$aRS->EOF) {
        	$intFactKeyDimVal = (int) @$aRS->fields["factkeydimval"];
		}
		if ($intFactKeyDimVal <= 0) {
	    	logString("Error retrieving the report id for this outbox");
			continue;
		}
		
		$intStdSecFactKey = 0;
		$sql = "SELECT DISTINCT A.FactKey 
			FROM {$objSurvey->SurveyTable} A 
			WHERE A.FactKeyDimVal = $intFactKeyDimVal AND A.EntrySectionID = 0 AND A.EntryCatDimID = 0";
    	logString($sql);
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
	    if ($aRS && !$aRS->EOF) {
        	$intStdSecFactKey = (int) @$aRS->fields["factkey"];
		}
		if ($intStdSecFactKey <= 0) {
	    	logString("Error retrieving the report single record id for this outbox");
			continue;
		}
		
		//Inicia el ciclo de reprocesamiento de las preguntas
		foreach ($arrQuestionIDs as $intQuestionID) {
	    	logString("Processing question: ".$intQuestionID);
			$objQuestion = @$objQuestionColl[$intQuestionID];
			if (is_null($objQuestion)) {
				logString('Unable to find the specified question '.$intQuestionID);
				continue;
			}
			
			$intSectionID = $objQuestion->SectionID;
			$objSection = @$objSectionColl[$intSectionID];
			if (is_null($objSection)) {
				logString('Unable to find the specified section '.$intSectionID);
				continue;
			}
			
			//Obtiene la respuesta a la pregunta desde el Outbox. El proceso original sólo reprocesaba la pregunta si había alguna respuesta
			$objAnswer = @$arrAnswers[$intQuestionID];
			if (is_null($objAnswer)) {
				//En este caso la pregunta no fué contestada, así que no hay que reprocesar nada mas
				//@JAPRWarning: Si alguna vez se requiere, se podría forzar a continuar con el grabado de un dato vacio, si es que la pregunta
				//se hubiera grabado con basura, en ese caso se debe brincar esta validación mediante algún parámetro
				logString("The question was not answered in this outbox ({$intQuestionID})");
				continue;
			}
			
			//Basado en el tipo de pregunta y sección, tiene que hacer los UPDATEs correspondientes
			//@JAPRWarning: Se podría reutilizar eventualmente el código de la función StoreSurveyData para preparar el dato específico a grabar,
			//ya que esa parte es igual entre dicha función y este proceso, sin embargo la parte final de los UPDATEs si es exclusiva de este 
			//proceso. Mientras tanto se tendrá que mantener sincronizado el código en ambos métodos
			switch ($objQuestion->QTypeID) {
				case qtpMulti:
					//Primero genera un array con las opciones de respuesta como índice y cuyo contenido será vacio, así se llenará según el
					//tipo de captura con el valor correspondiente, pero sólo si no es tipo de captura Checkbox
					if (count($objQuestion->SelectOptions) > 0 && $objQuestion->MCInputType != mpcCheckBox) {
						$arrSelectOptions = array_combine(array_keys(array_flip($objQuestion->SelectOptions)), array_fill(0, count($objQuestion->SelectOptions), ''));
					}
					else {
						$arrSelectOptions = array();
					}
					
					require_once("../model_manager/model.inc.php");
					require_once("../model_manager/modeldata.inc.php");
					require_once("../model_manager/modeldimension.inc.php");
					require_once("../model_manager/indicatorkpi.inc.php");
					
					//Obtiene las dimensiones de sus opciones de respuesta si es que aplican
					$existIndicators = false;
					$arrayTempDims = array();
					$arrayTempInds = array();
					if ($objQuestion->IsMultiDimension == 1) {
						$arrayTempDims = @$arrayInstancesMultiDim[$intQuestionID];
						if (is_null($arrayTempDims)) {
							$arrayTempDims = array();
							//Recorremos las opciones para revisar las dimensiones
							foreach ($objQuestion->SelectOptions as $optionKey => $optionValue) {
								$qDimID = 0;
								if (isset($objQuestion->QIndDimIds[$optionKey])) {
									$qDimID = (int) $objQuestion->QIndDimIds[$optionKey];
								}
								else {
									$qDimID = 0;
								}
								
								if (!is_null($qDimID) && $qDimID != 0) {
									$arrayTempDims[$optionKey] = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $objQuestion->ModelID, -1, $qDimID);
								}
								else {
									$arrayTempDims[$optionKey] = null;
								}
							}
							
							$arrayInstancesMultiDim[$intQuestionID] = $arrayTempDims;
						}
					}
					
					//Obtiene los indicadores de sus opciones de respuesta si es que aplican
					if ($objQuestion->MCInputType == mpcCheckBox) {
						$arrayTempInds = @$arrayInstancesMultiChoiceInds[$intQuestionID];
						if (is_null($arrayTempInds)) {
							$arrayTempInds = array();
							
							//Recorremos las opciones para revisar los indicadores
							foreach ($objQuestion->SelectOptions as $optionKey => $optionValue) {
								$indicatorid = 0;
								if (isset($objQuestion->QIndicatorIds[$optionKey])) {
									$indicatorid = (int) $objQuestion->QIndicatorIds[$optionKey];
								}
								else {
									$indicatorid = 0;
								}
								
								if (!is_null($indicatorid) && $indicatorid != 0) {
									$existIndicators = true;
									$arrayTempInds[$optionKey] = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $indicatorid);
								}
								else {
									$arrayTempInds[$optionKey] = null;
								}
							}
							
							//Si no hubiera al menos un indicador válido, entonces simplemente limpia el array
							if (!$existIndicators) {
								$arrayTempInds = array();
							}
							
							$arrayInstancesMultiChoiceInds[$intQuestionID] = $arrayTempInds;
						}
						else {
							$existIndicators = count($arrayTempInds) > 0;
						}
					}
					chdir($strOriginalWD);
					
					switch ($objSection->SectionType) {
						case sectDynamic:
							break;
						
						case sectMasterDet:
							break;
						
						default:
							//En cualquier otro tipo de sección, las preguntas Multiple Choice vienen como una cadena sólo de los valores 
							//seleccionados si es tipo CheckBox, o bien una cadena del dato introducido para cada valor por cada opción
							//posible de respuesta se hubiera o no seleccionado, en ambos casos separada por "_SVSep_"
							$strData = '';
							$arrMulti = @$objAnswer['multiAnswer'];
							if (!is_null($arrMulti) && is_array($arrMulti)) {
								foreach ($arrMulti as $strOption => $strValue) {
									$strOptionDec = $strOption;
									$strValueDec = $strValue;
									if ($blnDecodeUTF8) {
										$strOptionDec = utf8_decode($strOptionDec);
										$strValueDec = utf8_decode($strValueDec);
									}
									if ($objQuestion->MCInputType == mpcCheckBox) {
										//Se agrega al array el elemento seleccionado
										//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
										//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
										if ($objQuestion->QDisplayMode == dspLabelnum) {
											$arrSelectOptions[(int) $strValueDec] = $strOptionDec;
										}
										else {
											$arrSelectOptions[] = $strOptionDec;
										}
										//@JAPR
									}
									else {
										//En este caso se sobreescribe del array el elemento seleccionado
										//@JAPR 2013-01-23: Como los datos vienen codificados en utf8, se tienen que decodificar para comparar contra el array local
										//que no está codificado
										if (isset($arrSelectOptions[utf8_decode($strOptionDec)])) {
											$arrSelectOptions[utf8_decode($strOptionDec)] = $strValueDec;
										}
										//@JAPR
									}
								}
								if (count($arrSelectOptions) > 0) {
									//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
									//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
									if ($objQuestion->QDisplayMode == dspLabelnum) {
										//Ahora ordena a partir de las llaves en forma ascendente numérica
										ksort($arrSelectOptions, SORT_NUMERIC);
									}
									//@JAPR
									$strData = implode(';', $arrSelectOptions);
								}
								
								//En este punto, $strData contiene el valor exacto que se usará en la tabla paralela, mientras que $arrSelectOptions
								//contiene las descripciones que fueron marcadas junto al valor que le correspondia según el tipo de despliegue
								//Las preguntas múltiple choice sólo se graban en la tabal de hechos si son multi-dimensión, y en caso de ser 
								//numéricas generarán un indicador si están marcadas para ello nada mas. Como es una pregunta de sección estándar,
								//repetirá su valor en todos los registros que se hubieran generado, pero la parte numérica sólo se agregará para
								//el registro único así que hay que hacer 2 diferentes UPDATEs
								
								//Actualización de la tabla paralela: Todos los registros idénticos
								$sql = "UPDATE {$objSurvey->SurveyTable} SET 
										{$objQuestion->SurveyField} = ".$aRepository->DataADOConnection->Quote($strData)." 
									WHERE FactKeyDimVal = {$intFactKeyDimVal}";
								logString('Updating survey table: '.$sql);
								if($aRepository->DataADOConnection->Execute($sql) === false)
								{
									logString('Error updating survey table '.$aRepository->DataADOConnection->ErrorMsg());
									continue;
								}
								
								//En caso de que la pregunta sea múlti-dimensión, entonces si se debe hacer un grabado en la tabla de hechos, para 
								//lo cual recorrerá todas las opciones de respuesta para actualizar el key subrrogado de la respuesta seleccionada
								//en la dimensión de cada opción de respuesta, pero adicionalmente si es una pregunta tipo checkbox y contiene
								//score, se agregará como valor el score a cada indicador de las opciones seleccionadas
								$blnHasFields = false;
								$strAnd = '';
								if (($objQuestion->IsMultiDimension == 1 && $objQuestion->QDisplayMode != dspMatrix) || $existIndicators) {
									$sql = "UPDATE {$objSurvey->FactTable} SET ";
									$arrayValuesFlip = array_flip($arrSelectOptions);
									foreach ($objQuestion->SelectOptions as $optionKey => $optionValue) {
										$anInstanceDim = @$arrayTempDims[$optionKey];
										if (!is_null($anInstanceDim)) {
											$blnHasFields = true;
											$dimFieldKey = "RIDIM_".(@$anInstanceDim->Dimension->DimensionClaDescrip)."KEY";
											if ($objQuestion->MCInputType == mpcCheckBox) {
												//Si es tipo CheckBox, entonces las opciones ya deben estar creadas en la dimensión así que sólo
												//asigna el valor correspondiente al key subrrogado que le debería tocar siempre
												if (isset($arrayValuesFlip[$optionValue])) {
													$sql .= $strAnd.$dimFieldKey.' = 2';
												}
												else {
													$sql .= $strAnd.$dimFieldKey.' = 3';
												}
												$strAnd = ', ';
											}
											else {
												if (!is_null($anInstanceDim)) {
													$strValue = (string) @$arrayValuesFlip[$optionValue];
													$passValue = getInsertDimMultiValueKey($aRepository, $anInstanceDim, $strValue);
													if ($passValue === false || !is_numeric($passValue)) {
														//En este caso a diferencia del proceso normal de grabado, no se interrumpirá el grabado sino que
														//asignará el valor de No Aplica para permitir continuar con la actualización
														$passValue = 1;
													}
													$sql .= $strAnd.$dimFieldKey.' = '.$passValue;
													$strAnd = ', ';
												}
											}
										}
										
										//Verifica si tiene indicador para grabar el score de acuerdo a ello. A la fecha de implementación sólo
										//las preguntas tipo checkbox podían grabar scores. Adicionalmente, no se había validado bien y estas
										//preguntas al ser de secciones estándar, grababan el score en todos los registros incluyendo los que no
										//eran de su sección, esto va en contra de los valores numéricos que en dichos casos grabarían NULL, pero
										//se dejará así por consistencia aunque realmente sea un error
										if ($objQuestion->MCInputType == mpcCheckBox) {
											$anInstanceInd = @$arrayTempInds[$optionKey];
											if (!is_null($anInstanceInd)) {
												$blnHasFields = true;
												$fieldName = $anInstanceInd->field_name.$anInstanceInd->IndicatorID;
												$dblScore = @$objQuestion->Scores[$optionKey];
												$strAnd = ', ';
												
												//Si se usa un registro único para las estándar entonces limpia en este punto el Score porque mas
												//adelante se va a hacer el UPDATE exclusivamente de dicho registros, de lo contrario lo tiene que
												//repetir en cada registro
												if ($objSurvey->UseStdSectionSingleRec) {
													$sql .= $strAnd.$fieldName.' = NULL';
												}
												else {
													$sql .= $strAnd.$fieldName.' = '.StrIFNULL($dblScore);
												}
											}
										}
									}
									
									if ($blnHasFields) {
										$sql .= " WHERE {$fieldDimFactKeyKey} = {$intFactKeyDimVal}";
										logString('Updating fact table: '.$sql);
										if ($aRepository->DataADOConnection->Execute($sql) === false) {
											logString('Error updating fact table '.$aRepository->DataADOConnection->ErrorMsg());
											continue;
										}
									}
									
									//Si tiene Scores los procesa sólo en el registro único
									if ($existIndicators && $objSurvey->UseStdSectionSingleRec && $objQuestion->MCInputType == mpcCheckBox) {
										$blnHasFields = false;
										$strAnd = '';
										$sql = "UPDATE {$objSurvey->FactTable} SET ";
										foreach ($objQuestion->SelectOptions as $optionKey => $optionValue) {
											$anInstanceInd = @$arrayTempInds[$optionKey];
											if (!is_null($anInstanceInd)) {
												$blnHasFields = true;
												$fieldName = $anInstanceInd->field_name.$anInstanceInd->IndicatorID;
												$dblScore = @$objQuestion->Scores[$optionKey];
												$sql .= $strAnd.$fieldName.' = '.StrIFNULL($dblScore);
												$strAnd = ', ';
											}
										}
										
										if ($blnHasFields) {
											$sql .= " WHERE FactKey = {$intStdSecFactKey}";
											logString('Updating fact table (scores): '.$sql);
											if ($aRepository->DataADOConnection->Execute($sql) === false) {
												logString('Error updating fact table (scores) '.$aRepository->DataADOConnection->ErrorMsg());
												continue;
											}
										}
									}
								}
								
								//Si tiene Scores los procesa sólo en el registro único
								if ($existIndicators) {
									$blnHasFields = true;
								}
							}
					}
					break;
			}
			
			@ob_flush();
		}
		@ob_flush();
	} catch (Exception $e) {
		logString('There was an error processing the outbox '.$file.', Error: '.$e->getMessage());
		$arrOutboxFailed[] = $file;
	}
}

logString('Finishing eForms repair process');
ob_end_flush();
?>