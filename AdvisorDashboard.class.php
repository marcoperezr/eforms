<?php

// Advisor Dashboards class
// 26 - Sept - 2008
//

class AdvisorDashboard
{
	public static $registered = array();
	
	public $cla_escenario = -1;
	public $cla_usuario = -1;
	public $cla_gpo_esc = -1;
	public $nom_escenario = "";
	
	const mainSQL = "SELECT A.CLA_ESCENARIO, A.NOM_ESCENARIO, A.CLA_USUARIO, A.CLA_GPO_ESC
        		 FROM   SI_ESCENARIO A";
	
	public function register()
	{
		array_push(AdvisorDashboard::$registered, $this);
	}
	
	public static function withKeys($aCol)
	{
		if (is_null($aCol))
			return (AdvisorDashboard::$registered);

		$res = array();

		foreach (AdvisorDashboard::$registered as $each)
		{
			if (in_array($each->cla_escenario, $aCol))
				$res[] = $each;
		}
		
		return ($res);
	}

	public static function withKey($aKey)
	{
		foreach (AdvisorDashboard::$registered as $each)
			if ($each->cla_escenario == $aKey)
				return ($each);

		return (null);
	}
	
	public static function withKeyFromDB($sDashboardID, $aConnection)
	{
		$aSQL = AdvisorDashboard::mainSQL;

		if (is_null($sDashboardID)||$sDashboardID=="")
		{
			return NULL;
		}
		else
		{
			$aSQL .= " where A.CLA_ESCENARIO = ".$sDashboardID;
		}

		$aSQL .= " order by A.NOM_ESCENARIO";

		$recordSet = $aConnection->Execute($aSQL);

		$result = array();

		if($recordSet)
		{
			$aDashboard = AdvisorDashboard::withKey($recordSet->fields[0]);

			if (is_null($aDashboard))
			{
				$aDashboard = new AdvisorDashboard();
				$aDashboard->fillFromRowDB($recordSet->fields);
				$aDashboard->register();
			}
		}
		else
		{
			return NULL;
		}

		$recordSet->close();
		
		return($aDashboard);
	}
	
	public static function fromSQLFromDB($aSQL, $aConnection)
	{
		$recordSet = $aConnection->Execute($aSQL);

		$aCol = array();

		while (!$recordSet->EOF)
		{
			$anObject = new AdvisorDashboard();
			$anObject->fillFromRowDB($recordSet->fields);
			$anObject->register();

			$aCol[] = $anObject;

			$recordSet->MoveNext();
		}

		$recordSet->close();

		return($aCol);
	}

	public function fillFromRowDB($aRow)
	{
		$this->cla_escenario = $aRow[0];
		$this->nom_escenario = rtrim($aRow[1]);
		$this->cla_usuario = $aRow[2];
		$this->cla_gpo_esc = $aRow[3];
	}
}
?>