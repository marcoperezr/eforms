<?php

require_once("repository.inc.php");
require_once("initialize.php");

//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
require_once("customClasses.inc.php");

class BITAMAppUser extends BITAMObject
{
	public $UserID;
	public $UserName;
	public $Email;
	public $SV_Admin;
	public $SV_User;
	public $ArtusUser;
	public $CLA_USUARIO;
	public $Password;
	public $FullName;
	public $Position;
	public $Role;
	public $intConfigSecurity;
	public $OwnerID;
	public $ChangePassword;
	public $LdapPath;
	public $FirstName;
	public $LastName;
	public $SendPDFByEmail;
	public $Supervisor;
	
	public $DefaultSurvey;
	//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
	public $AltEmail;
	//@JAPR 2014-04-02: Agregada la ventana para permitir enviar mensajes vía Push
	public $MobileDataRead;
	public $MobileData;	//Array multidimensional con la información de los dispositivos móviles que ha usado el usuario (necesario para mensajes de Push)
    //@JAPR 2014-06-20: Agregados los templates para personalización del App
    public $TemplateID;
	//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	public $ProfileID;					//Perfil asociado al usuario para limitar las encuestas que puede ver al descargar definiciones
	//@JAPR
	public $CuentaCorreoAlt;
	
	//@AAL Agregado para eFormsV6 y siguientes, se agrega una imagen de perfil y los grupos al que pueda pertenecer un usuario
	public $Image;
	public $GroupsAssct;
	//@AAL 19/07/2015: es true cuando se desasociarón o desmarcaron todos los grupos del usuario, en caso contrario false
	public $DissociateAll;
	//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
	public $AgendasSurveysTemplate;
	//GCRUZ 2016-05-05. Agregar idioma
	public $Language;
	//GCRUZ 2016-06-09. Datos de la GeoCerca
	public $GeoFenceRadius;
	public $GeoFenceLat;
	public $GeoFenceLng;
	//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
	public $UserParam1;
	public $UserParam2;
	public $UserParam3;
	
	//@ears 2016-08-02 última fecha de sincronización
	public $LastsyncDate;
	//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
	public $DefaultKPIProduct;							//ID del producto (basado en las constantes de SI_MD_VERSION) al que ingresará el usuario directamente cuando haga login desde KPIOnline
	//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
	public $ArtusWebUserLicense;
	//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
	public $ArtusAdminUserLicense;
	public $ArtusDesignerUserLicense;
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->UserID = -1;
		$this->UserName = "";
		$this->Email = "";
		$this->SV_Admin = 0;
		$this->SV_User = 1;
		$this->ArtusUser = "";
		$this->CLA_USUARIO = -1;
		$this->Password = "";
		$this->FullName = "";
		$this->Position = "";
		$this->Role = -1;
		$this->intConfigSecurity = 0;
		$this->OwnerID = -1;
		$this->ChangePassword = 0;
		$this->LdapPath = "";
		$this->FirstName = "";
		$this->LastName = "";
		$this->SendPDFByEmail = 0;
		$this->Supervisor = 0;
		$this->DefaultSurvey = 0;
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		$this->AltEmail = '';
		$this->CuentaCorreoAlt = '';		
		//@JAPR 2014-04-02: Agregada la ventana para permitir enviar mensajes vía Push
		$this->MobileDataRead = false;
		$this->MobileData = array();
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
	    $this->TemplateID = 0;
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		$this->ProfileID = -1;
		//@JAPR
		//@AAL 10/07/2015 Agregado eventualmente, solo para preparar el campo (images/user.gif)
		$this->Image = "images/user1.gif";
		$this->GroupsAssct = array();
		$this->DissasociateAll = false;
		//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
		$this->AgendasSurveysTemplate = '';
		//GCRUZ 2016-05-05. Agregar idioma
		$this->Language = 1;
		//GCRUZ 2016-06-09. Datos de la GeoCerca
		$this->GeoFenceRadius = 0;
		$this->GeoFenceLat = 0;
		$this->GeoFenceLng = 0;
		//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
		$this->UserParam1 = '';
		$this->UserParam2 = '';
		$this->UserParam3 = '';
		//@ears 2016-08-02 última fecha de sincronización
		$this->LastsyncDate = '';
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		$this->DefaultKPIProduct = prodDefault;
		//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
		$this->ArtusWebUserLicense = 0;
		//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
		$this->ArtusAdminUserLicense = 0;
		$this->ArtusDesignerUserLicense = 0;
	}
	
	public static function getQuery($opts) {
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$sQuery = $strCalledClass::getMainQuery();
		if(is_array($opts)) {
			if(isset($opts["filter"])) {
				$filter = $opts["filter"];
				if(is_string($filter) && $filter != '') {
					$sQuery .= (" " . $filter);
				}
			}
			if(isset($opts["orderBy"])) {
				$orderBy = $opts["orderBy"];
				if(is_string($orderBy) && $orderBy != '') {
					$sQuery .= (" ORDER BY " .$orderBy);
				}
			}
		}
		return $sQuery;
	}
	
	public static function applyUptodate($version, &$target, $subject) {
		if (getMDVersion() >= $version) {
			$target .= $subject;
		}
	}
	
	public static function getMainQuery () {
		$sQuery = "";
		$strAdditionalFields = "";
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClass::applyUptodate(esvAltEMailForNotif, $strAdditionalFields, ", A.Password ");
		$strCalledClass::applyUptodate(esvAltEMailForNotif, $strAdditionalFields, ", B.AltEmail ");
		$strCalledClass::applyUptodate(esvDefaultSurvey, $strAdditionalFields, ", B.DefaultSurvey ");
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		$strCalledClass::applyUptodate(esvTemplateStyles, $strAdditionalFields, ", B.TemplateID ");
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		$strCalledClass::applyUptodate(esvSurveyProfiles, $strAdditionalFields, ", B.ProfileID ");
		//@JAPR
		$strCalledClass::applyUptodate(esvCuentaCorreoAlt, $strAdditionalFields, ", A.Cuenta_Correo_Alt ");
		//@AAL 21/07/2015: Agregado para mostrar la imagen de perfil del usuario
		$strCalledClass::applyUptodate(esveFormsv6, $strAdditionalFields, ", B.Image ");
		//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
		$strCalledClass::applyUptodate(esvAgendaExportTemplate, $strAdditionalFields, ", B.AgendasSurveysTemplate ");
		//GCRUZ 2016-05-05. Agregar idioma
		$strCalledClass::applyUptodate(esvUserLanguage, $strAdditionalFields, ", A.WHTMLANG ");
		//GCRUZ 2016-06-09. Datos de la GeoCerca
		$strCalledClass::applyUptodate(esvGeoFence, $strAdditionalFields, ", B.GeoFenceRadius, B.GeoFenceLat, B.GeoFenceLng ");
		//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
		$strCalledClass::applyUptodate(esvUserParams, $strAdditionalFields, ", B.UserParam1, B.UserParam2, B.UserParam3 ");
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		//Este cambio es obligatorio a partir de esta fecha de actualización, se modificó Index.php para asegurar la existencia de los campos incluso si no cumplieran el requisito
		//de Metadata que se hubiera empleado (que finalmente NO se aplicó de dicha manera, pero se dejó la referencia por si mas adelante se quería identificar de alguna manera), por
		//eso se valida contra la constante fija 1 de tal manera que cualquier versión va a buscar este campo invariablemente
		//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
		$strCalledClass::applyUptodate(1, $strAdditionalFields, ", A.ARTUS_DESKTOP, A.KPI_DEFAULT");
		//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
		$strCalledClass::applyUptodate(1, $strAdditionalFields, ", A.ARTUS_ADMIN, A.ARTUS_DESIGNER");
		$sQuery = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, B.UserName AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor $strAdditionalFields 
			FROM SI_USUARIO A 
				LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO";
		return $sQuery;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'::NewInstance($aRepository);');
		}
		return new $strCalledClass($aRepository);
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceWithID($aRepository, $anUserID)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'::NewInstanceWithID($aRepository, $anUserID);');
		}

		$anInstance = null;
		
		if (((int)  $anUserID) < 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		//$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail,B.Supervisor ";

		$sFilter = "WHERE B.UserID = ".((int)$anUserID);
		$qOpts = array("filter" => $sFilter);

		$sql = $strCalledClass::getQuery($qOpts);
		//@JAPR
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceWithClaUsuario($aRepository, $cla_usuario)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'::NewInstanceWithClaUsuario($aRepository, $cla_usuario);');
		}

		$anInstance = null;
		
		if (((int)  $cla_usuario) < 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		//$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor ";

		$sFilter = "WHERE A.CLA_USUARIO = ".((int) $cla_usuario);
		$qOpts = array("filter" => $sFilter);
		$sql = $strCalledClass::getQuery($qOpts);
		//@JAPR
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}

		return $anInstance;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceFromRS($aRepository, $aRS, $isCollection = false)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'::NewInstanceFromRS($aRepository, $aRS, $isCollection);');
		}

		$anInstance = $strCalledClass::NewInstance($aRepository);

		$anInstance->UserID = (int) @$aRS->fields["userid"];
		$anInstance->UserName = (string) @$aRS->fields["username"];
		$anInstance->Email = (string) @$aRS->fields["email"];
		$anInstance->Image = (string) @$aRS->fields["image"];
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		if (getMDVersion() >= esvAltEMailForNotif) {
			$anInstance->AltEmail = (string) @$aRS->fields["altemail"];
		}
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		$anInstance->ProfileID = (int) @$aRS->fields["profileid"];
		//@JAPR
		if (getMDVersion() >= esvCuentaCorreoAlt) {
			$anInstance->CuentaCorreoAlt = (string) @$aRS->fields["cuenta_correo_alt"];
		}
		//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
		if (getMDVersion() >= esvAgendaExportTemplate) {
			$anInstance->AgendasSurveysTemplate = (string) @$aRS->fields["agendassurveystemplate"];
		}
		//GCRUZ 2016-05-05. Agregar idioma
		if (getMDVersion() >= esvUserLanguage) {
			$anInstance->Language = (string) @$aRS->fields["whtmlang"];
		}
		//GCRUZ 2016-06-09. Datos de la GeoCerca
		if (getMDVersion() >= esvGeoFence) {
			$anInstance->GeoFenceRadius = (int) @$aRS->fields["geofenceradius"];
			$anInstance->GeoFenceLat = (double) @$aRS->fields["geofencelat"];
			$anInstance->GeoFenceLng = (double) @$aRS->fields["geofencelng"];
		}
		//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
		if (getMDVersion() >= esvUserParams) {
			$anInstance->UserParam1 = (string) @$aRS->fields["userparam1"];
			$anInstance->UserParam2 = (string) @$aRS->fields["userparam2"];
			$anInstance->UserParam3 = (string) @$aRS->fields["userparam3"];
		}
		$anInstance->SV_Admin = (int)$aRS->fields["sv_admin"];
		$anInstance->SV_User = (int)$aRS->fields["sv_user"];
		
		$anInstance->ArtusUser = $aRS->fields["artususer"];
		$anInstance->CLA_USUARIO = (int)$aRS->fields["cla_usuario"];
		
	 	if(array_key_exists("sendpdfbyemail",$aRS->fields))
	 	{
	 		$anInstance->SendPDFByEmail = (int)$aRS->fields["sendpdfbyemail"];
	 	}
	 	
	 	if(array_key_exists("supervisor",$aRS->fields))
	 	{
	 		$anInstance->Supervisor = (int)$aRS->fields["supervisor"];
	 	}
		
		if(array_key_exists("DefaultSurvey",$aRS->fields))
	 	{
	 		$anInstance->DefaultSurvey = (int)@$aRS->fields["DefaultSurvey"];
	 	}
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		$anInstance->DefaultKPIProduct = (int) @$aRS->fields["kpi_default"];
		//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
		$anInstance->ArtusWebUserLicense = (int) @$aRS->fields["artus_desktop"];
		//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
		$anInstance->ArtusAdminUserLicense = (int) @$aRS->fields["artus_admin"];
		$anInstance->ArtusDesignerUserLicense = (int) @$aRS->fields["artus_designer"];
    	
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		if ($anInstance->TemplateID <= 0) {
			$anInstance->TemplateID = -1;
		}
	    //@JAPR
		$anInstance->getRole();
		$anInstance->getArtusUserInfo();

		//Si no se trata de una colección nos traemos los grupos de usuarios existentes
		if(!$isCollection) {
			$sql = "SELECT CLA_ROL FROM SI_ROL_USUARIO WHERE CLA_USUARIO = {$anInstance->CLA_USUARIO} ORDER BY CLA_ROL";
			$RS = $aRepository->ADOConnection->Execute($sql);
			if ($RS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			while($RS && !$RS->EOF)
			{
				//if(!isset($anInstance->GroupsAssct[$RS->fields["cla_rol"]])){
					$anInstance->GroupsAssct[] =  $RS->fields["cla_rol"];
					//die("Rol: " . $RS->fields["cla_rol"] . "  claUsuario: " . $anInstance->CLA_USUARIO);
				//}
				$RS->MoveNext();
			}

			// sbrande_aalvarez_16jul2015: obtener FirstName y LastName de la saas_users
		    include("../../../fbm/conns.inc.php");
		    ////global $server, $server_user, $server_pwd, $masterdbname;
		    $mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
		    if ($mysql_Connection) 
		    {
		      $db_selected = mysql_select_db($masterdbname, $mysql_Connection);
		      if ($db_selected) 
		      {
		        require_once("../../../fbm/ESurvey.class.php");

		        // instantiate the host user
		        $aUser = SAASUser::getSAASUserWithEmail($anInstance->Email, $mysql_Connection);
		        if (!is_null($aUser))
		        {
		        	// se añadio la funcion utf8_decode para leer caracteres especiales shernandez 03/04/2018 #97QT1X
		          //@JAPR 2019-01-22: Corregido un bug, se detectó que algunos valores de este campo ya estaban codificados en Latin, por lo que decodificar forzado de utf8 generaba caracteres inválidos que provocaban
		          //errores al mostrar la página, por tanto se utilizará la función que analiza si es realmente necesario aplicar la decodificación (#A8I45F)
		          $anInstance->FirstName = decodeUTF8($aUser->FirstName);
		          $anInstance->LastName = decodeUTF8($aUser->LastName);
		          //@JAPR
		        }
		      }
		    }
			
			//@JAPR 2016-11-29: Corregido un bug, este dato sólo se usa cuando se carga la instancia individual de usuario y no cuando se carga como colección, y al ejecutar este
			//código cuando se trataba de una colección grande de usuarios provocaba que la ventana de diseño tardara mas de un minuto, así que se validará para no ejecutarse en la colección
			//@ears 2016-08-02 última fecha de sincronización
			require_once('eSurveyServiceMod.inc.php');					
			$aBITAMConnection = connectToKPIRepository();
			$sql = "SELECT  MAX(A.SurveyDate) AS LastsyncDate FROM SI_SV_SurveyTasksLog A 
				WHERE A.SessionID IS NOT NULL AND A.SessionID <> '' 
				AND A.Repository = " . $aBITAMConnection->ADOConnection->Quote((string) @$_SESSION["PABITAM_RepositoryName"])." AND A.UserID = '".$anInstance->Email     
				."' AND (A.TaskStatus=2 OR A.TaskStatus=3) AND (A.Reprocess IS NULL OR A.Reprocess=0)";
			$RSS = $aBITAMConnection->ADOConnection->Execute($sql);
			
			if ($RSS === false) 
			{
				die( translate("Error accessing")." SI_SV_SurveyTasksLog ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			if (!$RSS->EOF) {		
				$anInstance->LastsyncDate = $RSS->fields["LastsyncDate"];
			}
			//@JAPR
		}
		
		return $anInstance;
	}
	
	//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	//@OMMC 2016-10-14
	//Revisa que exista la tabla de relaciones entre repositorios y PEMs
	//$aTask 	(bool) Si desea retornar un arreglo con un registro de repositorio y PEM
	//$aData 	(object) Campos por los que se desea filtrar el registro
	/* Esta función recibirá una conexión a KPIOnline->FBM000 ya que no se ejecuta dentro del repositorio BITAM, adicionalmente recibe un nombre de Aplicación y su versión (basados
	en el dispositivo del usuario específico para el cual se consulta) con los cuales se determinará cuál es el archivo .pem específico buscado utilizando el repositorio al que apunta
	dicha aplicación, en caso de no recibir esa información entonces se usará automáticamente el primer repositorio al que tenga acceso el usuario, pero en ambos casos se usará el número
	de versión para condicionar que el archivo .pem sea >= que dicha versión, si no existieran ocurrencias en la tabla, entonces se usará un archivo .pem genérico basado exclusivamente
	en el número de versión recibido, o en su defecto en la versión actual del servicio si no se hubiera recibido dicho dato
	*/
	static function checkRepPemTable($aBITAMConnection, $aTask = false, $aData = null) {
		//$aBITAMConnection = connectToKPIRepository();
		$arrResponse = array();
		if (!is_array($aData)) {
			$aData = array();
		}
		
		//@JAPRWarning: En esta implementación se va a obviar el repositorio, ya que como esta función correrá ya sea dentro de un Administrador de eForms ligado
		//en automático a un repositorio, o bien dentro de un Agente de Push notifications que procesa las notificaciones repositorio a repositorio, en ambos casos ya se asignó la variable
		//de sesión con el nombre de repositorio así que tenemos exactamente 
		$strRepositoryName = (string) @$aData['repository'];
		if (!$strRepositoryName) {
			$strRepositoryName = (string) @$_SESSION["PABITAM_RepositoryName"];
		}
		
		$sql = "SELECT A.Repository, A.FormsVersion, A.AppName, A.PemFile 
			FROM SI_SV_FormsPems A 
			WHERE 1 = 2";
		if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
			$sql = "CREATE TABLE SI_SV_FormsPems (
				Repository VARCHAR(50) NOT NULL,
				AppName VARCHAR(50) NOT NULL,
				FormsVersion VARCHAR(50) NOT NULL,
				PemFile VARCHAR(100) NULL,
				PRIMARY KEY(Repository, AppName, FormsVersion)
			)";
			@$aBITAMConnection->ADOConnection->Execute($sql);
		}
		
		if ($aTask && $aData) {
			//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
			$intStoreApp = (int) @$aData['storeApp'];
			$strPEMFilePath = '';
			switch ($intStoreApp) {
				case apptTest:
					$strPEMFilePath = "PEMFiles/test/";
					break;
				//@JAPR 2016-10-25: Agregado el tipo de App de depuración para conectarla con el XCode
				case apptDev:
					$strPEMFilePath = "PEMFiles/dev/";
					break;
				//@JAPR
				default:	//apptStore
					$strPEMFilePath = "PEMFiles/prod/";
					break;
			}
			//@JAPR
			
			//Permite agregar un filtro para el query
			$strAnd = " AND ";
			$strFilter = (string) @$aData['filter'];
			if (!$strFilter) {
				$strAppName = (string) @$aData['appName'];
				$strFormsVersion = (string) @$aData['formsVersion'];
				if ($strAppName) {
					$strFilter .= $strAnd."A.AppName = ".$aBITAMConnection->ADOConnection->Quote($strAppName);
					$strAnd = " AND ";
				}
				
				if ($strFormsVersion) {
					//@JAPR 2016-11-01: Corregido un bug, estaba invertida la condición, la versión del App al que se enviará debe ser >= que la versión del PEM configurado, porque el
					//mismo PEM se puede reutilizar para varias versiones y cuando llegue a cambiar se deberá usar el mas cercano a la versión del App a la que se envíe, por eso se
					//ordena precisamente de manera ascendente la versión (#L9T2IU)
					$strFilter .= $strAnd.$aBITAMConnection->ADOConnection->Quote($strFormsVersion)." >= A.FormsVersion";
					$strAnd = " AND ";
				}
			}
			
			$intLimit = (int) @$aData['limit'];
			$strLimit = '';
			if ($intLimit > 0) {
				$strLimit = " LIMIT ".$intLimit;
			}
			
			//@JAPR 2016-10-21: Corregido un bug, hacía falta incluir al repositorio, porque si no en caso contrario cualquier repositorio con pem personalizado aunque fuera diferente
			//al de la cuenta solicitada se utilizaba, y eso causaba errores de archivos de certificados inválidos
			$sql = "SELECT A.Repository, A.AppName, A.FormsVersion, A.PemFile 
				FROM SI_SV_FormsPems A 
				WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepositoryName)." 
				{$strFilter} 
				ORDER BY A.Repository, A.AppName, A.FormsVersion DESC 
				{$strLimit}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n sql: {$sql}");
			}
			$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
			if ($aRS) {
				while (!$aRS->EOF) {
					$arrData = array();
					$arrData['appName'] = (string) @$aRS->fields['appname'];
					$arrData['repository'] = (string) @$aRS->fields['repository'];
					$arrData['formsVersion'] = (string) @$aRS->fields['formsversion'];
					//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
					$arrData['pemFile'] = $strPEMFilePath.((string) @$aRS->fields['pemfile']);
					//@JAPR
					$arrResponse[] = $arrData;
					$aRS->MoveNext();
				}
			} 
			
			//Si no se hubieran encontrado archivos .pem configurados, intenta obtener los archivos .pem configurados para este repositorio, en el entendido que si existen múltiples
			//Apps que apunten a él se estaría enviando el mensaje a todas ellas
			if (count($arrResponse) == 0) {
				$strAnd = " AND ";
				$strFilter = (string) @$aData['filter'];
				if ($strFormsVersion) {
					//@JAPR 2016-11-01: Corregido un bug, estaba invertida la condición, la versión del App al que se enviará debe ser >= que la versión del PEM configurado, porque el
					//mismo PEM se puede reutilizar para varias versiones y cuando llegue a cambiar se deberá usar el mas cercano a la versión del App a la que se envíe, por eso se
					//ordena precisamente de manera ascendente la versión (#L9T2IU)
					$strFilter .= $strAnd.$aBITAMConnection->ADOConnection->Quote($strFormsVersion)." >= A.FormsVersion";
					$strAnd = " AND ";
				}
				
				$sql = "SELECT A.Repository, A.AppName, A.FormsVersion, A.PemFile 
					FROM SI_SV_FormsPems A 
					WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepositoryName)." 
					{$strFilter} 
					ORDER BY A.Repository, A.AppName, A.FormsVersion DESC 
					{$strLimit}";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n sql: {$sql}");
				}
				$aRS = $aBITAMConnection->ADOConnection->Execute($sql);
				if ($aRS) {
					while (!$aRS->EOF) {
						$arrData = array();
						$arrData['appName'] = (string) @$aRS->fields['appname'];
						$arrData['repository'] = (string) @$aRS->fields['repository'];
						$arrData['formsVersion'] = (string) @$aRS->fields['formsversion'];
						//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
						$arrData['pemFile'] = $strPEMFilePath.((string) @$aRS->fields['pemfile']);
						//@JAPR
						$arrResponse[] = $arrData;
						$aRS->MoveNext();
					}
				} 
			}
			
			//Si aún no existiera un archivo .pem configurado ni a nivel repositorio, se tiene que utilizar un archivo fijo definido por la versión de eForms, ya que con el paso
			//del tiempo los certificados Apple tienden a caducar y es necesario regenerarlo, además que cada versión (mayor o algunas veces menor) de eForms tendría por fuerza que utilizar 
			//un .pem diferente
			if (count($arrResponse) == 0) {
				$strAppName = (string) @$aData['appName'];
				$strFormsVersion = (string) @$aData['formsVersion'];
				$strPEMFileName = '';
				if ($strFormsVersion >= esvRealPushMsg) {
					$strPEMFileName = "formsV602Push.pem";
				}
				else {
					//Versión ya no soportada para recibir notificaciones Push
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Using default PEM file: {$strPEMFileName}, repository: {$strRepositoryName}, appName: {$strAppName}, formsVersion: {$strFormsVersion}");
				}
				
				if ($strPEMFileName) {
					$arrData = array();
					$arrData['appName'] = $strAppName;
					$arrData['repository'] = $strRepositoryName;
					$arrData['formsVersion'] = $strFormsVersion;
					//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
					$arrData['pemFile'] = $strPEMFilePath.$strPEMFileName;
					//@JAPR
					$arrResponse[] = $arrData;
				}
			}
		}
		
		return $arrResponse;
	}
	
	function getRole()
	{
		$roleAdmin = (int)$this->SV_Admin;
		$roleUser = (int)$this->SV_User;
		
		if($roleAdmin >= 1)
		{
			$this->Role = 1;
		}
		else
		{
			$this->Role = 0;
		}
	}

	function getArtusUserInfo()
	{
		$sql = "SELECT PASSWORD, PUESTO, CONFIG_SECURITY, CLA_OWNER, LDAP_PATH FROM SI_USUARIO WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if($this->CLA_USUARIO==0)
		{
			$this->intConfigSecurity = 0;
		}
		else
		{
			$this->intConfigSecurity = $aRS->fields["config_security"];	
		}
		
		if($this->intConfigSecurity == 1)
		{
			if(isset($_SESSION["PAtheUserID"]) && $this->CLA_USUARIO==$_SESSION["PAtheUserID"])
			{
				$this->Password = (isset($_SESSION["PAthePassword"])?$_SESSION["PAthePassword"]:"");
			}
			else 
			{
				$this->Password = GetADPwd($this->ArtusUser);
			}
		}
		else 
		{
			$this->Password = rtrim($aRS->fields["password"]);
		}
		
		$this->Position = $aRS->fields["puesto"];
		$this->OwnerID = (int)$aRS->fields["cla_owner"];
		$this->LdapPath = rtrim($aRS->fields["ldap_path"]);
	}
	
	function updateProfile()
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if($this->CLA_USUARIO==0)
		{
			$strCalledClass::addUserFromClaUsuario($this->Repository, $this->CLA_USUARIO, 1);
			$this->SV_Admin = 1;
			$this->SV_User = 0;
		}
	}

	static function getUserPassword($aRepository, $aUserID)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$userPassword = "";
		$theAppUser = $strCalledClass::NewInstanceWithClaUsuario($aRepository, $aUserID);
		
		if(!is_null($theAppUser))
		{
			$userPassword = $theAppUser->Password;
			
		    if($userPassword!="")
		    {
		    	$userPassword = BITAMDecryptPassword($userPassword);
		    }
		}

		return $userPassword;
	}

	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		global $blnWebMode;
		if (!isset($blnWebMode)) {
			$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
		}

		$arrDef = array();
		$arrDef['UserID'] = $this->UserID;
		$arrDef['UserName'] = $this->UserName;
		$arrDef['Email'] = $this->Email;
		$arrDef['AltEmail'] = $this->AltEmail;
		$arrDef['FullName'] = $this->FullName;
		$arrDef['FirstName'] = $this->FirstName;
		$arrDef['LastName'] = $this->LastName;
		$arrDef['Password'] = ($this->Password != "") ? BITAMDecryptPassword($this->Password) : $this->Password;
		$arrDef['SendPDFByEmail'] = $this->SendPDFByEmail;
		$arrDef['DefaultSurvey'] = $this->DefaultSurvey;
		$arrDef['TemplateID'] = $this->TemplateID;
		$arrDef['GroupsAssct'] = $this->GroupsAssct;
		//$arrDef['role'] = [$this->Role];
		$arrDef['Image'] = $this->Image;
		//GCRUZ 2016-05-05. Agregar idioma
		if (getMDVersion() >= esvUserLanguage) {
			$arrDef['UsrLanguage'] = $this->Language;
		}
		//GCRUZ 2016-06-09. Datos de la GeoCerca
		if (getMDVersion() >= esvGeoFence) {
			$arrDef['GeoFenceRadius'] = $this->GeoFenceRadius;
			$arrDef['GeoFenceLat'] = $this->GeoFenceLat;
			$arrDef['GeoFenceLng'] = $this->GeoFenceLng;
		}
		//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
		if (getMDVersion() >= esvUserParams) {
			$arrDef['UserParam1'] = $this->UserParam1;
			$arrDef['UserParam2'] = $this->UserParam2;
			$arrDef['UserParam3'] = $this->UserParam3;
		}

		//@ears 2016-08-02 última fecha de sincronización		
		$arrDef['LastsyncDate'] = $this->LastsyncDate;
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		$arrDef['DefaultKPIProduct'] = $this->DefaultKPIProduct;
		
		return $arrDef;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);

		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);');
		}

		if (array_key_exists("UserID", $aHTTPRequest->POST))
		{
			$anUserID = $aHTTPRequest->POST["UserID"];
			if (is_array($anUserID))
			{
				$aCollection = $strCalledClass::NewInstanceByClaUsuario($aRepository, $anUserID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, $anUserID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}

				//$anInstance->generateDesignWindow();
				//die();
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}

				$aHTTPRequest->RedirectTo = $anInstance;
			}

			return null;
		}

		$anInstance = null;
		if (array_key_exists("UserID", $aHTTPRequest->GET))
		{
			$anUserID = $aHTTPRequest->GET["UserID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, $anUserID);

			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}

		$anInstance->generateDesignWindow();
		die();

		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("CLA_USUARIO", $anArray))
		{
			$this->CLA_USUARIO = (int)$anArray["CLA_USUARIO"];
		}

	 	if (array_key_exists("UserName", $anArray))
		{
			$this->UserName = stripslashes($anArray["UserName"]);
		}

		if (array_key_exists("Email", $anArray))
		{
			$this->Email = stripslashes($anArray["Email"]);
		}

		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		if (array_key_exists("AltEmail", $anArray))
		{
			$this->AltEmail = stripslashes($anArray["AltEmail"]);
		}
		//@JAPR
		
 		if (array_key_exists("ArtusUser", $anArray))
		{
			$this->ArtusUser = strtoupper(stripslashes($anArray["ArtusUser"]));
		}

 		if (array_key_exists("Password", $anArray))
		{
			$this->Password = stripslashes($anArray["Password"]);
		}

		if (array_key_exists("Position", $anArray))
		{
			$this->Position = stripslashes($anArray["Position"]);
		}
		
		if (array_key_exists("Role", $anArray))
		{
			$this->Role = (int)$anArray["Role"];
			
			if($this->Role==1)
			{
				$this->SV_Admin = 1;
				$this->SV_User = 0;
			}
			else 
			{
				$this->SV_Admin = 0;
				$this->SV_User = 1;
			}
		}
		
	 	if (array_key_exists("ChangePassword", $anArray))
		{
			$this->ChangePassword = (int) $anArray["ChangePassword"];
		}
		
	 	if (array_key_exists("SendPDFByEmail", $anArray))
		{
			$this->SendPDFByEmail = (int) $anArray["SendPDFByEmail"];
		}

		
	    if (array_key_exists("Supervisor", $anArray)) //20-sep-2011 Conchita, agregado supervisor
		{
			$this->Supervisor = (int)$anArray["Supervisor"];
		}
		
		if (array_key_exists("DefaultSurvey", $anArray)) //20-sep-2011 Conchita, agregado supervisor
		{
			$this->DefaultSurvey = (int)@$anArray["DefaultSurvey"];
		}
		
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
		if (array_key_exists("TemplateID", $anArray))
		{
			$this->TemplateID = $anArray["TemplateID"];
		}
		
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		if (array_key_exists("ProfileID", $anArray))
		{
			$this->ProfileID = $anArray["ProfileID"];
		}
	    //@JAPR
		if (array_key_exists("GroupsAssct", $anArray))
		{
			//Pasamos los elementos separados por , al Array de Grupos.
			if($anArray["GroupsAssct"] != ""){
				$this->GroupsAssct = explode(",", $anArray["GroupsAssct"]);
			}
		}
		//@AAL 19/07/2015: Empleado para saber si se han desmarcado todos los grupos de este usuario durante la edición.
		if (array_key_exists("DissociateAll", $anArray))
		{
			$this->DissociateAll = $anArray["DissociateAll"];
		}
		//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
		if (array_key_exists("AgendasSurveysTemplate", $anArray) && getMDVersion() >= esvAgendaExportTemplate) {
			$this->AgendasSurveysTemplate = $anArray["AgendasSurveysTemplate"];
		}
		//GCRUZ 2016-05-05. Agregar idioma
		if (array_key_exists("UsrLanguage", $anArray) && getMDVersion() >= esvUserLanguage) {
			$this->Language = intval($anArray["UsrLanguage"]);
		}
		//GCRUZ 2016-06-09. Datos de la GeoCerca
		if (array_key_exists("GeoFenceRadius", $anArray) && getMDVersion() >= esvGeoFence) {
			$this->GeoFenceRadius = intval($anArray["GeoFenceRadius"]);
		}
		if (array_key_exists("GeoFenceLat", $anArray) && getMDVersion() >= esvGeoFence) {
			$this->GeoFenceLat = floatval($anArray["GeoFenceLat"]);
		}
		if (array_key_exists("GeoFenceLng", $anArray) && getMDVersion() >= esvGeoFence) {
			$this->GeoFenceLng = floatval($anArray["GeoFenceLng"]);
		}
		//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
		if (array_key_exists("UserParam1", $anArray) && getMDVersion() >= esvUserParams) {
			$this->UserParam1 = $anArray["UserParam1"];
		}
		if (array_key_exists("UserParam2", $anArray) && getMDVersion() >= esvUserParams) {
			$this->UserParam2 = $anArray["UserParam2"];
		}
		if (array_key_exists("UserParam3", $anArray) && getMDVersion() >= esvUserParams) {
			$this->UserParam3 = $anArray["UserParam3"];
		}
		
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		if (array_key_exists("DefaultKPIProduct", $anArray)) {
			$this->DefaultKPIProduct = (int) @$anArray["DefaultKPIProduct"];
		}
		//@JAPR
		
		return $this;
	}

	function save()
	{
	 	if ($this->isNewObject())
		{	
			//@AAL 15/07/2015: Por alguna extraña razon SESSION["PAFBM_Mode"] no es 1 y no me dejaba guardar. se comenta para avanzr
			//hay que analizar por que pasa esto.
			//if($_SESSION["PAFBM_Mode"]!=1)
			//{
				$sql =  "SELECT ".
							$this->Repository->ADOConnection->IfNull("MAX(UserID)", "0")." + 1 AS UserID".
							" FROM SI_SV_Users";
	
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$this->UserID = (int) $aRS->fields["userid"];

				//Si nos encontramos en modo FBM en teoría no se debería permitir agregar usuarios 
				$initialKey = GetBitamInitialKey($this->Repository);
				
				$sql =  "SELECT ".$this->Repository->ADOConnection->IfNull("MAX(CLA_USUARIO)", "".$initialKey."")." + 1 AS UserID".
							" FROM SI_USUARIO";
	
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
	
				$temporalID = (int)$aRS->fields["userid"];
	
				if($temporalID<($initialKey+1))
				{
					$temporalID = $initialKey+1;
				}
				//GCRUZ 2016-05-05. Agregar idioma
				if (getMDVersion() >= esvUserLanguage) {
					$intLanguage = $this->Language;
				}
				else
					$intLanguage = $_SESSION["PAuserLanguageID"];
	
				$this->CLA_USUARIO = $temporalID;
				// Obtenemos el id del usuario logeado, primero lo tomamos de la sesion
				$this->OwnerID = $_SESSION["PABITAM_UserID"];
				
				//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
				$strAdditionalFields = ", KPI_DEFAULT";
				$strAdditionalValues = ", {$this->DefaultKPIProduct}";
				//@JAPR
				$sql = "INSERT INTO SI_USUARIO (".
				            "CLA_USUARIO".
				            ",NOM_CORTO".
				            ",PASSWORD".
				            ",NOM_LARGO".
				            ",PUESTO".
				            ",CUENTA_CORREO".
				            ",WHTMLANG".
				            ",EKTOS_ADMIN".
				            ",EKTOS_USER".
				            ",TIPO_USUARIO".
				            ",ARTUS_DESKTOP".
				            ",ARTUS_ADMIN".
				            ",ARTUS_DESIGNER".
				            ",PAPIRO_ADMIN".
							",PAPIRO_DESIGNER".
							",PAPIRO_DESKTOP".
							",ADVISOR_ADMIN".
							",ADVISOR_DESKTOP".
							",STRATEGO_ADMIN".
							",STRATEGO_DESKTOP".
				            ",CLA_OWNER".
							$strAdditionalFields.
							") VALUES (".
							$this->CLA_USUARIO.
							",".$this->Repository->ADOConnection->Quote(strtoupper($this->ArtusUser)).
							",".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password)).
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Position).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$intLanguage.
							","." 0 ".
							","." 0 ".
							","." 2 ".
							","." 1 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							","." 0 ".
							",".$this->OwnerID.
							$strAdditionalValues.
							")";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				//GCRUZ 2016-05-05. Agregar idioma
				if ($this->CLA_USUARIO == $_SESSION['PABITAM_UserID'])
				{
					$_SESSION["PAuserLanguageID"] = $intLanguage;
					switch($intLanguage)
					{
						case 1:
							$_SESSION["PAuserLanguage"] = 'SP';
							break;
						case 2:
							$_SESSION["PAuserLanguage"] = 'EN';
							break;
						case 4:
							$_SESSION["PAuserLanguage"] = 'PT';
							break;
						case 6:
							$_SESSION["PAuserLanguage"] = 'BZ';
							break;
					}
				}
				
				//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
				$strAdditionalFields = '';
				$strAdditionalValues = '';
				if (getMDVersion() >= esvAltEMailForNotif) {
					$strAdditionalFields .= ',AltEmail';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->AltEmail);
				}
				if (getMDVersion() >= esvDefaultSurvey) {
					$strAdditionalFields .= ',DefaultSurvey';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->DefaultSurvey);
				}
			    //@JAPR 2014-06-20: Agregados los templates para personalización del App
				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
				if (getMDVersion() >= esvTemplateStyles) {
					$strAdditionalFields .= ',TemplateID';
					$strAdditionalValues .= ','.$this->TemplateID;
				}
				//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
				if (getMDVersion() >= esvSurveyProfiles) {
					$strAdditionalFields .= ',ProfileID';
					$strAdditionalValues .= ','.$this->ProfileID;
				}
				//@AAL 2015-07-21: Agregado Para definira al usuario una foto de perfil
				if (getMDVersion() >= esveFormsv6) {
					$strAdditionalFields .= ',Image';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->Image);
				}
				//@JAPR
				//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
				if (getMDVersion() >= esvAgendaExportTemplate) {
					$strAdditionalFields .= ',AgendasSurveysTemplate';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->AgendasSurveysTemplate);
				}
				//GCRUZ 2016-06-09. Datos de la GeoCerca
				if (getMDVersion() >= esvGeoFence) {
					$strAdditionalFields .= ',GeoFenceRadius, GeoFenceLat, GeoFenceLng';
					$strAdditionalValues .= ','.$this->GeoFenceRadius.','.$this->GeoFenceLat.','.$this->GeoFenceLng;
				}
				//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
				if (getMDVersion() >= esvUserParams) {
					$strAdditionalFields .= ',UserParam1, UserParam2, UserParam3';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->UserParam1).','.$this->Repository->ADOConnection->Quote($this->UserParam2).','.$this->Repository->ADOConnection->Quote($this->UserParam3);
				}
				$sql = "INSERT INTO SI_SV_Users (".
							"UserID".
							", UserName".
				            ", Email".
				            ", SV_Admin".
				            ", SV_User".
				            ", ArtusUser".
				            ", CLA_USUARIO".
				            ", SendPDFByEmail".
				            ", Supervisor".
				            $strAdditionalFields.
				            ") VALUES (".
							$this->UserID.
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$this->SV_Admin.
							",".$this->SV_User.
							",".$this->Repository->ADOConnection->Quote($this->ArtusUser).
							",".$this->CLA_USUARIO.
							",".$this->SendPDFByEmail.
							",".$this->Supervisor.
							$strAdditionalValues.
							")";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			//}
		}
		else
		{
			//GCRUZ 2016-05-05. Agregar idioma
			if (getMDVersion() >= esvUserLanguage) {
				//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
				$strAdditionalValues = ", KPI_DEFAULT = {$this->DefaultKPIProduct}";
				$aSQL = "UPDATE SI_USUARIO 
							SET WHTMLANG = ".$this->Language." 
						{$strAdditionalValues} 
					WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
				//@JAPR
				if ($this->Repository->ADOConnection->Execute($aSQL) === false)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
				}
				if ($this->CLA_USUARIO == $_SESSION['PABITAM_UserID'])
				{
					$_SESSION["PAuserLanguageID"] = $this->Language;
					switch($this->Language)
					{
						case 1:
							$_SESSION["PAuserLanguage"] = 'SP';
							break;
						case 2:
							$_SESSION["PAuserLanguage"] = 'EN';
							break;
						case 4:
							$_SESSION["PAuserLanguage"] = 'PT';
							break;
						case 6:
							$_SESSION["PAuserLanguage"] = 'BZ';
							break;
					}
				}
			}
			
			$sqlExist = "SELECT CLA_USUARIO FROM SI_SV_Users WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
			$aRSExist = $this->Repository->ADOConnection->Execute($sqlExist);
			if ($aRSExist === false)
			{
				die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlExist);
			}
			
			if ($aRSExist->EOF)
			{
				
				$sql =  "SELECT ".
							$this->Repository->ADOConnection->IfNull("MAX(UserID)", "0")." + 1 AS UserID".
							" FROM SI_SV_Users";
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				$this->UserID = (int) $aRS->fields["userid"];

				//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
				$strAdditionalFields = '';
				$strAdditionalValues = '';
				if (getMDVersion() >= esvAltEMailForNotif) {
					$strAdditionalFields .= ',AltEmail';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->AltEmail);
				}
				if (getMDVersion() >= esvDefaultSurvey) {
					$strAdditionalFields .= ',DefaultSurvey';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->DefaultSurvey);
				}
			    //@JAPR 2014-06-20: Agregados los templates para personalización del App
				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
				if (getMDVersion() >= esvTemplateStyles) {
					$strAdditionalFields .= ',TemplateID';
					$strAdditionalValues .= ','.$this->TemplateID;
				}
				//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
				if (getMDVersion() >= esvSurveyProfiles) {
					$strAdditionalFields .= ',ProfileID';
					$strAdditionalValues .= ','.$this->ProfileID;
				}
				//@JAPR

				//@AAL 2015-07-21: Agregado Para definira al usuario una foto de perfil
				if (getMDVersion() >= esveFormsv6) {
					$strAdditionalFields .= ',Image';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->Image);
				}
				//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
				if (getMDVersion() >= esvAgendaExportTemplate) {
					$strAdditionalFields .= ',AgendasSurveysTemplate';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->AgendasSurveysTemplate);
				}
				//GCRUZ 2016-06-09. Datos de la GeoCerca
				if (getMDVersion() >= esvGeoFence) {
					$strAdditionalFields .= ',GeoFenceRadius, GeoFenceLat, GeoFenceLng';
					$strAdditionalValues .= ','.$this->GeoFenceRadius.','.$this->GeoFenceLat.','.$this->GeoFenceLng;
				}
				//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
				if (getMDVersion() >= esvUserParams) {
					$strAdditionalFields .= ',UserParam1, UserParam2, UserParam3';
					$strAdditionalValues .= ','.$this->Repository->ADOConnection->Quote($this->UserParam1).','.$this->Repository->ADOConnection->Quote($this->UserParam2).','.$this->Repository->ADOConnection->Quote($this->UserParam3);
				}
				$sql = "INSERT INTO SI_SV_Users (".    //revisar si aqui tambien va supervisor (conchita)
							"UserID".
							", UserName".
				            ", Email".
				            ", SV_Admin".
				            ", SV_User".
				            ", ArtusUser".
				            ", CLA_USUARIO".
				            ", Supervisor".
				            $strAdditionalFields.
				            ") VALUES (".
							$this->UserID.
							",".$this->Repository->ADOConnection->Quote($this->UserName).
							",".$this->Repository->ADOConnection->Quote($this->Email).
							",".$this->SV_Admin.
							",".$this->SV_User.
							",".$this->Repository->ADOConnection->Quote($this->ArtusUser).
							",".$this->CLA_USUARIO.
							",".$this->Supervisor.
							$strAdditionalValues.
							")";
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			else 
			{
				//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
				$strAdditionalFields = '';
				if (getMDVersion() >= esvAltEMailForNotif) {
					$strAdditionalFields .= ', AltEmail = '.$this->Repository->ADOConnection->Quote($this->AltEmail);
				}
				if (getMDVersion() >= esvDefaultSurvey) {
					$strAdditionalFields .= ', DefaultSurvey = '.$this->Repository->ADOConnection->Quote($this->DefaultSurvey);
				}
			    //@JAPR 2014-06-20: Agregados los templates para personalización del App
				//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
				if (getMDVersion() >= esvTemplateStyles) {
					$strAdditionalFields .= ', TemplateID = '.$this->TemplateID;
				}
				//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
				if (getMDVersion() >= esvSurveyProfiles) {
					$strAdditionalFields .= ', ProfileID = '.$this->ProfileID;
				}
				//@JAPR
				//@AAL 2015-07-21: En eFormsV6 y siguientes se agrega Image para definir al usuario una foto de perfil
				if (getMDVersion() >= esveFormsv6) {
					$strAdditionalFields .= ',Image = '.$this->Repository->ADOConnection->Quote($this->Image);
				}
				//GCRUZ 2016-02-25.  Agregada configuración para seleccionar formas al exportar el template para agendas
				if (getMDVersion() >= esvAgendaExportTemplate) {
					$strAdditionalFields .= ',AgendasSurveysTemplate = '.$this->Repository->ADOConnection->Quote($this->AgendasSurveysTemplate);
				}
				//GCRUZ 2016-06-09. Datos de la GeoCerca
				if (getMDVersion() >= esvGeoFence) {
					$strAdditionalFields .= ',GeoFenceRadius = '.$this->GeoFenceRadius.', GeoFenceLat = '.$this->GeoFenceLat.', GeoFenceLng = '.$this->GeoFenceLng;
				}
				//GCRUZ 2016-07-19. Parámetros extra de usuario para fórmulas
				if (getMDVersion() >= esvUserParams) {
					$strAdditionalFields .= ',UserParam1 = '.$this->Repository->ADOConnection->Quote($this->UserParam1).', UserParam2 = '.$this->Repository->ADOConnection->Quote($this->UserParam2).', UserParam3 = '.$this->Repository->ADOConnection->Quote($this->UserParam3);
				}
				$sql = 	"UPDATE SI_SV_Users SET ".
						" UserName = ".$this->Repository->ADOConnection->Quote($this->UserName).
						", Email = ".$this->Repository->ADOConnection->Quote($this->Email).
						", SV_Admin = ".$this->SV_Admin.
						", SV_User = ".$this->SV_User.
						", SendPDFByEmail = ".$this->SendPDFByEmail.
						", Supervisor = ".$this->Supervisor.
						$strAdditionalFields;
				if ($this->intConfigSecurity == 0)
				{
					$sql .= ", ArtusUser = ".$this->Repository->ADOConnection->Quote($this->ArtusUser);
				}
				$sql .= " WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}

			$sql = "";
			//@JAPR 2008-07-17: Con usuarios de LDAP no se debe actualizar el password ni el nombre corto
			//ConfigSecurity = 0 (Usuario BITAM), ConfigSecurity = 1 (Usuario LDAP)
			if ($this->intConfigSecurity == 0 && $_SESSION["PAFBM_Mode"]!=1)
			{
				$sql .= " NOM_CORTO = ".$this->Repository->ADOConnection->Quote($this->ArtusUser);
				
				if($this->ChangePassword)
				{	
					$sql .= ", PASSWORD = ".$this->Repository->ADOConnection->Quote(BITAMEncryptPassword($this->Password));
				}
				
				$sql .= ", ";
			}
			
			if($_SESSION["PAFBM_Mode"]!=1)
			{
				$sql .= " NOM_LARGO = ".$this->Repository->ADOConnection->Quote($this->UserName);
				$sql .= ",CUENTA_CORREO = ".$this->Repository->ADOConnection->Quote($this->Email);
				$sql .= ", PUESTO = ".$this->Repository->ADOConnection->Quote($this->Position);
			}
			
			if(trim($sql)!="")
			{
				$sqlUpd = "UPDATE SI_USUARIO SET ".$sql." WHERE CLA_USUARIO = ".$this->CLA_USUARIO;

				if ($this->Repository->ADOConnection->Execute($sqlUpd) === false)
				{
					die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlUpd);
				}
			}
			
			//@AAL 18/07/2015 Eliminamos e insertamos nuevamente Grupos asociados a este usuario
			if (count($this->GroupsAssct) > 0) {
				$sql = "DELETE FROM SI_ROL_USUARIO WHERE CLA_USUARIO = {$this->CLA_USUARIO}";
				$this->Repository->ADOConnection->Execute($sql);
				//@AAL 19/07/2015 si DissociateAll es true quiere decir que ya no insertaremos nuevamente los usuarios al grupo, solo se borraron de la tabla SI_ROL_USUARIO
				if (!$this->DissociateAll) {
					$Values = "";
					//Preparamos los usuarios a insertar para asociarlos al nuevo grupo
					for ($i = 0; $i < count($this->GroupsAssct); $i++) {
						$Values .= "({$this->GroupsAssct[$i]}, {$this->CLA_USUARIO}),";
					}
					$Values = substr($Values, 0, strlen($Values)-1);
					$sql = "INSERT INTO SI_ROL_USUARIO (CLA_ROL, CLA_USUARIO) VALUES" . $Values;
					$this->Repository->ADOConnection->Execute($sql);
					/*if ($this->Repository->ADOConnection->Execute($sql) === false)
					{
						die("Error accessing SI_ROL_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
					}*/
				}
				else{
					$this->DissociateAll = false;
				}
			}
		}
		
		return $this;
	}

	function remove()
	{
		$sql = "DELETE FROM SI_SV_Users WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_USUARIO WHERE CLA_USUARIO = ".$this->CLA_USUARIO;
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	//@JAPR 2014-04-02: Agregada la ventana para permitir enviar mensajes vía Push
	//Regresa un array con la información de los dispositivos móviles configurados para el usuario (lo asigna a la instancia también)
	function getMobileData() {
		if ($this->MobileDataRead) {
			return $this->MobileData;
		}
		
		$sql = "SELECT DeviceID, RegID, PhoneNumber, DeviceName 
			FROM SI_SV_RegID 
			WHERE UserID = {$this->CLA_USUARIO}";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_RegID ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$intDeviceNum = 0;
		while (!$aRS->EOF) {
			$intDeviceID = (int) @$aRS->fields["deviceid"];
			$strRegID = (string) @$aRS->fields["regid"];
			$strPhoneNumber = (string) @$aRS->fields["phonenumber"];
			$strDeviceName = (string) @$aRS->fields["devicename"];
			if ($strDeviceName == '') {
				$strDeviceName = translate("Mobile device")." #".($intDeviceNum++);
			}
			
			$arrMobileData = array();
			$arrMobileData['deviceID'] = $intDeviceID;
			$arrMobileData['deviceName'] = $strDeviceName;
			$arrMobileData['regID'] = $strRegID;
			$arrMobileData['phoneNum'] = $strPhoneNumber;
			if (strlen($strRegID) == 64) {
				$arrMobileData['type'] = 'IOS';
			}
			else {
				$arrMobileData['type'] = 'Android';
			}
			$this->MobileData[$strRegID] = $arrMobileData;
			$aRS->MoveNext();
		}
		
		return $this->MobileData;
	}
	//@JAPR

	//MAPR 2018-02-15: Por petición de DAbdo los mensajes deben llegar a todos los usuarios que usen el mismo usuario
	function getAllMobileData($sAppName = ''){
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		
		$strWhere = "";
		if ($sAppName) {
			$strWhere .= " AND AppName = ".$aBITAMConnection->ADOConnection->Quote($sAppName);
		}
		
		//Obtener el registro más reciente de SI_SV_UserDevices con el email del usuario
		//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
		//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
		//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
		//MAPR 2018-02-15: Se remueven del Query el Limit de 1 y el IsActive porque deberían recibirlo incluso aquellos que no son activos.
		$aSQL = "SELECT RegID, AppName, FormsVersion, UUID, UserAgent, StoreApp 
			FROM SI_SV_UserDevices 
			WHERE UserID = ".$aBITAMConnection->ADOConnection->Quote($this->Email)." 
				AND DeviceID > 0 {$strWhere} 
			ORDER BY LastDateID DESC";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$aSQL}");
		}
		$aRS = $aBITAMConnection->ADOConnection->Execute($aSQL);
		$this->MobileData = array();
		
		$strRegID = '';
		$intStoreApp = null;
		$arrMobileData = null;
		$arrMobiles = null;
		// die($aRS);
		while(!$aRS->EOF){
			if ($aRS && $aRS->_numOfRows != 0)
			{
				$strRegID = (string) @$aRS->fields["regid"];
				$arrMobileData = array();
				$arrMobileData['regID'] = $strRegID;
				if (strlen($strRegID) == 64) {
					$arrMobileData['type'] = 'IOS';
				}
				else {
					$arrMobileData['type'] = 'Android';
				}
				
				//App name sólo es reelevante si se está forzando a enviar por dicha aplicación, en caso contrario el primer AppName identificado es válido
				if ($sAppName) {
					$arrMobileData['appName'] = (string) @$aRS->fields["appname"];
				}
				$arrMobileData['formsVersion'] = (string) @$aRS->fields["formsversion"];
				//Agregada la información del dispositivo para poder hace un mejor reporte de log
				$arrMobileData['uuid'] = (string) @$aRS->fields["uuid"];
				$arrMobileData['userAgent'] = (string) @$aRS->fields["useragent"];
				//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
				$intStoreApp = @$aRS->fields["storeapp"];
				if (is_null($intStoreApp)) {
					//El default en caso de no estar asignado es considerar al App como de tienda, por si por error no se envía este valor en un App personalizada generada mas adelante,
					//así por lo menos las Apps que se subieran mal no tendrían que ser actualizadas de inmediato, mientras que las Apps de liga que si podemos cambiar son las que deberían
					//enviar este valor explíciamente en 0 para que se considere de liga. El default en el archivo mobileDetection.js es 1 para considerarla App de tienda
					$intStoreApp = apptStore;
				}
				$arrMobileData['storeApp'] = $intStoreApp;
				//@JAPR
				
			}
			if ($arrMobileData) {
				//El pemFile está basado en el appName que se usará, si no existe un appName entonces se usará el pemFile default del repositorio, y en caso de que no exista alguno
				//entonces se usara el pemFile de la versión que está utilizando el usuario
				$arrPEMFiles = BITAMAppUser::checkRepPemTable($aBITAMConnection, true, array_merge($arrMobileData, array('limit' => 1)));
				
				//Agrega la información del dispositivo al array del usuario, aunque ya realmente sólo existirá un dispositivo disponible para cada usuario
				//@JAPRWarning: Por ahora se enviará a un único .pem cada mensaje de Push, así que el primero configuraro será el utilizado
				if (!is_null($arrPEMFiles) && is_array($arrPEMFiles) && count($arrPEMFiles)) {
					$arrMobileData['pemFile'] = (string) @$arrPEMFiles[0]['pemFile'];
				}
				$this->MobileData[$strRegID] = $arrMobileData;
			}
			$aRS->MoveNext();
		}
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n User mobile data: {$this->UserName}");
			PrintMultiArray($this->MobileData);
		}
	}


	//GCRUZ 2016-05-10. getMobileDataV6. Dispositivos para mensajes push en V6
	//@JAPR 2016-10-17: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
	/* Identifica el RegID a utilizar para enviar notificaciones Push a este usuario, así como el archivo .pem según el repositorio actualmente configurado y el App que realizó el
	envío de la notificación, la cual se especifica mediante el parámetro $sAppName, en caso de no haber indicado una App, se utilizará cualquier App disponible por lo que obtendrá
	el primer archivo .pem que exista
	*/
	function getMobileDataV6($sAppName = '')
	{
		require_once('eSurveyServiceMod.inc.php');
		$aBITAMConnection = connectToKPIRepository();
		
		$strWhere = "";
		if ($sAppName) {
			$strWhere .= " AND AppName = ".$aBITAMConnection->ADOConnection->Quote($sAppName);
		}
		
		//Obtener el registro más reciente de SI_SV_UserDevices con el email del usuario
		//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
		//@JAPR 2016-10-21: Agregado el campo IsActive para identificar el dispositivo que se encuentra activo en cada cuenta, ya que si dos o mas cuentas entran con el mismo dispositivo,
		//sólo la última debería quedar con dicho dispositivo asignado (IsActive = 1), por lo que se hará un Update a las demás cuentas de ese dispositivo para desactivarlas (IsActive = 0)
		$aSQL = "SELECT RegID, AppName, FormsVersion, UUID, UserAgent, StoreApp 
			FROM SI_SV_UserDevices 
			WHERE UserID = ".$aBITAMConnection->ADOConnection->Quote($this->Email)." 
				AND DeviceID > 0 AND (IsActive IS NULL OR IsActive = 1) 
				{$strWhere} 
			ORDER BY LastDateID DESC LIMIT 1";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n sql: {$aSQL}");
		}
		$rsDevice = $aBITAMConnection->ADOConnection->Execute($aSQL);
		$this->MobileData = array();
		
		$strRegID = '';
		$intStoreApp = null;
		$arrMobileData = null;
		if ($rsDevice && $rsDevice->_numOfRows != 0)
		{
			$strRegID = (string) @$rsDevice->fields["regid"];
			$arrMobileData = array();
			$arrMobileData['regID'] = $strRegID;
			if (strlen($strRegID) == 64) {
				$arrMobileData['type'] = 'IOS';
			}
			else {
				$arrMobileData['type'] = 'Android';
			}
			
			//App name sólo es reelevante si se está forzando a enviar por dicha aplicación, en caso contrario el primer AppName identificado es válido
			if ($sAppName) {
				$arrMobileData['appName'] = (string) @$rsDevice->fields["appname"];
			}
			$arrMobileData['formsVersion'] = (string) @$rsDevice->fields["formsversion"];
			//Agregada la información del dispositivo para poder hace un mejor reporte de log
			$arrMobileData['uuid'] = (string) @$rsDevice->fields["uuid"];
			$arrMobileData['userAgent'] = (string) @$rsDevice->fields["useragent"];
			//@JAPR 2016-10-20: Agregado el campo StoreApp para identificar si este dispositivo corresponde a un App de tienda o App de liga (#L9T2IU)
			$intStoreApp = @$rsDevice->fields["storeapp"];
			if (is_null($intStoreApp)) {
				//El default en caso de no estar asignado es considerar al App como de tienda, por si por error no se envía este valor en un App personalizada generada mas adelante,
				//así por lo menos las Apps que se subieran mal no tendrían que ser actualizadas de inmediato, mientras que las Apps de liga que si podemos cambiar son las que deberían
				//enviar este valor explíciamente en 0 para que se considere de liga. El default en el archivo mobileDetection.js es 1 para considerarla App de tienda
				$intStoreApp = apptStore;
			}
			$arrMobileData['storeApp'] = $intStoreApp;
			//@JAPR
		}
		
		if ($arrMobileData) {
			//El pemFile está basado en el appName que se usará, si no existe un appName entonces se usará el pemFile default del repositorio, y en caso de que no exista alguno
			//entonces se usara el pemFile de la versión que está utilizando el usuario
			$arrPEMFiles = BITAMAppUser::checkRepPemTable($aBITAMConnection, true, array_merge($arrMobileData, array('limit' => 1)));
			
			//Agrega la información del dispositivo al array del usuario, aunque ya realmente sólo existirá un dispositivo disponible para cada usuario
			//@JAPRWarning: Por ahora se enviará a un único .pem cada mensaje de Push, así que el primero configuraro será el utilizado
			if (!is_null($arrPEMFiles) && is_array($arrPEMFiles) && count($arrPEMFiles)) {
				$arrMobileData['pemFile'] = (string) @$arrPEMFiles[0]['pemFile'];
			}
			$this->MobileData[$strRegID] = $arrMobileData;
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n User mobile data: {$this->UserName}");
			PrintMultiArray($this->MobileData);
		}
	}
	
	function isNewObject()
	{
		return ($this->CLA_USUARIO < 0);
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("User");
		}
		else
		{
			return $this->UserName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=AppUser";
		}
		else
		{
			return "BITAM_PAGE=AppUser&UserID=".$this->CLA_USUARIO;
		}
	}

	function get_Parent()
	{
		return BITAMAppUserCollection::NewInstanceByClaUsuario($this->Repository, null);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'CLA_USUARIO';
	}

	function get_FormFieldName() {
		//JAPR 2015-11-18: Habilitado el nombre de usuario para edición en cuentas que no son GeoControl (#QQCMGH)
		//Modificado para regresar la propiedad exacta a usar dependiendo de cual es la que tiene un valor no vacío (no es el uso correcto, pero tampoco es una función estática,
		//así que por tanto es susceptible de ser invocada por instancia en lugar de en forma genérica)
		if (trim($this->UserName) != '') {
			return 'UserName';
		}
		else {
			return 'Email';
		}
		//return 'UserName';
	}

	function get_FormFields($aUser)
	{
		global $blnTestingServer;
		global $gbIsGeoControl;
		
		$isReadOnly = false;

		//Verificar si se está en modo FBM y con usuario logueado diferente a Supervisor
		if($_SESSION["PAFBM_Mode"]==1 && $_SESSION["PAtheUserID"]!=0)
		{
			$isReadOnly = true;
		}
		
		//Si es usuario nuevo, se le asigna como propietario el usuario logueado
		if($this->isNewObject())
		{
			$this->OwnerID = $_SESSION["PAtheUserID"];
		}

		require_once("formfield.inc.php");
		$myFields = array();
		
		if($_SESSION["PAFBM_Mode"]!=1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ArtusUser";
			$aField->Title = translate("User Name");
			$aField->Type = "String";
			$aField->Size = 15;
			$aField->FormatMask = "aW";
			
			//@JAPR 2008-07-17: Con usuarios de LDAP no se debe actualizar el password ni el nombre corto
			//No se debe permitir cambiar el nombre corto del usuario SUPERVISOR (userID=0)
			//Sólo el owner del usuario puede cambiar el nombre corto
			//Sólo puede cambiarse el nombre corto si el campo Ldap_Path es vacío
			if ($this->intConfigSecurity == 1 || $this->CLA_USUARIO == 0 || $this->OwnerID!=$_SESSION["PAtheUserID"] || rtrim($this->LdapPath)!="" || $isReadOnly==true)
			{
				$aField->IsDisplayOnly = true;
			}

			$myFields[$aField->Name] = $aField;

			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Password";
			$aField->Title = translate("Password");
			$aField->Type = "String";
			$aField->IsVisible = false;
			$aField->IsDisplayOnly = $isReadOnly;
			$aField->AfterMessage = "<span id=spanChangePassword unselectable=on style=\"padding-top:3px;text-align:center;width:97px;height:22px;cursor:pointer;background-image:url(btngenerar.gif);backgroun-position:center center;font-family:tahoma;font-size:12px;font-weight:bold;color:#d5d5bc\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" onclick=\"this.style.color='#d5d5bc';openChangePassword()\">"
			.translate("Change").
			"</span>";
			$myFields[$aField->Name] = $aField;

			//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
			if (!$blnTestingServer) {
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "UserName";
				$aField->Title = translate("Full Name");
				$aField->Type = "String";
				$aField->Size = 255;
				$myFields[$aField->Name] = $aField;
			}
			//@JAPR
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Position";
			$aField->Title = translate("Job Position");
			$aField->Type = "String";
			$aField->Size = 255;
			$aField->IsDisplayOnly = $isReadOnly;
			$myFields[$aField->Name] = $aField;
		}
		
		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		if ($blnTestingServer && !$this->isNewObject()) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserName";
			$aField->Title = translate("Full Name");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Email";
		$aField->Title = translate("E-Mail");
		$aField->Type = "String";
		$aField->Size = 255;

		if($this->isNewObject())
		{
			$aField->IsDisplayOnly = false;
		}
		else 
		{
			$aField->IsDisplayOnly = true;	
		}
		$myFields[$aField->Name] = $aField;

		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		if (getMDVersion() >= esvAltEMailForNotif) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "AltEmail";
			$aField->Title = translate("Alternative E-Mail");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}
		
		//Se proceden a desplegar los campos de First Name y Last Name cuando nos encontramos
		//en modo KPI para que den de alta usuarios invitados
		if($_SESSION["PAFBM_Mode"]==1 && $this->isNewObject())
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Password";
			$aField->Title = translate("Password");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;

			$aField = BITAMFormField::NewFormField();
			$aField->Name = "FirstName";
			$aField->Title = translate("First Name");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;

			$aField = BITAMFormField::NewFormField();
			$aField->Name = "LastName";
			$aField->Title = translate("Last Name");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}

		if (true) //if($_SESSION["PAFBM_Mode"]==0 || ($_SESSION["PAFBM_Mode"]==1 && !$this->isNewObject()))
		{
			$aTypes = array();
			$aTypes[0] = translate("User");
			$aTypes[1] = translate("Administrator");
				
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Role";
			$aField->Title = translate("Role");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			if($_SESSION["PABITAM_UserRole"]==1)
			{
				$aField->IsDisplayOnly = false;
			}
			else 
			{
				$aField->IsDisplayOnly = true;
			}
			//@JAPR 2015-04-24: Validado que en GeoControl no se puedan administrar usuarios
			if ($gbIsGeoControl) {
				$aField->Title = "";
				$aField->IsVisible = false;
			}
			//@JAPR
			$aField->Options = $aTypes;
			$myFields[$aField->Name] = $aField;
		}
		
		if($_SESSION["PAFBM_Mode"]!=1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ChangePassword";
			$aField->Title = "";
			$aField->Type = "Integer";
			$aField->IsVisible = false;
			$myFields[$aField->Name] = $aField;
		}
		
		$options = array();
		$options[0] = translate("No");
		$options[1] = translate("Yes");
			
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SendPDFByEmail";
		$aField->Title = translate("Send PDF By Email");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $options;

		if($_SESSION["PABITAM_UserRole"]==1)
		{
			$aField->IsDisplayOnly = false;
		}
		else 
		{
			$aField->IsDisplayOnly = true;
		}

		$myFields[$aField->Name] = $aField;
		
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			// 20-sep-2011 cambios conchita: agregar Supervisor combobox
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "Supervisor";
			$aField->Title = translate("Supervisor");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $this->getSupervisorValues();		
			$myFields[$aField->Name] = $aField;
			//fin cambios conchita
		}
		
		if (getMDVersion() >= esvDefaultSurvey) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "DefaultSurvey";
			$aField->Title = translate("DEFAULTSURVEY");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $this->getSurveys();		
			$myFields[$aField->Name] = $aField;
		}
		
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		//@JAPRDescontinuado
		/*if (getMDVersion() >= esvTemplateStyles) {
			require_once('eformsAppCustomizationTpl.inc.php');
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "TemplateID";
			$aField->Title = translate("DEFAULTCOLORTEMPLATE");
			
			$intTemplateID = $this->TemplateID;
			$arrColorTemplates = array();
			$arrColorTemplates[-1] = translate("None");
			//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
			$objColorTemplatesColl = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
			if (!is_null($objColorTemplatesColl)) {
				foreach($objColorTemplatesColl->Collection as $objColorTemplate) {
					$arrColorTemplates[$objColorTemplate->TemplateID] = $objColorTemplate->TemplateName;
				}
			}
			
			if ($intTemplateID > 0)
			{
				if (!isset($arrColorTemplates[$intTemplateID]))
				{
					$arrColorTemplates[$intTemplateID] = translate("Current template");
				}
			}
			
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrColorTemplates;
			$myFields[$aField->Name] = $aField;
		}*/
		
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		if (getMDVersion() >= esvSurveyProfiles) {
			require_once("surveyProfile.inc.php");
			$arrProfiles = array(-1 => "(".translate("None").")");
			if ($this->ProfileID > 0) {
				$objSurveyProfile = BITAMSurveyProfile::NewInstanceWithID($this->Repository, $this->ProfileID);
				if (!is_null($objSurveyProfile)) {
					$arrProfiles = array($this->ProfileID => $objSurveyProfile->ProfileName);
				}
				else {
					$arrProfiles = array($this->ProfileID => "(".translate("Profile").")");
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "ProfileID";
			$aField->Title = translate("Profile");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arrProfiles;
			$aField->IsDisplayOnly = true;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		return $myFields;
	}
	
	function getSurveys() {
		require_once('survey.inc.php');
		$arrSurveys = array();
		array_push($arrSurveys, "None");
		$loadedSurveys = BITAMSurvey::getSurveys($this->Repository, true);
		foreach($loadedSurveys as $skey => $asurvey) {
			$arrSurveys[$skey] = $asurvey;
		}
		return $arrSurveys;
	}
	
	function getSupervisorValues()
	{   
		//Conchita 20-septiembre-2011 Función para obtener los valores de cada supervisor
		//$arrayValues["NULL"] = translate("Sin Supervisor");
		//$fieldName = "";

		$sql="SELECT CLA_USUARIO as Clave, EMAIL as EMAIL FROM SI_SV_Users";
		$aRS=$this->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
			
		while(!$aRS->EOF)
		{
			$value = $aRS->fields["clave"];
			$fieldName = $aRS->fields["email"];
			$arrayValues[$value] = $fieldName;
			
			$aRS->MoveNext();
		}

    	return $arrayValues;
	}

	function insideEdit()
	{
		//Sólo si es usuario BITAM se puede cambiar el Password
		//Con usuarios de LDAP no se debe actualizar el password
		if ($this->intConfigSecurity == 0 && $_SESSION["PAFBM_Mode"]!=1)
		{
?>		
		objSpanChangePassword = document.getElementById("spanChangePassword");
		objSpanChangePassword.style.backgroundImage = 'url(images\\btnBackground.gif)';
		objSpanChangePassword.style.cursor = 'pointer';
		objSpanChangePassword.onmouseover = new Function("this.style.color='#98fb98';");
		objSpanChangePassword.onmouseout = new Function("this.style.color='#d5d5bc';");
		objSpanChangePassword.onclick = new Function("openChangePassword();");
<?
		}
	}
	
	function insideCancel()
	{
		if($_SESSION["PAFBM_Mode"]!=1)
		{
?>
		objSpanChangePassword = document.getElementById("spanChangePassword");
		objSpanChangePassword.style.backgroundImage = 'url(images\\btnBackground_off.gif)';
		objSpanChangePassword.style.cursor = 'default';
		objSpanChangePassword.onmouseover = "";
		objSpanChangePassword.onmouseout = "";
		objSpanChangePassword.onclick = "";
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{	
 		$userNames = array();
 		
 		if($_SESSION["PAFBM_Mode"]!=1)
 		{
			$sql = "SELECT NOM_CORTO AS UserName FROM SI_USUARIO";
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			
			if ($aRS === false)
			{
				die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$userNames[] = strtoupper($aRS->fields["username"]);
				$aRS->MoveNext();
			}
 		}
		
		$numUsers = count($userNames);
?>
		<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
 		<script language="JavaScript">
 		
 		userNames = new Array;
<?
		for($i=0;$i<$numUsers;$i++)
		{
?>
			userNames[<?=$i?>]='<?=$userNames[$i]?>';
<?
		}
?>
 		function verifyFieldsAndSave(target)
 		{
 			var strAlert = '';
<?
			if($_SESSION["PAFBM_Mode"]!=1)
			{
?>
 			var oldUserName = '<?=$this->ArtusUser?>';
 			var UserNameStrAux = Trim(BITAMAppUser_SaveForm.ArtusUser.value);
 			var UserNameStr = UserNameStrAux.toUpperCase()
 			var statusTimeFields = true;
 			var i;

 			if(UserNameStr=='')
 			{
 				strAlert += '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"), translate("User Name"))?>';
 			}
 			else
 			{   
 				if(inArray(userNames, UserNameStr)==true && oldUserName!=UserNameStr)
 				{
 					strAlert += '\n- ' + '<?=sprintf(translate("%s already exists"), translate("This User Name"))?>';
 				}
 			}
<?
			}
?>

 			if(Trim(BITAMAppUser_SaveForm.UserName.value)=='')
 			{	
 				strAlert += '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Full Name"))?>';
 			}

			if(strAlert!='')
			{
				alert('<?=translate("Please verify")?>:' + strAlert);
			}
			else
			{
				BITAMAppUser_Ok(target);
			}
 		}
		
	 	function openChangePassword()
	 	{
			var userIDAux = BITAMAppUser_SaveForm.CLA_USUARIO.value;
			strPassword=window.showModalDialog('dialog.html', 'changeUserPassword.php?userID='+ userIDAux, 'help: no; status: no; scroll: yes; resizable: no; dialogHeight:250px; dialogWidth:280px;');

			if(strPassword!=null && strPassword!=undefined)
			{
				objChangePassword = BITAMAppUser_SaveForm.ChangePassword;
				objChangePassword.value = 1;
				objPassword = BITAMAppUser_SaveForm.Password;
				objPassword.value = strPassword;
			}
	 	}

		function validate_email(address) 
		{
			var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			return (reg.test(address))
		}
	 	
<?
		if($_SESSION["PAFBM_Mode"]==1)
		{
?>
		function inviteUsers(target)
		{
<?
		if($this->isNewObject())
		{
?>
			var strAlert = '';
			var strdata = '';
			 
			var semail = Trim(BITAMAppUser_SaveForm.Email.value);
			var spass = Trim(BITAMAppUser_SaveForm.Password.value);
			var sfirst = Trim(BITAMAppUser_SaveForm.FirstName.value);
			var slast = Trim(BITAMAppUser_SaveForm.LastName.value);
			var ocmbpdf = BITAMAppUser_SaveForm.SendPDFByEmail;
			var ssendpdfbyemail = ocmbpdf.options[ocmbpdf.selectedIndex].value;
			var ocmbsup = BITAMAppUser_SaveForm.Supervisor;
			var ssupervisor = ocmbsup.options[ocmbsup.selectedIndex].value;
			var ocmb = BITAMAppUser_SaveForm.Role;
			var iDefSurvey = 0;
			var objCmb = BITAMAppUser_SaveForm.DefaultSurvey;
			if (objCmb) {
				iDefSurvey = objCmb.options[objCmb.selectedIndex].value;
			}
			var iTemplateID = -1;
			var objCmb = BITAMAppUser_SaveForm.TemplateID;
			if (objCmb) {
				iTemplateID = objCmb.options[objCmb.selectedIndex].value;
				if (iTemplateID <= 0) {
					iTemplateID = -1;
				}
			}
			
			if (!validate_email(semail) || semail.length == 0)
			{
				strAlert += '\n- ' + '<?=translate("Invalid e-mail address")?>';
			}
			
			var sAltemail = '';
<?
		if (getMDVersion() >= esvAltEMailForNotif) {
?>
			var sAltemail = Trim(BITAMAppUser_SaveForm.AltEmail.value);
			if (!validate_email(sAltemail))
			{
				strAlert += '\n- ' + '<?=translate("Invalid alternative e-mail address")?>';
			}
<?
		}
?>
			
			if (spass.length == 0)
			{
 				strAlert += '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Password"))?>';
			}
			
 			if(sfirst.length == 0)
 			{	
 				strAlert += '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("First Name"))?>';
 			}
			
  			if(slast.length == 0)
 			{	
 				strAlert += '\n- ' + '<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Last Name"))?>';
 			}
			
			if(strAlert.length > 0)
			{
				alert('<?=translate("Please verify")?>:' + strAlert);
			}
			else
			{
			  var objform = document.forms.frmUploadUsersFile
			  var strNewUserData = semail + "_©_" + spass + "_©_" + sfirst + "_©_" + slast + "_©_" + ocmb.options[ocmb.selectedIndex].value + "_©_" + ssendpdfbyemail + "_©_" + ssupervisor + "_©_" + target;
			  strNewUserData += "_©_" + sAltemail + "_©_" + iDefSurvey + "_©_" + iTemplateID;
			  objform.txtNewUserData.value = strNewUserData
			  objform.submit()
/* sbrande: no more old process
				strdata = semail + "©" + sfirst + "©" + slast;
				
				var objform = document.forms.invite_form;
				objform.guests.value = strdata;
				objform.submit()
				
				BITAMAppUser_Ok(target);
*/				
			}
<?
		}
		else 
		{
?>
				BITAMAppUser_Ok(target);
<?
		}
?>
		}
		
		function refreshpage(target)
		{
		  arrData = target.split("_©_");
		  target = arrData[0];
		  if (arrData.length > 1 && arrData[1].length > 0)
		  {
		    alert("There was an error during the process: \n\n" + arrData[1]);
		  }
		  BITAMAppUser_Ok(target);
		}
<?
 		}
?>
 		</script>
<?
	}

	function generateAfterFormCode($aUser)
	{
		if($_SESSION["PAFBM_Mode"] != 1)
		{
			$functionSave = "verifyFieldsAndSave";
		}
		else 
		{
			$functionSave = "inviteUsers";
		}
?>
	<script language="JavaScript">
		objBody= document.getElementsByTagName("body");
		objBody[0].onkeypress= function onc(event)
		{
			var event = (window.event) ? window.event : event;
			//if (window.event.keyCode ==13)
			if (event.keyCode ==13)
			return false;
		}
<?
		if($_SESSION["PAFBM_Mode"]!=1)
		{
?>
		objOkSelfButton = document.getElementById("BITAMAppUser_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMAppUser_OkParentButton");
		
		objOkSelfButton.onclick=new Function("<?=$functionSave?>('self');");
		objOkParentButton.onclick=new Function("<?=$functionSave?>('parent');");
<?
		}
		else 
		{
			if($this->isNewObject())
			{
?>
 		BITAMAppUser_OkSelfButton.style.display = 'none';
<?
			}
			else 
			{
			
?>	
		objOkSelfButton = document.getElementById("BITAMAppUser_OkSelfButton");
		objOkSelfButton.onclick=new Function("<?=$functionSave?>('self');");
<?
			}
?>
		objOkParentButton = document.getElementById("BITAMAppUser_OkParentButton");
		objOkParentButton.onclick=new Function("<?=$functionSave?>('parent');");
<?
		}

		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMAppUser_OkNewButton");
 			objOkNewButton.onclick=new Function("<?=$functionSave?>('new');");
<?
		}
?>
		try {
			$('tr:not([style])[id^="Row_"]:has(td[class=""])').hide();
		} catch (e) {}
	</script>
	
<?
		if($_SESSION["PAFBM_Mode"] == 1 && $_SESSION["PABITAM_UserRole"] == 1)
		{
			if(isset($_SESSION['saasuser']) && isset($_SESSION["dbid"]))
			{
				$userid = $_SESSION['saasuser'];
				$dbid = $_SESSION["dbid"];
				
				//Verificamos si es un saasuser administrator
				$isAdmin = isSAASUserAdmin($userid, $dbid);

				
				if($isAdmin)
				{
?>
  <iframe name="frameUploadUsersFile" style="display:none"></iframe>
  <form name="frmUploadUsersFile" method="POST" action="uploadUsersFile.php" target="frameUploadUsersFile" style="display:none">
    <input type="hidden" name="txtNewUserData">
  </form>
  </nobr>
	<!--iframe id="iFrameInvite" name="iFrameInvite" style="display:none" width="100%" height="10px"></iframe>
	<form name="invite_form" action="../../../fbm/invite_people.php" method="post" target="iFrameInvite">
		<input name="user_id" type="hidden" value="<?=$userid?>">
		<input name="dbid" type="hidden" value="<?=$dbid?>">
		<input name="guests" type="hidden">
		<input name="tid" type="hidden" value="1">
		<input name="pid" type="hidden" value="1">
	</form-->
<?
				}
			}
 		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
	
	static function addUserFromClaUsuario($aRepository, $cla_usuario, $role=0)
	{
		$svadmin = 0;
		$svuser = 1;

		if($role==1)
		{
			$svadmin = 1;
			$svuser = 0;
		}
		
		$sql = "SELECT CLA_USUARIO, NOM_CORTO, NOM_LARGO, CUENTA_CORREO FROM SI_USUARIO WHERE CLA_USUARIO = ".$cla_usuario;
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if(!$aRS->EOF)
		{
			$sqlExist = "SELECT CLA_USUARIO FROM SI_SV_Users WHERE CLA_USUARIO = ".$cla_usuario;
			
			$aRSExist = $aRepository->ADOConnection->Execute($sqlExist);
			
			if ($aRSExist === false)
			{
				die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlExist);
			}
			
			if ($aRSExist->EOF)
			{
				$cla_usuario = $aRS->fields["cla_usuario"];
				$artusUser = $aRS->fields["nom_corto"];
				$userName = $aRS->fields["nom_largo"];
				$email = $aRS->fields["cuenta_correo"];
				
				$sqlInsert = 	"INSERT INTO SI_SV_Users (
								UserName 
								,Email 
								,SV_Admin
								,SV_User
								,ArtusUser 
								,CLA_USUARIO 
								) VALUES ( ".
								$aRepository->ADOConnection->Quote($userName).
								",".$aRepository->ADOConnection->Quote($email).
								",".$svadmin.
								",".$svuser.
								",".$aRepository->ADOConnection->Quote($artusUser).
								",".$cla_usuario.
								")";
								
				if ($aRepository->ADOConnection->Execute($sqlInsert) === false)
				{
					die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlInsert);
				}
			}
		}
	}
	
	static function addUsersFromClaUsuarioToUsersDim($aRepository, $anArrayOfClaUsuario = null)
	{
	  // get user dimension info!
	  $sql = "SELECT NOM_TABLA, NOM_FISICO FROM SI_DESCRIP_ENC WHERE NOM_LOGICO LIKE 'User'";
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
			return; // user dim info not found... return!
		}
		$strNomTabla = $aRS->fields["nom_tabla"];
		$strNomFisico = $aRS->fields["nom_fisico"];
		
		$where = " WHERE CLA_USUARIO > 0";
		if (!is_null($anArrayOfClaUsuario))
		{
			if (is_array($anArrayOfClaUsuario))
			{
				$where = " WHERE CLA_USUARIO IN (".implode(", ", $anArrayOfClaUsuario).")";
			}
			elseif (is_int($anArrayOfClaUsuario))
			{
				$where = " WHERE CLA_USUARIO = $anArrayOfClaUsuario";
			}
		}
	  
		// get user dim current keys
		//@JAPR 2014-06-12: Corregido un bug, no estaba indicando la cadena sobre la que se hace el reemplazo
		$whereDim = str_replace("CLA_USUARIO", $strNomTabla."KEY", $where);
		$sql = "SELECT ".$strNomTabla."KEY"." FROM $strNomTabla $whereDim";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		$arrUserDimCurrentKeys = array();
		if ($aRS !== false)
		{
			while(!$aRS->EOF)
			{
				$intKey = $aRS->fields[strtolower($strNomTabla."KEY")];
				$arrUserDimCurrentKeys[$intKey] = $intKey;
				$aRS->MoveNext();
			}
		}

		// get si_usuario info
		$sql = "SELECT CLA_USUARIO, CUENTA_CORREO FROM SI_USUARIO $where";
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false || $aRS->EOF)
		{
		  return; // no data to process
		}
		
		while (!$aRS->EOF)
		{
		  $intClaUsuario = $aRS->fields["cla_usuario"];
		  $strEmail = $aRS->fields["cuenta_correo"];
		  if (!array_key_exists($intClaUsuario, $arrUserDimCurrentKeys))
		  {
		    $sqlInsert = "INSERT INTO $strNomTabla (".$strNomTabla."KEY".", $strNomFisico) VALUES ($intClaUsuario, ".$aRepository->DataADOConnection->Quote($strEmail).")";
		    $aRSInsert = $aRepository->DataADOConnection->Execute($sqlInsert);
		    if ($aRSInsert === false)
		    {
		      return; // error!
		    }
		  }
	    $aRS->MoveNext();
		}
	}

	static function getAppUsers($aRepository)
	{
		$appusers = array();
		
		$sql="SELECT CLA_USUARIO, UserName FROM SI_SV_Users ORDER BY UserName";

		$aRS = $aRepository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$userid = (int)$aRS->fields["cla_usuario"];
			$username = $aRS->fields["username"];
			$appusers[$userid] = $username;

			$aRS->MoveNext();
		}

		return $appusers;
	}
	
	static function getNameByUserID($aRepository, $anUserID)
	{
		$userName = "";
		
		$sql = "SELECT UserName FROM SI_SV_Users WHERE UserID = ".$anUserID;
		
		$aRS = $aRepository->ADOConnection->Execute($sql);

		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		if(!$aRS->EOF)
		{
			$userName = $aRS->fields["username"];
		}
		
		return $userName;
	}
	
	static function getArtusRolesByClaUsuario($aRepository, $cla_usuario)
	{
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		$artusRoles =& BITAMGlobalFormsInstance::GetRolesByUserWithID($cla_usuario);
		if(!is_null($artusRoles))
		{
			return $artusRoles;
		}
		//@JAPR
		
		$artusRoles = array();
		
		$sql = "SELECT CLA_ROL FROM SI_ROL_USUARIO WHERE CLA_USUARIO = ".$cla_usuario;
		
		$aRS = $aRepository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$artusRoles[] = (int)$aRS->fields["cla_rol"];
			$aRS->MoveNext();
		}
		
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		BITAMGlobalFormsInstance::AddRolesByUserWithID($cla_usuario, $artusRoles);
		//@JAPR
		return $artusRoles;
	}
	
	//@JAPR 2014-04-04: Agregada la ventana para permitir enviar mensajes vía Push
	static function GetMobileDevicesByUser($aRepository)
	{		
		$sql = "SELECT DeviceID, UserID, RegID, PhoneNumber, DeviceName 
			FROM SI_SV_RegID 
			ORDER BY UserID, PhoneNumber, DeviceName, RegID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die(translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["userid"];
			$intDeviceNum = 0;
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["userid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["userid"];
					$intDeviceNum = 0;
					$aGroup = array();
				}
				
				$intDeviceID = (int) @$aRS->fields["deviceid"];
				$strDeviceName = (string) @$aRS->fields["devicename"];
				if ($strDeviceName == '') {
					$strDeviceName = translate("Mobile device")." #".($intDeviceNum++);
				}
				$aGroup[$intDeviceID] = $strDeviceName;
  				$aRS->MoveNext();

			} while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}
		
		return $allValues;
	}
	
	//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
	/* Carga la lista de permisos del usuario especificado considerando a los grupos de usuarios a los que pertenece, regresando un objeto indexado por los enumerados "oty" (en config.php)
	que representan las diferentes opciones de configuración (ventana) y el tipo de permiso configurado en SI_SV_Security que este usuario puede aplicar. Por default no se aplican
	restricciones a menos que explícitamente se configuren. Con un sólo grupo (o directamente al usuario) que tenga restringida alguna opción, el usuario no podrá accesarla aunque para
	otros grupos si la tenga, dando prioridad al usuario en todo momento (si el usuario directo NO tiene configuración, tomará lo de los grupos, pero si tiene configuración, entonces aplicará
	esa configuración directamente)
	El usuario Maestro (CLA_USUARIO == 1) no leerá esta configuración en ningún caso, ya que él siempre tiene todos los permisos aplicables
	Se dejará la carga de la seguridad como resultado de esta función en lugar de utilizar una colección, porque las variantes como colección forzarían a recorrerla toda para identificar
	un permiso específico, además a la fecha de implementación no había una ventana de configuración, por lo tanto no se requirió generar una clase para accesar a su metadata, así que esta
	función es el único lugar para consulta, pero se deberá crear una clase especial para manipularla vía processRequest.php cuando eventualmente se construya la ventana de configuración
	Para no tener que agregar un elemento mas al array según cada nueva configuración que se cree en el producto, la falta de la configuración implicará el permiso habilitado, lo mismo
	que un valor de 1 en dicha configuración, de tal manera que se pueda preguntar por (!isset(arrSecurity[settingID]) || arrSecurity[settingID]) para determinar el permiso
	El array de seguridad devuelto tendrá la sintaxis:
	$arrSecurity = array(
		SettingID#1: array(
				PermissionID#1: 1|0,
				PermissionID#2: 1|0,
				...
				PermissionID#n: 1|0,
			),
		...
		SettingID#n: array(
				PermissionID#1: 1|0,
				PermissionID#2: 1|0,
				...
				PermissionID#n: 1|0,
			),
	)
	*/
	static function GetUserSecurity($aRepository, $cla_usuario) {
		$arrSecurity = array();
		
		//Si el usuario es inválido o el Master, no regresa permisos para que tenga todo habilitado (no se supone que un usuario inválido con CLA_USUARIO > 0 habría podido llegar
		//hasta este punto, si eso sucede, se le otorgarán todos los permisos pero sería un error de login, no de seguridad - para no tener que pregenerar un array con todo en cero)
		$cla_usuario = (int) $cla_usuario;
		if ($cla_usuario <= usrMASTER) {
			return $arrSecurity;
		}
		
		//Primero verificará si el usuario tiene permisos habilitados directamente, si es así, esos no pueden ser sobreescritos por ningún permiso a nivel de grupo de usuario
		$arrDirectSecurity = array();
		
		$sql = "SELECT A.ObjectID, A.SettingID, A.ShowOpt 
			FROM SI_SV_Security A 
			WHERE A.ObjectType = ".otyUser." AND A.ObjectID IN (-2, {$cla_usuario}) 
			ORDER BY A.ObjectID, A.SettingID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		//En caso de error, lo mas probable es que la metada no estaba actualizada, en cuyo caso no habría sido posible asignar permisos y por tanto el default es que pueda ver todo
		if ($aRS === false) {
			return $arrSecurity;
		}
		
		//Asigna los permisos directos de este usuario
		while (!$aRS->EOF) {
			$intSettingID = (int) @$aRS->fields["settingid"];
			if ($intSettingID > 0) {
				$arrDirectSecurity[$intSettingID] = 1;
				$arrSecurity[$intSettingID] = array();
				
				$intShowOpt = (int) @$aRS->fields["showopt"];
				$arrSecurity[$intSettingID][asecShow] = $intShowOpt;
			}
			
			$aRS->MoveNext();
		}
		
		//Ahora carga los grupos de usuario a los que tiene acceso este usuario para obtener los permisos de cada uno y aplicarlos siempre y cuando no sobreescriban un permiso directo
		//del usuario
		$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $cla_usuario);
		$strUsrGroups = '';
		if (!is_array($roles)) {
			$roles = array();
		}
		$roles[] = -2;
		
		if (is_array($roles)) {
			$strUsrGroups = trim(implode(", ", $roles));
		}
		
		if ($strUsrGroups != '') {
			$sql = "SELECT A.ObjectID, A.SettingID, A.ShowOpt 
				FROM SI_SV_Security A 
				WHERE A.ObjectType = ".otyUserGroup." AND A.ObjectID IN ({$strUsrGroups}) 
				ORDER BY A.ObjectID, A.SettingID";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			//En caso de error, lo mas probable es que la metada no estaba actualizada, en cuyo caso no habría sido posible asignar permisos y por tanto el default es que pueda ver todo
			if ($aRS === false) {
				return $arrSecurity;
			}
			
			//Asigna los permisos directos del grupo de usuario procesado, evitante sobreescribir los permisos directos. En caso de reasignar un permiso, sólo será válido si el
			//permiso es para denegar, ya que con una sola ocurrencia de dicho tipo de permiso en alguno de los grupos, el usuario no tendrá acceso a la configuración correspondiente
			while (!$aRS->EOF) {
				$intSettingID = (int) @$aRS->fields["settingid"];
				if ($intSettingID > 0 && !isset($arrDirectSecurity[$intSettingID])) {
					if (!isset($arrSecurity[$intSettingID])) {
						$arrSecurity[$intSettingID] = array();
					}
					
					$intShowOpt = (int) @$aRS->fields["showopt"];
					//Si el nuevo permiso es para bloquear o bien el permiso no estaba aún asignado, se agrega al array de permisos, ya que si estaba previamente asignado, entonces
					//o no tendría caso actualizar al mismo valor, o no se hubiera podido habilitar un permiso deshabilitado por otro grupo
					if (!$intShowOpt || !isset($arrSecurity[$intSettingID][asecShow])) {
						$arrSecurity[$intSettingID][asecShow] = $intShowOpt;
					}
				}
				
				$aRS->MoveNext();
			}
		}
		
		return $arrSecurity;
	}
	
	//@JAPR 2019-08-08: Agregada la posibilidad de almacenar colores personalizados en algunos componentes de diseño del usuario (#BT0NMN)
	/* Obtiene los colores personalizados grabados para este usuario */
	function getCustomColorsArray() {
		$sql = "SELECT A.ColorID, A.Color 
			FROM SI_SV_EditorCustomColors A 
			WHERE A.UserID = {$this->UserID} 
			ORDER BY A.ColorID";
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		if ( !$aRS ) {
			return null;
		}
		
		$arrCustomizedColors = array();
		$intMaxColorID = 0;
		while (!$aRS->EOF) {
			$intColorID = (int) @$aRS->fields["colorid"];
			$strColor = (string) @$aRS->fields["color"];
			$arrCustomizedColors[$intColorID] = $strColor;
			if ( $intColorID > $intMaxColorID ) {
				$intMaxColorID = $intColorID;
			}
			$aRS->MoveNext();
		}
		
		//Verifica que no existan huecos en los colores, si los hay, genera el color blanco como default
		for ($intColorID = 1; $intColorID < $intMaxColorID; $intColorID++) {
			if ( !isset($arrCustomizedColors[$intColorID] ) ) {
				$arrCustomizedColors[$intColorID] = "#ffffff";
			}
		}
		
		//Al final debe reordenar los colores por su ID en caso de que lo huecos provocaran que se insertaran en orden incorrecto
		ksort($arrCustomizedColors);
		
		return $arrCustomizedColors;
	}
	
	/* Almacena los colores personalizados en la tabla correspondiente */
	function saveCustomColorsArray($aCustomColorsArr) {
		if ( !is_array($aCustomColorsArr) || !count($aCustomColorsArr) ) {
			return;
		}
		
		//Primero debe eliminar los colores previamente recibidos para este usuario
		$sql = "DELETE FROM SI_SV_EditorCustomColors 
			WHERE UserID = {$this->UserID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		//Inserta cada uno de los colores recibidos, los cuales deberían venir en el orden correcto del componente
		foreach ($aCustomColorsArr as $intColorID => $strColor) {
			if ( $strColor) {
				$sql = "INSERT INTO SI_SV_EditorCustomColors (UserID, ColorID, Color) 
					VALUES ({{$this->UserID}}, {$intColorID}, ".$this->Repository->ADOConnection->Quote($strColor).")";
				$this->Repository->DataADOConnection->Execute($sql);
			}
		}
	}
	//@JAPR
}

class BITAMAppUserCollection extends BITAMCollection
{
  private $logfile = "";
  
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstance($aRepository, $anArrayOfUserIDs = null)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'Collection::NewInstance($aRepository, $anArrayOfUserIDs);');
		}
		$anInstance = new $strCalledClass($aRepository);
		$where = "";

		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE B.UserID = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $anUserID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $anUserID; 
					}
					if ($where != "")
					{
						$where = "WHERE B.UserID IN (".$where.")";
					}
					break;
			}
		}
		
		if($where=="")
		{
			$where.=" WHERE ";
		}
		else
		{
			$where.=" AND ";
		}
		
		$where.=" A.NOM_LARGO NOT LIKE (".$aRepository->ADOConnection->Quote("dummy").") ";
		
		if($_SESSION["PAFBM_Mode"])
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$where.= " AND A.CLA_USUARIO > 0";
			//@JAPR
		}
		//@JAPR 2015-11-18: Corregido un bug, se estaban mostrando usuarios que no existían en SI_SV_Users, así que no se habrían podido cambiar sus configuraciones (#QQCMGH)
		$where.= " AND B.UserID > 0";
		
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		//$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor ";

		$sFilter = $where;
		//GCRUZ 2015-10-02. Ordenar por nombre y no por cla_usuario.
		//@JAPR 2015-11-19: Modificado el ordenamiento del campo para considerar UserName si tiene valor, pero si no entonces ordenar por EMail
		$sOrderBy = "CASE WHEN B.UserName IS NOT NULL AND RTRIM(B.UserName) <> '' THEN B.UserName ELSE A.CUENTA_CORREO END";	// "B.UserName";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$sql = BITAMAppUser::getQuery($qOpts);
		//@JAPR
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			//@AAL 17/07/2015: Agregado parámetro true para indicar si debe accesar a la api que obtiene el FirstName y lastName del SaasUser
			$anInstance->Collection[] = BITAMAppUser::NewInstanceFromRS($anInstance->Repository, $aRS, true);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function NewInstanceByClaUsuario($aRepository, $anArrayOfUserIDs = null)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'Collection::NewInstanceByClaUsuario($aRepository, $anArrayOfUserIDs);');
		}
		$anInstance = new $strCalledClass($aRepository);
		$where = "";

		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE A.CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $anUserID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int) $anUserID; 
					}
					if ($where != "")
					{
						$where = "WHERE A.CLA_USUARIO IN (".$where.")";
					}
					break;
			}
		}
		
		if($where=="")
		{
			$where.=" WHERE ";
		}
		else
		{
			$where.=" AND ";
		}
		
		$where.=" A.NOM_LARGO NOT LIKE (".$aRepository->ADOConnection->Quote("dummy").") ";
		
		if($_SESSION["PAFBM_Mode"])
		{
			//@JAPR 2016-07-21: Validado que sólo los usuarios creados por el Maestro o el propio Maestro estén disponibles para procesos de eForms (había un usuario dummy con
			//CLA_USUARIO = -1 en la cuenta de Workbench y eForms lo mostraba aunque no tenía nombre)
			$where.= " AND A.CLA_USUARIO > 0";
			//@JAPR
		}
		//@JAPR 2015-11-18: Corregido un bug, se estaban mostrando usuarios que no existían en SI_SV_Users, así que no se habrían podido cambiar sus configuraciones (#QQCMGH)
		$where.= " AND B.UserID > 0";
		
		//@JAPR 2012-10-08: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		//$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID, B.SendPDFByEmail, B.Supervisor ";
		
		$sFilter = $where;
		//@JAPR 2015-11-19: Modificado el ordenamiento del campo para considerar UserName si tiene valor, pero si no entonces ordenar por EMail
		$sOrderBy = "CASE WHEN B.UserName IS NOT NULL AND RTRIM(B.UserName) <> '' THEN B.UserName ELSE A.CUENTA_CORREO END";	// "B.UserName";
		$qOpts = array("filter" => $sFilter, "orderBy" => $sOrderBy);
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		$sql = $strCalledClass::getQuery($qOpts);
		//@JAPR
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Users ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			//@AAL 17/07/2015: Agregado parámetro true para indicar si debe accesar a la api que obtiene el FirstName y lastName del SaasUser
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS, true);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$aClassName = GetBITAMAppUserClass($aRepository, $strCalledClass);
		if ($aClassName != $strCalledClass)
		{
			return eval('return '.$aClassName.'Collection::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);');
		}
		
		$objUserCollection = $strCalledClass::NewInstanceByClaUsuario($aRepository, null);
		if (array_key_exists("logfile", $aHTTPRequest->GET))
		{
			$objUserCollection->logfile = $aHTTPRequest->GET["logfile"];
		}
		return $objUserCollection;
		//return BITAMAppUserCollection::NewInstanceByClaUsuario($aRepository, null);
	}

	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Users");
	}

	function get_QueryString()
	{
		return "BITAM_SECTION=AppUserCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=AppUser";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'CLA_USUARIO';
	}

	function get_FormFieldName() {
		return 'UserName';
	}

	function get_FormFields($aUser)
	{
		global $blnTestingServer;
		
		require_once("formfield.inc.php");
		
		$myFields = array();

		if($_SESSION["PAFBM_Mode"]!=1)
		{
			//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
			if (!$blnTestingServer) {
				$aField = BITAMFormField::NewFormField();
				$aField->Name = "UserName";
				$aField->Title = translate("Full Name");
				$aField->Type = "String";
				$aField->Size = 255;
				$myFields[$aField->Name] = $aField;
			}
			//@JAPR
		}

		//@JAPR 2013-05-06: Agregada la posibilidad de editar el nombre largo en las cuentas de kpionline (sólo en modo de testing, IUSACELL)
		if ($blnTestingServer) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserName";
			$aField->Title = translate("Full Name");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Email";
		$aField->Title = translate("E-Mail");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function addButtons($aUser)
	{
		global $gbIsGeoControl;
		
	  $isAdmin = false;
		if(isset($_SESSION['saasuser']) && isset($_SESSION["dbid"]))
		{
			$userid = $_SESSION['saasuser'];
			$dbid = $_SESSION["dbid"];

			//Verificamos si es un saasuser administrator
			$isAdmin = isSAASUserAdmin($userid, $dbid);
		}
		//if (TRUE) //($_SESSION["PA_ADMIN"] == 1)
		//@JAPR 2015-04-24: Validado que en GeoControl no se puedan administrar usuarios
		if ($isAdmin && !$gbIsGeoControl)
		{
?>
    <nobr>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadTemplate();"><img src="images/download_xls_templates.gif"> <?=translate("Download users template")."   "?>  </span>-->
		<button id="btnDownloadTemplate" class="alinkescfav" onclick="downloadTemplate();"><img src="images/download_xls_templates.gif" alt="<?=translate('Download users template')?>" title="<?=translate('Download users template')?>" displayMe="1" /> <?=translate("Download users template")?></button>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="uploadUsersFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> <?=translate("Upload users file")."   "?>  </span></span>-->
		<button id="btnUploadUsersFile" class="alinkescfav" onclick="uploadUsersFile();"><img src="images/load_data_XLS.gif" alt="<?=translate('Upload users file')?>" title="<?=translate('Upload users file')?>" displayMe="1" /> <?=translate("Upload users file")?></button>
    <form name="frmUploadUsersFile" method="POST" action="uploadUsersFile.php" enctype="multipart/form-data" target="frameUploadUsersFile" style="display:none">
      <input type="file" name="xlsfile">
      <input type="submit" name="txtsubmit" value="<?=translate("Upload file")?>">
      <input type="button" name="txtcancel" value="<?=translate("Cancel")?>" onclick="document.forms.frmUploadUsersFile.style.display='none';document.forms.frmUploadUsersFile.reset()">
    </form>
    </nobr>
<?
		}
		if (strlen($this->logfile) > 0)
		{
?>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="showLogFile('<?=$this->logfile?>');"><img src="images/showMsg.gif"> <?=translate("View last upload log")."   "?>  </span>-->
		<button id="btnShowLogFile" class="alinkescfav" onclick="showLogFile('<?=$this->logfile?>');"><img src="images/showMsg.gif" alt="<?=translate('View last upload log')?>" title="<?=translate('View last upload log')?>" displayMe="1" /> <?=translate("View last upload log")?></button>
<?		  
		}
	}
	
	function generateBeforeFormCode($aUser)
	{
?>	 
  <iframe name="frameUploadUsersFile" style="display:none"></iframe>
	<script language="JavaScript">
 		function downloadTemplate()
 		{
			window.open("downloadTemplate.php", "downloadtemplate", "menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no", 1)
 		}
 		function uploadUsersFile()
 		{
 		  document.forms.frmUploadUsersFile.style.display = "inline"
 		}
 		function refreshpage(sLogFile)
 		{
 		  sURL = "main.php?BITAM_SECTION=AppUserCollection"
 		  if (sLogFile.length > 0)
 		  {
 		    sURL += "&logfile=" + sLogFile
 		  }
 		  window.location.href = sURL
 		}
 		function showLogFile(sLogFile)
 		{
 		  window.open(sLogFile, "logfile", "menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=auto", 1)
 		}
 	</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
		
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		global $gbIsGeoControl;
		
		//@JAPR 2015-04-24: Validado que en GeoControl no se puedan administrar usuarios
		if($_SESSION["PABITAM_UserRole"]==1 && !$gbIsGeoControl)
		{
			if($_SESSION["PAFBM_Mode"]==1)
			{
				if(isset($_SESSION['saasuser']) && isset($_SESSION["dbid"]))
				{
					$userid = $_SESSION['saasuser'];
					$dbid = $_SESSION["dbid"];

					//Verificamos si es un saasuser administrator
					$isAdmin = isSAASUserAdmin($userid, $dbid);
					
					if($isAdmin==true)
					{
						return true;
					}
					else 
					{
						
						return false;
					}
				}
				else 
				{
					return false;
				}
			}
			else 
			{
				return true;
			}
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return false;
	}
}
?>