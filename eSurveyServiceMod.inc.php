<?php

//json_encode($c, JSON_FORCE_OBJECT)

/* Obtiene el JSon de todas las definiciones utilizando el nuevo esquema de Modelo de datos para las Apps, en el cual la propia instancia de cada
clase contendrá variables con los valores que va capturando
//@JAPR 2015-02-23: Rediseñado el Admin con DXHTML
Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array
//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
Agregados los parámetros $aUserID y $oDataToGet para especificar la información a cargar
*/
function getEFormsDefinitions($aRepository, $aParamsColl = null, $aUserID = -1, $oDataToGet = null) {

	global $appVersion;
	global $kpiUser;
	//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
	global $gbDesignMode;
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	$intUserID = $aUserID;
	if ( $aUserID == -1 ) {
		$intUserID = $_SESSION["PABITAM_UserID"];
	}
	
	$blnGetAllData = false;
	if ( is_null($oDataToGet) || !is_array($oDataToGet) || count($oDataToGet) == 0 ) {
		$oDataToGet = array();
		$blnGetAllData = true;
	}
	//@JAPR
	
	//@JAPR 2015-02-23: Rediseñado el Admin con DXHTML
	if (is_null($aParamsColl) || !is_array($aParamsColl)) {
		$aParamsColl = array();
	}
	//@JAPR
	
	//@JAPR 2012-10-22: Corregido un bug, los catálogos al no pertenecer a un usuario, deben filtrarse por el total de posibles encuestas
	//a las que tiene acceso el usuario, independientemente de la encuesta que fué solicitada, ya que el servicio regresa la información
	//por tipo de objeto sin dependencias entre ellos
	$allSurveysList = array();
	//@JAPR
    $surveyList = array();
	$catalogList = array();
	$agendaList = array();
	$statusAgendaList = array();
    $activeStatus = array(0 => 0, 1 => 1);
	$return['error'] = false;
	
    require_once('survey.inc.php');
    require_once('section.inc.php');
    require_once('question.inc.php');
    require_once('dataSource.inc.php');
    require_once('dataSourceFilter.inc.php');
    require_once('catalog.inc.php');
    require_once('surveyAgenda.inc.php');
    
	//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	require_once("surveyProfile.inc.php");
	if (getMDVersion() >= esvSurveyMenu) {
		require_once('surveyMenu.inc.php');
		$menuList = array();
	}
	
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $intUserID);

	//Identifica los parámetros de los elementos a cargar
	//@JAPR 2015-02-23: Rediseñado el Admin con DXHTML
	if (isset($aParamsColl['Survey'])) {
		$arrSurveyIDs = $aParamsColl['Survey'];
	}
	else {
		$arrSurveyIDs = getParamValue('Survey', 'both', '', true);
	}
	if (isset($aParamsColl['Catalog'])) {
		$arrCatalogIDs = $aParamsColl['Catalog'];
	}
	else {
		$arrCatalogIDs = getParamValue('Catalog', 'both', '', true);
	}
	if (isset($aParamsColl['Agenda'])) {
		$arrAgendaIDs = $aParamsColl['Agenda'];
	}
	else {
		$arrAgendaIDs = getParamValue('Agenda', 'both', '', true);
	}
	if (isset($aParamsColl['xcldSurvey'])) {
		$arrXcldSurveyIDs = $aParamsColl['xcldSurvey'];
	}
	else {
		$arrXcldSurveyIDs = getParamValue('xcldSurvey', 'both', '', true);
	}
	if (isset($aParamsColl['xcldCatalog'])) {
		$arrXcldCatalogIDs = $aParamsColl['xcldCatalog'];
	}
	else {
		$arrXcldCatalogIDs = getParamValue('xcldCatalog', 'both', '', true);
	}
	if (isset($aParamsColl['xcldAgenda'])) {
		$arrXcldAgendaIDs = $aParamsColl['xcldAgenda'];
	}
	else {
		$arrXcldAgendaIDs = getParamValue('xcldAgenda', 'both', '', true);
	}
	if (isset($aParamsColl['xcldCategories'])) {
		$intXcldCategories = $aParamsColl['xcldCategories'];
	}
	else {
		$intXcldCategories = getParamValue('xcldCategories', 'both', '(int)');
	}
	if (isset($aParamsColl['xcldUsers'])) {
		$intXcldUsers = $aParamsColl['xcldUsers'];
	}
	else {
		$intXcldUsers = getParamValue('xcldUsers', 'both', '(int)');
	}
	if (isset($aParamsColl['Menu'])) {
		$arrMenuIDs = $aParamsColl['Menu'];
	}
	else {
		$arrMenuIDs = getParamValue('Menu', 'both', '', true);
	}
	if (isset($aParamsColl['xcldMenu'])) {
		$arrXcldMenuIDs = $aParamsColl['xcldMenu'];
	}
	else {
		$arrXcldMenuIDs = getParamValue('xcldMenu', 'both', '', true);
	}
	//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//Si este parámetro viene asignado y es == 1, entonces significa que el request viene de una captura de browser en línea para una única forma (originalmente
	//utilizado para el Online Entry del Administrador de eForms en la ventana de diseño de la forma, pero se usará por igual para las URLs invocadas desde otros
	//productos) así que entre otras cosas los requests se comportarán un poco diferente para optimizarse en cuanto a la información que cargan o no cargan
	$intManualLogin = getParamValue('loginDataManual', 'both', '(int)');
	//@JAPR
	
	/*07Mayo2013*/
	$captureVia = getParamValue('CaptureVia', 'both', '', true);
	
	//Si se recibieron arrays, obtiene la lista de IDs, de lo contario se tuvo que haber recibido al menos un elemento o vacio
	if (is_null($arrSurveyIDs) || (!is_array($arrSurveyIDs) && (int)$arrSurveyIDs == 0)) {
		$arrSurveyIDs = '';
	}
	else {
		$arrSurveyIDs = (is_array($arrSurveyIDs)? implode(',', $arrSurveyIDs) : (int) $arrSurveyIDs);
	}
	$arrSurveyIDs = trim(str_replace("\'", "", $arrSurveyIDs));
	if (is_null($arrCatalogIDs) || (!is_array($arrCatalogIDs) && (int)$arrCatalogIDs == 0)) {
		$arrCatalogIDs = '';
	}
	else {
		$arrCatalogIDs = (is_array($arrCatalogIDs)? implode(',', $arrCatalogIDs) : (int) $arrCatalogIDs);
	}
	$arrCatalogIDs = trim(str_replace("\'", "", $arrCatalogIDs));
	if (is_null($arrAgendaIDs) || (!is_array($arrAgendaIDs) && (int)$arrAgendaIDs == 0)) {
		$arrAgendaIDs = '';
	}
	else {
		$arrAgendaIDs = (is_array($arrAgendaIDs)? implode(',', $arrAgendaIDs) : (int) $arrAgendaIDs);
	}
	$arrAgendaIDs = trim(str_replace("\'", "", $arrAgendaIDs));
	//@JAPR 2015-09-15: Corregido un bug, siempre estaba regresando las definiciones de menús aunque explícitamente no se hubieran pedido, ahora tomará en cuenta el parámetro
	if (is_null($arrMenuIDs) || (!is_array($arrMenuIDs) && (int)$arrMenuIDs== 0)) {
		//Se usará null para cargar todos los menús
		$arrMenuIDs = null;
	}
	else {
		//@JAPR 2015-09-15: Corregido un bug, siempre estaba regresando las definiciones de menús aunque explícitamente no se hubieran pedido, ahora tomará en cuenta el parámetro
		if (is_array($arrMenuIDs)) {
			//Ya es un array, no hay que hacer nada diferente
		}
		else {
			//Se asume que es una lista, se hace un explode para generar un array
			$arrMenuIDs = explode(",", $arrMenuIDs);
			//$arrMenuIDs = (is_array($arrMenuIDs)? implode(',', $arrMenuIDs) : (int) $arrMenuIDs);
		}
	}
	//$arrMenuIDs = trim(str_replace("\'", "", $arrMenuIDs));
	//@JAPR
	if (is_null($arrXcldSurveyIDs) || (!is_array($arrXcldSurveyIDs) && (int)$arrXcldSurveyIDs == 0)) {
		$arrXcldSurveyIDs = '';
	}
	else {
		$arrXcldSurveyIDs = (is_array($arrXcldSurveyIDs)? implode(',', $arrXcldSurveyIDs) : (int) $arrXcldSurveyIDs);
	}
	$arrXcldSurveyIDs = trim(str_replace("\'", "", $arrXcldSurveyIDs));
	if (is_null($arrXcldCatalogIDs) || (!is_array($arrXcldCatalogIDs) && (int)$arrXcldCatalogIDs == 0)) {
		$arrXcldCatalogIDs = '';
	}
	else {
		$arrXcldCatalogIDs = (is_array($arrXcldCatalogIDs)? implode(',', $arrXcldCatalogIDs) : (int) $arrXcldCatalogIDs);
	}
	$arrXcldCatalogIDs = trim(str_replace("\'", "", $arrXcldCatalogIDs));
	if (is_null($arrXcldAgendaIDs) || (!is_array($arrXcldAgendaIDs) && (int)$arrXcldAgendaIDs == 0)) {
		$arrXcldAgendaIDs = '';
	}
	else {
		$arrXcldAgendaIDs = (is_array($arrXcldAgendaIDs)? implode(',', $arrXcldAgendaIDs) : (int) $arrXcldAgendaIDs);
	}
	$arrXcldAgendaIDs = trim(str_replace("\'", "", $arrXcldAgendaIDs));
	if (is_null($arrXcldMenuIDs) || (!is_array($arrXcldMenuIDs) && (int)$arrXcldMenuIDs == 0)) {
		$arrXcldMenuIDs = '';
	}
	else {
		$arrXcldMenuIDs = (is_array($arrXcldMenuIDs)? implode(',', $arrXcldMenuIDs) : (int) $arrXcldMenuIDs);
	}
	$arrXcldMenuIDs = trim(str_replace("\'", "", $arrXcldMenuIDs));
	
    /*07Mayo2013*/
	//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
	//Si se está en modo de diseño entonces no se debe hacer join con los Shchedulers para obtener las definiciones
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['surveys']) || isset($oDataToGet['catalogs']) ) {
		if($captureVia==0 && !$gbDesignMode)
		{
			//Obtener el listado de encuestas disponibles para el usuario logueado x user
			//@JAPR 2012-10-22: Corregido un bug, los catálogos al no pertenecer a un usuario, deben filtrarse por el total de posibles encuestas
			//a las que tiene acceso el usuario, independientemente de la encuesta que fué solicitada, ya que el servicio regresa la información
			//por tipo de objeto sin dependencias entre ellos
			//@JAPR 2015-09-21: Corregido un bug, si se descargan formas vacías provocarán error por dejar la bandera de carga activada cuando no puedan redireccionar a la forma (#6HX3VT)
			//Se agrega el join para sólo obtener formas que si tienen secciones
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ")";
			if (count($roles) > 0) {
				//Obtener el listado de encuestas disponibles para el usuario logueado x rol
				//Se agrega el join para sólo obtener formas que si tienen secciones
				$sql .= " UNION 
					SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum 
					FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C
					WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (" . implode(",", $roles) . ") 
						AND A.Status IN (" . implode(",", $activeStatus) . ")";
			}
			//Obtiene el listado de encuestas disponibles para el usuario logeado x Agendas
			//Se agrega el join para sólo obtener formas que si tienen secciones

			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql .= " UNION 
				SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ") AND C.AgendaID NOT IN ( 
						SELECT DISTINCT AgendaID 
						FROM SI_SV_SurveyAgendaDet 
						WHERE Status = 2) 
				ORDER BY 2";
			//@JAPR
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting survey definition list';
				$allSurveysList[-1] = -1;		//Este valor es para que no se regresen elementos no deseados en caso de error
			}
			else {
				while (!$recordset->EOF) {
					$surveyID = (int) $recordset->fields["surveyid"];
					if (!isset($allSurveyList[$surveyID]))
					{
						$allSurveysList[$surveyID] = $surveyID;
					}
					$recordset->MoveNext();
				}
			}
		}/*07Mayo2013*/
		else
		{
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum 
					FROM SI_SV_Survey A 
					WHERE A.Status IN (" . implode(",", $activeStatus) . ")";
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) 
			{
				$return['error'] = true;
				$return['errmsg'] = 'Error getting survey definition list';
				$allSurveysList[-1] = -1;//Este valor es para que no se regresen elementos no deseados en caso de error
			}
			else
			{
				while (!$recordset->EOF)
				{
					$surveyID = (int) $recordset->fields["surveyid"];
					if (!isset($allSurveyList[$surveyID]))
					{
						$allSurveysList[$surveyID] = $surveyID;
					}
					$recordset->MoveNext();
				}
			}
		}
	}
	//@JAPR
    
    $strAllSurveyIDs = '';
    $arrAllSurveyIDs = array();
    if (count($allSurveysList) > 0) {
	    $arrAllSurveyIDs = array_keys($allSurveysList);
		$strAllSurveyIDs = implode(',', $arrAllSurveyIDs);
    }
    
	//@JAPR 2014-08-26: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	//Había faltado considerar los perfiles en la obtención de las definiciones, porque aunque se excluyeran al obtener las versiones, al no venir
	//dentro del parámetro $arrXcldSurveyIDs si se estaban regresando como definición al dispositivo
	$arrInvalidSurveys = array();
	$strInvalidSurveys = '';
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['surveys']) || isset($oDataToGet['catalogs']) ) {
		if (getMDVersion() >= esvSurveyProfiles) {
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $intUserID);
			if (!is_null($theAppUser)) {
				$intSurveyProfileID = (int) @$theAppUser->ProfileID;
				if ($intSurveyProfileID > 0) {
					$arrInvalidSurveys = BITAMSurveyProfile::GetInvalidSurveys($aRepository, $intSurveyProfileID);
					if (is_null($arrInvalidSurveys) || !is_array($arrInvalidSurveys)) {
						$arrInvalidSurveys = array();
					}
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Encuestas inválidas para el perfil");
					PrintMultiArray($arrInvalidSurveys);
					echo("<br>\r\n");
				}
				
				if (is_array($arrInvalidSurveys) && count($arrInvalidSurveys) > 0) {
					$arrInvalidSurveysKeys = array_keys($arrInvalidSurveys);
					$strInvalidSurveys = implode(',', $arrInvalidSurveysKeys);
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo("<br>\r\n IDs de encuestas inválidas para el perfil: ".$strInvalidSurveys);
						echo("<br>\r\n");
					}
					
					//Concatena al array de exclusión aquellas encuestas que se consideran inválidas
					if ($arrXcldSurveyIDs != '') {
						$arrXcldSurveyIDs .= ',';
					}
					$arrXcldSurveyIDs .= $strInvalidSurveys;
				}
			}
		}
	}
	//@JAPR
	
	//07Mayo2013
	//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
	//Si se está en modo de diseño entonces no se debe hacer join con los Shchedulers para obtener las definiciones
	//@JAPR 2015-07-20: Validado que no se descarguen al App definiciones que no corresponden con la versión 6 o posterior si el App es de dicha versión
	$strPostV6ObjectFilter = "";
	if ($appVersion >= esveFormsv6) {
		$strPostV6ObjectFilter = " AND A.CreationVersion >= ".esveFormsv6." ";
	}
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['surveys']) || isset($oDataToGet['catalogs']) ) {
		if($captureVia==0 && !$gbDesignMode)
		{
			//Obtener el listado de encuestas disponibles para el usuario logueado x user
			//@JAPR 2015-09-21: Corregido un bug, si se descargan formas vacías provocarán error por dejar la bandera de carga activada cuando no puedan redireccionar a la forma (#6HX3VT)
			//Se agrega el join para sólo obtener formas que si tienen secciones
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ") ".$strPostV6ObjectFilter;
			if ($arrSurveyIDs != '') {
			  $sql .= "AND A.SurveyID IN (".$arrSurveyIDs.") ";
			}
			if ($arrXcldSurveyIDs != '') {
			  $sql .= "AND A.SurveyID NOT IN (".$arrXcldSurveyIDs.") ";
			}
			if (count($roles) > 0) {
				//Obtener el listado de encuestas disponibles para el usuario logueado x rol
				//Se agrega el join para sólo obtener formas que si tienen secciones
				$sql .= " UNION 
				  SELECT DISTINCT A.SurveyID, A.SurveyName 
					FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C
				  WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (" . implode(",", $roles) . ") 
					AND A.Status IN (" . implode(",", $activeStatus) . ") ".$strPostV6ObjectFilter;
				if ($arrSurveyIDs != '') {
				  $sql .= "AND A.SurveyID IN (".$arrSurveyIDs.") ";
				}
				if ($arrXcldSurveyIDs != '') {
				  $sql .= "AND A.SurveyID NOT IN (".$arrXcldSurveyIDs.") ";
				}
			}

			//Obtiene el listado de encuestas disponibles para el usuario logeado x Agendas
			//Se agrega el join para sólo obtener formas que si tienen secciones
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql .= " UNION 
			SELECT DISTINCT A.SurveyID, A.SurveyName 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ") AND C.AgendaID NOT IN ( 
						SELECT DISTINCT AgendaID 
						FROM SI_SV_SurveyAgendaDet 
						WHERE Status = 2) ".$strPostV6ObjectFilter;
			if ($arrSurveyIDs != '') {
			  $sql .= "AND A.SurveyID IN (".$arrSurveyIDs.") ";
			}
			if ($arrXcldSurveyIDs != '') {
			  $sql .= "AND A.SurveyID NOT IN (".$arrXcldSurveyIDs.") ";
			}
			$sql .= "ORDER BY 2";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitions) Query para encuestas: {$sql}");
			}
			//@JAPR
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting survey definitions';
			}
			else {
				while (!$recordset->EOF) {
					$surveyID = (int) $recordset->fields["surveyid"];
					$surveyList[$surveyID] = $surveyID;
					$recordset->MoveNext();
				}
			}
		}//07Mayo2013
		else
		{
			//Obtener el listado de encuestas disponibles para el usuario logueado x user
			//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
			//Modificado para permitir recibir varias formas simultáneamente por compatibilidad con los otros queries, aunque en realidad en el modo Diseño sólo 
			//se utiliza una
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName 
					FROM SI_SV_Survey A 
					WHERE A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter;
			if ($arrSurveyIDs != '') {
			  $sql .= "AND A.SurveyID IN (".$arrSurveyIDs.") ";
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitions) Query para encuestas: {$sql}");
			}
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) 
			{
				$return['error'] = true;
				$return['errmsg'] = 'Error getting survey definitions';
			}
			else 
			{
				while (!$recordset->EOF)
				{
					$surveyID = (int) $recordset->fields["surveyid"];
					$surveyList[$surveyID] = $surveyID;
					$recordset->MoveNext();
				}
			}
		}
	}
		
	//@JAPR 2015-04-09: Corregido un bug, si se pedía sólo una forma o no se tenía acceso a ninguna, de todas formas traía la información de todos los catálogos
	//de todas las formas a menos que se pidiera exclusivamente un catálogo
	$strRequestedSurveyIDs = $arrSurveyIDs;
    //Lista de IDs de las encuestas a las que tiene acceso el usuario solicitado
    //@JAPRWarning: Validar que si el array es idéntico al total de encuestas, mejor se envíe vacio el String para no aplicar filtro
    $strSurveyIDs = '';
    $arrSurveyIDs = array();
    if (count($surveyList) > 0) {
	    $arrSurveyIDs = array_keys($surveyList);
		$strSurveyIDs = implode(',', $arrSurveyIDs);
    }
    
	//Obtiene el listado de los catálogos que son usados por preguntas en las encuestas a las que se tiene acceso
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['catalogs']) ) {
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		$sql = "SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Question A 
				WHERE A.CatalogID > 0 ";
		//@JAPR 2015-04-09: Corregido un bug, si se pedía sólo una forma o no se tenía acceso a ninguna, de todas formas traía la información de todos los catálogos
		//de todas las formas a menos que se pidiera exclusivamente un catálogo
		//Si se solicitó una Forma directa, entonces se aplica el filtro por ella en lugar de por todas las formas posible a las que se tiene acceso
		if ($strRequestedSurveyIDs && $strRequestedSurveyIDs != -1) {
			$sql .= "AND A.SurveyID IN (".$strRequestedSurveyIDs.") ";
		}
		else {
			if ($strAllSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strAllSurveyIDs.") ";
			}
			//@JAPR 2015-05-14: Corregido un bug, si no se tenía acceso a ninguna forma, al no aplicar filtro por ella se estaban devolviendo
			//todos los catálogos y eso llevaba además a datos innecesarios, posibles errores con eBavel, se validará que se aplique un filtro
			//inválido a menos que se hubiera pedido un catálogo específico (para permitir funcionar como Web Service) (#P1WYWM)
			else {
				if ($arrCatalogIDs == '') {
					$sql .= "AND A.SurveyID IN (-1) ";
				}
			}
			//@JAPR
		}
		//@JAPR 2014-08-26: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		//Había faltado considerar los perfiles en la obtención de las definiciones, porque aunque se excluyeran al obtener las versiones, al no venir
		//dentro del parámetro $arrXcldSurveyIDs si se estaban regresando como definición al dispositivo
		if ($strInvalidSurveys != '') {
			$sql .= "AND A.SurveyID NOT IN (".$strInvalidSurveys.") ";
		}
		if ($arrCatalogIDs != '') {
		  $sql .= "AND A.CatalogID IN (".$arrCatalogIDs.") ";
		}
		if ($arrXcldCatalogIDs != '') {
		  $sql .= "AND A.CatalogID NOT IN (".$arrXcldCatalogIDs.") ";
		}
		$sql .= " 
			UNION 
				SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Section A 
				WHERE A.CatalogID > 0 ";
		//@JAPR 2015-04-09: Corregido un bug, si se pedía sólo una forma o no se tenía acceso a ninguna, de todas formas traía la información de todos los catálogos
		//de todas las formas a menos que se pidiera exclusivamente un catálogo
		//Si se solicitó una Forma directa, entonces se aplica el filtro por ella en lugar de por todas las formas posible a las que se tiene acceso
		if ($strRequestedSurveyIDs && $strRequestedSurveyIDs != -1) {
			$sql .= "AND A.SurveyID IN (".$strRequestedSurveyIDs.") ";
		}
		else {
			if ($strAllSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strAllSurveyIDs.") ";
			}
			//@JAPR 2015-05-14: Corregido un bug, si no se tenía acceso a ninguna forma, al no aplicar filtro por ella se estaban devolviendo
			//todos los catálogos y eso llevaba además a datos innecesarios, posibles errores con eBavel, se validará que se aplique un filtro
			//inválido a menos que se hubiera pedido un catálogo específico (para permitir funcionar como Web Service) (#P1WYWM)
			else {
				if ($arrCatalogIDs == '') {
					$sql .= "AND A.SurveyID IN (-1) ";
				}
			}
			//@JAPR
		}
		//@JAPR 2014-08-26: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		//Había faltado considerar los perfiles en la obtención de las definiciones, porque aunque se excluyeran al obtener las versiones, al no venir
		//dentro del parámetro $arrXcldSurveyIDs si se estaban regresando como definición al dispositivo
		if ($strInvalidSurveys != '') {
			$sql .= "AND A.SurveyID NOT IN (".$strInvalidSurveys.") ";
		}
		if ($arrCatalogIDs != '') {
		  $sql .= "AND A.CatalogID IN (".$arrCatalogIDs.") ";
		}
		if ($arrXcldCatalogIDs != '') {
		  $sql .= "AND A.CatalogID NOT IN (".$arrXcldCatalogIDs.") ";
		}
		$sql .= " 
			UNION 
				SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Survey A 
				WHERE A.CatalogID > 0 ";
		//@JAPR 2015-04-09: Corregido un bug, si se pedía sólo una forma o no se tenía acceso a ninguna, de todas formas traía la información de todos los catálogos
		//de todas las formas a menos que se pidiera exclusivamente un catálogo
		//Si se solicitó una Forma directa, entonces se aplica el filtro por ella en lugar de por todas las formas posible a las que se tiene acceso
		if ($strRequestedSurveyIDs && $strRequestedSurveyIDs != -1) {
			$sql .= "AND A.SurveyID IN (".$strRequestedSurveyIDs.") ";
		}
		else {
			if ($strAllSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strAllSurveyIDs.") ";
			}
			//@JAPR 2015-05-14: Corregido un bug, si no se tenía acceso a ninguna forma, al no aplicar filtro por ella se estaban devolviendo
			//todos los catálogos y eso llevaba además a datos innecesarios, posibles errores con eBavel, se validará que se aplique un filtro
			//inválido a menos que se hubiera pedido un catálogo específico (para permitir funcionar como Web Service) (#P1WYWM)
			else {
				if ($arrCatalogIDs == '') {
					$sql .= "AND A.SurveyID IN (-1) ";
				}
			}
			//@JAPR
		}
		//@JAPR 2014-08-26: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		//Había faltado considerar los perfiles en la obtención de las definiciones, porque aunque se excluyeran al obtener las versiones, al no venir
		//dentro del parámetro $arrXcldSurveyIDs si se estaban regresando como definición al dispositivo
		if ($strInvalidSurveys != '') {
			$sql .= "AND A.SurveyID NOT IN (".$strInvalidSurveys.") ";
		}
		if ($arrCatalogIDs != '') {
		  $sql .= "AND A.CatalogID IN (".$arrCatalogIDs.") ";
		}
		if ($arrXcldCatalogIDs != '') {
		  $sql .= "AND A.CatalogID NOT IN (".$arrXcldCatalogIDs.") ";
		}
		$arrCatalogIDs = array();
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n(getEFormsDefinitions) Query para catálogos: {$sql}");
		}
		$recordset = $aRepository->DataADOConnection->Execute($sql);
		if ($recordset === false) {
			$return['error'] = true;
			$return['errmsg'] = 'Error getting catalog definitions';
		}
		else {
			while (!$recordset->EOF) {
				$catalogID = (int) $recordset->fields["catalogid"];
				$catalogList[$catalogID] = $catalogID;
				$recordset->MoveNext();
			}
		}
		//@JAPR 2012-09-12: Faltaba asignar el array ordenado de catálogos
		if (count($catalogList) > 0) {
			$arrCatalogIDs = array_keys($catalogList);
		}
	}
    //@JAPR
    
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['agendas']) ) {
		if (!$gbDesignMode) {
			//Obtiene el listado de agendas disponibles para el usuario logueado
			//$sql = "SELECT A.AgendaID, A.FilterText AS AgendaName, A.VersionNum 
			//	FROM SI_SV_SurveyAgenda A 
			//	WHERE A.UserID = " . $intUserID." ";
			//Conchita 2013-01-22 Agregado el filtrado para que solo se traiga agendas disponibles para contestar y no todas
			//@JAPR 2014-11-20: Modificado para utilizar sintaxis ANSI y para considerar a todas las agendas incluso si sólo tienen forma de CheckIn
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT DISTINCT A.AgendaID, A.FilterText AS AgendaName, A.VersionNum 
				FROM SI_SV_SurveyAgenda A 
					LEFT OUTER JOIN SI_SV_SurveyAgendaDet B ON A.AgendaID = B.AgendaID 
				WHERE A.UserID = " . $intUserID." ";
			$sql.= " AND 
				/*(B.Status IS NULL OR B.Status = 0) AND*/ A.AgendaID NOT IN ( 
					SELECT DISTINCT AgendaID 
					FROM SI_SV_SurveyAgendaDet 
					WHERE Status = 2)";
			if ($arrAgendaIDs != '') {
			  $sql .= " AND A.AgendaID IN (".$arrAgendaIDs.") ";
			}
			if ($arrXcldAgendaIDs != '') {
			  $sql .= " AND A.AgendaID NOT IN (".$arrXcldAgendaIDs.") ";
			}
			if ($appVersion >= esvAgendaRedesign) {
				$sql .= " AND A.CaptureStartDate = DATE(NOW()) ";
			}
			$sql .= "ORDER BY A.CaptureStartDate, A.CaptureStartTime";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitions) Query para agendas: {$sql}");
			}
			$arrAgendaIDs = array();
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting agenda definitions';
			}
			else {
				while (!$recordset->EOF) {
					$agendaID = (int) $recordset->fields["agendaid"];
					$agendaList[$agendaID] = $agendaID;
					$recordset->MoveNext();
				}
			}
			//@JAPR 2012-09-12: Faltaba asignar el array ordenado de agendas
			if (count($agendaList) > 0) {
				$arrAgendaIDs = array_keys($agendaList);
			}
		}
		else {
			//Se debe reasignar este array en este punto, ya que si entrara por el if en realidad es un string con un IN y en el propio if se convierte a array
			$arrAgendaIDs = array();
		}
	}
    //@JAPR
	
	//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
	//Array con la colección de encuestas que utilizan catálogos
	$arrSurveysByCatalogID = array();
	//Array con los catálogos que habiendo sido utilizados por una encuesta, no contienen filtros dinámicos en alguna de ella, por lo tanto
	//si tienen que regresar sus datos ya que en al menos un posible caso se usarán estáticos
	$arrCatalogsUsedWithNoDynFilter = array();
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['catalogs']) ) {
		//@JAPR 2014-06-16: Movido a este sitio porque durante la obtención de definiciones de los catálogos no siempre se contará con las instancias
		//de encuestas, así que debe obtener la información de filtros dinámicos directamente con un query
		//Recorre la lista de encuestas a las que tiene acceso el usuario pero que no es la lista de encuestas a regresar en las definiciones (ya que esa
		//está condicionada exclusivamente a lo que pidieron en el request), con esto se obtendrán encuesta a encuesta los catálogos usados por ellas
		//para aplicar la validación
		$arrSurveysWithDynamicFilters = array();
		$sql = "SELECT DISTINCT SurveyID, CatalogID FROM SI_SV_SurveyFilter";
		$recordset = $aRepository->DataADOConnection->Execute($sql);
		if ($recordset) {
			while (!$recordset->EOF) {
				$intSurveyID = (int) @$recordset->fields["surveyid"];
				$intCatalogID = (int) @$recordset->fields["catalogid"];
				if (!isset($arrSurveysWithDynamicFilters[$intSurveyID])) {
					$arrSurveysWithDynamicFilters[$intSurveyID] = array();
				}
				
				$arrSurveysWithDynamicFilters[$intSurveyID][$intCatalogID] = $intCatalogID;
				$recordset->MoveNext();
			}
		}
		
		//Sólo aplica la validación sobre las encuestas que tienen filtros dinámicos, las que no y que aún así utilicen catálogos, esos no se incluirán
		//en $arrSurveysByCatalogID así que mas adelante se obtendrán sus valores
		foreach ($arrAllSurveyIDs as $intSurveyID) {
			$objSurvey = BITAMSurvey::NewInstance($aRepository);
			$objSurvey->SurveyID = $intSurveyID;
			if (!isset($arrSurveysWithDynamicFilters[$intSurveyID])) {
				//@JAPR 2014-10-01: Corregido un bug, estaba mal esta validación porque $arrSurveysByCatalogID sólo contiene las encuestas por catálogo
				//donde si tengan filtro dinámico, así que si no había ninguno era correcto preguntar de esta manera, pero si había por lo menos alguna
				//entonces dependía de que el catálogo estuviera en $arrCatalogsUsedWithNoDynFilter, pero esa otra variable sólo se llenaba en el mismo
				//ciclo que la primera bajo la misma condición, así que nunca hubiera sido posible
				//En este caso, tiene que recorrer los catálogos de esta encuesta a pesar de que no tiene filtros dinámicos, y simplemente agregar
				//dichos catálogos al array de aquellos que precisamente deberán usar datos estáticos
				$arrUsedCatalogIDs = $objSurvey->getUsedCatalogsIDs();
				foreach ($arrUsedCatalogIDs as $intCatalogID) {
					if (!isset($arrCatalogsUsedWithNoDynFilter[$intCatalogID])) {
						$arrCatalogsUsedWithNoDynFilter[$intCatalogID] = $intCatalogID;
					}
				}
				continue;
			}
			
			$arrCatalogsWithDynamicFilters = $arrSurveysWithDynamicFilters[$intSurveyID];
			$arrUsedCatalogIDs = $objSurvey->getUsedCatalogsIDs();
			foreach ($arrUsedCatalogIDs as $intCatalogID) {
				if (isset($arrCatalogsUsedWithNoDynFilter[$intCatalogID])) {
					//Este catálogo ya se identificó como estático, así que no tiene caso continuar con la validación
					continue;
				}
				
				//Agrega la encuesta a la lista del catálogo y verifica si tiene filtros dinámicos para ella
				if (!isset($arrSurveysByCatalogID[$intCatalogID])) {
					$arrSurveysByCatalogID[$intCatalogID] = array();
				}
				$arrSurveysByCatalogID[$intCatalogID][$objSurvey->SurveyID] = $objSurvey->SurveyID;
				if (!isset($arrCatalogsWithDynamicFilters[$intCatalogID])) {
					//En este caso esta encuesta no tiene filtros dinámicos o no para este catálogo, por lo tanto se marca como un catálogo que
					//debe regresar datos estáticos
					if (!isset($arrCatalogsUsedWithNoDynFilter[$intCatalogID])) {
						$arrCatalogsUsedWithNoDynFilter[$intCatalogID] = $intCatalogID;
					}
				}
			}
		}
	}
	//@JAPR
	
  	//Obtiene el listado de definiciones de encuestas disponibles para el usuario logueado
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['surveys']) ) {
		if (count($surveyList) > 0) {
			//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Carga masiva de la forma y todos los elementos que dependen de ella para que se obtengan sus instancias mediante referencia
			$objSurveyColl = BITAMSurveyCollection::NewInstanceFromDBFull($aRepository, array_keys($surveyList));
			//$objSurveyColl = BITAMSurveyCollection::NewInstance($aRepository, array_keys($surveyList));
			//@JAPR
			$surveyList = array();
			
			foreach ($objSurveyColl as $objSurvey) {
				$surveyList[$objSurvey->SurveyID] = $objSurvey->getJSonDefinition();
			}
		}
	}
	
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['catalogs']) ) {
		if (count($catalogList) > 0) {
			//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Carga masiva de los catálogos y todos los elementos que dependen de ellos para que se obtengan sus instancias mediante referencia
			//@JAPR 2018-10-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
			$objCatalogColl = BITAMCatalogCollection::NewInstanceFromDBFull($aRepository, array_keys($catalogList));
			//$objCatalogColl = BITAMCatalogCollection::NewInstance($aRepository, array_keys($catalogList));
			//@JAPR
			$catalogList = array();
			
			foreach ($objCatalogColl as $objCatalog) {
				//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
				//@JAPR 2014-10-01: Corregido un bug, estaba mal esta validación porque $arrSurveysByCatalogID sólo contiene las encuestas por catálogo
				//donde si tengan filtro dinámico, así que si no había ninguno era correcto preguntar de esta manera, pero si había por lo menos alguna
				//entonces dependía de que el catálogo estuviera en $arrCatalogsUsedWithNoDynFilter, pero esa otra variable sólo se llenaba en el mismo
				//ciclo que la primera bajo la misma condición, así que nunca hubiera sido posible
				//@JAPR 2015-09-30: Validado que si se encuentra en modo diseño, siempre traiga los valores de los catálogos sin importar si tiene o no filtro dinámico
				if ($gbDesignMode) {
					$blnGetValues = true;
					//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
					if ( isset($oDataToGet['skipCatalogValues']) ) {
						//Este parámetro originalmente no debía aplicar para modo diseño, sin embargo se determinó que hay 2 puntos que pudieran cargar las definiciones en dicho modo,
						//uno de ellos es el preview donde SI se pueden necesitar los valores de catálogos, pero el otro es para las ventanas de diseño donde NO tiene sentido cargar los
						//valores de los catálogos porque no se utilizan, así que se habilitó el uso de este parámetro para ese request específicamente, que debería ser el único de los
						//dos casos donde siendo modo de diseño llegaría a este método
						$blnGetValues = false;
					}
					//@JAPR
				}
				else {
					$blnGetValues = !isset($arrSurveysByCatalogID[$objCatalog->CatalogID]) || isset($arrCatalogsUsedWithNoDynFilter[$objCatalog->CatalogID]);
					//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
					//Agregado el control de información a obtener mediante parámetro
					if ( isset($oDataToGet['skipCatalogValues']) ) {
						//Si se recibe este parámetro se forzará a no cargar los valores de los catálogos. Originalmente usado para generar la base de datos de SQLLite
						//para descargar a dispositivos, en esos casos no tenía sentido cargar los valores en este punto y posteriormente al procesar la base de datos,
						//ya que de este proceso únicamente se necesitaban las definiciones de catálogos
						$blnGetValues = false;
					}
				}
				//@JAPR
				if (!$blnGetValues && getParamValue('DebugBreak', 'both', '(int)', true)) {
					if ( isset($oDataToGet['skipCatalogValues']) ) {
						//En este caso simplemente imprime la información de los catálogos identificados con filtros dinámicos, ya que en ningún caso se obtendrán
						//los valores de los catálogos regresados
					}
					else {
						echo("<br>\r\nThis catalog always has dynamic filters applied, its definition won't include its attribute values: {$objCatalog->CatalogID}");
					}
				}
				$catalogList[$objCatalog->CatalogID] = $objCatalog->getJSonDefinition($blnGetValues);
				//@JAPR
			}
		}
	}

	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['agendas']) ) {
		if (count($agendaList) > 0) {
			$objAgendaColl = BITAMSurveyAgendaCollection::NewInstance($aRepository, array_keys($agendaList));
			$agendaList = array();
			
			foreach ($objAgendaColl as $objAgenda) {
				$agendaList[$objAgenda->AgendaID] = $objAgenda->getJSonDefinition();
			}
		}
		
		//statusagendalist
		//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
		if (!$gbDesignMode && $appVersion >= esvAgendaRedesign) {
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT B.AgendaID, B.SurveyID, A.FilterText AS AgendaName, A.EntryCheckInStartDate, A.VersionNum, B.status 
				FROM SI_SV_SurveyAgenda A, SI_SV_SurveyAgendaDet B WHERE A.AgendaID = B.AgendaID 
				AND A.UserID = " . $intUserID . " AND A.CaptureStartDate = DATE(NOW())";
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting status agenda list';
			} else {
				$arrObjAgendas = array();
				while (!$recordset->EOF) {
					$agendaID = (int) $recordset->fields["agendaid"];
					if (!isset($arrObjAgendas[$agendaID])) {
						$arrObjAgendas[$agendaID] = BITAMSurveyAgenda::NewInstanceWithID($aRepository, $agendaID);
					}
					if (!isset($statusAgendaList[$agendaID])) {
						$statusAgendaList[$agendaID] = array();
						$agendaName = (string) $recordset->fields["agendaname"];
						$versionNum = (int) $recordset->fields["versionnum"];
						$statusAgendaList[$agendaID]['id'] = $agendaID;
						$statusAgendaList[$agendaID]['name'] = $agendaName;
						$statusAgendaList[$agendaID]['version'] = $versionNum;
						$statusAgendaList[$agendaID]['status'] = $arrObjAgendas[$agendaID]->Status;
						$statusAgendaList[$agendaID]['checkInSurveyStatus'] = 0;
						foreach ($arrObjAgendas[$agendaID]->SurveyIDs as $agendaSurveyID) {
							$statusAgendaList[$agendaID]['surveys'][$agendaSurveyID] = 0;
						}
					}
					$asurveyid = (int) $recordset->fields["surveyid"];
					$asurveystatus = (int) $recordset->fields["status"];
					if (!is_null($recordset->fields["entrycheckinstartdate"])) {
						$statusAgendaList[$agendaID]['checkInSurveyStatus'] = 1;
					}
					if (isset($statusAgendaList[$agendaID]['surveys'][$asurveyid])) {
						$statusAgendaList[$agendaID]['surveys'][$asurveyid] = $asurveystatus;
					}
					$recordset->MoveNext();
				}
			}
			$return['statusAgendaList'] = $statusAgendaList;
		}
	}
	//@JAPR
	
	//Objetos globales que pueden ser usados indistintamente por cualquier encuesta
	
	//Obtiene la colección de categorías
	$categoryList = array();
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//Desde el inicio de eForms v6 ya no existe la pregunta tipo acción, así que no tiene caso continuar cargando esta información, por tanto se agregará "false" para
	//impedir que se siga cargando
	if (false && !$gbDesignMode && !$intXcldCategories) {
		$arrItems = BITAMCatalog::GetCategories($aRepository, false, true);
		//En la versión 4 los índices son asociativos
		foreach ($arrItems as $anItem) {
			$categoryList[$anItem['categoryID']] = $anItem['categoryName'];
		}
	}
	
	//Obtiene la colección de usuarios
	$userList = array();
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//Desde el inicio de eForms v6 ya no existe la pregunta tipo acción, así que no tiene caso continuar cargando esta información, por tanto se agregará "false" para
	//impedir que se siga cargando
	if (false && !$gbDesignMode && !$intXcldUsers) {
		$arrItems = BITAMCatalog::GetUsers($aRepository, false, true);
		//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
		//@JAPR 2013-05-06: Agregada la opción para enviara las acciones a ciertos usuarios especiales
		//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
		//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
		$blnUseMaxLongDummies = false;
		//@JAPR 2013-05-07: Pospuesto este cambio porque eBavel dejó de manejar las acciones como antes así que ya no funcionaba lo que hacía eForms
		//@JAPR 2013-07-09: Habilitado nuevamente este cambio porque ya se soportan acciones de eBavel otra vez
		if ($appVersion >= esvMultiDynamicAndStandarization && $appVersion < esveBavelSuppActions123) {
			//En este caso el App no soportaba valores negativos, por tanto se forzará a usar los IDs cercanos al Long como Supervisores y MySelf,
			//sin embargo primero se valida si la BD tiene o no algún usuario registrado con dicho ID, ya que si lo tiene, no puede basarse en este
			//rango para valores dummy
			$intMaxUserID = 0;
			$sql = "SELECT MAX(CLA_USUARIO) AS CLA_USUARIO FROM SI_USUARIO";
		    $aRS = $aRepository->ADOConnection->Execute($sql);
		    if ($aRS && !$aRS->EOF) {
	        	$intMaxUserID = (int) @$aRS->fields["cla_usuario"];
			}
			
			$blnUseMaxLongDummies = ($intMaxUserID < MAX_LONG -3);
		}
		else {
			//En este caso la nueva versión del App entiende perfectamente los IDs <= 0 como dummies y los respeta
		}
		
		//@JAPR 2013-07-15: Agregado el mapeo de responsables supervisores 1 al 3 y MySelf desde el servicio, ahora los valores negativos mayores
		//del -255 son válidos, por lo que se cambió el valor default de no asignado a -255, sin embargo para la pregunta anteriormente las
		//definiciones grababan un -1 como no asignado para el responsable, pero como ese valor corresponde al SUPERVISOR 1, se tendrá que
		//sumar uno al valor real grabado en la definición para representar al elemento correcto si los valores grabados son negativos, en
		//otras palabras, mientras el server usará -1, -2 y -3 para los SUPERVISOR 1, 2 y 3 respectivamente, en la definición del App se
		//grabarán -2, -3 y -4 respectivamente porque -1 ya era usado como el anterior valor de no asignado del QuestionCls.ResponsibleID y
		//por tanto todas las definiciones actuales ya traerían dicho valor
		$intV4DummyValue = 0;
		if ($appVersion >= esveBavelSuppActions123) {
			$intV4DummyValue = -1;
		}
		
		$intStartingId = 0;
		if ($blnUseMaxLongDummies) {
			$intStartingId = MAX_LONG;
		}
		$userList[(string) ($intStartingId - 0)] = '('.translate("Myself").')';								//En version >= esveBavelSuppActions123 Se manda 0
		$userList[(string) ($intStartingId - 1 + $intV4DummyValue)] = '('.translate("Supervisor").' 1)';	//En version >= esveBavelSuppActions123 Realmente se manda -2
		$userList[(string) ($intStartingId - 2 + $intV4DummyValue)] = '('.translate("Supervisor").' 2)';	//En version >= esveBavelSuppActions123 Realmente se manda -3
		$userList[(string) ($intStartingId - 3 + $intV4DummyValue)] = '('.translate("Supervisor").' 3)';	//En version >= esveBavelSuppActions123 Realmente se manda -4
		//@JAPR
		
		//En la versión 4 los índices son asociativos
		foreach ($arrItems as $anItem) {
			$userList[$anItem['userID']] = $anItem['userName'];
		}
	}
	
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//Cuando se trata de la captura en línea de una forma específica, no es necesario cargar los menús agrupadores de formas, por tanto se condicionará para no
	//realizar este proceso en tales situaciones
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['menus']) ) {
		if (!$intManualLogin && !$gbDesignMode && getMDVersion() >= esvSurveyMenu) {
			//@JAPR 2015-09-15: Corregido un bug, siempre estaba regresando las definiciones de menús aunque explícitamente no se hubieran pedido, ahora tomará en cuenta el parámetro
			$surveyMenuCollection = BITAMSurveyMenuCollection::NewInstance($aRepository, $arrMenuIDs);
			//@JAPR 2015-12-24: Corregido un bug, como se reutilizó $arrMenuIDs para menuOrder, se debe resetear o de lo contrario si no hubiera menús quedaría con
			//el valor igual al parámetro recibido porque no entraría al if/for de abajo
			$arrMenuIDs = array();
			$arrAllSurveyIDsByID = array_flip($arrAllSurveyIDs);
			if (!is_null($surveyMenuCollection)) {
				foreach($surveyMenuCollection as $aSurveyMenu) {
					//@JAPR 2015-12-24: Corregido un bug, no estaba validando si el menú realmente contenía formas a las que el usuario tenía acceso (#M8LIJE)
					//Verifica que por lo menos uno de los Surveys de este menú se encuentre entre la lista de Surveys a los que tiene acceso el usuario
					$blnValid = false;
					foreach ($aSurveyMenu->SurveyIDs as $intSurveyID) {
						//Si la forma se encuentra entre a las que tiene acceso este usuario y no está marcada como inválida, entonces es un menú válido
						if (isset($arrAllSurveyIDsByID[$intSurveyID]) && !isset($arrInvalidSurveys[$intSurveyID])) {
							$blnValid = true;
							break;
						}
					}
					
					if (!$blnValid) {
						continue;
					}
					//@JAPR
					
					$menuList[$aSurveyMenu->MenuID] = $aSurveyMenu->getJSonDefinition($aRepository);
				}
				if (count($menuList) > 0) {
					$arrMenuIDs = array_keys($menuList);
				}
			}
			
			$return['menuList'] = $menuList;
			$return['menuOrder'] = $arrMenuIDs;
		}
	}
	
	//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//Cuando se trata de la captura en línea de una forma específica y este es el request para cargar la definición de la forma, se agregará la carga de las
	//configuraciones globales ya que originalmente se habría cargado en el request para obtener las versioens de definiciones, mismo que ya no se ejecutará en estos
	//casos para evitar que tarde demasiado con ese proceso que no es necesario para la captura individual de una forma
	//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['settings']) ) {
		if ( $intManualLogin && $strRequestedSurveyIDs && $strRequestedSurveyIDs != -1 ) {
			//@JAPR 2018-07-13: Optimizada la carga de eForms (#G87C4W)
			$return['settings'] = getEFormsSettings($aRepository, $intUserID, $arrSurveyIDs);
		}
	}
	
    //$return['error'] = false;
    $return['surveyList'] = $surveyList;
	$return['catalogList'] = $catalogList;
	$return['agendaList'] = $agendaList;
    $return['surveyOrder'] = $arrSurveyIDs;
	$return['catalogOrder'] = $arrCatalogIDs;
	$return['agendaOrder'] = $arrAgendaIDs;
	$return['categoryList'] = $categoryList;
	$return['userList'] = $userList;
    return $return;
}

/* Obtiene el JSon de la lista de todas las encuestas, agendas, catálogos y demás con la versión actual para que el App pueda determinar si debe o
no actualizar sus definiciones
//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
Agregado el parámetro $aUserID para controlar sobre que usuario se obtiene la información, si no se envía se usará el usuario logeado en la sesión
Agregado el parámetro $oDataToGet para controlar que tipo de información se desea recuperar, si no se manda o se manda vacío, se asumirá que se desea obtener toda
la información que esta función es capaz de cargar
*/
function getEFormsDefinitionsVersions($aRepository, $aParamsColl = null, $aUserID = -1, $oDataToGet = null) {
	global $appVersion;
	global $kpiUser;
	//@JAPR 2014-03-18: Agregado el icono para las encuestas
	global $blnWebMode;
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	$intUserID = $aUserID;
	if ( $aUserID == -1 ) {
		$intUserID = $_SESSION["PABITAM_UserID"];
	}
	
	$blnGetAllData = false;
	if ( is_null($oDataToGet) || !is_array($oDataToGet) || count($oDataToGet) == 0 ) {
		$oDataToGet = array();
		$blnGetAllData = true;
	}
	
	//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
	if (is_null($aParamsColl) || !is_array($aParamsColl)) {
		$aParamsColl = array();
	}
	//@JAPR
	
	if (!isset($blnWebMode)) {
		$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	}
	
	//JAPR 2015-04-27: Agregado el grabado local de las actualizaciones
	global $appUpdateVersion;
	if (!isset($appUpdateVersion)) {
		$appUpdateVersion = getParamValue('updateVersion', 'both', '(int)', false);
	}
	
	//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
	global $gbDesignMode;
	//@JAPR
	
    $surveyList = array();
	$catalogList = array();
	$agendaList = array();
	if (getMDVersion() >= esvSurveyMenu) {
		require_once('surveyMenu.inc.php');
		$menuList = array();
	}
    $activeStatus = array(0 => 0, 1 => 1);
	$return['error'] = false;
	
    require_once('survey.inc.php');
    require_once('section.inc.php');
    require_once('question.inc.php');
    require_once('catalog.inc.php');
	//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
	require_once("surveyProfile.inc.php");
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
    $roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $intUserID);
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['surveys']) ) {
		//@JAPR 2014-03-18: Agregado el icono para las encuestas
		$strAdditionalFields = '';
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$strAdditionalFields .= ', A.SurveyImageText';
		}
		//@JAPR 2015-07-20: Agregado el soporte para imgenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
		if (getMDVersion() >= esveFormsv6) {
			$strAdditionalFields .= ', A.CreationVersion';
		}
		//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$strAdditionalFields .= ', A.Width, A.Height, A.TextPosition, A.TextSize, A.CustomLayout';
		}
		//@JAPR
		
		//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
		$arrInvalidSurveys = array();
		$theAppUser = null;
		if (getMDVersion() >= esvSurveyProfiles) {
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $intUserID);
			if (!is_null($theAppUser)) {
				$intSurveyProfileID = (int) @$theAppUser->ProfileID;
				if ($intSurveyProfileID > 0) {
					$arrInvalidSurveys = BITAMSurveyProfile::GetInvalidSurveys($aRepository, $intSurveyProfileID);
					if (is_null($arrInvalidSurveys) || !is_array($arrInvalidSurveys)) {
						$arrInvalidSurveys = array();
					}
				}
				
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n Encuestas inválidas para el perfil");
					PrintMultiArray($arrInvalidSurveys);
					echo("<br>\r\n");
				}
			}
		}
		
		//@JAPR 2015-04-20: Rediseñado el Admin con DXHTML
		//Si se está en modo de diseño entonces no se debe hacer join con los Shchedulers para obtener las definiciones
		//@JAPR 2015-07-20: Validado que no se descarguen al App definiciones que no corresponden con la versión 6 o posterior si el App es de dicha versión
		$strPostV6ObjectFilter = "";
		if ($appVersion >= esveFormsv6) {
			$strPostV6ObjectFilter = " AND A.CreationVersion >= ".esveFormsv6." ";
		}
		
		//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
		//Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
		$arrSurveyIDs = array();
		//@JAPR
		if (!$gbDesignMode) {
			//@JAPR 2015-09-21: Corregido un bug, si se descargan formas vacías provocarán error por dejar la bandera de carga activada cuando no puedan redireccionar a la forma (#6HX3VT)
			//Se agrega el join para sólo obtener formas que si tienen secciones
			//Obtener el listado de encuestas disponibles para el usuario logueado x user
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum $strAdditionalFields 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter;
			
			if (count($roles) > 0) {
				//Obtener el listado de encuestas disponibles para el usuario logueado x rol
				//Se agrega el join para sólo obtener formas que si tienen secciones
				$sql .= " UNION 
					SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum $strAdditionalFields 
					FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C
					WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND C.RolID IN (" . implode(",", $roles) . ") 
						AND A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter;
			}
			
			if (getMDVersion() >= esvAgendaRedesign) {
				//Se agrega el join para sólo obtener formas que si tienen secciones
				//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
				$sql .= " UNION 
				SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum $strAdditionalFields
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgenda B 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.CheckinSurvey AND B.UserID = " . $intUserID . "
				AND A.Status IN (" . implode(",", $activeStatus) . ") AND B.AgendaID NOT IN ( 
					SELECT DISTINCT AgendaID 
					FROM SI_SV_SurveyAgendaDet 
					WHERE STATUS = 2)".$strPostV6ObjectFilter;
			}
			
			//Obtiene el listado de encuestas disponibles para el usuario logeado x Agendas
			//Se agrega el join para sólo obtener formas que si tienen secciones
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			$sql .= " UNION 
				SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum $strAdditionalFields 
				FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
				WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID AND C.UserID = " . $intUserID . " 
					AND A.Status IN (" . implode(",", $activeStatus) . ") AND C.AgendaID NOT IN ( 
						SELECT DISTINCT AgendaID 
						FROM SI_SV_SurveyAgendaDet 
						WHERE Status = 2) {$strPostV6ObjectFilter} 
				ORDER BY 2";
			//@JAPR
		}
		else {
			//@JAPR 2016-07-06: Rediseñado el Admin con DXHTML
			//Agregado el parámetro $aParamsColl para indicar cuales de los valores que se hubieran tomado del request serán tomados de manera directa de dicho array (#XWOMIF)
			$strSurveysFilter = '';
			if (isset($aParamsColl['Survey'])) {
				$arrSurveyIDs = $aParamsColl['Survey'];
			}
			if (is_null($arrSurveyIDs) || (!is_array($arrSurveyIDs) && (int)$arrSurveyIDs == 0)) {
				$arrSurveyIDs = '';
			}
			else {
				$arrSurveyIDs = (is_array($arrSurveyIDs)? implode(',', $arrSurveyIDs) : (int) $arrSurveyIDs);
			}
			$arrSurveyIDs = trim(str_replace("\'", "", $arrSurveyIDs));
			if ($arrSurveyIDs != '') {
				$strSurveysFilter = " AND A.SurveyID IN ({$arrSurveyIDs}) ";
			}
			
			$sql = "SELECT DISTINCT A.SurveyID, A.SurveyName, A.VersionNum $strAdditionalFields 
					FROM SI_SV_Survey A 
					WHERE A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter.$strSurveysFilter;
		}
		//@JAPR
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n(getEFormsDefinitionsVersions) Query para encuestas: {$sql}");
		}
		$recordset = $aRepository->DataADOConnection->Execute($sql);
		if ($recordset === false) {
			$return['error'] = true;
			$return['errmsg'] = 'Error getting survey list';
		}
		else {
			while (!$recordset->EOF) {
				$surveyID = (int) $recordset->fields["surveyid"];
				//@JAPR 2014-07-10: Agregados los perfiles por usuario para limitar el acceso a las encuestas según reglas basadas en la definición de las mismas
				if (!isset($surveyList[$surveyID]) && !isset($arrInvalidSurveys[$surveyID]))
				{
					$surveyName = (string) $recordset->fields["surveyname"];
					$versionNum = (int) $recordset->fields["versionnum"];
					$creationVersion = (int) @$recordset->fields["creationversion"];
					$surveyList[$surveyID]['id'] = $surveyID;
					$surveyList[$surveyID]['name'] = $surveyName;
					$surveyList[$surveyID]['version'] = $versionNum;
					//@JAPR 2014-03-18: Agregado el icono para las encuestas
					$strDisplayImage = trim((string) $recordset->fields["surveyimagetext"]);
					if ($strDisplayImage != '') {
						$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
						if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
							$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
						}
						elseif (!$blnWebMode) {
							//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
							//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
							$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($aRepository, $strDisplayImage, true, false);
							//@JAPR
						}
						else {
							//@JAPR 2015-07-19: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
							//JAPR 2015-08-04: Modificada para utilizar DHTMLX
							if ($creationVersion >= esveFormsv6 && $appVersion < esveFormsDHTMLX) {
								$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
							}
							//@JAPR
						}
					}
					//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
					$surveyList[$surveyID]['displayImage'] = $strDisplayImage;
					//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
					if (getMDVersion() >= esvFormsLayouts) {
						$surveyList[$surveyID]['width'] = (int) @$recordset->fields["width"];
						$surveyList[$surveyID]['height'] = (int) @$recordset->fields["height"];
						$surveyList[$surveyID]['textPos'] = (int) @$recordset->fields["textposition"];
						//@JAPR 2016-02-11: Corregido un bug, el componente de captura estaba grabando espacio cuando se dejaba vacío, así que se limpiarán los espacios (#FSMUZC)
						$surveyList[$surveyID]['textSize'] = trim((string) @$recordset->fields["textsize"]);
						$surveyList[$surveyID]['customLayout'] = trim((string) @$recordset->fields["customlayout"]);
					}
					//@JAPR
				}
				$recordset->MoveNext();
			}
		}
	}
	//@JAPR
	
    //Lista de IDs de las encuestas a las que tiene acceso el usuario solicitado
    //@JAPRWarning: Validar que si el array es idéntico al total de encuestas, mejor se envíe vacio el String para no aplicar filtro
    $strSurveyIDs = '';
    $arrSurveyIDs = array();
    if (count($surveyList) > 0) {
	    $arrSurveyIDs = array_keys($surveyList);
		$strSurveyIDs = implode(',', $arrSurveyIDs);
    }
    
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['catalogs']) ) {
		//Obtiene el listado de los catálogos que son usados por preguntas en las encuestas a las que se tiene acceso
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		$arrCatalogIDs = array();
		if ($strSurveyIDs != '') {
			$sql = "SELECT DISTINCT A.CatalogID 
					FROM SI_SV_Question A 
					WHERE A.CatalogID > 0 ";
			if ($strSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strSurveyIDs.") ";
			}
			$sql .= " UNION 
					SELECT DISTINCT A.CatalogID 
					FROM SI_SV_Section A 
					WHERE A.CatalogID > 0 ";
			if ($strSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strSurveyIDs.") ";
			}
			$sql .= " UNION 
					SELECT DISTINCT A.CatalogID 
					FROM SI_SV_Survey A 
					WHERE A.CatalogID > 0 ";
			if ($strSurveyIDs != '') {
				$sql .= "AND A.SurveyID IN (".$strSurveyIDs.")";
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitionsVersions) Query para catálogos: {$sql}");
			}
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting catalog list';
			}
			else {
				while (!$recordset->EOF) {
					$catalogID = (int) $recordset->fields["catalogid"];
					$arrCatalogIDs[$catalogID] = $catalogID;
					$recordset->MoveNext();
				}
			}
		}
		
		$strCatalogIDs = '';
		if (count($arrCatalogIDs) > 0)
		{
			$arrCatalogIDs = array_keys($arrCatalogIDs);
			$strCatalogIDs = implode(',', $arrCatalogIDs);
			//@JAPR 2014-08-27: Agregada la validación de actualización para catálogos mapeados a eBavel
			$strAdditionalFields = '';
			if (getMDVersion() >= esveBavelCatalogs) {
				$strAdditionalFields .= ', eBavelAppID, eBavelFormID';
			}
			
			$sql = "SELECT A.CatalogID, A.CatalogName, A.VersionNum $strAdditionalFields 
				FROM SI_SV_Catalog A 
				WHERE A.CatalogID IN (".$strCatalogIDs.")";
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting catalog list';
			}
			else {
				while (!$recordset->EOF) {
					$catalogID = (int) $recordset->fields["catalogid"];
					$catalogName = (string) $recordset->fields["catalogname"];
					$versionNum = (int) $recordset->fields["versionnum"];
					$inteBavelAppID = (int) @$recordset->fields["ebavelappid"];
					$inteBavelFormID = (int) @$recordset->fields["ebavelformid"];
					$catalogList[$catalogID]['id'] = $catalogID;
					$catalogList[$catalogID]['name'] = $catalogName;
					//@JAPR 2014-08-27: Agregada la validación de actualización para catálogos mapeados a eBavel
					//Si el catálogo está mapeado a eBavel, se debe indicar que se actualice siempre en los dispositivos porque no se sabe cuando han
					//cambiado los valores de la forma de eBavel
					if ($inteBavelAppID > 0 && $inteBavelFormID > 0) {
						$catalogList[$catalogID]['version'] = -1;
					}
					else {
						$catalogList[$catalogID]['version'] = $versionNum;
					}
					//@JAPR
					$recordset->MoveNext();
				}
			}
		}
	}
	//@JAPR

    //2014.12.16 JCEM #O5YTV8 el siguiente codigo valida si aplica el scheduler para esta fecha , si aplica y la agenda no existe la genera
	$dFechaHoy = date('Y-m-d');
	$dHoraHoy = date('H:i');
	$dFechaSig = date('Y-m-d', strtotime($dFechaHoy.' + 1 day'));
	//@JAPR 2018-05-10: Optimizada la carga de eForms (#G87C4W)
	//Comentada la generación automática de agendas durante el login para evitar consumir ese tiempo en este punto y delegar ese proceso exclusivamente al agente
	/*
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
    if (!$gbDesignMode && getMDVersion() >= esvAgendaScheduler) {
        require_once('surveyAgendaScheduler.inc.php');
		//@JAPR 2015-10-26: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
		//Ahora se invocará a esta función envíando el ID del usuario que hizo el request para sólo generar sus Agendas, ya que en este punto no se trata del Agente de eForms
		//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
		BITAMSurveyAgendaSchedulerCollection::GenerateAgendasFromAgendaSchedulersByUser($aRepository, $intUserID);
		//@JAPR
    }
	*/
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['agendas']) ) {
		//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
		if (!$gbDesignMode) {
			//Obtiene el listado de agendas disponibles para el usuario logueado
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			//$sql = "SELECT A.AgendaID, A.FilterText AS AgendaName, A.VersionNum 
			//	FROM SI_SV_SurveyAgenda A 
			//	WHERE A.UserID = " . $intUserID;
			//Conchita 2013-01-22 agregado el filtrado para que solo traiga las agendas disponibles y no todas
			//@JAPR 2014-11-20: Modificado para utilizar sintaxis ANSI y para considerar a todas las agendas incluso si sólo tienen forma de CheckIn
			//JCEM 2014.12.22 Se agrega $dFechaHoy a la consulta para propositos de testing
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT 
						A.AgendaID
						,A.FilterText AS AgendaName
						,A.VersionNum
						,B.status 
					FROM 
						SI_SV_SurveyAgenda A
							LEFT OUTER JOIN SI_SV_SurveyAgendaDet B ON 
								A.AgendaID = B.AgendaID 
					WHERE 
							A.UserID = " . $intUserID. " 
						AND A.AgendaID NOT IN ( SELECT DISTINCT 
													AgendaID 
												FROM 
													SI_SV_SurveyAgendaDet 
												WHERE 
													Status = 2)";	
			//@JAPR 2014-12-23: Corregido un bug, la validación de versión estaba equivocada, se hacía contra una versión previa
			if (getMDVersion() >= esvAgendaScheduler) {
				$sql .= " AND A.CaptureStartDate = CAST('".$dFechaHoy."' AS DATETIME)";
			}
			
			//2014.12.19 JCEM se agrega funcionalidad para mostrar agendas dia siguiente por agendasscheduler
			if (getMDVersion() >= esvAgendaScheduler){
				//union hace distinct x default
				$currentDate = $dFechaHoy . ' ' . $dHoraHoy;
				//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
				$sql .= "\n UNION
								SELECT 
									A.AgendaID
									,A.FilterText AS AgendaName
									,A.VersionNum
									,B.status 
								FROM
									SI_SV_SurveyAgenda A
										INNER JOIN SI_SV_SurveyAgendaScheduler C ON
											A.AgendaSchedID = C.AgendaSchedID 
											AND IFNULL(C.GenerateAgendasNextDayTime, '') != ''
										
										LEFT JOIN SI_SV_SurveyAgendaDet B ON 
											A.AgendaID = B.AgendaID 
								WHERE
									A.UserID = " . $intUserID. "
									AND A.CaptureStartDate = DATE('".$dFechaSig."')
									AND CAST('". $currentDate ."' AS DATETIME) >= CAST(CONCAT(CAST(DATE_FORMAT('".$dFechaHoy."', '%Y-%m-%d') AS CHAR(10)),' ',C.GenerateAgendasNextDayTime) AS DATETIME)";
			}

			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitionsVersions) Query para agendas: {$sql}");
			}
			
			//the exe
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			
			if ($recordset === false) {
				$return['error'] = true;
				$return['errmsg'] = 'Error getting agenda list';
			}
			else {
				while (!$recordset->EOF) {
					$agendaID = (int) $recordset->fields["agendaid"];
					if (!isset($agendaList[$agendaID]))
					{
						$agendaName = (string) $recordset->fields["agendaname"];
						$versionNum = (int) $recordset->fields["versionnum"];
						$agendaList[$agendaID]['id'] = $agendaID;
						$agendaList[$agendaID]['name'] = $agendaName;
						$agendaList[$agendaID]['version'] = $versionNum;
					}
					
					$recordset->MoveNext();
				}
			}
		}
	}
	//@JAPR
	
    //$return['error'] = false;
    $return['surveyList'] = $surveyList;
	$return['catalogList'] = $catalogList;
	$return['agendaList'] = $agendaList;
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['updates']) ) {
		//@JAPR 2013-11-26: Agregado el proceso de Actualización del App enviando código en tags Scripts que serán ejecutados por un Eval al
		//momento de ser leídos por el App durante la descarga de las versiones de definiciones. Este proceso se puede controlar por diferentes
		//factores como por Repositorio, cuenta específica que hace la petición, versión del App, etc.
		//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
		if (!$gbDesignMode && $appVersion >= esvAgendaWithCatValues) {
			//$return['update'] = "alert('Ya me actualice');";
			//@JAPR 2016-03-31: Corregido un bug, cuando se agregó el archivo esurvey_mobile_update.js por parte de LRoux, no se consideró que la variable del código se reinicializa
			//con la lectura de la actualización desde la metadata, así que se estaba limpiando lo que se leía en el archivo
			$intServerUpdateVersion = 0;
			$strUpdateCode = '';
			//@JAPR 2015-10-14: Integrado cambio de LRoux para leer las actualizaciones desde un archivo en el server
			$esurvey_mobile_update = @file_get_contents('esurvey_mobile_update.js');
			if ($esurvey_mobile_update !== false && trim($esurvey_mobile_update) !== '') {
				//@JAPR 2016-03-31: Corregido un bug, cuando se agregó el archivo esurvey_mobile_update.js por parte de LRoux, no se consideró que la variable del código se reinicializa
				//con la lectura de la actualización desde la metadata, así que se estaba limpiando lo que se leía en el archivo
				//$return['update'] = $esurvey_mobile_update;
				//Se asignó el código en la variable en lugar de en el array de regreso, para que en caso de existir una actualización de Metadata (que era la forma correcta de hacerlo)
				//se tome como prioridad el código a actualizar en la metadata en lugar del código del archivo de LRoux
				$strUpdateCode = $esurvey_mobile_update;
				//Se dejará versión 1, pero eso es un error porque no tenemos versiones para archivos, así que se debe modificar manualmente este valor por servicio según las veces que se
				//ha modificado el archivo agregado por LRoux
				$intServerUpdateVersion = 1;
				//@JAPR
			}
			/*$return['update'] = "
			SurveyCls.prototype.openSectionSelector = function(useFirst) {
				var strEval = window.prompt(\"Enter value for evaluation:\");
				if (strEval) {
					var strRes = eval(strEval);
					alert(\"Value: \" + JSON.stringify(strRes));
				}
			}
			";
			*/
				
			//@JAPR 2015-04-27: Agregado el grabado local de las actualizaciones
			/* Con esta versión se controlará cuando el App debe auto-actualizarse basado en el código recibido por el server
			Si la versión enviada updateVersion == -1, quiere decir que el App debe tomar la actualización sin importar que tenga localmente en su archivo de 
			actualizaciones, con este mecanismo se puede  forzar de emergencia a actualizar todas las Apps sin importar lo que pudieran tener localmente.
			Si la versión enviada updateVersion == -2, quiere decir que entonces además de actualizar el posible código que pudiera tener, debe eliminar el archivo de 
			actualizaciones previamente almacenado en forma local, con este mecanismo se puede limpiar el código que se manda a las Apps si ya cumplen con cierta 
			versión que debería tener la corrección nativa.
			Si la versión que manda el server es > que la última que tiene el App, entonces el App descargará la actualización, la aplicará y posteriormente grabará
			el archivo de actualización si no ocurre ningún error. Si la versión del server es <= que la del App, entonces el App no realizará acción alguna y se
			quedará con el último código que tenía localmente actualizado
			La tabla SI_SV_AppUpdate debería contener el código de las últimas actualizaciones aplicadas a las cuentas, así como la versión máxima del App que puede
			aplicar la actualización, esto es, si el App es >= que la versión indicada en esta tabla, entonces ya no se envía la actualización a menos que exista otra
			mayor para cuya versión del App aún aplique, en ese caso, se debe enviar el updateVersion == -1 para que dicha App pueda eliminar sus actualizaciones
			locales previas
			*/
			//@JAPR 2016-03-31: Corregido un bug, cuando se agregó el archivo esurvey_mobile_update.js por parte de LRoux, no se consideró que la variable del código se reinicializa
			//con la lectura de la actualización desde la metadata, así que se estaba limpiando lo que se leía en el archivo
			//$intServerUpdateVersion = 0;
			//$strUpdateCode = '';
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			$sql = "SELECT A.VersionNum, A.MinVersionToUpdate, a.MaxVersionToUpdate, a.Description, A.UpdateCode 
				FROM SI_SV_AppUpdate A 
				WHERE A.UserID = {$intUserID} AND {$appVersion} >= A.MinVersionToUpdate AND 
					(A.MaxVersionToUpdate IS NULL OR {$appVersion} < A.MaxVersionToUpdate) 
				UNION 
				SELECT A.VersionNum, A.MinVersionToUpdate, a.MaxVersionToUpdate, a.Description, A.UpdateCode 
				FROM SI_SV_AppUpdate A 
				WHERE A.UserID = -1 AND {$appVersion} >= A.MinVersionToUpdate AND (A.MaxVersionToUpdate IS NULL OR {$appVersion} < A.MaxVersionToUpdate) 
				UNION 
				SELECT A.VersionNum, A.MinVersionToUpdate, a.MaxVersionToUpdate, a.Description, A.UpdateCode 
				FROM SI_SV_AppUpdate A 
					INNER JOIN SI_SV_AppUpdateUsers B ON A.VersionNum = B.VersionNum 
				WHERE A.UserID = 0 AND B.UserID = {$intUserID} AND {$appVersion} >= A.MinVersionToUpdate AND 
					(A.MaxVersionToUpdate IS NULL OR {$appVersion} < A.MaxVersionToUpdate) 
				ORDER BY 1 DESC";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitionsVersions) Query para actualizaciones: {$sql}");
			}
			$recordset = $aRepository->DataADOConnection->Execute($sql);
			if ($recordset) {
				//Sólo aplica la primer actualización que cumpla el parámetro, aunque realmente sólo debería haber una, sin embargo se podrían procesar en el orden:
				//1- Actualizaciones directas al usuario
				//2- Actualizaciones generales
				//3- Actualizaciones personalizadas
				if (!$recordset->EOF) {
					$intServerUpdateVersion = (int) @$recordset->fields["versionnum"];
					$strUpdateCode = (string) @$recordset->fields["updatecode"];
					//$recordset->MoveNext();
				}
			}
			
			$return['updateVersion'] = $intServerUpdateVersion;
			$return['update'] = $strUpdateCode;
			
			//Verifica si el App es de una versión posterior a la que pudiera necesitar una actualización, pero la propia App ya contiene la última actualización
			//(es decir, en el App existe el archivo de updates, pero su versión ya no requeriría ejecutarlo), si es así, se envía el comando para eliminar la
			//actualización local del App. De acuerdo al query, esto sólo podría pasar si no estuviera asignada la versión a actualizar, ya que el query sólo
			//regresa un número de versión si considera que es una actualización que aplica para la versión del App indicada
			if (!$intServerUpdateVersion && $appUpdateVersion) {
				$return['updateVersion'] = -2;
				$return['update'] = '';
			}
			else {
				//Verifica si la versión del App contiene o no la actualización que se pretendía enviar, si es así (es mayor a la máxima que se debía actualizar) entonces
				//no regresa código a actualizar
				if ($intServerUpdateVersion > 0 && $intServerUpdateVersion <= $appUpdateVersion) {
					$return['update'] = "";
					$return['updateVersion'] = 0;
				}
				else {
					//En este caso el server tiene una actualización mas reciente que el App, así que la debe enviar para que se aplique, o bien, en el server se
					//configuró una actualización forzada (versionNum = -1) por lo que debe descargarla sin importar que tenga el App
				}
			}
			//@JAPR
		}
	}
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['settings']) ) {
		$return['settings'] = getEFormsSettings($aRepository, $aUserID, $arrSurveyIDs);
	}
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['menus']) ) {
		//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
		if (!$gbDesignMode && getMDVersion() >= esvSurveyMenu) {
			$arrMenuIDs = array();
			$surveyMenuCollection = BITAMSurveyMenuCollection::NewInstance($aRepository);
			foreach($surveyMenuCollection as $aSurveyMenu) {
				//@JAPR 2015-12-24: Corregido un bug, no estaba validando si el menú realmente contenía formas a las que el usuario tenía acceso (#M8LIJE)
				//Verifica que por lo menos uno de los Surveys de este menú se encuentre entre la lista de Surveys a los que tiene acceso el usuario
				$blnValid = false;
				foreach ($aSurveyMenu->SurveyIDs as $intSurveyID) {
					//Si la forma se encuentra entre a las que tiene acceso este usuario y no está marcada como inválida, entonces es un menú válido
					if (isset($surveyList[$intSurveyID]) && !isset($arrInvalidSurveys[$intSurveyID])) {
						$blnValid = true;
						break;
					}
				}
				
				if (!$blnValid) {
					continue;
				}
				//@JAPR
				
				//@JAPR 2015-09-15: Agregado el soporte de menús agrupadores de formas con DHTMLX
				//Corregido un bug, la definición genérica no debe incluir todo el contenido del menú, sólo los datos básicos para pintar la lista
				$menuID = $aSurveyMenu->MenuID;
				//$creationVersion = (int) @$recordset->fields["creationversion"];
				$menuList[$menuID] = array();
				$menuList[$menuID]['id'] = $menuID;
				$menuList[$menuID]['name'] = $aSurveyMenu->MenuName;
				$menuList[$menuID]['version'] = $aSurveyMenu->VersionNum;
				//@JAPR 2014-03-18: Agregado el icono para las encuestas
				$strDisplayImage = trim($aSurveyMenu->MenuImageText);
				if ($strDisplayImage != '') {
					$strDisplayImage = trim(str_replace(array('<p>', '</p>'), '', $strDisplayImage));
					if (!$blnWebMode && $appVersion < esvIndependentImgDownload) {
						$strDisplayImage = (string) @BITAMSection::convertImagesToBase64($strDisplayImage);	
					}
					elseif (!$blnWebMode) {
						//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
						//@JAPR 2015-08-06: Agregado el parámetro $bUseImgTag para indicar si se desea o no como tag <Img>
						$strDisplayImage = (string) @BITAMSection::convertImagesToRelativePath($aRepository, $strDisplayImage, true, false);
						//@JAPR
					}
					else {
						//@JAPR 2015-07-19: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
						//JAPR 2015-08-04: Modificada para utilizar DHTMLX
						//if ($creationVersion >= esveFormsv6 && $appVersion < esveFormsDHTMLX) {
							$strDisplayImage = '<img src="'.$strDisplayImage.'" />';
						//}
						//@JAPR
					}
				}
				//@JAPR 2015-09-28: Corregido un bug, no estaba enviando la imagen al eliminarla, así que no se podía refrescar correctamente si alguna vez ya había estado asignada (#BTFQAV)
				$menuList[$menuID]['displayImage'] = $strDisplayImage;
				//@JAPR 2015-10-09: Corregido el despliegue de las formas de menus, estaba mostrando las formas fuera de su menu también porque los menús no indicaban las formas que
				//contenían sino hasta que eran cargados (#XW13KY)
				$menuList[$menuID]['surveyIDs'] = array();
				if (count($aSurveyMenu->SurveyIDs)) {
					$menuList[$menuID]['surveyIDs'] = $aSurveyMenu->SurveyIDs;
				}
				
				//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
				if (getMDVersion() >= esvFormsLayouts) {
					$arrDef['width'] = $aSurveyMenu->Width;
					$arrDef['height'] = $aSurveyMenu->Height;
					$arrDef['textPos'] = $aSurveyMenu->TextPosition;
					//@JAPR 2016-02-11: Corregido un bug, el componente de captura estaba grabando espacio cuando se dejaba vacío, así que se limpiarán los espacios (#FSMUZC)
					$arrDef['textSize'] = trim($aSurveyMenu->TextSize);
					$arrDef['customLayout'] = trim($aSurveyMenu->CustomLayout);
				}
				//@JAPR
			}
			if (count($menuList) > 0) {
				$arrMenuIDs = array_keys($menuList);
			}
			$return['menuList'] = $menuList;
			$return['menuOrder'] = $arrMenuIDs;
		}
	}
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Agregado el control de información a obtener mediante parámetro
	if ( $blnGetAllData || isset($oDataToGet['images']) ) {
		//@JAPR 2014-09-05: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
		//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
		if (!$gbDesignMode && $appVersion >= esvIndependentImgDownload) {
			//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
			//Agregado el parámetro $anArrayOfCatalogIDs para extraer las imagenes de sus atributos
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			//Cambiada la función getEFormsDefinitionsImageVersions por una que ahora obtenga las imágenes directamente de una tabla de eForms agrupada por usuario
			//para todas las formas a las que tiene acceso la última vez que el Agente procesó al usuario con la función original
			//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
			//Temporalmente forzado a usar la carga desde Metadata de las imágenes, porque se integró este archivo como parte de otro cambio de optimización pero
			//sin los demás archivos correspondientes para explotar la optimización de imágenes, así que habría quedado sin descargar las imágenes por completo
			if ( true || isset($oDataToGet['rawimages']) ) {
				$imageList = @getEFormsDefinitionsImageVersions($aRepository, $arrSurveyIDs, $arrCatalogIDs);
			}
			else {
				$imageList = @getEFormsDefinitionsImageVersionsCache($aRepository, $arrSurveyIDs, $arrCatalogIDs, $intUserID);
			}
			//@JAPR
			if (!is_null($imageList) && is_array($imageList) && count($imageList) > 0) {
				$return['imageList'] = $imageList;
			}
		}
	}
	//@JAPR
	
    return $return;
}

//@JAPR 2018-06-20: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
/* Obtiene el array con todas las definiciones que deben ser descargadas al App al hacer el login. Agregada para permitir reutilizar código con la actualización
de login, ya que ahora las definiciones se podrán descargar también junto a la definición de una forma en situaciones específicas que optimiza este issue */
function getEFormsSettings($aRepository, $aUserID = -1, $aSurveyIDs) {
	global $appVersion;
	global $kpiUser;
	//@JAPR 2014-03-18: Agregado el icono para las encuestas
	global $blnWebMode;
	global $gbDesignMode;
	
	$intUserID = $aUserID;
	//********************************************************************
	//Primer bloque original de Settings cargado anteriormente en getEFormsDefinitionsVersions
	//********************************************************************
	//@JAPR 2013-12-17: Cambiado el modo de uso del GPS
	$return = array();
	$return['settings'] = array();
	//$return['settings']['checkFileUpload'] = 1;
	require_once('settingsvariable.inc.php');
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'REGISTERGPSDATAAT');
	if (!is_null($objSetting)) {
		$return['settings']['saveGPSDataAtStartUp'] = (int) @$objSetting->SettingValue;
	}
	//Habilitar esta configuración para mostrar mensajes relativos a la carga de las APIs de Google y obtención de direcciones mediante el Geocoder
	//$return['settings']['debugMapsOnScreen'] = true;
	//Conchita 2014-09-03 agregado el header personalizado
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'CUSTOMHEADER');
	if (!is_null($objSetting)) {
		//Conchita 2014-11-25 Corregido bug que no aceptaba acentos en el custom header
		$return['settings']['customHeader'] = (string) @$objSetting->SettingValue;
	}
	//Conchita agregadas las nuevas settings de usecache y cacherequests
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'USECACHE');
	if (!is_null($objSetting)) {
	  //Conchita 2014-11-25 Corregido bug que no aceptaba acentos en el custom header
		$return['settings']['useCache'] = (int) $objSetting->SettingValue;
		
	}
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'CACHEREQUESTS');
	if (!is_null($objSetting)) {
		//Conchita 2014-11-25 Corregido bug que no aceptaba acentos en el custom header
		$return['settings']['cacheRequests'] = (string) @$objSetting->SettingValue;
	}
	//@JAPR 2014-08-26: Modificada la validación para que aplique sólo a ciertos espacios de KPIOnline
	$arrDBsWithAutoRefreshBeforeCapture = array();
	/*
	$arrDBsWithAutoRefreshBeforeCapture['fbm_bmd_0030'] = 1;
	$arrDBsWithAutoRefreshBeforeCapture['fbm_bmd_0866'] = 1;
	$arrDBsWithAutoRefreshBeforeCapture['fbm_bmd_1024'] = 1;
	*/
	if ($aRepository && @$aRepository->ADOConnection && @$aRepository->ADOConnection->databaseName) {
		if (isset($arrDBsWithAutoRefreshBeforeCapture[strtolower($aRepository->ADOConnection->databaseName)])) {
			$return['settings']['refreshDefinitionsBeforeEntry'] = true;
		}
	}
	//@JAPR 2014-08-27: Para efectos de testing, se dejó habilitada esta configuración (Barcel), pero el default debe ser deshabilitado
	$return['settings']['refreshDefinitionsBeforeEntry'] = false;
	
	//@JAPR 2014-06-19: Agregados los templates para personalización del App
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ENABLECOLORTEMPLATES');
	//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
	if (getMDVersion() >= esvTemplateStyles && ((int) @$objSetting->SettingValue) == 1) {
		//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
		//$objAppCustomization = BITAMEFormsAppCustomizationTpl::GetCurrentColorScheme($aRepository);
		$objAppCustomization = BITAMEFormsAppCustomStylesTpl::GetCurrentColorScheme($aRepository);
		if (!is_null($objAppCustomization)) {
			$arrCSSStyle = $objAppCustomization->getJSonDefinition();
			if (!is_null($arrCSSStyle) && is_array($arrCSSStyle) && count($arrCSSStyle) > 0) {
				$return['settings']['cssStyle'] = $arrCSSStyle;
			}
			else {
				$return['settings']['cssStyle'] = array();
			}
		}
	}
	//@JAPR 2016-05-12: Corregido un bug, dado a que no enviar la propiedad cssStyle provocaría que nunca se limpiara la configuración de templates en el App, se tendrá que enviar
	//el array vacío para forzar a que se sobreescriba la definición anterior en el App, esto va a provocar que entre a las validaciones de cssStyle ya que un objeto vacío aún así
	//se considera "algo", por lo que if(this.cssStyle) si entraría al if, pero al no haber ningún elemento simplemente no habrá estilos que aplicar y efectivamente se habría limpiado
	//la configuración de template
	else {
		$return['settings']['cssStyle'] = array();
	}
	
	//OMMC 2015-12-15: Agregado para ocultar las descripciones de las formas y los menús
	if (getMDVersion() >= esvHideDescription){
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'HIDEDESCRIPTION');
		if (!is_null($objSetting)) {
			$return['settings']['hideDescription'] = (int) $objSetting->SettingValue;
		}
	}
	
	//@JAPR 2016-02-04: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
	if (getMDVersion() >= esvFormsLayouts){
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTFORMSWIDTH');
		if (!is_null($objSetting)) {
			$return['settings']['formsWidth'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTFORMSHEIGHT');
		if (!is_null($objSetting)) {
			$return['settings']['formsHeight'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTFORMSTEXTPOSITION');
		if (!is_null($objSetting)) {
			$return['settings']['formsTextPos'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTFORMSTEXTSIZE');
		if (!is_null($objSetting)) {
			//@JAPR 2016-02-11: Corregido un bug, el componente de captura estaba grabando espacio cuando se dejaba vacío, así que se limpiarán los espacios (#FSMUZC)
			$return['settings']['formsTextSize'] = trim((string) @$objSetting->SettingValue);
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTFORMSCUSTOMHTML');
		if (!is_null($objSetting)) {
			//@JAPR 2016-02-11: Corregido un bug, el componente de captura estaba grabando espacio cuando se dejaba vacío, así que se limpiarán los espacios (#FSMUZC)
			$return['settings']['formsCustomLayout'] = trim((string) @$objSetting->SettingValue);
		}
	}

	//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
	if (getMDVersion() >= esvShowQNumbers) {
		$defQNumberDisplay = 1;
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'SHOWQUESTIONNUMBERS');
		//@JAPR 2016-04-07: Corregido un bug, se había quedado un || en lugar de un &&
		if (!is_null($objSetting) && $objSetting->SettingValue != '') {
			$return['settings']['showQNumbers'] = (int) $objSetting->SettingValue;
		}else{
			$return['settings']['showQNumbers'] = $defQNumberDisplay;
		}
	}

	//GCRUZ 2016-05-20: Agregado para mostrar las opciones del menú de 3 rayas
	if (getMDVersion() >= esvAppMenuOptions) {
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'APPMENUOPTIONS');
		//@JAPR 2016-04-07: Corregido un bug, se había quedado un || en lugar de un &&
		//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
		//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
		//el menú de 3 rayas del App (#EJTH2E)
		//Ahora si viene el valor de null entonces se regresará un null en lugar de un array (siempre y cuando la versión sea >= 6.02012), de lo contrario se regresará el array
		//ya sea vacío o con las opciones seleccionadas
		if ($appVersion >= esvHideMenuOptions) {
			if (!is_null($objSetting)) {
				//Verificará explícitamente en busca del valor NULL, si eso tiene, entonces eso se asignará como valor de la configuración
				if (is_null($objSetting->SettingValue)) {
					$return['settings']['appMenuOptions'] = null;
				}
				else {
					//En cualquier otra situación, si existe una configuración aunque esta sea vacía, regresará el objeto conteniendo las opciones seleccionadas
					if ($objSetting->SettingValue != '') {
						$appMenuOptions = explode(',', $objSetting->SettingValue);
						$arrAppMenuOptions = array();
						foreach ($appMenuOptions as $key => $value) {
							$arrAppMenuOptions[$value] = $value;
						}
						$return['settings']['appMenuOptions'] = $arrAppMenuOptions;
					}
					else {
						$return['settings']['appMenuOptions'] = array();
					}
				}
			}
			else {
				//Este caso sólo pudo deberse a un error, en esta situación se regresará NULL para que se muestren las opciones default del App
				$return['settings']['appMenuOptions'] = null;
			}
		}
		else {
			if (!is_null($objSetting) && $objSetting->SettingValue != '') {
				$appMenuOptions = explode(',', $objSetting->SettingValue);
				$arrAppMenuOptions = array();
				foreach ($appMenuOptions as $key => $value)
					$arrAppMenuOptions[$value] = $value;
				$return['settings']['appMenuOptions'] = $arrAppMenuOptions;
			}else{
				$return['settings']['appMenuOptions'] = '';
			}
		}
		//@JAPR
	}
	
	//@JAPR 2016-05-19: Agregada la configuración para envío de GPS constante desde el App (#WRWF3Y)
	$intAppGPSTrackingFreq = 0;
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'APPGPSTRACKER');
	if (!is_null($objSetting)) {
		$intAppGPSTrackingFreq = (int) @$objSetting->SettingValue;
		if ($intAppGPSTrackingFreq < 0) {
			$intAppGPSTrackingFreq = 0;
		}
		
		//Convierte el valor a segundos para enviar al App
		$intAppGPSTrackingFreq *= 60;
	}
	$return['settings']['gpsTrackingFrequency'] = $intAppGPSTrackingFreq;
	
	//@JAPR 2014-09-05: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
	//Ahora el nombre de projecto recibe un rol especial, ya que ahí es donde se guardarán las imagenes de las definiciones
	$return['settings']['project'] = strtolower((string) @$aRepository->ADOConnection->databaseName);

	//OMMC 2017-06-28: Agregado el cambio de key para el API de google maps.
	$return['settings']['mapsAPIKey'] = googleAPIKey;
	//@JAPR 2018-10-19: Integrado los Mapas de Apple (#64ISFM)
	if (getMDVersion() >= esvMapTypeSelector) {
		$intMapType = maptApple;
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'MAPTYPE');
		if (!is_null($objSetting)) {
			$intMapType = (int) @$objSetting->SettingValue;
			if ($intMapType < maptGoogle || $intMapType > maptApple) {
				$intMapType = maptApple;
			}
		}
		$return['settings']['mapType'] = $intMapType;
	}
	
	//OMMC 2016-07-26: Agregada la generación de URLs de la pregunta OCR debido al requerimiento de una key para permitir más requests, se deja esto para estandarizar con Google Maps
	//JAPR 2016-06-24: Modificada la generación de URLs de Google Maps debido al requerimiento de llave a partir de Junio 2016, para poder cambiar la URL desde el server (#F3M0A7)
	//Debido al problema con Google Maps en que a partir del 2016-06-22 comenzó a requerir una API Key para todos los requests a sus servicios, se modificó el App de versión
	//esvGoogleMapsAPIKey para que permita recibir desde el servidor los siguientes parámetros, con lo cual se modificará la manera de generar las peticiones hacia Google sin tener
	//que regenerar el App (excepto que el API cambie por completo los parámetros que requiere recibir), y que será posible controlar desde este archivo según el reposotirio, versión,
	//cuenta específica, etc.
	/*if (!is_null($theAppUser)) {
		//Modificar esta condición según lo que se requiera validar (cambiar a switch si es necesario)
		if ($theAppUser->UserName == 'jpuente497@bitam.com') {
			//API Key requerida por Google Maps
			//Esta llave se debe asignar según las especificaciones del App que se va a generar, ya que hay un límite de requests de aprox. 25,000 por día combinados entre todos los productos,
			//todas las versiones y todas las Apps generadas que utilicen esta llave, así que en caso de ser requerido, se deben generar llaves adicionales que se pueden modificar en este archivo
			//por App generada para hacerlas específicas (la llave incluirá el parámetro por si llegase a cambiar la implementación del request, o por si se debieran invocar parámetros adicionales)
			//Se podría aprovechar para enviar parámetros adicionales dentro de este String y se enviarán a todas las llamadas hacia Google Maps
			//$return['settings']['mapsAPIKey'] = "&key=AIzaSyC4t7DC1pPInDgprpagYe9zw1RZ6CtRi0A";
			//********************************************************************************
			//URL para invocar al método que agregar los Markers dentro de un Mapa ya generado
			//$return['settings']['mapsChartURL'] = "chart.googleapis.com/chart";
			//********************************************************************************
			//URL para invocar a la generación de un mapa (en general carga el API de Google incluso para usar los Geocoders)
			//$return['settings']['mapsAPIURL'] = "maps.googleapis.com/maps/api/js";
			//********************************************************************************
			//URL para invocar al servicio de Geocoders (desde have varias versiones el Geocoder se usa con el API de JavaScript directamente, pero existe esta llamada condicionada)
			//$return['settings']['mapsGeocodeURL'] = "maps.googleapis.com/maps/api/geocode/json";
			//********************************************************************************
			//Protocolo para invocar todas las llamadas al API de Google (a partir de cierta actualización de Google Maps, se requería que siempre fuera https, pero por si esto cambia
			//eventualmente se dejará configurable)
			//$return['settings']['mapsProtocol'] = "http://";
			//********************************************************************************
			//Parámetro que se debe envia para que lleve el key, ya que se construye distinto a googleMaps, se incluye de esta manera por si cambia el nombre del parámetro
			//$return['settings']['OCRKeyParam'] = "apikey";
			//********************************************************************************
			//Llave generada para el servicio de OCR del App de KPIForms
			//$return['settings']['OCRKey'] = "2968ae18d788957";
			//********************************************************************************
			//Ruta del servicio de OCR, la primera es la que se usa, sin embargo, el API tiene la segunda como respaldo. (sólo descomentar una)
			//$return['settings']['OCRService'] = "api.ocr.space/Parse/Image";
			//$return['settings']['OCRService'] = "apifree2.ocr.space/parse/image";
			//********************************************************************************
			//Protocolo del servicio de OCR, siempre ha sido HTTPS, se deja por si llega a cambiar
			//$return['settings']['OCRProtocol'] = "https://";
		}
	}*/
	//@JAPR
	
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	if (!$gbDesignMode && getMDVersion() >= esvDefaultSurvey) {
		//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
		$dsUserID = $intUserID;
		//@JAPR 2014-09-24: Corregido un bug, se estaba usando la función para cargar a partir del UserID en lugar del CLA_USUARIO, así que nunca leía
		//la configuración del usuario correcto si había disparidad en las claves entre SI_SV_Users y SI_USUARIO (algo que es frecuente)
		$dsUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $dsUserID);
		//@JAPR

		$defaultSurveyID = 0;
		if(isset($dsUser) && !is_null($dsUser) && isset($dsUser->DefaultSurvey) && $dsUser->DefaultSurvey > 0) {
			$defaultSurveyID = (int) @$dsUser->DefaultSurvey;
		} else {
			//EVEG faltaba buscar si no tiene surveydefault por el grupo al que pertenece el usuario
			require_once('usergroup.inc.php');
			$groups = BITAMUserGroupCollectionLimited::NewInstance($aRepository, $dsUserID);
			foreach($groups->Collection as $aGroup)
			{
				if($aGroup->DefaultSurvey>0)
				{
					$defaultSurveyID = (int) @$aGroup->DefaultSurvey;	
					break;
				}
			}
			if(!isset($defaultSurveyID) || $defaultSurveyID==0 || $defaultSurveyID==null){
				$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTSURVEY');
				if (!is_null($objSetting)) {
					$defaultSurveyID = (int) @$objSetting->SettingValue;
				}	
			}
		}
		if(!is_null(BITAMSurvey::NewInstanceWithID($aRepository, $defaultSurveyID)) && in_array($defaultSurveyID, $aSurveyIDs)) {
			$return['settings']['defaultSurvey'] = $defaultSurveyID;
		}
		else {
			//@JAPR 2015-05-12: Corregido un bug, si no tenía acceso a la forma no descargaba ningún ID, pero si este request hubiera venido de un cambio
			//de usuario en el App, donde el usuario anterior si tuviera un default válido, se habría quedado la configuración anterior y generaría error (#LNZJUY)
			$return['settings']['defaultSurvey'] = 0;
		}
	}
	
	//********************************************************************
	//Segundo bloque original de Settings cargado anteriormente en getEFormsDefinitionsVersions
	//********************************************************************
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	if (!$gbDesignMode && $appVersion >= esvAppCustomBackground) {
		//@JAPR 2015-09-03: Corregido un bug, si no hay una imagen realmente configurada NO debe regresar la url sin imagen porque termina sin mostrarla
		$intBackgroundVersion = 0;
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'BACKGROUNDIMAGEVERSION');
		if (!is_null($objSetting)) {
			$intBackgroundVersion = (int) @$objSetting->SettingValue;
			if ($intBackgroundVersion <= 0) {
				$intBackgroundVersion = 0;
			}
		}
		
		//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'BACKGROUNDIMAGE', true);
		//Conchita 2014-07-02 Agregado la imagen de fondo 
		if (!is_null($objSetting)) {
			//limpiar imagen de codigos
			$temp = (string) @$objSetting->SettingValue;
			
			$temp = trim(str_replace(array('<p>', '</p>'), '', $temp));
			$temp = trim(str_replace(array('<img', '/>'), '', $temp));
			
			$data = preg_replace('/(img|src)(\"|\'|\=\"|\=\')(.*)/i', "$3", $temp);
			$data = trim(str_replace(array('"'), '', $data));
			if (trim($data) != '') {
				//$data = (string) @BITAMSection::getArrayImages($temp);
				$return['settings']['backgroundImage'] = GetBaseURLPath().(string)@$data;//$objSetting->SettingValue;// @$data;//@$objSetting->SettingValue;
				//Conchita
			}
			else {
				//En este caso si se manda para que se pueda limpiar la imagen, pero se manda completamente vacío para que no intente poner una imagen que no existe
				$return['settings']['backgroundImage'] = "";
			}
			$return['settings']['backgroundImageVersion'] = $intBackgroundVersion;
			//@JAPR
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'MENUMARGINTOP');
		if (!is_null($objSetting)) {
			$return['settings']['menuMarginTop'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'MENUMARGINLEFT');
		if (!is_null($objSetting)) {
			$return['settings']['menuMarginLeft'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'MENUMARGINBOTTOM');
		if (!is_null($objSetting)) {
			$return['settings']['menuMarginBottom'] = (int) @$objSetting->SettingValue;
		}
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'MENUMARGINRIGHT');
		if (!is_null($objSetting)) {
			$return['settings']['menuMarginRight'] = (int) @$objSetting->SettingValue;
		}
	}
	
	//@JAPR 2016-07-06: Corregido un bug, en modo de diseño no hay necesidad de ejecutar este proceso (#XWOMIF)
	if (!$gbDesignMode) {
		if ($appVersion >= esvFormsMenuPosition) {
			$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'FORMSMENUPOSITION');
			if (!is_null($objSetting)) {
				$return['settings']['formsMenuPosition'] = (int) @$objSetting->SettingValue;
			
			}
		}
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'AGENDARADIO');
			if (!is_null($objSetting)) {
				$return['settings']['agendaRadio'] = (int) @$objSetting->SettingValue;
			}
		}
	}
	
	//@JAPR 2016-08-24: Corregido un bug, no estaba asignando las propiedades globales de estas dos configuraciones (#WWIJLY) (#Y3ATJH)
	if ($appVersion >= esvNavigationHistory) {
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ENABLENAVHISTORY');
		if (!is_null($objSetting)) {
			$return['settings']['enableNavHistory'] = (int) @$objSetting->SettingValue;
		}
	}
	
	if ($appVersion >= esvAutoFitFormsImage) {
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'AUTOFITIMAGE');
		if (!is_null($objSetting)) {
			//JAPR 2016-08-26: Corregido un bug, no se estaba usando la propiedad global con el nombre correcto (#Y3ATJH)
			$return['settings']['autoFitImage'] = (int) @$objSetting->SettingValue;
			$return['settings']['formsAutoFitImage'] = (int) @$objSetting->SettingValue;
		}
	}

	//MAPR 2018-02-19: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
	if ($appVersion >= esvAllMobile) {
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALLMOBILE');
		if (!is_null($objSetting)) {
			$return['settings']['allMobile'] = (int) @$objSetting->SettingValue;
		}	
	}
	
	//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
	if (getMDVersion() >= esvForgetCredentials){
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'FORGETCREDENTIALS');
		$intForgetCredentials = 0;
		if (!is_null($objSetting)) {
			$intForgetCredentials = (int) $objSetting->SettingValue;
		}
		$return['settings']['forgetCredentials'] = $intForgetCredentials;
	}
	
	//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
	//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
	//Se deberá mandar al App la versión del servicio para que el App pueda validar las respuestas en función de dicho valor, especialmente lo relacionado con el Upload de archivos
	$return['settings']['webServiceVersion'] = ESURVEY_SERVICE_VERSION;
	//@JAPR
	
	return $return['settings'];
}
//@JAPR

//OMMC 2016-10-05: Agregado el servicio para obtener los mensajes del server que le corresponden al usuario (#L9T2IU)
//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
//Recupera todos los mensajes push que se le enviaron a un usuario / dispositivo en específico
function getEFormsPushMessages($aRepository) {
	global $appVersion;
	
	$userAccount = getParamValue('UserID', 'both', "(string)");
	//@JAPR 2016-11-04: Modificado para diferenciar cuando se recibió vacío el RegID (lo cual denotaría un error) o cuando no se envió. Si no se recibe, entonces se asume que se quieren
	//todos los registros, en caso contrario incluso si se recibiera vacío se aplicaría el filtro para sólo extraer los registros solicitados (aunque no regrese ninguno)
	$userRegID = getParamValue('regid', 'both', "(string)", true);
	//@JAPR
	$arrResponse = array();
	
	if ($userAccount ) {
		//@JAPR 2016-10-19: Corregido un bug de urgencia, no estaba validada correctamente la metadata de eForms, las versiones de App de 6.02006 en adelante ya esperan los mensajes
		//Push asociados a un RegID pero no se había condicionado así que generaba un error, además que la metadata también debe estar migrada a la versión adecuada para que esto funcione (#L9T2IU)
		//Se validó el proceso para que ahora si la metadata ya es la correcta se pida el campo RegID y por tanto sólo regresará los mensajes que el App espera, pero al mismo tiempo el App
		//tiene que ser de la misma versión (6.02006), en cualquier otra combinación de circunstancias se regresarán los mensajes completos sin validación de RegID por lo que el app
		//podrá ver todos los dirigidos al usuario sin importar a que App se enviaron específicamente
		if (getMDVersion() >= esvRealPushMsg) {
			//En esta versión el estado del mensaje se almacena en el detalle por usuario del mensaje
			//@JAPR 2016-11-04: Modificado para diferenciar cuando se recibió vacío el RegID (lo cual denotaría un error) o cuando no se envió. Si no se recibe, entonces se asume que se quieren
			//todos los registros, en caso contrario incluso si se recibiera vacío se aplicaría el filtro para sólo extraer los registros solicitados (aunque no regrese ninguno)
			if (!is_null($userRegID) && $appVersion >= esvRealPushMsg) {
				//En este caso la metadata es la adecuada, se recibió desde el App el RegID así que se espera sólo cierto grupo de mensajes Push, y el App es de la versión correcta, por lo
				//tanto usa la versión mas completa del query, si alguna de estas condiciones no se cumple, significaría que no es el App adecuada o la metadata no es la correcta
				//@JAPR 2018-02-27: Validado que las notificaciones se puedan descargar al usuario independientemente del dispositivo usado si es que se encuentra
				//activa la configuración para enviar las notificaciones a todos los dispositivos históricos (#D44L4G)
				$strRegIdFilter = "B.RegID = ".$aRepository->DataADOConnection->Quote($userRegID)." AND ";
				if (getMDVersion() >= esvAllMobile) {
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository,'ALLMOBILE');
					if ( !is_null($objSetting) && $objSetting->SettingValue ) {
						$strRegIdFilter = "";
					}
				}
				
				$sql = "SELECT A.MessageID, A.CreationDateID, A.Title, A.pushmessage, B.State 
					FROM SI_SV_PushMsg A 
						INNER JOIN SI_SV_PushMsgDet B ON A.MessageID = B.MessageID 
						INNER JOIN ". (string) @$_SESSION["PABITAM_RepositoryName"].".SI_USUARIO C ON B.UserID = C.CLA_USUARIO 
					WHERE C.CUENTA_CORREO = ".$aRepository->DataADOConnection->Quote($userAccount)." AND 
						{$strRegIdFilter} 
						B.State IN (".pushMsgSent.", ".pushMsgReprocess.", ".pushMsgRead.")";
				//@JAPR
			}
			else {
				//En este caso no hay un RegID recibido o la versión del App no es la correcta, por lo tanto aunque si se tiene el campo nuevo, no hay necesidad de utilizarlo, sin embargo
				//no puede ir al query del else de abajo porque como ya tiene la nueva versión de Metadata, eso implica que ya se graban los UserID == CLA_USUARIO que es lo correcto,
				//así que realiza la versión del query de arriba pero sin el filtro extra por RegID
				$sql = "SELECT A.MessageID, A.CreationDateID, A.Title, A.pushmessage, B.State 
					FROM SI_SV_PushMsg A 
						INNER JOIN SI_SV_PushMsgDet B ON A.MessageID = B.MessageID 
						INNER JOIN ". (string) @$_SESSION["PABITAM_RepositoryName"].".SI_USUARIO C ON B.UserID = C.CLA_USUARIO 
					WHERE C.CUENTA_CORREO = ".$aRepository->DataADOConnection->Quote($userAccount)." AND 
						B.State IN (".pushMsgSent.", ".pushMsgReprocess.", ".pushMsgRead.")";
			}
		}
		else {
			//En este caso no hay nada que hacer, el campo aún no existe así que no podemos consultarlo, se regresarán simplemente los mensajes Push cuyo destino sea el usuario, pero
			//además en este caso se asume que el UserID corresponde erróneamente a SI_SV_Users.UserID porque así se había programado originalmente, así que necesita un Join adicional
			//con dicha tabla para poder obtener los mensajes a regresar
			//En esta versión el estado del mensaje se almacenaba en la tabla del mensaje
			$sql = "SELECT A.MessageID, A.CreationDateID, A.Title, A.pushmessage, A.State 
				FROM SI_SV_PushMsg A, ". (string) @$_SESSION["PABITAM_RepositoryName"].".SI_USUARIO B, ". (string) @$_SESSION["PABITAM_RepositoryName"].".SI_SV_Users C 
				WHERE A.DestinationID = C.UserID AND C.CLA_USUARIO = B.CLA_USUARIO  AND 
					B.CUENTA_CORREO = ".$aRepository->DataADOConnection->Quote($userAccount)." AND 
					A.State IN (".pushMsgSent.", ".pushMsgReprocess.", ".pushMsgRead.")";
		}
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$pushMessageID = (int) @$aRS->fields['MessageID'];
				
				if ($pushMessageID) {
					$arrResponse[$pushMessageID] = array();
					$arrResponse[$pushMessageID]['title'] = (string) @$aRS->fields['Title'];
					$arrResponse[$pushMessageID]['message'] = (string) @$aRS->fields['pushmessage'];
					$arrResponse[$pushMessageID]['timeStamp'] = (string) @$aRS->fields['CreationDateID'];
					$arrResponse[$pushMessageID]['status'] = (int) @$aRS->fields['State'];
					$arrResponse[$pushMessageID]['key'] = (int) @$aRS->fields['MessageID'];		
				}
				
				$aRS->MoveNext();
			}
		}
	} 
	
	//@JAPR 2016-10-14: Corregido código fuera de estándar, no hay necesidad de retornar json al app, ya viene como un object
	//La versión esvRealPushMsg ya espera el objeto tal como es el estándar, las versiones previas a eso sin embargo esperaban un string JSON, así que se valida según el app que hace
	//el request
	if ($appVersion >= esvRealPushMsg) {
		return $arrResponse;
	}
	else {
		return json_encode( $arrResponse );
	}
	//@JAPR
}

//OMMC 2016-10-05: Agregado el servicio para obtener los mensajes del server que le corresponden al usuario (#L9T2IU)
//@JAPR 2016-10-18: Agregado el envío de notificaciones push mediante un agente que está ejecutándose todo el tiempo (#L9T2IU)
//Actualiza el status de un mensaje push determinado, no retorna respuesta alguna.
function updateEFormsPushMessages($aRepository) {
	global $appVersion;
	
	$messageID = getParamValue('pushMsgID', 'both', "(int)");
	$messageStatus = getParamValue('pushMsgStatus', 'both', "(int)");
	$arrResponse = array();
	
	if ($messageID && $messageStatus) {
		//@JAPR 2016-10-14: Modificado el esquema de Push para envíar mensajes al último dispositivo utilizado por el usuario solamente (#L9T2IU)
		//La actualización del status del mensaje se debe realizar directamente sobre el envío a este usuario, no sobre el mensaje en si, ya que ese es una instancia que se pudo
		//haber enviado a uno o varios grupos de usuarios y por tanto no podría mantener múltiples status
		$sql = "UPDATE SI_SV_PushMsgDet 
				SET State = ".$messageStatus." 
			WHERE MessageID = {$messageID} AND UserID = ".$_SESSION["PABITAM_UserID"];
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$arrResponse[$messageID] = array();
			$arrResponse[$messageID]['key'] = (int) $messageID;
			$arrResponse[$messageID]['status'] = (int) $messageStatus;
		}
	}
	
	//@JAPR 2016-10-14: Corregido código fuera de estándar, no hay necesidad de retornar json al app, ya viene como un object
	//La versión esvRealPushMsg ya espera el objeto tal como es el estándar, las versiones previas a eso sin embargo esperaban un string JSON, así que se valida según el app que hace
	//el request
	if ($appVersion >= esvRealPushMsg) {
		return $arrResponse;
	}
	else {
		return json_encode( $arrResponse );
	}
	//@JAPR
}

//@JAPR 2014-09-03: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
/* Obtiene la versión (timestamp devuelto por la función GetFileLastModTimestamp) de todas las imagenes involucradas en la descarga de definiciones
hacia un dispositivo basado en el request que se recibió, es decir, considerando la visibilidad del usuario y el tipo de dispositivo entre otras cosas.
Las imagenes se consideran a partir de las encuestas a las que se tiene visibilidad, por lo que se recibirá como parámetro el mismo array de encuestas
que previamente ya se había calculado en la función getEFormsDefinitionsVersions.
Se regresará el array multidimensional conteniendo la versión (timestamp) de las imagenes, la ruta relativa donde se encuentran, y el nombre con el
que se deberán de grabar en el dispositivo, ya que ahí la ruta será simplemente relativa a la carpeta images, por lo que no se conservará la ruta
absoluta sino que deberá ser modificada durante la descarga de las definiciones
//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
//Agregado el parámetro $anArrayOfCatalogIDs para extraer las imagenes de sus atributos
//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
El query se filtrará como antes para los objetos que sean previos a V6, pero se filtrará buscando sólo la palabra CustomerImages (la ruta de imagenes personalizadas) para los que son
de v6 o posterior, ya que ahora el componente para imagenes en configuraciones que sólo eso debían tener es un simple upload. Para configuraciones que sean a partir de un editor HTML
se hará de la misma manera que antes
*/
function getEFormsDefinitionsImageVersions($aRepository, $anArrayOfSurveyIDs, $anArrayOfCatalogIDs = null) {
	global $blnWebMode;
	if (!isset($blnWebMode)) {
		$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	}
	global $appVersion;
	
	$return = array();
	
	if (is_null($anArrayOfSurveyIDs) || !is_array($anArrayOfSurveyIDs) || count($anArrayOfSurveyIDs) == 0) {
		return $return;
	}
	
	//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
	if (is_null($anArrayOfCatalogIDs) || !is_array($anArrayOfCatalogIDs)) {
		$anArrayOfCatalogIDs = array();
	}
	//@JAPR
	
	$strFieldNameCS = "ImageText";
	$strFieldName = strtolower("ImageText");
	$strSurveyIDs = implode(',', $anArrayOfSurveyIDs);
	
	$blnUseResponsiveDesign = false;
	$intDeviceID = dvcWeb;
	//@JAPR 2018-07-09: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	/*if (getMDVersion() >= esvResponsiveDesign) {
		$blnUseResponsiveDesign = true;
		$intDeviceID = identifyDeviceType();
	}*/
	//@JAPR
	
	//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
	//Filtro para todas las propiedades que se configuran con un editor HTML, se soportarán tag <IMG/> o style URL()
	$strHTMLEditorFilter = "NOT %s IS NULL AND %s <> '' AND (%s LIKE '%IMG%' OR %s LIKE '%URL%')";
	//Filtro para las propiedades en objetos de V6 que sólo contienen una imagen, en este caso la ruta es simplemente customerImages/fbm_bmd_####/Nombreimagen.ext
	$strV6ImagePropFilter = "NOT %s IS NULL AND %s <> '' AND %s LIKE '%customerimages%'";
	//Filtro aplicable a objetos creados antes de v6, donde todo se configuraba con el TinyMCE y se generaban como tags <IMG/> o style URL()
	$strPreV6ObjectFilter = " AND A.CreationVersion < ".esveFormsv6." ";
	//Filtro aplicable a objetos creados a partir de v6, donde las propiedades que sólo son una imagen se graban como una ruta customerImages/fbm_bmd_####/Nombreimagen sin <IMG/> o URL()
	$strPostV6ObjectFilter = " AND A.CreationVersion >= ".esveFormsv6." ";
	//Campo ficticio que representa una propiedad configurada con un editor HTML
	$strHTMLEditorField = ", ".imgptHTML." AS IsSingleImage";
	//Campo ficticio que representa una propiedad configurada con un file y que sólo contiene una ruta relativa a una imagen
	$strImagePropertyField = ", ".imgpPicture." AS IsSingleImage";
	//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
	//Campo ficticio que representa una propiedad configurada con un css y que sólo contiene la referencia a la imagen (se asume dentro de customerImages)
	$strCSSTyleField = ", ".imgpCSSStyle." AS IsSingleImage";
	//@JAPR
	
	//*********************************************************************************************************************************************************************
	//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
	//El primer query realiza la carga de las imagenes para objetos de las versiones anteriores a v6 (@JAPRDescontinuado), eventualmente se debe remover
	//*********************************************************************************************************************************************************************
	if ($appVersion < esveFormsv6) {
		$sql = '';
		$strAnd = '';
		//Imagenes de las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$sqlQuery = "SELECT DISTINCT A.SurveyImageText AS ImageText, ".otySurvey." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Survey A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND NOT A.SurveyImageText IS NULL AND 
					A.SurveyImageText <> ".$aRepository->DataADOConnection->Quote('').$strPreV6ObjectFilter;
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Imagenes de secciones formateadas
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.FormattedText END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Section A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.FormattedText", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.FormattedText AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Section A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.FormattedText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Imagenes del header y footer personalizado de secciones formateadas
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			//Footer
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.HTMLFooter IS NULL AND B.HTMLFooter <> '' THEN B.HTMLFooter ELSE A.HTMLFooter END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Section A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLFooter", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.HTMLFooter", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLFooter AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Section A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLFooter", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
			
			//Header
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.HTMLHeader IS NULL AND B.HTMLHeader <> '' THEN B.HTMLHeader ELSE A.HTMLHeader END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Section A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLHeader", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.HTMLHeader", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLHeader AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Section A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLHeader", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Texto formateado de preguntas tipo mensaje
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.QuestionMessage END) AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Question A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionMessage", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.QuestionMessage AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Question A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionMessage", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Imagenes de las preguntas
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.ImageText IS NULL AND B.ImageText <> '' THEN B.ImageText ELSE A.QuestionImageText END) AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Question A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionImageText", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.ImageText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.QuestionImageText AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_Question A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionImageText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Imagenes de las preguntas sketch
		//Conchita 2014-10-13 agregadas las imagenes de las preguntas tipo sketch
		if (getMDVersion() >= esvSketchImage) {
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.SketchImage IS NULL AND B.SketchImage <> '' THEN B.SketchImage ELSE A.SketchImage END) AS SketchImage, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Question A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.SketchImage", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.SketchImage", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.SketchImage AS SketchImage, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_Question A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.SketchImage", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Conchita
		//Imagenes de las opciones de respuesta
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.ImageText IS NULL AND B.ImageText <> '' THEN B.ImageText ELSE A.DisplayImage END) AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_QAnswers A 
					LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
					LEFT JOIN SI_SV_SurveyHTML B ON C.SurveyID = B.SurveyID AND B.ObjectType = ".otyAnswer." AND A.ConsecutiveID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.DisplayImage", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.ImageText", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.DisplayImage AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID 
				FROM SI_SV_QAnswers A 
					LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
				WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.DisplayImage", $strHTMLEditorFilter).") 
				)".$strPreV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Texto personalizado de opciones de respuesta
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.HTMLText END) AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_QAnswers A 
						LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
						LEFT JOIN SI_SV_SurveyHTML B ON C.SurveyID = B.SurveyID AND B.ObjectType = ".otyAnswer." AND A.ConsecutiveID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLText", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLText AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID 
					FROM SI_SV_QAnswers A 
						LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
					WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLText", $strHTMLEditorFilter).") 
					)".$strPreV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n(getEFormsDefinitionsImageVersions) Query para imagenes de versiones menores a v6: ".str_replace("\t","&nbsp;&nbsp;&nbsp;\t", str_replace("\r\n", "<br>\r\n", $sql)));
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$strImageText = (string) @$aRS->fields[$strFieldName];
				$images = BITAMSection::getArrayImages($strImageText);
				foreach ($images as $urlimg) {
					$strFilePath = GetPhysicalFilePathFromURL($urlimg);
					$strFileName = urldecode(GetFileNameFromFilePath($strFilePath));
					//@JAPR 2014-09-24: Corregido un bug, se debe aplicar la decodificación URL de la ruta porque si no la versión se regresa como 0, ya
					//que las funciones de acceso físico al archivo necesitan el nombre tal como existe en realidad, no como se usa en URLs
					$strVersionNum = GetFileLastModTimestamp(urldecode($strFilePath), 1);
					//@JAPR
					$return[$strFileName] = array();
					$return[$strFileName]['name'] = $strFileName;
					//@JAPR 2014-09-24: Corregido un bug, se debe aplicar la decodificación URL de la ruta sólo cuando se pide desde un dispositivo, ya
					//que al intentar descargar la imagen el propio dispositivo la va a volver a codificar para usarla como URL, así que no se debe
					//codificar doble o no se podrá descargar
					if ($blnWebMode) {
						$return[$strFileName]['src'] = $urlimg;
					}
					else {
						$return[$strFileName]['src'] = urldecode($urlimg);
					}
					//@JAPR
					$return[$strFileName]['version'] = $strVersionNum;
				}
				$aRS->MoveNext();
			}
		}
	}
	
	//*********************************************************************************************************************************************************************
	//@JAPR 2015-07-20: Agregado el soporte para imagenes en v6, ya que no se grabarán mas como un tag image, así que hay que agregarlo manualmente
	//El segundo query realiza la carga de las imagenes para objetos de las versiones a partir de v6
	//*********************************************************************************************************************************************************************
	if ($appVersion >= esveFormsv6) {
		//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
		//Obtiene los IDs de los templates de estilos que debe descargar, esto incluye el global + todos los personalizados para las formas a las que se tiene acceso
		$strTemplateStyles = '';
		$strAnd = '';
		$intTemplateID = 0;
		if (getMDVersion() >= esvTemplateStyles) {
			$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ENABLECOLORTEMPLATES');
			if (!is_null($objSetting) && ((int) @$objSetting->SettingValue) == 1) {
				$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTCOLORTEMPLATE');
				if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
					$intTemplateID = (int) @$objSetting->SettingValue;
				}
			}
			
			//Asigna el template global si es que hay alguno configurado
			if ($intTemplateID > 0) {
				$strTemplateStyles .= $strAnd.$intTemplateID;
				$strAnd = ', ';
			}
			
			//Verifica los templates por formas a las que se tiene acceso
			$sql = "SELECT DISTINCT A.TemplateID 
				FROM SI_SV_Survey A 
				WHERE A.SurveyID IN ({$strSurveyIDs})";
			if ($intTemplateID > 0) {
				$sql .= " AND A.TemplateID <> {$intTemplateID}";
			}
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS) {
				while (!$aRS->EOF) {
					$intTemplateID = (int) @$aRS->fields["templateid"];
					if ($intTemplateID > 0) {
						$strTemplateStyles .= $strAnd.$intTemplateID;
						$strAnd = ', ';
					}
					$aRS->MoveNext();
				}
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nTemplates para identificar imágenes: {$strTemplateStyles}");
			}
		}
		//@JAPR
		
		$sql = '';
		$strAnd = '';
		//Imagenes de las encuestas
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			$sqlQuery = "SELECT DISTINCT A.SurveyImageText AS ImageText, ".otySurvey." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
				FROM SI_SV_Survey A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND NOT A.SurveyImageText IS NULL AND 
					A.SurveyImageText <> ".$aRepository->DataADOConnection->Quote('').$strPostV6ObjectFilter;
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Imagenes de secciones formateadas
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.FormattedText END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
				FROM SI_SV_Section A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.FormattedText", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
				)".$strPostV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.FormattedText AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
				FROM SI_SV_Section A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.FormattedText", $strHTMLEditorFilter).") 
				)".$strPostV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Imagenes del header y footer personalizado de secciones formateadas
		if (getMDVersion() >= esvSectionFmtdHeadFoot) {
			//Footer
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.HTMLFooter IS NULL AND B.HTMLFooter <> '' THEN B.HTMLFooter ELSE A.HTMLFooter END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_Section A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLFooter", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.HTMLFooter", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLFooter AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_Section A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLFooter", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
			
			//Header
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.HTMLHeader IS NULL AND B.HTMLHeader <> '' THEN B.HTMLHeader ELSE A.HTMLHeader END) AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_Section A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otySection." AND A.SectionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLHeader", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.HTMLHeader", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLHeader AS ImageText, ".otySection." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_Section A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLHeader", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Texto formateado de preguntas tipo mensaje
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.QuestionMessage END) AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
				FROM SI_SV_Question A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionMessage", $strHTMLEditorFilter).") 
					OR 
					(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
				)".$strPostV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.QuestionMessage AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
				FROM SI_SV_Question A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionMessage", $strHTMLEditorFilter).") 
				)".$strPostV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//@JAPR 2015-08-24: Corregido un bug, no se estaban considerando las imágenes de la etiqueta de las preguntas (#1NIJ0Z)
		$sqlQuery = "SELECT DISTINCT A.QuestionTextHTML AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
			FROM SI_SV_Question A 
			WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
				(".str_ireplace("%s", "A.QuestionTextHTML", $strHTMLEditorFilter).") 
			)".$strPostV6ObjectFilter;
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		//@JAPR
		
		//Imagenes de las preguntas
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.ImageText IS NULL AND B.ImageText <> '' THEN B.ImageText ELSE A.QuestionImageText END) AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
				FROM SI_SV_Question A 
					LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionImageText", $strV6ImagePropFilter).") 
					OR 
					(".str_ireplace("%s", "B.ImageText", $strV6ImagePropFilter).") 
				)".$strPostV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.QuestionImageText AS ImageText, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
				FROM SI_SV_Question A 
				WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.QuestionImageText", $strV6ImagePropFilter).") 
				)".$strPostV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Imagenes de las preguntas sketch
		//Conchita 2014-10-13 agregadas las imagenes de las preguntas tipo sketch
		if (getMDVersion() >= esvSketchImage) {
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.SketchImage IS NULL AND B.SketchImage <> '' THEN B.SketchImage ELSE A.SketchImage END) AS SketchImage, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
					FROM SI_SV_Question A 
						LEFT JOIN SI_SV_SurveyHTML B ON A.SurveyID = B.SurveyID AND B.ObjectType = ".otyQuestion." AND A.QuestionID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.SketchImage", $strV6ImagePropFilter).") 
						OR 
						(".str_ireplace("%s", "B.SketchImage", $strV6ImagePropFilter).") 
					)".$strPostV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.SketchImage AS SketchImage, ".otyQuestion." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
					FROM SI_SV_Question A 
					WHERE A.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.SketchImage", $strV6ImagePropFilter).") 
					)".$strPostV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		
		//Conchita
		//Imagenes de las opciones de respuesta
		if ($blnUseResponsiveDesign) {
			$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.ImageText IS NULL AND B.ImageText <> '' THEN B.ImageText ELSE A.DisplayImage END) AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
				FROM SI_SV_QAnswers A 
					LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
					LEFT JOIN SI_SV_SurveyHTML B ON C.SurveyID = B.SurveyID AND B.ObjectType = ".otyAnswer." AND A.ConsecutiveID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
				WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.DisplayImage", $strV6ImagePropFilter).") 
					OR 
					(".str_ireplace("%s", "B.ImageText", $strV6ImagePropFilter).") 
				)".$strPostV6ObjectFilter;
		}
		else {
			$sqlQuery = "SELECT DISTINCT A.DisplayImage AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
				FROM SI_SV_QAnswers A 
					LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
				WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
					(".str_ireplace("%s", "A.DisplayImage", $strV6ImagePropFilter).") 
				)".$strPostV6ObjectFilter;
		}
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		
		//Texto personalizado de opciones de respuesta
		if (getMDVersion() >= esvPasswordLangAndRedesign) {
			if ($blnUseResponsiveDesign) {
				$sqlQuery = "SELECT DISTINCT (CASE WHEN NOT B.FormattedText IS NULL AND B.FormattedText <> '' THEN B.FormattedText ELSE A.HTMLText END) AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_QAnswers A 
						LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
						LEFT JOIN SI_SV_SurveyHTML B ON C.SurveyID = B.SurveyID AND B.ObjectType = ".otyAnswer." AND A.ConsecutiveID = B.ObjectID AND B.DeviceID = {$intDeviceID} 
					WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLText", $strHTMLEditorFilter).") 
						OR 
						(".str_ireplace("%s", "B.FormattedText", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			else {
				$sqlQuery = "SELECT DISTINCT A.HTMLText AS ImageText, ".otyAnswer." AS ObjectType, {$intDeviceID} AS DeviceID {$strHTMLEditorField} 
					FROM SI_SV_QAnswers A 
						LEFT JOIN SI_SV_Question C ON A.QuestionID = C.QuestionID 
					WHERE C.SurveyID IN ({$strSurveyIDs}) AND ( 
						(".str_ireplace("%s", "A.HTMLText", $strHTMLEditorFilter).") 
					)".$strPostV6ObjectFilter;
			}
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		$sqlQuery = "SELECT DISTINCT A.SurveyMenuImage AS SurveyMenuImage, ".otySurvey." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
			FROM SI_SV_Survey A 
			WHERE A.SurveyID IN ({$strSurveyIDs}) AND NOT A.SurveyMenuImage IS NULL AND 
				A.SurveyMenuImage <> ".$aRepository->DataADOConnection->Quote('').$strPostV6ObjectFilter;
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		//rtorres se agrega la descarga de las aimagenes faltantes de los menus.
		$sqlQuery = "SELECT DISTINCT A.MenuImageText AS MenuImageText, ".otyMenu." AS ObjectType, {$intDeviceID} AS DeviceID {$strImagePropertyField} 
			FROM SI_SV_Menu A 
			WHERE NOT A.MenuImageText IS NULL AND 
				A.MenuImageText <> ".$aRepository->DataADOConnection->Quote('');
		$sql .= $strAnd.$sqlQuery;
		$strAnd = "
			UNION 
			";
		//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
		//Sólo se debe incluir el query de estilos si hay por lo menos algún template involucrado
		if (getMDVersion() >= esvTemplateStyles && trim($strTemplateStyles) != '') {
			$sqlQuery = "SELECT DISTINCT A.Style AS {$strFieldNameCS}, ".otyAppCustStyles." AS ObjectType, {$intDeviceID} AS DeviceID {$strCSSTyleField} 
				FROM SI_SV_TemplateStyle A 
				WHERE A.TemplateID IN ({$strTemplateStyles}) AND NOT A.Style IS NULL AND 
					A.Style LIKE ".$aRepository->DataADOConnection->Quote('%background-image%');
			$sql .= $strAnd.$sqlQuery;
			$strAnd = "
				UNION 
				";
		}
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n(getEFormsDefinitionsImageVersions) Query para imagenes de versiones mayores a v6: ".str_replace("\t","&nbsp;&nbsp;&nbsp;\t", str_replace("\r\n", "<br>\r\n", $sql)));
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$strImageText = (string) @$aRS->fields[$strFieldName];
				$bSingleImage = (int) @$aRS->fields["issingleimage"];
				//Si se trata de una propiedad que sólo contiene una imagen, le agrega un tag IMG ficticio para que la función identifique correctamente la imagen y regrese el array esperado
				if ($bSingleImage) {
					//@JAPRWarning: Podría faltar concatenar la ruta de este servicio
					//@JAPR 2016-05-21: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
					switch ($bSingleImage) {
						case imgpCSSStyle:
							//Ahora el tipo imgpCSSStyle corresponderá con un estilo que es un JSON de propiedades CSS que pueden venir con URL, así que se parsea por ese objeto y se adapta 
							//para que funcione BITAMSection::getArrayImages
							$strImageText = ReplaceCSSStyleBackgroundImageForIMG($strImageText);
							break;
						default:
							$strImageText = '<img src="'.GetBaseURLPath().$strImageText.'" />';
							break;
					}
				}
				
				$images = BITAMSection::getArrayImages($strImageText);
				foreach ($images as $urlimg) {
					//OMMC 2015-11-18: Agregado para reemplazar el nombre del archivo ya que aparecía mal codificado en disco.
					$urlEncodedFileName = explode("/", $urlimg);
					$urlEncodedFileName = urlencode(end($urlEncodedFileName));
					
					$strFilePath = GetPhysicalFilePathFromURL($urlimg);

					$strFilePath = explode("\\", $strFilePath);
					$strFilePath[count($strFilePath)-1] = $urlEncodedFileName;
					$strFilePath = implode("\\", $strFilePath);

					$strFileName = urldecode(GetFileNameFromFilePath($strFilePath));
					//OMMC
					//@JAPR 2014-09-24: Corregido un bug, se debe aplicar la decodificación URL de la ruta porque si no la versión se regresa como 0, ya
					//que las funciones de acceso físico al archivo necesitan el nombre tal como existe en realidad, no como se usa en URLs
					$strVersionNum = GetFileLastModTimestamp(urldecode($strFilePath), 1);
					//@JAPR
					$return[$strFileName] = array();
					$return[$strFileName]['name'] = $strFileName;
					//@JAPR 2014-09-24: Corregido un bug, se debe aplicar la decodificación URL de la ruta sólo cuando se pide desde un dispositivo, ya
					//que al intentar descargar la imagen el propio dispositivo la va a volver a codificar para usarla como URL, así que no se debe
					//codificar doble o no se podrá descargar
					if ($blnWebMode) {
						//OMMC 2015-11-18: Agregado para reemplazar el nombre del archivo ya que aparecía mal codificado en disco.
						$urlimg = explode("/", $urlimg);
						$urlimg[count($urlimg)-1] = $urlEncodedFileName;
						$urlimg = implode("/", $urlimg);
						//OMMC
						$return[$strFileName]['src'] = $urlimg;
					}
					else {
						$return[$strFileName]['src'] = urldecode($urlimg);
					}
					//@JAPR
					$return[$strFileName]['version'] = $strVersionNum;
					//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
					//Sólo cuando se trata del agente de eForms que invoca a este proceso, asignará el DataSource del cual se extrajo la imagen
					//En este caso se trata de una imagen de definición, así que utiliza un DataSource dummy
					if (getParamValue('isagent', 'both', '(int)', true)) {
						$return[$strFileName]['did'] = -1;
					}
					//@JAPR
				}
				$aRS->MoveNext();
			}
		}
	}
	//@JAPR
	
	if (getMDVersion() >= esvCatalogAttributeImage) {
		//@JAPR 2017-03-03: Corregido un bug, los atributos con imágenes pero cuyos catálogos exclusivamente están asociados a filtros dinámicos, no deben descargar todas las imágenes
		//ya que no serán utilizadas necesariamente por el App o será bajo depanda cuando se resuelvan los filtros dinámicos (#6ZKO4O)
		$attributesImagesList = @getAttributesImageVersions($aRepository, $anArrayOfCatalogIDs, $anArrayOfSurveyIDs);
		if (!is_null($attributesImagesList) && is_array($attributesImagesList) && count($attributesImagesList) > 0) {
			foreach ($attributesImagesList as $fname => $attributeImage) {
				$return[$fname] = $attributeImage;
			}
		}
	}
	return $return;
}

//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
/* Esta función es equivalente a getEFormsDefinitionsImageVersions sólo que no consulta directamente los valores de los catálogos ni aplica ningún tipo de seguridad,
sino que consulta exclusivamente la tabla de caché de imágenes ya procesada por el agente usando la función original (donde si consultaba los catálogos y aplicaba
la seguridad del usuario), de tal forma que ya no consume tiempo adicional ni siquiera en verificar la versión de los archivos, de esta manera la obtención de las
versiones de imágenes será extremadamente rápida. No incluye imágenes de DataSources que tengan filtros dinámicos para el usuario
El usuario recibido opcionalmente debería ser el usuario logeado que está solicitando las versiones de definiciones, ya que esta función no está planeada para
funcionar de manera masiva para todos los usuarios, sino para obtener exclusivamente las imágenes de un usuario en particular
*/
function getEFormsDefinitionsImageVersionsCache($aRepository, $anArrayOfSurveyIDs, $anArrayOfCatalogIDs = null, $aUserID = -1) {
	global $blnWebMode;
	if (!isset($blnWebMode)) {
		$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	}
	global $appVersion;
	
	$return = array();
	
	if (is_null($anArrayOfSurveyIDs) || !is_array($anArrayOfSurveyIDs) || count($anArrayOfSurveyIDs) == 0) {
		return $return;
	}
	
	//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
	if (is_null($anArrayOfCatalogIDs) || !is_array($anArrayOfCatalogIDs)) {
		$anArrayOfCatalogIDs = array();
	}
	//@JAPR
	
	$strFieldNameCS = "ImageText";
	$strFieldName = strtolower("ImageText");
	$strSurveyIDs = implode(',', $anArrayOfSurveyIDs);
	$intUserID = $aUserID;
	if ( $aUserID == -1 ) {
		$intUserID = $_SESSION["PABITAM_UserID"];
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n(getEFormsDefinitionsImageVersionsCache) Query para imagenes de versiones mayores a v6: ".str_replace("\t","&nbsp;&nbsp;&nbsp;\t", str_replace("\r\n", "<br>\r\n", $sql)));
	}
	//Este query sólo necesita estar filtrado por el usuario, ya que todos los catálogos a los que tenga acceso serán necesarios descargarlos para poder ver
	//correctamente las formas, el dato del DataSourceID existe únicamente para optimización de consulta de datos hacia eBavel en conjunto con el campo LastDateID
	$sql = "SELECT A.FileName, A.Source, A.Version 
		FROM SI_SV_SurveyImgVersions A 
		WHERE A.UserID = {$intUserID}";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		while (!$aRS->EOF) {
			$strFileName = (string) @$aRS->fields["filename"];
			$urlimg = (string) @$aRS->fields["source"];
			$strVersionNum = (string) @$aRS->fields["version"];
			$return[$strFileName] = array();
			$return[$strFileName]['name'] = $strFileName;
			//@JAPR 2014-09-24: Corregido un bug, se debe aplicar la decodificación URL de la ruta sólo cuando se pide desde un dispositivo, ya
			//que al intentar descargar la imagen el propio dispositivo la va a volver a codificar para usarla como URL, así que no se debe
			//codificar doble o no se podrá descargar
			if ($blnWebMode) {
				$return[$strFileName]['src'] = $urlimg;
			}
			else {
				$return[$strFileName]['src'] = urldecode($urlimg);
			}
			//@JAPR
			$return[$strFileName]['version'] = $strVersionNum;
			
			$aRS->MoveNext();
		}
	}
	
	return $return;
}
//@JAPR

//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
/*
Esta función obtiene la lista de valores de los atributos marcados como imagen para todos los catálogos especificados, los cuales ya deben ser
rutas de imagenes ajustadas a la carpeta CustomerImages en el caso de atributos de catálogos de eForms, si el catálogo fuera de eBavel, se
asume que la ruta es relativa al servicio de eBavel, el cual se debe encontrar en el mismo server que eForms así que es usable de cualquier
manera, la única diferencia es que en ese caso se tiene que resolver la ruta a algo que exista si es que utiliza referencias específicas
por forma, aplicación o algún otro dato de eBavel, de tal forma que la ruta final sea una ruta absoluta/relativa pero directa sin necesidad
de mas reemplazos (probablemente sólo agregar la referencia al server y el protocolo)
Agregado el parámetro $anArrayOfCatalogIDs para extraer las imagenes de sus atributos
Se cambió el parámetro original para que en lugar de recibir las Formas recibiera directamente los catálogos a procesar
//@JAPR 2017-03-03: Corregido un bug, los atributos con imágenes pero cuyos catálogos exclusivamente están asociados a filtros dinámicos, no deben descargar todas las imágenes
//ya que no serán utilizadas necesariamente por el App o será bajo depanda cuando se resuelvan los filtros dinámicos (#6ZKO4O)
Agregado el parámetro $anArrayOfSurveyIDs para permitir filtrar los catálogos no usados dinámicamente pero sólo en las formas que serán devuelvas hacia el App
//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
Agregado el parámetro $aUserID para permitir especificar el usuario sobre el que se desea aplicar este proceso para evitar tener que utilizar siempre el
usuario de la sesión (originalmente utilizado para las invocaciones desde el Agente de eForms, ya que en dicho agente no existe el concepto de sesión para un usuario
específico, sino que se debe procesar para todos los usuarios del repositorio)
*/
function getAttributesImageVersions($aRepository, $anArrayOfCatalogIDs, $anArrayOfSurveyIDs, $aUserID = -1) {
	global $blnWebMode;
	if (!isset($blnWebMode)) {
		$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	}
	//@JAPR 2017-03-03: Corregido un bug, los atributos con imágenes pero cuyos catálogos exclusivamente están asociados a filtros dinámicos, no deben descargar todas las imágenes
	//ya que no serán utilizadas necesariamente por el App o será bajo depanda cuando se resuelvan los filtros dinámicos (#6ZKO4O)
	global $appVersion;
	
	$attributesImagesList = array();
	
	if (is_null($anArrayOfCatalogIDs) || !is_array($anArrayOfCatalogIDs)) {
		$anArrayOfCatalogIDs = array();
	}
	
	if (count($anArrayOfCatalogIDs) > 0) {
		//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
		//Agregado el parámetro $aUserID para permitir especificar el usuario sobre el que se desea aplicar este proceso
		if ( $aUserID == -1 ) {
			$intUserID = $_SESSION["PABITAM_UserID"];
		}
		else {
			$intUserID = $aUserID;
		}
		//@JAPR
		
		$blnCanSaveeBavelImages = true;
		$streBavelService = '';
		require_once('settingsvariable.inc.php');
		$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
		if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nNo eBavel path configured: ".$blnCanSaveeBavelImages);
			}
			$blnCanSaveeBavelImages = false;
		}
		else {
			//@JAPR 2014-12-18: Corregido un bug, estaba usando ruta física en lugar de URL absoluta
			//$arrURL = parse_url($objSetting->SettingValue);
			//$streBavelService = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']);
			$streBavelService = $objSetting->SettingValue;
			//@JAPR
		}
		
		//@JAPR 2017-03-03: Corregido un bug, los atributos con imágenes pero cuyos catálogos exclusivamente están asociados a filtros dinámicos, no deben descargar todas las imágenes
		//ya que no serán utilizadas necesariamente por el App o será bajo depanda cuando se resuelvan los filtros dinámicos (#6ZKO4O)
		//Verifica que los catálogos estén utilizados por lo menos en alguna pregunta y/o sección que no tenga un filtro dinámico
		$strCatalogIDs = implode(',', $anArrayOfCatalogIDs);
		$strSurveyIDs = implode(',', $anArrayOfSurveyIDs);
		$arrCatalogIDs = array();
		
		//Obtiene los catálogos de la lista recibida que en las formas especificadas no tienen filtros dinámicos, ya que en ese caso son catálogos que si deberán descargar imágenes
		if ($appVersion >= esvDynAppImagesFromServer) {
			$sql = "SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Question A 
				WHERE A.CatalogID IN ({$strCatalogIDs}) AND A.SurveyID IN ({$strSurveyIDs}) 
					AND RTRIM(A.DataSourceFilter) = ".$aRepository->DataADOConnection->Quote("");
			$sql .= " UNION 
				SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Section A 
				WHERE A.CatalogID IN ({$strCatalogIDs}) AND A.SurveyID IN ({$strSurveyIDs}) 
					AND RTRIM(A.DataSourceFilter) = ".$aRepository->DataADOConnection->Quote("");
			$sql .= " UNION 
				SELECT DISTINCT A.CatalogID 
				FROM SI_SV_Survey A 
				WHERE A.CatalogID IN ({$strCatalogIDs}) AND A.SurveyID IN ({$strSurveyIDs})";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n(getEFormsDefinitionsVersions) Query para catálogos con atributos imagen sin filtro dinámico: {$sql}");
			}
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS) {
				while (!$aRS->EOF) {
					$intCatalogID = (int) @$aRS->fields['catalogid'];
					if ($intCatalogID > 0) {
						$arrCatalogIDs[] = $intCatalogID;
					}
					$aRS->MoveNext();
				}
			}
		}
		else {
			$arrCatalogIDs = $anArrayOfCatalogIDs;
		}
		
		//Si al finalizar la validación ningún catálogo se usa de manera estática, regresa la colección de imágenes vacía
		if (count($arrCatalogIDs) == 0) {
			return $attributesImagesList;
		}
		//@JAPR
		
		$arrayAttributes = array();
		//@JAPR 2017-03-03: Corregido un bug, los atributos con imágenes pero cuyos catálogos exclusivamente están asociados a filtros dinámicos, no deben descargar todas las imágenes
		//ya que no serán utilizadas necesariamente por el App o será bajo demanda cuando se resuelvan los filtros dinámicos (#6ZKO4O)
		//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
		$sql = "SELECT A.CatalogID, A.MemberID, A.MemberOrder 
			FROM SI_SV_CatalogMember A 
			WHERE A.CatalogID IN (".implode(',', $arrCatalogIDs).") AND A.IsImage > 0 
			ORDER BY A.CatalogID, A.MemberOrder";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n(getAttributesImageVersions) Query para catálogos con atributos imagen: {$sql}");
		}
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			while (!$aRS->EOF) {
				$tmpCatalogID = (int) @$aRS->fields['catalogid'];
				if (!isset($arrayAttributes[$tmpCatalogID])) {
					$arrayAttributes[$tmpCatalogID] = array();
				}
				
				$intCatMemberID = @$aRS->fields['memberid'];
				$arrayAttributes[$tmpCatalogID][$intCatMemberID] = $intCatMemberID;
				$aRS->MoveNext();
			}
		}
		
		//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
		//Modificado este código, por cada atributo de eBavel se estaba cargando su API y configuraciones así como verificando si era o no posible obtener datos
		//mediante ella, siendo que dicho proceso sólo era necesario ejecutarlo una única vez, así que se agregó una bandera para no repetirlo
		//Recorre todos los atributos imagen y extrae los valores de cada uno
		$blnEBavelAvailabilityVerified = false;
		$blnCanLoadeBavelImages = false;
		//@JAPR
		$arrImageExt = array('jpg'=>'jpg', 'png'=>'png', 'gif'=>'gif');
		foreach ($arrayAttributes as $intCatalogID => $arrCatMembers) {
			$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
			$objDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $objCatalog->DataSourceID);
			//Si no se encontró el catálogo o este es de eBavel y no se cuenta con los elementos necesarios para soportarlo, omite este atributo
			if (is_null($objCatalog) || ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0 && !$blnCanSaveeBavelImages)) {
				continue;
			}
			
			//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
			//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
			//@JAPR 2016-10-11: Corregido un bug, para eBavel 6.0 existe diferencia entre la ruta según el tipo de campo para el que se consulta, por lo tanto es necesario obtener la ruta
			//de todos los tipos de campo que pueden almacenar imagenes y decidir cual corresponde al atributo para el que se usará según el tipo de campo (#P04H52)
			$arreBavelPath = array('widget_photo' => '', 'widget_signature' => '', 'widget_qr' => '');
			$manager = null;
			//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
			//Modificado este código, por cada atributo de eBavel se estaba cargando su API y configuraciones así como verificando si era o no posible obtener datos
			//mediante ella, siendo que dicho proceso sólo era necesario ejecutarlo una única vez, así que se agregó una bandera para no repetirlo
			if (!$blnEBavelAvailabilityVerified && $objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0 && $objDataSource->eBavelAppCodeName != '') {
				$blnEBavelAvailabilityVerified = true;
				$blnCanLoadeBavelImages = true;
				require_once('settingsvariable.inc.php');
				require_once('eBavelIntegration.inc.php');
				$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'EBAVELURL');
				if (is_null($objSetting) || trim($objSetting->SettingValue == '')) {
					$blnCanLoadeBavelImages = false;
				}
				
				if ($blnCanLoadeBavelImages) {
					//Verica si el código de eBavel está disponible
					$strPath = '';
					$arrURL = parse_url($objSetting->SettingValue);
					$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
					$strPath = str_replace('/', '\\', $strPath);
					$strPath = str_replace('\\\\', '\\', $strPath);
					if (file_exists($strPath))
					{
						@require_once($strPath);
					}
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						if (!function_exists('NotifyError')) {
							echo("<br>\r\nNo NotifyError function");
						}
						else {
							echo("<br>\r\nNotifyError function exists");
						}
					}
					
					if (!class_exists('Tables'))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\nNo eBavel code loaded");
						}
						$blnCanLoadeBavelImages = false;
					}
				}
			}
			//@JAPR
			
			//Sólo si se trata de un DataSource de tipo eBavel y todo está correctamente configurado, creará la instancia de este objeto para obtener la ruta real de las imágenes
			//desde eBavel en el proceso siguiente
			$arreBavelFieldsColl = array();
			$streBavelPath = '';
			if ($objDataSource->eBavelAppCodeName != '' && $blnCanLoadeBavelImages) {
				$params = array();
				//@JAPR 2016-10-10: Corregido un bug, se había usado el nombre lógico en lugar del nombre físico para obtener la ruta de imagenes de eBavel (#P04H52)
				$params['sectionname'] = $objDataSource->eBavelFormName;
				//@JAPR
				$params['widgettype'] = 'widget_photo';
				$params['ext'] = 'jpg';
				
				//@JAPR 2017-01-12: Corregido un bug de compatibilidad hacia atrás, con eBavel v5 los métodos agregados para corregir el caso del issue #P04H52 no funcionan si no
				//se utiliza un FetchMode numérico, así que se había corregido para casos con eBavel V6 nada mas (#LUUWUS)
				$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
				//@JAPR
				$manager = new Tables();
				$manager->notifyErrorService = true;
				$manager->applicationCodeName = $objDataSource->eBavelAppCodeName;
				$manager->init( $aRepository->ADOConnection );
				
				//@JAPR 2016-10-11: Corregido un bug, para eBavel 6.0 existe diferencia entre la ruta según el tipo de campo para el que se consulta, por lo tanto es necesario obtener la ruta
				//de todos los tipos de campo que pueden almacenar imagenes y decidir cual corresponde al atributo para el que se usará según el tipo de campo (#P04H52)
				$streBavelPathTmp = $manager->getFilePath($params, null, true);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("getAttributesImageVersions getFilePath photo: {$streBavelPathTmp}, AppName: {$objDataSource->eBavelAppCodeName}");
				}
				$fileInfo = @pathinfo($streBavelPathTmp);
				if (is_null($fileInfo) || !is_array($fileInfo)) {
					$fileInfo = array('dirname' => $streBavelPathTmp);
				}
				$arreBavelPath['widget_photo'] = str_ireplace("/", "\\", (string) @$fileInfo['dirname']."/");
				$streBavelPath = $arreBavelPath['widget_photo'];
				
				$params['widgettype'] = 'widget_signature';
				$streBavelPathTmp = $manager->getFilePath($params, null, true);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("getAttributesImageVersions getFilePath signature: {$streBavelPathTmp}, AppName: {$objDataSource->eBavelAppCodeName}");
				}
				$fileInfo = @pathinfo($streBavelPathTmp);
				if (is_null($fileInfo) || !is_array($fileInfo)) {
					$fileInfo = array('dirname' => $streBavelPathTmp);
				}
				$arreBavelPath['widget_signature'] = str_ireplace("/", "\\", (string) @$fileInfo['dirname']."/");
				
				$params['widgettype'] = 'widget_qr';
				$streBavelPathTmp = $manager->getFilePath($params, null, true);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("getAttributesImageVersions getFilePath qr: {$streBavelPathTmp}, AppName: {$objDataSource->eBavelAppCodeName}");
				}
				$fileInfo = @pathinfo($streBavelPathTmp);
				if (is_null($fileInfo) || !is_array($fileInfo)) {
					$fileInfo = array('dirname' => $streBavelPathTmp);
				}
				$arreBavelPath['widget_qr'] = str_ireplace("/", "\\", (string) @$fileInfo['dirname']."/");
				
				//@JAPR 2017-01-12: Corregido un bug de compatibilidad hacia atrás, con eBavel v5 los métodos agregados para corregir el caso del issue #P04H52 no funcionan si no
				//se utiliza un FetchMode numérico, así que se había corregido para casos con eBavel V6 nada mas (#LUUWUS)
				$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
				//@JAPR
				
				//Obtiene la lista de campos de esta forma de eBavel para poder identificar el tipo de Widget que representa y así ajustar la ruta a utilizar para obtener la imagen
				$arreBavelFieldsColl = @GetEBavelFieldsForms($aRepository, true, $objCatalog->eBavelAppID, array($objCatalog->eBavelFormID));
				//@JAPR
			}
			
			if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl)) {
				$arreBavelFieldsColl = array();
			}
			//@JAPR
			
			$resultFilter = BITAMDataSourceFilter::generateResultFilterByUser($aRepository, $objCatalog->DataSourceID, $intUserID);
			//$resultFilter = $objCatalog->generateResultFilterByUser($intUserID);
			foreach ($arrCatMembers as $intCatMemberID) {
				$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $intCatMemberID);
				//@JAPR 2015-12-21: Corregido un bug, al cambiar el método usado para obtener los valores, el primer parámetro ya no correspondía con un array
				//de IDs por lo que dejó de funcionar correctamente este proceso. CatalogFilter ya no es requerido en este punto (#SS2EKF)
				//$aCatalogFilter = BITAMCatalogFilter::NewInstance($aRepository, $intCatalogID, true);
				if (is_null($aCatalogMember) /*|| is_null($aCatalogFilter)*/) {
					continue;
				}
				
				//$attribPosition = $aCatalogMember->MemberOrder -1;
				//@JAPR 2015-12-21: Corregido un bug, al cambiar el método usado para obtener los valores, el primer parámetro ya no correspondía con un array
				//de IDs por lo que dejó de funcionar correctamente este proceso (#SS2EKF)
				$arrAttributes = BITAMCatalog::GetCatalogAttributes($aRepository, $intCatalogID, 0, array($intCatMemberID));
				//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
				//Modificado el proceso para que el query de catálogos de eBavel pueda regresar la fecha de última modificación del registro (Sólo cuando se trata 
				//del agente de eForms que invoca a este proceso)
				$blnEBavelDataSource = false;
				$strLastModifiedDate = '';
				if (getParamValue('isagent', 'both', '(int)', true) && $objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
					$blnEBavelDataSource = true;
					$attribValues = $objDataSource->getAttributeValues(array_values($arrAttributes), $intUserID, $resultFIlter, 4, false, false, false, null, true, true);
				}
				else {
					$attribValues = $objDataSource->getAttributeValues(array_values($arrAttributes), $intUserID, $resultFIlter, 2);
				}
				//@JAPR
				//$attribValues = $aCatalogFilter->getCatalogValues($attribPosition, false, false, false, null, $resultFilter);
				//Los valores del catálogo que corresponde a imagenes, siempre son relativos al servicio de eForms que está corriendo, excepto
				//si se trata de un catálogo de eBavel, en ese caso se asume que son relativos a la ruta del servicio de eBavel configurado en
				//los settings de eForms, así que antes de realmente utilizar la imagen se debe aplicar el ajuste de path correspondiente
				unset($attribValues['sv_ALL_sv']);
				foreach ($attribValues as $value) {
					//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
					//Modificado el proceso, ahora según el tipo de DataSource, pudiera venir un array o sólo un valor puntual
					if ( $blnEBavelDataSource ) {
						$strValue = $value["value"];
						$strLastModifiedDate = (string) @$value["lastDate"];
					}
					else {
						$strValue = $value;
					}
					$fileInfo = @pathinfo($strValue);
					//@JAPR
					if (is_null($fileInfo) || !is_array($fileInfo)) {
						$fileInfo = array();
					}
					
					$fileExt = strtolower((string) @$fileInfo['extension']);
					if ($fileExt != '') {
						if (isset($arrImageExt[$fileExt])) {
							$fileName = (string) @$fileInfo['basename'];
							if ($fileName) {
								$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
								//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
								//Modificado el proceso, ahora según el tipo de DataSource, pudiera venir un array o sólo un valor puntual
								if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
									//Si el catálogo es de eBavel, entonces la ruta es relativa a dicho servicio
									$urlimg = $streBavelService.$strValue;
								}
								else {
									//Si el catálogo es de eForms, entonces la ruta es relativa al servicio de eForms
									$urlimg = $streFormsService.$strValue;
								}
								//@JAPR
								$urlimg = str_ireplace("\\", "/", $urlimg);
								
								//@JAPR 2014-12-22: Corregido un bug, no estaba usando la ruta real sino sólo la porción relativa, por lo tanto no
								//generaba la ruta completa al servicio
								//@JAPR 2016-09-14: Corregido un bug, a partir de eBavel 6.0, las imágenes asociadas a los campos de sus formas ya no se almacenan físicamente en el mismo lugar que en
								//las versiones anteriores, por lo que se debe utilizar este proceso para obtener la ruta físcia real y así poder obtener los datos de fecha y hora (#P04H52)
								if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
									//@JAPR 2016-10-11: Corregido un bug, para eBavel 6.0 existe diferencia entre la ruta según el tipo de campo para el que se consulta, por lo tanto es necesario obtener la ruta
									//de todos los tipos de campo que pueden almacenar imagenes y decidir cual corresponde al atributo para el que se usará según el tipo de campo (#P04H52)
									$strTagName = (string) @$arreBavelFieldsColl[$aCatalogMember->eBavelFieldID]['tagname'];
									$streBavelPathTmp = (string) @$arreBavelPath[$strTagName];
									//En caso de algún tipo de error o que no se hubiera podido recuperar el path, usará el path default que corresponde al original para campos foto
									if (!$streBavelPathTmp) {
										$streBavelPathTmp = $streBavelPath;
									}
									
									//Ahora el $value contiene el nombre del archivo pero con parte de la ruta donde se encuentra, así que se utilizará $fileName que sólo tiene el nombre
									//del archivo, ya que se concatenará la ruta obtenida según el tipo de campo
									$strFilePath = $streBavelPathTmp.$fileName;
									//@JAPR
								}
								else {
									$strFilePath = GetPhysicalFilePathFromURL($urlimg);
								}
								//@JAPR
								$strVersionNum = GetFileLastModTimestamp(urldecode($strFilePath), 1);
								$attributesImagesList[$fileName] = array();
								$attributesImagesList[$fileName]['name'] = $fileName;
								$attributesImagesList[$fileName]['src'] = ($blnWebMode) ? $urlimg : urldecode($urlimg);
								$attributesImagesList[$fileName]['version'] = GetFileLastModTimestamp(urldecode($strFilePath), 1);
								//@JAPR 2018-05-17: Optimizada la carga de eForms (#G87C4W)
								//Sólo cuando se trata del agente de eForms que invoca a este proceso, asignará el DataSource del cual se extrajo la imagen
								//En este caso se trata de una imagen de definición, así que utiliza un DataSource dummy
								if (getParamValue('isagent', 'both', '(int)', true)) {
									$attributesImagesList[$fileName]['did'] = $objCatalog->DataSourceID;
									if ( $blnEBavelDataSource ) {
										$attributesImagesList[$fileName]['lastDate'] = $strLastModifiedDate;
									}
								}
								//@JAPR
							}
						}
					}
				}
			}
		}
	}
	
	return $attributesImagesList;
}

/* Carga el archivo de idioma y lo regresa a la aplicación que lo solicita en formato JSON
*/
function GetLanguageFile($aRepository) {
	$strLanguage = getParamValue('language', 'both', "(string)", true);
	$strLanguage = trim(strtolower($strLanguage));
	$return['error'] = false;
	if ($strLanguage == '') {
        $return['error'] = true;
        $return['errmsg'] = 'Language not specified';
		return $return;
	}
	
	$strFileName = $strLanguage.".lang";
	$path = getcwd()."\\"."language";
	clearstatcache();
	if (!file_exists($path)) {
		if (!mkdir($path, null, true)) {
	        $return['error'] = true;
    	    $return['errmsg'] = 'Language path could not be created';
			return $return;
		}
	}
	
	$strCompletePath = $path."\\".$strFileName;
	if (!file_exists($strCompletePath)) {
        $return['error'] = true;
        $return['errmsg'] = 'Language file not found: '.$strCompletePath;
		return $return;
	}
	
	$strJSON = @file_get_contents($strCompletePath);
	if ($strJSON === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error reading the language file';
		return $return;
	}
	
	//Termina sin error después de confirmar que el archivo existe y se puede leer su contenido
	$strJSON = str_replace("\r\n",'',$strJSON);
	$intPos = strpos($strJSON, '{');
	if ($intPos !== false) {
		$strJSON = substr($strJSON, $intPos);
	}
	$return['data'] = str_replace("\r\n",'',$strJSON);
	return $return;
}

//@JAPR 2014-12-17: Agregado el valor default para preguntas tipo Foto y Sketch
/* Recibe un archivo de sketch y lo coloca en la ruta especificada según los parámetros de quien lo envía
*/
function uploadSketch($aRepository) {
	$return['error'] = false;
	
	$strFileName = trim(getParamValue('filename', 'both', "(string)"));
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$strImgData = getParamValue('data', 'both', "(string)", true);
	if (is_null($strImgData)) {
        $return['error'] = true;
        $return['errmsg'] = 'No data was received for this sketch';
        return $return;
	}
	
	if ($surveyID <= 0 || $questionID <= 0 || $strFileName == '') {
        $return['error'] = true;
        $return['errmsg'] = 'No source question data was provided for this sketch';
        return $return;
	}
	
    $path = getcwd()."\\"."tmpsurveyimages\\".$surveyDate.$surveyHour;
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
	        $return['error'] = true;
	        $return['errmsg'] = 'Sketch image path could not be created';
	        return $return;
		}
	}
	
	$strFileName .= '.jpg';
	$strCompletePath = $path."\\".$strFileName;
	$status = file_put_contents($strCompletePath, base64_decode($strImgData));
	if (!$status) {
		$strError = error_get_last();
		if (isset($strError['message'])) {
			$strError = (string) @$strError['message'];
		}
		else {
			$strError = '';
		}
        $return['error'] = true;
        $return['errmsg'] = 'There was an error while saving the sketch data'.($strError)?':'.$strError:'';
        return $return;
	}
	
	$path = "tmpsurveyimages/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
	$newPath = $path.$strFileName;
	$result["path"] = $newPath;
	$result["surveyID"] = $surveyID;
	$result["questionID"] = $questionID;
	
	// to pass data through iframe you will need to encode all html tags
	//echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	return $result;
}

/* Recibe un archivo de foto y lo coloca en la ruta especificada según los parámetros de quien lo envía
*/
function uploadPhoto($aRepository) {
	$return['error'] = false;
	
	require_once('qqFileUploader.class.php');
	//$sectionName = $_REQUEST["sectionName"];
	//echo "{success:true, sectionName:'{$sectionName}', codeName:'{$applicationCodeName}'}";
	
	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array();
	// max file size in bytes
	$sizeLimit = SV_PHOTO_SIZELIMIT;	//10 * 1024 * 1024;
	
	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	
	if ($surveyID <= 0 || $questionID <= 0) {
        $return['error'] = true;
        $return['errmsg'] = 'No source question data was provided for this photo';
        return $return;
	}
    
    $strTMPName = "photo_".$surveyID."_".$questionID."_".$_SESSION["PABITAM_UserID"].".jpg";
    //$path = "surveytmpimages/".$theRepositoryName."/".$strPath;
    $path = "tmpsurveyimages/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
	if(!is_dir($path))	
	{
		mkdir($path, null, true);
	}
    //$path = "tmpsurveyimages/";
	
	$result = $uploader->handleUpload($path);
	
	$newPath = $path . @$result["filename"];
	$result["path"] = $newPath;
	$result["surveyID"] = $surveyID;
	$result["questionID"] = $questionID;

	// to pass data through iframe you will need to encode all html tags
	//echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	return $result;
}

/* Recibe un archivo de audio y lo coloca en la ruta especificada según los parámetros de quien lo envía
*/
function uploadAudio($aRepository) {
	$return['error'] = false;
	require_once('qqFileUploader.class.php');
	
	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array();
	// max file size in bytes
	$sizeLimit = SV_DOCUMENT_SIZELIMIT;
	
	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$audioFileName = getParamValue('audioFileName', 'both', "(string)");
	if(!$audioFileName){
		$audioFileName = '';
	}
	if ($surveyID <= 0 || $questionID <= 0) {
        $return['error'] = true;
        $return['errmsg'] = 'No source question data was provided for this audio';
        return $return;
	}
    
    $strTMPName = $audioFileName;
    //$path = "surveytmpimages/".$theRepositoryName."/".$strPath;
    $path = "syncdata/".$_SESSION["PABITAM_RepositoryName"]."/";
	if(!is_dir($path))	
	{
		mkdir($path, null, true);
	}
    //$path = "tmpsurveyimages/";
	
	$result = $uploader->handleUpload($path);
	if($audioFileName == ''){
		
		$newPath = $path . @$result["filename"];
		$result["path"] = $newPath;
		$result["surveyID"] = $surveyID;
		$result["questionID"] = $questionID;
	}
	else {
		$strTMPName = $audioFileName;
		$newPath = $path . @$result["filename"];
		//tenemos que renombrar el archivo
		rename($newPath , $path.$strTMPName);
		//$result["path"] = $newPath;
		$result["path"] = $path.$strTMPName; //se sobreescribe el path 
		$result["surveyID"] = $surveyID;
		$result["questionID"] = $questionID;
	}
	// to pass data through iframe you will need to encode all html tags
	//echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	return $result;
}


/* Recibe un archivo de audio y lo coloca en la ruta especificada según los parámetros de quien lo envía
*/
function uploadVideo($aRepository) {
	$return['error'] = false;
	require_once('qqFileUploader.class.php');
	//$sectionName = $_REQUEST["sectionName"];
	//echo "{success:true, sectionName:'{$sectionName}', codeName:'{$applicationCodeName}'}";
	
	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array();
	// max file size in bytes
	$sizeLimit = SV_DOCUMENT_SIZELIMIT;
	
	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$videoFileName = getParamValue('videoFileName', 'both', "(string)");
	//$path = "surveytmpimages/".$theRepositoryName."/".$strPath;
    $path = "syncdata/".$_SESSION["PABITAM_RepositoryName"]."/"; 

	if ($surveyID <= 0 || $questionID <= 0) {
        $result['error'] = true;
        $result['errmsg'] = 'No source question data was provided for this video';
        return $result;
	}
    
	if (!is_dir($path)) {
		if (!mkdir($path, null, true)) {
	        $result['error'] = true;
	        $result['errmsg'] = 'Video path could not be created';
	        return $result;
		}
	}
	
	$result = $uploader->handleUpload($path);
	if (!$result) {
		$strError = error_get_last();
		if (isset($strError['message'])) {
			$strError = (string) @$strError['message'];
		} else {
			$strError = '';
		}
        $result['error'] = true;
        $result['errmsg'] = 'There was an error while saving the video data'.($strError)?':'.$strError:'';
        return $result;
	}

	if ($videoFileName == '') {
		$newPath = $path . @$result["filename"];

		$result['error'] = true;
        $result['errmsg'] = 'No file name was provided for this video';
		$result['path'] = $newPath;
		$result['surveyID'] = $surveyID;
		$result['questionID'] = $questionID;
        return $result;
	} else {
		$strTMPName = $videoFileName;
		$newPath = $path . @$result["filename"];
		//tenemos que renombrar el archivo
		rename($newPath , $path.$strTMPName);
		$result['path'] = $path.$strTMPName; //se sobreescribe el path 
		$result['surveyID'] = $surveyID;
		$result['questionID'] = $questionID;
		// to pass data through iframe you will need to encode all html tags
		//echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		return $result;
	}
}



/* Recibe un archivo de documento y lo coloca en la ruta especificada según los parámetros de quien lo envía
*/
function uploadDocument($aRepository) {
	$return['error'] = false;

	require_once('qqFileUploader.class.php');
	//$sectionName = $_REQUEST["sectionName"];
	//echo "{success:true, sectionName:'{$sectionName}', codeName:'{$applicationCodeName}'}";

	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array();
	// max file size in bytes
	$sizeLimit = SV_DOCUMENT_SIZELIMIT;

	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

	$surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);

	if ($surveyID <= 0 || $questionID <= 0) {
		$return['error'] = true;
		$return['errmsg'] = 'No source question data was provided for this document';
		return $return;
		//die("{'error':'No source question data was provided for this document'}");
	}

	$strTMPName = "document_".$surveyID."_".$questionID."_".$_SESSION["PABITAM_UserID"].".jpg";
	//$path = "surveytmpdocuments/".$theRepositoryName."/".$strPath;
	$path = "tmpsurveydocuments/".$surveyDate.$surveyHour.(($surveyDate.$surveyHour != '')?"/":"");
	if(!is_dir($path))
	{
		mkdir($path, null, true);
	}
	//$path = "tmpsurveyimages/";

	$result = $uploader->handleUpload($path);

	$newPath = $path . @$result["filename"];
	$result["path"] = $newPath;
	$result["surveyID"] = $surveyID;
	$result["questionID"] = $questionID;

	// to pass data through iframe you will need to encode all html tags
	//echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	return $result;
}

/* Recibe un archivo de foto (como B64) enviado durante el proceso de sincronización y lo coloca en la ruta temporal del proceso según los 
parámetros de quien lo envía, para que al momento de recibirse la captura de encuesta se pueda tomar y agregar a las tablas necesarias
*/
function uploadSyncSignatureData($aRepository) {
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	//$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	if (is_null($strFileName) || trim($strFileName) == '') {
		//Genera el nombre de la foto a partir de los parámetros
		$strFileName = 'signature_'.$kpiUser.'_'.$surveyDate.'_'.$surveyHour.'_'.$surveyID;//.'_'.$questionID+'_'.$recordNum;
	}
	if (strpos(strtolower($strFileName), '.jpg') === false) {
		$strFileName .= '.jpg';
	}
	
	if ($surveyID <= 0 ) { // || $questionID <= 0) {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'No source question data was provided for this signature';
        return $return;
        */
	    die("errmsg=No source question data was provided for this signature");
	}
    
	$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
			/*
	        $return['error'] = true;
	        $return['errmsg'] = 'Project synchronization path could not be created';
	        return $return;
	        */
		    die("errmsg=Project synchronization path could not be created");
		}
	}
    
	//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
	$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$strCompletePath = $path."\\".$strFileName;
	$insertImage = getParamValue('data', 'post', "(string)", true);
	if (is_null($insertImage)) {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'No data was received for this signature';
        return $return;
        */
	    die("errmsg=No data was received for this signature");
	}
	$status = file_put_contents($strCompletePath, base64_decode($insertImage));

	/*
	//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma
	if ($aQuestion->QTypeID == qtpSignature)
	{
		//Si la pregunta es tipo firma, realmente lo que llega es un .png, pero se disfraza como .jpg
		//En este caso NO se va a renombrar el archivo a Signature_####_###, se dejará como si fuera una Foto de cualquier pregunta
		$status = true;
	}
	//@JAPR
	else
	{
	*/
	
	//La firma viene de un Canvas el cual no sale rotado, por lo que no es necesario rotar para corregir la posición
	/*
	if(!preg_match('/android/i',$nav) && ($insertImage != "photoimage" && $insertImage != '')) {
		//la imagen se guarda rotada, se debe volver a rotar en el servidor
		$origen = imagecreatefromjpeg($strCompletePath);
		//$rotar = imagerotate($origen, -90, 0);
		//$status = imagejpeg($rotar, $strCompletePath);
		$status = imagejpeg($origen, $strCompletePath);
	}
	*/
	//}
	if ($status) {
		//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
		//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
		$result = 'Survey data was saved successfully.';	//'OK';
		header("Content-Type: text/plain");
		echo($result);
		//@JAPR
		/*
		$result["path"] = $path;
		$result["surveyID"] = $surveyID;
		$result["questionID"] = $questionID;
		*/
	}
	else {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'There was an error while saving the signature data';
        return $return;
        */
	    die("errmsg=There was an error while saving the signature data");
	}
	
	return $result;
}

/* Recibe un archivo de foto (como B64) enviado durante el proceso de sincronización y lo coloca en la ruta temporal del proceso según los 
parámetros de quien lo envía, para que al momento de recibirse la captura de encuesta se pueda tomar y agregar a las tablas necesarias
*/
function uploadSyncPhotoData($aRepository) {
	global $appVersion;
	
	require_once('question.inc.php');
	
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	if (is_null($strFileName) || trim($strFileName) == '') {
		//Genera el nombre de la foto a partir de los parámetros
		$strFileName = 'photo_'.$kpiUser.'_'.$surveyDate.'_'.$surveyHour.'_'.$surveyID.'_'.$questionID+'_'.$recordNum;
	}
	if (strpos(strtolower($strFileName), '.jpg') === false) {
		$strFileName .= '.jpg';
	}
	
	if ($surveyID <= 0 || $questionID <= 0) {
	    die("errmsg=No source question data was provided for this photo");
	}
    
	$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
			/*
	        $return['error'] = true;
	        $return['errmsg'] = 'Project synchronization photo path could not be created';
	        return $return;
	        */
		    die("errmsg=Project synchronization path could not be created");
		}
	}
    
	//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
	$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$strCompletePath = $path."\\".$strFileName;
	$insertImage = getParamValue('data', 'both', "(string)", true);
	if (is_null($insertImage)) {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'No data was received for this photo';
        return $return;
        */
	    die("errmsg=No data was received for this photo");
	}
	$status = file_put_contents($strCompletePath, base64_decode($insertImage));

	/*
	//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma
	if ($aQuestion->QTypeID == qtpSignature)
	{
		//Si la pregunta es tipo firma, realmente lo que llega es un .png, pero se disfraza como .jpg
		//En este caso NO se va a renombrar el archivo a Signature_####_###, se dejará como si fuera una Foto de cualquier pregunta
		$status = true;
	}
	//@JAPR
	else
	{
	*/
	//La firma viene de un Canvas el cual no sale rotado, por lo que no es necesario rotar para corregir la posición
	$aQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $questionID);
	if (is_null($aQuestion)) {
	    //@JAPR 2015-07-29: Removida esta validación, si la pregunta ya no existe en el servicio, el archivo será ignorado pero se debe poder procesar el outbox sin generar error
		$status = true;
		//$status = false;
		//@JAPR
	}
	elseif ($appVersion < 3.00018 && $aQuestion->QTypeID != qtpSignature && $aQuestion->QTypeID != qtpSketch && !preg_match('/android/i',$nav) && ($insertImage != "photoimage" && $insertImage != '')) {
	//if(!preg_match('/android/i',$nav) && ($insertImage != "photoimage" && $insertImage != '')) {
		//la imagen se guarda rotada, se debe volver a rotar en el servidor
		//$origen = imagecreatefromjpeg($strCompletePath);
		//$rotar = imagerotate($origen, -90, 0);
		//$status = imagejpeg($rotar, $strCompletePath);
	}
	//}
	if ($status) {
		//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
		//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
		$result = 'Survey data was saved successfully.';	//'OK';
		header("Content-Type: text/plain");
		echo($result);
		//@JAPR
		/*
		$result["path"] = $path;
		$result["surveyID"] = $surveyID;
		$result["questionID"] = $questionID;
		*/
	}
	else {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'There was an error while saving the photo data';
        return $return;
        */
	    die("errmsg=There was an error while saving the photo data");
	}
	
	return $result;
}

/*Función encargada de sincronizar todos los documentos de la aplicación móvil
*/
//OMMC 2016-02-03: Agregado syncDocuments
function uploadSyncDocumentData($aRepository) {
	global $appVersion;
	
	require_once('question.inc.php');
	
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$recordNum = getParamValue('recordNum', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	$strFileType = getParamValue('fileType', 'both', "(string)");
	$strFileExt = getParamValue('fileExt', 'both', "(string)");
	if (is_null($strFileName) || trim($strFileName) == '') {
		//Genera el nombre de la foto a partir de los parámetros
		$strFileName = 'document_'.$kpiUser.'_'.$surveyDate.'_'.$surveyHour.'_'.$surveyID.'_'.$questionID+'_'.$recordNum;
	}

	if ($surveyID <= 0 || $questionID <= 0) {
	    die("errmsg=No source question data was provided for this document");
	}
    
	$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
		    die("errmsg=Project synchronization path could not be created");
		}
	}
    
	//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
	$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';

	//OMMC 2016-05-23: Cuando es un video se le debe concatenar la extensión
	if ($strFileType == 'video') {
		$strCompletePath = $path."\\".$strFileName.$strFileExt;
	} else {
		$strCompletePath = $path."\\".$strFileName;	
	}
	
	$insertDocument = getParamValue('data', 'both', "(string)", true);
	if (is_null($insertDocument)) {
	    die("errmsg=No data was received for this document");
	}
	
	//OMMC 2016-01-25: Dependiendo del tipo de documento es lo que hará, en caso de video sólo mueve el archivo, en caso de foto tiene que extraer el base64 antes.
	switch($strFileType){
		case 'photo':
			$status = file_put_contents($strCompletePath, base64_decode($insertDocument));	
		break;
		case 'video':
			$status = file_put_contents($strCompletePath, $insertDocument);
		break;
	}
	

	$aQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $questionID);
	if (is_null($aQuestion)) {
	    //@JAPR 2015-07-29: Removida esta validación, si la pregunta ya no existe en el servicio, el archivo será ignorado pero se debe poder procesar el outbox sin generar error
		$status = true;
		//@JAPR
	}

	if ($status) {
		//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
		//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
		$result = 'Survey data was saved successfully.';	//'OK';
		header("Content-Type: text/plain");
		echo($result);
		//@JAPR
	}
	else {
	    die("errmsg=There was an error while saving the document data");
	}
	
	return $result;
}

/* Recibe un archivo de outbox (como JSOn) enviado durante el proceso de sincronización y lo coloca en la ruta temporal del proceso según los
parámetros de quien lo envía, una vez hecho eso inicia el proceso de sincronización invocando al saveAnswersMobile2 pero sólo hasta después de
haber formateado los datos que recibió tal como el proceso original los esperaba. Al termina el proceso deberá de eliminar todos los archivos
tamporales recibidos (outbox, fotos y demás)
*/
function uploadOutboxData($aRepository, $theUser = null) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("function uploadOutboxData", 1, 1, "color:purple;");
	}
	
	//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	global $gstrOutboxFilePath;
	global $gstrOutboxFileName;
	global $dieOnError;
	//@JAPR 2013-06-21: Agregado el agente de eForms
	global $blnIsAgentEnabled;
	global $arrDBsExcludedFromAgent;
	global $arrDBsEnabledForAgent;
	global $BITAMRepositories;
	//@JAPR 2017-09-21: Agregado el array para forzar a generar tareas del agente a pesar de que el repositorio se encuentre excluído dentro de $arrDBsExcludedFromAgent,
	//ya que se generará un agente específico que procesará las tareas de dicho repositorio, sólo el agente general NO los procesará (#0LC7NT)
	global $arrDBsForcedForAgent;
	if (!isset($arrDBsForcedForAgent)) {
		$arrDBsForcedForAgent = array();
	}
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	global $arrEventParams;
	global $giOperationID;
	//@JAPR
	
	//Formato del nombre d archivo: 'surveyanswers_' + SettingsMngr.user + '_' + SurveyCls.surveyDate + '_' + SurveyCls.surveyHour + '_' + SurveyCls.id + '_' + AgendaCls.id + '_finished.dat'
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
	$kpiPass = getParamValue('Password', 'both', "(string)");
    $surveyID = getParamValue('surveyID', 'both', "(int)");
    $agendaID = getParamValue('agendaID', 'both', "(int)");
	$questionID = getParamValue('questionID', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$surveyDate = str_replace(array('/', '-', ' '), '', $surveyDate);
	$surveyHour = str_replace(array(':', ' '), '', $surveyHour);
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	$blnEdit = (bool) getParamValue('edit', 'both', "(int)");
	
	if (is_null($strFileName) || trim($strFileName) == '') {
		//Genera el nombre del outbox a partir de los parámetros
		$strFileName = 'surveyanswers_'.$kpiUser.'_'.$surveyDate.'_'.$surveyHour.'_'.$surveyID.(($agendaID > 0)?'_'.$agendaID:'').'_finished.dat';
	}
	if (strpos(strtolower($strFileName), '.dat') === false) {
		$strFileName .= '.dat';
	}
	
	if ($surveyID <= 0 || $surveyDate == '' || $surveyHour == '') {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'Not enough survey data was provided to store this outbox';
        return $return;
        */
	    die("errmsg=Not enough survey data was provided to store this outbox");
	}
    
	$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
			/*
	        $return['error'] = true;
	        $return['errmsg'] = 'Project synchronization path could not be created';
	        return $return;
	        */
		    die("errmsg=Project synchronization path could not be created");
		}
	}
	
	//@JAPR 2018-09-25: Agregado el archivo log con el detalle de los Uploads realizados durante la sincronización (#GPS1S4)
	//Graba el archivo con el detalle del upload de todos los demás archivos que fueron enviados al servidor, incluyendo aquellos que por alguna razón fallaron
	$strFileLogData = getParamValue('filesLog', 'both', "(string)", true);
	if ( !is_null($strFileLogData) && is_string($strFileLogData) && trim($strFileLogData) != '' ) {
		$strFileLogDataPath = $path."\\".str_ireplace(".dat", ".log", $strFileName);
		$blnFileLogDataStatus = @file_put_contents($strFileLogDataPath, $strFileLogData);
	}
	//@JAPR
	
	$strCompletePath = $path."\\".$strFileName;
	$strOutboxData = getParamValue('data', 'both', "(string)", true);
 	$blnTestURL = getParamValue('testurl', 'both', "(int)", false);
	if ((is_null($strOutboxData) || trim($strOutboxData) == '') && !$blnTestURL) {
	    die("errmsg=No data was received for this outbox");
	}
	if ($blnTestURL) {
		$status = true;
		//@JAPR 2013-02-28: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		//Si ya está haciendo el proceso de ReUpload, entonces se necesita reportar los errores tal como ocurran
		$dieOnError = true;
		//@JAPR 2015-12-02: Corregido un bug, cuando se procesaban las tareas con el agente, entraba a este If por el parámetro testurl, así que nunca asignaba el nombre de archivo
		$gstrOutboxFileName = $strFileName;
		//@JAPR
	}
	else {
		//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		$gstrOutboxFilePath = $path;
		$gstrOutboxFileName = $strFileName;
		//@JAPR
		$status = file_put_contents($strCompletePath, $strOutboxData);
		
		//@JAPR 2018-10-17: Agregado el proceso de revisión de archivos subidos desde el dispositivo móvil para generar un error en caso de que no se encuentre alguno (#4C9HAY)
		if ( !CheckUploadedFilesForOutbox($path, $strOutboxData) ) {
			die("errmsg=There was an error while saving the outbox data, some files are missing, please contact your System Administrator");
		}
		//@JAPR
	}
	
	if ($status) {
		//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		//Ahora creará una carpeta adicional donde se respaldarán los archivos en caso de error
		$path = getcwd()."\\"."syncerrors\\".trim($_SESSION["PABITAM_RepositoryName"]);
		if (!file_exists($path))
		{
			if (!mkdir($path, null, true))
			{
				/*
		        $return['error'] = true;
		        $return['errmsg'] = 'Project synchronization path could not be created';
		        return $return;
		        */
			    die("errmsg=Project synchronization error path could not be created");
			}
		}
		
		//@JAPR 2013-06-21: Agregado el agente de eForms
		//Si está configurado el agente, entonces el proceso de sincronización se interrumpirá en este punto pues ya terminó de grabar el
		//archivo de la sincronización, excepto si el parámetro $blnTestURL está asignado porque eso significaría que se trata ya sea del proceso
		//de ReUpload, de una sincronización manual realizada en caso de depuración o error previo, o bien que se trata del propio Agente de eForms,
		//Aunque en este último caso también se agregará una nueva variable $blnIsAgent para indicarlo
		$blnIsAgent = getParamValue('isagent', 'both', "(int)", false);
		$captureVia = getParamValue('CaptureVia', 'both', "(int)", false);
		if (!$blnTestURL && $blnIsAgentEnabled && !$blnEdit) {	//&& $captureVia == 0
			/*
			$blnIsAgentEnabled = false;
			$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ENABLEEFORMSAGENT');
			if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
			{
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nNo eForms Agent is active for this repository");
				}
			}
			else {
				$blnIsAgentEnabled = (bool) (int) @$objSetting->SettingValue;
			}
			*/
			
			$theRepositoryName = '';
			$theUserName = '';
			try {
			    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
			    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
				//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
				if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
			        header('HTTP/1.0 401 Unauthorized');
			        echo $strPrjAndUser;
			        exit();
			    }
				
			    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
			    $strRepositoryName = $arrayPrjUsr[0];
			    $strUserName = $arrayPrjUsr[1];
				
			    $theRepositoryName = $strRepositoryName;
			    $theUserName = $strUserName;
			} catch (Exception $e) {
				//Este error no se reporta en log ni vía EMail, pero no puede continuar si no se logró cargar la información así que no 
				//continuará, ya que de todas maneras el proceso original de Upload también hubiera fallado de esta manera si no hubiera
				//leído los mismos datos
		        header('HTTP/1.0 401 Unauthorized');
		        echo "Error reading user info from kpionline";
		        exit();
			}
			
			//En este punto verifica si el repositorio de esta captura no se encuentra entre los excluidos, si fuera así entonces no agrega la
			//tarea sino que continua con el grabado como se hacía anteriormente
			//@JAPR 2017-09-21: Agregado el array para forzar a generar tareas del agente a pesar de que el repositorio se encuentre excluído dentro de $arrDBsExcludedFromAgent,
			//ya que se generará un agente específico que procesará las tareas de dicho repositorio, sólo el agente general NO los procesará (#0LC7NT)
			//Ahora adicionalmente verificará que el repositorio NO se encuentre habilitado para forzar el agente, antes de decidir grabar en línea la captura
			if (isset($arrDBsExcludedFromAgent[strtoupper($theRepositoryName)]) && !isset($arrDBsForcedForAgent[strtoupper($theRepositoryName)])) {
				//Se debe excluir este repositorio
				$blnIsAgentEnabled = false;
			}
			//@JAPR 2013-06-28: Agregada la lista de bases de datos que son las únicas que van a usar el Agente. Sólo aplica si hasta este punto
			//aun se consideraba habilitado el agente para la base de datos actualmente procesada, si es así, se verifica que la lista de bases
			//de datos con el agente habilitado esté vacia (lo cual significa que todas lo tienen habilitado), o bien que esta base de datos si
			//se encuentre en dicha lista
			if ($blnIsAgentEnabled) {
				if (!isset($arrDBsEnabledForAgent) || !is_array($arrDBsEnabledForAgent)) {
					$arrDBsEnabledForAgent = array();
				}
				
				//@JAPR 2016-01-14: Modificado para que ahora el Agente sólo procese aquellas tareas que pertenecen exclusivamente a repositorios que están configurados en este
				//servicio donde está corriendo el archivo php del Agente, independientemente de si dicho repositorio existe en el array $arrDBsEnabledForAgent, dicho array sólo
				//servirá a partir de ahora para limitar las BDs que sean las únicas que se deban procesar con el Agente, lo cual en v6 es completamente irrelevante ya que todo
				//repositorio debería ser procesado precisamente por el agente, así que con este cambio se podrá dejar $arrDBsEnabledForAgent completamente vacío, sólo llenar
				//$arrDBsExcludedFromAgent con repositorios que por alguna extraña razón se quisieran poner en Stand By (es decir, no procesar sus capturas) y simplemente va a
				//procesar todos los repositorios que se encuentren en el archivo .ini (#WUH8DI)
				//Para facilitar las cosas, simplemente se sobreescribirá internamente el array $arrDBsEnabledForAgent con los repositorios del archivo EKT_cfg.ini, de tal manera
				//que lo configurado manualmente en el archivo eFormsAgentConfig.inc.php será completamente irrelevante, así todos los repositorios del archivo .ini se considerarán
				//válidos para procesar, excepto si están configurados en el array $arrDBsExcludedFromAgent
				$arrDBsEnabledForAgent = array();
				//En caso de haberse leído por lo menos un repositorio del archivo .ini, los agrega como repositoriso válidos tal como hubieran estado en el archivo eFormsAgentConfig.inc.php
				if (isset($BITAMRepositories) && is_array($BITAMRepositories)) {
					//@JAPR 2016-04-01: Corregido un bug, se estaba sobreescribiendo $aRepository en este ciclo
					foreach ($BITAMRepositories as $aRepositoryName => $aRepositoryTmp) {
						$strAgentRepositoryName = strtoupper((string) $aRepositoryName);
						$arrDBsEnabledForAgent[$strAgentRepositoryName] = 1;
					}
				}
				//@JAPR
				
				if (count($arrDBsEnabledForAgent) > 0) {
					if (!isset($arrDBsEnabledForAgent[strtoupper($theRepositoryName)])) {
						$blnIsAgentEnabled = false;
					}
				}
				//@JAPR
			}
			//@JAPR
			
			//Si se encuentra activo el Agente de eForms, por lo tanto agrega una nueva tarea de carga para que el agente la procese mas adelante
			//y ya no continua con la carga del outbox en este instante, regresando simplemente el status de Ok
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			//En caso de estar grabando los datos desde una pregunta Update Data, no se generará tarea del Agente sino que la
			//procesará en este momento
			$intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
			if ($blnIsAgentEnabled && !$intDataDestinationID) {
				$statlogfile = "./uploaderror/eforms.log";
				$friendlyDate = date('Y-m-d H:i:s');
				$strSurveyDate = getParamValue('surveyDate', 'both', "(string)");
				$strSurveyHour = getParamValue('surveyHour', 'both', "(string)");
				if($strSurveyDate != '' && $strSurveyHour != '')
				{
					$lastDate = substr($strSurveyDate, 0, 10)." ".substr($strSurveyHour, 0, 8);
				} else {
					$lastDate = $friendlyDate;
				}
				
				//Si logró llegar a este punto, quiere decir que si hay conexión a la FBM000 y se logró identificar al usuario que hizo el request,
				//por lo que es seguro continuar
				$strStatus = "";
				try {
					$strStatus = addEFormsAgentTask($strFileName, $theRepositoryName);
				} catch (Exception $e) {
					$strStatus = $e->getMessage();
				}
				
				//Si todo estuvo Ok, registra el log y termina el proceso, de lo contrario envía el reporte vía EMail y dependiendo de si es o no
				//una captura vía móvil, regresa un código de error o le permite continuar ($dieOnError)
			    if ($strStatus == "OK") {
			        $strStatus = "Survey data was saved successfully.";
					$stat_action = "[".$friendlyDate."] [Action: Agent task configured] [UserID: ".$kpiUser."] [UserName: ".$theUserName."] [Repository: ".$theRepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
					@error_log($stat_action, 3, $statlogfile);
			
			    } else {
					$stat_action = "[".$friendlyDate."] [Action: Agent task error] [UserID: ".$kpiUser."] [UserName: ".$theUserName."] [Repository: ".$theRepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."] [Error: {$strStatus}]\r\n";
					@error_log($stat_action, 3, $statlogfile);
			    	
					$dieOnError = !((bool) @backUpSyncOutboxData());
			    	
					if (!$dieOnError) {
						try {
							//@JAPR 2014-12-10: Corregido un bug, estaba usando la variable de repositorio equivocada
							@reportSurveyEntryError($aRepository, $strStatus, $theUser);
						}
						catch (Exception $e) {
							//No reporta mas errores, continua para indicar al App que todo estuvo ok pues para este momento ya respaldó los archivos
							//con los que se puede restaurar la captura
						}
					}
					
					if ($dieOnError) {
						$strStatus = "There was an error while saving the outbox, if you do not found it on the reports page, please contact your System Administrator. ".$strStatus;
					}
					else {
						$strStatus = "Survey data was saved successfully.";
					}
			    }
				
			    header("Content-Type: text/plain");
			    echo($strStatus);
				$arrEventParams["ProcessID"] = procAddAgentTask;
				$arrEventParams["Description"] = "Create agent task: {$strFileName}";
				$arrEventParams["DBServer"] = $aRepository->DataADODBServer;
				$arrEventParams["MemoryPico"] = memory_get_peak_usage(true);
				$arrEventParams["EndDate"] = date('Y-m-d H:i:s');
				RegisterOperation($arrEventParams, $giOperationID);
			    exit();
			}
		}
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("llamar a storeSurveyCapture", 1, 1, "color:purple;");
		}
		
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		//Reenviado el parámetro $theUser para no tener que cargarlo dentro de la función
		$result = storeSurveyCapture($aRepository, $theUser);
		/*
		$result["path"] = $path;
		$result["surveyID"] = $surveyID;
		$result["questionID"] = $questionID;
		*/
	}
	else {
		/*
        $return['error'] = true;
        $return['errmsg'] = 'There was an error while saving the outbox data';
        return $return;
        */
	    die("errmsg=There was an error while saving the outbox data");
	}
	
	return $result;
}

//@JAPR 2013-03-26: Agregado el botón de reporte de error en el App
/* Recibe un archivo de cualquier tipo enviado por un App mediante el botón uploadFiles
*/
function uploadFileData($aRepository) {
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
	$strSessionID = trim(getParamValue('sessionID', 'both', "(string)"));
	$strFileType = getParamValue('filetype', 'both', "(string)");
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	if (is_null($strFileName) || trim($strFileName) == '') {
	    die("errmsg=No filename data was provided for this file");
	}
	
	$path = getcwd()."\\"."appdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if ($strSessionID == '') {
		$strSessionID = date('YmdHis');
	}
	$path .= "\\".$strSessionID;
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
		    die("errmsg=Project synchronization path could not be created");
		}
	}
    
	//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
	$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$strCompletePath = $path."\\".$strFileName;
	$strFileData = getParamValue('data', 'both', "(string)", true);
	if (is_null($strFileData)) {
	    die("errmsg=No data was received for this file");
	}
	$status = file_put_contents($strCompletePath, $strFileData);
	
	if ($status) {
		$result = 'OK';
	}
	else {
	    die("errmsg=There was an error while uploading the file");
	}
	
	return $result;
}

/* Recibe un archivo de log del usuario enviado por un App y además de dejarlo en una carpeta especial lo procesa para grabar el log en
una tabla donde se podrá consultar
Esta función se invoca siempre de manera independiente del proceso, por lo tanto no importa si regresa o no con código de error, el App
va a ignorar la respuesta y continuará con el proceso de sincronización
*/
function uploadLogData($aRepository) {
	$result = '';
	$kpiUser = getParamValue('UserID', 'both', "(string)");
	$strSessionID = trim(getParamValue('sessionID', 'both', "(string)"));
	//$strFileName = getParamValue('filename', 'both', "(string)", true);
	//if (is_null($strFileName) || trim($strFileName) == '') {
	//    die("errmsg=No filename data was provided for this file");
	//}
	
	$path = getcwd()."\\"."synclog\\".trim($_SESSION["PABITAM_RepositoryName"]);
	if ($strSessionID == '') {
		$strSessionID = date('YmdHis');
	}
	$strFileName = $kpiUser.$strSessionID.'.log';
	//$path .= "\\".$strSessionID;
	if (!file_exists($path))
	{
		if (!mkdir($path, null, true))
		{
		    die("errmsg=Project synchronization log path could not be created");
		}
	}
    
	//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
	$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$strCompletePath = $path."\\".$strFileName;
	$strFileData = getParamValue('data', 'both', "(string)", true);
	if (is_null($strFileData)) {
	    die("errmsg=No data was received for this log file");
	}
	$status = file_put_contents($strCompletePath, $strFileData);
	
	//Una vez grabado el archivo, inicia el procesamiento para almacenarlo en la tabla
	try {
		//@JAPR 2017-12-19: Agregados al script de actualización ALTER TABLE que se estaban ejecutando directo en código pero provocaban table locks, 
		//para esta fecha toda BD de eForms ya deben tener estos campos, pero se integran al script por si alguna BD mas vieja se migra de versión
		/*$sql = "ALTER TABLE SI_SV_AppLog ADD AppSessionID VARCHAR(20) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog ADD EventID INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog ADD EventStepID INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog ADD ConnectionType VARCHAR(50) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog MODIFY COLUMN AgendaID INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog MODIFY COLUMN SurveyID INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog MODIFY COLUMN EventDateID DATETIME NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog MODIFY COLUMN UploadDateID DATETIME NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_AppLog MODIFY COLUMN GMTZone INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);*/
		//@JAPR
		
		//Separa todas las entradas de registro, las cuales viene separadas por ***\r\n e internamente cada campo sólo por \r\n
		if (trim($strFileData) != '') {
			$arrEntries = explode("***\r\n", $strFileData);
			foreach ($arrEntries as $strEvent) {
				$arrFields = explode("\r\n", $strEvent);
				$strLogStatus = insertAppLogLine($aRepository, $arrFields);
			}
		}
	} catch (Exception $e) {
		//No reporta este error, es sólo para no afectar al grabado
	}
	
	if ($status) {
		//@JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
		//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
		$result = 'Survey data was saved successfully.';	//'OK';
		header("Content-Type: text/plain");
		echo($result);
		//@JAPR
	}
	else {
	    die("errmsg=There was an error while uploading the file");
	}
	
	return $result;
}

//@JAPR 2015-03-09: A partir de este día se agregó esta función como función de instancia, así que se debería optar por utilizarla de ahí
//directamente cuando ya se tiene la instancia generada en lugar de invocar a esta función que cargará de nuevo la instancia
function GetAgendaFilter ($aRepository, $agendaID) {
	
	require_once('surveyAgenda.inc.php');
	
	$strFilter = '';
	$anAgenda = BITAMSurveyAgenda::NewInstanceWithID($aRepository, $agendaID);
	if ($anAgenda->FilterText != '') {
		$arrFilterText = explode("|", $anAgenda->FilterText);
		$catalogID = $anAgenda->CatalogID;
		$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($aRepository, true, $catalogID);
		foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) {
			$first = true;
			$curCatMember = 0;
			foreach ($tmpCatalog as $ckey => $tmpCatMember) {
				$tmpMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $ckey);
				$memberID = $tmpMember->MemberID;
				$memberValue = $aRepository->DataADOConnection->Quote($arrFilterText[$curCatMember]);
				if (!$first) {
					$strFilter .= ' && ';
				}
				//@CMID66 = 'The Home Depot' AND @CMID65 <> 'Home Depot Tampico'
				$strFilter .= ("@CMID".$memberID."=".$memberValue);
				$curCatMember++;
				$first = false;
			}
		}
	}
	return $strFilter;
}

//@JAPR 2014-06-10: Agregado el refresh de catálogos dinámico
//Dado un catálogo y un filtro recibido como parámetro en una petición HTTP desde el App, regresa el objeto con los datos del catálogo en el mismo
//formato requerido por el App, ya que sobreescribirán al objeto Catalog local
function GetDynamicCatalogValues($aRepository) {
	$return = array();
	$return['error'] = false;
	
    $intCatalogID = getParamValue('catalogID', 'both', "(int)");
	if ($intCatalogID <= 0) {
        $return['error'] = true;
        $return['errmsg'] = 'Error retrieving dynamic catalog values, invalid catalog ID';
	}
    
	require_once('catalog.inc.php');
	$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
	if (is_null($objCatalog)) {
        $return['error'] = true;
        $return['errmsg'] = 'Error loading the catalog';
	}
	
	//Termina sin error regresando los datos del catálogo
	if (!$return['error']) {
		$strFilter = getParamValue('filterText', 'both', "(string)");
		global $appVersion;
		if ($appVersion >= esvAgendaRedesign) {
			$parameterAgendaID = getParamValue('agendaID', 'both', "(int)");
			if ($parameterAgendaID) {
				//@JAPR 2014-11-24: Corregido un bug, cuando se va a sobreescribir el filtro con el de la agenda, hay que cambiar la referencia
				//al catálogo que se debe utilizar también por el de la agenda, de lo contrario las variables de atributos no se reemplazarán
				//correctamente
				$objAgenda = @BITAMSurveyAgenda::NewInstanceWithID($aRepository, $parameterAgendaID);
				if (!is_null($objAgenda)) {
					//@JAPR 2014-11-27: Corregido un bug, sólo debe sobreescribir el filtro si los datos pedidos son del mismo catálogo de la agenda
					if ($objAgenda->CatalogID && $objAgenda->CatalogID == $intCatalogID) {
						$intCatalogID = $objAgenda->CatalogID;
						$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $intCatalogID);
					}
				}
				//@JAPR 2014-11-27: Corregido un bug, sólo debe sobreescribir el filtro si los datos pedidos son del mismo catálogo de la agenda
				if (!is_null($objAgenda)) {
					if ($objAgenda->CatalogID && $objAgenda->CatalogID == $intCatalogID) {
						$strFilter = GetAgendaFilter($aRepository, $parameterAgendaID);
					}
				}
			}
		}
		if (trim($strFilter) == '') {
			//Si no hay filtro, no es necesario cargar valores nuevamente, así se respetará lo que ya tuviera el App
			return $return;
		}
		
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    $intMaxOrder = 0;
		$arrAttributes = BITAMCatalog::GetCatalogAttributes($aRepository, $intCatalogID, $intMaxOrder);

		//OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA
		$strDynamicFilter = '';
		$sql = "SELECT DataMemberLatitudeID, DataMemberLongitudeID, MapRadio FROM si_sv_question where catalogID = " . $intCatalogID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ( $aRS && !$aRS->EOF) {
			$intCatMemberLatitudeID = (int) @$aRS->fields['datamemberlatitudeid'];
			$intCatMemberLongitudeID = (int) @$aRS->fields['datamemberlongitudeid'];
			$intMapRadio = (int) @$aRS->fields['mapradio'];
			if ( $intCatMemberLatitudeID > 0 &&  $intCatMemberLongitudeID > 0 ) {
				
				//OMMC 2019-04-29: Agregada configuración del radio para cálculo de coordenadas #QEGRGA
				$aUserLat = getParamValue('syncLat', 'both', '(float)', false);
				$aUserLng = getParamValue('syncLong', 'both', '(float)', false);
				$intRadioValue = defaultMapRadio;

				if ($intMapRadio > 0) {
					$intRadioValue = defaultMapRadio * ($intMapRadio / 10000);
				}

				$tmpValue = null;
				$minLat = $aUserLat - $intRadioValue;
				$maxLat = $aUserLat + $intRadioValue;
				if (!($minLat < $maxLat)) {
					$tmpValue = $maxLat;
					$maxLat = $minLat;
					$minLat = $tmpValue;
				}

				$tmpValue = null;
				$minLng = $aUserLng - $intRadioValue;
				$maxLng = $aUserLng + $intRadioValue;
				if (!($minLng < $maxLng)) {
					$tmpValue = $maxLng;
					$maxLng = $minLng;
					$minLng = $tmpValue;
				}
				//Se construye el nuevo filtro con el radio 
				$strDynamicFilter = ' AND ({@DMID'.$intCatMemberLatitudeID.'} BETWEEN '.$minLat.' AND '.$maxLat.') AND ({@DMID'.$intCatMemberLongitudeID.'} BETWEEN '.$minLng.' AND '.$maxLng.')';	
			}
		}
		$strFilter = BITAMCatalog::TranslateAttributesVarsToFieldNames($aRepository, $intCatalogID, $strFilter.$strDynamicFilter);
		$objCatalogValues = $objCatalog->getAttributeValues(array_values($arrAttributes), -1, $strFilter, null, null, false, true);
		if (!is_null($objCatalogValues) && is_array($objCatalogValues)) {
			$return['catalogID'] = $intCatalogID;
			$return['attributeValues'] = $objCatalogValues;
		}
	}
	
	return $return;
}

//@JAPR 2014-08-20: Agregados los datos dinámicos de secciones tipo Recap
/* Dada una sección y un filtro recibido como parámetro en una petición HTTP desde el App, regresa el objeto con los datos de eBavel en forma
de un array donde cada registro es un objeto indexado por el ID numérico de los campos de eBavel, con dicho array se podrán realizar
diferentes acciones en el App, la primera consistiendo en llenar la sección tipo Recap con datos pre-existentes
*/
function GetDynamicSectionValues($aRepository) {
	$return = array();
	$return['error'] = false;
	
	$iReturnType = ebdtJSonArrayForApps;
    $intSectionID = getParamValue('sectionID', 'both', "(int)");
	if ($intSectionID <= 0) {
        $return['error'] = true;
        $return['errmsg'] = 'Error retrieving dynamic section values, invalid section ID';
	}
    
	require_once('section.inc.php');
	require_once('eBavelIntegration.inc.php');
	$objSection = BITAMSection::NewInstanceWithID($aRepository, $intSectionID);
	if (is_null($objSection)) {
        $return['error'] = true;
        $return['errmsg'] = 'Error loading the section';
	}
	
	//Obtiene la lista de campos de la sección, esto es, todos aquellos que estén mapeados como preguntas
	/*
	$arreBavelFieldIDs = array();
	$questionCollection = BITAMQuestionCollection::NewInstanceBySection($aRepository, $intSectionID);
	foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
		if ($aQuestion->eBavelFieldID > 0) {
			$arreBavelFieldIDs[] = $aQuestion->eBavelFieldID;
		}
	}
	$arreBavelFieldIDs = null;
	
	if (count($arreBavelFieldIDs) == 0) {
        $return['error'] = true;
        $return['errmsg'] = 'Error loading the section fields';
	}
	*/
	
	//Termina sin error regresando los datos del catálogo
	if (!$return['error']) {
		$return = array();
		$strFilter = getParamValue('filterText', 'both', "(string)");
		//Si no hay filtro, se considera que se desea obtener todos los valores disponibles en la forma mapeada a esta sección, pero en
		//caso de haberse especificado, se debe convertir a los valores requeridos por los objetos de eBavel para ser procesado
		if (trim($strFilter) != '') {
			$strFilter = BITAMSection::TranslateFieldVarsToFieldNames($aRepository, $intSectionID, $strFilter);
		}
		
		$objSectionValues = $objSection->GetEBavelFieldValues(null, $strFilter, $iReturnType);
		if (!is_null($objSectionValues) && is_array($objSectionValues)) {
			$return['sectionID'] = $intSectionID;
			$return['sectionValues'] = $objSectionValues;
		}
	}
	
	return $return;
}

//@JAPR 2012-11-05: Agregada la edición de capturas en v4 desde el Browser
/* Dado un objeto de reporte que representa a una captura, identifica las respuestas y las convierte al formato de v4.01000 regresando
un array para posteriormente generar una cadena JSon que puede ser descargada al App y utilizada para editar una captura grabada en el server
El parámetro $anEntryID representa un FactKey para invocar a la carga del reporte. Para este punto se asume que ya se encuentra logeado el
usuario que tiene permisos de hacer la carga
*/
function loadSurveyCapture($aRepository) {
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	
	$status = 'OK';
	$return['error'] = false;
	
	// ***********************************************************************************************************************
	//Inicia la identificación de parámetros
	// ***********************************************************************************************************************
	$arrAnswers = array();
	
    $surveyID = getParamValue('surveyID', 'both', "(int)");
	if ($surveyID <= 0) {
		return 'errmsg=No survey data was received for this outbox';
	}
	
	require_once('survey.inc.php');
	require_once('section.inc.php');
	require_once('question.inc.php');
	require_once('catalog.inc.php');
	require_once('catalogmember.inc.php');
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if (is_null($surveyInstance)) {
		return 'errmsg=The specified survey was not found ('.$surveyID.')';
	}
	
	//El entryID no es indispensable, si no se recibe se procede a generar una captura nueva de la encuesta especificada, en caso de
	//recibirlo entonces si se genera el array de respuestas correctamente con lo que fué respondido en el entryID especificado
	$entryID = getParamValue('entryID', 'both', "(int)");
	$intFactKeyDimVal = $entryID;
	$objReportData = null;
	if ($entryID > 0) {
		//El entryID recibido es el FactKeyDimVal, pero para instanciar a report se necesita el FactKey, así que obtenemos el último FactKey
		//del FactKeyDimVal especificado
		$sql = "SELECT MAX(t1.FactKey) AS FactKey FROM ".$surveyInstance->SurveyTable." t1 WHERE t1.FactKeyDimVal = ".$entryID;
	    $aRS = $aRepository->DataADOConnection->Execute($sql);
	    if ($aRS && !$aRS->EOF) {
	    	$entryID = (int) $aRS->fields["factkey"];
	    }
	}
	
	require_once("reportExt.inc.php");
	if ($entryID > 0) {
		$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $entryID, $surveyID);
		if (is_null($objReportData)) {
			return 'errmsg=Error retrieving the stored answers for this survey entry ('.$entryID.')';
		}
	}
	else {
		$objReportData = BITAMReportExt::NewInstance($aRepository, $surveyID);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		PrintMultiArray($objReportData);
	}
	
	$arrData = array();
	//Genera un array indexado por ID de sección para facilitar identificarlas mientras se procesan las respuestas
	$sectionCollection = BITAMSectionCollection::NewInstance($aRepository, $surveyID);
	$arrSectionsByID = array();
	$arrSectionValuesByID = array();			//Contiene los keys de cada valor del catálogo usado como opción múltiple indexados ascendentemente desde 0
	$arrSectionPagesByID = array();				//Contiene los keys de cada valor del catálogo que generó una página dinámica indexados ascendentemente desde 0
	$arrSectionValuesByPage = array();			//Contiene los keys de cada valor del catálogo usado como opción múltiple indexados por el key de la página donde existe (es un array multidimensional, la dimensión externa es simplemente el número de página)
	$arrPagesBySectionValues = array();			//Contiene los keys de cada página pero indexado por el key de cada valor del catálogo usado como opción múltiple
	$arrSectionValuesByDesc = array();			//Contiene los keys de cada valor del catálogo pero indexado por la descripción
	$arrHasMultiChoiceQuestions = array();
	foreach ($sectionCollection as $aSection) {
		$arrSectionsByID[$aSection->SectionID] = $aSection;
		$arrHasMultiChoiceQuestions[$aSection->SectionID] = false;
	}
	
	//Genera un array indexado por ID de pregunta para facilitar identificarlas mientras se procesan las respuestas
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	$arrQuestionsByID = array();
	foreach ($questionCollection as $aQuestion) {
		$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion;
		
		//Agrega la bandera de preguntas múltiples en secciones dinámicas
		if ($aQuestion->QTypeID == qtpMulti) {
			$objSection = @$arrSectionsByID[$aQuestion->SectionID];
			if (!is_null($objSection)) {
				$arrHasMultiChoiceQuestions[$objSection->SectionID] = true;
			}
		}
	}
	
	//Obtiene los valores del catálogo para las secciones que sean dinámicas
	$dynamicSectionCatID = 0;
	$dynamicSection = null;
	$arrSectionValue = array();
	$arrSectionPage = array();
	$arrSCDynQuestionsBySectionID = array();
	foreach ($sectionCollection as $aSection) {
		if ($aSection->SectionType != sectDynamic) {
			continue;
		}
		
		$intSectionID = $aSection->SectionID;
		//Identifica si es necesario obtener el Array de padres usados para generar las secciones, ya que si no hay pregunta múltiple no es necesario
		$blnGenerateParentValuesArr = false;
		//Contiene la instancia de la pregunta de Single Choice que filtra para generar las subsecciones dinámicas
		$aSCDynQuestionInstance = null;
		$dynamicSection = $aSection;
		$dynamicSectionCatID = $dynamicSection->CatalogID;
		$dynamicSectionID = $aSection->SectionID;
		$blnDynamicSectionFound = false;
		
		//Por cada sección dinámica genera el SectionValue grabado y además carga un array con los valores de los dos atributos usados en ella
		//para poder hacer match entre el key recibido y el key de su atributo padre
		foreach ($questionCollection->Collection as $questionKey => $aQuestion)
		{
			//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
			if (!$blnDynamicSectionFound)
			{
				if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $dynamicSectionCatID)
				{
					//Como pudiera haber varias preguntas de catálogo sincronizadas, debemos tomar la última para saber exactamente por qué atributo 
					//se debe filtrar
					$aSCDynQuestionInstance = $aQuestion;
				}
			}
			
			//si las preguntas pertenecen a la seccion dinamica y que estan dentro de la seccion
			//entonces si son repetibles y se ignoran dichas preguntas
			if($aQuestion->SectionID == $dynamicSectionID)
			{
				$blnDynamicSectionFound = true;
				//En este caso, no importa realmente si hay o no multiples, como se tienen que ordenar los valores se hace el proceso del If
				//siguiente de todas formas, por lo que además se forzará al break pues ya se identificó la pregunta generadora de la dinámica
				//if ($aQuestion->QTypeID == qtpMulti)
				//{
			        //@JAPR 2012-06-29: Corregido un bug, no había por qué hacer este Break, eso provocaba que ya no se grabaran las preguntas posteriores a las dinámicas
					$blnGenerateParentValuesArr = true;
					//break;
					//@JAPR
				//}
				break;
				//continue;
			}
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\nblnGenerateParentValuesArr: $blnGenerateParentValuesArr<br>\r\n");
			echo("<br>\r\nIsNull(aSCDynQuestionInstance): ".(is_null($aSCDynQuestionInstance))."<br>\r\n");
		}
		if ($entryID > 0 && $blnGenerateParentValuesArr && !is_null($aSCDynQuestionInstance))
		{
			$arrSCDynQuestionsBySectionID[$intSectionID] = $aSCDynQuestionInstance;
			//Con la instancia de la pregunta que generó a la sección dinámica, obtenemos todos los valores grabados para ella en esta captura, ya
			//que esos serán los únicos valores posibles a usar (en este punto no importa si no vienen ordenados)
			$strSurveyField = strtolower($aSCDynQuestionInstance->SurveyField);
			$sql = "SELECT DISTINCT ".$aSCDynQuestionInstance->SurveyField." 
				FROM ".$surveyInstance->SurveyTable." 
				WHERE FactKeyDimVal = ".$intFactKeyDimVal;
			//@JAPR 2013-02-12: Agregada la edición de datos históricos
			if ($surveyInstance->DissociateCatDimens) {
				//Si se usan dimensiones independientes del catálogo, es seguro que existe el campo que identifica a los registros por sección,
				//así que basado en eseo se ordenan por FactKey ya que esa es la secuencia en la que se generan las páginas durante el grabado
				//y es el mismo orden en el que se generaron visualmente las páginas dinámicas junto a sus multiple choice
				$sql .= " AND EntrySectionID = ".$intSectionID." 
					ORDER BY FactKey";
			}
			//@JAPR
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false)
			{
				$intIndex = -1;
			    while (!$aRS->EOF) {
					//@JAPR 2013-02-11: Agregada la edición de datos históricos
			    	if ($surveyInstance->DissociateCatDimens) {
			    		//En este caso realmente no interesa el valor, ya que todos los índices se regresarán como negativos pues el catálogo pudo
			    		//haber sido modificado así que es irrelevante el valor del key subrrogado del mismo que tuviera originalmente, se basará
			    		//sólo en las descripciones extraídas de la tabla de hechos/paralela
			    		$arrSectionValue[] = $intIndex--;
			    	}
			    	else {
			    		$arrSectionValue[] = (int) @$aRS->fields[$strSurveyField];
			    	}
			    	//@JAPR
			    	$aRS->MoveNext();
			    }
			}
			
		//Por cada sección dinámica identifica el SectionValue recibido y además carga un array con los valores de los dos atributos usados
		//en ella para poder hacer match entre el key recibido y el key de su atributo padre
		//$arrSectionValue = @$jsonData['sectionValue'.$intSectionID];
		//$arrSectionPage = @$jsonData['sectionPage'.$intSectionID];
		//if (!is_null($arrSectionValue)) {
			//El array de SectionPage contiene el key que generó cada página dinámica, aunque no hay referencia a cuales de las opciones múltiples
			//le pertenecen, eso se determina aquí en el server
			//$arrSectionPagesByID[$intSectionID] = $arrSectionPage;
			$arrPagesBySectionValues[$intSectionID] = array(); // array_fill(0, count($arrSectionValue), '');
			$arrSectionValuesByPage[$intSectionID] = array();
			$arrSectionValuesByDesc[$intSectionID] = array();
			
			//if (count($arrSectionPage) == 0) {
			//	continue;
			//}
			
			//Ahora obtiene los valores reales del catálogo basado en los keys de las páginas generadas para reducir el tamaño del filtro
			$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aSection->CatalogID);
			if (is_null($objCatalog)) {
				continue;
			}
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $objCatalog->CatalogID);
			if (is_null($objCatalogMembersColl)) {
				continue;
			}
			
			//Identifica los atributos usados como generador de página y de opciones de Multiple Choice
			$arrAttributeIDs = array();
			foreach ($objCatalogMembersColl->Collection as $intIndex => $objCatalogMember) {
				//Cuando se llegó al atributo de la sección debe salir del ciclo pero primero agrega ese y el atributo siguiente
				if ($objCatalogMember->MemberID == $aSection->CatMemberID) {
					//@JAPR 2013-02-11: Agregada la edición de datos históricos
					$arrAttributeIDs[] = array('dimID' => $objCatalogMember->ClaDescrip, 'indDimID' => (int) @$objCatalogMember->IndDimID);
					if (isset($objCatalogMembersColl->Collection[$intIndex +1])) {
						$arrAttributeIDs[] = array('dimID' => $objCatalogMembersColl->Collection[$intIndex +1]->ClaDescrip, 
							'intDimID' => (int) @$objCatalogMembersColl->Collection[$intIndex +1]->IndDimID);
					}
					break;
				}
			}
			
			if (count($arrAttributeIDs) == 0) {
				continue;
			}
			//El filtro contendrá todos los keys que fueron usados como páginas en la captura
			if (count($arrSectionValue) == 0) {
				$strFilter = 'AND '.$objCatalog->TableName.'KEY = -1 ';
			}
			else {
				$strFilter = 'AND '.$objCatalog->TableName.'KEY IN ('.implode(',', $arrSectionValue).') ';
			}
			
			//@JAPR 2013-02-11: Agregada la edición de datos históricos
			//Si la encuesta usa catálogos desasociados, se envian estos parámetros para forzar a usar sólo los datos históricos y no el catálogo
			//como actualmente se encuentra
			$intEntryID = null;
			$intEntrySurveyID = null;
			$bJustGetHistoricEntryData = false;
			if ($surveyInstance->DissociateCatDimens) {
				$intEntryID = $entryID;
				$intEntrySurveyID = $surveyID;
				$bJustGetHistoricEntryData = true;
			}
			
			$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter);
			//$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter, $intEntryID, $intEntrySurveyID, $bJustGetHistoricEntryData);
			//@JAPR
			//Sobreescribe el array de sectionValues porque podía venir desordenado, pero con la carga de valores anterior ya se ordenó, así que
			//se va a volver a llenar
			$arrSectionValue = array();
			//@JAPR 2013-02-11: Agregada la edición de datos históricos
			if ($surveyInstance->DissociateCatDimens) {
				//En este caso se basará en los datos leídos del reporte para generar los arrays de sectionValue, ya que debe asignar keys negativos
				//para cada página que se hubiera grabado previamente
				$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
				if (!is_null($objAnswersColl) && is_array($objAnswersColl)) {
					$intIndex = -1;
					if ($arrHasMultiChoiceQuestions[$intSectionID]) {
						//Si hay preguntas múltiples, se debe hacer un procesamiento mas complejo porque por cada key del SectionValue se tiene que indicar
						//a qué página pertenece para poder repetir la respuesta de cada pregunta Simple Choice por tantos keys existan en cada página
						$intPageNum = 0;
						$intPageKey = $intIndex;
						foreach ($objAnswersColl as $aPageValue) {
							if ($aPageValue['DynamicPageDSC'] != '') {
								//Se trata de un registro que representa a la página, así que incrementa los contadores y agrega el valor de la 
								//página al array correspondiente
								$intPageKey = $intIndex;
								$intPageNum = count($arrSectionPage);
								$arrSectionPage[$intPageNum] = $intPageKey;
								$arrSectionValuesByPage[$intSectionID][$intPageNum] = array();
								$arrSectionValuesByDesc[$intSectionID][$intPageNum] = array();
							}
							
							//Cualquier valor representa el de un checkbox, así que lo agrega a dicho array y mas hace referencia a la página en la
							//que está definido
							$intKey = $intIndex;
							$arrSectionValue[] = $intKey;
							$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
							$arrSectionValuesByPage[$intSectionID][$intPageNum][] = $intKey;
							//Este valor SI se debe decodificar, porque se usará para comparaciones durante la generación
							$arrSectionValuesByDesc[$intSectionID][$intPageNum][$aPageValue['desc']] = $intKey;
							$intIndex--;
						}
					}
					else {
						//Si no hay preguntas múltiple, entonces cada key del section value tendrá una única respuesta así que no se hace nada extra
						foreach ($objAnswersColl as $aPageValue) {
							$intPageKey = $intIndex;
							$arrSectionValue[] = $intPageKey;
							$arrSectionPage[] = $intPageKey;
							$arrPagesBySectionValues[$intSectionID][$intPageKey] = $intPageKey;
							$intIndex--;
						}
						$arrSectionValuesByPage[$intSectionID] = array_keys($arrPagesBySectionValues[$intSectionID]);
					}
				}
			}
			else {
				if ($arrHasMultiChoiceQuestions[$intSectionID]) {
					//Si hay preguntas múltiples, se debe hacer un procesamiento mas complejo porque por cada key del SectionValue se tiene que indicar
					//a qué página pertenece para poder repetir la respuesta de cada pregunta Simple Choice por tantos keys existan en cada página
					$intIndex = 0;
					foreach ($arrCatalogValues as $aPageValue) {
						$intPageKey = (int) @$aPageValue['key'];
						$arrSectionPage[] = $intPageKey;
						if (isset($aPageValue['children'])) {
							$arrSectionValuesByPage[$intSectionID][$intIndex] = array();
							$arrSectionValuesByDesc[$intSectionID][$intIndex] = array();
							foreach ($aPageValue['children'] as $aSectionValue) {
								$intKey = (int) @$aSectionValue['key'];
								$arrSectionValue[] = $intKey;
								$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
								$arrSectionValuesByPage[$intSectionID][$intIndex][] = $intKey;
								$arrSectionValuesByDesc[$intSectionID][$intIndex][$aSectionValue['value']] = $intKey;
							}
							$intIndex++;
						}
					}
				}
				else {
					//Si no hay preguntas múltiple, entonces cada key del section value tendrá una única respuesta así que no se hace nada extra
					foreach ($arrCatalogValues as $aPageValue) {
						$intPageKey = (int) @$aPageValue['key'];
						$arrSectionValue[] = $intPageKey;
						$arrSectionPage[] = $intPageKey;
						$arrPagesBySectionValues[$intSectionID][$intPageKey] = $intPageKey;
					}
					/*
					foreach ($arrSectionValue as $intIndex => $intKey) {
						$arrPagesBySectionValues[$intSectionID][$intKey] = $intKey;
					}
					*/
					//@JAPR 2012-11-28: Corregido un bug, estaba dejando como índice el mismo key y se esperaba un consecutivo de página
					$arrSectionValuesByPage[$intSectionID] = array_keys($arrPagesBySectionValues[$intSectionID]);
				}
			}
			//@JAPR

			//if (isset($arrCatalogValues[0])) {
			//	$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$arrCatalogValues[0]['value'];
			//	$arrCatalogValues = $arrCatalogValues[0]['children'];
			//}
			
			//Se asigna el array de SectionValue a esta sección tal como viene del server, ya que eso representa los elementos que pudieron
			//ser capturados en esta encuesta específica y todos los elementos del $_POST para el grabado se basarán en este mismo orden
			$arrSectionValuesByID[$intSectionID] = $arrSectionValue;
			//En este momento sólo existe una sección dinámica por encuesta, así que genera en el $_POST el valor de sectionValue
			$arrData['sectionValue'.$intSectionID] = $arrSectionValue;
			$arrData['sectionPage'.$intSectionID] = $arrSectionPage;
		//}
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo ("<br>\r\n");
		echo ("<br>\r\nPages (arrSectionPage):");
		PrintMultiArray($arrSectionPage);
		echo ("<br>\r\nPages ByID (arrSectionPagesByID):");
		PrintMultiArray($arrSectionPagesByID);
		echo ("<br>\r\nPages by section value (arrPagesBySectionValues):");
		PrintMultiArray($arrPagesBySectionValues);
		echo ("<br>\r\nSectionValues ByPage (arrSectionValuesByPage):");
		PrintMultiArray($arrSectionValuesByPage);
		echo ("<br>\r\nSectionValues ByDesc (arrSectionValuesByDesc):");
		PrintMultiArray($arrSectionValuesByDesc);
		echo ("<br>\r\nSectionValues ByID (arrSectionValuesByID):");
		PrintMultiArray($arrSectionValuesByID);
		//echo ("<br>\r\nSectionValues ByKey:");
		//PrintMultiArray($arrSectionValueByKey);
		//die();
	}
	
	//Prepara un array vacio genérico para llenar una respuesta posteriormente
	$emptyAnswer = array(
		"id" => 0,
		"catalogFilter" => new stdClass(),
		"multiAnswer" => new stdClass(),
		"isNull" => 1,
		"dirty" => 0,
		"answer" => '',
		"comment" => '',
		"photo" => '',
		"document" => '',
		"factKey" => ''
	);
	
	//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
	$surveyInstance->getSpecialFieldsStatus();
	$blnUseSingleRecord = ($surveyInstance->UseStdSectionSingleRec && $surveyInstance->UseDynamicPageField);
	//Identifica las preguntas que provocan que se generen las páginas dinámicas
	//Inicia el barrido de las respuestas del reporte, recorriendo una a una las preguntas de la encuesta y agregando al array según
	//el tipo y la sección a la que pertenece
	$arrMasterSectionRecs = array();
	
	foreach ($questionCollection as $objQuestion) {
		$fieldName = 'dim_'.$objQuestion->QuestionID;
		$fieldNameKey = 'dim_'.$objQuestion->QuestionID."KEY";
		$objSection = @$arrSectionsByID[$objQuestion->SectionID];
		if (is_null($objSection) || $objSection->SectionType == sectFormatted) {
			//No debería ser posible que no existiera la sección, pero si se da entonces continua con la siguiente pregunta para evitar errores
			//Tampoco se pueden procesar preguntas de secciones formateadas
			continue;
		}
		
		$intSectionID = $objSection->SectionID;
		//Valida los tipos de preguntas que no pueden grabar ningún dato para saltarlos
		//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
		//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
		//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
		//Cambiado a un switch para mayor facilidad
		$blnValidQuestion = true;
		switch ($objQuestion->QTypeID) {
			case qtpMessage:
			case qtpSkipSection:
			case qtpShowValue:
			case qtpSync:
			case qtpPassword:
			case qtpSection:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
				$blnValidQuestion = false;
				break;
		}
		
		if (!$blnValidQuestion) {
			continue;
		}
		//@JAPR
		
		$strQuestionID = (string)$objQuestion->QuestionID;
		//Se dejó el mismo switch que en el método storeSurveyCapture para que sea fácil identificar la equivalencia
		//Basado en el tipo de pregunta, llena las propiedades correctas del array de respuesta para que sea cargado en el draft
		switch ($objQuestion->QTypeID) {
			case qtpMulti:
				//Si la pregunta es Multiple Choice por el momento no se soportan de catálogo excepto dentro de la sección dinámica, donde
				//no se soportarían normales, así que únicamente valida por el tipo de Sección
				
				//Obtiene el array de respuestas posibles de esta pregunta (sólo aplica si no es de sección dinámica, pues en ese caso
				//vendría vacio, tampoco aplicaría para checkboxes pues en ese caso se graba directamente la respuesta seleccionada), este 
				//array debe estar ordenado tal como fueron definidas ya que en ese mismo orden se capturan
				$arrSelectOptions = $objQuestion->SelectOptions;
				switch ($objSection->SectionType) {
					case sectDynamic:
						//Si se trata de una sección dinámica, se debe generar un array con tantas páginas como existan en el filtro según
						//la respuesta de catálogo correspondiente, ya que aunque sólo generaramos el array con las respuestas que hay en
						//la captura realmente grabada, si no se envian las páginas intermedias vacias se interpretaría erróneamente el
						//draft al momento de desplegar. Como es una pregunta múltiple, adicionalmente hay que agregar como respuesta cada
						//uno de los valores seleccionados en el índice de la página que se corresponda según el catálogo
						//@JAPRWarning: Por el momento sólo se regresarán las páginas que fueron grabadas, pero se debe de modificar tarde o
						//temprano para incluir las páginas completas que mostraría el catálogo
						
						//Esta preguna se debe procesar después de la del catálogo correspondiente, pero como las preguntas se cargan por orden
						//se asume que obviamente al momento de haber llegado aquí ya se procesó la que genera la sección
						$aSCDynQuestionInstance = @$arrSCDynQuestionsBySectionID[$intSectionID];
						if (is_null($aSCDynQuestionInstance)) {
							continue;
						}
						$arrCatalogFilter = @$arrAnswers[(string) $aSCDynQuestionInstance->QuestionID]['catalogFilter'];
						//Si no hay un filtro de la pregunta de catálogo en este punto, quiere decir que no se contestó por lo que no hay necesidad
						//de generar la sección dinámica tampoco
						if (is_null($arrCatalogFilter)) {
							continue;
						}
						
						$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
						if (is_null($arrSectionValue)) {
							continue;
						}
						
						$arrSectionValueByKey = array_flip($arrSectionValue);
						$arrSectionPageByKey = array_flip($arrSectionPage);
						$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							//$intIndex representa el registro negativo en la misma secuencia de la colección de valores dinámicos, ya que durante
							//la edición no existen keys subrrogados del catálogo realmente pues estos pudieron haber cambiado de descripciones,
							//así que se simula usando valores negativos
							$intIndex = ($intIdx + 1) * -1;
							$strData = (string) @$aRecAnswer[$fieldName];
							$strValue = (string) @$aRecAnswer['desc'];
							if (trim($strData) != '' && trim($strValue) != '') {
								//@JAPR 2013-02-11: Agregada la edición de datos históricos
								if ($surveyInstance->DissociateCatDimens) {
									$intKey = $intIndex;
								}
								else {
									$intKey = @$aRecAnswer['id'];
								}
								$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
								//@JAPR
								$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
								//Primero llena las páginas que pudieran venir vacias para esta pregunta
								if (!isset($arrAnswers[$strQuestionID])) {
									$intNumPages = 0;
									$arrAnswers[$strQuestionID] = array();
								}
								else {
									$intNumPages = count($arrAnswers[$strQuestionID]);
								}
								if ($intPageNum+1 > $intNumPages) {
									for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
										if (!isset($arrAnswers[$strQuestionID][$intCont])) {
											//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
											$anAnswer = $emptyAnswer;
											$anAnswer['id'] = $objQuestion->QuestionID;
											$anAnswer['isNull'] = 1;
											$anAnswer['dirty'] = 0;
											$anAnswer['catalogFilter'] = $arrCatalogFilter;
											//@JAPR 2012-11-27: Corregido un bug, no se puede accesar como array el objeto, así que se tiene que asignar
											//como propiedad dinámica, pero para esto se agrega como stdClass
											$anAnswer['multiAnswer'] = new stdClass();
											$anAnswer['factKey'] = 0;
											$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
										}
									}
								}
								
								//Obtiene la referencia al registro de esta página para modificar el valor correspondiente, para este momento
								//ya deben existir todos los registros hasta esta página por lo que nada mas actualiza datos de edición
								$anAnswer = @$arrAnswers[$strQuestionID][$intPageNum];
								if (!is_null($anAnswer)) {
									$anAnswer['isNull'] = 0;
									$anAnswer['dirty'] = 1;
									//@JAPR 2012-11-26: Codificación utf-8
									//@JAPR 2012-11-26: La pregunta sólo se debe regresar si tiene un valor, así que para las numéricas o checkbox
									//se verifica que este sea > 0
									$blnAddAnswer = true;
									if ($objQuestion->MCInputType == mpcCheckBox || $objQuestion->MCInputType == mpcNumeric) {
										$blnAddAnswer = (int) $strData > 0;
									}
									if ($blnAddAnswer) {
										//@JAPR 2012-11-27: Corregido un bug, no se puede accesar como array el objeto, así que se tiene que asignar
										//como propiedad dinámica
										$strValIdx = $strValue;
										$anAnswer['multiAnswer']->$strValIdx = $strData;
									}
									$anAnswer['factKey'] = $intPageKey;
									$arrAnswers[$strQuestionID][$intPageNum] = $anAnswer;
								}
							}
						}
						break;
						
					case sectMasterDet:
						//En el caso de secciones múltiples, las preguntas Multiple Choice vienen como un multiAnswer sólo con los valores
						//seleccionados usados como índices, si son tipo CheckBox traerán un valor de 1 y en los otros tipos traen el valor
						//capturado en cada opción. Se genera un array de las respuestas, un índice por cada registro de la sección
						$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							$objAnswer = array();
							$strData = (string) @$aRecAnswer[$fieldName];
							if (trim($strData) != '') {
								$objAnswer = @explode(';', $strData);
							}
							
							if (is_null($objAnswer) || !is_array($objAnswer)) {
								$objAnswer = array();
							}
							
							$arrSelectOptionsTemp = array();
							if ($objQuestion->MCInputType != mpcCheckBox) {
								//En este caso la opción a agregar si no viene vacia se basa en el índice
								foreach ($objAnswer as $intOptionNum => $strValue) {
									if (isset($arrSelectOptions[$intOptionNum]) && trim($strValue) != '') {
										$strOption = $arrSelectOptions[$intOptionNum];
										if (trim($strOption) != '') {
											$arrSelectOptionsTemp[$strOption] = $strValue;
										}
									}
								}
							}
							else {
								//En este caso se agrega la opción tal como viene, puesto que ya es un texto de opción seleccionado
								$tmpPos = 1;
								foreach ($objAnswer as $intOptionNum => $strOption) {
									$arrSelectOptionsTemp[$strOption] = $tmpPos;
									if ($objQuestion->QDisplayMode == dspLabelnum) {
										$tmpPos++;
									}
								}
							}
							
							//Debe agregar el registro incluso si viene vacio
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = (count($arrSelectOptionsTemp) == 0)?1:0;
							$anAnswer['dirty'] = ($anAnswer['isNull'] == 1)?0:1;
							$anAnswer['multiAnswer'] = $arrSelectOptionsTemp;
							$anAnswer['factKey'] = (int) @$aRecAnswer['FactKey'];
							$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
						}
						break;
						
					default:
						//En cualquier otra sección, las preguntas Multiple Choice vienen como un multiAnswer sólo con los valores
						//seleccionados usados como índices, si son tipo CheckBox traerán un valor de 1 y en los otros tipos traen el valor
						//capturado en cada opción. Se genera un array de las respuestas, un índice por cada registro de la sección
						$strData = (string) @$objReportData->$fieldName;
						if (trim($strData) == '') {
							continue;
						}
						
						$objAnswer = @explode(';', $strData);
						if (is_null($objAnswer) || !is_array($objAnswer)) {
							$objAnswer = array();
						}
						
						$arrSelectOptionsTemp = array();
						if ($objQuestion->MCInputType != mpcCheckBox) {
							//En este caso la opción a agregar si no viene vacia se basa en el índice
							foreach ($objAnswer as $intOptionNum => $strValue) {
								if (isset($arrSelectOptions[$intOptionNum]) && trim($strValue) != '') {
									$strOption = $arrSelectOptions[$intOptionNum];
									if (trim($strOption) != '') {
										$arrSelectOptionsTemp[$strOption] = $strValue;
									}
								}
							}
						}
						else {
							//En este caso se agrega la opción tal como viene, puesto que ya es un texto de opción seleccionado
							$tmpPos = 1;
							foreach ($objAnswer as $intOptionNum => $strOption) {
								$arrSelectOptionsTemp[$strOption] = $tmpPos;
								if ($objQuestion->QDisplayMode == dspLabelnum) {
									$tmpPos++;
								}
							}
						}
						
						//Debe agregar el registro incluso si viene vacio
						$anAnswer = $emptyAnswer;
						$anAnswer['id'] = $objQuestion->QuestionID;
						$anAnswer['isNull'] = (count($arrSelectOptionsTemp) == 0)?1:0;
						$anAnswer['dirty'] = ($anAnswer['isNull'] == 1)?0:1;
						$anAnswer['multiAnswer'] = $arrSelectOptionsTemp;
						$arrAnswers[$strQuestionID] = $anAnswer;
						break;
				}
				break;
				//agregado tipo llamada
			case qtpCallList:
			case qtpSingle:
				if ($objQuestion->CatalogID > 0) {
					//Si la pregunta es de catálogo, se debe obtener la jerarquía completa de atributos dependiendo del valor del key grabado
					//para generar el catalogFilter, además de enviar tanto el parámetro key como la descripción correspondiente de la opción
					//seleccionada
					$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $objQuestion->CatalogID);
					if (is_null($objCatalog)) {
						continue;
					}
					$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $objQuestion->CatalogID);
					if (is_null($objCatalogMembersColl)) {
						continue;
					}
					
					//Obtiene la lista de atributos de la pregunta (incluyendo todos los padres)
					$arrAttributeIDs = array();
					foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
						//@JAPR 2013-02-11: Agregada la edición de datos históricos
						$arrAttributeIDs[] = array('dimID' => $objCatalogMember->ClaDescrip, 'indDimID' => (int) @$objCatalogMember->IndDimID);
						
						//Cuando se llegó al atributo de la pregunta debe salir del ciclo porque no interesan mas atributos
						if ($objCatalogMember->MemberID == $objQuestion->CatMemberID) {
							break;
						}
					}
					
					if (count($arrAttributeIDs) == 0) {
						continue;
					}
					
					$strFilter = 'AND '.$objCatalog->TableName.'KEY = ';
					switch ($objSection->SectionType) {
						//@JAPR 2013-02-14: Agregado el soporte de preguntas simple choice de catálogo dentro de dinámicas, que hasta v3 era
						//una restricción debido a la manera de armar el HTML pero en v4 ya no hay problema
						case sectDynamic:
							//En el caso de secciones dinámicas, la pregunta ya se graba con el key del valor específico del catálogo, así 
							//que si hay respuesta usa directamente dicho valor para cargar la jerarquía completa
							//Se genera un array de las respuestas, un índice por cada registro de la sección
							$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
							if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
								continue;
							}
							
							//Si se trata de una sección dinámica, se debe generar un array con tantas páginas como existan en el filtro según
							//la respuesta de catálogo correspondiente, ya que aunque sólo generaramos el array con las respuestas que hay en
							//la captura realmente grabada, si no se envian las páginas intermedias vacias se interpretaría erróneamente el
							//draft al momento de desplegar. Como es una pregunta múltiple, adicionalmente hay que agregar como respuesta cada
							//uno de los valores seleccionados en el índice de la página que se corresponda según el catálogo
							//@JAPRWarning: Por el momento sólo se regresarán las páginas que fueron grabadas, pero se debe de modificar tarde o
							//temprano para incluir las páginas completas que mostraría el catálogo
							
							//Esta preguna se debe procesar después de la del catálogo correspondiente, pero como las preguntas se cargan por orden
							//se asume que obviamente al momento de haber llegado aquí ya se procesó la que genera la sección
							//En el caso de secciones dinámicas, las preguntas Simple Choice vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar
							//$aSCDynQuestionInstance = @$arrSCDynQuestionsBySectionID[$intSectionID];
							//if (is_null($aSCDynQuestionInstance)) {
							//	continue;
							//}
							//$arrCatalogFilter = @$arrAnswers[(string) $aSCDynQuestionInstance->QuestionID]['catalogFilter'];
							//Si no hay un filtro de la pregunta de catálogo en este punto, quiere decir que no se contestó por lo que no hay necesidad
							//de generar la sección dinámica tampoco
							//if (is_null($arrCatalogFilter)) {
							//	continue;
							//}
							
							$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
							if (is_null($arrSectionValue)) {
								continue;
							}
							
							$arrSectionValueByKey = array_flip($arrSectionValue);
							$arrSectionPageByKey = array_flip($arrSectionPage);
							$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
							if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
								continue;
							}
							
							foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
								//$intIndex representa el registro negativo en la misma secuencia de la colección de valores dinámicos, ya que durante
								//la edición no existen keys subrrogados del catálogo realmente pues estos pudieron haber cambiado de descripciones,
								//así que se simula usando valores negativos
								$intIndexDyn = ($intIdx + 1) * -1;
								
								//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
								$aDynamicPageDSC = (string) @$aRecAnswer["DynamicPageDSC"];
								if ($blnUseSingleRecord && $aDynamicPageDSC == '') {
									//Si se usa el registro único y este no es él, entonces no lo puede usar para este tipo de preguntas
									continue;
								}
								
								//@JAPR 2013-02-11: Agregada la edición de datos históricos
								if ($surveyInstance->DissociateCatDimens) {
									//Como se manejan catálogos independientes, ya se tienen los valores exactos que se deben enviar en la instancia
									//del reporte y no es necesario hacer join con el catálogo
									$intCatKey = -1;
									$strValue = '';
									$arrCatalogFilter = array("-1" => '');
									$strCatDimValAnswers = (string) @$aRecAnswer[$objQuestion->SurveyCatField];
									$arrayCatValues = array();
									if ($strCatDimValAnswers != '') {
										$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
										$arrayCatClaDescrip = array();
										$arrayTemp = array();
										$arrayInfo = array();
										foreach($arrayCatPairValues as $element)
										{
											$arrayInfo = explode('_SVSep_', $element);
											//Obtener el cla_descrip y almacenar (key_1178)
											$arrayTemp = explode('_', $arrayInfo[0]);
											$arrayCatClaDescrip[] = $arrayTemp[1];
											//Obtener el valor del catalogo
											$arrayCatValues[] = $arrayInfo[1];
										}
										
										$intIndex = 0;
										$intMax = count($arrAttributeIDs);
										for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
											if (isset($arrayCatValues[$intIndex])) {
												$strValue = $arrayCatValues[$intIndex];
												//Este valor NO se debe decodificar porque se regresará al App y ahi todo es utf8
												$arrCatalogFilter[(string) $intIndex] = $strValue;
											}
										}
									}
									
									//@JAPR 2013-02-11: Agregada la edición de datos históricos
									if ($surveyInstance->DissociateCatDimens) {
										$intKey = $intIndexDyn;
									}
									else {
										$intKey = @$aRecAnswer['id'];
									}
									$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
									//@JAPR
									$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
									//Primero llena las páginas que pudieran venir vacias para esta pregunta
									if (!isset($arrAnswers[$strQuestionID])) {
										$intNumPages = 0;
										$arrAnswers[$strQuestionID] = array();
									}
									else {
										$intNumPages = count($arrAnswers[$strQuestionID]);
									}
									if ($intPageNum+1 > $intNumPages) {
										for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
											if (!isset($arrAnswers[$strQuestionID][$intCont])) {
												//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
												$anAnswer = $emptyAnswer;
												$anAnswer['id'] = $objQuestion->QuestionID;
												$anAnswer['isNull'] = 1;
												$anAnswer['dirty'] = 0;
												//$anAnswer['catalogFilter'] = $arrCatalogFilter;
												$anAnswer['factKey'] = 0;
												$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
											}
										}
									}
									
									if ($strValue != '') {
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 0;
										$anAnswer['dirty'] = 1;
										$anAnswer['catalogFilter'] = $arrCatalogFilter;
										$anAnswer['answer'] = $strValue;
										$anAnswer['key'] = $intCatKey;
										$anAnswer['factKey'] = $intPageKey;
										$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
									}
								}
								else {
									$intData = (int) @$aRecAnswer[$fieldName];
									if ($intData > 0) {
										$intCatKey = 0;
										$strValue = '';
										$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter.$intData.' ');
										//Se supone que sólo puede haber un valor con este Key, así que hace un proceso de recorrido por cada nodo
										//concatenando en el String hasta encontrar el último nivel solicitado
										$arrLevelValues = $arrCatalogValues;
										$intIndex = 0;
										$intMax = count($arrAttributeIDs);
										$arrCatalogFilter = array("-1" => '');
										while (count($arrLevelValues) > 0 && $intIndex < $intMax) {
											if (isset($arrLevelValues[0])) {
												$intCatKey = $arrLevelValues[0]['key'];
												$strValue = $arrLevelValues[0]['value'];
												$arrCatalogFilter[(string) $intIndex] = $strValue;
												$arrLevelValues = $arrLevelValues[0]['children'];
											}
											
											$intIndex++;
										}
										
										//@JAPR 2013-02-11: Agregada la edición de datos históricos
										if ($surveyInstance->DissociateCatDimens) {
											$intKey = $intIndexDyn;
										}
										else {
											$intKey = @$aRecAnswer['id'];
										}
										$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
										//@JAPR
										$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
										//Primero llena las páginas que pudieran venir vacias para esta pregunta
										if (!isset($arrAnswers[$strQuestionID])) {
											$intNumPages = 0;
											$arrAnswers[$strQuestionID] = array();
										}
										else {
											$intNumPages = count($arrAnswers[$strQuestionID]);
										}
										if ($intPageNum+1 > $intNumPages) {
											for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
												if (!isset($arrAnswers[$strQuestionID][$intCont])) {
													//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
													$anAnswer = $emptyAnswer;
													$anAnswer['id'] = $objQuestion->QuestionID;
													$anAnswer['isNull'] = 1;
													$anAnswer['dirty'] = 0;
													//$anAnswer['catalogFilter'] = $arrCatalogFilter;
													$anAnswer['factKey'] = 0;
													$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
												}
											}
										}
										
										if ($strValue != '') {
											$anAnswer = $emptyAnswer;
											$anAnswer['id'] = $objQuestion->QuestionID;
											$anAnswer['isNull'] = 0;
											$anAnswer['dirty'] = 1;
											$anAnswer['catalogFilter'] = $arrCatalogFilter;
											$anAnswer['answer'] = $strValue;
											$anAnswer['key'] = $intCatKey;
											$anAnswer['factKey'] = $intPageKey;
											$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
										}
										
									}
								}
							}
							break;
							//@JAPR
							
						case sectMasterDet:
							//En el caso de secciones múltiples, la pregunta ya se graba con el key del valor específico del catálogo, así 
							//que si hay respuesta usa directamente dicho valor para cargar la jerarquía completa
							//Se genera un array de las respuestas, un índice por cada registro de la sección
							$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
							
							if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
								continue;
							}
							
							$intFactKey = -1;
							foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
								//@JAPR 2013-02-11: Agregada la edición de datos históricos
								if ($surveyInstance->DissociateCatDimens) {
									//Como se manejan catálogos independientes, ya se tienen los valores exactos que se deben enviar en la instancia
									//del reporte y no es necesario hacer join con el catálogo
									$intCatKey = -1;
									$strValue = '';
									$arrCatalogFilter = array("-1" => '');
									$strCatDimValAnswers = (string) @$aRecAnswer[$objQuestion->SurveyCatField];
									$arrayCatValues = array();
									if ($strCatDimValAnswers != '') {
										$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
										$arrayCatClaDescrip = array();
										$arrayTemp = array();
										$arrayInfo = array();
										foreach($arrayCatPairValues as $element)
										{
											$arrayInfo = explode('_SVSep_', $element);
											//Obtener el cla_descrip y almacenar (key_1178)
											$arrayTemp = explode('_', $arrayInfo[0]);
											$arrayCatClaDescrip[] = $arrayTemp[1];
											//Obtener el valor del catalogo
											$arrayCatValues[] = $arrayInfo[1];
										}
										
										$intIndex = 0;
										$intMax = count($arrAttributeIDs);
										for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
											if (isset($arrayCatValues[$intIndex])) {
												$strValue = $arrayCatValues[$intIndex];
												$arrCatalogFilter[(string) $intIndex] = $strValue;
											}
										}
									}
									
									if ($strValue != '') {
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 0;
										$anAnswer['dirty'] = 1;
										$anAnswer['catalogFilter'] = $arrCatalogFilter;
										$anAnswer['answer'] = $strValue;
										$anAnswer['key'] = $intCatKey;
										$anAnswer['factKey'] = (int) @$intFactKey;
										$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
									}
								}
								else {
									$intData = (int) @$aRecAnswer[$fieldName];
									if ($intData > 0) {
										$intCatKey = 0;
										$strValue = '';
										$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter.$intData.' ');
										//Se supone que sólo puede haber un valor con este Key, así que hace un proceso de recorrido por cada nodo
										//concatenando en el String hasta encontrar el último nivel solicitado
										$arrLevelValues = $arrCatalogValues;
										$intIndex = 0;
										$intMax = count($arrAttributeIDs);
										$arrCatalogFilter = array("-1" => '');
										while (count($arrLevelValues) > 0 && $intIndex < $intMax) {
											if (isset($arrLevelValues[0])) {
												$intCatKey = $arrLevelValues[0]['key'];
												$strValue = $arrLevelValues[0]['value'];
												$arrCatalogFilter[(string) $intIndex] = $strValue;
												$arrLevelValues = $arrLevelValues[0]['children'];
											}
											
											$intIndex++;
										}
										
										if ($strValue != '') {
											$anAnswer = $emptyAnswer;
											$anAnswer['id'] = $objQuestion->QuestionID;
											$anAnswer['isNull'] = 0;
											$anAnswer['dirty'] = 1;
											$anAnswer['catalogFilter'] = $arrCatalogFilter;
											$anAnswer['answer'] = $strValue;
											$anAnswer['key'] = $intCatKey;
											$anAnswer['factKey'] = (int) @$aRecAnswer['FactKey'];
											$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
										}
									}
								}
								$intFactKey--;
								//@JAPR
							}
							break;
						
						default:
							if ($surveyInstance->DissociateCatDimens) {
								//Como se manejan catálogos independientes, ya se tienen los valores exactos que se deben enviar en la instancia
								//del reporte y no es necesario hacer join con el catálogo
								$intCatKey = -1;
								$strValue = '';
								$arrCatalogFilter = array("-1" => '');
								$strCatDimValAnswers = (string) @$objReportData->{$objQuestion->SurveyCatField};
								$arrayCatValues = array();
								if ($strCatDimValAnswers != '') {
									$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
									$arrayCatClaDescrip = array();
									$arrayTemp = array();
									$arrayInfo = array();
									foreach($arrayCatPairValues as $element)
									{
										$arrayInfo = explode('_SVSep_', $element);
										//Obtener el cla_descrip y almacenar (key_1178)
										$arrayTemp = explode('_', $arrayInfo[0]);
										$arrayCatClaDescrip[] = $arrayTemp[1];
										//Obtener el valor del catalogo
										$arrayCatValues[] = $arrayInfo[1];
									}
									
									$intIndex = 0;
									$intMax = count($arrAttributeIDs);
									for ($intIndex = 0; $intIndex < $intMax; $intIndex++) {
										if (isset($arrayCatValues[$intIndex])) {
											$strValue = $arrayCatValues[$intIndex];
											$arrCatalogFilter[(string) $intIndex] = $strValue;
										}
									}
								}
								
								if ($strValue != '') {
									$anAnswer = $emptyAnswer;
									$anAnswer['id'] = $objQuestion->QuestionID;
									$anAnswer['isNull'] = 0;
									$anAnswer['dirty'] = 1;
									$anAnswer['catalogFilter'] = $arrCatalogFilter;
									$anAnswer['answer'] = $strValue;
									$anAnswer['key'] = $intCatKey;
									$arrAnswers[$strQuestionID] = $anAnswer;
								}
							}
							else {
								//En este caso la pregunta ya se graba con el key del valor específico del catálogo, así que si hay respuesta
								//usa directamente dicho valor para cargar la jerarquía completa
								$intData = (int) @$objReportData->$fieldNameKey;
								//@JAPR 2012-11-26: Agregada una validación, en algunos casos se podrían grabar registros de Maestro-Detalle o de otras
								//secciones que NO tuvieran un valor especificado para la pregunta que filtra a la sección dinámica, si sucede que tal
								//registro fué el $entryID, entonces el reporte se cargó sin dato para esta pregunta, pero eso no quiere decir que no
								//exista algun registro de la sección dinámica correctamente grabado, así que antes de descartarlo primero se busca
								//si existe tal registro filtrando por el campo de sección (si está habilitado) o por el primer registro donde sea
								//mayor que 1 que es el registro de No Aplica
								if ($intData <= 1) {
									//Siendo simple choice de catálogo, verificamos si es la generadora de la sección dinámica, si lo es y no trae valor
									//hay que buscar uno adicional
									if($objQuestion->CatalogID == $dynamicSectionCatID) {
										//Verifica si la pregunta es o no la generadora de la sección
										$aSCDynQuestionInstance = null;
										foreach ($arrSCDynQuestionsBySectionID as $aGeneratorQuestion) {
											//echo ("<br>\r\nThis Question: ".$objQuestion->QuestionID);
											//echo ("<br>\r\nGeneratorQuestion: ".$aGeneratorQuestion->QuestionID);
											if ($objQuestion->QuestionID == $aGeneratorQuestion->QuestionID) {
												//echo ("<br>\r\nFound");
												$aSCDynQuestionInstance = $aGeneratorQuestion;
												break;
											}
										}
										//$aSCDynQuestionInstance = @$arrSCDynQuestionsBySectionID[$objQuestion->SectionID];
										if (!is_null($aSCDynQuestionInstance)) {
											//Es la pregunta generadora de la sección dinámica, así que buscamos cualquier otro posible valor dentro
											//de la captura, a sabiendas que hasta la fecha no se puede tener mas de una sección dinámica, pero
											//considerando que eventualmente se podrá así que para esos casos ya debería existir el registro con el
											//EntrySectionID y se filtra por él
											$sql = "SELECT ".$fieldName." AS SCHDynGenKey 
												FROM ".$surveyInstance->SurveyTable." 
												WHERE FactKeyDimVal = ".$intFactKeyDimVal." AND ".$fieldName." > 1";
											if ($useSourceFields) {
												$strEntrySectionFilter = " AND (EntrySectionID = ".$objQuestion->SectionID." OR EntrySectionID = -1 OR EntrySectionID IS NULL)";
											}
											$sql .= " ORDER BY FactKey DESC";
											$aRS = $aRepository->DataADOConnection->Execute($sql);
											if ($aRS !== false) {
												$intData = (int) @$aRS->fields["schdyngenkey"];
											}
										}
									}
								}
								
								//die('$fieldName: '.$fieldName.' $fieldName value: '.@$objReportData->$fieldName);
								if ($intData > 0) {
									$intCatKey = 0;
									$strValue = '';
									$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter.$intData.' ');
									//Se supone que sólo puede haber un valor con este Key, así que hace un proceso de recorrido por cada nodo
									//concatenando en el String hasta encontrar el último nivel solicitado
									$arrLevelValues = $arrCatalogValues;
									$intIndex = 0;
									$intMax = count($arrAttributeIDs);
									$arrCatalogFilter = array("-1" => '');
									while (count($arrLevelValues) > 0 && $intIndex < $intMax) {
										if (isset($arrLevelValues[0])) {
											$intCatKey = $arrLevelValues[0]['key'];
											$strValue = $arrLevelValues[0]['value'];
											$arrCatalogFilter[(string) $intIndex] = $strValue;
											$arrLevelValues = $arrLevelValues[0]['children'];
										}
										
										$intIndex++;
									}
									
									if ($strValue != '') {
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 0;
										$anAnswer['dirty'] = 1;
										$anAnswer['catalogFilter'] = $arrCatalogFilter;
										$anAnswer['answer'] = $strValue;
										$anAnswer['key'] = $intCatKey;
										$arrAnswers[$strQuestionID] = $anAnswer;
									}
								}
							}
							//@JAPr
						break;
					}
				}
				else {
					//Si la pregunta no es de catálogo, entonces simplemente usa como valor la respuesta directa recibida, y puede pertenecer
					//a cualquier tipo de sección aunque en dinámicas depende de si hay o no una pregunta multiple choice la forma en que
					//se generará el elemento del array
					switch ($objSection->SectionType) {
						case sectDynamic:
							//Si se trata de una sección dinámica, se debe generar un array con tantas páginas como existan en el filtro según
							//la respuesta de catálogo correspondiente, ya que aunque sólo generaramos el array con las respuestas que hay en
							//la captura realmente grabada, si no se envian las páginas intermedias vacias se interpretaría erróneamente el
							//draft al momento de desplegar. Como es una pregunta múltiple, adicionalmente hay que agregar como respuesta cada
							//uno de los valores seleccionados en el índice de la página que se corresponda según el catálogo
							//@JAPRWarning: Por el momento sólo se regresarán las páginas que fueron grabadas, pero se debe de modificar tarde o
							//temprano para incluir las páginas completas que mostraría el catálogo
							
							//Esta preguna se debe procesar después de la del catálogo correspondiente, pero como las preguntas se cargan por orden
							//se asume que obviamente al momento de haber llegado aquí ya se procesó la que genera la sección
							//En el caso de secciones dinámicas, las preguntas Simple Choice vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar
							//$aSCDynQuestionInstance = @$arrSCDynQuestionsBySectionID[$intSectionID];
							//if (is_null($aSCDynQuestionInstance)) {
							//	continue;
							//}
							//$arrCatalogFilter = @$arrAnswers[(string) $aSCDynQuestionInstance->QuestionID]['catalogFilter'];
							//Si no hay un filtro de la pregunta de catálogo en este punto, quiere decir que no se contestó por lo que no hay necesidad
							//de generar la sección dinámica tampoco
							//if (is_null($arrCatalogFilter)) {
							//	continue;
							//}
							
							$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
							if (is_null($arrSectionValue)) {
								continue;
							}
							
							$arrSectionValueByKey = array_flip($arrSectionValue);
							$arrSectionPageByKey = array_flip($arrSectionPage);
							$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
							if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
								continue;
							}
							
							foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
								//$intIndex representa el registro negativo en la misma secuencia de la colección de valores dinámicos, ya que durante
								//la edición no existen keys subrrogados del catálogo realmente pues estos pudieron haber cambiado de descripciones,
								//así que se simula usando valores negativos
								$intIndex = ($intIdx + 1) * -1;
								
								//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
								$aDynamicPageDSC = (string) @$aRecAnswer["DynamicPageDSC"];
								if ($blnUseSingleRecord && $aDynamicPageDSC == '') {
									//Si se usa el registro único y este no es él, entonces no lo puede usar para este tipo de preguntas
									continue;
								}
								//@JAPR
								
								$strValue = (string) @$aRecAnswer[$fieldName];
								//@JAPR 2013-02-11: Agregada la edición de datos históricos
								if ($surveyInstance->DissociateCatDimens) {
									$intKey = $intIndex;
								}
								else {
									$intKey = @$aRecAnswer['id'];
								}
								$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
								//@JAPR
								$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
								//Primero llena las páginas que pudieran venir vacias para esta pregunta
								if (!isset($arrAnswers[$strQuestionID])) {
									$intNumPages = 0;
									$arrAnswers[$strQuestionID] = array();
								}
								else {
									$intNumPages = count($arrAnswers[$strQuestionID]);
								}
								if ($intPageNum+1 > $intNumPages) {
									for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
										if (!isset($arrAnswers[$strQuestionID][$intCont])) {
											//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
											$anAnswer = $emptyAnswer;
											$anAnswer['id'] = $objQuestion->QuestionID;
											$anAnswer['isNull'] = 1;
											$anAnswer['dirty'] = 0;
											//$anAnswer['catalogFilter'] = $arrCatalogFilter;
											$anAnswer['factKey'] = 0;
											$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
										}
									}
								}
								
								//Obtiene la referencia al registro de esta página para modificar el valor correspondiente, para este momento
								//ya deben existir todos los registros hasta esta página por lo que nada mas actualiza datos de edición
								$anAnswer = @$arrAnswers[$strQuestionID][$intPageNum];
								if (!is_null($anAnswer)) {
									$anAnswer['isNull'] = 0;
									$anAnswer['dirty'] = 1;
									$anAnswer['answer'] = $strValue;
									$anAnswer['factKey'] = $intPageKey;
									$arrAnswers[$strQuestionID][$intPageNum] = $anAnswer;
								}
							}
							break;
							
						case sectMasterDet:
							//En el caso de secciones múltiples, las preguntas Simple Choice vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar
							$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
							if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
								continue;
							}
							
							foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
								$strValue = (string) @$aRecAnswer[$fieldName];
								
								//Debe agregar el registro incluso si viene vacio
								$anAnswer = $emptyAnswer;
								$anAnswer['id'] = $objQuestion->QuestionID;
								$anAnswer['isNull'] = 0;
								$anAnswer['dirty'] = 1;
								$anAnswer['answer'] = $strValue;
								$anAnswer['factKey'] = (int) @$aRecAnswer['FactKey'];
								$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
							}
							break;
							
						default:
							//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
							$strValue = (string) @$objReportData->$fieldName;
							if (trim($strValue) == '') {
								continue;
							}
							
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['answer'] = $strValue;
							$arrAnswers[$strQuestionID] = $anAnswer;
							break;
					}
				}
				break;
			
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
				//La pregunta Sketch Plus graba la imagen tomada con la cámara la cual debe conservar íntegra, además de la imagen modificada, pero adicionalmente grabará "n" elementos
				//múltiples que representan una colección de puntos colocados sobre el Sketch y cada uno apuntando a un registro de la sección Maestro-detalle configurada, por lo que cuando
				//se utilice como parte de una sección multi-registro, cada registro deberá contener en realidad la colección de estos puntos graficados lo que convierte a esta pregunta
				//en una matriz de "n" x "m" registros, con bloques de la sección maestro-detalle que pertenecen a diferentes registros de la sección donde reside la Sketch (NO hay manera
				//de agrupar los registros de la maestro-detalle directamente, se tienen que reprocesar basados en el orden de la colección en la pregunta Sketch)
				break;
			//@JAPR
			
			case qtpSketch:	
			case qtpSignature:
			case qtpPhoto:
			case qtpOCR:
				//La pregunta tipo foto sólo contiene como extra un comentario, puede pertenecer a cualquier tipo de sección 
				//dependiendo de lo cual puede o no venir un array (en el caso de dinámicas se maneja como si fuera una pregunta Open), sin
				//embargo en este ciclo no se asignan ni fotos ni comentarios, así que eso se deja para mas adelante
				break;
			case qtpAudio:
			case qtpVideo:
			//Conchita agregados audio y video 20140924
			case qtpDocument:
				//La pregunta tipo documento sólo contiene como extra un comentario, puede pertenecer a cualquier tipo de sección
				//dependiendo de lo cual puede o no venir un array (en el caso de dinámicas se maneja como si fuera una pregunta Open), sin
				//embargo en este ciclo no se asignan ni documentos ni comentarios, así que eso se deja para mas adelante
				break;
			
			//Estas preguntas siempre vienen como texto (o según el tipo), con comentario y foto opcional, y pueden pertenecer a cualquier 
			//tipo de sección, dependiendo de lo cual puede o no venir un array. NO se hacen validaciones por el tipo de dato, aunque se 
			//podrían integrar, por ahora se delega eso al App que envía las respuestas
			case qtpOpenDate:
			case qtpOpenTime:
			case qtpOpenNumeric:
			case qtpOpenString:
			case qtpOpenAlpha:
			//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
			case qtpGPS:
			//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
			case qtpMyLocation:
			//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
			case qtpBarCode:
				switch ($objSection->SectionType) {
					//En el caso de secciones dinámicas y múltiples, las preguntas Open vienen como un valor por cada "página" generada,
					//y de esa misma forma es como se deben grabar (es un array en ambos casos), sin embargo en las dinámicas depende del
					//total de páginas que se generaron según la opción seleccionada
					case sectDynamic:
						//Si se trata de una sección dinámica, se debe generar un array con tantas páginas como existan en el filtro según
						//la respuesta de catálogo correspondiente, ya que aunque sólo generaramos el array con las respuestas que hay en
						//la captura realmente grabada, si no se envian las páginas intermedias vacias se interpretaría erróneamente el
						//draft al momento de desplegar. Como es una pregunta múltiple, adicionalmente hay que agregar como respuesta cada
						//uno de los valores seleccionados en el índice de la página que se corresponda según el catálogo
						//@JAPRWarning: Por el momento sólo se regresarán las páginas que fueron grabadas, pero se debe de modificar tarde o
						//temprano para incluir las páginas completas que mostraría el catálogo
						
						//En el caso de secciones dinámicas, estas preguntas vienen como un valor por cada "página" generada,
						//y de esa misma forma es como se deben grabar
						$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
						if (is_null($arrSectionValue)) {
							continue;
						}
						
						$arrSectionValueByKey = array_flip($arrSectionValue);
						$arrSectionPageByKey = array_flip($arrSectionPage);
						$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							//$intIndex representa el registro negativo en la misma secuencia de la colección de valores dinámicos, ya que durante
							//la edición no existen keys subrrogados del catálogo realmente pues estos pudieron haber cambiado de descripciones,
							//así que se simula usando valores negativos
							$intIndex = ($intIdx + 1) * -1;
							
							//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
							$aDynamicPageDSC = (string) @$aRecAnswer["DynamicPageDSC"];
							if ($blnUseSingleRecord && $aDynamicPageDSC == '') {
								//Si se usa el registro único y este no es él, entonces no lo puede usar para este tipo de preguntas
								continue;
							}
							//@JAPR
							
							$strValue = (string) @$aRecAnswer[$fieldName];
							//@JAPR 2013-02-11: Agregada la edición de datos históricos
							if ($surveyInstance->DissociateCatDimens) {
								$intKey = $intIndex;
							}
							else {
								$intKey = @$aRecAnswer['id'];
							}
							$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
							//@JAPR
							$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
							//Primero llena las páginas que pudieran venir vacias para esta pregunta
							if (!isset($arrAnswers[$strQuestionID])) {
								$intNumPages = 0;
								$arrAnswers[$strQuestionID] = array();
							}
							else {
								$intNumPages = count($arrAnswers[$strQuestionID]);
							}
							if ($intPageNum+1 > $intNumPages) {
								for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
									if (!isset($arrAnswers[$strQuestionID][$intCont])) {
										//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 1;
										$anAnswer['dirty'] = 0;
										$anAnswer['factKey'] = 0;
										$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
									}
								}
							}
							
							//Obtiene la referencia al registro de esta página para modificar el valor correspondiente, para este momento
							//ya deben existir todos los registros hasta esta página por lo que nada mas actualiza datos de edición
							$anAnswer = @$arrAnswers[$strQuestionID][$intPageNum];
							if (!is_null($anAnswer)) {
								$anAnswer['isNull'] = 0;
								$anAnswer['dirty'] = 1;
								$anAnswer['answer'] = $strValue;
								$anAnswer['factKey'] = $intPageKey;
								//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
								//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
								if ($objQuestion->QTypeID == qtpGPS || $objQuestion->QTypeID == qtpMyLocation) {
									$arrGPSData = SplitGPSData($strValue);
									$anAnswer['latitude'] = $arrGPSData[0];
									$anAnswer['longitude'] = $arrGPSData[1];
									$anAnswer['accuracy'] = $arrGPSData[2];
								}
								//@JAPR
								$arrAnswers[$strQuestionID][$intPageNum] = $anAnswer;
							}
						}
						
						break;
						
					case sectMasterDet:
						//En el caso de secciones múltiples, las preguntas Open vienen como un valor por cada "página" generada,
						//y de esa misma forma es como se deben grabar
						$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							$strValue = (string) @$aRecAnswer[$fieldName];
							
							//Debe agregar el registro incluso si viene vacio
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['answer'] = $strValue;
							$anAnswer['factKey'] = (int) @$aRecAnswer['FactKey'];
							//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
							//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
							if ($objQuestion->QTypeID == qtpGPS || $objQuestion->QTypeID == qtpMyLocation) {
								$arrGPSData = SplitGPSData($strValue);
								$anAnswer['latitude'] = $arrGPSData[0];
								$anAnswer['longitude'] = $arrGPSData[1];
								$anAnswer['accuracy'] = $arrGPSData[2];
							}
							//@JAPR
							$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
						}
						break;
					
					default:
						//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
						$strValue = (string) @$objReportData->$fieldName;
						if (trim($strValue) == '') {
							continue;
						}
						
						$anAnswer = $emptyAnswer;
						$anAnswer['id'] = $objQuestion->QuestionID;
						$anAnswer['isNull'] = 0;
						$anAnswer['dirty'] = 1;
						$anAnswer['answer'] = $strValue;
						//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
						//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
						if ($objQuestion->QTypeID == qtpGPS || $objQuestion->QTypeID == qtpMyLocation) {
							$arrGPSData = SplitGPSData($strValue);
							$anAnswer['latitude'] = $arrGPSData[0];
							$anAnswer['longitude'] = $arrGPSData[1];
							$anAnswer['accuracy'] = $arrGPSData[2];
						}
						//@JAPR
						$arrAnswers[$strQuestionID] = $anAnswer;
						break;
				}
				break;
			
			case qtpAction:
				//Las preguntas tipo acción son casi idénticas a las Open alfanuméricas, sólo que contienen 3 valores adicionales a grabar
				//que son un Responsable, Deadline y una Categoría, así que la "Respuesta" en si es una combinación de estos 4 datos
				//en el orden: ResponsibleID_SVSep_DeadLine_SVSep_CategoryID_SVSep_ActionText
				switch ($objSection->SectionType) {
					//En el caso de secciones dinámicas y múltiples, las preguntas acción vienen como un valor por cada "página" generada,
					//y de esa misma forma es como se deben grabar (es un array en ambos casos), sin embargo en las dinámicas depende del
					//total de páginas que se generaron según la opción seleccionada
					case sectDynamic:
						//Si se trata de una sección dinámica, se debe generar un array con tantas páginas como existan en el filtro según
						//la respuesta de catálogo correspondiente, ya que aunque sólo generaramos el array con las respuestas que hay en
						//la captura realmente grabada, si no se envian las páginas intermedias vacias se interpretaría erróneamente el
						//draft al momento de desplegar. Como es una pregunta múltiple, adicionalmente hay que agregar como respuesta cada
						//uno de los valores seleccionados en el índice de la página que se corresponda según el catálogo
						//@JAPRWarning: Por el momento sólo se regresarán las páginas que fueron grabadas, pero se debe de modificar tarde o
						//temprano para incluir las páginas completas que mostraría el catálogo
						
						//En el caso de secciones dinámicas, estas preguntas vienen como un valor por cada "página" generada,
						//y de esa misma forma es como se deben grabar
						$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
						if (is_null($arrSectionValue)) {
							continue;
						}
						
						$arrSectionValueByKey = array_flip($arrSectionValue);
						$arrSectionPageByKey = array_flip($arrSectionPage);
						$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							//$intIndex representa el registro negativo en la misma secuencia de la colección de valores dinámicos, ya que durante
							//la edición no existen keys subrrogados del catálogo realmente pues estos pudieron haber cambiado de descripciones,
							//así que se simula usando valores negativos
							$intIndex = ($intIdx + 1) * -1;
							
							//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
							$aDynamicPageDSC = (string) @$aRecAnswer["DynamicPageDSC"];
							if ($blnUseSingleRecord && $aDynamicPageDSC == '') {
								//Si se usa el registro único y este no es él, entonces no lo puede usar para este tipo de preguntas
								continue;
							}
							//@JAPR
							
							$strValue = (string) @$aRecAnswer[$fieldName];
							$arrActionData = array();
							if (trim($strValue) != '') {
								$arrActionData = explode('_SVSep_', $strValue);
							}
							
							//@JAPR 2013-02-11: Agregada la edición de datos históricos
							if ($surveyInstance->DissociateCatDimens) {
								$intKey = $intIndex;
							}
							else {
								$intKey = @$aRecAnswer['id'];
							}
							$intPageKey = @$arrPagesBySectionValues[$intSectionID][$intKey];
							//@JAPR
							$intPageNum = (int) @$arrSectionPageByKey[$intPageKey];
							//Primero llena las páginas que pudieran venir vacias para esta pregunta
							if (!isset($arrAnswers[$strQuestionID])) {
								$intNumPages = 0;
								$arrAnswers[$strQuestionID] = array();
							}
							else {
								$intNumPages = count($arrAnswers[$strQuestionID]);
							}
							if ($intPageNum+1 > $intNumPages) {
								for ($intCont = 0; $intCont <= $intPageNum; $intCont++) {
									if (!isset($arrAnswers[$strQuestionID][$intCont])) {
										//Agrega una registro vacio, si mas adelante se le asigna un valor entonces se modificará
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 1;
										$anAnswer['dirty'] = 0;
										$anAnswer['factKey'] = 0;
										$arrAnswers[$strQuestionID][$intCont] = $anAnswer;
									}
								}
							}
							
							//Obtiene la referencia al registro de esta página para modificar el valor correspondiente, para este momento
							//ya deben existir todos los registros hasta esta página por lo que nada mas actualiza datos de edición
							$anAnswer = @$arrAnswers[$strQuestionID][$intPageNum];
							if (!is_null($anAnswer)) {
								$anAnswer['isNull'] = 0;
								$anAnswer['dirty'] = 1;
								if (isset($arrActionData[0])) {
									$anAnswer['responsibleID'] = (string) @$arrActionData[0];
								}
								if (isset($arrActionData[1])) {
									$anAnswer['daysDueDate'] = (string) @$arrActionData[1];
								}
								if (isset($arrActionData[2])) {
									$anAnswer['categoryID'] = (string) @$arrActionData[2];
								}
								$anAnswer['answer'] = (string) @$arrActionData[3];
								$anAnswer['factKey'] = $intPageKey;
								$arrAnswers[$strQuestionID][$intPageNum] = $anAnswer;
							}
						}
						break;
						
					case sectMasterDet:
						//En el caso de secciones múltiples, las preguntas acción vienen como un valor por cada "página" generada,
						//y de esa misma forma es como se deben grabar
						$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
						if (is_null($objAnswersColl) || !is_array($objAnswersColl)) {
							continue;
						}
						
						foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
							$strValue = (string) @$aRecAnswer[$fieldName];
							$arrActionData = array();
							if (trim($strValue) != '') {
								$arrActionData = explode('_SVSep_', $strValue);
							}
							
							//Debe agregar el registro incluso si viene vacio
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							if (isset($arrActionData[0])) {
								$anAnswer['responsibleID'] = (string) @$arrActionData[0];
							}
							if (isset($arrActionData[1])) {
								$anAnswer['daysDueDate'] = (string) @$arrActionData[1];
							}
							if (isset($arrActionData[2])) {
								$anAnswer['categoryID'] = (string) @$arrActionData[2];
							}
							$anAnswer['answer'] = (string) @$arrActionData[3];
							$anAnswer['factKey'] = (int) @$aRecAnswer['FactKey'];
							$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
						}
						break;
					
					default:
						//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
						$strValue = (string) @$objReportData->$fieldName;
						if (trim($strValue) == '') {
							continue;
						}
						$arrActionData = explode('_SVSep_', $strValue);
						
						$anAnswer = $emptyAnswer;
						$anAnswer['id'] = $objQuestion->QuestionID;
						$anAnswer['isNull'] = 0;
						$anAnswer['dirty'] = 1;
						if (isset($arrActionData[0])) {
							$anAnswer['responsibleID'] = (string) @$arrActionData[0];
						}
						if (isset($arrActionData[1])) {
							$anAnswer['daysDueDate'] = (string) @$arrActionData[1];
						}
						if (isset($arrActionData[2])) {
							$anAnswer['categoryID'] = (string) @$arrActionData[2];
						}
						$anAnswer['answer'] = (string) @$arrActionData[3];
						$arrAnswers[$strQuestionID] = $anAnswer;
						break;
				}
				break;
			
			default:
				//Cualquier nuevo tipo de pregunta será ignorado pues no se sabe como capturarlo
				break;
		}
	}
	
	//@JAPR 2013-05-15: Corregido un bug en la edición con preguntas tipo Foto, como estas no se generan en el array de datos múltiples, se estaban
	//asignando las fotos al primer índice de la pregunta sin importar a cual pertenecieran en realidad ya que el array de respuestas estaba vacio,
	//se deberá verificar el array de datos para agregar manualmente respuestas vacias a estos tipos de preguntas
	//$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$intSectionID];
	//Array multidimensional indexado por sección (Dinámica o Maestro-Detalle) y posteriormente por pregunta que contiene el array de los FactKeys
	//en el orden que fueron generados para poder ser utilizados en el restaurado de las imagenes (y documentos)
	$arrProcessedPhotoQuestions = array();
	
	//Agrega las fotos a las preguntas que estén asignadas, si no había una respueta para dicha pregunta, genera la instancia sólo
	//con el contenido del comentario
	$sql = "SELECT FactKey, MainFactKey, SurveyID, QuestionID, PathImage
		FROM SI_SV_SurveyAnswerImage WHERE SurveyID = ".$surveyID." AND FactKey = ".$intFactKeyDimVal." 
		ORDER BY FactKey, QuestionID, MainFactKey";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS !== false)
	{
		while (!$aRS->EOF) {
			$intQuestionID = (int) @$aRS->fields['questionid'];
			$objQuestion = @$arrQuestionsByID[$intQuestionID];
			if (is_null($objQuestion)) {
				continue;
			}
			
			$intMainFactKey = (int) @$aRS->fields['mainfactkey'];
			$pathimage = trim($aRS->fields["pathimage"]);
			if($pathimage != "")
			{
				$pathimage = "surveyimages/".trim($_SESSION["PABITAM_RepositoryName"])."/".$pathimage;
				$objSection = @$arrSectionsByID[$objQuestion->SectionID];
				if (is_null($objSection) || $objSection->SectionType == sectFormatted) {
					//No debería ser posible que no existiera la sección, pero si se da entonces continua con la siguiente pregunta para evitar errores
					//Tampoco se pueden procesar preguntas de secciones formateadas
					continue;
				}
				
				$strQuestionID = (string)$objQuestion->QuestionID;
				//@JAPR 2013-05-15: Corregido un bug en la edición con preguntas tipo Foto, como estas no se generan en el array de datos múltiples, se estaban
				//asignando las fotos al primer índice de la pregunta sin importar a cual pertenecieran en realidad ya que el array de respuestas estaba vacio,
				//se deberá verificar el array de datos para agregar manualmente respuestas vacias a estos tipos de preguntas
				//Genera el array de los FactKeys de la pregunta tipo Foto/Firma si es la primera vez que se intenta utilizar
				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				if ($objQuestion->QTypeID == qtpPhoto || $objQuestion->QTypeID == qtpSignature || $objQuestion->QTypeID == qtpSketch || $objQuestion->QTypeID == qtpSketchPlus 
						|| $objQuestion->QTypeID == qtpOCR) {
					if ($objSection->SectionType == sectMasterDet) {	//$objSection->SectionType == sectDynamic
						if (!isset($arrProcessedPhotoQuestions[$objQuestion->SectionID])) {
							$arrProcessedPhotoQuestions[$objQuestion->SectionID] = array();
						}
						
						if (!isset($arrProcessedPhotoQuestions[$objQuestion->SectionID][$intQuestionID])) {
							//Genera el array de FactKeys de esta pregunta y llena el objeto de respuestas con instancias vacias
							$arrProcessedPhotoQuestions[$objQuestion->SectionID][$intQuestionID] = array();
							$objAnswersColl = @$objReportData->ArrayMasterDetSectionValues[$objQuestion->SectionID];
							if (!is_null($objAnswersColl) && is_array($objAnswersColl)) {
								foreach ($objAnswersColl as $intIdx => $aRecAnswer) {
									$intFactKey = (int) @$aRecAnswer['FactKey'];
									if ($intFactKey > 0) {
										$anAnswer = $emptyAnswer;
										$anAnswer['id'] = $objQuestion->QuestionID;
										$anAnswer['isNull'] = 0;
										$anAnswer['dirty'] = 1;
										//$anAnswer['photo'] = $pathimage;
										$anAnswer['factKey'] = $intFactKey;
										$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
									}
								}
							}
						}
					}
				}
				//@JAPR
				
				switch ($objSection->SectionType) {
					case sectDynamic:
						//Las páginas dinámicas a diferencia de las Maestro-Detalle, se tienen que pregenerar con los valores adecuados
						//según el filtro, así que en caso de no existir el registro ya no lo agrega, sólo localiza al que pertenece
						//esta imagen
						$intIdx = 0;
						if (isset($arrAnswers[$strQuestionID])) {
							if (!is_array($arrAnswers[$strQuestionID])) {
								//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
								continue;
							}

							foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
								$intFactKey = (int) @$objAnswer['factKey'];
								if ($intFactKey == $intMainFactKey) {
									//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
									$arrAnswers[$strQuestionID][$intIdx]['photo'] = $pathimage;
								}
							}
						}
						break;
					
					case sectMasterDet:
						//En estas secciones hay múltiples registros, así que debe localizar al que pertenece este comentario, si no lo
						//encuentra entonces lo agrega al final
						$blnAdd = true;
						$intIdx = 0;
						if (isset($arrAnswers[$strQuestionID])) {
							if (!is_array($arrAnswers[$strQuestionID])) {
								//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
								continue;
							}

							foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
								$intFactKey = (int) @$objAnswer['factKey'];
								if ($intFactKey == $intMainFactKey) {
									//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
									$arrAnswers[$strQuestionID][$intIdx]['photo'] = $pathimage;
									$blnAdd = false;
								}
							}
						}

						//Si no se encontró el registro de la captura, tiene que agregar uno nuevo
						if ($blnAdd) {
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['photo'] = $pathimage;
							$anAnswer['factKey'] = $intMainFactKey;
							$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
						}
						break;

					default:
						//Se trata de una pregunta con respuesta única, así que sólo agrega la instancia y el comentario
						if (!isset($arrAnswers[$strQuestionID])) {
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['photo'] = $pathimage;
							$arrAnswers[$strQuestionID] = $anAnswer;
						}
						else {
							$arrAnswers[$strQuestionID]['photo'] = $pathimage;
						}
						break;
				}
			}

			$aRS->MoveNext();
		}
	}
	
	//Agrega los documentos a las preguntas que estén asignadas, si no había una respueta para dicha pregunta, genera la instancia sólo
	//con el contenido del comentario
	$sql = "SELECT FactKey, MainFactKey, SurveyID, QuestionID, PathDocument
		FROM SI_SV_SurveyAnswerDocument WHERE SurveyID = ".$surveyID." AND FactKey = ".$intFactKeyDimVal." 
		ORDER BY FactKey, QuestionID, MainFactKey";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS !== false)
	{
		while (!$aRS->EOF) {
			$intQuestionID = (int) @$aRS->fields['questionid'];
			$objQuestion = @$arrQuestionsByID[$intQuestionID];
			if (is_null($objQuestion)) {
				continue;
			}

			$intMainFactKey = (int) @$aRS->fields['mainfactkey'];
			$pathdocument = trim($aRS->fields["pathdocument"]);
			if($pathdocument != "")
			{
				$pathdocument = "surveydocuments/".trim($_SESSION["PABITAM_RepositoryName"])."/".$pathdocument;
				$objSection = @$arrSectionsByID[$objQuestion->SectionID];
				if (is_null($objSection) || $objSection->SectionType == sectFormatted) {
					//No debería ser posible que no existiera la sección, pero si se da entonces continua con la siguiente pregunta para evitar errores
					//Tampoco se pueden procesar preguntas de secciones formateadas
					continue;
				}

				$strQuestionID = (string)$objQuestion->QuestionID;
				switch ($objSection->SectionType) {
					case sectDynamic:
						//Las páginas dinámicas a diferencia de las Maestro-Detalle, se tienen que pregenerar con los valores adecuados
						//según el filtro, así que en caso de no existir el registro ya no lo agrega, sólo localiza al que pertenece
						//este comentario
						$intIdx = 0;
						if (isset($arrAnswers[$strQuestionID])) {
							if (!is_array($arrAnswers[$strQuestionID])) {
								//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
								continue;
							}

							foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
								$intFactKey = (int) @$objAnswer['factKey'];
								if ($intFactKey == $intMainFactKey) {
									//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
									$arrAnswers[$strQuestionID][$intIdx]['document'] = $pathdocument;
								}
							}
						}
						break;

					case sectMasterDet:
						//En estas secciones hay múltiples registros, así que debe localizar al que pertenece este comentario, si no lo
						//encuentra entonces lo agrega al final
						$blnAdd = true;
						$intIdx = 0;
						if (isset($arrAnswers[$strQuestionID])) {
							if (!is_array($arrAnswers[$strQuestionID])) {
								//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
								continue;
							}

							foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
								$intFactKey = (int) @$objAnswer['factKey'];
								if ($intFactKey == $intMainFactKey) {
									//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
									$arrAnswers[$strQuestionID][$intIdx]['document'] = $pathdocument;
									$blnAdd = false;
								}
							}
						}

						//Si no se encontró el registro de la captura, tiene que agregar uno nuevo
						if ($blnAdd) {
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['document'] = $pathdocument;
							$anAnswer['factKey'] = $intMainFactKey;
							$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
						}
						break;

					default:
						//Se trata de una pregunta con respuesta única, así que sólo agrega la instancia y el comentario
						if (!isset($arrAnswers[$strQuestionID])) {
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['document'] = $pathdocument;
							$arrAnswers[$strQuestionID] = $anAnswer;
						}
						else {
							$arrAnswers[$strQuestionID]['document'] = $pathdocument;
						}
						break;
				}
			}

			$aRS->MoveNext();
		}
	}
	
	//Agrega los comentarios a las preguntas que estén asignadas, si no había una respueta para dicha pregunta, genera la instancia sólo
	//con el contenido del comentario
	$sql = "SELECT FactKey, MainFactKey, SurveyID, QuestionID, StrComment 
		FROM SI_SV_SurveyAnswerComment WHERE SurveyID = ".$surveyID." AND FactKey = ".$intFactKeyDimVal." 
		ORDER BY FactKey, QuestionID, MainFactKey";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS !== false)
	{
	    while (!$aRS->EOF) {
	    	$intQuestionID = (int) @$aRS->fields['questionid'];
			$objQuestion = @$arrQuestionsByID[$intQuestionID];
			if (is_null($objQuestion)) {
				continue;
			}
			
	    	$intMainFactKey = (int) @$aRS->fields['mainfactkey'];
			$strComment = trim($aRS->fields["strcomment"]);
			if($strComment != "")
			{
				$objSection = @$arrSectionsByID[$objQuestion->SectionID];
				if (is_null($objSection) || $objSection->SectionType == sectFormatted) {
					//No debería ser posible que no existiera la sección, pero si se da entonces continua con la siguiente pregunta para evitar errores
					//Tampoco se pueden procesar preguntas de secciones formateadas
					continue;
				}
				
				$strQuestionID = (string)$objQuestion->QuestionID;
				switch ($objSection->SectionType) {
						case sectDynamic:
							//Las páginas dinámicas a diferencia de las Maestro-Detalle, se tienen que pregenerar con los valores adecuados
							//según el filtro, así que en caso de no existir el registro ya no lo agrega, sólo localiza al que pertenece
							//este comentario
							$intIdx = 0;
							if (isset($arrAnswers[$strQuestionID])) {
								if (!is_array($arrAnswers[$strQuestionID])) {
									//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
									continue;
								}
								
								foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
									$intFactKey = (int) @$objAnswer['factKey'];
									if ($intFactKey == $intMainFactKey) {
										//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
										$arrAnswers[$strQuestionID][$intIdx]['comment'] = $strComment;
									}
								}
							}
							break;
						
						case sectMasterDet:
							//En estas secciones hay múltiples registros, así que debe localizar al que pertenece este comentario, si no lo 
							//encuentra entonces lo agrega al final
							$blnAdd = true;
							$intIdx = 0;
							if (isset($arrAnswers[$strQuestionID])) {
								if (!is_array($arrAnswers[$strQuestionID])) {
									//Si por alguna razón está asignado pero no es un array, omite esta asignación, aunque eso no debería ser posible
									continue;
								}
								
								foreach ($arrAnswers[$strQuestionID] as $intIdx => $objAnswer) {
									$intFactKey = (int) @$objAnswer['factKey'];
									if ($intFactKey == $intMainFactKey) {
										//Encontró el registro de esta captura, así que sobreescribe y ya no agrega uno nuevo
										$arrAnswers[$strQuestionID][$intIdx]['comment'] = $strComment;
										$blnAdd = false;
									}
								}
							}
							
							//Si no se encontró el registro de la captura, tiene que agregar uno nuevo
							if ($blnAdd) {
								$anAnswer = $emptyAnswer;
								$anAnswer['id'] = $objQuestion->QuestionID;
								$anAnswer['isNull'] = 0;
								$anAnswer['dirty'] = 1;
								$anAnswer['comment'] = $strComment;
								$anAnswer['factKey'] = $intMainFactKey;
								$arrAnswers[$strQuestionID][$intIdx] = $anAnswer;
							}
							break;
							
					default:
						//Se trata de una pregunta con respuesta única, así que sólo agrega la instancia y el comentario
						if (!isset($arrAnswers[$strQuestionID])) {
							$anAnswer = $emptyAnswer;
							$anAnswer['id'] = $objQuestion->QuestionID;
							$anAnswer['isNull'] = 0;
							$anAnswer['dirty'] = 1;
							$anAnswer['comment'] = $strComment;
							$arrAnswers[$strQuestionID] = $anAnswer;
						}
						else {
							$arrAnswers[$strQuestionID]['comment'] = $strComment;
						}
						break;
				}
			}
			
	        $aRS->MoveNext();
	    }
	}
	
	$arrData['isOutbox'] = 1;
	$arrData['answers'] = $arrAnswers;
	
	//Conchita * checar si tiene firma
	if($surveyInstance->HasSignature == 1)
	{
		$strSignatureData = '';
		$arrData['signature'] =  $strSignatureData;
	}
	
	//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
	//Agrega el parámetro que indica la cantidad de registros de las secciones Maestro-Detalle
	foreach ($arrMasterSectionRecs as $intSectionID => $intTotalRecords) {
		$postKey = "masterSectionRecs".$intSectionID;
		$_POST[$postKey] = $intTotalRecords +1;
	}
	//@JAPR
	
    $return['target']['result'] = $arrData;
    return $return;
}

//@JAPR 2013-12-17: Agregada la validación para ignorar errores cuando si se logró procesar el Outbox
function checkOutboxFile($aRepository) {
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	
	$return['error'] = false;
	if (trim($strFileName) == '') {
        $return['error'] = true;
        $return['errmsg'] = 'File name not specified';
		return $return;
	}
	
	if (strpos(strtolower($strFileName), '.dat') === false) {
		$strFileName .= '.dat';
	}
	
	$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	clearstatcache();
	if (!file_exists($path)) {
		if (!mkdir($path, null, true)) {
	        $return['error'] = true;
    	    $return['errmsg'] = 'Project synchronization path could not be created';
			return $return;
		}
	}
	
	$strCompletePath = $path."\\".$strFileName;
	if (!file_exists($strCompletePath)) {
        $return['error'] = true;
        $return['errmsg'] = 'File not found';
		return $return;
	}
	
	$strJSON = @file_get_contents($strCompletePath);
	if ($strJSON === false) {
        $return['error'] = true;
        $return['errmsg'] = 'Error reading the file';
		return $return;
	}
	
	//Termina sin error después de confirmar que el archivo existe y se puede leer su contenido
	$return['fileFound'] = 1;
	return $return;
}
//@JAPR

//@JAPR 2012-09-18: Agregada la sincronización en la v4 del App
/* Identifica los parámetros recibidos y los convierte a los que se usaban en el antiguo método saveAnswersMobile2 para que puedan ser grabados
por el método original durante una sincronización
//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
//Reenviado el parámetro $theUser para no tener que cargarlo dentro de la función
*/
function storeSurveyCapture($aRepository, $aUser = null) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("function storeSurveyCapture", 1, 1, "color:purple;");
	}
	
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	global $usePhotoPaths;
	global $useDocumentPaths;
	//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	global $useRecoveryPaths;
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	global $objReportData;
	global $arrEditedFactKeys;
	//@JAPR 2012-11-23: Corregida la fecha de inicio de actualización
	global $strLastUpdateDate;
	global $strLastUpdateEndDate;
	global $strLastUpdateTime;
	global $strLastUpdateEndTime;
	//@JAPR 2013-02-13: Agregada la edición de datos históricos
	global $arrSectionValuesByDesc;
	global $arrSectionPagesByDesc;
	//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//global $arrSectionValuesByID;	//Array local usado para generar los datos del POST, por simplicidad se dejará con los keys subrrogados recibidos aunque hubieran sido remapeados, ya que solo se usa para determinar la posición dentro del POST 
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	global $arrSectionValuesByMultiChoiceAnswerFromApp;
	//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	//global $arrSectionOptionByMultiChoiceAnswerFromApp;
	//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	global $jsonData;
	//@JAPR 2013-08-26: Agregada una validación para grabar a partir del filtro en lugar de la respuesta,
	//esto porque en algunos móviles (caso de Pulso) la respuesta de simple choice estaba llegando como
	//un valor de hora siendo que el key y catalogFilter si venían correctamente del catálogo, así que
	//como no se logró determinar la causa de esto, se validará en el server
	global $appVersion;
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	global $arrDateValues;
	global $arrDimKeyValues;
	global $arrDimDescValues;
	global $arrIndicValues;
	global $arrAllDimensions;
	global $arrIndicFields;
	global $strIndicFieldsArrayCode;
	global $gbEnableIncrementalAggregations;
	global $gbTestIncrementalAggregationsData;
	global $intComboID;
	global $arrDefDimValues;
	global $blnWebMode;
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	$aSectionEvalRecord = array();
	global $arrQuestionScoresFromApp;
	$arrQuestionScoresFromApp = array();
	//@JAPR 2014-11-24: Agregadas variables para las secciones
	global $aSectionEvalPage;
	$aSectionEvalPage = array();
	//@JAPR 2015-05-08: Corregido un bug, al tener una sección dinámica previa a una sección Inline sin catálogo, la función getCatValueKeys
	//usada en esta función generaba un die porque no estaba asignada la variable $gblShowErrorSurvey, siendo que simplemente esperaba un
	//error y continuar regresando un false tal como pasaría en el interior del proceso de grabado (#NFWDPN)
	//Se asignará esta variable sólo previo a utilizar la función de error, ya que no se sabe que otro efecto podría provocar (se respaldará)
	global $gblShowErrorSurvey;
	$blShowErrorSurvey = $gblShowErrorSurvey;
	//@JAPR 2015-08-24: Enviados todos los valores de los atributos al server sin importar si se usaron o no
	global $garrCatalogDefs;
	global $garrCatalogData;
	//@JAPR
	
	//EVEG se agrego esta variable global para usarla en el envio de correo del data destination
	global $arrAnswers;
	
	//Variables para reprocesos de destinos
	global $taskIDRePro;
	$taskIDRePro = getParamValue('TaskID', 'both', "(string)");	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("taskIDRePro:".$taskIDRePro, 1, 1, "color:purple;");
	}

	if (!isset($arrDateValues)) {
		$arrDateValues = array();
	}
	if (!isset($arrDimKeyValues)) {
		$arrDimKeyValues = array();
	}
	if (!isset($arrDimDescValues)) {
		$arrDimDescValues = array();
	}
	if (!isset($arrIndicValues)) {
		$arrIndicValues = array();
	}
	if (!isset($arrAllDimensions)) {
		$arrAllDimensions = array();
	}
	if (!isset($arrIndicFields)) {
		$arrIndicFields = array();
	}
	if (!isset($intComboID)) {
		$intComboID = 0;
	}
	if (!isset($strIndicFieldsArrayCode)) {
		$strIndicFieldsArrayCode = '';
	}
	if (!isset($arrDefDimValues)) {
		$arrDefDimValues = array();
	}
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n kpiUser Array");
		PrintMultiArray($arrDefDimValues);
	}
	
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	//Si se utilizarán agregaciones incrementales, verifica que este repositorio las tenga activadas, así como que esta encuesta tenga agregaciones
	//definidas, de lo contrario desactiva la bandera por el resto de la sesión para no afectar al grabado
	if ($gbEnableIncrementalAggregations) {
		require_once('settingsvariable.inc.php');
		$objSetting = @BITAMSettingsVariable::NewInstanceWithName($aRepository, 'USEINCREMENTALAGGREGATIONS');
		if (is_null($objSetting) || ((int) trim($objSetting->SettingValue)) == 0)
		{
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nIncremental aggregations disabled");
			}
			$gbEnableIncrementalAggregations = false;
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nIncremental aggregations enabled");
			}
		}
	}
	if ($gbTestIncrementalAggregationsData) {
		$gbEnableIncrementalAggregations = true;
	}
	
	//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
	//En caso de estar grabando por una pregunta Update Data, no se invoca al proceso de grabado normal sino a la función
	//que solo ejecutará el destino de datos recibido
	$intDataDestinationID = getParamValue('dataDestinationID', 'both', "(int)");
	$return = array();
	//@JAPR
	
	$status = 'OK';
	//Este es el punto inicial de grabado de las capturas, así que aquí asigna el valor de la fecha/hora de sincronización
	$syncDate = date("Y-m-d");
	$syncTime = date("H:i:s");
	//@JAPR
	
	// ***********************************************************************************************************************
	// Grabado del archivo de respuestas obtenido desde el App
	// ***********************************************************************************************************************
	$blnDebugSave = getParamValue('debugSave', 'both', "(boolean)");
	//$blnDebugSave = true;
	if ($blnDebugSave)
	{
		ob_start();
		//echo("Post Data: <br>");
		echo('$POST = ');
		var_export($_POST);
		echo(";\r\n");
		//echo("Get Data: <br>");
		echo('$GET =');
		var_export($_GET);
		echo(";\r\n");
	    $strData = trim(ob_get_contents());
		ob_end_clean();
  		$statlogfile = "./uploaderror/ESurvey_SyncData_".date('YmdHis').".log";
	  	@error_log($strData, 3, $statlogfile);
	    header("Content-Type: text/plain");
	    echo("Data file was stored in the server");
	    exit();
	}
	elseif (false)
		{
		//Activar sólo si se quiere dejar registro de los datos subidos por los encuestadores, se puede incluso condicionar a sólo ciertas cuentas
		//Es similar al parámetro debugSave con la excepción de que no manda la salida de datos al buffer sino que lo maneja todo como variables
		//para simplemente grabarlas en un archivo y atrapa todos los errores para no interrumpir el proceso de grabado
		try
		{
			$kpiUser = '';
		    if (isset($_POST['UserID']))
		    {
			    $kpiUser = $_POST['UserID'];
		    }
			
			$thisDate = date("Y-m-d H:i:s");
			if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
			{
				$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
			}
			$thisDate = str_replace(':', '', $thisDate);
			$thisDate = str_replace('-', '', $thisDate);
			$thisDate = str_replace(' ', '', $thisDate);

			$agendaID = 0;
			if (array_key_exists("agendaID", $_POST))
			{
				$agendaID = (int) $_POST["agendaID"];
			}
			
		    $surveyID = 0;
		    if (array_key_exists("surveyID", $_POST)) {
		        $surveyID = (int) $_POST["surveyID"];
		    }
		    
		    if (trim($kpiUser) != '' && $thisDate != '' && $surveyID > 0)
		    {
		    	$strData = "";
				$strData .= '$POST = '.var_export($_POST, true).";\r\n";
				$strData .= '$GET ='.var_export($_GET, true).";\r\n";
			    
		  		$statlogfile = "./uploaderror/SyncData_".$kpiUser."_Ag".$agendaID."_Sv".$surveyID."_Dte".$thisDate.".log";
			  	@error_log($strData, 3, $statlogfile);
		    }
		}
		catch (Exception $e)
		{
			//No es necesario reportar este error
		}
	}
	
	//@JAPR 2012-11-23: El parámetro "edit" es enviado por el browser al grabar una edición, pero si se sabe que un archivo ya en el server 
	//corresponde con una edición y se reprocesa en forma directa mediante una URL forzada (o el proceso de "Re-upload") entonces no necesariamente
	//pudiera venir este parámetro, por lo que se leerá del propio contenido del archivo para saber que se trata de una edición
	//@JAPRWarning: El comentario anterior es buena idea, pero en la implementación inicial se optará por mejor en el proceso de Re-Upload enviar
	//manualmente el parámetro "edit", aunque en el grabado de edición desde el browser si se incluirá dicho parámetro en el archivo que llega al server
	$blnEdit = (bool) getParamValue('edit', 'both', "(int)");
	//@JAPR 2013-03-19: Variable para forzar al uso de registro único (sólo activar este código si se quiere un servicio eSurveyV4QA para 
	//actualización a esta funcionalidad)
	//global $ActivateSingleRecordConfigForUpdate;
	//$ActivateSingleRecordConfigForUpdate = ($blnEdit)?1:0;
	//@JAPR
	
	// ***********************************************************************************************************************
	//Inicia la identificación de parámetros
	// ***********************************************************************************************************************
	//Obtiene el contenido de la captura del Outbox, si no se recibió como parámetro entonces lo busca en disco
	$strJSON = @getParamValue('data', 'both', "(string)", true);
	if (is_null($strJSON) || trim($strJSON) == '') {
		$strFileName = getParamValue('filename', 'both', "(string)", true);
		if (trim($strFileName) == '') {
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			if ($intDataDestinationID) {
				$return['error'] = true;
				$return['errmsg'] = 'No survey data was received for this outbox';
				return $return;
			}
			else {
				return 'errmsg=No data was received for this outbox';
			}
			//@JAPR
		}
		
		if (strpos(strtolower($strFileName), '.dat') === false) {
			$strFileName .= '.dat';
		}
		$path = getcwd()."\\"."syncdata\\".trim($_SESSION["PABITAM_RepositoryName"]);
	
		$strCompletePath = $path."\\".$strFileName;
		$strJSON = @file_get_contents($strCompletePath);
		if ($strJSON === false)
		{
	    	generateErrorMessage(-408, "The data file name specified was not found: ".$strCompletePath);
		}
	}
	
	//@JAPR 2012-10-23: Corregida la sincronización de encuestas, si el archivo JSon recibido del Outbox era muy grande, el json_decode no
	//funcionaba así que se cambió el método de parseo pero se regresaba ahora un objeto, por lo que hubo que convertirlo a array de nuevo
	//die($strJSON);
	require_once('json.class.php');
	$json = new Services_JSON();
	//$jsonData = $json->decode($strJSON);
	//@Conchita: Integrado por @JAPR 2014-10-02
	//Se cambió porque no llegaba Answers en el JSon cuando la imagen de las preguntas Sketch era muy grande (+150kb)
	//$jsonData = $json->decode($strJSON); //antes estaba asi
	$jsonData = json_decode($strJSON); //ahora se puso asi
	//@Conchita
	$jsonData = object_to_array($jsonData);
	$blnDecodeUTF8 = getParamValue('utf8decode', 'both', "(int)", true);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nblnDecodeUTF8: $blnDecodeUTF8<br>\r\n");
		echo("<br>\r\njsonData");
		PrintMultiArray($jsonData);
	}
	
	//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
	//Para la versión V6 va a necesitar la colección de Scores de cada opción de respuesta recibida, así que es necesario agregar el dato optionScores
	$_POST["optionScores"] = @$jsonData["optionScores"];
	if (!is_array($_POST["optionScores"])) {
		$_POST["optionScores"] = array();
	}
	
	//@JAPR 2015-08-24: Enviados todos los valores de los atributos al server sin importar si se usaron o no
	$garrCatalogDefs = @$jsonData["allAttribs"];
	if (is_null($garrCatalogDefs)) {
		$garrCatalogDefs = array();
	}
	
	$garrCatalogData = @$jsonData["allAttribValues"];
	if (is_null($garrCatalogData)) {
		$garrCatalogData = array();
	}
	//die();
	//@JAPR
	
	//Agrega los parámetros fijos al $_POST
	//@JAPR 2012-10-10: Corregido un bug, estos dos parámetros ya tuvieron que haber llegado asignados o no hubiera llegado a este punto, así que
	//no se requiere sobreescribirlos
	/*
	$_POST["UserID"] = @$jsonData['user'];
	if (is_null($_POST["UserID"])) {
		unset($_POST["UserID"]);
		$_POST["UserID"] = @$_GET['UserID'];
	}
	$_POST["Password"] = @$jsonData['Password'];
	if (is_null($_POST["Password"])) {
		unset($_POST["Password"]);
		$_POST["Password"] = @$_GET['Password'];
	}
	*/
	//Finalmente se deben sobreescribir los parámetros sólo si en $_POST no vienen y en $_GET si, porque el proceso original usa $_POST
	//pero a este código se puede llegar mediante una URL de prueba que envía sólo como $_GET
	if ((!isset($_POST["UserID"]) || is_null($_POST["UserID"])) && isset($_GET["UserID"])) {
		$_POST["UserID"] = (string) @$_GET['UserID'];
	}
	if ((!isset($_POST["Password"]) || is_null($_POST["Password"])) && isset($_GET["Password"])) {
		$_POST["Password"] = (string) @$_GET['Password'];
	}
	//@JAPR
	
	$_POST["surveyDate"] = @$jsonData['surveyDate'];
	if (is_null($_POST["surveyDate"])) {
		unset($_POST["surveyDate"]);
	}
	else {
		$strLastUpdateDate = $_POST["surveyDate"];
	}
	$_POST["surveyHour"] = @$jsonData['surveyHour'];
	if (is_null($_POST["surveyHour"])) {
		unset($_POST["surveyHour"]);
	}
	else {
		$strLastUpdateTime = $_POST["surveyHour"];
	}
	$_POST["surveyEndDate"] = @$jsonData['surveyEndDate'];
	if (is_null($_POST["surveyEndDate"])) {
		unset($_POST["surveyEndDate"]);
	}
	else {
		$strLastUpdateEndDate = $_POST["surveyEndDate"];
	}
	$_POST["surveyEndHour"] = @$jsonData['surveyEndHour'];
	if (is_null($_POST["surveyEndHour"])) {
		unset($_POST["surveyEndHour"]);
	}
	else {
		$strLastUpdateEndTime = $_POST["surveyEndHour"];
	}
	
	//@MABH20130221
	$_POST["serverSurveyDate"] = @$jsonData['serverSurveyDate'];
	if (is_null($_POST["serverSurveyDate"])) {
		unset($_POST["serverSurveyDate"]);
	}
	$_POST["serverSurveyHour"] = @$jsonData['serverSurveyHour'];
	if (is_null($_POST["serverSurveyHour"])) {
		unset($_POST["serverSurveyHour"]);
	}
	$_POST["serverSurveyEndDate"] = @$jsonData['serverSurveyEndDate'];
	if (is_null($_POST["serverSurveyEndDate"])) {
		unset($_POST["serverSurveyEndDate"]);
	}
	$_POST["serverSurveyEndHour"] = @$jsonData['serverSurveyEndHour'];
	if (is_null($_POST["serverSurveyEndHour"])) {
		unset($_POST["serverSurveyEndHour"]);
	}
	//@JAPR 2016-12-30: Agregadas las fechas de edición según el servidor (#44O0VU)
	//Fechas de última edición del outbox local
	$_POST["editStartDate"] = @$jsonData['editStartDate'];
	if (is_null($_POST["editStartDate"])) {
		unset($_POST["editStartDate"]);
	}
	$_POST["editStartHour"] = @$jsonData['editStartHour'];
	if (is_null($_POST["editStartHour"])) {
		unset($_POST["editStartHour"]);
	}
	$_POST["editEndDate"] = @$jsonData['editEndDate'];
	if (is_null($_POST["editEndDate"])) {
		unset($_POST["editEndDate"]);
	}
	$_POST["editEndHour"] = @$jsonData['editEndHour'];
	if (is_null($_POST["editEndHour"])) {
		unset($_POST["editEndHour"]);
	}
	//Fechas de última edición del outbox local según el server
	$_POST["editServerStartDate"] = @$jsonData['editServerStartDate'];
	if (is_null($_POST["editServerStartDate"])) {
		unset($_POST["editServerStartDate"]);
	}
	$_POST["editServerStartHour"] = @$jsonData['editServerStartHour'];
	if (is_null($_POST["editServerStartHour"])) {
		unset($_POST["editServerStartHour"]);
	}
	$_POST["editServerEndDate"] = @$jsonData['editServerEndDate'];
	if (is_null($_POST["editServerEndDate"])) {
		unset($_POST["editServerEndDate"]);
	}
	$_POST["editServerEndHour"] = @$jsonData['editServerEndHour'];
	if (is_null($_POST["editServerEndHour"])) {
		unset($_POST["editServerEndHour"]);
	}
	//@JAPR
	
	$_POST["surveyID"] = @$jsonData['surveyID'];
	if (is_null($_POST["surveyID"])) {
		unset($_POST["surveyID"]);
	}
	$_POST["agendaID"] = @$jsonData['agendaID'];
	if (is_null($_POST["agendaID"])) {
		unset($_POST["agendaID"]);
	}
	//$_POST["surveyLatitude"] = @$jsonData['surveyLatitude'];
	$_POST["surveyLatitude"] = @$jsonData['latitude'];
	if (is_null($_POST["surveyLatitude"])) {
		unset($_POST["surveyLatitude"]);
	}
	//$_POST["surveyLongitude"] = @$jsonData['surveyLongitude'];
	$_POST["surveyLongitude"] = @$jsonData['longitude'];
	if (is_null($_POST["surveyLongitude"])) {
		unset($_POST["surveyLongitude"]);
	}
	$_POST["surveyAccuracy"] = @$jsonData['accuracy'];
	if (is_null($_POST["surveyAccuracy"])) {
		unset($_POST["surveyAccuracy"]);
	}
	$_POST["element"] = @$jsonData['element'];
	if (is_null($_POST["element"])) {
		unset($_POST["element"]);
	}	
	//Valida para que en caso de no enviar este parámetro se asuma que se está sincronizando un Outbox
	if (!isset($_POST["surveyFinished"])) {
		$_POST["surveyFinished"] = 4;
	}
	if ((!isset($_POST["entryID"]) || is_null($_POST["entryID"])) && isset($_GET["entryID"])) {
		$_POST["entryID"] = @$_GET['entryID'];
	}
	if (isset($_POST["entryID"]) && is_null($_POST["entryID"])) {
		unset($_POST["entryID"]);
	}
	
	//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	try {
		$sqlCREATE = "CREATE TABLE SI_SV_SurveyLog ( 
				CreationDateID DATETIME NOT NULL, 
				Repository VARCHAR(50) NOT NULL, 
				SurveyID INTEGER NOT NULL, 
				AgendaID INTEGER NOT NULL,
				UserID VARCHAR(255) NOT NULL, 
				WebMode INTEGER NULL, 
				Latitude DOUBLE NULL, 
				Longitude DOUBLE NULL, 
				Accuracy DOUBLE NULL,
				SyncLatitude DOUBLE NULL, 
				SyncLongitude DOUBLE NULL, 
				SyncAccuracy DOUBLE NULL,
				SurveyDate DATETIME NULL, 
				FileName VARCHAR(255) NULL, 
				FormsPath VARCHAR(255) NULL, 
				FormsVersion VARCHAR(50) NULL, 
				UserAgent TEXT NULL
			)";
		@$aRepository->DataADOConnection->Execute($sqlCREATE);
		
		//@JAPR 2017-12-19: Agregados al script de actualización ALTER TABLE que se estaban ejecutando directo en código pero provocaban table locks, 
		//para esta fecha toda BD de eForms ya deben tener estos campos, pero se integran al script por si alguna BD mas vieja se migra de versión
		/*$sql = "ALTER TABLE SI_SV_SurveyLog ADD Accuracy DOUBLE NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD SyncAccuracy DOUBLE NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD screenSize VARCHAR(255) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD platform VARCHAR(255) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD Status INTEGER DEFAULT 0";
		@$aRepository->DataADOConnection->Execute($sql);
		//@JAPR 2015-07-07: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD SessionID VARCHAR(60) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD DeviceName VARCHAR(60) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD UDID VARCHAR(100) NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD DeviceID INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
		$sql = "ALTER TABLE SI_SV_SurveyLog ADD Deleted INTEGER NULL";
		@$aRepository->DataADOConnection->Execute($sql);
		//@JAPR
		*/
		
		$sRepository = @(isset($_SESSION["PABITAM_RepositoryName"]))?$_SESSION["PABITAM_RepositoryName"]:'';
		$sRepository = $aRepository->DataADOConnection->Quote($sRepository);
		$currDate = $aRepository->DataADOConnection->DBTimeStamp(date('Y-m-d H:i:s'));
		$surveyID = (int) @$_POST["surveyID"];
		$agendaID = (int) @$_POST["agendaID"];
		$kpiUser = $aRepository->DataADOConnection->Quote(getParamValue('UserID', 'both', "(string)"));
		$dblSyncLat = getParamValue('syncLat', 'both', "(float)", true);
		if (is_null($dblSyncLat) || !is_numeric($dblSyncLat)) {
			$dblSyncLat = 0;
		}
		$dblSyncLong = getParamValue('syncLong', 'both', "(float)", true);
		if (is_null($dblSyncLong) || !is_numeric($dblSyncLong)) {
			$dblSyncLong = 0;
		}
		$dblSyncAcc = getParamValue('syncAcc', 'both', "(float)", true);
		if (is_null($dblSyncAcc) || !is_numeric($dblSyncAcc)) {
			$dblSyncAcc = 0;
		}
		//@JAPR 2015-10-29: Corregido un bug, estos datos no se estaban grabando en la tabla de la forma ya que nunca se agregaban al $_POST, venían como $_GET únicamente (#NEHQCF)
		$_POST["syncLat"] = $dblSyncLat;
		$_POST["syncLong"] = $dblSyncLong;
		$_POST["syncAcc"] = $dblSyncAcc;
		//@JAPR
		
		$dblLat = getParamValue('surveyLatitude', 'both', "(float)", true);
		if (is_null($dblLat) || !is_numeric($dblLat)) {
			$dblLat = 0;
		}
		$dblLong = getParamValue('surveyLongitude', 'both', "(float)", true);
		if (is_null($dblLong) || !is_numeric($dblLong)) {
			$dblLong = 0;
		}
		$dblAccuracy = getParamValue('surveyAccuracy', 'both', "(float)", true);
		if (is_null($dblAccuracy) || !is_numeric($dblAccuracy)) {
			$dblAccuracy = 0;
		}
		$surveyDate = getParamValue('surveyDate', 'both', "(string)");
		$surveyHour = getParamValue('surveyHour', 'both', "(string)");
		$surveyDate = $aRepository->DataADOConnection->DBTimeStamp($surveyDate.' '.$surveyHour);
		$strFileName = getParamValue('filename', 'both', "(string)", true);
		$strFileName = $aRepository->DataADOConnection->Quote($strFileName);
		$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
		$strFormsPath = $aRepository->DataADOConnection->Quote($streFormsService);
		$strAppVersion = $aRepository->DataADOConnection->Quote($appVersion);
		$nav = @(isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
		$nav = $aRepository->DataADOConnection->Quote($nav);
		
		//Conchita agregados los parametros de tamaño de pantalla y tipo de dispositivo
		$platform ='';
		$screenSize = '';
		if (isset($_POST['platform'])) {
			$platform =(string) $_POST['platform'];
		}
		if (isset($_POST['screenSize'])) {		
			$screenSize = $_POST['screenSize'];
		}
		$screenSize = $aRepository->DataADOConnection->Quote($screenSize);
		$platform = $aRepository->DataADOConnection->Quote($platform);
		//@JAPR 2015-07-07: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
		$strSessionID = getParamValue('sessionID', 'both', "(string)");
		$strDeviceName = getParamValue('deviceName', 'both', "(string)");
		$strUDID = getParamValue('uuid', 'both', "(string)");
		//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
		//Si no viene el parámetro deviceID, querría decir que no pasó por el Agente así que debe venir el parámetro platform y por ende se procesa desde ese (si tampcoo viniera dicho
		//parámetro entonces sería un error, pues se trataría de una versión muy vieja del App). Este dato se obtiene en la funciòn identifyDeviceType
		//$intDeviceID = getParamValue('deviceID', 'both', '(int)', true);
		$intDeviceID = identifyDeviceType();
		//@JAPR
		$strAdditionalFields = "";
		$strAdditionalValues = "";
		$strAdditionalFields .= ', SessionID';
		$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote($strSessionID);
		$strAdditionalFields .= ', DeviceName';
		$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote($strDeviceName);
		$strAdditionalFields .= ', UDID';
		$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote($strUDID);
		//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
		$strAdditionalFields .= ', DeviceID';
		$strAdditionalValues .= ', '.$aRepository->DataADOConnection->Quote($intDeviceID);
		//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
		$strAdditionalFields .= ', Deleted';
		$strAdditionalValues .= ', 0';
		//@JAPR
		$sql = "INSERT INTO SI_SV_SurveyLog (CreationDateID, Repository, SurveyID, AgendaID, UserID, WebMode, Latitude, Longitude, Accuracy, 
					SyncLatitude, SyncLongitude, SyncAccuracy, SurveyDate, FileName, FormsPath, FormsVersion, UserAgent, screenSize, platform {$strAdditionalFields}) 
				VALUES ({$currDate}, {$sRepository}, {$surveyID}, {$agendaID}, {$kpiUser}, ".((int) @$blnWebMode).", {$dblLat}, {$dblLong}, {$dblAccuracy}, 
					{$dblSyncLat}, {$dblSyncLong}, {$dblSyncAcc}, {$surveyDate}, {$strFileName}, {$strFormsPath}, {$strAppVersion}, {$nav}, {$screenSize}, {$platform} {$strAdditionalValues})";
		//@JAPR
		@$aRepository->DataADOConnection->Execute($sql);
	} catch (Exception $e) {
		//No reporta este error, es sólo para no afectar a las capturas
	}
	//@JAPR
	
	$entryID = 0;
	$intFactKeyDimVal = 0;
	if ($blnEdit) {
		$entryID = getParamValue('entryID', 'both', "(int)");
		$intFactKeyDimVal = $entryID;
		
		//@JAPR 2013-05-20: Corregido un bug, puede venir el parámetro de edición en capturas vía Web, pero si no hay entryID entonces realmente
		//no es una edición
		if ($entryID > 0) {
			//@JAPR 2012-11-23: Si se tratara de una edición, debe utilizar la fecha y hora original
			if (isset($jsonData['originalDate']) && trim($jsonData['originalDate']) != '') {
				$_POST["surveyDate"] = $jsonData['originalDate'];
			}
			if (isset($jsonData['originalHour']) && trim($jsonData['originalHour']) != '') {
				$_POST["surveyHour"] = $jsonData['originalHour'];
			}
			if (isset($jsonData['originalEndDate']) && trim($jsonData['originalEndDate']) != '') {
				$_POST["surveyEndDate"] = $jsonData['originalEndDate'];
			}
			if (isset($jsonData['originalEndHour']) && trim($jsonData['originalEndHour']) != '') {
				$_POST["surveyEndHour"] = $jsonData['originalEndHour'];
			}
			if (isset($jsonData['originalLatitude']) && trim($jsonData['originalLatitude']) != '') {
				$_POST["surveyLatitude"] = $jsonData['originalLatitude'];
			}
			if (isset($jsonData['originalLongitude']) && trim($jsonData['originalLongitude']) != '') {
				$_POST["surveyLongitude"] = $jsonData['originalLongitude'];
			}
			//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
			if (getMDVersion() >= esvAccuracyValues) {
				if (isset($jsonData['originalAccuracy']) && trim($jsonData['originalAccuracy']) != '') {
					$_POST["surveyAccuracy"] = $jsonData['originalAccuracy'];
				}
			}
			//@JAPR
		}
		//@JAPR
		
		//Asigna los datos del usuario, pero no del que hace el request sino del usuario original, para esto deberá sobreescribir las variables
		//de sesión ya que esas se usarán a lo largo del proceso de grabado
		$intCreatorID = 0;
		if (isset($jsonData['creatorID']) && (int) trim($jsonData['creatorID']) > 0) {
			$intCreatorID = (int) trim($jsonData['creatorID']);
		}
		
		if ($intCreatorID != $_SESSION["PABITAM_UserID"]) {
			//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		    $theUser = BITAMeFormsUser::NewInstanceWithID($aRepository, $intCreatorID);
		    if (!is_null($theUser)) {
		    	$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $theUser->UserID);
		    }
			
		    if (!is_null($theUser) && !is_null($theAppUser)) {
			    //$_SESSION["PABITAM_RepositoryName"] = $aRepository->RepositoryName;
			    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
			    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
			    $_SESSION["PABITAM_Password"] = $theUser->Password;
			    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
			    $_SESSION["PAtheUserID"] = $theUser->UserID;
			    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
			    //$_SESSION["PAgenerateLog"] = $generateLog;
			    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
			    $_SESSION["SV_USER"] = $theAppUser->SV_User;
			    //$_SESSION["PAuserLanguageID"] = $userLanguageID;
			    //$_SESSION["PAuserLanguage"] = $userLanguage;
			    //$_SESSION["PAchangeLanguage"] = false;
			    //$_SESSION["PAArtusWebVersion"] = 6.0;
			    //$_SESSION["PAFBM_Mode"] = $valueFBMMode;
		    }
		}
		
		$_POST["userID"] = $_SESSION["PABITAM_UserID"];
		$_POST["appUserID"] = $_SESSION["PAtheAppUserID"];
	}
	
	//@JAPR 2012-10-10: Agregada la sincronización de las fotos
	//Con este parámetro se forzará a que al grabar las fotos simplemente se haga un copiado desde la carpeta de sincronización a la carpeta final
	//ya que no se recibe en el $_POST el B64 de las fotos
	$_POST["photo_paths"] = 1;
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	$usePhotoPaths = true;
	$_POST["document_paths"] = 1;
	$useDocumentPaths = true;
	//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
	$useRecoveryPaths = (bool) getParamValue('recover', 'both', "(int)");
	//@JAPR
	
	//Ahora llena los parámetros de las preguntas, pero para eso primero obtiene la colección de preguntas de esta encuesta para que dependiendo
	//del tipo sea de donde extraiga los datos (sólo si se recibió un surveyID)
	$surveyID = (int) @$_POST["surveyID"];
	if ($surveyID <= 0) {
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		if ($intDataDestinationID) {
			$return['error'] = true;
			$return['errmsg'] = 'No survey data was received for this outbox';
			return $return;
		}
		else {
			return 'errmsg=No survey data was received for this outbox';
		}
		//@JAPR
	}
	
	require_once('survey.inc.php');
	require_once('section.inc.php');
	require_once('question.inc.php');
	require_once('catalog.inc.php');
	require_once('catalogmember.inc.php');
	//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
    require_once("processsurveyfunctions.php");
	//@JAPR 2014-11-07: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
    require_once('surveyAgenda.inc.php');
    //@JAPR
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if (is_null($surveyInstance)) {
		//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
		if ($intDataDestinationID) {
			$return['error'] = true;
			$return['errmsg'] = 'The specified survey was not found ('.$surveyID.')';
			return $return;
		}
		else {
			return 'errmsg=The specified survey was not found ('.$surveyID.')';
		}
		//@JAPR
	}
	
	//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
	if ($surveyInstance->CreationAdminVersion >= esveFormsv6) {
		//Si la forma es de v6, no tiene modelos asociados así que desactiva cualquier código relacionado con agregaciones o que usarían los modelos
		$gbEnableIncrementalAggregations = false;
		$gbTestIncrementalAggregationsData = false;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\neForms v6 Form ({$surveyInstance->SurveyID}, Version: {$surveyInstance->CreationAdminVersion}), disabling model related processes");
		}
	}
	//@JAPR
	
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	//Si se utilizarán agregaciones incrementales, verifica que este repositorio las tenga activadas, así como que esta encuesta tenga agregaciones
	//definidas, de lo contrario desactiva la bandera por el resto de la sesión para no afectar al grabado
	if ($gbEnableIncrementalAggregations) {
		$sql = "SELECT COUNT(*) AS NumAggregations FROM SI_AG_NEW WHERE CLA_CONCEPTO = ".$surveyInstance->ModelID;
	    $aRS = $aRepository->ADOConnection->Execute($sql);
	    if ($aRS && !$aRS->EOF) {
	    	$gbEnableIncrementalAggregations = ((int) @$aRS->fields["numaggregations"] > 0);
	    }
	    else {
	    	$gbEnableIncrementalAggregations = false;
	    }
	}
	if ($gbTestIncrementalAggregationsData) {
		$gbEnableIncrementalAggregations = true;
	}
	
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	//Obtiene la instancia de la captura (sin datos) especificada que se desea editar
	$objReportData = null;
    if ($blnEdit) {
		if ($entryID > 0) {
			//El entryID recibido es el FactKeyDimVal, pero para instanciar a report se necesita el FactKey, así que obtenemos el último FactKey
			//del FactKeyDimVal especificado
			$sql = "SELECT MAX(t1.FactKey) AS FactKey FROM ".$surveyInstance->SurveyTable." t1 WHERE t1.FactKeyDimVal = ".$entryID;
		    $aRS = $aRepository->DataADOConnection->Execute($sql);
		    if ($aRS && !$aRS->EOF) {
		    	$entryID = (int) $aRS->fields["factkey"];
		    }
		    else {
		    	$entryID = 0;
		    }
		}
    	
		require_once("reportExt.inc.php");
		if ($entryID > 0) {
	    	//require_once('report.inc.php');
			//$objReportData = BITAMReport::NewInstanceWithID($aRepository, $entryID, $surveyID, false);
			$objReportData = BITAMReportExt::NewInstanceWithID($aRepository, $entryID, $surveyID);
		}
    }
	
	//@JAPR 2015-07-10 Agregado el soporte de Formas v6 con un grabado completamente diferente
	if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
		//@JAPR 2013-03-07: Validado que si no existen las columnas de las fecha de sincronización se creen automáticamente
		$sql = "SELECT SyncDate FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD SyncDate VARCHAR(64) NULL";
			$aRepository->DataADOConnection->Execute($sql);
		}
		
		$sql = "SELECT SyncTime FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD SyncTime VARCHAR(64) NULL";
			$aRepository->DataADOConnection->Execute($sql);
		}
	}
	
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//Verifica si existen los campos para almacenar el originen del registro de la tabla de datos
	$useSourceFields = false;
	//@JAPR 2015-07-10 Agregado el soporte de Formas v6 con un grabado completamente diferente
	if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
		$sql = "SELECT EntrySectionID, EntryCatDimID FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$useSourceFields = true;
		}
	}
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	$useRecVersionField = false;
	//@JAPR 2015-07-10 Agregado el soporte de Formas v6 con un grabado completamente diferente
	if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
		$sql = "SELECT eFormsVersionNum FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$useRecVersionField = true;
		}
		else {
			//Este campo si se debe agregar automáticamente para permitir mantener el histórico de eForms e implementar cambios futuros
			$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD eFormsVersionNum VARCHAR(10) NULL";
			if ($aRepository->DataADOConnection->Execute($sql) !== false) {
				$useRecVersionField = true;
			}
		}
	}
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	$useDynamicPageField = false;
	//@JAPR 2015-07-10 Agregado el soporte de Formas v6 con un grabado completamente diferente
	if ($surveyInstance->CreationAdminVersion < esveFormsv6) {
		$sql = "SELECT DynamicPageDSC FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS) {
			$useDynamicPageField = true;
		}
		else {
			//Este campo si se debe agregar automáticamente para permitir mantener el histórico de eForms e implementar cambios futuros
			$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD DynamicPageDSC VARCHAR(255) NULL";
			if ($aRepository->DataADOConnection->Execute($sql) !== false) {
				$useDynamicPageField = true;
			}
		}
	}
	//@JAPR
	
	//Genera un array indexado por ID de sección para facilitar identificarlas mientras se procesan las respuestas
	$sectionCollection = BITAMSectionCollection::NewInstance($aRepository, $surveyID);
	$arrSectionsByID = array();
	$arrSectionValuesByID = array();			//Contiene los keys de cada valor del catálogo usado como opción múltiple indexados ascendentemente desde 0
												//Array local usado para generar los datos del POST, por simplicidad se dejará con los keys subrrogados recibidos aunque hubieran sido remapeados, ya que solo se usa para determinar la posición dentro del POST
	$arrSectionPagesByID = array();				//Contiene los keys de cada valor del catálogo que generó una página dinámica indexados ascendentemente desde 0
	//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
	$arrSectionDescByID = array();				//Contiene los valores totales del catálogo como se grabaron en el App de cada valor que generó una página dinámica/opción múltiple indexados ascendentemente desde 0 (es equivalente a $arrSectionValuesByID)
	$arrSectionDescStrByID = array();			//Equivalente a $arrSectionDescByID solo que en lugar de que sea un array, ya es el string en formato de simple choice de catálogo usando usando separadores especiales y la referencia al cla_descrio de cada atributo
	$arrSectionValuesByMultiChoiceAnswerFromApp = array();	//Contiene los números de página de cada opción múltiple indexados ascendentemente desde 0 (equivalente a $arrSectionValuesByMultiChoiceAnswer pero tal como son recibidos por el App)
	//$arrSectionOptionByMultiChoiceAnswerFromApp = array();	//Contiene las descripciones de cada opción múltiple indexados ascendentemente desde 0 (equivalente a $arrSectionValuesByMultiChoiceAnswer pero tal como son recibidos por el App)
	//@JAPR
	$arrSectionValuesByPage = array();			//Contiene los keys de cada valor del catálogo usado como opción múltiple indexados por el key de la página donde existe (es un array multidimensional, la dimensión externa es simplemente el número de página)
												//Array local usado para generar los datos del POST, por simplicidad se dejará con los keys subrrogados recibidos aunque hubieran sido remapeados, ya que solo se usa para determinar la posición dentro del POST 
												//Si se trata de una edición, contiene los keys dummy negativos que representan las posiciones simplemente, no los keys subrrogados reales del catálogo
	//@JAPR 2013-08-14: Corregido un bug, con la optimización para grabar a partir de las descripciones de las
	//preguntas de catálogo, se afectó al grabado de las dinámicas cuando se cambia la sección durante la
	//edición, ya que en esos casos no vendrán valores dummies negativos sino que se regresan los keys subrrogados
	//reales que el App tenía para las opciones del catálogo utilizadas en la sección dinámica, por tanto no
	//se puede usar el array de keys por página dummy sino que se debe elegir usar el array real basado
	//también en los datos recibidos del App
	$arrSectionValuesByPageFromApp = array();	//Contiene los keys de cada valor del catálogo usado como opción múltiple indexados por el key de la página donde existe (es un array multidimensional, la dimensión externa es simplemente el número de página)
												//Array local usado para generar los datos del POST, por simplicidad se dejará con los keys subrrogados recibidos aunque hubieran sido remapeados, ya que solo se usa para determinar la posición dentro del POST 
												//Si se trata de una edición, en este caso si contiene los keys recibidos tal cual del App, ya que si no fueran negativos querría decir que el App cambió como se genera la dinámica, así que hay que grabar a partir
												//de lo que el App envía en lugar de reutilizar lo que está en las tablas
	$blnStoreDataFromApp = array();				//Variable sólo usada durante la edición para determinar si se usará $arrSectionValuesByPageFromApp (en caso de ser true) o $arrSectionValuesByPage (en caso de ser false) según si el App mandó keys subrrogados
												//positivos o negativos, lo cual indica respectivamente que cambió como se genera la dinámica o bien que respetó lo que ya estaba grabado. Si no se hace esta diferencia, los datos grabados se pierden para algunos
												//tipos de preguntas como las Multiple choice o las simple choice normales en la dinaámica, ya que dependen de los keys subrrogados de cada página
	$arrSectionValuesByDescFromApp = array();	//Contiene los keys de cada valor del catálogo pero indexado por la descripción de los checkboxes dinámicos
												//Array global usado durante el proceso de grabado en la tabla de hechos para obtener las páginas por SectionValue. Si se usan descripciones para grabar, entonces es irrelevante porque no se usará este array ya que
												//para ello se tuvieron que haber remapeado los keys y existiría la posibilidad de que varios vinieran cono NA
												//Si se trata de una edición, en este caso si contiene los keys recibidos tal cual del App, ya que si no fueran negativos querría decir que el App cambió como se genera la dinámica, así que hay que grabar a partir
												//de lo que el App envía en lugar de reutilizar lo que está en las tablas
	$arrSectionPagesByDescFromApp = array();	//Contiene los keys de cada valor del catálogo pero indexado por la descripción de las páginas dinámicas
												//Array global usado durante el proceso de grabado en la tabla de hechos para obtener la descripción de las páginas por índice. Si se usan descripciones para grabar, entonces es irrelevante porque no se usará este array ya que
												//para ello se tuvieron que haber remapeado los keys y existiría la posibilidad de que varios vinieran cono NA
												//Si se trata de una edición, en este caso si contiene los keys recibidos tal cual del App, ya que si no fueran negativos querría decir que el App cambió como se genera la dinámica, así que hay que grabar a partir
												//de lo que el App envía en lugar de reutilizar lo que está en las tablas
	//@JAPR
	$arrPagesBySectionValues = array();			//Contiene los keys de cada página pero indexado por el key de cada valor del catálogo usado como opción múltiple
	$arrSectionValuesByDesc = array();			//Contiene los keys de cada valor del catálogo pero indexado por la descripción de los checkboxes dinámicos
												//Array global usado durante el proceso de grabado en la tabla de hechos para obtener las páginas por SectionValue. Si se usan descripciones para grabar, entonces es irrelevante porque no se usará este array ya que
												//para ello se tuvieron que haber remapeado los keys y existiría la posibilidad de que varios vinieran cono NA
												//Si se trata de una edición, contiene los keys dummy negativos que representan las posiciones simplemente, no los keys subrrogados reales del catálogo
	$arrSectionPagesByDesc = array();			//Contiene los keys de cada valor del catálogo pero indexado por la descripción de las páginas dinámicas
												//Array global usado durante el proceso de grabado en la tabla de hechos para obtener la descripción de las páginas por índice. Si se usan descripciones para grabar, entonces es irrelevante porque no se usará este array ya que
												//para ello se tuvieron que haber remapeado los keys y existiría la posibilidad de que varios vinieran cono NA
												//Si se trata de una edición, contiene los keys dummy negativos que representan las posiciones simplemente, no los keys subrrogados reales del catálogo
	$arrHasMultiChoiceQuestions = array();
	$dynamicSectionID = 0;
	$dynamicSectionCatID = 0;
	foreach ($sectionCollection as $aSection) {
		$arrSectionsByID[$aSection->SectionID] = $aSection;
		$arrHasMultiChoiceQuestions[$aSection->SectionID] = false;
		//@JAPREliminar: Esto es irrelevante en este punto, ya que mas abajo se vuelven a asignar estas variables al recorrer las secciones
		if ($aSection->SectionType == sectDynamic) {
			$dynamicSectionID = $aSection->SectionID;
			$dynamicSectionCatID = $aSection->CatalogID;
		}
	}
	
	//Genera un array indexado por ID de pregunta para facilitar identificarlas mientras se procesan las respuestas
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	$arrQuestionsByID = array();
	foreach ($questionCollection as $aQuestion) {
		$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion;
		
		//Agrega la bandera de preguntas múltiples en secciones dinámicas
		if ($aQuestion->QTypeID == qtpMulti) {
			$objSection = @$arrSectionsByID[$aQuestion->SectionID];
			//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			//Esta validación de múltiple choice sólo aplica a secciones dinámicas, no a las Inline, pero se usa el array en el mismo código
			if (!is_null($objSection) && $objSection->SectionType == sectDynamic) {
				$arrHasMultiChoiceQuestions[$objSection->SectionID] = true;
			}
			//@JAPR
		}
	}
	
	//Si en este punto aun sigue activo el uso de agregaciones incrementales, quiere decir que todo está perfectamente configurado así que prepara
	//los arrays para almacenar los datos por cada registro grabado/editado en la secuencia en que se llenará
	if ($gbEnableIncrementalAggregations) {
		BITAMSurvey::PrepareIncrementalAggregationsArrays($aRepository, $surveyID, true);
	}
	
	//Contiene el factkey por cada registro de la sección dinámica, se va genera cuando se lee el sectionValue del POST y se tiene que hacer match,
	//contra los registros ya grabados que coincidan con estos valores, si no se logra hacer match quiere decir que se trata de un valor nuevo
	//que fué posible capturar así que se agrega a la captura existente
	$arrDynamicSectionFKs = array();
	
	//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	//Identifica si la sección dinámica (en caso de haberla) tiene o no preguntas múltiple-choice y asigna el valor del atributo a usar para
	//grabar la columna con el valor completo del catálogo para sus registros cuando la encuesta utiliza catálogos desasociados. Esta variable
	//se deja como global para reutilizar en cualquier de los métodos de grabado o Update usados mas adelante
	global $dynamicSectionMemberID;
	$dynamicSectionMemberID = 0;
	global $dynamicSectionWithMultiChoice;
	$dynamicSectionWithMultiChoice = false;
	//@JAPR
	//Obtiene los valores del catálogo para las secciones que sean dinámicas
	$arrSCDynQuestionsBySectionID = array();
	$arrSectionValueByKey = array();	//Array local usado para generar los datos del POST
	$dynamicSectionID = 0;
	$dynamicSectionCatID = 0;
	//@JAPR 2013-08-13: Corregido un bug, en algún punto se optimizó el proceso para grabar preguntas simple-choice de catálogo a partir de las
	//descripciones, pero al hacerlo el FactKey enviado a la edición se manenó como un número negativo empezando en -1 para representar a todos
	//los registros de la maestro-detalle que existían durante la edición, así que en el proceso de grabado de la edición, el valor negativo del
	//FactKey estaba siendo ignorado por el proceso por lo que en caso de no haber existido una pregunta de un tipo diferente en la sección, no
	//se hubiera registrado que FactKeys fueron actualizados provocando que al final fueran eliminados. Ahora se mapearán esos FactKeys negativos
	//con el valor real en la misma posición relativa dentro de la tabla paralela para hacer correctamente la actualización de FactKeys usados
	$arrMasterSectionFKsFromEntry = array();
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	$arrInlineSectionFKsFromEntry = array();
	$aSCDynQuestionInstance = null;
	//@JAPR
	foreach ($sectionCollection as $aSection) {
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		$inlineSectionID = 0;
		$inlineSectionCatID = 0;
		$inlineSectionMemberID = 0;
		//@JAPR 2013-08-13: Corregido un bug, en algún punto se optimizó el proceso para grabar preguntas simple-choice de catálogo a partir de las
		//descripciones, pero al hacerlo el FactKey enviado a la edición se manejó como un número negativo empezando en -1 para representar a todos
		//los registros de la maestro-detalle que existían durante la edición, así que en el proceso de grabado de la edición, el valor negativo del
		//FactKey estaba siendo ignorado por el proceso por lo que en caso de no haber existido una pregunta de un tipo diferente en la sección, no
		//se hubiera registrado que FactKeys fueron actualizados provocando que al final fueran eliminados. Ahora se mapearán esos FactKeys negativos
		//con el valor real en la misma posición relativa dentro de la tabla paralela para hacer correctamente la actualización de FactKeys usados
		//@JAPR 2014-05-30: Agregado el tipo de sección Inline
		if ($entryID > 0 && $surveyInstance->DissociateCatDimens && ($aSection->SectionType == sectMasterDet || $aSection->SectionType == sectInline) && $useSourceFields) {
			$intSectionID = $aSection->SectionID;
			$sql = "SELECT DISTINCT FactKey 
				FROM {$surveyInstance->SurveyTable} 
				WHERE FactKeyDimVal = {$intFactKeyDimVal} AND EntrySectionID = {$intSectionID} 
				ORDER BY FactKey";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false) {
				switch ($aSection->SectionType) {
					case sectMasterDet:
						$arrMasterSectionFKsFromEntry[$intSectionID] = array();
						break;
					case sectInline:
						$arrInlineSectionFKsFromEntry[$intSectionID] = array();
						break;
				}
				$intIndex = -1;
			    while (!$aRS->EOF) {
			    	$intFactKey = (int) @$aRS->fields["factkey"];
			    	if ($intFactKey > 0) {
			    		//Se remapea el índice negativo decremental con el FactKey de la misma posición incremental, ya que es el mismo orden de
			    		//grabado durante la edición
						switch ($aSection->SectionType) {
							case sectMasterDet:
					    		$arrMasterSectionFKsFromEntry[$intSectionID][$intIndex] = $intFactKey;
					    		break;
							case sectInline:
								$arrInlineSectionFKsFromEntry[$intSectionID][$intIndex] = $intFactKey;
								break;
						}
				    	$intIndex--;
			    	}
			    	$aRS->MoveNext();
			    }
			}
		}
		//@JAPR
		
		//JAPRWarningBarcel: Las secciones Inline se manejarán en ciertos aspectos como las Dinámicas, este es uno de ellos, hay que generar arrays
		//con los valores del catálogo que generaron registros para la sección
		//@JAPRWarningBarcel: Habilitar este código cuando se esté listo para empezar a verdaderamente grabar las secciones Inline, por lo pronto
		//se saltará todo esto y simplemente se van a generar las acciones de eBavel de los registros recibidos
		if ($aSection->SectionType != sectDynamic && $aSection->SectionType != sectInline) {
			continue;
		}
		
		$intSectionID = $aSection->SectionID;
		//@JAPR 2014-05-28: Agregado el tipo de sección Inline
		//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
		if ($aSection->SectionType == sectDynamic) {
			$dynamicSectionID = $aSection->SectionID;
			$dynamicSectionMemberID = $aSection->CatMemberID;
			$dynamicSectionCatID = $aSection->CatalogID;
		}
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		elseif ($aSection->SectionType == sectInline) {
			$inlineSectionID = $aSection->SectionID;
			$inlineSectionMemberID = $aSection->CatMemberID;
			$inlineSectionCatID = $aSection->CatalogID;
		}
		
		//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		//Movido este código porque el Array generado en este punto se utilizará en funciones que se deben usar al momento de leer la sección dinámica
		
		//@JAPR 2013-01-30: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
		//Si hay sección dinámica, la variable $dynamicSectionMemberID ya contiene el ID del atributo que usa para generar sus páginas, pero ahora se
		//comprueba si además hay alguna pregunta multiple-choice ya que de ser así entonces se tiene que utilizar el atributo siguiente pues
		//ese es el que genera las opciones de checkbox
		
		//Este array contiene todos los atributos que están en el catálogo de la sección dinámica hasta el atributo a usar ya sea para sus páginas
		//o para sus opciones múltiples, indexado por el MemberID
		global $arrDynamicCatalogAttributes;
		//@JAPREliminar: No se utiliza realmente
		global $arrDynamicCatalogIDs;
		//@JAPR 2014-05-28: Agregado el tipo de sección Inline
		global $arrInlineCatalogAttributes;
		//@JAPREliminar: No se utiliza realmente
		global $arrInlineCatalogIDs;
		//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
		if ($aSection->SectionType == sectDynamic) {
			$arrDynamicCatalogAttributes[-1] = array();
			$arrDynamicCatalogIDs = array();
		}
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		elseif ($aSection->SectionType == sectInline) {
			if (!is_array($arrInlineCatalogAttributes)) {
				$arrInlineCatalogAttributes = array();
			}
			$arrInlineCatalogAttributes[$inlineSectionID] = array();
			if (!is_array($arrInlineCatalogIDs)) {
				$arrInlineCatalogIDs = array();
			}
			$arrInlineCatalogIDs[$inlineSectionID] = 0;
		}
		
		//@JAPR 2014-05-28: Agregado el tipo de sección Inline
		//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
		if ($aSection->SectionType == sectDynamic && $dynamicSectionID > 0 && $dynamicSectionCatID > 0) {
			$sql = "SELECT A.QuestionID 
				FROM SI_SV_Question A, SI_SV_Section B  
				WHERE A.SectionID = B.SectionID AND A.SurveyID = {$surveyID} AND B.SectionID = {$dynamicSectionID} 
					AND A.QTypeID = ".qtpMulti;
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false)
			{
			    if (!$aRS->EOF) {
			    	//Si hay preguntas multiples en la dinámica, por tanto se obtiene el siguiente atributo del catálogo
			    	$dynamicSectionWithMultiChoice = true;
			    }
			}
			
			//Genera el array de atributos hasta los usados por la sección dinámica, dependiendo de si tiene o no preguntas multiple-choice
	    	$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $dynamicSectionCatID);
	    	if (!is_null($objCatalogMembersColl)) {
	    		$arrDynamicCatalogIDs[] = $dynamicSectionCatID;
	    		$blnOnlyNextAttr = false;
				foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
					$arrDynamicCatalogAttributes[-1][$objCatalogMember->MemberID] = array($objCatalogMember->ClaDescrip, $objCatalogMember->IndDimID);
					if ($blnOnlyNextAttr) {
						break;
					}
					
					if ($objCatalogMember->MemberID == $dynamicSectionMemberID) {
						//Sólo se debe procesar hasta el atributo siguiente al de la sección dinámica según el orden definido
						$blnOnlyNextAttr = true;
						//Si no hay preguntas múltiple choice entonces no es necesario usar un atributo mas
						if (!$dynamicSectionWithMultiChoice) {
							break;
						}
					}
				}
	    	}
		}
		//@JAPR 2014-05-29: Agregado el tipo de sección Inline
		//En el caso de las secciones Inline, este proceso sólo aplica si hay un catálogo asociado, ya que se pueden generar por opciones fijas
		elseif ($aSection->SectionType == sectInline) {
			//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			if ($inlineSectionID > 0 && $inlineSectionCatID > 0) {
				//Genera el array de atributos hasta los usados por la sección Inline
		    	$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $inlineSectionCatID);
		    	if (!is_null($objCatalogMembersColl)) {
		    		$arrInlineCatalogIDs[$inlineSectionID] = $inlineSectionCatID;
		    		$blnOnlyNextAttr = false;
					foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
						$arrInlineCatalogAttributes[$inlineSectionID][$objCatalogMember->MemberID] = array($objCatalogMember->ClaDescrip, $objCatalogMember->IndDimID);
						
						if ($objCatalogMember->MemberID == $inlineSectionMemberID) {
							break;
						}
					}
		    	}
			}
			else {
				//En este caso tiene que ser una sección Inline con opciones fijas, así que se simulará un atributo único Name sólo para que el
				//resto de los procesos funcionen correctamente (se utilizará el ID 0 para todos los atributos dummy creados de esta forma)
				$objCatalogMember = BITAMCatalogMember::NewInstance($aRepository, 0);
				$arrInlineCatalogAttributes[$inlineSectionID][0] = array(0, 0);
			}
		}
		//@JAPR
		
		//Por cada sección dinámica identifica el SectionValue recibido y además carga un array con los valores de los dos atributos usados
		//en ella para poder hacer match entre el key recibido y el key de su atributo padre
		$arrSectionValue = @$jsonData['sectionValue'.$intSectionID];
		$arrSectionPage = @$jsonData['sectionPage'.$intSectionID];
		//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		$arrSectionDesc = @$jsonData['sectionDesc'.$intSectionID];
		$arrSectionDescStr = array();
		if (!is_null($arrSectionValue)) {
			//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//Movido este código porque el Array generado en este punto se utilizará en funciones que se deben usar al momento de leer la sección dinámica
			//Identifica si es necesario obtener el Array de padres usados para generar las secciones, ya que si no hay pregunta múltiple no es necesario
			$blnGenerateParentValuesArr = false;
			
			//@JAPR 2014-05-28: Agregado el tipo de sección Inline
			//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
			if ($aSection->SectionType == sectDynamic) {
				//Contiene la instancia de la pregunta de Single Choice que filtra para generar las subsecciones dinámicas
				$aSCDynQuestionInstance = null;
				$dynamicSection = $aSection;
				$dynamicSectionCatID = $dynamicSection->CatalogID;
				$dynamicSectionID = $aSection->SectionID;
				$blnDynamicSectionFound = false;
				
				//Por cada sección dinámica genera el SectionValue grabado y además carga un array con los valores de los dos atributos usados en ella
				//para poder hacer match entre el key recibido y el key de su atributo padre
				foreach ($questionCollection->Collection as $questionKey => $aQuestion)
				{
					//@JAPR 2012-06-08: Agregada una validación para preguntas que son Open (cualquier tipo) dentro de secciones dinámicas
					if (!$blnDynamicSectionFound)
					{
						if ($aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID == $dynamicSectionCatID)
						{
							//Como pudiera haber varias preguntas de catálogo sincronizadas, debemos tomar la última para saber exactamente por qué atributo 
							//se debe filtrar
							$aSCDynQuestionInstance = $aQuestion;
						}
					}
					
					//si las preguntas pertenecen a la seccion dinamica y que estan dentro de la seccion
					//entonces si son repetibles y se ignoran dichas preguntas
					if($aQuestion->SectionID == $dynamicSectionID)
					{
						$blnDynamicSectionFound = true;
						//En este caso, no importa realmente si hay o no multiples, como se tienen que ordenar los valores se hace el proceso del If
						//siguiente de todas formas, por lo que además se forzará al break pues ya se identificó la pregunta generadora de la dinámica
						//if ($aQuestion->QTypeID == qtpMulti)
						//{
					        //@JAPR 2012-06-29: Corregido un bug, no había por qué hacer este Break, eso provocaba que ya no se grabaran las preguntas posteriores a las dinámicas
							$blnGenerateParentValuesArr = true;
							//break;
							//@JAPR
						//}
						break;
						//continue;
					}
				}
			}
			//@JAPR
			
			//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//Se generará el array con los números de página por cada posición de Checkbox basado en los keys subrrogados recibidos, los cuales
			//vienen en orden y es seguro que donde empieza uno de los keys del array de páginas es donde empieza la página y los siguientes keys
			//deberán reutilizar el mismo número de página
			$intPageNum = -1;
			$arrSectionValuesByMultiChoiceAnswerFromApp[$intSectionID] = array();
			//$arrSectionOptionByMultiChoiceAnswerFromApp[$intSectionID] = array();
			//@JAPR 2013-08-14: Corregido un bug, con la optimización para grabar a partir de las descripciones de las
			//preguntas de catálogo, se afectó al grabado de las dinámicas cuando se cambia la sección durante la
			//edición, ya que en esos casos no vendrán valores dummies negativos sino que se regresan los keys subrrogados
			//reales que el App tenía para las opciones del catálogo utilizadas en la sección dinámica, por tanto no
			//se puede usar el array de keys por página dummy sino que se debe elegir usar el array real basado
			//también en los datos recibidos del App
			$arrSectionValuesByPageFromApp[$intSectionID] = array();
			$arrSectionValuesByDescFromApp[$intSectionID] = array();
			//Por default durante la edición se asumirá que no cambió el contenido de las páginas de la dinámica, si se encuentra algún key subrrogado
			//positivo entonces se cambiará este valor a true para hacer el ajuste correspondiente al llenar las variables del POST para el grabado
			$blnStoreDataFromApp[$intSectionID] = false;
			//@JAPR
			$arrSectionPagesByIDFlip = array_flip($arrSectionPage);
			
			//A partir de que encuentre el $intSectionValue == $intSectionValuePage, el número de página deberá incrementarse en uno
			$arrSectionValueCopy = $arrSectionValue;
			foreach ($arrSectionValueCopy as $intIndex => $intSectionValue) {
				if (isset($arrSectionPagesByIDFlip[$intSectionValue])) {
					//Como el SectionValue si es el inicio de una página, actualiza el número de esta para los siguientes keys del array SectionValue
					$intPageNum = $arrSectionPagesByIDFlip[$intSectionValue];
				}
				
				$arrSectionValuesByMultiChoiceAnswerFromApp[$intSectionID][] = $intPageNum;
				//@JAPR 2013-08-14: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
				//recibido por el App
				if (!isset($arrSectionValuesByPageFromApp[$intSectionID][$intPageNum])) {
					$arrSectionValuesByPageFromApp[$intSectionID][$intPageNum] = array();
				}
				$arrSectionValuesByPageFromApp[$intSectionID][$intPageNum][] = $intSectionValue;
				if ($intSectionValue > 0) {
					//Si el valor recibido en el SectionValue es > 0, quiere decir una de 2 cosas:
					//1- Es una captura nueva, así que realmente este array va a ser irrelevante pero se deja asignado de todas formas
					//2- Es una edición donde se cambió por completo el valor de catálogo que genera a la dinámica, así que se reconstruyó y por
					//	tanto no vienen los dummies negativos, en ese caso se tiene que usar el array construido a partir del App y no el contruido
					//	a partir de los datos actualmente grabados
					$blnStoreDataFromApp[$intSectionID] = true;
				}
				
				//Si existe una descripción de combinación para este registro dinámico, la procesa para actualizar el SectionValue
				$strCatDimValAnswers = @$arrSectionDesc[$intIndex];
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\nTesting dynamic value: $intSectionValue ==> ".((is_array($strCatDimValAnswers) && count($strCatDimValAnswers) > 0)?(implode('_SVSep_', $strCatDimValAnswers)):''));
				}
				
				//if (trim($strCatDimValAnswers) == '' || $intSectionValue <= 1) {
				if (is_null($strCatDimValAnswers) || !is_array($strCatDimValAnswers) || count($strCatDimValAnswers) == 0 || $intSectionValue < 0) {
					//En este caso no hay descripción o llegó dañada, así que no se puede hacer nada al respecto y se tendrá que procesar como antes
					//de que existieran catálogos desasociados
					//También entraría aquí si el key subrrogado fuera el NA o uno de edición, pues en esos casos o automáticamente se procesará
					//con las descripciones (si fuera el NA) o no se procesará en absoluto si viniera de una edición, en cualquier otro caso un
					//valor de esos tipos representa un error y no se puede hacer nada con él
					continue;
				}
				
				//@JAPR 2013-08-14: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
				//recibido por el App
				//En este caso se llena el array de section values a partir de las descripciones recibidas en el App, por si se reconstruyó la
				//sección dinámica durante la edición. Se asumirá que los atributos no han cambiado por lo que debe ser seguro utilizar el último
				//índice del array para obtener la descripción de los Checkbox de las multiple choice dentro de la dinámica (sólo aplica para ese
				//tipo de preguntas realmente)
				if (!isset($arrSectionValuesByDescFromApp[$intSectionID][$intPageNum])) {
					$arrSectionValuesByDescFromApp[$intSectionID][$intPageNum] = array();
				}
				$strValue = (string) @$strCatDimValAnswers[count($strCatDimValAnswers) -1];
				$arrSectionValuesByDescFromApp[$intSectionID][$intPageNum][$strValue] = $intSectionValue;
				
				/* Ahora hay que reprocesar los keys subrrogados recibidos de la sección dinámica para:
					1- Verificar si el key sigue apuntando a las mismas descripciones, si es así entonces puede reutilizarlo
					2- Remapear el Key a uno nuevo que sea al que apunta la combinación
					3- En caso de ya no existir la combinación, cambiar el Key al valor de NA (1)
				*/
				
				//Convierte el array recibido en un String en el formato de las simple choice de catálogo
				//Sí se regresó un array con descripciones, pero ahora verifica si estas coinciden con las recibidas por el App
				//Como el valor $strCatDimValAnswers viene del App y solo son descripciones en el orden de los atributos cuando fueron grabados
				//pero no tenemos forma de saber que orden era ese, se asumirá que no han agregado mas atributos intermedios o por lo menos no
				//que afecten a la sección dinámica y se reutilizará el mismo array previamente calculado de los atributos que actualmente
				//deberían estar siendo usados en la dinámica para hacer match con este array, se sobreescribirá la variable con un string
				//en el formato en el que se generan las preguntas simple choice de catálogo
				//$arrayCatPairValues = explode('_SVElem_', $strCatDimValAnswers);
				$arrayCatClaDescrip = array();
				$arrayCatValues = array();
				$strNewCatDimValAnswers = '';
				$strAnd = '';
				$intMemberOrder = 0;
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
				if ($aSection->SectionType == sectDynamic) {
					foreach ($arrDynamicCatalogAttributes[-1] as $aMemberData) {
						if (!isset($strCatDimValAnswers[$intMemberOrder])) {
							//Si ya no viene asignado un valor para un atributo en esta posición, significaría que actualmente la sección dinámica
							//tiene mas atributos que los que traía esta captura lo cual es un error, sin embargo intentará continuar con lo que
							//pudo identificar aunque los resultados estarán equivocados
							break;
						}
						
						$intClaDescrip = $aMemberData[0];
						$strValue = (string) @$strCatDimValAnswers[$intMemberOrder];
						$strNewCatDimValAnswers .= $strAnd.'key_'.$intClaDescrip.'_SVSep_'.$strValue;
						
						$arrayCatClaDescrip[] = $intClaDescrip;
						$arrayCatValues[] = $strValue;
						$strAnd = '_SVElem_';
						$intMemberOrder++;
					}
				}
				elseif ($aSection->SectionType == sectInline) {
					foreach ($arrInlineCatalogAttributes[$inlineSectionID] as $aMemberData) {
						if (!isset($strCatDimValAnswers[$intMemberOrder])) {
							//Si ya no viene asignado un valor para un atributo en esta posición, significaría que actualmente la sección dinámica
							//tiene mas atributos que los que traía esta captura lo cual es un error, sin embargo intentará continuar con lo que
							//pudo identificar aunque los resultados estarán equivocados
							break;
						}
						
						$intClaDescrip = $aMemberData[0];
						$strValue = (string) @$strCatDimValAnswers[$intMemberOrder];
						$strNewCatDimValAnswers .= $strAnd.'key_'.$intClaDescrip.'_SVSep_'.$strValue;
						
						$arrayCatClaDescrip[] = $intClaDescrip;
						$arrayCatValues[] = $strValue;
						$strAnd = '_SVElem_';
						$intMemberOrder++;
					}
				}
				//@JAPR
				$arrSectionDescStr[$intIndex] = $strNewCatDimValAnswers;
				
				//1- Verificar si el key actual aun existe y si apunta al mismo lugar que las descripciones recibidas
				$arrayMapStrValues = array();
				$arrayMapStrValues[0] = $intSectionValue;
				//No se envía el MemberID porque se desea usar el total de atributos de la sección dinámica, además se solicita que regrese vacio
				//en caso de no encontrar nada para poder distinguir fácilmente, y que se utilice CLA_DESCRIP en lugar de IndDimID para armar el
				//string del filtro de la combinación y poder comparar con lo recibido por el App
				//@JAPR 2014-05-30: Agregado el tipo de sección Inline
				//Agregado el parámetro $aSectionID, si se especifica se asumirá que NO se trata de una sección dinámica sino una Inline
				$intCatalogID = 0;
				switch ($aSection->SectionType) {
					case sectDynamic:
						$intCatalogID = $dynamicSectionCatID;
						break;
					case sectInline:
						$intCatalogID = $inlineSectionCatID;
						break;
				}
				
				//@JAPR 2015-08-25: Validado que el proceso de eForms v6 ya no requiera extraer la combinación del catálogo
				if (getMDVersion() >= esveFormsv6) {
					//Se tiene que asignar esta variable que originalmente se asignaba mas abajo, ya que al forzar al valor de 1 en esta función usará esta variable
					$arrSectionDescStrByID[$intSectionID] = $arrSectionDescStr;
					
					//Se forzará el valor 1 para que entre  por el código que reutiliza la colección de valores recibida del App
					$arrayMapStrValues[0] = 1;
					$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $intCatalogID, -1, $intIndex, '', 1, 1, $inlineSectionID);
				}
				else {
					$arrDynamicRecValue = getCatValueDescForSurveyField($aRepository, $arrayMapStrValues, $intCatalogID, -1, -1, '', 1, 1, $inlineSectionID);
				}
				
				//@JAPR 2013-05-09: Corregido un bug, la validación no contemplaba el caso cuando el key ya no existe en el catálogo, en esos
				//casos se regresa un array completamente vacio
				if (!is_array($arrDynamicRecValue) || count($arrDynamicRecValue) == 0 || trim((string) @$arrDynamicRecValue[0]) == '') {
					$arrDynamicRecValue = null;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo ("<br>\r\nUnable to retrieve this combination by its SectionValue: $intSectionValue<br>");
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo ("<br>\r\narrDynamicRecValue for $intSectionValue:");
						PrintMultiArray($arrDynamicRecValue);
					}
					
					//Se procesa el valor obtenido dividiendolo por sus atributos
					$arrayCatPairValuesApp = explode('_SVElem_', $arrDynamicRecValue[0]);
					//$arrayCatClaDescripApp = array();
					$arrayCatValuesApp = array();
					$arrayTemp = array();
					$arrayInfo = array();
					foreach($arrayCatPairValuesApp as $element)
					{
						$arrayInfo = explode('_SVSep_', $element);
						
						//Obtener el cla_descrip y almacenar (key_1178)
						$arrayTemp = explode('_', $arrayInfo[0]);
						//$arrayCatClaDescripApp[] = $arrayTemp[1];
						
						//Obtener el valor del catalogo
						$arrayCatValuesApp[$arrayTemp[1]] = $arrayInfo[1];
					}
					
					//Verifica si la combinación recibida aun es la misma que existe en el server para este key
					$intMemberOrder = 0;
					foreach ($arrayCatClaDescrip as $aClaDescrip) {
						if (!isset($arrayCatValuesApp[$aClaDescrip]) || (string) @$arrayCatValuesApp[$aClaDescrip] != (string) @$arrayCatValues[$intMemberOrder]) {
							//En este caso no hay coincidencia entre el valor de este atributo, se asume que la combinación ha cambiado y por tanto
							//habrá que buscar un nuevo key subrrogado, para esto limpia el array y termina el ciclo de comparación
							$arrDynamicRecValue = null;
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo ("<br>\r\nNot the same combination for the SectionValue: $intSectionValue");
							}
							break;
						}
						$intMemberOrder++;
					}
				}
				
				//Si no se pudo encontrar la combinación mediante el key o si esta ya no corresponde con la descripción, entonces intenta encontrar
				//el nuevo key de dicha combinación o regresa el NA si no se logra encontrar
				if (is_null($arrDynamicRecValue)) {
					$arrayStrValues = array();
					$arrayStrValues[0] = $strNewCatDimValAnswers;
					//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
					$arrayPassValues = null;
					//Este método sólo aplica si es una sección dinámica, pudiese aplicar si fuera una sección Inline con catálogo, pero no
					//necesariamente porque esas secciones se pueden generar sin necesidad de una pregunta previa del mismo catálogo que las genere,
					//así que simplemente sólo es para dinámicas y por ende se valida que si es Inline sin catálogo (que es la única forma en que pudo
					//haber llegado aquí al fallar getCatValueDescForSurveyField por no haber catálogo) continue y deje el valor de SectionValue que
					//traía, ya que ese representaría la posición de la opción de respuesta que generó el registro mas que un elemento del catálogo
					if (!is_null($aSCDynQuestionInstance)) {
						//@JAPR 2015-05-08: Corregido un bug, al tener una sección dinámica previa a una sección Inline sin catálogo, la función getCatValueKeys
						//usada en esta función generaba un die porque no estaba asignada la variable $gblShowErrorSurvey, siendo que simplemente esperaba un
						//error y continuar regresando un false tal como pasaría en el interior del proceso de grabado (#NFWDPN)
						//Se asignará esta variable sólo previo a utilizar la función de error, ya que no se sabe que otro efecto podría provocar (se respaldará)
						$gblShowErrorSurvey = false;
						$arrayPassValues = getCatValueKeys($aRepository, $aSCDynQuestionInstance, $arrayStrValues);
						$gblShowErrorSurvey = $blShowErrorSurvey;
						//@JAPR
					}
					//@JAPR
					if ($arrayPassValues === false || !is_array($arrayPassValues) || count($arrayPassValues) == 0) {
						//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
						if ($aSection->SectionType == sectInline && $aSection->CatalogID <= 0) {
							//En este caso se respeta el SectionValue que trae pues representa el número de opción de respuesta fija del registro
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo ("<br>\r\nProcessing a fixed Inline record: $intSectionValue");
							}
						}
						else {
							//En este caso se había intentado identificar el key subrrogado del valor, como no se encontró se usa el key de NA
							//lo cual en las v4+ forzará a grabar a partir de las descripciones recibidas y no del key subrrogado del catálogo
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo ("<br>\r\nUnable to find a new match for this combination: $intSectionValue");
							}
							$intSectionValue = 1;
						}
						//@JAPR
					}
					else {
						$intSectionValue = (int) @$arrayPassValues[0];
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo ("<br>\r\nNew SectionValue found for this combination: $intSectionValue");
						}
						if ($intSectionValue < 1) {
							$intSectionValue = 1;
						}
					}
					
					//Sobreescribe el SectionValue en el array que se utilizará en el resto del proceso para que ya utilice los keys ajustados
					$arrSectionValue[$intIndex] = $intSectionValue;
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						echo ("<br>\r\nThe combination is the same for: $intSectionValue");
					}
				}
			}
			
			/* Termina el remapeo de los SectionValues recibidos
			******************************************************************************************************************
			******************************************************************************************************************
			******************************************************************************************************************
			*/
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\narrSectionValue {$intSectionID}");
				PrintMultiArray($arrSectionValue);
				echo("<br>\r\narrSectionValueCopy {$intSectionID}");
				PrintMultiArray($arrSectionValueCopy);
			}
			//@JAPR
			
			$arrSectionValueByKey = array_flip($arrSectionValue);
			//@JAPR 2014-05-28: Agregado el tipo de sección Inline
			//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
			if ($aSection->SectionType == sectDynamic) {
				if (count($arrSectionValue) == 0) {
					$arrDynamicSectionFKs[$intSectionID] = array();
				}
				else {
					$arrDynamicSectionFKs[$intSectionID] = array_fill(0, count($arrSectionValue), null);
				}
			}
			else {
				if (count($arrSectionValue) == 0) {
					$arrInlineSectionFKs[$intSectionID] = array();
				}
				else {
					$arrInlineSectionFKs[$intSectionID] = array_fill(0, count($arrSectionValue), null);
				}
			}
			//@JAPR
			
			//Primero se asigna el array de SectionValue a esta sección tal como viene del server, ya que eso representa los elementos que pudieron
			//ser capturados en esta encuesta específica y todos los elementos del $_POST para el grabado se basarán en este mismo orden
			//$arrSectionValuesByID[$intSectionID] = $arrSectionValue;
			$arrSectionValuesByID[$intSectionID] = $arrSectionValueCopy;
			//El array de SectionPage contiene el key que generó cada página dinámica, aunque no hay referencia a cuales de las opciones múltiples
			//le pertenecen, eso se determina aquí en el server
			$arrSectionPagesByID[$intSectionID] = $arrSectionPage;
			//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
			//El array SectionDesc contiene el valor completo en el formato de grabado de las simple choice de catálogo para cada opción múltiple o
			//página según el caso, en el mismo orden que $arrSectionValuesByID
			$arrSectionDescByID[$intSectionID] = $arrSectionDesc;
			$arrSectionDescStrByID[$intSectionID] = $arrSectionDescStr;
			//@JAPR 2014-05-29: Agregado el tipo de sección Inline
			//Como ahora se agregarán a este Array todas las secciones Inline, se agregará un valor dummy para la sección dinámica con el key -1
			if ($aSection->SectionType == sectDynamic) {
				//Sólo se necesita realmente para las variables globales
				//$arrSectionValuesByID[-1] = $arrSectionValueCopy;
				//$arrSectionPagesByID[-1] = $arrSectionPage;
				$arrSectionDescByID[-1] = $arrSectionDesc;
				$arrSectionDescStrByID[-1] = $arrSectionDescStr;
			}
			//@JAPR
			
			$arrPagesBySectionValues[$intSectionID] = array(); // array_fill(0, count($arrSectionValue), '');
			$arrSectionValuesByPage[$intSectionID] = array();
			$arrSectionValuesByDesc[$intSectionID] = array();
			$arrSectionPagesByDesc[$intSectionID] = array();
			//@JAPR 2013-08-15: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
			//recibido por el App
			$arrSectionPagesByDescFromApp[$intSectionID] = array();
			//@JAPR
			if (count($arrSectionPage) == 0) {
				continue;
			}
			
			if ($entryID > 0 && $blnGenerateParentValuesArr && !is_null($aSCDynQuestionInstance))
			{
				//@JAPR 2013-02-12: Agregada la edición de datos históricos
				//Durante la edición, si no se modifican los keys subrrogados enviados y por tanto estos quedan como negativos, quiere decir que
				//se está dejando la sección dinámica tal como estaba en el server, por tanto simplemente se hace match del factkey en el order
				//en que se carga pues se asume que no pudo haber cambiado, de lo contrario si ahora vienen keys positivos quiere decir que 
				//seleccionaron nuevamente una combinación del catálogo por lo que ya cambió por completo la combinación a grabar, incluso si
				//la combinación realmente fuera la misma que la original (ya que esto hubiera reconstruido la sección dinámica en el App)
				$blnDummySectionValues = true;
				foreach ($arrSectionValue as $intSectionValue) {
					if ($intSectionValue > 0) {
						$blnDummySectionValues = false;
						break;
					}
				}
				//@JAPR
				
				$arrSCDynQuestionsBySectionID[$intSectionID] = $aSCDynQuestionInstance;
				//Este método podría marcar error en capturas grabadas en las versiones originales si había capturas de Maestro-Detalle al mismo
				//tiempo que de dinámicas, ya que la pregunta en cuestión repetía el key para los registros de la maestro-detalle y no había forma
				//de identificar si era un registro de una sección o de otra, así que para mas o menos impedir esos casos se tomará el último valor
				//ya que las dinámicas se grababan al final
				//Con las capturas de v4 o posterior ya tenemos el campo correctamente armado
				$strEntrySectionFilter = '';
				if ($useSourceFields) {
					$strEntrySectionFilter = " AND (EntrySectionID = ".$dynamicSectionID." OR EntrySectionID = -1 OR EntrySectionID IS NULL)";
				}
				//Con la instancia de la pregunta que generó a la sección dinámica, obtenemos todos los valores grabados para ella en esta captura, ya
				//que esos serán los únicos valores posibles a usar (en este punto no importa si no vienen ordenados)
				$strSurveyField = strtolower($aSCDynQuestionInstance->SurveyField);
				$sql = "SELECT DISTINCT FactKey, ".$aSCDynQuestionInstance->SurveyField." 
					FROM ".$surveyInstance->SurveyTable." 
					WHERE FactKeyDimVal = ".$intFactKeyDimVal.$strEntrySectionFilter.
					" ORDER BY FactKey";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS !== false)
				{
					$intIndex = -1;
				    while (!$aRS->EOF) {
						//@JAPR 2013-02-11: Agregada la edición de datos históricos
				    	$intFactKey = (int) @$aRS->fields["factkey"];
				    	if ($blnDummySectionValues && $surveyInstance->DissociateCatDimens) {
				    		//En este caso realmente no interesa el valor, ya que todos los índices se regresarán como negativos pues el catálogo pudo
				    		//haber sido modificado así que es irrelevante el valor del key subrrogado del mismo que tuviera originalmente, se basará
				    		//sólo en las descripciones extraídas de la tabla de hechos/paralela
				    		$intKey = $intIndex--;
				    	}
				    	else {
					    	$intKey = (int) @$aRS->fields[$strSurveyField];
				    	}
				    	
				    	if ($intFactKey > 0 && isset($arrSectionValueByKey[$intKey])) {
				    		//Se trata de un valor que se está capturando de nuevo, por tanto le actualizamos el FactKey
							//@JAPR 2014-05-28: Agregado el tipo de sección Inline
							//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
							if ($aSection->SectionType == sectDynamic) {
					    		$arrDynamicSectionFKs[$intSectionID][$arrSectionValueByKey[$intKey]] = $intFactKey;
							}
							else {
					    		$arrInlineSectionFKs[$intSectionID][$arrSectionValueByKey[$intKey]] = $intFactKey;
							}
							//@JAPR
				    	}
				    	//@JAPR
				    	$aRS->MoveNext();
				    }
				}
			}
			
			//Ahora obtiene los valores reales del catálogo basado en los keys de las páginas generadas para reducir el tamaño del filtro
			//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			//Para las secciones Inline que tienen opciones fijas, salta todas estas validaciones pues sólo aplicarán a las de catálogo o dinámicas
			if ($aSection->CatalogID > 0) {
				$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aSection->CatalogID);
				if (is_null($objCatalog)) {
					continue;
				}
				$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $objCatalog->CatalogID);
				if (is_null($objCatalogMembersColl)) {
					continue;
				}
			
				//Identifica los atributos usados como generador de página y de opciones de Multiple Choice
				$arrAttributeIDs = array();
				foreach ($objCatalogMembersColl->Collection as $intIndex => $objCatalogMember) {
					//Cuando se llegó al atributo de la sección debe salir del ciclo pero primero agrega ese y el atributo siguiente
					if ($objCatalogMember->MemberID == $aSection->CatMemberID) {
						//@JAPR 2013-02-11: Agregada la edición de datos históricos
						$arrAttributeIDs[] = array('dimID' => $objCatalogMember->ClaDescrip, 'indDimID' => (int) @$objCatalogMember->IndDimID);
						//@JAPR 2014-05-28: Agregado el tipo de sección Inline
						//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
						if ($aSection->SectionType == sectDynamic) {
							if (isset($objCatalogMembersColl->Collection[$intIndex +1])) {
								$arrAttributeIDs[] = array('dimID' => $objCatalogMembersColl->Collection[$intIndex +1]->ClaDescrip, 
									'indDimID' => (int) @$objCatalogMembersColl->Collection[$intIndex +1]->IndDimID);
							}
						}
						//@JAPR
						break;
					}
				}
				
				if (count($arrAttributeIDs) == 0) {
					continue;
				}
			}
			//@JAPR
			
			//JAPRWarningBarcel: Leer comentario, falta implementar bien
			//@JAPR 2014-05-30: Agregado el tipo de sección Inline
			//Para la versión 4 o posterior ya no se requería ir al catálogo a extraer los valores porque se graban a partir de las descripciones, 
			//pero para la versión 5 además se agregaron catálogos basados en eBavel, los cuales no se pueden obtener de la misma manera y por tanto
			//los filtros armados aquí deberán de condicionarse por el tipo de catálogo, sin embargo bajo la primer premisa, por el momeno simplemente
			//no se van a cargar los valores, porque en una v5 ya se debería poder generar siempre a partir de las descripciones
			$arrCatalogValues = array();
			//@JAPR 2014-06-24: Agregado el soporte para secciones Inline con opciones fijas para generar sus registros
			//Para las secciones Inline que tienen opciones fijas, salta todas estas validaciones pues sólo aplicarán a las de catálogo o dinámicas
			//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Se agregó la validación de la versión de la forma, ya que en V6 es completamente seguro asumir que el propio archivo .dat contendrá los valores
			//de las secciones, así que no hay necesidad de obtenerlos desde el catálogo sino que se reutilizará lo recibido como datos de captura
			if ($surveyInstance->CreationAdminVersion < esveFormsv6 && $aSection->CatalogID > 0) {
				if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
					//En este caso por ahora no se cargarán los valores del catálogo
				}
				else {
					//El filtro contendrá todos los keys que fueron usados como páginas en la captura
					if (count($arrSectionValue) == 0) {
						$strFilter = 'AND '.$objCatalog->TableName.'KEY = -1 ';
					}
					else {
						$strFilter = 'AND '.$objCatalog->TableName.'KEY IN ('.implode(',', $arrSectionValue).') ';
					}
					$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter);
				}
			}
			
			//@JAPR 2013-02-12: Agregada la edición de datos históricos
			//@JAPR 2013-02-20: Corregido un bug, si no es una edición entonces no hay reporte, así que se usa el método original basado en el
			//catálogo
			if ($surveyInstance->DissociateCatDimens && !is_null($objReportData)) {
				//En este caso se basará en los datos leídos del reporte para generar los arrays de sectionValue, ya que debe asignar keys negativos
				//para cada página que se hubiera grabado previamente
				//@JAPR 2013-04-11: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Cuando se usa la edición de una captura, al basarse en el reporte ya carga un array de respuestas con paginación así que se usará
				//el mismo método que ya se tenía previamente
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
				if ($aSection->SectionType == sectDynamic) {
					$objAnswersColl = @$objReportData->ArrayDynSectionValues[$intSectionID];
				}
				else {
					$objAnswersColl = @$objReportData->ArrayInlineSectionValues[$intSectionID];
				}
				//@JAPR
				if (!is_null($objAnswersColl) && is_array($objAnswersColl)) {
					$intIndex = -1;
					if ($arrHasMultiChoiceQuestions[$intSectionID]) {
						//Si hay preguntas múltiples, se debe hacer un procesamiento mas complejo porque por cada key del SectionValue se tiene que indicar
						//a qué página pertenece para poder repetir la respuesta de cada pregunta Simple Choice por tantos keys existan en cada página
						$intPageNum = -1;
						$intPageKey = $intIndex;
						foreach ($objAnswersColl as $aPageValue) {
							if ($aPageValue['DynamicPageDSC'] != '') {
								//Se trata de un registro que representa a la página, así que incrementa los contadores y agrega el valor de la 
								//página al array correspondiente
								$intPageKey = $intIndex;
								$intPageNum++;
								//$intPageNum = count($arrSectionPage);
								//$arrSectionPage[$intPageNum] = $intPageKey;
								$arrSectionValuesByPage[$intSectionID][$intPageNum] = array();
								$arrSectionValuesByDesc[$intSectionID][$intPageNum] = array();
								$arrSectionPagesByDesc[$intSectionID][$intPageNum] = $aPageValue['DynamicPageDSC'];
							}
							
							//Cualquier valor representa el de un checkbox, así que lo agrega a dicho array y mas hace referencia a la página en la
							//que está definido
							$intKey = $intIndex;
							//$arrSectionValue[] = $intKey;
							$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
							$arrSectionValuesByPage[$intSectionID][$intPageNum][] = $intKey;
							$arrSectionValuesByDesc[$intSectionID][$intPageNum][$aPageValue['desc']] = $intKey;
							$intIndex--;
						}
						
						//@JAPR 2013-08-15: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
						//recibido por el App
						//En este caso, aunque se utilice un reporte para cargar los datos, si se regeneró la sección dinámica durante la edición
						//entonces ya no se pueden usar los datos que traía el reporte, por tanto se tienen que identificar las páginas directamente
						//de los datos recibidos por el App
						if ($blnStoreDataFromApp[$intSectionID] && isset($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
							$intDynamicPageNumber = -1;
							$intPageNum = 0;
							$arrPagesByAttribDesc = array();
							foreach ($arrSectionDesc as $intIndex => $arrCombinationValues) {
								$intMembersCount = count($arrCombinationValues);
								$strPageValueDesc = (string) @$arrCombinationValues[$intMembersCount -2];
								//Se genera la página o se reutiliza el número de la previamente generada
								if (!isset($arrPagesByAttribDesc[$strPageValueDesc])) {
									//Genera una nueva página correspondiente a esta descripción de subsección dinámica
									$intDynamicPageNumber++;
									$arrPagesByAttribDesc[$strPageValueDesc] = $intDynamicPageNumber;
									$arrSectionPagesByDescFromApp[$intSectionID][$intDynamicPageNumber] = $strPageValueDesc;
								}
							}
							
							$arrSectionPagesByDesc[$intSectionID] = $arrSectionPagesByDescFromApp[$intSectionID];
						}
						//@JAPR
					}
					else {
						//Si no hay preguntas múltiple, entonces cada key del section value tendrá una única respuesta así que no se hace nada extra
						foreach ($objAnswersColl as $aPageValue) {
							$intPageKey = $intIndex;
							//$arrSectionValue[] = $intPageKey;
							//$arrSectionPage[] = $intPageKey;
							$arrPagesBySectionValues[$intSectionID][$intPageKey] = $intPageKey;
							$intIndex--;
						}
						$arrSectionValuesByPage[$intSectionID] = array_keys($arrPagesBySectionValues[$intSectionID]);
					}
				}
			}
			else {
				//@JAPR 2013-04-11: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
				//Si se recibieron las descripciones completas de los elementos desde el App, se utilizarán dichas descripciones para generar las
				//páginas ya que no se puede confiar en los keys subrrogados pues pudieran ya no hacer match con las mismas combinaciones y/o
				//haberse cambiado por otros o al registro de NA, de todas formas las descripciones completas indican la paginación tal como se
				//hizo la captura
				if (isset($arrSectionDesc) && is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
					//En este caso si se recibió el array con los valores completos y debe venir en el mismo orden de páginas que se usó en el App,
					//así que simplemente se recorre y se obtiene todos los posibles valores del penúltimo de los atributos pues ese es el que
					//representa a las páginas
					$intDynamicPageNumber = -1;
					$intPageNum = 0;
					$arrPagesByAttribDesc = array();
					foreach ($arrSectionDesc as $intIndex => $arrCombinationValues) {
						$intMembersCount = count($arrCombinationValues);
						$strPageValueDesc = (string) @$arrCombinationValues[$intMembersCount -2];
						$strMultiDesc = (string) @$arrCombinationValues[$intMembersCount -1];
						//Este es el key original del catálogo que usó la captura, no el key remapeado a como está actualmente el catálogo
						//$intKey = $arrSectionValueCopy[$intIndex];
						$intKey = $arrSectionValue[$intIndex];
						$intKeyOrig = $arrSectionValueCopy[$intIndex];
						if ($arrHasMultiChoiceQuestions[$intSectionID]) {
							//Se genera la página o se reutiliza el número de la previamente generada
							if (!isset($arrPagesByAttribDesc[$strPageValueDesc])) {
								//Genera una nueva página correspondiente a esta descripción de subsección dinámica
								$intDynamicPageNumber++;
								$arrPagesByAttribDesc[$strPageValueDesc] = $intDynamicPageNumber;
								$arrSectionValuesByPage[$intSectionID][$intDynamicPageNumber] = array();
								$arrSectionValuesByDesc[$intSectionID][$intDynamicPageNumber] = array();
								$arrSectionPagesByDesc[$intSectionID][$intDynamicPageNumber] = $strPageValueDesc;
								$intPageKey = $intKey;
							}
							
							$intPageNum = (int) @$arrPagesByAttribDesc[$strPageValueDesc];
							//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
							//Se debe usar el array de keys originalmente recibido y no los remapeados, ya que se usará para asignar las posiciones
							//de las respuestas al procesar el outbox y en el remapeo varios keys pudieran cambia a 1
							//$arrSectionValuesByPage[$intSectionID][$intPageNum][] = $intKey;
							$arrSectionValuesByPage[$intSectionID][$intPageNum][] = $intKeyOrig;
							$arrSectionValuesByDesc[$intSectionID][$intPageNum][$strMultiDesc] = $intKeyOrig;
							//@JAPR
							
							//Si el key de este valor es el NA, significa que no pudo ser mapeado así que se generará un array donde por posición
							//se indicará en que página estaba dicho valor no mapeado
							if ($intKey == 1) {
								if (!isset($arrPagesBySectionValues[$intSectionID][$intKey])) {
									$arrPagesBySectionValues[$intSectionID][$intKey] = array();
								}
								
								//En este caso para el mismo key subrrogado de NA se indica la página por su pisición en el array de valores múltiples
								$arrPagesBySectionValues[$intSectionID][$intKey][$intIndex] = $intPageKey;
							}
							else {
								//Cualquer otro key si mapeado simplemente se agrega al array indicando la página directamente
								$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
							}
						}
						else {
							$intPageKey = $intKey;
							
							//Si el key de este valor es el NA, significa que no pudo ser mapeado así que se generará un array donde por posición
							//se indicará en que página estaba dicho valor no mapeado
							if ($intKey == 1) {
								if (!isset($arrPagesBySectionValues[$intSectionID][$intKey])) {
									$arrPagesBySectionValues[$intSectionID][$intKey] = array();
								}
								
								//En este caso para el mismo key subrrogado de NA se indica la página por su pisición en el array de valores múltiples
								$arrPagesBySectionValues[$intSectionID][$intKey][$intIndex] = $intPageKey;
							}
							else {
								//Cualquer otro key si mapeado simplemente se agrega al array indicando la página directamente
								$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
							}
							
							//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
							//Se debe usar el array de keys originalmente recibido y no los remapeados, ya que se usará para asignar las posiciones
							//de las respuestas al procesar el outbox y en el remapeo varios keys pudieran cambia a 1
							//$arrSectionValuesByPage[$intSectionID][] = $intKey;
							$arrSectionValuesByPage[$intSectionID][] = $intKeyOrig;
						}
					}
				}
				else {
					//En este caso no hay un array con los valores completos, por lo tanto se tiene que usar la lista de keys y obtener las
					//descripciones desde el catálogo, pero en caso de haber cambiado los valores de página se va a hacer un desajuste y no se
					//podrá grabar como originalmente se hizo desde el App. Este caso sólo podría ser por una captura de una versión v4 muy vieja
					if ($arrHasMultiChoiceQuestions[$intSectionID]) {
						//Si hay preguntas múltiples, se debe hacer un procesamiento mas complejo porque por cada key del SectionValue se tiene que indicar
						//a qué página pertenece para poder repetir la respuesta de cada pregunta Simple Choice por tantos keys existan en cada página
						$intIndex = 0;
						foreach ($arrCatalogValues as $aPageValue) {
							$intPageKey = (int) @$aPageValue['key'];
							if (isset($aPageValue['children'])) {
								$arrSectionValuesByPage[$intSectionID][$intIndex] = array();
								$arrSectionValuesByDesc[$intSectionID][$intIndex] = array();
								$arrSectionPagesByDesc[$intSectionID][$intIndex] = (string) @$aPageValue['value'];
								foreach ($aPageValue['children'] as $aSectionValue) {
									$intKey = (int) @$aSectionValue['key'];
									$arrPagesBySectionValues[$intSectionID][$intKey] = $intPageKey;
									$arrSectionValuesByPage[$intSectionID][$intIndex][] = $intKey;
									//@JAPR 2013-04-25: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
									//Se debe usar el array de keys originalmente recibido y no los remapeados, ya que se usará para asignar las posiciones
									//de las respuestas al procesar el outbox y en el remapeo varios keys pudieran cambia a 1
									$arrSectionValuesByDesc[$intSectionID][$intIndex][$aSectionValue['value']] = $intKeyOrig;
								}
								$intIndex++;
							}
						}
					}
					else {
						//Si no hay preguntas múltiple, entonces cada key del section value tendrá una única respuesta así que no se hace nada extra
						foreach ($arrSectionValue as $intIndex => $intKey) {
							$arrPagesBySectionValues[$intSectionID][$intKey] = $intKey;
						}
						//@JAPR 2012-11-28: Corregido un bug, estaba dejando como índice el mismo key y se esperaba un consecutivo de página
						$arrSectionValuesByPage[$intSectionID] = array_keys($arrPagesBySectionValues[$intSectionID]);
					}
				}
				//@JAPR
			}
			//@JAPR
			
			//@JAPR 2014-05-28: Agregado el tipo de sección Inline
			//Esto sólo aplica a las secciones dinámicas, no a las secciones Inline aunque se comporten igual
			switch ($aSection->SectionType) {
				case sectDynamic:
					//En este momento sólo existe una sección dinámica por encuesta, así que genera en el $_POST el valor de sectionValue
					$_POST['sectionValue'] = $arrSectionValue;
					$_POST['sectionDesc'] = $arrSectionDesc;
					break;
				case sectInline:
					$_POST['inlineValue'.$inlineSectionID] = $arrSectionValue;
					$_POST['inlineDesc'.$inlineSectionID] = $arrSectionDesc;
					break;
			}
			//@JAPR
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo ("<br>\r\n");
		echo ("<br>\r\nPages ByID (arrSectionPagesByID):");
		PrintMultiArray($arrSectionPagesByID);
		echo ("<br>\r\nPages by section value (arrPagesBySectionValues):");
		PrintMultiArray($arrPagesBySectionValues);
		echo ("<br>\r\nSectionValues ByPage (arrSectionValuesByPage):");
		PrintMultiArray($arrSectionValuesByPage);
		//@JAPR 2013-08-14: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
		//recibido por el App
		echo ("<br>\r\nSectionValues ByPage from App (arrSectionValuesByPageFromApp):");
		PrintMultiArray($arrSectionValuesByPageFromApp);
		//@JAPR
		echo ("<br>\r\nSectionValues ByDesc (arrSectionValuesByDesc):");
		PrintMultiArray($arrSectionValuesByDesc);
		//@JAPR 2013-08-14: Ahora dependiendo de los keys subrrogados recibidos, se decidirá si se usa un array local de páginas o el array
		//recibido por el App
		echo ("<br>\r\nSectionValues ByDesc from App (arrSectionValuesByDescFromApp):");
		PrintMultiArray($arrSectionValuesByDescFromApp);
		//@JAPR
		echo ("<br>\r\nSectionValues ByPageDesc (arrSectionPagesByDesc):");
		PrintMultiArray($arrSectionPagesByDesc);
		echo ("<br>\r\nSectionValues ByID (arrSectionValuesByID):");
		PrintMultiArray($arrSectionValuesByID);
		echo ("<br>\r\nSectionValues ByKey (arrSectionValueByKey):");
		PrintMultiArray($arrSectionValueByKey);
		//@JAPR 2013-03-27: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		echo ("<br>\r\nDynamic Values ByID (arrSectionDescByID):");
		PrintMultiArray($arrSectionDescByID);
		echo ("<br>\r\nDynamic String Values ByID (arrSectionDescStrByID):");
		PrintMultiArray($arrSectionDescStrByID);
		//@JAPR 2013-04-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
		echo ("<br>\r\nDynamic Pages by multi-checkbox value from App (arrSectionValuesByMultiChoiceAnswerFromApp):");
		PrintMultiArray($arrSectionValuesByMultiChoiceAnswerFromApp);
		//@JAPR 2013-08-14: Corregido un bug, con la optimización para grabar a partir de las descripciones de las
		//preguntas de catálogo, se afectó al grabado de las dinámicas cuando se cambia la sección durante la
		//edición, ya que en esos casos no vendrán valores dummies negativos sino que se regresan los keys subrrogados
		//reales que el App tenía para las opciones del catálogo utilizadas en la sección dinámica, por tanto no
		//se puede usar el array de keys por página dummy sino que se debe elegir usar el array real basado
		//también en los datos recibidos del App
		echo ("<br>\r\nDynamic section FKs (arrDynamicSectionFKs):");
		PrintMultiArray($arrDynamicSectionFKs);
		//@JAPR 2013-08-20: Corregido un bug, si durante la edición se capturaban nuevos registros de Maestro-detalle con simple choice de catálogo,
		//estaba dejando literalmente el valor NOCHANGE como respuesta por lo que se grababa mal
		echo ("<br>\r\nMaster-det section FKs from App (arrMasterSectionFKsFromEntry):");
		PrintMultiArray($arrMasterSectionFKsFromEntry);
		//@JAPR 2014-05-30: Agregado el tipo de sección Inline
		echo ("<br>\r\nInline section FKs (arrInlineSectionFKs):");
		PrintMultiArray($arrInlineSectionFKs);
		echo ("<br>\r\nInline section FKs from App (arrInlineSectionFKsFromEntry):");
		PrintMultiArray($arrInlineSectionFKsFromEntry);
		echo ("<br>\r\nallAttribs from app:");
		PrintMultiArray($garrCatalogDefs);
		echo ("<br>\r\nallAttribValues from app:");
		PrintMultiArray($garrCatalogData);
		//@JAPR
		//die();
	}
	
	//Inicia el barrido de las respuestas recibidas
	$arrAnswers = @$jsonData['answers'];
	if (is_null($arrAnswers) || !is_array($arrAnswers)) {
		$arrAnswers = array();
	}
	
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	//Agrega al POST el valor del score total de la captura para tenerlo disponible en todo momento
	$_POST['SurveyScore'] = (float) @$jsonData['score'];
	//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
	$_POST['AgendaCancelled'] = (int) @$jsonData['agendaCancelled'];
	//@JAPR
	
	//La fecha final de la captura por default es la fecha recibida para dicho fin, pero si no se recibió entonces es la fecha actual real del server
	$thisDate = date("Y-m-d H:i:s");
	if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
	{
		$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
	}
	
	// @LROUX 2012-11-29: Modificado para enviar la captura a una funcion customizada (PCP API).
	require_once("customClasses.inc.php");
	$aCustomFunction = GetBITAMCustomStoreSurveyCaptureFunction($aRepository);
	if ($aCustomFunction != '')
	{
		if (function_exists($aCustomFunction))
		{
			$aCustomResult = $aCustomFunction($aRepository, $surveyInstance, $jsonData, @$arrQuestionsByID);
			if (!$aCustomResult)
			{
				return;
			}
		}
	}
	
	//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
	//Contiene la cantidad de registros de secciones maestro-detalle, se va generando conforme se leen las preguntas de estas secciones,
	//se verifica cual es el máximo índice de una de sus respuestas (ya que no todas las preguntas se mandan siempre) y esa la considera
	//como el máximo registro capturado
	$arrMasterSectionRecs = array();
	//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
	//Contiene el factkey por cada registro de la sección maestro-detalle, se va generando conforme se leen las preguntas de estas secciones,
	//se verifica si trae o no asignado un factKey, si no lo trae es porque o se trata de una pregunta que originalmente no se había contestado
	//y por tanto en el draft no estaba incluida, o bien porque es un registro nuevo, así que en esos casos no asigna factKey, de lo 
	//contrario sólo podría ser una pregunta que se descargó en el Draft a editar y por tanto trae un factKey en dicho registro, así que
	//almacena ese valor. Teoricamente 2 o mas preguntas del mismo registro no podrían traer diferentes factKeys, sólo pueden existir 0s o
	//el mismo factKey en las respuestas del mismo registro de esta sección
	$arrMasterSectionFKs = array();
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	//Aunque las secciones Inline son mas parecidas a las Dinámicas, son realmente un híbrido combinado con las Maestro detalle por lo menos en el
	//uso de preguntas de catálogo (incluso del mismo que generó la Inline) internamente, no reciben trato especial las Múltiple choice, aunque en
	//realidad no crecen dinámicamente como las Maestro detalle sino que están condicionadas a un conjunto fijo de opciones como las dinámicas
	$arrInlineSectionRecs = array();
	$arrInlineSectionFKs = array();
	//@JAPR
	
	foreach ($arrAnswers as $intQuestionID => $objAnswer) {
		$objQuestion = @$arrQuestionsByID[$intQuestionID];
		if (!is_null($objQuestion)) {
			$postKey = 'qfield'.$intQuestionID;
			//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
			//Para las formas de V6 se necesita en las preguntas tipo Múltiple Choice con captura que no sea de CheckBox, las opciones que fueron marcadas en el mismo orden que
			//el dato capturado para cada opción
			$postKeyOptions = 'qoptions'.$intQuestionID;
			//@JAPR
			$postKeyComment = "qcomment".$intQuestionID;
			$postKeyImage = "qimageencode".$intQuestionID;
			$postKeyDocument = "qdocument".$intQuestionID;
			$postKeyLat = "qlat".$intQuestionID;
			$postKeyLong = "qlong".$intQuestionID;
			$postKeyAcc = "qacc".$intQuestionID;
			//@JAPR 2014-10-29: Agregado el mapeo de Scores
			$postKeyNum = 'qfield'.$objQuestion->QuestionNumber;
			//@JAPR
			
			//La pregunta aun existe, así que asigna el parámetro correspondiente según el tipo de pregunta
			$objSection = @$arrSectionsByID[$objQuestion->SectionID];
			if (is_null($objSection) || $objSection->SectionType == sectFormatted) {
				//No debería ser posible que no existiera la sección, pero si se da entonces continua con la siguiente pregunta para evitar errores
				//Tampoco se pueden procesar preguntas de secciones formateadas
				continue;
			}
			$intSectionID = $objSection->SectionID;
			
			//Valida los tipos de preguntas que no pueden grabar ningún dato para saltarlos
			//@JAPR 2013-04-15: Agregados nuevos tipos de pregunta Sincronización
			//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
			//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
			//Cambiado a un switch para mayor facilidad
			$blnValidQuestion = true;
			switch ($objQuestion->QTypeID) {
				case qtpMessage:
				case qtpSkipSection:
				case qtpShowValue:
				case qtpSync:
				case qtpPassword:
				case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
				//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
				case qtpUpdateDest:
					$blnValidQuestion = false;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			//@JAPR
			
			//Basado en el tipo de pregunta, prepara el parámetro $_POST para que sea interpretado correctamente por el proceso de grabado
			switch ($objQuestion->QTypeID) {
				case qtpMulti:
					//Si la pregunta es Multiple Choice por el momento no se soportan de catálogo excepto dentro de la sección dinámica, donde
					//no se soportarían normales, así que únicamente valida por el tipo de Sección
					
					/*
					$arrSelectOptions = array();
					//Si la pregunta depende de otra para llenar sus opciones, se tiene que obtener la lista de opciones de la pregunta original
					if ($objQuestion->SourceQuestionID > 0) {
						
					}
					*/
					
					//Primero genera un array con las opciones de respuesta como índice y cuyo contenido será vacio, así se llenará según el
					//tipo de captura con el valor correspondiente, pero sólo si no es tipo de captura Checkbox
					//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
					//En V6 el proceso de grabado no usará el total de opciones que había en el server, sino las recibidas en el archivo de datos, así que no debe preparar este array
					//con las opciones existentes sino dejarlo vacio tal como si fuera tipo CheckBox
					if ($surveyInstance->CreationAdminVersion < esveFormsv6 && count($objQuestion->SelectOptions) > 0 && $objQuestion->MCInputType != mpcCheckBox) {
						$arrSelectOptions = array_combine(array_keys(array_flip($objQuestion->SelectOptions)), array_fill(0, count($objQuestion->SelectOptions), ''));
					}
					else {
						$arrSelectOptions = array();
					}
					
					switch ($objSection->SectionType) {
						case sectDynamic:
							//Si se trata de una sección dinámica, se debe enviar un array equivalente al SectionValue pero conteniendo un 1 o 0
							//dependiendo de si se seleccionó o no la opción (si es tipo Checkbox), o bien el dato capturado para cada opción
							//y vacío para las que no capturaron, esto por cada key del atributo que generó las opciones Múltiples
							
							//Los arrays deben contener tantos elementos como SectionValue existan en esta sección
							$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
							if (is_null($arrSectionValue)) {
								continue;
							}
							
							//@JAPR 2013-01-24: Agregada la opción para grabar indicadores como NULL en preguntas numéricas
							$_POST[$postKey] = array_fill(0, count($arrSectionValue), (($surveyInstance->UseNULLForEmptyNumbers)?'':'0'));
							//@JAPR
							$_POST[$postKeyComment] = array_fill(0, count($arrSectionValue), '');
							$_POST[$postKeyImage] = array_fill(0, count($arrSectionValue), '');
							$arrSectionValueByKey = array_flip($arrSectionValue);
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intPageNum => $aRecAnswer) {
									//Por cada página generada debe obtener el array de los SectionValue que se deben modificar para
									//sobreescribir en todos esos índices
									//$arrPageValues = @$arrSectionValuesByPage[$intSectionID][$intPageNum];
									//if (is_null($arrPageValues)) {
									//@JAPR 2013-08-14: Corregido un bug, con la optimización para grabar a partir de las descripciones de las
									//preguntas de catálogo, se afectó al grabado de las dinámicas cuando se cambia la sección durante la
									//edición, ya que en esos casos no vendrán valores dummies negativos sino que se regresan los keys subrrogados
									//reales que el App tenía para las opciones del catálogo utilizadas en la sección dinámica, por tanto no
									//se puede usar el array de keys por página dummy sino que se debe elegir usar el array real basado
									//también en los datos recibidos del App
									if ($blnEdit && $entryID > 0 && @$blnStoreDataFromApp[$intSectionID]) {
										$arrValuesByDesc = @$arrSectionValuesByDescFromApp[$intSectionID][$intPageNum];
									}
									else {
										$arrValuesByDesc = @$arrSectionValuesByDesc[$intSectionID][$intPageNum];
									}
									//@JAPR
									if (is_null($arrValuesByDesc)) {
										continue;
									}
									
									$arrMulti = @$aRecAnswer['multiAnswer'];
									if (!is_null($arrMulti) && is_array($arrMulti)) {
										foreach ($arrMulti as $strOption => $strValue) {
											$strOptionDec = $strOption;
											$strValueDec = $strValue;
											if ($blnDecodeUTF8) {
												$strOptionDec = $strOptionDec;
												$strValueDec = $strValueDec;
											}
											if (isset($arrValuesByDesc[$strOption])) {
												$intIdx = $arrValuesByDesc[$strOption];
												$intData = $strValueDec;
												if ($intData != '') {
													$intIdx = (int) @$arrSectionValueByKey[$intIdx];
													$_POST[$postKey][$intIdx] = $intData;
												}
											}
										}
									}
									
									//La foto y el comentario son independientes de la respuesta, así que esas se repiten en todos los índices
									//de esta página
									//@JAPRWarning: Creo que esto está mal, debería mandarse por pregunta así que se hizo ese ajuste
									/*
									foreach ($arrPageValues as $intKey) {
										if (!isset($arrSectionValueByKey[$intKey])) {
											continue;
										}
									*/
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										if ($blnDecodeUTF8) {
											$strData = $strData;
										}
										//$_POST[$postKeyComment][$intIdx] = $strData;
										$_POST[$postKeyComment] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										//$_POST[$postKeyImage][$intIdx] = $strData;
										$_POST[$postKeyImage] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										//$_POST[$postKeyImage][$intIdx] = $strData;
										$_POST[$postKeyDocument] = $strData;
									}
									//}
									
									//@JAPR 2014-10-29: Agregado el mapeo de Scores
									//Agrega al array de Scores el valor por cada respuesta de la sección
									if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
										$arrQuestionScoresFromApp[$postKeyNum] = array();
									}
									$arrQuestionScoresFromApp[$postKeyNum][$intPageNum] = (float) @$aRecAnswer['score'];
									//@JAPR
								}
							}
							break;
						
						case sectMasterDet:
						//@JAPR 2014-05-28: Agregado el tipo de sección Inline
						case sectInline:
							//En el caso de secciones múltiples, las preguntas Multiple Choice vienen como una cadena sólo de los valores 
							//seleccionados si es tipo CheckBox, o bien una cadena del dato introducido para cada valor por cada opción
							//posible de respuesta se hubiera o no seleccionado, en ambos casos separada por "_SVSep_", sólo que se genera un Array
							//con una respuesta por cada página
							$_POST[$postKey] = array();
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intIdx => $aRecAnswer) {
									$strData = '';
									$arrMulti = @$aRecAnswer['multiAnswer'];
									$arrSelectOptionsTemp = $arrSelectOptions;
									if (!is_null($arrMulti) && is_array($arrMulti)) {
										foreach ($arrMulti as $strOption => $strValue) {
											$strOptionDec = $strOption;
											$strValueDec = $strValue;
											if ($blnDecodeUTF8) {
												$strOptionDec = $strOptionDec;
												$strValueDec = $strValueDec;
											}
											if ($objQuestion->MCInputType == mpcCheckBox) {
												//Se agrega al array el elemento seleccionado
												//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
												//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
												if ($objQuestion->QDisplayMode == dspLabelnum) {
													$arrSelectOptionsTemp[(int) $strValueDec] = $strOptionDec;
												}
												else {
													$arrSelectOptionsTemp[] = $strOptionDec;
												}
												//@JAPR
											}
											else {
												//En este caso se sobreescribe del array el elemento seleccionado
												//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
												//Si se trata de una forma de v6, debe agregar el elemento, ya que el array se limpió al iniciar este proceso, agregando lo aquí se asegura que
												//llegará al proceso de grabado el valor capturado para esta opción de respuesta como respuesta adicional
												if ($surveyInstance->CreationAdminVersion >= esveFormsv6 || isset($arrSelectOptionsTemp[$strOptionDec])) {
													$arrSelectOptionsTemp[$strOptionDec] = $strValueDec;
												}
												//@JAPR
											}
										}
										if (count($arrSelectOptionsTemp) > 0) {
											//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
											//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
											if ($objQuestion->QDisplayMode == dspLabelnum) {
												//Ahora ordena a partir de las llaves en forma ascendente numérica
												ksort($arrSelectOptionsTemp, SORT_NUMERIC);
											}
											//@JAPR
											$strData = implode('_SVSep_', $arrSelectOptionsTemp);
										}
									}
									if ($strData != '') {
										$_POST[$postKey][$intIdx] = $strData;
										//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
										//Si se trata de una forma de v6, este dato fue la lista de opciones marcadas cuando es pregunta tipo CheckBox, o bien los datos adicionales respondidos
										//cuando es otro tipo de captura, pero en ese segundo caso necesitamos saber a que opciones correspondían así que se agrega un segundo parámetro
										if ($surveyInstance->CreationAdminVersion >= esveFormsv6 || isset($arrSelectOptionsTemp[$strOptionDec])) {
											if (count($arrSelectOptionsTemp) > 0) {
												$_POST[$postKeyOptions][$intIdx] = implode('_SVSep_', array_keys($arrSelectOptionsTemp));
											}
										}
									}
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
									
									//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
									$intFactKey = (int) @$aRecAnswer['factKey'];
									if ($intFactKey > 0) {
										//@JAPR 2014-05-28: Agregado el tipo de sección Inline
										if ($objSection->SectionType == sectInline) {
											if (!isset($arrInlineSectionFKs[$intSectionID])) {
												$arrInlineSectionFKs[$intSectionID] = array();
											}
											$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										else {
											if (!isset($arrMasterSectionFKs[$intSectionID])) {
												$arrMasterSectionFKs[$intSectionID] = array();
											}
											$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										//@JAPR
									}
									
									//@JAPR 2014-10-29: Agregado el mapeo de Scores
									//Agrega al array de Scores el valor por cada respuesta de la sección
									if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
										$arrQuestionScoresFromApp[$postKeyNum] = array();
									}
									$arrQuestionScoresFromApp[$postKeyNum][$intIdx] = (float) @$aRecAnswer['score'];
									//@JAPR
								}
								
								//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
								//@JAPR 2014-05-28: Agregado el tipo de sección Inline
								if ($objSection->SectionType == sectInline) {
									if (!isset($arrInlineSectionRecs[$intSectionID])) {
										$arrInlineSectionRecs[$intSectionID] = $intIdx;
									}
									else {
										if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
											$arrInlineSectionRecs[$intSectionID] = $intIdx;
										}
									}
								}
								else {
									if (!isset($arrMasterSectionRecs[$intSectionID])) {
										$arrMasterSectionRecs[$intSectionID] = $intIdx;
									}
									else {
										if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
											$arrMasterSectionRecs[$intSectionID] = $intIdx;
										}
									}
								}
								//@JAPR
							}
							break;
						
						default:
							//En cualquier otro tipo de sección, las preguntas Multiple Choice vienen como una cadena sólo de los valores 
							//seleccionados si es tipo CheckBox, o bien una cadena del dato introducido para cada valor por cada opción
							//posible de respuesta se hubiera o no seleccionado, en ambos casos separada por "_SVSep_"
							$strData = '';
							$arrMulti = @$objAnswer['multiAnswer'];
							if (!is_null($arrMulti) && is_array($arrMulti)) {
								foreach ($arrMulti as $strOption => $strValue) {
									$strOptionDec = $strOption;
									$strValueDec = $strValue;
									if ($blnDecodeUTF8) {
										$strOptionDec = $strOptionDec;
										$strValueDec = $strValueDec;
									}
									if ($objQuestion->MCInputType == mpcCheckBox) {
										//Se agrega al array el elemento seleccionado
										//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
										//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
										if ($objQuestion->QDisplayMode == dspLabelnum) {
											$arrSelectOptions[(int) $strValueDec] = $strOptionDec;
										}
										else {
											$arrSelectOptions[] = $strOptionDec;
										}
										//@JAPR
									}
									else {
										//En este caso se sobreescribe del array el elemento seleccionado
										//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
										//Si se trata de una forma de v6, debe agregar el elemento, ya que el array se limpió al iniciar este proceso, agregando lo aquí se asegura que
										//llegará al proceso de grabado el valor capturado para esta opción de respuesta como respuesta adicional
										if ($surveyInstance->CreationAdminVersion >= esveFormsv6 || isset($arrSelectOptions[$strOptionDec])) {
											$arrSelectOptions[$strOptionDec] = $strValueDec;
										}
										//@JAPR
									}
								}
								if (count($arrSelectOptions) > 0) {
									//@JAPR 2013-07-18: Corregido un bug, las preguntas ranking no estaban grabandose en la secuencia correcta
									//cuando las respuestas eran numéricas, así que se usará el valor recibido en lugar de la posición en el array
									if ($objQuestion->QDisplayMode == dspLabelnum) {
										//Ahora ordena a partir de las llaves en forma ascendente numérica
										ksort($arrSelectOptions, SORT_NUMERIC);
									}
									//@JAPR
									$strData = implode('_SVSep_', $arrSelectOptions);
								}
							}
							if ($strData != '') {
								$_POST[$postKey] = $strData;
								//@JAPR 2015-07-11: Agregado el soporte de Formas v6 con un grabado completamente diferente
								//Si se trata de una forma de v6, este dato fue la lista de opciones marcadas cuando es pregunta tipo CheckBox, o bien los datos adicionales respondidos
								//cuando es otro tipo de captura, pero en ese segundo caso necesitamos saber a que opciones correspondían así que se agrega un segundo parámetro
								if ($surveyInstance->CreationAdminVersion >= esveFormsv6 || isset($arrSelectOptions[$strOptionDec])) {
									if (count($arrSelectOptions) > 0) {
										$_POST[$postKeyOptions] = implode('_SVSep_', array_keys($arrSelectOptions));
									}
								}
								//@JAPR
							}
							$strData = (string) @$objAnswer['comment'];
							if ($strData != '') {
								if ($blnDecodeUTF8) {
									$strData = $strData;
								}
								$_POST[$postKeyComment] = $strData;
							}
							$strData = (string) @$objAnswer['photo'];
							if ($strData != '') {
								if ($blnDecodeUTF8) {
									$strData = $strData;
								}
								$_POST[$postKeyImage] = $strData;
							}
							$strData = (string) @$objAnswer['document'];
							if ($strData != '') {
								$_POST[$postKeyDocument] = $strData;
							}
							
							//@JAPR 2014-10-29: Agregado el mapeo de Scores
							//Agrega al array de Scores el valor por cada respuesta de la sección
							if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
								$arrQuestionScoresFromApp[$postKeyNum] = array();
							}
							$arrQuestionScoresFromApp[$postKeyNum][0] = (float) @$objAnswer['score'];
							//@JAPR
							break;
					}
					break;
					
					//Conchita 2013-05-03 agregado callList
				case qtpCallList:
				case qtpSingle:
					if ($objQuestion->CatalogID > 0) {
						//Si la pregunta es de catálogo, se debe obtener la jerarquía completa de atributos dependiendo del valor del key recibido
						//ya que como funciona la v4 no es posible identificar para las maestro-detalle toda la jerarquía a partir del filtro,
						//ya que el filtro es una referencia idéntica en todos los elementos así que sólo contendría la jerarquía del último
						//seleccionado
						$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $objQuestion->CatalogID);
						if (is_null($objCatalog)) {
							continue;
						}
						$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $objQuestion->CatalogID);
						if (is_null($objCatalogMembersColl)) {
							continue;
						}
						
						//Obtiene la lista de atributos de la pregunta (incluyendo todos los padres)
						$arrAttributeIDs = array();
						foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
							//@JAPR 2013-02-11: Agregada la edición de datos históricos
							$arrAttributeIDs[] = array('dimID' => $objCatalogMember->ClaDescrip, 'indDimID' => (int) @$objCatalogMember->IndDimID);
							
							//Cuando se llegó al atributo de la pregunta debe salir del ciclo porque no interesan mas atributos
							if ($objCatalogMember->MemberID == $objQuestion->CatMemberID) {
								break;
							}
						}
						
						if (count($arrAttributeIDs) == 0) {
							continue;
						}
						
						$strFilter = 'AND '.$objCatalog->TableName.'KEY = ';
						
						switch ($objSection->SectionType) {
							//@JAPR 2013-02-14: Agregado el soporte de preguntas simple choice de catálogo dentro de dinámicas, que hasta v3 era
							//una restricción debido a la manera de armar el HTML pero en v4 ya no hay problema
							case sectDynamic:
							//@JAPR
							case sectMasterDet:
							//@JAPR 2014-05-28: Agregado el tipo de sección Inline
							case sectInline:
								if (is_array($objAnswer)) {
									foreach ($objAnswer as $intIdx => $aRecAnswer) {
										$intData = (int) @$aRecAnswer['key'];
										//@JAPR 2013-08-23: Agregada una validación para los casos donde el key venía como 0 por alguna razón
										//no determinada, siendo que si traía una respuesta correctamente seleccionada
										$arrCatalogFilter = @$aRecAnswer['catalogFilter'];
										$strValue = (string) @$aRecAnswer['answer'];
										if ($intData > 0 || ($intData == 0 && $strValue != '' && !is_null($arrCatalogFilter) && is_array($arrCatalogFilter) && count($arrCatalogFilter) > 0)) {
											//@JAPR 2013-05-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
											//A partir de ahora intentará reconstruir el String de la simple choice a partir de las descripciones recibidas,
											//si hubiera algún problema al intentarlo, se utilizará el método original. Es seguro reconstruir con el array
											//de descripciones recibido porque el key de estas preguntas ya no se usa realmente, sino que con dicho array se
											//intentará obtener mas adelante el key precisamente, pero esos casos se validan ya durante el grabado
											$blnGetAnswerFromCatalog = true;
											
											$intMax = count($arrAttributeIDs);
											$arrCatalogFilter = @$aRecAnswer['catalogFilter'];
											$strValue = (string) @$aRecAnswer['answer'];
											if (is_null($arrCatalogFilter) || !is_array($arrCatalogFilter)) {
												$arrCatalogFilter = array();
											}
											
											//Recorre cada atributo y extrae la parte del filtro correspondiente para concatenarlo, si no encuentra
											//un atributo se considera que no es válida la respuesta y la omite
											$strData = '';
											if ($strValue != '') {
												for ($intIndex = 0, $intLastAttrib = $intMax -1; $intIndex < $intMax; $intIndex++) {
													if ($intIndex < $intLastAttrib) {
														//@JAPR 2015-08-25: Enviada la definición del catálogo desde el App
														//Corregido un bug, para las preguntas auto-complete no venían todos los atributos, así que ahora si no se encuentra alguno, lo
														//buscará en el array de todos los valores antes de determinar el valor como inválido
														if (!isset($arrCatalogFilter[$intIndex])) {
															$arrAllValues = @$garrCatalogData['q'.$objQuestion->QuestionID][$intIdx];
															if (is_null($arrAllValues)) {
																$arrAllValues = array();
															}
															if (isset($arrAllValues[$intIndex])) {
																$arrCatalogFilter[$intIndex] = $arrAllValues[$intIndex];
															}
														}
														//@JAPR
														
														if (!isset($arrCatalogFilter[$intIndex])) {
															$strData = '';
															break;
														}
														$strCatFilter = $arrCatalogFilter[$intIndex];
														$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strCatFilter;
													}
													else {
														//@JAPR 2013-08-26: Agregada una validación para grabar a partir del filtro en lugar de la respuesta,
														//esto porque en algunos móviles (caso de Pulso) la respuesta de simple choice estaba llegando como
														//un valor de hora siendo que el key y catalogFilter si venían correctamente del catálogo, así que
														//como no se logró determinar la causa de esto, se validará en el server
														if ($appVersion >= esvPulsoSaveFromCatFilter && isset($arrCatalogFilter[$intIndex])) {
															$strCatFilter = $arrCatalogFilter[$intIndex];
															$strValue = $strCatFilter;
														}
														//@JAPR
												
														//El último valor es el atributo correspondiente a la pregunta, así que se extrae de la respuesta
														$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strValue;
														//Terminó de identificar el string con descripciones, ya no es necesario usara el catálogo
														$blnGetAnswerFromCatalog = false;
														break;
													}
												}
											}
											
											//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
											//Se agregó la validación de la versión de la forma, ya que en V6 es completamente seguro asumir que el propio archivo .dat contendrá los valores
											//de las secciones, así que no hay necesidad de obtenerlos desde el catálogo sino que se reutilizará lo recibido como datos de captura
											if ($surveyInstance->CreationAdminVersion < esveFormsv6 && $blnGetAnswerFromCatalog) {
												//Si no se hubiera logrado construir el string de catálogo con las descripciones recibidas, se intentará
												//utilizando el catálogo como se hacía antes, lo cual llevará a grabar una descripción incorrecta si el
												//catálogo se ha reorganizado o en el peor de los casos a no grabar la pregunta si ya no existe el key
												$strData = '';
												$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter.$intData.' ');
												//Se supone que sólo puede haber un valor con este Key, así que hace un proceso de recorrido por cada nodo
												//concatenando en el String hasta encontrar el último nivel solicitado
												$arrLevelValues = $arrCatalogValues;
												$intIndex = 0;
												$intMax = count($arrAttributeIDs);
												while (count($arrLevelValues) > 0 && $intIndex < $intMax) {
													if (isset($arrLevelValues[0])) {
														//Este valor NO se debe decodificar porque antes del grabado todo se procesa como utf8
														$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$arrLevelValues[0]['value'];
														$arrLevelValues = $arrLevelValues[0]['children'];
													}
													
													$intIndex++;
												}
											}
											//@JAPR
											
											if ($strData != '') {
												$_POST[$postKey][$intIdx] = substr($strData, 8);
												//Valida sólo para numéricas, ya que el resto de las preguntas se resuelve a String así que no hay tanto 
												//problema realmente si no contiene el formato que se esperaba
												//@JAPREliminar: Esto está de mas, ya se filtró arriba el tipo de pregunta a Simple choice o CallList
												if ($objQuestion->QTypeID == qtpOpenNumeric) {
													if (!is_numeric($_POST[$postKey][$intIdx])) {
														$_POST[$postKey][$intIdx] = '';
													}
												}
											}
										}
										//@JAPR 2013-02-11: Agregada la edición de datos históricos
										elseif ($surveyInstance->DissociateCatDimens && $intData < 0) {
											//Si se usan catálogos con dimensiones independientes, entonces si se puede recibir un valor negativo
											//y ser válido ya que eso significa que es el valor con el que estaba grabada la pregunta previamente,
											//por tanto se puede simplemente regenerar el string pero usando las propias descripciones que venían
											//como parte del filtro de la respuesta
											$intMax = count($arrAttributeIDs);
											$arrCatalogFilter = @$aRecAnswer['catalogFilter'];
											$strValue = (string) @$aRecAnswer['answer'];
											if (is_null($arrCatalogFilter) || !is_array($arrCatalogFilter)) {
												$arrCatalogFilter = array();
											}
											
											//Recorre cada atributo y extrae la parte del filtro correspondiente para concatenarlo, si no encuentra
											//un atributo se considera que no es válida la respuesta y la omite
											$strData = '';
											if ($strValue != '') {
												for ($intIndex = 0, $intLastAttrib = $intMax -1; $intIndex < $intMax; $intIndex++) {
													if ($intIndex < $intLastAttrib) {
														if (!isset($arrCatalogFilter[$intIndex])) {
															$strData = '';
															break;
														}
														$strCatFilter = $arrCatalogFilter[$intIndex];
														$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strCatFilter;
													}
													else {
														//El último valor es el atributo correspondiente a la pregunta, así que se extrae de la respuesta
														$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strValue;
														break;
													}
												}
											}
											
											if ($strData != '') {
												//@JAPR 2013-02-13: Agregada la edición de datos históricos
												//Se optó mejor por mandar un valor dummy que al procesar la pregunta se traducirá a que no hará
												//el UPDATE, de tal forma que el valor actualmente grabdo permanezca
												//@JAPR 2013-08-20: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
												//validar para no usar NOCHANGE sino el valor de catálogo correcto
												//Las preguntas simple choice de catálogo durante la edición, pudieran tomar el valor de un elemento
												//del catálogo con key negativo pero no necesariamente ser el mismo que tenía este registro, así
												//que usar NOCHANGE en ese sentido es incorrecto, por lo que se forzará siempre a usar las descripciones
												//recibidas simplemente
												if (!isset($_POST[$postKey.'Cat'])) {
													$_POST[$postKey.'Cat'] = array();
												}
												$_POST[$postKey.'Cat'][$intIdx] = substr($strData, 8);
												$_POST[$postKey][$intIdx] = "NOCHANGE";
												/*
												$_POST[$postKey][$intIdx] = substr($strData, 8);
												//Valida sólo para numéricas, ya que el resto de las preguntas se resuelve a String así que no hay tanto 
												//problema realmente si no contiene el formato que se esperaba
												if ($objQuestion->QTypeID == qtpOpenNumeric) {
													if (!is_numeric($_POST[$postKey][$intIdx])) {
														$_POST[$postKey][$intIdx] = '';
													}
												}
												*/
												//@JAPR
											}
										}
										//@JAPR
										
										$strData = (string) @$aRecAnswer['comment'];
										if ($strData != '') {
											$_POST[$postKeyComment][$intIdx] = $strData;
										}
										$strData = (string) @$aRecAnswer['photo'];
										if ($strData != '') {
											$_POST[$postKeyImage][$intIdx] = $strData;
										}
										$strData = (string) @$aRecAnswer['document'];
										if ($strData != '') {
											$_POST[$postKeyDocument][$intIdx] = $strData;
										}
										
										//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
										$intFactKey = (int) @$aRecAnswer['factKey'];
										//@JAPR 2013-08-13: Corregido un bug, en algún punto se optimizó el proceso para grabar preguntas simple-choice de catálogo a partir de las
										//descripciones, pero al hacerlo el FactKey enviado a la edición se manenó como un número negativo empezando en -1 para representar a todos
										//los registros de la maestro-detalle que existían durante la edición, así que en el proceso de grabado de la edición, el valor negativo del
										//FactKey estaba siendo ignorado por el proceso por lo que en caso de no haber existido una pregunta de un tipo diferente en la sección, no
										//se hubiera registrado que FactKeys fueron actualizados provocando que al final fueran eliminados. Ahora se mapearán esos FactKeys negativos
										//con el valor real en la misma posición relativa dentro de la tabla paralela para hacer correctamente la actualización de FactKeys usados
										//@JAPR 2013-08-14: Corregido un bug, esta parte sólo aplica para secciones maestro-detalle, sin embargo en este tipo
										//de preguntas puede entrar a este código tanto para ellas como para las dinámicas, así que se debe validar para
										//no afectar si se tienen ambos tipos de secciones en la encuesta
										if ($objSection->SectionType == sectMasterDet) {
											if ($intFactKey < 0 && $blnEdit && $entryID > 0) {
												//Si la pregunta es simple choice de catálogo y usa catálogos desasociados, entonces los FactKeys si
												//son válidos negativos por lo que se remapea el FactKey a actualizar basado en el array que previamente
												//se cargó con los FactKeys por sección
												$intFactKey = (int) @$arrMasterSectionFKsFromEntry[$intSectionID][$intFactKey];
											}
											//@JAPR
											if ($intFactKey > 0) {
												if (!isset($arrMasterSectionFKs[$intSectionID])) {
													$arrMasterSectionFKs[$intSectionID] = array();
												}
												$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
											}
										}
										//@JAPR 2014-05-28: Agregado el tipo de sección Inline
										elseif ($objSection->SectionType == sectInline) {
											if ($intFactKey < 0 && $blnEdit && $entryID > 0) {
												//Si la pregunta es simple choice de catálogo y usa catálogos desasociados, entonces los FactKeys si
												//son válidos negativos por lo que se remapea el FactKey a actualizar basado en el array que previamente
												//se cargó con los FactKeys por sección
												$intFactKey = (int) @$arrInlineSectionFKsFromEntry[$intSectionID][$intFactKey];
											}
											//@JAPR
											if ($intFactKey > 0) {
												if (!isset($arrInlineSectionFKs[$intSectionID])) {
													$arrInlineSectionFKs[$intSectionID] = array();
												}
												$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
											}
										}
										
										//@JAPR 2014-10-29: Agregado el mapeo de Scores
										//Agrega al array de Scores el valor por cada respuesta de la sección
										if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
											$arrQuestionScoresFromApp[$postKeyNum] = array();
										}
										$arrQuestionScoresFromApp[$postKeyNum][$intIdx] = (float) @$aRecAnswer['score'];
										//@JAPR
									}
									
									//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
									//@JAPR 2013-08-14: Corregido un bug, esta parte sólo aplica para secciones maestro-detalle, sin embargo en este tipo
									//de preguntas puede entrar a este código tanto para ellas como para las dinámicas, así que se debe validar para
									//no afectar si se tienen ambos tipos de secciones en la encuesta
									if ($objSection->SectionType == sectMasterDet) {
										if (!isset($arrMasterSectionRecs[$intSectionID])) {
											$arrMasterSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
												$arrMasterSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
									//@JAPR 2014-05-28: Agregado el tipo de sección Inline
									elseif ($objSection->SectionType == sectInline) {
										if (!isset($arrInlineSectionRecs[$intSectionID])) {
											$arrInlineSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
												$arrInlineSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
									//@JAPR
								}
								break;
							
							default:
								//Obtiene los valores del catálogo según el Key de la opción seleccionada
								$intData = (int) @$objAnswer['key'];
								
								//@JAPR 2013-04-15: Agregada validación (caso Rich) para que si la pregunta simple choice trae un key subrrogado que no
								//corresponde con los keys de la sección dinámica, entonces reutilice el primer key de la sección dinámica ya que por
								//alguna razón que no se identificó la pregunta simple choice traía un key de un valor de otra combinación diferente a la
								//dinámica aunque las descripciones que venían con dicho key si eran las correctas según la dinámica, pero esta diferencia
								//causaba que se grabaran combinaciones de tiendas diferentes con el cliente para la misma captura
								if ($dynamicSectionCatID == $objQuestion->CatalogID && !in_array($intData, $arrSectionValue) && count($arrSectionValue) > 0) {
									$intData = (int) @$arrSectionValue[0];
								}
								
								//@JAPR 2013-08-23: Agregada una validación para los casos donde el key venía como 0 por alguna razón
								//no determinada, siendo que si traía una respuesta correctamente seleccionada
								$arrCatalogFilter = @$objAnswer['catalogFilter'];
								$strValue = (string) @$objAnswer['answer'];
								if ($intData > 0 || ($intData == 0 && $strValue != '' && !is_null($arrCatalogFilter) && is_array($arrCatalogFilter) && count($arrCatalogFilter) > 0)) {
									//@JAPR 2013-05-09: Modificado para usar las descripciones al momento de grabar preguntas de catálogo y dinámicas
									//A partir de ahora intentará reconstruir el String de la simple choice a partir de las descripciones recibidas,
									//si hubiera algún problema al intentarlo, se utilizará el método original. Es seguro reconstruir con el array
									//de descripciones recibido porque el key de estas preguntas ya no se usa realmente, sino que con dicho array se
									//intentará obtener mas adelante el key precisamente, pero esos casos se validan ya durante el grabado
									$blnGetAnswerFromCatalog = true;
									
									$intMax = count($arrAttributeIDs);
									$arrCatalogFilter = @$objAnswer['catalogFilter'];
									$strValue = (string) @$objAnswer['answer'];
									if (is_null($arrCatalogFilter) || !is_array($arrCatalogFilter)) {
										$arrCatalogFilter = array();
									}
									
									//Recorre cada atributo y extrae la parte del filtro correspondiente para concatenarlo, si no encuentra
									//un atributo se considera que no es válida la respuesta y la omite
									$strData = '';
									if ($strValue != '') {
										for ($intIndex = 0, $intLastAttrib = $intMax -1; $intIndex < $intMax; $intIndex++) {
											if ($intIndex < $intLastAttrib) {
												//@JAPR 2015-08-25: Enviada la definición del catálogo desde el App
												//Corregido un bug, para las preguntas auto-complete no venían todos los atributos, así que ahora si no se encuentra alguno, lo
												//buscará en el array de todos los valores antes de determinar el valor como inválido
												if (!isset($arrCatalogFilter[$intIndex])) {
													$arrAllValues = @$garrCatalogData['q'.$objQuestion->QuestionID][0];
													if (is_null($arrAllValues)) {
														$arrAllValues = array();
													}
													if (isset($arrAllValues[$intIndex])) {
														$arrCatalogFilter[$intIndex] = $arrAllValues[$intIndex];
													}
												}
												//@JAPR
												
												if (!isset($arrCatalogFilter[$intIndex])) {
													$strData = '';
													break;
												}
												$strCatFilter = $arrCatalogFilter[$intIndex];
												$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strCatFilter;
											}
											else {
												//@JAPR 2013-08-26: Agregada una validación para grabar a partir del filtro en lugar de la respuesta,
												//esto porque en algunos móviles (caso de Pulso) la respuesta de simple choice estaba llegando como
												//un valor de hora siendo que el key y catalogFilter si venían correctamente del catálogo, así que
												//como no se logró determinar la causa de esto, se validará en el server
												if ($appVersion >= esvPulsoSaveFromCatFilter && isset($arrCatalogFilter[$intIndex])) {
													$strCatFilter = $arrCatalogFilter[$intIndex];
													$strValue = $strCatFilter;
												}
												//@JAPR
												
												//El último valor es el atributo correspondiente a la pregunta, así que se extrae de la respuesta
												$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strValue;
												//Terminó de identificar el string con descripciones, ya no es necesario usara el catálogo
												$blnGetAnswerFromCatalog = false;
												break;
											}
										}
									}
									
									//@JAPR 2015-07-07: Agregado el soporte de Formas v6 con un grabado completamente diferente
									//Se agregó la validación de la versión de la forma, ya que en V6 es completamente seguro asumir que el propio archivo .dat contendrá los valores
									//de las secciones, así que no hay necesidad de obtenerlos desde el catálogo sino que se reutilizará lo recibido como datos de captura
									if ($surveyInstance->CreationAdminVersion < esveFormsv6 && $blnGetAnswerFromCatalog) {
										//Si no se hubiera logrado construir el string de catálogo con las descripciones recibidas, se intentará
										//utilizando el catálogo como se hacía antes, lo cual llevará a grabar una descripción incorrecta si el
										//catálogo se ha reorganizado o en el peor de los casos a no grabar la pregunta si ya no existe el key
										$strData = '';
										$arrCatalogValues = BITAMCatalog::GetCatalogAttributeValues($aRepository, $objCatalog, $arrAttributeIDs, -1, $strFilter.$intData.' ');
										//Se supone que sólo puede haber un valor con este Key, así que hace un proceso de recorrido por cada nodo
										//concatenando en el String hasta encontrar el último nivel solicitado
										$arrLevelValues = $arrCatalogValues;
										$intIndex = 0;
										$intMax = count($arrAttributeIDs);
										
										while (count($arrLevelValues) > 0 && $intIndex < $intMax) {
											if (isset($arrLevelValues[0])) {
												//Este valor NO se debe decodificar porque antes del grabado todo se procesa como utf8
												$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$arrLevelValues[0]['value'];
												$arrLevelValues = $arrLevelValues[0]['children'];
											}
											
											$intIndex++;
										}
									}
									//@JAPR
									
									if ($strData != '') {
										$_POST[$postKey] = substr($strData, 8);
									}
								}
								//@JAPR 2013-02-11: Agregada la edición de datos históricos
								elseif ($surveyInstance->DissociateCatDimens && $intData < 0) {
									//Si se usan catálogos con dimensiones independientes, entonces si se puede recibir un valor negativo
									//y ser válido ya que eso significa que es el valor con el que estaba grabada la pregunta previamente,
									//por tanto se puede simplemente regenerar el string pero usando las propias descripciones que venían
									//como parte del filtro de la respuesta
									$intMax = count($arrAttributeIDs);
									$arrCatalogFilter = @$objAnswer['catalogFilter'];
									$strValue = (string) @$objAnswer['answer'];
									if (is_null($arrCatalogFilter) || !is_array($arrCatalogFilter)) {
										$arrCatalogFilter = array();
									}
									
									//Recorre cada atributo y extrae la parte del filtro correspondiente para concatenarlo, si no encuentra
									//un atributo se considera que no es válida la respuesta y la omite
									$strData = '';
									if ($strValue != '') {
										for ($intIndex = 0, $intLastAttrib = $intMax -1; $intIndex < $intMax; $intIndex++) {
											if ($intIndex < $intLastAttrib) {
												if (!isset($arrCatalogFilter[$intIndex])) {
													$strData = '';
													break;
												}
												$strCatFilter = $arrCatalogFilter[$intIndex];
												$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strCatFilter;
											}
											else {
												//El último valor es el atributo correspondiente a la pregunta, así que se extrae de la respuesta
												$strData .= '_SVElem_key_'.$arrAttributeIDs[$intIndex]['dimID'].'_SVSep_'.$strValue;
												break;
											}
										}
									}
									
									if ($strData != '') {
										//@JAPR 2013-02-13: Agregada la edición de datos históricos
										//Se optó mejor por mandar un valor dummy que al procesar la pregunta se traducirá a que no hará
										//el UPDATE, de tal forma que el valor actualmente grabdo permanezca
										//@JAPR 2013-08-20: Corregido un bug, durante la edición cuando se agregan nuevos registros, se debe
										//validar para no usar NOCHANGE sino el valor de catálogo correcto
										//Las preguntas simple choice de catálogo durante la edición, pudieran tomar el valor de un elemento
										//del catálogo con key negativo pero no necesariamente ser el mismo que tenía este registro, así
										//que usar NOCHANGE en ese sentido es incorrecto, por lo que se forzará siempre a usar las descripciones
										//recibidas simplemente
										//Como en este caso la pregunta es de una sección estándar, se utilizará el valor previo siempre y cuando
										//sea del mismo catálogo de la dinámica, ya que no puede cambiar el valor (si lo hubiera hecho, no habría
										//entrado a ese If porque $intData > 0), sin embargo sólo aplica si se graba un registro editado, porque
										//en registros nuevos se debe forzar a usar el valor del catálogo, así que se generará un parámetro
										//adicional en esos casos (que a la fecha sólo podrían ser los registros de la Maestro-detalle)
										//Analizando mejor el caso, sea o no del mismo catálogo de la dinámica, sólo podría haber un valor previamente
										//grabado por tratarse de una pregunta de sección estándar, así que en todos los casos se respetará el valor
										//previo, sólo si se requiriera grabar como nuevo registro (nuevos registros de maestro-detalle) se reemplazará
										//por la combinación para que sea buscado el ID correspondiente
										$_POST[$postKey.'Cat'] = substr($strData, 8);
										$_POST[$postKey] = "NOCHANGE";
										//$_POST[$postKey] = substr($strData, 8);
										//@JAPR
									}
								}
								//@JAPR
								
								$strData = (string) @$objAnswer['comment'];
								if ($strData != '') {
									$_POST[$postKeyComment] = $strData;
								}
								$strData = (string) @$objAnswer['photo'];
								if ($strData != '') {
									$_POST[$postKeyImage] = $strData;
								}
								$strData = (string) @$objAnswer['document'];
								if ($strData != '') {
									$_POST[$postKeyDocument] = $strData;
								}
								
								//@JAPR 2014-10-29: Agregado el mapeo de Scores
								//Agrega al array de Scores el valor por cada respuesta de la sección
								if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
									$arrQuestionScoresFromApp[$postKeyNum] = array();
								}
								$arrQuestionScoresFromApp[$postKeyNum][0] = (float) @$objAnswer['score'];
								//@JAPR
								break;
						}
					}
					else {
						//Si la pregunta no es de catálogo, entonces simplemente usa como valor la respuesta directa recibida, y puede pertenecer
						//a cualquier tipo de sección aunque en dinámicas depende de si hay o no una pregunta multiple choice la forma en que
						//se generará el parámetro $_POST
						switch ($objSection->SectionType) {
							case sectDynamic:
								//En el caso de secciones dinámicas, las preguntas Simple Choice vienen como un valor por cada "página" generada
								//aunque si existieran preguntas Multiple Choice en la sección entonces dicho valor se repite para cada índice
								//del SectionValue de la "página" a la que pertenece
								
								//Los arrays deben contener tantos elementos como SectionValue existan en esta sección
								$arrSectionValue = @$arrSectionValuesByID[$intSectionID];
								if (is_null($arrSectionValue)) {
									continue;
								}
								
								$_POST[$postKey] = array_fill(0, count($arrSectionValue), '');
								$_POST[$postKeyComment] = array_fill(0, count($arrSectionValue), '');
								$_POST[$postKeyImage] = array_fill(0, count($arrSectionValue), '');
								$arrSectionValueByKey = array_flip($arrSectionValue);
								if (is_array($objAnswer)) {
									foreach ($objAnswer as $intPageNum => $aRecAnswer) {
										//Por cada página generada debe obtener el array de los SectionValue que se deben modificar para
										//sobreescribir en todos esos índices
										//@JAPR 2013-08-14: Corregido un bug, con la optimización para grabar a partir de las descripciones de las
										//preguntas de catálogo, se afectó al grabado de las dinámicas cuando se cambia la sección durante la
										//edición, ya que en esos casos no vendrán valores dummies negativos sino que se regresan los keys subrrogados
										//reales que el App tenía para las opciones del catálogo utilizadas en la sección dinámica, por tanto no
										//se puede usar el array de keys por página dummy sino que se debe elegir usar el array real basado
										//también en los datos recibidos del App
										if ($blnEdit && $entryID > 0 && @$blnStoreDataFromApp[$intSectionID]) {
											$arrPageValues = @$arrSectionValuesByPageFromApp[$intSectionID][$intPageNum];
										}
										else {
											$arrPageValues = @$arrSectionValuesByPage[$intSectionID][$intPageNum];
										}
										//@JAPR
										if (is_null($arrPageValues)) {
											continue;
										}
										//@JAPR 2012-11-28: Corregido un bug, si la sección dinámica no tiene multiple-choice, este valor no es
										//realmente un array, por lo que se genera con el valor como único elemento para no cambiar el código que si
										//espera un array
										if (!is_array($arrPageValues)) {
											$arrPageValues = array($arrPageValues);
										}
										
										foreach ($arrPageValues as $intKey) {
											if (!isset($arrSectionValueByKey[$intKey])) {
												continue;
											}
											
											$intIdx = $arrSectionValueByKey[$intKey];
											$strData = (string) @$aRecAnswer['answer'];
											if ($strData != '') {
												$_POST[$postKey][$intIdx] = $strData;
											}
											$strData = (string) @$aRecAnswer['comment'];
											if ($strData != '') {
												$_POST[$postKeyComment][$intIdx] = $strData;
											}
											$strData = (string) @$aRecAnswer['photo'];
											if ($strData != '') {
												$_POST[$postKeyImage][$intIdx] = $strData;
											}
											$strData = (string) @$aRecAnswer['document'];
											if ($strData != '') {
												$_POST[$postKeyDocument][$intIdx] = $strData;
											}
										}
										
										//@JAPR 2014-10-29: Agregado el mapeo de Scores
										//Agrega al array de Scores el valor por cada respuesta de la sección
										if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
											$arrQuestionScoresFromApp[$postKeyNum] = array();
										}
										$arrQuestionScoresFromApp[$postKeyNum][$intPageNum] = (float) @$aRecAnswer['score'];
										//@JAPR
									}
								}
								break;
								
							case sectMasterDet:
							//@JAPR 2014-05-28: Agregado el tipo de sección Inline
							case sectInline:
								//En el caso de secciones múltiples, las preguntas Simple Choice vienen como un valor por cada "página" generada,
								//y de esa misma forma es como se deben grabar
								$_POST[$postKey] = array();
								$_POST[$postKeyComment] = array();
								$_POST[$postKeyImage] = array();
								//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
								$intIdx = null;
								//@JAPR
								
								if (is_array($objAnswer)) {
									foreach ($objAnswer as $intIdx => $aRecAnswer) {
										$strData = (string) @$aRecAnswer['answer'];
										if ($strData != '') {
											$_POST[$postKey][$intIdx] = $strData;
										}
										$strData = (string) @$aRecAnswer['comment'];
										if ($strData != '') {
											$_POST[$postKeyComment][$intIdx] = $strData;
										}
										$strData = (string) @$aRecAnswer['photo'];
										if ($strData != '') {
											$_POST[$postKeyImage][$intIdx] = $strData;
										}
										$strData = (string) @$aRecAnswer['document'];
										if ($strData != '') {
											$_POST[$postKeyDocument][$intIdx] = $strData;
										}
										
										//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
										$intFactKey = (int) @$aRecAnswer['factKey'];
										if ($intFactKey > 0) {
											//@JAPR 2014-05-28: Agregado el tipo de sección Inline
											if ($objSection->SectionType == sectInline) {
												if (!isset($arrInlineSectionFKs[$intSectionID])) {
													$arrInlineSectionFKs[$intSectionID] = array();
												}
												$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
											}
											else {
												if (!isset($arrMasterSectionFKs[$intSectionID])) {
													$arrMasterSectionFKs[$intSectionID] = array();
												}
												$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
											}
											//@JAPR
										}
										
										//@JAPR 2014-10-29: Agregado el mapeo de Scores
										//Agrega al array de Scores el valor por cada respuesta de la sección
										if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
											$arrQuestionScoresFromApp[$postKeyNum] = array();
										}
										$arrQuestionScoresFromApp[$postKeyNum][$intIdx] = (float) @$aRecAnswer['score'];
										//@JAPR
									}
									
									//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
									//@JAPR 2014-05-28: Agregado el tipo de sección Inline
									//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
									if ( !is_null($intIdx) ) {
										if ($objSection->SectionType == sectInline) {
											if (!isset($arrInlineSectionRecs[$intSectionID])) {
												$arrInlineSectionRecs[$intSectionID] = $intIdx;
											}
											else {
												if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
													$arrInlineSectionRecs[$intSectionID] = $intIdx;
												}
											}
										}
										else {
											if (!isset($arrMasterSectionRecs[$intSectionID])) {
												$arrMasterSectionRecs[$intSectionID] = $intIdx;
											}
											else {
												if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
													$arrMasterSectionRecs[$intSectionID] = $intIdx;
												}
											}
										}
									}
									//@JAPR
								}
								break;
							
							default:
								//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
								$strData = (string) @$objAnswer['answer'];
								if ($strData != '') {
									$_POST[$postKey] = $strData;
								}
								$strData = (string) @$objAnswer['comment'];
								if ($strData != '') {
									$_POST[$postKeyComment] = $strData;
								}
								$strData = (string) @$objAnswer['photo'];
								if ($strData != '') {
									$_POST[$postKeyImage] = $strData;
								}
								$strData = (string) @$objAnswer['document'];
								if ($strData != '') {
									$_POST[$postKeyDocument] = $strData;
								}
								
								//@JAPR 2014-10-29: Agregado el mapeo de Scores
								//Agrega al array de Scores el valor por cada respuesta de la sección
								if (!isset($arrQuestionScoresFromApp[$postKeyNum])) {
									$arrQuestionScoresFromApp[$postKeyNum] = array();
								}
								$arrQuestionScoresFromApp[$postKeyNum][0] = (float) @$objAnswer['score'];
								//@JAPR
								break;
						}
					}
					break;
				
				case qtpSketch:
				//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				case qtpSketchPlus:
				//@JAPR
				case qtpSignature:
				case qtpAudio:
				case qtpVideo:
				//Conchita 20140924 agregados audio y video
				case qtpDocument:
				case qtpPhoto:
				case qtpOCR:
					//La pregunta tipo foto sólo contiene como extra un comentario, puede pertenecer a cualquier tipo de sección 
					//dependiendo de lo cual puede o no venir un array (en el caso de dinámicas se maneja como si fuera una pregunta Open)
					switch ($objSection->SectionType) {
						case sectDynamic:
							//En el caso de secciones dinámicas, las preguntas Open vienen como un valor por cada "página" generada,
							//en este caso NO se repiten por los section values que existan en cada página
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intPageNum => $aRecAnswer) {
									/*
									//Por cada página generada debe obtener el array de los SectionValue que se deben modificar para
									//sobreescribir en todos esos índices
									$arrPageValues = @$arrSectionValuesByPage[$intSectionID][$intPageNum];
									if (is_null($arrPageValues)) {
										continue;
									}
									if (!is_array($arrPageValues)) {
										$arrPageValues = array($arrPageValues);
									}
									
									foreach ($arrPageValues as $intKey) {
										if (!isset($arrSectionValueByKey[$intKey])) {
											continue;
										}
										
										$intIdx = $arrSectionValueByKey[$intKey];
									*/
									$intIdx = $intPageNum;
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									//}
								}
								//@JAPR
							}
							break;
							
						case sectMasterDet:
						//@JAPR 2014-05-28: Agregado el tipo de sección Inline
						case sectInline:
							//En el caso de secciones múltiples, las preguntas Open vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar (es un array en ambos casos)
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							$_POST[$postKeyDocument] = array();
							$_POST[$postKeyDocumentType] = array();
							//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
							$intIdx = null;
							//@JAPR
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intIdx => $aRecAnswer) {
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									//@JAPR 2019-02-20: Corregido un bug, no se estaban considerando las preguntas Sketchs en secciones que no fueran estándar, así que no se procesaba
									//la imagen de lo dibujado por el usuario sino solo de la foto tomada (#6CIBFK)
									//@JAPR 2019-02-15: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
									// Si es sketch buscamos la propiedad canvasPhoto que es lo que trae del canvas que se dibujo
									if($objQuestion->QTypeID == qtpSketch || $objQuestion->QTypeID == qtpSketchPlus){
										//@JAPR 2019-02-15: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
										//Para la pregunta Sketch+ tendrá que haber 2 propiedades a grabar, la normal 'document' donde se graba la foto tomada con el trazo realizado, pero 
										//también el de la foto originalmente tomada para permitir su procesamiento posterior en caso de edición o visualización desde la bitácora de capturas,
										//para ello se agregará la propiedad 'sourceDocument' de manera que se pueda heredar y grabar en una nueva tabla exclusiva de Sketch+
										if ( $objQuestion->QTypeID == qtpSketchPlus ) {
											$strDataStored = $strData;
											if ($strDataStored != '') {
												$_POST[$postKeyImage.'Stored'][$intIdx] = $strDataStored;
											}
											
											//Adicionalmente para la pregunta Sketch+ es necesario guardar como respuesta los trazos realizados, por lo tanto asigna el valor de la respuesta
											//de la pregunta, el cual anteriormente era ignorado porque no se podía regenerar el trazo con la pregunta Sketch normal
											$strData = (string) @$aRecAnswer['answer'];
											$_POST[$postKey][$intIdx] = $strData;
										}
										//@JAPR
										$strData = (string) @$aRecAnswer['canvasPhoto'];
									}
									//@JAPR
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
									
									//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
									$intFactKey = (int) @$aRecAnswer['factKey'];
									if ($intFactKey > 0) {
										//@JAPR 2014-05-28: Agregado el tipo de sección Inline
										if ($objSection->SectionType == sectInline) {
											if (!isset($arrInlineSectionFKs[$intSectionID])) {
												$arrInlineSectionFKs[$intSectionID] = array();
											}
											$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										else {
											if (!isset($arrMasterSectionFKs[$intSectionID])) {
												$arrMasterSectionFKs[$intSectionID] = array();
											}
											$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										//@JAPR
									}
								}
								
								//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
								//@JAPR 2014-05-28: Agregado el tipo de sección Inline
								//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
								if ( !is_null($intIdx) ) {
									if ($objSection->SectionType == sectInline) {
										if (!isset($arrInlineSectionRecs[$intSectionID])) {
											$arrInlineSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
												$arrInlineSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
									else {
										if (!isset($arrMasterSectionRecs[$intSectionID])) {
											$arrMasterSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
												$arrMasterSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
								}
								//@JAPR
							}
							break;
						
						default:
							//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
							$strData = (string) @$objAnswer['comment'];
							if ($strData != '') {
								$_POST[$postKeyComment] = $strData;
							}
							$strData = (string) @$objAnswer['photo'];
							// Si es sketch buscamos la propiedad canvasPhoto que es lo que trae del canvas que se dibujo
							//@JAPR 2019-02-15: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
							if($objQuestion->QTypeID == qtpSketch || $objQuestion->QTypeID == qtpSketchPlus){
								//@JAPR 2019-02-15: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
								//Para la pregunta Sketch+ tendrá que haber 2 propiedades a grabar, la normal 'document' donde se graba la foto tomada con el trazo realizado, pero 
								//también el de la foto originalmente tomada para permitir su procesamiento posterior en caso de edición o visualización desde la bitácora de capturas,
								//para ello se agregará la propiedad 'sourceDocument' de manera que se pueda heredar y grabar en una nueva tabla exclusiva de Sketch+
								if ( $objQuestion->QTypeID == qtpSketchPlus ) {
									$strDataStored = $strData;
									if ($strDataStored != '') {
										$_POST[$postKeyImage.'Stored'] = $strDataStored;
									}
									
									//Adicionalmente para la pregunta Sketch+ es necesario guardar como respuesta los trazos realizados, por lo tanto asigna el valor de la respuesta
									//de la pregunta, el cual anteriormente era ignorado porque no se podía regenerar el trazo con la pregunta Sketch normal
									$strData = (string) @$objAnswer['answer'];
									$_POST[$postKey] = $strData;
								}
								//@JAPR
								$strData  =(string) @$objAnswer['canvasPhoto'];
							}
							if ($strData != '') {
								$_POST[$postKeyImage] = $strData;
							}
							$strData = (string) @$objAnswer['document'];
							if ($strData != '') {
								$_POST[$postKeyDocument] = $strData;
							}
							$strData = (string) @$objAnswer['documentType'];
							if ($strData != '') {
								$_POST[$postKeyDocumentType] = $strData;
							}
									
							//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
							if ( $objQuestion->QTypeID == qtpSketchPlus ) {
							}
							//@JAPR
							break;
					}
					break;
				
				//Estas preguntas siempre vienen como texto (o según el tipo), con comentario y foto opcional, y pueden pertenecer a cualquier 
				//tipo de sección, dependiendo de lo cual puede o no venir un array. NO se hacen validaciones por el tipo de dato, aunque se 
				//podrían integrar, por ahora se delega eso al App que envía las respuestas
				case qtpOpenDate:
				case qtpOpenTime:
				case qtpOpenNumeric:
				case qtpOpenString:
				case qtpOpenAlpha:
				case qtpCalc: 			//@JAPR 2013-01-14: Faltaba considerar a las preguntas calculadas
				//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				case qtpGPS:
				//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
				case qtpMyLocation:
				//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					switch ($objSection->SectionType) {
						case sectDynamic:
							//En el caso de secciones dinámicas, las preguntas Open vienen como un valor por cada "página" generada,
							//en este caso NO se repiten por los section values que existan en cada página
							$_POST[$postKey] = array();
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intPageNum => $aRecAnswer) {
									/*
									//Por cada página generada debe obtener el array de los SectionValue que se deben modificar para
									//sobreescribir en todos esos índices
									$arrPageValues = @$arrSectionValuesByPage[$intSectionID][$intPageNum];
									if (is_null($arrPageValues)) {
										continue;
									}
									if (!is_array($arrPageValues)) {
										$arrPageValues = array($arrPageValues);
									}
									
									foreach ($arrPageValues as $intKey) {
										if (!isset($arrSectionValueByKey[$intKey])) {
											continue;
										}
										
										$intIdx = $arrSectionValueByKey[$intKey];
									*/
									$intIdx = $intPageNum;
									$strData = (string) @$aRecAnswer['answer'];
									if ($strData != '') {
										//@JAPR 2013-08-26: Agregada una validación para grabar a partir del defaultValue en lugar de la respuesta para
										//preguntas que son numéricas (ya que son las únicas que realmente se puede comprobar el tipo de dato capturado),
										//esto porque en algunos móviles (caso de Pulso) la respuesta numérica estaba llegando como un valor de hora, 
										//pero el defaultValue si venía correctamente asignado, como no se logró determinar la causa de esto, se validará 
										//en el server
										if (!is_numeric($strData)) {
											$strDefData = (string) @$aRecAnswer['defaultAnswer'];
											if (is_numeric($strDefData)) {
												$strData = $strDefData;
											}
										}
										//@JAPR
										
										$_POST[$postKey][$intIdx] = $strData;
										//Valida sólo para numéricas, ya que el resto de las preguntas se resuelve a String así que no hay tanto 
										//problema realmente si no contiene el formato que se esperaba
										if ($objQuestion->QTypeID == qtpOpenNumeric) {
											if (!is_numeric($_POST[$postKey][$intIdx])) {
												$_POST[$postKey][$intIdx] = '';
											}
										}
									}
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
									//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
									$strData = trim((string) @$aRecAnswer['latitude']);
									if ($strData != '') {
										$_POST[$postKeyLat][$intIdx] = $strData;
									}
									$strData = trim((string) @$aRecAnswer['longitude']);
									if ($strData != '') {
										$_POST[$postKeyLong][$intIdx] = $strData;
									}
									$strData = trim((string) @$aRecAnswer['accuracy']);
									if ($strData != '') {
										$_POST[$postKeyAcc][$intIdx] = $strData;
									}
									//@JAPR
									//}
								}
								//@JAPR
							}
							break;
							
						case sectMasterDet:
						//@JAPR 2014-05-28: Agregado el tipo de sección Inline
						case sectInline:
							//En el caso de secciones múltiples, las preguntas Open vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar (es un array en ambos casos)
							$_POST[$postKey] = array();
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
							$intIdx = null;
							//@JAPR
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intIdx => $aRecAnswer) {
									$strData = (string) @$aRecAnswer['answer'];
									if ($strData != '') {
										//@JAPR 2013-08-26: Agregada una validación para grabar a partir del defaultValue en lugar de la respuesta para
										//preguntas que son numéricas (ya que son las únicas que realmente se puede comprobar el tipo de dato capturado),
										//esto porque en algunos móviles (caso de Pulso) la respuesta numérica estaba llegando como un valor de hora, 
										//pero el defaultValue si venía correctamente asignado, como no se logró determinar la causa de esto, se validará 
										//en el server
										if (!is_numeric($strData)) {
											$strDefData = (string) @$aRecAnswer['defaultAnswer'];
											if (is_numeric($strDefData)) {
												$strData = $strDefData;
											}
										}
										//@JAPR
										
										$_POST[$postKey][$intIdx] = $strData;
										//Valida sólo para numéricas, ya que el resto de las preguntas se resuelve a String así que no hay tanto 
										//problema realmente si no contiene el formato que se esperaba
										if ($objQuestion->QTypeID == qtpOpenNumeric) {
											if (!is_numeric($_POST[$postKey][$intIdx])) {
												$_POST[$postKey][$intIdx] = '';
											}
										}
									}
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
									
									//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
									$intFactKey = (int) @$aRecAnswer['factKey'];
									if ($intFactKey > 0) {
										//@JAPR 2014-05-28: Agregado el tipo de sección Inline
										if ($objSection->SectionType == sectInline) {
											if (!isset($arrInlineSectionFKs[$intSectionID])) {
												$arrInlineSectionFKs[$intSectionID] = array();
											}
											$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										else {
											if (!isset($arrMasterSectionFKs[$intSectionID])) {
												$arrMasterSectionFKs[$intSectionID] = array();
											}
											$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										//@JAPR
									}
									
									//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
									$strData = (string) @$aRecAnswer['latitude'];
									if ($strData != '') {
										$_POST[$postKeyLat][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['longitude'];
									if ($strData != '') {
										$_POST[$postKeyLong][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['accuracy'];
									if ($strData != '') {
										$_POST[$postKeyAcc][$intIdx] = $strData;
									}
									//@JAPR
								}
								
								//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
								//@JAPR 2014-05-28: Agregado el tipo de sección Inline
								//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
								if ( !is_null($intIdx) ) {
									if ($objSection->SectionType == sectInline) {
										if (!isset($arrInlineSectionRecs[$intSectionID])) {
											$arrInlineSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
												$arrInlineSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
									else {
										if (!isset($arrMasterSectionRecs[$intSectionID])) {
											$arrMasterSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
												$arrMasterSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
								}
								//@JAPR
							}
							break;
						
						default:
							//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
							$strData = (string) @$objAnswer['answer'];
							if ($strData != '') {
								//@JAPR 2013-08-26: Agregada una validación para grabar a partir del defaultValue en lugar de la respuesta para
								//preguntas que son numéricas (ya que son las únicas que realmente se puede comprobar el tipo de dato capturado),
								//esto porque en algunos móviles (caso de Pulso) la respuesta numérica estaba llegando como un valor de hora, 
								//pero el defaultValue si venía correctamente asignado, como no se logró determinar la causa de esto, se validará 
								//en el server
								if (!is_numeric($strData)) {
									$strDefData = (string) @$objAnswer['defaultAnswer'];
									if (is_numeric($strDefData)) {
										$strData = $strDefData;
									}
								}
								//@JAPR
								
								$_POST[$postKey] = $strData;
							}
							$strData = (string) @$objAnswer['comment'];
							if ($strData != '') {
								$_POST[$postKeyComment] = $strData;
							}
							$strData = (string) @$objAnswer['photo'];
							if ($strData != '') {
								$_POST[$postKeyImage] = $strData;
							}
							$strData = (string) @$objAnswer['document'];
							if ($strData != '') {
								$_POST[$postKeyDocument] = $strData;
							}
							//@JAPR 2014-01-28: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
							$strData = (string) @$objAnswer['latitude'];
							if ($strData != '') {
								$_POST[$postKeyLat] = $strData;
							}
							$strData = (string) @$objAnswer['longitude'];
							if ($strData != '') {
								$_POST[$postKeyLong] = $strData;
							}
							$strData = (string) @$objAnswer['accuracy'];
							if ($strData != '') {
								$_POST[$postKeyAcc] = $strData;
							}
							//@JAPR
							break;
					}
					break;
					
				case qtpAction:
					//Las preguntas tipo acción son casi idénticas a las Open alfanuméricas, sólo que contienen 3 valores adicionales a grabar
					//que son un Responsable, Deadline y una Categoría, así que la "Respuesta" en si es una combinación de estos 4 datos
					//en el orden: ResponsibleID_SVSep_DeadLine_SVSep_CategoryID_SVSep_ActionText
					switch ($objSection->SectionType) {
						case sectDynamic:
							//En el caso de secciones dinámicas, las preguntas Open vienen como un valor por cada "página" generada,
							//en este caso NO se repiten por los section values que existan en cada página
							$_POST[$postKey] = array();
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intPageNum => $aRecAnswer) {
									/*
									//Por cada página generada debe obtener el array de los SectionValue que se deben modificar para
									//sobreescribir en todos esos índices
									$arrPageValues = @$arrSectionValuesByPage[$intSectionID][$intPageNum];
									if (is_null($arrPageValues)) {
										continue;
									}
									if (!is_array($arrPageValues)) {
										$arrPageValues = array($arrPageValues);
									}
									
									foreach ($arrPageValues as $intKey) {
										if (!isset($arrSectionValueByKey[$intKey])) {
											continue;
										}
										
										$intIdx = $arrSectionValueByKey[$intKey];
									*/
									$intIdx = $intPageNum;
									$strActionData = getActionDataFromAnswer($aRecAnswer, $objQuestion, $thisDate);
									
									if ($strActionData != '') {
										$_POST[$postKey][$intIdx] = $strActionData;
									}
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
								}
							}
							break;
							
						case sectMasterDet:
						//@JAPR 2014-05-28: Agregado el tipo de sección Inline
						case sectInline:
							//En el caso de secciones múltiples, las preguntas Open vienen como un valor por cada "página" generada,
							//y de esa misma forma es como se deben grabar (es un array en ambos casos)
							$_POST[$postKey] = array();
							$_POST[$postKeyComment] = array();
							$_POST[$postKeyImage] = array();
							//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
							$intIdx = null;
							//@JAPR
							
							if (is_array($objAnswer)) {
								foreach ($objAnswer as $intIdx => $aRecAnswer) {
									$strActionData = getActionDataFromAnswer($aRecAnswer, $objQuestion, $thisDate);
									
									if ($strActionData != '') {
										$_POST[$postKey][$intIdx] = $strActionData;
									}
									$strData = (string) @$aRecAnswer['comment'];
									if ($strData != '') {
										$_POST[$postKeyComment][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['photo'];
									if ($strData != '') {
										$_POST[$postKeyImage][$intIdx] = $strData;
									}
									$strData = (string) @$aRecAnswer['document'];
									if ($strData != '') {
										$_POST[$postKeyDocument][$intIdx] = $strData;
									}
									
									//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
									$intFactKey = (int) @$aRecAnswer['factKey'];
									if ($intFactKey > 0) {
										//@JAPR 2014-05-28: Agregado el tipo de sección Inline
										if ($objSection->SectionType == sectInline) {
											if (!isset($arrInlineSectionFKs[$intSectionID])) {
												$arrInlineSectionFKs[$intSectionID] = array();
											}
											$arrInlineSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										else {
											if (!isset($arrMasterSectionFKs[$intSectionID])) {
												$arrMasterSectionFKs[$intSectionID] = array();
											}
											$arrMasterSectionFKs[$intSectionID][$intIdx] = $intFactKey;
										}
										//@JAPR
									}
								}
								
								//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
								//@JAPR 2014-05-28: Agregado el tipo de sección Inline
								//@JAPR 2019-05-31: Corrección, este índice no siempre se estaba asignando y generaba errores (#1LJPF2)
								if ( !is_null($intIdx) ) {
									if ($objSection->SectionType == sectInline) {
										if (!isset($arrInlineSectionRecs[$intSectionID])) {
											$arrInlineSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrInlineSectionRecs[$intSectionID] < $intIdx) {
												$arrInlineSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
									else {
										if (!isset($arrMasterSectionRecs[$intSectionID])) {
											$arrMasterSectionRecs[$intSectionID] = $intIdx;
										}
										else {
											if ($arrMasterSectionRecs[$intSectionID] < $intIdx) {
												$arrMasterSectionRecs[$intSectionID] = $intIdx;
											}
										}
									}
								}
								//@JAPR
							}
							break;
						
						default:
							$strActionData = getActionDataFromAnswer($objAnswer, $objQuestion, $thisDate);
							
							//En cualquier otro tipo de sección simplemente viene una respuesta por encuesta
							if ($strActionData != '') {
								$_POST[$postKey] = $strActionData;
							}
							$strData = (string) @$objAnswer['comment'];
							if ($strData != '') {
								$_POST[$postKeyComment] = $strData;
							}
							$strData = (string) @$objAnswer['photo'];
							if ($strData != '') {
								$_POST[$postKeyImage] = $strData;
							}
							$strData = (string) @$objAnswer['document'];
							if ($strData != '') {
								$_POST[$postKeyDocument] = $strData;
							}
							break;
					}
					break;
					
				default:
					//Cualquier nuevo tipo de pregunta será ignorado pues no se sabe como grabarlo
					break;
			}
		}
	}
	
	//Conchita * checar si tiene firma
	if($surveyInstance->HasSignature==1)
	{
		$postKey = "qsignature";
		$_POST[$postKey] =  @$jsonData['signature'];
	}
	
	//@JAPR 2012-10-22: Corregido un bug, faltaba el parámetro con el total de registros de la sección Maestro-detalle
	//Agrega el parámetro que indica la cantidad de registros de las secciones Maestro-Detalle
	foreach ($arrMasterSectionRecs as $intSectionID => $intTotalRecords) {
		$postKey = "masterSectionRecs".$intSectionID;
		$_POST[$postKey] = $intTotalRecords +1;
	}
	
	//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
	foreach ($arrMasterSectionFKs as $intSectionID => $arrFKs) {
		$postKey = "masterSectionFKs".$intSectionID;
		$_POST[$postKey] = $arrFKs;
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		foreach ($arrFKs as $intFactKey) {
			$arrEditedFactKeys[$intFactKey] = $intFactKey;
		}
	}
	
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	//Agrega el parámetro que indica la cantidad de registros de las secciones Inline
	foreach ($arrInlineSectionRecs as $intSectionID => $intTotalRecords) {
		$postKey = "inlineSectionRecs".$intSectionID;
		$_POST[$postKey] = $intTotalRecords +1;
	}
	
	//@JAPR 2012-11-08: Agregada la edición de capturas de encuestas
	foreach ($arrInlineSectionFKs as $intSectionID => $arrFKs) {
		$postKey = "inlineSectionFKs".$intSectionID;
		$_POST[$postKey] = $arrFKs;
		
		//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
		foreach ($arrFKs as $intFactKey) {
			$arrEditedFactKeys[$intFactKey] = $intFactKey;
		}
	}
	//@JAPR
	
	//Asigna el valor del RegisterID si se trata de una edición
	//@JAPR 2013-05-20: Corregido un bug, puede venir el parámetro de edición en capturas vía Web, pero si no hay entryID entonces realmente
	//no es una edición
	if ($blnEdit && $entryID > 0) {
		foreach ($arrDynamicSectionFKs as $intSectionID => $arrFKs) {
			$_POST['registerID'] = $arrDynamicSectionFKs[$intSectionID];
			
			//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
			foreach ($arrFKs as $intFactKey) {
				$arrEditedFactKeys[$intFactKey] = $intFactKey;
			}
			//A la fecha de este desarrollo, sólo podía haber una sección dinámica, así que sale luego de asignar la primera
			break;
		}
	}
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		//@JAPR 2014-10-29: Agregado el mapeo de Scores
		echo ("<br>\r\narrQuestionScoresFromApp:");
		PrintMultiArray($arrQuestionScoresFromApp);
		//@JAPR
		echo ("<br>\r\n_POST:");
		PrintMultiArray($_POST);
	}
	
	if (getParamValue('DebugAbort', 'both', '(int)', true)) {
		echo ("<br>\r\nAborting process");
		die();
	}
	
	//Inicia el grabado de la captura de encuesta, ya sea vía la edición o grabado como nueva captura
	//@JAPR 2013-05-20: Corregido un bug, puede venir el parámetro de edición en capturas vía Web, pero si no hay entryID entonces realmente
	//no es una edición
	if ($blnEdit && $entryID > 0) {
		require('processSurvey.php');
	}
	else {
		//@JAPR 2015-07-08 Agregado el soporte de Formas v6 con un grabado completamente diferente
		if ($surveyInstance->CreationAdminVersion >= esveFormsv6) {
			//@JAPR 2015-08-27: Validado que si la ruta no existe entonces la genere para que no marque errores el proceso de grabado
			if (!file_exists(getcwd()."\\"."uploaderror")) {
				@mkdir(getcwd()."\\"."uploaderror");
			}
			
			//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
			//En caso de estar grabando por una pregunta Update Data, no se invoca al proceso de grabado normal sino a la función
			//que solo ejecutará el destino de datos recibido
			if ($intDataDestinationID) {
				//Reenviado el parámetro $theUser para no tener que cargarlo dentro de la función
				$return = saveDataDestinationV6($aRepository, $aUser, $intDataDestinationID);
			}
			else {
				saveAnswersMobileV6();
			}
			//@JAPR
		}
		else {
			saveAnswersMobile2();
		}
		//@JAPR
	}
	
	//@JAPR 2013-11-04: Agregado el grabado incremental de agregaciones
	//Al terminar de procesar la encuesta correctamente, se procede a actualizar las agregaciones en forma incremental
	if ($gbEnableIncrementalAggregations && !$gbTestIncrementalAggregationsData) {
		//@JAPR 2013-11-11: Agregado el grabado incremental de agregaciones
		//@JAPR 2013-11-19: Modificado para forzar a usar agregaciones incrementales incluso durante la edición
		if (count($arrDimDescValues) > 0) {
			//@JAPR 2013-11-19: Modificado para forzar a usar agregaciones incrementales incluso durante la edición
			if ($blnEdit && $entryID > 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\nBefore Incremental aggregations (Edition)");
				}
			}
			else {
				//Si es un grabado de una nueva captura, por lo menos debería tener algún registro insertado antes de recalcular los agregados
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\nBefore Incremental aggregations");
				}
			}
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo ("<br>\r\n<br>\r\nIncremental aggregations data arrays:");
				echo ("<br>\r\narrDateValues:");
				PrintMultiArray($arrDateValues);
				echo ("<br>\r\narrDimKeyValues: ".@count($arrDimKeyValues));
				PrintMultiArray($arrDimKeyValues);
				echo ("<br>\r\narrDimDescValues: ".@count($arrDimDescValues));
				PrintMultiArray($arrDimDescValues);
				echo ("<br>\r\narrIndicValues: ".@count($arrIndicValues));
				PrintMultiArray($arrIndicValues);
				echo ("<br>\r\narrAllDimensions: ".@count($arrAllDimensions));
				PrintMultiArray($arrAllDimensions);
				echo ("<br>\r\narrIndicFields: ".@count($arrIndicFields));
				PrintMultiArray($arrIndicFields);
				echo ("<br>\r\nstrIndicFieldsArrayCode: ".$strIndicFieldsArrayCode);
			}
			
			//Ahora se utilizará directamente el cinfocube.inc.php del Model Manager para poder utilizar los métodos de agregaciones
			//@require_once("cinfocube.inc.php");
			require_once("../model_manager/cinfocube.inc.php");
			//@JAPR
			$theCube = @BITAMCInfoCube::NewInstance($aRepository, $surveyInstance->ModelID, false);
			if (!is_null($theCube)) {
				$strErrDesc = '';
				$aResult = @$theCube->IncrementalAgregations($arrDateValues, $arrDimKeyValues, $arrDimDescValues, $arrIndicValues, $arrAllDimensions, $arrIndicFields, $strErrDesc);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo ("<br>\r\n<br>\r\nIncremental aggregations result: ".$aResult.', Error: '.$strErrDesc);
				}
			}
		}
	}
	if ($gbEnableIncrementalAggregations) {
		//Termina la ejecución en este punto, ya que originalmente lo debía hacer en los métodos de grabado
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo ("<br>\r\n<br>\r\nFinishing saving with Incremental aggregations");
		}
		//@JAPR 2013-11-21: Corregido un bug, no debe terminar el proceso en este punto porque de lo contrario ya no se recibe la notificación
		//de que todo terminó Ok en el App, y termina generando un error, sólo si estuviera activado el agente hubiera funcionado porque se sale
		//antes de llegar a este punto pero con el Status correcto
		//exit();
	}
	
	//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
	return $return;
	//@JAPR
}

//Dada una pregunta y una fecha así como una instancia de respuesta a una pregunta tipo Acción, regresa el String de la respuesta de la acción
//como se debe enviar a SaveAnswersMobile2 (se podría integrar como parte de BITAMQuestion)
function getActionDataFromAnswer($aRecAnswer, $objQuestion = null, $thisDate = null) {
	global $appVersion;
	
	if (is_null($aRecAnswer) || is_null($objQuestion) || is_null($thisDate)) {
		return '';
	}
	
	if ((string) @$aRecAnswer['answer'] == '') {
		return '';
	}
	
	$strActionData = '';
	//@JAPR 2013-07-15: Agregado el mapeo de Supervisores y MySelf como responsables de acciones desde el servicio
	$blnUseDefaultResponsible = false;
	if (isset($aRecAnswer['responsibleID'])) {
		if ($appVersion >= esveBavelSuppActions123) {
			//A partir de esta versión es posible recibir valores negativos o incluso el 0, sólo el -1 es un vació o -255 un inválido
			$intResponsibleID = @trim($aRecAnswer['responsibleID']);
			if ($intResponsibleID > 0) {
				$strActionData .= $intResponsibleID;
			}
			elseif ($intResponsibleID != -1 && $intResponsibleID >= -4) {
				//En este caso es 0 o los negativos soportados hasta la fecha que van desde -4 hasta -2 para los 3 Supervisores
				$strActionData .= $intResponsibleID;
			}
			else {
				//Cualquier otro valor no se conocía en esta versión
				$blnUseDefaultResponsible = true;
			}
		}
		else {
			//En versiones anteriores no se podían enviar valores <= 0 como responsables, así que sólo son válidos los enteros positivos
			if (((int) trim($aRecAnswer['responsibleID'])) > 0) {
				//Es un responsable directamente asignado
				$strActionData .= $aRecAnswer['responsibleID'];
			}
			else {
				$blnUseDefaultResponsible = true;
			}
		}
	}
	//@JAPR 2013-08-16: Corregido un bug, cuando se configura el responsable de la acción desde el server, no viene asignada la propiedad por lo
	//que debe entrar al IF de abajo y no lo estaba haciendo
	else {
		$blnUseDefaultResponsible = true;
	}
	
	if ($blnUseDefaultResponsible) {
		//Si no se especificó el responsable entonces se debe usar el default de la pregunta
		if ($appVersion >= esveBavelSuppActions123) {
			$intResponsibleID = (int) @$objQuestion->ResponsibleID;
			if ($intResponsibleID > 0) {
				$strActionData .= $intResponsibleID;
			}
			elseif ($intResponsibleID != -1 && $intResponsibleID >= -4) {
				//En este caso es 0 o los negativos soportados hasta la fecha que van desde -4 hasta -2 para los 3 Supervisores
				$strActionData .= $intResponsibleID;
			}
		}
		else {
			//En versiones anteriores no se podían enviar valores <= 0 como responsables, así que sólo son válidos los enteros positivos
			if ((int) $objQuestion->ResponsibleID > 0) {
				$strActionData .= $objQuestion->ResponsibleID;
			}
		}
	}
	//@JAPR
	$strActionData .= "_SVSep_";
	
	if (isset($aRecAnswer['daysDueDate']) && trim($aRecAnswer['daysDueDate']) != '') {
		$strActionData .= $aRecAnswer['daysDueDate'];
	}
	else {
		//Si no se especificó el deadline entonces se debe usar el default de la pregunta
		if ((int) $objQuestion->DaysDueDate != 0) {
			$strActionData .= days_add($thisDate, abs((int) $objQuestion->DaysDueDate));
		}
	}
	$strActionData .= "_SVSep_";
	
	if (isset($aRecAnswer['categoryID']) && ((int) trim($aRecAnswer['categoryID'])) > 0) {
		$strActionData .= $aRecAnswer['categoryID'];
	}
	else {
		//Si no se especificó el responsable entonces se debe usar el default de la pregunta
		if ((int) $objQuestion->CategoryID > 0) {
			$strActionData .= $objQuestion->CategoryID;
		}
	}
	$strActionData .= "_SVSep_";
	$strActionData .= (string) @$aRecAnswer['answer'];
	
	return $strActionData;
}

//@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
function connectToKPIRepository() {
	require_once('cubeClasses.php');
	global $blnWebMode;
	global $appVersion;
	
	@require_once("bpaemailconfiguration.inc.php");
	
	$aConfiguration = @BPAEmailConfiguration::readConfiguration();
	if (is_null($aConfiguration)) {
		return "Unable to read eForms configuration file";
	}
	
	ob_start();
	//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
	//$pass = BITAMDecryptPassword($aConfiguration->fbm_db_pwd);
	$pass = $aConfiguration->fbm_db_pwd;
	//@JAPR
	$aBITAMConnection = BITAMConnection::NewInstance($aConfiguration->fbm_db_server, $aConfiguration->fbm_db_user, $pass, "fbm000", 'mysql');
	if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
		$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
		ob_end_clean();
		return "Unable to connect to '".@$aConfiguration->fbm_db_server."': {$strErrorMsg}";
	}
	
	//@JAPR 2015-12-22: Corregido un bug, no se estaba cerrando el buffer de salida al terminar la función, sólo cuando marcaba error (en este caso no se
	//requiere obtener el texto que pudiese haberse generado)
	ob_end_flush();
	//@JAPR
	
	return $aBITAMConnection;
}

/* Agrega una nueva tarea de carga de datos de una encuesta en la base de datos FBM000 configurada para este servicio de eForms que está 
procesando la petición. Al momento de llegar a esta función ya debe estar grabado el archivo de Outbox y debió haber incluido el archivo
conns.inc.php en algún punto previo para tener acceso a los datos de conexión
//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
Agregado el parámetro $aSurveyTask para agregar la nueva tarea a partir de la definición de esta tarea específica recibida como parámetro
*/
function addEFormsAgentTask($sFileName, $sRepository, $aSurveyTask = null) {
	require_once('cubeClasses.php');
	/*
	if (!isset($server))
	{
		global $server;
		global $server_user;
		global $server_pwd;
		global $masterdbname;
	}
	*/
	global $blnWebMode;
	global $appVersion;
	//@JAPR
	
	/*
	//require_once('../../../fbm/conns.inc.php');
	@require_once("bpaemailconfiguration.inc.php");
	
	$aConfiguration = @BPAEmailConfiguration::readConfiguration();
	if (is_null($aConfiguration)) {
		return "Unable to read eForms configuration file";
	}
	
	ob_start();
	//@JAPR 2013-11-06: Modificado para reutilizar los archivos del Model Manager, los cuales no requieren el password desencriptado
	//$pass = BITAMDecryptPassword($aConfiguration->fbm_db_pwd);
	$pass = $aConfiguration->fbm_db_pwd;
	//@JAPR
	$aBITAMConnection = BITAMConnection::NewInstance($aConfiguration->fbm_db_server, $aConfiguration->fbm_db_user, $pass, "fbm000", 'mysql');
	//echo("server: {$server}, server_user: {$server_user}, server_pwd: {$server_pwd}, masterdbname: {$masterdbname}");
	if (is_null($aBITAMConnection) || is_null($aBITAMConnection->ADOConnection)) {
		$strErrorMsg = str_ireplace("\r\n\r\n", "\r\n", str_ireplace('<br>', "<br>\r\n", ob_get_contents()));
		ob_end_clean();
		return "Unable to connect to '".@$aConfiguration->fbm_db_server."': {$strErrorMsg}";
	}
	*/
	$aBITAMConnection = connectToKPIRepository();
	if (!is_object($aBITAMConnection)) {
		if (is_string($aBITAMConnection)) {
			return $aBITAMConnection;
		}
		else {
			return "Unknown connection error";
		}
	}
	
	$sqlCREATE = "CREATE TABLE SI_SV_SurveyTasks ( 
			SurveyTaskID INTEGER NOT NULL AUTO_INCREMENT, 
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			SurveyID INTEGER NOT NULL, 
			AgendaID INTEGER NOT NULL,
			UserID VARCHAR(255) NOT NULL, 
			WebMode INTEGER NULL, 
			Latitude DOUBLE NULL, 
			Longitude DOUBLE NULL, 
			SurveyDate DATETIME NULL, 
			TaskStatus INTEGER NULL, 
			TaskStartDate DATETIME NULL, 
			FileName VARCHAR(255) NULL, 
			FormsPath VARCHAR(255) NULL, 
			FormsVersion VARCHAR(50) NULL, 
			AgentID VARCHAR(50) NULL, 
			Executing INT NULL, 
			UserAgent TEXT NULL,
			SourceTaskID INTEGER NULL,
			SavingTries INTEGER NULL,
			SurveyKey INTEGER NULL,
			PRIMARY KEY (SurveyTaskID) 
		) AUTO_INCREMENT=1";
	
	$sqlCREATELog = "CREATE TABLE SI_SV_SurveyTasksLog ( 
			SurveyTaskLogID INTEGER NOT NULL AUTO_INCREMENT, 
			SurveyTaskID INTEGER NOT NULL, 
			CreationDateID DATETIME NOT NULL, 
			Repository VARCHAR(50) NOT NULL, 
			SurveyID INTEGER NOT NULL, 
			AgendaID INTEGER NOT NULL,
			UserID VARCHAR(255) NOT NULL, 
			WebMode INTEGER NULL, 
			Latitude DOUBLE NULL, 
			Longitude DOUBLE NULL, 
			SurveyDate DATETIME NULL, 
			TaskStatus INTEGER NULL, 
			TaskStartDate DATETIME NULL, 
			FileName VARCHAR(255) NULL, 
			FormsPath VARCHAR(255) NULL, 
			FormsVersion VARCHAR(50) NULL, 
			AgentID VARCHAR(50) NULL, 
			LogFileName VARCHAR(255) NULL, 
			UserAgent TEXT NULL,
			SourceTaskID INTEGER NULL,
			SavingTries INTEGER NULL,
			SurveyKey INTEGER NULL,
			PRIMARY KEY (SurveyTaskLogID) 
		) AUTO_INCREMENT=1";
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	//Una vez con la conexión, lo primero que hace es crear las tablas de tareas por si no se encontraban
	/*if (!$aBITAMConnection->ADOConnection->Execute($sqlCREATE)) {
		//No debe reportar este error
		//return "Error creating survey task table: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sqlCREATE}";
	}
	if (!$aBITAMConnection->ADOConnection->Execute($sqlCREATELog)) {
		//No debe reportar este error
		//return "Error creating survey task log table: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sqlCREATELog}";
	}*/
	
	//@JAPR 2018-02-01: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms
	/*//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD UserAgent TEXT NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD UserAgent TEXT NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SourceTaskID INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SourceTaskID INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SavingTries INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SavingTries INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	//@JAPR
	//@AAL 03/7/2015 Agregada la columna SessionID para EformsV6 y siguientes. Sirve para agrupar y obtener la diferencia entre el Max(creationtionDateID) y Min(creationtionDateID)
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SessionID VARCHAR(60) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SessionID VARCHAR(60) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DeviceName VARCHAR(60) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DeviceName VARCHAR(60) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD UDID VARCHAR(100) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD UDID VARCHAR(100) NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD Accuracy INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD Accuracy INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DeviceID INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DeviceID INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	//@JAPR
	
	//Campo para guardar el id del destino que falló
	$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD DestinationID INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	//Campo para guardar el status de procesamiento
	//Cuando el destino que falló no se ha reprocesado el campo ProcessStatus tendrá valor null
	//Cuando el destino que falló ya se mandó a reprocesar el campo ProcessStatus tendrá 1, independientemente si el
	//resultado fue exitoso o no
	$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD ProcessStatus INTEGER NULL";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	//DestinationIDs en SI_SV_SurveyTasks se utiliza para guardar cuales destinos serán procesados,
	//esto es en el caso en que la tarea haya sido agregada a la tabla SI_SV_SurveyTasks debido a que desde la pantalla de destinos
	//se seleccinaron destinos fallidos para reprocesarse, de esta manera sólo se procesarán los destinos especificados en el campo DestinationIDs 
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD DestinationIDs TEXT";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}

	//Reprocess en SI_SV_SurveyTasks tendrá el valor de 1 cuando la tarea haya sido agregaga a la tabla debido al reproceso manual de destinos
	//desde la pantalla de Bítacora de destinos
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD Reprocess INTEGER";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	
	//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos
	//SurveyKey en SI_SV_SurveyTasks contiene el valor del Id de la captura, mientras que SurveyId contiene el valor del Id de la forma, este nuevo campo
	//SurveyKey es aplicable y servirá como join entre la tabla de tareas, log de tareas y log de destinos de datos
	$sql = "ALTER TABLE SI_SV_SurveyTasks ADD SurveyKey INTEGER NULL";
	$aBITAMConnection->ADOConnection->Execute($sql);
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD SurveyKey INTEGER NULL";
	$aBITAMConnection->ADOConnection->Execute($sql);
	$sql = "ALTER TABLE SI_SV_SurveyDestinationLog ADD SurveyKey INTEGER NULL";
	$aBITAMConnection->ADOConnection->Execute($sql);
	//@JAPR
	
	//DestinationIDs en SI_SV_SurveyTasks se utiliza para guardar cuales destinos serán procesados,
	//esto es en el caso en que la tarea haya sido agregada a la tabla SI_SV_SurveyTasks debido a que desde la pantalla de destinos
	//se seleccinaron destinos fallidos para reprocesarse, de esta manera sólo se procesarán los destinos especificados en el campo DestinationIDs 
	//DestinationIDs  también se graba en SI_SV_SurveyTasksLog
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD DestinationIDs TEXT";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}

	//Reprocess en SI_SV_SurveyTasks tendrá el valor de 1 cuando la tarea haya sido agregaga a la tabla debido al reproceso manual de destinos
	//desde la pantalla de Bítacora de destinos, este valor también de graba en SI_SV_SurveyTasksLog
	$sql = "ALTER TABLE SI_SV_SurveyTasksLog ADD Reprocess INTEGER";
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
	}
	*/
	//@JAPR

	$kpiUser = $aBITAMConnection->ADOConnection->Quote(getParamValue('UserID', 'both', "(string)"));
	$kpiPass = getParamValue('Password', 'both', "(string)");
    $surveyID = getParamValue('surveyID', 'both', "(int)");
    $agendaID = getParamValue('agendaID', 'both', "(int)");
	$surveyDate = getParamValue('surveyDate', 'both', "(string)");
	$surveyHour = getParamValue('surveyHour', 'both', "(string)");
	$strFileName = getParamValue('filename', 'both', "(string)", true);
	$SessionID = getParamValue('sessionID', 'both', "(string)");
	//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	//Cuando se está creando la tarea desde una sincronización aún no se puede determinar el id de la captura, así que se usará -1 en ese caso
	$surveyKey = -1;
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	$strDeviceName = $aBITAMConnection->ADOConnection->Quote(getParamValue('deviceName', 'both', "(string)"));
	$strUDID = $aBITAMConnection->ADOConnection->Quote(getParamValue('uuid', 'both', "(string)"));
	$dblSyncLat = getParamValue('syncLat', 'both', "(float)", true);
	if (is_null($dblSyncLat) || !is_numeric($dblSyncLat)) {
		$dblSyncLat = 0;
	}
	$dblSyncLong = getParamValue('syncLong', 'both', "(float)", true);
	if (is_null($dblSyncLong) || !is_numeric($dblSyncLong)) {
		$dblSyncLong = 0;
	}
	//@JAPR 2015-07-30: Corregido un bug, no estaba enviando el valor del Accuracy para el grabado en las tablas de datos (#TS6KG1)
	$dblSyncAcc = getParamValue('syncAcc', 'both', "(float)", true);
	if (is_null($dblSyncAcc) || !is_numeric($dblSyncAcc)) {
		$dblSyncAcc = 0;
	}
	//@AAL Agregado para agrupar y obtener la diferencia entre el Max(creationtionDateID) y Min(creationtionDateID)
	$SessionID = (is_null($SessionID)) ? "" : $aBITAMConnection->ADOConnection->Quote($SessionID);
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	//Si no viene el parámetro deviceID, querría decir que no pasó por el Agente así que debe venir el parámetro platform y por ende se procesa desde ese (si tampcoo viniera dicho
	//parámetro entonces sería un error, pues se trataría de una versión muy vieja del App). Este dato se obtiene en la funciòn identifyDeviceType
	//$intDeviceID = getParamValue('deviceID', 'both', '(int)', true);
	$intDeviceID = identifyDeviceType();
	//@JAPR
	
	$currDate = $aBITAMConnection->ADOConnection->DBTimeStamp(date('Y-m-d H:i:s'));
	$sRepository = $aBITAMConnection->ADOConnection->Quote($sRepository);
	$surveyDate = $aBITAMConnection->ADOConnection->DBTimeStamp($surveyDate.' '.$surveyHour);
	//$surveyHour = $aBITAMConnection->ADOConnection->DBTimeStamp($surveyHour);
	$strEmpty = $aBITAMConnection->ADOConnection->Quote('');
	$strFileName = $aBITAMConnection->ADOConnection->Quote($sFileName);
	$streFormsService = (((string) @$_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/";
	$strFormsPath = $aBITAMConnection->ADOConnection->Quote($streFormsService);
	$strAppVersion = $aBITAMConnection->ADOConnection->Quote($appVersion);
	//@JAPR 2013-11-22: Agregada la información del dispositivo que realiza la petición
	$nav = @(isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
	$nav = $aBITAMConnection->ADOConnection->Quote($nav);
	
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
	$intTaskStatus = stsPending;
	$strAdditionalFields = '';
	$strAdditionalValues = '';
	$blnRescheduleTask = false;
	$blnIsWebMode = $blnWebMode;
	$intSavingTries = 0;
	$intExecuting = 0;
	$strTaskStartDate = "NULL";
	if (!is_null($aSurveyTask)) {
		$blnRescheduleTask = true;
		$intSavingTries = $aSurveyTask->SavingTries;
		$surveyID = $aSurveyTask->SurveyID;
		$agendaID = (int) @$aSurveyTask->AgendaID;
		$blnIsWebMode = $aSurveyTask->WebMode;
		$surveyDate = $aBITAMConnection->ADOConnection->DBTimeStamp($aSurveyTask->SurveyDate);
		$dblSyncLat = $aSurveyTask->SyncLatitude;
		$dblSyncLong = $aSurveyTask->SyncLongitude;
		//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
		$dblSyncAcc = (int) @$aSurveyTask->SyncAccuracy;
		//@JAPR
		$strFormsPath = $aBITAMConnection->ADOConnection->Quote($aSurveyTask->FormsPath);
		$strAppVersion = $aBITAMConnection->ADOConnection->Quote($aSurveyTask->FormsVersion);
		$kpiUser = $aBITAMConnection->ADOConnection->Quote($aSurveyTask->User);
		$nav = $aBITAMConnection->ADOConnection->Quote($aSurveyTask->UserAgent);
		if ($aSurveyTask->TaskStartDate != '') {
			$strTaskStartDate = $aBITAMConnection->ADOConnection->DBTimeStamp($aSurveyTask->TaskStartDate);
		}
		$intExecuting = $aSurveyTask->Executing;
		
		$strAdditionalFields .= ", SourceTaskID, SavingTries";
		$strAdditionalValues .= ", {$aSurveyTask->SurveyTaskID}, {$intSavingTries}";
		$intTaskStatus = stsRescheduled;
		//@JAPR 2015-09-07: Corregido un bug, faltaba heredar el SessionID durante el proceso de Reschedule
		$SessionID = $aSurveyTask->SessionID;
		//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
		//Cuando se está reprocesando un destino de datos, la tarea ya existe y hay un registro asociado, por tanto se reutiliza
		$surveyKey = $aSurveyTask->SurveyKey;
		//@JAPR
		
		//Agregados los campos:
		//	- DestinationIDs que guarda los IDs de destinos seleccionados para reprocesar,
		//	de manera que cuando se ejecute la tarea sólo reprocesará los destinos especificados
		//	- Reprocess vale 1 si la tarea fue programada debido a un reproceso ejecutado por el usuario desde la pantalla de destinos
		if(count($aSurveyTask->DestinationIDs)>0)
		{
			$strDestinationIDs = $aBITAMConnection->ADOConnection->Quote(implode(",", $aSurveyTask->DestinationIDs));
		}
		else
		{
			$strDestinationIDs = $aBITAMConnection->ADOConnection->Quote("");
		}
		$strAdditionalFields .= ", DestinationIDs, Reprocess";
		$strAdditionalValues .= ", {$strDestinationIDs}, {$aSurveyTask->Reprocess}";
	}
	//@JAPR 2015-07-14: Agregado el ID de sesión de sincronización, el nombre del dispositivo y el udid para identificar el dispositivo utilizado
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	//@JAPR 2015-08-14: Agregado el DeviceID (obtenido del parámetro platform) para identificar fácilmente el dispositivo que sincronizó, ya que el UserAgent es demasiado grande
	$strAdditionalFields .= ", SessionID, DeviceName, UDID, Accuracy, DeviceID";
	$strAdditionalValues .= ", {$SessionID}, {$strDeviceName}, {$strUDID}, {$dblSyncAcc}, {$intDeviceID}";
	//@JAPR 2018-02-01: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	$strAdditionalFields .= ", SurveyKey";
	$strAdditionalValues .= ", {$surveyKey}";
	//@JAPR
	
	//Intenta insertar la tarea con la definición actualmente procesada
	//@JAPR 2015-02-03: Agregado el botón para recalendarizar las tareas del agente que hubieran fallado
	$intSurveyTaskID = 0;
	$sql = "INSERT INTO SI_SV_SurveyTasks (CreationDateID, Repository, SurveyID, AgendaID, UserID, WebMode, Latitude, Longitude, 
			SurveyDate, TaskStatus, TaskStartDate, FileName, FormsPath, FormsVersion, AgentID, Executing, UserAgent {$strAdditionalFields}) 
		VALUES ({$currDate}, {$sRepository}, {$surveyID}, {$agendaID}, {$kpiUser}, ".((int) @$blnIsWebMode).", {$dblSyncLat}, {$dblSyncLong}, 
			{$surveyDate}, {$intTaskStatus}, {$strTaskStartDate}, {$strFileName}, {$strFormsPath}, {$strAppVersion}, {$strEmpty}, {$intExecuting}, {$nav} {$strAdditionalValues})";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n sql: {$sql}");
	}
	if (!$aBITAMConnection->ADOConnection->Execute($sql)) {
		return "Error adding a new eForms agent task for this entry: ".@$aBITAMConnection->ADOConnection->ErrorMsg().", query: {$sql}";
	}
	else {
		//En este caso se logró insertar la tarea, se obtiene el ID agregado en el campo auto-incremental
		if ($blnRescheduleTask) {
			$intSurveyTaskID = (int) @$aBITAMConnection->ADOConnection->_insertid();
		}
	}
	
	if ($blnRescheduleTask) {
		return $intSurveyTaskID;
	}
	//@JAPR
	return "OK";
}

//JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
/* Función utilizada para obtener la imagen que el App envía de manera segura para poder utilizar dentro de un canvas en procesos de generación de PDF y demás, recibiendo sólo el
nombre de la imágen y el ID del DataSource del catálogo correspondiente para determinar si se trata de una imagen de eForms o de eBavel
Este método sólo se debe ejecutar directo desde un request explícitamente preparado para descargar la imagen, ya que no se realizará autenticación del usuario ni se registrará en el
monitor de operaciones ni muchas otras cosas, sólo se creará la conexión al respositorio, se realizará una consulta muy específica para determinar el tipo de catálogo y en caso de ser
necesaria la ruta de eBavel, de tal manera que el path regresado ya sea el adecuado para que se resuelva la imagen correctamente
*/
function GetCORSSafeCatalogAttributeImage() {
	global $appVersion;
	global $BITAMRepositories;
	
	//La función finalmente regresará el contenido de la imagen, salvo en el caso de eBavel en el cual se regresa la URL directamente hacia el REST API de eBavel el cual se encargará
	//de descargar la imagen correctamente, pero por default símplemente regresará vacío
	$strImgContent = '';
	
	//Todos los parámetros fueron recibidos del request, así que se leen directamente desde ahí
	$strImageName = getParamValue('fileName', 'both', "(string)", true);
	$strProjectName = getParamValue('projectName', 'both', "(string)", true);
	$intDataSourceID = getParamValue('dataSourceID', 'both', "(int)", true);
	if (is_null($strImageName) || !$strImageName || is_null($strProjectName) || !$strProjectName
			|| is_null($intDataSourceID) || $intDataSourceID <= 0) {
		die($strImgContent);
	}
	$strImageName = urldecode($strImageName);
	
	$theRepositoryName = $strProjectName;
	if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
		die($strImgContent);
	}

	$theRepository = $BITAMRepositories[$theRepositoryName];
	$aRepository = $theRepository;
	if (!$theRepository->open()) {
		die($strImgContent);
	}
	
	require_once("dataSource.inc.php");
	$objDataSource = BITAMDataSource::NewInstanceWithID($aRepository, $intDataSourceID);
	if (is_null($objDataSource)) {
		die($strImgContent);
	}
	
	if ($objDataSource->Type == dstyeBavel) {
		//Con los DataSources de tipo eBavel, las rutas de imágenes serán resueltas por el propio eBavel, sólo es necesario obtener la URL que apunte al servicio correcto
		//Las Apps ya habrían sido descargadas con la ruta absoluta de eBavel la cual es CORS Safe, si algún App hubiera llegado a este punto para eBavel entonces simplemente se
		//regresa el valor recibido, aunque lo correcto es que la propia App ya hubiera incluído la liga sin necesidad de enviarla al server para regresar lo mismo, eso habría resuelto
		//la URL de eBavel correctamente con la imagen correspondiente
		$strImgContent = $strImageName;
	}
	else {
		//Para el resto de los catálogos la imagen ya debería encontrarse directamente en la ruta de CustomerImages de eForms para el repositorio correspondiente, así que simplemente
		//cargará la imagen y regresará su contenido
		
		//En este caso generalmente la imagen viene con un formato como el siguiente debido a que fue descargado su catálogo a un dispositivo móvil
		//{@IMGPATH}images/fbm_bmd_0462/oxxo.jpg
		//Por tanto deberá obtener sólo la referencia al nombre de la imagen, ya que el resto de la información es obtenida directamente de los parámetros
		$strCustomerImagesPath = "./customerimages/{$theRepositoryName}/";
		$strFileName = $strCustomerImagesPath.GetFileNameFromFilePath($strImageName);
		if (!file_exists($strFileName)) {
			die($strImgContent);
		}
		
		$strImgContent = file_get_contents($strFileName);
		$finfo = new finfo(FILEINFO_MIME);
		$mimeType = $finfo->buffer($strImgContent);
		header("Content-Type: {$mimeType}");
	}
	
	die($strImgContent);
}
//@JAPR 2018-10-17: Agregado el proceso de revisión de archivos subidos desde el dispositivo móvil para generar un error en caso de que no se encuentre alguno (#4C9HAY)
/* Verifica que todos los archivos referenciados en el Outbox se encuentren en la carpeta correspondiente donde deberían haber sido subidos por el App (este proceso sólo aplica para apps
de dispositivo, ya que en browser los archivos se suben en línea y a una carpeta diferente) */
function CheckUploadedFilesForOutbox($sSyncPath, $sOutboxData) {
	global $blnWebMode;
	//@JAPR 2019-02-28: Corregido un bug, no se había considerado la compatibilidad hacia atrás en casos donde se usaba la pregunta UpdateData, ya que dicha pregunta no sincronizaba 
	//archivos de ningún tipo sino solo respuestas hasta que se cambió en el issue #OA1YQ0, por lo tanto la revisión de esta función no aplica antes del App que sube los archivos (#DIA5G4)
	global $appVersion;
	
	if (!isset($blnWebMode)) {
		$blnWebMode = getParamValue('webMode', 'both', '(int)', false);
	}
	
	$blnOk = true;
	if ( $blnWebMode ) {
		return $blnOk;
	}
	
	//@JAPR 2019-02-28: Corregido un bug, no se había considerado la compatibilidad hacia atrás en casos donde se usaba la pregunta UpdateData, ya que dicha pregunta no sincronizaba 
	//archivos de ningún tipo sino solo respuestas hasta que se cambió en el issue #OA1YQ0, por lo tanto la revisión de esta función no aplica antes del App que sube los archivos (#DIA5G4)
	$intUpdateDataQuestionID = getParamValue('updateDataQuestionID', 'both', "(int)");
	//JAPR 2019-03-05: Modificada la pregunta UpdateData para que envíe todos los archivos asociados a la captura y procese todos los registros (#OA1YQ0)
	//En conjunto con el issue #DIA5G4, ahora si se trata de una pregunta UpdateData y de la versión que si soporta upload de archivos, sólo validará que se hubieran subido correctamente
	//si además la pregunta está configurada para ello, por lo tanto leerá el parámetro uploadMediaFiles que solo se recibiría si fuera dicho caso
	$intUploadMediaFiles = getParamValue('uploadMediaFiles', 'both', "(int)");
	if ( $intUpdateDataQuestionID > 0 && ($appVersion < esvUpdateDataWithFiles || !$intUploadMediaFiles) ) {
		//Si se trata de una sincronización de pregunta UpdateData y NO es la versión que soporta upload de archivos en esos casos, entonces no debe verificar los posibles archivos que
		//la sincronización normal subiría para que no impida que la UpdateData continue con su grabado. Se agregó la condición altera que aunque si sea la versión correcta, no valide los
		//archivos si el request no los envió porque la pregunta está específicamente configurada para no mandarlos
		return $blnOk;
	}
	//@JAPR
	
	try {
		$sOutboxData = trim((string) $sOutboxData);
		$sSyncPath = trim((string) $sSyncPath);
		//Si no hay nada que procesar, no genera un error
		if ( !$sSyncPath || !$sOutboxData ) {
			return false;
		}
		
		//Convierte el string de datos en un objeto que pueda ser analizado
		require_once('json.class.php');
		$json = new Services_JSON();
		$jsonData = json_decode($sOutboxData); //ahora se puso asi
		$jsonData = object_to_array($jsonData);

		//Inicia el barrido de las respuestas recibidas
		$arrAnswers = @$jsonData['answers'];
		if (is_null($arrAnswers) || !is_array($arrAnswers)) {
			$arrAnswers = array();
		}
		
		clearstatcache();
		//Recorre todas las respuestas recibidas en busca de algún archivo que debió subir para confirmar que se encuentre en la carpeta de sincronización, en caso de no ser así reportará
		//el error para que se cancele el proceso de sincronización en el App
		foreach ($arrAnswers as $intQuestionID => $objAnswer) {
			if (is_array($objAnswer)) {
				//Como no se tiene cargada la definición de la forma en este punto, la única validación que es posible hacer es verificar si los índices recibidos son o no numéricos,
				//si los son quiere decir que se trata de una pregunta de sección multi-registro por lo que procesa todo el bloque de $objAnswer como un array de registros, de lo contrario
				//considera que es una respuesta de sección estándar y procesa $objAnswers como una única respuesta
				if ( isset($objAnswer[0]) ) {
					foreach ($objAnswer as $intPageNum => $aRecAnswer) {
						$strData = (string) @$aRecAnswer['photo'];
						if ($strData != '') {
							//Los archivos de fotos se generan en el server con extensión ".jpg"
							$strCompletePath = $sSyncPath."\\".$strData.".jpg";
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
						
						$strData = (string) @$aRecAnswer['canvasPhoto'];
						if ($strData != '') {
							//Los archivos de fotos se generan en el server con extensión ".jpg"
							$strCompletePath = $sSyncPath."\\".$strData.".jpg";
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
						
						//@JAPR 2019-04-24: Agregada la validación de upload correcto para archivos multimedia (#8DKBAF)
						//Archivos Multi-media (Audios, Videos y Documentos)
						$strData = (string) @$aRecAnswer['document'];
						if ($strData != '') {
							//En el caso de este tipo de archivos, deberá cortar la porción del path que pudieran traer ya que se buscarán todos en la carpeta de sincronización directamente
							$strData = GetFileNameFromFilePath($strData);
							//Los archivos de fotos se generan en el server con extensión ".jpg"
							$strCompletePath = $sSyncPath."\\".$strData;
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
						//@JAPR
					}
				}
				else {
					if ( isset($objAnswer['photo']) ) {
						$strData = (string) @$objAnswer['photo'];
						if ($strData != '') {
							//Los archivos de fotos se generan en el server con extensión ".jpg"
							$strCompletePath = $sSyncPath."\\".$strData.".jpg";
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
					}
					
					//Fotos de Sketch
					if ( isset($objAnswer['canvasPhoto']) ) {
						$strData = (string) @$objAnswer['canvasPhoto'];
						if ($strData != '') {
							//Los archivos de fotos se generan en el server con extensión ".jpg"
							$strCompletePath = $sSyncPath."\\".$strData.".jpg";
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
					}
					
					//@JAPR 2019-04-24: Agregada la validación de upload correcto para archivos multimedia (#8DKBAF)
					//Archivos Multi-media (Audios, Videos y Documentos)
					if ( isset($objAnswer['document']) ) {
						$strData = (string) @$objAnswer['document'];
						if ($strData != '') {
							//En el caso de este tipo de archivos, deberá cortar la porción del path que pudieran traer ya que se buscarán todos en la carpeta de sincronización directamente
							$strData = GetFileNameFromFilePath($strData);
							$strCompletePath = $sSyncPath."\\".$strData;
							if (!file_exists($strCompletePath)) {
								return false;
							}
						}
					}
					//@JAPR
				}
			}
		}
	} catch (Exception $e) {
		$blnOk = false;
	}
	
	return $blnOk;
}
//@JAPR
?>