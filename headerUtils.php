<?php
require_once("utils.inc.php");

function getDashboardName($aRepository,$stageKey)
{
	//Obtiene el nombre del escenario 
	$sql = "SELECT NOM_ESCENARIO FROM SI_ESCENARIO WHERE CLA_ESCENARIO = ".$stageKey;
	
	$aRS = $aRepository->ADOConnection->Execute($sql);
			
	if (!$aRS || $aRS->EOF)
	{
		die( translate("Error accessing")." SI_ESCENARIO ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	$aDashboardName = $aRS->fields["nom_escenario"];
	
	return $aDashboardName;

}

function createLinkByDashboardID($serverArtus, $repositoryName, $stageKey, $artusUserShortName, $artusPassword, $periodBeginMonth, $periodEndMonth)
{

$arrayBeginMonth = explode(" ",$periodBeginMonth);
$dateBeginMonth = str_replace("-","",trim ($arrayBeginMonth[0]));
$paramBeginMonth = "SPI_01_000004".$dateBeginMonth;
$params = $paramBeginMonth;

$repositoryName64 = BITAMEncode($repositoryName);
$repositoryName64Url = urlencode($repositoryName64);

$stageKey64 = BITAMEncode($stageKey);
$stageKey64Url = urlencode($stageKey64);

$artusUserShortName64 = BITAMEncode($artusUserShortName);
$artusUserShortName64Url = urlencode($artusUserShortName64);

$artusPassword64 = BITAMEncode($artusPassword);
$artusPassword64Url = urlencode($artusPassword64);

$params64 = BITAMEncode($params);
$params64Url = urlencode($params64);

$enlaceMain = $serverArtus."LoadEsc.asp?repository=".$repositoryName64."&stage=".$stageKey64."&user=".$artusUserShortName64."&password=".$artusPassword64;

//$enlaceMainURL =$serverArtus.'LoadEsc.asp?repository='.$repositoryName64Url.'&stage='.$stageKey64Url.'&user='.$artusUserShortName64Url.'&password='.$artusPassword64Url;


return $enlaceMain;

}

function getBeginMonth($Period)
{
	if($Period == "0000-00-00")
	{
		$Period = date("Y-m-d");
	}
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $Period);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//Mes
	$monthDate = $arrayDate[1];
	
	$firstDayDate = "01";
	
	$initialTime = "00:00:00";
	
	$periodStartDate = $yearDate."-".$monthDate."-".$firstDayDate." ".$initialTime;
	
	return $periodStartDate;
}

function getEndMonth($Period)
{
	if($Period == "0000-00-00")
	{
		$Period = date("Y-m-d");
	}
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $Period);
		
	//Anio
	$yearDate = $arrayDate[0];
	
	//Mes
	$monthDate = $arrayDate[1];
	
	$lastDayDateInt = getLastDay($monthDate, $yearDate);
	
	$lastDayDate = $lastDayDateInt < 10 ? "0".$lastDayDateInt : "".$lastDayDateInt."";
	
	$finalTime = "23:59:59";
	
	$periodFinalDate = $yearDate."-".$monthDate."-".$lastDayDate." ".$finalTime;
	
	return $periodFinalDate;
}


function getLastDay($month,$year){ 
    $lastDay=28; 
    while (checkdate((int)$month, $lastDay + 1, (int)$year))
    { 
       $lastDay++; 
    } 
    return $lastDay;
}

function getPeriodFilter($periodDateBegin, $periodDateEnd)
{
	$arrayPeriodDateBegin = explode(" ",$periodDateBegin);
	$datePeriodDateBegin = str_replace("-","",trim ($arrayPeriodDateBegin[0]));

	$arrayPeriodDateEnd = explode(" ",$periodDateEnd);
	$datePeriodDateEnd = str_replace("-","",trim ($arrayPeriodDateEnd[0]));
	$paramPeriodDate = "SPI_01_000004".$datePeriodDateBegin."_AWSep_"."SPD_01_000004".$datePeriodDateEnd;

	$params = $paramPeriodDate;
	
	$params64 = BITAMEncode($params);
	$params64Url = urlencode($params64);

	
	return "&params=".$params64;
}


function getPeriodFilter1($periodDateBegin, $periodDateEnd,$type,$period,$dimensionFilter)
{
	$cadperiod ="";
	$strperiod= (string) $period;
	
	$cperiod= str_pad($strperiod,6,"0",STR_PAD_LEFT);
		
	$arrayPeriodDateBegin = explode(" ",$periodDateBegin);
	$datePeriodDateBegin = str_replace("-","",trim ($arrayPeriodDateBegin[0]));

	$arrayPeriodDateEnd = explode(" ",$periodDateEnd);
	$datePeriodDateEnd = str_replace("-","",trim ($arrayPeriodDateEnd[0]));

	if (($type == 0) || ($type==2)) 
	{
		$paramPeriodDate = "SPI_01_".$cperiod.$datePeriodDateBegin;
	}
	else 
	{
		$paramPeriodDate = "SPI_01_".$cperiod.$datePeriodDateBegin."_AWSep_"."SPD_01_".$cperiod.$datePeriodDateEnd;
	}
	$params = $paramPeriodDate."_AWSep_".$dimensionFilter;
	
	$params64 = BITAMEncode($params);
	$params64Url = urlencode($params64);

	
	return "&params=".$params64;
}




function getMonthsFromNowDateToNumYearAgo($nowDate, $numberYears)
{
	if(is_null($nowDate) || trim($nowDate)=="")
	{
		$nowDate = date("Y-m-d");
	}
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $nowDate);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//AnioAgo
	$yearDateAgo = (int)$arrayDate[0]-$numberYears;
	
	//Mes
	$monthDate = $arrayDate[1];
	
	$firstDayDate = "01";
	
	$initialTime = "00:00:00";
	
	$j = (int)$monthDate;
	
	$i = (int)$yearDate;
	
	$arrayMonths = array();

	for( ; $i>=$yearDateAgo; $i-- )
	{
		
		$j = ($j==0?12:$j);
		
		for( ; $j>=1; $j-- )
		{
			$strMonth = $j < 10 ? "0".$j : "".$j."";
			$arrayMonths[] = $i."-".$strMonth."-".$firstDayDate." ".$initialTime;
		}
	
	}
	return $arrayMonths;
}


function getMonthsFromAmountYearBeforeToAmountYearAfter($beforeYears, $afterYears)
{
	$nowDate = date("Y-m-d");
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $nowDate);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//AnioAfter
	$yearDateAfter = (int)$arrayDate[0]+$afterYears;

	//AnioBefore
	$yearDateBefore = (int)$arrayDate[0]-$beforeYears;
	
	$firstDayDate = "01";
	
	$initialTime = "00:00:00";
	
	//Anio Inicial
	$i = $yearDateAfter;
	
	//Month December
	$j = 12;
	
	$arrayMonths = array();

	for( ; $i>=$yearDateBefore; $i-- )
	{
		
		$j = ($j==0?12:$j);
		
		for( ; $j>=1; $j-- )
		{
			$strMonth = $j < 10 ? "0".$j : "".$j."";
			$arrayMonths[] = $i."-".$strMonth."-".$firstDayDate." ".$initialTime;
		}
	
	}
	return $arrayMonths;
}

function getMonthsFromDateToDate($beginDate, $finalDate)
{
	
	$arrayBeginDate = array();
	$arrayBeginDate = explode("-", $beginDate);
	
	$yearBeginDate = (int)$arrayBeginDate[0];
	$monthBeginDate = (int)$arrayBeginDate[1];
	
	$arrayFinalDate = array();
	$arrayFinalDate = explode("-", $finalDate);
	
	$yearFinalDate = (int)$arrayFinalDate[0];
	$monthFinalDate = (int)$arrayFinalDate[1];
	
	
	
	$firstDayDate = "01";
	
	$initialTime = "00:00:00";
	
	$i = $yearBeginDate;
	$j = $monthBeginDate;
	
	$arrayMonths = array();

	for( ; $i<=$yearFinalDate; $i++ )
	{
		
		$j = ($j==13?1:$j);
		
		if($i==$yearFinalDate)
		{
			$limite = $monthFinalDate;
		}
		else 
		{
			$limite = 12;
		}
		
		for( ; $j<=$limite; $j++ )
		{
			$strMonth = $j < 10 ? "0".$j : "".$j."";
			$arrayMonths[] = $i."-".$strMonth."-".$firstDayDate." ".$initialTime;
		}
	}
	
	return $arrayMonths;
}

function getCurrentMonth()
{
	$Period = date("Y-m-d");
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $Period);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//Mes
	$monthDate = $arrayDate[1];
	
	$firstDayDate = "01";
	
	$initialTime = "00:00:00";
	
	$currentMonth = $yearDate."-".$monthDate."-".$firstDayDate." ".$initialTime;
	
	return $currentMonth;
}

function getFirstDayOfYear()
{
	$nowDate = date("Y-m-d");
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $nowDate);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//Mes
	$firstMonthYear = "01";
	
	//Day
	$firstDay = "01";
	
	$firstDayOfYear = $yearDate."-".$firstMonthYear."-".$firstDay;
	
	return $firstDayOfYear;
}

function getLastDayOfYear()
{
	$nowDate = date("Y-m-d");
	
	$arrayDate = array();
	
	$arrayDate = explode("-", $nowDate);
	
	//Anio
	$yearDate = $arrayDate[0];
	
	//Mes
	$lastMonthYear = "12";
	
	//Day
	$lastDay = "31";
	
	$lastDayOfYear = $yearDate."-".$lastMonthYear."-".$lastDay;
	
	return $lastDayOfYear;

}

function getFormattedPeriod($iPeriodID,$iFixedLength)
{
	$aFormattedPeriodID = "".$iPeriodID."";
	$aLength = strlen($aFormattedPeriodID);
	
	for($i=$aLength; $i<$iFixedLength; $i++)
	{
		$aFormattedPeriodID = '0'.$aFormattedPeriodID;
	}
	
	return $aFormattedPeriodID;
}
?>