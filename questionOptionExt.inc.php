<?php
require_once("questionOption.inc.php");
require_once("object.trait.php");

class BITAMQuestionOptionExt extends BITAMQuestionOption
{
	use BITAMObjectExt;
	function __construct($aRepository, $aQuestionID)
	{
		parent::__construct($aRepository, $aQuestionID);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionOptionExt&QuestionID=".$this->QuestionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionOptionExt&QuestionID=".$this->QuestionID."&QuestionOptionID=".$this->QuestionOptionID;
		}
	}
	
	function generateAfterFormCode($aUser)
	{
		//@JAPR 2015-04-30: Corregido un bug, al integrar el nuevo archivo se desactivaron las validaciones previas al grabado
 		parent::generateAfterFormCode($aUser);
		//@JAPR
		
 		$myFormName = get_class($this);
		$intSelSurveyID = getParamValue('SelSurveyID', 'both', '(int)');
		
		//Si se trata de un objeto nuevo, el grabado se debe hacer hacia un archivo alternativo que procese todos estos requests
		if ($this->isNewObject()) {
?>
	 	<script language="JavaScript">
			<?=$myFormName?>_SaveForm.action = '<?='processRequest.php?Process=Add&ObjectType='.otyOption.'&'.$this->get_QueryString()?>&SelSurveyID=<?=$intSelSurveyID?>';
		</script>
<?
		}
	}
}

class BITAMQuestionOptionExtCollection extends BITAMQuestionOptionCollection
{
	function get_AddRemoveQueryString()
	{
		$intSelSurveyID = getParamValue('SelSurveyID', 'both', '(int)');
		return "BITAM_PAGE=QuestionOptionExt&QuestionID=".$this->QuestionID."&SelSurveyID=".$intSelSurveyID;
	}
}
?>