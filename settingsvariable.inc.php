<?php

require_once("repository.inc.php");
require_once("initialize.php");
//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
//@JAPRDescontinuado
//require_once("eformsAppCustomizationTpl.inc.php");
//@JAPR 2016-02-23: Agregados los templates de estilos personalizados de v6
require_once("eformsAppCustomStylesTpl.inc.php");

class BITAMSettingsVariable extends BITAMObject
{
	public $SettingID;
	public $SettingName;
	public $SettingValue;
	public $ArrayVariables;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	//@JAPR 2014-06-18: Agregados los templates para personalización del App
	public $CustomizedColors;
	public $incrementVersion;
	public $oldBackground;
	//@JAPR 2014-08-05: Agregado el Responsive Design
	public $ResponsiveDesignProps;
	public $CustomHeader;
	public $UseCache;
	public $CacheRequests;
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->SettingID = -1;
		$this->SettingName= "";
		$this->SettingValue= "";
		$this->ArrayVariables=array();
		$this->ArrayVariableValues=array();
		$this->ArrayVariablesExist=array();
		$this->incrementVersion = false;
		$this->oldBackground = "";
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		$this->CustomizedColors = '';
		//@JAPR 2014-08-05: Agregado el Responsive Design
		//Si una variable debe ajustarse para Responsive Design, se tiene que agregar a este objeto para que sea considerada en dicho esquema
		//agregando un elemento adicional a la instancia de captura por cada tipo de dispositivo
		$this->ResponsiveDesignProps = array();
		$this->ResponsiveDesignProps['BACKGROUNDIMAGE'] = array();
		$this->CustomHeader = "";
		$this->UseCache = "";
		$this->CacheRequests = "";
	}

	static function NewInstance($aRepository)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}

	/*
	//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
	Se agregó el parámetro $bDefaultIfEmpty para indicar cuando el valor de la propiedad se debe restaurar al default en lugar de usar el
	específico del dispositivo que hace el request, lo cual sucede cuando el fondo de un dispositivo realmente no está configurado y debe usar
	el fondo default
	*/
	static function NewInstanceWithID($aRepository, $aSettingsVariableID, $bDefaultIfEmpty = false)
	{
		$anInstance = null;
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (((int)  $aSettingsVariableID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT SettingID, SettingName, SettingValue FROM SI_SV_Settings WHERE SettingID = ".((int) $aSettingsVariableID) ;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Settings ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}

		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (!is_null($anInstance) && isset($anInstance->ResponsiveDesignProps[$anInstance->SettingName])) {
				//En este caso esta instancia requiere Responsive Design, por lo que se cargan dichos valores
				if (getMDVersion() >= esvResponsiveDesign) {
					//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
					$strCalledClass::GetResponsiveDesignProps($aRepository, $anInstance, null, $bDefaultIfEmpty);
				}
			}
		}
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2014-08-05: Agregado el Responsive Design
	/* Carga las propiedades específicas del Responsive Design basado en los parámetros, agregándolas a las instancias especificadas en la
	colección o array proporcionado, de tal forma que este método se puede invocar bajo demanda sólo en los casos donde realmente se necesiten
	estas propiedades sin tener que forzar a que el NewInstanceFromRS se sature con queries y asignaciones que no van a ser utilizadas
	El parámetro $anArrayOfObjects se asumirá que es un array ya indexado por el SettingID, sin embargo si se trata de una colección
	de configuraciones, entonces primero se procede a crear el array interno para facilitar el acceso a las instancias. En este caso para 
	configuraciones, debido a que en la única instancia se manejan los valores como un array indexado por los SettingID, se deberá de
	recibir una instancia de BITAMSettingsVariable y se realizará un tratamiento especial para actualizar sólo directamente en los arrays
	de la instancia y en las propiedades dinámicas generadas a partir de ellos
	//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
	Se agregó el parámetro $bDefaultIfEmpty para indicar cuando el valor de la propiedad se debe restaurar al default en lugar de usar el
	específico del dispositivo que hace el request, lo cual sucede cuando el fondo de un dispositivo realmente no está configurado y debe usar
	el fondo default
	//@JAPR 2018-06-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	//@JAPRDescontinuada en V6
	*/
	static function GetResponsiveDesignProps($aRepository, $anArrayOfObjects, $anArrayOfObjectIDs = null, $bDefaultIfEmpty = false) {
		return true;
		global $gblShowErrorSurvey;
		
		$intDeviceID = null;
		if (is_null($anArrayOfObjectIDs) || !is_array($anArrayOfObjectIDs)) {
			$anArrayOfObjectIDs = array();
		}
		
		//Asigna el array de secciones indexado por el ID
		$blnIsParentInstance = false;
		$arrObjects = $anArrayOfObjects;
		$strClassName = '';
		if (is_object($anArrayOfObjects)) {
			$strClassName = get_class($anArrayOfObjects);
		}
		if ($strClassName == 'BITAMSettingsVariable' || $strClassName == 'BITAMSettingsVariableExtended') {
			//En este caso se recibirá tratamiento especial, se recibió una instancia de pregunta y se asume que se quieren procesar las
			//propiedades de sus opciones de respuesta directamente en la propia instancia de pregunta, en lugar de actualizarla en las
			//instancias de opciones de respuestas como funcionaría generalmente esta función para otros tipos de objetos
			$blnIsParentInstance = true;
			//Se debe asignar el DeviceID por si se va a cargar sólo para la instancia solicitada para regresar las definiciones
			if ($strClassName == 'BITAMSettingsVariable') {
				$intDeviceID = identifyDeviceType();
			}
		}
		else {
			if (!is_array($anArrayOfObjects)) {
				$arrObjects = array();
				foreach ($anArrayOfObjects->Collection as $objObject) {
					$arrObjects[$objObject->SectionID] = $objObject;
				}
			}
		}
		
		//Si no se hubiera especificado ningún filtro, utiliza el array de instancias para obtener las llaves y cargar sólo los objetos que
		//fueron recibidos, de esta forma se puede sólo recibir el array de instancias sin especificar mas parámetros y funcionará bien
		$anArrayOfObjectIDsFlipped = array();
		if (count($anArrayOfObjectIDs) == 0) {
			if ($blnIsParentInstance) {
				//En este caso se extraerán todos los objetos puesto que no hay demasiados por estar limitada esta funcionalidad en los settings
				$anArrayOfObjectIDs = array();
			}
			else {
				$anArrayOfObjectIDs = array_keys($arrObjects);
			}
		}
		
		//Genera el filtro basado en los parámetros especificados
		$where = "";
		if (!is_null($intDeviceID)) {
			$where .= " AND A.DeviceID = {$intDeviceID}";
		}
		
		$filter = '';
		if (!is_null($anArrayOfObjectIDs))
		{
			switch (count($anArrayOfObjectIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.ObjectID = ".((int)$anArrayOfObjectIDs[0]);
					break;
				default:
					foreach ($anArrayOfObjectIDs as $anObjectID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $anObjectID; 
					}
					if ($filter != "")
					{
						$filter = " AND A.ObjectID IN (".$filter.")";
					}
					break;
			}
		}
		$where .= $filter;
		
		$sql = "SELECT A.ObjectID, A.DeviceID, A.ImageText 
			FROM SI_SV_SurveyHTML A 
			WHERE A.SurveyID = 0 AND A.ObjectType = ".otySetting.$where;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyHTML ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF)
		{
			$intObjectID = (int) @$aRS->fields["objectid"];
			$intDeviceID = (int) @$aRS->fields["deviceid"];
			
			$strProperty = '';
			$strPropValue = '';
			switch ($intObjectID) {
				case 17:
					$strProperty = 'BACKGROUNDIMAGE';
					$strPropValue = (string) @$aRS->fields["imagetext"];
					break;
			}
			
			if ($strProperty != '') {
				if ($blnIsParentInstance) {
					//En este caso se tiene que actualizar directamente en el objeto recibido según el tipo de clase de la que se trate
					if ($strClassName == 'BITAMSettingsVariableExtended') {
						//Se trata de una instancia con propiedades dinámicas por cada valor
						$strDeviceName = getDeviceNameFromID($intDeviceID);
						$strProperty .= $strDeviceName;
						$arrObjects->$strProperty = $strPropValue;
					}
					elseif ($strClassName == 'BITAMSettingsVariable') {
						//En este caso se trata de una instancia única de objeto, por lo que el query debió haber cargado sólo el valor de esta
						//configuración así que se sobreescribe directamente en la instancia
						//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
						if (trim($strPropValue) == '' && !is_null($intDeviceID) && $intDeviceID && $bDefaultIfEmpty) {
							//En este caso simplemente se deja el valor default ya que el dispositivo no tenía un valor configurado
							$strPropValue = $arrObjects->SettingValue;
						}
						//@JAPR
						$arrObjects->SettingValue = $strPropValue;
					}
					
					//Marca el dispositivo como ya existente para generar un UPDATE
					$arrObjects->ResponsiveDesignProps[$strProperty][$intDeviceID] = 1;
				}
				else {
					//En este caso se trata de una colección de configuraciones, así que se obtiene la instancia mediante el Id de la configuración
					//y se asigna el valor directamente a la instancia tal como si se hubiera pedido sólo una configuración
					$objObject = @$arrObjects[$intObjectID];
					if (!is_null($objObject)) {
						//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
						if (trim($strPropValue) == '' && !is_null($intDeviceID) && $intDeviceID && $bDefaultIfEmpty) {
							$strPropValue = $objObject->SettingValue;
						}
						//@JAPR
						$objObject->SettingValue = $strPropValue;
						//Marca el dispositivo como ya existente para generar un UPDATE
						$objObject->ResponsiveDesignProps[$strProperty][$intDeviceID] = 1;
					}
				}
			}
			
			$aRS->MoveNext();
		}
	}
	//@JAPR
	
	/*
	//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
	Se agregó el parámetro $bDefaultIfEmpty para indicar cuando el valor de la propiedad se debe restaurar al default en lugar de usar el
	específico del dispositivo que hace el request, lo cual sucede cuando el fondo de un dispositivo realmente no está configurado y debe usar
	el fondo default
	*/
	static function NewInstanceWithName($aRepository, $aSettingsVariableName, $bDefaultIfEmpty = false)
	{
		$anInstance = null;
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (trim($aSettingsVariableName) == '')
		{
			return $anInstance;
		}
		
		$sql = "SELECT SettingID, SettingName, SettingValue FROM SI_SV_Settings WHERE SettingName = ".$aRepository->DataADOConnection->Quote($aSettingsVariableName);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Settings ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}

		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (!is_null($anInstance) && isset($anInstance->ResponsiveDesignProps[$anInstance->SettingName])) {
				//En este caso esta instancia requiere Responsive Design, por lo que se cargan dichos valores
				if (getMDVersion() >= esvResponsiveDesign) {
					//@JAPR 2014-11-11: Corregido un bug al descargar la definición del Background
					$strCalledClass::GetResponsiveDesignProps($aRepository, $anInstance, null, $bDefaultIfEmpty);
				}
			}
		}
		//@JAPR
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		
		$anInstance->SettingID = (int) $aRS->fields["settingid"];
		$anInstance->SettingName = translate(rtrim( $aRS->fields["settingname"] ));
		$anInstance->SettingValue = $aRS->fields["settingvalue"];
		
		if ($anInstance->SettingName == 'EBAVELURL')
		{
			$anInstance->SettingValue = str_replace('\\', '/', trim($anInstance->SettingValue));
			if (substr($anInstance->SettingValue, -1, 1) != '/')
			{
				$anInstance->SettingValue .= '/';
			}
		}
		return $anInstance;
	}
	
	//Llena la instancia de settings con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariableValues = array();
		
		$this->ArrayVariables[1] = 'MEETING';
		$this->ArrayVariableValues['MEETING'] = '';
		$this->ArrayVariables[2] = 'CATEGORYCATALOG';
		$this->ArrayVariableValues['CATEGORYCATALOG'] = '';
		$this->ArrayVariables[3] = 'CATEGORYMEMBER';
		$this->ArrayVariableValues['CATEGORYMEMBER'] = '';
		$this->ArrayVariables[4] = 'EBAVELURL';
		$this->ArrayVariableValues['EBAVELURL'] = '';
		//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
		//Servicio de eForms alternativo al que ciertas Apps (condicionado por versión) se conectarán en lugar del especificado en SAAS_Databases
		$this->ArrayVariables[5] = 'ALTEFORMSSERVICE';
		$this->ArrayVariableValues['ALTEFORMSSERVICE'] = '';
		$this->ArrayVariables[6] = 'ALTEFORMSSERVICEVERSION';
		$this->ArrayVariableValues['ALTEFORMSSERVICEVERSION'] = '';
		//Versíón mínima requerida para que se regrese el servicio alternativo como el servicio válido de eForms a usar (no aplica para v2
		//porque esa versión no podía alterar dinámicamente su carpeta de servicio). Si la versión que hace el request para identificar su
		//servicio es >= que la indicada en esta configuración, entonces en lugar de regresar lo indicado en SAAS_Databases, se regresará
		//como servicio la configuración ALTEFORMSSERVICE si es que está definida
		$this->ArrayVariables[7] = 'REQUIREDVERSIONFORALTSERVER';
		$this->ArrayVariableValues['REQUIREDVERSIONFORALTSERVER'] = '';
		//Contiene la ruta (absoluta vía HTTP o relativa) a la que se deberán copiar las imagenes que sean subidas a este servicio, si no
		//se especifica entonces no se realiza copia alguna y se dejan donde siempre
		$this->ArrayVariables[8] = 'SURVEYIMAGESMIRRORFOLDER';
		$this->ArrayVariableValues['SURVEYIMAGESMIRRORFOLDER'] = '';
		//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		if (getMDVersion() >= esveBavelSuppActions) {
			$this->ArrayVariables[9] = 'EBAVELDEFAULTACTIONSTATUS';
			$this->ArrayVariableValues['EBAVELDEFAULTACTIONSTATUS'] = '';
			$this->ArrayVariables[10] = 'EBAVELDEFAULTACTIONSOURCE';
			$this->ArrayVariableValues['EBAVELDEFAULTACTIONSOURCE'] = '';
		}
		//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
		$this->ArrayVariables[11] = 'USEINCREMENTALAGGREGATIONS';
		$this->ArrayVariableValues['USEINCREMENTALAGGREGATIONS'] = '';
		//@JAPR 2013-12-17: Cambiado el modo de uso del GPS
		$this->ArrayVariables[12] = 'REGISTERGPSDATAAT';
		$this->ArrayVariableValues['REGISTERGPSDATAAT'] = '';
		//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
		$this->ArrayVariables[13] = 'CUSTOMIZEDSTORESURVEYSERVICE';
		$this->ArrayVariableValues['CUSTOMIZEDSTORESURVEYSERVICE'] = '';
		//@JAPR
		$this->ArrayVariables[14] = 'DEFAULTSURVEY';
		$this->ArrayVariableValues['DEFAULTSURVEY'] = '';
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
			$this->ArrayVariables[15] = 'ENABLECOLORTEMPLATES';
			$this->ArrayVariableValues['ENABLECOLORTEMPLATES'] = '';
			$this->ArrayVariables[16] = 'DEFAULTCOLORTEMPLATE';
			$this->ArrayVariableValues['DEFAULTCOLORTEMPLATE'] = '';
		}
		//Conchita 2014-07-01 custom background image
		if (getMDVersion() >= esvAppCustomBackground) {
			$this->ArrayVariables[17] = 'BACKGROUNDIMAGE';
			$this->ArrayVariableValues['BACKGROUNDIMAGE'] = '';
			$this->ArrayVariables[18] = 'MENUMARGINTOP';
			$this->ArrayVariableValues['MENUMARGINTOP'] = '';
			$this->ArrayVariables[19] = 'MENUMARGINLEFT';
			$this->ArrayVariableValues['MENUMARGINLEFT'] = '';
			$this->ArrayVariables[20] = 'MENUMARGINBOTTOM';
			$this->ArrayVariableValues['MENUMARGINBOTTOM'] = '';
			$this->ArrayVariables[21] = 'MENUMARGINRIGHT';
			$this->ArrayVariableValues['MENUMARGINRIGHT'] = '';
			$this->ArrayVariables[22] = 'BACKGROUNDIMAGEVERSION';
			$this->ArrayVariableValues['BACKGROUNDIMAGEVERSION'] = 0;
		}
		//@AAL 18/05/2015 Issue #HZ9V7Z: Estas opción Nunca se termino de implementar por lo tanto se comenta por falta de utilidad.
		/*if (getMDVersion() >= esvFormsMenuPosition) {
			$this->ArrayVariables[23] = 'FORMSMENUPOSITION';
			$this->ArrayVariableValues['FORMSMENUPOSITION'] = '';
		}*/

		//Conchita 2014-09-03. Agregado el titulo personalizado para forms
		$this->ArrayVariables[24] = 'CUSTOMHEADER';
		$this->ArrayVariableValues['CUSTOMHEADER'] = '';
		//@AAL 18/05/2015 Issue #HZ9V7Z: Estas opción Nunca se termino de implementar por lo tanto se comenta por falta de utilidad.
		/*if (getMDVersion() >= esvAgendaRedesign) {
			$this->ArrayVariables[25] = 'AGENDARADIO';
			$this->ArrayVariableValues['AGENDARADIO'] = '';
		}*/
		if (getMDVersion() >= esvIsInvisible) {
			$this->ArrayVariables[26] = 'USECACHE';
			$this->ArrayVariableValues['USECACHE'] = '';
			$this->ArrayVariables[27] = 'CACHEREQUESTS';
			$this->ArrayVariableValues['CACHEREQUESTS'] = '';	
		}
		//@JAPR 2015-10-27: Agregado el proceso para generar todas las Agendas desde el Agente de eForms
		//Esta configuración NO es para uso de eForms, sino del agente de eForms exclusivamente, NO se registra en el DWH como las demás sino en la FBM000, y se debe configurar manualmente
		//ya que no hay una interfaz para ella por ahora, el valor default en caso de no estar configurada es de 2200 (10PM)
		//$this->ArrayVariables[28] = 'AUTOAGENDASGENERATIONTIME';
		//$this->ArrayVariableValues['AUTOAGENDASGENERATIONTIME'] = '';	
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		//Esta configuración se utiliza dentro de un proceso que es ejecutado por el Agente de eForms, sin embargo se debe grabar en el DWH como la mayoría de las configuraciones, aunque
		//en este momento no hay una interfaz para configurarla directamente, el valor default en caso de no estar configurada es de 60 (60min)
		//Representa el tiempo que debe transcurrir entre el reporte previo de error de sincronización de DataSource para volver a reportarlo, siempre y cuando el error anterior sea el
		//mismo que el nuevo error (lo que representaría un problema no atendido), ya que si es un error diferente se reportará de inmediato
		//$this->ArrayVariables[29] = 'DATASOURCEERRREPORTINTERVAL';
		//$this->ArrayVariableValues['DATASOURCEERRREPORTINTERVAL'] = '';	
		//@JAPR

		//OMMC 2015-12-15: Agregado para ocultar las descripciones de la forma en el admin. HIDEDESCRIPTION
		if(getMDVersion() >= esvHideDescription){
			//@JAPR 2016-01-18: Corregido un bug, el siguiente consecutivo se había reutilizado de una configuración del Agente, así que se cambiará al siguiente ID en general
			$this->ArrayVariables[30] = 'HIDEDESCRIPTION';
			$this->ArrayVariableValues['HIDEDESCRIPTION'] = 0;
		}
		//GCRUZ 2016-01-21. Opción para hora de ejecución de destinos incrementales
		if (getMDVersion()>=esvDestinationIncremental)
		{
			$this->ArrayVariables[31] = 'AUTOINCREMENTALGENERATIONTIME';
			$this->ArrayVariableValues['AUTOINCREMENTALGENERATIONTIME'] = '1200';
		}
		//@JAPR 2016-01-28: Agregados los LayOuts personalizados por forma (#FSMUZC)
		if (getMDVersion() >= esvFormsLayouts) {
			$this->ArrayVariables[32] = 'DEFAULTFORMSWIDTH';
			$this->ArrayVariableValues['DEFAULTFORMSWIDTH'] = 100;
			$this->ArrayVariables[33] = 'DEFAULTFORMSHEIGHT';
			$this->ArrayVariableValues['DEFAULTFORMSHEIGHT'] = 200;
			$this->ArrayVariables[34] = 'DEFAULTFORMSTEXTPOSITION';
			$this->ArrayVariableValues['DEFAULTFORMSTEXTPOSITION'] = svtxpBottom;
			$this->ArrayVariables[35] = 'DEFAULTFORMSTEXTSIZE';
			$this->ArrayVariableValues['DEFAULTFORMSTEXTSIZE'] = '50%';
			$this->ArrayVariables[36] = 'DEFAULTFORMSCUSTOMHTML';
			$this->ArrayVariableValues['DEFAULTFORMSCUSTOMHTML'] = '';
		}
		//OMMC 2016-04-05: Agregado para mostrar los números de preguntas
		if (getMDVersion() >= esvShowQNumbers) {
			$this->ArrayVariables[37] = 'SHOWQUESTIONNUMBERS';
			$this->ArrayVariableValues['SHOWQUESTIONNUMBERS'] = 1;
		}
		
		//RV: Agregar variable se configuración global para definir si los links se muestran en el frame o en una ventana flotante
		if (getMDVersion() >= esvConfLinks) {
			$this->ArrayVariables[38] = 'LINKSDISPLAYMODE';
			$this->ArrayVariableValues['LINKSDISPLAYMODE'] = 1;
		}

		//GCRUZ 2016-04-18. Opciónes para menú de 3 rayas en app
		if (getMDVersion() >= esvAppMenuOptions)
		{
			$this->ArrayVariables[39] = 'APPMENUOPTIONS';
			//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
			//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
			//el menú de 3 rayas del App (#EJTH2E)
			$this->ArrayVariableValues['APPMENUOPTIONS'] = null;
		}
		
		//@JAPR 2016-05-19: Agregada la configuración para envío de GPS constante desde el App (#WRWF3Y)
		$this->ArrayVariables[40] = 'APPGPSTRACKER';
		$this->ArrayVariableValues['APPGPSTRACKER'] = 0;
		//@JAPR

		//GCRUZ 2016-06-13. Se agregaron 2 configuraciones para generación de cubo de agendas
		//Para uso del agente de eforms, no configurable por el momento desde alguna interfaz.
		//$this->ArrayVariables[41] = 'AGENDASCUBEGENERATIONTIMEAM';
		//$this->ArrayVariableValues['AGENDASCUBEGENERATIONTIMEAM'] = 0900;
		//$this->ArrayVariables[42] = 'AGENDASCUBEGENERATIONTIMEPM';
		//$this->ArrayVariableValues['AGENDASCUBEGENERATIONTIMEPM'] = 2100;
		//@JAPR 2016-06-13: Agregada la configuración para limitar los resultados de los valores de catálogo en modo de diseño exclusivamente ($gbDesignMode) (#14KN4H)
		//$this->ArrayVariables[43] = 'DESIGNCATALOGROWSLIMIT';
		//$this->ArrayVariableValues['DESIGNCATALOGROWSLIMIT'] = DEFAULT_CATALOGVALUES_LIMIT;
		//@JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		if (getMDVersion() >= esvNavigationHistory) {
			$this->ArrayVariables[44] = 'ENABLENAVHISTORY';
			$this->ArrayVariableValues['ENABLENAVHISTORY'] = 0;
		}
		//@JAPR 2016-08-22: Agregada la configuración para ajustar las imagenes del menú de formas al total del área usada sin importar la escala (#Y3ATJH)
		if (getMDVersion() >= esvAutoFitFormsImage) {
			$this->ArrayVariables[45] = 'AUTOFITIMAGE';
			$this->ArrayVariableValues['AUTOFITIMAGE'] = 0;
		}
		//@JAPR
		//MAPR 2018-02-19: A petición de DAbdo ahora el push se mandará a todos los dispositivos que usen la misma cuenta.
		if (getMDVersion() >= esvAllMobile) {
			$this->ArrayVariables[46] = 'ALLMOBILE';
			$this->ArrayVariableValues['ALLMOBILE'] = 0;
		}
		//MAPR 2018-05-21: Se integra la configuración para el modo en segundo plano activado
		if ( getMDVersion() >= esvBackgroundMode ) {
			$this->ArrayVariables[47] = 'BACKGROUNDMODE';
			$this->ArrayVariableValues['BACKGROUNDMODE'] = 0;
		}
		//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
		//OMMC 2019-01-19: Comentado para que no pueda moverse el setting de mapas
		// if ( getMDVersion() >= esvMapTypeSelector ) {
		// 	$this->ArrayVariables[48] = 'MAPTYPE';
		// 	$this->ArrayVariableValues['MAPTYPE'] = 1;
		// }
		//@JAPR 2019-05-06: Modificado para mostrar valores dummy en los atributos en lugar de realizar la consulta real a los catálogos (#270TYE)
		//Esta es una configuración interna, NO debe ser visible para el usuario, sólo se puede configurar bajo demanda vía el equipo de Soporte / Provisioning
		//$this->ArrayVariables[56] = 'DISABLEPREVIEWDUMMIES';
		//$this->ArrayVariableValues['DISABLEPREVIEWDUMMIES'] = 1;
		//@JAPR 2019-06-19: Agregada la configuración para olvidar el último login al App y solicitad credenciales nuevamente al ingresar (#GHEM2Z)
		if ( getMDVersion() >= esvForgetCredentials ) {
			$this->ArrayVariables[58] = 'FORGETCREDENTIALS';
			$this->ArrayVariableValues['FORGETCREDENTIALS'] = 0;
		}
		//@JAPR
	}
	
	static function NewInstanceAll($aRepository)
	{
			
		$anInstance = null;
		$sql = "SELECT SettingID, SettingName, SettingValue FROM SI_SV_Settings Order By SettingID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Settings ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstanceTemp = $strCalledClass::NewInstance($aRepository);
		$anInstanceTemp->resetDefaultSettings();
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesExist = array();
		
		while(!$aRS->EOF)
		{
			$variableId = $aRS->fields["settingid"];
			$variableName = $aRS->fields["settingname"];
			$SettingValue = $aRS->fields["settingvalue"];
			$ArrayVariables[$variableId] = $variableName;
			$ArrayVariableValues[$variableName]=$SettingValue;
			$ArrayVariablesExist[$variableName]=true;
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMSettingsVariableExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMSettingsVariableExtended extends '.$strCalledClass."\n";
		$strtemp = $strtemp.'	{'."\n";
		foreach($ArrayVariables as $variable)
		{   
			$variableValue=$ArrayVariableValues[$variable];
			//$strtemp = $strtemp.'public $'.$variable.' = "'.$variableValue.'";'."\n";
			$strtemp = $strtemp.'public $'.$variable.' = "'.str_replace('"', "'", $variableValue).'";'."\n";
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (isset($anInstanceTemp->ResponsiveDesignProps[$variable])) {
				//En este caso la propiedad aplica para Responsive Design, por tanto se prepara la instancia de captura agregando una propiedad
				//dinámica para cada dispositivo
				for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					//Se limpiará el valor porque aquí sólo se crea la instancia, posterior a invocar a este método se deberá invocar el que
					//va a sobreescribir las propiedades basado en que la instancia es la extendida
					$variableValue = '';
					$strtemp = $strtemp.'public $'.$variable.$strDeviceName.' = "'.str_replace('"', "'", $variableValue).'";'."\n";
				}
			}
			//@JAPR
		}
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp=addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMSettingsVariableExtended($aRepository);'."\n";
		$strtemp=addcslashes( $strtemp, '\\');
		eval($strtemp);
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		
		return($theobject);
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		if (array_key_exists("SettingID", $aHTTPRequest->POST))
		{
			$anInstance = $strCalledClass::NewInstanceAll($aRepository);
			
			//guardar nombre anterior de la imagen y en el save comparar los nombres, si si incrementar version
			//Conchita 2014-07-07
			$objSetting = $strCalledClass::NewInstanceWithName($aRepository, 'BACKGROUNDIMAGE');
			if (!is_null($objSetting)) {
				 $anInstance->oldBackground =(string) @$objSetting->SettingValue;
			}
			$anInstance->updateFromArray($aHTTPRequest->POST);
			$aResult = $anInstance->save();
			
			if ($aResult !== 0)
			{
				//$anInstance->ReportErrorAndExit($aResult);
			}

			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}

		$anInstance = $strCalledClass::NewInstanceAll($aRepository);
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (!is_null($anInstance)) {
				if (getMDVersion() >= esvResponsiveDesign) {
					$strCalledClass::GetResponsiveDesignProps($aRepository, $anInstance);
				}
			}
		}
		//@JAPR
		
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		if (array_key_exists("CustomizedColors", $anArray)) {
			$this->CustomizedColors = $anArray["CustomizedColors"];
		}
		//@JAPR
		
		foreach($this->ArrayVariables as $variable)
		{
			if (array_key_exists($variable, $anArray))
			{
				$this->$variable = $anArray[$variable];
			}
			
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if (isset($this->ResponsiveDesignProps[$variable])) {
				//En este caso la propiedad aplica para Responsive Design, por tanto se prepara la instancia de captura agregando una propiedad
				//dinámica para cada dispositivo
				for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					$strProperty = $variable.$strDeviceName;
					$this->$strProperty = @$anArray[$strProperty];
				}
			}
			//@JAPR
		}
		return $this;
	}

	function save()
	{
		//@JAPR 2014-08-05: Agregado el Responsive Design
		//Se cargará la instancia dinámica para permitir comparar si los valores de las propiedades de responsive design han cambiado
		//@JAPR 2015-06-24: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstanceOld = null;
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		//Estas propiedades están descontinuadas a partir de eForms v6
		if (getMDVersion() < esveFormsv6 ) {
			if (getMDVersion() >= esvResponsiveDesign) {
				$anInstanceOld = $strCalledClass::NewInstanceAll($this->Repository);
				if (!is_null($anInstanceOld)) {
					$strCalledClass::GetResponsiveDesignProps($this->Repository, $anInstanceOld);
				}
			}
		}
		//@JAPR
		
		foreach($this->ArrayVariables as $variableID => $variable)
		{
			$sql="";
			$variableValue=$this->$variable;
			
			if (isset($this->ArrayVariablesExist[$variable])) {
				//Conchita 2014-07-08 Verificamos si es el backgroundimage y si esta modificado
				$blnResponsiveDesignProp = false;
				if ($variable == 'BACKGROUNDIMAGE') {
					//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
					//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
					//el menú de 3 rayas del App (#EJTH2E)
					if (is_null($variableValue)) {
						$variableValue = '';
					}
					//@JAPR
					
					//revisar q la url no este en la nueva (si esta quiere decir que no se modifico y no se incrementa version
					if (@strpos($variableValue, $this->oldBackground) === false) {
						$this->incrementVersion = true; //quiere decir que tenemos que incrementar version
					}
					//Corregido el bug de que cuando no habia imagen y lo dejabas asi incrementaba version
					if ($variableValue ==='' && $this->oldBackground ==='') {
						$this->incrementVersion = false;
					}
				}
				
				if($variable == 'BACKGROUNDIMAGEVERSION'){
					if($this->incrementVersion == true){
						//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
						//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
						//el menú de 3 rayas del App (#EJTH2E)
						if (is_null($variableValue)) {
							$variableValue = 0;
						}
						//@JAPR
						
						$variableValue = $variableValue + 1;
					}
				}
				
				//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
				//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
				//el menú de 3 rayas del App (#EJTH2E)
				$sql = "UPDATE SI_SV_Settings SET SettingValue = ".((is_null($variableValue))?"NULL":$this->Repository->DataADOConnection->Quote($variableValue))." WHERE ";
				//@JAPR
				$sql .= "SettingName=".$this->Repository->DataADOConnection->Quote($variable);
			}
			else {
				//@JAPR 2017-01-06: Corregido un bug, la configuración debe tener NULL por default, de tal manera que se asume todas las configuraciones habilitadas por default
				//y así todas las opciones aparezcan seleccionadas automáticamente, que es diferente a si explícitamente fuera vacía lo que significaría que ni siquiera se debería ver
				//el menú de 3 rayas del App (#EJTH2E)
				$sql = "INSERT INTO SI_SV_Settings (SettingID, SettingName, SettingValue) VALUES (".$variableID.", ".
					$this->Repository->DataADOConnection->Quote($variable).", ".
					((is_null($variableValue))?"NULL":$this->Repository->DataADOConnection->Quote($variableValue)).")";
				//@JAPR
			}
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Settings ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			//@JAPR 2014-08-05: Agregado el Responsive Design
			if ($anInstanceOld && isset($this->ResponsiveDesignProps[$variable])) {
				//En este caso compara pero contra el valor específico de la configuración para Responsive Design
				for ($intDeviceID = dvciPod; $intDeviceID <= dvcTablet; $intDeviceID++) {
					$strDeviceName = getDeviceNameFromID($intDeviceID);
					$strProperty = $variable.$strDeviceName;
					
					$strOldValue = (string) @$anInstanceOld->$strProperty;
					$strNewValue = (string) @$this->$strProperty;
					if ($strNewValue != $strOldValue) {
						$this->incrementVersion = true;
						$strEmpty = $this->Repository->DataADOConnection->Quote("");
						$strDisplayImage = $this->Repository->DataADOConnection->Quote($strNewValue);
						if ((int) @$anInstanceOld->ResponsiveDesignProps[$strProperty][$intDeviceID]) {
							$sql = "UPDATE SI_SV_SurveyHTML SET 
									FormattedText = {$strEmpty} 
									, HTMLHeader = {$strEmpty} 
									, HTMLFooter = {$strEmpty} 
									, ImageText = {$strDisplayImage} 
								WHERE ObjectType = ".otySetting." AND DeviceID = {$intDeviceID} AND ObjectID = {$variableID}";
						}
						else {
							$sql = "INSERT INTO SI_SV_SurveyHTML (SurveyID, ObjectType, ObjectID, DeviceID, FormattedText, HTMLHeader, HTMLFooter, ImageText) 
								VALUES (0, ".otySetting.", {$variableID}, {$intDeviceID}, 
									{$strEmpty}, {$strEmpty}, {$strEmpty}, {$strDisplayImage})";
						}
						$this->Repository->DataADOConnection->Execute($sql);
					}
				}
			}
			//@JAPR
		}
	}
	
	function ReportErrorAndExit($anError)
	{
		switch ($anError)
		{
			case -4: // There was an error while saving the file
				$strErrorMsg = translate("There was an error while saving the file");
				break;
			case -3: // the file exists and "Overwrite" checkbox at client was not checked
				$strErrorMsg = translate("The file already exists");
				break;
			case -2: // budget dir could not be created
				$strErrorMsg = translate("Folder not found").": documents ".translate("and")." ".translate("Could not create folder:")." documents";
				break;
			case -1: // docs dir could not be found
				$strErrorMsg = translate("Folder not found").": documents";
				break;
			case 1: // upload_max_filesize directive in php.ini exceeded
				$strErrorMsg = translate("The uploaded file exceeds the upload_max_filesize directive in php.ini");
				break;
			case 2: // MAX_FILE_SIZE directive in HTML form exceeded
				$strErrorMsg = translate("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form");
				break;
			case 3: // The uploaded file was only partially uploaded
				$strErrorMsg = translate("The uploaded file was only partially uploaded");
				break;
			case 4: // No file was uploaded
				$strErrorMsg = translate("No file was uploaded");
				break;
			case 5: //File not uploaded
				$strErrorMsg = translate("File not uploaded");
				break;
			case 6: // Missing a temporary folder
				$strErrorMsg = translate("Missing a temporary folder at server");
				break;
			case 7: // Failed to write file to disk
				$strErrorMsg = translate("Failed to write file to disk");
				break;
			case 8: // File upload stopped by extension
				$strErrorMsg = translate("File upload stopped by invalid extension");
				break;
			default:
				$strErrorMsg = translate("Unknown error");
		}

		$strResponseCode = "<html><body>"
		."<br>"
		."<center>"
		."<span style=\"font-family:MS Shell Dlg; font-size:13pt; font-weigth:bold; color:#003366\">"
		.$strErrorMsg
		."</span>"
		."<br><br>"
		."<a href=\"javascript:window.location=document.referrer;\" style=\"font-family:MS Shell Dlg;font-size:11pt\">".translate("Back")."</a>"
		."</center>"
		."</body></html>";
		
		echo $strResponseCode;
		
		exit();
	}	
	
	function isNewObject()
	{
		return false;
	}

	function get_Title()
	{
		return translate("Settings Variables");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=SettingsVariable";
	}

	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SettingID';
	}
	
	
	function get_FormFields($aUser)
	{
		global $gbIsGeoControl;
		$trace = @debug_backtrace();
		PrintMultiArray($trace);
		die('FormDFieldsSettings');
		require_once("formfield.inc.php");

		$myFields = array();
		$categoryCatalogField = null;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CustomizedColors";
		$aField->Title = translate("Color templates");
		$aField->Type = "String";
		$aField->Size = 255;
		//$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		/*@AAL 18/05/2015 Issue #HZ9V7Z: Estas opciones (AGENDARADIO Y FORMSMENUPOSITION) Nunca se terminaron de implementar por lo tanto 
		se remueven del arreglo para no estar visibles para el usuario.*/
		unset($this->ArrayVariables[23]);
		unset($this->ArrayVariables[25]);
		
		foreach($this->ArrayVariables as $variableID=>$variable)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $variable;
			$aField->Title = translate($variable);

			if ($variableID == 22) {
				continue;
			}
			
			//@JAPR 2014-07-09: Validación para Ciudadano Vigilante y GeoControl (IUSACELL)
			if ($gbIsGeoControl) {
				$blnValidSetting = true;
				//Las siguientes configuraciones no aplican para estos servicios
				switch ($variableID) {
					case 5:
					case 6:
					case 7:
					case 8:
					case 11:
					case 13:
						$blnValidSetting = false;
						break;
				}
				
				if (!$blnValidSetting) {
					continue;
				}
			}
			//@JAPR
			
			switch($variableID)
			{
				case 2:			// Catálogo de Categorías
					require_once('catalog.inc.php');
					//Primero obtiene los valores actuales
					$intCurrentCatalogID = (int) $variable;
					
					//Muestra la lista de todos los catálogos a los que tiene acceso el usuario logeado, incluyendo el previamente seleccionado
					//por si él no tiene acceso
					$arrCatalogs = BITAMCatalog::getCatalogs($this->Repository, true);
					
					//Verifica si el catálogo actualmente seleccionado está dentro de la lista de catálogos del usuario, si no está lo agrega como
					//dummy
					if ($intCurrentCatalogID > 0)
					{
						if (!isset($arrCatalogs[$intCurrentCatalogID]))
						{
							$arrCatalogs[$intCurrentCatalogID] = 'Current catalog';
						}
					}
					
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrCatalogs;
					$categoryCatalogField = $aField;
					break;
					
				case 3:			// Atributos del catálogo de categorías
					require_once('surveyAgenda.inc.php');
					
					//Si por alguna razón la configuración del catálogo de categoría no estaba asignada, se tiene que cargar el campo con la lista
					//y resetear la configuración del miembro del catálogo usado
					if (is_null($categoryCatalogField))
					{
						$intCurrentCatalogID = 0;
						$variable = 0;
						require_once('catalog.inc.php');
						$categoryCatalogField = BITAMFormField::NewFormField();
						$categoryCatalogField->Name = 'CATEGORYCATALOG';
						$categoryCatalogField->Title = translate('CATEGORYCATALOG');
						$categoryCatalogField->Type = "Object";
						$categoryCatalogField->VisualComponent = "Combobox";
						$categoryCatalogField->Options = BITAMCatalog::getCatalogs($this->Repository, true);;
						$myFields[$categoryCatalogField->Name] = $categoryCatalogField;
					}
					else 
					{
						$intCurrentCatalogID = (int) @$this->ArrayVariableValues['CATEGORYCATALOG'];
					}
					
					//Primero obtiene los valores actuales
					$intCurrentMemberID = (int) $variable;
					
					//Muestra la lista de todos los catálogos a los que tiene acceso el usuario logeado, incluyendo el previamente seleccionado
					//por si él no tiene acceso
					$arrUserCatalogMembers = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository);
					
					//Verifica si el catálogo actualmente seleccionado está dentro de la lista de catálogos del usuario, si no está lo agrega como
					//dummy
					if ($intCurrentCatalogID > 0)
					{
						if (!isset($arrUserCatalogMembers[$intCurrentCatalogID]))
						{
							$arrUserCatalogMembers[$intCurrentCatalogID] = array($intCurrentMemberID => 'Current member');
						}
					}
					
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrUserCatalogMembers;
					$aField->Parent = $categoryCatalogField;
					$categoryCatalogField->Children[] = $aField;
					break;
					
				case 11:			// Utilizar agregaciones incrementales
					$arrBoolean = array(0 => translate('No'), 1 => translate('Yes'));
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrBoolean;
					break;

				//@JAPR 2013-12-17: Cambiado el modo de uso del GPS
				case 12:			// Variar el momento de registro de la posición GPS
					$arrOptions = array(0 => translate('When saving data entry'), 1 => translate('When starting data entry'));
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrOptions;
					break;
				//@JAPR
				
				case 14:
					require_once('survey.inc.php');
					$intCurrentSurveyID = (int) $variable;
					$arrSurveys = array();
					array_push($arrSurveys, "None");
					$loadedSurveys = BITAMSurvey::getSurveys($this->Repository, true);
					foreach($loadedSurveys as $skey => $asurvey) {
						$arrSurveys[$skey] = $asurvey;
					}
					
					if ($intCurrentSurveyID > 0)
					{
						if (!isset($arrSurveys[$intCurrentSurveyID]))
						{
							$arrSurveys[$intCurrentSurveyID] = 'Current survey';
						}
					}
					
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrSurveys;
					break;
					
				//@JAPR 2014-06-18: Agregados los templates para personalización del App
				case 15:			// Utilizar agregaciones incrementales
					$arrBoolean = array(0 => translate('No'), 1 => translate('Yes'));
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrBoolean;
					break;
					
				case 16:
					$intTemplateID = (int) $variable;
					$arrColorTemplates = array();
					$arrColorTemplates[-1] = translate("None");
					//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
					$objColorTemplatesColl = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
					if (!is_null($objColorTemplatesColl)) {
						foreach($objColorTemplatesColl->Collection as $objColorTemplate) {
							$arrColorTemplates[$objColorTemplate->TemplateID] = $objColorTemplate->TemplateName;
						}
					}
					
					if ($intTemplateID > 0)
					{
						if (!isset($arrColorTemplates[$intTemplateID]))
						{
							$arrColorTemplates[$intTemplateID] = translate("Current template");
						}
					}
					
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrColorTemplates;
					break;
				
				case 17: 
					//Conchita 2014-07-02 tiny mce para la imagen de fondo
					//@JAPR 2014-08-04: Agregado el Responsive Design
					if (getMDVersion() >= esvResponsiveDesign) {
						$arrDevices = array();
						$arrDevices[] = getDeviceNameFromID(dvciPod);
						$arrDevices[] = getDeviceNameFromID(dvciPad);
						$arrDevices[] = getDeviceNameFromID(dvciPadMini);
						$arrDevices[] = getDeviceNameFromID(dvciPhone);
						$arrDevices[] = getDeviceNameFromID(dvcCel);
						$arrDevices[] = getDeviceNameFromID(dvcTablet);
						$aField->MultiDeviceNames = $arrDevices;
					}
					
					$aField->Type = "LargeStringHTML";
					$aField->Size = 5000;
					break;
					//Conchita
				
				case 23:			
					$arrOptions = array(
						0 => translate('Center'), 
						1 => translate('Left'),
						2 => translate('Right')
					);
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrOptions;
					break;
				case 24: 
					//Conchita 2014-09-03 header personalizado
					$aField->Title = translate("Custom Header");
					$aField->Type = "String";
					$aField->Size = 500;
					break;
				case 25:
					$aField->Type = "Integer";
					break;
				case 26:
					$arrOptions = array(
						0 => translate('No'), 
						1 => translate('Yes')
					);
					$aField->Type = "Object";
					$aField->VisualComponent = "Combobox";
					$aField->Options = $arrOptions;
					break;

				case 27:
					$aField->Title = translate("Number of cache requests");
					$aField->Type = "String";
					$aField->Size = 500;
					break;
				
				//OMMC 2015-12-15: Agregado para ocultar descripción de las formas en el admin. HIDEDESCRIPTION
				//@JAPR 2016-01-18: Corregido un bug, el siguiente consecutivo se había reutilizado de una configuración del Agente, así que se cambiará al siguiente ID en general
				case 30:
					if(getMDVersion() >= esvHideDescription){
						$arrBoolean = array(0 => translate('No'), 1 => translate('Yes'));
						$aField->Type = "Object";
						$aField->VisualComponent = "Combobox";
						$aField->Options = $arrBoolean;
					}
					break;
				
				case 31:
					//GCRUZ 2016-01-21. Opción para hora de ejecución de destinos incrementales
					if (getMDVersion()>=esvDestinationIncremental)
					{
					}


				default:
				{
					$aField->Type = "String";
					$aField->Size = 255;
					break;
				}
			}
			
			$myFields[$aField->Name] = $aField;
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
 	function addButtons($aUser)
	{
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		//@JAPRDescontinuado
		/*
		if($_SESSION["PABITAM_UserRole"]==1 && getMDVersion() >= esvTemplateStyles)
		{
?>
			<button id="colorsBtn" class="alinkescfav" onclick="javascript: customizeApp();"><img src="images/copy.gif" alt="<?=translate("Color templates")?>" title="<?=translate("Color templates")?>" displayMe="1" /> <?=translate("Color templates")?></button>
<?
		}
		*/
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		$myFormName = get_class($this);
		
		//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		if (getMDVersion() >= esvTemplateStyles) {
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
		<?//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
		//@JAPRDescontinuado?>
		/*function customizeApp()
		{
			openWindowDialog('main.php?BITAM_PAGE=EFormsAppCustomization&TemplateID=-1&UserID=-1', [window], [], 600, 600, customizeAppDone, 'customizeAppDone');
		}
		
		function customizeAppDone(sValue, sParams)
		{
			var strInfo = sValue;
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.CustomizedColors.value = strInfo;
				} catch (e) {
				}
			}
		}
		*/
		<?//@JAPR?>
	</script>
<?
		}
 	}
 	
	function get_Children($aUser)
	{
		$myChildren = array();
		
		if (!$this->isNewObject())
		{
			//@JAPR 2016-03-15: Agregados los templates de estilos personalizados de v6
			if (getMDVersion() >= esvTemplateStyles) {
				//@JAPR 2016-02-26: Agregados los templates de estilos personalizados de v6
				$myChildren[] = BITAMEFormsAppCustomStylesTplCollection::NewInstance($this->Repository);
			}
		}
		
		return $myChildren;
	}
}
?>