<?php
require 'libs/Google/autoload.php';
require_once "libs/Google/Service/Oauth2.php";
require_once "eFormsDataDestinations.inc.php";
define('APPLICATION_NAME', 'Drive API Quickstart');
define('CLIENT_SECRET_PATH_DRIVE', __DIR__ . '/'.'libs/Google/client_secret_gDrive.json');
define('SCOPES', implode(' ', array('https://www.googleapis.com/auth/drive','email','profile')));

class googleDriveConnection {
    //Funcion para crear la coneccion y establecer los permisos de la aplicacion de eForms con la cuenta de googleDrive
    
    /** @var string Nombre de la conexion, debe ser el email */
    public $name = '';

    /** @var int Id de la conexion en la base de datos */
    public $id;

    static function create(){
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfigFile(CLIENT_SECRET_PATH_DRIVE);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');

        $sCallbackURL = 'http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . '?' . 'BITAM_SECTION=eFormsDataDestinationsCollection&action=validateGoogle';

        $client->setRedirectUri( ($sCallbackURL) );
        $authUrl = $client->createAuthUrl();

        header('location:'.$authUrl);
        exit;
    }
    function getAuthorizationUrl($emailAddress, $state=null) {
      global $CLIENT_ID, $REDIRECT_URI, $SCOPES;
      $client = new Google_Client();

      $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfigFile(CLIENT_SECRET_PATH_DRIVE);
      $client->setAccessType('offline');
      $client->setApprovalPrompt('force');
      //$client->setState($state);
      //$client->setScopes($SCOPES);
      $tmpUrl = parse_url($client->createAuthUrl());
      $query = explode('&', $tmpUrl['query']);
      $query[] = 'user_id=' . urlencode($emailAddress);
      return
          $tmpUrl['scheme'] . '://' . $tmpUrl['host'] . $tmpUrl['port'] .
          $tmpUrl['path'] . '?' . implode('&', $query);
    }
    function getUserInfo($credentials){
      $apiClient = new Google_Client();
      //$apiClient->setUseObjects(true);
      $apiClient->setAccessToken($credentials);      
      $userInfoService = new Google_Service_Oauth2($apiClient);

      $userInfo = null;
      try {
        $userInfo = $userInfoService->userinfo->get();
      } catch (Google_Exception $e) {
        print 'An error occurred: ' . $e->getMessage();
      }
      if ($userInfo != null && $userInfo->getId() != null) {
        return $userInfo;
      } else {
        throw new NoUserIdException();
      }
    }
    function exchangeCode($authorizationCode){
        try {
            //global $CLIENT_ID, $CLIENT_SECRET, $REDIRECT_URI;
            $client = new Google_Client();
            $client->setApplicationName(APPLICATION_NAME);
            $client->setScopes(SCOPES);
            $client->setAuthConfigFile(CLIENT_SECRET_PATH_DRIVE);
            $client->setRedirectUri('http' . ( $_SERVER['SERVER_NAME'] == 'localhost' ? '' : 's' ) . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME'] . '?' . 'BITAM_SECTION=eFormsDataDestinationsCollection&action=validateGoogle');
            //$client->setAccessType('offline');
            //$client->setApprovalPrompt('force');
            //echo $client->authenticate($authorizationCode);
            //$_GET['code'] = $authorizationCode;
            $client->authenticate($authorizationCode);

            return $client->getAccessToken();

        }catch(Google_Auth_Exception $e){
            print 'An error occurred in exchangeCode: ' . $e->getMessage();
            throw new CodeExchangeException(null);
        }
    }
    function getCredentials($authorizationCode,$emailAddress=null,$state=null){
        try {
            if( $authorizationCode ){
                $credentials = $this->exchangeCode($authorizationCode);
                $userInfo = $this->getUserInfo($credentials);
                $emailAddress = $userInfo->getEmail();
                $userId = $userInfo->getId();
                $credentialsArray = json_decode($credentials, true);
                /**
                 * Guardamos siempre la conexion en la base de datos pues utilizan la misma cuenta de GDRIVE en diferentes bases de datos.
                 */
                if (isset($credentialsArray['refresh_token'])){
                    $this->storeCredentials($emailAddress, $credentials);
                    return $credentials;
                }
            }else{
                $credentials = $this->getStoredCredentials($emailAddress);
                $credentialsArray = json_decode($credentials, true);
                if ($credentials != null /*&& isset($credentialsArray['refresh_token'])*/) {
                    return $credentials;
                }else{
                    return null;
                }
            }
        }catch(CodeExchangeException $e){
            return null;
            print 'An error occurred during code exchange.';
            // Drive apps should try to retrieve the user and credentials for the current
            // session.
            // If none is available, redirect the user to the authorization URL.
            //$e->setAuthorizationUrl(getAuthorizationUrl($emailAddress, $state));
            //throw $e;
        }catch(NoUserIdException $e){
            return null;
            //print 'No e-mail address could be retrieved.';
        }
        //-??
        // No refresh token has been retrieved.
        //$authorizationUrl = getAuthorizationUrl($emailAddress, $state);
        //throw new NoRefreshTokenException($authorizationUrl);
    }
    function getStoredCredentials( $emailAddress ){
        try {

            if( is_numeric( $emailAddress ) )
                $rs = $this->Repository->DataADOConnection->Execute( 'SELECT `access_token`, `name`, `id` FROM si_sv_servicesconnection WHERE `type` = 3 AND `id` = ' . $emailAddress);
            else
                $rs = $this->Repository->DataADOConnection->Execute( 'SELECT `access_token`, `name`, `id` FROM si_sv_servicesconnection WHERE `type` = 3 AND `name` = ' . $this->Repository->DataADOConnection->Quote( $emailAddress ) );
			
            if( $rs && !$rs->EOF )
            {
                $this->id = $rs->fields['id'];
                $this->name = $rs->fields['name'];
                return $rs->fields['access_token'];
            }

            return null;
        }catch (Google_Exception $e) {
            return null;
            //print 'An error occurred: ' . $e->getMessage();
        }
    }

    /**
     * Almacena en la base de datos el token para acceder a la aplicacion en forma offline
     * @param  string $emailAddress El email de la cuenta de google
     * @param  string $credentials  El token en formato json
     * @return mixed                Regresa la llave del registro en la base de datos o false si no pudo guardar la informacion
     */
    function storeCredentials($emailAddress,$credentials) {

        $this->name = $emailAddress;

        if( $id = BITAMeFormsDataDestinations::saveConnection( ['email' => $emailAddress, 'access_token' => $credentials, 'type' => 3], $this->Repository ) ) {

            $this->id = $id;

            return $this->id;
        } else {
            return false;
        }
    }

    function uploadFile($userAccount,$fileOrign, $filename = null,$realPath=null){
        $credentials=$this->getCredentials(null,$userAccount);
        if(isset($credentials)){
            try{
                $service=$this->buildService($credentials);
                if($service==null)
                    return(array('error' =>1 ,'msg' =>"No se pudo establecer el servicio con googleDrive" ));

				//GCRUZ 2016-02-02. Buscar primero si el archivo ya existe, si existe sobreescribirlo
				//GCRUZ 2016-02-04. Quitar el filtro de b�squeda por mimeType
				if((string) $realPath != ''){
					$queryString="trashed=false and '".$realPath."' in parents and title='".basename($filename)."' and mimeType != 'application/vnd.google-apps.folder'";
				}
				else
				{
					$queryString="trashed=false and  title='".basename($filename)."' and mimeType != 'application/vnd.google-apps.folder'";
				}
				$parameters = array();
				$parameters["q"]= $queryString;
	            $files = $service->files->listFiles($parameters);
				$bFileFound = false;
				$fileFound = null;
				if(count($files->items)>0){
					$modifiedDateFile=null;
					$bFileFound = true;
					foreach ($files->items as $aItem){
						if(!isset($modifiedDateFile) || $modifiedDateFile < strtotime($aItem->modifiedDate)){
							$fileFound=$aItem;
							$modifiedDateFile=strtotime($aItem->modifiedDate);
						}
					}
				}

				if (!$bFileFound)
				{
					$file= new Google_Service_Drive_DriveFile();

					if( $filename )
						$file->title = $filename;
					else
						$file->title = $fileOrign;


					$chunkSizeBytes = 1 * 1024 * 1024;

					// Call the API with the media upload, defer so it doesn't immediately return.
					//$client->setDefer(true);
					$finfo = finfo_open(FILEINFO_MIME_TYPE);
					//$request = $service->files->insert($file);
					$result = $service->files->insert(
						$file,
						array(
							'data' => file_get_contents($fileOrign),
							'mimeType' =>finfo_file($finfo, $fileOrign),
							'uploadType' => 'media'
							)
						);
					if(isset($realPath))
						$this->insertFileIntoFolder($service,$realPath,$result->id);
					
					finfo_close($finfo);
				}
				else
				{
					// First retrieve the file from the API.
					$fileUpdate = $service->files->get($fileFound->id);
					// File's new metadata.
					// File's new content.
					$data = file_get_contents($fileOrign);
					$additionalParams = array(
						'newRevision' => false,
						'data' => $data,
						'uploadType' => 'media'
					);

					// Send the request to the API.
					$updatedFile = $service->files->update($fileFound->id, $fileUpdate, $additionalParams);

				}
                return(array('error' =>0 ,'msg' =>"Se envio el archivo a la cuenta de: ".$userAccount));
            }catch (GoogleDrive_Exception $e) {
                return(array('error' =>1 ,'msg' =>"No se envio el archivo al destino de datos" ));
            }
        }else{
            return(array('error' =>1 ,'msg' =>"No se pudieron obtener las credenciales de googleDrive" ));
        }
    }
    function insertFileIntoFolder($service, $folderId, $fileId) {
        $newChild = new Google_Service_Drive_ChildReference();
        $newChild->setId($fileId);
        try {
            return $service->children->insert($folderId, $newChild);
        } catch (Exception $e) {
            print "An error occurred: " . $e->getMessage();
        }
    }
    function downloadFile($userAccount,$fileOrign,$filename=null, $realPath = null){

        $credentials=$this->getCredentials(null,$userAccount);
        if( $credentials ) {
            $parameters = array();
            $service=$this->buildService($credentials);
            $optParams = array(
              'maxResults' => 10,
            );
            //EVEG se cambio el parametro que recibe la funcion ahora el parametro fileOrign no es el id del archivo a descargar si no es la carpeta del archivo
            if((string) $realPath != ''){
				//GCRUZ 2016-02-04. Quitar el filtro de b�squeda por mimeType
				$queryString="trashed=false and '".$realPath."' in parents and title='".basename($fileOrign)."' and mimeType != 'application/vnd.google-apps.folder'";
            }else{
                $queryString="trashed=false and title='".basename($filename)."' and mimeType != 'application/vnd.google-apps.folder'";
            }
            $parameters["q"]= $queryString;
            $files = $service->files->listFiles($parameters);

            if(count($files->items)>0){
                $modifiedDateFile=null;
                foreach ($files->items as $aItem){
                    if(!isset($modifiedDateFile) || $modifiedDateFile < strtotime($aItem->modifiedDate)){
                        $file=$aItem;
                        $modifiedDateFile=strtotime($aItem->modifiedDate);
                    }
                }
            }
            //EVEG se obtiene el file por su id
            /*try{
                $file = $service->files->get($fileOrign);
            }catch(Google_Service_Exception $e){
                return array('success' => false, 'file' => $fileOrign,'error'=>$e->getMessage());
            }
            if($file["explicitlyTrashed"]){
                return array('success' => false, 'file' => $fileOrign,'error'=>'The file is in the trash');   
            }*/
            if(isset($file)){
                $title = $file->title;
                $downloadUrl = $file->getDownloadUrl();
                if (!$downloadUrl) {
                    $exportLinks = $file->getExportLinks();
                    switch ($file->getMimeType()) {
                        //case self::FILE_TYPE_SPREADSHEET:
                        //case self::FILE_TYPE_FORM:
                        case "application/vnd.google-apps.spreadsheet":
                            $exportFormat = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                            $title=str_replace(".xlsx", ".xls", $title);
                            $title=str_replace(".xls", ".xlsx", $title);
                            break;
                        /*case self::FILE_TYPE_DOCUMENT:
                            $exportFormat = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
                            break;
                        case self::FILE_TYPE_PRESENTATION:
                            $exportFormat = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
                            break;*/
                        default:
                            $keys = array_keys($exportLinks);
                            $exportFormat = $keys[0];
                            break;
                    }

                    $downloadUrl = $exportLinks[$exportFormat];
                }

                // Print the names and IDs for up to 10 files.
                if ($downloadUrl){
                    $request = new Google_Http_Request($downloadUrl, 'GET', null, null);
                    $httpRequest = $service->getClient()->getAuth()->authenticatedRequest($request);
                    if ($httpRequest->getResponseHttpCode() == 200) {
                        //@JAPR 2015-09-08: Corregido un bug, crea la ruta para descarga del archivo local
                        $strFilePath = "loadexcel/uploads/Datasources/googledrive/";
                        if (!file_exists($strFilePath)) {
                            @mkdir($strFilePath, null, true);
                        }
                        $local_file = $strFilePath.$title;
                        //@JAPR
                        $fileD = fopen($local_file, "w+b");
                        fwrite($fileD, $httpRequest->getResponseBody());
                        fclose($fileD);
                        return array('success' => true, 'file' => $title);
                    }else {
                        return array('success' => false, 'file' => $fileOrign);
                    }
                }else{
                    return array('success' => false, 'file' => 'robert de popo');
                }
            }
        }
    }

    public function getClient( $credentials = null ) {

        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfigFile(CLIENT_SECRET_PATH_DRIVE);
        $client->setAccessType('offline');

        if( $credentials ) {
            
            $client->setAccessToken($credentials);

            /** Si el token a expirado, refrescamos el token y lo volvemos a guardar */
            if ($client->getAuth()->isAccessTokenExpired()) {
                $client->getAuth()->refreshToken( $client->getRefreshToken() );

                $userInfo = $this->getUserInfo( $client->getAccessToken() );

                $this->storeCredentials($userInfo->getEmail(), $client->getAccessToken());
            }
        }

        return $client;

    }

    public function buildService($credentials){
        
        try {
                        
            return new Google_Service_Drive( $this->getClient( $credentials ) );

        } catch (Google_Drive_Exception $e) {
            return null;
        }
        
    }

    function getFileID($listFiles,$fileOrign){
        foreach ($listFiles->items as $aObjFile){
            if($aObjFile->title==$fileOrign){
                return ($aObjFile->id);
            }
        }
    }
    function createFolder($client,$folderName){
        $service = new Google_Service_Drive($client);
        $file= new Google_Service_Drive_DriveFile();
        $file->title = $folderName;
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $file->setMimeType('application/vnd.google-apps.folder');
        $result = $service->files->insert(
            $file,
            array(
                'mimeType' =>'application/vnd.google-apps.folder'
                )
            );
        finfo_close($finfo);
    }

    function getTreeFiles($idAccount, $folderId=null) {
        
        $credentials = $this->getCredentials(null, $idAccount); 
        
        if(isset($credentials)) {

            $parameters = array();
            
            $strHTMLTreeFolders = '';
            
            $service = $this->buildService($credentials);

            if( $folderId ) {
                $queryString = "trashed=false and '{$this->name}' in owners and '{$folderId}' in parents";
            } else {
                //@JAPR 2018-06-07: Corregido un bug, Google Drive cambi� la manera en que funciona su API (v2) as� que dej� de ser compatible este c�digo (#VLMNXW)
                //Agregado expl�citamente el root folder para obtener los archivos de raiz en cualquier tipo de cuenta (hab�a algunas cuentas donde se ten�a que
                //usar getParents pero en la colecci�n obtenida de esa manera no estaba asignada la propiedad isRoot, por tanto no mostraba los archivos ni carpetas)
                $queryString = "trashed=false and '{$this->name}' in owners and 'root' in parents";
                //@JAPR
            }

            if (!isset($this->showfiles)){
                $queryString .= " and mimeType='application/vnd.google-apps.folder'";
            }

            $parameters["q"]= $queryString;

            $files = $service->files->listFiles($parameters);

            $count = 0;
            
            if(count($files->items)>0) {
                
                foreach ($files->items as $aItem) {
                    //@JAPR 2018-06-07: Corregido un bug, Google Drive cambi� la manera en que funciona su API (v2) as� que dej� de ser compatible este c�digo (#VLMNXW)
                    //Con el API v2 ya no exist�a una propiedad "parents" sino un m�todo "getParents", sin embargo incluso con la colecci�n regresada por dicho m�todo,
                    //algunas cuentas no ten�an asignada correctamente la propiedad "isRoot" por lo que la validaci�n original ya no permit�a desplegar los archivos
                    //ni carpetas, por lo tanto se removi� el uso de esa propiedad y se opt� por filtrar por "root" al invocar a listFiles
                    //$objParentsColl = property_exists($aItem, "parents")?$aItem->parents:$aItem->getParents( );
                    //if($folderId || $objParentsColl[0]->isRoot) {
                    if ( $aItem->mimeType == "application/vnd.google-apps.folder") {
                        $strHTMLTreeFolders.="<li id='{$aItem->id}' name='{$aItem->id}'><a data-pathnumeric='' data-pathlabel='' folder-name='{$aItem->title}'><span class='fa fa-folder-o'></span>{$aItem->title}</a><ul class='collapse'><li><a>".translate('Loading')."</a></li></ul></li>";
                        $count++;
                    } else if (($aItem->mimeType=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || $aItem->mimeType== "application/vnd.ms-excel")&& $this->showfiles){
                        $strHTMLTreeFolders.="<li id='{$aItem->id}' name='{$aItem->id}' class='fileclass'><a data-pathnumeric='' data-pathlabel='' folder-name='{$aItem->title}'><span class='fa fa-file'></span>{$aItem->title}</a></li>";
                        $count++;
                    }
                    //}
                    //@JAPR
                }

            } else {
                $strHTMLTreeFolders.="";
            }
            if ($count<1) {
                $strHTMLTreeFolders ="";
            }
            //$strHTMLTreeFolders.="</ul>";
            return $strHTMLTreeFolders;
        } else {
            return array('success' => false, 'msg' => "<p>" . translate("Connection Failure") . "<p>");
        }
    }
}
?>