<?php
class HTTPConnection {
    //@JAPR 2018-01-25: Agregado control de errores en los request tipo HTTP (#RXVH44)
    public $errorCode = 0;						//Código HTTP de error del último request. 0 o 200 significa que no hay error
    public $errorDesc = '';						//Descripción de error del último request (no necesariamente equivale a la descripción correspondiente a un código de error HTTP, se puede usar para respuestas HTTP 200 pero validadas contra códigos de error personalizados)
    //@JAPR

    function getStoredCredentials($httpConnection){
        try {

            $rs = $this->Repository->DataADOConnection->Execute( 'SELECT `user`,`pass`, `pass_parameter`, `user_parameter`,`type_request`FROM si_sv_servicesconnection WHERE `type` = 6 AND `name` = ' . $this->Repository->DataADOConnection->Quote( $httpConnection ) );

            if( $rs && !$rs->EOF )
            {
                return (array('httpUsername' =>$rs->fields['user'] ,'httpPassword' =>$rs->fields['pass'],'httpServer' =>$httpConnection,'passParameter' =>$rs->fields['pass_parameter'],'userParameter' =>$rs->fields['user_parameter'],'httpTypeRequest' =>$rs->fields['type_request']));
            }

            return null;
        }catch (Google_Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }    
	//GCRUZ 2016-01-12. Agregado parámetro getQueryString, para parámetros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
    function uploadFile($userAccount,$fileOrign, $filename = null, $realPath = null, $parameters = null, $getQueryString = ''){
        $arrCredentialsHTTP=$this->getStoredCredentials($userAccount);

        $this->errorCode = 0;
        $this->errorDesc = '';
        if ($arrCredentialsHTTP["httpTypeRequest"]==ddrtPOST){
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
			if (is_array($fileOrign))
			{
				$post = array();
				for ($postFilesCount=0; $postFilesCount < count($fileOrign); $postFilesCount++)
				{
					$mimeTypeFile=finfo_file($finfo, $fileOrign[$postFilesCount]);
		            $post['file['.$postFilesCount.']'] = new CurlFile($fileOrign[$postFilesCount], $mimeTypeFile, $filename[$postFilesCount]);
				}
			}
			else
			{
            $mimeTypeFile=finfo_file($finfo, $fileOrign);
            $post = array('file' => new CurlFile($fileOrign, $mimeTypeFile, $filename));
			}
            if ($parameters){
                preg_match_all("/([a-zA-Z0-9\*\+\-\ñ\@\á\é\í\ó\ú]+) *= *([a-zA-Z0-9\*\+\-\ñ\@\á\é\í\ó\ú]+)/", $parameters, $match, PREG_PATTERN_ORDER);
                if(count($match[0]))
                {
                    for($i = 0; $i < count($match[0]); $i++) {
                        $post[$match[1][$i]] = $match[2][$i];
                    }
                }                
            }
			//GCRUZ 2016-01-12. Agregado parámetro getQueryString, para parámetros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
			$getQueryString = ($getQueryString == '')? '' : '?'.$getQueryString;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$userAccount.$getQueryString);
            curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_USERPWD, $arrCredentialsHTTP["httpUsername"]. ':' . $arrCredentialsHTTP["httpPassword"]);

            $result=curl_exec ($ch);
            //@JAPR 2018-01-25: Agregado control de errores en los request tipo HTTP (#RXVH44)
            $arrRequestInfo = @curl_getinfo($ch);
            if ( $arrRequestInfo && is_array($arrRequestInfo) ) {
                if (getParamValue('DebugBreak', 'both', '(int)', true)) {
                    PrintMultiArray($arrRequestInfo);
                }
                $this->errorCode = @$arrRequestInfo["http_code"];
                if ( is_null($this->errorCode) ) {
                    $this->errorCode = 200;
                }
				else {
					$this->errorCode = (int) $this->errorCode;
				}
            }
            $this->errorDesc = (string) @curl_error($ch);
            //@JAPR
            curl_close ($ch);
            return $result;
        }else{
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimeTypeFile=finfo_file($finfo, $fileOrign);

            // specify custom header
            $customHeader = array(
                "Content-type: $mimeTypeFile"
            );
            if ($parameters){
                preg_match_all("/([a-zA-Z0-9\*\+\-\ñ\@\á\é\í\ó\ú]+) *= *([a-zA-Z0-9\*\+\-\ñ\@\á\é\í\ó\ú]+)/", $parameters, $match, PREG_PATTERN_ORDER);
                if(count($match[0]))
                {
                    for($i = 0; $i < count($match[0]); $i++) {
                        $customHeader[] = $match[1][$i] . ":" . $match[2][$i];
                    }
                }                
            }
            $handle = curl_init ($userAccount);
            $fd = fopen ($fileOrign, 'r');
            // specify custom header
            
            $curlOptArr = array(
                CURLOPT_PUT => TRUE,
                CURLOPT_HEADER => TRUE,
                CURLOPT_HTTPHEADER => $customHeader,
                CURLOPT_INFILESIZE => filesize($fileOrign),
                CURLOPT_INFILE => $fd,
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
                CURLOPT_USERPWD => $arrCredentialsHTTP["httpUsername"]. ':' . $arrCredentialsHTTP["httpPassword"],
                CURLOPT_RETURNTRANSFER => TRUE
            );
            curl_setopt_array($handle, $curlOptArr);
            $ret = curl_exec($handle);
            $errRet = curl_error($handle);
            //@JAPR 2018-01-25: Agregado control de errores en los request tipo HTTP (#RXVH44)
            $arrRequestInfo = @curl_getinfo($ch);
            if ( $arrRequestInfo && is_array($arrRequestInfo) ) {
                if (getParamValue('DebugBreak', 'both', '(int)', true)) {
                    PrintMultiArray($arrRequestInfo);
                }
                $this->errorCode = @$arrRequestInfo["http_code"];
                if ( is_null($this->errorCode) ) {
                    $this->errorCode = 200;
                }
				else {
					$this->errorCode = (int) $this->errorCode;
				}
            }
            $this->errorDesc = $errRet;
            //@JAPR
            curl_close($handle);
            return array('sucess' =>$ret ,'error' =>$errRet );
        }
    }
    function downloadAttributes($userAccount){
        $intConnectionTimeOut = 600;
        $intRequestTimeOut = 600;
        $httpServer = $userAccount['httpServer'];
        $httpUsername = $userAccount['httpUsername'];
        $httpPassword = $userAccount['httpPassword'];
        $httpParameters = $userAccount['httpParameters'];
        $catalogId = $userAccount['catalogId'];
        $strURL = $httpServer;

        if ($httpParameters!='') {
            $strURL = $strURL . '?' . $httpParameters;
        }
        //Checar con raquel funcion de remplazo cde constantes 
        //$arrPostFields = array();
        try {
            $ch = curl_init();
            if ($ch === false) {
                logString("There was an error starting the curl session", '', true, true);
                return false;
            }
            
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $intConnectionTimeOut);
            curl_setopt($ch, CURLOPT_TIMEOUT, $intRequestTimeOut);
            if ($httpUsername && $httpUsername != ''){
                curl_setopt($ch, CURLOPT_USERPWD, $httpUsername . ':' . $httpPassword); 
            }
            //@JAPR 2015-02-03: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
            //Agregada para que los errores HTTP 400+ generen un error en lugar de regresar el HTML de la página de error como si la petición hubiera
            //sido exitosa
            curl_setopt($ch, CURLOPT_FAILONERROR, true);
            //echo("<br>\r\n Request URL: {$strURL}");
            
            //curl_setopt($ch, CURLOPT_HEADER, true); 
            curl_setopt($ch, CURLOPT_URL, $strURL);
            //@JAPR 2015-05-04: Corregido un bug, con PhP 5.6 comenzó a marcar un error si se especificaba el request como POST pero sin datos, así que se
            //cambió para que se mandara como GET
            //curl_setopt($ch, CURLOPT_POST, true);
            //@JAPR
            //curl_setopt($ch, CURLOPT_REFERER, '');
            //curl_setopt($ch, CURLOPT_HEADER, $header OR $follow);
            //curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            //curl_setopt($ch, CURLOPT_USERPWD, "[jpuente:jpuente]");
            //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); 
            //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2); 
            //curl_setopt($ch, CURLOPT_POSTFIELDS, $arrPostFields);
            
            $strResult = curl_exec($ch);
            //@JAPR 2015-02-04: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
            //Gracias a la opción CURLOPT_FAILONERROR, ahora los errores de HTTP si entran por este IF, así que es seguro asumir que si entró por
            //aquí significa que no pudo grabar absolutamente nada de la captura y se puede reintentar nuevamente, generando una tarea recalendarizada
            //para que se procese en otro momento
            $intLastCURLError = 0;
            $strLastCURLError = '';
            $strErrorDesc = '';
            if ($strResult === false) {
                $strErrorDesc = curl_error($ch);
                return array ('success' => false, 'msg'=> $strErrorDesc);
            }
            else {
                //@JAPR 2015-02-05: Modificado el Agente para considerar tareas con status de Rescheduled y Manual para que sean procesadas o ignoradas
                //Antes de liberar los recursos obtiene la información del último error detectado, esto cuando el error no es un HTTP 400+, ya que en ese
                //caso hubiera entrado por el If
                $intLastCURLError = @curl_errno($ch);
                $strLastCURLError = @curl_error($ch);
            }
            curl_close($ch);
            
            //Si no hubo error en el request, verifica si hubo error durante el grabado
            if ($strResult !== false) {
                $arrAtributesname= array();
                $multiaRRay= array();
                $definition= array();
                $arrdefinition= array();
                $arrExcelColumns= array();
                $arrAttributes = json_decode($strResult);
                if (!$arrAttributes){
                   return array ('success' => false, 'msg'=> translate('Invalid link service')); 
                }
                if (property_exists ($arrAttributes , 'columns' )) {
                    //Cuando viene indexado numericamente tenemos una propiedad extra enla clase llamada columns
                }else{
                    // Si no viene indexado numericamente significa que podemos tomar los nombres de los atributos directamente
                    // del objeto value
                    foreach ($arrAttributes->values as $key => $value) {
                        $arrValue = get_object_vars($value);
                        foreach ($arrValue as $key2 => $value2) {
                            $multiaRRay[$key2][] = $value2;
                            if (!in_array($key2, $arrAtributesname)){
                                $arrAtributesname[] = $key2;
                            }
                        }
                    }
                }
                if ($catalogId != '-1'){
                    $sql = 'SELECT membername, fieldname FROM SI_SV_datasourceMember WHERE datasourceid = ' . $catalogId;
                    $rs = $this->Repository->DataADOConnection->Execute($sql);
                    while( $rs && !$rs->EOF )
                    {
                        $definition['text']= $rs->fields['membername'];
                        $definition['value']=$rs->fields['fieldname'];
                        $arrdefinition[] = $definition;
                        $rs->MoveNext();
                    }
                    for($i = 0; $i < count( $arrAtributesname ); $i++)
                    {
                        $bFound = false;
                        for($x = 0; $x < count( $arrdefinition ); $x++)
                        {
                            if( $arrdefinition[$x]['text'] == $arrAtributesname[$i] )
                            {
                                $bFound = true; break;
                            }
                        }

                        if(!$bFound)
                        {
                            $arrExcelColumns[] = $arrAtributesname[$i];
                        }
                    }
                }
                return array ('success' => true, 'arrAtributesname'=> $arrAtributesname, 'multiaRRay'=> $arrAttributes->values, 'arrExcelColumns' => $arrExcelColumns, 'arrdefinition' => $arrdefinition );
            }
        } catch (Exception $e) {
            return array ('success' => false, 'msg'=> $e->getMessage());
        }
    }
}
?>