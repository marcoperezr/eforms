<?php

class eMailConnection {
    //@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
    public $DataDestination = null;
    public $options = null;
    //@JAPR
    
    function __construct($aDestination = null, $syncDataDestinationOpt = null) {
        $this->DataDestination = $aDestination;
        $this->options = $syncDataDestinationOpt;
    }
	
    function sendMail($originFile, $aRepository, $aSurvey, $additionaleMail = null){
        
        $eMail=$this->createMail( $originFile, $aRepository, $aSurvey, $additionaleMail );
        return $eMail;

    }

    function createMail($originFile, $aRepository, $objExpSurvey, $additionaleMail = null)
    {
        
        require_once( __DIR__ . "/../exportsurvey.inc.php");
        //@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
        global $arrAdditionalEMails;
        //@JAPR 2014-04-09: Agregada la variable para identificar el envío de reportes desde la captura o desde la opción para forzarlo en la lista de reportes
        global $blnSendPDFEntries;
        //@JAPR
        global $theUser;
    
        require_once( __DIR__ . "/../appuser.inc.php");

        $objResource = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $theUser->UserID);
        
        //EVEG validamos que el usuario tenga permitido enviar correos de captura
		//@JAPR 2016-09-14: Modificado el comportamiento de los destinos de datos, este día para un proyecto, DAbdo solicitó que independientemente de si el usuario que captura tiene o
		//no activada la configuración de recibir notificaciones, el destino tipo EMail se mande a los EMails alternativos que existieran configurados, así que sólo se saldrá en esta
		//condición si no está activada dicha configuración y no hay mas cuentas a las que se deba intentar enviar el correo (#LSXEYR)
		$blnExistsAlternativeEMail = false;
		if (isset($arrAdditionalEMails) && is_array($arrAdditionalEMails) && count($arrAdditionalEMails) > 0) {
			foreach ($arrAdditionalEMails as $strEMail) {
				if (trim($strEMail) != '') {
					$blnExistsAlternativeEMail = true;
					break;
				}
			}
		}
		
        if ($additionaleMail) {
            $arrAdditionalEMails2 = explode(";", $additionaleMail);
            if (isset($arrAdditionalEMails2) && is_array($arrAdditionalEMails2) && count($arrAdditionalEMails2) > 0) {
				foreach ($arrAdditionalEMails2 as $strEMail2) {
					if (trim($strEMail2) != '') {
						$blnExistsAlternativeEMail = true;
						break;
					}
				}
			}
		}
		
		//Si no está activado el envío al propio usuario o no hay correos alternativos como destino, debe cancelar el envío del correo
        if ($objResource->SendPDFByEmail===0 && !$blnExistsAlternativeEMail) {
            return(array('error' =>0 ,'msg' =>"The user: ".$objResource->Email." has no active sending e-mail: "));
        }
		//@JAPR
        
        $strCreatorStatusEmail = $objResource->Email;
        $strCreatorStatusUserName = $objResource->Email;
        
        require_once( __DIR__ . "/../bpaemailconfiguration.inc.php");
        
        $anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
        
        if(is_null($anInstanceEmailConf))
        {
            return false;
        }
      
        $SMTPServer = $anInstanceEmailConf->fbm_email_server;
        
        if (strlen($SMTPServer) == 0) 
        {
            return false;
        }

        $SMTPPort = $anInstanceEmailConf->fbm_email_port;
        
        if (strlen($SMTPPort) == 0) 
        {
            return false;
        }
        
        $SMTPUserName = $anInstanceEmailConf->fbm_email_username;
        
        if (strlen($SMTPUserName) == 0)
        {
            return false;
        }

        $SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
        
        //@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
        //que sólo incluya eForms su versión si no lo hizo ya eBavel
        if(!class_exists("PHPMailer")){
            require_once(__DIR__ . "/../mail/class.phpmailer.php");
        }   
        //@JAPR
        
        $mail = new PHPMailer();
        $mail->Mailer = "smtp";
        $mail->Host = $SMTPServer;
        $mail->Port = $SMTPPort;
        $mail->SMTPAuth = TRUE;
		//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
		//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
		$mail->SMTPAutoTLS = false;
		//$mail->SMTPDebug = false;
		//$mail->Debugoutput = 'echo';
		//$mail->SMTPSecure = '';
		//$mail->CharSet = 'UTF-8';
		//@JAPR
        $mail->Username = $SMTPUserName;
        $mail->Password = $SMTPPassword;

        //Se modifico el envio de correos para que el usuario que se loguea al servidor de correos
        //sea el mismo que vaya en el FROM, pero en el momento en que se contesta el correo entonces
        //el Reply To va dirigido a la persona q realmente si creo el correo
        //$mail->From = $strCreatorStatusEmail;
        //$mail->FromName = $strCreatorStatusUserName;
        $mail->From = $anInstanceEmailConf->fbm_email_source;
        $mail->FromName = $strCreatorStatusUserName;
        $mail->AddReplyTo($strCreatorStatusEmail, $strCreatorStatusUserName);

        $mail->IsHTML(TRUE);
        
        $maillanguage = $_SESSION["PAuserLanguage"];
        $mail->SetLanguage(strtolower($maillanguage),"mail/language/"); //set the language
        
        //@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
        if ( !is_null($this->DataDestination) && !is_null($this->options) && trim((string) $this->DataDestination->emailsubject) ) {
            $arrInfo = array();
            $mail->Subject = utf8_decode(replaceQuestionVariablesAdvV6((string) $this->DataDestination->emailsubject, @$this->options["arrJSONData"]['answers'], true, $aRepository, (int) @$this->options["arrJSONData"]['surveyID'], false, false, false, $arrInfo, 0, $this->options));
        }
        else {
            $mail->Subject = translate("eSurvey - Survey Captured: ").utf8_decode($objExpSurvey->SurveyName);
        }
        $existRecipients = false;

        $tempEmail = $strCreatorStatusEmail;
        //$tempEmail = "iris.suarez@gmail.com";
        $tempUserName = $strCreatorStatusUserName;
        
		//@JAPR 2018-04-17: Corregido un bug, con el cambio de comportamiento de la propiedad SendPDFByEmail (issue #LSXEYR), se había permitido enviar el EMail
		//aunque el usuario no tuviera activada esa propiedad pero hubiera EMails alternativos configurados por forma o destino de datos, sin embargo al propio usuario
		//se le seguía enviando así que se validará para que ya no lo haga (#4MQXUK)
		if ( $objResource->SendPDFByEmail ) {
			$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmail);

			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString( 'send email: ' );
				ECHOString( 'From: ' . $mail->From );
				ECHOString( 'FromName: ' . $mail->FromName );
				ECHOString( 'AddAddress: ' . $tempEmail );
			}

			if ($blnCheckMail)
			{
				$mail->AddAddress($tempEmail, $tempUserName);
				$existRecipients = true;
			}
			
			//@JAPR 2012-10-09: Agregado el EMail alterno para envio de notificaciones y cualquier otro evento del eForms
			$tempEmailCC = @$objResource->AltEmail;
			if (trim($tempEmailCC) != '') {
				$blnCheckMail = BPAEmailConfiguration::check_email_address($tempEmailCC);
				if ($blnCheckMail)
				{
					$mail->AddAddress($tempEmailCC, $tempUserName);
					$existRecipients = true;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString( 'AltAddAddress: ' . $tempEmailCC );
					}
				}
			}
		}
        
        //@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
        //Agrega las cuentas de correo adicionales configuradas para la encuesta, en este punto se asume que ya están validadas por lo menos para que
        //se vean como un EMail, si están bien armadas o no son reales, ya se sale del alcance y se dejará al server de correo que lo resuelva
        if (isset($arrAdditionalEMails) && is_array($arrAdditionalEMails) && count($arrAdditionalEMails) > 0) {
            foreach ($arrAdditionalEMails as $strEMail) {
                $strEMail=str_replace("'","", $strEMail);
                @$mail->AddAddress($strEMail, "");
                $existRecipients = true;
            }
			
			//Faltaba reportar estos EMails adicionales en la bitácora de salida
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString( 'send email: ' );
				ECHOString( 'additionaleMail: ' . @implode(", ", $arrAdditionalEMails) );
				ECHOString( 'FromName: ' . $mail->FromName );
				/*ECHOString( 'AddAddress: ' . $tempEmail );*/
			}
        }
        //@JAPR
        if ($additionaleMail) {
            $arrAdditionalEMails2 = explode(";", $additionaleMail);
            if (isset($arrAdditionalEMails2) && is_array($arrAdditionalEMails2) && count($arrAdditionalEMails2) > 0) {
                foreach ($arrAdditionalEMails2 as $strEMail2) {
                    $strEMail2=str_replace("'","", $strEMail2);
                    @$mail->AddAddress($strEMail2, "");
                    $existRecipients = true;
                }
            }

        }
        if (getParamValue('DebugBreak', 'both', '(int)', true)) {
            ECHOString( 'send email: ' );
            ECHOString( 'additionaleMail: ' . $additionaleMail );
            ECHOString( 'FromName: ' . $mail->FromName );
            /*ECHOString( 'AddAddress: ' . $tempEmail );*/
        }
        
        //Enviaremos copias ocultas a los correos que se encuentran en la variable global
        //$KPAMailTesting definida en el config con la finalidad de monitorear el envio de correos
        global $KPAMailTesting;
        
        foreach ($KPAMailTesting as $emailTest)
        {
            $blnCheckMail = BPAEmailConfiguration::check_email_address($emailTest);
            if ($blnCheckMail)
            {
                $mail->AddBCC($emailTest, "");
                $existRecipients = true;
            }
        }
    
    $mail->Body = $this->buildMailBody( $objExpSurvey, $objResource, $aRepository );
    if(!isset($mail->Body) || $mail->Body=='')
        return(array('error' =>1 ,'msg' =>'Mail construction failure'));
    //Aqui tengo que invocar la funcion de rigo para obtener el pdf y atacharlo
    
    if (!is_null($originFile))
    {
        $mail->AddAttachment($originFile);
    }
    
    if($existRecipients)
    {
		//@JAPR 2018-04-17: Corregido un bug, con el cambio de comportamiento de la propiedad SendPDFByEmail (issue #LSXEYR), se había permitido enviar el EMail
		//aunque el usuario no tuviera activada esa propiedad pero hubiera EMails alternativos configurados por forma o destino de datos, sin embargo al propio usuario
		//se le seguía enviando así que se validará para que ya no lo haga (#4MQXUK)
		if ( $objResource->SendPDFByEmail ) {
			AddCuentaCorreoAltToMail(@$objResource, $mail);
		}
		//@JAPR
        if (!@$mail->Send())
        {
            //Enviamos al log el error generado
            global $SendMailLogFile;
            
            $aLogString = "\r\n\r\neSurvey - Survey Captured: ".$objExpSurvey->SurveyName.": ".date('Y-m-d H:i:s');
            $aLogString.="\r\n";
            $aLogString.="\r\n\r\n".$mail->ErrorInfo;
            $aLogString.="\r\n";
			
            //@JAPR 2019-09-17: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
            error_log($aLogString, 3, GeteFormsLogPath().$SendMailLogFile);
            //@JAPR
			
            if (getParamValue('DebugBreak', 'both', '(int)', true)) {
                ECHOString( 'Email error: ' . $mail->ErrorInfo);
            }
        }
    }else{
        return(array('error' =>1 ,'msg' =>'No recipient: '.$tempEmail));
    }
    
    unset($mail);
    return(array('error' =>0 ,'msg' =>"The file is properly upload to eMail: ".$tempEmail ));
    return true;
    }

    function buildMailBody( $aSurvey, $anUser, $aRepository )
    {
		//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		if ( !is_null($this->DataDestination) && !is_null($this->options) && trim((string) $this->DataDestination->emailbody) ) {
			$strEMailBody = replaceQuestionVariablesAdvV6((string) $this->DataDestination->emailbody, @$this->options["arrJSONData"]['answers'], true, $aRepository, (int) @$this->options["arrJSONData"]['surveyID'], false, false, false, $arrInfo, 0, $this->options);
			$strtemp = "<html><body>".utf8_decode($strEMailBody)."</body></html>";
		}
		else {
			$strMailTitle = translate("Survey Captured Notification - ").htmlentities($aSurvey->SurveyName);
			$strProjectName = translate("Survey").": ".htmlentities($aSurvey->SurveyName);
			$strProjectManager=translate("Survey Captured by")." ". $anUser->UserName;
			$strProjectManager.=" ".translate("on")." "/*.date('F j, Y H:i:s', strtotime($POST["DateID"]))*/;
			$strAdditionalNotes = "Review attachment....";
			$strtemp = "<html>"."\n"
			."<style type=text/css>"."\n"
			."  A:active { color: #00008b }"."\n"
			."  A:visited { color: #708090 }"."\n"
			."  A:link { color: #00008b }"."\n"
			."  A:hover { color: #4169e1 }"."\n"
			."  A { font-family: verdana; font-size: 8pt }"."\n"
			."</style>"."\n"
			."<body>"."\n"
			."<div style=\"font-family:verdana;color:#336699;font-size:13pt\"><b>".$strMailTitle."</b></div>"."\n"
			."<table border=0 cellPadding=5 cellSpacing=1 style=\"font-family:verdana;font-weight:normal;font-size:10pt\">"."\n"
			."  <tr>"."\n"
			."    <td colspan=2><hr noshade width=100% size=1></td>"."\n"
			."  </tr>"."\n"
			."  <tr>"."\n"
			."    <td>&nbsp;</td>"."\n"
			."  </tr>"."\n"
			."  <tr>"."\n"
			."    <td>".$strProjectName."</td>"."\n"
			."  </tr>"."\n"
			."  <tr>"."\n"
			."    <td>".$strProjectManager."</td>"."\n"
			."  </tr>"."\n"
			."  <tr>"."\n"
			."    <td>".$strAdditionalNotes."</td>"."\n"
			."  </tr>"."\n"
			."</table>"."\n"
			."<br>"."\n";
			$strtemp .= "</body>"."\n"
			."</html>"."\n";
		}
		//@JAPR
		
        return $strtemp;
    }
}