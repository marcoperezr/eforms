<?php
require_once __DIR__ . '/' ."libs/Dropbox/autoload.php";

use \Dropbox as dbx;
$callBackURL=substr(__FILE__,strrpos(__FILE__, "wwwroot")+7);
$callBackURL=preg_replace('/\\\/', '/', $callBackURL);//str_replace($callBackURL,"\\"."\\","/");
//define('URL_CALLBACK',"http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
define('URL_CALLBACK',"https://".$_SERVER['SERVER_NAME'].$callBackURL);
define('APPLICATION_NAME_DPX', 'Bitam eForms');
define('CLIENT_SECRET_PATH', __DIR__ . '/' .'libs/Dropbox/client_secret_dropBox.json');

class dropBoxConnection {
    //Funcion para crear la coneccion y establecer los permisos de la aplicacion con la cuenta de serviceCloud
    function create(){
        $authorizeUrl = $this->getWebAuth()->start();
        header('location:'.$authorizeUrl);
        exit;
    }
    function getWebAuth(){
        $appInfo = dbx\AppInfo::loadFromJsonFile(CLIENT_SECRET_PATH);
        $csrfTokenStore = new dbx\ArrayEntryStore($_SESSION, 'dropbox-auth-csrf-token');
        return new dbx\WebAuth($appInfo, "KPIForms",URL_CALLBACK,$csrfTokenStore);
    }
    function exchangeCode($code){
        $appCredentialFile = file_get_contents(CLIENT_SECRET_PATH);
        $jsonCredentialApp = json_decode($appCredentialFile,true);
        $sesion = curl_init("https://api.dropbox.com/1/oauth2/token");
        $parametros_post = 'code='.$code."&grant_type=authorization_code&client_id={$jsonCredentialApp["key"]}&client_secret={$jsonCredentialApp["secret"]}&redirect_uri=".URL_CALLBACK;
        curl_setopt ($sesion, CURLOPT_POST, true);
        curl_setopt ($sesion, CURLOPT_POSTFIELDS, $parametros_post);
        curl_setopt($sesion, CURLOPT_HEADER, false);
        curl_setopt($sesion, CURLOPT_RETURNTRANSFER,true);
        curl_setopt( $sesion, CURLOPT_SSL_VERIFYPEER, FALSE);
        $credential=curl_exec($sesion);
        curl_close($sesion);
        return $credential;
    }
    function getCredentials($authCode,$userAccount=null){
        if(isset($authCode)){
            return $this->storeCredentials($this->exchangeCode($authCode));
        }else{
            return $this->getStoredCredentials($userAccount);
        }
    }
    function getStoredCredentials($emailAddress){
        try {

            $rs = $this->Repository->DataADOConnection->Execute( 'SELECT `id`,`access_token` FROM si_sv_servicesconnection WHERE `type` = 2 AND `id` = ' . $this->Repository->DataADOConnection->Quote( $emailAddress ) );

            if( $rs && !$rs->EOF )
            {
                return array('id' => $rs->fields['id'], 'access_token' => $rs->fields['access_token']);
            }

            return null;
        }catch (DPX_Exception $e) {
            return null;
        }
    }
    function storeCredentials($credential){
        try {
            $ObjtCredential=json_decode($credential);
            $dbxClient = new dbx\Client($ObjtCredential->access_token, "PHP-Example/1.0");
            $accountInfo = $dbxClient->getAccountInfo();
            
            $this->name = $accountInfo["email"];

            if($id = BITAMeFormsDataDestinations::saveConnection( ['email' => $accountInfo["email"], 'access_token' => $credential, 'type' => 2], $this->Repository )){
                $this->id = $id;
                return array('id' =>$this->id, 'email' =>$accountInfo["email"]);
            } else {
                return false;
            }
        }catch (Google_Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }
    function uploadFile($userAccount,$fileOrign,$filename,$pathDestination){
        $credentials=$this->getCredentials(null,$userAccount);
        if(isset($credentials)){
             try {
                $ObjtCredential=json_decode($credentials["access_token"]);
                $dbxClient = new dbx\Client($ObjtCredential->access_token, "PHP-Example/1.0");
                $accountInfo = $dbxClient->getAccountInfo();
                $f = fopen($fileOrign, "rb");
                //linea para crear una carpet
                //$dbxClient->createFolder("/FolderTEst");
				//GCRUZ 2016-02-04. Cambiar el WriteMode de add() -> force(), para forzar sobreescribir archivos
                $result = $dbxClient->uploadFile("/".$pathDestination.$filename, dbx\WriteMode::force(), $f);
                fclose($f);
                return(array('error' =>0 ,'msg' =>"The file is properly upload to dropbox with account: ".$userAccount ));
            }catch (DPX_Exception $e) {
                return(array('error' =>1 ,'msg' =>$e->getMessage()));
            }
        }else{
            return(array('error' =>1 ,'msg' =>"We were unable to obtain credentials DropBox" ));
        }
    }
    function downloadFile($userAccount,$fileOrign, $filename){
        $credentials=$this->getCredentials(null,$userAccount);
        if(isset($credentials)){
            $ObjtCredential=json_decode($credentials["access_token"]);
            $dbxClient = new dbx\Client($ObjtCredential->access_token, "PHP-Example/1.0");
            $accountInfo = $dbxClient->getAccountInfo();
			//@JAPR 2015-09-08: Corregido un bug, crea la ruta para descarga del archivo local
			$strFilePath = "loadexcel/uploads/Datasources/dropbox/";
			if (!file_exists($strFilePath)) {
				@mkdir($strFilePath, null, true);
			}
			//GCRUZ 2016-02-04. Validar filename como null
			if (is_null($filename))
				$filename = basename($fileOrign);
            $local_file = $strFilePath.$filename;
			//@JAPR
            $f = fopen($local_file, "w+b");
			try
			{
	            $fileMetadata = $dbxClient->getFile("/".$fileOrign, $f);
			}
			catch(DPX_Exception $e)
			{
				fclose($f);
				return array('success' => false, 'file' => $filename);
			}
            fclose($f);
			//GCRUZ 2016-02-04. Validar que el archivo se haya bajado correctamente.
			if (file_exists($local_file) && filesize($local_file) != 0)
	            return array('success' => true, 'file' => $filename);
			else
				return array('success' => false, 'file' => $filename);
        }else {
            return array('success' => false, 'file' => $filename);
        }
    }
    function getTreeFiles($userAccount,$folderId=null){
        $credentials=$this->getCredentials(null,$userAccount);

        if(isset($credentials)){
            $ObjtCredential=json_decode($credentials["access_token"]);
            $dbxClient = new dbx\Client($ObjtCredential->access_token, "PHP-Example/1.0");
            
            $pathFile="/";
            if(isset($folderId))
                $pathFile=$folderId;
            
            $items=$dbxClient->getMetadataWithChildren($pathFile)["contents"];
            $strHTMLTreeFolders="";
            if(count($items)>0){
                foreach ($items as $aItem){
                    if($aItem["is_dir"]){
                        if(isset($folderId) || ($aItem["root"]=="dropbox")){
                            if(isset($folderId))
                                $aItemPath=GetFileNameFromFilePath($aItem["path"]);//str_replace($folderId,'',$aItem["path"]);
                            else
                                $aItemPath=$aItem["path"];
                            
                            if(strpos($aItemPath, '/')===0){
                                $aItemPath=substr($aItemPath,1,strlen($aItemPath));
                            }

                            $strHTMLTreeFolders.="<li id='{$aItem["path"]}' name='{$aItem["path"]}'><a data-pathnumeric='' data-pathlabel='' folder-name='{$aItem["path"]}'><span class='fa fa-folder-o'></span>{$aItemPath}</a><ul class='collapse'><li><a>".translate('Loading')."</a></li></ul></li>";
                        }
                    }else if (isset($this->showfiles) && $this->showfiles && $aItem["icon"] == "page_white_excel") {
                        if(isset($folderId) || ($aItem["root"]=="dropbox")){
                            $aItemPath = GetFileNameFromFilePath($aItem["path"]);
                            $strHTMLTreeFolders.="<li id='{$aItem["path"]}' name='{$aItem["path"]}' class='fileclass'><a data-pathnumeric='' data-pathlabel='' folder-name='{$aItem["path"]}'><span class='fa fa-file'></span>{$aItemPath}</a></li>";
                        }
                    }
                }
            }else{
                $strHTMLTreeFolders.="";
            }
            if(strlen($strHTMLTreeFolders)==0)
                $strHTMLTreeFolders.="";
        }else {/*@JRPP 2015-08-29 Aqui deberia de aparecer un mensjae relevante a la falla de conexion por credenciales*/
             $strHTMLTreeFolders =array('success' => false, 'msg' => "<p>" . translate("Connection Failure") . "<p>");
        }
        return $strHTMLTreeFolders;
    }
    function getUserInfo($credentials){
    }
}
if(isset($_GET['code'])){
    //var_dump($_GET['code']);
    echo "<html> 
        <head> 
        </head> 
        <body> 
        <script> 
            window.opener.fnSendCode('{$_GET['code']}');
        </script>         
        </body> 
        </html>";
    exit;
}
?>