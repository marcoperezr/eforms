<?php
class connectionCloud {
    /**
    * $conexion = google|dropbox|onedrive	
    **/
    static function createConnection($cloudService, $aRepository,$getCodeAuth=false){
    	
    	$aConnectionCloud = null;

    	if(is_file(__DIR__ . '/' . $cloudService.'Connection.class.php'))
        {
        	require_once(__DIR__ . '/' . $cloudService.'Connection.class.php');
        	switch ($cloudService){
			    case 'googleDrive':
				    $aConnectionCloud = new googleDriveConnection();
				    if($getCodeAuth)
				    	$aConnectionCloud->create();
				    
			        break;
		        case 'dropBox':
				    $aConnectionCloud = new dropBoxConnection();
				    if($getCodeAuth)
				    	$aConnectionCloud->create();
			        
			        break;
			}
			$aConnectionCloud->Repository = $aRepository;
        }

        return $aConnectionCloud;
    }   

	//GCRUZ 2016-01-12. Agregado parámetro getQueryString, para parámetros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
	//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
	//Agregado el parámetro $aDestination para permitir procesar adecuadamente con todas las configuraciones el destino que origina el upload. Originalmente usado
	//para destinos tipo EMail, para enviar un subject y body personalizado
	//Agregado el parámetro $syncDataDestinationOpt, el cual contiene una serie de variables enviadas a la clase SyncDataDestination con información de la captura,
	//necesaria para permitir el correcto reemplazo de todas las posibles variables a usar en los destinos
	static function uploadFile($cloudService,$userAccount,$fileOrign, $aRepository, $aSurvey = null, $filename = null, $logString = null,$realPath=null, $parameters = null, $getQueryString='', $additionaleMail = null, $aDestination = null, $syncDataDestinationOpt = null) {

    	$sPath = __DIR__ . '\\' . $cloudService.'Connection.class.php';

    	if(is_file( $sPath ) || $cloudService == 'FTP')
        {
			if( $cloudService != 'FTP' )
				require_once( $sPath );
			else
				require_once( __DIR__ . '\\..\\' . 'ftp.php' );

        	connectionCloud::logString('cloudService: ' . $cloudService);

        	switch ($cloudService){
			    case 'googleDrive':
				    $aConnectionCloud = new googleDriveConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    $sucess=$aConnectionCloud->uploadFile($userAccount,$fileOrign, $filename,$realPath);
				    return $sucess;
			        break;
		        case 'dropBox':
				    $aConnectionCloud = new dropBoxConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    $sucess=$aConnectionCloud->uploadFile($userAccount,$fileOrign, $filename,$realPath);
				    return $sucess;
			        break;
		        case 'eMail':
		        	//@JAPR 2018-01-17: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
		        	$aConnectionCloud = new eMailConnection($aDestination, $syncDataDestinationOpt);
		        	//@JAPR
		        	$sucess=$aConnectionCloud->sendMail($fileOrign, $aRepository, $aSurvey, $additionaleMail);
		        	return $sucess;
		        	break;
		        case 'FTP':
		        	try {
					    
					    $ftp = new Ftp;
						
						$ftp_server = str_replace('ftps://', '', str_replace('ftp://', '', $userAccount['ftp']));
					    
						$logString( 'Connect FTP: '. $ftp_server .', user: '. $userAccount['user'] .', password: '. $userAccount['pass'] );
					    
					    //EVEG Buscamos si se definio un puerto para el servidor ftp
				        preg_match("/:[0-9]{4}/", $ftp_server,$coincidencias);        
				        if(isset($coincidencias) && isset($coincidencias[0])){
				            $ftp_server=str_replace($coincidencias[0],"",$ftp_server);
				            $conn_id = $ftp->connect( $ftp_server,substr($coincidencias[0],1));;            
				        }else{
				            $conn_id = $ftp->connect( $ftp_server );
				        }

					    if($ftp->login($userAccount['user'], $userAccount['pass'])){
					    	$ftp->pasv(true);

					    	$logString(' File origin: ' . $fileOrign);
					    	$logString(' File name: ' . $filename);
							
							if($realPath=="/")
								$realPath="";
							
							$filename=str_replace(" ","_",$filename);
							$filename=str_replace(":","",$filename);
							$destinationFTP=$realPath.urlencode ($filename);
							//$destinationFTP=$realPath.basename($fileOrign);

							$logString(' Destino: ' . $destinationFTP);
							
							//@JAPR 2018-01-25: Agregado control de errores en los request tipo FTP (#RXVH44)
							$res = $ftp->put($destinationFTP,$fileOrign, FTP_BINARY);
							$strFTPError = $ftp->errorMsg;
							$ftp->close();

							if ( !$res ) {
								$logString( 'Archivo no enviado' );
								return(array('error' =>1 ,'msg' =>"Error uploading to FTP: ".$strFTPError ));
							}
							else {
								$logString( 'Archivo enviado' );
								return(array('error' =>0 ,'msg' =>"The file is properly upload to FTP: ".$ftp_server ));
							}
							//@JAPR
					    }else{
					    	$logString('Fallo el login en :'.$ftp_server);
					    	return(array('error' =>1 ,'msg' =>'Login FTP failure'));
					    }

					} catch (FtpException $e) {
						$logString( 'FTP Error: ' . $e->getMessage() );
					    return(array('error' =>1 ,'msg' =>$e->getMessage()));
					}
					break;
				case 'HTTP':
				 	$aConnectionCloud = new HTTPConnection();
				    $aConnectionCloud->Repository = $aRepository;
					//GCRUZ 2016-01-12. Agregado parátro getQueryString, para parátros extra enviados por GET al HTTP. Ejemplo: fileType=IMG
				    $httpResponse=$aConnectionCloud->uploadFile($userAccount,$fileOrign, $filename,$realPath, $parameters, $getQueryString);
				    //@JAPR 2018-01-26: Corregido un bug con el reporte de la respuesta, para POST regresaba un String pero para PUT regresaba un array así que no
				    //era consistente el comportamiento (#RXVH44)
				    if ( is_object($httpResponse) || is_array($httpResponse) ) {
				    	connectionCloud::logString('response HTTP: ' . var_export($httpResponse, true));
				    }
				    else {
				    	connectionCloud::logString('response HTTP: ' . $httpResponse);
				    }
				    if ( $aConnectionCloud->errorDesc || ($aConnectionCloud->errorCode && $aConnectionCloud->errorCode != 200) ) {
				    	connectionCloud::logString('HTTP request error: ' . $aConnectionCloud->errorCode . " - " . $aConnectionCloud->errorDesc);
				    	return(array('error' =>1 ,'msg' =>"Error uploading to HTTP (HTTP code #{$aConnectionCloud->errorCode}): ".$aConnectionCloud->errorDesc ));
				    }
				    else {
				    	return(array('error' =>0 ,'msg' =>"The file is properly upload to HTTP: ".$userAccount ));
				    }
				    break;
			}
        }
    }

    static function downloadFile($cloudService,$userAccount,$aRepository,$fileOrign,$filename = null, $realPath = null) {
    	if(is_file(__DIR__ . '/' . $cloudService.'Connection.class.php'))
        {
        	require_once(__DIR__ . '/' . $cloudService.'Connection.class.php');
        	switch ($cloudService){
			    case 'googleDrive':
				    $aConnectionCloud = new googleDriveConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    return $aConnectionCloud->downloadFile($userAccount,$fileOrign,$filename, $realPath);
			        break;
		        case 'dropBox':
				    $aConnectionCloud = new dropBoxConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    return $aConnectionCloud->downloadFile($userAccount,$fileOrign,$filename);
			        break;
				case 'FTP':
				    $aConnectionCloud = new FTPConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    return $aConnectionCloud->downloadFile($userAccount,$fileOrign);
			        break;
			}
        }
    }
    static function getTreeFiles($cloudService,$userAccount,$aRepository,$idFolder=null,$showfiles=false) {
    	if(is_file(__DIR__ . '/' . $cloudService.'Connection.class.php'))
        {
        	require_once(__DIR__ . '/' . $cloudService.'Connection.class.php');
        	switch ($cloudService){
			    case 'googleDrive':
				    $aConnectionCloud = new googleDriveConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    $aConnectionCloud->showfiles=$showfiles;
				    return $aConnectionCloud->getTreeFiles($userAccount,$idFolder);
			        break;
		        case 'dropBox':
				    $aConnectionCloud = new dropBoxConnection();
				    $aConnectionCloud->Repository = $aRepository;
				     $aConnectionCloud->showfiles=$showfiles;
				    return $aConnectionCloud->getTreeFiles($userAccount,$idFolder);
			        break;
		        case 'FTP':
				    $aConnectionCloud = new FTPConnection();
				    $aConnectionCloud->Repository = $aRepository;
				    $aConnectionCloud->showfiles=$showfiles;
				    return $aConnectionCloud->getTreeFiles($userAccount,$idFolder);
			        break;
			}
        }
    }
	static function downloadAttributes($cloudService,$userAccount,$aRepository) {
		if(is_file(__DIR__ . '/' . $cloudService.'Connection.class.php'))
		{
			require_once(__DIR__ . '/' . $cloudService.'Connection.class.php');
			switch ($cloudService){
				case 'HTTP':
					$aConnectionCloud = new HTTPConnection();
					$aConnectionCloud->Repository = $aRepository;
					return $aConnectionCloud->downloadAttributes($userAccount);
					break;
			}
		}
	}

    static function logString( $sMsg )
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString( $sMsg );
		}
		@logeBavelString( $sMsg );
	}
}
?>