<?php
class FTPConnection {
    //Funcion para crear la coneccion y establecer los permisos de la aplicacion de eForms con la cuenta de googleDrive
    function create(){
        $client = new Google_Client();
        $client->setApplicationName(APPLICATION_NAME);
        $client->setScopes(SCOPES);
        $client->setAuthConfigFile(CLIENT_SECRET_PATH);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $authUrl = $client->createAuthUrl();
        header('location:'.$authUrl);
        exit;
    }
    function getCredentials($authorizationCode,$emailAddress=null,$state=null){
    }
    function getStoredCredentials($ftpConnection){
        try {

            $rs = $this->Repository->DataADOConnection->Execute( 'SELECT `user`,`pass`,`name`  FROM si_sv_servicesconnection WHERE `type` = 5 AND `id` = ' . $this->Repository->DataADOConnection->Quote( $ftpConnection ));

            if( $rs && !$rs->EOF )
            {
                return (array('ftpUsername' =>$rs->fields['user'] ,'ftpPassword' =>$rs->fields['pass'],'ftpServer' =>$rs->fields['name']));
            }

            return null;
        }catch (Google_Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }
    function storeCredentials($emailAddress,$credentials){
        try {

            BITAMeFormsDataDestinations::saveConnection( ['email' => $emailAddress, 'access_token' => $credentials, 'type' => 3], $this->Repository );

        }catch (Google_Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
    }
    function uploadFile($userAccount,$fileOrign, $filename = null,$realPath=null){
    }
    function downloadFile($userAccount,$fileOrign, $folderId=null){
        if(!is_array($userAccount)){
            $userAccount=$this->getStoredCredentials($userAccount);
        }

        $ftp_server = str_replace('ftps://', '', str_replace('ftp://', '', $userAccount['ftpServer']));
        //@MAPR 2016-07-25: Se verifica que $FTP_SERVER contenga el caracter ':' para determinar si cuenta con puerto
        if (strpos($ftp_server, ':')) {
            //@MAPR 2016-07-25: Se crea un arreglo que contenga Host y Puerto para usarlos como parametros en $CONN_ID
            $ftp_connect_parameters = explode(":", $ftp_server);
            $conn_id = ftp_connect($ftp_connect_parameters[0], $ftp_connect_parameters[1]);
        }else {
            //@MAPR 2016-07-25: En caso de que no contenga el caracter ':' se hace conexión usando el parametro $FTP_SERVER 
            $conn_id = ftp_connect($ftp_server);

        }
        // iniciar sesión con nombre de usuario y contraseña
        if(ftp_login($conn_id, $userAccount['ftpUsername'], $userAccount['ftpPassword'])){
            // Obtener los archivos contenidos en el directorio actual
            ftp_pasv($conn_id, true);
			//@JAPR 2015-09-08: Corregido un bug, crea la ruta para descarga del archivo local
			$strFilePath = "loadexcel/uploads/Datasources/FTP/";
			if (!file_exists($strFilePath)) {
				@mkdir($strFilePath, null, true);
			}
			$filename = GetFileNameFromFilePath($fileOrign);
            $local_file = $strFilePath.$filename;
			//@JAPR
            if (ftp_get($conn_id, $local_file, $fileOrign, FTP_BINARY)) {
              return array('success' => true, 'file' => $filename);
            } else {
              return array('success' => false, 'file' => $filename);
            }
        }
    }
    function getTreeFiles($userAccount,$folderId=null){
        if(gettype($userAccount) !="array"){
			$userAccount=$this->getStoredCredentials($userAccount);
    	}

    	$ftp_server = str_replace('ftps://', '', str_replace('ftp://', '', $userAccount['ftpServer']));
		
        //EVEG Buscamos si se definio un puerto para el servidor ftp
        preg_match("/:[0-9]{4}/", $ftp_server,$coincidencias);        
        if(isset($coincidencias) && isset($coincidencias[0])){
            $ftp_server=str_replace($coincidencias[0],"",$ftp_server);
            $conn_id = @ftp_connect($ftp_server,substr($coincidencias[0],1));            
        }else{
            $conn_id = @ftp_connect($ftp_server);
        }

        if(!$conn_id){
            //return ("<p>" . translate("Connection Failure") . "<p>");
            return array('success' => false, 'msg' => "<p>" . translate('Invalid FTP link service(ftp:\/\/your_FTP.domain_extension)') . "<p>");
        }
		// iniciar sesión con nombre de usuario y contraseña
        $login = @ftp_login($conn_id, $userAccount['ftpUsername'], $userAccount['ftpPassword']);
		if($login){
			// Obtener los archivos contenidos en el directorio actual
			ftp_pasv($conn_id, true);

			$pathFolder="";
            if(!is_null($folderId)){
                $pathFolder=$folderId;
            }else {
                $pathFolder="";
            }
            $contents = ftp_nlist($conn_id, $pathFolder);
			// output $contents
			$strHTMLTreeFolders="";
			//ftp_chdir($conn_id,ftp_pwd($conn_id));
            if($contents){
                foreach ($contents as $aFile) {
                    /*2015-09-22@JRPP: Debido a que FTP te regresa arbitrariamente $afile con o sin path utilizaremos una 
                    funcion que nos regrese siempre el nombre del archivo y la asignaremos a una variable que sera 
                    el nombre verdadero del archivo*/
                    $realFile = GetFileNameFromFilePath($aFile);
                    $realpath = $realFile;
                    if(/*!strpos($pathFolder, "/") &&*/ !is_null($folderId)) {
                        $realpath = $pathFolder."/".$realFile;
                    }
                    if(ftp_size($conn_id, $realpath)<0){
                        if(!is_null($folderId)){
                            $idElement=$pathFolder."/".$realFile;
                        }else{
                            $idElement=$realFile;
                        }

                        $strHTMLTreeFolders.="<li id='{$idElement}' name='{$idElement}'><a data-pathnumeric='' data-pathlabel='' folder-name='{$realFile}'><span class='fa fa-folder-o'></span>{$realFile}</a><ul class='collapse'><li><a>".translate('Loading')."</a></li></ul></li>";
                        //ftp_cdup($conn_id);
                    }else if($this->showfiles){
                        $filetype = substr($aFile, -3);
                        if($filetype == "xls" ||$filetype == "lsx" ){
                            if(!is_null($folderId)){
                                $idElement=$pathFolder."/".$realFile;
                            }else{
                                $idElement=$aFile;
                            }

                            $strHTMLTreeFolders.="<li id='{$idElement}' name='{$idElement}' class='fileclass'><a data-pathnumeric='' data-pathlabel='' folder-name='{$realFile}'><span class='fa fa-file'></span>{$realFile}</a></li>";
                        }
                       
                    }
                }
            }else{
                $strHTMLTreeFolders.="";
            }
            if(strlen($strHTMLTreeFolders)==0){
                $strHTMLTreeFolders.="";
            }
            $respuesta = $strHTMLTreeFolders;
		}else {
            //$respuesta = "<p>" . translate("Connection Failure") . "<p>";
            $respuesta = array('success' => false, 'msg' => "<p>" . translate("Invalid user or password") . "<p>");
        }
    	ftp_close($conn_id);
    	return $respuesta;
    }
}
?>