<?php
require_once("repository.inc.php");
require_once("initialize.php");
require_once("settingsvariable.inc.php");
require_once("eformsAppCustomStyles.inc.php");

class BITAMEFormsAppCustomStylesTpl extends BITAMObject
{
	public $TemplateID;					//ID de definición de templates para permitir reutilizar en diferentes usuarios como si fuera un catálogo sin tener que perder antiguas definiciones
	public $TemplateName;				//Cuando se carga la instancia para grabar un template, contiene el nombre asignado al mismo
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->TemplateID = -1;
		$this->TemplateName = '';
	}
	
	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aTemplateID)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (((int)  $aTemplateID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.TemplateName 
			FROM SI_SV_TemplateStyleTpls A 
			WHERE A.TemplateID = {$aTemplateID}";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceWithName($aRepository, $aTemplateName)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (trim($aSettingsVariableName) == '')
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.TemplateID, A.TemplateName 
			FROM SI_SV_TemplateStyleTpls A 
			WHERE A.TemplateName = ".$aRepository->DataADOConnection->Quote($aTemplateName);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->TemplateID = (int) @$aRS->fields["templateid"];
		$anInstance->TemplateName = (string) @$aRS->fields["templatename"];
		
		return $anInstance;
	}
	
	/* Dados los parámetros, obtiene la configuración de interface del App que actualmente tiene asignado el usuario logeado, si no se especifica el
	parámetro $aUserID entonces se utilizará el usuario actual de la sesión. El esquema a utilizar se obtiene como sigue:
	- Si hay configuración de Template por usuario, se utiliza primero dicha configuración
	- Si hay configuración de Template global, se utiliza dicha configuración
	- Si no hay Template configurado se utilizará la configuración de colores directa de Settings
	- En caso de no poder cargar el template indicado, se utilizarán los colores defaults del App (que es el equivalente a cuando nunca configuraron
		colores en los settings)
	//@JAPR 2016-03-22: DAbdo determinó que los nuevos templates no deberían aplicar en el esquema de seguridad (por usuario o grupo), sólo global o por forma
	Ya no aplicarán las configuraciones por usuario o grupo de usuario, además ahora se agrega la configuración específica por forma
	Agregado el parámetro $surveyID para indicar la forma de la cual se deberá identificar si tiene o no configuración de template personalizada
	*/
	static function GetCurrentColorScheme($aRepository, $aUserID = -1, $surveyID = -1) {
		$strCalledClass = static::class;		
		if (is_null($aUserID) || (int) $aUserID <= 0) {
			$aUserID = $_SESSION["PABITAM_UserID"];
		}
		if ($aUserID <= 0) {
			$aUserID = -1;
		}
		
	    //@JAPR 2014-06-20: Agregados los templates para personalización del App
	    //Lee la configuración default del usuario especificado
		$intTemplateID = -1;
		//@JAPR 2016-03-22: DAbdo determinó que los nuevos templates no deberían aplicar en el esquema de seguridad (por usuario o grupo), sólo global o por forma
	    /*$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($aRepository, $aUserID);
	    if (!is_null($theAppUser)) {
	    	$intTemplateID = $theAppUser->TemplateID;
	    }
		
		//Leer la configuración default por grupo
		if ($intTemplateID <= 0) {
			require_once('usergroup.inc.php');
			$groups = BITAMUserGroupCollectionLimited::NewInstance($aRepository, $aUserID);
			foreach($groups->Collection as $aGroup)
			{
				if($aGroup->TemplateID>0)
				{
					$intTemplateID = (int) @$aGroup->TemplateID;	
					break;
				}
			}
		}*/
		
		//Lee la configuración default global si no había una por usuario o grupo
		if ($intTemplateID <= 0) {
			$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DEFAULTCOLORTEMPLATE');
			if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
				$intTemplateID = (int) @$objSetting->SettingValue;
			}
		}
		
		if ($intTemplateID <= 0) {
			$intTemplateID = -1;
		}
		
		//@JAPR 2016-03-22: DAbdo determinó que los nuevos templates no deberían aplicar en el esquema de seguridad (por usuario o grupo), sólo global o por forma
		//Lee la configuración de template por forma si es que se especificó alguna
		$surveyID = (int) $surveyID;
		if ($surveyID > 0) {
			require_once('survey.inc.php');
			$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
			if (!is_null($surveyInstance)) {
				if ($surveyInstance->TemplateID > 0) {
					$intTemplateID = $surveyInstance->TemplateID;
				}
			}
		}
		//@JAPR
		
		//Carga la instancia del esquema de color
		//@JAPR 2014-10-29: Corregido un bug, no se estaba limpiando el ID del usuario, así que buscaba el template pero configurado por el usuario
		//específico, siendo que el usuario sólo debe servir para validar la configuración de usuario antes que la global, pero todos los templates
		//de colores se configuran en forma global realmente
		$anInstance = BITAMEFormsAppCustomStyles::NewInstanceAll($aRepository, $intTemplateID);
		//@JAPR
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("TemplateID", $aHTTPRequest->POST))
		{
			$intTemplateID = $aHTTPRequest->POST["TemplateID"];
			
			if (is_array($intTemplateID))
			{
				$aCollection = BITAMEFormsAppCustomStylesTplCollection::NewInstance($aRepository, $intTemplateID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intTemplateID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				$aResult = $anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("TemplateID", $aHTTPRequest->GET))
		{
			$intTemplateID = $aHTTPRequest->GET["TemplateID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intTemplateID);

			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("TemplateID", $anArray)) {
			$this->TemplateID = $anArray["TemplateID"];
		}
		
		if (array_key_exists("TemplateName", $anArray)) {
			$this->TemplateName = $anArray["TemplateName"];
		}
		
		return $this;
	}
	
	function save()
	{	
	 	if ($this->isNewObject()) {
			$sql =  "SELECT ".$this->Repository->DataADOConnection->IfNull("MAX(TemplateID)", "0")." + 1 AS TemplateID".
				" FROM SI_SV_TemplateStyleTpls";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->TemplateID = (int) $aRS->fields["templateid"];
			$sql = "INSERT INTO SI_SV_TemplateStyleTpls (".
						" TemplateID".
			            ", TemplateName".
			            ") VALUES (".
			            $this->TemplateID.
						",".$this->Repository->DataADOConnection->Quote($this->TemplateName).
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	 	else {
			$sql = "UPDATE SI_SV_TemplateStyleTpls SET ".
					"TemplateName = ".$this->Repository->DataADOConnection->Quote($this->TemplateName).
				" WHERE TemplateID = ".$this->TemplateID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	}
	
	/* Elimina todos los valores previamente grabados para los estilos de este template sin eliminar su definición
	*/
	function clear() {
		$sql = "DELETE FROM SI_SV_TemplateStyle WHERE TemplateID = {$this->TemplateID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false) {
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	/* Sobreescribe los estilos globales según el elemento especificado aplicando los estilos recibidos en el request
	Si la combinación global no existía, entonces la crea en ese momento
	*/
	function setGlobalStyle($anStyleInstance, $anArray) {
		//Primero identifica el objeto sobre el que se hizo la configuración (debe ser un sólo elemento, NO un array)
		$intSectionType = (int) @$anArray["SectionType"];
		$intObjectType = (int) @$anArray["ItemType"];
		$intQTypeID = (int) @$anArray["QTypeID"];
		$intQDisplayMode = (int) @$anArray["QDisplayMode"];
		$intElementType = (int) @$anArray["ElementType"];
		$strFieldsToSet = trim((string) @$anArray["Fields"]);
		
		if ($intObjectType <=0 || $intElementType <= 0 || $strFieldsToSet == '') {
			return;
		}
		if ($intObjectType == otyQuestion && ($intQTypeID <= 0 || $intQDisplayMode <= 0)) {
			return;
		}
		
		$arrFieldsToSet = explode(",", $strFieldsToSet);
		$btnApplyAll = (bool) @$anArray["btnApplyAll"];
		$btnApplyGroup = (bool) @$anArray["btnApplyGroup"];
		
		//Asigna el objeto global que será modificado
		$intGblSectionType = -1;
		$intGblObjectType = -1;
		$arrGblQTypeID = array();
		$intGblQDisplayMode = -1;
		switch ($intObjectType) {
			case otySection:
				break;
			case otyQuestion:
				//En el caso de las preguntas, lo que las hace globales es el QTypeID, así que dependiendo del botón a usar, se generará para todas o sólo para el grupo correspondiente
				if ($btnApplyAll) {
					$arrGblQTypeID[] = -1;
				}
				elseif ($btnApplyGroup) {
					switch ($intQTypeID) {
						case qtpOpenNumeric:
						case qtpOpenDate:
						case qtpOpenString:
						case qtpOpenAlpha:
						case qtpOpenTime:
						case qtpPassword:
							$arrGblQTypeID = array(qtpOpenNumeric, qtpOpenDate, qtpOpenString, qtpOpenAlpha, qtpOpenTime, qtpPassword);
							break;
						case qtpSingle:
							$arrGblQTypeID = array(qtpSingle);
							break;
					}
				}
				break;
		}
		
		if (count($arrGblQTypeID) == 0) {
			return;
		}
		
		//Los campos que se reciben son todos los de la ventana de configuración, sin embargo eso puede incluir diferentes elementos, así que debe agrupar a cada uno de los elementos
		//que fueron recibidos
		//Como mínimo es seguro que el elemento seleccionado debe recorrerse
		$arrElements = array($intElementType => $intElementType);
		foreach ($arrFieldsToSet as $strPropName) {
			//Todo campo de propiedad debe venir con el separador "_", opcionalmente en el primer índice vendría un "|" con el # de elemento si es que pertenece a uno diferente al recibido
			$arrParts = explode("_", $strPropName);
			if (count($arrParts) > 1) {
				$arrElemParts = explode("|", $arrParts[0]);
				if (count($arrElemParts) > 1) {
					$intGblElementType = (int) @$arrElemParts[1];
					if ($intGblElementType > 0 && !isset($arrElements[$intGblElementType])) {
						$arrElements[$intGblElementType] = $intGblElementType;
					}
				}
			}
		}
		$intGblElementType = -1;
		
		//En este punto ya debe tener el array de objetos a los que se va a aplicar, así que empieza a propagar las combinaciones
		//Todas estas combinaciones son globales, por lo que SettingType == tsstGlobal
		foreach ($arrElements as $intGblElementType) {
			switch ($intObjectType) {
				case otySection:
					break;
				case otyQuestion:
					foreach ($arrGblQTypeID as $intGblQTypeID) {
						//Primero verifica si existe configuración global para esta combinación, ya que si no entonces la debe insertar
						$sql = "SELECT * FROM SI_SV_TemplateStyle 
							WHERE TemplateID = {$this->TemplateID} AND SettingType = ".tsstGlobal." AND ObjectType = {$intObjectType} 
								AND SectionType = -1 AND QuestionType = {$intGblQTypeID} AND DisplayType = -1 
								AND ElementType = {$intGblElementType}";
						$aRS = $this->Repository->DataADOConnection->Execute($sql);
						if ($aRS === false) {
							return;
						}
						
						$strStyle = "";
						if ($aRS->EOF) {
							//En este caso no se encontraba esta combinación, así que debe insertarla incluso vacía (se actualizará posteriormente
							$sql = "INSERT INTO SI_SV_TemplateStyle (TemplateID, SettingType, ObjectType, SectionType, QuestionType, DisplayType, ElementType, Style) 
								VALUES ({$this->TemplateID}, ".tsstGlobal.", {$intObjectType}, {$intSectionType}, {$intGblQTypeID}, -1, {$intGblElementType}, ".
									$this->Repository->DataADOConnection->Quote($strStyle).")";
							if (!$this->Repository->DataADOConnection->Execute($sql)) {
								continue;
							}
						}
						else {
							//Lee el estilo actualmente configurado
							$strStyle = (string) @$aRS->fields["style"];
						}
						
						//Sobreescribe las propiedades que acaban de cambiar de este estilo según lo que se mandó a grabar
						//Primero decodifica el estilo actual para manejarlo como array
						try {
							$objJSON = json_decode($strStyle);
							if (is_null($objJSON)) {
								$objJSON = array();
							}
							else {
								$objJSON = object_to_array($objJSON);
							}
						} catch (Exception $e) {
							$objJSON = array();
						}
						
						//Luego decodifica el array de estilos recibidos
						$strNewStyle = (string) @$anArray["Style"];
						try {
							$arrNewJSON = json_decode($strNewStyle);
							if (is_null($arrNewJSON)) {
								$arrNewJSON = array();
							}
							else {
								$arrNewJSON = object_to_array($arrNewJSON);
							}
						} catch (Exception $e) {
							$arrNewJSON = array();
						}
						
						//Ahora identifica los campos que deben cambiar y primero los elimina del estilo actualmente aplicado, para luego sobreescribirlos si aún vienen en el nuevo estilo
						$blnHasValue = false;
						foreach ($arrFieldsToSet as $strPropName) {
							//Sólo serán campos cuyo estilo pertenezca al que se está procesando en esta iteración, esto es, es el mismo estilo del ciclo (tiene |Elemn# == al recorrido) 
							//o es el estilo del elemento default recibido (no tiene |Elemen#)
							$blnValid = false;
							if ($intGblElementType == $intElementType) {
								//Verifica si es el elemento default, no tiene "|"
								$blnValid = (stripos($strPropName, "|") === false) || (stripos($strPropName, "|{$intGblElementType}_") !== false);
							}
							else {
								//Verifica si es el elemento del ciclo, tiene "|$intGblElementType_"
								$blnValid = (stripos($strPropName, "|{$intGblElementType}_") !== false);
							}
							
							if (trim($strPropName) == "" || !$blnValid) {
								continue;
							}
							
							unset($objJSON[$strPropName]);
							if (isset($arrNewJSON[$strPropName]) && trim($arrNewJSON[$strPropName]) != '') {
								$blnHasValue = true;
								$strPropValue = (string) @$arrNewJSON[$strPropName];
								$objJSON[$strPropName] = $strPropValue;
							}
						}
						
						//Finalmente si aún quedan elementos por grabar, actualiza el registro, en caso contrario lo elimina
						//if (count($objJSON) > 0) {
						if ($blnHasValue) {
							$arrJSON = array();
							foreach ($objJSON as $strPropName => $strPropValue) {
								if ($strPropName != '' && trim($strPropValue) != '') {
									$arrJSON[$strPropName] = $strPropValue;
								}
							}
							
							$strStyle = json_encode($arrJSON, JSON_FORCE_OBJECT);
							$sql = "UPDATE SI_SV_TemplateStyle SET 
									Style = ".$this->Repository->DataADOConnection->Quote($strStyle)." 
								WHERE TemplateID = {$this->TemplateID} AND SettingType = ".tsstGlobal." AND ObjectType = {$intObjectType} AND 
									SectionType = {$intSectionType} AND QuestionType = {$intGblQTypeID} AND 
									DisplayType = -1 AND ElementType = {$intGblElementType}";
							$this->Repository->DataADOConnection->Execute($sql);
						}
						else {
							$sql = "DELETE FROM SI_SV_TemplateStyle 
								WHERE TemplateID = {$this->TemplateID} AND SettingType = ".tsstGlobal." AND ObjectType = {$intObjectType} AND 
									SectionType = {$intSectionType} AND QuestionType = {$intGblQTypeID} AND 
									DisplayType = -1 AND ElementType = {$intGblElementType}";
							$this->Repository->DataADOConnection->Execute($sql);
						}
					}
					break;
			}
		}
	}
	
	function remove($bJustMobileTables = false)
	{
		$sql = "DELETE FROM SI_SV_TemplateStyle WHERE TemplateID = {$this->TemplateID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_AppCustomization ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$sql = "DELETE FROM SI_SV_TemplateStyleTpls WHERE TemplateID = {$this->TemplateID}";
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->TemplateID < 0);
	}
	
	function get_Title()
	{
		return translate("Color template");
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=EFormsAppCustomStylesTpl";
		}
		else
		{
			return "BITAM_PAGE=EFormsAppCustomStylesTpl&TemplateID=".$this->TemplateID;
		}
	}
	
	function get_Parent()
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'TemplateID';
	}
	
	function get_FormFieldName()
	{
		return 'TemplateName';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TemplateName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	 
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}

 	function addButtons($aUser)
	{
		if ($_SESSION["PABITAM_UserRole"]==1 && !$this->isNewObject())
		{
?>
			<button id="colorsBtn" class="alinkescfav" onclick="javascript: customizeApp();"><img src="images/copy.gif" alt="<?=translate("Colors")?>" title="<?=translate("Colors")?>" displayMe="1" /> <?=translate("Colors")?></button>
<?
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
		//@JAPR 2014-06-18: Agregados los templates para personalización del App
		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
		function customizeApp()
		{
			openWindowDialog('main.php?BITAM_PAGE=EFormsAppCustomStyles&TemplateID=<?=$this->TemplateID?>&UserID=-1', [window], [], 600, 600, customizeAppDone, 'customizeAppDone');
		}
		
		function customizeAppDone(sValue, sParams)
		{
			var strInfo = sValue;
			if(strInfo!=null && strInfo!=undefined)
			{
				try {
					<?=$myFormName?>_SaveForm.CustomizedColors.value = strInfo;
				} catch (e) {
				}
			}
		}
	</script>
<?
 	}
}

class BITAMEFormsAppCustomStylesTplCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfTemplateIDs = null)
	{
		$strCalledClass = static::class;
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance = new $strCalledClass($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfTemplateIDs))
		{
			switch (count($anArrayOfTemplateIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.TemplateID = ".((int) $anArrayOfTemplateIDs[0]);
					break;
				default:
					foreach ($anArrayOfTemplateIDs as $intTemplateID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$intTemplateID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.TemplateID IN (".$where.")";
					}
					break;
			}
		}
		
		$sql = "SELECT t1.TemplateID, t1.TemplateName 
			FROM SI_SV_TemplateStyleTpls t1 ".$where." ORDER BY t1.TemplateName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_TemplateStyleTpls ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		//return BITAMSettingsVariable::NewInstance($this->Repository);
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Color templates");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=SettingsVariable";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=EFormsAppCustomStylesTpl";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'TemplateID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "TemplateName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_FormFieldName()
	{
		return 'TemplateName';
	}
}
?>