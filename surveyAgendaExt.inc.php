<?php
	
include_once "surveyAgenda.inc.php";

class BITAMSurveyAgendaExtCollection extends BITAMSurveyAgendaCollection
{
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{

		$res = ['collection'=> []];

		if( $action = getParamValue('action', 'both', '(string)', true) )
		{
			if( $action == "getCollection" )
			{
				$filter = [ 'UserID' => null, 'SearchString' => null, 'anArrayOfAgendaIDs' => null, 'status' => null, 'CheckPointID' => null, 'anEndDate' => null, 'aStartDate' => null ];

				if( $arrFilter = getParamValue('filter', 'both', '(array)', true) )
				{
					$filter = array_merge( $filter, $arrFilter );
				}

				if( getParamValue('arrAgendaSchedIDs', 'both', '(array)', true) )
				{
					$filter['anArrayOfAgendaIDs'] = getParamValue('arrAgendaSchedIDs', 'both', '(array)', true);
				}

				if( isset($filter['Date']) )
				{
					list($filter['aStartDate'], $filter['anEndDate']) = explode('@', $filter['Date']);
				}

				$aCollection = BITAMSurveyAgendaExtCollection::NewInstance($aRepository, $filter['anArrayOfAgendaIDs'], $filter['UserID'], $filter['anEndDate'], null, false, $filter['aStartDate'], null, 100, false, null, $filter['SearchString'], $filter['status'], $filter['CheckPointID']);

				$defFilters = [
					[ 
						"key" => "CheckPointID"
						, "description" => translate('Check point')
						, "propKey" => "CheckPointID"
						, "propDesc" => "CheckPointName"
					 ],
					[ 
						"key" => "UserID"
						, "description" => translate('User')
						, "propKey" => "UserID"
						, "propDesc" => "UserName"
					 ]
				];

				foreach ($aCollection->Collection as $each)
				{
					$sStatus = getStatusDesc( $each->Status );
					$res['collection'][] = [ 
						'AgendaName' => $each->getAgendaName()
						, 'UserName' => $each->UserName
						, 'CheckpointName' => $each->CheckPointName
						, 'FilterText' => $each->FilterText
						, 'Status' => $sStatus
						, 'CaptureStartDate' => $each->CaptureStartDate
						, 'CaptureStartTime' => implode(':', $each->CaptureStartTime)
						, 'checkInTime' => $each->getCheckInTime()
						, 'AgendaID' => $each->AgendaID
						, 'canRemove' => $each->canRemove( $aUser )
						, 'canEdit' => $each->canEdit( $aUser )
					];


					foreach ($defFilters as $eachFilter)
					{
						if( !isset( $res['filter'][ $eachFilter['key'] ] ) )
						{
							$res['filter'][ $eachFilter['key'] ] = [
								'description' => $eachFilter['description'],
								'elements' => []
							];	
						}

						if( trim($each->$eachFilter['propDesc']) != '' )
						{
							if( !isset( $res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ] ) )
								$res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ] = [ 'id' => $each->$eachFilter['propDesc'], 'num' => 1 ];
							else
								$res['filter'][ $eachFilter['key'] ]['elements'][ $each->$eachFilter['propKey'] ]['num'] += 1;
						}
					}

					if( !isset( $res['filter'][ 'status' ]['elements'][ $each->Status ] ) )
					{	
						$res['filter'][ 'status' ]['description'] = translate('Status');
						$res['filter'][ 'status' ]['elements'][ $each->Status ] = [ 'id' => $sStatus, 'num' => 1 ];
					}
					else
						$res['filter'][ 'status' ]['elements'][ $each->Status ]['num'] += 1;
				}
			} else if( $action == "reschedule" )
			{
				$aCollection = BITAMSurveyAgendaCollection::PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser);

				$arrIds = @$_POST['agendaRescheduleID'];

				$arrIds = array_flip(explode(',', $arrIds));

				foreach ($aCollection->Collection as $each)
				{
					if( isset( $arrIds[$each->AgendaID] ) )
					{
						$res['collection'][] = [ 
							'AgendaName' => $each->getAgendaName()
							, 'UserName' => $each->UserName
							, 'CheckpointName' => $each->CheckPointName
							, 'FilterText' => $each->FilterText
							, 'Status' => getStatusDesc( $each->Status )
							, 'CaptureStartDate' => $each->CaptureStartDate
							, 'CaptureStartTime' => implode(':', $each->CaptureStartTime)
							, 'checkInTime' => $each->getCheckInTime()
							, 'AgendaID' => $each->AgendaID
							, 'canRemove' => $each->canRemove( $aUser )
							, 'canEdit' => $each->canEdit( $aUser )
						];
					}
				}
			} else if( $action == "save" )
			{
				$anAgendaID = getParamValue('AgendaID', 'both', '(int)', true);

				$anInstance = BITAMSurveyAgenda::NewInstanceWithID($aRepository, $anAgendaID);

				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
				}

				$data = array_merge($aHTTPRequest->GET, $aHTTPRequest->POST);

				unset( $data['AgendaID'] );

				$anInstance->updateFromArray( $data );

				$anInstance->save();

				$res['collection'][] = [ 
					'AgendaName' => $anInstance->getAgendaName()
					, 'UserName' => $anInstance->UserName
					, 'CheckpointName' => $anInstance->CheckPointName
					, 'FilterText' => $anInstance->FilterText
					, 'Status' => getStatusDesc( $anInstance->Status )
					, 'CaptureStartDate' => $anInstance->CaptureStartDate
					, 'CaptureStartTime' => implode(':', $anInstance->CaptureStartTime)
					, 'checkInTime' => $anInstance->getCheckInTime()
					, 'AgendaID' => $anInstance->AgendaID
					, 'canRemove' => $anInstance->canRemove( $aUser )
					, 'canEdit' => $anInstance->canEdit( $aUser )					
				];
			} else if( $action == 'edit' ) {

				$anAgendaID = getParamValue('selected', 'both', '(int)', true);

				$anInstance = BITAMSurveyAgenda::NewInstanceWithID($aRepository, $anAgendaID);

				$res['id'] = $anInstance->AgendaID;
				$res['AgendaSchedName'] = $anInstance->AgendaName;
				$res['UserID'] = $anInstance->UserID;
				$res['CheckPointID'] = $anInstance->CheckPointID;
				$res['SurveyIDs'] = $anInstance->SurveyIDs;
				$res['FilterText'] = explode('|', $anInstance->FilterText);
				$res['FilterTextAttr'] = explode('|', $anInstance->FilterTextAttr);
				$res['CaptureStartDate'] = $anInstance->CaptureStartDate;
				$res['CaptureStartTime_Hours'] = ( $anInstance->CaptureStartTime_Hours < 10 ? '0' . $anInstance->CaptureStartTime_Hours : $anInstance->CaptureStartTime_Hours );
				$res['CaptureStartTime_Minutes'] = ( $anInstance->CaptureStartTime_Minutes < 10 ? '0' . $anInstance->CaptureStartTime_Minutes : $anInstance->CaptureStartTime_Minutes );

				foreach ($res['FilterTextAttr'] as $key => $value)
				{
					$res['FilterTextValue'][ $value ] = $res['FilterText'][$key];
					//$res['DSC_' . $value][  ] = $res['FilterText'][$key];
				}
			}
		}

		header("Content-Type:application/json; charset=UTF-8");

		echo json_encode( $res );

		exit();
	}
}

function getStatusDesc( $intStatus )
{
	switch ( $intStatus ) {
		case agdOpen:
			$sStatus = translate('Open');
			break;
		case agdAnswered:
			$sStatus = translate('Answered');
			break;
		case agdRescheduled:
			$sStatus = translate('Rescheduled');
			break;
		case agdFinished:
			$sStatus = translate('Finished');
			break;
		case agdCanceled:
			$sStatus = translate('Canceled');
			break;
	}

	return $sStatus;
}

?>