<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
//para asignarlos a las variables de session que se ocupa en el model manager
//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

//Procedemos a actualizar el modelo
require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/model.inc.php");
require_once("../model_manager/modeldata.inc.php");
				
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Tomamos el primer objeto Survey, ya que este contiene los IDs relacionados con el cubo global "Surveys"
if(isset($surveyCollection->Collection[0]))
{
	$surveyInstance = $surveyCollection->Collection[0];

	if(!is_null($surveyInstance->GblSurveyGlobalDimID) && $surveyInstance->GblSurveyGlobalDimID>0 && !is_null($surveyInstance->GblCountIndID) && $surveyInstance->GblCountIndID>0)
	{
		$fieldName = "RIDIM_".$surveyInstance->GblSurveyGlobalDimID."KEY";

		$sql = "UPDATE SI_INDICADOR SET FORMULA_BD = ".$aRepository->ADOConnection->Quote("COUNT(DISTINCT t1.".$fieldName.")")." 
				WHERE CLA_CONCEPTO = ".$surveyInstance->GblSurveyModelID." AND CLA_INDICADOR = ".$surveyInstance->GblCountIndID;

		$status = "";
		if ($aRepository->ADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." SI_INDICADOR ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
			$status = "No Actualizada";
		}
		else 
		{
			echo('UPDATE: '.$sql.'<br>');
			$status = "Actualizada";
		}

		if($status == "Actualizada")
		{
			//Llamamos a esta funcion para poder exportar todo
			$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
			$anInstanceModel->ModelName = "Surveys";
			$anInstanceModel->readPeriods();
			$anInstanceModel->save();
			
			echo('Cubo Global Surveys => '.$status.'<br>');
		}
	}
}
?>