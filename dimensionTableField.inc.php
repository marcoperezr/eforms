<?php

require_once("tableField.inc.php");
//require_once("formula.inc.php");
$EKTFormatsPath='./config/Formats.ini';

// Contiene información referente a un campo real dentro de una tabla asociado a una dimensión
class BITAMDimensionTableField extends BITAMTableField
{
	public $consecutivo;
	public $cla_descrip;
	public $cla_concepto;
	public $OrderID;
	public $Formula;
	public $FormulaBD;
	public $AllValues;
	public $FormatMask;
	
	//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
	function __construct($aRepository, $aDimensionID = -1, $aCubeID = -1) //, $aDescripKey = -1)
	{	
		global $appSettings;
		
		$aDimension = null;
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if ($aDescripKey > 0)
			{
				$aDimension = BITAMDimension::NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID, false);
			}
		}
		elseif ($aDimensionID > 0 && $aCubeID > 0)
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		//}
		//@JAPR
		$intClaDescrip = -1;
		$strTableName = '';
		if (!is_null($aDimension))
		{
			$intClaDescrip = $aDimension->cla_descrip;
			$strTableName = $aDimension->nom_tabla;
		}
		
		BITAMTableField::__construct($aRepository, $strTableName, false);
		$this->consecutivo = $aDimensionID;
		$this->cla_descrip = $intClaDescrip;
		$this->cla_concepto = $aCubeID;
		$this->OrderID = 1;
		$this->Formula="";
		$this->FormulaBD="";
		$this->AllValues="";		
		$this->FormatMask="";
	}

	static function NewInstance($aRepository, $aDimensionID = -1, $aCubeID = -1)
	{
		return new BITAMDimensionTableField($aRepository, $aDimensionID, $aCubeID);
	}
	
	static function NewInstanceWithName($aRepository, $aDimensionID, $aCubeID, $aFieldName)
	{
		$anInstance = new BITAMDimensionTableField($aRepository, $aDimensionID, $aCubeID);
		$anInstance->getFieldData($aFieldName);
		if ($anInstance->isNewObject())
		{
			$anInstance = null;
		}
		
		$sql = 'SELECT A.CLA_DESCRIP, A.FieldName, A.OrderID, A.FormulaBD, A.FormatMask '.
			'FROM SI_EKT_DimensionFields A '.
			'WHERE A.CLA_DESCRIP = '.$anInstance->cla_descrip.' AND '.
				'A.FieldName = '.$aRepository->ADOConnection->Quote($aFieldName).' '.
			'ORDER BY A.CLA_DESCRIP, A.OrderID';
		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		$aRSInstance = null;
		if (!$aRS->EOF)
		{
			$aRSInstance = BITAMDimensionTableField::NewInstanceFromRS($aRepository, $aRS);
		}
		if (!is_null($aRSInstance))
		{
			$anInstance->OrderID = (int)$aRSInstance->OrderID;
			$anInstance->Formula = (string)$aRSInstance->Formula;
			$anInstance->FormulaBD = (string)$aRSInstance->FormulaBD;
			$anInstance->FormatMask = (string)$aRSInstance->FormatMask;
		}
		return $anInstance;
	}

	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMDimensionTableField::NewInstance($aRepository);
		$anInstance->OrderID = (int)$aRS->fields["orderid"];
		//$anInstance->Formula = (string)$aRS->fields["formula"];
		$anInstance->FormulaBD = (string)$aRS->fields["formulabd"];
		//@JAPRWarning: Funcionalidad faltante
		if(false)
		{
		 	$aFormulaInstance=BITAMFormula::NewInstanceWithStrFormula($aRepository,$anInstance->FormulaBD,true,null,8,-1);
		 	$anInstance->Formula = $aFormulaInstance->getStrFormulaWithCubeAndIndicatorName();
		}
		//@JAPR
		$anInstance->FormatMask = (string)$aRS->fields["formatmask"];
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = null;
		$aFieldName = '';
		$aDimensionID = -1;
		$aCubeID = -1;
		if (array_key_exists("DimensionID", $aHTTPRequest->GET) && array_key_exists("CubeID", $aHTTPRequest->GET))
		{
			$aDimensionID = (int)$aHTTPRequest->GET["DimensionID"];
			$aCubeID = (int)$aHTTPRequest->GET["CubeID"];
		}
		
		//Actualiza las fórmulas
		if (array_key_exists("Name", $aHTTPRequest->POST))
		{
			$aFieldName = $aHTTPRequest->POST["Name"];
			if ($aFieldName != '' && $aDimensionID > 0 && $aCubeID > 0)
			{
				$anInstance = BITAMDimensionTableField::NewInstanceWithName($aRepository, $aDimensionID, $aCubeID, $aFieldName);
				if (!is_null($anInstance))
				{
					$anInstance->updateFromArray($aHTTPRequest->GET);
					$anInstance->updateFromArray($aHTTPRequest->POST);
					$anInstance->save();
					if (array_key_exists("go", $_POST))
					{
						$go = $_POST["go"];
					}
					else
					{
						$go = "self";
					}
					if ($go == "parent")
					{
						$anInstance = $anInstance->get_Parent();
					}
					else
					{
						if ($go == "new")
						{
							$go == "self";
						}
					}
				}
			}
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		
		if (array_key_exists("FieldName", $aHTTPRequest->GET))
		{
			$aFieldName = $aHTTPRequest->GET["FieldName"];
		}
		
		if ($aFieldName != '' && $aDimensionID > 0 && $aCubeID > 0)
		{
			$anInstance = BITAMDimensionTableField::NewInstanceWithName($aRepository, $aDimensionID, $aCubeID, $aFieldName);
			if (is_null($anInstance))
			{
				$anInstance = BITAMDimensionTableField::NewInstance($aRepository, $aDimensionID, $aCubeID);
			}
		}
		else
		{
			$anInstance = BITAMDimensionTableField::NewInstance($aRepository, $aDimensionID, $aCubeID);
		}
		return $anInstance;
	}
	
	function ConvertFormula($formula,$filtros)
	{
		$result = array();
		$dimen = null;
		$x = null;
		
		$cTotal = explode('+',$filtros);
		/*
		cTotal[0] = filtros
		cTotal[1] = Dimensiones
		*/
		$aInstanceFormula = BITAMFormula::NewInstanceWithStrFormula($this->Repository,$formula,false,null,-1,-1,true);
		
		//No capturables
		foreach ($aInstanceFormula->NoCapIndIDs as $key => $value )
		{
			$noCapId = '{'.$value.'}';
			$noCapName = $aInstanceFormula->NoCapTermReplace[$key];
			$formula = str_replace($noCapName,$noCapId,$formula);
		}
		
		//capturables
		/*
		foreach ($aInstanceFormula->CapIndIDs as $key => $value )
		{
			$noCapId = '['.$value.']';
			$noCapName = $aInstanceFormula->CapTermReplace[$key];
			$formula = str_replace($noCapName,$noCapId,$formula);
		}
		*/
		//Indicadores Globales
		foreach ($aInstanceFormula->GlobalIndIDs as $key => $value )
		{
			$noCapId = '#'.$value;
			$noCapName = $aInstanceFormula->GlobalIndReplace[$key];
			$formula = str_replace($noCapName,$noCapId,$formula);
		}
		
		if($cTotal[0] != "")
		{
			$sql = "DELETE FROM SI_EKT_DimFieldsFormulaFilter ".
				"WHERE CLA_DESCRIP = ".$this->cla_descrip." AND FieldName = ".$this->Repository->ADOConnection->Quote($this->Name);
			$aRS = $this->Repository->ADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die ( translate("Error deleting")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$x = $cTotal[0];
			if ( $x != "")
			{
				$y = substr($x,0,strlen($x)-1);
				$w = explode('|',$y);
				
				for($i=0;$i<count($w);$i++)
				{
					$a = stripos($w[$i],',');
					$w[$i][$a] = ';';
					$b = explode(';',$w[$i]);
					
					$sql = "INSERT INTO SI_EKT_DimFieldsFormulaFilter (CLA_DESCRIP,FieldName,FormulaIndicatorID,FilterID,Filter,Levels) ".
						"VALUES (".$this->cla_descrip.",".$this->Repository->ADOConnection->Quote($this->Name).",".
							$b[0].",".$b[1].",".$this->Repository->ADOConnection->Quote($b[2]).",".$this->Repository->ADOConnection->Quote($b[3]).")";
					$aRS = $this->Repository->ADOConnection->Execute($sql);
					if ($aRS === false)
					{
						die ( translate("Error inserting")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
				}
			}
		}
		
		if($cTotal[1]!="")
		{
		   $aux = explode(",",$cTotal[1]); 
           sort($aux); 
           reset($aux); 
	       $dimen = implode(",",$aux); 
		}
		
		$result[0]= $formula;
		$result[1]= $dimen;
		
		return $result;
	}
	
	function save()
	{
		$x='';
		$y='';
		$Termformula = array();
		
	 	if ($this->isNewObject())
		{
		}
		else
		{
			//@JAPRWarning: Funcionalidad faltante
			//$Termformula = $this->ConvertFormula($this->Formula,$this->AllValues);
			$strFormatMask = $this->FormatMask;
			if ($strFormatMask === '\'0\'')
			{
				$strFormatMask = '0';
			}
			$sql = "UPDATE SI_EKT_DimensionFields SET ".
					"OrderID = ".$this->OrderID.", ".
					//"FormulaBD = ".$this->Repository->ADOConnection->Quote($Termformula[0]).", ".
					"FormatMask = ".$this->Repository->ADOConnection->Quote($strFormatMask).
				" WHERE CLA_DESCRIP = ".$this->cla_descrip." AND FieldName = ".$this->Repository->ADOConnection->Quote($this->Name);
			if ($this->Repository->ADOConnection->Execute($sql) === false)
			{
				die( translate("Error updating")." SI_EKT_DimensionFields ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if(trim($this->Formula) == "")
			{
				$sql="DELETE FROM SI_EKT_DimFieldsFormulaFilter ".
					"WHERE CLA_DESCRIP = ".$this->cla_descrip." AND FieldName = ".$this->Repository->ADOConnection->Quote($this->Name);
				if ($this->Repository->ADOConnection->Execute($sql) === false)
				{
					die( translate("Error deleting")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		return $this;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("OrderID", $anArray))
		{
			$this->OrderID = (int)$anArray["OrderID"];
		}
		
		if (array_key_exists("Formula", $anArray))
		{
			$this->Formula = (string)$anArray["Formula"];
		}
		
		if (array_key_exists("FormulaBD", $anArray))
		{
			$this->FormulaBD = (string)$anArray["FormulaBD"]; 
		}
		
		if (array_key_exists("FormatMask", $anArray))
		{
			$this->FormatMask = (string)$anArray["FormatMask"];
		}
 		
		if (array_key_exists("AllValues", $anArray))
		{
			$this->AllValues = $anArray["AllValues"];
		}
		
		return $this;
	}
	
	function saveAssociation()
	{
		$sql = "INSERT INTO SI_EKT_DimensionFields (CLA_DESCRIP, FieldName, OrderID) VALUES (".
			((int) $this->cla_descrip).", ".$this->Repository->ADOConnection->Quote($this->Name).", ".$this->OrderID.")";
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function get_QueryString()
	{
		global $appSettings;
		
		if ($this->isNewObject())
		{
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				return "BITAM_PAGE=DimensionTableField&DescripID=".$this->cla_descrip.'&CubeID='.$this->cla_concepto;
			}
			else
			{
			*/
				return "BITAM_PAGE=DimensionTableField&DimensionID=".$this->consecutivo.'&CubeID='.$this->cla_concepto;
			//}
			//@JAPR
		}
		else
		{
			//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
			/*
			if ($appSettings["UseGlobalDimensions"] == 1)
			{
				return "BITAM_PAGE=DimensionTableField&DescripID=".$this->cla_descrip.'&CubeID='.$this->cla_concepto.'&FieldName='.$this->Name;
			}
			else
			{
			*/
				return "BITAM_PAGE=DimensionTableField&DimensionID=".$this->consecutivo.'&CubeID='.$this->cla_concepto.'&FieldName='.$this->Name;
			//}
			//@JAPR
		}
	}

	function isNewObject()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			return ($this->consecutivo < 0 || $this->Name == '');
		}
		else
		{
			return ($this->consecutivo < 0 || $this->cla_concepto < 0 || $this->Name == '');
		}
		//@JAPR
	}

	function get_Parent()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if ($this->cla_descrip)
			{
				return $aDimension = BITAMDimensionTableFieldCollection::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto);
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
		*/
			if (($this->cla_concepto > 0 || $appSettings["UseGlobalDimensions"] == 1) && $this->consecutivo > 0)
			{
				return $aDimension = BITAMDimensionTableFieldCollection::NewInstance($this->Repository, $this->consecutivo, $this->cla_concepto);
			}
			else 
			{
				return $this->Repository;
			}
		//}
		//@JAPR
	}
	
	function canEdit($aUser)
	{
		return true;
	}
	
	function get_FormFields($aUser)
	{
		global $EKTFormatsPath;
		
		$myFields = parent::get_FormFields($aUser);
		foreach ($myFields as $aField)
		{
			$aField->IsDisplayOnly = true;
		}
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OrderID";
		$aField->Title = translate("Order");
		$aField->Type = "Integer";
		$aField->Size = 10;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Formula";
		$aField->Title = translate("Formula");
		//$aField->IsDisplayOnly = true;
		$aField->Type = "LargeString";
		$aField->Size = 80;
		$aField->OnChange = "checkFormulaIndicators();";
		$aField->AfterMessage = "<img id=editFormulaImg name=editFormulaImg style=\"display:none\" src=\"images/editFormula.gif\" onclick=\"javascript:modifyFormula();\">";
		$myFields[$aField->Name] = $aField;

		/* Formatos disponibles leídos del archivo ./config/Formats.ini, si no existe el archivo, lo crea con formatos default
		*/
		$aFormatArray = array();
		$fHandler = @fopen($EKTFormatsPath, "r");
		if ($fHandler)
		{
			// Lee los formatos configurados previamente
			while (!feof($fHandler))
			{
		        $strBuffer = trim(fgets($fHandler));
		        if (!array_key_exists($strBuffer, $aFormatArray))
		        {
			        if ($strBuffer === '0')
			        {
			        	$aFormatArray['\''.$strBuffer.'\''] = $strBuffer;
			        }
			        else
			        {
		        		$aFormatArray[$strBuffer] = $strBuffer;
			        }
		        }
		    }
		    fclose($fHandler);			
		}
		else 
		{
			// Genera el archivo con los formatos default
			$aFormatArray[''] = '';
			$aFormatArray['\'0\''] = '0';
			$aFormatArray['#'] = '#';
			$aFormatArray['#,##0'] = '#,##0';
			$aFormatArray['$#,##0'] = '$#,##0';
			$aFormatArray['#,##0.00'] = '#,##0.00';
			$aFormatArray['$#,##0.00'] = '$#,##0.00';
			$aFormatArray['# %'] = '# %';
			$aFormatArray['0 %'] = '0 %';
			$aFormatArray['0.0 %'] = '0.0 %';
			$strBuffer = implode("\r\n", $aFormatArray);
			$fHandler = @fopen($EKTFormatsPath, 'w');
			if ($fHandler) 
			{
				fwrite($fHandler, $strBuffer);
				fclose($fHandler);
			}
		}

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormatMask";
		if ($this->FormatMask === '0')
		{
			$this->FormatMask = '\'0\'';
		}
		$aField->Title = translate("Format");
		$aField->Type = "Object";
		$aField->VisualComponent = "ComboBox";
		$aField->Options = $aFormatArray;
		$aField->Size = 30;
		$myFields[$aField->Name] = $aField; //AllValues
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FormulaBD";
		$aField->Title = "";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField; //AllValues

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AllValues";
		$aField->Title = "";
		$aField->Type = "LargeString";
		$aField->Size = 5000;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
	{
		$pathFilterPage = '';
		$sql="SELECT SettingsVariableName, Value from SI_EKT_SettingsVariables WHERE SettingsVariableName IN (".
			$this->Repository->ADOConnection->Quote("WEBPATH").', '.
			$this->Repository->ADOConnection->Quote("PROJECTNAME").')';
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if (!$aRS)
		{
				die( translate("Error accessing")." SI_EKT_SettingsVariables ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		while (!$aRS->EOF)
		{
			$aSettingVariableName = $aRS->fields["settingsvariablename"];
			switch ($aSettingVariableName)
			{
				case 'WEBPATH':
					$pathFilterPage = $aRS->fields["value"];
					break;
				case 'PROJECTNAME':
					$projectname = $aRS->fields["value"];
					break;
			}
	      	$aRS->MoveNext(); 
		}
		
		$_SESSION["pathFilter"]=$pathFilterPage;
		$_SESSION["project"]=$projectname;
 		
		//@JAPRWarning: Me parece que "checkFormulaIndicators" ya no se debe usar, no genera nada útil
		//regCad -> Carga los filtros por indicador aplicados en la fórmula del campo de dimensión
?>
	<script language="JavaScript">
		arrayGral = new Array();
		arrayIndice = new Array();
		arrayValues= new Array();
		arrayAuxValues = new Array();
		arrayDimA = new Array();
		aCheckedDimension = new Array();
		val='';
		dimension='';
		
        function regCad()
        {
	       cadGral = "";
<?
		$sql = "SELECT * FROM SI_EKT_DimFieldsFormulaFilter A ".
       		"WHERE A.CLA_DESCRIP = ".$this->cla_descrip. " AND A.FieldName = ".$this->Repository->ADOConnection->Quote($this->Name);
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if (!$aRS)
		{
			die( translate("Error accessing")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}           
		
		if (!$aRS->EOF) 
		{
       		$blnAddLevels = false;
			//BLOERA : Entra aqui cuando EXISTA filtros y Dimensiones en las colummas estadisticas 
			while (!$aRS->EOF)
			{
				$blnAddLevels = true;
?>
	        cadGral = cadGral +'<?=$aRS->fields["formulaindicatorid"]?>'+';'+'<?=$aRS->fields["filterid"]?>'+';'+'<?=$aRS->fields["filter"]?>'+','+'<?=$aRS->fields["levels"]?>'+'|';
<?
		      	$aRS->MoveNext(); 
			}
			
			//Las fórmulas de campos dimensiones no se pueden agrupar por dimensiones, así que esta parte no aplica
?>
			cadGral = cadGral + '+';
<?
			/*
			if ($blnAddLevels)
			{
	       		$sql = "SELECT SharedDimensions FROM SI_EKT_CalculatedIndicators WHERE BudgetID  =".$this->BudgetID. " AND CalculatedIndicatorID =".$this->CalculatedIndicatorID;
				$aRS = $this->Repository->ADOConnection->Execute($sql);
				if (!$aRS)
				{
					die( translate("Error accessing")." SI_EKT_CalculatedIndicators ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				if(!$aRS->EOF)
				{
?>
			cadGral = cadGral + '+' + '<?=$aRS->fields["shareddimensions"]?>'; 
<?
				}
			}
			*/
		}           
		else 
		{
			//BLOERA : Entra aqui cuando no haya filtro  solo dimensiones marcadas en las colummas estadisticas
			//Las fórmulas de campos dimensiones no se pueden agrupar por dimensiones, así que esta parte no aplica
?>
			cadGral = cadGral + '+';
<?
			/*
			$sql2 = "SELECT SharedDimensions FROM SI_EKT_CalculatedIndicators WHERE BudgetID=".$this->BudgetID. " AND CalculatedIndicatorID=".$this->CalculatedIndicatorID;                                                                        
				
			$aRS2 = $this->Repository->ADOConnection->Execute($sql2);
			if (!$aRS2)
			{
				die( translate("Error accessing")." SI_EKT_CalculatedIndicators ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			if(!$aRS2->EOF)
			{
?>
			cadGral = cadGral + '+' + '<?=$aRS2->fields["shareddimensions"]?>'; 
<?
			}
			*/
		}
?>
			return cadGral;
        }

<?
		//Genera el arreglo de filtros por indicador aplicado en la fórmula
		$sql = "SELECT * FROM SI_EKT_DimFieldsFormulaFilter A ".
       		"WHERE A.CLA_DESCRIP = ".$this->cla_descrip. " AND A.FieldName = ".$this->Repository->ADOConnection->Quote($this->Name);
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if (!$aRS)
		{
			die( translate("Error accessing")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$intLastFormulaIndicatorID = '';
		$intFormulaIndicatorID = '';
		$i=0;
		$j=0;
		while (!$aRS->EOF)
		{
			$intFormulaIndicatorID = (int)$aRS->fields["formulaindicatorid"];
			if ($intFormulaIndicatorID != $intLastFormulaIndicatorID)
			{	
				$sql1 =  " SELECT ".$this->Repository->ADOConnection->IfNull("MAX(A.FilterID)", "0")." AS FilterID FROM SI_EKT_DimFieldsFormulaFilter A ".
	       			"WHERE A.CLA_DESCRIP = ".$this->cla_descrip. " AND A.FieldName = ".$this->Repository->ADOConnection->Quote($this->Name)." AND A.FormulaIndicatorID = ".$intFormulaIndicatorID;
				$aRS1 = $this->Repository->ADOConnection->Execute($sql1);
				if (!$aRS1)
				{
					die( translate("Error accessing")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql1);
				}
				$intNextFilerID = $aRS1->fields["FilterID"];
?>
 		arrayIndice[<?=$j?>]=[<?=$intFormulaIndicatorID?>,<?=$intNextFilerID?>];
<?
				$j++;
			}
?>
 		arrayValues[<?=$i?>]=['<?=$intFormulaIndicatorID?>',<?=(int)$aRS->fields["filterid"]?>,'<?=(string)$aRS->fields["filter"]?>','<?=(string)$aRS->fields["levels"]?>'];
<?
 			$intLastFormulaIndicatorID = $intFormulaIndicatorID;
 			$i++;
			$aRS->MoveNext();
		}

		//El periodo es para invocar a la ventana de filtros, así que cualquier periodo del cubo sirve
		$aPeriodID = 4;
		$aPeriodDimensionCollection = BITAMDimensionCollection::NewInstancePeriodDims($this->Repository, $this->cla_concepto, false);
		if (!is_null($aPeriodDimensionCollection))
		{
			//Obtenemos un periodo existente en este cubo, de preferencia Mensual si está asociado, si no, el último encontrado
			foreach ($aPeriodDimensionCollection as $aPeriodDimension)
			{
				$aPeriodID = $aPeriodDimension->cla_periodo;
				if ($aPeriodID == 4)
				{
					break;
				}
			}
		}
		
?>
		periodID = <?=$aPeriodID?>;

		arrayGral[0] = arrayIndice;
 		arrayGral[1] = arrayValues;
		arrayGral[4] = arrayDimA;
 		arrayGral[5] = 1;
 		arrayGral[6] = aCheckedDimension;

		function modifyFormula()
		{
			
			var dimen = "";
			var anOrigFormula = BITAMDimensionTableField_SaveForm.Formula.value;
			arrayGral = showModalDialog("buildformulaCalInd.php?formulaType=8", [BITAMDimensionTableField_SaveForm.Formula.value,periodID,'<?=$_SERVER["HTTP_REFERER"]?>','<?=$_SESSION["pathFilter"]?>',arrayGral], 'dialogHeight:400px; dialogWidth:510px; center: yes; help: no; status: no; scroll: no; resizable: no;');
			
			BITAMDimensionTableField_SaveForm.Formula.value = arrayGral[2];
			arrayValues = arrayGral[1];
			arrayIndice = arrayGral[0];
			aCheckedDimension = arrayGral[6];
			dimension = '';
			if (arrayGral[5]==0)
			{
				arrayValues = arrayGral[7];
				BITAMDimensionTableField_SaveForm.Formula.value = anOrigFormula;
				for(i = 0; i < arrayValues.length; i++)
				{
					if(arrayValues[i] != undefined)
					{
						indID = arrayValues[i][0];
						filID = arrayValues[i][1];
						filtro = arrayValues[i][2];
						nivel = arrayValues[i][3];
						
						val=val+indID+';'+filID+';'+filtro+','+nivel+'|';
					}
				}
				
				for ( i =0; i<aCheckedDimension.length; i++)
				{
					dimen+=aCheckedDimension[i]+',';
				}
				
				if (dimen.length > 0)
				{
					dimension = dimen.substring(0,dimen.length-1);
				}
			}
			else
			{
				BITAMDimensionTableField_SaveForm.Formula.value = arrayGral[2];
				arrayValues = arrayGral[1];
				arrayIndice = arrayGral[0];
				aCheckedDimension = arrayGral[6];
				
				for(i = 0; i < arrayValues.length; i++)
				{
					if(arrayValues[i] != undefined)
					{
						indID = arrayValues[i][0];
						filID = arrayValues[i][1];
						filtro = arrayValues[i][2];
						nivel = arrayValues[i][3];
						
						val=val+indID+';'+filID+';'+filtro+','+nivel+'|';
					}
				}
				
				for ( i =0; i<aCheckedDimension.length; i++)
				{
					dimen+=aCheckedDimension[i]+',';
				}
				
				if (dimen.length > 0)
				{
					dimension = dimen.substring(0,dimen.length-1);
				}
			}
			
			val += '+'+dimension;
			BITAMDimensionTableField_SaveForm.AllValues.value = val;
			dimension = '';
			val = '';
		}
		
        function checkFormulaIndicators()
		{
			/*
			var formula=BITAMDimensionTableField_SaveForm.Formula.value;
			//Obtener los indicadores incluidos en la fórmula
			
			var formulaElements=formula.split("[");
			
			//Arreglo de cadenas q contiene los nombres de los indicadores incluidos en la fórmula
			var formulaIndicatorNames= new Array();
			
			var numElements=formulaElements.length;
			
			var elements=new Array();
			var indicatorStr="";
			var count=0;
			
			for (i=0; i<numElements; i++)
			{
				if(formulaElements[i].search("]")>=0)
				{	
					
					elements=formulaElements[i].split("]");
					
					indicatorStr=elements[0];
	
					if (indicatorStr!="")
					{
						formulaIndicatorNames[count]=indicatorStr;
						count++;
					}
					
				}
			}
			var valid=true;
			var numFormulaIndicators=formulaIndicatorNames.length;
			var numIndicators=BITAMDimensionTableField_SaveForm.IndicatorID.length;
			var indicators= BITAMDimensionTableField_SaveForm.IndicatorID;
			*/
		}
		
        function CustomizedEdit()
        {
           if(BITAMDimensionTableField_SaveForm.AllValues.value == "")
           {
	           BITAMDimensionTableField_SaveForm.AllValues.value = regCad();
	           BITAMDimensionTableField_Edit();
           }
           else
           {
               BITAMDimensionTableField_Edit()
           }
        }
        //<-----------BLOERA
		
	</script>
<?
	}

 	//Se agrego esta funcion para que carge inicialmente la dims
	function generateAfterFormCode($aUser)
    {
    	//BLOERA  -->>
?>
    <script language="JavaScript">              
<?
       if(!$this->isNewObject())
       {
?>
	       objEditButton = document.getElementById("BITAMDimensionTableField_EditButton");
	       objEditButton.onclick = new Function("CustomizedEdit();");
<?
       }
?>                                           
       BITAMDimensionTableField_SaveForm.AllValues.value = regCad();
    </script>                      
<?
	    //BLOERA  -->>
    }

}

// Contiene una colección de campos referentes a una tabla específica asociada a una dimensión
class BITAMDimensionTableFieldCollection extends BITAMTableFieldCollection
{
	public $consecutivo;
	public $cla_descrip;
	public $cla_concepto;
	
	function __construct($aRepository, $aDimensionID = -1, $aCubeID = -1, $aDescripKey = -1)
	{	
		global $appSettings;

		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			$aDimension = BITAMDimension::NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID, false);
		}
		else 
		{
		*/
			$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		//}
		//@JAPR
		$intClaDescrip = -1;
		$strTableName = '';
		if (!is_null($aDimension))
		{
			$intClaDescrip = $aDimension->cla_descrip;
			$strTableName = $aDimension->nom_tabla;
		}
		
		BITAMTableFieldCollection::__construct($aRepository, $strTableName, false);
		$this->consecutivo = $aDimensionID;
		$this->cla_descrip = $intClaDescrip;
		$this->cla_concepto = $aCubeID;
	}

	// Recupera todos los campos de la tabla de catálogo ya asociados a la dimensión
	static function NewInstance($aRepository, $aDimensionID = -1, $aCubeID = -1)
	{
		$anInstance =  new BITAMDimensionTableFieldCollection($aRepository, $aDimensionID, $aCubeID);
		$arrFields = array();
		$arrExcludedFields = array();
		$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		
		if (is_null($aDimension))
		{
			return $anInstance;
		}

		$arrExcludedFields[strtoupper($aDimension->nom_fisico)] = strtoupper($aDimension->nom_fisico);
		$arrExcludedFields[strtoupper($aDimension->nom_fisicok)] = strtoupper($aDimension->nom_fisicok);
		if (count($aDimension->FatherParDescriptor) > 0)
		{
			$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
			if (!is_null($aParentDimension))
			{
				$arrExcludedFields[strtoupper($aParentDimension->nom_fisicok)] = $aParentDimension->nom_fisicok;
			}
		}
		
		$sql = "SELECT A.CLA_DESCRIP, A.FieldName, A.OrderID, A.FormulaBD, A.FormatMask FROM SI_EKT_DimensionFields A ".
			"WHERE A.CLA_DESCRIP = ".$aDimension->cla_descrip." ORDER BY A.OrderID";
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$aFieldName = $aRS->fields["fieldname"];
			if (!array_key_exists(strtoupper($aFieldName), $arrExcludedFields))
			{
				//Arreglo bidimensional indexado por nombre. Contiene toda la info para llenar los campos
				$arrFields[strtoupper($aFieldName)] = array();
				$arrFields[strtoupper($aFieldName)][] = $aFieldName;
				$arrFields[strtoupper($aFieldName)][] = (int)$aRS->fields["orderid"];
				//$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formula"];
				$formulaBD=(string)$aRS->fields["formulabd"];
				//@JAPRWarning: Funcionalidad faltante
				if(false)
				{
				 	$aFormulaInstance=BITAMFormula::NewInstanceWithStrFormula($aRepository,$formulaBD,true,null,8,-1);
				 	$arrFields[strtoupper($aFieldName)][] = $aFormulaInstance->getStrFormulaWithCubeAndIndicatorName();
				}
			 	$arrFields[strtoupper($aFieldName)][] = '';
				//@JAPR
				$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formulabd"];
				$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formatmask"];
			}
			$aRS->MoveNext();
		}
		if (count($arrFields) > 0)
		{
			$anInstance->getFieldCollectionData($arrFields, true, true);
		}
		
		return $anInstance;
	}

	//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
	// Recupera todos los campos de la tabla de catálogo ya asociados a la dimensión
	/*
	static function NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID = -1)
	{
		$anInstance =  new BITAMDimensionTableFieldCollection($aRepository, -1, $aCubeID, $aDescripKey);
		$arrFields = array();
		$arrExcludedFields = array();
		$aDimension = BITAMDimension::NewInstanceWithDescripID($aRepository, $aDescripKey, $aCubeID, false);
		
		if (is_null($aDimension))
		{
			return $anInstance;
		}

		$arrExcludedFields[strtoupper($aDimension->nom_fisico)] = strtoupper($aDimension->nom_fisico);
		$arrExcludedFields[strtoupper($aDimension->nom_fisicok)] = strtoupper($aDimension->nom_fisicok);
		if (count($aDimension->FatherParDescriptor) > 0)
		{
			$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
			if (!is_null($aParentDimension))
			{
				$arrExcludedFields[strtoupper($aParentDimension->nom_fisicok)] = $aParentDimension->nom_fisicok;
			}
		}
		
		$sql = "SELECT A.CLA_DESCRIP, A.FieldName, A.OrderID, A.FormulaBD, A.FormatMask FROM SI_EKT_DimensionFields A ".
			"WHERE A.CLA_DESCRIP = ".$aDimension->cla_descrip." ORDER BY A.OrderID";
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$aFieldName = $aRS->fields["fieldname"];
			if (!array_key_exists(strtoupper($aFieldName), $arrExcludedFields))
			{
				//Arreglo bidimensional indexado por nombre. Contiene toda la info para llenar los campos
				$arrFields[strtoupper($aFieldName)] = array();
				$arrFields[strtoupper($aFieldName)][] = $aFieldName;
				$arrFields[strtoupper($aFieldName)][] = (int)$aRS->fields["orderid"];
				//$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formula"];
				$formulaBD=(string)$aRS->fields["formulabd"];
				//@JAPRWarning: Funcionalidad faltante
				if(false)
				{
				 	$aFormulaInstance=BITAMFormula::NewInstanceWithStrFormula($aRepository,$formulaBD,true,null,8,-1);
				 	$arrFields[strtoupper($aFieldName)][] = $aFormulaInstance->getStrFormulaWithCubeAndIndicatorName();
				}
			 	$arrFields[strtoupper($aFieldName)][] = '';
				//@JAPR
				$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formulabd"];
				$arrFields[strtoupper($aFieldName)][] = (string)$aRS->fields["formatmask"];
			}
			$aRS->MoveNext();
		}
		if (count($arrFields) > 0)
		{
			$anInstance->getFieldCollectionData($arrFields, true, true);
		}
		
		return $anInstance;
	}
	*/
	//@JAPR
	
	// Recupera todos los campos de la tabla de catálogo, excepto aquellos que son parte de la definición de la dimensión (clave y descripción, además de la clave del Padre)
	static function NewInstanceForAssociation($aRepository, $aDimensionID, $aCubeID)
	{
		$anInstance =  new BITAMDimensionTableFieldCollection($aRepository, $aDimensionID, $aCubeID);
		$arrFields = array();
		$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		
		if (!is_null($aDimension))
		{
			$arrFields[strtoupper($aDimension->nom_fisico)] = $aDimension->nom_fisico;
			$arrFields[strtoupper($aDimension->nom_fisicok)] = $aDimension->nom_fisicok;
			//Se agrego este elemento ya que hay que registrar la llave subrogada si es que existe, para 
			//que no sea tomada en cuenta como un campo adicional
			$arrFields[strtoupper($aDimension->nomfisicok_descripkeys)] = $aDimension->nomfisicok_descripkeys;
			//@JAPR 2010-01-28: Agregada la carga automática de Dimensiones - Atributo
			$featuresDimension = BITAMDimension::getFeaturesDimension($aRepository, $aDimensionID, $aCubeID);
			if (!is_null($featuresDimension))
			{
				if (array_key_exists('attributes', $featuresDimension))
				{
					foreach ($featuresDimension['attributes'] as $anAttributeConsecutive => $anAttributeInfo)
					{
						//Se asume que las dimensiones - atributo siempre son dimensiones sin Llave, puesto que una llave alternativa a la de la 
						//dimensión primaria (o a la Subrogada en todo caso) es innecesaria realmente, por ende sólo agregamos este campo
						$strAttributeFieldName = $anAttributeInfo["attribute_nom_fisicok_bd"];
						if (trim($strAttributeFieldName) != '')
						{
							$arrFields[strtoupper($strAttributeFieldName)] = $strAttributeFieldName;
						}
					}
					
					if (array_key_exists('parents', $featuresDimension))
					{
						foreach ($featuresDimension['parents'] as $aParentConsecutive => $aParentInfo)
						{
							$strParentFieldName = $aParentInfo["parent_nom_fisicok_join"];
							if (trim($strParentFieldName) != '')
							{
								$arrFields[strtoupper($strParentFieldName)] = $strParentFieldName;
							}
						}
					}
				}
			}
			//@JAPR
			
			if (count($aDimension->FatherParDescriptor) > 0)
			{
				$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
				if (!is_null($aParentDimension))
				{
					$arrFields[strtoupper($aParentDimension->nom_fisicok)] = $aParentDimension->nom_fisicok;
				}
			}
		}

		$anInstance->getFieldCollectionData($arrFields, false);
		
		return $anInstance;
	}
	
	// Recupera todos los campos de la tabla de catálogo que fueron seleccionados para asociación
	static function NewInstanceForSaving($aRepository, $aDimensionID, $aCubeID, $aFieldsCollection)
	{
		$anInstance =  new BITAMDimensionTableFieldCollection($aRepository, $aDimensionID, $aCubeID);
		$arrFields = array();
		$arrExcludedFields = array();
		$aDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimensionID, $aCubeID, false);
		
		if (!is_null($aDimension))
		{
			$arrExcludedFields[strtoupper($aDimension->nom_fisico)] = $aDimension->nom_fisico;
			$arrExcludedFields[strtoupper($aDimension->nom_fisicok)] = $aDimension->nom_fisicok;
			if (count($aDimension->FatherParDescriptor) > 0)
			{
				$aParentDimension = BITAMDimension::NewInstanceWithID($aRepository, $aDimension->FatherParDescriptor[0], $aCubeID, false);
				if (!is_null($aParentDimension))
				{
					$arrExcludedFields[strtoupper($aParentDimension->nom_fisicok)] = $aParentDimension->nom_fisicok;
				}
			}
		}

		foreach ($aFieldsCollection as $anIndex => $aFieldName)
		{
			if (!array_key_exists(strtoupper($aFieldName), $arrExcludedFields))
			{
				$arrFields[strtoupper($aFieldName)] = $aFieldName;
			}
		}
		
		$anInstance->getFieldCollectionData($arrFields, true);
		
		return $anInstance;
	}
	
	static function NewInstanceEmpty($aRepository, $aDimensionID = -1, $aCubeID = -1)
	{
		return new BITAMDimensionTableFieldCollection($aRepository, $aDimensionID, $aCubeID);
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = null;
		$aDimensionID = -1;
		$aDescripID = -1;
		$aCubeID = -1;
		if (array_key_exists("DimensionID", $aHTTPRequest->GET) && array_key_exists("CubeID", $aHTTPRequest->GET)  && array_key_exists("DescripID", $aHTTPRequest->GET))
		{
			$aDimensionID = (int)$aHTTPRequest->GET["DimensionID"];
			$aDescripID = (int)$aHTTPRequest->GET["DescripID"];
			$aCubeID = (int)$aHTTPRequest->GET["CubeID"];
		}

		if ($aDimensionID > 0 && $aCubeID > 0)
		{
			$action = array_key_exists("action", $aHTTPRequest->GET) ? $aHTTPRequest->GET["action"] : "";
			if (array_key_exists("Name", $aHTTPRequest->POST))
			{
				$aFieldName = $aHTTPRequest->POST["Name"];
				if (is_array($aFieldName))
				{
					$anArrayOfFieldNames = $aFieldName;
					switch ($action)
					{
						case 'associate':
							$filter = "";
							switch (count($anArrayOfFieldNames))
							{
								case 0:
									break;
								case 1:
									$filter = " AND FieldName <> ".$aRepository->ADOConnection->Quote($anArrayOfFieldNames[0]);
									break;
								default:
									foreach ($anArrayOfFieldNames as $aFieldName)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= $aRepository->ADOConnection->Quote($aFieldName);
									}
									if ($filter != "")
									{
										$filter = " AND FieldName NOT IN (".$filter.")";
									}
									break;
							}
							//Borrar los campos asociados a la dimensión menos los que deben permanecer asociados
							$sql = "DELETE FROM SI_EKT_DimFieldsFormulaFilter WHERE CLA_DESCRIP = ".((int) $aDescripID).$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die(translate("Error accessing")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							$sql = "DELETE FROM SI_EKT_DimensionFields WHERE CLA_DESCRIP = ".((int) $aDescripID).$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die(translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Obtener cuales campos ya estaban asociados anteriormente para no realizar la asociación otra vez
							$sql = "SELECT FieldName FROM SI_EKT_DimensionFields WHERE CLA_DESCRIP = ".$aDescripID;
							$aRS = $aRepository->ADOConnection->Execute($sql);
							if ($aRS === false)
							{
								die(translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
							}
							
							//Elementos ya asociados
							$associatedFieldNames=array();
							while (!$aRS->EOF)
							{
								$associatedFieldNames[] = $aRS->fields["fieldname"];
								$aRS->MoveNext();
							}
							
							//Elementos que faltan por asociarse
							$elementsToAssociate=array();
							for ($i=0; $i < count($anArrayOfFieldNames) ;$i++)
							{
								if (!in_array($anArrayOfFieldNames[$i], $associatedFieldNames))
								{
									$elementsToAssociate[] = $anArrayOfFieldNames[$i];		
								}
							}

							//Asociar elementos
							if(count($elementsToAssociate) > 0)
							{
								$intOrderID = 1;
								$sql = 'SELECT '.
									$aRepository->ADOConnection->IfNull('MAX(A.OrderID)', '0').' + 1 AS OrderID FROM SI_EKT_DimensionFields A '.
									'WHERE A.CLA_DESCRIP = '.(int) $aDescripID;
								$aRS = $aRepository->ADOConnection->Execute($sql);
								if ($aRS !== false)
								{
									if (!$aRS->EOF)
									{
										$intOrderID = (int)$aRS->fields["orderid"];
									}
								}
								
								$aCollection = BITAMDimensionTableFieldCollection::NewInstanceForSaving($aRepository, $aDimensionID, $aCubeID, $elementsToAssociate);
								foreach ($aCollection as $anInstanceToAssociate)
								{
									$anInstanceToAssociate->OrderID = $intOrderID;
									$anInstanceToAssociate->saveAssociation();
									$intOrderID++;
								}
							}
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
							
						case 'dissociate':
							$filter = "";
							switch (count($anArrayOfFieldNames))
							{
								case 0:
									break;
								case 1:
									$filter = " AND FieldName = ".$aRepository->ADOConnection->Quote($anArrayOfFieldNames[0]);
									break;
								default:
									foreach ($anArrayOfFieldNames as $aFieldName)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= $aRepository->ADOConnection->Quote($aFieldName); 
									}
									if ($filter != "")
									{
										$filter = " AND FieldName IN (".$filter.")";
									}
									break;
							}
							if ($filter != "")
							{
								$sql = "DELETE FROM SI_EKT_DimFieldsFormulaFilter WHERE CLA_DESCRIP = ".((int) $aDescripID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die(translate("Error accessing")." SI_EKT_DimFieldsFormulaFilter ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
								
								$sql = "DELETE FROM SI_EKT_DimensionFields WHERE CLA_DESCRIP = ".((int) $aDescripID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die(translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
								}
							}
							$aCollection = BITAMDimensionTableFieldCollection::NewInstance($aRepository, $aDimensionID, $aCubeID);
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
					}
					return null;
				}
			}
			$anInstance = null;
			switch ($action)
			{
				case 'associate':
				case 'dissociate':
					$aHTTPRequest->RedirectTo = BITAMDimensionTableFieldCollection::NewInstance($aRepository, $aDimensionID, $aCubeID);
					break;
				default:
					$anInstance = BITAMDimensionTableFieldCollection::NewInstanceForAssociation($aRepository, $aDimensionID, $aCubeID);
					break;
			}
			return $anInstance;
		}
		return null;
	}
	
	// Obtiene solo las claves de los campos actualmente asociados
	function get_AssociatedIDs()
	{
		$anArray = array();

		$sql = "SELECT A.CLA_DESCRIP, A.FieldName, A.OrderID FROM SI_EKT_DimensionFields A ".
			"WHERE A.CLA_DESCRIP = ".$this->cla_descrip." ".
			'ORDER BY A.OrderID';
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die(translate("Error accessing")." SI_EKT_DimensionFields ".translate("table").": ".$anInstance->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$anArray[$aRS->fields["fieldname"]] = strtoupper($aRS->fields["fieldname"]);
			$aRS->MoveNext();
		}
		
		return $anArray;
	}
	
	function get_Parent()
	{
		global $appSettings;

		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			if ($this->cla_descrip > 0)
			{
				return $aDimension = BITAMDimension::NewInstanceWithDescripID($this->Repository, $this->cla_descrip, $this->cla_concepto, false);
			}
			else 
			{
				return $this->Repository;
			}
		}
		else
		{
		*/
			if (($this->cla_concepto > 0 || $appSettings["UseGlobalDimensions"] == 1) && $this->consecutivo > 0)
			{
				return $aDimension = BITAMDimension::NewInstanceWithID($this->Repository, $this->consecutivo, $this->cla_concepto, false);
			}
			else 
			{
				return $this->Repository;
			}
		//}
		//@JAPR
	}

	function get_Title()
	{	
		$title = translate("Fields");
		
		return $title;
	}
	
	function get_AssociateDissociateQueryString()
	{
		return "BITAM_SECTION=DimensionTableFieldCollection&DimensionID=".$this->consecutivo.'&CubeID='.$this->cla_concepto.'&DescripID='.$this->cla_descrip;
	}
	
	function get_QueryString()
	{
		global $appSettings;
		
		//@JAPR 2011-06-21: Agregada la carga de las dimensiones pero sólo en forma global sin join con los cubos
		/*
		if ($appSettings["UseGlobalDimensions"] == 1)
		{
			return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=DimensionTableFieldCollection&DescripID=".$this->cla_descrip.'&CubeID='.$this->cla_concepto;
		}
		else
		{
		*/
			return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=DimensionTableFieldCollection&DimensionID=".$this->consecutivo.'&CubeID='.$this->cla_concepto;
		//}
		//@JAPR
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'Name';
	}
	
	function get_FormFields($aUser)
	{
		$myFields = parent::get_FormFields($aUser);
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "OrderID";
		$aField->Title = translate("Order");
		$aField->Type = "Integer";
		$aField->Size = 10;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Formula";
		$aField->Title = translate("Formula");
		$aField->Type = "LargeString";
		$aField->Size = 80;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

}


?>