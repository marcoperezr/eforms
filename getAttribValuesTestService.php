<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: X-Requested-With, X-File-Name, Content-Type, User-Agent, Referer, Origin");
header("Access-Control-Allow-Methods: GET, POST");
header("Content-Type:text/html; charset=UTF-8");

/* Archivo de prueba para obtención de datos de un catálogo vía HTTP
La función regresa un JSON con la colección de datos basada en el parámetro "dataFormat", donde los valores posibles son:
0 = El formato default regresa un array de registros donde cada elemento contiene una array de pares columna/valor, donde el índice es el nombre de la columna directamente
Ejemplo:
{
	values:[
		{"Column1":"Value1"}, {"Column2":"Value2"}, ..., {"ColumnN":"ValueN"},
		{"Column1":"Value1"}, {"Column2":"Value2"}, ..., {"ColumnN":"ValueN"},
		...
		{"Column1":"Value1"}, {"Column2":"Value2"}, ..., {"ColumnN":"ValueN"}
	]
}


1 = El formato con estructura externa regresa el mismo array quye el dataFormat=0, pero en este caso el índice de cada valor es la posición de la columna, adicionalmente agrega un elemento
	que	representa el array de nombres de columnas en el mismo orden que vienen en el array de valores
Ejemplo:
{
	values:[
		["Value1", "Value2", ..., "ValueN"],
		["Value1", "Value2", ..., "ValueN"],
		...
		["Value1", "Value2", ..., "ValueN"],
	],
	columns:[
		"Column1", "Column2", ..., "ColumnN"
	]
}
	
	Se puede recibir el parámetro "columns" que consiste en un array unidimensional con los nombres de las columnas que se desea obtener como resultado en el orden en que se desean, 
de tal manera que la respuesta vendrá sólo con los datos de dichas columnas en el array de valores independientemente del formato. Se puede recibir también como un String separado por
"," de los nombres de columnas. Si no se recibe este array, se asume que se utilizarán todas las columnas disponibles
Ejemplo:
	columns[0]=Column1
	&columns[1]=Column2
	&columns[N]=ColumnN
ó
	columns=Column1,Column2,ColumnN

	
	Se puede recibir el parámetro "filters", que consiste en un array multidimensional donde indexado por el nombre de la columna, contiene como subarray la lista de los valores que deben
ser filtrados para ella representando el equivalente a un IN, aplicando un AND para el resto de las columnas filtradas de esta manera
Ejemplo:
	filters[Column1][0]=Value1
	&filters[Column1][1]=Value2
	&filters[Column1][N]=ValueN
	&filters[Column2][0]=Value1
	&filters[Column2][1]=Value2
	&filters[Column2][N]=ValueN
ó
	?filters[Column1][0]=Value1&filters[Column1][1]=Value2&filters[Column1][N]=ValueN&filters[Column2][0]=Value1&filters[Column2][1]=Value2&filters[Column2][N]=ValueN
en PhP
	$_POST["filters"] = array(
		"Column1" => array("Value1", "Value2", ..., "ValueN"),
		"Column2" => array("Value1", "Value2", ..., "ValueN"),
		...
		"ColumnN" => array("Value1", "Value2", ..., "ValueN")
	)

	El parámetro "format" permite especificar que la respuesta debe o no ser un JSON si es que se manda format=json, en cuyo caso se agrega el header correcto para convertirla
	
El resultado de este archivo es un catálogo de Geografía estándar para un par de Países, conteniendo los atributos:
- País
- Estado
- Ciudad
- Tienda
- Latitud
- Longitud
*/

require_once("utils.inc.php");
define('datFmtResulset', 0);
define('datFmtResulsetWithCols', 1);

$intAddContentType = (strtolower(getParamValue('format', 'both', '(string)')) == "json");
if ($intAddContentType) {
	header("Content-Type: application/json");
}

$arrDefaultColumns = array("Country", "State", "City", "Store", "Latitude", "Longitude");
$arrColumnsByIdx = $arrDefaultColumns;
$arrColumnsByName = array_flip($arrDefaultColumns);
$arrResultColumns = array();

//Lee las columnas a regresar
$arrColumns = getParamValue('columns', 'both', '(array)', null);
if (is_null($arrColumns) || !is_array($arrColumns)) {
	$strColumns = getParamValue('columns', 'both', '(string)', null);
	if (is_null($strColumns) || trim($strColumns) == '') {
		$arrColumns = array();
	}
	else {
		$arrColumns = explode(',', $strColumns);
	}
}

if (count($arrColumns) > 0) {
	//En este caso se pidieron sólo ciertas columnas, así que se remueve del array de datos aquellas que no se usarán
	$arrColumnsByIdx = array();
	foreach ($arrColumns as $strColumnName) {
		if (isset($arrColumnsByName[$strColumnName])) {
			$arrColumnsByIdx[] = $strColumnName;
		}
	}
	
	if (count($arrColumnsByIdx) > 0) {
		$arrColumnsByName = array();
		foreach ($arrColumnsByIdx as $intIndex => $strColumnName) {
			$arrColumnsByName[$strColumnName] = $intIndex;
		}
		$arrResultColumns = $arrColumnsByIdx;
	}
	else {
		//No había columnas válidas, así que resetea los arrays al default
		$arrColumnsByIdx = $arrColumns;
		$arrColumnsByName = array_flip($arrColumns);
		$arrResultColumns = $arrColumns;
	}
}
else {
	$arrResultColumns = $arrDefaultColumns;
}

//Lee el formato esperado de la respuesta
$intDataFormat = getParamValue('dataFormat', 'both', '(int)');
switch ($intDataFormat) {
	case datFmtResulsetWithCols:
		break;
	default:
		$intDataFormat = datFmtResulset;
		break;
}

//Lee los filtros a aplicar
$arrFilters = getParamValue('filters', 'both', '(array)', null);
if (is_null($arrFilters) || !is_array($arrFilters)) {
	$arrFilters = array();
}

//Inicia el llenado del array de datos fijos pero basado en los filtros
$arrData = array();
if ($arrFmtValue = GetFormattedValue("México, Tamaulipas, Tampico, Liverpool, 22.260718, -97.876520", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("México, Tamaulipas, Tampico, SAM's, 22.288079, -97.874747", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("México, Veracruz, Naranjos, Elektra, 21.351578, -97.687990", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("México, Veracruz, Naranjos, Soriana Express, 21.346549, -97.687999", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("México, Yucatán, Mérida, Ferrotlapalería del norte, 20.975726, -89.580842", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("México, Yucatán, Mérida, Servicios Industriales Moal, 20.957854, -89.642774", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, Texas, Fort Worth, Ross Dress for Less, 32.755480, -97.350784", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, Texas, Fort Worth, Mattel Toy Store, 32.829075, -97.351814", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, Colorado, Denver, Louis Vuitton, 39.716326, -104.952972", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, Colorado, Denver, La Guapa Boutique, 39.769652, -105.023696", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, California, Los Angeles, Apple Store, 34.075464, -118.378777", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("USA, California, Los Angeles, Dearden's, 34.043733, -118.251372", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("Canada, Ontario, Toronto, Marshall's, 43.714378, -79.358492", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("Canada, Ontario, Toronto, Tim Hortons, 43.653887, -79.377702", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("Canada, Ontario, Kitchener, Mi Tienda Latina, 43.448303, -80.490976", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;
if ($arrFmtValue = GetFormattedValue("Canada, Ontario, Kitchener, Strasburg Convenience Store, 43.413904, -80.480546", $arrColumns, $intDataFormat, $arrFilters)) $arrData[] = $arrFmtValue;

//Formatea el array de datos y prepara la respuesta JSON
$arrResult = array();
$arrResult["values"] = $arrData;
if ($intDataFormat == datFmtResulsetWithCols) {
	$arrResult["columns"] = $arrResultColumns;
}
echo(json_encode($arrResult));

/* Regresa el valor indicado en $oValue (que contiene todos los datos para la columna) pero sólo con las columnas indicadas en $oColumns en dicha secuencia, usando el formato
definido por $iDataFormat, y aplicando los filtros indicados en $oFilters. Si el valor en cuestión no cumple con los filtros entonces regresará NULL, lo cual significa que no es un
valor válido para este caso. $oValue siempre viene con índices que representan los nombres de las columnas
*/
function GetFormattedValue($oValue, $oColumns = array(), $iDataFormat = datFmtResulset, $oFilters = array()) {
	global $arrDefaultColumns;
	
	if (!is_array($oValue) && trim($oValue) != '') {
		//Se asume que es un String separado por ", ", así que genera el array en base a ello
		$oValue = explode(", ", $oValue);
		$oValue = array_combine($arrDefaultColumns, $oValue);
	}
	
	if (is_null($oValue) || !is_array($oValue) || count($oValue) == 0) {
		return null;
	}
	
	try {
		foreach ($oFilters as $strColumnName => $arrFilteredValues) {
			if (isset($oValue[$strColumnName]) && !in_array($oValue[$strColumnName], $arrFilteredValues)) {
				return null;
			}
		}
	} catch (Exception $e) {
		//Cualquier error significaría un parámetro de filtros inválido, así que se considera válido este dato por default
	}
	
	//Si llega a este punto, quiere decir que el valor si pasó por los filtros (o no había) así que se debe agregar a la colección formateado según las columnas indicadas
	if (count($oColumns) > 0) {
		//Debe recuperar sólo las columnas solicitadas
		$arrValue = array();
		foreach ($oColumns as $strColumnName) {
			$arrValue[$strColumnName] = $oValue[$strColumnName];
		}
	}
	else {
		$arrValue = $oValue;
	}
	
	//Debe regresar en el formato adecuado
	if ($iDataFormat == datFmtResulsetWithCols) {
		$arrValue = array_values($arrValue);
	}
	
	return $arrValue;
}
?>