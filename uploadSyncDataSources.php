<?php
require_once("utils.inc.php");
require_once("dateTimeUtils.inc.php");
require_once("dataSource.inc.php");
require_once("serviceConnection.inc.php");
require_once('settingsvariable.inc.php');
require_once('datadestinations/connectionCloud.class.php');
require_once('loadExcel/class/excel.class.php');
require_once("loadExcel/class/excelupload.class.php");

/* Recorre todos los DataSources definidos y carga sus datos desde el archivo de XLS para aquellos que tienen una fuente externa para carga, siempre y
cuando se cumpla la frecuencia de carga según el servidor de KPIOnline para el momento en que se está procesando la tarea del Agente de eForms que lanzó
este proceso
//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
Ahora este proceso adicionalmente grabará la fecha de última modificación de los registros de Datasources tipo eBavel extraída directamente de la tabla de la forma
en el ambiente de producción para almacenarla en la tabla de eForms, de tal manera que se pueda consultar fácilmente durante la descarga de definiciones
Adicionalmente este proceso se encargará de obtener las versiones de imágenes de todos los usuarios del repositorio invocando a una nueva función para tal fin, (no se
utilizará la instancia de $theAppUser recibida, la cual simplemente es el usuario Maestro del repositorio, ya que para el proceso original de sincronización de 
DataSources no se requería un usuario en especial y en este contexto no existe una sesión de usuario salvo precisamente este usuario Maestro, así que será ignorado)
*/
function SyncDataSources($aRepository, $theAppUser) {
	global $appVersion;
	global $kpiUser;
	global $gbDesignMode;
	
	$return = array();
	$return['error'] = false;
	
	$strOriginalWD = getcwd();
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
        ECHOString("Processing external DataSources", 1, 1, "color:blue;");
	}
	$objDataSourceCollection = BITAMDataSourceCollection::NewInstance($aRepository);
	if (!is_object($objDataSourceCollection)) {
		$return['error'] = true;
		$return['errmsg'] = (string) @$objDataSourceCollection;
		return $return;
	}
	
	//@JAPR 2015-11-18: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
	$arrSyncErrors = array();
	$intMinutesInterval = 60;
	$objSetting = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'DATASOURCEERRREPORTINTERVAL');
	if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
		$intMinutesInterval = (int) $objSetting->SettingValue;
	}
	
	//@JAPR 2015-09-14: Agregada la configuración para la hora a partir de la que inicia la carga del DataSource
	$syncDate = date("Y-m-d H:i:s");
	$syncHour = '000000';
	$strSyncHour = '00:00:00';
	if (($intPos = strpos($syncDate, " ")) !== false) {
		$strSyncHour = substr($syncDate, $intPos +1);
		$syncHour = str_replace(":", "", $strSyncHour);
		$syncDate = substr($syncDate, 0, $intPos);
	}
	//@JAPR
	
	foreach ($objDataSourceCollection as $objDataSource) {
		//@JAPR 2015-12-11: Agregado el procesamiento de DataSources de tipo HTTP
		if ($objDataSource->Type == dstyTable) {
			//Este tipo de catálogo no requiere procesamiento de datos, así que se excluye para no generar un error
			continue;
		}
		//@JAPR
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Checking DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})");
		}
		
		//Sólo procesa los DataSources que son de tipos que soportan sincronización externa
		if ($objDataSource->eBavelAppID > 0) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) is an eBavel Catalog, skipping this DataSource", 1, 1, "color:orange;");
			}
			continue;
		}
		
		//Sólo procesa aquellos DataSources que tienen una frecuencia de sincronización y un origen externo
		//@JAPR 2015-09-14: Habilitada la frecuencia de carga de los DataSources
		if ($objDataSource->RecurPatternType == frecNone || !$objDataSource->RecurActive) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) has no frequency configured, skipping this DataSource", 1, 1, "color:orange;");
			}
			continue;
		}
		
		if ($objDataSource->ServiceConnectionID <= 0) {
			//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
			//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) has no service connection configured, skipping this DataSource";
			$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} has no service connection configured, skipping this Catalog"));
			if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
				$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
				$objDataSource->updateLastDataSyncError($strMsg);
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString($strMsg, 1, 1, "color:orange;");
			}
			continue;
		}
		
		//Obtiene la instancia de la conexión externa
		$objServiceConnection = BITAMServiceConnection::NewInstanceWithID($aRepository, $objDataSource->ServiceConnectionID);
		if (!is_object($objServiceConnection)) {
			//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
			//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) do not has an external connection configured: ".(string) $objServiceConnection;
			$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} do not has an external connection configured")).": ".(string) $objServiceConnection;
			if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
				$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
				$objDataSource->updateLastDataSyncError($strMsg);
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString($strMsg, 1, 1, "color:orange;");
			}
			continue;
		}
		
		//En este punto el DataSource es válido para sincronización, así que verifica si se cumple su frecuencia y si no se había procesado ya
		$strLastSyncDate = $objDataSource->LastSyncDate;
		//@JAPR 2015-09-14: Agregada la configuración para la hora a partir de la que inicia la carga del DataSource
		$strLastSyncHour = "000000";
		if ($strLastSyncDate != '' && ($intPos = strpos($strLastSyncDate, " ")) !== false) {
			$strLastSyncHour = substr($strLastSyncDate, $intPos +1);
			$strLastSyncHour = str_replace(":", "", $strLastSyncHour);
			//Se asume que la hora si estaba completa ya que el campo es tipo Date, pero en caso de no estar correcta se complementará con 0s a la derecha
			$intLength = strlen($strLastSyncHour);
			if ($intLength < 6) {
				$strLastSyncHour = $strLastSyncHour.str_repeat("0", 6 - $intLength);
			}
			
			$strLastSyncDate = substr($strLastSyncDate, 0, $intPos);
		}
		
		//@JAPR 2015-09-14: Agregada la configuración para la hora a partir de la que inicia la carga del DataSource
		$strUploadStartTime = trim($objDataSource->getCaptureStartTime());
		$strUploadStartTime = str_replace(":", "", $strUploadStartTime);
		$intLength = strlen($strUploadStartTime);
		if ($intLength < 6) {
			$strUploadStartTime = $strUploadStartTime.str_repeat("0", 6 - $intLength);
		}
		
		//El intervalo en minutos que se debe repetir para volver a realizar la carga de los datos de este DataSource
		$intSyncInterval = $objDataSource->Interval;
		//@JAPR
		
		//Verifica si la fecha actual es válida para ser sincronizada
		//@JAPR 2015-09-14: Habilitada la frecuencia de carga de los DataSources
		if (!$objDataSource->isValidDate($syncDate)) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) is not configured for synchronization today", 1, 1, "color:royalblue;");
			}
			continue;
		}
		
		//@JAPR 2015-09-14: Agregada la configuración para la hora a partir de la que inicia la carga del DataSource
		//Verifica si ya se cumplió la hora de inicio de las cargas en caso de estar indicada
		if ($strUploadStartTime != '') {
			if ((int) $syncHour < (int) $strUploadStartTime) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) has not reached its synchronization time", 1, 1, "color:royalblue;");
				}
				continue;
			}
		}
		
		//Si ya se había sincronizado el día actual, ya no debe ser procesado nuevamente, adicionalmente se hubiera o no sincronizado este día, verifica si hay una hora inicial de
		//sincronización o si hay un intervalo, en cuyos casos verifica si ya se cumplió o no antes de hacer la carga de datos
		$blnValid = true;
		if ($strUploadStartTime == '') {
			$strUploadStartTime = '000000';
		}
		
		//Primero verifica si no hay una fecha de última sincronización o si esta es diferente de la actual, ya que en ese caso sólo debe validar si ya llegó o no a la hora inicial de 
		//sincronización antes de continuar
		$strNextCycleMsg = '';
		if ($strLastSyncDate == '' || $strLastSyncDate != $syncDate) {
			if ((int) $syncHour < (int) $strUploadStartTime) {
				//En este caso no ha llegado a la hora mínima de sincronización, así que no hará la carga de datos todavía
				$blnValid = false;
			}
		}
		else {
			//Por definición en este caso ya no es válido, a menos que se cumpla un intervalo diferente de carga el mismo día
			$blnValid = false;
			
			//Como ya pasó por lo menos por una sincronización este día, verifica si hay configuradas mas y si ya llegó a alguna de ellas basado en la última hora de sincronización,
			//para esto busca la siguiente hora de intervalo mayor que la última hora de sincronización, y verifica si la hora actual ya es mayor a dicha hora, si no lo es significaría
			//que la hora actual aún está entre la última hora de sincronización y la siguiente configurada así que omitirá el proceso en este punto
			if ($intSyncInterval > 0) {
				$strStartTime = substr($strUploadStartTime, 0, 2).":".substr($strUploadStartTime, 2, 2).":".substr($strUploadStartTime, 4, 2);
				$arrTimes = GetDateTimeIntervalArrBetweenDates("{$syncDate} {$strStartTime}", "{$syncDate} 23:59:59", $intSyncInterval, 'N', null, 'His');
				if (count($arrTimes) > 0) {
					foreach ($arrTimes as $strTime) {
						if ((int) $strTime > (int) $strLastSyncHour) {
							//Esta es la primer hora del intervalo que es posterior a la última carga, simplemente verifica si la hora actual también es mayor y si es así entonces si
							//es un momento válido para procesar la carga
							//En este punto, dependiendo de la saturación de actividad del agente, podrían haberse saltado varias horas de intervalor de carga, lo que pasará es que va
							//a identificar la mas antigua e invariablemente cargará los datos, pero al grabar la última sincronización habría omitido todas las horas intermedias, no se
							//realizarán el total de cargas configuradas pero habrá mantenido los datos al día según sus posibilidades hasta el último momento configurado
							if ((int) $syncHour > (int) $strTime) {
								$blnValid = true;
							}
							else {
								//Indica la siguiente hora de sincronización configurada
								$strNextCycleMsg = "Next upload hour will be at ".substr($strTime, 0, 2).":".substr($strTime, 2, 2).":".substr($strTime, 4, 2);
							}
							break;
						}
					}
				}
			}
		}
		
		//Si no es válida significaría que ya se había sincronizado y no hay intervalo u hora inicial que validar o bien que esta ya se cumplió y no ha llegado el nuevo
		//ciclo a cargar, o bien que no se ha sincronizado aún y no ha llegado a la hora inicial de sincronización
		if (!$blnValid) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) has been synchronized today. {$strNextCycleMsg}", 1, 1, "color:royalblue;");
			}
			continue;
		}
		//@JAPR
		
		//Si llega a este punto significa que se debe procesar la carga del DataSource con el archivo XLS obtenido desde la fuente externa
		//Primero descarga el archivo desde la fuente externa configurada
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) is valid for this process", 1, 1, "color:blue;");
		}
		$nameCloud = '';
		$upload_dir = '';
		//@JAPR 2015-09-08: Corregido un bug, el Type del DataSource No corresponde internamente a los tipos de fuentes externas, así que se debe hacer una conversión
		//(NO se podría aplicar la misma constante que para DataDestinations)
		//EVEG Si esl datasource es googleDrive se manda como parametro de path del archivo el id de la carpeta y no la ruta relativa
		$strPathFile = '';
		switch ($objDataSource->Type) {
			case dstyGoogleDrive:
				$nameCloud = 'googleDrive';
				$upload_dir = "loadExcel/uploads/Datasources/googledrive/";
				$strPathFile=$objDataSource->IdGoogleDrive;
				break;
			case dstyDropBox:
				$nameCloud="dropBox";
				$upload_dir = "loadExcel/uploads/Datasources/dropbox/";
				$strPathFile=$objDataSource->Path;
				break;
			case dstyFTP:
				$nameCloud="FTP";
				$upload_dir = 'loadExcel/uploads/Datasources/FTP/';
				$strPathFile=$objDataSource->Path;
				break;
			//@JAPR 2015-12-11: Agregado el procesamiento de DataSources de tipo HTTP
			case dstyHTTP:
				$nameCloud="HTTP";
				$upload_dir = 'loadExcel/uploads/Datasources/HTTP/';
				//$strPathFile=$objDataSource->Path;
				break;
			//@JAPR
			default:
				$strPathFile=$objDataSource->Path;
				break;
		}
		$accountCloud = $objServiceConnection->ServiceID;
		
		if ($nameCloud == '') {
			//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
			//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) has a wrong source type: {$objDataSource->Type}";
			$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} has a wrong source type")).": {$objDataSource->Type}";
			if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
				$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
				$objDataSource->updateLastDataSyncError($strMsg);
			}
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString($strMsg, 1, 1, "color:red;");
			}
			continue;
		}
		
		if ($objDataSource->Type == dstyHTTP) {
			//El caso de HTTP simplemente invoca a un servicio que realizará el Request y cargará la información en la tabla del DataSource
			//@JAPR 2015-12-22: Agregado el reporte de errores de sincronización de DataSources tipo HTTP (#P8JOZV)
			$arrResult = $objDataSource->updateAttributesHTML();
			if (!is_array($arrResult)) {
				$strMsg = str_ireplace("{var1}", "{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})", translate("Catalog {var1} had a connection error with the service"));
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			elseif (!isset($arrResult['success']) || !$arrResult['success']) {
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} had a connection error with the service")).": ".(string) @$arrResult['msg'];
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
		}
		else {
			//El resto de los DataSources que entran por este else, son casos que pueden ser cargados vía MS Excel ya que descargan un archivo con los datos
			if (trim($objDataSource->Path) == '') {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) must have a path to the external source file";
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} must have a path to the external source file"));
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			
			if (trim($objDataSource->Document) == '') {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) must have a filename to the external source file";
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} must have a filename to the external source file"));
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			
			//Se obtiene el nombre de archivo de una configuración diferente, ya que no en todas las fuentes aplica en Path
			$strFilename = GetFileNameFromFilePath($objDataSource->Document);
			
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) downloading external file: {$objDataSource->Path} => {$strFilename}");
			}
			$arrResult = connectionCloud::downloadFile($nameCloud, $accountCloud, $aRepository, $strPathFile, $strFilename);
			if (!is_array($arrResult)) {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) had an error while downloading the external file";
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} had an error while downloading the external file"));
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			$strFilename = trim((string) @$arrResult['file']);
			$strFilename = $upload_dir.$strFilename;
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) external file downloaded: {$strFilename}");
			}
			
			if (!isset($arrResult['success']) || !$arrResult['success'] || $strFilename == '') {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) had an error while downloading the external file: ".$strFilename;
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} had an error while downloading the external file")).": ".$strFilename;
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			
			if (!file_exists($strFilename)) {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) was unable to find the specified file: ".$strFilename;
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} was unable to find the specified file")).": ".$strFilename;
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			
			//En este punto ya se tiene descargado el archivo XLS externo, así que continua con el proceso de carga
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) uploading XLS file {$strFilename}");
			}
			$anExcelUpload = new ExcelUpload( $strFilename, $objDataSource->DataSourceID );
			if (is_null($anExcelUpload)) {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) was unable to load the XLS class";
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} was unable to load the XLS class"));
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			
			$arrFields = array();
			$objDataSourceMemberColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $objDataSource->DataSourceID);
			if (is_null($objDataSourceMemberColl) || !is_object($objDataSourceMemberColl)) {
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) unable to load DataSourceMembers: ".(string) $objDataSourceMemberColl;
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} was unable to load the attributes")).": ".(string) $objDataSourceMemberColl;
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
			foreach ($objDataSourceMemberColl as $objDataSourceMember) {
				$arrFields[] = array('columntable' => $objDataSourceMember->FieldName, 'columnexcel' => $objDataSourceMember->MemberName);
			}
			$selectedSheet = 'Sheet1$';
			if (trim($objDataSource->SheetName) != '') {
				$selectedSheet = $objDataSource->SheetName;
			}
			$arrResult = $anExcelUpload->uploadCatalog($arrFields, $selectedSheet, $objDataSource->DataSourceName, $aRepository->DataADOConnection, $theAppUser, $aRepository);
			if (!$arrResult) {
				$strErrDesc = (string) $anExcelUpload->lastErrorMsg;
				//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
				//$strMsg = "DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) had an error while uploading the external file: {$strErrDesc}";
				$strMsg = str_ireplace("{var1}", translate("{$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})"), translate("Catalog {var1} had an error while uploading the external file")).": {$strErrDesc}";
				if (CanReportDataSourceError($objDataSource, $strMsg, null, $intMinutesInterval)) {
					$arrSyncErrors[$objDataSource->DataSourceID] = $strMsg;
					$objDataSource->updateLastDataSyncError($strMsg);
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString($strMsg, 1, 1, "color:red;");
				}
				continue;
			}
		}
		//@JAPR
		
		$sql = "UPDATE SI_SV_DataSource SET 
				LastSyncDate = ".$aRepository->DataADOConnection->DBTimeStamp("{$syncDate} {$strSyncHour}")." 
			WHERE DataSourceID = {$objDataSource->DataSourceID}";
		$aRepository->DataADOConnection->Execute($sql);
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) was processed successfully", 1, 1, "color:darkgreen;");
		}
		
		//@JAPR 2015-11-20: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
		//Al terminar de procesar correctamente el DataSource, se limpia el último mensaje de error que hubiera asignado
		$objDataSource->updateLastDataSyncError("");
		//@JAPR
	}
	
	//@JAPR 2018-05-07: Optimizada la carga de eForms (#G87C4W)
	//Agregado el proceso de actualización de la fecha de última modificación de los datos de los DataSource tipo eBavel o aquellos donde los datos son modificados
	//fuera de los procesos de eForms
	//A partir de este punto, la instancia de $theAppUser se reemplazará por la iteración de cada usuario que pueda tener una forma y/o catálogo asignada según el
	//caso, ya que específicamente para los catálogos de eBavel la seguridad es muy importante y se requerirá actualizar la variable de sesión o bien indicar el
	//usuario del contexto durante el setRepository (que precisamente se logra con $theAppUser) para que se aplique la propia seguridad de eBavel, por tanto la tabla
	//que contendrá este tipo de información cachada será indexada por usuario ya que de lo contrario se tendría que actualizar a la misma frecuencia múltiples usuarios
	//siendo que no necesariamente comparten los mismos datos en los catálogos
	//@JAPRDescontinuado, esta idea no funcionó, se optará por descargar una base de datos al dispositivo para permitir consultar en línea offline con filtros
	/*foreach ($objDataSourceCollection as $objDataSource) {
		//@JAPR 2015-12-11: Agregado el procesamiento de DataSources de tipo HTTP
		if ($objDataSource->Type != dstyeBavel) {
			//Este tipo de catálogo no requiere grabar la fecha de última modificación de datos
			continue;
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Checking Data Last Modified Date for DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID})");
		}
		
		//Para datasources de eBavel, deberá obtener el MAX de la fecha de última modificación que es un campo de sistema
		$strLastModifiedDate = $objDataSource->getCatalogValuesLastModifiedDate();
		if ( !is_null($strLastModifiedDate) && $strLastModifiedDate !== false ) {
			//En este punto ya se tiene descargado el archivo XLS externo, así que continua con el proceso de carga
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) updating data last modified date {$strLastModifiedDate}", 1, 1, "color:blue;");
			}
			
			//$objDataSourceUpdated = $objDataSource->save(false);
			//if ( is_object($objDataSourceUpdated) ) {
			BITAMDataSource::UpdateLastDataDateInDataSource($aRepository, $objDataSource->DataSourceID, $strLastModifiedDate);
			//}
		}
		
		/ *if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("DataSource {$objDataSource->DataSourceName} ({$objDataSource->DataSourceID}) was processed successfully", 1, 1, "color:darkgreen;");
		}* /
	}*/
	
	//@JAPR 2018-05-15: Optimizada la carga de eForms (#G87C4W)
	//Invoca al proceso que obtendrá las versiones de definiciones de todas las imágenes de atributos tipo imagen para todos los usuarios del repositorio en
	//todas las formas que sean utilizadas por dicho usuario
	UpdateDataSourcesImageVersions($aRepository);
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		//@JAPR 2015-11-19: Corregido el mensaje de proceso terminado, no coincidía con el que esperaba el Agente así que se reportaba un error en el proceso cuando no lo había
		ECHOString("DataSources were processed successfully.", 1, 1, "color:blue;");
	}
	
	if (!$return['error']) {
		$return['errmsg'] = "DataSources were processed successfully.";
	}
	
	//@JAPR 2015-11-19: Agregado el reporte de errores de sincronización de DataSources (KNLFBK)
	//En caso de haber existido al menos un error, debe notificar vía EMail para que sean atendidos
	if (count($arrSyncErrors) > 0) {
		reportDataSourceError($aRepository, $arrSyncErrors, $theAppUser);
	}
	//@JAPR
	
	return $return;
}

/* Esta función consultará a todos los usuarios que se encuentren asociados a alguna forma ya sea de manera directa o mediante algún grupo de usuario, para consultar
los registros de los DataSources en busca de imágenes que se deban actualizar en la tabla de versiones de las imágenes, para optimizar el proceso de revisión de
imágenes nuevas durante la obtención de versiones de definiciones. El proceso barrerá todos los registros con la función getEFormsDefinitionsImageVersions original
y posteriormente actualizará la tabla de versiones de imágenes, eliminando aquellas que ya no se encuentren utilizadas para este usuario específico (ya sea por 
seguridad o porque la forma correspondiente ya no es visible)
*/
function UpdateDataSourcesImageVersions($aRepository) {
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing DataSource image versions", 1, 1, "color:blue;");
	}
	
	//Primero debe obtner la lista de usuarios del repositorio que tienen acceso por lo menos a una forma con secciones
	$arrUsersIDs = array();
	$activeStatus = array(0 => 0, 1 => 1);
	//$strPostV6ObjectFilter = "";
	//if ($appVersion >= esveFormsv6) {
	$strPostV6ObjectFilter = " AND A.CreationVersion >= ".esveFormsv6." ";
	//}
	
	//Todos los usuarios que por lo menos tengan una forma con secciones asignada directamente
	$sql = "SELECT DISTINCT C.UserID AS UserID 
		FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C 
		WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID 
			AND A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter;
	//Todos los usuarios que por lo menos tengan una forma con secciones asignada mediante sus roles
	$sql .= " UNION 
		SELECT DISTINCT D.CLA_USUARIO AS UserID 
		FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C, SI_ROL_USUARIO D 
		WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID AND D.CLA_ROL = C.RolID 
			AND A.Status IN (" . implode(",", $activeStatus) . ")".$strPostV6ObjectFilter;
	//Todos los usuarios que por lo menos tengan una forma con secciones asignada directamente por medio de agenda (forma de checkin)
	$sql .= " UNION 
		SELECT DISTINCT B.UserId AS UserID 
		FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgenda B 
		WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.CheckinSurvey 
		AND A.Status IN (" . implode(",", $activeStatus) . ") AND B.AgendaID NOT IN ( 
			SELECT DISTINCT AgendaID 
			FROM SI_SV_SurveyAgendaDet 
			WHERE STATUS = 2)".$strPostV6ObjectFilter." 
		AND B.CaptureStartDate = DATE(NOW())";
	//Todos los usuarios que por lo menos tengan una forma con secciones asignada directamente por medio de agenda (formas adicionales o de v6)
	$sql .= " UNION 
		SELECT DISTINCT C.UserId AS UserID 
		FROM SI_SV_Survey A, SI_SV_Section A1, SI_SV_SurveyAgendaDet B, SI_SV_SurveyAgenda C 
		WHERE A.SurveyID = A1.SurveyID AND A.SurveyID = B.SurveyID AND B.AgendaID = C.AgendaID 
			AND A.Status IN (" . implode(",", $activeStatus) . ") AND C.AgendaID NOT IN ( 
				SELECT DISTINCT AgendaID 
				FROM SI_SV_SurveyAgendaDet 
				WHERE Status = 2) {$strPostV6ObjectFilter} 
			AND C.CaptureStartDate = DATE(NOW()) 
	ORDER BY 1";
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("(UpdateDataSourcesImageVersions) Query para Usuarios con formas asignadas: {$sql}");
	}
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		while (!$aRS->EOF) {
			$intUserID = (int) @$aRS->fields['userid'];
			if ($intUserID > 0) {
				$arrUsersIDs[$intUserID] = $intUserID;
			}
			
			$aRS->MoveNext();
		}
	}
	
	//Recorre todos los usuarios obteniendo la lista de imágenes de los catálogos que tiene asignados mediante sus formas, para esto evidentemente tendrá que cargar
	//la información de las formas y catálogos antes de obtener la lista de imágenes. Cada usuario puede aplicar seguridad diferente no solo en eForms, sino en eBavel,
	//por lo tanto no hay una manera eficiente de agrupar a los usuarios en este proceso excepto identificar los filtros específicos combinados entre productos de
	//cada catálogo y mantener un array en memoria con el String para hacer match antes de consultar el siguiente catálogo para otro usuario, pero dicho proceso
	//tendría que recaer en la función que identifica las imágenes y no en este proceso que solo la invoca, así que aquí simplemente se procesará el resultado
	foreach ($arrUsersIDs as $intUserID) {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("(UpdateDataSourcesImageVersions) Processing images for User: {$intUserID}");
		}
		$arrJSON = @getEFormsDefinitionsVersions($aRepository, null, $intUserID, array('surveys' => 1, 'catalogs' => 1, 'images' => 1, 'rawimages' => 1));
		if ( !is_null($arrJSON) && is_array($arrJSON) && isset($arrJSON['imageList']) && is_array($arrJSON['imageList']) ) {
			$arrImagesList = $arrJSON['imageList'];
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("(UpdateDataSourcesImageVersions) Images identified for this user: ".count($arrImagesList));
			}
			
			//Primero tiene que marcar todas las imágenes como no procesadas para saber cuales tendrá que eliminar al terminar el proces. A la fecha de implementación,
			//NO se podían tener 2 imágenes con el mismo nombre para el mismo repositorio que tuvieran contenido diferente, por ello la imágen funciona como llave
			//del array y lo hará dentro de la tabla de procesamiento
			$sql = "UPDATE SI_SV_SurveyImgVersions SET 
					Processed = 0
				WHERE UserID = {$intUserID}";
			$aRepository->DataADOConnection->Execute($sql);
			
			//Recorre todas las imágenes identificadas y las inserta / actualiza con la última fecha de modificación detectada
			foreach($arrImagesList as $strFileName => $arrImageData) {
				$sql = "UPDATE SI_SV_SurveyImgVersions SET 
						Processed = 1
						, DataSourceID = ".(int) @$arrImageData['did']." 
						, Version = ".(string) @$arrImageData['version']." 
						, Source = ".$aRepository->DataADOConnection->Quote((string) @$arrImageData['src'])." 
						, LastDateID = ".$aRepository->DataADOConnection->DBTimeStamp((string) @$arrImageData['lastDate'])."
					WHERE UserID = {$intUserID} AND FileName = ".$aRepository->DataADOConnection->Quote($strFileName);
				$aRepository->DataADOConnection->Execute($sql);
				$intAffectedRows = $aRepository->DataADOConnection->_affectedrows();
				if ( !$intAffectedRows ) {
					//En caso de no haber actualizado ningún registro, se asume que no existía esta imagen por lo que deberá insertarla, es mas probable que la imagen
					//si exista a que esté cambiando constantemente el diseño de la forma/filtros y que no exista entre un procesamiento del agente y el siguiente
					$sql = "INSERT INTO SI_SV_SurveyImgVersions (UserID, FileName, Source, Version, DataSourceID, Processed) 
							VALUES ({$intUserID}, ".$aRepository->DataADOConnection->Quote($strFileName).
						", ".$aRepository->DataADOConnection->Quote((string) @$arrImageData['src']).
						", ".(string) @$arrImageData['version'].", ".(int) @$arrImageData['did'].", 1)";
					$aRepository->DataADOConnection->Execute($sql);
				}
			}
			
			//Al terminar de procesar las imágenes deberá eliminar aquellas que no fueron actualizadas en este momento, ya que eso significaría que no son utilizadas
			//por ninguna de las formas de este usuario actualmente
			$sql = "DELETE FROM SI_SV_SurveyImgVersions 
				WHERE UserID = {$intUserID} AND Processed = 0";
			$aRepository->DataADOConnection->Execute($sql);
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("(UpdateDataSourcesImageVersions) Unable to get Images for this User");
			}
		}
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("DataSource image versions were processed successfully", 1, 1, "color:blue;");
	}
}

/* Dado un DataSource y la hora actual así como un intervalo en minutos, verifica si ha pasado suficiente tiempo para considerar que se debe volver a reportar el error, comparando
si el mensaje de error previo era el mismo que el último reportado en el DataSource, ya que en caso de ser diferente automáticamente considerará válido el reporte
Si no se especifica el intervalo o este es <= 0, se asume que si debe reportar automáticamente el error
*/
function CanReportDataSourceError($oDataSource, $sNewSyncError, $aCurrTime = null, $aMinutosInterval = 0) {
	if (is_null($oDataSource)) {
		return false;
	}
	
	if (is_null($aCurrTime)) {
		$aCurrTime = date('Y-m-d H:i:s');
	}
	
	//Si no se pueden convertir ambos valores de fecha a objeto fecha, es inválido el proceso y regresa false para no reportar error (la fecha del DataSource puede venir null y considerarse
	//válido, ya que significaría que nunca se había reportado un error para ese DataSource)
	try {
		if (is_null($oDataSource->LastSyncErrorDate)) {
			return true;
		}
		
		$dteDate1 = $oDataSource->LastSyncErrorDate;
		$dteDate2 = $aCurrTime;
	}
	catch (Exception $e) {
		return false;
	}
	
	if (is_null($aMinutosInterval) || !is_numeric($aMinutosInterval)) {
		$aMinutosInterval = (int) @$aMinutosInterval;
	}
	
	$strLastDataSourceSyncError = strtolower(trim((string) @$oDataSource->LastSyncError));
	$sNewSyncError = strtolower(trim((string) $sNewSyncError));
	if ($aMinutosInterval <= 0) {
		//Si no se especificaron minutos de intervalo, se considera que nunca se desea repetir el reporte de error si ya está repetido respecto al último ocurrido
		if ($strLastDataSourceSyncError == $sNewSyncError) {
			return false;
		}
	}
	else {
		//En este caso compara el tiempo transcurrido desde el último reporte, si es mayor que el intervalo configurado, si se puede repetir la notificación de error, o bien si el error
		//no es el mismo que el último ocurrido
		if ($strLastDataSourceSyncError == $sNewSyncError) {
			$intMinutes = (int) @DateDiff($dteDate1, $dteDate2, 'i');
			if ($intMinutes < $aMinutosInterval) {
				return false;
			}
		}
	}
	
	//En este caso si se debe reportar el error
	return true;
}
?>