<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
@session_start();
//@JAPR 2015-02-05: Agregado soporte para php 5.6
if (isset($_SESSION["PABITAM_RepositoryName"]) && isset($_SESSION["PABITAM_UserName"]))
{
	
    $thisDate = date("Y-m-d H:i:s");
	require_once("repository.inc.php");
	require_once('config.php');
	require_once('cubeClasses.php');
	
	global $BITAMRepositories;
	global $appVersion;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	global $useSourceFields;
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	global $useRecVersionField;
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	global $useDynamicPageField;
	//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
	global $dieOnError;
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
	global $eBavelAppCodeName;
	global $arrayPDF;
	global $arrActionItems;
	$arrActionItems = array();
	global $arrNumericQAnswersColl;
	$arrNumericQAnswersColl = array();
	//@JAPR 2013-02-07: Agregada la opción para enviara las acciones a ciertos usuarios especiales
	global $blnEBavelSupLoaded;
	$blnEBavelSupLoaded = false;
	global $arrEBavelSupervisors;
	$arrEBavelSupervisors = array();
	//@JAPR 2013-05-06: Corregido un bug, la funcionalidad de Supervisores adicionales no funcionaba con Apps antes de esveBavelMapping, así que se
	//validará para que en esos casos se force a enviar IDs positivos que permitan usarla, y solo en las mas recientes envíe IDs negativos como debe ser
	global $blnUseMaxLongDummies;
	$blnUseMaxLongDummies = false;
	//@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
	global $arrEditedFactKeys;
	global $objReportData;
	//@JAPR 2013-01-31: Agregada la opción para desasociar las dimensiones de preguntas de los catálogos
	global $arrDynamicCatalogAttributes;
	global $gbEnableIncrementalAggregations;
	//@JAPR 2014-05-29: Agregado el tipo de sección Inline
	global $arrInlineCatalogAttributes;
	
	$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
	if (!array_key_exists($theRepositoryName, $BITAMRepositories))
	{
		header("Location: index.php?error=1");
		exit();
	}
	
	$theRepository = $BITAMRepositories[$theRepositoryName];
	if (!$theRepository->open())
	{
		header("Location: index.php?error=2");
		exit();
	}	
	
	$theUserName = $_SESSION["PABITAM_UserName"];
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
	
	if (is_null($theUser))
	{
		header("Location: index.php?error=3");
		exit();
	}
	
	//Cargar diccionario del lenguage a utilizar
	if (array_key_exists("PAuserLanguage", $_SESSION))
	{
		InitializeLocale($_SESSION["PAuserLanguageID"]);
		LoadLanguageWithName($_SESSION["PAuserLanguage"]);
	}
	else 
	{	//English
		InitializeLocale(2);
		LoadLanguageWithName("EN");
	}
	
	//Agregar código para generar un archivo con los parámetros recibidos para grabar la encuesta
	//se tomó de eSurveyService.php
	//$blnDebugSave = getParamValue('debugSave', 'both', "(boolean)");
	if (false)
	{
		ob_start();
		//echo("Post Data: <br>");
		echo('$POST = ');
		var_export($_POST);
		echo(";\r\n");
		//echo("Get Data: <br>");
		echo('$GET =');
		var_export($_GET);
		echo(";\r\n");
	    $strData = trim(ob_get_contents());
		ob_end_clean();
  		$statlogfile = "./uploaderror/ESurvey_SyncData_".date('YmdHis').".log";
	  	@error_log($strData, 3, $statlogfile);
	    header("Content-Type: text/plain");
	    echo("Data file was stored in the server");
	    exit();
	}
	elseif (false)
	{
		//Activar sólo si se quiere dejar registro de los datos subidos por los encuestadores, se puede incluso condicionar a sólo ciertas cuentas
		//Es similar al parámetro debugSave con la excepción de que no manda la salida de datos al buffer sino que lo maneja todo como variables
		//para simplemente grabarlas en un archivo y atrapa todos los errores para no interrumpir el proceso de grabado
		try
		{
			$kpiUser = '';
		    if (isset($_POST['UserID']))
		    {
			    $kpiUser = $_POST['UserID'];
		    }

			$thisDate = date("Y-m-d H:i:s");
			if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
			{
				$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
			}
			$thisDate = str_replace(':', '', $thisDate);
			$thisDate = str_replace('-', '', $thisDate);
			$thisDate = str_replace(' ', '', $thisDate);

			$agendaID = 0;
			if (array_key_exists("agendaID", $_POST))
			{
				$agendaID = (int) $_POST["agendaID"];
			}
			
		    $surveyID = 0;
		    if (array_key_exists("surveyID", $_POST)) {
		        $surveyID = (int) $_POST["surveyID"];
		    }
		    
		    if (trim($kpiUser) != '' && $thisDate != '' && $surveyID > 0)
		    {
		    	$strData = "";
				$strData .= '$POST = '.var_export($_POST, true).";\r\n";
				$strData .= '$GET ='.var_export($_GET, true).";\r\n";
			    
		  		$statlogfile = "./uploaderror/SyncData_".$theUser->UserName."_Ag".$agendaID."_Sv".$surveyID."_Dte".$thisDate.".log";
			  	@error_log($strData, 3, $statlogfile);
		    }
		}
		catch (Exception $e)
		{
			//No es necesario reportar este error
		}
	}
	/*Fin de código para generar archivo*/
	
	if(array_key_exists("action", $_GET) && $_GET["action"]=="testsavefromfile")
	{
		testSaveMethodParams($theRepository);
	}

	$surveyID = 0;
	if(array_key_exists("surveyID", $_POST))
	{
		$surveyID = $_POST["surveyID"];
	}
	
	//@JAPR 2012-03-14: Agregado el grabado del Status de las Agendas
	$agendaID = 0;
	if (array_key_exists("agendaID", $_POST))
	{
		$agendaID = (int) $_POST["agendaID"];
	}
	//@JAPR
	$statlogfile = "./uploaderror/eformsWeb.log";
	$friendlyDate = date('Y-m-d H:i:s');
	
	$thisDate = date("Y-m-d H:i:s");
	if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"])) {
		$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
	}
	
	if(isset($_POST["surveyDate"]) && isset($_POST["surveyHour"]))
	{
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
	} else {
		$lastDate = $thisDate;
	}
	
	$strDateID = getParamValue('DateID', 'both', "(string)");
	$strHourID = getParamValue('HourID', 'both', "(string)");
	$intApplicatorID = getParamValue('ApplicatorID', 'both', "(int)");
	if($strDateID != '' && $strHourID != '' && $intApplicatorID > 0) {
		$lastDate = $strDateID." ".$strHourID;
	}
	
	$stat_action = "[".$friendlyDate."] [Action: Connect Saving Answers] [UserID - Email: ".$theUser->UserName."] [SurveyID: ".@$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
	@error_log($stat_action, 3, $statlogfile);
	
	require_once("survey.inc.php");
	require_once("question.inc.php");
	require_once("processsurveyfunctions.php");
	
	//Se genera instancia de encuesta dado un SurveyID
	$surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);

    //@JAPR 2013-03-07: Validado que si no existen las columnas de las fecha de sincronización se creen automáticamente
	$sql = "SELECT SyncDate FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD SyncDate VARCHAR(64) NULL";
		$theRepository->DataADOConnection->Execute($sql);
	}
	
	$sql = "SELECT SyncTime FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD SyncTime VARCHAR(64) NULL";
		$theRepository->DataADOConnection->Execute($sql);
	}
	
	//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
	//Verifica si existen los campos para almacenar el originen del registro de la tabla de datos
	$useSourceFields = false;
	$sql = "SELECT EntrySectionID, EntryCatDimID FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useSourceFields = true;
	}
	//@JAPR 2013-01-10: Agregado el campo con el número de versión de eForms que grabó el registro de captura
	$useRecVersionField = false;
	$sql = "SELECT eFormsVersionNum FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useRecVersionField = true;
	}
	else {
		//Este campo si se debe agregar automáticamente para permitir mantener el histórico de eForms e implementar cambios futuros
		$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD eFormsVersionNum VARCHAR(10) NULL";
		if ($theRepository->DataADOConnection->Execute($sql) !== false) {
			$useRecVersionField = true;
		}
	}
	//@JAPR 2013-01-10: Agregado el campo para identificación del registro de página de una sección dinámica
	$useDynamicPageField = false;
	$sql = "SELECT DynamicPageDSC FROM ".$surveyInstance->SurveyTable." WHERE 1 = 2";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS) {
		$useDynamicPageField = true;
	}
	else {
		//Este campo si se debe agregar automáticamente para permitir mantener el histórico de eForms e implementar cambios futuros
		$sql = "ALTER TABLE {$surveyInstance->SurveyTable} ADD DynamicPageDSC VARCHAR(255) NULL";
		if ($theRepository->DataADOConnection->Execute($sql) !== false) {
			$useDynamicPageField = true;
		}
	}
	//@JAPR
	
	//Verificamos si existe una seccion dinamica
	//Si la funcion regresa cero quiere decir que no existe seccion dinamica
	//En caso contrario si regresa algo diferente de cero entonces es el ID de la seccion dinamica
	$dynamicSectionID = BITAMSection::existDynamicSection($theRepository, $surveyID);
	$hasDynamicSection = false;
	
	$hasCategoryDimension = false;
	$blnEdit = false;
	$arrayData = $_POST;

    foreach ($arrayData as $key => $value) {///////AQUI
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
            	//@JAPR 2013-08-15: Agregado un nivel mas de conversión de arrays, si se requiere otro ya mejor cambiar a llamadas recursivas
            	if (is_array($value1)) {
            		foreach ($value1 as $key2 => $value2) {
						//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            			$arrayData[$key][$key1][$key2] = $arrayData[$key][$key1][$key2];
            		}
            	}
            	else {
					//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
	                $arrayData[$key][$key1] = $arrayData[$key][$key1];
            	}
            	//@JAPR
            }
        } else {
			//@JAPR 2015-03-20: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
            $arrayData[$key] = $arrayData[$key];
        }
    }/////AQUI
	
	$strResult = '';
	//$strDateID = getParamValue('DateID', 'both', "(string)");
	//$strHourID = getParamValue('HourID', 'both', "(string)");
	//$intApplicatorID = getParamValue('ApplicatorID', 'both', "(int)");
	//if(array_key_exists("DateID", $_GET) && array_key_exists("HourID", $_GET) && array_key_exists("ApplicatorID", $_GET))
	if($strDateID != '' && $strHourID != '' && $intApplicatorID > 0)
	{
		//Se trata de un proceso de edicion
		$dateID = $strDateID;	//$_GET["DateID"];
		$hourID = $strHourID;	//$_GET["HourID"];
		$userID = $intApplicatorID;	//$_GET["ApplicatorID"];
		$lastDate = $dateID." ".$hourID;
		$blnEdit = true;
		if($dynamicSectionID>0)
		{
			//Aqui verificamos con el sectionValue y registerID que realmente si se crearon arreglos
			//de preguntas en las encuestas para poder capturar respuestas para mas de una encuesta
			$postKey1 = "sectionValue";
			//$postKey2 = "registerID";
			if(isset($arrayData[$postKey1]))	// && isset($arrayData[$postKey2])
			{
				$hasDynamicSection = true;
			}
		}
		
		/*
		//Verificamos si la encuesta trae valores de CategoryDimension
		$postKey = "qCatDimVal";
		if(isset($arrayData[$postKey]))
		{
			$hasCategoryDimension = true;
		}
		*/
		$hasCategoryDimensions = hasCategoryDimensions($theRepository, $surveyID);
        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
        $hasMasterDetSections = false;
        $arrMasterDetSections = BITAMSection::existMasterDetSections($theRepository, $surveyID, true);
        if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
        {
			//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
        	$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
        	
        	//Verifica si realmente se capturaron preguntas de la sección de Maestro detalle, ya que si no es así entonces simplemente
        	//se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
        	//por lo que no habría problema en procesarlas
        	for($i = 0; $i < count($arrMasterDetSections); $i++)
        	{
        		$intSectionID = $arrMasterDetSections[$i];
        		$postKey = "masterSectionRecs".$intSectionID;
        		if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0))
        		{
        			//Con una sola sección que si capturara un Maestro - Detalle, se asume que si vienen preguntas como array por lo que
        			//se usará el método alterno de grabado
        			$hasMasterDetSections = true;
        			//break;
        		}
        		else 
        		{
        			//Si esta sección siendo un Maestro - Detalle NO capturó ningún conjunto de respuestas, en el mejor de los casos
        			//llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
        			//pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
        			//(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
		            foreach ($questionCollection->Collection as $questionElement) {
		            	$postKey = "qfield".$questionElement->QuestionID;
		            	if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]))
		            	{
		            		//En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
		            		//"masterSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
		            		//no se conocía el concepto de Maestro - Detalle, en cuyo caso no debió haber llegado entonces un Array sino
		            		//las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
		            		//con el is_array también
		            		$arrayData[$postKey] = '';
		            	}
		            }
        		}
        	}
        }
        else {
        	$arrMasterDetSections = array();
        	$arrMasterDetSectionsIDs = array();
        }
        //@JAPR
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		$blnSaveSingleStandardData = false;
		if ($surveyInstance->UseStdSectionSingleRec) {
			$blnSaveSingleStandardData = true;
		}
        
		//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
		//Esta variable contendrá el Key asignado a esta captura de encuesta en los casos donde se deban invocar varios de los métodos
		//de grabado encadenados, por ejemplo encuestas que tienen Secciones Maestro - Detalle y Dinámica, ya que al invocar primero al
		//grabado de la Maestro - Detalle se debe respetar el $factKeyDimVal asignado ahí cuando se invoque al grabado de la Dinámica
		//Como se trata de una edición, este valor ya viene asignado en los datos recibidos, así que sólo se reutiliza
		$factKeyDimVal = $arrayData['entryID'];
        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
        //Si hay por lo menos una sección maestro detalle, por ahora no se permiten secciones dinámicas ni preguntas de categoría de
        //dimensiones, ya que el procesamiento de este tipo de secciones es muy parecido al de las secciones dinámicas pero son métodos
        //diferentes por lo que no se pueden combinar aun (eventualmente se podrían combinar con relativa facilidad secciones dinámicas
        //y Maestros detalles reenviando a multipleSaveDynamicData dentro de multipleSaveMasterDetData, pero por ahora no se soporta.
        //El grabado de preguntas de categorías de dimensión se complica aun en el uso de estos tipos de secciones especiales
        //Si no hay seccion dinamica se sigue procesando los datos del POST como siempre se han hecho
        if ($hasMasterDetSections)
        {
			//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
        	$strResult = multipleUpdateMasterDetData($theRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $arrMasterDetSections, $factKeyDimVal);
        	
			//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
			//Si además de la sección Maestro - Detalle existe una sección dinámica, se invoca al grabado de la sección dinámica con el
			//mismo método que ya se tenía pero primero elimina las respuestas de las secciones Maestro - Detalle del Array recibido
			//ya que todas esas fueron procesadas en el método anterior
			if ($hasDynamicSection && ($strResult == "" || $strResult == "OK"))
			{
				$arrayDataWOMasterDet = $arrayData;
	        	for($i = 0; $i < count($arrMasterDetSections); $i++)
	        	{
	        		$intSectionID = $arrMasterDetSections[$i];
        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
		            foreach ($questionCollection->Collection as $questionElement)
		            {
		            	//Elimina todas las respuestas de preguntas de Maestro - Detalle para no causar conflicto al grabar la dinámica
						$postKey = "qfield".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$postKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$postKey]);
		            	}
		            	$otherPostKey = "qimageencode".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            	$otherPostKey = "qcomment".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            	$otherPostKey = "qactions".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            }
	        	}
				
	        	//Se invoca al llenado de la sección dinámica, como ya se limpiaron todas las preguntas tipo Maestro - Detalle y el método
	        	//que graba las dinámicas no sabe diferenciar entre una sección estática o una Maestro - Detalle, simplemente procesará
	        	//dichas preguntas como si fueran no dinámicas, asignandole el valor de "NA" según su tipo de pregunta
	        	//@JAPR 2012-06-07: Corregido un bug, no se estaba enviando el $factKeyDimVal así que en la parte dinámica generaba un nuevo
	        	//registro de respuestas
				$strResult = multipleUpdateDynamicData($theRepository, $surveyID, $arrayDataWOMasterDet, $dateID, $hourID, $userID, $thisDate, $dynamicSectionID, $factKeyDimVal);
			}
        	//@JAPR
        }
		else if($hasCategoryDimension)
		{
			//@JAPRWarning: Validación temporal para impedir que exista pérdida de datos, no se soportará editar categorías de dimensión solas por ahora
			die('Not supported (category dimension edition)');
			$strResult = multipleUpdateCategoryDimData($theRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate);
		}
		else if($hasDynamicSection)
		{
			$strResult = multipleUpdateDynamicData($theRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $dynamicSectionID, $factKeyDimVal);
		}
		else 
		{
			$strResult = updateData($theRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, null, 0, 0, $factKeyDimVal);
			//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
			//Si entró por aquí quiere decir que ya grabó un único registro, así que no es necesario grabar otro mas adelante
            $blnSaveSingleStandardData = false;
            //@JAPR
		}
		
		//@JAPR 2013-01-10: Agregada la opción para que las preguntas estándar se graben en un registro único
		//Graba el registro con las respuestas de secciones estándar (sólo si se procesaron otro tipo de secciones previamente)
		if ($blnSaveSingleStandardData && ($strResult == "" || $strResult == "OK")) {
			$strResult = singleUpdateStandardData($theRepository, $surveyID, $arrayData, $dateID, $hourID, $userID, $thisDate, $factKeyDimVal);
		}
		
		//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
        //Si hay definida alguna acción de eBavel según la captura, procede a crear cada una mediante la instancia de eBavel
        if (isset($arrActionItems) && is_array($arrActionItems) && count($arrActionItems) > 0) {
        	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
        	foreach ($arrActionItems as $intActionIdx => $anActionData) {
        		try {
				    $manager = new Tables();
				    $manager->applicationCodeName = $eBavelAppCodeName;
				    $manager->init($theRepository->ADOConnection);
	        		$insertData = array();
	        		$insertData[-1] = $anActionData;
					$manager->insertData('ACTIONITEMS', $insertData, array(), $theRepository->ADOConnection);
        		} catch (Exception $e) {
        			//Por el momento no reportará este error
        		}
        	}
		   	$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
        }
        
        //@JAPR 2012-11-20: Agregada la eliminación de registros durante la edición de capturas
        if (!is_null($arrEditedFactKeys) && !is_null($objReportData)) {
	        //Obtiene el total de FactKeys con el que quedó la encuesta
	        $arrEntryFactKeys = array();
	        $strAdditionaFields = '';
	        if ($useSourceFields) {
				$strAdditionaFields = ", EntrySectionID, EntryCatDimID";
	        }
	        $sql = "SELECT FactKey $strAdditionaFields FROM ".$surveyInstance->SurveyTable." WHERE FactKeyDimVal = ".$factKeyDimVal;
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS) {
			    while (!$aRS->EOF) {
			    	$intFactKey = (int) @$aRS->fields["factkey"];
					//@JAPR 2013-01-15: Agregada la opción para que las preguntas estándar se graben en un registro único
					//Si la encuesta está configurada para grabar un registro único para secciones estándar, entonces valida que no se elimine dicho
					//registro único ya que contiene las preguntas estándar y se daña la captura sin él, esto lo hace verificando que no sea el
					//registro donde no está asignada ni la sección ni la pregunta categoría de dimensión, ya que si lo estuvieran serían registros
					//de secciones/preguntas que graban múltiples registros así que esos se deben eliminar si no se ocuparon mas
					if ($useSourceFields && $surveyInstance->UseStdSectionSingleRec) {
						$intEntrySectionID = (int) @$aRS->fields["entrysectionid"];
						$intEntryCatDimID = (int) @$aRS->fields["entrycatdimid"];
						if ($intEntrySectionID == 0 && $intEntryCatDimID == 0) {
							$intFactKey = 0;
						}
					}
					if ($intFactKey > 0) {
			    		$arrEntryFactKeys[$intFactKey] = $intFactKey;
					}
					//@JAPR
			        $aRS->MoveNext();
				}
			}
	        
	        //Recorre los FactKeys que fueron editados para descartarlos y de esa forma sólo eliminar aquellos que no fueron editados
			foreach ($arrEditedFactKeys as $intFactKey) {
				if (isset($arrEntryFactKeys[$intFactKey])) {
					$arrEntryFactKeys[$intFactKey] = 0;
					unset($arrEntryFactKeys[$intFactKey]);
				}
			}
			
			//Elimina los factkeys que hubieran quedado en la captura sin edición
			if (count($arrEntryFactKeys) > 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\nRemoving non updated entry records: ");
					PrintMultiArray($arrEntryFactKeys);
				}
				
				//@JAPR 2013-11-05: Agregado el borrado incremental de agregaciones (cancela los valores actuales)
				//Invoca al método que cancelará los datos de los agregados para los FactKeys que fueron borrados
				if ($gbEnableIncrementalAggregations) {
					require_once('report.inc.php');
					@BITAMReport::IncrementalAggregationsRemove($theRepository, $surveyID, $factKeyDimVal, $arrEntryFactKeys);
				}
				//@JAPR
				
				//Envía los parámetros necesarios para sólo eliminar registros específicos sin eliminar en si la captura completa
				$objReportData->remove(false, $arrEntryFactKeys);
			}
        }
        //@JAPR
	}
	else 
	{
		if($dynamicSectionID>0)
		{
			//Aqui verificamos con el sectionValue que realmente si se crearon arreglos
			//de preguntas en las encuestas para poder capturar respuestas para mas de una
			//encuesta
			$postKey = "sectionValue";
			if(isset($arrayData[$postKey]))
			{
				$hasDynamicSection = true;
			}
		}
		
		//Verificamos si la encuesta trae valores de CategoryDimension
		$postKey = "qCatDimVal";
		if(isset($arrayData[$postKey]))
		{
			$hasCategoryDimension = true;
		}

		$hasMasterDetSections = false;
        $arrMasterDetSections = BITAMSection::existMasterDetSections($theRepository, $surveyID, true);
        if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0)
        {
            //Verifica si realmente se capturaron preguntas de la sección de Maestro detalle, ya que si no es así entonces simplemente
            //se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
            //por lo que no habría problema en procesarlas
            for($i = 0; $i < count($arrMasterDetSections); $i++)
            {
                $intSectionID = $arrMasterDetSections[$i];
                $postKey = "masterSectionRecs".$intSectionID;
                if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0))
                {
                    //Con una sola sección que si capturara un Maestro - Detalle, se asume que si vienen preguntas como array por lo que
                    //se usará el método alterno de grabado
                    $hasMasterDetSections = true;
                    //break;
                }
                else 
                {
                    //Si esta sección siendo un Maestro - Detalle NO capturó ningún conjunto de respuestas, en el mejor de los casos
                    //llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
                    //pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
                    //(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
                    $questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
                    foreach ($questionCollection->Collection as $questionElement) {
                        $postKey = "qfield".$questionElement->QuestionID;
                        if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]))
                        {
                            //En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
                            //"masterSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
                            //no se conocía el concepto de Maestro - Detalle, en cuyo caso no debió haber llegado entonces un Array sino
                            //las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
                            //con el is_array también
                            $arrayData[$postKey] = '';
                        }
                    }
                }
            }
        }
        
		
        //@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
		//Esta variable contendrá el Key asignado a esta captura de encuesta en los casos donde se deban invocar varios de los métodos
		//de grabado encadenados, por ejemplo encuestas que tienen Secciones Maestro - Detalle y Dinámica, ya que al invocar primero al
		//grabado de la Maestro - Detalle se debe respetar el $factKeyDimVal asignado ahí cuando se invoque al grabado de la Dinámica
        $factKeyDimVal = 0;
        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
        //Si hay por lo menos una sección maestro detalle, por ahora no se permiten secciones dinámicas ni preguntas de categoría de
        //dimensiones, ya que el procesamiento de este tipo de secciones es muy parecido al de las secciones dinámicas pero son métodos
        //diferentes por lo que no se pueden combinar aun (eventualmente se podrían combinar con relativa facilidad secciones dinámicas
        //y Maestros detalles reenviando a multipleSaveDynamicData dentro de multipleSaveMasterDetData, pero por ahora no se soporta.
        //El grabado de preguntas de categorías de dimensión se complica aun en el uso de estos tipos de secciones especiales
        //Si no hay seccion dinamica se sigue procesando los datos del POST como siempre se han hecho
        if ($hasMasterDetSections)
        {
			//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
        	$strResult = multipleSaveMasterDetData($theRepository, $surveyID, $arrayData, $thisDate, $arrMasterDetSections, $factKeyDimVal);
        	
			//@JAPR 2012-06-06: Integradas las secciones dinámicas con las Maestro - Detalle
			//Si además de la sección Maestro - Detalle existe una sección dinámica, se invoca al grabado de la sección dinámica con el
			//mismo método que ya se tenía pero primero elimina las respuestas de las secciones Maestro - Detalle del Array recibido
			//ya que todas esas fueron procesadas en el método anterior
			if ($hasDynamicSection && ($strResult == "" || $strResult == "OK"))
			{
				$arrayDataWOMasterDet = $arrayData;
	        	for($i = 0; $i < count($arrMasterDetSections); $i++)
	        	{
	        		$intSectionID = $arrMasterDetSections[$i];
        			$questionCollection = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
		            foreach ($questionCollection->Collection as $questionElement)
		            {
		            	//Elimina todas las respuestas de preguntas de Maestro - Detalle para no causar conflicto al grabar la dinámica
						$postKey = "qfield".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$postKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$postKey]);
		            	}
		            	$otherPostKey = "qimageencode".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            	$otherPostKey = "qcomment".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            	$otherPostKey = "qactions".$questionElement->QuestionID;
		            	if (isset($arrayDataWOMasterDet[$otherPostKey]))
		            	{
		            		unset($arrayDataWOMasterDet[$otherPostKey]);
		            	}
		            }
	        	}
				
	        	//Se invoca al llenado de la sección dinámica, como ya se limpiaron todas las preguntas tipo Maestro - Detalle y el método
	        	//que graba las dinámicas no sabe diferenciar entre una sección estática o una Maestro - Detalle, simplemente procesará
	        	//dichas preguntas como si fueran no dinámicas, asignandole el valor de "NA" según su tipo de pregunta
	        	//@JAPR 2012-06-07: Corregido un bug, no se estaba enviando el $factKeyDimVal así que en la parte dinámica generaba un nuevo
	        	//registro de respuestas
				$strResult = multipleSaveDynamicData($theRepository, $surveyID, $arrayDataWOMasterDet, $thisDate, $dynamicSectionID, $factKeyDimVal);
			}
        	//@JAPR
        }
        else if($hasCategoryDimension)
		{
			$strResult = multipleSaveCategoryDimData($theRepository, $surveyID, $arrayData, $thisDate);
		}
        else if ($hasDynamicSection) 
        {
            $strResult = multipleSaveDynamicData($theRepository, $surveyID, $arrayData, $thisDate, $dynamicSectionID);
        } 
		else 
		{
			//@JAPR 2012-10-20: Agregadas las columnas para identificar las secciones a las que corresponden los registros de las tablas de capturas
        	//Es un registro de secciones estándar, así que no se graba información del SectionID ni de Categoría de dimensión
			$strResult = saveData($theRepository, $surveyID, $arrayData, $thisDate);
		}
	}
	
	//@JAPR 2012-03-14: Agregado el grabado del Status de las Agendas
	if ($agendaID > 0 && ($strResult == '' || $strResult == 'OK'))
	{
		//Si se envió el ID de la agenda y se logró grabar la captura de encuesta, se actualiza el Status de la misma para que ya no se
		//vuelva a considerar en la descarga de las Agendas hacia los móviles
		$sql = "UPDATE SI_SV_SurveyAgendaDet SET Status = 1 ".
			"WHERE AgendaID = ".$agendaID." AND SurveyID = ".$surveyID;
		$theRepository->DataADOConnection->Execute($sql);
	}
	
	//Termina de actualizar la información, así que en v4 en lugar de continuar con el HTML original debe regresar una indicación de que todo
	//estuvo ok o el Status de error
	$logdate = date('YmdHis');
	if ($appVersion >= esveFormsV41Func) {
		$strProcessType = ($blnEdit)?"Update data":"Save data";
	    if ($strResult == "OK") {
	        $strResult = "Survey data was saved successfully.";
	
			//guardamos en el log
			$stat_action = "[".$friendlyDate."] [Action: $strProcessType ok] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
			@error_log($stat_action, 3, $statlogfile);
	
	    } else {
			//Guardamos en el log de errores
			$thelog = "[".$friendlyDate."] [Action: $strProcessType error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $strResult . " POST = " . print_r($_POST,true);
			$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
			@error_log($thelog, 3, $thefile);
	        
			//guardamos en el log
			$stat_action = "[".$friendlyDate."] [Action: $strProcessType error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
			@error_log($stat_action, 3, $statlogfile);
			
			//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
			//En este caso como es captura desde browser, hay que regresar el error exacto
			/*
			if ($dieOnError) {
				$strResult = "There was an error while saving the outbox, if you do not found it on the reports page, please contact your System Administrator";
			}
			else {
				$strResult = "Survey data was saved successfully.";
			}
			*/
	    }
		
	    header("Content-Type: text/plain");
	    echo($strResult);
	    
		//@JAPR 2013-11-05: Agregado el grabado incremental de agregaciones
	    if (!$gbEnableIncrementalAggregations && !$gbTestIncrementalAggregationsData) {
	    	//Si no se van ni a grabar ni a probar agregaciones incrementales, entonces si termina la ejecución en este punto, de lo contrario
	    	//regresa al método StoreSurveyCapture para procesar a los agregados incrementales
		    exit();
	    }
	    else {
	    	//Evita que se despliegue el HTML y regresa el control al script que incluyó a este
	    	return;
	    }
	    //@JAPR
	}
	//@JAPR	
}
else 
{
	//Si es el app de versión 4, no redirecciona sino que regresa el status de error
	if ($appVersion >= esveFormsV41Func) {
		die();
	}
}

function testSaveMethodParams($aRepository)
{
	//$blnDebug = getParamValue('debug', 'both', "(boolean)");
	$strDataFileName = getParamValue('datafilename', 'both', "(string)");
	if ($strDataFileName == '')
	{
    	generateErrorMessage(-407, "The data file name was not specified");
	}
	
	$strParameters = @file_get_contents("./uploaderror/".$strDataFileName);
	if ($strParameters == false)
	{
    	generateErrorMessage(-408, "The data file name specified was not found: ./uploaderror/".$strDataFileName);
	}
    
	$GET = '';
	$POST = '';
	if (trim($strParameters) != '')
	{
		@eval($strParameters);
	}
	
    header("Content-Type: text/plain");
	echo("GET: \r\n");
	var_dump($GET);
	echo("\r\nPOST: \r\n");
	var_dump($POST);
	foreach($GET as $aKey => $aValue)
	{
		$_GET[$aKey] = $aValue;
	}
	foreach($POST as $aKey => $aValue)
	{
		$_POST[$aKey] = $aValue;
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <!-- Change this if you want to allow scaling -->
        <!--<meta name="viewport" content="width=default-width; user-scalable=no" />-->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>eSurvey</title>
		<link href='favicon.ico' rel='shortcut icon' type='image/x-icon'/>
		<link href='favicon.ico' rel='icon' type='image/x-icon'/>
        <!-- iPad/iPhone specific css below, add after your main css >
		<link rel="stylesheet" media="only screen and (max-device-width: 1024px)" href="ipad.css" type="text/css" />		
		<link rel="stylesheet" media="only screen and (max-device-width: 480px)" href="iphone.css" type="text/css" />		
		-->
        
        <!-- If you application is targeting iOS BEFORE 4.0 you MUST put json2.js from http://www.JSON.org/json2.js into your www directory and include it here -->
        <link rel="stylesheet" href="jquery.mobile-1.0/jquery.mobile-1.0.css"/>
        <link type="text/css" href="jquery.mobile-forms.css" rel="stylesheet"/>
        <script type="text/javascript" src="jquery-1.6.2.js"></script>
        <script type="text/javascript" src="jquery.mobile-1.0/jquery.mobile-1.0.js"></script>
        
        <script type="text/javascript" charset="utf-8">
		function onBodyLoad()
		{

		}
        </script>
    </head>
    <body onload="onBodyLoad()">
        <div data-role="page" id="pageProcess" data-theme="c">
            <div data-role="header" data-id="headerPageProcess" data-theme="f" data-backbtn="false">
                <h1><?=$surveyInstance->SurveyName?></h1>
<?
	if((!isset($_SESSION["SVExternalPage"]) || $_SESSION["SVExternalPage"]==0) && (!isset($_SESSION["SVFromESurveyAdmin"]) || $_SESSION["SVFromESurveyAdmin"]==0))
	{
?>
                <a href="surveyList.php" data-ajax="false" class="ui-btn-right" data-theme="f">Back</a>
<?
	}
?>
            </div>
            <div data-role="content" id="contentPageProcess">
				<h3 style="text-align:center">
<?
$strMessage = "Thank you for taking our survey. Your response is very important to us.";

if(trim($surveyInstance->ClosingMsg)!="")
{
	$strMessage = $surveyInstance->ClosingMsg;
}
?>
					<?=$strMessage?>
				</h3>
            </div>
            <br><br>
            <div data-role="footer" data-id="footerPageProcess" style="position: absolute; bottom: 0;" data-theme="f">
<?
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
		if(!preg_match('/MSIE/i',$u_agent)) 
    	{
?>  
        <div data-role="navbar">
        <ul>
        <li><a href="javascript:openWindow('http://kpionline.bitam.com/english');">Powered By BITAM</a></li>
        <li><a href="#"><?=translate("About")?></a></li>
        </ul>
        </div>
<?
    	} 
    	else 
    	{
?> 
        <div data-inline="true">
			<a data-theme="a" style="width:49.6%;margin:0;padding:0;" href="javascript:openWindow('http://kpionline.bitam.com/english');" data-role="button" data-inline="true">Powered By BITAM</a>
			<a data-theme="a" style="width:49.6%;margin:0;padding:0;" href="#" data-role="button" data-inline="true"><?=translate("About")?></a>
		</div>
<?
    	}
    	
    	//Detectamos si la variable de session $_SESSION["SVExternalPage"] existe y es igual a 1
    	if(isset($_SESSION["SVExternalPage"]) && $_SESSION["SVExternalPage"]==1)
    	{
			session_unset();
			session_destroy();
			setcookie("PABITAM_UserName", "");
    	}
?>
            </div>
        </div>
    </body>
</html>