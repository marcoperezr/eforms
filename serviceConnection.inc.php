<?php

require_once("repository.inc.php");

class BITAMServiceConnection extends BITAMObject
{
	public $ForceNew;
	public $ServiceID;					//Identificador único de la conexión
	public $ServiceName;
	public $AccessToken;
	public $User;
	public $Password;
	public $FTP;
	public $Type;						//Tipo de destino, no todos aplican para esta tabla, ya que eBavel no requiere configuración especial aquí
	public $PassParameter;
	public $UserParameter;
	public $TypeRequest;				//Tipo de request realizado, POST, PUT, etc.
	public $Servicedbtype;				//Tipo de conexion de base de datos 0 = MySQL, 1 = SQLServer
	
	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->ForceNew = false;
		$this->ServiceID = 0;
		$this->ServiceName = '';
		$this->AccessToken = '';
		$this->User = '';
		$this->Password = '';
		$this->FTP = '';
		$this->Type = ddesNone;
		$this->PassParameter = '';
		$this->UserParameter = '';
		$this->TypeRequest = ddrtPOST;
		$this->Servicedbtype = ddrtMySQL;
	}
	
	static function NewInstance($aRepository)
	{
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	//@JAPR 2013-11-27: Agregado el Atributo de Accuracy
	//Agregado el parámetro $bFromCollection para indicar que se está cargando la instancia desde la colección, si es así, no realiza la carga
	//de algunos elementos adicionales ni otros procesos que podrían resultar muy tardados
	static function NewInstanceWithID($aRepository, $aServiceID)
	{
		//@JAPR 2013-06-05: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$anInstance = null;
		if (((int) $aServiceID) < 0)
		{
			return $anInstance;
		}
		
		//$anInstance =& BITAMGlobalFormsInstance::GetSurveyInstanceWithID($aServiceID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}

		$strAdditionalFields = '';
		if(getMDVersion()>=esvTypeOfDatabase){
				$strAdditionalFields .= ', servicedbtype ';
		}
		$sql = "SELECT t1.id, t1.name, t1.access_token, t1.user, t1.pass, t1.ftp, t1.type, t1.pass_parameter, t1.user_parameter, t1.type_request {$strAdditionalFields}
			FROM SI_SV_ServicesConnection t1 
			WHERE t1.id = {$aServiceID} 
			ORDER BY t1.name";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
		    if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		    else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		    }
		}
		
		if (!$aRS->EOF)
		{
			//Crea la instancia a partir de la clase que realmente ejecuta el método
			$strCalledClass = static::class;
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS, $bFromCollection = false)
	{
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->ServiceID = (int) $aRS->fields["id"];
		$anInstance->ServiceName = (string) $aRS->fields["name"];
		$anInstance->AccessToken = (string) $aRS->fields["access_token"];
		$anInstance->User = (string) $aRS->fields["user"];
		$anInstance->Password = (string) $aRS->fields["pass"];
		$anInstance->FTP = (string) $aRS->fields["ftp"];
		$anInstance->Type = (int) $aRS->fields["type"];
		$anInstance->PassParameter = (string) $aRS->fields["pass_parameter"];
		$anInstance->UserParameter = (string) $aRS->fields["user_parameter"];
		$anInstance->TypeRequest = (int) $aRS->fields["type_request"];
		//@JRPP 2016-01-14 Se Agrega el campo servicedbtype 
		if (getMDVersion()>=esvTypeOfDatabase) {
			$anInstance->Servicedbtype = (int) $aRS->fields["servicedbtype"];	
		}

		
		//BITAMGlobalFormsInstance::AddSurveyInstanceWithID($anInstance->ServiceID, $anInstance);
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		if (array_key_exists("ServiceID", $aHTTPRequest->POST))
		{
			$aServiceID = $aHTTPRequest->POST["ServiceID"];
			if (is_array($aServiceID))
			{
				$aCollection = BITAMServiceConnectionCollection::NewInstance($aRepository, $aServiceID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aServiceID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("ServiceID", $aHTTPRequest->GET))
		{
			$aServiceID = $aHTTPRequest->GET["ServiceID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$aServiceID);
			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("ServiceID", $anArray))
		{
			$this->ServiceID = (int)$anArray["ServiceID"];
		}
		
	 	if (array_key_exists("ServiceName", $anArray))
		{
			$this->ServiceName = rtrim($anArray["ServiceName"]);
		}
		
		if (array_key_exists("Type", $anArray))
		{
			$this->Type = (int)$anArray["Type"];
		}
		
	 	if (array_key_exists("AccessToken", $anArray))
		{
			$this->AccessToken = rtrim($anArray["AccessToken"]);
		}

	 	if (array_key_exists("User", $anArray))
		{
			$this->User = rtrim($anArray["User"]);
		}
		
		if(array_key_exists("Password", $anArray))
		{
			$this->Password = (string) $anArray["Password"];
		}
		
		if(array_key_exists("FTP", $anArray))
		{
			$this->FTP = (string) $anArray["FTP"];
		}
		
		if(array_key_exists("PassParameter", $anArray))
		{
			$this->PassParameter = (string) $anArray["PassParameter"];
		}
		
		if(array_key_exists("UserParameter", $anArray))
		{
			$this->UserParameter = (string) $anArray["UserParameter"];
		}
		
		if(array_key_exists("TypeRequest", $anArray))
		{
			$this->TypeRequest = (int) $anArray["TypeRequest"];
		}

		if(array_key_exists("Servicedbtype", $anArray))
		{
			$this->Servicedbtype = (int) $anArray["Servicedbtype"];
		}
		
		return $this;
	}
	
	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	function save($bJustMobileTables = false) {
		global $gblShowErrorSurvey;
		global $gblModMngrErrorMessage;
		$gblModMngrErrorMessage = '';
		
		$currentDate = date("Y-m-d H:i:s");
		$strCaptureStartTime = "";
		$strCaptureEndTime = "";
		
		$isNewObj = $this->isNewObject();
		
		//@JAPR 2014-09-12: Agregados los datos extendidos de modificación
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		//@JAPR
		
		$strOriginalWD = getcwd();
	 	if ($this->isNewObject()) {
			$sql =  "SELECT ".$this->Repository->DataADOConnection->IfNull("MAX(id)", "0")." + 1 AS ServiceID".
				" FROM SI_SV_ServicesConnection";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->ServiceID = (int) $aRS->fields["serviceid"];
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			//@JRPP 2016-01-14 Se Agrega el campo servicedbtype al insert de la clase
			if(getMDVersion()>=esvTypeOfDatabase){
				$strAdditionalFields .= ', servicedbtype ';
			}
			$sql = "INSERT INTO SI_SV_ServicesConnection (".
				" id".
				", name".
				", access_token".
				", user".
				", pass".
				", ftp".
				", type".
				", pass_parameter".
				", user_parameter".
				", type_request".
				$strAdditionalFields.
				") VALUES (".
				$this->ServiceID.
				",".$this->Repository->DataADOConnection->Quote($this->ServiceName).
				",".$this->Repository->DataADOConnection->Quote($this->AccessToken).
				",".$this->Repository->DataADOConnection->Quote($this->User).
				",".$this->Repository->DataADOConnection->Quote($this->Password).
				",".$this->Repository->DataADOConnection->Quote($this->FTP).
				",".$this->Type.
				",".$this->Repository->DataADOConnection->Quote($this->PassParameter).
				",".$this->Repository->DataADOConnection->Quote($this->UserParameter).
				",".$this->TypeRequest.
				((getMDVersion()>=esvTypeOfDatabase)?",".$this->Servicedbtype:"").
				")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
		else {
			$strAdditionalValues = '';
			if (getMDVersion()>=esvTypeOfDatabase) {
				$strAdditionalValues .= ", servicedbtype = " . $this->Servicedbtype;
			}
			$sql = "UPDATE SI_SV_ServicesConnection SET ".
					"name = ".$this->Repository->DataADOConnection->Quote($this->ServiceName).
					", type = ".$this->Type.
					", access_token = ".$this->Repository->DataADOConnection->Quote($this->AccessToken).
					", user = ".$this->Repository->DataADOConnection->Quote($this->User).
					", pass = ".$this->Repository->DataADOConnection->Quote($this->Password).
					", ftp = ".$this->Repository->DataADOConnection->Quote($this->FTP).
					", pass_parameter = ".$this->Repository->DataADOConnection->Quote($this->PassParameter).
					", user_parameter = ".$this->Repository->DataADOConnection->Quote($this->UserParameter).
					", type_request = ".$this->Repository->DataADOConnection->Quote($this->TypeRequest).
					$strAdditionalValues.
				" WHERE id = ".$this->ServiceID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
			
		return $this;
	}

	//@JAPR 2015-07-26: Descontinuado el parámetro $bJustMobileTables
	function remove($bJustMobileTables = false)
	{
		$sql = "UPDATE SI_SV_DataSource SET id_ServiceConnection = ".ddesNone. " 
			WHERE id_ServiceConnection = {$this->ServiceID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "UPDATE SI_SV_DataDestinations SET id_connection = ".ddesNone. " 
			WHERE id_connection = {$this->ServiceID}";
		$this->Repository->DataADOConnection->Execute($sql);
		
		$sql = "DELETE FROM SI_SV_ServicesConnection WHERE id = ".$this->ServiceID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->ServiceID < 0 || $this->ForceNew);
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
	//directamente de este
	function getJSonDefinition() {
		global $appVersion;
		global $gbDesignMode;
		
		$arrDef = array();
		$arrDef['id'] = $this->ServiceID;
		$arrDef['name'] = $this->ServiceName;
		$arrDef['type'] = $this->Type;
		$arrDef['user'] = $this->User;
		$arrDef['ftp'] = $this->FTP;
		$arrDef['userParameter'] = $this->UserParameter;
		$arrDef['requestType'] = $this->TypeRequest;
		$arrDef['servicedbtype'] = $this->Servicedbtype;
		
		return $arrDef;
	}
}

class BITAMServiceConnectionCollection extends BITAMCollection
{
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
	}
	
	static function NewInstance($aRepository, $anArrayOfServicesIDs = null, $exceptInactive=true)
	{
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		//@JAPR 2015-03-31: Rediseñado el Admin con DHTMLX
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$where = "";
		if (!is_null($anArrayOfServicesIDs))
		{
			switch (count($anArrayOfServicesIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.id = ".((int) $anArrayOfServicesIDs[0]);
					break;
				default:
					foreach ($anArrayOfServicesIDs as $aServiceID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$aServiceID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.id IN (".$where.")";
					}
					break;
			}
		}
		
		if($exceptInactive==true)
		{
			if($where!="")
			{
				$where.=" AND ";
			}
			else 
			{
				$where.=" WHERE ";
			}
			
			$where.=" t1.Status <> ".svstInactive;
		}

		$strAdditionalFields = '';
		if(getMDVersion()>=esvTypeOfDatabase){
			$strAdditionalFields .= ', servicedbtype ';
		}
		$sql = "SELECT t1.id, t1.name, t1.access_token, t1.user, t1.pass, t1.ftp, t1.type, t1.pass_parameter, t1.user_parameter, t1.type_request {$strAdditionalFields}
			FROM SI_SV_ServicesConnection t1 ".$where." 
			ORDER BY t1.name";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false) {
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_ServicesConnection ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		while (!$aRS->EOF) {
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS, true);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>