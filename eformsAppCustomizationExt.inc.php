<?php
//Esta clase permite configurar la vista de la App, seleccionando entre los colores y varios elementos que se enviarán al App para cambiar el Look
require_once("repository.inc.php");
require_once("initialize.php");
require_once("eformsappcustomization.inc.php");
require_once("object.trait.php");

class BITAMEFormsAppCustomizationExt extends BITAMEFormsAppCustomization
{
	
	use BITAMObjectExt;
	public $TreeArr;
  	function __construct($aRepository)
	{
		parent::__construct($aRepository);
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
		$this->TreeArr=array();
	}
	
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=EFormsAppCustomizationTplExt";
		}
		else
		{
			return "BITAM_PAGE=EFormsAppCustomization&TemplateID=".$this->TemplateID;
		}
	}
	
	function get_Image() {
		
		$strImage = "<img src=\"images/admin/template.png\" />";
		
		return $strImage;
	}
	
	function getTree()
	{
        $treeArr = array( 'Buttons' => array(
                                            'Text' => array(),
                                            'ButtonUp' => array(),
                                            'ButtonHover' => array(),
                                            'ButtonDown' => array(),
                                      ),
                          'ButtonBackToFormList' => array(
                                                        'DEFAULT' => array(),
                                                        'Border'=> array()
                                                    ),
                          'TopButtonBars' => array(
                                                 'DEFAULT' => array()
                                             ),
                          'SectionsAndQuestionsArea' => array(
                                                        'DEFAULT' => array(),
                                                        'Texts' => array()
                                                    ),
                          'Others' => array(
                                          'DEFAULT' => array()
                                      )
                        );

        
        
        //recorreo y clasifico      
        foreach($this->ArrayVariables as $variableID => $variable)
		{
            switch ($variable) {
                //seccion botones. subsec:textos-0-1
                case ('CLRTMLPButtonLabelColor'):
                
                    $treeArr['Buttons']['Text'][] = $variable;
                    break;
                
                //seccion botones, subsec:Botón deshabilitado, no utilizable-0-2
                case 'CLRTMPLButtonUpBorderColor':
                case 'CLRTMPLButtonUpBackgroundColorStart':
                case 'CLRTMPLButtonUpBackgroundColorEnd':
                    
                    $treeArr['Buttons']['ButtonUp'][] = $variable;
                    break;
                
                //sec:botones, subsec:Botón al pasar el puntero encima-0-3
                case 'CLRTMPLButtonHooverBorderColor':
                case 'CLRTMPLButtonHooverBackgroundColorStart':
                case 'CLRTMPLButtonHooverBackgroundColorEnd':
                
                    $treeArr['Buttons']['ButtonHover'][] = $variable;
                    break;
                
                //sec:botones, sub:Boton up estado normal-0-4
                case 'CLRTMPLButtonDownBorderColor':
                case 'CLRTMPLButtonDownBackgroundColorStart':
                case 'CLRTMPLButtonDownBackgroundColorEnd':
                
                    $treeArr['Buttons']['ButtonDown'][] = $variable;
                    break;
                
                //sec:Botón de regresar a la lista formas-1-0, sub:''
                case 'CLRTMLPMainButtonBackgroundColorStart':
                case 'CLRTMLPMainButtonBackgroundColorStop1':
                case 'CLRTMLPMainButtonBackgroundColorStop2':
                case 'CLRTMPLMainButtonBackgroundColorEnd':
                
                    $treeArr['ButtonBackToFormList']['DEFAULT'][] = $variable;
                    break;
                
                //sec:Botón de regresar a la lista formas, sub:Borde-1-1
                case ('CLRTMLPMainButtonBorderColor'):
                    
                    $treeArr['ButtonBackToFormList']['Border'][] = $variable;
                    break;
                
                //sec:Barra Superior e Inferior-2-0, sub:''
                case 'CLRTMLPBarsBorderColor':
                case 'CLRTMLPBarsBackgroundColorStart':
                case 'CLRTMLPBarsBackgroundColorEnd':
                    
                    $treeArr['TopButtonBars']['DEFAULT'][] = $variable;
                    break;
                    
                //sec:Secciones y área de preguntas3-0, sub:''
                case 'CLRTMLPSectionBackgroundColorStart':
                case 'CLRTMLPSectionBackgroundColorEnd':
                                
                    $treeArr['SectionsAndQuestionsArea']['DEFAULT'][] = $variable;
                    break;
                
                //sec:Textos-4-0, sub:''
                case 'CLRTMPLSectionTitleColor':
                case 'CLRTMPLOuterLabelColor':
                case 'CLRTMPLInnerLabelColor':
                    
                    $treeArr['SectionsAndQuestionsArea']['Texts'][] = $variable;
                    break;
                
                //no clasificados
                default:
                
                    $treeArr['Others']['DEFAULT'][] = $variable;
                    break;
            }
        }
        
        //si la seccion others no tiene elementos la quitamos
        if (count($treeArr['Others']['DEFAULT']) === 0){
            unset($treeArr['Others']);
        }
		$this->TreeArr = $treeArr;
	}
	
	function generateForm($aUser) {
		//Se pone en false para indicar que no está desplegada la colección de templates de colores si no que estará desplegara la 
		//ventada de propiedades de un template específico
		$this->generateBeforeFormCode($aUser);
?>
<html>
<?
	//Contiene lo necesario para el manejo del grid
	require_once("genericgrid.inc.php");
?>
	<script>
		var tabColors = "colors";
		var objFormTabs;					//Cejillas de la opción de configuraciones
<?
		$arrJSON = BITAMEFormsAppCustomizationExt::getJSonDefinition($this->Repository);
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		$strJSONDefinitions = json_encode($arrJSON);
?>		
		var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
		var objAppCust = objFormsDefs.data;
		
		//******************************************************************************************************
		EFormsAppCustomizationCls.prototype = new GenericItemCls();
		EFormsAppCustomizationCls.prototype.constructor = EFormsAppCustomizationCls;
		function EFormsAppCustomizationCls() {
			this.objectType = AdminObjectType.otyAppCustomization;
			this.objectIDFieldName = "TemplateID";
		}
		
		/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
		//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
		*/
		EFormsAppCustomizationCls.prototype.getDataFields = function(oFields) {
			var blnAllFields = false;
			if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
				blnAllFields = true;
			}
			var objParams = {};
<?			
			foreach($this->ArrayVariables as $variable)
			{				
?>
			if (blnAllFields || oFields["<?=$variable?>"]) {
				objParams["<?=$variable?>"] = this.<?=$variable?>;
			}
<?
			}
?>	
			if (blnAllFields || oFields["TemplateName"]) {
				objParams["TemplateName"] = this.TemplateName;
			}

			return objParams;
		}
		
		//******************************************************************************************************			
		function doOnLoad() {
			console.log('doOnLoad');
			
			addClassPrototype();
			
			objMainLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});
			objMainLayout.cells("a").appendObject("divDesign");
			objMainLayout.cells("a").hideHeader();
			//objMainLayout.setSizes();
			objFormTabs = new dhtmlXTabBar({
				parent:"divDesignBody"
			});
			objFormTabs.addTab(tabColors, "<span class='tabFormsOpts'><?=translate("Colors")?></span>", null, null, true);
			objFormTabs.setArrowsMode("auto");
			objFormTabs.tabs(tabColors).attachLayout({
				parent: document.body,
				pattern: "1C"
			});
			objFormsLayout = objFormTabs.tabs(tabColors).getAttachedObject();
			//Evento del cambio de tab para permitir grabar información que hubiera cambiado
			objFormTabs.attachEvent("onTabClick", function(id, lastId) {
				console.log('objFormTabs.onTabClick ' + id + ', lastId == ' + lastId);
			});
			objFormTabs.attachEvent("onContentLoaded", function(id) {
				objFormTabs.tabs(id).progressOff();
			});
			
			//Genera la ventana de propiedades
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?>");
			objPropsLayout = objFormsLayout.cells(dhxPropsCell).attachLayout({pattern: "2U"});
			objPropsLayout.setSizes();
			objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Properties")?>");
			objPropsLayout.cells(dhxPropsDetailCell).hideHeader();
			objFormsLayout.cells(dhxPropsCell).hideHeader();
			objPropsLayout.cells("b").setText("<?=translate("Preview")?>");
			objPropsLayout.cells("b").hideHeader();
			
			//Agrega el evento para procesos cuando termina de cargar cada frame del layout
			objPropsLayout.attachEvent("onContentLoaded", function(id) {
				console.log('objPropsLayout.onContentLoaded ' + id);
				objPropsLayout.cells(id).progressOff();
			});
			
			doShowAppCustomizationProps();
			//Marca la opción de Colors como seleccionada
			//$("#divDesignHeader").find("[tabname='colors']").addClass("linktdSelected");
		}
		/*
		function doChangeTab(sTabId) {
			console.log('doChangeTab ' + sTabId);
			//debugger;
			var objEvent = this.event || window.event;
			if (objEvent) {
				var objTarget = objEvent.target || objEvent.srcElement;
				if (objTarget) {
					$("#divDesignHeader td").removeClass("linktdSelected");
					$(objTarget).addClass("linktdSelected");
				}
			}
			if (!sTabId || !objFormTabs || !objFormTabs.tabs(sTabId)) {
				return;
			}
			//alert("doChangeTab-sTabId:"+sTabId);
			objFormTabs.tabs(sTabId).setActive();
		}
		*/
		function addClassPrototype() {
			if (!objAppCust) {
				return;
			}
			
			$.extend(objAppCust, new EFormsAppCustomizationCls());
		}
		
		function doShowAppCustomizationProps(){
			console.log('doShowAppCustomization');
			objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
			generateAppCustomizationProps();
			//Generar vista previa
			
			generatePreview();
		}
		
			/* Obtiene la referencia al objeto indicado por el tipo y id de los parámetros. Si se necesita un objeto específico de alguna colección que le pertenece, se utilizan 
			los parámetros child (esto se debería aplicar sólo si el objeto principal no tiene una forma directa de ser accesado, originalmente utilizado para las opciones de
			respuesta fijas de preguntas/secciones)
			*/
			function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
				var objObject = objAppCust;
				
				return objObject;
			}
		
		function generatePreview()
		{	
			objPropsLayout.cells("b").detachObject(true);
			objPropsLayout.cells("b").attachObject("PreviewHTMLSection");
		}
		function generateAppCustomizationProps() 
		{
			console.log('generateAppCustomizationProps');
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Customization")?>");
			
			//Generar todos los fields
			objTabsColl = {
				"generalTab":
				{
					id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true,defaultVisibility:{name:true, descrip:true}
				}
			};
<?
			$this->getTree();
?>			
			var objCommon = {id:1, parentType:AdminObjectType.otyAppCustomization, tab:"generalTab"};
			objTabsColl["generalTab"].fields =  new Array(
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"TemplateID", label:"<?=translate("Template")?>", type:GFieldTypes.alphanum, maxLength:255})),
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"TemplateName", label:"<?=translate("Template")?>", type:GFieldTypes.alphanum, maxLength:255})),
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"UserID", label:"<?=translate("User")?>", type:GFieldTypes.alphanum, maxLength:255})),
<?
		$numSetVariables = count($this->ArrayVariables);
		$iSetVar = 1;
        foreach($this->TreeArr as $sections => $content) {
?>            
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$sections?>", label:"<p style=\"font-weight:bold;\"><?=translate($sections)?></p>", type:GFieldTypes.readOnly, maxLength:255})),
<?
            foreach($content as $subsection => $subcontent){
                if ($subsection !== 'DEFAULT'){
?>
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$subsection?>", label:"<p style=\"text-indent:20px !important;\"><?=translate($subsection)?></p>", type:GFieldTypes.readOnly, maxLength:255})),
<?
					$styleTab = "text-indent:40px !important;";
                }else{
					$styleTab = "text-indent:20px !important;";
                }
                foreach($subcontent as $options ){
?>
					new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$options?>", label:"<p style=\"<?=$styleTab?>\"><?=translate($options)?></p>", type:GFieldTypes.colorSelector, maxLength:255}))<?if($iSetVar<$numSetVariables){?>,<?}?>
<?
					$iSetVar++;
                }
            }
        }
				
?>			
				/*
			<?
				$numSetVariables = count($this->ArrayVariables);
				$iSetVar = 1;
				foreach($this->ArrayVariables as $variable)
				{	
					$fieldType="input";
					switch($fieldType)
					{
						case "input":
						{
			?>
						new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"<?=$variable?>", label:"<?=translate($variable)?>", type:GFieldTypes.colorSelector, maxLength:255}))<?if($iSetVar<$numSetVariables){?>,<?}?>
			<?		
							break;
						}
						default:
						{
							break;
						}
					}
					$iSetVar++;
				}
			?>
				*/
			);
			//JAPR 2015-06-20: Agregado el parámetro con el contenedor del grid de propiedades
			//Los detalles de la forma se muestran en el tab de propiedades
			generateFormDetail(getAppCustomizationFieldsDefaulVisibility(objTabsColl), AdminObjectType.otyAppCustomization, 1, objPropsLayout.cells(dhxPropsDetailCell));
		}

		/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default*/
		function getAppCustomizationFieldsDefaulVisibility(oTabsColl) {
			console.log("getAppCustomizationFieldsDefaulVisibility ");
			
			var strTabId = "generalTab";
			if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
				oTabsColl[strTabId].defaultVisibility = {};
				
<?
        foreach($this->TreeArr as $sections => $content) {
?>            
			oTabsColl[strTabId].defaultVisibility["<?=$sections?>"] = true;				
<?
            foreach($content as $subsection => $subcontent){
                if ($subsection !== 'DEFAULT'){
?>
				oTabsColl[strTabId].defaultVisibility["<?=$subsection?>"] = true;								
<?
                }
				
                foreach($subcontent as $options ){
?>
					oTabsColl[strTabId].defaultVisibility["<?=$options?>"] = true;
<?
                }
            }
        }
			/*
				foreach($this->ArrayVariables as $variable)
				{
			?>
				oTabsColl[strTabId].defaultVisibility["<?=$variable?>"] = true;
			<?
				}
			*/
?>
				//oTabsColl[strTabId].defaultVisibility["TemplateID"] = true;
				oTabsColl[strTabId].defaultVisibility["TemplateName"] = true;
				//oTabsColl[strTabId].defaultVisibility["UserID"] = true;
			}
			return oTabsColl;
		}
		
		/* Regresa la definición del campo a partir de la combinación tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
		que se está desplegando en el momento en que se solicita, ya que esos son los que están almacenados en el array
		//JAPR 2015-06-25: Agregado el parámetro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
		que se está desplegando en la celda de propiedades
		*/
		function getFieldDefinition(sTabName, sFieldName, bForForm) {
			console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					for (var intCont in objTab.fields) {
						var objFieldTmp = objTab.fields[intCont];
						if (objFieldTmp && objFieldTmp.name == sFieldName) {
							objField = objFieldTmp;
							break;
						}
					}
				}
			}
			
			return objField;
		}
		
		/* Idéntica a getFieldDefinition pero en lugar del nombre de campo el cual es útil en un grid de propiedades, se utiliza el índice del campo, el cual es útil en
		un grid de valores */
		function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
			console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					objField = objTab.fields[iFieldIdx];
				}
			}
			
			return objField;
		}
	</script>
	
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<div id="divColors" style="display:none;height:100%;width:100%;">
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
<!--
			<div id="divDesignHeader" class="linkToolbar" style="height:30px;width:100%">
				<table style="/*width:100%;*/height:100%;">
					<tbody>
						<tr>
							<td class="linkTD" tabName="colors" onclick="doChangeTab($(this).attr('tabName'))"><?=translate("Colors")?></td>
						</tr>
				  </tbody>
				</table>
			</div>
-->
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
		<div id="PreviewHTMLSection" style="display: none; width: 100%; height: 335px">
			<br>
			<div class="object_title"><?= translate("Preview") ?></div>
			<hr class="object_title_hr">
						 
			<div id="section_header" class="ui-header ui-bar-f" style="position: relative;  height: 45px;">
				<a id="btn_mnuformas" class="UIButton ui-btn-up-b left ui-btn-left" style="position:absolute; top:8px;cursor:pointer">
					<span class="pivot"></span>
					Botón regresar menú formas
				</a>
				 <div style="position:absolute; top:15px; left:45%">
							<label id="top_bar_label" class="inBarLabel"> Barra Superior </label>
				</div>
				<a id="btn_nav_next" style="position:absolute;top:8px;right:10px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
						<span class="ui-btn-text">Botón navegador</span>
						<span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
					</span>
				</a>
			</div>
			<div id="section_body" class="ui-content ui-body-c" style=" ">
				<h3 id="section_body_title" style="font-weight:bold;font-size:18;">Ejemplo de título en sección</h3>
				<hr>
			</div>
			<div id="qfield2_area" data-role="fieldcontain" style="width:100%; position: relative;margin: 0;" class="ui-content ui-field-contain ui-body-c ui-br">
					<label id="section_body_label" class="inSecLabel" style="font-size: 14px;text-indent: 1em;">
						Ejemplo de etiqueta dentro de sección</label>
					<br>
					<br>

					<a id="btn_nav_next" style="position: relative;width: 200px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
						<span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
							<span class="ui-btn-text">Botón en sección</span>
						</span>
					</a>
					<br>
			</div>
			<div id="no_section" data-role="fieldcontain" style="margin: 0; " class="ui-field-contain ui-body ui-br">
				<div><label id="label_out_section" for="" style="position: relative;width:100%;font-size:14;" class="ui-input-text">Ejemplo de etiqueta fuera de sección</label></div>
			</div>
			<div id="section_footer" style="width: 100%;height:45px;position: relative;" class="ui-footer ui-bar-f" role="contentinfo">
				<div style="position:absolute; top:15px; left:46%">
				   <label id="bottom_bar_label" class="inBarLabel"> Barra Inferior </label>
				</div>

				<a id="btn_nav_next" style="position:absolute;top:6px;right:10px;" class="ui-btn-right ui-btn ui-btn-up-b ui-btn-icon-left ui-btn-corner-all ui-shadow">
					<span class="ui-btn-inner ui-btn-corner-all" aria-hidden="true" style="overflow: visible;">
						<span class="ui-btn-text">Botón navegador</span>
						<span class="ui-icon ui-icon-arrow-r ui-icon-shadow"></span>
					</span>
				</a>
			</div>
		</div>
		
	</body>
</html>
<?
		die();
	}	
}
?>