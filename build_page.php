<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
//OMMC 2015-10-01: Se hace la validación del browser para permitir o negar el acceso.
require_once('browser_validation.php');
grantService();

$showAllFrames = true;

if(isset($_SESSION["PAdisplayAllFrames"]))
{
	if($_SESSION["PAdisplayAllFrames"]==0)
	{
		$showAllFrames = false;
	}
}

if($showAllFrames)
{
	$strMainFrameset='4%,96%';
}
else 
{
	$strMainFrameset='0%,100%';
}

$strSaveFrameSize = '100%,*';

$_SESSION["PABITAM_UserID"];
require_once('appuser.inc.php');

$AppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
//RV Mayo2016: Agregar variable de Password que se usará para abrir la opción de Reports
//$strUserPassword = BITAMAppUser::getUserPassword($theRepository, $AppUser->CLA_USUARIO);
//@JAPR 2016-06-30: Corregido un bug, el password estaba visible desencriptado (#VRNM0H)
$strUserPassword = BITAMEncode($AppUser->Password);
//@JAPR 2016-07-06: Agregado el número de versión al header del portal (#Z80I0W)
$streFormsVersion = getMDVersion();
//DAbdo solicitó que la versión desplegada tuviera el formato: V.vsp
//donde asumiendo una versión grabada en formato Artus como 6.02001 (versión mayor.versión menor en 2 dígitos + revisón en 3 dígitos): 
//V == Versión mayor (4, 5, 6, etc.)
//v == Versión menor en 1 dígito (01 => 1, 02 => 2, etc.)
//sp == Revisión en 2 dígitos (000 => 00, 001 => 01, etc.)
$intPos = stripos($streFormsVersion, ".");
if ($intPos !== false) {
	$streFormsVersionTemp = substr($streFormsVersion, 0, $intPos+1);
	$streFormsVersion = substr($streFormsVersion, $intPos+1);
	$arrDigits = str_split($streFormsVersion, 1);
	$intSubVersionLength = count($arrDigits);
	switch ($intSubVersionLength) {
		case 1:
			$streFormsVersionTemp .= $arrDigits[0];
			break;
		case 2:
			$streFormsVersionTemp .= $arrDigits[1];
			break;
		default:
			$streFormsVersionTemp .= $arrDigits[1];
			if ($intSubVersionLength >= 5) {
				if ($arrDigits[2] != '0') {
					$streFormsVersionTemp .= $arrDigits[2];
				}
				$streFormsVersionTemp .= $arrDigits[3];
				$streFormsVersionTemp .= $arrDigits[4];
			}
			else {
				$streFormsVersionTemp .= $arrDigits[2];
				$streFormsVersionTemp .= $arrDigits[3];
			}
			break;
	}
	$streFormsVersion = $streFormsVersionTemp;
}
//@JAPR
$UserMail=$AppUser->Email;

//RV Mayo2016: Agregar variable de ruta de artus para utilizarla en la opción Reports
//GCRUZ 2015-09-23 Obtener datos de artus del repositorio
require_once("eSurveyServiceMod.inc.php");
$aBITAMConnection = connectToKPIRepository();
$aSQL = "SELECT KPIServer, ArtusPath FROM saas_databases WHERE repository = ".$aBITAMConnection->ADOConnection->Quote($_SESSION["PABITAM_RepositoryName"]);
$rsArtusPath = $aBITAMConnection->ADOConnection->Execute($aSQL);
$strArtusPath = 'https://';
if ($rsArtusPath && $rsArtusPath->_numOfRows != 0)
{
	$strArtusPath .= $rsArtusPath->fields['KPIServer'].'/'.$rsArtusPath->fields['ArtusPath'].'/';
}

if (array_key_exists("menuType", $_GET))
{
	$menuType = (int) $_GET["menuType"];
}
else 
{
	$menuType = 0;
}
$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
$strScriptPath = $path_parts["dirname"];
$fromArtusLinker = 0;
if (isset($_GET["fromArtusLinker"])) {
	$fromArtusLinker = 1;
}
if (isset($_GET["fromArtusLinker"])) {
	$fromArtusLinker = 1;
}
require_once("eFormsAgentConfig.inc.php");
global $blnIsAgentEnabled;
//21Ene2013: Validar que si no existe la tabla SI_SV_SurveyTasks no se muestre el menú de Tasks
$displayTasks = true;
$sqlTasks = "SELECT SurveyTaskID FROM SI_SV_SurveyTasks";
$aRSTasks = $theRepository->DataADOConnection->Execute($sqlTasks);
if ($aRSTasks === false)
{
	//La tabla no existe, por lo tanto no se despliega la opción Tasks
	$displayTasks = false;
}
$displayTasks = true;
//@JAPR 2014-04-01: Agregada la ventana para permitir enviar mensajes vía Push
$displayPush = false;
if (getMDVersion() >= esvPasswordLangAndRedesign) {
	$sql = "SELECT COUNT(*) AS Regs FROM SI_SV_REGID";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$displayPush = ((int) @$aRS->fields["regs"]) > 0;
	}
}
global $gbIsGeoControl;
?>
<html>
<head>
	<title>KPIOnline Forms -  <?= $UserMail ?> </title>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '281867902146528');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=281867902146528&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

	<meta http-equiv="x-ua-compatible" content="IE=10" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href='favicon.ico' rel='shortcut icon' type='image/x-icon'/>
	<link href='favicon.ico' rel='icon' type='image/x-icon'/>
	<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
	<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
	<script src="js/codebase/dhtmlxFmtd.js"></script>
	<link type="text/css" href="loadexcel/css/font-awesome.min.css" rel="stylesheet"/>
	<script type="text/javascript" src="js/json2.min.js"></script>
	<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
	<style>
	/* it's important to set width/height to 100% for full-screen init */
	html, body {
		width: 100%;
		height: 100%;
		margin: 0px;
		overflow: hidden;
	}
	.dhx_dataview .dhx_dataview_default_item, .dhx_dataview .dhx_dataview_default_item_selected {
		  border-right: 0px solid #cccccc;
	}
	.dhx_dataview {
		overflow-y:auto !important;
	}
	div#fondo{
		position:absolute;
		top:0px;
		left:0px;
		width:100%;
		height:100%;
		background-color: #000;
		filter:alpha(opacity=60);
		-moz-opacity: 0.6;
		opacity: 0.6;
		z-index: 10;
		text-align: center;
		color: #825824;
	}
	</style>
	<script language="JavaScript">
		var menuType = <?=$menuType?>;
		var fromArtusLinker = <?=$fromArtusLinker?>;
		var UserRole = <?=(int) @$_SESSION["PABITAM_UserRole"]?>;
		var eFormsMDVersion = <?=getMDVersion()?>;
		var esvSurveyMenu = <?=esvSurveyMenu?>;
		var esvAdminWYSIWYG = <?=esvAdminWYSIWYG?>;
		var esvAgendaScheduler = <?=esvAgendaScheduler?>;
		var esveFormsAgent = <?=esveFormsAgent?>;
		var blnIsAgentEnabled = <?=(int) ($blnIsAgentEnabled && $displayTasks)?>;
		var gbIsGeoControl = <?=(int) $gbIsGeoControl?>;
		var displayPush = <?=(int) $displayPush?>;
		var objFormsLayoutMenu = undefined;
		var dhxMenu = "a";
		var dhxList = "b";
		var objMainLayout = undefined;
		var dhxBanner = "a";
		var dhxBody = "b";
		var objDataViewB;

		var isGeoControl = 0;
<?
if ($gbIsGeoControl)
{
?>
		isGeoControl = 1;
<?
}
?>	
		function onLoad() {
			/*2015-06-03@JRPP Añadimos un layout para colocar la imagen de header de nuestro portal */
			//@JAPR 2016-03-17: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize
			objMainLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"2E"
			});
			objMainLayout.setSeparatorSize(0, 0);
			objMainLayout.cells(dhxBanner).hideHeader();
			objMainLayout.cells(dhxBody).hideHeader();
			objMainLayout.cells(dhxBanner).fixSize(false, true);
			objMainLayout.cells(dhxBanner).setHeight(55);

			var strHTMLLinks = "";
			if (isGeoControl)
			{
				strHTMLLinks = "";
			}
			else
			{
				//strHTMLLinks = "<a href=\"http://www.kpiforms.com/chat/chat.php?a=5b488\" target=\"_blank\"><img src=\"http://www.kpiforms.com/chat/image.php?a=3f628&amp;id=5&amp;type=inlay\" align=\"left\" title=\"<?=translate("Live Chat Support")?>\"></a>"
				strHTMLLinks = "<img style=\"cursor:pointer;\" onclick=\"window.open('http://www.kpiforms.com/chat/chat.php?a=5b488', '', 'width=200,height=100')\" src=\"http://www.kpiforms.com/chat/image.php?a=3f628&amp;id=5&amp;type=inlay\" align=\"left\" title=\"<?=translate("Live Chat Support")?>\">";
			}
			
			objMainLayout.cells(dhxBanner).attachHTMLString(
				"<div id=\"divBanner\" style=\"overflow:hidden;width:100%;height:55px;background-image:url(images/admin/bannerForms.jpg);/*background-size:100% 100%;*/background-repeat:repeat-x;background-position:left top;\">"+
					"<img src=\"images/menubars2.png\" style=\"position:absolute;left:10px;top:18px;cursor:pointer;\" onclick=\"showHideMenu()\">"+
					"<img src=\"images/admin/kpiformsLogo.png\" style=\"position:absolute;left:40px;top:0px;cursor:pointer;\" onclick=\"doExecuteURL('Portal');\">"+
					"<div class=\"divInfo\" style=\"padding-top:0%\">"+
						"<span id=\"displayLoc\"></span>"+
						"<br>"+
						"<span id=\"currentUser\"><?=$UserMail?></span>"+
						"<span id=\"currentVersion\" title=\"<?=translate("Version")?>\">(<?=$streFormsVersion?>)</span>"+
						"<br>"+
						strHTMLLinks+
						"<span id=\"logOut\" onclick=\"doExecuteURL('LogOut')\"><?=translate("Logout")?></span>"+
					"</div>"+
				"</div>"
			);
			//@JAPR
			
			//@JAPR 2016-03-17: Modificado para que todos los componentes residan en layouts de DHTMLX y así no existan problemas de resize
			//Cambiada la manera de generar el Layout
			objFormsLayoutMenu = objMainLayout.cells(dhxBody).attachLayout({pattern: "2U"});
			/*objFormsLayoutMenu = new dhtmlXLayoutObject({
				parent: "divBody",
				pattern: "2U"
			});*/
			//@JAPR
			
			objFormsLayoutMenu.attachEvent("onContentLoaded", function(id) {
				objFormsLayoutMenu.cells(id).progressOff();
			});
			
			objFormsLayoutMenu.cells(dhxMenu).setWidth("105");
			objFormsLayoutMenu.cells(dhxMenu).fixSize(true, true);
			objFormsLayoutMenu.cells(dhxMenu).setText("Bitam E-Survey");
			objFormsLayoutMenu.cells(dhxMenu).hideHeader();
			objFormsLayoutMenu.cells(dhxMenu).hideArrow();
			objFormsLayoutMenu.cells(dhxList).setText("<?=translate("Surveys")?>");
			objFormsLayoutMenu.cells(dhxList).hideHeader();
			objFormsLayoutMenu.cells(dhxMenu).collapse();
			
			generateDataView();
		}
		//RV Nov2016: #0EVPBD La variable se saca de la función para que esté global y poderla modificar desde settingsvariableExt.inc.php
		//cuando se edita su valor desde la ventana de configuraciones, ya que si no se hace era necesario refrescar el frame de menus para que tomara
		//el nuevo valor
		var defLinksDisplayMode;
		function generateDataView(loadMainFrame) {
			if (loadMainFrame == undefined) {
				loadMainFrame = true;
			}
			
			objFormsLayoutMenu.cells(dhxMenu).detachObject(true);
			objDataViewB = objFormsLayoutMenu.cells(dhxMenu).attachDataView({
				container:"menuOptionsCell",
				type:{
					template:"<img style='height:48; width:48; padding-left: 10px;' src=#picture#><div style=' font-size:10px;font-weight:bold; text-align: center; width: 70px;' data-tooltip='#tooltip#'>#description#</div>",
					width:"100%; widthNA:100",
					border:0,
					height:68
				}
			});
			
	//JAPR 2015-09-03: Validado que un usuario que no es Administrador no tenga acceso a mas ventanas excepto la de reportes
<?	
	//@JAPR 2015-09-03: Validado que sólo los administradores puedan acceder a ciertas opciones de eForms
	//@JAPR 2016-08-23: Corregido el registro de seguridad por usuario, no estaba considerando al repositorio así que mismos usuarios de diferentes BDs habrían tenido conflicto (#KY40NF)
	$arrSecurity = (array) @$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$_SESSION["PABITAM_UserID"]];
	if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
?>

<?
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otySurvey][asecShow]) || $arrSecurity[otySurvey][asecShow]) {
?>
			objDataViewB.add({id:"Forms",description:"<?=translate("Surveys")?>", picture: "images/admin/forms_64x64.png"});
<?
		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyDataSource][asecShow]) || $arrSecurity[otyDataSource][asecShow]) {
?>
			objDataViewB.add({id:"Catalogos",description:"<?=translate("Catalogs")?>", picture: "images/admin/catalogs_64x64.png"});
<?
		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyAgenda][asecShow]) || $arrSecurity[otyAgenda][asecShow]) {
?>
			objDataViewB.add({id:"Agendas",description:"<?=translate("Agendas")?>", picture: "images/admin/agenda_64x64.png"});
<?
		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyUserGroup][asecShow]) || $arrSecurity[otyUserGroup][asecShow]) {
?>
			objDataViewB.add({id:"User Groups",description:"<?=translate("Groups")?>", picture: "images/admin/groups_64x64.png"});
<?
		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyUser][asecShow]) || $arrSecurity[otyUser][asecShow]) {
?>
			objDataViewB.add({id:"Users",description:"<?=translate("Users")?>", picture: "images/admin/users_64x64.png"});
<?
		}
?>
			//objDataViewB.add({id:"Notifications",description:"<?=translate("Notifications")?>", picture: "images/admin/notifications_64x64.png"});
<?
	}
?>	

<?	
	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
	if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otySetting][asecShow]) || $arrSecurity[otySetting][asecShow]) {
?>
			objDataViewB.add({id:"Settings",description:"<?=translate("Settings")?>", picture: "images/admin/settings_64x64.png"});
<?
		}
?>		
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		//if (!isset($arrSecurity[otySyncLog][asecShow]) || $arrSecurity[otySyncLog][asecShow]) {
//?>
//			objDataViewB.add({id:"Tasks",description:"<?=translate("Sync log")?>", picture: "images/admin/log_64x64.png"});
//<?
//		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
//		if (!isset($arrSecurity[otyDataDestinationLog][asecShow]) || $arrSecurity[otyDataDestinationLog][asecShow]) {
//?>
			//EVEGA Validamos que la version sea la indicada para mostrar el log de dataDestinations
//			if (eFormsMDVersion>=6.00008) {
//				objDataViewB.add({id:"Destinations",description:"<?=translate("Datadestination log")?>", picture: "images/admin/data_destination.png"});
//			}
<?
	
?>
		
			//GCRUZ 2015-08-25. Menus
			//if (eFormsMDVersion>=6.00009) {
				//GCRUZ 2015-09-18. Ocultar del menu lateral, ahora se mostraría en la lista de formas
				//objDataViewB.add({id:"Menu",description:"<?=translate("Menús")?>", picture: "images/admin/taskgroup2.png"});
			//}
<?
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyPushMsg][asecShow]) || $arrSecurity[otyPushMsg][asecShow]) {
			//GCRUZ 2016-04-29: Agregada bitácora de mensajes push.
			if (getMDVersion() >= esvPushMsgLog)
			{
?>
				objDataViewB.add({id:"PushMsgLog",description:"<?=translate("Push Messages")?>", picture: "images/admin/bitacoranotificaciones.png"});
			//JAPR
<?
			}
		}
	}
?>

<?
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		//@ears
		if (!isset($arrSecurity[otyReport][asecShow]) || $arrSecurity[otyReport][asecShow]) {
?>
			objDataViewB.add({id:"Reports",description:"<?=translate("Forms Log")?>", picture: "images/admin/report_64x64.png"});
<?	
		}
?>

<?
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		//@ears
		if (!isset($arrSecurity[otySyncLog][asecShow]) || $arrSecurity[otySyncLog][asecShow]) {
?>
			objDataViewB.add({id:"Tasks",description:"<?=translate("Sync log")?>", picture: "images/admin/log_64x64.png"});
<?
		}
		
		//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
		if (!isset($arrSecurity[otyDataDestinationLog][asecShow]) || $arrSecurity[otyDataDestinationLog][asecShow]) {
?>
			//EVEGA Validamos que la version sea la indicada para mostrar el log de dataDestinations
			if (eFormsMDVersion>=6.00008) {
				objDataViewB.add({id:"Destinations",description:"<?=translate("Datadestination log")?>", picture: "images/admin/data_destination.png"});
			}
<?
		}
		
		//@JAPR 2016-11-04: Agregada la configuración de permisos en el Administrador (#KY40NF)
		//Habían agregado nuevas opciones al menú lateral pero no se habían validado correctamente según lo indicado en este issue
		if (!isset($arrSecurity[otyPortal][asecShow]) || $arrSecurity[otyPortal][asecShow]) {
?>
			objDataViewB.add({id:"Portal",description:"<?=translate("Portal")?>", picture: "images/admin/portal_64x64.png"});
<?
		}
		
		//@JAPR 2016-11-04: Agregada la configuración de permisos en el Administrador (#KY40NF)
		//Habían agregado nuevas opciones al menú lateral pero no se habían validado correctamente según lo indicado en este issue
		//@JAPR 2016-11-04: Validado que WebDesigner sólo funcione a partir de la versión donde fue implementado correctamente por parte de Artus (#IP53DM)
		//@JAPR 2016-11-04: Resultó que este enlace cargaba Artus Web y no Web Designer, así que a pesar de que el issue indicado generado para un cliente reportaba Web Designer, no
		//se necesitaba validar nada contra la versión de Artus Web porque no se carga Web Designer directo (#IP53DM)
		if ((!isset($arrSecurity[otyWebDesigner][asecShow]) || $arrSecurity[otyWebDesigner][asecShow])) {
			//@JAPR 2016-11-04: Modificado el título de la opción a petición de LRoux aunque quedaría inconsistente al equivalente en la ventana de Portal pedida por DAbdo (#IP53DM)
			//RV Mayo2016: Agregar una opción que al darle click abra la misma ventana que se abre cuando se da click en el encabezado "My Reports"
			//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)
			//Si no es la versión con soporte de Web Designer entonces sólo el permiso de Artus Desktop era válido, de lo contrario se solicitó que con cualquier permiso de Artus
			//fuera posible abrir el Web Designer aunque técnicamente hablando (confirmado por IRetta) sólo los que tuvieran permiso de Designer y Web lo podrían abrir
			if ((getWebVersion() < ARTUSWEB_WEBDESIGNER_VERSION && $AppUser->ArtusWebUserLicense > 0) || (
				$AppUser->ArtusWebUserLicense + $AppUser->ArtusAdminUserLicense + $AppUser->ArtusDesignerUserLicense) > 0) {
?>
			objDataViewB.add({id:"ArtusReports",description:"<?=translate(((getWebVersion() >= ARTUSWEB_WEBDESIGNER_VERSION)?"Artus":"Analytics"))?>", picture: "images/admin/<?if (getWebVersion() >= ARTUSWEB_WEBDESIGNER_VERSION) {?>webDesigner.png<?} else {?>dashboard.PNG<?}?>"});
<?
			}
		}
		//@JAPR
	
	if (getMDVersion() >= esvLinks) {
		require_once("link.inc.php");
		$links = BITAMLinkCollection::NewInstance($theRepository, null, 1);
		foreach($links as $aLink)
		{
			if($aLink->Image=="")
			{
				$linkImg = "images/admin/eformslink.PNG";
			}
			else
			{
				$linkImg = $aLink->Image;
			}
			$link = $aLink->Link;
			$link = TranslateGlobalVariables($theRepository, $link);
			if((string) @$_SERVER['HTTPS'] == 'on')
			{
				$link=str_ireplace('http://', 'https://', $link);
			}
			//@JAPR 2018-11-06: Corregido un bug con el escape de caracteres especiales (comilla doble entre otros) (#S0SRC1)
			$link = GetValidJScriptText($link, array("fullhtmlspecialchars" => 1));
?>			
			objDataViewB.add({id:"custom<?=$aLink->LinkID?>",description:"<?=GetValidJScriptText($aLink->LinkName, array("fullhtmlspecialchars" => 1))?>", picture: "<?=$linkImg?>", bitCustomURL:"<?=$link?>", DisplayMode:"<?=$aLink->DisplayMode?>"});
<?
			//@JAPR
		}
	}
?>	
	//RV 2016-04-15: Soportar configuración de modo de visualización de los enlaces
	defLinksDisplayMode = 1; //Frame
<?	
	if (getMDVersion() >= esvConfLinks) {
		$linkDispMode = BITAMSettingsVariable::NewInstanceWithName($theRepository, "LINKSDISPLAYMODE");
		//@JAPR 2016-04-22: Corregido un bug, faltaba esta validación de objeto existente
		if (!is_null($linkDispMode)) {
?>
		defLinksDisplayMode = <?=(int)$linkDispMode->SettingValue?>;
<?
		}
		//@JAPR
	}
?>
			
			objDataViewB.attachEvent("onItemClick", function (id, ev, html) {
				if(id.indexOf("custom")!=-1)
				{	
					var data = this.get(id);
					if(data.bitCustomURL)
					{
						if(data.bitCustomURL!="")
						{
							//RV 2016-04-15: Soportar configuración de modo de visualización de los enlaces
							var displayMode;
							displayMode=data.DisplayMode;
							if(displayMode==0)
							{
								displayMode = defLinksDisplayMode;
							}
							
							if(displayMode==1)
							{
								doExecuteURL(id, ev, html, data.bitCustomURL);
							}
							else
							{
								d = new Date();
								y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
								var aWindow = window.open(data.bitCustomURL, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
							}
						}
					}
				}
				else if (id=="ArtusReports")
				{
					try {
						<?//@JAPR 2016-06-30: Corregido un bug, el password estaba visible desencriptado (#VRNM0H)
						//@JAPR 2017-03-07: Agregado el enlace alternativo para Web Designer dependiendo de la versión de Artus Web instalada (#FVYYQS)?>
						//@JAPR 2017-03-07: Ajustada la apariencia de la ventana emergente donde se abrirán otros productos (#FVYYQS)
						//No hay manera de controlar que se abra una nueva ventana vs nuevo tab, cuando se cambia el parámetro status=yes automáticamente abre nuevo tab, este comportamiento
						//se configura en el browser, así que para funcionalidad completa de la ventana tendrá que ser nuevo tab
						var strURL = "<?=$strArtusPath?>check.php?repository=<?=(string) @$_SESSION["PABITAM_RepositoryName"]?>&username=<?=$AppUser->ArtusUser?>&pwdencripted=1&password=<?=$strUserPassword?><?if (getWebVersion() >= ARTUSWEB_WEBDESIGNER_VERSION) {?>&bWD=1<?} else {?>&params=NOSCRIPT<?}?>";
						d = new Date();
						y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
						var aWindow = window.open(strURL, y, 'width=800,height=600,fullscreen=yes,titlebar=yes,location=yes,menubar=yes,toolbar=yes,status=yes,resizable=yes,scrollbars=yes',1);
					} catch(e) {
						alert(e);
					}
				}
				else
				{
					doExecuteURL(id, ev, html);
				}
			});
			
			if ((menuType === 0 || menuType === 1) && !fromArtusLinker) {
				/*Cargamos el portal*/
					if(loadMainFrame)
					{
						objDataViewB.select("Portal");
						doExecuteURL("Portal");
					}
			}
		}
		
		function showHideMenu() {
			if (objFormsLayoutMenu && objFormsLayoutMenu.cells(dhxMenu)) {
				if (objFormsLayoutMenu.cells(dhxMenu).isCollapsed()) {
					objFormsLayoutMenu.cells(dhxMenu).expand();
				}
				else {
					objFormsLayoutMenu.cells(dhxMenu).collapse();
				}
			}
		}
		
		/* Invoca a la URL correspondiente según la opción a la que se le hizo click. Si se recibe el parámetro sCustomURL, entonces se utilizará la URL
		recibida en ese parámetro para cargar la página especificada, aunque en ese caso es irrelevante le id pues se asume que se está invocando desde el
		propio frame de la celda con el contenido para cambiarse a otra ventana
		*/
		function doExecuteURL(id, ev, html, sCustomURL) {
			if (!objFormsLayoutMenu || (!id && !sCustomURL)) {
				return;
			}
			
			//Si hay una URL personalizada, se le da preferencia a ella
			if (sCustomURL) {
				setTimeout(function() {
					objFormsLayoutMenu.cells(dhxList).progressOn();
					objFormsLayoutMenu.cells(dhxList).attachURL(sCustomURL);
				}, 100);
				return;
			}
			var textHeader = "";
			var strURL = "";
			switch (id) {
<?
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otySurvey][asecShow]) || $arrSecurity[otySurvey][asecShow]) {
?>
				case "Forms":
					strURL = "main.php?BITAM_SECTION=SurveyExtCollection";
					textHeader = "<?=translate("Surveys")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyMenu][asecShow]) || $arrSecurity[otyMenu][asecShow]) {
?>
				case "Menu":
					strURL = "main.php?BITAM_SECTION=SurveyMenuExtCollection";
					textHeader = "<?=translate("My Forms")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyDataSource][asecShow]) || $arrSecurity[otyDataSource][asecShow]) {
?>
				case "Catalogos":
					strURL = "main.php?BITAM_SECTION=DataSourceCollection";
					textHeader = "<?=translate("My Catalogs")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyAgenda][asecShow]) || $arrSecurity[otyAgenda][asecShow]) {
?>
				case "Agendas":
					strURL = "main.php?BITAM_SECTION=AgendasCollection";
					textHeader = "<?=translate("Agendas")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyUserGroup][asecShow]) || $arrSecurity[otyUserGroup][asecShow]) {
?>
				case "User Groups":
					strURL = "main.php?BITAM_SECTION=UserGroupExtCollection";
					textHeader = "<?=translate("Groups")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyUser][asecShow]) || $arrSecurity[otyUser][asecShow]) {
?>
				case "Users":
					strURL = "main.php?BITAM_SECTION=AppUserExtCollection";
					textHeader = "<?=translate("Users")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyReport][asecShow]) || $arrSecurity[otyReport][asecShow]) {
?>
				case "Reports":
					strURL = "main.php?BITAM_SECTION=ReportCollection&TypeID=1";
					textHeader = "<?=translate("Forms Log")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otySetting][asecShow]) || $arrSecurity[otySetting][asecShow]) {
?>
				case "Settings":
					strURL = "main.php?BITAM_PAGE=SettingsVariableExt";
					textHeader = "<?=translate("Settings")?>";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otySyncLog][asecShow]) || $arrSecurity[otySyncLog][asecShow]) {
?>
				case "Tasks":
					//strURL = "main.php?BITAM_SECTION=SurveyTaskCollection";
					strURL = "main.php?BITAM_SECTION=ReportCollection&TypeID=0";
					textHeader = "<?=translate("Sync log")?>";
					/*2015-06-15@JRPP Se omite la URL debido a que momentaneamente esta accion nos lleva a la construccion de la pagina antigua*/
					//strURL = "";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyDataDestinationLog][asecShow]) || $arrSecurity[otyDataDestinationLog][asecShow]) {
?>
				case "Destinations":
					//EVEGA
					strURL = "main.php?BITAM_SECTION=ReportCollection&TypeID=2";
					textHeader = "<?=translate("Datadestination log")?>";
					/*2015-06-15@JRPP Se omite la URL debido a que momentaneamente esta accion nos lleva a la construccion de la pagina antigua*/
					//strURL = "";
					break;
<?
			}
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otyPushMsg][asecShow]) || $arrSecurity[otyPushMsg][asecShow]) {
?>
				//GCRUZ 2016-04-29: Agregada bitácora de mensajes push.
				case "PushMsgLog":
					strURL = "main.php?BITAM_SECTION=PushMsgCollection";
					textHeader = "<?=translate("Push Messages")?>";
					break;
<?
			}
?>
				case "Portal":
					strURL = "build_portal.php";
					textHeader = "<?=translate("Portal")?>";
					if (objFormsLayoutMenu && objFormsLayoutMenu.cells(dhxMenu)) {
						if (objFormsLayoutMenu.cells(dhxMenu).isCollapsed()) {
							objFormsLayoutMenu.cells(dhxMenu).expand();
						}
						//OMMC 2015-10-15: Una vez que se despliega el menu al cargar el portal, se hace un barrido de los elementos del menú, si hay alguno que fue seleccionado previamente se quita el css que lo hacía ver seleccionado (#26RFN4).
						var mySelectedIndex = objFormsLayoutMenu.cells(dhxMenu).cell.innerHTML;
						if(mySelectedIndex != "" && $(mySelectedIndex).find("div .dhx_dataview_default_item_selected").length > 0){
							$("div .dhx_dataview_item").removeClass("dhx_dataview_default_item_selected");
						}
					}
					break;
				//JAPR 2015-11-24: Agregada la liga de LogOut del producto (#20FK27)
				case "LogOut":
					strURL = "loadKPILogin.php";
					textHeader = "<?=translate("Logout")?>";
					break;
<?
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otySubscription][asecShow]) || $arrSecurity[otySubscription][asecShow]) {
?>
				case "Suscriptions":
					strURL = "main.php?BITAM_PAGE=manageSuscriptions";
					textHeader = "<?=translate("Manage Suscriptions")?>";
					break;
<?
			}
?>
			}
			
			if (textHeader) {
				$("#displayLoc").html(textHeader);
			}
			
			if (strURL) {
				objFormsLayoutMenu.cells(dhxList).progressOn();
				objFormsLayoutMenu.cells(dhxList).attachURL(strURL);
			}
		}
		
		function refreshDataViewMenu(objLink) {
			console.log("refreshDataViewMenu:"+objLink.LinkID);
			//debugger;
					
			var objItem = objDataViewB.get("custom"+objLink.LinkID);
			
			
			if(objItem==undefined)
			{
				if(objLink.Status==1)
				{
					//Si no existe el elemento en el dataview y el status es igual a 1 (visible) 
					//entonces se trata de un cambio de status de cero a 1 
					//Por lo tanto se agrega el elemento				
					var link = objLink.Link;
					link = link.replace(/@LOGIN/gi,"<?=$AppUser->ArtusUser?>");
					<?//@JAPR 2016-07-21: Corregida la encriptación del password con el método de BITAM?>
					link = link.replace(/@ENCRYPTEDPASSWORD/gi,"<?=urlencode(BITAMEncode($AppUser->Password))?>");
					<?
					if((string) @$_SERVER['HTTPS'] == 'on')
					{
					?>
					link = link.replace(/http:\/\//gi,'https://');
					<?
					}
					?>
					
					
					if(objLink.Image=="" || objLink.Image=="images/admin/emptyimg.png")
					{
						var linkImg = "images/admin/eformslink.PNG";
					}
					else
					{
						var linkImg = objLink.Image;
					}
					
					objDataViewB.add({id:"custom"+objLink.LinkID, description:objLink.LinkName, picture: linkImg, bitCustomURL:link});
					return;
				}
				else
				{
					//Si no existe el elemento en el dataview y el status es igual a Cero
					//entonces no se refresca el elemento en el dataview por que no está visible
					return;
				}
			}
			
			objItem.description=objLink.LinkName;
			
			if (objLink.Image == "" || objLink.Image == "images/admin/emptyimg.png") {
				var linkImg = "images/admin/eformslink.PNG";
			}
			else
			{
				var linkImg = objLink.Image;
			}
			
			objItem.picture=linkImg;
			
			var link = objLink.Link;
			link = link.replace(/@LOGIN/gi,"<?=$AppUser->ArtusUser?>");
			<?//@JAPR 2016-07-21: Corregida la encriptación del password con el método de BITAM?>
			link = link.replace(/@ENCRYPTEDPASSWORD/gi,"<?=urlencode(BITAMEncode($AppUser->Password))?>");
			<?
			if((string) @$_SERVER['HTTPS'] == 'on')
			{
			?>
			link = link.replace(/http:\/\//gi,'https://');
			<?
			}
			?>
			objItem.bitCustomURL=link;
			objItem.DisplayMode=objLink.DisplayMode;
			console.log("***objItem.description:"+objItem.description);
			console.log("***objItem.picture:"+objItem.picture);
			console.log("***objItem.bitCustomURL:"+objItem.bitCustomURL);
			console.log("***objItem.DisplayMode:"+objItem.DisplayMode);
			objDataViewB.refresh("custom"+objLink.LinkID);
			if(objLink.Status==0)
			{
				console.log("objItem status=0");
				objDataViewB.remove("custom"+objLink.LinkID);
			}
		}
	</script>
</head>
<body onload="onLoad()">
</body>
</html>