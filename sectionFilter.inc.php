<?php
//@JAPRDescontinuada: Para V6 ya no se utiliza esta clase, los filtros de las preguntas se graban directamente en question.inc.php y de ahí como BITAMSurveyFilter
require_once("repository.inc.php");
require_once("initialize.php");
require_once("survey.inc.php");
require_once("filtersv.inc.php");

class BITAMSectionFilter extends BITAMObject
{
	public $FilterID;						//ID único del filtro
	public $SectionID;						//ID de sección para la que aplica
	public $FilterName;
	public $FilterText;						//Texto SQL del filtro, a diferencia del mismo campo en SI_SV_CatFilter, este se generará como un filtro SQL tradicional, con variables para representar los campos de las formas de eBavel (ej. @eBFID15) y con posibilidad de usar variables del App
	public $FilterTextUsr;					//Text SQL del filtro, a diferencia de FilterText, este contiene nombres de variables entendibles por el usuario (ej. @AttrZone) en lugar de las variables internas con IDs (ej. @eBFID15, donde 15 es el id_fieldform)
	public $FilterLevels;					//No aplica, dejado por compatibilidad con la tabla, aunque internamente contendrá los IDs de campos de eBavel usados como variables @AttrName en el FilterText
	public $eBavelFormID;					//ID de la forma de eBavel asociada a la sección (este valor no se define en esta instancia sino que se hereda de la sección correspondiente)
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	public $SurveyID;						//ID de la encuesta de la sección a la que pertenece el filtro
	
	public $AttribFields;
	public $AttribNames;
	public $AttribIDs;
	public $AttribPositions;
	public $AttribMemberIDs;
	public $AttribMemberVarNames;
	public $NumAttribFields;
	
	function __construct($aRepository, $aSectionID)
	{
		BITAMObject::__construct($aRepository);
		$this->FilterID = -1;
		$this->SectionID = $aSectionID;
		$this->FilterName = "";
		$this->FilterText = "";
		$this->FilterTextUsr = "";
		$this->FilterLevels = "";
		$this->eBavelFormID = 0;
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$this->SurveyID = 0;
		
		$this->AttribFields = array();
		$this->AttribNames = array();
		$this->AttribIDs = array();
		$this->AttribPositions = array();
		$this->AttribMemberIDs = array();
		$this->AttribMemberVarNames = array();
		$this->NumAttribFields = 0;
		
		$objSection = BITAMSection::NewInstanceWithID($aRepository, $aSectionID);
		if (!is_null($objSection)) {
			$this->eBavelFormID = $objSection->SectionFormID;
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			$this->SurveyID = $objSection->SurveyID;
		}
	}
	
	static function NewInstance($aRepository, $aSectionID)
	{
		return new BITAMSectionFilter($aRepository, $aSectionID);
	}
	
	static function NewInstanceWithID($aRepository, $aFilterID)
	{
		$anInstance = null;
		
		if (((int) $aFilterID) <= 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$sql = "SELECT B.SurveyID, A.SectionID, A.FilterID, A.FilterName, A.FilterText, A.FilterLevels, B.SectionFormID 
			FROM SI_SV_SectionFilter A 
				INNER JOIN SI_SV_Section B ON A.SectionID = B.SectionID 
			WHERE A.FilterID = ".$aFilterID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter, SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSectionFilter::NewInstanceFromRS($aRepository, $aRS);
		}
		
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aSectionID = (int) @$aRS->fields["sectionid"];
		$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
		$anInstance->FilterID = (int) @$aRS->fields["filterid"];
		$anInstance->FilterName = (string) @$aRS->fields["filtername"];
		$anInstance->FilterText = (string) @$aRS->fields["filtertext"];
		$anInstance->FilterLevels = (string) @$aRS->fields["filterlevels"];
		
		$anInstance->addAttributeFields();
		return $anInstance;
	}
	
	function addAttributeFields($bNullFilters = false) {
		if ($this->eBavelFormID <= 0) {
			return;
		}
		
		$arreBavelFields = @GetEBavelFieldsForms($this->Repository, true, null, array($this->eBavelFormID));
		$numFields = count($arreBavelFields);
		$arrUsedVarNames = array();
		$this->FilterTextUsr = $this->FilterText;
		$i = 0;
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		foreach ($arreBavelFields as $inteBavelFieldID => $objeBavelField) {
			$fieldName = 'DSC_'.$objeBavelField['id'];
			$fieldNameSTR = $fieldName."STR";
			$this->AttribFields[$i] = $fieldName;
			$this->AttribIDs[$i] = $objeBavelField['id'];
			$this->AttribNames[$i] = $objeBavelField['label'];
			$this->AttribMemberIDs[$fieldName] = $inteBavelFieldID;
			$this->AttribPositions[$fieldName] = $i;
			$strFieldVarName = GetEBavelFieldVariable($objeBavelField);
			if (isset($arrUsedVarNames[$strFieldVarName])) {
				$strFieldVarName .= $inteBavelFieldID;
			}
			$arrUsedVarNames[$strFieldVarName] = $strFieldVarName;
			$arrUsedVarNamesByPos[$inteBavelFieldID] = $strFieldVarName;
			$this->AttribMemberVarNames[$inteBavelFieldID] = $strFieldVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$inteBavelFieldID] = '@eBFID'.$inteBavelFieldID;
			//$this->FilterTextUsr = str_ireplace('@eBFID'.$inteBavelFieldID, $strFieldVarName, $this->FilterTextUsr);
			
			//Agregado el parámetro $bNullFilters para inicializar las propiedades que representan los valores filtrados con un valor null en lugar de hacerlo
			//con cadena vacia
			if ($bNullFilters) {
				$this->$fieldName = null;
				$this->$fieldNameSTR = null;
			}
			else {
				$this->$fieldName = "sv_ALL_sv";
				$this->$fieldNameSTR = "All";
			}
			$i++;
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		natcasesort($arrVariableNames);
		$arrVariableNames = array_reverse($arrVariableNames, true);
		foreach ($arrVariableNames as $inteBavelFieldID => $strMemberVarName)	{
			$strFieldVarName = (string) @$arrUsedVarNamesByPos[$inteBavelFieldID];
			$this->FilterTextUsr = str_ireplace($strMemberVarName, $strFieldVarName, $this->FilterTextUsr);
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está editando, así que traducimos las variables a números
		$this->FilterTextUsr = $this->TranslateVariableQuestionIDsByNumbers($this->FilterTextUsr);
	    //@JAPR
		
		$this->NumAttribFields = $numFields;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("SectionID", $aHTTPRequest->GET))
		{
			$aSectionID = (int) $aHTTPRequest->GET["SectionID"];
		}
		else
		{
			$aSectionID = 0;
		}
		
		if (array_key_exists("FilterID", $aHTTPRequest->POST))
		{
			$aFilterID = $aHTTPRequest->POST["FilterID"];
			
			if (is_array($aFilterID))
			{
				$aCollection = BITAMSectionFilterCollection::NewInstance($aRepository, $aSectionID, $aFilterID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				
				$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
				$anInstance = $anInstance->get_Parent();
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			else
			{
				$anInstance = BITAMSectionFilter::NewInstanceWithID($aRepository, (int)$aFilterID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
				}
				
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		
		if (array_key_exists("FilterID", $aHTTPRequest->GET))
		{
			$aFilterID = $aHTTPRequest->GET["FilterID"];
			$anInstance = BITAMSectionFilter::NewInstanceWithID($aRepository, (int)$aFilterID);
			
			if (is_null($anInstance))
			{
				$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
			}
		}
		else
		{
			$anInstance = BITAMSectionFilter::NewInstance($aRepository, $aSectionID);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("FilterID", $anArray))
		{
			$this->FilterID = (int)$anArray["FilterID"];
		}
		
		if (array_key_exists("FilterName", $anArray))
		{
			$this->FilterName = rtrim($anArray["FilterName"]);
		}
		
		if (array_key_exists("FilterTextUsr", $anArray))
		{
			$this->FilterTextUsr = rtrim($anArray["FilterTextUsr"]);
		}
		
		return $this;
	}
	
	//Toma el valor del FilterTextUsr y cambia las variables de usuario por las variables internas con las que se debe grabar
	function encodeFilterText($aFilterTextUsr) {
		if ($this->eBavelFormID <= 0) {
			return '';
		}
		$strFilterText = $this->FilterTextUsr;
		
		$arreBavelFields = @GetEBavelFieldsForms($this->Repository, true, null, array($this->eBavelFormID));
		$arrUsedVarNames = array();
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
	    $arrVariableNames = array();
	    $arrUsedVarNamesByPos = array();
		foreach ($arreBavelFields as $inteBavelFieldID => $objeBavelField) {
			$strFieldVarName = GetEBavelFieldVariable($objeBavelField);
			if (isset($arrUsedVarNames[$strFieldVarName])) {
				$strFieldVarName .= $inteBavelFieldID;
			}
			$arrUsedVarNames[$strFieldVarName] = $strFieldVarName;
			$arrUsedVarNamesByPos[$inteBavelFieldID] = $strFieldVarName;
		    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
		    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
		    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		    //Ahora el reemplazo se hará al final, ya que se hubiesen ordenado los nombres de variables en forma natural y descendente
		    $arrVariableNames[$inteBavelFieldID] = '@eBFID'.$inteBavelFieldID;
			//$strFilterText = str_ireplace($strFieldVarName, '@eBFID'.$inteBavelFieldID, $strFilterText);
		}
		
	    //@JAPR 2014-08-28: Corregido un bug, si los nombres iniciaban con el mismo prefijo, el primero siempre dañaba a los demás porque al primero
	    //no se le concatenaba la posición. Ahora primero generará un array de nombres que posteriormente ordenará de forma natural e iniciará el
	    //reemplazo desde el mayor al menor para reducir los casos de posibles errores (sólo quedarían los problemas de ordenamiento de la función)
		//Ahora recorre a la inversa el array de nombres de variables pero después de haberlo ordenado ascendentemente (manteniendo los Keys)
		//En este caso el ordenamiento no es por el array de variables sino por el de nombres de atributos, ya que se ordena por quien se busca
		//para que el reemplazo no deje caracteres adicionales que alteren la expresión grabada
		natcasesort($arrUsedVarNamesByPos);
		$arrUsedVarNamesByPos = array_reverse($arrUsedVarNamesByPos, true);
		foreach ($arrUsedVarNamesByPos as $inteBavelFieldID => $strFieldVarName)	{
			$strMemberVarName = (string) @$arrVariableNames[$inteBavelFieldID];
			$strFilterText = str_ireplace($strFieldVarName, $strMemberVarName, $strFilterText);
		}
	    //@JAPR
		
		return $strFilterText;
	}
	
	//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, esvExtendedModifInfo, otySectionFilter, $this->FilterID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		//@JAPR 2014-09-30: Corregido un bug, este objeto a la fecha de implementación no tiene campos de auditoría como LastModAdminVersion, así que
		//se asumirá que siempre que grabe lo hará mediante IDs, por lo que cuando despliegue la configuración debe traducir a números de pregunta
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, esvExtendedModifInfo, $bServerVariables);
	}
	//@JAPR
	
	function save()
	{
	 	if ($this->isNewObject())
		{
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(FilterID)", "0")." + 1 AS FilterID".
						" FROM SI_SV_SectionFilter";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->FilterID = (int)$aRS->fields["filterid"];
			$sql = "INSERT INTO SI_SV_SectionFilter (".
						"SectionID".
						",FilterID".
						",FilterName".
						",FilterText".
						",FilterLevels".
			            ") VALUES (".
						$this->SectionID.
						",".$this->FilterID.
						",".$this->Repository->DataADOConnection->Quote($this->FilterName).
						",".$this->Repository->DataADOConnection->Quote($this->FilterText).
						",".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
						")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		else
		{
			$this->FilterText = $this->encodeFilterText($this->FilterTextUsr);
			//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->FilterText = $this->TranslateVariableQuestionNumbersByIDs($this->FilterText, optyFilterText);
			//@JAPR
			$sql = "UPDATE SI_SV_SectionFilter SET ".
					" FilterName = ".$this->Repository->DataADOConnection->Quote($this->FilterName).
					", FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					", FilterLevels = ".$this->Repository->DataADOConnection->Quote($this->FilterLevels).
					" WHERE FilterID = ".$this->FilterID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
        //@JAPR 2014-10-03: Corregido un bug, no estaba actualizando la versión de la definición al grabar
    	BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
        //@JAPR
		return $this;
	}
	
	function remove()
	{		
		$sql = "DELETE FROM SI_SV_SectionFilter WHERE FilterID = ".$this->FilterID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->FilterID <= 0);
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Filter");
		}
		else
		{
			return $this->FilterName;
		}
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SectionFilter&SectionID=".$this->SectionID;
		}
		else
		{
			return "BITAM_PAGE=SectionFilter&SectionID=".$this->SectionID."&FilterID=".$this->FilterID;
		}
	}
	
	function get_Parent()
	{
		return BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return "FilterID";
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		//Campos para apoyo en la edición del FilterSQL
		if ($this->eBavelFormID > 0 && $this->NumAttribFields > 0) {
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "FilterTextUsr";
			$aField->Title = translate("Filter");
			$aField->Type = "LargeString";
			$aField->Size = 5000;
			$myFields[$aField->Name] = $aField;
			
			//Genera la combo de campos de eBavel, agrega dinámicamente la propiedad con un valor de 0 para que no seleccione a alguno en particular
			$this->eBavelFieldID = 0;
			$arreBavelFields = array();
			foreach ($this->AttribMemberIDs as $fieldName => $inteBavelFieldID) {
				if ($inteBavelFieldID > 0) {
					$intIndex = (int) @$this->AttribPositions[$fieldName];
					$strFieldLabel = (string) @$this->AttribNames[$intIndex];
					$arreBavelFields[$inteBavelFieldID] = $strFieldLabel;
				}
			}
			
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "eBavelFieldID";
			$aField->Title = translate("Fields");
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			$aField->Options = $arreBavelFields;
			$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
				"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
				"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
				"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
				"onclick=\"this.style.color='#d5d5bc';addAttrName();\">";
			$myFields[$aField->Name] = $aField;
			
			//Genera las combos con los valores de cada atributo
			$arrDefaultValue = array("sv_ALL_sv" => "All");
			for ($i=0; $i < $this->NumAttribFields; $i++)
			{
				$fieldName = $this->AttribFields[$i];
				$labelText = $this->AttribNames[$i];
				
				$aField = BITAMFormField::NewFormField();
				$aField->Name = $fieldName;
				$aField->Title = $labelText;
				$aField->Type = "Object";
				$aField->VisualComponent = "Combobox";
				$aField->Options = $arrDefaultValue;
				$aField->AfterMessage = "<input type=\"button\" value=\"".translate("Add")."\" ".
					"style=\"width:100px; border:0px; cursor:pointer; color:rgb(213, 213, 188); ".
					"background-image:url(btngenerar.gif); background-size: 100% 100%; ".
					"background-repeat: no-repeat;\" onmouseover=\"this.style.color='#98fb98'\" onmouseout=\"this.style.color='#d5d5bc'\" ".
					"onclick=\"this.style.color='#d5d5bc';addAttrValue('{$fieldName}');\">";
				$myFields[$aField->Name] = $aField;
			}
		}
		
		return $myFields;
	}
	
	function generateBeforeFormCode($aUser)
	{
		if ($this->eBavelFormID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	<script language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
		var filterAttribIDs = new Array;
		var filterAttribNames = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMembersPos = new Array;
		var filterAttribMembersVars = new Array;
		var filterAttribMemberFields = new Array;
		var filterAttribValGot = new Array;
<?
		foreach ($this->AttribIDs as $position => $attribID)
		{
			$attribName = $this->AttribFields[$position];
			$attribMemberID = $this->AttribMemberIDs[$attribName];
			$attribVarName = $this->AttribMemberVarNames[$attribMemberID];
?>
		filterAttribIDs[<?=$position?>]='<?=$attribID?>';
		filterAttribNames[<?=$position?>]='<?=$attribName?>';
		filterAttribPos['<?=$attribName?>']=<?=$position?>;
		filterAttribMembers['<?=$attribName?>']=<?=$attribMemberID?>;
		filterAttribMembersPos['<?=$position?>']=<?=$attribMemberID?>;
		filterAttribMembersVars[<?=$attribMemberID?>]='<?=$attribVarName?>';
		filterAttribMemberFields[<?=$attribMemberID?>]='<?=$attribName?>';
		filterAttribValGot[<?=$position?>]=0;
<?
		}
?>
		var numFilterAttributes = <?=$this->NumAttribFields?>;
		
<?
		if(count($this->AttribFields)>0)
		{
?>
		function refreshAttributes(attribName, event)
		{
			var event = (window.event) ? window.event : event;
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			var changedMemberID = filterAttribMembersPos[thisPosition -1];
			if (changedMemberID === undefined) {
				changedMemberID = -1;
			}
			
			if (filterAttribValGot[thisPosition]) {
				return;
			}
			
			var xmlObj = getXMLObject();
			if (xmlObj == null)
			{
				alert("XML Object not supported");
				return(new Array);
			}
			
		 	xmlObj.open("POST", "geteBavelFieldValues.php?noFilters=1&eBavelFormID=<?=$this->eBavelFormID?>&eBavelFieldID="+filterAttribMembers[attribName], false);
		 	xmlObj.send(null);
		 	strResult = Trim(unescape(xmlObj.responseText));
			
		 	arrayAttrib = strResult.split("_SV_AT_SV_");
		 	var numAttrib = arrayAttrib.length;
		 	var refreshAttrib = new Array();
		 	
		 	for (var i=0; i<numAttrib; i++)
		 	{
		 		if(arrayAttrib[i]!='')
		 		{
			 		var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
			 		var membID = atribInfo[0];
			 		var attribValues = new Array();
			 		if (atribInfo[1] != '') {
			 			attribValues = atribInfo[1].split("_SV_VAL_SV_");
			 		}
			 		var numAtVal = attribValues.length;
					
			 		var obj = document.getElementsByName(filterAttribMemberFields[membID])[0];
			 		
			 		obj.length = 0;
			 		 
					var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
					obj.options[obj.options.length] = newOption;
					
			 		for(j=0; j<numAtVal; j++)
			 		{
						var newOption = new Option(attribValues[j], attribValues[j]);
						obj.options[obj.options.length] = newOption;
			 		}
			 		
			 		refreshAttrib[i]=membID;
		 		}
		 	}
		 	filterAttribValGot[thisPosition] = 1;
		}
		
		function addAttrValue(attribName) {
			var thisPosition = filterAttribPos[attribName];
			if (thisPosition === undefined) {
				return;
			}
			
			var objSelect = document.getElementsByName(attribName);
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			if (!filterAttribValGot[thisPosition]) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == 'sv_ALL_sv') {
				return;
			}
			strSelectedVal = "'" + strSelectedVal + "'";

			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strSelectedVal;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strSelectedVal + strText.substr(intSelEnd);
			}
		}
		
		function addAttrName() {
			var objSelect = document.getElementsByName("eBavelFieldID");
			if (!objSelect || !objSelect.length) {
				return;
			}
			
			var strSelectedVal = objSelect[0].value;
			if (strSelectedVal == '0') {
				return;
			}
			var strAttribVar = filterAttribMembersVars[strSelectedVal];
			if (!strAttribVar) {
				return;
			}
			
			var objFilterText = document.getElementsByName("FilterTextUsr");
			if (!objFilterText || !objFilterText.length) {
				return;
			}
			objFilterText = objFilterText[0];
			
			var intSelStart = objFilterText.selectionStart;
			var intSelEnd = objFilterText.selectionEnd;
			var strText = objFilterText.value;
			if (strText === undefined || Trim(strText) == null || Trim(strText) == '') {
				objFilterText.value = strAttribVar;
				return;
			}
			if (intSelStart == intSelEnd) {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelStart);
			}
			else {
				objFilterText.value = strText.substr(0, intSelStart) + strAttribVar + strText.substr(intSelEnd);
			}
		}
<?
		}
?>
	</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
		if ($this->eBavelFormID <= 0 || $this->NumAttribFields == 0) {
			return;
		}
?>
	 	<script language="JavaScript">
<?
		if($this->NumAttribFields>0)
		{	
			foreach($this->AttribFields as $position => $attribName)
			{
?>
		if (document.getElementsByName('<?=$attribName?>')[0].addEventListener)
		{
			//document.getElementsByName('<?=$attribName?>')[0].addEventListener("click", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
			document.getElementsByName('<?=$attribName?>')[0].addEventListener("focus", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
		}
		else 
		{
		    //document.getElementsByName('<?=$attribName?>')[0].attachEvent("onclick", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		    document.getElementsByName('<?=$attribName?>')[0].attachEvent("onfocus", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
		}
<?
			}
		}
?>
	</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//Obtiene la definición en JSon de este objeto y sus dependencias
	function getJSonDefinition() {
		$arrDef = array();
		$arrDef['sectionID'] = $this->SectionID;
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		//Cuando se sobreescribe la propiedad en este método, quiere decir que se está descargando definición, así que traducimos las variables a números
		$strFilterText = $this->TranslateVariableQuestionIDsByNumbers($this->FilterText);
		$arrDef['filterText'] = $strFilterText;
		//@JAPR
		return $arrDef;
	}
	//@JAPR
}

class BITAMSectionFilterCollection extends BITAMCollection
{
	public $SectionID;
	
	function __construct($aRepository, $aSectionID)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->SectionID = $aSectionID;
	}
	
	static function NewInstance($aRepository, $aSectionID, $anArrayOfFilterIDs = null)
	{
		//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		if( is_null($anArrayOfFilterIDs) || count($anArrayOfFilterIDs)<=0 )
		{
			$anInstance =& BITAMGlobalFormsInstance::GetSectionFilterCollectionBySectionWithID($aSectionID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		//@JAPR
		
		$anInstance = new BITAMSectionFilterCollection($aRepository, $aSectionID);
		
		$filter = "";
		if (!is_null($anArrayOfFilterIDs))
		{
			switch (count($anArrayOfFilterIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND A.FilterID = ".((int)$anArrayOfFilterIDs[0]);
					break;
				default:
					foreach ($anArrayOfFilterIDs as $aFilterID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aFilterID; 
					}
					
					if ($filter != "")
					{
						$filter = " AND A.FilterID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$sql = "SELECT B.SurveyID, A.SectionID, A.FilterID, A.FilterName, A.FilterText, A.FilterLevels, B.SectionFormID 
			FROM SI_SV_SectionFilter A 
				INNER JOIN SI_SV_Section B ON A.SectionID = B.SectionID 
			WHERE A.SectionID = ".$aSectionID.$filter." 
			ORDER BY FilterID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter, SI_SV_Section ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMSectionFilter::NewInstanceFromRS($aRepository, $aRS);
			$aRS->MoveNext();
		}
		
		//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		if ( is_null($anArrayOfFilterIDs) || count($anArrayOfFilterIDs)<=0 )
		{
			BITAMGlobalFormsInstance::AddSectionFilterCollectionBySectionWithID($aSectionID, $anInstance);
		}
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2018-06-22: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Obtiene la colección de filtros de secciones pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección para 
	cada forma además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance pero con todos
	los parámetros en null excepto el SectionID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null)
	{
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrSectionFiltersBySectionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "WHERE C.SurveyID = ".((int) $anArrayOfSurveyIDs[0]);
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = "WHERE C.SurveyID IN (".$filter.")";
					}
					break;
			}
		}
		
		//@JAPR 2014-09-15: Agregados los datos extendidos de modificación
		$sql = "SELECT B.SurveyID, A.SectionID, A.FilterID, A.FilterName, A.FilterText, A.FilterLevels, B.SectionFormID 
			FROM SI_SV_SectionFilter A 
				INNER JOIN SI_SV_Section B ON A.SectionID = B.SectionID 
				INNER JOIN SI_SV_Survey C ON B.SurveyID = C.SurveyID 
			{$filter} 
			ORDER BY C.SurveyID, A.SectionID, A.FilterID";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SectionFilter, SI_SV_Section, SI_SV_Survey ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$objSectionFilter = BITAMSectionFilter::NewInstanceFromRS($anInstance->Repository, $aRS);
			$anInstance->Collection[] = $objSectionFilter;
			
			$intSectionID = $objSectionFilter->SectionID;
			if ( !isset($arrSectionFiltersBySectionID[$intSectionID]) ) {
				$arrSectionFiltersBySectionID[$intSectionID] = array();
			}
			$arrSectionFiltersBySectionID[$intSectionID][] = $objSectionFilter;
			
			$aRS->MoveNext();
		}

		//Finalmente procesa el array de secciones para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché (en este punto se
		//asume que este método se ejecutó posterior a la carga masiva de todos los padres, así que se usarán las colecciones que ya deberían estar cachadas)
		if (isset(BITAMGlobalFormsInstance::$arrAllSectionInstances)) {
			$arrAllSectionFilterCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllSectionInstances as $objSection) {
				$intSectionID = $objSection->SectionID;
				//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
				$objSectionFilterCollection[$intSectionID] = new $strCalledClassColl($aRepository, $intSectionID);
				//$objSectionFilterCollection = new $strCalledClassColl($aRepository, $intSectionID);
				if ( isset($arrSectionFiltersBySectionID[$intSectionID]) ) {
					$objSectionFilterCollection[$intSectionID]->Collection = $arrSectionFiltersBySectionID[$intSectionID];
				}
				BITAMGlobalFormsInstance::AddSectionFilterCollectionBySectionWithID($intSectionID, $objSectionFilterCollection[$intSectionID]);
			}
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("SectionID", $aHTTPRequest->GET))
		{
			$aSectionID = (int) $aHTTPRequest->GET["SectionID"];
		}
		else
		{
			$aSectionID = 0;
		}
		
		return BITAMSectionFilterCollection::NewInstance($aRepository, $aSectionID);
	}
	
	function get_Parent()
	{
		return BITAMSection::NewInstanceWithID($this->Repository, $this->SectionID);
	}
	
	function get_Title()
	{	
		return translate("Filters");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=Section&SectionID=".$this->SectionID;
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SectionFilter&SectionID=".$this->SectionID;
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FilterID';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
}
?>