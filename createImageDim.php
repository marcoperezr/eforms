<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");

global $gblEFormsNA;
foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Se genera instancia de coleccion de preguntas dado un SurveyID
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyInstance->SurveyID);
	
	foreach ($questionCollection->Collection as $questionInstance) 
	{
		if(!is_null($questionInstance->HasReqPhoto) && $questionInstance->HasReqPhoto>0)
		{
			if(is_null($questionInstance->ImgDimID) || $questionInstance->ImgDimID==0)
			{
				$strOriginalWD = getcwd();
		
				//Se obtiene el cla_usuario y nom_corto de Artus del usuario logueado asi como el nombre del repositorio 
				//para asignarlos a las variables de session que se ocupa en el model manager
				//@JAPR 2015-02-09: Agregado soporte para php 5.6
				//session_register("BITAM_UserID");
				//session_register("BITAM_UserName");
				//session_register("BITAM_RepositoryName");
				$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
				$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
				$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
		
				require_once("../model_manager/filescreator.inc.php");
				require_once("../model_manager/modeldimension.inc.php");
				require_once("../model_manager/modeldata.inc.php");
				
				$dimensionName = "IMG ".$questionInstance->AttributeName;
				$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $questionInstance->ModelID);
				$anInstanceModelDim->Dimension->Estatus = 1;
				$anInstanceModelDim->Dimension->ParentID = -1;
				$anInstanceModelDim->Dimension->IsIncremental = 0;
				$anInstanceModelDim->Dimension->Created_By = CREATED_BY_RAPID_WEB;
				$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
				$anInstanceModelDim->Dimension->UseKey = 0;
			
				//Verificamos si hay dimension existente con el nombre de AttributeName en caso contario
				//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
				$cla_descrip = $questionInstance->getDimClaDescripByDimName($dimensionName);
				
				if($cla_descrip!=-1)
				{
					$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
					$anInstanceModelDim->save();
					
					chdir($strOriginalWD);
	
					//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
					$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
					$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
					
					$dimTableName = $anInstanceModelDim->Dimension->TableName;
					$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
					
					//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
					$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					
					if (!$aRS || $aRS->EOF)
					{
						die( translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
			
					$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
					
					$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
					
					if ($aRepository->DataADOConnection->Execute($sql) === false)
					{
						die( translate("Error accessing")." ".$factTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
		
					//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
					$questionInstance->ImgDimID = $cla_descrip;
				}
				else 
				{
					$anInstanceModelDim->save();
					
					chdir($strOriginalWD);
					
					//Insertamos el valor de No Aplica en el catalogo
					$questionInstance->insertNoApplyValue($anInstanceModelDim);
	
					//Procedemos a actualizar el campo de dimension con el valor de no aplica en la tabla de hechos
					$dimFieldKey = $anInstanceModelDim->Dimension->TableName."KEY";
					$factTableName = $anInstanceModelDim->Dimension->oModel->nom_tabla;
					
					$dimTableName = $anInstanceModelDim->Dimension->TableName;
					$dimFieldDesc = $anInstanceModelDim->Dimension->FieldDescription;
					
					//Obtenemos el ID con el que se inserto el valor de No Aplica para una dimension determinada
					$sql = "SELECT ".$dimFieldKey." FROM ".$dimTableName." WHERE ".$dimFieldDesc." = ".$aRepository->DataADOConnection->Quote($gblEFormsNA);
					
					$aRS = $aRepository->DataADOConnection->Execute($sql);
					
					if (!$aRS || $aRS->EOF)
					{
						die( translate("Error accessing")." ".$dimTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
			
					$noApplyKey = $aRS->fields[strtolower($dimFieldKey)];
					
					$sql = "UPDATE ".$factTableName." SET ".$dimFieldKey." = ".$noApplyKey;
					
					if ($aRepository->DataADOConnection->Execute($sql) === false)
					{
						die( translate("Error accessing")." ".$factTableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
					}
			
					//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
					$questionInstance->ImgDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
				}
				
				$sql = "UPDATE SI_SV_Question SET ImgDimID = ".$questionInstance->ImgDimID." WHERE QuestionID = ".$questionInstance->QuestionID." AND SurveyID = ".$questionInstance->SurveyID;
				
				if ($aRepository->DataADOConnection->Execute($sql) === false)
				{
					die( translate("Error accessing")." SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
}
?>