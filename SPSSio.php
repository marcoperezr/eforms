﻿<?php
//@ears 2016-05-27: programación de exportación de datos a formato SPSS 
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("configSPSSio.php");
require_once("survey.inc.php");
require_once("question.inc.php");
require_once("catalog.inc.php");
require_once("dataSource.inc.php");
require_once("dataSourceMember.inc.php");
require_once("utils.inc.php");
require_once('csvExport/csvExport.inc.php');
require_once('user.inc.php');

define('sspsDefaultMCYesValue', 1);
define('sspsDefaultMCNoValue', 2);

define('spssStringVariable', 0);
define('spssNumericVariable', 1);

$fileName='';
global $sSpathfileError; //ruta de archivo de bitacora de errores
global $err;  //codigo de error de funciones spss
global $bSupportTypes; //tipos de pregunta que se pueden exportar @ears 2016-07-04
global $afile;		   //apoyo para generar la bitácora de errores	@ears 2016-07-07
$afile = array();

//wrapper
global $objSPSS;
$objSPSS = new COM("SPSSio.ModSPSSCls"); 	 
//wrapper


	$date_begin = (isset($_REQUEST['startDate']) && trim($_REQUEST['startDate']) != '')? date('Y-m-d', strtotime($_REQUEST['startDate'])).' 00:00:00': null;
	$date_end = (isset($_REQUEST['endDate']) && trim($_REQUEST['endDate']) != '')? date('Y-m-d', strtotime($_REQUEST['endDate'])).' 23:59:59': null;	
	$userIDs = (isset($_REQUEST['userID']) && trim($_REQUEST['userID']) != '')? explode(',', $_REQUEST['userID']): array();	
	$bOutputFile = (isset($_REQUEST['outputFile']) && trim($_REQUEST['outputFile']) != '')? intval($_REQUEST['outputFile']) : true;
	$dateTwoState = (isset($_REQUEST['dateTwoState']) && trim($_REQUEST['dateTwoState']) != '')? $_REQUEST['dateTwoState'] : '';
	$searchForm = (isset($_REQUEST['searchForm']) && trim($_REQUEST['searchForm']) != '')? $_REQUEST['searchForm'] : '';

	switch($dateTwoState)
	{
		case 'T':
			$date_begin = date('Y-m-d').' 00:00:00';
			$date_end = date('Y-m-d').' 23:59:59';
			break;
		case 'W':
			$weekDay = intval(date('w'));
			$date_begin = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
			$date_end = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
			break; 
		case 'M':
			$tDay = date('t');
			$date_begin = date('Y-m-').'01 00:00:00';
			$date_end = date('Y-m-').$tDay.' 23:59:59';
			break;
		case 'Y':
			$date_begin = date('Y-').'01-01 00:00:00';
			$date_end = date('Y-').'12-31 23:59:59';
			break;
	}

if (!isset($_REQUEST['SurveyID']) || $_REQUEST['SurveyID'] == '')
{	
	//@ears 2016-07-13 modificaciones para filtar formas
	//se presentó el caso de exportar por la captura de un solo día en donde solo figuraba una sola forma
	//no se consideraba dicha forma y marcaba error, con los siguientes cambios se busca que no necesariamente
	//se tenga que seleccionar la única forma que en ese momento se muestre en pantalla
	
	//Obtener datos standard de la forma
	$anADOConnection = $theRepository->DataADOConnection;
	$surveyID = array();

	if (!isset($_REQUEST['userID']) || $_REQUEST['userID'] == '') {
		$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 ORDER BY SurveyID";
		$aRS = $theRepository->DataADOConnection->Execute($sql);
		IF (!$aRS->EOF) {
			$surveyID[] = $aRS->fields['SurveyID'];						
		}	
	}	
	else	
		foreach($userIDs as $userid => $auser) {
			//El query se filtra por la combinación User y SurveyDate
			$sql="SELECT DISTINCT SurveyID FROM si_sv_surveylog WHERE SurveyDate BETWEEN " .$anADOConnection->Quote($date_begin) ." AND ".$anADOConnection->Quote($date_end)." AND STATUS = 1 AND deleted = 0 AND UserID = '" .$auser."' ORDER BY SurveyID";
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			IF (!$aRS->EOF) {
				$surveyID[] = $aRS->fields['SurveyID'];						
			}
			break;	
			/*
			while (!$aRS->EOF) {
					$surveyID[] = $aRS->fields['SurveyID'];
					$aRS->Movenext();
			}
			*/
		}			
		
		/*
		//Cargar TODAS las surveyIDs
		$loadedSurveys = BITAMSurvey::getSurveys($theRepository, true);
		$surveyID = array();
		foreach($loadedSurveys as $skey => $asurvey)
			$surveyID[] = $skey;		
		*/	
}
else 
	$surveyID = explode(',', $_REQUEST['SurveyID']);	

foreach ($surveyID as $aSurveyID)
{	
	/*	
	global $arrQuestionsStruct;
	global $objQuestionsCollByID;
	$arrQuestionsStruct = array();
	*/	
	//SPSSio($theRepository, $aSurveyID, $date_begin, $date_end, $userIDs);
	SPSSio($theRepository, $aSurveyID, $date_begin, $date_end, $userIDs, $searchForm);
	//break;
}

exit();

/*codigo spssio solo crea variables*/
function SPSSio($aRepository, $aSurveyID, $date_begin, $date_end, $userIDs, $searchForm)
{
	global $objSPSS;
	global $arrQuestionsStruct;
	global $objQuestionsCollByID;
	global $afile;
	global $sSpathfileError;

	$arrQuestionsStruct = array();
		
	$survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
	$sections = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
	$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
	if (is_null($questions)) {
		//die('could not load questions!');
		die();
	}
	
	if ($searchForm != '' && strpos(strtolower($survey->SurveyName), strtolower($searchForm)) === false)
		continue;	
		
	//para escritura de archivo .sav
	//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	$fileName = GeteFormsLogPath();
	//bitacora de error 
	$sSpathfileError = date('YmdHis').'_ReportSurvey_'.$aSurveyID.'_error_log.txt';
	//@JAPR
	$file = date('YmdHis').'_ReportSurvey_'.$survey->SurveyID.'.sav';	
	
	$bSupportTypes = false;
	foreach($questions as $objQuestion) {
		if (($objQuestion->QTypeID == qtpOpenNumeric) || ($objQuestion->QTypeID == qtpOpenAlpha) || ($objQuestion->QTypeID == qtpOpenString) || ($objQuestion->QTypeID == qtpSingle) || ($objQuestion->QTypeID == qtpMulti)) {
			$bSupportTypes = true;			
		}		
	}
	
	if (!$bSupportTypes){
		die();
	}		
	
	$data = EsurveyGetDataToExport($aRepository, $survey, $sections, $questions, $date_begin, $date_end, $userIDs, false, '', false);
	
	if (is_null($data) || !is_array($data)) {
		$line = "Error loading survey data". ','.date('Y-m-d H:i:s'); 
		array_push($afile, $line);
		ErrorSPSS($fileName);
		//die("Error loading survey data");
		die("(".__METHOD__.") ".translate("Error accessing"));
	}
	
	//PrintMultiArray($data);
	//die();
	
	$fH=0;
	//codificaciòn de archivo .sav
	$err=$objSPSS->myspssSetInterfaceEncoding(SPSS_ENCODING_UTF8);
	if ($err!=SPSS_OK) {
		$bErr=true;
		//die('Error Interface Encoding');											
		$line = "Error Interface Encoding". ','.date('Y-m-d H:i:s'); 
		array_push($afile, $line);
		ErrorSPSS($fileName);
		die("(".__METHOD__.") ".translate("Error accessing"));
		
	}

	//print "Carga de Datos";
	$bErr=false;
	/*
	$survey = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
	$sections = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
	$questions = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
	$questions = null;	
	if (is_null($questions)) {
		//die('Could not load questions!');
		$line = "Could not load questions!". ','.date('Y-m-d H:i:s'); 
		array_push($afile, $line);
		ErrorSPSS($fileName);
		die("(".__METHOD__.") ".translate("Error accessing"));		
	}
	*/
	
	//creaciòn para escritura de archivo .sav
	$err=$objSPSS->myspssOpenWrite($fileName.$file,$fH);
	if ($err!=SPSS_OK) {
		$bErr=true;
		//die('Error spssOpenWrite');									
		$line = "Error spssOpenWrite". ','.date('Y-m-d H:i:s');
		array_push($afile, $line);
		ErrorSPSS($fileName);
		die("(".__METHOD__.") ".translate("Failed to write file to disk"));				
	}
	$strqtpMulti='';
		
	$arrQuestionsStruct['QuestionsDef'] = array();	//Contendrá el mapeo por ID de pregunta para identificar el nombre que se le dará a la variable
	$arrQuestionsStruct['QuestionsTypes'] = array();	//Contendrá el mapeo por ID de pregunta para identificar el tipo de datos de la variable a grabar en SPSS
	$arrQuestionsStruct['QuestionsVars'] = array();	//Contendrá la lista de variables con el valor default correcto para poder reutilizarlo en el llenado de los registros
	$arrQuestionsStruct['QuestionsIDs'] = array();	//Contendrá la lista de variables con el ID de la pregunta correspondiente para poder aplicar validaciones al momento de generar valores
	$objQuestionsCollByID = array();
	
	//print('questions');
	//PrintMultiArray($questions);
	//die();
	
	//secciones normales 
	foreach ($questions as $objQuestion) {
		//if verifica preguntas válidas (no todas las preguntas son exportables aún )		@ears 2016-07-04
		if (($objQuestion->QTypeID == qtpOpenNumeric) || ($objQuestion->QTypeID == qtpOpenAlpha) || ($objQuestion->QTypeID == qtpOpenString) || ($objQuestion->QTypeID == qtpSingle) || ($objQuestion->QTypeID == qtpMulti)) {
		
			if ($objQuestion->SectionType != sectNormal) {
					continue;
			}

			$strQuestionID = (string) $objQuestion->QuestionID;
			$intQuestionID = $objQuestion->QuestionID;
			$objQuestionsCollByID[$intQuestionID] = $objQuestion;
			
			$valueSourceType = $objQuestion->ValuesSourceType;
			$iDataSourceMemberID = $objQuestion->DataSourceMemberID;
			if (strlen($objQuestion->AttributeName) == 0) {														
				if ($valueSourceType == tofCatalog){
						//$vQuestion=QFieldSPSS.$strQuestionID.'_attr_'.$iDataSourceMemberID;	
					if (($objQuestion->QTypeID == qtpDocument) || ($objQuestion->QTypeID == qtpPhoto) || ($objQuestion->QTypeID == qtpAudio) || ($objQuestion->QTypeID == qtpVideo)) {
						$vQuestion = QFieldSPSSD.$strQuestionID.'_'.$iDataSourceMemberID;
					}
					else {	
						$vQuestion=QFieldSPSS.$strQuestionID.'_'.$iDataSourceMemberID;
					}	
				} 
				else {
						if (($objQuestion->QTypeID == qtpDocument) || ($objQuestion->QTypeID == qtpPhoto) || ($objQuestion->QTypeID == qtpAudio) || ($objQuestion->QTypeID == qtpVideo)) {
							$vQuestion=QFieldSPSSD.$strQuestionID;
						}
						else {
							$vQuestion=QFieldSPSS.$strQuestionID;
						}		
				}
			}				
			else {
				if ($valueSourceType == tofCatalog){
					//$vQuestion=$objQuestion->AttributeName.'_attr_'.$iDataSourceMemberID;
					$vQuestion=$objQuestion->AttributeName.'_'.$iDataSourceMemberID;
				}
				else {
					$vQuestion=$objQuestion->AttributeName;
				}	
				
			}
			
			if ($objQuestion->QTypeID == qtpMulti) {
				$arrQuestionsStruct['QuestionsDef'][$objQuestion->QuestionID] = array();
				$arrQuestionsStruct['QuestionsTypes'][$objQuestion->QuestionID] = array();
				$arrQuestionsStruct['QuestionsVars'][$vQuestion] = array();
			}
			else {
				if ($objQuestion->QTypeID == qtpSingle) {
					$arrQuestionsStruct['QuestionsDef']["values".$objQuestion->QuestionID] = array();
				}
				$arrQuestionsStruct['QuestionsDef'][$objQuestion->QuestionID] = $vQuestion;
				switch ($objQuestion->QTypeID)  {
					case qtpOpenNumeric:
					case qtpCalc:
					case qtpSingle:
						$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssNumericVariable;
						break;
					default:
						$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssStringVariable;
						break;
				}
				$arrQuestionsStruct['QuestionsVars'][$vQuestion] = '';
				$arrQuestionsStruct['QuestionsIDs'][$vQuestion] = $intQuestionID;
			}
			
			switch ($objQuestion->QTypeID) {
				case qtpOpenNumeric:
					//print($vQuestion);
					$err = CreateVarNumericSingle($objSPSS,$fH,$vQuestion,qtpOpenNumeric);
					if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error CreateVarNumericSingle -- qtpOpenNumeric -- Secciones Normales');
							$line = "Error CreateVarNumericSingle -- qtpOpenNumeric -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));											
						}						
					break;
				case qtpOpenAlpha:
					//print $vQuestion;						
					//variables										
					
					$err=$objSPSS->myspssSetVarName($fH,$vQuestion,$objQuestion->QLength);	
					if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error spssSetVarName -- qtpOpenAlpha -- Secciones Normales');
							$line = "Error spssSetVarName -- qtpOpenAlpha -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
						}
					else {
							//numèrico (2) o alfanumérico (1) o desconocido (0)
							$err=$objSPSS->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
							if ($err!=SPSS_OK) {
								$bErr=true;
								//die('Error spssSetVarMeasureLevel -- qtpOpenAlpha -- Secciones Normales');									
								$line = "Error spssSetVarMeasureLevel -- qtpOpenAlpha -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
									
							}																	
						}							
						
					break;
				case qtpOpenString:							
					//print $vQuestion;						
					//variables
					//@ears 2016-07-12 código de generación de variable, también se debe soportar este tipo de pregunta
					
					$err=$objSPSS->myspssSetVarName($fH,$vQuestion,2000);	
					if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error spssSetVarName -- qtpOpenString -- Secciones Normales');
							$line = "Error spssSetVarName -- qtpOpenString -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
						}
					else {
							//numèrico (2) o alfanumérico (1) o desconocido (0)
							$err=$objSPSS->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
							if ($err!=SPSS_OK) {
								$bErr=true;
								//die('Error spssSetVarMeasureLevel -- qtpOpenString -- Secciones Normales');									
								$line = "Error spssSetVarMeasureLevel -- qtpOpenString -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
									
							}																	
						}							
						
					break;
				case qtpDocument:
					//@ears 2016-07-12 de momento no se requiere la exportación de estos datos					
					break;
				case qtpAudio:
					//@ears 2016-07-12 de momento no se requiere la exportación de estos datos
					break;				
				case qtpPhoto:
					//@ears 2016-07-12 de momento no se requiere la exportación de estos datos
					break;				
				case qtpVideo:	
					//@ears 2016-07-12 de momento no se requiere la exportación de estos datos
					break;				
				case qtpSingle:
					//print($vQuestion);
					$err = CreateVarNumericSingle($objSPSS,$fH,$vQuestion,qtpSingle);
					if ($err!=SPSS_OK) {
						$bErr=true;
						//die('Error CreateVarNumericSingle -- qtpSingle -- Secciones Normales');									
						$line = "Error CreateVarNumericSingle -- qtpSingle -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
							
					}																							
					else {
						$valueSourceType = $objQuestion->ValuesSourceType;
						$catalogID = $objQuestion->CatalogID;
						$dataSourceID = $objQuestion->DataSourceID;
						
						$err = AddValues($aRepository,$objSPSS,$fH,$intQuestionID,$vQuestion,$valueSourceType,$catalogID,$dataSourceID);
						if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error AddValues -- qtpSingle -- Secciones Normales');									
							$line = "Error AddValues -- qtpSingle -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
								
						}																														
					}

					//si la schoice es de catalogo, enseguida creamos atributos antes de pasar a la sig pregunta del conjunto de preguntas
					//esto para que se vea un orden en las preguntas de este tipo con sus respectivos atributos
					
					if ($objQuestion->ValuesSourceType == tofCatalog){
							AddSChoiceAttributes($aRepository,$questions,$objQuestion,$objSPSS,$fH,$vQuestion,0);												
					}										
					break;					
				case qtpMulti:
					$valueSourceType = $objQuestion->ValuesSourceType;
					$catalogID = $objQuestion->CatalogID;
					$dataSourceID = $objQuestion->DataSourceID;
					
					$strReturn=CreateVarMultipleChoice($aRepository,$objSPSS,$questions,$objQuestion,$fH,$intQuestionID,$vQuestion,$objQuestion->QuestionText,$valueSourceType,$catalogID,$dataSourceID,0);														  
					$arrReturn=explode('|',$strReturn);
					$iErr=(integer) $arrReturn[0];
					if ($iErr!=SPSS_OK) {
						$bErr=true;
						//die('Error CreateVarMultipleChoice -- qtpMulti -- Secciones Normales');									
						$line = "Error CreateVarMultipleChoice -- qtpMulti -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
							
					}	
					else {
								$strqtpMulti.='$'. $vQuestion . '=D1 1 ' . strlen($vQuestion) . ' ' . $vQuestion . $arrReturn[1] . ' \n' . chr(13) . chr(10);
								//print ($strqtpMulti);
								//die();
					}	
					break;
			}
			//para todos los casos menos multiples choice
			if ($objQuestion->QTypeID != qtpMulti) {
				
				if (!$bErr){
					$pos=strrpos($vQuestion, "Selector");
					if ($pos === false) {					
						$vNameQuestion=$objQuestion->QuestionText;
						//print $vNameQuestion;
						//etiquetas a  variables
						$err=$objSPSS->myspssSetVarLabel($fH,$vQuestion,$vNameQuestion);
						if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error spssSetVarLabel -- No Multiple Choice -- Secciones Normales');		
							$line = "Error spssSetVarLabel -- No Multiple Choice -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
								
						}																	
					}	
				}											
			}
		}	
	}	// end foreach
		
		 	
	//multiregistro		
	$i=0;
	//@ears 2016-07-05 corregido solo tomaba la última sección siempre
	foreach($sections as $objSection){
		if (($objSection->SectionType == sectMasterDet) || ($objSection->SectionType == sectInline)){
			$sectionsMulti[$i] = $objSection->SectionID.'|'.$objSection->SectionName;
			$i++;
		}	
	}
	
	foreach($sectionsMulti as $sectionsM => $arrSections) {
		$arrSect=explode('|',$arrSections);
		$sectionID=$arrSect[0];
		$sectionName=$arrSect[1];
		$nRecs=0;
		foreach ($data['data'] as $intRecordNum => $arrRowData) {			
			if (!isset($arrRowData['multiData'])) {					
				continue;
			}					
			if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName])) {
				if ($nRecs < count($arrRowData['multiData'][$sectionID.':'.$sectionName])){
					$nRecs=count($arrRowData['multiData'][$sectionID.':'.$sectionName]);
				}
			}
		}	
		$nRecs = ($nRecs == 0)?1:$nRecs; //@ears 2016-07-06 validando, en caso de que no exista captura alguna, de todas formas deben generarse las variables
		$i=1;
		while ($i<=$nRecs) {												
			foreach ($questions as $objQuestion) {
				//if (($objQuestion->SectionType == sectMasterDet) || ($objQuestion->SectionType == sectInline)) {
				//if verifica preguntas válidas (no todas las preguntas son exportables aún )		@ears 2016-07-04			
				if (($objQuestion->QTypeID == qtpOpenNumeric) || ($objQuestion->QTypeID == qtpOpenAlpha) || ($objQuestion->QTypeID == qtpOpenString) || ($objQuestion->QTypeID == qtpSingle) || ($objQuestion->QTypeID == qtpMulti)) {

					if ($objQuestion->SectionID == $sectionID)	{
						if($objQuestion->QuestionText == 'Selector') {
							continue;
						}	
						$strQuestionID = (string) $objQuestion->QuestionID;
						$intQuestionID = $objQuestion->QuestionID;
						$objQuestionsCollByID[$intQuestionID] = $objQuestion;
						
						$valueSourceType = $objQuestion->ValuesSourceType;
						$iDataSourceMemberID = $objQuestion->DataSourceMemberID;
						if (strlen($objQuestion->AttributeName) == 0) {														
							if ($valueSourceType == tofCatalog){
								//$vQuestion=QFieldSPSS.$strQuestionID.'_attr_'.$iDataSourceMemberID;						
								$nQuestion=$objQuestion->QuestionNumber;
								$vQuestion=QFieldSPSS.$strQuestionID.'_rec'.$i.'_'.$iDataSourceMemberID;

							} 
							else {
								$nQuestion=$objQuestion->QuestionNumber;
								$vQuestion=QFieldSPSS.$strQuestionID.'_rec'.$i;							
							}
						}				
						else {
							if ($valueSourceType == tofCatalog){
								//$vQuestion=$objQuestion->AttributeName.'_attr_'.$iDataSourceMemberID;
								$nQuestion=$objQuestion->QuestionNumber;
								$vQuestion=$objQuestion->AttributeName.'_rec'.$i.'_'.$iDataSourceMemberID;

							}
							else {
								$nQuestion=$objQuestion->QuestionNumber;
								$vQuestion=$objQuestion->AttributeName.'_rec'.$i;
							}					
						}
						
						if ($objQuestion->QTypeID == qtpMulti) {
							$arrQuestionsStruct['QuestionsDef'][$objQuestion->QuestionID.'_'.$i] = array();
							$arrQuestionsStruct['QuestionsTypes'][$objQuestion->QuestionID.'_'.$i] = array();
							$arrQuestionsStruct['QuestionsVars'][$vQuestion] = array();
						}
						else {
							if ($objQuestion->QTypeID == qtpSingle) {
								$arrQuestionsStruct['QuestionsDef']["values".$objQuestion->QuestionID.'_'.$i] = array();
							}
							$arrQuestionsStruct['QuestionsDef'][$objQuestion->QuestionID.'_'.$i] = $vQuestion;
							switch ($objQuestion->QTypeID)  {
								case qtpOpenNumeric:
								case qtpCalc:
								case qtpSingle:
									$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssNumericVariable;
									break;
								default:
									$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssStringVariable;
									break;
							}
							$arrQuestionsStruct['QuestionsVars'][$vQuestion] = '';
							$arrQuestionsStruct['QuestionsIDs'][$vQuestion] = $intQuestionID;
						}
						
						switch ($objQuestion->QTypeID) {
							case qtpOpenNumeric:
								//print($vQuestion);
								$err = CreateVarNumericSingle($objSPSS,$fH,$vQuestion,qtpOpenNumeric);
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error CreateVarNumericSingle -- qtpOpenNumeric');
									$line = "Error CreateVarNumericSingle -- qtpOpenNumeric -- Multiregistro". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
										
									}						
								break;
							case qtpOpenAlpha:						
								//print $vQuestion;						
								//variables
								$err=$objSPSS->myspssSetVarName($fH,$vQuestion,$objQuestion->QLength);	
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error spssSetVarName -- qtpOpenAlpha');
									$line = "Error spssSetVarName -- qtpOpenAlpha -- Multiregistro". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																												
									}
								else {
										//numèrico (2) o alfanumérico (1) o desconocido (0)
										$err=$objSPSS->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
										if ($err!=SPSS_OK) {
											$bErr=true;
											//die('Error spssSetVarMeasureLevel -- qtpOpenAlpha');									
											$line = "Error spssSetVarMeasureLevel -- qtpOpenAlpha -- Multiregistro". ','.date('Y-m-d H:i:s'); 
											array_push($afile, $line);
											ErrorSPSS($fileName);
											die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																								
										}																	
									}							
								
								break;
							case qtpOpenString:							
								//print $vQuestion;						
								//variables
								$err=$objSPSS->myspssSetVarName($fH,$vQuestion,2000);	
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error spssSetVarName -- qtpOpenString');
									$line = "Error spssSetVarName -- qtpOpenString -- Multiregistro". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																												
									}
								else {
										//numèrico (2) o alfanumérico (1) o desconocido (0)
										$err=$objSPSS->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
										if ($err!=SPSS_OK) {
											$bErr=true;
											//die('Error spssSetVarMeasureLevel -- qtpOpenString');									
											$line = "Error spssSetVarMeasureLevel -- qtpOpenString -- Multiregistro". ','.date('Y-m-d H:i:s'); 
											array_push($afile, $line);
											ErrorSPSS($fileName);
											die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																								
										}																	
									}							
							
								break;
							case qtpSingle:
								//print($vQuestion);
								$err = CreateVarNumericSingle($objSPSS,$fH,$vQuestion,qtpSingle);
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error CreateVarNumericSingle -- qtpSingle');									
									$line = "Error CreateVarNumericSingle -- qtpSingle -- Multiregistro". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																									
										
								}																							
								else {
									$valueSourceType = $objQuestion->ValuesSourceType;
									$catalogID = $objQuestion->CatalogID;
									$dataSourceID = $objQuestion->DataSourceID;
									
									$err = AddValues($aRepository,$objSPSS,$fH,$intQuestionID,$vQuestion,$valueSourceType,$catalogID,$dataSourceID);
									if ($err!=SPSS_OK) {
										$bErr=true;
										//die('Error AddValues -- qtpSingle');									
										$line = "Error AddValues -- qtpSingle -- Multiregistro". ','.date('Y-m-d H:i:s'); 
										array_push($afile, $line);
										ErrorSPSS($fileName);
										die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																									
											
									}																														
								}

								//si la schoice es de catalogo, enseguida creamos atributos antes de pasar a la sig pregunta del conjunto de preguntas
								//esto para que se vea un orden en las preguntas de este tipo con sus respectivos atributos
								
								if ($objQuestion->ValuesSourceType == tofCatalog){
										AddSChoiceAttributes($aRepository,$questions,$objQuestion,$objSPSS,$fH,$vQuestion,$i);
								}										
								break;					
							case qtpMulti:
								$valueSourceType = $objQuestion->ValuesSourceType;
								$catalogID = $objQuestion->CatalogID;
								$dataSourceID = $objQuestion->DataSourceID;
								
								$strReturn=CreateVarMultipleChoice($aRepository,$objSPSS,$questions,$objQuestion,$fH,$intQuestionID,$vQuestion,$objQuestion->QuestionText,$valueSourceType,$catalogID,$dataSourceID,$i);								                                  
								$arrReturn=explode('|',$strReturn);
								$iErr=(integer) $arrReturn[0];
								if ($iErr!=SPSS_OK) {
									$bErr=true;
									//die('Error CreateVarMultipleChoice -- qtpMulti');									
									$line = "Error CreateVarMultipleChoice -- qtpMulti -- Multiregistro". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																			
								}	
								else {
											$strqtpMulti.='$'. $vQuestion . '=D1 1 ' . strlen($vQuestion) . ' ' . $vQuestion . $arrReturn[1] . ' \n' . chr(13) . chr(10);
											//print ($strqtpMulti);
											//die();
								}														
								break;
								
								
						}
						//para todos los casos menos multiples choice
						if ($objQuestion->QTypeID != qtpMulti) {
							
							if (!$bErr){
								$pos=strrpos($vQuestion, "Selector");
								if ($pos === false) {					
									$vNameQuestion=$objQuestion->QuestionText;
									//print $vNameQuestion;
									//etiquetas a  variables
									$err=$objSPSS->myspssSetVarLabel($fH,$vQuestion,$vNameQuestion);
									if ($err!=SPSS_OK) {
										$bErr=true;
										//die('Error spssSetVarLabel -- No Multiple Choice');									
										$line = "Error spssSetVarLabel -- No Multiple Choice -- Multiregistro". ','.date('Y-m-d H:i:s'); 
										array_push($afile, $line);
										ErrorSPSS($fileName);
										die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																														
									}																	
								}	
							}
						}															

					}
				}	
			}// end foreach questions
			$i++;
		}// while					
	}//end foreach sections array	
		
		//establezco mis variables de respuesta mùltiple en un conjunto de respuestas mùltiples			
		$err=$objSPSS->myspssSetMultRespDefs($fH,$strqtpMulti);
		if ($err!=SPSS_OK) {
			$bErr=true;
			//die('Error spssSetMultRespDefs -- qtpMulti');									
			$line = "Error spssSetMultRespDefs -- qtpMulti". ','.date('Y-m-d H:i:s'); 
			array_push($afile, $line);
			ErrorSPSS($fileName);
			die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																														
				
		}				
			
		//print ($strqtpMulti);
		//PrintMultiArray($arrQuestionsStruct['QuestionsDef']);
		//die();		

		//$arrQuestionsStruct['QuestionsDef'] 
		//$arrQuestionsStruct['QuestionsTypes'] 
		//$arrQuestionsStruct['QuestionsVars'] 
		//$arrQuestionsStruct['QuestionsIDs'] 
		
		
		//cierra el encabezado sino se hace no se crea
		$err=$objSPSS->myspssCommitHeader($fH);
		if ($err!=SPSS_OK) {
			$bErr=true;
			//die('Error spssCommitHeader');											
			$line = "Error spssCommitHeader". ','.date('Y-m-d H:i:s'); 
			array_push($afile, $line);
			ErrorSPSS($fileName);
			die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																														
				
		}			
		
		//Inicia la generación de los datos de las capturas utilizando el array de estructuras para identificar las variables que debe llenar
		$arrQuestionsDef = $arrQuestionsStruct['QuestionsDef'];
		$arrQuestionsTypes = $arrQuestionsStruct['QuestionsTypes'];
		$arrAnswers = $arrQuestionsStruct['QuestionsVars'];
		$arrVariablePointers = array();
				
		//PrintMultiArray($data['data']);
		//print('<br>');
		//die();
		//print('questionsdef');
		//PrintMultiArray($arrQuestionsDef);
		//print('<br>');
		//print('answers');
		//PrintMultiArray($arrAnswers);
		//die();		

	//$arrAllAnswers = array();
	$strKeyField=1;
	$indiceMulti=0;
	foreach ($data['data'] as $intRecordNum => $arrRowData) {
		$arrAnswers = $arrQuestionsStruct['QuestionsVars'];
		//asignación de respuestas -- secciones estandar o normales
		foreach ($questions as $objQuestion) {
			//if verifica preguntas válidas (no todas las preguntas son exportables aún )		@ears 2016-07-04			
			if (($objQuestion->QTypeID == qtpOpenNumeric) || ($objQuestion->QTypeID == qtpOpenAlpha) || ($objQuestion->QTypeID == qtpOpenString) || ($objQuestion->QTypeID == qtpSingle) || ($objQuestion->QTypeID == qtpMulti)) {
			
				if (!isset($arrQuestionsDef[$objQuestion->QuestionID])) {
					continue;
				}				
				//Sólo para las preguntas de secciones estándar
				if ($objQuestion->SectionType == sectNormal) {
					switch ($objQuestion->QTypeID) {
						case qtpSingle:
							if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID])) {
								if ($objQuestion->ValuesSourceType == tofCatalog) {  //schoice de catalogo
									if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID])) {
										$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID]] = $arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
									}								
									
									foreach ($arrRowData['stdmultiData'][$objQuestion->QuestionID] as $intDataSourceMemberID => $strDataSourceMemberValue) {
										if ($intDataSourceMemberID != $objQuestion->DataSourceMemberID) {
											$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$intDataSourceMemberID]] = $strDataSourceMemberValue;
										}
									}
									/*echo("<br>Datos del row #$intRecordNum para la pregunta {$objQuestion->QuestionID}<br>");
									PrintMultiArray($arrAnswers);
									die();*/
								}
								else {		
									//En este caso el valor se encuentra en el subarray stdmultiData							
									if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID])) {
										$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID]] = $arrRowData['stdmultiData'][$objQuestion->QuestionID];
									}								
								}	
							}
							break;
						case qtpMulti:						
							if ($objQuestion->ValuesSourceType == tofCatalog) {  //mchoice de catalogo
								//respuesta a la pregunta
								if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID]) && is_array($arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID])) {
									$arrMultiAnswers = $arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
									foreach ($arrMultiAnswers as $strAnswer) {
										if (isset($arrQuestionsDef[$objQuestion->QuestionID][$strAnswer])) {
											$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID][$strAnswer]] = sspsDefaultMCYesValue;
										}
									}																							
								}
								
								if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID]) && is_array($arrRowData['stdmultiData'][$objQuestion->QuestionID])) {																		
									foreach ($arrRowData['stdmultiData'][$objQuestion->QuestionID] as $intDataSourceMemberID => $strDataSourceMemberValue) {
										if ($intDataSourceMemberID == $objQuestion->DataSourceMemberID) {												
											$arrMAnswers = $arrRowData['stdmultiData'][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
										}
										if ($intDataSourceMemberID != $objQuestion->DataSourceMemberID) {												
											foreach($arrMAnswers as $iAnswer => $sAnswer) {
												$arrAnswersAttrib = $arrRowData['stdmultiData'][$objQuestion->QuestionID][$intDataSourceMemberID];
												$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$sAnswer.'_'.$intDataSourceMemberID]] = $arrAnswersAttrib[$iAnswer] ;//$strDataSourceMemberValue;
												/*
												foreach($arrAnswersAttrib as $sAnswer) {
													print($arrQuestionsDef[$objQuestion->QuestionID.'_'.$strAnswer.'_'.$intDataSourceMemberID].'_'.$sAnswer);
													print('<br>');	
													$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$strAnswer.'_'.$intDataSourceMemberID]] = $sAnswer;//$strDataSourceMemberValue;												
												}
												*/
											}			
										}	
									}																																													
								}		
							}
							else {		
								//En este caso el valor se encuentra en el subarray stdmultiData							
								if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID]) && is_array($arrRowData['stdmultiData'][$objQuestion->QuestionID])) {
									$arrMultiAnswers = $arrRowData['stdmultiData'][$objQuestion->QuestionID];
									foreach ($arrMultiAnswers as $strAnswer) {
										if (isset($arrQuestionsDef[$objQuestion->QuestionID][$strAnswer])) {
											$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID][$strAnswer]] = sspsDefaultMCYesValue;
										}
									}
								}								
							}										
							//En este caso el valor se encuentra en el subarray stdmultiData, adicionalmente es un array con el número de respuestas seleccionadas
							//anterior
							/*
							if (isset($arrRowData['stdmultiData'][$objQuestion->QuestionID]) && is_array($arrRowData['stdmultiData'][$objQuestion->QuestionID])) {
								$arrMultiAnswers = $arrRowData['stdmultiData'][$objQuestion->QuestionID];
								foreach ($arrMultiAnswers as $strAnswer) {
									if (isset($arrQuestionsDef[$objQuestion->QuestionID][$strAnswer])) {
										$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID][$strAnswer]] = sspsDefaultMCYesValue;
									}
								}
							}								
							*/
									/*foreach ($arrRowData['stdmultiData'][$objQuestion->QuestionID] as $intMChoiceOpt => $arrAttribValues) {
										//Este código es el caso cuando es MChoice de catálogo
										$strMainAttrVal = (int) @$arrAttribValues[$objQuestion->DataSourceMemberID]
										if ($strMainAttrVal) {
											$intMainAttrKey = arrayX[$strMainAttrVal];
											$arrQuestionsDef[$objQuestion->QuestionID."_Opt_".$intMainAttrKey.'_'.$intDataSourceMemberID]
										}
										
										MChoice normal
										QField6841_1					//Para el 1er valor de QuestionOptions (donde _1 es el score si es que está asignado o la posición y es $intMainAttrKey)
										QField6841_2					//Para el 2do valor de catálogo ( " )
										
										MChoice de catálogo
										QField6841_954_attr_1369		//Para el 1er valor de QuestionOptions (donde _954 es el key del valor de catálogo y es $intMainAttrKey)
										QField6841_954_attr_1370
										QField6841_954_attr_1371
										QField6841_1070_attr_1369		//Para el 2do valor de catálogo ( " )
										QField6841_1070_attr_1370
										QField6841_1070_attr_1371
										
										//Para el caso de multi-registro, la diferencia radica en que todo este proceso está corriendo dentro de otro ciclo que recorre las rows de la
										//sección, así que hay que agregar otro separador al nombre de variable de esta manera:
										MChoice normal
										QField6841_rec1_1					
										QField6841_rec1_2					
										QField6841_rec2_1					
										QField6841_rec2_2					
										QField6841_rec3_1					
										QField6841_rec3_2					
										
										MChoice de catálogo
										QField6841_rec1_954_attr_1369		//Para el 1er valor de QuestionOptions (donde _954 es el key del valor de catálogo y es $intMainAttrKey)
										QField6841_rec1_954_attr_1370
										QField6841_rec1_954_attr_1371
										QField6841_rec1_1070_attr_1369		//Para el 2do valor de catálogo ( " )
										QField6841_rec1_1070_attr_1370
										QField6841_rec1_1070_attr_1371
										QField6841_rec2_954_attr_1369		//Para el 1er valor de QuestionOptions (donde _954 es el key del valor de catálogo y es $intMainAttrKey)
										QField6841_rec2_954_attr_1370
										QField6841_rec2_954_attr_1371
										QField6841_rec2_1070_attr_1369		//Para el 2do valor de catálogo ( " )
										QField6841_rec2_1070_attr_1370
										QField6841_rec2_1070_attr_1371
									}*/
							
							
							break;
						default:
							$blnIsNumeric = false;
							//El valor se encuentra directamente en el array de respuestas
							if (isset($arrRowData[$objQuestion->QuestionID])) {
								$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID]] = $arrRowData[$objQuestion->QuestionID];
							}
							break;
					} //
				}
			}	
		}//questions secciones normales
				
		//asignacion de respuestas secciones multiregistro		
		$arrSect=array();
		$sectionID=0;
		$sectionName='';
		foreach($sectionsMulti as $sectionsM => $arrSections) {
			$arrSect=explode('|',$arrSections);
			$sectionID=$arrSect[0];
			$sectionName=$arrSect[1];
			
			if (!isset($arrRowData['multiData'])) {					
				continue;
			}					
			if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName])) {
				$nRecs=count($arrRowData['multiData'][$sectionID.':'.$sectionName]);
				$i=1;
				//while ($i<=$nRecs) {
				foreach($arrRowData['multiData'][$sectionID.':'.$sectionName] as $iKeyField => $arrKeyField) {
					//print($strKeyField);
					//$arrAnswers = $arrQuestionsStruct['QuestionsVars'];														
					foreach ($questions as $objQuestion) {	
						//if verifica preguntas válidas (no todas las preguntas son exportables aún )		@ears 2016-07-04			
						if (($objQuestion->QTypeID == qtpOpenNumeric) || ($objQuestion->QTypeID == qtpOpenAlpha) || ($objQuestion->QTypeID == qtpOpenString) || ($objQuestion->QTypeID == qtpSingle) || ($objQuestion->QTypeID == qtpMulti)) {					
							if ($objQuestion->SectionID == $sectionID)	{
								if ($objQuestion->QuestionText == 'Selector'){
									continue;
								}
								switch ($objQuestion->QTypeID) {
									case qtpSingle:
										if ($objQuestion->ValuesSourceType == tofCatalog) {  //schoice de catalogo
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID])) {
													$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i]] = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
											}									
										
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID])){
												foreach ($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID] as $intDataSourceMemberID => $strDataSourceMemberValue) {
													if ($intDataSourceMemberID != $objQuestion->DataSourceMemberID) {
														$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_rec'.$i.'_'.$intDataSourceMemberID]] = $strDataSourceMemberValue;
													} 
												}
											}														
										}	
										else {		
											//En este caso el valor se encuentra en el subarray multiData							
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID])) {
												$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i]] = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID];
											}								
										}	
										//}
										break;
									case qtpMulti:
										if ($objQuestion->ValuesSourceType == tofCatalog) {  //mchoice de catalogo
											//respuesta a la pregunta
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID]) && is_array($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID])) {											
												$arrMAnswersMC = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
												foreach ($arrMAnswersMC as $strAnswer) {
													if (isset($arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer])) {
														$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer]] = sspsDefaultMCYesValue;
													}
												}																							
											}
											
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID]) && is_array($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID])) {																		
												foreach ($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID] as $intDataSourceMemberID => $strDataSourceMemberValue) {
													if ($intDataSourceMemberID == $objQuestion->DataSourceMemberID) {												
														$arrMAnswersMC = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$objQuestion->DataSourceMemberID];
													}
													if ($intDataSourceMemberID != $objQuestion->DataSourceMemberID) {												
														foreach($arrMAnswersMC as $iAnswer => $sAnswer) {
															$arrAnswersAttrib = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID][$intDataSourceMemberID];
															$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$sAnswer.'_rec_'.$i.'_'.$intDataSourceMemberID]] = $arrAnswersAttrib[$iAnswer] ;//$strDataSourceMemberValue;
															/*
															foreach($arrAnswersAttrib as $sAnswer) {
																print($arrQuestionsDef[$objQuestion->QuestionID.'_'.$strAnswer.'_'.$intDataSourceMemberID].'_'.$sAnswer);
																print('<br>');	
																$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$strAnswer.'_'.$intDataSourceMemberID]] = $sAnswer;//$strDataSourceMemberValue;												
															}
															*/
														}			
													}	
												}																																													
											}		
										}																																				
										else {		
											//En este caso el valor se encuentra en el subarray multiData													
											if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID]) && is_array($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID])) {
												$arrMulti = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID];
												foreach ($arrMulti as $strAnswer) {
													if (isset($arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer])) {
														$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer]] = sspsDefaultMCYesValue;
													}
												}
											}								
										}										
									
										//En este caso el valor se encuentra en el subarray multiData, adicionalmente es un array con el número de respuestas seleccionadas
										/*
										if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$strKeyField][$objQuestion->QuestionID]) && is_array($arrRowData['multiData'][$sectionID.':'.$sectionName][$strKeyField][$objQuestion->QuestionID])) {
											$arrMultiAnswers = $arrRowData['multiData'][$sectionID.':'.$sectionName][$strKeyField][$objQuestion->QuestionID];
											foreach ($arrMultiAnswers as $strAnswer) {
												if (isset($arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer])) {
													$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i][$strAnswer]] = sspsDefaultMCYesValue;
												}
											}
										}
										*/	
										break;
									default:
										$blnIsNumeric = false;
										//El valor se encuentra directamente en el array de respuestas
										if (isset($arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID])) {
											$arrAnswers[$arrQuestionsDef[$objQuestion->QuestionID.'_'.$i]] = $arrRowData['multiData'][$sectionID.':'.$sectionName][$iKeyField][$objQuestion->QuestionID];
										}
										break;
								} //
							}	
						}	
					}// end foreach questions
					$i++;  //la $i bien podría ser reemplezada por $iKeyField, pero hay que probar que el resultado sea el mismo, si fuera el mismo resultado puede desaparecer $i
					//$strKeyField++;	
				}// while					
			}			
		}
		
		//print('<br>');			
		//PrintMultiArray($arrAnswers);
		//Aquí abrir el handle del new record
		//print('QuestionsIDs <br>');
		//PrintMultiArray($arrQuestionsStruct['QuestionsIDs']);
		//PrintMultiArray($arrQuestionsTypes);
		//PrintMultiArray($arrRowData);
		//print('<br>');
		
		//PrintMultiArray($objQuestionsCollByID);
		//print('<br>');
		foreach ($arrAnswers as $strVariableName => $strVariableValue) {				
			$intQuestionID = (int) @$arrQuestionsStruct['QuestionsIDs'][$strVariableName];
			$objQuestion = @$objQuestionsCollByID[$intQuestionID];
			//PrintMultiArray($objQuestion);
			if (is_null($objQuestion)) {
				continue;
			}
			
			if (is_array($arrAnswers[$strVariableName])){
			}
			else {
				$pos=strrpos($strVariableName, "Selector");
				if ($pos === false) {		
					$intVariableType = (integer) @$arrQuestionsTypes[$strVariableName];
					//print('Revisando Tipos de Dato');
					//print(@$arrQuestionsTypes[$strVariableName]); 
					//print('<br>');
					//PrintMultiArray($arrQuestionsTypes);
					//$str=$obj->myspssGetVarHandle($fH,$strVariableName);
					//$arr=explode('|',$str);
					//$arr[1] = $arrVariablePointers[$strVariableName];
					//print(@$arrQuestionsTypes[$strVariableName]); 
					//print('dentro del if');
					//print($strVariableName.'-'.$intVariableType.'<br>');
					$str=$objSPSS->myspssGetVarHandle($fH,$strVariableName);
					$arrReturn=explode('|',$str);
					$iErr=(integer) $arrReturn[0];
					if ($iErr!=SPSS_OK) {
						$bErr=true;
						//die('Error spssGetVarHandle');	
						$line = "Error spssGetVarHandle". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																														
							
					}							
					
					switch ($intVariableType) {
						case spssNumericVariable:							
							//$err=$objSPSS->myspssSetValueNumeric($fH,$arrVariablePointers[$strVariableName],$strVariableValue);
							$intVariableValue = (float) $strVariableValue;
							if ($objQuestion->QTypeID == qtpSingle) {
								//print($strVariableName.' '.$strVariableValue);															
								$intVariableValue = (int) @$arrQuestionsStruct['QuestionsDef']["values".$intQuestionID][$strVariableValue];
								//print($strVariableName.' '.$intVariableValue);
							}
							//print($arrReturn[1]);
							//print($strVariableName);
							 
							$err=$objSPSS->myspssSetValueNumeric($fH,$arrReturn[1],$intVariableValue);
							if ($err!=SPSS_OK) {
								$bErr=true;
								//die('Error spssSetValueNumeric -- Answers');											
								$line = "Error spssSetValueNumeric -- Answers". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																														
								
							}									
							break;
						default:
							//$err=$objSPSS->myspssSetValueChar($fH,$arrVariablePointers[$strVariableName],$strVariableValue);
							//print('alpha');
							//print('<br>');
							//print($arrReturn[1].'-'.$strVariableValue.'<br>');
							//print('<br>');
							//print('checar atributos');
							//print($strVariableName);
							//print('<br>');
							IF ($strVariableValue == 'NA') {
								$err=$objSPSS->myspssSetValueChar($fH,$arrReturn[1],'');
							}
							else {
								$err=$objSPSS->myspssSetValueChar($fH,$arrReturn[1],$strVariableValue);
							}
							if ($err!=SPSS_OK) {
								$bErr=true;
								//die('Error spssSetValueChar -- Answers');											
								$line = "Error spssSetValueChar -- Answers". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																						
							}																
							break;
					}
				}	
			}							
						
		}
								
		//Aquí cerrar el handle del new record			
		//se hace commit para guardar registro
		$err=$objSPSS->myspssCommitCaseRecord($fH);
		if ($err!=SPSS_OK) {
			$bErr=true;
			//die('Error spssCommitCaseRecord');											
			$line = "Error spssCommitCaseRecord". ','.date('Y-m-d H:i:s'); 
			array_push($afile, $line);
			ErrorSPSS($fileName);
			die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																						
			
			
		}	
		//$arrAllAnswers[$intRecordNum] = $arrAnswers;
	}		
	//print('<br>');
	//PrintMultiArray($arrAllAnswers);
	//cerrando archivo	
	$err=$objSPSS->myspssCloseWrite($fH);			
	if ($err!=SPSS_OK) {
		$bErr=true;
		//die('Error spssCloseWrite');											
		$line = "Error spssCloseWrite". ','.date('Y-m-d H:i:s'); 
		array_push($afile, $line);
		ErrorSPSS($fileName);
		die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																								
	}		
	
	$sfileSav = $file;
	$spathfileSav =$fileName.$file; //'log/20160628183442_ReportSurvey_584.sav'; //'log/20160623135543_ReportSurvey_527.sav';
	
	//se confirma archivo generado para descarga
	//sobre el $err del CloseWrite
	
	if ($err == SPSS_OK)
	{
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfileSav."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfileSav));		
		while (ob_get_level()) {
			ob_end_clean();
		}						
		readfile($spathfileSav);
		exit();
	}	
	
}

//funciones de apoyo

//@ears 2016-07-12 de momento queda comentada a reserva de contemplar los tipos de pregunta multimedia previa confirmación
/*
function CreateVarAlphaStringDoc ($fH,$varQuestion,$longitud) {
	global $afile;
	//print $vQuestion;						
	//variables
	$err = 0;
	$err=$objSPSS->myspssSetVarName($fH,$varQuestion,$longitud);	
	if ($err!=SPSS_OK) {
			$bErr=true;
			//die('Error spssSetVarName -- Secciones Normales');
			$line = "Error spssSetVarName -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
			array_push($afile, $line);
			ErrorSPSS($fileName);
			die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
		}
	else {
			//numèrico (2) o alfanumérico (1) o desconocido (0)
			$err=$objSPSS->myspssSetVarMeasureLevel($fH,$varQuestion,SPSS_MLVL_NOM);
			if ($err!=SPSS_OK) {
				$bErr=true;
				//die('Error spssSetVarMeasureLevel  -- Secciones Normales');									
				$line = "Error spssSetVarMeasureLevel -- Secciones Normales". ','.date('Y-m-d H:i:s'); 
				array_push($afile, $line);
				ErrorSPSS($fileName);
				die("(".__METHOD__.") ".translate("Failed to write file to disk"));																		
					
			}																	
		}							
}
*/

function CreateVarNumericSingle	($obj,$fH,$varQuestion,$typeID){	
	global $afile;		
	
	//variables
	$err=0;
	$pos=strrpos($varQuestion, "Selector");
	if ($pos === false) {
		$err=$obj->myspssSetVarName($fH,$varQuestion,0);	
		if ($err!=SPSS_OK) {
			$bErr=true;
			//die('Error SetVarName -- CreateVarNumericSingle');
			$line = "Error SetVarName -- CreateVarNumericSingle". ','.date('Y-m-d H:i:s'); 
			array_push($afile, $line);
			ErrorSPSS($fileName);
			die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																
			}
		else {
				//numèrico (2) o alfanumérico (1) o desconocido (0)
				$err=$obj->myspssSetVarMeasureLevel($fH,$varQuestion,SPSS_MLVL_ORD);
				if ($err!=SPSS_OK) {
					$bErr=true;
					//die('Error SetVarMeasureLevel -- CreateVarNumericSingle');									
					$line = "Error SetVarMeasureLevel -- CreateVarNumericSingle". ','.date('Y-m-d H:i:s'); 
					array_push($afile, $line);
					ErrorSPSS($fileName);
					die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																
						
				}
				else {
					//se establece formato para casos de variables numèricas enteras que no aparezcan como floats, sino como enteras
					if ($typeID == qtpSingle) {
						$err=$obj->myspssSetVarPrintFormat($fH,$varQuestion,SPSS_FMT_F,0,4);  //esto es para definir cantidades enteras					
					}
					if ($typeID == qtpOpenNumeric) { 
						//para definir cantidades flotantes
						$err=$obj->myspssSetVarPrintFormat($fH,$varQuestion,SPSS_FMT_F,4,16);  //esto es para definir cantidades flotantes						
					}	
					if ($err!=SPSS_OK) {
						$bErr=true;
						//die('Error SetVarPrintFormat -- CreateVarNumericSingle');									
						$line = "Error SetVarPrintFormat -- CreateVarNumericSingle". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																							
					}									
				}																	
			}			 
		
	}
	return $err;
}
		
function AddValues($aRepository,$obj,$fH,$intQuestionID,$varQuestion,$iValuesSourceType,$aCatalogID,$iDataSourceID) {
	global $arrQuestionsStruct;
	global $afile;

	$err=0;	
	$pos=strrpos($varQuestion, "Selector");	
	if ($pos === false) {
			if ($iValuesSourceType == tofCatalog){  //si viene de catàlogo
				$intMaxOrder = 0;
				$strDataSourceFilter = '';
				//@JAPR
				$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
				$arrDef['attributes'] = BITAMCatalog::GetCatalogAttributes($aRepository, $aCatalogID, $intMaxOrder,null);
				$arrDef['attributesOrder'] = array();
				foreach ($arrDef['attributes'] as $aMemberID) {
					$arrDef['attributesOrder'][] = (int) $aMemberID['id'];
				}
				//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
				//$arrDef['attributeValues'] = BITAMCatalog::GetCatalogAttributeValues($this->Repository, $this, array_values($arrDef['attributes']));
				//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
				//if ($bGetValues) {
					//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
					//Dado a que en v6 los catálogos están únicamente ligados a una misma pregunta, obtiene además los DataSourceFilter aplicables para este catálogo según si es de una sección
					//o una pregunta, siempre habría sólo uno por cada caso pero aunque hubiera varios en el peor de los casos podría concatenar todos, aunque eso no sería correcto
					if ($iDataSourceID > 0 && trim($strDataSourceFilter) != '') {
						//@JAPR 2015-09-30: Corregido un bug, esta función no consideraba DataSources de tipo eBavel, además su función está repetida en TranslateAttributesVarsToFieldNames
						//por lo que se optará por esta última y se descontinuará esta función
						//$strDataSourceFilter = BITAMDataSource::ReplaceMemberIDsByFields($this->Repository, $this->DataSourceID, $strDataSourceFilter);
						$strDataSourceFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($aRepository, $iDataSourceID, $strDataSourceFilter);
						//@JAPR 2015-11-02: Corregido un bug con filtros dinámicos en el preview del Admin, las variables de preguntas/secciones no se pueden resolver pero no se reemplazaban y generaban error (#X7ZU1E)
						if ($gbDesignMode) {
							//Si está en modo diseño, nadie reemplaza las variables de preguntas y secciones ya que eso es labor del App, así que las cambia a NULL
							$strDataSourceFilter = ReplaceAppVariablesForDummyValues($strDataSourceFilter);
						}
						//@JAPR
					}
					$arrDef['attributeValues'] = BITAMCatalog::GetCatalogAttributeValues($aRepository,$aCatalog,array_values($arrDef['attributes']),-1,$strDataSourceFilter,null,null,false,false);
					//@JAPR
				/*}
				else {
					$arrDef['attributeValues'] = array();
				}
				*/
				
				//print('atributos catalogo');
				//PrintMultiArray($arrDef['attributeValues']);	
				//die();
				//PrintMultiArray($arrQuestionsStruct['QuestionsDef']);
				//die();		

				//con los atributos generar las variables 	
				foreach ($arrDef['attributeValues'] as $strAttrib) {
									
						$vOptionKey = $strAttrib['key'];			
						$arrQuestionsStruct['QuestionsDef']["values".$intQuestionID][$strAttrib['value']] = $vOptionKey;
						$arrQuestionsStruct['QuestionsTypes'][$varQuestion] = spssNumericVariable;
						$arrQuestionsStruct['QuestionsVars'][$varQuestion] = sspsDefaultMCYesValue;
						$arrQuestionsStruct['QuestionsIDs'][$varQuestion] = $intQuestionID;
								
						//asignación de valores en vista de variables
						$err=$obj->myspssSetVarNValueLabel($fH,$varQuestion,$vOptionKey,$strAttrib['value']);				
						if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error SetVarNValueLabel -- AddValues');									
							$line = "Error SetVarNValueLabel -- AddValues". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																							
								
						}
				}
					
			}
			
			else {  //sino viene de catálogo
				$qoption = BITAMQuestionOptionCollection::NewInstance($aRepository, $intQuestionID);
				foreach ($qoption as $objQOption) {		
				
					if (strlen($objQOption->Score) !=0) {
						//$vOptionScore = $objQOption->Score;							
						if (is_numeric($objQOption->Score)) {
							$vOptionScore = $objQOption->Score;	
						}
						else {
							$vOptionScore = (string) $objQOption->QuestionOptionID;
						}							
					}
					else{ 
						$vOptionScore = (string) $objQOption->QuestionOptionID;
					}
				
					$arrQuestionsStruct['QuestionsDef']["values".$intQuestionID][$objQOption->QuestionOptionName] = $vOptionScore;
					$arrQuestionsStruct['QuestionsTypes'][$varQuestion] = spssNumericVariable;
					$arrQuestionsStruct['QuestionsVars'][$varQuestion] = sspsDefaultMCYesValue;
					$arrQuestionsStruct['QuestionsIDs'][$varQuestion] = $intQuestionID;

					//print($varQuestion);
					//print('<br>');
					//print($vOptionScore);		
					//print('<br>');
					//print($objQOption->QuestionOptionName);		
					//print('<br>');
					//asignación de valores en vista de variables
					$err=$obj->myspssSetVarNValueLabel($fH,$varQuestion,$vOptionScore,$objQOption->QuestionOptionName);				
					if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error SetVarNValueLabel -- AddValues');									
							$line = "Error SetVarNValueLabel -- AddValues". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																														
					}														
				}											
			}
	}
	return $err;
}

function CreateVarMultipleChoice($aRepository,$obj,$qST,$objQ,$fH,$intQuestionID,$varQuestion,$vQuestionT,$iValuesSourceType,$aCatalogID,$iDataSourceID,$regCaptura) {
	global $arrQuestionsStruct;
	global $afile;

	$err=0;
	$strMultRespDefs='';
	$i=1;
	
	if ($iValuesSourceType == tofCatalog)  //si viene de catàlogo
		{
				$intMaxOrder = 0;
				$strDataSourceFilter = '';
				//@JAPR
				$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogID);
				$arrDef['attributes'] = BITAMCatalog::GetCatalogAttributes($aRepository, $aCatalogID, $intMaxOrder,null);
				$arrDef['attributesOrder'] = array();
				foreach ($arrDef['attributes'] as $aMemberID) {
					$arrDef['attributesOrder'][] = (int) $aMemberID['id'];
				}
				//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
				//$arrDef['attributeValues'] = BITAMCatalog::GetCatalogAttributeValues($this->Repository, $this, array_values($arrDef['attributes']));
				//@JAPR 2014-06-12: Validado para que sólo regrese valores del catálogo si hay por lo menos una encuesta que no tenga filtro dinámico para él
				//if ($bGetValues) {
					//@JAPR 2015-08-02: Agregado el tipo de pregunta GetData
					//Dado a que en v6 los catálogos están únicamente ligados a una misma pregunta, obtiene además los DataSourceFilter aplicables para este catálogo según si es de una sección
					//o una pregunta, siempre habría sólo uno por cada caso pero aunque hubiera varios en el peor de los casos podría concatenar todos, aunque eso no sería correcto
					if ($iDataSourceID > 0 && trim($strDataSourceFilter) != '') {
						//@JAPR 2015-09-30: Corregido un bug, esta función no consideraba DataSources de tipo eBavel, además su función está repetida en TranslateAttributesVarsToFieldNames
						//por lo que se optará por esta última y se descontinuará esta función
						//$strDataSourceFilter = BITAMDataSource::ReplaceMemberIDsByFields($this->Repository, $this->DataSourceID, $strDataSourceFilter);
						$strDataSourceFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($aRepository, $iDataSourceID, $strDataSourceFilter);
						//@JAPR 2015-11-02: Corregido un bug con filtros dinámicos en el preview del Admin, las variables de preguntas/secciones no se pueden resolver pero no se reemplazaban y generaban error (#X7ZU1E)
						if ($gbDesignMode) {
							//Si está en modo diseño, nadie reemplaza las variables de preguntas y secciones ya que eso es labor del App, así que las cambia a NULL
							$strDataSourceFilter = ReplaceAppVariablesForDummyValues($strDataSourceFilter);
						}
						//@JAPR
					}
					$arrDef['attributeValues'] = BITAMCatalog::GetCatalogAttributeValues($aRepository,$aCatalog,array_values($arrDef['attributes']),-1,$strDataSourceFilter,null,null,false,false);
					//@JAPR
				/*}
				else {
					$arrDef['attributeValues'] = array();
				}
				*/
				
				//print('atributos catalogo');
				//PrintMultiArray($arrDef['attributeValues']);	
				//die();
				//PrintMultiArray($arrQuestionsStruct['QuestionsDef']);
				//die();		

				//con los atributos generar las variables 	
				foreach ($arrDef['attributeValues'] as $strAttrib) {
					
						//originalmente el indice i se colocaba al final de la nomenclatura de la variable
						//se solicitó se colocara de forma intermedia		
						$arrVQOption = explode('_',$varQuestion);						
						$vQOption = $arrVQOption[0].'_'.$i; //$varQuestion . '_' . $i;	
						$nRows = count($arrVQOption)-1;
						$j=1;
						while ($j<=$nRows){
							//print($arrVQOption[$j]);
							if (isset($arrVQOption[$j])) {
								$vQOption.= '_' . $arrVQOption[$j];	
							}	
							$j++;
						}	
						//$pos_ = strrpos($vQOption, "_"); //ultima ocurrencia de _	
						//$vQOption = substr($vQOption,0,$pos_);
						
						//$vQOptionDef=$varQuestion . '_' . $i;
						
						if ($objQ->SectionType == sectNormal) {																	
							$arrQuestionsStruct['QuestionsDef'][$intQuestionID][$strAttrib['value']] = $vQOption;
						}
						if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {																	
							$arrQuestionsStruct['QuestionsDef'][$intQuestionID.'_'.$regCaptura][$strAttrib['value']] = $vQOption;
						}
						
						$arrQuestionsStruct['QuestionsTypes'][$vQOption] = spssNumericVariable;
						$arrQuestionsStruct['QuestionsVars'][$vQOption] = sspsDefaultMCNoValue;
						$arrQuestionsStruct['QuestionsIDs'][$vQOption] = $intQuestionID;

						$err=CreateVarNumericSingle($obj,$fH,$vQOption,qtpSingle);
						if ($err!=SPSS_OK) {
								$bErr=true;
								//die('Error CreateVarNumericSingle -- CreateVarMultipleChoice');									
								$line = "Error CreateVarNumericSingle -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																							
								
						}									
						else {
								$vNameQOption=$vQuestionT. ' ' .$strAttrib['value'];
								//etiquetas a  variables
								$err=$obj->myspssSetVarLabel($fH,$vQOption,$vNameQOption);
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error spssSetVarLabel -- Multiple Choice');									
									$line = "Error spssSetVarLabel -- Multiple Choice". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																	
								}
								else {
										//asignaciòn de valores en vista de variables
										$err=$obj->myspssSetVarNValueLabel($fH,$vQOption,'1',translate("Yes"));				
										if ($err!=SPSS_OK) {
											$bErr=true;
											//die('Error spssSetVarNValueLabel -- CreateVarMultipleChoice');									
											$line = "Error spssSetVarNValueLabel -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
											array_push($afile, $line);
											ErrorSPSS($fileName);
											die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																													
										}										
										else {
											$err=$obj->myspssSetVarNValueLabel($fH,$vQOption,'2',translate("No"));				
											if ($err!=SPSS_OK) {
												$bErr=true;
												//die('Error spssSetVarNValueLabel -- CreateVarMultipleChoice');									
												$line = "Error spssSetVarNValueLabel -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
												array_push($afile, $line);
												ErrorSPSS($fileName);
												die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																	
													
											}																			
										}		
								}
						}																			
						$strMultRespDefs.= ' ' . $vQOption;						
						
						//si la mchoice es de catalogo, enseguida creamos atributos antes de pasar a la sig respuesta del conjunto de respuestas
						//esto para que se vea un orden en las variables del archivo de este tipo con sus respectivos atributos
						AddMChoiceAttributes($aRepository,$qST,$objQ,$obj,$fH,$intQuestionID,$vQOption,$i,$iDataSourceID,$regCaptura,$strAttrib['value']);											
						$i++;
				}
		}
    else
	{
			$qoption = BITAMQuestionOptionCollection::NewInstance($aRepository, $intQuestionID);
			foreach ($qoption as $objQOption) {				
				$strQoption=(string) $objQOption->QuestionOptionID;
				$vQOption=$varQuestion . '_' . $i;
				//print ($vQOption);
				if ($objQ->SectionType == sectNormal) {																					
					$arrQuestionsStruct['QuestionsDef'][$intQuestionID][$objQOption->QuestionOptionName] = $vQOption;
				}
				if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {					
					$arrQuestionsStruct['QuestionsDef'][$intQuestionID.'_'.$regCaptura][$objQOption->QuestionOptionName] = $vQOption;
				}				
				$arrQuestionsStruct['QuestionsTypes'][$vQOption] = spssNumericVariable;
				$arrQuestionsStruct['QuestionsVars'][$vQOption] = sspsDefaultMCNoValue;
				$arrQuestionsStruct['QuestionsIDs'][$vQOption] = $intQuestionID;
				$err=CreateVarNumericSingle($obj,$fH,$vQOption,qtpSingle);
				if ($err!=SPSS_OK) {
						$bErr=true;
						//die('Error CreateVarNumericSingle -- CreateVarMultipleChoice');									
						$line = "Error CreateVarNumericSingle -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																	
						
				}									
				else {
						$vNameQOption=$vQuestionT. ' ' .$objQOption->QuestionOptionName;
						//etiquetas a  variables
						$err=$obj->myspssSetVarLabel($fH,$vQOption,$vNameQOption);
						if ($err!=SPSS_OK) {
							$bErr=true;
							//die('Error spssSetVarLabel -- Multiple Choice');									
							$line = "Error spssSetVarLabel -- Multiple Choice". ','.date('Y-m-d H:i:s'); 
							array_push($afile, $line);
							ErrorSPSS($fileName);
							die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																									
						}
						else {
								//asignaciòn de valores en vista de variables
								$err=$obj->myspssSetVarNValueLabel($fH,$vQOption,'1',translate("Yes"));				
								if ($err!=SPSS_OK) {
									$bErr=true;
									//die('Error spssSetVarNValueLabel -- CreateVarMultipleChoice');									
									$line = "Error spssSetVarNValueLabel -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
									array_push($afile, $line);
									ErrorSPSS($fileName);
									die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																																			
								}										
								else {
									$err=$obj->myspssSetVarNValueLabel($fH,$vQOption,'2',translate("No"));				
									if ($err!=SPSS_OK) {
										$bErr=true;
										//die('Error spssSetVarNValueLabel -- CreateVarMultipleChoice');									
										$line = "Error spssSetVarNValueLabel -- CreateVarMultipleChoice". ','.date('Y-m-d H:i:s'); 
										array_push($afile, $line);
										ErrorSPSS($fileName);
										die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																									
											
									}																			
								}		
						}
				}	
				$strMultRespDefs.= ' ' . $vQOption;
				$i++;
			}	//end foreach	
				
	}	
	$strReturn = (string) $err . '|' . $strMultRespDefs;					
	return $strReturn; 
}	

function AddSChoiceAttributes($aRepository,$qST,$objQ,$obj,$fH,$sVQOption,$sKeyField) {
	
	global $arrQuestionsStruct;
	global $objQuestionsCollByID;
	global $afile;
		//para casos de preguntas de catalogo schoice 
		//solo se crean y configuran las variables para el archivo SPSS
		
//	foreach ($questions as $objQuestion) {
		//if ($objQuestion->SectionType != sectNormal) {
	$bErr=false;		
	$strQuestionID = (string) $objQ->QuestionID;
	$intQuestionID = $objQ->QuestionID;
	//$objQuestionsCollByID[$intQuestionID] = $objQ;
	
	$valueSourceType = $objQ->ValuesSourceType;
	$iiDataSourceID = $objQ->DataSourceID;
	
	$datasourcemembers = BITAMDataSourceMemberCollection::NewInstance($aRepository, $iiDataSourceID);    
		if ($valueSourceType == tofCatalog){				
			if ($objQ->QTypeID == qtpSingle) {
				foreach($datasourcemembers as $objDSM) {
				//foreach ($qST as $objquestionsAttrib) {							
					//if (($objquestionsAttrib->ValuesSourceType == tofCatalog) && ($objquestionsAttrib->DataSourceID == $iiDataSourceID)) {									
						$iDataSourceMemberID = $objDSM->MemberID;  //$objquestionsAttrib->DataSourceMemberID;
						if ($objQ->DataSourceMemberID != $iDataSourceMemberID) {
							$questionTextAttrib = $objDSM->MemberName;
							$objQuestionsCollByID[$iDataSourceMemberID] = $objQ;								
							$vQuestion=$sVQOption.'_attr_'.$iDataSourceMemberID;	
							/*
							if ($objQ->SectionType == sectNormal) {																	
								if (strlen($objQ->AttributeName) == 0) {	
									$vQuestion=QFieldSPSS.$strQuestionID.'_attr_'.$iDataSourceMemberID;	
								}
								else {
									$vQuestion=$objQ->AttributeName.'_attr_'.$iDataSourceMemberID;
								}
							}
							
							if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {																
								if (strlen($objQ->AttributeName) == 0) {	
									$vQuestion=QFieldSPSS.$strQuestionID.'_rec'.$sKeyField.'_attr_'.$iDataSourceMemberID;	
								}
								else {
									$vQuestion=$objQ->AttributeName.'_rec'.$sKeyField.'_attr_'.$iDataSourceMemberID;
								}									
							}							
							*/
							
							//esto se utilizarà para vaciar los datos en el archivo SPSS		
							
							if ($objQ->SectionType == sectNormal) {	
								$arrQuestionsStruct['QuestionsDef'][$objQ->QuestionID.'_'.$iDataSourceMemberID] = $vQuestion;
							}
							if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {	
								$arrQuestionsStruct['QuestionsDef'][$objQ->QuestionID.'_rec'.$sKeyField.'_'.$iDataSourceMemberID] = $vQuestion;								
							}	
							$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssStringVariable;
							$arrQuestionsStruct['QuestionsVars'][$vQuestion] = '';
							$arrQuestionsStruct['QuestionsIDs'][$vQuestion] = $iDataSourceMemberID;
							
							//se crea la variable para el archivo SPSS
							//print($i);
							$err=$obj->myspssSetVarName($fH,$vQuestion,255);	
							if ($err!=SPSS_OK) {
								$bErr=true;
								//print($err);
								//die('Error spssSetVarName -- single choice attributes');
								$line = "Error spssSetVarName -- single choice attributes". ','.date('Y-m-d H:i:s'); 
								array_push($afile, $line);
								ErrorSPSS($fileName);
								die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																									
									
								}
							else {
									//numèrico (2) o alfanumérico (1) o desconocido (0)
									$err=$obj->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
									if ($err!=SPSS_OK) {
										$bErr=true;
										//die('Error spssSetVarMeasureLevel -- single choice attributes');									
										$line = "Error spssSetVarMeasureLevel -- single choice attributes". ','.date('Y-m-d H:i:s'); 
										array_push($afile, $line);
										ErrorSPSS($fileName);
										die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																									
											
									}																	
								}

							if (!$bErr){
								$pos=strrpos($vQuestion, "Selector");
								if ($pos === false) {
									$vNameQuestion=$objQ->QuestionText.' '.$questionTextAttrib;
									//print $vNameQuestion;
									//etiquetas a  variables
									$err=$obj->myspssSetVarLabel($fH,$vQuestion,$vNameQuestion);
									if ($err!=SPSS_OK) {
										$bErr=true;
										//die('Error spssSetVarLabel -- single choice attributes');									
										$line = "Error spssSetVarLabel -- single choice attributes". ','.date('Y-m-d H:i:s'); 
										array_push($afile, $line);
										ErrorSPSS($fileName);
										die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																																				
									}																	
								}	
							}
						}				
					//}							
				}//end foreach datasourcemembers		
			}
		}				
	//}	
	//}   //end foreach atributos schoice catalogo	
	//print('QuestionsDef al terminar AddSChoiceAttributes');
	//PrintMultiArray($arrQuestionsStruct['QuestionsIDs']);
}

function AddMChoiceAttributes($aRepository,$qST,$objQ,$obj,$fH,$iintQuestionId,$sVQOption,$sKeyField,$iiDataSourceID,$iRegCaptura,$strAttribValue) {
	global $arrQuestionsStruct;
	global $objQuestionsCollByID;
	global $afile;
		//para casos de preguntas de catalogo mchoice 
		//solo se crean y configuran las variables de los atributos para el archivo SPSS		
	$bErr=false;		
	//$strQuestionID = (string) $iintQuestionId;
		    
		//print($iiDataSourceID);	
		//die();

	$datasourcemembers = BITAMDataSourceMemberCollection::NewInstance($aRepository, $iiDataSourceID);    		
	//foreach ($qST as $objquestionsAttrib) {			
	foreach($datasourcemembers as $objDSM) {
		//if (($objquestionsAttrib->ValuesSourceType == tofCatalog) && ($objquestionsAttrib->DataSourceID == $iiDataSourceID)) {									
		$iDataSourceMemberID = $objDSM->MemberID;  //$objquestionsAttrib->DataSourceMemberID;
		if ($objQ->DataSourceMemberID != $iDataSourceMemberID) {
			$questionTextAttrib = $objDSM->MemberName;
			$objQuestionsCollByID[$iDataSourceMemberID] = $objQ;
			
			if ($objQ->SectionType == sectNormal) {	
				$vQuestion=$sVQOption.'_attr_'.$iDataSourceMemberID;
			}
			
			if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {	
				$vQuestion=$sVQOption.'_attr_'.$iDataSourceMemberID;					
				/*	
				if (strlen($objQ->AttributeName) == 0) {	
					$vQuestion=QFieldSPSS.$strQuestionID.'_rec'.$sKeyField.'_attr_'.$iDataSourceMemberID;	
				}
				else {
					$vQuestion=$objQ->AttributeName.'_rec'.$sKeyField.'_attr_'.$iDataSourceMemberID;
				}
				*/	
			}												
			//esto se utilizarà para vaciar los datos en el archivo SPSS		
			
			if ($objQ->SectionType == sectNormal) {	
				$arrQuestionsStruct['QuestionsDef'][$objQ->QuestionID.'_'.$strAttribValue.'_'.$iDataSourceMemberID] = $vQuestion;
			}
			if (($objQ->SectionType == sectMasterDet) || ($objQ->SectionType == sectInline)) {	
				$arrQuestionsStruct['QuestionsDef'][$objQ->QuestionID.'_'.$strAttribValue.'_rec_'.$iRegCaptura.'_'.$iDataSourceMemberID] = $vQuestion;
			}	
			$arrQuestionsStruct['QuestionsTypes'][$vQuestion] = spssStringVariable;
			$arrQuestionsStruct['QuestionsVars'][$vQuestion] = '';
			$arrQuestionsStruct['QuestionsIDs'][$vQuestion] = $iDataSourceMemberID;
			
			//se crea la variable para el archivo SPSS
			//print($i);
			$err=$obj->myspssSetVarName($fH,$vQuestion,255);	
			if ($err!=SPSS_OK) {
				$bErr=true;
				//print($err);
				//die('Error spssSetVarName -- single choice attributes');
				$line = "Error spssSetVarName -- single choice attributes". ','.date('Y-m-d H:i:s'); 
				array_push($afile, $line);
				ErrorSPSS($fileName);
				die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																																									
				}
			else {
					//numèrico (2) o alfanumérico (1) o desconocido (0)
					$err=$obj->myspssSetVarMeasureLevel($fH,$vQuestion,SPSS_MLVL_NOM);
					if ($err!=SPSS_OK) {
						$bErr=true;
						//die('Error spssSetVarMeasureLevel -- single choice attributes');									
						$line = "Error spssSetVarMeasureLevel -- single choice attributes". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																																											
					}																	
				}

			if (!$bErr){
				$pos=strrpos($vQuestion, "Selector");
				if ($pos === false) {
					$vNameQuestion=$objQ->QuestionText.' '.$questionTextAttrib;
					//print $vNameQuestion;
					//etiquetas a  variables
					$err=$obj->myspssSetVarLabel($fH,$vQuestion,$vNameQuestion);
					if ($err!=SPSS_OK) {
						$bErr=true;
						//die('Error spssSetVarLabel -- single choice attributes');									
						$line = "Error spssSetVarLabel -- single choice attributes". ','.date('Y-m-d H:i:s'); 
						array_push($afile, $line);
						ErrorSPSS($fileName);
						die("(".__METHOD__.") ".translate("Failed to write file to disk"));																																																																																																																				
							
					}																	
				}	
			}
		}				
		//}							
	}		
	//print('QuestionsDef al terminar AddSChoiceAttributes');
	//PrintMultiArray($arrQuestionsStruct['QuestionsIDs']);
}	
	
	
//$sSpathfileError
function ErrorSPSS($sfileName){
	global $sSpathfileError;
	global $afile;
		
	$LN = chr(13).chr(10);	
	$blnWriteFile = true;
	$sPathFile = $sfileName.$sSpathfileError;
	$fStats = fopen($sPathFile, 'w+');
	//print('si entra aquí');
	if ($fStats)
	{
		if(!fwrite($fStats, implode($LN, $afile)))
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to write file contents in '.$sPathFile, 3, GeteFormsLogPath().'ExportSPSS.txt');
			//@JAPR
		}

		fclose($fStats);
	}
	else
	{
		$blnWriteFile = false;
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log('\r\n'.'Unable to open to write file contents in '.$sPathFile, 3, GeteFormsLogPath().'ExportSPSS.txt');
		//@JAPR
	}

	if($blnWriteFile)
	{
		if(!is_file($sPathFile))
		{
			echo ("(".__METHOD__.") "."Error file path:{$sPathFile}");
			exit();
		}
		else
		{
		}
	}
	else
	{
		echo ("(".__METHOD__.") "."Error file path:{$sPathFile}");
		exit();
	}

}
	
?>