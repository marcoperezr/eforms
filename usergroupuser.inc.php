<?php 

require_once("repository.inc.php");
require_once("usergroup.inc.php");
require_once("appUser.inc.php");
require_once("../model_manager/modeldata.inc.php");

class BITAMUserGroupUserCollection extends BITAMCollection
{
	public $UserGroupID;
	
	function __construct($aRepository, $aUserGroupID)
	{
		BITAMCollection::__construct($aRepository);
		$this->UserGroupID = $aUserGroupID;
	}

	static function NewUserGroupUserCollection($aRepository, $aUserGroupID, $anArrayOfUserIDs = null)
	{
		$anInstance = new BITAMUserGroupUserCollection($aRepository, $aUserGroupID);

		$filter = "";
		if (!is_null($anArrayOfUserIDs))
		{
			switch (count($anArrayOfUserIDs))
			{
				case 0:
					break;
				case 1:
					$filter = "AND t1.CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
					break;
				default:
					foreach ($anArrayOfUserIDs as $aUserID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int) $aUserID; 
					}
					if ($filter != "")
					{
						$filter = "AND t1.CLA_USUARIO IN (".$filter.")";
					}
					break;
			}
		}

//		$sql = "SELECT A.CLA_USUARIO, A.NOM_CORTO AS ArtusUser, A.NOM_LARGO AS UserName, A.CUENTA_CORREO AS Email, B.SV_Admin, B.SV_User, B.UserID FROM SI_USUARIO A LEFT JOIN SI_SV_Users B ON A.CLA_USUARIO = B.CLA_USUARIO ".$where." ORDER BY A.NOM_LARGO";
		
		$sql = 
		'SELECT t1.CLA_USUARIO AS UserID '.
		'		,t1.CLA_USUARIO '.
		'		,t1.NOM_CORTO AS UserName '.
		'		,t1.NOM_CORTO AS artususer '.
		'		,B.SV_Admin, B.SV_User, B.UserID '.
		'		,t1.PASSWORD AS Password '.
		'		,t1.PASSWORDV AS PasswordV '.		
		'		,t1.NOM_LARGO AS FullName '.
		'		,t1.CUENTA_CORREO AS EMail '.
		'		,t1.ARTUS_ADMIN AS IsArtusAdministrator '.
		'		,t1.ARTUS_DESIGNER AS IsArtusDesigner '.
		'		,t1.ARTUS_DESKTOP AS IsArtusUser '.
		'		,t1.PAPIRO_ADMIN AS IsPapiroAdministrator '.
		'		,t1.PAPIRO_DESIGNER AS IsPapiroDesigner '.
		'		,t1.PAPIRO_DESKTOP AS IsPapiroUser '.		
		'		,t1.ADVISOR_ADMIN AS IsAdvisorAdministrator '.
		'		,t1.ADVISOR_DESKTOP AS IsAdvisorUser '.
		'		,t1.STRATEGO_ADMIN AS IsStrategoAdministrator '.
		'		,t1.STRATEGO_DESKTOP AS IsStrategoUser '.
		'		,t1.EKTOS_ADMIN AS IsEktosAdministrator '.
		'		,t1.EKTOS_USER AS IsEktosUser '.						
		'		,t1.CACHE AS CacheClean '.
		'		,t1.NOANACOL AS CollaborativeAnalisysSupport '.
		'		,t1.EASTITLE AS HeaderEA '.
		'		,t1.CLA_OWNER AS OwnerID '.
		'		,t2.NOM_CORTO AS OwnerName '.
		'		,t1.TIPO_USUARIO AS UserType '.		
		'		,t1.CONFIG_SECURITY as ConfigSecurity '.
		'		,t1.INICIALES as Iniciales '.
		'		,t1.LAST_DISABLED as LastDisabled '.		
		'		,t1.NO_EXPORTAR as NoExportar '.		
		'FROM 	SI_USUARIO t1 INNER JOIN SI_USUARIO t2 ON (t2.CLA_USUARIO = t1.CLA_OWNER) INNER JOIN SI_ROL_USUARIO t3 ON (t3.CLA_USUARIO = t1.CLA_USUARIO) LEFT JOIN SI_SV_Users B ON (t1.CLA_USUARIO = B.CLA_USUARIO) '.
		'WHERE	 '.
		'  t3.CLA_ROL = '.$aUserGroupID.' '.$filter;

		if (InModeSAAS())
		{
			$sql = $sql.' and t1.CLA_USUARIO > 0 ';
		}
		
		$sql = $sql.' ORDER BY 2';
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_USUARIO table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupUser::NewUserGroupUserFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewUserGroupUserCollectionForAssociation($aRepository, $aUserGroupID)
	{
		$anInstance = new BITAMUserGroupUserCollection($aRepository, $aUserGroupID);

		$sql = 
		'SELECT t1.CLA_USUARIO AS UserID '.
		'		,t1.CLA_USUARIO '.
		'		,t1.NOM_CORTO AS UserName '.
		'		,t1.NOM_CORTO AS artususer '.
		'		,B.SV_Admin, B.SV_User, B.UserID '.
		'		,t1.PASSWORD AS Password '.
		'		,t1.PASSWORDV AS PasswordV '.		
		'		,t1.NOM_LARGO AS FullName '.
		'		,t1.CUENTA_CORREO AS EMail '.
		'		,t1.ARTUS_ADMIN AS IsArtusAdministrator '.
		'		,t1.ARTUS_DESIGNER AS IsArtusDesigner '.
		'		,t1.ARTUS_DESKTOP AS IsArtusUser '.
		'		,t1.PAPIRO_ADMIN AS IsPapiroAdministrator '.
		'		,t1.PAPIRO_DESIGNER AS IsPapiroDesigner '.
		'		,t1.PAPIRO_DESKTOP AS IsPapiroUser '.		
		'		,t1.ADVISOR_ADMIN AS IsAdvisorAdministrator '.
		'		,t1.ADVISOR_DESKTOP AS IsAdvisorUser '.
		'		,t1.STRATEGO_ADMIN AS IsStrategoAdministrator '.
		'		,t1.STRATEGO_DESKTOP AS IsStrategoUser '.
		'		,t1.EKTOS_ADMIN AS IsEktosAdministrator '.
		'		,t1.EKTOS_USER AS IsEktosUser '.						
		'		,t1.CACHE AS CacheClean '.
		'		,t1.NOANACOL AS CollaborativeAnalisysSupport '.
		'		,t1.EASTITLE AS HeaderEA '.
		'		,t1.CLA_OWNER AS OwnerID '.
		'		,t2.NOM_CORTO AS OwnerName '.
		'		,t1.TIPO_USUARIO AS UserType '.		
		'		,t1.CONFIG_SECURITY as ConfigSecurity '.
		'		,t1.INICIALES as Iniciales '.
		'		,t1.LAST_DISABLED as LastDisabled '.		
		'		,t1.NO_EXPORTAR as NoExportar '.
		'FROM 	SI_USUARIO t1 INNER JOIN SI_USUARIO t2 ON ( t2.CLA_USUARIO = t1.CLA_OWNER ) LEFT JOIN SI_SV_Users B ON (t1.CLA_USUARIO = B.CLA_USUARIO) '.
		'WHERE 1 = 1 ';

		if (InModeSAAS())
		{
			$sql = $sql.' and t1.CLA_USUARIO > 0 ';
		}
		
		$sql = $sql.' ORDER BY 2';
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_USUARIO table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupUser::NewUserGroupUserFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function NewUserGroupUserCollectionForSaving($aRepository, $aUserGroupID, $anArrayOfUserIDs)
	{
		$anInstance = new BITAMUserGroupUserCollection($aRepository, $aUserGroupID);

		$filter = "";
		switch (count($anArrayOfUserIDs))
		{
			case 0:
				break;
			case 1:
				$filter = "AND t1.CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
				break;
			default:
				foreach ($anArrayOfUserIDs as $aUserID)
				{
					if ($filter != "")
					{
						$filter .= ", ";
					}
					$filter .= (int) $aUserID; 
				}
				if ($filter != "")
				{
					$filter = "AND t1.CLA_USUARIO IN (".$filter.")";
				}
				break;
		}

		$sql = 
		'SELECT t1.CLA_USUARIO AS UserID '.
		'		,t1.CLA_USUARIO '.
		'		,t1.NOM_CORTO AS UserName '.
		'		,t1.NOM_CORTO AS artususer '.
		'		,B.SV_Admin, B.SV_User, B.UserID '.
		'		,t1.PASSWORD AS Password '.
		'		,t1.PASSWORDV AS PasswordV '.		
		'		,t1.NOM_LARGO AS FullName '.
		'		,t1.CUENTA_CORREO AS EMail '.
		'		,t1.ARTUS_ADMIN AS IsArtusAdministrator '.
		'		,t1.ARTUS_DESIGNER AS IsArtusDesigner '.
		'		,t1.ARTUS_DESKTOP AS IsArtusUser '.
		'		,t1.PAPIRO_ADMIN AS IsPapiroAdministrator '.
		'		,t1.PAPIRO_DESIGNER AS IsPapiroDesigner '.
		'		,t1.PAPIRO_DESKTOP AS IsPapiroUser '.		
		'		,t1.ADVISOR_ADMIN AS IsAdvisorAdministrator '.
		'		,t1.ADVISOR_DESKTOP AS IsAdvisorUser '.
		'		,t1.STRATEGO_ADMIN AS IsStrategoAdministrator '.
		'		,t1.STRATEGO_DESKTOP AS IsStrategoUser '.
		'		,t1.EKTOS_ADMIN AS IsEktosAdministrator '.
		'		,t1.EKTOS_USER AS IsEktosUser '.						
		'		,t1.CACHE AS CacheClean '.
		'		,t1.NOANACOL AS CollaborativeAnalisysSupport '.
		'		,t1.EASTITLE AS HeaderEA '.
		'		,t1.CLA_OWNER AS OwnerID '.
		'		,t2.NOM_CORTO AS OwnerName '.
		'		,t1.TIPO_USUARIO AS UserType '.		
		'		,t1.CONFIG_SECURITY as ConfigSecurity '.
		'		,t1.INICIALES as Iniciales '.
		'		,t1.LAST_DISABLED as LastDisabled '.		
		'		,t1.NO_EXPORTAR as NoExportar '.		
		'FROM 	SI_USUARIO t1 INNER JOIN SI_USUARIO t2 ON ( t2.CLA_USUARIO = t1.CLA_OWNER ) LEFT JOIN SI_SV_Users B ON (t1.CLA_USUARIO = B.CLA_USUARIO) '.
		'WHERE 1 = 1 '.
		$filter.
		'  AND  t1.CLA_USUARIO NOT IN '.
		'         (SELECT t3.CLA_USUARIO '.
		'            FROM SI_ROL_USUARIO t3 '.
		'            WHERE  t3.CLA_ROL = '.$aUserGroupID.') '.
		' ORDER BY 2';
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_USUARIO table: ".$anInstance->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMUserGroupUser::NewUserGroupUserFromRS($anInstance->Repository, $aUserGroupID, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest)
	{
		if (array_key_exists("UserGroupID", $aHTTPRequest->GET))
		{
			$action = array_key_exists("action", $aHTTPRequest->GET) ? $aHTTPRequest->GET["action"] : "";
			$aUserGroupID = $aHTTPRequest->GET["UserGroupID"];
			if (array_key_exists("CLA_USUARIO", $aHTTPRequest->POST))
			{
				$aUserID = $aHTTPRequest->POST["CLA_USUARIO"];
				if (is_array($aUserID))
				{
					$anArrayOfUserIDs = $aUserID;
					switch ($action)
					{
						case 'associate':
							$filter = "";
							switch (count($anArrayOfUserIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_USUARIO <> ".((int) $anArrayOfUserIDs[0]);
									break;
								default:
									foreach ($anArrayOfUserIDs as $aUserID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aUserID; 
									}
									if ($filter != "")
									{
										$filter = " AND CLA_USUARIO NOT IN (".$filter.")";
									}
									break;
							}
							$sql = "select CLA_USUARIO as cla_usuario from SI_ROL_USUARIO WHERE CLA_ROL = ".((int) $aUserGroupID).$filter;
							$aUsersDeleted = array();
							
							$rst = $aRepository->ADOConnection->Execute($sql);
							$ncont = 0;
							if ($rst)
							{
								while (!$rst->EOF)
								{
									$aUsersDeleted[$ncont] = $rst->fields["cla_usuario"];
									$ncont++;
									$rst->MoveNext();
								}
							}
							
							$sql = "DELETE FROM SI_ROL_USUARIO WHERE CLA_ROL = ".((int) $aUserGroupID).$filter;
							if ($aRepository->ADOConnection->Execute($sql) === false)
							{
								die("Error accessing SI_ROL_USUARIO table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
							}
							
							savelogfile($sql);

							$aCollection = BITAMUserGroupUserCollection::NewUserGroupUserCollectionForSaving($aRepository, $aUserGroupID, $anArrayOfUserIDs);
							foreach ($aCollection as $anInstanceToAssociate)
							{
// Dentro de esta funcion, exportamos a PHP los usuarios que quedaron dentro del grupo de usuario								
								$anInstanceToAssociate->saveAssociation();
							}
// Exportamos a PHP los usuarios que fueron quitados del grupo de usuario							
							for ($ncont=0; $ncont < count($aUsersDeleted); $ncont++)
							{
								ExportUserToPHP($aRepository, $aUsersDeleted[$ncont]);
							}
							
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
						case 'dissociate':
							$filter = "";
							switch (count($anArrayOfUserIDs))
							{
								case 0:
									break;
								case 1:
									$filter = " AND CLA_USUARIO = ".((int) $anArrayOfUserIDs[0]);
									break;
								default:
									foreach ($anArrayOfUserIDs as $aUserID)
									{
										if ($filter != "")
										{
											$filter .= ", ";
										}
										$filter .= (int) $aUserID; 
										
									}
									if ($filter != "")
									{
										$filter = " AND CLA_USUARIO IN (".$filter.")";
									}
									break;
							}
							if ($filter != "")
							{
								$sql = "DELETE FROM SI_ROL_USUARIO WHERE CLA_ROL = ".((int) $aUserGroupID).$filter;
								if ($aRepository->ADOConnection->Execute($sql) === false)
								{
									die("Error accessing SI_ROL_USUARIO table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
								}
								foreach ($anArrayOfUserIDs as $aUserID)
								{
// Exportamos a PHP los usuarios que fueron quitados del grupo de usuario
									ExportUserToPHP($aRepository, (int) $aUserID);
								}
							}
							$aCollection = BITAMUserGroupUserCollection::NewUserGroupUserCollection($aRepository, $aUserGroupID);
							$aHTTPRequest->RedirectTo = $aCollection;
							break;
					}
					return null;
				}
			}
			$anInstance = null;
			switch ($action)
			{
				case 'associate':
				case 'dissociate':
					$aHTTPRequest->RedirectTo = BITAMUserGroupUserCollection::NewUserGroupUserCollection($aRepository, $aUserGroupID);
					break;
				default:
					$anInstance = BITAMUserGroupUserCollection::NewUserGroupUserCollectionForAssociation($aRepository, $aUserGroupID);
					break;
			}
			return $anInstance;
		}
		return null;
	}
	
	function get_AssociatedIDs()
	{
		$anArray = array();

		$sql = 
		"SELECT t1.CLA_USUARIO AS UserID, t1.NOM_CORTO AS UserName ".
		"FROM 	SI_USUARIO t1, SI_ROL_USUARIO t2 ".
		"WHERE	t2.CLA_USUARIO = t1.CLA_USUARIO ".
		"  AND  t2.CLA_ROL = ".((int) $this->UserGroupID).
		" ORDER BY 2";
		
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		while (!$aRS->EOF)
		{
			$anArray[(int) $aRS->fields["userid"]] = strtoupper(rtrim($aRS->fields["username"]));
			$aRS->MoveNext();
		}
		
		return $anArray;
	}
	
	function get_Parent()
	{
		return BITAMUserGroup::NewUserGroupWithID($this->Repository, $this->UserGroupID);
	}
	
	function get_Image()
	{
		return "<img src=images/usuarios.gif>";
	}
	
	function get_Title()
	{
		return translate("Users");
	}

	function get_QueryString()
	{
		return $this->get_Parent()->get_QueryString()."&BITAM_SECTION=UserGroupUserCollection";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=UserGroupUser&UserGroupID=".$this->UserGroupID;
	}


	function get_AssociateDissociateQueryString()
	{
		if (InModeSAAS())
		{
			if ($this->UserGroupID == 1)
			{
// Si es el grupo de usuario Administradores, solo se muestra la lista pero no se permite manipular la configuracion
				return null;				
			}
		}

		return "BITAM_SECTION=UserGroupUserCollection&UserGroupID=".$this->UserGroupID;
	}

	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'CLA_USUARIO';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();

		if($_SESSION["PAFBM_Mode"]!=1)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = "UserName";
			$aField->Title = translate("Full Name");
			$aField->Type = "String";
			$aField->Size = 255;
			$myFields[$aField->Name] = $aField;
		}

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Email";
		$aField->Title = translate("E-Mail");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}	
	
}

class BITAMUserGroupUser extends BITAMObject
{
	public $UserGroupID;
	public $User;
	
	function __construct($aRepository, $aUserGroupID)
	{
		BITAMObject::__construct($aRepository);
		$this->UserGroupID = (int) $aUserGroupID;
		$this->User = BITAMAppUser::NewInstance($aRepository);
	}

	static function NewUserGroupUser($aRepository, $aUserGroupID)
	{
		return new BITAMUserGroupUser($aRepository, $aUserGroupID);
	}

	static function NewUserGroupUserWithUserID($aRepository, $aUserGroupID, $aUserID)
	{
		
		$anInstance = null;
		if (((int) $aUserGroupID) < 0)
		{
			return $anInstance;
		}
		if (((int) $aUserID) < 0)
		{
			return $anInstance;
		}
		$sql = 
		'SELECT t1.CLA_USUARIO AS UserID '.
		'		,t1.NOM_CORTO AS UserName '.
		'		,t1.NOM_CORTO AS artususer '.
		'		,t1.PASSWORD AS Password '.
		'		,t1.PASSWORDV AS PasswordV '.		
		'		,t1.NOM_LARGO AS FullName '.
		'		,t1.CUENTA_CORREO AS EMail '.
		'		,t1.ARTUS_ADMIN AS IsArtusAdministrator '.
		'		,t1.ARTUS_DESIGNER AS IsArtusDesigner '.
		'		,t1.ARTUS_DESKTOP AS IsArtusUser '.
		'		,t1.PAPIRO_ADMIN AS IsPapiroAdministrator '.
		'		,t1.PAPIRO_DESIGNER AS IsPapiroDesigner '.
		'		,t1.PAPIRO_DESKTOP AS IsPapiroUser '.		
		'		,t1.ADVISOR_ADMIN AS IsAdvisorAdministrator '.
		'		,t1.ADVISOR_DESKTOP AS IsAdvisorUser '.
		'		,t1.STRATEGO_ADMIN AS IsStrategoAdministrator '.
		'		,t1.STRATEGO_DESKTOP AS IsStrategoUser '.
		'		,t1.EKTOS_ADMIN AS IsEktosAdministrator '.
		'		,t1.EKTOS_USER AS IsEktosUser '.				
		'		,t1.CACHE AS CacheClean '.
		'		,t1.NOANACOL AS CollaborativeAnalisysSupport '.
		'		,t1.EASTITLE AS HeaderEA '.
		'		,t1.CLA_OWNER AS OwnerId '.
		'		,t2.NOM_CORTO AS OwnerName '.
		'		,t1.TIPO_USUARIO AS UserType '.		
		'		,t1.CONFIG_SECURITY as ConfigSecurity '.
		'		,t1.INICIALES as Iniciales '.
		'		,t1.LAST_DISABLED as LastDisabled '.				
		'		,t1.NO_EXPORTAR as NoExportar '.		
		'from 	SI_USUARIO t1, SI_USUARIO t2, SI_ROL_USUARIO t3 '.
		'WHERE	t2.CLA_USUARIO = t1.CLA_OWNER '.
		'  AND  t3.CLA_USUARIO = t1.CLA_USUARIO '.
		'  AND  t3.CLA_ROL = '.((int) $aUserGroupID).
		'  AND  t1.CLA_USUARIO = '.((int) $aUserID);

		$aRS = $aRepository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("Error accessing SI_USUARIO table: ".$aRepository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMUserGroupUser::NewUserGroupUserFromRS($aRepository, $aUserGroupID, $aRS);
		}
		return $anInstance;
	}

	static function NewUserGroupUserFromRS($aRepository, $aUserGroupID, $aRS)
	{
		$anInstance = BITAMUserGroupUser::NewUserGroupUser($aRepository, $aUserGroupID);
		$anInstance->User = BITAMAppUser::NewInstanceFromRS($aRepository, $aRS);
		return $anInstance;
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("UserGroupID", $aHTTPRequest->GET))
		{
			$aUserGroupID = (int) $aHTTPRequest->GET["UserGroupID"];
			if (array_key_exists("UserID", $aHTTPRequest->POST))
			{
				$aUserID = $aHTTPRequest->POST["UserID"];
				if (is_array($aUserID))
				{
					$aCollection = BITAMUserGroupUserCollection::NewUserGroupUserCollection($aRepository, $aUserGroupID, $aUserID);
					foreach ($aCollection as $anInstanceToRemove)
					{
						$anInstanceToRemove->remove();
					}
					$aHTTPRequest->RedirectTo = $aCollection;
				}
				else
				{
					$anInstance = BITAMUserGroupUser::NewUserGroupUserWithUserID($aRepository, $aUserGroupID, $aUserID);
					if (is_null($anInstance))
					{
						$anInstance = BITAMUserGroupUser::NewUserGroupUser($aRepository, $aUserGroupID);
					}
					$anInstance->updateFromArray($aHTTPRequest->GET);
					$anInstance->updateFromArray($aHTTPRequest->POST);
					$anInstance->save();
					if (array_key_exists("go", $_POST))
					{
						$go = $_POST["go"];
					}
					else
					{
						$go = "self";
					}
					if ($go == "parent")
					{
						$anInstance = $anInstance->get_Parent();
					}
					else
					{
						if ($go == "new")
						{
							$anInstance = BITAMUserGroupUser::NewUserGroupUser($aRepository, $aUserGroupID);
						}
					}
					$aHTTPRequest->RedirectTo = $anInstance;
				}
				return null;
			}
			$anInstance = null;
			if (array_key_exists("UserID", $aHTTPRequest->GET))
			{
				$aUserID = $aHTTPRequest->GET["UserID"];
				$anInstance = BITAMUserGroupUser::NewUserGroupUserWithUserID($aRepository, $aUserGroupID, $aUserID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMUserGroupUser::NewUserGroupUser($aRepository, $aUserGroupID);
				}
			}
			else
			{
				$anInstance = BITAMUserGroupUser::NewUserGroupUser($aRepository, $aUserGroupID);
			}
			return $anInstance;
		}
		return null;
	}

	function updateFromArray($anArray)
	{
 		if (array_key_exists("UserGroupID", $anArray))
		{
			$this->UserGroupID = (int) $anArray["UserGroupID"];
		}
		$this->User->updateFromArray($anArray);
		return $this;
	}

	function saveAssociation()
	{
		$sql = "INSERT INTO SI_ROL_USUARIO (".
					"CLA_ROL".
					",CLA_USUARIO".
					") VALUES (".
					((int) $this->UserGroupID).
					",".((int) $this->User->CLA_USUARIO).
					")";
		if ($this->Repository->ADOConnection->Execute($sql) === false)
		{
			die("Error accessing SI_ROL_USUARIO table: ".$this->Repository->ADOConnection->ErrorMsg().". Executing: ".$sql);
		}
		
		ExportUserToPHP($this->Repository, (int) $this->User->CLA_USUARIO);
		
		return $this;
	}

	function isNewObject()
	{
		return ($this->User->CLA_USUARIO < 0);
	}

	function get_Image()
	{
		return "<img src=images/usuarios.gif>";
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New User");
		}
		else
		{
			return $this->User->UserName;
		}
	}

	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=UserGroupUser&UserGroupID=".$this->UserGroupID;
		}
		else
		{
//			return "BITAM_PAGE=UserGroupUser&UserGroupID=".$this->UserGroupID."&UserID=".$this->User->UserID;
			return "BITAM_PAGE=AppUser&UserID=".$this->User->CLA_USUARIO;
		}
	}

	function get_Parent()
	{
		return BITAMUserGroupUserCollection::NewUserGroupUserCollection($this->Repository, $this->UserGroupID);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'CLA_USUARIO';
	}

	function get_FieldValueOf($aFieldName)
	{
		if ($aFieldName == 'UserGroupID')
    	{
    		$aFieldValue = $this->get_FieldValueOf($aFieldName);
    	}
    	else
    	{
    		$aFieldValue = $this->User->get_FieldValueOf($aFieldName);
    	}
		return $aFieldValue;
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		return $this->User->get_FormFields($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		// @EA 2005-06-22
		$RepositoryType = $_SESSION["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			$RepositoryType = $_COOKIE["BITAM_RepositoryType"];
		if (is_null($RepositoryType))
			// Produccion
			$RepositoryType = 2;		

		if ($RepositoryType == 1)
		{
			//@JAPR 2015-07-10: Corregido un bug, estas clases deben usar la variable de sesión de eForms
			$UserName = @$_SESSION["PABITAM_UserName"];
			if (is_null($UserName))
				$UserName = $_COOKIE["BITAM_Name"];
			if (is_null($UserName))
				$UserName = "SUPERVISOR";
			if ( $UserName == $this->User->OwnerName )
				return true;
			else
				return false;
		}
		else 
			return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		// @EA 2005-06-22
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		return $this->canRemove($aUser);
	}	

}

?>