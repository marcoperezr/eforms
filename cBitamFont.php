<?php
require_once("cBitamGridSel.php");

class cBitamFont
{
	public $name;
	public $align      = 'right';
	public $img_closed = 'images/down.gif';
	public $arr_fonts  = null; //array(	'times'   => 'Times New Roman',	'arial'   => 'Arial perrona-.ttf', 'courier' => 'Courier');
	public $comb_fonts = null; //array( 'times-.ttf'  => array('font' => 'times', 'bold' => '',        'italic' => ''), 'times-b.ttf' => array('font' => 'times', 'bold' => 'checked', 'italic' => ''), 'times-bi.ttf'=> array('font' => 'times', 'bold' => 'checked', 'italic' => 'checked'), 'timesi.ttf' => array('font' => 'times', 'bold' => '',        'italic' => 'checked'),
	public $arr_sizes  = null; //array( '3' =>'3 pto',   '4'=>'4 pto',   '5'=>'5 pto',   '6'=>'6 pto',   '7'=>'7 pto',

    function BITAMGetSiguiente(&$sCadena, $Token = ',')
    {
    	$npos = strpos($sCadena, $Token);
    	if ($npos !== false)
    	{
    		$stmp = substr($sCadena, 0, $npos);
    		$sCadena = substr($sCadena, $npos + strlen($Token));
    		return $stmp;
    	}
    	else
    	{
            if (strlen(trim($sCadena)) > 0)
            {
            	$tmpCadena = $sCadena;
            	$sCadena = '';
                return $tmpCadena;
            }
    		else
    		{
                return '';
    		}
    	}
    }

    function CheckFontFile($file)
    {
        $aChartFonts=null;
    	if(file_exists(dirname(__FILE__).'/chart/'.$file))
    	{
    	    $aChartFonts = array();
    		$lines       = file(dirname(__FILE__).'/chart/'.$file);
        	foreach ($lines as $line)
        	{
        	    $line = trim($line);
        	    if ((strlen($line)>0))
        	    {
        	        $line    = explode('=', $line);
        	        $line[0] = trim($line[0]);
        	        $line[1] = trim($line[1]);
        	        if(count($line)>=2)
        	            $aChartFonts[$line[0]]=explode(',', $line[1]);
        	    }
        	}
       	}
        return($aChartFonts);
    }

    public function __destruct()
    {
        unset($this->comb_fonts);
    	unset($this->arr_fonts);
    	unset($this->arr_sizes);
    }

	public function cBitamFont($name)
	{
	   $this->name       = $name;
	   $this->arr_fonts  = array();
	   $this->comb_fonts = array();
	   $this->arr_sizes  = array();

	   global $aChartFonts;
	   //if($aChartFonts==null && count($aChartFonts)==0) ;
	       $aChartFonts=$this->CheckFontFile('fonts.txt');

	   //Fonts Names
	   foreach ($aChartFonts as $key=>$font)
	   {
	       //Clave y Descripcion(Clave Fromateada)
	       $this->arr_fonts[$key]=ucwords($key);
	       //Adicionar los arrays de las fuentes disponibles)
	       $j=count($aChartFonts[$key]);

           $f=$font[0]; $f=$this->BITAMGetSiguiente($f, '.ttf');
           $this->comb_fonts[$font[0]]                       = array('font'=>$key, 'bold'=>'',        'italic'=>''       );
           $this->comb_fonts[$j>=2?$font[1]:$f.'_NA_B.ttf']  = array('font'=>$key, 'bold'=>'checked', 'italic'=>''       );
           $this->comb_fonts[$j>=3?$font[2]:$f.'_NA_I.ttf']  = array('font'=>$key, 'bold'=>'',        'italic'=>'checked');
           $this->comb_fonts[$j>=4?$font[3]:$f.'_NA_BI.ttf'] = array('font'=>$key, 'bold'=>'checked', 'italic'=>'checked');
	   }

	   //Font Sizes
	   for($i=3; $i<=40; $i++) $this->arr_sizes[$i]= $i.' pto';
	}


	public function colorDecToHex2($dec)
	{
		if ($dec >= 0 )
		{
			$color = dechex($dec);
			$color = substr('000000', strlen($color)).$color;
			$color = '#'.substr($color, 4, 2).substr($color, 2, 2).substr($color, 0, 2);
		}
		else
			$color = -1;
		return($color);
	}

	public function getFontChart($fontName, $bBold=false, $bItalic=false)
	{
	   global $aChartFonts;
	   //Fonts Names
	   $fontName=strtolower($fontName);
	   foreach ($aChartFonts as $key=>$font)
	   {    //Buscar el nombre normal de la fuente
			if($fontName==$key)
			{
			   $j=count($aChartFonts[$key]);
			   if($j>=4 && $bBold && $bItalic) return($font[3]);
			   if($j>=3 && $bItalic)           return($font[2]);
			   if($j>=2 && $bBold)             return($font[1]);
			   return($font[0]);
			}
	   }
	   return('calibri.ttf');
	}

	public function getFontDesc($FontKey)
	{
	   global $aChartFonts;
       if(strpos($FontKey, '-')===false)  $FontKey=str_ireplace('.ttf', '-.ttf', $FontKey);
       $aTemp               = explode('-', $FontKey);
       $FontKey             = $aTemp[0].'.ttf';
	   foreach ($aChartFonts as $key=>$font)
	   {
	   	  for($i=0; $i<count($aChartFonts[$key]); $i++)
	   	  	  if($aChartFonts[$key][$i]==$FontKey)
	   	  	  	  return($key);
	   }
	   return('arial');
	}

	public function gen_control($left, $top, $item_def, $type, $width, $bUseForGraph = true, $bDisplay=true)
	{
		$sDisplay = ($bDisplay) ? 'table' : 'none';
		$html_Code = '<table cellspacing="0" border="0" id="TB_'.$this->name.'" style="display:'.$sDisplay.';">
						<tr>
							<td class=selector_left style="font-family: Verdana, Arial, Helvetica, sans-serif; color: #59512b; font-size: 11px;">
								<img src="images/font.gif" height="15px" width="17px"></td>
							<td class=selector_right style="cursor:pointer;" onclick="show_hide_sel(\'id_'.$type.'_'.$this->name.'\', this, \''.$this->align.'\');" width="12px" align=center>
								<img id="id_'.$type.'_'.$this->name.'_img_up_down" src="images/advanced_search.gif" style="vertical-align:middle"></td></tr></table>';
		$html_Code .= '<div id=id_'.$type.'_'.$this->name.' style="background-color:white; position:absolute; top:'.$top.'; left:'.$left.'; width:'.$width.'; border:1px solid #dfd2c0; display:none; z-index:100;"><table>';
		//onmouseout="set_Tempor(\'id_div_'.$this->name.'\');" onmouseover="clear_Tempor();"
		unset($sDisplay);
		if ($bUseForGraph) {
			if (!in_array($item_def[1],$this->arr_sizes))
			{
				$this->arr_sizes[$item_def[1]] = $item_def[1].' pto';
				ksort($this->arr_sizes);
				reset($this->arr_sizes);
	    	}

	//LogFileDebug("cbitamfont.log", "item_def[0]=".$item_def[0]);
	        $aTemp               = $item_def[0];
	        $aTemp               = explode('-', $aTemp);
	        $underline           = (strpos($aTemp[1], 'u.ttf')!==false?'checked':'');
	        $aTemp[1]            = str_ireplace('u.ttf', '.ttf', $aTemp[1]);
	        //Tipo
	        global $aChartFonts;
	  	    //if($aChartFonts==null && count($aChartFonts)==0) ;
		    //$aChartFonts=$this->CheckFontFile('fonts.txt');

	        if(isset($aChartFonts[$aTemp[0]]))
	        {
	        	if ($aTemp[1]=='bi.ttf')    //boldItalic
	        		$fontName = (count($aChartFonts[$aTemp[0]])==4 ? $aChartFonts[$aTemp[0]][3]  : 'arialbi.ttf');
	        	elseif ($aTemp[1]=='i.ttf') //italic
	        		$fontName = (count($aChartFonts[$aTemp[0]])>=3 ? $aChartFonts[$aTemp[0]][2]  : 'ariali.ttf');
	        	elseif ($aTemp[1]=='b.ttf') //bold
	        		$fontName = (count($aChartFonts[$aTemp[0]])>=2 ? $aChartFonts[$aTemp[0]][1]  : 'arialb.ttf');
	        	else                        //normal
	        		$fontName = $aChartFonts[$aTemp[0]][0];
	        }
	        else
	        {
	            //LogFileDebug("cbitamfont.log", "No se encontro");
	            $fontName = 'arial.ttf';
	        }
		}
		else {
			// Name|Bold|Italic|Underline|Size|BG|FC
			$fontName = $item_def[0];
			$this->comb_fonts[$fontName]['font'] = $item_def[0];
			$this->comb_fonts[$fontName]['bold'] = ($item_def[1]=='1'?'checked':'');
			$this->comb_fonts[$fontName]['italic'] = ($item_def[2]=='1'?'checked':'');
			$underline = ($item_def[3]=='1'?'checked':'');
			$item_def[1] = $item_def[4];
		}

//$html_Code .='<script language="JavaScript">alert("'.$this->name.'");</script>'; //$item_def[0]
        // FN        FS FC BC R
        //times-.ttf|10|0| -1|0_F_

		$html_Code .= '<tr><td class=dataLabel2 colspan=2 align=center><script language="JavaScript">document.write(ML[288]);</script>&nbsp;&nbsp;&nbsp;';
		if ($bUseForGraph) {
	        if(array_key_exists($fontName, $this->comb_fonts)==false) {
	            $fontName='arial.ttf';
	        }
		}

		$html_Code .= '<select class=dataLabel2 name="font_family_'.$this->name.'" id="id_font_family_'.$this->name.'">'.get_select_options($this->arr_fonts, $this->comb_fonts[$fontName]['font']).'</select></td>';

		$html_Code .= '<td class=dataLabel2 colspan=2 align=center><script language="JavaScript">document.write(ML[289]);</script>&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<select name="font_size_'.$this->name.'" id="id_font_size_'.$this->name.'">'.get_select_options($this->arr_sizes, $item_def[1]).'</select></td></tr>';
		$html_Code .= '<tr><td class=dataLabel2 colspan=4 align=center><input name="font_bold_'.$this->name.'" id="id_font_bold_'.$this->name.'" type="checkbox" style="background-color:transparent; border: none;" '.$this->comb_fonts[$fontName]['bold'].'>&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<script language="JavaScript">document.write(ML[290]);</script>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<input name="font_italic_'.$this->name.'" id="id_font_italic_'.$this->name.'" type="checkbox" style="background-color:transparent; border: none;" '.$this->comb_fonts[$fontName]['italic'].'>&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<script language="JavaScript">document.write(ML[291]);</script>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<input name="font_underline_'.$this->name.'" id="id_font_underline_'.$this->name.'" type="checkbox" style="background-color:transparent; border: none;" '.$underline.'>&nbsp;&nbsp;&nbsp;';
		$html_Code .= '<script language="JavaScript">document.write(ML[428]);</script></td></tr>';

		$html_Code .= '<tr><td class="dataLabel2" align="center">';
		$color_sel = new cBitamGridSel('nieto_font_bgcolor_'.$this->name);
		if ($bUseForGraph) {
			$rgb_color = $this->colorDecToHex2($item_def[3]);
		}
		else {
			$rgb_color = $item_def[5];
		}
		$rgb_color_hint = $rgb_color;
		if ($rgb_color_hint == -1)
		{
			$rgb_color_hint = getMLText(416);
			if ($rgb_color_hint == '')
			{
				$rgb_color_hint = $rgb_color;
			}
		}
		$html_Code .= $color_sel->gen_selector_color(array('key' => $rgb_color, 'image' => $rgb_color, 'hint' => $rgb_color_hint));
		$html_Code .= '</td>';
		$html_Code .= '<td class=dataLabel2><script language="JavaScript">document.write(ML[292]);</script></td>';
		$html_Code .= '<td class="dataLabel2" align="center">';

		$color_sel = new cBitamGridSel('nieto_font_forecolor_'.$this->name);
		if ($bUseForGraph) {
			$rgb_color = $this->colorDecToHex2($item_def[2]);
		}
		else {
			$rgb_color = $item_def[6];
		}
		$rgb_color_hint = $rgb_color;
		if ($rgb_color_hint == -1)
		{
			$rgb_color_hint = getMLText(416);
			if ($rgb_color_hint == '')
			{
				$rgb_color_hint = $rgb_color;
			}
		}
		$html_Code .= $color_sel->gen_selector_color(array('key' => $rgb_color, 'image' => $rgb_color, 'hint' => $rgb_color_hint));
		$html_Code .= '</td>';
		$html_Code .= '<td class=dataLabel2 id=textFontColor><script language="JavaScript">document.write(ML[359]);</script></td></tr>';

		$html_Code .= '</table></div>';
		return $html_Code;
	}
}

function getMLText($sKey)
{
	global $userlang;
	$sTextRet = '';
	switch ($sKey)
	{
		case 416:
			if (isset($userlang) && $userlang)
			{
				switch ($userlang)
				{
					case 'BE':
					case 'EN':
						$sTextRet='Transparent';
						break;
					case 'BZ':
					case 'PT':
						$sTextRet='Transparente';
						break;
					case 'CA':
						$sTextRet='Transparent';
						break;
					case 'ES':
					case 'SP':
						$sTextRet='Transparente';
						break;
					case 'IT':
						$sTextRet='Trasparente';
						break;
					case 'NL':
						$sTextRet='Transparant';
						break;
				}
			}
			break;

	}
	return $sTextRet;
}
?>