<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$strPath = "";
$surveyID = -1;
$reportID = -1;

if(array_key_exists("surveyID",$_GET) && array_key_exists("reportID",$_GET))
{
	$surveyID = $_GET["surveyID"];
	$reportID = $_GET["reportID"];

	$sql = "SELECT PathImage FROM SI_SV_SurveySignatureImg WHERE FactKey = ".$reportID." AND SurveyID = ".$surveyID;
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	
	if($aRS && !$aRS->EOF)
	{
		$strPath = $aRS->fields["pathimage"];
	}
}

if (trim($strPath) != "")
{
	$path = "surveysignature/".$theRepositoryName."/".$strPath;
	$path = str_replace("\\", "/", $path);
	header("Location: ".$path);
}
?>