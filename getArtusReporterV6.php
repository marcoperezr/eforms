<?php
include_once("AdvisorHTTPRequest.class.php");

function getArtusReporterV6($aDestination, $cla_value, $aRepository)
{
	//@JAPR 2014-09-08: Agregado el log de requests
	global $blnSaveRequestsLog;
	
	$aConnection = $aRepository->ADOConnection;
	//obtenemos las claves de los escenarios

	$cla_dimensions = 0;
	$cla_user = 0;
	$cla_concepto = 0;
	$validStagesAttachments = array();
	$validReportsAttachments = array();
	if ($aDestination->emailformat == ddeftDashboard) {
		$validStagesAttachments[] = $aDestination->cla_dashboar;
	}else if ($aDestination->emailformat == ddeftARS) {
		$validReportsAttachments[] = $aDestination->cla_ars;
	}
	
	$cla_dimensions = $aDestination->cla_dimension;
	/*@JRPP 2015-15-02: De momento el usuario se quedara fijo en 1 que es el usuario administrador*/
	$cla_user = 1;

	if(count($validReportsAttachments) == 0 && count($validStagesAttachments) == 0)
	{
		return false;
	}
	
	$webHTMLURL = generateURL($cla_user, $validStagesAttachments, $validReportsAttachments, $cla_dimensions, $cla_value, $aConnection);
	//@JAPR 2014-09-08: Agregado el log de requests
	try {
		if ($blnSaveRequestsLog) {
			$strReqRepository = (string) @$aRepository->ADOConnection->databaseName;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			$strReqFileName = GeteFormsLogPath()."WebHTMLRequests_{$strReqRepository}.log";
			//@JAPR
			$intReqSurveyID = (int) @$_POST["surveyID"];
			$strReqDate = (string) @$_POST["surveyDate"];
			$strReqHour = (string) @$_POST["surveyHour"];
			$strReqFullDate = "";
			if ($strReqDate && $strReqHour) {
				$strReqFullDate = (string) @substr($strReqDate, 0, 10)." ".substr($strReqHour, 0, 8);
			}
			$intReqUserID = (int) @$_SESSION["PAtheUserID"];
			
			$strReqMsg = "Request for User='{$intReqUserID}', Survey='{$intReqSurveyID}', Date='{$strReqFullDate}': {$webHTMLURL}";
			@error_log($strReqMsg, 3, $strReqFileName);
		}
	} catch (Exception $e) {}
	//@JAPR
	
	//@JARP 2012-06-25: Agregada validación para reportar errores de generación del PDF a partir de escenarios
	$blnHTTPError = false;
	$intHTTPError = 0;
	try
	{
		if(!is_dir('tmp'))
		{
			mkdir("tmp");
		}
		
		$nCount = 0;
		do 
		{
			$eachFileName = createUniqueTemporaryFilename('tmp', 'pdf');
			if($nCount == 1)
			{
				//a partir del segundo intento agregamos el modo debug
				$webHTMLURL = $webHTMLURL . "&bDebug=1";
			}
			
			// get the file from the server
			$fopenOptions = array('timeout'=>120);
			//@JARP 2012-06-25: Agregada validación para reportar errores de generación del PDF a partir de escenarios
			$blnHTTPError = false;
			$aCon = Advisor_fopen($webHTMLURL, $fopenOptions, $blnHTTPError, $intHTTPError);
			//@JAPR
			
			$dest = fopen($eachFileName, 'wb');
			fwrite($dest, $aCon);
			fclose($dest);
			@error_log("Nombre de Archivo: " . $eachFileName , 3, $strReqFileName);
			$tempFilename = $eachFileName;
			
			$nCount = $nCount + 1;
			
		//mientras el tamaño del archivo descargado sea menor de 2k bytes y solo 3 intentos
		}while (filesize($tempFilename) < 2000 && $nCount < 3);
		
		//si ya fueron los tres intentos y no se descargo correctamente el archivo
		if(filesize($tempFilename) < 2000)
		{
			require_once("bpaemailconfiguration.inc.php");
	
			$anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
			
			if(!is_null($anInstanceEmailConf))
			{
				$SMTPServer = $anInstanceEmailConf->fbm_email_server;	
				$SMTPPort = $anInstanceEmailConf->fbm_email_port;
				$SMTPUserName = $anInstanceEmailConf->fbm_email_username;
				
				if (strlen($SMTPServer) != 0 && strlen($SMTPPort) != 0 && strlen($SMTPUserName) != 0) 
				{
					$SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
					//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
					//que sólo incluya eForms su versión si no lo hizo ya eBavel
					if(!class_exists("PHPMailer")){
						require_once("mail/class.phpmailer.php");
					}	
					//@JAPR
					
					$mail = new PHPMailer();
					$mail->Mailer = "smtp";
					$mail->Host = $SMTPServer;
					$mail->Port = $SMTPPort;
					$mail->SMTPAuth = TRUE;
					//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
					//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
					$mail->SMTPAutoTLS = false;
					//$mail->SMTPDebug = false;
					//$mail->Debugoutput = 'echo';
					//$mail->SMTPSecure = '';
					//$mail->CharSet = 'UTF-8';
					//@JAPR
					$mail->Username = $SMTPUserName;
					$mail->Password = $SMTPPassword;
				
					
					$mail->From = $anInstanceEmailConf->fbm_email_source;
								
					$mail->IsHTML(TRUE);
					$mail->Subject = 'Error esurvey envio de correo pdf schedulerID: ' . $schedulerID;
					$mail->AddAddress("aramirez@bitam.com", "aramirez");
					$mail->AddAddress("iretta@bitam.com", "iretta");
					$mail->AddAddress("hgarza@kpionline.com", "hgarza");
					$mail->AddAddress("internal_esurvey@bitam.com", "esurvey");
					$mail->Body = "Fallo el envio de correo: url: " . $webHTMLURL;
					//@JARP 2012-06-25: Agregada validación para reportar errores de generación del PDF a partir de escenarios
					if ($blnHTTPError)
					{
						$mail->Body .= "\r\n\r\nHTTP Error: ".$intHTTPError;
					}
					//@JAPR
					AddCuentaCorreoAltToMail($theUser, $mail);
					$mail->Send();
				}
				return false;
			}
		}

		return $tempFilename;
	}
	catch (Exception $e)
	{
		//$e->getMessage();
		//@JARP 2012-06-25: Agregada validación para reportar errores de generación del PDF a partir de escenarios
		require_once("bpaemailconfiguration.inc.php");

		$anInstanceEmailConf = BPAEmailConfiguration::readConfiguration();
		
		if(!is_null($anInstanceEmailConf))
		{
			$SMTPServer = $anInstanceEmailConf->fbm_email_server;	
			$SMTPPort = $anInstanceEmailConf->fbm_email_port;
			$SMTPUserName = $anInstanceEmailConf->fbm_email_username;
			
			if (strlen($SMTPServer) != 0 && strlen($SMTPPort) != 0 && strlen($SMTPUserName) != 0) 
			{
				$SMTPPassword = $anInstanceEmailConf->fbm_email_pwd;
				//@JAPR 2012-10-10: Validada la integración con eBavel, como eBavel mandará eMails, incluye primero la clase PhPMailer así que se debe validar
				//que sólo incluya eForms su versión si no lo hizo ya eBavel
				if(!class_exists("PHPMailer")){
					require_once("mail/class.phpmailer.php");
				}	
				//@JAPR
				
				$mail = new PHPMailer();
				$mail->Mailer = "smtp";
				$mail->Host = $SMTPServer;
				$mail->Port = $SMTPPort;
				$mail->SMTPAuth = TRUE;
				//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
				//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
				$mail->SMTPAutoTLS = false;
				//$mail->SMTPDebug = false;
				//$mail->Debugoutput = 'echo';
				//$mail->SMTPSecure = '';
				//$mail->CharSet = 'UTF-8';
				//@JAPR
				$mail->Username = $SMTPUserName;
				$mail->Password = $SMTPPassword;
			
				
				$mail->From = $anInstanceEmailConf->fbm_email_source;
							
				$mail->IsHTML(TRUE);
				$mail->Subject = 'Error esurvey envio de correo pdf schedulerID: ' . $schedulerID;
				$mail->AddAddress("aramirez@bitam.com", "aramirez");
				$mail->AddAddress("iretta@bitam.com", "iretta");
				$mail->AddAddress("hgarza@kpionline.com", "hgarza");
				$mail->AddAddress("internal_esurvey@bitam.com", "esurvey");
				$mail->Body = "Fallo el envio de correo: url: " . $webHTMLURL;
				if ($blnHTTPError)
				{
					$mail->Body .= "\r\n\r\nHTTP Error: ".$intHTTPError;
				}
				$mail->Body .= "\r\n".$e->getMessage();
				AddCuentaCorreoAltToMail($theUser, $mail);
				$mail->Send();
			}
			return false;
		}
		//@JAPR
		
		return false;
	}

}

function generateURL($cla_user, $validStagesAttachments, $validReportsAttachments, $cla_dimensions, $cla_value, $aConnection)
{
	$webHTML = getConfigurationKeyFromDB(615, $aConnection);
	$webHTMLProject = getConfigurationKeyFromDB(616, $aConnection);
	
	//obtenemos el user nom_corto y su passsword
	$sql = "SELECT NOM_CORTO, PASSWORD FROM SI_USUARIO WHERE CLA_USUARIO = {$cla_user}";
	$rs = $aConnection->execute($sql);	
	if($rs === false)
	{
		return false;
	}
	
	//nom_corto
	$username = $rs->fields['NOM_CORTO'];
	$userpassword = trim(BITAMDecryptPassword($rs->fields['PASSWORD']));
	
	//obtenemos el cla_concepto
	$sql = "SELECT CLA_CONCEPTO FROM si_cpto_llave WHERE CLA_DESCRIP = {$cla_dimensions}";
	
	$rs = $aConnection->execute($sql);	
	if($rs === false)
	{
		return false;
	}
	$cla_concepto = $rs->fields['CLA_CONCEPTO'];
	if($cla_concepto < 10)
	{
		$cla_concepto = '0'.$cla_concepto;
	}
	
	$sql = "select consecutivo from si_cpto_llave where cla_descrip = {$cla_dimensions}";
	$rs = $aConnection->execute($sql);	
	if($rs === false)
	{
		return false;
	}
	$keyDimension = $rs->fields['consecutivo'];
	if($keyDimension < 10)
	{
		$keyDimension = '0'.$keyDimension;
	}
	
	$filters = "DIM_{$cla_concepto}_{$keyDimension}_{$cla_value}";
		
	// Get the authorization username/password for the Web HTML Server
	$webAuthUsername = getConfigurationKeyFromDB(609, $aConnection);
	$webAuthPassword = getConfigurationKeyFromDB(610, $aConnection);
	if (!is_null($webAuthPassword))
		$webAuthPassword = BITAMDecryptPassword($webAuthPassword);

	// Assign the authorization information to the 
	if (!is_null($webAuthUsername) && !is_null($webAuthPassword))
	{
	 	$fopenOptions['authUsername'] = $webAuthUsername;
	 	$fopenOptions['authPassword'] = $webAuthPassword;
	}
	
	$webHTMLURL = $webHTML . "/AdvisorInfoBookPrinter.php?";
	
	$webHTMLURL .= "repository=" . BITAMEncode($webHTMLProject);
	
	$webHTMLURL .= "&user=" . BITAMEncode($username);
	$webHTMLURL .= "&password=" . BITAMEncode($userpassword);
		
	$objList = "";
	foreach ($validStagesAttachments as $eachAttachment)
	{
		if ($objList != "")
			$objList .= ",";

		$objList .= $eachAttachment;
	}
	if($objList != '')
	{
		$webHTMLURL .= "&stage=";
		$webHTMLURL .= BITAMEncode($objList);
	}
	
	$objList = "";
	foreach ($validReportsAttachments as $eachAttachment)
	{
		if ($objList != "")
			$objList .= ",";

		$objList .= $eachAttachment;
	}

	if($objList != '')
	{
		$webHTMLURL .= "&report=";
		$webHTMLURL .= BITAMEncode($objList);
	}
	
	// add the format information
	$webHTMLURL .= "&fmt=" . BITAMEncode('PDF');
	$webHTMLURL .= "&sps=" . BITAMEncode($filters);
	
	return $webHTMLURL;
}

function getConfigurationKeyFromDB($aKey, $aConnection)
{
	$aSQL = "select CLA_CONFIGURA, CLA_USUARIO, REF_CONFIGURA from SI_CONFIGURA  where CLA_USUARIO = -2 and CLA_CONFIGURA = " .  $aKey;

	$recordSet = $aConnection->Execute($aSQL);
	if ($recordSet->EOF)
		$result = null;
	else
		$result = $recordSet->fields['REF_CONFIGURA'];

	$recordSet->close();

	return ($result);
}

function Advisor_fopen($anURL, &$options = array(), &$bHTTPError = false, &$iHTTPError)
{
	$http = new AdvisorHTTPRequest($anURL);

	if (isset($options['timeout']))
		$http->_timeout = $options['timeout'];

	if (isset($options['authUsername']))
		$http->_authUsername = $options['authUsername'];

	if (isset($options['authPassword']))
		$http->_authPassword = $options['authPassword'];

	if (isset($options['usePOST']))
		$http->_usePOST = $options['usePOST'];

	$http->execute();

	$options['lastURL'] = $http->_url;
	$options['lastHeaders'] = $http->headers;

	if ($http->code >= 400)
	{
		$iHTTPError = $http->code;
		$bHTTPError = true;
	 	if (isset($options['exception']))
			throw new Exception($http->statusCode, $http->code);
		else
			return(null);
	}
	else
		return($http->body);
}

function createUniqueTemporaryFilename($tempDir, $ext = 'tmp')
{
	//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
	//Asigna el prefijo que se utilizará como nombre de archivo de PDF
	global $strPDFFileNamePrefix;
	global $intPDFFilaNamePrefixCont;
	
	$strPrefix = 'bft';
	if (trim((string) @$strPDFFileNamePrefix) != '') {
		$strPrefix .= '_'.$strPDFFileNamePrefix.'_';
	}
	
	//Modificado para no hacer un ciclo infinito sino utilizar una combinación de parámetros de la captura para generar nombres únicos
	$intPDFFilaNamePrefixCont++;
	$aFilename = $tempDir ."/". $strPrefix . sprintf("%04x", $intPDFFilaNamePrefixCont) . ".{$ext}";
	/*
	do
	{
		$aFilename = $tempDir ."/". "bft" . sprintf("%04x", rand(0, 32768)) . ".{$ext}";
	}
	while (file_exists($aFilename));
	*/
	
	return $aFilename;

}

?>