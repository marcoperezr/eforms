<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}
session_start();

$repName = "";
if(array_key_exists("repName",$_GET))
{
	$repName = $_GET["repName"];
}

$passwordType = 0;
if(array_key_exists("passwordType",$_GET))
{
	$passwordType = (int) $_GET["passwordType"];
}

if($passwordType==1)
{
	$titlePassword="MetaData";
}
else
{
	$titlePassword="Data";
}

?>
<HTML>
<HEAD>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$titlePassword?></title>
<link rel="stylesheet" type="text/css" href="css/default2.css">
<link rel="stylesheet" type="text/css" href="css/tree.css">
<link rel="stylesheet" type="text/css" href="css/main.css">

	<script language="JavaScript" type="text/javascript" src="js/browserSniffer.js"></script>
	<script language="JavaScript">
		var MONTH_NAMES = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
		var MONTH_ABBR = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		var DAY_NAMES = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
		var DAY_ABBR = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
		var MONTH_REGEXP = '(';
		for (var _i = MONTH_NAMES.length - 1; _i >= 0; _i--)
		{
			if (_i == MONTH_NAMES.length - 1)
			{
				MONTH_REGEXP += MONTH_NAMES[_i];
			}
			else
			{
				MONTH_REGEXP += ('|' + MONTH_NAMES[_i]);
			}
			MONTH_REGEXP += ('|' + MONTH_ABBR[_i]);
			if ((_i + 1) < 10)
			{
				MONTH_REGEXP += ('|0' + (_i + 1));
			}
			MONTH_REGEXP += ('|' + (_i + 1));
		}
		MONTH_REGEXP += ')?';
		
		var DECIMAL_SYMBOL = '.';
		var DIGIT_GROUPING_SYMBOL = ',';
	</script>
	<script language="JavaScript" type="text/javascript" src="js/calendar.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/main.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/autofill.js"></script>
	<script language="JavaScript" type="text/javascript" src="js/common.js"></script>
	<SCRIPT LANGUAGE="JavaScript" SRC="BITAMXMLObject.js"></SCRIPT>

<SCRIPT LANGUAGE="JavaScript">

function BITAMPassword__Ok()
{
	var newPasswordStr = BITAMPassword_SaveForm.NewPassword._value;

	window.top.returnValue = newPasswordStr;

	//Se agrego esta instruccion, ya que Safari no funciona con window.top.returnValue
	window.returnValue = newPasswordStr;

	top.close();
}

function BITAMPassword__Cancel()
{
    window.top.returnValue = null;
    window.returnValue = null;
    top.close();
}

</SCRIPT>
</HEAD>

<BODY style="background-color:#f5f5f5" >

<div class="collection_title"><?=$titlePassword?><hr></div>

<div class="object_buttons">
	<input type="image" title="Save" id="BITAMPassword_OkButton" class="object_okButton" src="images/ok.png" onclick="javascript: BITAMPassword__Ok();">
	<input type="image" title="Cancel" id="BITAMPassword__CancelButton" class="object_cancelButton" src="images/cancel.png" onclick="javascript: BITAMPassword__Cancel();">
</div>


<form id="BITAMPassword_SaveForm" >
<table class="object_table">
	<tr><td>&nbsp;</td></tr>
	<tr id="Row_NewPassword">
		<td class="object_field_title">
			Password
			</td>
		<td colspan="3" class="object_field_value_3">
					<input type="password"
			        id=""
					name="NewPassword"
					size="17"
					maxlength=""
					value=""
					_value=""
					onfocus="javascript:
								var v = formatString(this['_value'], 'N');
								this.value = v;
								if (this.createTextRange)
								{
									var r = this.createTextRange();
									r.moveEnd('character', v.length);
									r.select();
									}"
					onkeypress="javascript: event.returnValue = validateFormat(this, unformatStringRegExpStr('N'), event.keyCode);"
					onblur="javascript:
    							var v = unformatString(this.value, 'N');
    							this['_value'] = v;
								this.value = formatString(v, 'N');"
					>
		</td>
	</tr>
</table>
</form>
</BODY>
</HTML>