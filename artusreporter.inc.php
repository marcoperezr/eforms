<?php

include_once("surveyScheduler.inc.php");
include_once("AdvisorDashboardGroupCollection.class.php");
include_once("AdvisorDashboardGroup.class.php");
include_once("AdvisorDashboard.class.php");
include_once("AdvisorDashboardFilters.class.php");

//include_once("getArtusReporter.php");

class BITAMArtusReporter extends BITAMObject
{
	public $id;
	public $schedulerID;
	public $surveyID;
	public $tipo_objeto;
	public $cla_objeto;
	public $cla_dimension;
	public $nom_escenario;
	public $keyObjeto;
	
	public $dashboardPublicGroup = array();
	public $onlydashboard = array();
	public $onlyreports = array();
	
	public $Repository;
	
	function __construct($aRepository, $schedulerID, $surveyID)
	{
		$this->id = 0;
		$this->Repository = $aRepository;
		$this->schedulerID = $schedulerID;
		$this->surveyID = $surveyID;
		$this->tipo_objeto = 0;
		$this->cla_objeto = 0;
		
		$cla_usuario = $_SESSION['PABITAM_UserID'];
		
		$aRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
		$aDashboardCollection = AdvisorDashboardGroupCollection::getUserPubDashboardGroupsCollection(array($cla_usuario), null, $aRepository);
		$aReportCollection = array();
		$arrDashboardReport = array();
		if (getMDVersion()>=esveFormsv6) {

			$aSQL = "SELECT cla_descrip_sv FROM si_sv_modelartus WHERE id_survey = {$surveyID}";
			$aRS = $aRepository->DataADOConnection->execute($aSQL);
			$FactKeyDimID = 0;
			if (!$aRS->EOF) {
				$FactKeyDimID = $aRS->fields['cla_descrip_sv'];
			}

		}else {
			$aSQL = "SELECT FactKeyDimID FROM si_sv_survey WHERE SurveyID = {$surveyID}";
			$aRS = $aRepository->DataADOConnection->execute($aSQL);
			if ($aRS === false)	{
				die( translate("Error accessing")." si_sv_survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
			}
			$FactKeyDimID = 0;
			if (!$aRS->EOF) {
				$FactKeyDimID = $aRS->fields['FactKeyDimID'];
			}
		}
				
		$dashboardPublicGroup = array();
		for($i =0; $i < count($aDashboardCollection->DashboardGroupCollection->Dashboards); $i++)
		{
			$key = $aDashboardCollection->DashboardGroupCollection->Dashboards[$i]->cla_escenario;
			$value = $aDashboardCollection->DashboardGroupCollection->Dashboards[$i]->nom_escenario;
			$components = getComponents($aRepository->ADOConnection, $key, $nType = 140);
			
			$bExist = false;
			foreach ($components as $eachComponent)
			{
				if($eachComponent->Comp->Dimension == $FactKeyDimID)
				{
					$bExist = true;
				}
			}
			
			//if($bExist)
			{
				$dashboardPublicGroup[$key] = "(D) - " . $value;
				$arrDashboardReport["1_".$key] = "(D) - " . $value;
				$this->onlydashboard[$key] = $value;
			}
		}
		
		//reportes de artus, primero sacamos los reportes publicados al usuario
		$aSQL = "SELECT FU.CLA_REPORTE as CLA_REPORTE, F.NOMBRE as NOMBRE, RF.CLA_FOLDER FROM SI_REPORTE_USUARIO FU, SI_REPORTE F, SI_REPORTE_FOLDER RF
				WHERE FU.CLA_USUARIO = {$cla_usuario} AND FU.CLA_REPORTE = F.CLA_REPORTE AND F.CLA_REPORTE=RF.CLA_REPORTE
                ORDER BY F.NOMBRE";
		$aRS = $aRepository->ADOConnection->execute($aSQL);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." REPORTS : ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
		}		
		while (!$aRS->EOF)
		{
			$key = $aRS->fields[0];
			$value = $aRS->fields[1];
			$aReportCollection[$key] = $value;
			$arrDashboardReport["5_".$key] = $value;
			$this->onlyreports[$key] = $value;
			$aRS->MoveNext();
		}		
		
		//despues sacamos los reportes del cual el usuario es propietario
		$aSQL = "select CLA_USUARIO, CLA_REPORTE, NOMBRE from SI_REPORTE WHERE CLA_USUARIO = {$cla_usuario}";
		$aRS = $aRepository->ADOConnection->execute($aSQL);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_REPORTE : ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
		}	
		while (!$aRS->EOF)
		{
			//@JAPR 2013-02-14: Corregido un bug, se estaba asignando como clave de reporte la clave del usuario
			$key = $aRS->fields[1];
			//@JAPR
			$value = $aRS->fields[2];
			$aReportCollection[$key] = "(R) - " . $value;
			$arrDashboardReport["5_".$key] = "(R) - " . $value;
			$this->onlyreports[$key] = $value;
			$aRS->MoveNext();
		}
		
		$this->cla_dimension = $FactKeyDimID;
		$this->dashboardPublicGroup = $arrDashboardReport;
		
		$aRepository->ADOConnection->SetFetchMode(3);
	}
	
	static function NewInstance($aRepository, $schedulerID, $surveyID)
	{
		return new BITAMArtusReporter($aRepository, $schedulerID, $surveyID);
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$anInstance = BITAMArtusReporter::NewInstance($aRepository, (int)$aRS->fields["schedulerID"], (int)$aRS->fields["surveyID"]);
		
		$anInstance->id = $aRS->fields["id"];
		$anInstance->tipo_objeto = $aRS->fields["tipo_objeto"];
		$anInstance->cla_objeto = $aRS->fields["cla_objeto"];
		$anInstance->nom_escenario = @$aRS->fields["nom_escenario"];		
		$anInstance->surveyID = $aRS->fields["surveyID"];		
		$anInstance->keyObjeto = $anInstance->tipo_objeto . "_" . $anInstance->cla_objeto;
				
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("schedulerID", $aHTTPRequest->GET))
		{
			$schedulerID = (int) $aHTTPRequest->GET["schedulerID"];
		}
		else
		{
			$schedulerID = 0;
		}
		
		if (array_key_exists("surveyID", $aHTTPRequest->GET))
		{
			$surveyID = (int) $aHTTPRequest->GET["surveyID"];
		}
		else
		{
			$surveyID = 0;
		}
		
		if (array_key_exists("id", $aHTTPRequest->POST) && is_array($aHTTPRequest->POST['id']))
		{
			$ids = $aHTTPRequest->POST['id'];
			
			$sql = "SELECT A.id, schedulerID, {$surveyID} as surveyID, tipo_objeto, cla_objeto
					FROM SI_SV_artusreporter A
					WHERE id in (". implode(",", $ids) .")";
			
			$aRS = $aRepository->ADOConnection->Execute($sql);
			
			while(!$aRS->EOF)
			{
				$anInstance = BITAMArtusReporter::NewInstanceFromRS($aRepository, $aRS);
				$anInstance->remove();
				$aRS->MoveNext();
			}
			
			$anInstance = $anInstance->get_Parent();
			return $anInstance;
			
		}
		
		/*
		if (array_key_exists("keyObjeto", $aHTTPRequest->GET))
		{
			$keyObjeto = (int) $aHTTPRequest->GET["keyObjeto"];
			$keyObjeto = explode("_", $keyObjeto);
			$tipo_objeto = (int) $keyObjeto[0];
			$cla_objeto = (int) $keyObjeto[1];
		}
		else
		{
			$tipo_objeto = 0;
			$cla_objeto = 0;
		}*/
		
		if (array_key_exists("cla_objeto", $aHTTPRequest->GET))
		{
			$cla_objeto = (int) $aHTTPRequest->GET["cla_objeto"];
		}
		else 
		{
			$cla_objeto = 0;
		}
		
		if (array_key_exists("tipo_objeto", $aHTTPRequest->GET))
		{
			$tipo_objeto = (int) $aHTTPRequest->GET["tipo_objeto"];
		}
		else 
		{
			$tipo_objeto = 0;
		}	
		
		$anInstance = BITAMArtusReporter::NewInstance($aRepository, $schedulerID, $surveyID);
		$anInstance->cla_objeto = $cla_objeto;
		$anInstance->tipo_objeto = $tipo_objeto;
		$anInstance->keyObjeto = $tipo_objeto . "_" . $cla_objeto;
				
		if(array_key_exists("keyObjeto", $aHTTPRequest->POST))
		{
			$key = $aHTTPRequest->POST['keyObjeto'];
			$key = explode("_", $key);
			$anInstance->tipo_objeto = $key[0];
			$anInstance->cla_objeto = $key[1];
			$anInstance->keyObjeto = $aHTTPRequest->POST['keyObjeto'];
			$anInstance->cla_dimension = $aHTTPRequest->POST['cla_dimension'];
			$anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMArtusReporter::NewInstance($aRepository, $schedulerID, $surveyID);
				}
			}
			$aHTTPRequest->RedirectTo = $anInstance;
		}
		
		return $anInstance;
	}
	
	function isNewObject()
	{
		return ($this->tipo_objeto <= 0 && $this->cla_objeto <= 0);
	}
	
	function get_Title()
	{
		return translate("Add")."- ".translate("Dashboard")." / ".translate("Report");
	}
	
	function get_FormIDFieldName()
	{
		return 'schedulerID';
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_Parent()
	{
		return BITAMSurveyScheduler::NewInstanceWithID($this->Repository, $this->schedulerID);
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$optionsGral = $this->dashboardPublicGroup;
		
		$aField = BITAMFormField::NewFormField();
		//$aField->Name = "cla_objeto";
		$aField->Name = "keyObjeto";
		$aField->Title = translate("Name");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = $optionsGral;
		$myFields[$aField->Name] = $aField;
		
		
		return $myFields;
	}
	
    function generateInsideFormCode($aUser)
    {
?>                          
                   <input type="hidden" name="cla_dimension" id="cla_dimension" value="<?php echo $this->cla_dimension; ?>">
<?
    }

	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=ArtusReporter&schedulerID={$this->schedulerID}&surveyID={$this->surveyID}";
		}
		else
		{
			return "BITAM_PAGE=ArtusReporter&schedulerID={$this->schedulerID}&surveyID={$this->surveyID}&cla_objeto={$this->cla_objeto}&tipo_objeto={$this->tipo_objeto}";
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		return false;
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
	
	function save()
	{
		$aSQL = "INSERT INTO si_sv_artusreporter (".
						" schedulerID".
			            ", tipo_objeto".
			            ", cla_objeto".
			            ", cla_dimension".
			            ", cla_user".
			            ") VALUES (".
			            $this->schedulerID .
			            "," . $this->tipo_objeto .
			            "," . $this->cla_objeto .
			            "," . $this->cla_dimension .
			            "," . $_SESSION['PABITAM_UserID'] .
			            ")";
			            
		if($this->Repository->ADOConnection->execute($aSQL) === false)
		{
			die( translate("Error accessing")." SI_SV_artusreporter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
			            
	}
	
	function remove()
	{
		$sql = "DELETE from SI_SV_artusreporter WHERE id = {$this->id}";
		
		if ($this->Repository->ADOConnection->execute($sql) === false)
		{
			die( translate("Error accessing")." SI_SV_artusreporter ".translate("table").": ".$this->Repository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}	
	
}

class BITAMArtusReporterCollection extends BITAMCollection
{
	public $SchedulerID;
	public $SurveyID;
	public $Repository;
	
	function __construct($aRepository, $aUser)
	{
		$this->Repository = $aRepository;
	}
	
	static function NewInstance($aRepository, $aUser, $SchedulerID, $SurveyID)
	{
		$anInstance = new BITAMArtusReporterCollection($aRepository, $aUser);
		
		$anInstance->SchedulerID = $SchedulerID;
		$anInstance->SurveyID = $SurveyID;

		//escenarios de artus
		$sql = "SELECT A.id, schedulerID, {$SurveyID} as surveyID, tipo_objeto, cla_objeto, B.NOM_ESCENARIO 
		FROM SI_SV_artusreporter A, SI_ESCENARIO B 
		WHERE A.schedulerID = {$anInstance->SchedulerID} and A.cla_objeto = B.cla_escenario AND A.tipo_objeto = 1;";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		//$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_artusreporter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$anInstance->Collection = array();
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMArtusReporter::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		//reportes de artus
		$sql = "SELECT A.id, schedulerID, {$SurveyID} as surveyID, tipo_objeto, cla_objeto, B.NOMBRE as NOM_ESCENARIO
		FROM SI_SV_artusreporter A, SI_REPORTE B 
		WHERE A.schedulerID = {$anInstance->SchedulerID} and A.cla_objeto = B.cla_reporte AND A.tipo_objeto = 5";
		
		$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
		//$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_artusreporter ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = BITAMArtusReporter::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}

		return $anInstance;
	}
	
	function get_Title()
	{
		/*if ($this->isNewObject())
		{
			return translate("New")." ".translate("reporter");
		}
		else
		{
			return translate("Reporter")." ".$this->MemberName;
		}*/
		
		return translate("Dashboards")." / ".translate("Reports");
	}
	
	function get_FormIDFieldName()
	{
		return 'id';
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	function get_FormFields($aUser)
	{
		$myFields = array();

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "nom_escenario";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		return $myFields;
	}
	
	function get_AddRemoveQueryString()
	{
		//return "BITAM_PAGE=ArtusReporter&schedulerID={$this->SchedulerID}&surveyID={$this->SurveyID}";
		return "BITAM_PAGE=ArtusReporter&schedulerID={$this->SchedulerID}&surveyID={$this->SurveyID}";
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		return true;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		return true;
	}
}

?>