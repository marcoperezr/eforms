<?php

function ldapLogin($aServer, $aDomain, $aUserName, $aPassword)
{
	$ldap_conn = @ldap_connect('ldap://'.$aServer);
	if ($ldap_conn == false)
	{
		return false;
	}
	if (trim($aDomain) == '')
	{
		return false;
	}
	if (trim($aUserName) == '')
	{
		return false;
	}
	if (trim($aPassword) == '')
	{
		return false;
	}
    @ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
	$ldapLogin = @ldap_bind($ldap_conn, trim($aUserName).'@'.trim($aDomain), trim($aPassword));
	@ldap_unbind($ldap_conn);
	return $ldapLogin;
}
?>