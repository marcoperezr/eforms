<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$catagoryDimID = -1;
if (array_key_exists("CategoryDim", $_GET))
{
	$catagoryDimID = $_GET["CategoryDim"];
}

$surveyID = -1;
if (array_key_exists("SurveyID", $_GET))
{
	$surveyID = $_GET["SurveyID"];
}

$strMembers = "";
$sql= "SELECT CategoryDimName, ElementDimName, ElementIndName FROM SI_SV_Question WHERE SurveyID = ".$surveyID." 
		AND QTypeID = 3 AND QDisplayMode = 0 AND MCInputType = 1 AND IsMultiDimension <> 1 
		AND UseCategoryDim = 1 AND CategoryDimID = ".$catagoryDimID;

$aRS = $theRepository->DataADOConnection->Execute($sql);

if ($aRS === false)
{
	die(translate("Error accessing")." SI_SV_Question ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}

if(!$aRS->EOF)
{
	$categoryDimName = $aRS->fields["categorydimname"];
	$elementDimName = $aRS->fields["elementdimname"];
	$elementIndName = $aRS->fields["elementindname"];
	$strMembers = $categoryDimName."_SVSep_".$elementDimName."_SVSep_".$elementIndName;
}

header('Content-Type: text/plain; charset=utf-8');
echo $strMembers;
?>