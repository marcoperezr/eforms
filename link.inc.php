<?php
require_once("repository.inc.php");
require_once("initialize.php");
require_once("settingsvariable.inc.php");
//EXT
require_once("object.trait.php");

class BITAMLink extends BITAMObject
{
	//EXT
	use BITAMObjectExt;
	
	public $LinkID;
	public $LinkName;
	public $Link;
	public $Image;
	public $Status;
	public $DisplayMode;
	
  	function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->LinkID = -1;
		$this->LinkName = '';
		$this->Link = '';
		$this->Image = '';
		//0: No Visible, 1: Visible
		$this->Status = 1;
		//El tipo de despliegue cuando se crea un nuevo enlace será el tipo de despliegue predeterminado
		$this->DisplayMode = 0;
		
		//EXT
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	static function NewInstance($aRepository)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository);
	}
	
	static function NewInstanceWithID($aRepository, $aLinkID)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (((int)  $aLinkID) < 0)
		{
			return $anInstance;
		}
		
		$strAdditionalFields="";
		if (getMDVersion() >= esvConfLinks) 
		{
			$strAdditionalFields.=", DisplayMode";
		}
		
		$sql = "SELECT A.LinkID, A.LinkName, A.Link, Image, Status".$strAdditionalFields.
			" FROM SI_SV_Link A 
			WHERE A.LinkID = ".$aLinkID;
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Link ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceWithName($aRepository, $aLinkName)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		//@JAPR 2016-04-15: Corregido un bug, faltaba el $ al hacer referencia a la variable
		if (trim($aLinkName) == '')
		{
			return $anInstance;
		}

		$strAdditionalFields="";
		if (getMDVersion() >= esvConfLinks) 
		{
			$strAdditionalFields.=", DisplayMode";
		}
		
		$sql = "SELECT A.LinkID, A.LinkName, A.Link, Image, Status".$strAdditionalFields.
			" FROM SI_SV_Link A 
			WHERE A.LinkName = ".$aRepository->DataADOConnection->Quote($aLinkName);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_Link ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository);
		$anInstance->LinkID = (int) @$aRS->fields["linkid"];
		$anInstance->LinkName = (string) trim(@$aRS->fields["linkname"]);
		$anInstance->Link = (string) trim(@$aRS->fields["link"]);
		$anInstance->Image = (string) trim(@$aRS->fields["image"]);
		$anInstance->Status = (int) @$aRS->fields["status"];
		
		if (getMDVersion() >= esvConfLinks) 
		{			
			$anInstance->DisplayMode = (int) @$aRS->fields["displaymode"];
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		if (array_key_exists("LinkID", $aHTTPRequest->POST))
		{
			$intLinkID = $aHTTPRequest->POST["LinkID"];
			
			if (is_array($intLinkID))
			{
				$aCollection = BITAMLinkCollection::NewInstance($aRepository, $intLinkID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intLinkID);
				if (is_null($anInstance))
				{
					$anInstance = $strCalledClass::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				$aResult = $anInstance->save();
				
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = $strCalledClass::NewInstance($aRepository);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("LinkID", $aHTTPRequest->GET))
		{
			$intLinkID = $aHTTPRequest->GET["LinkID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intLinkID);

			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = $strCalledClass::NewInstance($aRepository);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		if (array_key_exists("LinkID", $anArray)) {
			$this->LinkID = $anArray["LinkID"];
		}
		
		if (array_key_exists("LinkName", $anArray)) {
			$this->LinkName = trim($anArray["LinkName"]);
		}
		
		if (array_key_exists("Link", $anArray)) {
			$this->Link = trim($anArray["Link"]);
		}
		
		if (array_key_exists("Image", $anArray)) {
			$this->Image = trim($anArray["Image"]);
		}
		
		if (array_key_exists("Status", $anArray)) {
			$this->Status = $anArray["Status"];
		}
		
		if (array_key_exists("DisplayMode", $anArray)) {
			$this->DisplayMode = $anArray["DisplayMode"];
		}
		
		return $this;
	}
	
	function save()
	{	
		//@JAPR 2016-04-18: Corregido un bug, faltaba esta referencia
		global $gblShowErrorSurvey;

		if ($this->isNewObject()) {
			$sql =  "SELECT ".$this->Repository->DataADOConnection->IfNull("MAX(LinkID)", "0")." + 1 AS LinkID".
				" FROM SI_SV_Link";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->LinkID = (int) $aRS->fields["linkid"];
			
			$strAdditionalFields="";
			$strAdditionalValues="";
			if (getMDVersion() >= esvConfLinks) 
			{
				$strAdditionalFields.=", DisplayMode";
				$strAdditionalValues.=", ".$this->DisplayMode;
			}
			
			$sql = "INSERT INTO SI_SV_Link (".
						" LinkID".
			            ", LinkName".
						", Link".
						", Image".
						", Status".
						$strAdditionalFields.
			            ") VALUES (".
			            $this->LinkID.
						",".$this->Repository->DataADOConnection->Quote($this->LinkName).
						",".$this->Repository->DataADOConnection->Quote($this->Link).
						",".$this->Repository->DataADOConnection->Quote($this->Image).
						",".$this->Status.
						$strAdditionalValues.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	 	else {
			
			$strAdditionalValues="";
			if (getMDVersion() >= esvConfLinks) 
			{
				$strAdditionalValues.=", DisplayMode = ".$this->DisplayMode;
			}
			
			$sql = "UPDATE SI_SV_Link SET ".
					"LinkName = ".$this->Repository->DataADOConnection->Quote($this->LinkName).
					", Link = ".$this->Repository->DataADOConnection->Quote($this->Link).
					", Image = ".$this->Repository->DataADOConnection->Quote($this->Image).
					", Status = ".$this->Status.
					$strAdditionalValues.
				" WHERE LinkID = ".$this->LinkID;

			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	}
	
	function remove($bJustMobileTables = false)
	{
		$sql = "DELETE FROM SI_SV_Link WHERE LinkID = ".$this->LinkID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->LinkID < 0);
	}
	
	function get_Title()
	{
		return translate("Link");
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=Link";
		}
		else
		{
			return "BITAM_PAGE=Link&LinkID=".$this->LinkID;
		}
	}
	
	function get_Parent()
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'LinkID';
	}
	
	function get_FormFieldName()
	{
		return 'LinkName';
	}
	
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "LinkName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	 
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//EXT
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Image() {
		$strImage = $this->Image;
		
		if($strImage)
		{
			$strImage = "<img src=\"{$strImage}\" />";
		}
		else
		{
			$strImage = "<img src=\"images/admin/eformslink.PNG\" />";
		}
		return $strImage;
	}
	function getJSonDefinition() {
		
		$arrDef = array();
		$arrDef['id'] = $this->LinkID;
		$arrDef['LinkID'] = $this->LinkID;
		$arrDef['LinkName'] = $this->LinkName;
		$arrDef['Link'] = $this->Link;
		$arrDef['Image'] = $this->Image;
		$arrDef['Status'] = $this->Status;
		$arrDef['DisplayMode'] = $this->DisplayMode;
			
		return $arrDef;
	}
	
	function generateForm($aUser) {
		$this->generateBeforeFormCode($aUser);
?>
<html>
<?
	//Contiene lo necesario para el manejo del grid
	require_once("genericgrid.inc.php");
?>
	<script>
		var tabLinks = "links";
		var objFormTabs;					//Cejillas de la opción de configuraciones
		
		var opDisplayModeLinks;
		opDisplayModeLinks = {
			0:"<?=translate("Default")?>",
			1:"<?=translate("Current page")?>",
			2:"<?=translate("New window")?>"
		};
	
<?
		//@JAPR 2016-07-08: Corregido un bug, la función getJsonDefinition no es estática y se estaba invocando como si lo fuera
		$arrJSON = $this->getJSonDefinition();
		//@JAPR
		if (is_null($arrJSON) || !is_array($arrJSON)) {
			$arrJSON = array('data' => array());
		}
		else {
			$arrJSON = array('data' => $arrJSON);
		}
		$strJSONDefinitions = json_encode($arrJSON);
?>		
		var objFormsDefs = JSON.parse("<?=addslashes($strJSONDefinitions)?>");
		var objLink = objFormsDefs.data;
		console.log("*******************objLink.Link: " + objLink.Link);
		
		//******************************************************************************************************
		LinkCls.prototype = new GenericItemCls();
		LinkCls.prototype.constructor = LinkCls;
		function LinkCls() {
			this.objectType = AdminObjectType.otyLink;
			this.objectIDFieldName = "LinkID";
		}
		
		/* Regresa el array POST con los campos ya adaptados al nombre a utilizar durante el proceso de grabado en el server
		//El parámetro oFields contiene los nombres de todos los campos que deben ser actualizados, utilizando los nombres de propiedades de la instancia
		*/
		LinkCls.prototype.getDataFields = function(oFields) {
			var blnAllFields = false;
			if (gbFullSave || !oFields || !$.isPlainObject(oFields) || $.isEmptyObject(oFields)) {
				blnAllFields = true;
			}
			var objParams = {};

			if (blnAllFields || oFields["LinkName"]) {
				objParams["LinkName"] = this.LinkName;
			}
			
			if (blnAllFields || oFields["Link"]) {
				objParams["Link"] = this.Link;
			}
			
			if (blnAllFields || oFields["Status"]) {
				objParams["Status"] = this.Status;
			}
			if (blnAllFields || oFields["Image"]) {
				objParams["Image"] = this.Image;
			}
			
			if (blnAllFields || oFields["DisplayMode"]) {
				objParams["DisplayMode"] = this.DisplayMode;
			}
			
			return objParams;
		}
		
		//******************************************************************************************************			
		function doOnLoad() {
			console.log('doOnLoad');
			
			addClassPrototype();
			
			objMainLayout = new dhtmlXLayoutObject({
				parent: document.body,
				pattern:"1C"
			});
			objMainLayout.cells("a").appendObject("divDesign");
			objMainLayout.cells("a").hideHeader();
			//objMainLayout.setSizes();
			objFormTabs = new dhtmlXTabBar({
				parent:"divDesignBody"
			});
			objFormTabs.addTab(tabLinks, "<span class='tabFormsOpts'><?=translate("Links")?></span>", null, null, true);
			objFormTabs.setArrowsMode("auto");
			objFormTabs.tabs(tabLinks).attachLayout({
				parent: document.body,
				pattern: "1C"
			});
			objFormsLayout = objFormTabs.tabs(tabLinks).getAttachedObject();
			//Evento del cambio de tab para permitir grabar información que hubiera cambiado
			objFormTabs.attachEvent("onTabClick", function(id, lastId) {
				console.log('objFormTabs.onTabClick ' + id + ', lastId == ' + lastId);
			});
			objFormTabs.attachEvent("onContentLoaded", function(id) {
				objFormTabs.tabs(id).progressOff();
			});
			
			//Genera la ventana de propiedades
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Properties")?>");
			objPropsLayout = objFormsLayout.cells(dhxPropsCell).attachLayout({pattern: "1C"});
			objPropsLayout.setSizes();
			objPropsLayout.cells(dhxPropsDetailCell).setText("<?=translate("Properties")?>");
			objPropsLayout.cells(dhxPropsDetailCell).hideHeader();
			objFormsLayout.cells(dhxPropsCell).hideHeader();
			
			//Agrega el evento para procesos cuando termina de cargar cada frame del layout
			objPropsLayout.attachEvent("onContentLoaded", function(id) {
				console.log('objPropsLayout.onContentLoaded ' + id);
				objPropsLayout.cells(id).progressOff();
			});
			
			doShowLinksProps();
		}
		function addClassPrototype() {
			if (!objLink) {
				return;
			}
			
			$.extend(objLink, new LinkCls());
		}
		
		function doShowLinksProps(){
			console.log('doShowLinksProps');
			objPropsLayout.cells(dhxPropsDetailCell).detachObject(true);
			generateLinksProps();
		}
		
		function getObject(iObjectType, iObjectID, iChildObjectType, iChildObjectID) {
			var objObject = objLink;
			return objObject;
		}
		
		function generateLinksProps() 
		{
			console.log('generateLinksProps');
			objFormsLayout.cells(dhxPropsCell).setText("<?=translate("Links")?>");
			
			//Generar todos los fields
			objTabsColl = {
				"generalTab":
				{
					id:"generalTab", name:"<?=translate("General")?>", activeOnLoad:true,defaultVisibility:{name:true, descrip:true}
				}
			};
			
			var objCommon = {id:objLink.LinkID, parentType:AdminObjectType.otyLink, tab:"generalTab"};
			objTabsColl["generalTab"].fields =  new Array(
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"LinkName", label:"<?=translate("Name")?>", type:GFieldTypes.alphanum, maxLength:255})),
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"Link", label:"<?=translate("Link")?>", type:GFieldTypes.alphanum, maxLength:255})),
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"Image", label:"<?=translate("Image")?>", type:GFieldTypes.uploadImg, maxLength:255})),
				new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"Status", label:"<?=translate("Visible")?>", type:GFieldTypes.combo, options:optionsYesNo}))
			);
			
<?
			if (getMDVersion() >= esvConfLinks)
			{
?>
				objTabsColl["generalTab"].fields.push(new FieldCls($.extend(true, {}, objCommon, objAlpha, {name:"DisplayMode", label:"<?=translate("LINKSDISPLAYMODE")?>", type:GFieldTypes.combo, options:opDisplayModeLinks})));
<?
			}
?>
			
			generateFormDetail(getLinkFieldsDefaulVisibility(objTabsColl), AdminObjectType.otyLink, objLink.LinkID, objPropsLayout.cells(dhxPropsDetailCell));
		}

		/* Modifica el objeto recibido con el conjunto de los campos (propiedades) que son visibles por default*/
		function getLinkFieldsDefaulVisibility(oTabsColl) {
			console.log("getLinkFieldsDefaulVisibility ");
			
			var strTabId = "generalTab";
			if (oTabsColl[strTabId] && oTabsColl[strTabId].fields) {
				oTabsColl[strTabId].defaultVisibility = {};
				oTabsColl[strTabId].defaultVisibility["LinkID"] = true;
				oTabsColl[strTabId].defaultVisibility["LinkName"] = true;
				oTabsColl[strTabId].defaultVisibility["Link"] = true;
				oTabsColl[strTabId].defaultVisibility["Image"] = true;
				oTabsColl[strTabId].defaultVisibility["Status"] = true;
<?
				if (getMDVersion() >= esvConfLinks)
				{
?>
					oTabsColl[strTabId].defaultVisibility["DisplayMode"] = true;
<?
				}
?>

			}
			return oTabsColl;
		}
		
		/* Regresa la definición del campo a partir de la combinación tab/field. Se asume que siempre se hace referencia a los campos del tipo de objeto
		que se está desplegando en el momento en que se solicita, ya que esos son los que están almacenados en el array
		//JAPR 2015-06-25: Agregado el parámetro bForForm para indicar que se solicitan los campos directos del detalle de la forma, de lo contrario se asume que se trata del objeto
		que se está desplegando en la celda de propiedades
		*/
		function getFieldDefinition(sTabName, sFieldName, bForForm) {
			console.log('getFieldDefinition ' + sTabName + '.' + sFieldName);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					for (var intCont in objTab.fields) {
						var objFieldTmp = objTab.fields[intCont];
						if (objFieldTmp && objFieldTmp.name == sFieldName) {
							objField = objFieldTmp;
							break;
						}
					}
				}
			}
			
			return objField;
		}
		
		/* Idéntica a getFieldDefinition pero en lugar del nombre de campo el cual es útil en un grid de propiedades, se utiliza el índice del campo, el cual es útil en
		un grid de valores */
		function getFieldDefinitionByIndex(sTabName, iFieldIdx, bForForm) {
			console.log('getFieldDefinitionByIndex ' + sTabName + '.' + iFieldIdx);
			var objField = undefined;
			
			//var objTmpTabsColl = (bForForm)?objSurveyTabsColl:objTabsColl;
			var objTmpTabsColl = objTabsColl;
			if (objTmpTabsColl && objTmpTabsColl[sTabName]) {
				var objTab = objTmpTabsColl[sTabName];
				if (objTab && objTab.fields) {
					objField = objTab.fields[iFieldIdx];
				}
			}
			
			return objField;
		}
	</script>
	
	<body onload="doOnLoad();" onunload="doOnUnload();">
		<iframe id="frameRequests" name="frameRequests" style="display:none;">
		</iframe>
		<form id="frmRequests" style="display:none;" method="post" target="frameRequests">
		</form>
		<div id="divLinks" style="display:none;height:100%;width:100%;">
		</div>
		<div id="divDesign" style="display:none;height:100%;width:100%;">
			<div id="divDesignBody" class="hiddenTabs" style="height:95%;width:100%;overflow:hidden;">
			</div>
		</div>
		<iframe id="frameUploadFile" name="frameUploadFile" style="display:none; position:absolute; left:100, top:150">
		</iframe>
	</body>
</html>
<?
		die();
	}	
}

class BITAMLinkCollection extends BITAMCollection
{	//EXT
	use BITAMCollectionExt;
	public $ObjectType;

	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
		//EXT
		$this->ObjectType = otyLink;
		$this->ContainerID = "links";
	}
	
	static function NewInstance($aRepository, $anArrayOfLinkIDs = null, $status=null)
	{
		$strCalledClass = static::class;

		global $gblShowErrorSurvey;
		
		$anInstance = new $strCalledClass($aRepository);
		
		$where = "";
		if (!is_null($anArrayOfLinkIDs))
		{
			switch (count($anArrayOfLinkIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE t1.LinkID = ".((int) $anArrayOfLinkIDs[0]);
					break;
				default:
					foreach ($anArrayOfLinkIDs as $intLinkID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$intLinkID; 
					}
					if ($where != "")
					{
						$where = "WHERE t1.LinkID IN (".$where.")";
					}
					break;
			}
		}
		
		if(!is_null($status))
		{
			if($where!="")
			{
				$where.= " AND Status = ".$status;
			}
			else
			{
				$where.= " WHERE Status = ".$status;
			}
		}
		
		$strAdditionalFields="";
		if (getMDVersion() >= esvConfLinks) 
		{
			$strAdditionalFields.=", DisplayMode";
		}
		
		$sql = "SELECT t1.LinkID, t1.LinkName, t1.Link, t1.Image".$strAdditionalFields.
			" FROM SI_SV_Link t1 ".$where." ORDER BY t1.LinkName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Link ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, null);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Links");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=Link";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=Link";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'LinkID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "LinkName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_FormFieldName()
	{
		return 'LinkName';
	}
	
	//EXT
	function get_CustomButtons() {
		$arrButtons = array();
		$arrButtons[] = array("id" => "add", "url" => "", "onclick" => "addNewObject();", "isDefault" => 1);
		return $arrButtons;
	}
	
	function generateAfterFormCode($aUser) {
?>
	<script language="JavaScript">
		var objWindows;
		var objDialog;
		var intDialogWidth = 380;
		var intDialogHeight = 350;
		var reqAjax = 1;
		
		//Descarga la instancia
		function doUnloadDialog() {
			if (objDialog) {
				var objForm = objDialog.getAttachedObject();
				if (objForm && objForm.unload) {
					objForm.unload();
					objForm = null;
				}
			}
			
			if (objWindows) {
				if (objWindows.unload) {
					objWindows.unload();
					objWindows = null;
					objWindows = new dhtmlXWindows({
						image_path:"images/"
					});
				}
			}
			
			objDialog = null;
		}
		
		//Manda los datos al server para ser procesados
		function doSendData(objFunction) {
			if (!objDialog) {
				return;
			}

			objDialog.progressOn();
			var objForm = objDialog.getAttachedObject();
			var objData = objForm.getFormData();
			objForm.lock();
			objForm.send("processRequest.php", "post", function(loader, response) {
				if (!loader || !response) {
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (loader.xmlDoc && loader.xmlDoc.status != 200) {
					alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				setTimeout(function () {
					try {
						var objResponse = JSON.parse(response);
					} catch(e) {
						alert(e + "\r\n" + response.substr(0, 1000));
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					
					if (objResponse.error) {
						alert(objResponse.error.desc);
						objDialog.progressOff();
						objForm.unlock();
						return;
					}
					else {
						if (objResponse.warning) {
							alert(objResponse.warning.desc);
						}
					}
					
					//Sólo si todo sale Ok cierra el diálogo y continua cargando
					doUnloadDialog();
					if (objFunction && typeof objFunction == "function")
					{
						console.log("dosenddata antes de llamar función:"+objResponse.newObject.LinkID);
						objFunction(objResponse.newObject);
					}
					var strURL = objResponse.url;
					if (strURL && parent.doExecuteURL) {
						//debugger;
						parent.doExecuteURL("<?=$this->ContainerID?>", undefined, undefined, strURL);
					}
				}, 100);
			});
		}
		
		//Muestra el diálogo para crear un nuevo link
		function addNewObject() {
			if (!objWindows) {
				//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
			
			objDialog = objWindows.createWindow({
				id:"newObject",
				left:0,
				top:0,
				width:intDialogWidth,
				height:intDialogHeight,
				center:true,
				modal:true
			});
			
			objDialog.setText("<?=translate("New")?>"+" "+"<?=translate("Link")?>");
			objDialog.denyPark();
			objDialog.denyResize();
			objDialog.setIconCss("without_icon");
			
			objDialog.attachEvent("onClose", function() {
				return true;
			});
			
			/* El diálogo para crear los links simplemente pide el nombre que se le asignará al nueva link*/
			var objFormData = [
				{type:"settings"/*, offsetLeft:20*/},
				{type:"input", name:"LinkName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
				{type:"input", name:"Link", label:"<?=translate("Link")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
				{type:"block", blockOffset:0, offsetLeft:100, list:[
					{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
					{type:"newcolumn"},
					{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
				]},
				{type:"input", name:"LinkID", value:-1, hidden:true},
				{type:"input", name:"RequestType", value:reqAjax, hidden:true},
				{type:"input", name:"Process", value:"Add", hidden:true},
				{type:"input", name:"ObjectType", value:<?=otyLink?>, hidden:true}
			];
			
			var objForm = objDialog.attachForm(objFormData);
			objForm.adjustParentSize();
			objForm.setItemFocus("LinkName");
			objForm.attachEvent("onBeforeValidate", function (id) {
				console.log('Before validating the form: id == ' + id);
			});
			
			objForm.attachEvent("onAfterValidate", function (status) {
				console.log('After validating the form: status == ' + status);
			});
			
			objForm.attachEvent("onValidateSuccess", function (name, value, result) {
				console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onValidateError", function (name, value, result) {
				console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
			});
			
			objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
				if (!name || !inp || !ev || !ev.keyCode) {
					return;
				}
				
				switch (ev.keyCode) {
					case 13:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnOk"])
						}, 100);
						break;
					case 27:
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnCancel"])
						}, 100);
						break;
				}
			});
			
			objForm.attachEvent("onButtonClick", function(name) {
				switch (name) {
					case "btnCancel":
						setTimeout(function () {
							doUnloadDialog();
						}, 100);
						break;
						
					case "btnOk":
						setTimeout(function() {
							if (objForm.validate()) {
								doSendData(
								function(objLink)
								{
									var link = objLink.Link;
									link = link.replace(/@LOGIN/gi,"<?=$aUser->UserName?>");
									<?//@JAPR 2016-07-21: Corregida la encriptación del password con el método de BITAM?>
									link = link.replace(/@ENCRYPTEDPASSWORD/gi,"<?=urlencode(BITAMEncode($aUser->Password))?>");
									<?
									if((string) @$_SERVER['HTTPS'] == 'on')
									{
									?>
									link = link.replace(/http:\/\//gi,'https://');
									<?
									}
									?>
									if(objLink.Image=="" || objLink.Image=="images/admin/emptyimg.png")
									{
										var linkImg = "images/admin/eformslink.PNG";
									}
									else
									{
										var linkImg = objLink.Image;
									}
									window.parent.parent.objDataViewB.add({id:"custom"+objLink.LinkID,description:objLink.LinkName, picture:linkImg, bitCustomURL:link,DisplayMode:objLink.DisplayMode});
									console.log("despues de agregar:"+objLink.LinkID);
								}
								);
							}
						}, 100);
						break;
				}
			});
		}
	</script>
<?
	}
	
	function afterRemove($aUser)
	{
?>
	for (var iRemove in arrValid)
	{
		window.parent.parent.objDataViewB.remove("custom"+arrValid[iRemove]);
	}
<?		
	}
}
?>