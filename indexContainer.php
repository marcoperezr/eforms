<?php
/* Página que contiene un iframe donde se inserta el código de eForms, debido a que por la manera en que funciona JQueryMobile no se pudo limitar el height del
body ni agregar un div padre para colocar la imagen del dispositivo, por tanto el App de eForms correrá dentro de un iFrame el cual estará encerrado en un div
donde se dimensionará y se colocará de manera externa el dispositivo correspondiente, dicho iFrame se redimensionará conforma el previo determine según el
dispositivo seleccionado, lo mismo que cambiará la imagen desplegada
*/
require_once("config.php");
global $widthByDisp;
global $heightByDisp;

$strWidthsObj = json_encode($widthByDisp);
$strHeightsObj = json_encode($heightByDisp);

$strWidthsContainerObj = json_encode($widthContainerByDisp);
$strHeightsContainerObj = json_encode($heightContainerByDisp);

$intSurveyID = (int) @$_REQUEST['SurveyID'];
?>
<html>
	<head>
		<meta http-equiv="x-ua-compatible" content="IE=10" />
      	<script type="text/javascript" src="js/json2.min.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
		<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
		<script>
		
		//Dimensions default por dispositivo
		var objDeviceWidths = <?=$strWidthsObj?>;
		var objDeviceHeights = <?=$strHeightsObj?>;
		
		var objDeviceContainerWidths = <?=$strWidthsContainerObj?>;
		var objDeviceContainerHeights = <?=$strHeightsContainerObj?>;
		
		/* Establece las dimensiones del iFrame del App según el tipo de dispositivo especificado, además de colocar la imagen correspondiente en el div Contenedor
		ajustado a la orientación seleccionada
		*/
		function setDevice(iDisp, iOrientation, oCustomSize) {
			iOrientation = iOrientation.toLowerCase();
			iDispLc = iDisp.toLowerCase();
			
			if (iOrientation == 'portrait') {
				var objWidths = objDeviceWidths;
				var objHeights = objDeviceHeights;
				
				var objContainerWidths = objDeviceContainerWidths;
				var objContainerHeights = objDeviceContainerHeights;
			}
			else {
				var objHeights = objDeviceWidths;
				var objWidths = objDeviceHeights;
				
				var objContainerWidths = objDeviceContainerHeights;
				var objContainerHeights = objDeviceContainerWidths;
			}
			
			var intWidth = objWidths[iDisp];
			var intHeight = objHeights[iDisp];
			if (oCustomSize) {
				if (oCustomSize.width) {
					intWidth = oCustomSize.width;
				}
				
				if (oCustomSize.height) {
					intHeight = oCustomSize.height;
				}
			}
			
			intWidth = parseInt(String(intWidth).replace('px', ''));
			intHeight = parseInt(String(intHeight).replace('px', ''));
			var objiFrame = $('#frmDevice');
			if (objiFrame && objiFrame.length) {
				if (iDisp == 'Default' && !oCustomSize) {
					objiFrame.width('100%');
					objiFrame.height('100%');
				}
				else {
					objiFrame.width(intWidth);
					objiFrame.height(intHeight);
				}
			}
			
			var objDiv = $('#divDevice');
			if (objDiv && objDiv.length) {
				if (iDisp == 'Default' && !oCustomSize) {
					objDiv.width('100%');
					objDiv.height('100%');
				}
				else {
					//objDiv.width(intWidth + 250);
					//objDiv.height(intHeight + 250);
					objDiv.width(objContainerWidths[iDisp]);
					objDiv.height(objContainerHeights[iDisp]);
				}
				
				//Si hay un tamaño personalizado, no se puede utilizar una imagen fija para representarlo así que se forza a limpiarla
				if (iDisp == 'Default' || (oCustomSize && (oCustomSize.width || oCustomSize.height))) {
					objDiv.css('background-image','');
				}
				else {
					objDiv.css('background-image','url(images/devices/'+iDispLc+'_'+iOrientation.substr(0, 4)+'.png)');
				}
			}
			
			//Asignar cual dispositivo está seleccionado
			if (objiFrame[0].hasOwnProperty('giDevice')) 
			{
				objiFrame[0].giDevice = iDisp;
			}
			else
			{	
				objiFrame[0].contentWindow.giDevice = iDisp;
			}
		}
		</script>
	</head>
	<body onload="setDevice('Default', 'portrait');" style="overflow:scroll;background-color:white;">
		<div id="divDevice" style="margin:auto;background-repeat:no-repeat;background-position:center center;">
			<iframe id="frmDevice" style="background-color:white;margin:auto;display:block;top:50%;position:relative;transform:translateY(-50%);-webkit-transform: translateY(-50%);" src="indexDesign.php?SurveyID=<?=$intSurveyID?>&dte=<?=date("YmdHis")?>">
			</iframe>
		</div>
	</body>
</html>
