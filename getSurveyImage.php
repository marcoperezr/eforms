<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$data = "";
$strPath = "";
$surveyID = -1;
$folioID = -1;
$questionID = -1;
$secuencia_reng = -1;
$secuencia_col = -1;
$numPictures = 0;
$arrPictures = array();
if(array_key_exists("surveyID",$_GET) && array_key_exists("folioID",$_GET) && array_key_exists("questionID",$_GET))
{
	$surveyID = $_GET["surveyID"];
	$folioID = $_GET["folioID"];
	$questionID = $_GET["questionID"];
	$theMainFactKey = 0;
	
	//@JAPR 2011-07-11: Unificada la tabla de imagenes en una sï¿œla tabla de encuestas, ahora la llave es FactKey
	//$sql = "SELECT escape_foto FROM enc_res_imagen WHERE id_folio = ".$folioID." AND id_encuesta = ".$surveyID." AND id_registro = ".$questionID;
	$sql = "SELECT PathImage FROM SI_SV_SurveyAnswerImage WHERE FactKey = ".$folioID." AND SurveyID = ".$surveyID." AND QuestionID = ".$questionID;
	if(isset($_GET["mainFactKey"]) && $_GET["mainFactKey"] != '') {
    	$theMainFactKey = $_GET["mainFactKey"];
		$sql .= " AND MainFactKey = ".$theMainFactKey;
	}
	//@JAPR 2013-05-14 Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	//Agregado el ordenamiento por MainFactKey para que se presenten en el orden de captura de la propia sección múltiple
	$sql .= " ORDER BY MainFactKey";
	//@JAPR
    $aRS = $theRepository->DataADOConnection->Execute($sql);
	if($aRS && !$aRS->EOF)
	{
		//@JAPR 2013-05-14 Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
		while(!$aRS->EOF)
		{
			$strPathImage = (string) @$aRS->fields["pathimage"];
			if (trim($strPathImage) != '') {
				$strPath = $strPathImage;
				$arrPictures[] = $strPathImage;
				$numPictures++;
			}
			$aRS->MoveNext();
		}
		//@JAPR
	}
}

if(trim($data)!="")
{
	$path = "surveyimages/".$data;
	//clearstatcache();
	//$strData = file_get_contents($path);
	/*
	header("Content-type: image/jpg");
	header('Content-Disposition: inline; filename="'.$data.'"');
	header('Content-Length: '.filesize($path));
	*/
	header("Location: ".$path);
	
	/*
	$aFileLength = filesize($path);
	header("Content-Type: image/jpg");
	header(sprint("Content-Disposition: inline; filename=\"%s\"", $data));
	header("Content-Length: ".$aFileLength);
	header("Cache-control: private");
	header ("Connection: close");
	@readfile($path);
	*/
	//readfile($path);
	//die();
}
elseif (trim($strPath) != "")
{
	//@JAPR 2013-05-14 Corregida la exportación de imagenes y comentarios en secciones Maestro-Detalle y Dinámicas
	//Si existen varias imagenes, genera un HTML para mostrarlas en lugar de usar un header
	if ($numPictures > 1 || true) {
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	</head>
	<body style="margin: 0px;">
<?
		foreach ($arrPictures as $strPath) {
			$path = "surveyimages/".$theRepositoryName."/".$strPath;
			$path = str_replace("\\", "/", $path);
?>
		<img style="-webkit-user-select: none; cursor: -webkit-zoom-in;" src="<?=$path?>">
<?
		}
?>
	</body>
</html>
<?
	}
	else {
		$path = "surveyimages/".$theRepositoryName."/".$strPath;
		$path = str_replace("\\", "/", $path);
		header("Location: ".$path);
	}
}
?>