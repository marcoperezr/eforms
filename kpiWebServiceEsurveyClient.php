<?php

$action = '';
if (!isset($_REQUEST['action']))
	$action = 'register';
else
	$action = $_REQUEST['action'];

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once('kpiWebServiceEsurveyClient.inc.php');

require_once('eSurveyServiceMod.inc.php');
$aBITAMConnection = connectToKPIRepository();
$strRepository = $theRepository->RepositoryName;
// get the databaseid
$aSQL = "SELECT DatabaseID FROM saas_databases WHERE Repository = ".$aBITAMConnection->ADOConnection->Quote($strRepository);
$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
if ($rsAdminData && $rsAdminData->_numOfRows != 0)
{
	$intDatabaseID = $rsAdminData->fields['DatabaseID'];
}
else
	die('kpiWebService.Error: Failed to obtain databaseid Data!');
// get the admin email and productid
$aSQL = "SELECT A.ProductID, B.Email FROM saas_regs A, saas_users B WHERE A.databaseid = ".$intDatabaseID." AND A.hostid = 0 AND A.userid = B.userid";
$rsAdminData = $aBITAMConnection->ADOConnection->Execute($aSQL);
if ($rsAdminData && $rsAdminData->_numOfRows != 0)
{
	$strAdminEmail = $rsAdminData->fields['Email'];
	$intProductID = $rsAdminData->fields['ProductID'];
}
else
	die('kpiWebService.Error: Failed to obtain host Data!');

$kpiWebServiceClient = new getKpiWebServiceClient();

switch($action)
{
	case 'register':

		// syntax: email_©_pass_©_first_©_last_©_role_©_target
		if (!isset($_POST["txtNewUserData"]))
			die('kpiWebService.Error: Failed to obtain new user data from POST!');
		$strNewUserData = $_POST["txtNewUserData"];
		$arrNewUserData = explode("_©_", $strNewUserData);
		if (count($arrNewUserData) < 5)
			die('kpiWebService.Error: Invalid new user data params from POST!');
		$strEmail = $arrNewUserData[0];
		//@JAPR 2015-05-20: Corregido un bug, el EMail tenía que estar en minúsculas porque así es como se grabará en la tabla de usuarios, si no
		//se hace de esa manera, no podrá hacer match en el array y no insertará en la tabla de usuarios de eForms (#GR7N2W)
		$strEmail = strtolower($strEmail);
		$strPassword = $arrNewUserData[1];
		$strFirstName = $arrNewUserData[2];
		$strLastName = $arrNewUserData[3];
		$role = (int) $arrNewUserData[4];
		//@JAPR
		//$arrData[$strEmail]["role"] = $arrNewUserData[4];
		//$arrData[$strEmail]["sendpdfbyemail"] = $arrNewUserData[5];
		//$arrData[$strEmail]["supervisor"] = $arrNewUserData[6];

		$arrGuestData = array(
			"guestEmail" => $strEmail,
			"guestPassword" => $strPassword,
			"hostEmail" => $strAdminEmail,
			"productID" => $intProductID,
			"sendInternalEmail" => 0,
			"guestFirstName" => $strFirstName,
			"guestLastName" => $strLastName
		);
		$kpiWebServiceClient->registerUser($arrGuestData);
		// everything's ok!
		// get the UserID from si_sv_users
		$aSQL = "SELECT UserID FROM SI_SV_USERS WHERE Email = ".$theRepository->ADOConnection->Quote($strEmail);
		$rsNewUser = $theRepository->ADOConnection->Execute($aSQL);
		if ($rsNewUser && $rsNewUser->_numOfRows != 0)
			$newUserID = $rsNewUser->fields['UserID'];
		else
			die('kpiWebService.Error: could not get the new UserID from SI_SV_USERS');
		
		//Agregar actualización del rol de usuario
		
		if($role==0)
		{
			$roleUpdate = "SV_Admin = 0, SV_User = 0";
		}
		else
		{
			if($role==1)
			{
				$roleUpdate = "SV_Admin = 1, SV_User = 0";
			}
			else
			{
				$roleUpdate = "SV_Admin = 0, SV_User = 1";
			}
		}
		
		$aSQL = "UPDATE SI_SV_USERS SET ".$roleUpdate." WHERE UserID = ".$newUserID;
		if ($theRepository->ADOConnection->Execute($aSQL) === false)
			die('kpiWebService.Error: Unable to update role:'.$roleUpdate.'!');
		
		echo ("kpiWebService.Success:".$newUserID);

		break;

	case 'getUsers':
		$users = '';
		$users = $kpiWebServiceClient->getUsers($strAdminEmail, $intProductID);
		echo ("kpiWebService.Success:".$users);
		break;
	case 'suscribe' || 'unsuscribe':
		if (!isset($_POST["txtUsersData"]))
			die('kpiWebService.Error: Failed to obtain users data from POST!');
		if (!isset($_REQUEST["suscriptionType"]))
			die('kpiWebService.Error: Failed to obtain suscriptionType from POST!');
		$intPermission = -1;
		if ($_REQUEST['suscriptionType'] == 'admin')
			$intPermission = 1;
		if ($_REQUEST['suscriptionType'] == 'guest')
			$intPermission = 2;
		if ($intPermission == -1)
			die('kpiWebService.Error: Invalid suscriptionType!');
		$arrUsersData = json_decode($_POST["txtUsersData"], true);
		$arrUsers = array();
		foreach ($arrUsersData as $aUserData)
		{
			$arrUsers[] = $aUserData['name'].':'.$intPermission;
		}
		$intProductID = 5;
		if ($action == 'unsuscribe')
		{
			$intProductID = $intProductID * -1;
			//GCRUZ 2017-12-21. Validar apps de EBavel
			$arrInvalidUnsuscribeUsers = array();
			/*
			foreach ($arrUsersData as $aUserData)
			{
				if ($intPermission == 1)
				{
					$aSQL = "SELECT COUNT(*) AS 'Admin' FROM si_forms_applications_det WHERE cla_master = 1 AND cla_detail = (SELECT CLA_USUARIO FROM SI_USUARIO WHERE CUENTA_CORREO = ".$theRepository->ADOConnection->Quote($aUserData['name']).")";
					$rsUnsuscribeUsers = $theRepository->ADOConnection->Execute($aSQL);
					if ($rsUnsuscribeUsers && $rsUnsuscribeUsers->_numOfRows != 0)
					{
						if (intval($rsUnsuscribeUsers->fields['Admin']) > 0)
							$arrInvalidUnsuscribeUsers[] = $aUserData['name'];
					}
				}
				elseif ($intPermission == 2)
				{
					$aSQL = "SELECT COUNT(*) AS 'Guest' FROM si_forms_applications_det WHERE cla_master > 1 AND cla_detail = (SELECT CLA_USUARIO FROM SI_USUARIO WHERE CUENTA_CORREO = ".$theRepository->ADOConnection->Quote($aUserData['name']).")";
					$rsUnsuscribeUsers = $theRepository->ADOConnection->Execute($aSQL);
					if ($rsUnsuscribeUsers && $rsUnsuscribeUsers->_numOfRows != 0)
					{
						if (intval($rsUnsuscribeUsers->fields['Guest']) > 0)
							$arrInvalidUnsuscribeUsers[] = $aUserData['name'];
					}
				}
			}
			*/
			if (count($arrInvalidUnsuscribeUsers) > 0)
			{
				die('kpiWebService.Error: Users have EBavel permission assigned, unable to remove permission for users:'.implode(',', $arrInvalidUnsuscribeUsers));
			}

		}
		//echo $kpiWebServiceClient->suscribeUsers($strAdminEmail, $intProductID, $arrUsers);
		//Actualizar en SI_SV_USERS
		//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
		$permissionUpdate = "";
		$permissionUpdateBMD = "";
		if ($action == 'unsuscribe') {
			$permissionUpdate = "SV_Admin = 0, SV_User = 0";
			$permissionUpdateBMD = "EFORMS_ADMIN = 0, EFORMS_DESKTOP = 0, EBAVEL_ADMIN = 0, EBAVEL_USER = 0";
		}
		else {
			if ($intPermission == 1) {
				$permissionUpdate = "SV_Admin = 1, SV_User = 0";
				$permissionUpdateBMD = "EFORMS_ADMIN = 1, EFORMS_DESKTOP = 0, EBAVEL_ADMIN = 1, EBAVEL_USER = 0";
			}
			elseif ($intPermission == 2) {
				$permissionUpdate = "SV_Admin = 0, SV_User = 1";
				$permissionUpdateBMD = "EFORMS_ADMIN = 0, EFORMS_DESKTOP = 1, EBAVEL_ADMIN = 0, EBAVEL_USER = 1";
			}
			else {
				$permissionUpdate = "SV_Admin = 0, SV_User = 0";
				$permissionUpdateBMD = "EFORMS_ADMIN = 0, EFORMS_DESKTOP = 0, EBAVEL_ADMIN = 0, EBAVEL_USER = 0";
			}
		}
		//@JAPR
		foreach ($arrUsersData as $aUserData)
		{
			$aSQL = "UPDATE SI_SV_USERS SET ".$permissionUpdate." WHERE Email = ".$theRepository->ADOConnection->Quote($aUserData['name']);
			$theRepository->ADOConnection->Execute($aSQL);
			if ($theRepository->ADOConnection->Affected_Rows() == 0)
				die('kpiWebService.Error: Unable to update permission for user:'.$aUserData['name'].'!');
			
			//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
			$aSQL = "UPDATE SI_USUARIO SET ".$permissionUpdateBMD." WHERE CUENTA_CORREO = ".$theRepository->ADOConnection->Quote($aUserData['name']);
			$theRepository->ADOConnection->Execute($aSQL);
			//@JAPR
		}
		echo 'kpiWebService.Success:';
		break;
	default:
		echo ('kpiWebService.Error: unrecognized action');
}

?>