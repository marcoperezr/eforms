var bitamTrees = new Array;

function BitamTN(aContentsID, aContents, aState, afatherNodeID, childrenLevel, billable, taskSWork, taskStartDate, taskEndDate, complete, rollup, status, childrenURL)
{	
	this.id = null;
	this.contents = aContents;
	this.contentsID = aContentsID;
	this.state = aState;
	this.childrenURL = childrenURL;
	this.level = 0;
	this.children = new Array();
	this.childrenCreated = false;
	this.tree = null;
	this.fatherNodeID = afatherNodeID;
	this.childrenLength = 0;
	this.status = parseInt(status);
	this.childrenLevel = parseInt(childrenLevel);
	this.billable = billable;
	this.taskSWork = taskSWork;
	this.taskStartDate = taskStartDate;
	this.taskEndDate = taskEndDate;
	this.complete = complete;
	this.rollup = rollup;
	
	if(this.billable=="&nbsp;")
	{
		this.billableSaved = 0; //Valor del campo Billable que está grabado
		this.billableNew = 0;  //Contendrá el nuevo valor del campo Billable cambiado desde el árbol
	}
	else
	{
		this.billableSaved = 1; //Valor del campo Billable que está grabado
		this.billableNew = 1;  //Contendrá el nuevo valor del campo Billable cambiado desde el árbol
	}
}

function BitamT()
{
	this.id = bitamTrees.length;

	this.innerText = "";

	this.nodes = new Array();
	this.roots = new Array();
	
	this.expandedBranchImage = "images/minusGray.gif";
	this.collapsedBranchImage = "images/plusGray.gif";
	this.leafImage = "images/bulletGray.gif";

	bitamTrees[bitamTrees.length] = this;
}

function getElementWithID(anID)
{
	return (document.getElementById(anID));
}

function buildTree(aTree)
{
	//debugger;
	aTree.innerText = '<table border=0 cellspacing=0 cellpadding=0>';

	var size = aTree.roots.length;
	for (var i = 0; i < size; i++)
	{
		buildTreeNodeOn(aTree.roots[i], aTree, 0);
	}

	aTree.innerText += '</table>';
}

function getMenuImage(aNode)
{
	//debugger;
	var status=aNode.status;
	var menuImage;

	switch (status)
	{
		case -1:
			menuImage="";
			break;
   		case 0:
			menuImage="images/DefinitionOnTime.png";
			break;
   		case 1:
			menuImage="images/DefinitionOnWarning.png";
			break;
   		case 2:
			menuImage="images/DefinitionLate.png";
			break;
   		case 3:
			menuImage="images/AuthorizationOnTime.png";
			break;
		case 4:
			menuImage="images/AuthorizationOnWarning.png";
			break;
		case 5:
			menuImage="images/AuthorizationLate.png";
			break;
		case 6:
			menuImage="images/RedefinitionOnTime.png";
			break;
		case 7:
			menuImage="images/RedefinitionOnWarning.png";
			break;
		case 8:
			menuImage="images/RedefinitionLate.png";
			break;
		case 9:
			menuImage="images/AuthorizedOnTime.png";
			break;
   		default:
   			menuImage="";
			break;
   }

   return(menuImage);
}

function getTitleImage(aNode)
{
	//debugger;
	var status=aNode.status;
	var titleImage;

	switch (status)
	{
		case -1:
			titleImage="";
			break;
		case 0:
			titleImage="Definition-OnTime";
			break;
   		case 1:
			titleImage="Definition-OnWarning";
			break;
   		case 2:
			titleImage="Definition-Late";
			break;
   		case 3:
			titleImage="Authorization-OnTime";
			break;
   		case 4:
			titleImage="Authorization-OnWarning";
			break;
		case 5:
			titleImage="Authorization-Late";
			break;	
		case 6:
			titleImage="Redefinition-OnTime";
			break;
		case 7:
			titleImage="Redefinition-OnWarning";
			break;
		case 8:
			titleImage="Redefinition-Late";
			break;
		case 9:
			titleImage="Authorized-OnTime";
			break;
		default:
   			titleImage="";
			break;
   }

   return(titleImage);
}

function bitamNodeString(aNode)
{	
	var str;
	var menuImage;
	var titleImage;
	var clase;
	
	str = "";
	menuImage="images/TreeMenu.gif";
	titleImage="Menu";
	clase = "TreeNode";
	
	str += '<table id="table' + aNode.id + '"  border="0" cellspacing="0" cellpadding="0" class="treeNodeStyle" width="100%">';
	
	str += '<tbody id="tbody' + aNode.id + '" width="100%">'
	
	str += '<tr id="tableRow' + aNode.id + '" width="100%"><td id="space' + aNode.id + '" noWrap width="' + ((15 * aNode.level)+1) + 'px">&nbsp;</td>';

	if (aNode.childrenLength == 0 && (aNode.childrenURL == null))
	{
		var anImage = aNode.tree.leafImage;
		str += '<td noWrap id="cellImg' + aNode.id +'" width="15px"><img id="img' + aNode.id + '" border="0" src="' + anImage + '"></td>';
		str += '<td noWrap id="cellImgMenu' + aNode.id + '" width="15px"><img id="imgMenu' + aNode.id + '" border="0" src="' + menuImage + '" title="' + titleImage + '" style="cursor:pointer" onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)"></td>';
	}
	else
	{
		if (aNode.state)
		{
			var anImage = aNode.tree.expandedBranchImage;
		}
		else
		{
			var anImage = aNode.tree.collapsedBranchImage;
		}
		
		str += '<td noWrap id="cellImg' + aNode.id +'" width="15px"><a href=javascript:bitamNodeIDClicked(' + aNode.id + ',' + aNode.tree.id + ')><img id="img' + aNode.id + '" border="0" src="' + anImage + '"></a></td>';
		str += '<td noWrap id="cellImgMenu' + aNode.id + '" width="15px"><img id="imgMenu' + aNode.id + '" border="0" src="' + menuImage +'" title="' + titleImage + '" style="cursor:pointer" onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)"></td>';
	}
	
	str += '<td noWrap class="' + clase + '" id="contents' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';" onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)" width="' + (300-(15 * aNode.level)) + 'px">' + aNode.contents + '</td>';

	str += '<td noWrap class="' + clase + '" id="infoBillable' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';"	onclick="changeBillableField(' + aNode.id + ',' + aNode.tree.id + ', 1, \'\')" align="center" width="20px">' + aNode.billable + '</td>';
	str += '<td noWrap class="' + clase + '" id="infoWork' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';"	onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)" align="right" width="70px">' + aNode.taskSWork + '</td>';
	str += '<td noWrap class="' + clase + '" id="infoStart' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';"	onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)" width="70px">' + aNode.taskStartDate + '</td>';
	str += '<td noWrap class="' + clase + '" id="infoFinish' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';"	onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)" width="70px">' + aNode.taskEndDate + '</td>';
	str += '<td noWrap class="' + clase + '" id="infoComplete' + aNode.id + '" noWrap style="cursor:pointer" onmouseover="this.className=\'HoverTreeNode\';" onmouseout="this.className=\'' + clase + '\';"	onclick="showHierarchyMenu(' + aNode.id + ',' + aNode.tree.id + ',event)" align="right" width="50px">' + aNode.complete + '</td>';
	
	str += '</tr></tbody></table>';

	return(str);
}

function bitamRegisterNodeOnTree(aNode, aTree, aLevel)
{
	aNode.id = aTree.nodes.length;
	aNode.level = aLevel;
	aNode.tree = aTree;
	aTree.nodes[aNode.id] = aNode;
	
	if(aNode.fatherNodeID != -1)
	{	aFatherNodeID = aNode.fatherNodeID;
		aTree.nodes[aFatherNodeID].childrenLength++;
	}
}

function buildTreeNodeOn(aNode, aTree, aLevel)
{
	//debugger;
	bitamRegisterNodeOnTree(aNode, aTree, aLevel);

	aTree.innerText += '<tr id=row' + aNode.id + ' ><td id=cellNode' + aNode.id + '>';

	aTree.innerText += bitamNodeString(aNode);

	aTree.innerText += '</td></tr>';

	if (aNode.state)
	{
		aNode.childrenCreated = true;

		var size = aNode.childrenLength;
		for (var i = 0; i < size; i++)
		{
			buildTreeNodeOn(aNode.children[i], aTree, aLevel + 1);
		}
	}
}

function bitamNodeIDClicked(aNodeID, aTreeID)
{
	//debugger
	var aTree = bitamTrees[aTreeID];
	var aNode = aTree.nodes[aNodeID];

	if (aNode.state)
	{	
		bitamCollapseNode(aNode);
	}
	else
	{	
		bitamExpandNode(aNode);
	}
}

function createChildNodesFrom(aNode)
{
	var aRow = getElementWithID('row' + aNode.id);
	var rowIndex = aRow.rowIndex;

	var aTable = aRow.parentNode.parentNode;
	var newRow, newCell, aCode;

	var i = aNode.children.length - 1;

	while (i >= 0)
	{
		var subNode = aNode.children[i];

		bitamRegisterNodeOnTree(subNode, aNode.tree, aNode.level + 1);

		newRow = aTable.insertRow(rowIndex + 1);
		newRow.id = 'row' + subNode.id;
		 
		//newRow.ondragenter="dragEnter();"
		//newRow.ondragover="dragOver();"
		
		newCell = newRow.insertCell(0);
		aCode = bitamNodeString(subNode);
		newCell.innerHTML = aCode;
		newCell.id='cellNode' + subNode.id; 

		i--;
	}
}

function bitamExpandNode(aNode)
{	
	var anObject;
	
	stateAux = aNode.state;
	
	aNode.state = true;
	anObject = getElementWithID('img' + aNode.id);
	imageAux=anObject.src;
	anObject.src = 'images/minusGray.gif';

	if (!aNode.childrenCreated)
	{
		if (aNode.childrenURL != null )
			fillChildrenFromServer(aNode);

		createChildNodesFrom(aNode);
		aNode.childrenCreated = true;
	}
	else
	{
		var size = aNode.children.length;
		for (var i = 0; i < size; i++)
		{
			var childNode = aNode.children[i];//
			
			if (childNode!=null)
			{
				anObject = getElementWithID('row' + childNode.id);
				anObject.style.display = '';
				if (childNode.childrenLength > 0 && childNode.childrenCreated&& childNode.state==true)
				{	
					bitamExpandNode(childNode);
					
				}
			}
		}
	}
}

function bitamCollapseNode(aNode)
{
	stateAux = aNode.state;
	
	aNode.state = false;
	
	anObject = getElementWithID('img' + aNode.id);
	imageAux=anObject.src;
	anObject.src = 'images/plusGray.gif';			
	
	var size = aNode.children.length;
	for (var i = 0; i < size; i++)
	{
		var childNode = aNode.children[i];

		if (childNode!=null)
		{
			anObject = getElementWithID('row' + childNode.id);
			anObject.style.display = 'none';

			if (childNode.childrenLength > 0 && childNode.childrenCreated & childNode.state==true)
			{	
				anObjectChild = getElementWithID('img' + childNode.id);
				imageAux=anObjectChild.src;
				bitamCollapseNode(childNode);
				childNode.state=true;
				anObjectChild.src = imageAux;			
			}
		}
	}
}

function convertStringToBitamTNForNode(aString)
{	
	var nodes = new Array();

	if (aString.length == 0)
	{
		return(nodes);
	}

	var realString = unescape(aString);

	var sep = String.fromCharCode(30);

	var elements = realString.split(sep);

	sep = String.fromCharCode(31);

	for (var i = 0; i < elements.length; i++)
	{
		var parts = elements[i].split(sep);

		nodes[nodes.length] = new BitamTN(parts[0], parts[1], parts[2] == 'true', parts[3], parts[4], parts[5], parts[6], parts[7], parts[8], parts[9], parts[10], parts[11], parts[12]);
	}

	return(nodes);
}

function fillChildrenFromServer(aNode)
{
	//debugger;
	var xmlObj = getXMLObject();

	if (xmlObj == null)
	{
		alert("XML Object not supported");
		return(new Array);
	}

 	xmlObj.open("POST", aNode.childrenURL+"&fatherNodeID="+aNode.id, false);

 	xmlObj.send(null);
 	
 	//debugger;
	aNode.children = convertStringToBitamTNForNode(xmlObj.responseText);
}

function handleDragStart()
{
	var dragData = window.event.dataTransfer;

	dragData.setData('text', window.event.srcElement.innerText);
	dragData.effectAllowed = 'copy';
	dragData.dropEffect = 'copy';
}

function handleDragEnd()
{
	window.event.dataTransfer.clearData();
}

function addNode(aNode, aFather)
{
	var aRow = getElementWithID('row' + aFather.id);
	var rowIndex = aRow.rowIndex;

	var aTable = aRow.parentNode.parentNode;
	var newRow, newCell, aCode;

	aFather.children[aFather.children.length]=aNode;
		
	bitamRegisterNodeOnTree(aNode, aFather.tree, aFather.level + 1);

	newRow = aTable.insertRow(rowIndex + 1);
	
	newRow.id = 'row' + aNode.id;
	
	newCell = newRow.insertCell(0);
	aCode = bitamNodeString(aNode);
	newCell.innerHTML = aCode;
	newCell.id='cellNode' + aNode.id; 
}