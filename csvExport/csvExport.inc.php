<?php

//GCRUZ 2016-01-19. Agregar Parámetro para filtrar las fechas por SyncEndDate y SyncEndTime.
//GCRUZ 2016-01-21. Agregar parámetro para filtrar por captura obtenida desde la tabla si_sv_incdatadestinations
function EsurveyGetDataToExport( $aRepository, $survey, $sections, $questions, $date_begin = null, $date_end = null, $users = array(), $bSyncDate = false, $datadestinationsSQL = '', $bIncludeName = true )
{
	//$bIncludeName = false;
	global $strNewAttribSep;
	$arrData = array();
	//Obtener datos standard de la forma
	$anADOConnection = $aRepository->DataADOConnection;
	//El query se filtra por la combinación Survey, User y SurveyDate, la cual siempre es única
	$userIDS = "'".implode("','", $users)."'";
	$rowFields = array();
	$aSQLData = array();
	$aSQLFROM = array();
	$aSQLWHERE = array();
	$aSQLWHERE[] = '1=1';
	//MAPR 2018-03-13: Se agregó SyncLatitude y SyncLongitude al reporte de Excel. (#8PG2B2)
	$aSQLData[] = "A.".BITAMSurvey::$KeyField.", '{$survey->SurveyName}' AS 'SurveyName', A.UserID, A.UserEMail, A.DateID, A.DateKey, A.StartDate, A.StartTime, A.EndDate, A.EndTime, A.ServerStartDate, A.ServerStartTime, A.ServerEndDate, A.ServerEndTime, 
		A.StartDateEdit, A.EndDateEdit, A.StartTimeEdit, A.EndTimeEdit, A.SyncDate, A.SyncTime, A.SyncEndDate, A.SyncEndTime, A.eFormsVersionNum, A.SyncLatitude, A.SyncLongitude, A.Latitude, A.Longitude, A.Accuracy, 
		A.Country, A.State, A.City, A.ZipCode, A.Address";
	$aSQLFROM[] = "{$survey->SurveyTable} A";
	//MAPR 2018-03-13: Se agregó SyncLatitude y SyncLongitude al reporte de Excel. (#8PG2B2)
	$StdRowFields = array('SurveyKey','SurveyName','UserID','UserEMail','DateID','DateKey','StartDate','StartTime','EndDate','EndTime','SyncDate','SyncTime','SyncEndDate','SyncEndTime', 'SyncLatitude', 'SyncLongitude','Latitude','Longitude','Accuracy','Country','State','City','ZipCode','Address');

	//GCRUZ 2016-05-20. Distancia y tiempo de traslado
	//GCRUZ 2016-05-19. Validar si existen las columnas de tiempo y distancia de traslado
	$sql = "SHOW COLUMNS FROM {$survey->SurveyTable} LIKE 'Transfer%'";
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	$bHasTransferData = false;
	if ($aRS && $aRS->_numOfRows >= 2)
		$bHasTransferData = true;
	if ($bHasTransferData)
	{
		$aSQLData[] ="A.TransferTime, A.TransferDistance";
		$StdRowFields[] = 'TransferTime';
		$StdRowFields[] = 'TransferDistance';
	}


	$questionRowFields = array();
	//GCRUZ 2015-10-27. Aplicar formato a las preguntas
	$questionRowFieldsFormat = array();

	//******************************************************************************************************************************************************
	//A continuación obtiene los datos de todas las preguntas, pero primero de las secciones estándar combinadas, ya que esas sólo contiene un único valor por pregunta
	//******************************************************************************************************************************************************

	$strTableName = $survey->SurveyStdTable;
	$strKeyField = BITAMSurvey::$KeyField;
	$strAdditionalFields = "";
	$strAnd = ", ";
	$arrValidFields = array();
	$arrQuestionsByID = array();
	$JOINCount = 0;
	foreach ($questions->Collection as $objQuestion) {
		if ($objQuestion->SectionType != sectNormal) {
			continue;
		}

		$intQuestionID = $objQuestion->QuestionID;
		//Para secciones estándar
		//@JAPR 2016-06-08: Agregado el parámetro $bIncludeName para poder reutilizar el array de datos sin referencia al nombre de pregunta en el proceso de SPSS
		$strPropName = trim($anADOConnection->Quote($objQuestion->QuestionID.(($bIncludeName)?(':'.$objQuestion->getAttributeName()):'')), "'");
//		$anInstance->$strPropName = array("value" => '', "image" => '', "document" => '', "comment" => '');
		$arrQuestionsByID[$intQuestionID] = $objQuestion;

		//Valida si la pregunta tiene o no un valor que capturar como respuesta
		//GCRUZ 2015-09-02. Preguntas que ya no se van a mostrar. Issue: 53NPOT
		$blnValidQuestion = true;
		switch ($objQuestion->QTypeID) {
			case qtpPhoto:
			case qtpDocument:
			case qtpSignature:
			case qtpSketch:
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
			//@JAPR
			case qtpSkipSection:
			case qtpMessage:
			case qtpSync:
			case qtpPassword:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar
			case qtpExit:
			case qtpAudio:
			case qtpVideo:
			case qtpSection:
			//Estas preguntas se generan en una tabla diferente, así que aunque tengan respuesta, no pueden ser parte de este query
			case qtpSingle:
			case qtpMulti:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
			case qtpOCR:
				$blnValidQuestion = false;
				break;
		}

		$arrValidFields[$intQuestionID]['value'] = $blnValidQuestion;
		if ($blnValidQuestion) {
			//Si es una pregunta con respuesta, agrega el campo al query
			if (array_search($strPropName, $questionRowFields) === false)
			{
				$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_0',-6)] = $strPropName;
				//GCRUZ 2015-10-27. Aplicar formato a las preguntas
				if ($objQuestion->FormatMask != '')
					$questionRowFieldsFormat[substr('000'.$objQuestion->QuestionNumber.'_0',-6)] = array ('type' => 'numeric', 'format' => $objQuestion->FormatMask);
				if ($objQuestion->DateFormatMask != '')
					$questionRowFieldsFormat[substr('000'.$objQuestion->QuestionNumber.'_0',-6)] = array ('type' => 'date', 'format' => $objQuestion->DateFormatMask);
			}
			$strAdditionalFields .= $strAnd."J{$JOINCount}.{$objQuestion->DescFieldAttr} AS '{$strPropName}'";
		}
		
		//Valida si la pregunta tiene o no una imagen que grabar como parte de la respuesta
		$blnValidQuestion = false;
		switch ($objQuestion->QTypeID) {
			//GCRUZ 2015-10-21. Preguntas tipo barcode no muestran imagen
			case qtpBarCode:
			case qtpOCR:
				$blnValidQuestion = false;
				break;				
			case qtpPhoto:
			case qtpSignature:
			case qtpSketch:
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
			//@JAPR
				$blnValidQuestion = true;
				break;
			
			default:
				$blnValidQuestion = ($objQuestion->HasReqPhoto > 0);
				break;
		}
		
		$arrValidFields[$intQuestionID]['image'] = $blnValidQuestion;
		if ($blnValidQuestion) {
			//Si es una pregunta con imagen, agrega el campo al query
			if (array_search($strPropName.'Img', $questionRowFields) === false)
				$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_1',-6)] = $strPropName.'Img';
			$strAdditionalFields .= $strAnd."J{$JOINCount}.{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} AS '{$strPropName}Img'";
		}
		
		//Valida si la pregunta tiene o no un documento que grabar como parte de la respuesta
		$blnValidQuestion = false;
		switch ($objQuestion->QTypeID) {
			case qtpOCR:
				$blnValidQuestion = false;
				break;
			case qtpDocument:
			case qtpAudio:
			case qtpVideo:
				$blnValidQuestion = true;
				break;
			
			default:
				$blnValidQuestion = ($objQuestion->HasReqDocument > 0);
				break;
		}
		
		$arrValidFields[$intQuestionID]['document'] = $blnValidQuestion;
		if ($blnValidQuestion) {
			//Si es una pregunta con documento, agrega el campo al query
			if (array_search($strPropName.'Doc', $questionRowFields) === false)
				$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_2',-6)] = $strPropName.'Doc';
			$strAdditionalFields .= $strAnd."J{$JOINCount}.{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} AS '{$strPropName}Doc'";
		}
		
		//Valida si es una pregunta con comentario
		$blnValidQuestion = ($objQuestion->HasReqComment > 0);
		$arrValidFields[$intQuestionID]['comment'] = $blnValidQuestion;
		
		if ($blnValidQuestion) {
			//Si es una pregunta con comentario, agrega el campo al query
			if (array_search($strPropName.'Comm', $questionRowFields) === false)
				$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_3',-6)] = $strPropName.'Comm';
			$strAdditionalFields .= $strAnd."J{$JOINCount}.{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} AS '{$strPropName}Comm'";
		}
	}

	//El query se filtra directamente por el ID de reporte consultado
	if ($strAdditionalFields != '')
	{
		$strAdditionalFields = trim($strAdditionalFields, ",");
		$aSQLData[] = "{$strAdditionalFields}";
		$aSQLFROM[] = "{$strTableName} J{$JOINCount} ON A.".BITAMSurvey::$KeyField."=J{$JOINCount}.".BITAMSurvey::$KeyField;
		$JOINCount++;
	}

	if (count($users) > 0)
		$aSQLWHERE[] = "AND A.UserEMail IN ({$userIDS})";
	if ($date_begin != null && $date_end != null)
	{
		if (!$bSyncDate)
			$aSQLWHERE []= " AND A.DateID BETWEEN ".$anADOConnection->Quote($date_begin)." AND ".$anADOConnection->Quote($date_end);
		else
			$aSQLWHERE []= " AND CONCAT(A.SyncEndDate, ' ', A.SyncEndTime) BETWEEN ".$anADOConnection->Quote($date_begin)." AND ".$anADOConnection->Quote($date_end);
	}
	if ($datadestinationsSQL != '')
	{
		$aSQLWHERE[] = " AND A.".BITAMSurvey::$KeyField." IN (".$datadestinationsSQL.") ";
	}

	$aSQL = 'SELECT ' . implode(' , ', $aSQLData);
	$aSQL .= ' FROM ' . implode(' LEFT JOIN ', $aSQLFROM);
	$aSQL .= ' WHERE ' . implode(' ', $aSQLWHERE);
	$aSQL .= ' ORDER BY 1';

	//GCRUZ 2018-08-09. Remover caracteres escapados de la lista de campos, para que los pueda encontrar en la lista de fields del rowSet
	foreach ($questionRowFields as $key => $value)
		$questionRowFields[$key] = stripslashes($value);

	$stdData = array();
	$rsData = $anADOConnection->Execute($aSQL);
	if ($rsData && $rsData->_numOfRows != 0)
	{
		while (!$rsData->EOF)
		{
			$row = array();
			$dataRow = array();
			foreach ($rsData->fields as $field => $value)
			{
				$row[$field] = $value;
				if (array_search($field, $questionRowFields) !== false || array_search($field, $StdRowFields) !== false)
					$dataRow[$field] = $value;
			}
			$row['multiData'] = array();
			$row['stdmultiData'] = array();
			$dataRow['multiData'] = array();
			$dataRow['stdmultiData'] = array();
			$stdData[$rsData->fields['SurveyKey']] = $row;
			$arrData[$rsData->fields['SurveyKey']] = $dataRow;
			$rsData->MoveNext();
		}
	}
	$surveyKeysIN = "(SELECT ".BITAMSurvey::$KeyField." FROM {$survey->SurveyTable} A ".' WHERE ' . implode(' ', $aSQLWHERE).")";


	if (!is_null($sections)) {
		//******************************************************************************************************************************************************
		//A continuación obtiene los datos de todas las preguntas de secciones multi-registro sección por sección
		//******************************************************************************************************************************************************

//		$multiData = array();

		//Datos los keys de la sección multi-registro correspondiente, indica cual es la posición dentro del array de respuestas que le toca
		$arrSectionsByID = array();
		$arrSectionIndexByKeys = array();
//		$anInstance->SectRowsCount = array();
		$arrMultiRowSectionsByID = array();
		foreach ($sections->Collection as $objSection) {
			$arrSectionsByID[$objSection->SectionID] = $objSection;
			if ($objSection->SectionType != sectMasterDet && $objSection->SectionType != sectInline) {
				continue;
			}
			
			$arrSectionIndexByKeys[$objSection->SectionID] = array();
			$arrMultiRowSectionsByID[$objSection->SectionID] = $objSection;
//			$anInstance->SectRowsCount[$objSection->SectionID] = 0;
			$strTableName = $objSection->SectionTable;
			$strKeyField = BITAMSection::$KeyField;
			//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
			$strDescField = '';
			$bInlineFirstQuestion = true;
			$intInlineFirstQuestion = 0;
			$bHasValidQuestion = false;
			$bInlineWithAttributes = false;
			//GCRUZ
			$strAdditionalFields = "";
			$strAnd = ", ";
			$arrValidFields = array();
			$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $objSection->SectionID);

			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->SectionID != $objSection->SectionID) {
					continue;
				}
				
				$intQuestionID = $objQuestion->QuestionID;
				//Para secciones multi-registro
				//@JAPR 2016-06-08: Agregado el parámetro $bIncludeName para poder reutilizar el array de datos sin referencia al nombre de pregunta en el proceso de SPSS
				$strPropName = trim($anADOConnection->Quote($objQuestion->QuestionID.(($bIncludeName)?(':'.$objQuestion->getAttributeName()):'')), "'");
//				$anInstance->$strPropName = array("value" => array(), "image" => array(), "document" => array(), "comment" => array());
				$arrQuestionsByID[$intQuestionID] = $objQuestion;
				
				//Valida si la pregunta tiene o no un valor que capturar como respuesta
				//GCRUZ 2015-09-02. Preguntas que ya no se van a mostrar. Issue: 53NPOT
				$blnValidQuestion = true;
				switch ($objQuestion->QTypeID) {
					case qtpPhoto:
					case qtpDocument:
					case qtpSignature:
					case qtpSketch:
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpSketchPlus:
					//@JAPR
					case qtpSkipSection:
					case qtpMessage:
					case qtpSync:
					case qtpPassword:
					//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
					case qtpUpdateDest:
					case qtpAudio:
					case qtpVideo:
					case qtpSection:
					//Estas preguntas se generan en una tabla diferente, así que aunque tengan respuesta, no pueden ser parte de este query
					case qtpSingle:
					case qtpMulti:
					case qtpOCR:
						$blnValidQuestion = false;
						break;
				}
				
				$arrValidFields[$intQuestionID]['value'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
					$bHasValidQuestion = true;
					//Si es una pregunta con respuesta, agrega el campo al query
					if (array_search("{$strPropName}", $questionRowFields) === false)
						$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_2',-6)] = $strPropName;
					//GCRUZ 2015-10-27. Aplicar formato a las preguntas
					if ($objQuestion->FormatMask != '')
						$questionRowFieldsFormat[substr('000'.$objQuestion->QuestionNumber.'_2',-6)] = array ('type' => 'numeric', 'format' => $objQuestion->FormatMask);
					if ($objQuestion->DateFormatMask != '')
						$questionRowFieldsFormat[substr('000'.$objQuestion->QuestionNumber.'_2',-6)] = array ('type' => 'date', 'format' => $objQuestion->DateFormatMask);
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldAttr} AS '{$strPropName}'";
				}
				
				//Valida si la pregunta tiene o no una imagen que grabar como parte de la respuesta
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					//GCRUZ 2015-10-21. Preguntas tipo barcode no muestran imagen
					case qtpBarCode:
					case qtpOCR:
						$blnValidQuestion = false;
						break;
					//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
					case qtpPhoto:
					case qtpSignature:
					case qtpSketch:
					case qtpSketchPlus:
					//@JAPR
						$blnValidQuestion = true;
						break;
					
					default:
						$blnValidQuestion = ($objQuestion->HasReqPhoto > 0);
						break;
				}
				
				$arrValidFields[$intQuestionID]['image'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
					$bHasValidQuestion = true;
					//Si es una pregunta con imagen, agrega el campo al query
					if (array_search("{$strPropName}Img", $questionRowFields) === false)
						$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_3',-6)] = "{$strPropName}Img";
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} AS '{$strPropName}Img'";
				}
				
				//Valida si la pregunta tiene o no un documento que grabar como parte de la respuesta
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					case qtpDocument:
					case qtpAudio:
					case qtpOCR:
						$blnValidQuestion = false;
						break;					
					case qtpVideo:
						$blnValidQuestion = true;
						break;
					
					default:
						$blnValidQuestion = ($objQuestion->HasReqDocument > 0);
						break;
				}
				
				$arrValidFields[$intQuestionID]['document'] = $blnValidQuestion;
				if ($blnValidQuestion) {
					//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
					$bHasValidQuestion = true;
					//Si es una pregunta con documento, agrega el campo al query
					if (array_search("{$strPropName}Doc", $questionRowFields) === false)
						$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_4',-6)] = "{$strPropName}Doc";
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} AS '{$strPropName}Doc'";
				}
				
				//Valida si es una pregunta con comentario
				$blnValidQuestion = ($objQuestion->HasReqComment > 0);
				$arrValidFields[$intQuestionID]['comment'] = $blnValidQuestion;
				
				if ($blnValidQuestion) {
					//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
					$bHasValidQuestion = true;
					//Si es una pregunta con comentario, agrega el campo al query
					if (array_search("{$strPropName}Comm", $questionRowFields) === false)
						$questionRowFields[substr('000'.$objQuestion->QuestionNumber.'_5',-6)] = "{$strPropName}Comm";
					$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} AS '{$strPropName}Comm'";
				}

				//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
				//GCRUZ 2015-09-01. Manipulamos únicamente la primera pregunta VALIDA para meter el campo SectionDesc
				if ($objSection->SectionType == sectInline && $bInlineFirstQuestion/* && $bHasValidQuestion*/)
				{
					$bInlineFirstQuestion = false;
					//GCRUZ 2015-11-10. Se seleccionará la primera pregunta de sección por el QuestioNumber
					$objInlineFirstQuestion = $objQuestionColl->Collection[0];
					$intInlineFirstQuestion = $objInlineFirstQuestion->QuestionID;

					$strDescField = ', '.$objSection->DescField." AS '{$intInlineFirstQuestion}:{$objSection->SectionName} : Description'".', ';
					//GCRUZ 2015-09-01. Si la sección inline tiene atributos de catálogo se agregarán todas las columnas de la tabla
					if ($objSection->ValuesSourceType == 1 && $objSection->DataSourceMemberID > 0)
					{
						$strDescField = ', '.$strTableName.'.*, ';
						$bInlineWithAttributes = true;
					}
					else
					{
						if (array_search("{$intInlineFirstQuestion}:{$objSection->SectionName} : Description", $questionRowFields) === false)
							$questionRowFields[substr('000'.$objInlineFirstQuestion->QuestionNumber.'_0',-6)] = "{$intInlineFirstQuestion}:{$objSection->SectionName} : Description";
					}
				}
			}
			
			//El query se filtra directamente por el ID de reporte consultado
			$strAdditionalFields = trim($strAdditionalFields, ",");
//			$aSQLData[] = "{$strAdditionalFields}";
//			$aSQLFROM[] = "{$strTableName} B ON A.{BITAMSurvey::$KeyField}=B.{BITAMSurvey::$KeyField}";
			//GCRUZ 2015-09-01. Agregar SectionDesc como parte de los datos de la sección Inline
			if (trim($strDescField) == '')
				$strDescField = ', ';
			else
			{
				if (trim($strAdditionalFields) == '')
					$strDescField = ', '.trim($strDescField, ", ");
			}
			$sql = "SELECT ".BITAMSurvey::$KeyField." AS 'SurveyKeyField', {$strKeyField} {$strDescField} {$strAdditionalFields} 
				FROM {$strTableName} WHERE ".BITAMSurvey::$KeyField." IN {$surveyKeysIN}";

			$rsData = $anADOConnection->Execute($sql);
			if ($rsData && $rsData->_numOfRows != 0)
			{
				while (!$rsData->EOF)
				{
					if ($bInlineWithAttributes)
					{
						$strFieldName = strtolower($objSection->DescField);
						$strValue = (string) @$rsData->fields[$strFieldName];
						$strAttributeDesc = (string) @$rsData->fields[strtolower('AttributeDesc')];
						$arrAttributeDesc = explode('_SVElem_', $strAttributeDesc);
						$arrAttributeFields = array();
						$arrAttributeFieldsNames = array();
						foreach ($arrAttributeDesc as $anAttribute)
						{
							$arrAttributeDef = explode('_', $anAttribute);
							$strFieldName = strtolower('Attribute_'.$arrAttributeDef[1]);
							$strValue = (string) @$aRS->fields[$strFieldName];
							$arrAttributeFields[] = $strFieldName;
							$arrAttributeFieldsNames[] = $arrAttributeDef[2];
							if (strpos($arrAttributeDef[0], '*') !== false)
								break;
						}
					}
					
					$row = array();
					$dataRow = array();
					$countAttribFields = 0;
					foreach ($rsData->fields as $field => $value)
					{
						$row[$field] = $value;
						if (array_search($field, $questionRowFields) !== false || array_search($field, $StdRowFields) !== false)
							$dataRow[$field] = $value;
						if ($bInlineWithAttributes && array_search($field, $arrAttributeFields) !== false)
						{
							$AttributeName = $arrAttributeFieldsNames[array_search($field, $arrAttributeFields)];
							if (array_search("{$intInlineFirstQuestion}:{$objSection->SectionName} : {$AttributeName}", $questionRowFields) === false)
							{
								$questionRowFields[substr('00000'.$objInlineFirstQuestion->QuestionNumber.'_0_'.$countAttribFields,-8)] = "{$intInlineFirstQuestion}:{$objSection->SectionName} : {$AttributeName}";
								$countAttribFields++;
							}
							$dataRow["{$intInlineFirstQuestion}:{$objSection->SectionName} : {$AttributeName}"] = $value;
						}
					}
//					$multiData[$rsData->fields[$strKeyField]] = $row;
					$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$objSection->SectionID.':'.$objSection->SectionName][$rsData->fields[$strKeyField]] = $row;
					$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$objSection->SectionID.':'.$objSection->SectionName][$rsData->fields[$strKeyField]] = $dataRow;
					$rsData->MoveNext();
				}
			}

		}

		//******************************************************************************************************************************************************
		//A continuación obtiene los datos de todas las preguntas simple y múltiple choice de todas las secciones por igual utilizando un union
		//******************************************************************************************************************************************************

//		$multiData = array();

		//Dado a que cada pregunta simple o múltiple choice tiene una tabla individual, se tendrá que generar un UNION de todas las tablas en un único query, siempre y cuando
		//las preguntas NO sean de catálogo, ya que si lo son, la estructura no se comparte debido a los diferentes atributos que se pueden emplear por catálogo, así que esas
		//tendrán que ser consultas separadas por cada pregunta

		//Primero extrae todas las preguntas que son simple o múltiple choice pero no de catálogo, ya que se pueden unificar sus queries
		$sqlUNION = '';
		$strAndUNION = '';
		$sqlStandard = '';
		$strAndStandard = '';
		$sqlMulti = '';
		$strAndMulti = '';

		//GCRUZ 2015-11-12. Question Number para preguntas tipo selector
		$sectionsFirstQuestionNumber = array();
		$sectionsSelectorQuestion = array();

		foreach ($sections->Collection as $objSection) {
			if ($objSection->SectionType == sectFormatted) {
				continue;
			}
			
			$blnMulti = isset($arrMultiRowSectionsByID[$objSection->SectionID]);
			$strKeyField = BITAMSection::$KeyField;
			$strAnd = ", ";
			$objQuestionColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $objSection->SectionID);

			//GCRUZ 2015-11-12. Obtener el questionnumber de la primera pregunta, debiera ser el inicial
			if (is_array($objQuestionColl->Collection) && count($objQuestionColl->Collection) > 0)
			{
				$objFirstQuestion = $objQuestionColl->Collection[0];
				$intFirstQuestionNumber = $objFirstQuestion->QuestionNumber;
			}
			else
				$intFirstQuestionNumber = 0;
			$sectionsFirstQuestionNumber[$objSection->SectionID] = $intFirstQuestionNumber;
			$sectionsSelectorQuestion[$objSection->SectionID] = $objSection->SelectorQuestionID;

			foreach ($objQuestionColl->Collection as $objQuestion) {
				if ($objQuestion->SectionID != $objSection->SectionID) {
					continue;
				}
				
				$intQuestionID = $objQuestion->QuestionID;
//				$strPropName = $objQuestion->SurveyField;
				
				//Todas las secciones pero sólo preguntas SChoice y MChoice
				//@JAPR 2016-06-08: Agregado el parámetro $bIncludeName para poder reutilizar el array de datos sin referencia al nombre de pregunta en el proceso de SPSS
				$strPropName = trim($anADOConnection->Quote($objQuestion->QuestionID.(($bIncludeName)?(':'.$objQuestion->getAttributeName()):'')), "'");
				//$anInstance->$strPropName = array("value" => array(), "image" => array(), "document" => array(), "comment" => array());
				$strAdditionalFields = "";
				
				//Valida si la pregunta es o no simple o múltiple choice sin catálogo
				$blnValidQuestion = false;
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
					case qtpMulti:
						if ($objQuestion->ValuesSourceType != tofCatalog) {
							$blnValidQuestion = true;
						}
						break;
				}
				
				if (!$blnValidQuestion) {
					continue;
				}
				
				//En este caso como se trata de un UNION, no se puede usar strPropName como Alias, debe ser un texto genérico
				$strAdditionalFields .= $strAnd."{$objQuestion->DescFieldAttr} AS QOption";
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Las preguntas simple choice no tienen un campo adicional, sin embargo deben agregarlo para que no falle el SELECT con UNIONs combinando múltiple choice
						$strAdditionalFields .= $strAnd."NULL AS QOptionVal";
						break;
					case qtpMulti:
						//Las preguntas múltiple choice tienen un campo adicional con un valor extra que se debe consultar
						$strAdditionalFields .= $strAnd."{$objQuestion->ValueField} AS QOptionVal";
						break;
				}
				
				$strTableName = $objQuestion->QuestionDetTable;
				//El query se filtra directamente por el ID de reporte consultado mediante un join a la tabla de la sección correspondiente
				$sql = "SELECT B.".BITAMSurvey::$KeyField." AS 'SurveyKeyField', ".$anADOConnection->Quote($objSection->SectionName)." AS SectionName, {$objSection->SectionID} AS SectionID, {$objSection->SectionType} AS SectionType, {$objQuestion->QuestionID} AS QuestionID, {$objQuestion->QuestionNumber} AS QuestionNumber, A.{$strKeyField}, ".
						$aRepository->DataADOConnection->Quote($strPropName)." AS QuestionField {$strAdditionalFields} 
					FROM {$strTableName} A 
						INNER JOIN {$objSection->SectionTable} B ON A.".BITAMSection::$KeyField." = B.".BITAMSection::$KeyField;
				$sqlUNION .= $strAndUNION.$sql." WHERE B.".BITAMSurvey::$KeyField." IN {$surveyKeysIN}";
				$strAndUNION = " UNION ";
			}
		}
		$rsData = $anADOConnection->Execute($sqlUNION);
		if ($rsData && $rsData->_numOfRows != 0)
		{
			while (!$rsData->EOF)
			{
				if (array_search($rsData->fields['QuestionField'], $questionRowFields) === false)
				{
					//GCRUZ 2015-11-12. Si es una pregunta tipo selector, hay que cambiar su questionnumber
					if (intval($rsData->fields['QuestionID']) == $sectionsSelectorQuestion[intval($rsData->fields['SectionID'])])
					{
						$questionRowFields[substr('000'.$sectionsFirstQuestionNumber[intval($rsData->fields['SectionID'])].'_1',-6)] = $rsData->fields['QuestionField'];
					}
					else
						$questionRowFields[substr('000'.$rsData->fields['QuestionNumber'].'_2',-6)] = $rsData->fields['QuestionField'];
				}
				$row = array();
				foreach ($rsData->fields as $field => $value)
					$row[$field] = $value;
				$dataRow = $rsData->fields['QOption'];
//				$multiData[$rsData->fields[$strKeyField]] = $row;
				$objQuestion = @$arrQuestionsByID[$rsData->fields['QuestionID']];
				switch ($objQuestion->QTypeID) {
					case qtpSingle:
						//Si la pregunta es simple choice, no debe permitir concatenar valores incluso si anteriormente fue múltiple choice y se grabó con múltiples respuestas
						if (intval($rsData->fields['SectionType']) == sectNormal)
						{
							$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField']] = $row;
							$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField']] = $dataRow;
						}
						else
						{
							$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField']] = $row;
							$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField']] = $dataRow;
						}
						break;
						
					case qtpMulti:
						//Las preguntas múltiple choice adicionalmente pudieran tener un valor extra capturado por cada opción, si es que estaban configuradas de esa manera al capturar
//						if ($strOptionVal != '') {
//							$strSuffix = " = ".$strOptionVal;
//						}
						
//						$strAnd = ($strValue)?"<br>":"";
						if ($row['QOptionVal'] != '')
						{
							if (array_search($rsData->fields['QuestionField'].':Value', $questionRowFields) === false)
								$questionRowFields[substr('000'.$rsData->fields['QuestionNumber'].'_3',-6)] = $rsData->fields['QuestionField'].':Value';
							$row['QOption = QOptionVal'] = $row['QOption'] . ' = ' . $row['QOptionVal'];
//							$dataRow = $row['QOption'] . ' = ' . $row['QOptionVal'];
						}
						if (intval($rsData->fields['SectionType']) == sectNormal)
						{
							$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField']][] = $row;
							$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField']][] = $dataRow;
							if ($row['QOptionVal'] != '')
							{
								$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField'].':Value'][] = $row['QOptionVal'];
								$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['QuestionField'].':Value'][] = $row['QOptionVal'];
							}
						}
						else
						{
							$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField']][] = $row;
							$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField']][] = $dataRow;
							if ($row['QOptionVal'] != '')
							{
								/*
								$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField'].':Value'][] = $row;
								$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField'].':Value'][] = $dataRow;
								*/
								$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField'].':Value'][] = $row['QOptionVal'];
								$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['QuestionField'].':Value'][] = $row['QOptionVal'];								
							}
						}
						break;
				}
				$rsData->MoveNext();
			}
		}

		//******************************************************************************************************************************************************
		//A continuación obtiene los datos de todas las preguntas simple y múltiple choice de catálogo de todas las secciones por igual utilizando un query por cada una
		//******************************************************************************************************************************************************
		//Dado a que cada pregunta simple o múltiple choice tiene una tabla individual pero con estructura muy diferente entre cada pregunta, se tendrá que generar un query por cada
		//pregunta para extraer la información de los atributos exclusivos que cada una utilizó
		$strKeyField = BITAMSection::$KeyField;
		$strKeyField = strtolower($strKeyField);
		$fieldStruct = strtolower(BITAMQuestion::$CatStructField);
//		$objQuestionColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		foreach ($questions->Collection as $objQuestion) {
			if ($objQuestion->ValuesSourceType != tofCatalog) {
				continue;
			}
			
			$intQuestionID = $objQuestion->QuestionID;
			$intSectionID = $objQuestion->SectionID;
			//Valida si la pregunta es o no simple o múltiple choice sin catálogo
			$blnValidQuestion = false;
			switch ($objQuestion->QTypeID) {
				case qtpSingle:
				case qtpMulti:
					$blnValidQuestion = true;
					break;
			}
			
			if (!$blnValidQuestion) {
				continue;
			}
			
			$blnMulti = isset($arrMultiRowSectionsByID[$objQuestion->SectionID]);
			$objSection = $arrSectionsByID[$objQuestion->SectionID];
			$strSectionName = $anADOConnection->Quote($objSection->SectionName);
//			$strPropName = strtolower($objQuestion->SurveyField);
			//Sólo MChoice de catálogo
			//@JAPR 2016-06-08: Agregado el parámetro $bIncludeName para poder reutilizar el array de datos sin referencia al nombre de pregunta en el proceso de SPSS
			$strPropName = trim($anADOConnection->Quote($objQuestion->QuestionID.(($bIncludeName)?(':'.$objQuestion->getAttributeName()):'')), "'");
			$fieldAttrib = strtolower(BITAMQuestion::$DescFieldCatMember);
			$fieldVal = "";
			//La consulta se generará con todos los campos, ya que los campos de atributos históricos no se eliminan, además que existe un campo de definición de atributo que nos
			//indica exactamente la estructura que había al grabar cada valor, por tanto no es necesaria una referencia al catálogo actual de la pregunta sino sólo el contenido de
			//la tabla
			switch ($objQuestion->QTypeID) {
				case qtpMulti:
					//Las preguntas múltiple choice tienen un campo adicional con un valor extra que se debe consultar
					$fieldVal = strtolower($objQuestion->ValueField);
					break;
			}
			
			$strTableName = $objQuestion->QuestionDetTable;
			//El query se filtra directamente por el ID de reporte consultado mediante un join a la tabla de la sección correspondiente
			$sql = "SELECT B.SurveyKey AS 'SurveyKeyField', {$objQuestion->QuestionID} AS QuestionID, {$objQuestion->QuestionNumber} AS QuestionNumber, {$strSectionName} AS SectionName, {$objSection->SectionID} AS SectionID, {$objQuestion->SectionType} AS SectionType, '{$strPropName}' AS 'Question', A.* 
				FROM {$strTableName} A 
					INNER JOIN {$objSection->SectionTable} B ON A.".BITAMSection::$KeyField." = B.".BITAMSection::$KeyField." WHERE B.SurveyKey IN {$surveyKeysIN}";

			$rsData = $anADOConnection->Execute($sql);
			$questionOrder = 1;
			$countRows = 0;
			if ($rsData && $rsData->_numOfRows != 0)
			{
				while (!$rsData->EOF)
				{
					//$countRows=0;
//					if (array_search($rsData->fields['Question'], $questionRowFields) === false)
//						$questionRowFields[substr('000'.$rsData->fields['QuestionNumber'].'_0',-6)] = $rsData->fields['Question'];
					$row = array();
					foreach ($rsData->fields as $field => $value)
						$row[$field] = $value;
					$objQuestion = @$arrQuestionsByID[$rsData->fields['QuestionID']];
					//En caso de que la pregunta sea múltiple choice, debe concatenar la respuesta con la anterior que tuviera ya grabada, si es simple choice sólo hay una respuesta
					//así que usa directamente la última
					$strOptionVal = '';
					switch ($objQuestion->QTypeID) {
						case qtpMulti:
							//Las preguntas múltiple choice adicionalmente pudieran tener un valor extra capturado por cada opción, si es que estaban configuradas de esa manera al capturar
							if ($strOptionVal != '') {
								//@JAPRWarning: Por ahora no existen preguntas múltiple choice de catálogo
								//$strSuffix = " = ".$strOptionVal;
							}
							break;
					}
					$strCatStructValues = $row[$fieldStruct];
					if (trim($strCatStructValues) == '') {
						$rsData->MoveNext();
						continue;
					}
					$arrCatStructValues = explode($strNewAttribSep, $strCatStructValues);
					$row['AttribValues'] = array();
					$dataRow = array();
					$strValue = '';
					foreach ($arrCatStructValues as $arrKey => $strCatMemberDef) {
						$arrCatMemberParts = explode("_", $strCatMemberDef, 3);
						$strKey = (string) @$arrCatMemberParts[0];
						$strKey = strpos($strKey, "*");
						$strDataSourceMemberID = (int) @$arrCatMemberParts[1];
						$strDataSourceMemberName = (string) @$arrCatMemberParts[2];
						//Si encuentra una definición inválida debe terminar
						if ($strDataSourceMemberID <= 0) {
							break;
						}
						$strAttribValue = (string) @$rsData->fields[$fieldAttrib.$strDataSourceMemberID];
						if ((trim($strAttribValue) == 'NA' || trim($strAttribValue) == '') && $bIncludeName)  
							continue;
						if (array_search($rsData->fields['Question'].':'.$strDataSourceMemberName, $questionRowFields) === false)
						{
							$orderKey = substr('000'.$questionOrder, -3);
							$questionRowFields[substr('000'.$rsData->fields['QuestionNumber'].'_'.$orderKey,-8)] = $rsData->fields['Question'].':'.$strDataSourceMemberName;
							$questionOrder++;
						}
						if (intval($rsData->fields['SectionType']) == sectNormal)
						{
							if ($objQuestion->QTypeID == qtpMulti && !$bIncludeName)
							{
								$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']][$strDataSourceMemberID][$countRows] = $strAttribValue;
								$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']][$strDataSourceMemberID][$countRows] = $strAttribValue;
							}
							else {
								if (!$bIncludeName) {
									if (!isset($stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']])) {
										$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']]=array();
										$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']]=array();
									}									
									$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']][$strDataSourceMemberID] = $strAttribValue;
									$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question']][$strDataSourceMemberID] = $strAttribValue;
								}
								else
								{
									$stdData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question'].':'.$strDataSourceMemberName] = $strAttribValue;
									$arrData[$rsData->fields['SurveyKeyField']]['stdmultiData'][$rsData->fields['Question'].':'.$strDataSourceMemberName] = $strAttribValue;
								}								
							}
						}
						else
						{
							if ($objQuestion->QTypeID == qtpMulti && !$bIncludeName)
							{
								$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']][$strDataSourceMemberID][$countRows] = $strAttribValue;
								$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']][$strDataSourceMemberID][$countRows] = $strAttribValue;
							}
							else  {
								if (!$bIncludeName) {
									if (!isset($stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']])) {
										$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']]=array();
										$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']]=array();
									}		
									$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']][$strDataSourceMemberID] = $strAttribValue;
									$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question']][$strDataSourceMemberID] = $strAttribValue;								
								}								
								else
								{
									$stdData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question'].':'.$strDataSourceMemberName] = $strAttribValue;
									$arrData[$rsData->fields['SurveyKeyField']]['multiData'][$rsData->fields['SectionID'].':'.$rsData->fields['SectionName']][$rsData->fields[$strKeyField]][$rsData->fields['Question'].':'.$strDataSourceMemberName] = $strAttribValue;
								}								
							}
						}
						$row['AttribValues'][] = $strDataSourceMemberName." = ".$strAttribValue;
						$dataRow[] = $strDataSourceMemberName." = ".$strAttribValue;
						$strValue .= $strAnd.$strDataSourceMemberName." = ".$strAttribValue;
						$strAnd = "<br>";
					}
					$countRows++;
					$rsData->MoveNext();
				}
			}
		}
//		var_dump($rowFields);
//		var_dump($stdData[9]);
//		var_dump($arrData[9]);
//		exit;
	}
//	exit;

	$rowFields = array();
	$rowFieldsFormat = array();
	foreach($StdRowFields as $aField)
		$rowFields[] = $aField;
	ksort($questionRowFields);
	ksort($questionRowFieldsFormat);
	foreach ($questionRowFieldsFormat as $keyField => $format)
		$rowFieldsFormat[$questionRowFields[$keyField]] = $format;
	foreach($questionRowFields as $aField)
		$rowFields[] = $aField;
	return array('fields' => $rowFields, 'data' => $arrData, 'fieldsFormat' => $rowFieldsFormat);
}

function EsurveyGetCSVData($data)
{
	$fields = $data['fields'];
	$arrData = $data['data'];
	$fieldsFormat = $data['fieldsFormat'];

	$csvRows = array();
	$headerFields = array();
	foreach ($fields as $afield)
	{
		$headerField = explode(':', $afield, 2);
		if (count($headerField) > 1)
			$headerFields[] = utf8_decode($headerField[1]);
		else
			$headerFields[] = utf8_decode($headerField[0]);
	}
	$csvRows[] = $headerFields;
	foreach ($arrData as $aSurveyKey => $aSurvey)
	{
		//StdData de Survey
		$csvData = array();
		foreach ($fields as $aField)
			$csvData[$aField] = '';
		//datos planos
		foreach ($aSurvey as $fieldSurvey => $dataSurvey)
		{
			if (!is_array($dataSurvey))
			{
				if (isset($fieldsFormat[$fieldSurvey]) && is_array($fieldsFormat[$fieldSurvey]) && $fieldsFormat[$fieldSurvey]['format'] != '')
				{
					/*if ($fieldsFormat[$fieldSurvey]['type'] == 'numeric')
						$dataSurvey = formatNumber($dataSurvey, $fieldsFormat[$fieldSurvey]['format']);
					else*/if ($fieldsFormat[$fieldSurvey]['type'] == 'date')
						$dataSurvey = formatDateTime($dataSurvey, $fieldsFormat[$fieldSurvey]['format']);
					$csvData[$fieldSurvey] = '"'.utf8_decode($dataSurvey).'"';
				}
				else
					$csvData[$fieldSurvey] = '"'.utf8_decode($dataSurvey).'"';
			}
		}
		//std multi data
		$bStdHasMultipleChoice = false;
		$arrStdMultipleChoiceData = array();
		if (isset($aSurvey['stdmultiData']))
		{
		foreach ($aSurvey['stdmultiData'] as $fieldStdMultiData => $stdMultiData)
		{
			if (!is_array($stdMultiData))
				$csvData[$fieldStdMultiData] = '"'.utf8_decode($stdMultiData).'"';
			elseif (count($stdMultiData) > 0)
			{
				$bStdHasMultipleChoice = true;
				if (strpos($fieldStdMultiData, ':Value') === false)
					$arrStdMultipleChoiceData[$fieldStdMultiData] = $stdMultiData;
			}
		}
		}
		if (!$bStdHasMultipleChoice)
		{
					//multi data
					if (count($aSurvey['multiData']) > 0)
					{
						foreach ($aSurvey['multiData'] as $sectionName => $sectionData)
						{
							//datos "planos" de seccion
							$arrcsvSectionData = array();
							foreach($sectionData as $sectionKey => $arrSectionValues)
							{
								$csvSectionData = array();
								$csvSectionData = $csvData;
								$bHasMultipleChoice = false;
								$arrMultipleChoiceData = array();
								foreach ($arrSectionValues as $sectionField => $sectionValue)
								{
									if (!is_array($sectionValue))
									{
										if (isset($fieldsFormat[$sectionField]) && is_array($fieldsFormat[$sectionField]) && $fieldsFormat[$sectionField]['format'] != '')
										{
											/*if ($fieldsFormat[$fieldSurvey]['type'] == 'numeric')
												$dataSurvey = formatNumber($dataSurvey, $fieldsFormat[$sectionField]['format']);
											else*/if ($fieldsFormat[$sectionField]['type'] == 'date')
												$dataSurvey = formatDateTime($sectionValue, $fieldsFormat[$sectionField]['format']);
											$csvSectionData[$sectionField] = '"'.utf8_decode($sectionValue).'"';
										}
										else
											$csvSectionData[$sectionField] = '"'.utf8_decode($sectionValue).'"';
									}
									elseif (count($sectionValue) > 0)
									{
										$bHasMultipleChoice = true;
										$arrMultipleChoiceData[$sectionField] = $sectionValue;
									}
								}
								if (!$bHasMultipleChoice)
									$csvRows[] = $csvSectionData;
								else
								{
									foreach ($arrMultipleChoiceData as $multipleChoincefield => $multipleChoiceValues)
									{	
									    //ears 2017-01-24 debe permitir generar la información para mchoice que no tengan especificado un tipo ya sea numérico o texto, así
										//como los casos en donde si se hagan estas especificaciones
										$pos=strrpos($multipleChoincefield, "Value");
										if ($pos === false) {
											$csvMultipleChoiceData = array();
											$csvMultipleChoiceData = $csvSectionData;																						
											
											$i=0;
											foreach ($multipleChoiceValues as $aMultipleChoiceValue)
											{																							
												$csvMultipleChoiceData[$multipleChoincefield] = '" '.utf8_decode($aMultipleChoiceValue).'"';													
												if (isset($csvMultipleChoiceData[$multipleChoincefield.':Value'])){
													foreach ($arrMultipleChoiceData as $mChoiceField => $mChoicevalues){
														if ($mChoiceField == ($multipleChoincefield.':Value')) {
															$csvMultipleChoiceData[$multipleChoincefield.':Value'] = '"'.utf8_decode($mChoicevalues[$i]).'"';
															break;
														}	
													}	
													$i++;
												} 												
												$csvRows[] = $csvMultipleChoiceData;
											}											
										}
									}
								}
								$arrcsvSectionData[] = $csvSectionData;
							}
						}
					}
					else
					{
						$csvRows[] = $csvData;
					}
		}
		else
		{
			$csvStdSectionData = array();
			foreach ($arrStdMultipleChoiceData as $StdMultipleChoincefield => $StdMultipleChoiceValues)
			{
				$csvStdSectionData = $csvData;
				foreach ($StdMultipleChoiceValues as $aStdMultipleChoiceValuesKey => $aStdMultipleChoiceValues)
				{
					$csvStdSectionData[$StdMultipleChoincefield] = '"'.utf8_decode($aStdMultipleChoiceValues).'"';
					if (isset($aSurvey['stdmultiData'][$StdMultipleChoincefield.':Value']))
						$csvStdSectionData[$StdMultipleChoincefield.':Value'] = '"'.utf8_decode($aSurvey['stdmultiData'][$StdMultipleChoincefield.':Value'][$aStdMultipleChoiceValuesKey]).'"';
					//multi data
					if (count($aSurvey['multiData']) > 0)
					{
						foreach ($aSurvey['multiData'] as $sectionName => $sectionData)
						{
							//datos "planos" de seccion
							$arrcsvSectionData = array();
							foreach($sectionData as $sectionKey => $arrSectionValues)
							{
								$csvSectionData = array();
								$csvSectionData = $csvStdSectionData;
								$bHasMultipleChoice = false;
								$arrMultipleChoiceData = array();
								foreach ($arrSectionValues as $sectionField => $sectionValue)
								{
									if (!is_array($sectionValue))
										$csvSectionData[$sectionField] = '"'.utf8_decode($sectionValue).'"';
									elseif (count($sectionValue) > 0)
									{
										$bHasMultipleChoice = true;
										$arrMultipleChoiceData[$sectionField] = $sectionValue;
									}
								}
								if (!$bHasMultipleChoice)
									$csvRows[] = $csvSectionData;
								else
								{
									foreach ($arrMultipleChoiceData as $multipleChoincefield => $multipleChoiceValues)
									{
									    //ears 2017-01-25 debe permitir generar la información para mchoice que no tengan especificado un tipo ya sea numérico o texto, así
										//como los casos en donde si se hagan estas especificaciones
										$pos=strrpos($multipleChoincefield, "Value");
										if ($pos === false) {
											$csvMultipleChoiceData = array();
											$csvMultipleChoiceData = $csvSectionData;																						
											
											$i=0;
											foreach ($multipleChoiceValues as $aMultipleChoiceValue)
											{																							
												$csvMultipleChoiceData[$multipleChoincefield] = '" '.utf8_decode($aMultipleChoiceValue).'"';													
												if (isset($csvMultipleChoiceData[$multipleChoincefield.':Value'])){
													foreach ($arrMultipleChoiceData as $mChoiceField => $mChoicevalues){
														if ($mChoiceField == ($multipleChoincefield.':Value')) {
															$csvMultipleChoiceData[$multipleChoincefield.':Value'] = '"'.utf8_decode($mChoicevalues[$i]).'"';
															break;
														}	
													}	
													$i++;
												} 												
												$csvRows[] = $csvMultipleChoiceData;
											}											
										}																			
										/*
										$csvMultipleChoiceData = array();
										$csvMultipleChoiceData = $csvSectionData;
										foreach ($multipleChoiceValues as $aMultipleChoiceValue)
										{
											$csvMultipleChoiceData[$multipleChoincefield] = '"'.utf8_decode($aMultipleChoiceValue).'"';
											$csvRows[] = $csvMultipleChoiceData;
										}
										*/
									}
								}
								$arrcsvSectionData[] = $csvSectionData;
							}
						}
					}
					else
					{
						$csvRows[] = $csvStdSectionData;
					}
				}
			}
		}
	}
	return $csvRows;
}

function EsurveyCSVExport($data, $fileName = '')
{
	$csvRows = EsurveyGetCSVData($data);
	$bHeader = true;
	foreach ($csvRows as $aRow)
	{
		$newRow = array();
		foreach ($aRow as $aField)
		{
			if ($bHeader)
			{
				$newRow[] = $aField;
			}
			elseif (trim($aField) != '')
			{
				//GCRUZ 2017-02-28. "Escapar" comillas dobles.
				//$newField = trim($aField, '"');
				$newField = substr($aField, 1);
				$newField = substr($newField, 0, -1);
				$newField = str_replace('"', '""', $newField);
				$newField = '"'.$newField.'"';
				$newRow[] = $newField;
			}
			else
				$newRow[] = $aField;
		}
		$bHeader = false;
		error_log(implode(',',$newRow).PHP_EOL, 3, $fileName);
	}
}

function EsurveyCSVIncremental($source, $incremental)
{
	$handleSource = fopen($source, 'a');
	$handleIncremental = fopen($incremental, 'r');
	//Leer las Columnas del incremental
	$data = fgets($handleIncremental);
	//Leer los datos
	while(!feof($handleIncremental))
	{
		$data = fgets($handleIncremental);
		//Guardar al final del archivo source
		fputs($handleSource, $data);
	}
	fclose($handleIncremental);
	fclose($handleSource);
	//Reemplazar archivo incremental con el source ya modificado
	@copy($source, $incremental);
}

function EsurveyXLSExport($data, $fileName = '', $sheet = 'WorkSheet')
{
	/** PHPExcel */
	$csvRows = EsurveyGetCSVData($data);
	//error_log(print_r($csvRows, true).PHP_EOL, 3, 'csvExport/data.log');
	require_once('../phpexcel/Classes/PHPExcel.php');

	// instantiate Excel.Application
	$bLoaded = false;
	try	
	{
		if (file_exists($fileName))
		{
			$objPHPExcel = PHPExcel_IOFactory::load($fileName);
			$bLoaded = true;
		}
		else
		{
			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
		}
	} 
	catch (Exception  $e) 
	{
		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	// Set properties
	$objPHPExcel->getProperties()->setCreator("BITAM EForms");

	$sheetNames = $objPHPExcel->getSheetNames();
	$sheetCount = $objPHPExcel->getSheetCount();
	if (array_search($sheet, $sheetNames) === false)
	{
		if (!$bLoaded)
			$objPHPExcel->setActiveSheetIndex(0);
		else
		{
			$objPHPExcel->createSheet($sheetCount);
			$objPHPExcel->setActiveSheetIndex($sheetCount);
		}
	}
	else
	{
		$objPHPExcel->setActiveSheetIndexByName($sheet);
	}

	// create xls
	$o = $objPHPExcel;

	// get active sheet
	$a = $o->getActiveSheet();
	if (strlen (trim($sheet) ) >= 30)
		$sheet = substr(trim($sheet), 0, 25).'...';
	//GCRUZ 2015-11-12. Remover caracteres no permitidos en el nombre de la hoja: *  /  ?  [  ]  ,  :   \
	$sheet = str_replace('*', '', $sheet);
	$sheet = str_replace('/', '', $sheet);
	$sheet = str_replace('?', '', $sheet);
	$sheet = str_replace('[', '', $sheet);
	$sheet = str_replace(']', '', $sheet);
	$sheet = str_replace(',', '', $sheet);
	$sheet = str_replace(':', '', $sheet);
	$sheet = str_replace('\\', '', $sheet);
	$a->setTitle(utf8_encode(trim($sheet)));

	// set default column width
	$a->getDefaultColumnDimension()->setWidth(20.71); // si ponemos 20, con los bordes queda en 19.29


	$ncounter = 1;
	$strFirst = "";
	$strCounter = "A";
	$intColumnCounter = 65;
	$arrFormattedColumns = array();

	//Columnas
	foreach ($csvRows[0] as $key => $fields)
	{
		$strIndex = $strFirst.chr($intColumnCounter);
//		echo $strIndex.$ncounter . '=' . $fields . "<br>";
		$a->setCellValue($strIndex.$ncounter, utf8_encode($fields));
		if (isset($data['fieldsFormat'][$data['fields'][$key]]) && is_array($data['fieldsFormat'][$data['fields'][$key]]) && $data['fieldsFormat'][$data['fields'][$key]]['format'] != '' && $data['fieldsFormat'][$data['fields'][$key]]['type'] == 'numeric')
			$arrFormattedColumns[$strIndex] = $data['fieldsFormat'][$data['fields'][$key]]['format'];
		if (chr($intColumnCounter) == "Z")
		{
			$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
			$intColumnCounter = 65;
		}
		else 
		{
			$intColumnCounter++;;
		}
	}
/*
	foreach ($this->ExcelColumns as $strExcelColumn)
	{
/* sbf: tomar en cuenta?
        if ($ncounter > $o->Columns->Count)
        {
          $this->SetError("SaaS.29005", "Warning: number of data columns exceeded the available Excel (R) number of columns!", __LINE__);
          break;
        }
*/
/*
		$ncounter++;
		$strIndex = $strFirst.$strCounter;
		// set value
		// @SBF-14dic2011-agregado el utf8_encode para no tener problemas con acentos
		$a->setCellValue($strIndex."1", utf8_encode($strExcelColumn));

		if ($strCounter == "Z")
		{
			$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
			$strCounter = "A";
		}
		else 
		{
			$strCounter = chr(ord($strCounter) + 1);
		}
	}
*/
	// set range alignment
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// set fill style
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');

	// set font style from array
	$arrStyle = array();
	$arrStyle["font"] = array();
	$arrStyle["font"]["name"] = "Arial";
	$arrStyle["font"]["size"] = 10;
	$arrStyle["font"]["bold"] = true;
	$arrStyle["font"]["color"] = array("rgb" => "0000FF");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->applyFromArray($arrStyle);

	// set border style from array
	$arrBorderStyle = array();
	$arrBorderStyle["allborders"] = array();
	$arrBorderStyle["allborders"]["style"] = PHPExcel_Style_Border::BORDER_THIN;
	$arrBorderStyle["allborders"]["color"] = array("rgb" => "000000");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getBorders()->applyFromArray($arrBorderStyle);
//	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);

	// select A2!
	$ncounter++;
	$a->setSelectedCell(chr(65).$ncounter);

	//Valores del rs
	for ($i=1; $i<count($csvRows); $i++)
	{
		$intColumnCounter = 65;
		$strFirst = "";
		foreach ($csvRows[$i] as $field => $value)
		{
			//GCRUZ 2017-02-28. Hacer trim en vez de replace, para no eliminar comillas "interiores".
			//$value = str_replace('"', '', $value);
			if (trim($value) != '')
			{
				//$value = trim($value, '"');
				$value = substr($value, 1);
				$value = substr($value, 0, -1);
			}
			$strIndex = $strFirst.chr($intColumnCounter);
//			echo $strIndex.$ncounter . '=' . $value . "<br>";
			$a->setCellValue($strIndex.$ncounter, utf8_encode($value));
			if (isset($arrFormattedColumns[$strIndex]) && $arrFormattedColumns[$strIndex] != '')
				$a->getStyle($strIndex.$ncounter)->getNumberFormat()->setFormatCode($arrFormattedColumns[$strIndex]);
//			$a->getStyle($strIndex.$ncounter)->getAlignment()->setWrapText(true);
//			$a->getStyle($strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
			if (chr($intColumnCounter) == "Z")
			{
				$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
				$intColumnCounter = 65;
			}
			else 
			{
				$intColumnCounter++;;
			}
		}
		$ncounter++;
	}
//	$a->getProtection()->setSheet(true);

	unset($r);
	unset($a);
	unset($o);

	$ncounter = 256;
	$strXLSPath = $fileName.($ncounter > 255 ? "" : "");

	// save file
	if ($ncounter > 255)
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	}
	else
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	}

	try 
	{
		$objWriter->save($strXLSPath);
	}
	catch (Exception  $e) 
	{
		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	unset($objWriter);

	// process completed successfully!
	return array(true);
}

function EsurveyXLSIncremental($source, $incremental)
{
	/** PHPExcel */
	require_once('../phpexcel/Classes/PHPExcel.php');

	// instantiate Excel.Application
	$bLoaded = false;
	try	
	{
		//Abrir archivo source
		$objPHPExcelSource = PHPExcel_IOFactory::load($source);
		//Abrir archivo incremental
		$objPHPExcelIncremental = PHPExcel_IOFactory::load($incremental);
	} 
	catch (Exception  $e) 
	{
		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	// Set properties
	// create xls
	// get active sheet
	$sheetSource = $objPHPExcelSource->getActiveSheet();
	$sheetIncremental = $objPHPExcelIncremental->getActiveSheet();

	//Posicionarse al final del archivo source
	$bLastSourceRow = false;
	$sourceCounter = 0;
	while(!$bLastSourceRow)
	{
		$sourceCounter++;
		if ($sheetSource->getCell('A'.$sourceCounter)->getValue() == '')
			$bLastSourceRow = true;
	}

	//Recorrer archivo incremental inciando en A2
	$highestColumn = $sheetIncremental->getHighestColumn();
	$incrementalCounter = 2;
	$bLastRow = false;
	while (!$bLastRow)
	{
		$strFirst = "";
		$strCounter = "A";
		$intColumnCounter = 65;

		do
		{
			$strIndex = $strFirst.chr($intColumnCounter);
			//Obtener valor de la celda
			$cellValue = $sheetIncremental->getCell($strIndex.$incrementalCounter)->getValue();
			//Grabar el valor de la celda en el archivo source
			$sheetSource->setCellValue($strIndex.$sourceCounter, $cellValue);
			if (chr($intColumnCounter) == "Z")
			{
				$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
				$intColumnCounter = 65;
			}
			else 
			{
				$intColumnCounter++;;
			}
		} while ($strIndex != $highestColumn);
		$incrementalCounter++;
		if ($sheetIncremental->getCell('A'.$incrementalCounter)->getValue() == '')
			$bLastRow = true;
		$sourceCounter++;
	}

	unset($sheetIncremental);
	unset($sheetSource);

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcelSource, 'Excel2007');

	try 
	{
		$objWriter->save($source);
	}
	catch (Exception  $e) 
	{
		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	unset($objWriter);
	unset($objPHPExcelSource);
	unset($objPHPExcelIncremental);

	// process completed successfully!
	//Reemplazar archivo incremental con el source ya modificado
	@copy($source, $incremental);
	return array(true);
}

?>