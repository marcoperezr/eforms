<?php
require_once('utils.inc.php');

@error_log('Contenido de files: '.print_r($_FILES,true),3,'tmptestlog.log'); 
@error_log('Count de files: '.count($_FILES),3,'tmptestlog.log');
@error_log('Contenido de get: '.print_r($_GET,true),3,'tmptestlog.log'); 
@error_log('Contenido de POST: '.print_r($_POST,true),3,'tmptestlog.log'); 
@error_log('Contenido de POST: '.print_r($_POST,true),3,'tmptestlog.log'); 

$strRequest = strtolower(getParamValue('action', 'both'));
@error_log("\r\n".'Action: '.$strRequest,3,'conchitatmp.log');

$sError = '';

if (array_key_exists("file", $_FILES))
{
	if (is_array($_FILES["file"]))
	{
		if ($_FILES['file']['error'] == 1){
			$sError = "Exceed Max Server Upload";
		}
		elseif ($_FILES['file']['error'] == 2){
			$sError = "Exceed Max Post Size";
		}
		elseif ($_FILES['file']['error'] == 3){
			$sError ="Partial Upload";
		}
		elseif ($_FILES['file']['error'] == 4){
			$sError = "No File Uploaded";
		}
		elseif (is_uploaded_file($_FILES["file"]["tmp_name"]))
		{
			$upload_config = array(
				'upload_goodext' =>
				  array (
				    0 => 'mp3',
					1 => 'mp4'
				
				  ),
				  'upload_dir' => 'log/',
				  'upload_maxsize' => 10000000 // Soporta Hasta 5 MegasBytes (MB)
			);


			$load_image = $_FILES["file"]["tmp_name"];

			require_once("test_upload_file.php");

			$upload_file = new UploadFile('file');

			if(trim($upload_config['upload_dir'])!="")
			{
				if ($upload_file->confirm_upload())
				{
					$filename = $upload_file->get_stored_file_name();
			        $file_mime_type = $upload_file->mime_type;
					$upload_file->final_move('');
					$bAdded = true;
				}
				if ($upload_file->sError != '' && !$bAdded) {
					$sError = $upload_file->sError;
				}
			}
			unset($upload_file);
		}
	}
}

?>