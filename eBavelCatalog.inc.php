<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("catalog.inc.php");
require_once("catalogmember.inc.php");
require_once("catalogfilter.inc.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");

//@LROUX 2012-11-23: Modificado para enviarlo a una subclase customizada (PCP API).
require_once("customClasses.inc.php");
require_once("eBavelCatalogValue.inc.php");
require_once('eBavelIntegration.inc.php');

class BITAMeBavelCatalog extends BITAMCatalog 
{
    function addButtons($aUser)
    {
        if (!$this->isNewObject())
        {
    ?>
        <button id="btnCatalogValues" class="alinkescfav" onclick="setParentValuesIDs();"><img src="images/catalogEntry.gif" alt="<?=translate('Catalog values')?>" title="<?=translate('Catalog values')?>" displayMe="1" /> <?=translate("Catalog values")?></button>
    <?  }
    }
    
    function generateBeforeFormCode($aUser)
    {   
?>
    <script language="JavaScript">

        /*@AAL 19/03/2015: Variables empladas para controlar el switcheo entre una aplicación, Forma o Vista*/
        var Type = <?= $this->eBavelFormType ?>;
        var AppID = <?= $this->eBavelAppID ?>;
        /*@AAL 19/03/2015:: Esta función actualiza la lista que mostrará ya sea los elementos de las Formas o de las vistas*/
        function UpdateCatalogeBavel()
        {
            var ObjComb = BITAMeBavelCatalog_SaveForm.eBavelFormID;
            //Si no se ha seleccionado ninguna aplicación entonces hacemos todo a NONE
            if(BITAMeBavelCatalog_SaveForm.eBavelAppID.value == "0" || ObjComb.options.length == 0 || AppID.toString() != BITAMeBavelCatalog_SaveForm.eBavelAppID.value){
                BITAMeBavelCatalog_SaveForm.eBavelFormType.selectedIndex = 0;
                BITAMeBavelCatalog_SaveForm.eBavelFormID.selectedIndex = 0;
                var eBavelFormID = BITAMeBavelCatalog_SaveForm.eBavelFormIDTemp.cloneNode(true);
                ObjComb.parentNode.replaceChild(eBavelFormID, ObjComb);
                eBavelFormID.style.display = "inline";
                eBavelFormID.setAttribute("name",  "eBavelFormID");
            }
            else{
                //Si hemos selecionado una aplicación y la lista de las formas o vistas contienen elementos
                if(BITAMeBavelCatalog_SaveForm.eBavelFormIDTemp.options.length > 0 || BITAMeBavelCatalog_SaveForm.eBavelViewIDTemp.options.length > 0){
                    var eBavelFormID = null;
                    //Si estamos solicitando los elementos de las formas.
                    if(BITAMeBavelCatalog_SaveForm.eBavelFormType.value == "<?=ebftForms?>" && Type != 0){
                        eBavelFormID = BITAMeBavelCatalog_SaveForm.eBavelFormIDTemp.cloneNode(true);
                        ObjComb.parentNode.replaceChild(eBavelFormID, ObjComb);
                        eBavelFormID.style.display = "inline";
                        eBavelFormID.setAttribute("name",  "eBavelFormID");
                        Type = 0;
                    } //Si hemos selecconado la opción 1 estamos indicando que queremos los elementos de las vistas y clonamos el Cbbx oculto
                    else if(BITAMeBavelCatalog_SaveForm.eBavelFormType.value == "<?=ebftViews?>" && Type != 1){
                        eBavelFormID = BITAMeBavelCatalog_SaveForm.eBavelViewIDTemp.cloneNode(true);
                        ObjComb.parentNode.replaceChild(eBavelFormID, ObjComb);
                        eBavelFormID.style.display = "inline";
                        eBavelFormID.setAttribute("name",  "eBavelFormID");
                        Type = 1;
                    }//Como no se mostraban visibles entonces los mostramos.
                    ObjComb = null;
                }
                else{//Si se llega a este caso es por que no hay elementos ya sea en las vistas o formas y clonamos el Cbbx oculto
                    ObjComb.options.length = 0;
                    ObjComb.options.add(new Option("(<?=translate('None')?>)", "0"));
                }
            }
        }
        //@AAL
        
        function setParentValuesIDs()
        {
            frmSetCatalogValues.submit();
        }
    </script>
<?
    }
    
    function generateAfterFormCode($aUser)
    {
?>
        <form name="frmSetCatalogValues" action="main.php?BITAM_SECTION=eBavelCatalogValueCollection&CatalogID=<?=$this->CatalogID?>" method="post" target="body">
        </form>
<?
    }
}

?>