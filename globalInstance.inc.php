<?php


class BITAMGlobalInstance
{
	// Colección de todos los cubos del SDK para obtener datos de sus tablas
	static $arrAllSDKCubes=array();
	// Colección de todas las dimensiones indexadas por "CBO".idCubo."DIM".idDimension (Consecutivo)
	static $arrAllDimensions=array();
	// Colección de todos los atributos (indicadores base) indexados por "CBO".idCubo."ATR".idAtributo (Consecutivo)
	static $arrAllAttributes=array();
	// Colección de todas las agregaciones indexadas por "CBO".idCubo."AGR".idAgregacion (Consecutivo)
	static $arrAllAggregations=array();
	// Colección de todos los grupos de indicadores indexadas por "IGR".idGrupo
	static $arrAllIndicatorGroups=array();
	// Colección de llaves de dimensiones indexados por "CBO".idCubo."DIM".idDimension."FLT".FiltroCompleto (puede ser vacio)
	static $arrAllDimensionKeysByFilter=array();
	// Colección de valores de dimensiones (clase BITAMDimensionValue) indexados por "CBO".idCubo."DIM".idDimension."KEY".idValor
	static $arrAllDimensionValues=array();
	// Colección de valores de dimensiones (manejados como arreglo) indexados por "CBO".idCubo."DIM".idDimension."FLT".FiltroCompleto (puede ser vacio)
	static $arrAllDimensionValuesByFilter=array();
	//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
	//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
	static $arrAllDimensionSubrrKeysByFilter=array();
	static $arrAllDimensionParentsFldsByFilter=array();
	//@JAPR
	// Colección de todos los usuarios del repositorio
	static $arrAllUsers=array();
	// Colección de todas las versiones globales del repositorio
	static $arrAllGVersions=array();
	// Fechas formateadas
	static $arrFormattedDates=array();
	// Fechas desplazadas indexadas por 'DTE'.Fecha.'PER'.idPeriodo.'SHT'.Desplazamiento
	static $arrAllShiftedDates=array();
	//Coleccion de todos los array de filtros de usuario
	static $arrAllUsrFilters=array();
	static $arrAllUsrGpsFilters=array();
	//Coleccion de todos los objetos tipo BITAMIndicator
	static $arrAllIndicators=array();
	//Coleccion de las jerarquias padres de dimensiones
	static $arrAllFatherHierarchyDimensions=array();
	//Coleccion de las jerarquias hijas de dimensiones
	static $arrAllChildHierarchyDimensions=array();
	//Coleccion de las dimensiones de tipo periodo
	static $arrAllPeriodDimensions=array();
	//Coleccion de las instancias BITAMCube
	static $arrAllBITAMCube=array();
	static $arrAllBITAMCubeByName=array();
	//Coleccion de las instancias BITAMPeriod
	static $arrAllBITAMPeriod=array();

	//Conjunto de NomFisicoK por Cubo y Nombre Logico
	static $arrAllNomFisK=array();
	//Conjunto de Consecutivo por Cubo y Nombre Logico de dimensión
	static $arrAllConsecutives=array();
	
	//Conjunto de todos los descripkeys de un cubo
	static $arrAllDescriptKeysCollection=array();
	//Contenido de Tablas
	static $arrTables=array();
	//Contenido de getTuple(Cubo,Indicador,Dimension,Valor,Fecha)
	static $arrTuples=array();
	//Contenido de getTupleArray(Cubo,Indicador,ArrayDimensionValor,Fecha)
	static $arrTuplesArray=array();
	//Contenido de getDimField(Cubo,Dimension,Campo,Valor,ValorParametro)
	static $arrDimFields=array();
	//Claves de las fechas de un cubo según la tabla de tiempo, indexadas por 'CBO'.idCubo.'DTE'.Fecha
	static $arrAllDateIDs=array();
	//Fechas comprendidas entre un rango especificado ajustadas al periodo, indexadas por 'PER'.idPeriodo.'SDTE'.FechaInicio.'EDTE'.FechaFinal
	static $arrAllDateRanges=array();
	//Arreglos de tamaño fijo vacios (valor default especificado) indexados por 'ROW'.numRows.'COL'.numCols.'IVAL'.valDefault
	static $arrAllNewArrays=array();
	//Coleccion de todos los objetos tipo BITAMIndicator cuando fueron cargados por nombre y clave de cubo, indexados por 'CBO'.idCubo.'NME'.NomIndic
	static $arrAllIndicatorsByNameAndCubeID=array();
	//Boleano que indica si un campo existe en una tabla, indexado por 'TBL'.nomTabla.'FLD'.nomCampo
	static $arrAllExistsTableField=array();
	//Colección con todos los cubos a los que pertenecen todos los indicadores del respositorio (query único)
	static $arrAllCubeIDsByIndicatorIDs=array();
	//Colección con los IDs de dimensión (en forma de arreglo) que son compartidos entre dos cubos, indexada por 'SRC'.sourceCubeID.'TRG'.targetCubeID.'DIM'.idsDimension
	static $arrAllDimensionIDsBetweenCubes=array();
	//Colección de colecciones de dimensiones indexadas por la serie de parámetros utilizada para cargarla, como sigue:
	//'CBO'.idCubo.'DIM'.idsDimen.'IN'.incluirIn.'APER'.AgregarPeriodos.'DUP'.ExcluirDuplicados
	static $arrAllDimensionCollections=array();
	//Colección de colecciones de dimensiones tiempo indexadas por 'CBO'.idCubo
	static $arrAllPeriodDimensionCollections=array();
	//Colección con la estructura de una tabla tal como la regresa el MetaColumns, indexada por 'TBL'.nomTabla.'CNN'.(true=Datos|false=Repositorio)
	static $arrAllMetaColumns=array();
	//Consecutivo de una dimension dado el nombre de la dimension y la clave de cubo
	static $arrDimIDs=array();
	//Conjunto de las llaves de fechas de la tabla de tiempos, dado un nombre de tabla, nombre de campo de busqueda, nombre de campo llave, y la fecha buscada
	static $arrDateKeys=array();
	//Contiene los IDs de dimensiones por nombre de dimensión
	static $arrAllDimensionIDs=array();
	//Contiene el ID del cubo por su nombre y tipo de cubo
	static $arrAllCubeIDsByCubeNameAndCubeType=array();
	//Contiene el ID del indicador por su nombre y id de cubo
	static $arrAllIndicatorIDsByIndicatorNameAndCubeID=array();
	//Contiene la configuracion de usar agregaciones por cada cubo
	static $arrAllUseAggregationsByCubeID=array();
	//Conjunto de todos los procedimientos almacenados del proyecto
	static $arrAllGenSPCollection=array();
	//Conjunto de todas las fechas de inicio de periodos presupuestales
	static $arrStartDatesPeriod=array();
	//Conjunto de todas las fechas final de periodos presupuestales
	static $arrEndDatesPeriod=array();
	//Colección de todos los joins de dimensiones por valor dimension indexados por "CBO".idCubo."TBL".tablename."FKBD".nomfisicokbd."TIPDK".tipodatok."KEYVAL".keyvalue
	static $arrAllDimJoinValueByKeyValue=array();

	static function ObjectCount($aArrayName)
	{
		$intCount=0;
		$strCode='';
		
		if(trim($aArrayName)=='')
		{
			return 0;
		}
		
		$strCode='$intCount=count(self::'.$aArrayName.');';
		if($strCode!='')
		{
			eval($strCode);
		}
		
		return $intCount;
	}
	
	static function ResetCollections()
	{
		self::$arrAllSDKCubes=array();
		self::$arrAllDimensions=array();
		self::$arrAllAttributes=array();
		self::$arrAllAggregations=array();
		self::$arrAllIndicatorGroups=array();
		self::$arrAllDimensionKeysByFilter=array();
		self::$arrAllDimensionValues=array();
		self::$arrAllDimensionValuesByFilter=array();
		self::$arrAllDimensionSubrrKeysByFilter=array();
		self::$arrAllDimensionParentsFldsByFilter=array();
		self::$arrAllUsers=array();
		self::$arrAllGVersions=array();
		self::$arrFormattedDates=array();
		self::$arrAllShiftedDates=array();
		self::$arrAllUsrFilters=array();
		self::$arrAllUsrGpsFilters=array();
		self::$arrAllIndicators=array();
		self::$arrAllFatherHierarchyDimensions=array();
		self::$arrAllChildHierarchyDimensions=array();
		self::$arrAllPeriodDimensions=array();
		self::$arrAllBITAMCube=array();
		self::$arrAllBITAMCubeByName=array();
		self::$arrAllBITAMPeriod=array();
		self::$arrAllNomFisK=array();
		self::$arrAllConsecutives=array();
		self::$arrAllDescriptKeysCollection=array();
		self::$arrTables=array();
		self::$arrTuples=array();
		self::$arrTuplesArray=array();		
		self::$arrDimFields=array();
		self::$arrAllDateIDs=array();
		self::$arrAllDateRanges=array();
		self::$arrAllNewArrays=array();
		self::$arrAllIndicatorsByNameAndCubeID=array();
		self::$arrAllExistsTableField=array();
		self::$arrAllCubeIDsByIndicatorIDs=array();
		self::$arrAllDimensionIDsBetweenCubes=array();
		self::$arrAllDimensionCollections=array();
		self::$arrAllPeriodDimensionCollections=array();
		self::$arrAllMetaColumns=array();
		self::$arrDimIDs=array();
		self::$arrDateKeys=array();		
		self::$arrAllDimensionIDs=array();
		self::$arrAllCubeIDsByCubeNameAndCubeType=array();
		self::$arrAllIndicatorIDsByIndicatorNameAndCubeID=array();
		self::$arrAllUseAggregationsByCubeID=array();
		self::$arrAllGenSPCollection=array();
		self::$arrStartDatesPeriod=array();
		self::$arrEndDatesPeriod=array();
		self::$arrAllDimJoinValueByKeyValue=array();
	}

	static function &GetSDKCubeWithID($aCubeID)
	{
		$strInstanceKey='CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllSDKCubes[$strInstanceKey]))
		{
			$theInstance=self::$arrAllSDKCubes[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSDKCubeWithID($aCubeID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID;
		if(!isset(self::$arrAllSDKCubes[$strInstanceKey]))
		{
			self::$arrAllSDKCubes[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetDimensionInstanceWithID($aCubeID, $aDimensionID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID;
		$theInstance=null;
		if(isset(self::$arrAllDimensions[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensions[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionInstanceWithID($aCubeID, $aDimensionID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID;
		if(!isset(self::$arrAllDimensions[$strInstanceKey]))
		{
			self::$arrAllDimensions[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetAttributeInstanceWithID($aCubeID, $anAttributeID)
	{
		$strInstanceKey='CBO'.$aCubeID.'ATR'.$anAttributeID;
		$theInstance=null;
		if(isset(self::$arrAllAttributes[$strInstanceKey]))
		{
			$theInstance=self::$arrAllAttributes[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddAttributeInstanceWithID($aCubeID, $anAttributeID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'ATR'.$anAttributeID;
		if(!isset(self::$arrAllAttributes[$strInstanceKey]))
		{
			self::$arrAllAttributes[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetAggregationInstanceWithID($aCubeID, $anAggregationID)
	{
		$strInstanceKey='CBO'.$aCubeID.'AGR'.$anAggregationID;
		$theInstance=null;
		if(isset(self::$arrAllAggregations[$strInstanceKey]))
		{
			$theInstance=self::$arrAllAggregations[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddAggregationInstanceWithID($aCubeID, $anAggregationID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'AGR'.$anAggregationID;
		if(!isset(self::$arrAllAggregations[$strInstanceKey]))
		{
			self::$arrAllAggregations[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetDimensionValueInstanceWithID($aCubeID, $aDimensionID, $aValueID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'KEY'.$aValueID;
		$theInstance=null;
		if(isset(self::$arrAllDimensionValues[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionValues[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionValueInstanceWithID($aCubeID, $aDimensionID, $aValueID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'KEY'.$aValueID;
		if(!isset(self::$arrAllDimensionValues[$strInstanceKey]))
		{
			self::$arrAllDimensionValues[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetIndicatorGroupInstanceWithID($anIndicatorGroupID)
	{
		$strInstanceKey='IGR'.$anIndicatorGroupID;
		$theInstance=null;
		if(isset(self::$arrAllIndicatorGroups[$strInstanceKey]))
		{
			$theInstance=self::$arrAllIndicatorGroups[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddIndicatorGroupInstanceWithID($anIndicatorGroupID, &$aObject)
	{
		$strInstanceKey='IGR'.$anIndicatorGroupID;
		if(!isset(self::$arrAllIndicatorGroups[$strInstanceKey]))
		{
			self::$arrAllIndicatorGroups[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetDimensionKeysWithFilter($aCubeID, $aDimensionID, $aFilter)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		$theInstance=null;
		if(isset(self::$arrAllDimensionKeysByFilter[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionKeysByFilter[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionKeysWithFilter($aCubeID, $aDimensionID, $aFilter, &$aKeysArr)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		if(!isset(self::$arrAllDimensionKeysByFilter[$strInstanceKey]))
		{
			self::$arrAllDimensionKeysByFilter[$strInstanceKey] =& $aKeysArr;
		}
	}
	
	static function &GetDimensionValuesWithFilter($aCubeID, $aDimensionID, $aFilter)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		$theInstance=null;
		if(isset(self::$arrAllDimensionValuesByFilter[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionValuesByFilter[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionValuesWithFilter($aCubeID, $aDimensionID, $aFilter, &$aValuesArr)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		if(!isset(self::$arrAllDimensionValuesByFilter[$strInstanceKey]))
		{
			self::$arrAllDimensionValuesByFilter[$strInstanceKey] =& $aValuesArr;
		}
	}
	
	//@JAPR 2010-07-19: Corregido un bug, no se estaba grabando el padre correctamente cuando el Join a su tabla no era el mismo que el ID
	//además no existe forma de identificar el tipo de dato del Join por lo que se asumirá Numérico por ahora para soporte a KPIOnline (#22023)
	static function &GetDimensionSubrrKeysWithFilter($aCubeID, $aDimensionID, $aFilter)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		$theInstance=null;
		if(isset(self::$arrAllDimensionSubrrKeysByFilter[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionSubrrKeysByFilter[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionSubrrKeysWithFilter($aCubeID, $aDimensionID, $aFilter, &$aValuesArr)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		if(!isset(self::$arrAllDimensionSubrrKeysByFilter[$strInstanceKey]))
		{
			self::$arrAllDimensionSubrrKeysByFilter[$strInstanceKey] =& $aValuesArr;
		}
	}
	
	static function &GetDimensionParentsFldsWithFilter($aCubeID, $aDimensionID, $aFilter)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		$theInstance=null;
		if(isset(self::$arrAllDimensionParentsFldsByFilter[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionParentsFldsByFilter[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionParentsFldsWithFilter($aCubeID, $aDimensionID, $aFilter, &$aValuesArr)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'FLT'.$aFilter;
		if(!isset(self::$arrAllDimensionParentsFldsByFilter[$strInstanceKey]))
		{
			self::$arrAllDimensionParentsFldsByFilter[$strInstanceKey] =& $aValuesArr;
		}
	}
	//@JAPR
	
	static function &GetUserInstanceWithID($aUserID)
	{
		$strInstanceKey='USR'.$aUserID;
		$theInstance=null;
		if(isset(self::$arrAllUsers[$strInstanceKey]))
		{
			$theInstance=self::$arrAllUsers[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddUserInstanceWithID($aUserID, &$aObject)
	{
		$strInstanceKey='USR'.$aUserID;
		if(!isset(self::$arrAllUsers[$strInstanceKey]))
		{
			self::$arrAllUsers[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetGlobalVersionInstanceWithID($aVersionID)
	{
		$strInstanceKey='GVER'.$aVersionID;
		$theInstance=null;
		if(isset(self::$arrAllGVersions[$strInstanceKey]))
		{
			$theInstance=self::$arrAllGVersions[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddGlobalVersionInstanceWithID($aVersionID, &$aObject)
	{
		$strInstanceKey='GVER'.$aVersionID;
		if(!isset(self::$arrAllGVersions[$strInstanceKey]))
		{
			self::$arrAllGVersions[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetFormattedDateTime($aDateTime,$aFormat)
	{
		$strInstanceKey='DTE'.$aDateTime.'FMT'.$aFormat;
		$theFormattedDate=null;
		if(isset(self::$arrFormattedDates[$strInstanceKey]))
		{
			$theFormattedDate=self::$arrFormattedDates[$strInstanceKey];
		}
		return $theFormattedDate;
	}
	
	static function AddFormattedDateTime($aDateTime,$aFormat,$aFormattedDate)
	{
		$strInstanceKey='DTE'.$aDateTime.'FMT'.$aFormat;
		if(!isset(self::$arrFormattedDates[$strInstanceKey]))
		{
			self::$arrFormattedDates[$strInstanceKey] = $aFormattedDate;
		}
	}
	
	/* Optimiza los llamados a BITAMPeriod->periodDateTime
		Desplaza la fecha especificada $aPeriodsToAdd periodos hacia adelante o atrás, basada en el tipo de periodo especificado
	con $aPeriodType
	*/
	static function &GetPeriodDateTime($aDateTime,$aPeriodsToAdd,$aPeriodType)
	{
		$strInstanceKey='DTE'.$aDateTime.'PER'.$aPeriodType.'SHT'.$aPeriodsToAdd;
		$theFormattedDate=null;
		if(isset(self::$arrAllShiftedDates[$strInstanceKey]))
		{
			$theFormattedDate=self::$arrAllShiftedDates[$strInstanceKey];
		}
		return $theFormattedDate;
	}
	
	static function AddPeriodDateTime($aDateTime,$aPeriodsToAdd,$aPeriodType,$aShiftedDate)
	{
		$strInstanceKey='DTE'.$aDateTime.'PER'.$aPeriodType.'SHT'.$aPeriodsToAdd;
		if(!isset(self::$arrAllShiftedDates[$strInstanceKey]))
		{
			self::$arrAllShiftedDates[$strInstanceKey] = $aShiftedDate;
		}
	}
	
	static function &GetUsrFilter($UserID, $aCubeID)
	{
		$strInstanceKey='USR'.$UserID.'CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllUsrFilters[$strInstanceKey]))
		{
			$theInstance=self::$arrAllUsrFilters[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddUserFilter($UserID, $aCubeID, &$aObject)
	{
		$strInstanceKey='USR'.$UserID.'CBO'.$aCubeID;
		if(!isset(self::$arrAllUsrFilters[$strInstanceKey]))
		{
			self::$arrAllUsrFilters[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetUsrGpsFilter($UserID, $aCubeID)
	{
		$strInstanceKey='USR'.$UserID.'CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllUsrGpsFilters[$strInstanceKey]))
		{
			$theInstance=self::$arrAllUsrGpsFilters[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddUserGpsFilter($UserID, $aCubeID, &$aObject)
	{
		$strInstanceKey='USR'.$UserID.'CBO'.$aCubeID;
		if(!isset(self::$arrAllUsrGpsFilters[$strInstanceKey]))
		{
			self::$arrAllUsrGpsFilters[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetIndicator($aIndicatorID)
	{
		$strInstanceKey='IND'.$aIndicatorID;
		$theInstance=null;
		if(isset(self::$arrAllIndicators[$strInstanceKey]))
		{
			$theInstance=self::$arrAllIndicators[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddIndicator($aIndicatorID, &$aObject)
	{
		$strInstanceKey='IND'.$aIndicatorID;
		if(!isset(self::$arrAllIndicators[$strInstanceKey]))
		{
			self::$arrAllIndicators[$strInstanceKey] =& $aObject;
		}
		//Optimizado para agregar el indicador además a la colección por nombre y clave de cubo
		$strInstanceKey='CBO'.$aObject->cla_concepto.'NME'.$aObject->nom_indicador;
		if(!isset(self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey]))
		{
			self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetFatherHierarchy($aCubeID, $aDimensionID, $aHierarchyID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'HRCY'.$aHierarchyID;
		$theInstance=null;
		if(isset(self::$arrAllFatherHierarchyDimensions[$strInstanceKey]))
		{
			$theInstance=self::$arrAllFatherHierarchyDimensions[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddFatherHierarchy($aCubeID, $aDimensionID, $aHierarchyID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'HRCY'.$aHierarchyID;
		if(!isset(self::$arrAllFatherHierarchyDimensions[$strInstanceKey]))
		{
			self::$arrAllFatherHierarchyDimensions[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetChildHierarchy($aCubeID, $aDimensionID, $aHierarchyID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'HRCY'.$aHierarchyID;
		$theInstance=null;
		if(isset(self::$arrAllChildHierarchyDimensions[$strInstanceKey]))
		{
			$theInstance=self::$arrAllChildHierarchyDimensions[$strInstanceKey];
		}
		return $theInstance;
	}

	static function AddChildHierarchy($aCubeID, $aDimensionID, $aHierarchyID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionID.'HRCY'.$aHierarchyID;
		if(!isset(self::$arrAllChildHierarchyDimensions[$strInstanceKey]))
		{
			self::$arrAllChildHierarchyDimensions[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetPeriodDimension($aCubeID, $aPeriodID, $withValues)
	{
		$strInstanceKey='CBO'.$aCubeID.'PERDIM'.$aPeriodID.'WITHVAL'.$withValues;
		$theInstance=null;
		if(isset(self::$arrAllPeriodDimensions[$strInstanceKey]))
		{
			$theInstance=self::$arrAllPeriodDimensions[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddPeriodDimension($aCubeID, $aPeriodID, $withValues, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'PERDIM'.$aPeriodID.'WITHVAL'.$withValues;
		if(!isset(self::$arrAllPeriodDimensions[$strInstanceKey]))
		{
			self::$arrAllPeriodDimensions[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetBITAMCube($aCubeID)
	{
		$strInstanceKey='CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllBITAMCube[$strInstanceKey]))
		{
			$theInstance=self::$arrAllBITAMCube[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddBITAMCube($aCubeID, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID;
		if(!isset(self::$arrAllBITAMCube[$strInstanceKey]))
		{
			self::$arrAllBITAMCube[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetBITAMCubeByName($aCubeName)
	{
		$strInstanceKey='CBOName'.$aCubeName;
		$theInstance=null;
		if(isset(self::$arrAllBITAMCubeByName[$strInstanceKey]))
		{
			$theInstance=self::$arrAllBITAMCubeByName[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddBITAMCubeByName($aCubeName, &$aObject)
	{
		$strInstanceKey='CBOName'.$aCubeName;
		if(!isset(self::$arrAllBITAMCubeByName[$strInstanceKey]))
		{
			self::$arrAllBITAMCubeByName[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetBITAMPeriod($aPeriodID)
	{
		$strInstanceKey='PER'.$aPeriodID;
		$theInstance=null;
		if(isset(self::$arrAllBITAMPeriod[$strInstanceKey]))
		{
			$theInstance=self::$arrAllBITAMPeriod[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddBITAMPeriod($aPeriodID, &$aObject)
	{
		$strInstanceKey='PER'.$aPeriodID;
		if(!isset(self::$arrAllBITAMPeriod[$strInstanceKey]))
		{
			self::$arrAllBITAMPeriod[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetDescriptKeysCollection($aCubeID)
	{
		$strInstanceKey='DESCKEY'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllDescriptKeysCollection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDescriptKeysCollection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDescriptKeysCollection($aCubeID, &$aObject)
	{
		$strInstanceKey='DESCKEY'.$aCubeID;
		if(!isset(self::$arrAllDescriptKeysCollection[$strInstanceKey]))
		{
			self::$arrAllDescriptKeysCollection[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetNomFisKByCubeIDAndNomLogico($aCubeID, $aNomLogico)
	{
		$strInstanceKey='NOMFISK'.$aCubeID.$aNomLogico;
		$theInstance=null;
		if(isset(self::$arrAllNomFisK[$strInstanceKey]))
		{
			$theInstance=self::$arrAllNomFisK[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddNomFisKByCubeIDAndNomLogico($aCubeID, $aNomLogico, $aObject)
	{
		$strInstanceKey='NOMFISK'.$aCubeID.$aNomLogico;
		if(!isset(self::$arrAllNomFisK[$strInstanceKey]))
		{
			self::$arrAllNomFisK[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetConsecutiveByCubeIDAndNomLogico($aCubeID, $aNomLogico)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aNomLogico;
		$theInstance=null;
		if(isset(self::$arrAllConsecutives[$strInstanceKey]))
		{
			$theInstance=self::$arrAllConsecutives[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddConsecutiveyCubeIDAndNomLogico($aCubeID, $aNomLogico, &$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aNomLogico;
		if(!isset(self::$arrAllConsecutives[$strInstanceKey]))
		{
			self::$arrAllConsecutives[$strInstanceKey] =& $aObject;
		}
	}
	
	static function &GetDimensionIDByName($aCubeID, $aDimensionName)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionName;
		$theInstance=null;
		if(isset(self::$arrAllDimensionIDs[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionIDs[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionIDByName($aCubeID, $aDimensionName, $dimensionID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$aDimensionName;
		if(!isset(self::$arrAllDimensionIDs[$strInstanceKey]))
		{
			self::$arrAllDimensionIDs[$strInstanceKey] =& $dimensionID;
		}
	}
	
	//Obtiene el Id del cubo por medio del nombre y tipo del cubo
	static function &GetCubeIDByCubeNameAndCubeType($aCubeName, $aCubeType)
	{
		$strInstanceKey='CBONAME'.$aCubeName.'CBOTYPE'.$aCubeType;
		$theInstance=null;
		if(isset(self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega el Id del cubo por medio del nombre y tipo del cubo
	static function AddCubeIDByCubeNameAndCubeType($aCubeName, $aCubeType, $aCubeID)
	{
		$strInstanceKey='CBONAME'.$aCubeName.'CBOTYPE'.$aCubeType;
		if(!isset(self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey]))
		{
			self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey] =& $aCubeID;
		}
	}
	
	//Obtiene el Id del cubo por medio del nombre y tipo del cubo
	static function &GetIndicatorIDByIndicatorNameAndCubeID($aCubeName, $aCubeType)
	{
		$strInstanceKey='CBONAME'.$aCubeName.'CBOTYPE'.$aCubeType;
		$theInstance=null;
		if(isset(self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega el Id del cubo por medio del nombre y tipo del cubo
	static function AddIndicatorIDByIndicatorNameAndCubeID($aCubeName, $aCubeType, $aCubeID)
	{
		$strInstanceKey='CBONAME'.$aCubeName.'CBOTYPE'.$aCubeType;
		if(!isset(self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey]))
		{
			self::$arrAllCubeIDsByCubeNameAndCubeType[$strInstanceKey] =& $aCubeID;
		}
	}
	
	static function &GetTableContent($aTableName,$filter)
	{
		$strInstanceKey='TABLE_'.$aTableName.'_FILTER_'.$filter;
		$theInstance=null;
		if(isset(self::$arrTables[$strInstanceKey]))
		{
			$theInstance=self::$arrTables[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddTableContent($aTableName, &$aObject,$filter)
	{
		$strInstanceKey='TABLE_'.$aTableName.'_FILTER_'.$filter;
		if(!isset(self::$arrTables[$strInstanceKey]))
		{
			self::$arrTables[$strInstanceKey] =& $aObject;
		}
	}
	
	static function GetTuples($cubeName, $indicatorName, $dimensionName, $dimensionValue, $date)
	{
		$strInstanceKey='CBO'.$cubeName.'IND'.$indicatorName.'DIM'.$dimensionName.'VAL'.$dimensionValue.'DATE'.$date;
		$theInstance=null;
		if(isset(self::$arrTuples[$strInstanceKey]))
		{
			$theInstance=self::$arrTuples[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddTuples($cubeName, $indicatorName, $dimensionName, $dimensionValue, $date, $aObject)
	{
		$strInstanceKey='CBO'.$cubeName.'IND'.$indicatorName.'DIM'.$dimensionName.'VAL'.$dimensionValue.'DATE'.$date;
		if(!isset(self::$arrTuples[$strInstanceKey]))
		{
			self::$arrTuples[$strInstanceKey] =& $aObject;
		}
	}
	
	static function GetTuplesArray($cubeName, $indicatorName, $dimensionsNameValue, $date)
	{
		$strInstanceKey='CBO'.$cubeName.'IND'.$indicatorName.'DIMVAL'.$dimensionsNameValue.'DATE'.$date;
		$theInstance=null;
		if(isset(self::$arrTuplesArray[$strInstanceKey]))
		{
			$theInstance=self::$arrTuplesArray[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddTuplesArray($cubeName, $indicatorName, $dimensionsNameValue, $date, $aObject)
	{
		$strInstanceKey='CBO'.$cubeName.'IND'.$indicatorName.'DIMVAL'.$dimensionsNameValue.'DATE'.$date;
		if(!isset(self::$arrTuplesArray[$strInstanceKey]))
		{
			self::$arrTuplesArray[$strInstanceKey] =& $aObject;
		}
	}

	static function GetDimFields($cubeName, $dimensionName, $fieldName, $dimensionValue, $parameterValue)
	{
		$strInstanceKey='CBO'.$cubeName.'DIM'.$dimensionName.'FLD'.$fieldName.'VAL'.$dimensionValue.'PAR'.$parameterValue;
		$theInstance=null;
		if(isset(self::$arrDimFields[$strInstanceKey]))
		{
			$theInstance=self::$arrDimFields[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimFields($cubeName, $dimensionName, $fieldName, $dimensionValue, $parameterValue, $aObject)
	{
		$strInstanceKey='CBO'.$cubeName.'DIM'.$dimensionName.'FLD'.$fieldName.'VAL'.$dimensionValue.'PAR'.$parameterValue;
		if(!isset(self::$arrDimFields[$strInstanceKey]))
		{
			self::$arrDimFields[$strInstanceKey] = $aObject;
		}
	}

	/* Optimiza los llamados a saveCalculatedIndicator.checkTimeTableCalcInd
		Verifica si la fecha especificada ya se encuentra grabada en el cubo, si es así regresa su ID, si no entonces la agrega
	*/
	static function &GetDateIDFromCube($aCubeID,$aDateTime)
	{
		$strInstanceKey='CBO'.$aCubeID.'DTE'.$aDateTime;
		$theInstance=null;
		if(isset(self::$arrAllDateIDs[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDateIDs[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDateIDFromCube($aCubeID,$aDateTime,$aDateTimeID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DTE'.$aDateTime;
		if(!isset(self::$arrAllDateIDs[$strInstanceKey]))
		{
			self::$arrAllDateIDs[$strInstanceKey] = $aDateTimeID;
		}
	}
	
	/* Optimiza los llamados a artusmetadata.getRange
		Obtiene las fechas contenidas entre un rango, ajustadas al periodo especificado
	*/
	static function &GetDateRange($aStartDate,$anEndDate,$aPeriodID, $separator)
	{
		$strInstanceKey='PER'.$aPeriodID.'SDTE'.$aStartDate.'EDTE'.$anEndDate.'SEP'.$separator;
		$theInstance=null;
		if(isset(self::$arrAllDateRanges[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDateRanges[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDateRange($aStartDate,$anEndDate,$aPeriodID,$separator,$aDatesRange)
	{
		$strInstanceKey='PER'.$aPeriodID.'SDTE'.$aStartDate.'EDTE'.$anEndDate.'SEP'.$separator;
		if(!isset(self::$arrAllDateRanges[$strInstanceKey]))
		{
			self::$arrAllDateRanges[$strInstanceKey] = $aDatesRange;
		}
	}

	/* Optimiza los llamados a utils.GetNewArray, formulaResult::GetNewArray y formulaEvaluator::GetNewArray
		Genera un arreglo del tamaño especificado rellenandolo con el valor indicado
	*/
	static function &GetNewEmptyArray($aNumRows,$aNumColumns,$anInitialValue)
	{
		if (is_array($anInitialValue))
		{
			return null;
		}
		if (is_null($anInitialValue))
		{
			$anInitialValue = '[NULL]';
		}
		$strInstanceKey='ROW'.$aNumRows.'COL'.$aNumColumns.'IVAL'.$anInitialValue;
		$theInstance=null;
		if(isset(self::$arrAllNewArrays[$strInstanceKey]))
		{
			$theInstance=self::$arrAllNewArrays[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddNewEmptyArray($aNumRows,$aNumColumns,$anInitialValue,$aNewArray)
	{
		if (is_null($anInitialValue))
		{
			$anInitialValue = '[NULL]';
		}
		$strInstanceKey='ROW'.$aNumRows.'COL'.$aNumColumns.'IVAL'.$anInitialValue;
		if(!isset(self::$arrAllNewArrays[$strInstanceKey]))
		{
			self::$arrAllNewArrays[$strInstanceKey] = $aNewArray;
		}
	}

	/* Optimiza los llamados a BITAMIndicator.NewInstanceWithIndicatorNameAndCubeID
		Carga un indicador basado en su nombre y cubo
	*/
	static function &GetIndicatorByNameAndCubeID($aIndicatorName,$aCubeID)
	{
		$strInstanceKey='CBO'.$aCubeID.'NME'.$aIndicatorName;
		$theInstance=null;
		if(isset(self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey]))
		{
			$theInstance=self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddIndicatorByNameAndCubeID($aIndicatorName,$aCubeID,&$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID.'NME'.$aIndicatorName;
		if(!isset(self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey]))
		{
			self::$arrAllIndicatorsByNameAndCubeID[$strInstanceKey] =& $aObject;
		}
		//Optimizado para agregar el indicador además a la colección por clave
		$strInstanceKey='IND'.$aObject->cla_indicador;
		if(!isset(self::$arrAllIndicators[$strInstanceKey]))
		{
			self::$arrAllIndicators[$strInstanceKey] =& $aObject;
		}
	}
	
	/* Optimiza los queries para verificar si la tabla contiene un campo especifico (especialmente usado para verificar si la
	tabla de hechos contiene o no un campo fecha)
	*/
	static function &GetExistsFieldOnTable($aTableName,$aFieldName)
	{
		$strInstanceKey='TBL'.$aTableName.'FLD'.$aFieldName;
		$theInstance=null;
		if(isset(self::$arrAllExistsTableField[$strInstanceKey]))
		{
			$theInstance=self::$arrAllExistsTableField[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddExistsFieldOnTable($aTableName,$aFieldName,$aExistsStatus)
	{
		$strInstanceKey='TBL'.$aTableName.'FLD'.$aFieldName;
		if(!isset(self::$arrAllExistsTableField[$strInstanceKey]))
		{
			self::$arrAllExistsTableField[$strInstanceKey] = $aExistsStatus;
		}
	}

	/* Optimiza la carga de los cubos por indicador
		Esta función solo puede almacenar un elemento, que es el arreglo completo de cubos por indicador, así que no requiere
	parámetros para ser invocada
	*/
	static function &GetCubeIDsByIndicatorIDs()
	{
		$strInstanceKey='ALL';
		$theInstance=null;
		if(isset(self::$arrAllCubeIDsByIndicatorIDs[$strInstanceKey]))
		{
			$theInstance=self::$arrAllCubeIDsByIndicatorIDs[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddCubeIDsByIndicatorIDs(&$aObject)
	{
		$strInstanceKey='ALL';
		if(!isset(self::$arrAllCubeIDsByIndicatorIDs[$strInstanceKey]))
		{
			self::$arrAllCubeIDsByIndicatorIDs[$strInstanceKey] =& $aObject;
		}
	}

	/* Optimiza la carga de los IDs de dimensiones compartidos entre dos cubos (join por CLA_DESCRIP)
		El parámetro $dimensionIDs no es un arreglo sino una lista separada ',' con los IDs a comparar
	*/
	static function &GetSharedDimensionIDsBetweenCubes($dimensionIDs,$aSourceCubeID,$aTargetCubeID)
	{
		$strInstanceKey='SRC'.$aSourceCubeID.'TRG'.$aTargetCubeID.'DIM'.$dimensionIDs;
		$theInstance=null;
		if(isset(self::$arrAllDimensionIDsBetweenCubes[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionIDsBetweenCubes[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddSharedDimensionIDsBetweenCubes($dimensionIDs,$aSourceCubeID,$aTargetCubeID,&$aObject)
	{
		$strInstanceKey='ALL';
		if(!isset(self::$arrAllDimensionIDsBetweenCubes[$strInstanceKey]))
		{
			self::$arrAllDimensionIDsBetweenCubes[$strInstanceKey] =& $aObject;
		}
	}

	/* Optimiza la carga de las dimensiones de acuerdo a la serie de parámetros que son especificados
		Esta función no debe usarse a la ligera, dada a la gran cantidad de parámetros, si se especifica uno diferente se
	generará una colección distinta y por tanto un elemento diferente del arreglo. No se debe combinar con llamados que si
	recuperan valores de las dimensiones, ya que las instancias de BITAMDimension regresadas no dependen de ello (lo mismo
	aplica para la carga individual de Dimensiones) y podría regresar sin valores aunque se pidan si anteriormente se había
	cargado la misma colección sin valores (el primero en ejecutarse es quien se queda en el Arreglo)
	*/
	static function &GetDimensionCollections($aDimensionIDs,$aCubeID,$in,$includePeriods,$bExcludeDuplicated)
	{
		$strDimensionIDs = '';
		if (!is_null($aDimensionIDs))
		{
			$strDimensionIDs = implode(',',$aDimensionIDs);
		}
		
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$strDimensionIDs.'IN'.$in.'APER'.$includePeriods.'DUP'.$bExcludeDuplicated;
		$theInstance=null;
		if(isset(self::$arrAllDimensionCollections[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimensionCollections[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimensionCollections($aDimensionIDs,$aCubeID,$in,$includePeriods,$bExcludeDuplicated,&$aObject)
	{
		$strDimensionIDs = '';
		if (!is_null($aDimensionIDs))
		{
			$strDimensionIDs = implode(',',$aDimensionIDs);
		}
		
		$strInstanceKey='CBO'.$aCubeID.'DIM'.$strDimensionIDs.'IN'.$in.'APER'.$includePeriods.'DUP'.$bExcludeDuplicated;
		if(!isset(self::$arrAllDimensionCollections[$strInstanceKey]))
		{
			self::$arrAllDimensionCollections[$strInstanceKey] =& $aObject;
		}
	}

	/* Optimiza la carga de las dimensiones dimensiones tiempo basandose en el cubo del cual se solicitan
	*/
	static function &GetPeriodDimensionCollections($aCubeID)
	{
		$strInstanceKey='CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllPeriodDimensionCollections[$strInstanceKey]))
		{
			$theInstance=self::$arrAllPeriodDimensionCollections[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddPeriodDimensionCollections($aCubeID,&$aObject)
	{
		$strInstanceKey='CBO'.$aCubeID;
		if(!isset(self::$arrAllPeriodDimensionCollections[$strInstanceKey]))
		{
			self::$arrAllPeriodDimensionCollections[$strInstanceKey] =& $aObject;
		}
	}

	/* Optimiza la carga de la estructura de las tablas para evitar que se consulte el esquema de la base de datos
		Dependiendo del parámetro $bDataConnection se indica si la tabla especificada está en la fuente (true) o en el repositorio (false)
	*/
	static function &GetTableSchema($aTableName,$bDataConnection)
	{
		$strInstanceKey='TBL'.$aTableName.'CNN'.$bDataConnection;
		$theInstance=null;
		if(isset(self::$arrAllMetaColumns[$strInstanceKey]))
		{
			$theInstance=self::$arrAllMetaColumns[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddTableSchema($aTableName,$bDataConnection,&$aObject)
	{
		$strInstanceKey='TBL'.$aTableName.'CNN'.$bDataConnection;
		if(!isset(self::$arrAllMetaColumns[$strInstanceKey]))
		{
			self::$arrAllMetaColumns[$strInstanceKey] =& $aObject;
		}
	}

	static function &GetDimIDFromDimName($aCubeID, $dimName)
	{
		$strInstanceKey='CBO'.$aCubeID.'DimName'.strtoupper($dimName);
		$theInstance=null;
		if(isset(self::$arrDimIDs[$strInstanceKey]))
		{
			$theInstance=self::$arrDimIDs[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDimIDFromDimName($aCubeID, $dimName, $dimID)
	{
		$strInstanceKey='CBO'.$aCubeID.'DimName'.strtoupper($dimName);
		if(!isset(self::$arrDimIDs[$strInstanceKey]))
		{
			self::$arrDimIDs[$strInstanceKey] =&$dimID;
		}
	}
	
	static function &GetDateKey($tableName, $fieldWhere, $fieldKey, $date)
	{
		$strInstanceKey='PERIODTABLE'.$tableName.'FIELDWHERE'.$fieldWhere.'FIELDKEY'.$fieldKey.'DATE'.$date;
		$theInstance=null;
		if(isset(self::$arrDateKeys[$strInstanceKey]))
		{
			$theInstance=self::$arrDateKeys[$strInstanceKey];
		}
		return $theInstance;
	}
	
	static function AddDateKey($tableName, $fieldWhere, $fieldKey, $date, $aObject)
	{
		$strInstanceKey='PERIODTABLE'.$tableName.'FIELDWHERE'.$fieldWhere.'FIELDKEY'.$fieldKey.'DATE'.$date;;
		
		if(!isset(self::$arrDateKeys[$strInstanceKey]))
		{
			self::$arrDateKeys[$strInstanceKey] =& $aObject;
		}
	}
	
	//Obtiene el Id del cubo por medio del nombre y tipo del cubo
	static function &GetUseAggregationByCubeID($aCubeID)
	{
		$strInstanceKey='CBO'.$aCubeID;
		$theInstance=null;
		if(isset(self::$arrAllUseAggregationsByCubeID[$strInstanceKey]))
		{
			$theInstance=self::$arrAllUseAggregationsByCubeID[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega el Id del cubo por medio del nombre y tipo del cubo
	static function AddUseAggregationByCubeID($aCubeID, $useAggregations)
	{
		$strInstanceKey='CBO'.$aCubeID;
		if(!isset(self::$arrAllUseAggregationsByCubeID[$strInstanceKey]))
		{
			self::$arrAllUseAggregationsByCubeID[$strInstanceKey] =& $useAggregations;
		}
	}

	//Obtener la coleccion de sp's del proyecto
	static function &GetGenStoredProcedureCollection()
	{
		$strInstanceKey='GenSPCol';
		$theInstance=null;
		if(isset(self::$arrAllGenSPCollection[$strInstanceKey]))
		{
			$theInstance=self::$arrAllGenSPCollection[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agregar la colección de sp's del proyecto
	static function AddGenStoredProcedureCollection(&$aObject)
	{
		$strInstanceKey='GenSPCol';
		if(!isset(self::$arrAllGenSPCollection[$strInstanceKey]))
		{
			self::$arrAllGenSPCollection[$strInstanceKey] =& $aObject;
		}
	}
	
	//Obtiene la fecha inicial del periodo
	static function &GetStartDateByPeriodID($aBudgetPeriodID)
	{
		$strInstanceKey='BDGPRD'.$aBudgetPeriodID;
		$theInstance=null;
		if(isset(self::$arrStartDatesPeriod[$strInstanceKey]))
		{
			$theInstance=self::$arrStartDatesPeriod[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega la fecha inicial del periodo
	static function AddStartDateByPeriodID($aBudgetPeriodID, $dateID)
	{
		$strInstanceKey='BDGPRD'.$aBudgetPeriodID;
		if(!isset(self::$arrStartDatesPeriod[$strInstanceKey]))
		{
			self::$arrStartDatesPeriod[$strInstanceKey] =& $dateID;
		}
	}
	
	//Obtiene la fecha final del periodo
	static function &GetEndDateByPeriodID($aBudgetPeriodID)
	{
		$strInstanceKey='BDGPRD'.$aBudgetPeriodID;
		$theInstance=null;
		if(isset(self::$arrEndDatesPeriod[$strInstanceKey]))
		{
			$theInstance=self::$arrEndDatesPeriod[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega la fecha final del periodo
	static function AddEndDateByPeriodID($aBudgetPeriodID, $dateID)
	{
		$strInstanceKey='BDGPRD'.$aBudgetPeriodID;
		if(!isset(self::$arrEndDatesPeriod[$strInstanceKey]))
		{
			self::$arrEndDatesPeriod[$strInstanceKey] =& $dateID;
		}
	}
	
	//Obtiene el dimJoinValue para el keyValue solicitado
	static function &GetDimJoinValueByKeyValue($cubeid, $nom_tabla, $nom_fisicok_bd, $tipo_datok, $keyValue)
	{
		$strInstanceKey="CBO".$cubeid."TBL".$nom_tabla."FKBD".$nom_fisicok_bd."TIPDK".$tipo_datok."KEYVAL".$keyValue;
		$theInstance=null;
		if(isset(self::$arrAllDimJoinValueByKeyValue[$strInstanceKey]))
		{
			$theInstance=self::$arrAllDimJoinValueByKeyValue[$strInstanceKey];
		}
		return $theInstance;
	}
	
	//Agrega el dimJoinValue para el keyValue procesado
	static function AddDimJoinValueByKeyValue($cubeid, $nom_tabla, $nom_fisicok_bd, $tipo_datok, $keyValue, $dimJoinValue)
	{
		$strInstanceKey="CBO".$cubeid."TBL".$nom_tabla."FKBD".$nom_fisicok_bd."TIPDK".$tipo_datok."KEYVAL".$keyValue;
		if(!isset(self::$arrAllDimJoinValueByKeyValue[$strInstanceKey]))
		{
			self::$arrAllDimJoinValueByKeyValue[$strInstanceKey] =& $dimJoinValue;
		}
	}
}
?>