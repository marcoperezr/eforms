VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ModSPSSCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
                                                                                                                           

'Public Declare Function spssSetInterfaceEncoding Lib "spssio32.dll" Alias "spssSetInterfaceEncoding@4" _
'                                (ByVal iEncoding As Integer) As Integer
'Use this function to change the interface encoding. If the call is successful, all text communicated to or
'from the I/O Module in subsequent calls will be in the specified mode. Also, all text in files written will
'be in the specified mode. There can be no open files when this call is made.

Public Function myspssSetInterfaceEncoding(ByVal iEncoding As Long) As Long
    Dim err As Long
    err = spssSetInterfaceEncoding(iEncoding)
    myspssSetInterfaceEncoding = err
End Function


Public Function myspssOpenRead(ByVal fileName As String, ByRef handle As Long) As Long
    Dim err As Long
    err = spssOpenRead(fileName, handle)
    myspssOpenRead = err
End Function

Public Function myspssGetNumberofVariables(ByVal handle As Long, ByRef numVars As Long) As Long
    Dim err As Long
    err = spssGetNumberofVariables(handle, numVars)
    myspssGetNumberofVariables = err
End Function

Public Function myspssGetVarInfo(ByVal handle As Long, ByVal iVar As Long, ByVal varName As String, ByRef varType As Long) As Long
    Dim err As Long
    err = spssGetVarInfo(handle, iVar, varName, varType)
    myspssGetVarInfo = err
End Function

Public Function myspssCloseRead(ByVal handle As Long) As Long
    Dim err As Long
    err = spssCloseRead(handle)
    myspssCloseRead = err
End Function


'Public Declare Function spssOpenWrite Lib "spssio32.dll" Alias "spssOpenWrite@8" _
'                                (ByVal fileName As String, ByRef handle As Long) As Long

'This function opens a file in preparation for creating a new IBM SPSS Statistics data file and returns a
'handle that should be used for subsequent operations on the file.
Public Function myspssOpenWrite(ByVal fileName As String, ByRef handle As Long) As Long
    Dim err As Long
    err = spssOpenWrite(fileName, handle)
    myspssOpenWrite = err
End Function

'Public Declare Function spssCloseWrite Lib "spssio32.dll" Alias "spssCloseWrite@4" _
                                (ByVal handle As Long) As Long

'This function closes the data file associated with handle, which must have been opened for writing using
'spssOpenWrite. The file handle handle becomes invalid and no further operations can be performed using
'it.
Public Function myspssCloseWrite(ByVal handle As Long) As Long
    Dim err As Long
    err = spssCloseWrite(handle)
    myspssCloseWrite = err
End Function


'Public Declare Function spssSetVarName Lib "spssio32.dll" Alias "spssSetVarName@12" _
                                (ByVal handle As Long, ByVal varName As String, ByVal varType As Long) As Long
                                
'This function creates a new variable named varName, which will be either numeric or string based on
'varLength. If the latter is zero, a numeric variable with a default format of F8.2 will be created; if it is
'greater than 0 and less than or equal to 32767, a string variable with length varLength will be created; any
'other value will be rejected as invalid. For better readability, the macros SPSS_NUMERIC and SPSS_STRING(
'length ) may be used as values for varLength.
Public Function myspssSetVarName(ByVal handle As Long, ByVal varName As String, ByVal varType As Long) As Long
    Dim err As Long
    err = spssSetVarName(handle, varName, varType)
    myspssSetVarName = err
End Function

'Public Declare Function spssCommitHeader Lib "spssio32.dll" Alias "spssCommitHeader@4" _
                                (ByVal handle As Long) As Long
                                
'This function writes the data dictionary to the data file associated with handle. Before any case data can
'be written, the dictionary must be committed; once the dictionary has been committed, no further
'changes can be made to it.
Public Function myspssCommitHeader(ByVal handle As Long) As Long
    Dim err As Long
    err = spssCommitHeader(handle)
    myspssCommitHeader = err
End Function

'Public Declare Function spssGetVarHandle Lib "spssio32.dll" Alias "spssGetVarHandle@12" _
                                (ByVal handle As Long, ByVal varName As String, ByRef varHandle As Double) As Long
                                
'This function returns a handle for a variable, which can then be used to read or write (depending on how
'the file was opened) values of the variable. If handle is associated with an output file, the dictionary must
'be written with spssCommitHeader before variable handles can be obtained via spssGetVarHandle.
Public Function myspssGetVarHandle(ByVal handle As Long, ByVal varName As String) As String
    Dim err As Long
    Dim varHandle As Double
    
    err = spssGetVarHandle(handle, varName, varHandle)
    myspssGetVarHandle = CStr(err) & "|" & CStr(varHandle)
End Function

'Public Declare Function spssSetValueChar Lib "spssio32.dll" Alias "spssSetValueChar@16" _
                                (ByVal handle As Long, ByVal varHandle As Double, ByVal value As String) As Long

'This function sets the value of a string variable for the current case. The current case is not written out to
'the data file until spssCommitCaseRecord is called.
'handle. Handle to the data file
Public Function myspssSetValueChar(ByVal handle As Long, ByVal varHandle As String, ByVal value As String) As Long
    Dim err As Long
    Dim myvarHandle As Double
    
    myvarHandle = CDbl(varHandle)
    err = spssSetValueChar(handle, myvarHandle, value)
    myspssSetValueChar = err
End Function

'Public Declare Function spssSetValueNumeric Lib "spssio32.dll" Alias "spssSetValueNumeric@20" _
                                (ByVal handle As Long, ByVal varHandle As Double, ByVal value As Double) As Long
                                
'This function sets the value of a numeric variable for the current case. The current case is not written out
'to the data file until spssCommitCaseRecord is called.
'handle. Handle to the data file
Public Function myspssSetValueNumeric(ByVal handle As Long, ByVal varHandle As String, ByVal value As String) As Long
    Dim err As Long
    Dim myvarHandle As Double
    Dim myvalue As Double
    
    myvarHandle = CDbl(varHandle)
    myvalue = CDbl(value)
    err = spssSetValueNumeric(handle, myvarHandle, myvalue)
    myspssSetValueNumeric = err
End Function


'Public Declare Function spssCommitCaseRecord Lib "spssio32.dll" Alias "spssCommitCaseRecord@4" _
                                (ByVal handle As Long) As Long
                                
'This function writes a case to the data file specified by the handle. It must be called after setting the
'values of variables through spssSetValueNumeric and spssSetValueChar. Any variables left unset will get
'the system-missing value if they are numeric and all blanks if they are strings. Unless
'spssCommitCaseRecord is called, the case will not be written out.
Public Function myspssCommitCaseRecord(ByVal handle As Long) As Long
    Dim err As Long
    err = spssCommitCaseRecord(handle)
    myspssCommitCaseRecord = err
End Function

'Public Declare Function spssSetVarMeasureLevel Lib "spssio32.dll" Alias "spssSetVarMeasureLevel@12" _
                                (ByVal handle As Long, ByVal varName As String, ByVal measureLevel As Long) As Long
                                
'This function sets the value of the measurement level attribute of a variable
Public Function myspssSetVarMeasureLevel(ByVal handle As Long, ByVal varName As String, ByVal measureLevel As Long) As Long
    Dim err As Long
    err = spssSetVarMeasureLevel(handle, varName, measureLevel)
    myspssSetVarMeasureLevel = err
End Function

'Public Declare Function spssSetVarLabel Lib "spssio32.dll" Alias "spssSetVarLabel@12" _
                                (ByVal handle As Long, ByVal varName As String, ByVal varLabel As String) As Long

'This function sets the label of a variable.
Public Function myspssSetVarLabel(ByVal handle As Long, ByVal varName As String, ByVal varLabel As String) As Long
    Dim err As Long
    err = spssSetVarLabel(handle, varName, varLabel)
    myspssSetVarLabel = err
End Function



'Public Declare Function spssSetVarCValueLabel Lib "spssio32.dll" Alias "spssSetVarCValueLabel@16" _
                                (ByVal handle As Long, ByVal varName As String, ByVal value As String, ByVal label As String) As Long
'This function changes or adds a value label for the specified value of a short string variable. The label
'should be a null-terminated string not exceeding 60 characters in length.
Public Function myspssSetVarCValueLabel(ByVal handle As Long, ByVal varName As String, ByVal value As String, ByVal label As String) As Long
    Dim err As Long
    err = spssSetVarCValueLabel(handle, varName, value, label)
    myspssSetVarCValueLabel = err
End Function

'Public Declare Function spssSetVarCValueLabels Lib "spssio32.dll" Alias "spssSetVarCValueLabels@20" _
                               (ByVal handle As Integer, ByRef varNames() As String, ByVal numofVars As Integer, ByRef values() As String, ByRef labels() As String, ByVal numofLabels As Integer) As Integer
'This function defines a set of value labels for one or more short string variables. Value labels already
'defined for any of the given variable(s), if any, are discarded (if the labels are shared with other variables,
'they remain associated).

Public Function myspssSetVarCValueLabels(ByVal handle As Long, ByRef varNames() As String, ByVal numofVars As Long, ByRef values() As String, ByRef labels() As String, ByVal numofLabels As Long) As Long
    Dim err As Long
    err = spssSetVarCValueLabels(handle, varNames, numofVars, values, labels, numofLabels)
    myspssSetVarCValueLabels = err
End Function


'    Public Declare Function spssSetVarNValueLabel Lib "spssio32.dll" Alias "spssSetVarNValueLabel@20" _
                                (ByVal handle As Long, ByVal varName As String, ByVal value As Double, ByVal label As String) As Long
'This function changes or adds a value label for the specified value of a numeric variable. The label
'should be a null-terminated string not exceeding 60 characters in length.

Public Function myspssSetVarNValueLabel(ByVal handle As Long, ByVal varName As String, ByVal value As Double, ByVal label As String) As Long
    Dim err As Long
    err = spssSetVarNValueLabel(handle, varName, value, label)
    myspssSetVarNValueLabel = err
End Function


'    Public Declare Function spssSetMultRespDefs Lib "spssio32.dll" Alias "spssSetMultRespDefs@8" _
                                (ByVal handle As Long, ByVal mrespDefs As String) As Long
    
'This function is used to write multiple response definitions to the file. The definitions are stored as a
'null-terminated code page or UTF-8 string based on whether the spssGetInterfaceEncoding() type is
'SPSS_ENCODING_CODEPAGE or SPSS_ENCODING_UTF8.

Public Function myspssSetMultRespDefs(ByVal handle As Long, ByVal mrespDefs As String) As Long
    Dim err As Long
    err = spssSetMultRespDefs(handle, mrespDefs)
    myspssSetMultRespDefs = err
End Function


'CREADA SOLO PARA REALIZAR UNA PRUEBA
'Public Function myspssSetMultRespDefsPHP(ByVal handle As Long, ByVal mrespDefs As String) As Long
'    Dim err As Long
'    Dim i As Long
'    Dim strconjuntos As String
'    Dim strMRespDefs As String
'    Dim pos As Long
'    Dim longitud As Integer
'    Dim subcad As String
'
'    strconjuntos = Trim$(mrespDefs)
'
'    Do While (Len(strconjuntos) > 0)
'        pos = InStr(1, strconjuntos, "|")
'        longitud = (pos - 1)
'        subcad = Left$(strconjuntos, longitud)
'
'        If Len(Right$(strconjuntos, Len(strconjuntos) - (Len(subcad) + 1))) = 0 Then
'            strMRespDefs = strMRespDefs & subcad
'        Else
'            strMRespDefs = strMRespDefs & subcad & " \n" & vbCrLf
'        End If
'        strconjuntos = Right$(strconjuntos, Len(strconjuntos) - (Len(subcad) + 1))
'    Loop
'
'    err = spssSetMultRespDefs(handle, strMRespDefs)
'    myspssSetMultRespDefsPHP = err
'End Function

'Public Declare Function spssSetVarPrintFormat Lib "sp  ssio32.dll" Alias "spssSetVarPrintFormat@20" _
                                (ByVal handle As Long, ByVal varName As String, ByVal printType As Long, ByVal printDec As Long, ByVal printWidth As Long) As Long

'This function sets the print format of a variable.
Public Function myspssSetVarPrintFormat(ByVal handle As Long, ByVal varName As String, ByVal printType As Long, ByVal printDec As Long, ByVal printWidth As Long) As Long
    Dim err As Long
    err = spssSetVarPrintFormat(handle, varName, printType, printDec, printWidth)
    myspssSetVarPrintFormat = err
End Function

'Public Declare Function spssSetVarWriteFormat Lib "spssio32.dll" Alias "spssSetVarWriteFormat@20" _
                                (ByVal handle As Long, ByVal varName As String, ByVal writeType As Long, ByVal writeDec As Long, ByVal writeWidth As Long) As Long
'This function sets the write format of a variable.
Public Function myspssSetVarWriteFormat(ByVal handle As Long, ByVal varName As String, ByVal writeType As Long, ByVal writeDec As Long, ByVal writeWidth As Long) As Long
    Dim err As Long
    err = spssSetVarWriteFormat(handle, varName, writeType, writeDec, writeWidth)
    myspssSetVarWriteFormat = err
End Function

