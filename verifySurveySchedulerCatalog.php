<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el AttributeID
$SurveyID = "";
if (array_key_exists("SurveyID", $_POST))
{
	$SurveyID = $_POST["SurveyID"];
}

//Obtener el CatalogID
$CatalogID = "";
if (array_key_exists("CatalogID", $_POST))
{
	$CatalogID = $_POST["CatalogID"];
}

require_once("survey.inc.php");
require_once("catalog.inc.php");

//obtenemos el campo donde estan los emails
$sql = "SELECT COUNT(*) CountAttributes FROM SI_SV_CatalogMember WHERE CatalogID = ".$CatalogID;
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_CatalogMember ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
$countattributes = 0;
if (!$aRS->EOF) {
    $countattributes = (int) $aRS->fields["countattributes"];
}

//verificamos si la encuesta contiene una pregunta con ese catalogo
$sql = "SELECT QuestionID FROM SI_SV_Question WHERE SurveyID = ".$SurveyID." 
		AND QTypeID = ".qtpSingle." AND UseCatalog = 1 AND CatalogID = ".$CatalogID;
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS === false)
{
    die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Question ".translate("table").": ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
}
$hasQuestionCatalog = false;

if (!$aRS->EOF) 
{
    $hasQuestionCatalog = true;
}

//@JAPR 2014-02-14: Removida la configuración del catálogo de la encuesta
//Se eliminó la necesidad de verificar que se trate del mismo catálogo de la encuesta, ya que ahora puede haber múltiples catálogos involucrados
if($countattributes > 0 && $hasQuestionCatalog==true) {
   $response = ""; 
} else {
   $response = "false";
}
//@JAPR

header('Content-Type: text/plain; charset=utf-8');
echo $response;
?>