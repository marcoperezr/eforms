<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("surveyFieldMap.inc.php");
require_once('eBavelIntegration.inc.php');

class BITAMQuestionActionMap extends BITAMObject
{
	public $FieldMapID;						//Identificador único de la tabla
	public $SurveyID;						//ID de la encuesta para la que se está configurando la columna
	public $QuestionID;						//ID de la pregunta para la que se está configurando la columna
	public $SurveyFieldID;					//ID que identifica el tipo de columna de la que se trata (sólo los IDs negativos son los mismos ID entre todas las encuestas para el mismo tipo de columna, el resto de los IDs representan los QuestionIDs específicos de cada encuesta - no usados originalmente, pero por si alguna vez se solicitaban -. 0 (default) == No es un mapeo de columna)
	public $SurveyFieldText;				//Etiqueta para desplegar durante la configuración con el tipo de columna 
	public $eBavelFieldID;					//ID de FieldForm de eBavel al que se mapea esta columna
	public $eBavelAppID;					//ID de la aplicación de eBavel de la que se extraerán los campos para usar en el mapeo
	public $eBavelFormID;					//ID de la forma de eBavel de la que se extraerán los campos para usar en el mapeo
	public $ArrayVariables;					//Contenido de la tabla cuando se genera la instancia, utilizado para crear dinámicamente las propiedades cuando se carga como instancia para la página de configuración
	public $ArrayVariablesKeys;
	public $ArrayVariableValues;
	public $ArrayVariablesExist;
	public $ArrayVariablesLabels;
	public $ArrayVariablesTypes;
	static $ArrayQuestionData = array();	//Datos utilizados de las preguntas (sólo son el Nombre, Etiqueta y tipo). Se usa así para no tener que cargar la instancia completa de las preguntas
	static $SurveysLoaded = array();		//Lista de las encuestas que ya han cargado la información de las preguntas
	
  	function __construct($aRepository, $aSurveyID = 0)
	{
		BITAMObject::__construct($aRepository);
		$this->FieldMapID = -1;
		$this->SurveyID = -1;
		$this->QuestionID = -1;
		$this->SurveyFieldID = 0;
		$this->eBavelFieldID = -1;
		$this->eBavelAppID = -1;
		$this->eBavelFormID = -1;
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		$this->ArrayVariablesLabels = array();
		$this->ArrayVariablesTypes = array();
		if ($aSurveyID > 0) {
			self::GetQuestionData($aRepository, $aSurveyID);
		}
	}
	
	//Obtiene los nombres físicos y etiquetas de las preguntas de la encuesta especificada y llena los arrays estáticos con esa información
	static function GetQuestionData($aRepository, $aSurveyID) {
		if (isset(self::$SurveysLoaded[$aSurveyID])) {
			return;
		}
		
		$questionCollection = @BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
		if (!is_null($questionCollection)) {
			foreach ($questionCollection->Collection as $questionKey => $aQuestion) {
				$strQuestionField = 'qfield'.$aQuestion->QuestionID;
				self::$ArrayQuestionData[$strQuestionField] = array();
				self::$ArrayQuestionData[$strQuestionField]['Label'] = $aQuestion->QuestionText;
				self::$ArrayQuestionData[$strQuestionField]['Name'] = $strQuestionField;	//$aQuestion->AttributeName;
				self::$ArrayQuestionData[$strQuestionField]['Type'] = $aQuestion->QTypeID;
				//@JAPR 2013-07-19: Agregada la instancia porque se necesitaba mas datos, así que para no terminar duplicando el array se agregará
				//simplemente la instancia a la colección
				self::$ArrayQuestionData[$strQuestionField]['Obj'] = $aQuestion;
			}
		}
		
		self::$SurveysLoaded[$aSurveyID] = 1;
	}
	
	static function NewInstance($aRepository, $aSurveyID = 0)
	{
		return new BITAMQuestionActionMap($aRepository, $aSurveyID);
	}
	
	static function NewInstanceWithID($aRepository, $aFieldMapID)
	{
		$anInstance = null;
		
		if (((int) $aFieldMapID) < 0)
		{
			return $anInstance;
		}
		
		$sql = "SELECT A.FieldMapID, A.SurveyID, A.QuestionID, A.SurveyFieldID, A.eBavelFieldID 
			FROM SI_SV_SurveyQuestionActionFieldsMap A 
			WHERE A.FieldMapID = ".((int) $aFieldMapID);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = BITAMQuestionActionMap::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$intSurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance = BITAMQuestionActionMap::NewInstance($aRepository, $intSurveyID);
		$anInstance->FieldMapID = (int) @$aRS->fields["fieldmapid"];
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->QuestionID = (int) @$aRS->fields["questionid"];
		$anInstance->SurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
		$anInstance->eBavelFieldID = (int) @$aRS->fields["ebavelfieldid"];
		$anInstance->SurveyFieldText = BITAMQuestionActionMap::GetSurveyColumnText($this->SurveyFieldID);
		return $anInstance;
	}
	
	//Regresa la etiqueta correspondiente a la columna indicada
	static function GetSurveyColumnText($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		if ($aSurveyFieldID < 0) {
			//Se trata de un campo default, internamente se graba como negativo, pero al ser identificado se tiene que hacer como positivo
			//Si es un campo específico de acciones, se obtiene la descripción de esta clase, de lo contrario se debe tratar de un campo default
			//de una captura así que se obtiene de la clase original
			$aSurveyFieldID *= -1;
			switch ($aSurveyFieldID) {
				case safidDescription:
					$strSurveyFieldText = translate('Action description');
					break;
				case safidResponsible:
					$strSurveyFieldText = translate('Action responsible');
					break;
				case safidCategory:
					$strSurveyFieldText = translate('Action category');
					break;
				case safidDueDate:
					$strSurveyFieldText = translate('Action due date');
					break;
				case safidStatus:
					$strSurveyFieldText = translate('Action status');
					break;
				case safidSource:
					$strSurveyFieldText = translate('Action source');
					break;
				case safidSourceID:
					$strSurveyFieldText = translate('Action source id');
					break;
				case safidSourceRowKey:
					$strSurveyFieldText = translate('Action source entry id');
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case safidTitle:
					$strSurveyFieldText = translate('Action title');
					break;
				//@JAPR
				default:
					$strSurveyFieldText = BITAMSurveyFieldMap::GetSurveyColumnText($aSurveyFieldID);
					break;
			}
		}
		else {
			//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
			$strQuestionField = 'qfield'.$aSurveyFieldID;
			$strSurveyFieldText = @BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField]['Label'];
			if (is_null($strSurveyFieldText)) {
				$strSurveyFieldText = translate('Unknown');
			}
		}
		
		return $strSurveyFieldText;
	}
	
	//Regresa el nombre identificador de la columna para permitir la captura usando el Framework. Si regresa vacio entonces se trata de una columna
	//no soportada en esta versión
	static function GetSurveyColumnName($aSurveyFieldID) {
		$aSurveyFieldID = (int) $aSurveyFieldID;
		
		$strSurveyFieldText = '';
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		if ($aSurveyFieldID < 0) {
			//Se trata de un campo default, internamente se graba como negativo, pero al ser identificado se tiene que hacer como positivo
			//Si es un campo específico de acciones, se obtiene la descripción de esta clase, de lo contrario se debe tratar de un campo default
			//de una captura así que se obtiene de la clase original
			$aSurveyFieldID *= -1;
			switch ($aSurveyFieldID) {
				case safidDescription:
					$strSurveyFieldText = 'ActFldDescription';
					break;
				case safidResponsible:
					$strSurveyFieldText = 'ActFldResponsible';
					break;
				case safidCategory:
					$strSurveyFieldText = 'ActFldCategory';
					break;
				case safidDueDate:
					$strSurveyFieldText = 'ActFldDueDate';
					break;
				case safidStatus:
					$strSurveyFieldText = 'ActFldStatus';
					break;
				case safidSource:
					$strSurveyFieldText = 'ActFldSource';
					break;
				case safidSourceID:
					$strSurveyFieldText = 'ActFldSourceID';
					break;
				case safidSourceRowKey:
					$strSurveyFieldText = 'ActFldSourceEntryID';
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case safidTitle:
					$strSurveyFieldText = 'ActFldTitle';
					break;
				//@JAPR
				default:
					$strSurveyFieldText = BITAMSurveyFieldMap::GetSurveyColumnName($aSurveyFieldID);
					break;
			}
		}
		else {
			//El resto de los textos se obtendrían directamente de las etiquetas de las preguntas, ya que los FieldIDs > 0 son siempre preguntas
			$strQuestionField = 'qfield'.$aSurveyFieldID;
			$strSurveyFieldText = @BITAMQuestionActionMap::$ArrayQuestionData[$strQuestionField]['Name'];
			if (is_null($strSurveyFieldText)) {
				$strSurveyFieldText = '';
			}
		}
		
		return $strSurveyFieldText;
	}
	
	//Regresa id de la columna a partir del nombre identificador. Los valores se regresarán negativos para campos default o positivos para preguntas
	static function GetSurveyColumnID($aSurveyFieldName) {
		$aSurveyFieldName = strtolower(trim((string) $aSurveyFieldName));
		
		$intSurveyFieldID = 0;
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//Primero verifica si el nombre corresponde a un campo de pregunta, si no es así entonces ya intenta con los campos default de acciones
		//y posteriormente de la encuesta
		if (stripos($aSurveyFieldName, 'qfield') !== false) {
			//Se trata de una pregunta
			$intSurveyFieldID = (int) str_ireplace('qfield', '', $aSurveyFieldName);
		}
		else {
			//Es un campo default ya sea de acciones o de la encuesta
			switch ($aSurveyFieldName) {
				case 'actflddescription':
					$intSurveyFieldID = safidDescription;
					break;
				case 'actfldresponsible':
					$intSurveyFieldID = safidResponsible;
					break;
				case 'actfldcategory':
					$intSurveyFieldID = safidCategory;
					break;
				case 'actfldduedate':
					$intSurveyFieldID = safidDueDate;
					break;
				case 'actfldstatus':
					$intSurveyFieldID = safidStatus;
					break;
				case 'actfldsource':
					$intSurveyFieldID = safidSource;
					break;
				case 'actfldsourceid':
					$intSurveyFieldID = safidSourceID;
					break;
				case 'actfldsourceentryid':
					$intSurveyFieldID = safidSourceRowKey;
					break;
				//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
				case 'actfldtitle':
					$intSurveyFieldID = safidTitle;
					break;
				//@JAPR
				default:
					$intSurveyFieldID = BITAMSurveyFieldMap::GetSurveyColumnID($aSurveyFieldName);
					break;
			}
		}
		
		return $intSurveyFieldID;
	}
	
	//Llena la instancia de campos de eBavel con los valores default
	function resetDefaultSettings()
	{
		$this->ArrayVariables = array();
		$this->ArrayVariablesKeys = array();
		$this->ArrayVariableValues = array();
		$this->ArrayVariablesExist = array();
		$this->ArrayVariablesLabels = array();
		$this->ArrayVariablesTypes = array();
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		
		//Primero obtiene la lista de columnas de eBavel de la forma especificada
		//Este array obtendrá indexado por el tagname, objetos con datos genéricos de cada campo de eBavel de la Aplicación/Forma seleccionada
		try {
			$arreBavelFields = GetEBavelCollectionByField(@GetEBavelFieldsForms($this->Repository, true, $this->eBavelAppID, array($this->eBavelFormID), null), '', 'key', false, 'tagname');
		} catch (Exception $e) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\nException reading eBavel action fields collection: ".$e->getMessage());
			}
			$arreBavelFields = array();
		}
		
		foreach ($arreBavelFields as $strTagName => $aFieldsColl) {
			//No se puede usar array_combine porque no se desea reorganizar los índices numéricos de los keys de cada campo en $aFieldsColl
			$intQTypeID = convertEBavelToEFormsFieldType($strTagName);
			
			//Si es un tipo de pregunta inválido continua con los siguientes campos
			//@JAPR 2013-05-02: Agregado el tipo de pregunta inválido, porque algunos tipos negativos son dummies para soporte de eBavel
			if ($intQTypeID == qtpInvalid) {
				continue;
			}
			
			foreach ($aFieldsColl as $intKey => $aField) {
				$inteBavelFieldID = (int) @$aField['id_fieldform'];
				if ($inteBavelFieldID <= 0) {
					continue;
				}
				
				$strFieldName = 'id_fieldform'.$inteBavelFieldID;
				$this->ArrayVariablesKeys[$inteBavelFieldID] = 0;
				$this->ArrayVariables[$inteBavelFieldID] = $strFieldName;
				$this->ArrayVariableValues[$inteBavelFieldID] = 0;
				$this->ArrayVariablesExist[$inteBavelFieldID] = false;
				$strFieldLabel = (string) @$aField['label'];
				if (trim($strFieldLabel) == '') {
					$strFieldLabel = $strFieldName;
				}
				$this->ArrayVariablesLabels[$inteBavelFieldID] = $strFieldLabel;
				$this->ArrayVariablesTypes[$inteBavelFieldID] = $intQTypeID;
			}
		}
	}
	
	//Carga la configuración completa para una opción de respuesta (esta es la instancia que se deberá usar para desplegar la ventana de configuración)
	static function NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionID, $aneBavelAppID = 0, $aneBavelFormID = 0)
	{
		require_once("survey.inc.php");
		require_once("question.inc.php");
		
		//@JAPR 2013-05-31: Agregado el proceso de migración a catálogos independientes
		global $gblShowErrorSurvey;
		
		$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $aSurveyID);
		$anInstanceQuestion = BITAMQuestion::NewInstanceWithID($aRepository, $aQuestionID);
		$intQuestionID = (int) @$anInstanceQuestion->QuestionID;
		$inteBavelAppID = $aneBavelAppID;
		if ($inteBavelAppID <= 0) {
			$inteBavelAppID = (int) @$surveyInstance->eBavelAppID;
		}
		$inteBavelFormID = $aneBavelFormID;
		if ($inteBavelFormID <= 0) {
			$inteBavelFormID = (int) @$anInstanceQuestion->eBavelActionFormID;
		}
		
		$anInstanceTemp = null;
		$sql = "SELECT FieldMapID, SurveyID, QuestionID, SurveyFieldID, eBavelFieldID 
			FROM SI_SV_SurveyQuestionActionFieldsMap 
			WHERE SurveyID = $aSurveyID AND QuestionID = $aQuestionID 
			Order By FieldMapID";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		
		$anInstanceTemp = BITAMQuestionActionMap::NewInstance($aRepository, $aSurveyID);
		$anInstanceTemp->SurveyID = $aSurveyID;
		$anInstanceTemp->QuestionID = $intQuestionID;
		$anInstanceTemp->eBavelAppID = $inteBavelAppID;
		$anInstanceTemp->eBavelFormID = $inteBavelFormID;
		$anInstanceTemp->resetDefaultSettings();
		
		$ArrayVariables = &$anInstanceTemp->ArrayVariables;
		$ArrayVariablesKeys = &$anInstanceTemp->ArrayVariablesKeys;
		$ArrayVariableValues = &$anInstanceTemp->ArrayVariableValues;
		$ArrayVariablesLabels = &$anInstanceTemp->ArrayVariablesLabels;
		$ArrayVariablesTypes = &$anInstanceTemp->ArrayVariablesTypes;
		$ArrayVariablesExist = array();
		
		while(!$aRS->EOF) {
			$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
			$intSurveyID = (int) @$aRS->fields["surveyid"];
			$intQuestionID = (int) @$aRS->fields["questionid"];
			$intSurveyFieldID = (int) @$aRS->fields["surveyfieldid"];
			$inteBavelFieldID = $aRS->fields["ebavelfieldid"];
			//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
			//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
			
			//Si el campo de la configuración aun existe, entonces actualiza los arrays, de lo contrario elimina la configuración de la tabla
			if (isset($ArrayVariables[$inteBavelFieldID])) {
				$ArrayVariablesKeys[$inteBavelFieldID] = $intFieldMapID;
				$ArrayVariableValues[$inteBavelFieldID] = $intSurveyFieldID;
				$ArrayVariablesExist[$inteBavelFieldID] = true;
			}
			else {
				$sql = "DELETE FROM SI_SV_SurveyQuestionActionFieldsMap 
					WHERE QuestionID = ".$aQuestionID." AND EBavelFieldID = $inteBavelFieldID";
				$aRepository->DataADOConnection->Execute($sql);
			}
			$aRS->MoveNext();
		}
		
		$strtemp = 			'if (!class_exists("BITAMQuestionActionMapExtended"))'."\n";
		$strtemp = $strtemp.'{'."\n";
		$strtemp = $strtemp.'	class BITAMQuestionActionMapExtended extends BITAMQuestionActionMap'."\n";
		$strtemp = $strtemp.'	{'."\n";
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		foreach($ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$intVariableValue = (int) @$ArrayVariableValues[$inteBavelFieldID];
			$strtemp = $strtemp.'		public $'.$strVariableName.' = '.$intVariableValue.';'."\n";
			
		}
		
		$strtemp = $strtemp.'	}'."\n";
		$strtemp = $strtemp.'}'."\n";
		//@JAPR 2012-05-18: Validación para evitar que invocar múltiples veces al SaveData en la misma sesión provoque que esta clase dinámica
		//sea redeclarada, de todas formas, las configuraciones dificilmente cambiarán durante la ejecución de una llamada de grabado como para 
		//que esto pudiera ser un problema
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		$strtemp = "";
		//@JAPR
		$strtemp = $strtemp.'$theobject = new BITAMQuestionActionMapExtended($aRepository);'."\r\n";
		$strtemp = addcslashes( $strtemp, '\\');
		eval($strtemp);
		
		$theobject->SurveyID = $aSurveyID;
		$theobject->QuestionID = $intQuestionID;
		$theobject->ArrayVariables = $ArrayVariables;
		$theobject->ArrayVariablesKeys = $ArrayVariablesKeys;
		$theobject->ArrayVariableValues = $ArrayVariableValues;
		$theobject->ArrayVariablesExist = $ArrayVariablesExist;
		$theobject->ArrayVariablesLabels = $ArrayVariablesLabels;
		$theobject->ArrayVariablesTypes = $ArrayVariablesTypes;
		$theobject->IsDialog = true;
		
		return($theobject);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$inteBavelAppID = (int) @$aHTTPRequest->GET["eBavelAppID"];
		$inteBavelFormID = (int) @$aHTTPRequest->GET["eBavelFormID"];
		$aSurveyID = (int) @$aHTTPRequest->POST["SurveyID"];
		$aQuestionID = (int) @$aHTTPRequest->POST["QuestionID"];
		if ($aSurveyID > 0 && $aQuestionID > 0)
		{
			$anInstance = BITAMQuestionActionMap::NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionID, $inteBavelAppID, $inteBavelFormID);
			$anInstance->updateFromArray($aHTTPRequest->POST);
			$aResult = $anInstance->save();
			
			if (array_key_exists("go", $_POST))
			{
				$go = $_POST["go"];
			}
			else
			{
				$go = "self";
			}
			
			if ($go == "parent")
			{
				$anInstance = $anInstance->get_Parent();
			}
			else
			{
				if ($go == "new")
				{
					$anInstance = BITAMQuestionActionMap::NewInstance($aRepository, $aSurveyID);
				}
			}
			
			$aHTTPRequest->RedirectTo = $anInstance;
			return null;
		}
		else {
			$aSurveyID = (int) @$aHTTPRequest->GET["SurveyID"];
			$aQuestionID = (int) @$aHTTPRequest->GET["QuestionID"];
		}
		
		$anInstance = BITAMQuestionActionMap::NewInstanceWithAnswerID($aRepository, $aSurveyID, $aQuestionID, $inteBavelAppID, $inteBavelFormID);
		if (!is_null($anInstance)) {
			$anInstance->eBavelAppID = $inteBavelAppID;
			$anInstance->eBavelFormID = $inteBavelFormID;
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			if (array_key_exists($strVariableName, $anArray))
			{
				$this->$strVariableName = $anArray[$strVariableName];
				$this->ArrayVariableValues[$inteBavelFieldID] = $anArray[$strVariableName];
			}
		}
		return $this;
	}
	
	function save()
	{
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$sql = "";
			$intVariableValue = (int) @$this->$strVariableName;
			if (isset($this->ArrayVariablesExist[$inteBavelFieldID]) && $this->ArrayVariablesExist[$inteBavelFieldID])
			{
				if ($intVariableValue != 0) {
					$sql = "UPDATE SI_SV_SurveyQuestionActionFieldsMap SET SurveyFieldID = ".$intVariableValue.
					" WHERE QuestionID = ".$this->QuestionID." AND EBavelFieldID = $inteBavelFieldID";
				}
				else {
					$sql = "DELETE FROM SI_SV_SurveyQuestionActionFieldsMap 
						WHERE QuestionID = ".$this->QuestionID." AND EBavelFieldID = $inteBavelFieldID";
				}
			}
			else 
			{
				$sql =  "SELECT ".
					$this->Repository->DataADOConnection->IfNull("MAX(FieldMapID)", "0")." + 1 AS FieldMapID".
					" FROM SI_SV_SurveyQuestionActionFieldsMap";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if (!$aRS || $aRS->EOF)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				$intFieldMapID = (int) @$aRS->fields["fieldmapid"];
				
				if ($intVariableValue != 0) {
					$sql = "INSERT INTO SI_SV_SurveyQuestionActionFieldsMap (FieldMapID, SurveyID, QuestionID, eBavelFieldID, SurveyFieldID) VALUES (".
						$intFieldMapID.", ".$this->SurveyID.", ".$this->QuestionID.", ".$inteBavelFieldID.", ".$intVariableValue.")";
				}
				else {
					$sql = "";
				}
			}
			
			if ($sql != '' && $this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyQuestionActionFieldsMap ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
	}
	
	function isNewObject()
	{
		return false;
	}
	
	function get_Title()
	{
		return translate("eBavel action field mapping");
	}
	
	function get_QueryString()
	{
		return "BITAM_PAGE=QuestionActionMap";
	}
	
	function get_Parent()
	{
		//return $this->Repository;
		return $this;
	}
	
	function get_PathArray()
	{
		//$parent = $this->get_Parent();
		//$pathArray = $parent->get_PathArray();
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'FieldMapID';
	}
	
	function get_FormFields($aUser)
	{
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
		global $garrActionFields;

		require_once("formfield.inc.php");
		require_once("question.inc.php");
		
		$myFields = array();
		$categoryCatalogField = null;
		
		//Agrega la encuesta ya que realmente la ventana graba en base a ella y no a un campo específico
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "SurveyID";
		$aField->Title = translate("Survey");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->SurveyID => 'Survey');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "QuestionID";
		$aField->Title = translate("Question");
		$aField->Type = "Object";
		$aField->VisualComponent = "Combobox";
		$aField->Options = array($this->QuestionID => 'Question');
		$aField->InTitle = true;
		$aField->IsVisible = false;
		$myFields[$aField->Name] = $aField;

		//Obtiene la lista de aplicaciones de eBavel creadas en el repositorio
		$arreBavelApps = array(0 => '('.translate('None').')');
		$arreBavelApps = GetEBavelCollectionByField(@GetEBavelApplications($this->Repository), 'applicationName', 'key', true);
		
		$aFieldApps = BITAMFormField::NewFormField();
		$aFieldApps->Name = "eBavelAppID";
		$aFieldApps->Title = translate("eBavel application");
		$aFieldApps->Type = "Object";
		$aFieldApps->VisualComponent = "Combobox";
		$aFieldApps->Options = $arreBavelApps;
		//@JAPR 2013-04-18: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
		$aFieldApps->InTitle = true;
		$aFieldApps->IsVisible = false;
		$myFields[$aFieldApps->Name] = $aFieldApps;
		
		//Obtiene la lista de formas de eBavel creadas en el repositorio
		$arreBavelForms = array('' => array('' => '('.translate('None').')'));
		$arreBavelForms = GetEBavelCollectionByField(@GetEBavelSections($this->Repository), 'sectionName', 'key', true, 'appId');
		
		$aFieldForms = BITAMFormField::NewFormField();
		$aFieldForms->Name = "eBavelFormID";
		$aFieldForms->Title = translate("eBavel form");
		$aFieldForms->Type = "Object";
		$aFieldForms->VisualComponent = "Combobox";
		$aFieldForms->Options = $arreBavelForms;
		$aFieldForms->InTitle = true;
		$aFieldForms->IsVisible = false;
		$myFields[$aFieldForms->Name] = $aFieldForms;
		$aFieldForms->Parent = $aFieldApps;
		$aFieldApps->Children[] = $aFieldForms;
		
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//En este caso se generan arrays por tipos de preguntas, incluyendo adicionalmente los datos fijos de la acción ajustados a ciertos tipos,
		//posteriormente se generarán los arrays de las preguntas/datos fijos de la encuesta que apliquen según el tipo de campo de eBavel
		$arrEmptyeBavelForms = array(0 => '('.translate('None').')');
        $questionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $this->SurveyID);
        if (is_null($questionCollection)) {
        	$questionCollection = new stdClass();
        	$questionCollection->Collection = array();
        }
		
        $arreFormsNumericQuestions = array();
        $arreFormsAlphaQuestions = array();
        $arreFormsTextQuestions = array();
        $arreFormsDateQuestions = array();
        $arreFormsTimeQuestions = array();
        $arreFormsSimpleChoiceQuestions = array();
        $arreFormsMultipleChoiceQuestions = array();
        $arreFormsPhotoQuestions = array();
        $arreFormsSignatureQuestions = array();
        $arreFormsDocumentQuestions = array();
        $arreFormsEMailQuestions = array();
        $arreFormsAllQuestions = array();
        $arreFormsLocationQuestions = array();
        
        //Agrega las columnas default
		//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
        //for ($intSurveyFieldID = sfidStartDate; $intSurveyFieldID <= safidSourceID; $intSurveyFieldID++) {
        foreach ($garrActionFields as $intSurveyFieldID) {
        	$intQTypeID = getDefaultEFormsColumnQTypeID($intSurveyFieldID);
        	if ($intQTypeID == qtpInvalid) {
        		continue;
        	}
        	
        	$intNegSurveyFieldID = $intSurveyFieldID * -1;
        	$strColumnName = BITAMQuestionActionMap::GetSurveyColumnText($intNegSurveyFieldID);
        	$blnValid = true;
        	switch ($intQTypeID) {
        		case qtpOpenNumeric:
        		case qtpCalc:
        			$arreFormsNumericQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpSingle:
			        $arreFormsSimpleChoiceQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpMulti:
			        $arreFormsMultipleChoiceQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpOpenDate:
        			$arreFormsDateQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpOpenString:
        			$arreFormsTextQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpOpenAlpha:
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		case qtpAction:
        			$arreFormsAlphaQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpPhoto:
        			$arreFormsPhotoQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
        		case qtpSignature:
					//@JAPRPendiente
        			//$arreFormsSignatureQuestions[$intNegSurveyFieldID] = $strColumnName;
        			$blnValid = false;
        			break;
        		case qtpAudio:
				case qtpVideo:
        		case qtpDocument:
					//@JAPRPendiente
        			//$arreFormsDocumentQuestions[$intNegSurveyFieldID] = $strColumnName;
        			$blnValid = false;
        			break;
        			
        		case qtpOpenTime:
        			$arreFormsTimeQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        		
				//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
        		case qtpLocation:
        			$arreFormsLocationQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpEMail:
        			$arreFormsEMailQuestions[$intNegSurveyFieldID] = $strColumnName;
        			break;
        			
        		case qtpShowValue:
        		case qtpMessage:
        		case qtpSkipSection:
        		case qtpSync:
        		case qtpCallList:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
        		case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
        		case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
        		default:
        			$blnValid = false;
        			break;
        	}
        	
        	if ($blnValid) {
        		$arreFormsAllQuestions[$intNegSurveyFieldID] = $strColumnName;
        	}
        }
        
        //Agrega las columnas de las preguntas
        foreach ($questionCollection->Collection as $objQuestion) {
        	$intQTypeID = $objQuestion->QTypeID;
        	$intQuestionID = $objQuestion->QuestionID;
        	$strQuestionNumber = $objQuestion->QuestionNumber.'- ';
        	$strQuestionText = $strQuestionNumber.$objQuestion->QuestionText;
        	$blnValid = true;
        	
        	switch ($intQTypeID) {
        		case qtpOpenNumeric:
        		case qtpCalc:
        			$arreFormsNumericQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpSingle:
			        $arreFormsSimpleChoiceQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpMulti:
			        $arreFormsMultipleChoiceQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpOpenDate:
        			//@JAPR 2013-07-15: Corregido un bug, se estaba mapeando hacia las columnas numéricas
        			$arreFormsDateQuestions[$intQuestionID] = $strQuestionText;
        			//@JAPR
        			break;
        			
        		case qtpOpenString:
        			$arreFormsTextQuestions[$intQuestionID] = $strQuestionText;
        			break;
        			
        		case qtpOpenAlpha:
        			$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;

        		//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;
        			
        		case qtpAction:
        			$arreFormsAlphaQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpPhoto:
        			$arreFormsPhotoQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
				//@JAPR 2014-01-29: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
        			$arreFormsLocationQuestions[$intQuestionID] = $strQuestionText;
        			break;
				//@JAPR
        			
        		case qtpSignature:
					//@JAPRPendiente
        			//$arreFormsSignatureQuestions[$intQuestionID] = $strQuestionText;
        			$blnValid = false;
        			break;
        		case qtpAudio:
				case qtpVideo:
        		case qtpDocument:
					//@JAPRPendiente
        			//$arreFormsDocumentQuestions[$intQuestionID] = $strQuestionText;
        			$blnValid = false;
        			break;
        			
        		case qtpOpenTime:
        			$arreFormsTimeQuestions[$intQuestionID] = $strQuestionText;
        			break;
        		
        		case qtpShowValue:
        		case qtpMessage:
        		case qtpSkipSection:
        		case qtpSync:
        		case qtpCallList:
				//@JAPR 2014-03-10: Agregado el tipo de pregunta Password
        		case qtpPassword:
				//@JAPR 2014-10-02: Agregadas las preguntas tipo sección (Inline)
        		case qtpSection:
				//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
				case qtpExit:
        		default:
        			$blnValid = false;
        			break;
        	}
        	
        	if ($blnValid) {
        		$arreFormsAllQuestions[$intQuestionID] = $strQuestionText;
        	}
        }
        
		//Genera todos los campos de cada columna a configurar, todos podrán configurarse hacia un campo alfanumérico de eBavel + el campo que
		//corresponde con el tipo específico de cada columna
		//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
		//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
		//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
		foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
		{
			$aField = BITAMFormField::NewFormField();
			$aField->Name = $strVariableName;
			//$aField->Title = BITAMQuestionActionMap::GetSurveyColumnText($intSurveyFieldID);
			$aField->Title = $this->ArrayVariablesLabels[$inteBavelFieldID];
			$aField->Type = "Object";
			$aField->VisualComponent = "Combobox";
			
			$intQTypeID = (int) @$this->ArrayVariablesTypes[$inteBavelFieldID];
			$arrMyeBavelFormsFields = array();
			//@JAPR 2013-07-03: Para esta clase se mapearan los campos de eBavel hacia preguntas y datos fijos de eForms, en lugar de mapear campos de
			//eForms hacia campos de eBavel como se hizo en el grabado de datos normal
			$blnValid = true;
			switch ($intQTypeID) {
				case qtpEMail:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsEMailQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpOpenNumeric:
				case qtpCalc:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsNumericQuestions;
					break;
					
				case qtpSingle:
				case qtpCallList:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsSimpleChoiceQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpMulti:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsMultipleChoiceQuestions;
					break;
				
				case qtpOpenDate:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsDateQuestions + $arreFormsAlphaQuestions;
					break;
				
				case qtpOpenString:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
					break;
				
				case qtpOpenAlpha:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
					break;
					
				//@AAL 06/05/2015: Agregado para soportar preguntas tipo código de barras
				case qtpBarCode:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAllQuestions;
					break;

				case qtpPhoto:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsPhotoQuestions;
					break;
				
				case qtpAction:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsAlphaQuestions;
					break;
				
				case qtpSignature:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsSignatureQuestions;
					break;
				
				case qtpOpenTime:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsTimeQuestions;
					break;
				case qtpAudio:
				case qtpVideo:
				case qtpDocument:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsDocumentQuestions;
					break;
					
				//@JAPR 2014-02-19: Agregada el tipo de pregunta GPS para almacenar el punto donde se va capturando una sección
				//Corregido el tipo qtpGPS, debe ser igual a qtpLocation, aunque para las columnas default no aplica qtpGPS
				case qtpGPS:
				case qtpLocation:
					$arrMyeBavelFormsFields = $arrEmptyeBavelForms + $arreFormsLocationQuestions;
					break;
					
				default:
					//No se supone que debería haber llegado a este punto si fuera un campo no soportado, pero si lo hizo simplemente se ignorará
					$blnValid = false;
					break;
			}
			
			if (!$blnValid) {
				continue;
			}
			
			asort($arrMyeBavelFormsFields);
			$aField->Options = $arrMyeBavelFormsFields;
			$myFields[$aField->Name] = $aField;
			//$aField->Parent = $aFieldForms;
			//$aFieldForms->Children[] = $aField;
		}
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function generateBeforeFormCode($aUser)
 	{
 		$myFormName = get_class($this);
?>
	<script language="javascript" src="js/dialogs.js"></script>
 	<script language="JavaScript">
 		function returnFieldsData()
 		{
 			var objFieldNames = new Object();
<? 			
			//foreach($this->ArrayVariables as $intSurveyFieldID => $strVariableName)
			foreach($this->ArrayVariables as $inteBavelFieldID => $strVariableName)
			{
?>
				objFieldNames['<?=$strVariableName?>'] = Trim(<?=$myFormName?>_SaveForm.<?=$strVariableName?>.value);
<?
			}
 ?>			
 			
 			var strAnd = '';
 			var sParamReturn = '';
 			var arrKeys = Object.keys(objFieldNames); 
 			var intLength = arrKeys.length;
			for (var intCont = intLength -1; intCont >= 0; intCont--) {
				var strFieldName = arrKeys[intCont];
				sParamReturn += strAnd + strFieldName + '=' + objFieldNames[strFieldName];
				strAnd = '|';
			}
 			
			try {
				if (window.setDialogReturnValue)
				{
					setDialogReturnValue(sParamReturn);
				}
			}
			catch (e) {
			}
			try {
				owner.returnValue = sParamReturn;
			}
			catch (e)
			{
			}
			
			//window.close();
			cerrar();
 		}
	</script>
<?
 	}
 	
	function generateAfterFormCode($aUser)
	{
		$myFormName = get_class($this);
		
?>
	<script language="JavaScript">
		objOkSelfButton = document.getElementById("<?=$myFormName?>_OkSelfButton");
		objOkParentButton = document.getElementById("<?=$myFormName?>_OkParentButton");
		
		if (objOkSelfButton != null) {
			objOkSelfButton.onclick = new Function("returnFieldsData();");
		}
		if (objOkParentButton != null) {
			objOkParentButton.style.display = 'none';
		}
		
		var streBavelFieldData = '';
		var objField = null;
		if (window.opener && window.opener.BITAMQuestion_SaveForm && window.opener.BITAMQuestion_SaveForm.eBavelActionFieldsData) {
			objField = window.opener.BITAMQuestion_SaveForm.eBavelActionFieldsData.value;
		}
		try {
			if (objField != null) {
				streBavelFieldData = window.opener.document.getElementsByName('eBavelActionFieldsData')[0].value;
			}

			if (streBavelFieldData) {
				var arrFieldData = streBavelFieldData.split('|');
				for (var intIndex in arrFieldData) {
					var strFieldInfo = Trim(arrFieldData[intIndex]);
					if (strFieldInfo) {
						var arrFieldInfo = strFieldInfo.split('=');
						if (arrFieldInfo.length >= 2) {
							var strFieldName = arrFieldInfo[0];
							var intFieldID = parseInt(arrFieldInfo[1]);
							/*
							if (intFieldID <= 0) {
								intFieldID = 0;
							}
							*/
							
							var objFieldColl = document.getElementsByName(strFieldName);
							if (objFieldColl.length) {
								objFieldColl[0].value = intFieldID;
							}
						}
					}
				}
			}
		}
		catch (e) {
		}
	</script>
<?
	}
}
?>