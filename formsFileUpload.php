<?php
/* Este archivo sólo sirve para recibir archivos sincronizados desde las Apps de eForms iniciando con 6.02001. Originalmente usado para recibir el archivo de posiciones GPS registradas
offline en los dispositivos. NO debe tener funcionalidad para conectarse con bases de datos ni nada similar, ya que debe ser un request rapidísimo
*/
$return = array();
$return['error'] = false;
$blnError = false;

//Verifica si se está recibiendo o no un archivo, ya que si no se recibe no tiene caso que continue con el registro de la información y no debe intentar generar ningún archivo de datos
$strFileName = (string) @$_REQUEST['fileName'];
if (trim($strFileName) == '') {
	$return['errmsg'] = "No file was received";
	$blnError = true;
}

if (!$blnError) {
	clearstatcache();
	$strFormsAppDataPath = getcwd()."\\eFormsAppData";

	if (!file_exists($strFormsAppDataPath)) {
		if (!mkdir($strFormsAppDataPath, null, true)) {
			$blnError = true;
			$return['error'] = true;
			$return['errmsg'] = 'Error creating the app data path in the KPIOnline Server';
		}
	}
}

$strNewFileName = '';
if (!$blnError) {
	require_once('qqFileUploader.class.php');
	define('SV_DOCUMENT_SIZELIMIT', 30 * 1024 * 1024);
	
	// list of valid extensions, ex. array("jpeg", "xml", "bmp")
	$allowedExtensions = array();
	// max file size in bytes
	$sizeLimit = SV_DOCUMENT_SIZELIMIT;
	
	$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);
	$result = $uploader->handleUpload("eFormsAppData/");
	if (!is_null($result) && is_array($result)) {
		$blnError = ((int) @$result['success']) != true;
		if (!$blnError) {
			$strNewFileName = (string) @$result["filename"];
			$_REQUEST['offlineFileName'] = $strNewFileName;
			$return['offlineFileName'] = $strNewFileName;
		}
	}
}

if (!$blnError) {
	$kpiUser = (string) @$_REQUEST['UserID'];
	$kpiPass = (string) @$_REQUEST['Password'];
	$strUUID = (string) @$_REQUEST['uuid'];
	$strData = json_encode($_REQUEST);
	$return['serverSurveyDate'] = date("Y-m-d");
	$return['serverSurveyHour'] = date("H:i:s");
	//El nombre de archivo que se utilizará será la combinación de los datos que identifican al usuario en este punto del tiempo (según el server)
	$strFileName = "offlineData_".$kpiUser."_".str_replace("-", "", $return['serverSurveyDate'])."_".str_replace(":", "", $return['serverSurveyHour'])."_".$strUUID;
	$intTries = 0;
	$strSuffix = '';
	$strExt = '.log';
	do {
		$intTries++;
		$blnFileExists = file_exists($strFormsAppDataPath."\\".$strFileName.$strSuffix.$strExt);
		if ($blnFileExists) {
			$strSuffix = '_'.$intTries;
		}
		else {
			@error_log($strData, 3, $strFormsAppDataPath."\\".$strFileName.$strSuffix.$strExt);
		}
	} while($blnFileExists && $intTries < 100);
}

header("Content-Type: text/javascript");
if (isset($_GET['jsonpCallback'])) 
{
	echo($_GET['jsonpCallback'] . '(' . json_encode($return) . ');');
} 
else 
{
	echo(json_encode($return));
}
?>