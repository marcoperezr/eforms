<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("utils.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Parámetro para indicar si el template de Agendas generado debe llenarse con las definiciones actuales o si simplemente se genera el esqueleto vacio
$blnFill = 0;
if (array_key_exists("fill", $_GET))
{
	$blnFill = (bool) (int) $_GET["fill"];
}
if (array_key_exists("fill", $_POST))
{
	$blnFill = (bool) (int) $_POST["fill"];
}

//GCRUZ 2015-09-03. Parámetros de fitlro del layout
$startDate = (isset($_REQUEST['startDate']))? $_REQUEST['startDate'] : null;
$endDate = (isset($_REQUEST['endDate']))? $_REQUEST['endDate'] : null;
$CheckPointIDs = (isset($_REQUEST['CheckPointIDs']))? explode(',', $_REQUEST['CheckPointIDs']) : null;
$UserIDs = (isset($_REQUEST['UserIDs']))? explode(',', $_REQUEST['UserIDs']) : null;
$StatusIDs = (isset($_REQUEST['StatusIDs']))? explode(',', $_REQUEST['StatusIDs']) : null;
$searchFilter = (isset($_REQUEST['searchFilter']))? $_REQUEST['searchFilter'] : null;

//GCRUZ 2016-02-25. Formas seleccionadas para mostrar en el template
$fillSurveys = (isset($_REQUEST['fillsurveys']) && trim($_REQUEST['fillsurveys']) != '')? explode(',', $_REQUEST['fillsurveys']) : array();
$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
if (count($fillSurveys) > 0)
{
	$theAppUser->updateFromArray(array('AgendasSurveysTemplate' => $_REQUEST['fillsurveys']));
	$theAppUser->save();
}


require_once("survey.inc.php");
require_once('surveyAgenda.inc.php');
require_once("user.inc.php");
require_once("catalog.inc.php");

$blnWriteFile = true;
$sfile = 'AgendasTemplate-'.session_id().'-DTE'.date('YmdHis').'.csv';
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogPath = GeteFormsLogPath();
$spathfile = $strLogPath.$sfile;
//@JAPR
$LN = chr(13).chr(10);

//Genera la cabecera del archivo
$aSurveyCollection = BITAMSurveyCollection::NewInstance($theRepository, null, false);

if (is_null($aSurveyCollection) || count($aSurveyCollection->Collection) == 0)
{
	$blnWriteFile = false;
	//@JAPR 2019-09-19: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	error_log("\r\n"."There're not surveys available to schedule in an agenda", 3, $strLogPath.'ExportAgendas.txt');
	//@JAPR
}
else 
{
	//Obtiene el Array con los nombres de encuestas y otro indexado por IDs para marcarlas existentes
	$arrSurveyNames = array();
	$arrSurveyCheck = array();
	//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
	$arrSurveyNamesByID = array();
	foreach ($aSurveyCollection as $aSurvey)
	{
		//GCRUZ 2016-02-25. Formas seleccionadas para mostrar en el template
		if (count($fillSurveys) > 0 && array_search($aSurvey->SurveyID, $fillSurveys) === false)
			continue;
		$arrSurveyNames[] = '"'.str_replace('"', '""', $aSurvey->SurveyName).'"';
		$arrSurveyCheck[$aSurvey->SurveyID] = 0;
		//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
		$arrSurveyNamesByID[$aSurvey->SurveyID] = str_replace('"', '""', $aSurvey->SurveyName);
	}
}

if ($blnWriteFile)
{
	//@JAPR 2014-11-03: Modificado el esquema de agendas
	//Ya no se agregará el campo de CatalogAttribute
	//$line = '"EMail","CatalogName","CatalogAttribute","Filter","Date","Time",';
	//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
	//GCRUZ 2015-09-03. Agregada columna de CheckPoint
	//GCRUZ 2015-09-03. Agregada columna de Status
	//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
//	$line = '"EMail","CatalogName","CheckPoint","Filter","Status","Date","Time","Check-in Form",';

	//EARS 2016-12-30 se añade columna Name issue X17ERA
	$line = '"Name","EMail","CheckPoint","Filter","Status","Date","Time","Check-in Form",';
	//@JAPR
	$line .= implode(',', $arrSurveyNames);
	$fStats = fopen($spathfile, 'w+');
	if ($fStats)
	{
		if(!fwrite($fStats, $line))
		{
			$blnWriteFile = false;
			//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
			error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportAgendas.txt');
			//@JAPR
		}
	}
	else
	{
		$blnWriteFile = false;
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		error_log('\r\n'.'Unable to open to write file contents in '.$spathfile, 3, $strLogPath.'ExportAgendas.txt');
		//@JAPR
	}

	//GCRUZ 2015-09-04. Generar la exportación en XLS
	require_once('../phpexcel/Classes/PHPExcel.php');
	// instantiate Excel.Application
	$bLoaded = false;
	try	
	{
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
	} 
	catch (Exception  $e) 
	{
		echo "Error creating xls object";
		exit();
//		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}

	// Set properties
	$objPHPExcel->getProperties()->setCreator("BITAM EForms");

	$sheetNames = $objPHPExcel->getSheetNames();
	$sheetCount = $objPHPExcel->getSheetCount();
	$objPHPExcel->setActiveSheetIndex(0);

	// create xls
	$o = $objPHPExcel;

	// get active sheet
	$a = $o->getActiveSheet();
	$a->setTitle('Agendas');

	// set default column width
	$a->getDefaultColumnDimension()->setWidth(20.71); // si ponemos 20, con los bordes queda en 19.29


	$ncounter = 1;
	$strFirst = "";
	$strCounter = "A";
	$intColumnCounter = 65;

	//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
	//EARS 2016-12-30 se añade columna Name issue X17ERA
	if ($blnFill)
		$xlsColumns = explode(',', 'Name,EMail,CheckPoint,Filter,Status,Date,Time,Check-in Form,'.implode(',', $arrSurveyNames));
	else
		$xlsColumns = explode(',', 'Name,EMail,CheckPoint,Filter,Date,Time,Check-in Form,'.implode(',', $arrSurveyNames));

	//Columnas
	foreach ($xlsColumns as $fields)
	{
		$fields = str_replace('"', '', $fields);
		$strIndex = $strFirst.chr($intColumnCounter);
//		echo $strIndex.$ncounter . '=' . $fields . "<br>";
		$a->setCellValue($strIndex.$ncounter, $fields);
		if (chr($intColumnCounter) == "Z")
		{
			$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
			$intColumnCounter = 65;
		}
		else 
		{
			$intColumnCounter++;;
		}
	}

	// set range alignment
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// set fill style
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FFFF99');

	// set font style from array
	$arrStyle = array();
	$arrStyle["font"] = array();
	$arrStyle["font"]["name"] = "Arial";
	$arrStyle["font"]["size"] = 10;
	$arrStyle["font"]["bold"] = true;
	$arrStyle["font"]["color"] = array("rgb" => "0000FF");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->applyFromArray($arrStyle);

	// set border style from array
	$arrBorderStyle = array();
	$arrBorderStyle["allborders"] = array();
	$arrBorderStyle["allborders"]["style"] = PHPExcel_Style_Border::BORDER_THIN;
	$arrBorderStyle["allborders"]["color"] = array("rgb" => "000000");
	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getBorders()->applyFromArray($arrBorderStyle);
//	$a->getStyle(chr(65).$ncounter.':'.$strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_PROTECTED);


}

if ($blnFill && $blnWriteFile)
/*if ($blnWriteFile)*/
{
	//GCRUZ 2015-09-04. Generar la exportación en XLS	

	// select A2!
	$ncounter++;
	$a->setSelectedCell(chr(65).$ncounter);
	$i = 1;
	
	//Llena todos los registros con las agendas existentes
	//GCRUZ 2015-09-03. Pasar los parámetros de filtro al generar la colección de agendas
	$anAgendaCollection = BITAMSurveyAgendaCollection::NewInstance($theRepository, null, $UserIDs, $endDate, null, false, $startDate, null, 100, false, null, $searchFilter, $StatusIDs, $CheckPointIDs);
	
	if (!is_null($anAgendaCollection) && count($anAgendaCollection->Collection) > 0)
	{
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$arrUsers = BITAMeFormsUser::GetUsers($theRepository, false);
		$arrCatalogs = BITAMCatalog::getCatalogs($theRepository, false);
		$arrCatalogMembers = BITAMSurveyAgenda::getCatMembersByCatalogID($theRepository);
		
		foreach ($anAgendaCollection as $anAgenda)
		{
			$arrSurveyIDs = $arrSurveyCheck;
			foreach ($anAgenda->SurveyIDs as $intSurveyID)
			{
				if (isset($arrSurveyIDs[$intSurveyID]))
				{
					$arrSurveyIDs[$intSurveyID] = 1;
				}
			}
			
			$strEMail = str_replace('"', '""', (string) @$arrUsers[$anAgenda->UserID]);
			//GCRUZ 2015-09-03. Agregada columna de CheckPoint
			$strCheckPoint = str_replace('"', '""', (string) $anAgenda->CheckPointName);
			$strCatalog = str_replace('"', '""', (string) @$arrCatalogs[$anAgenda->CatalogID]);
			//@JAPR 2014-11-03: Modificado el esquema de agendas
			//Ya no se agregará el campo de CatalogAttribute
			//$strCatMember = str_replace('"', '""', (string) @$arrCatalogMembers[$anAgenda->CatalogID][$anAgenda->AttributeID]);
			//@JAPR
			$strFilterText = str_replace('"', '""', $anAgenda->FilterText);
			//EARS 2016-12-30 se añade columna Name issue X17ERA
			$strAgendaName = GetValidJScriptText($anAgenda->AgendaName);
			//GCRUZ 2015-09-03. Agregada columna de Status
			$strStatus = '';
			switch($anAgenda->Status)
			{
				case agdOpen:
					$strStatus = 'Open';
					break;
				case agdAnswered:
					$strStatus = 'Answered';
					break;
				case agdRescheduled:
					$strStatus = 'Rescheduled';
					break;
				case agdFinished:
					$strStatus = 'Finished';
					break;
				case agdCanceled:
					$strStatus = 'Canceled';
					break;
			}
			//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			$strCheckInFormName = str_replace('"', '""', (string)@$arrSurveyNamesByID[$anAgenda->CheckinSurvey]);
			//@JAPR 2014-11-03: Modificado el esquema de agendas
			//Ya no se agregará el campo de CatalogAttribute
			//Corregido un bug, el <enter> se estaba insertando antes de la lista de encuestas en lugar de antes del EMail del usuario
			//$line = '"'.$strEMail.'","'.$strCatalog.'","'.$strCatMember.'","'.$strFilterText.'","'.$anAgenda->CaptureStartDate.'","'.$anAgenda->getAgendaTime().'",';
			//@JAPR 2014-11-03: Agregada la columna con el nombre de la encuesta de Check-In configurada para la agenda
			//GCRUZ 2015-09-03. Agregada columna de CheckPoint
			//GCRUZ 2015-09-04. Quitar columna de nombre de catálogo
			/*$line = $LN.'"'.$strEMail.'","'.$strCheckPoint.'","'.$strFilterText.'","'.$strStatus.'","'.$anAgenda->CaptureStartDate.'","'.$anAgenda->getAgendaTime().'","'.$strCheckInFormName.'",';*/
			$line = $LN.'"'.$strAgendaName.'","'.$strEMail.'","'.$strCheckPoint.'","'.$strFilterText.'","'.$strStatus.'","'.$anAgenda->CaptureStartDate.'","'.$anAgenda->getAgendaTime().'","'.$strCheckInFormName.'",';
			//@JAPR
			$line .= implode(',', $arrSurveyIDs);
			if(!fwrite($fStats, $line))
			{
				$blnWriteFile = false;
				//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
				error_log('\r\n'.'Unable to write file contents in '.$spathfile, 3, $strLogPath.'ExportAgendas.txt');
				//@JAPR
			}
			
			$xlsRows = explode(',', str_replace($LN, '', str_replace('"', '', $line)));
			$intColumnCounter = 65;
			$strFirst = "";
			foreach ($xlsRows as $field => $value)
			{
				$value = str_replace('"', '', $value);
				$strIndex = $strFirst.chr($intColumnCounter);
	//			echo $strIndex.$ncounter . '=' . $value . "<br>";
				$a->setCellValue($strIndex.$ncounter, $value);
	//			$a->getStyle($strIndex.$ncounter)->getAlignment()->setWrapText(true);
	//			$a->getStyle($strIndex.$ncounter)->getProtection()->setLocked(PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
				if (chr($intColumnCounter) == "Z")
				{
					$strFirst = (strlen($strFirst) == 0 ? "A" : chr(ord($strFirst) + 1));
					$intColumnCounter = 65;
				}
				else 
				{
					$intColumnCounter++;;
				}
			}
			$ncounter++;
			$i++;
			
			/*
			if (!$blnWriteFile)
			{
				break;
			}
			*/
		}
	}
}

//Descarga el archivo o genera el error de exportación

if($blnWriteFile)
{
	//GCRUZ 2015-09-04. Generar la exportación en XLS	
	//	$a->getProtection()->setSheet(true);

	unset($r);
	unset($a);
	unset($o);

	$fileName = str_replace('.csv', '.xls', $spathfile);
	$strXLSPath = $fileName.($ncounter > 255 ? "x" : "");

// save file
	if ($ncounter > 255)
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	}
	else
	{
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	}

	try 
	{
		$objWriter->save($strXLSPath);
		$spathfile = $fileName.($ncounter > 255 ? "x" : "");
		$sfile = str_replace('.csv', '.xls', $sfile);
		$sfile = $sfile.($ncounter > 255 ? "x" : "");
	}
	catch (Exception  $e) 
	{
		echo "Error file path:{$fileName}";
		exit();
//		return array(false, $e->getCode(), $e->getMessage(), $e->getLine());
	}
  
	unset($objWriter);
	
	

	if(!is_file($spathfile))
	{
		echo "Error file path:{$spathfile}";
		exit();
	}
	else
	{		
		/*
		header('Content-Type: text/csv'); 
		header('Content-Disposition: attachment;filename="'.$sfile.'"');
		header('Content-Length: '.filesize($spathfile));
		header('Cache-Control: max-age=0');
		*/
		
		//fix for IE catching or PHP bug issue		
		header("Pragma: public");
		header("Expires: 0"); // set expiration time
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment; filename=\"".$sfile."\";");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: ".filesize($spathfile));
		while (ob_get_level()) {
			ob_end_clean();
		}										
		readfile($spathfile);
		exit();
	}
}
else
{
	echo "Error file path:{$spathfile}";
	exit();
}

?>