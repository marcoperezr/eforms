<?php
require_once("checkCurrentSession.inc.php");
//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
require_once("uploadFileExt.php");
//@JAPR
$sError = '';
$blnError = false;
$path = str_replace("\\", "/", realpath("."))."/customerimages/".$_SESSION["PABITAM_RepositoryName"];
ob_start();
if (!file_exists($path)) {
	if (!mkdir($path, null, true)) {
		$strErrDesc = trim(ob_get_contents());
		$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$strErrDesc;
		$blnError = true;
		//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
		//Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará en el evento onImageUploadFail
		print_r("{state: false, extra: '{$strErrDesc}'}");
		die();
		//@JAPR
	}
}

if (@$_REQUEST["action"] == "uploadImage" && array_key_exists("file", $_FILES))
{
	if (is_array($_FILES["file"]))
	{
		$state = 'true'; $filename = '';
		if ($_FILES['file']['error'] == 1){
			$sError = "Exceed Max Server Upload";
			$state = 'false';
		}
		elseif ($_FILES['file']['error'] == 2){
			$sError = "Exceed Max Post Size";
			$state = 'false';
		}
		elseif ($_FILES['file']['error'] == 3){
			$sError = "Partial Upload";
			$state = 'false';
		}
		elseif ($_FILES['file']['error'] == 4){
			$sError = "No File Uploaded";
			$state = 'false';
		}
		elseif (is_uploaded_file($_FILES["file"]["tmp_name"]))
		{
			//Ejemplo de como debe estar $path "E:/inetpub/wwwroot/artus/gen8/ESurveyV6/customerimages/fbm_bmd_0532/form.GIF";
			//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
			$filename = $_FILES["file"]["name"];
			$strXLSFileName = replaceInvalidFilenameChars($path."/".StripSpecialCharsFromFileNames($_FILES["file"]["name"], "photo"));
			$strXLSFileTempName = replaceInvalidFilenameChars($_FILES['file']['tmp_name']);
			$arrFileInfo = getExtendedFileInfo($strXLSFileName);
			
			if ( $arrFileInfo && is_array($arrFileInfo) ) {
				if ( !checkValidExtension(@$arrFileInfo["fileExtension"], null, true) ) {
					//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
					//Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará en el evento onImageUploadFail
					$blnError = true;
					print_r("{state: false, extra: '".translate("Invalid file extension")."'}");
					die();
					//@JAPR
				}
			}
			//@JAPR
			
			if (!$blnError) {
				//OMMC 2015-10-08: si $strXLSFileName que es el nombre de la imagen contenía algún caracter especial, no se mostraba, se codifica como UTF8 para que se muestre.
				move_uploaded_file($strXLSFileTempName, utf8_decode($strXLSFileName));
				//move_uploaded_file($strXLSFileTempName, $strXLSFileName);
				$strErrDesc = trim(ob_get_contents());
				if ($strErrDesc != '') {
					$strErrDesc = translate("File not uploaded").", ".translate("Error").": ".$strErrDesc;
					$blnError = true;
					//@JAPR 2018-01-15: Agregada validación para huecos de seguridad y control de extensiones en upload de archivos (#JMK4PR)
					//Faltaba reportar los errores, la propiedad extra del objeto cuando no hay state o este es false, indica el error que se reportará en el evento onImageUploadFail
					print_r("{state: false, extra: '{$strErrDesc}'}");
					die();
					//@JAPR
				}
			}
		}

		header("Content-Type: text/html; charset=utf-8");
    	print_r("{state: ".$state.", itemId: '".@$_REQUEST["itemId"]."', itemValue: '".$filename."'}");
	}
}

if (@$_REQUEST["action"] == "loadImage")
{
    $i = "images/transparent.gif";
    if (isset($_REQUEST["itemValue"]) && $_REQUEST["itemValue"] != '')
    {
		$k = "customerimages/".$_SESSION["PABITAM_RepositoryName"]."/".$_REQUEST["itemValue"];
	    if (file_exists($k))
		{
			$i = $k;
		}
    }

    $filename = basename($i);
	$file_extension = strtolower(substr(strrchr($filename,"."),1));

	switch( $file_extension ) {
	    case "gif": $ctype="image/gif"; break;
	    case "png": $ctype="image/png"; break;
	    case "jpeg":
	    case "jpg": $ctype="image/jpg"; break;
	    case "bmp": $ctype="image/bmp"; break;
	    case "ico": $ctype="image/ico"; break;
	    default:
	}
	
	header("Content-Typ: " . $ctype);
	print_r(file_get_contents($i));

}