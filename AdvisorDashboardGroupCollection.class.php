<?php
class AdvisorDashboardGroupCollection
{
	public $DashboardGroupCollection = null;
	
	public static function getDashboardGroupCollection($aUserIDsCollection, $aRoleIDsCollection, $aRepository)
	{
		if (count($aUserIDsCollection) > 1)
		{
			$aDashboardGroupCollection = AdvisorDashboardGroupCollection::getUserPubDashboardGroupsCollection($aUserIDsCollection, $aRoleIDsCollection, $aRepository);
		}
		else
		{
			$aDashboardGroupCollection = new AdvisorDashboardGroupCollection();
			$aDashboardGroupCollection->DashboardGroupCollection = AdvisorDashboardGroup::loadUserDashboardGroups($aUserIDsCollection, $aRepository->ADOConnection);
			//AdvisorDashboardGroup::$registered = array();
			//AdvisorDashboard::$registered = array();
			$aDashboardGroupCollection->DashboardGroupCollection[] = AdvisorDashboardGroup::getAllUserPubDashboardGroups($aUserIDsCollection, array(), $aRepository->ADOConnection);
		}
		return $aDashboardGroupCollection;
	}
	
	public static function getUserPubDashboardGroupsCollection($aUserIDsCollection, $aRoleIDsCollection, $aRepository)
	{
		$aDashboardGroupCollection = new AdvisorDashboardGroupCollection();
		$aDashboardGroupCollection->DashboardGroupCollection = AdvisorDashboardGroup::getAllUserPubDashboardGroups($aUserIDsCollection, $aRoleIDsCollection, $aRepository->ADOConnection);
		
		return $aDashboardGroupCollection;
	}

	public function generateFields($aConnection = null)
	{
		require_once('builderwidget/BuilderWidget.php');
		$data = array();
		if (is_array($this->DashboardGroupCollection))
		{
			foreach ($this->DashboardGroupCollection as $aDashboardGroupCol)
			{
				//$element = new Builder_WidgetDataNode($aDashboardGroupCol->nom_gpo_esc, $aDashboardGroupCol->cla_gpo_esc);
				$element = new Builder_WidgetDataNode($aDashboardGroupCol->nom_gpo_esc, $aDashboardGroupCol->cla_gpo_esc, false, null, array('tipo_objeto' => 1));
				foreach ($aDashboardGroupCol->Dashboards as $aDashboard)
				{
					$hasFilters = false;
					if (!is_null($aConnection))
					{
						$hasFilters = AdvisorUtils::dashboardHasFilters($aDashboard->cla_escenario, $aConnection);
					}
					$element->data[] = new Builder_WidgetDataNode($aDashboard->nom_escenario, $aDashboard->cla_escenario, false, null, array('tipo_objeto' => 1, 'hasFilters' => $hasFilters));
				}
				$data[] = $element;
			}
		} else
		{
			$aDashboardGroupCol = $this->DashboardGroupCollection;
			//$element = new Builder_WidgetDataNode($aDashboardGroupCol->nom_gpo_esc, $aDashboardGroupCol->cla_gpo_esc);
			$element = new Builder_WidgetDataNode($aDashboardGroupCol->nom_gpo_esc, $aDashboardGroupCol->cla_gpo_esc, false, null, array('tipo_objeto' => 1));
			foreach ($aDashboardGroupCol->Dashboards as $aDashboard)
			{
				$hasFilters = false;
				if (!is_null($aConnection))
				{
					$hasFilters = AdvisorUtils::dashboardHasFilters($aDashboard->cla_escenario, $aConnection);
				}
				$element->data[] = new Builder_WidgetDataNode($aDashboard->nom_escenario, $aDashboard->cla_escenario, false, null, array('tipo_objeto' => 1, 'hasFilters' => $hasFilters));
			}
			$data[] = $element;
		}
		
		return $data;
	}
}
?>