<?php

function getGoogleMapsAddressWithLatLng($latitude, $longitude)
{
	$strGPSURL = "http://maps.google.com/maps/api/geocode/json?sensor=false".googleAPIKey."&language=es&latlng=$latitude,$longitude";

	$googleAddress = false;
	$intGetAddressAttempt = 1;
	while ($googleAddress === false && $intGetAddressAttempt < 5)
	{
	$ch = curl_init(); 
	if ($ch) {
		curl_setopt($ch, CURLOPT_URL, $strGPSURL); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		$strJSON = curl_exec($ch);
		if ($strJSON) {
			curl_close($ch);
//			$jsonData = json_decode($strJSON, true);
			//Convierte la respuesta a un objeto para manipular el resultado
			$arrGPSData = @getGPSDataFromJson($strJSON);
			if (!is_null($arrGPSData) && is_array($arrGPSData)) {
				$googleAddress = array();
				$googleAddress['FullAddress'] = @$arrGPSData['FullAddress'];
				$googleAddress['lat'] = $latitude;
				$googleAddress['lng'] = $longitude;
				$googleAddress['Country'] = @$arrGPSData['Country'];
				$googleAddress['State'] = @$arrGPSData['State'];
				$googleAddress['City'] = @$arrGPSData['City'];
				$googleAddress['ZipCode'] = @$arrGPSData['ZipCode'];
			}
			elseif ($arrGPSData == 'OVER_QUERY_LIMIT')
			{
				error_log(date('Y-m-d H:i:s').'===Error al obtener direccion de google maps con lat: '.$latitude.', lng:'.$longitude.'==='.PHP_EOL.$strJSON.PHP_EOL, 3, 'google.address.maps.inc.php.log');
				$intGetAddressAttempt--;
			}

		}
		else {
			curl_close($ch);
		}
	}
	$intGetAddressAttempt++;
	}
	return $googleAddress;
}

function getGoogleMapsAddresswWithAddress($address)
{
	$address = str_replace(' ', '+', $address);
	$address = utf8_encode($address);
	$strGPSURL = "http://maps.google.com/maps/api/geocode/json?sensor=false".googleAPIKey."&language=es&address=$address";

	$googleAddress = false;
	$intGetAddressAttempt = 1;
	while ($googleAddress === false && $intGetAddressAttempt < 5)
	{
	$ch = curl_init();
	if ($ch) {
		curl_setopt($ch, CURLOPT_URL, $strGPSURL); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
		$strJSON = curl_exec($ch);
		if ($strJSON) {
			curl_close($ch);
			$jsonData = json_decode($strJSON, true);
			if (isset($jsonData['status']) && strtoupper($jsonData['status']) == 'OK')
			{
				if (isset($jsonData['results'][0]['geometry']['location']))
				{
					$location_data = $jsonData['results'][0]['geometry']['location'];
					if (isset($location_data['lat']) && isset($location_data['lng']))
					{
						$googleAddress = array();
						$googleAddress['lat'] = $location_data['lat'];
						$googleAddress['lng'] = $location_data['lng'];
					}
					error_log(date('Y-m-d H:i:s').'===OK obtener direccion de google maps con: '.$address.'==='.PHP_EOL, 3, 'google.address.maps.inc.php.log');
				}
			}
			else
			{
				error_log(date('Y-m-d H:i:s').'===Error al obtener direccion de google maps con: '.$address.'==='.PHP_EOL.$strJSON.PHP_EOL, 3, 'google.address.maps.inc.php.log');
				if (isset($jsonData['status']) && strtoupper($jsonData['status']) == 'OVER_QUERY_LIMIT')
					$intGetAddressAttempt += 0;
				else
					$intGetAddressAttempt = 5;
			}
		}
		else {
			curl_close($ch);
		}
	}
	$intGetAddressAttempt++;
	}
	return $googleAddress;
}

function getlatlngDifference($lat1, $lng1, $lat2, $lng2)
{
	//(where R is the radius of the Earth, 6373000 m)
	$R = 6373000;
	//Transform degree values to radians
	$lat1 = $lat1 * pi() / 180;
	$lng1 = $lng1 * pi() / 180;
	$lat2 = $lat2 * pi() / 180;
	$lng2 = $lng2 * pi() / 180;

	$dlng = $lng2 - $lng1;
	$dlat = $lat2 - $lat1;
	$a = pow((sin($dlat/2)), 2) + cos($lat1) * cos($lat2) * pow((sin($dlng/2)), 2);
	$c = 2 * atan2(sqrt($a), sqrt(1-$a));
	$d = $R * $c;
	return $d;
}

/*
//Test
echo "<pre>";
$google_address1 = getGoogleMapsAddresswWithAddress('5+de+Mayo+num+17,+Col+Mirador,+Cerro+Azul+Veracruz');
var_dump($google_address1);
$google_address2 = getGoogleMapsAddresswWithAddress('5+de+Mayo+num+16,+Col+Mirador,+Cerro+Azul+Veracruz');
var_dump($google_address2);
*/

//$diff = getlatlngDifference(22.2492541, -97.8680898, 22.24909, -97.86814);
//var_dump($diff);

/*
//exit;
$nwe_google_address = getGoogleMapsAddressWithLatLng($google_address1['lat'], $google_address1['lng']);
var_dump($nwe_google_address);
$nwe_google_address = getGoogleMapsAddressWithLatLng($google_address2['lat'], $google_address2['lng']);
var_dump($nwe_google_address);
*/
?>