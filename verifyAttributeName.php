<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Obtener el AttributeName
$AttributeName = "";
if (array_key_exists("AttributeName", $_POST))
{
	$AttributeName = $_POST["AttributeName"];
}

//Obtener el AttributeNameOld
$AttributeNameOld = "";
if (array_key_exists("AttributeNameOld", $_POST))
{
	$AttributeNameOld = $_POST["AttributeNameOld"];
}

//Obtener el ObjectID
$ObjectID = 0;
if (array_key_exists("ObjectID", $_POST))
{
	$ObjectID = (int)$_POST["ObjectID"];
}

require_once("catalog.inc.php");

$response = "ERROR";

//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
$strReservedErrorMsg = translate("{var1} is a reserved word, use another name please");
//@JAPR
//Si se trata de un nuevo objeto se debe validar los nuevos nombres de atributo
if($ObjectID==-1)
{
	//Verificamos en la tabla si_descrip_enc si el AttributeName ya se encuentra dado de alta
	//como nom_logico en dicha tabla
	$exist = BITAMCatalog::existThisAttributeName($theRepository, $AttributeName);
	
	if(!$exist)
	{
		$response = "OK";
	}
	else 
	{
		//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
		if ($exist === true) {
			$response = "ERROR<SVSEP>Can not add this catalog/attribute because there is a dimension with the same name";
		}
		else {
			global $garrReservedDimNames;
			$strReservedWord = (string) @array_keys($garrReservedDimNames)[(int) $exist -1];
			$strReservedErrorMsg = str_ireplace('{var1}', $strReservedWord, $strReservedErrorMsg);
			$response = "ERROR<SVSEP>".$strReservedErrorMsg;
		}
		//@JAPR
	}
}
else 
{
	//Verificamos si hubo cambio en los nombres de atributos
	if($AttributeName!=$AttributeNameOld)
	{
		//Verificamos en la tabla si_descrip_enc si el AttributeName ya se encuentra dado de alta
		//como nom_logico en dicha tabla
		$exist = BITAMCatalog::existThisAttributeName($theRepository, $AttributeName);
	
		if(!$exist)
		{
			$response = "OK";
		}
		else 
		{
			//@JAPR 2015-05-07: Agregadas palabras reservadas para no ser utilizadas como nombres cortos de preguntas (#11CYMH)
			if ($exist === true) {
				$response = "ERROR<SVSEP>Can not edit this catalog/attribute because there is a dimension with the same name";
			}
			else {
				global $garrReservedDimNames;
				$strReservedWord = (string) @array_keys($garrReservedDimNames)[(int) $exist -1];
				$strReservedErrorMsg = str_ireplace('{var1}', $strReservedWord, $strReservedErrorMsg);
				$response = "ERROR<SVSEP>".$strReservedErrorMsg;
			}
			//@JAPR
		}
	}
	else 
	{
		$response = "OK";
	}
}

header('Content-Type: text/plain; charset=utf-8');
echo $response;
?>