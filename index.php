<?php
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}

require_once('config.php');
require_once("repository.inc.php");
require_once('appuser.inc.php');

if (array_key_exists("goto", $_GET))
{
	$goto = $_GET["goto"];
	$url = base64_decode($goto);
}
else
{
	$goto = "";
	$url = "main.php";
}
session_start();

//@JAPR 2015-10-16: Removida la variable de sesión PAArtusLinker, ya que no se utiliza en la versión v6 y el Framework cambió, así que no sería compatible la manera de redireccionar
//a una ventana específica, pero si causaba que al entrar por KPIOnline o servicios que no eran la página directa (escenarios por ejemplo, pasando por loginBitam.php) al intentar marcar
//un error redireccionando a index.php (lo cual por si mismo ya era un error, así no se deben reportar los problemas que cancelen sesión) generara un ciclo infinito. Este parámetro
//sólo se asignará ahora desde loginBitam.php así que únicamente se valida en el primer login, aquí mismo se removerá para que no cause mas problemas
if ((array_key_exists("BITAM_RepositoryName", $_POST) && array_key_exists("BITAM_UserName", $_POST) &&
	array_key_exists("BITAM_Password", $_POST)) || array_key_exists("PAArtusLinker",$_SESSION))
{
	if(array_key_exists("PAArtusLinker",$_SESSION))
	{
		$theRepositoryName = $_SESSION["PABITAM_RepositoryName"];
		$theUserName = strtoupper($_SESSION["PABITAM_UserName"]);
		$thePassword = $_SESSION["PABITAM_Password"];
		unset($_SESSION["PAArtusLinker"]);
	}
	else
	{
		$theRepositoryName = $_POST["BITAM_RepositoryName"];
		$theUserName = strtoupper($_POST["BITAM_UserName"]);
		$thePassword = $_POST["BITAM_Password"];
	}
	
	if (!array_key_exists($theRepositoryName, $BITAMRepositories))
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", "");
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=1".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	
	$theRepository = $BITAMRepositories[$theRepositoryName];
	if (!$theRepository->open())
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", "");
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=2".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}

	//@JAPR 2008-09-20: Validación de la versión de la Metadata
	$anArtusMDVersion = 0.0;
	$sql = 'SELECT TIPO_PRODUCTO, MAX(VERSION) AS Version '.
			'FROM SI_MD_VERSION '.
			'WHERE TIPO_PRODUCTO IN (1) '.
			'GROUP BY TIPO_PRODUCTO';
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if (!$aRS)
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", "");
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=8".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	/*
	if (isset ($_SESSION["EFormLogged"])) ////////////AQUI
	{
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=10" . ($goto == "" ? "" : "&goto=" . $goto) . "\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	$_SESSION["EFormLogged"]=1;/////AQUI
	*/
	
	while (!$aRS->EOF)
	{
		switch ($aRS->fields["tipo_producto"])
		{
			case 1:
				$anArtusMDVersion = (float) $aRS->fields["version"];
				$anArtusMDVersion = round($anArtusMDVersion, 5);
				break;
		}
		$aRS->MoveNext();
	}
	
	//Verifica contra la metadata de Artus
	if ($anArtusMDVersion < ARTUS_MD_REQUIRED_VERSION)
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", "");
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=9".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	
	//@JAPR 2019-01-30: Debido a un incidente reciente, LRoux indicó que ya no deberían realizarse ALTER TABLEs continuos para evitar Table locks, así que se
	//comentarán todos los siguientes estatutos bajo el entendido que la única tabla de metadata de KPIOnline tendría siempre que estar al día manualmente,
	//de cualquier manera es buena idea seguir agregando los ALTERs en este bloque como un histórico de los cambios sufridos en el Agente de eForms (#BBGFMH)
	/*//@JAPR 2016-10-07: Agregada la sincronización de permisos entre las tablas de usuario y la configuración del producto inicial (#MX5O2V)
	//Debido a que este cambio se realiza independiente de la Metadata de BITAM, ya que fue un ajuste masivo involucrando a diferentes productos y que teóricamente agregaría las columnas
	//y actualizaría defaults en todos los repositorios, sin embargo eForms no puede depender de ello así que él mismo se encarga de aplicarlo para no provocar errores al usarlo
	$sql = "ALTER TABLE SI_USUARIO ADD EFORMS_ADMIN INT NULL";
	$theRepository->ADOConnection->Execute($sql);
	
	$sql = "ALTER TABLE SI_USUARIO ADD EFORMS_DESKTOP INT NULL";
	$theRepository->ADOConnection->Execute($sql);
	
	$sql = "ALTER TABLE SI_USUARIO ADD KPI_DEFAULT INT NULL";
	$theRepository->ADOConnection->Execute($sql);*/
	//@JAPR
	
	//Verificar si se encuentra en modo FBM
	$sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";
	$aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);
	if (!$aRSFBM)
	{
		die( translate("Error accessing")." SI_CONFIGURA ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlFBM);
	}
	
	$valueFBMMode = 0;
	
	if (!$aRSFBM->EOF)
	{
		if($aRSFBM->fields["ref_configura"]!==null && trim($aRSFBM->fields["ref_configura"])!=="")
		{
			$valueFBMMode = (int)trim($aRSFBM->fields["ref_configura"]);
		}
	}
	
	//@JAPR 2014-11-19: Agregada la dimensión Agenda
	@AutoUpdateRequiredMetadata($theRepository);
	
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
	$theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
	if (is_null($theUser))
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", $theUserName);
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=3".((!isset($_GET["SSO"]))?"":"&SSO=1").($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}

	$theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
	if(is_null($theAppUser))
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", "");
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=4".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	
	//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
	//Carga la seguridad del usuario logeado para mantenerla en memoria por si aplica en diferentes ventanas y/o procesos
	//Por el momento dado a que esta seguridad no puede ser modificada desde la interfaz, es factible sólo cargarla una vez y dejarla en la sesión permanentemente, sólo que se indexará
	//utilizando el ID del usuario para que en caso de mezcla de sesiones que no sea por medio de un login, al no encontrar la seguridad por lo menos intentará cargarla de nuevo
	//@JAPR 2016-08-23: Corregido el registro de seguridad por usuario, no estaba considerando al repositorio así que mismos usuarios de diferentes BDs habrían tenido conflicto (#KY40NF)
	//@JAPR 2016-11-04: Modificado para que en este punto que es un registro de login (no se repite constantemente), siempre se cargue la seguridad independientemente de si ya
	//había o no alguna cargada
	//if (!isset($_SESSION["PABITAM_Security"]) || !isset($_SESSION["PABITAM_Security"][$theRepository->RepositoryName]) || !isset($_SESSION["PABITAM_Security"][$theRepository->RepositoryName][$theUser->UserID])) {
		if (!isset($_SESSION["PABITAM_Security"])) {
			$_SESSION["PABITAM_Security"] = array();
		}
		
		if (!isset($_SESSION["PABITAM_Security"][$theRepository->RepositoryName])) {
			$_SESSION["PABITAM_Security"][$theRepository->RepositoryName] = array();
		}
		
		$arrSecurity = BITAMAppUser::GetUserSecurity($theRepository, $theUser->UserID);
		if (is_array($arrSecurity)) {
			$_SESSION["PABITAM_Security"][$theRepository->RepositoryName][$theUser->UserID] = $arrSecurity;
		}
	//}
	//@JAPR
	
	//Arreglo que contendrá los valores de cada uno de los roles del usuario (Tabla SI_SV_Users)
	$userRoles = array();
	$userRoles["SV_Admin"] = $theAppUser->SV_Admin;
	$userRoles["SV_User"] = $theAppUser->SV_User;
	
	//Si el usuario no existe en la tabla SI_PA_Security, entonces no se podrá loguear
	if(($userRoles["SV_Admin"]==0 && $userRoles["SV_User"]==0))
	{
		if($valueFBMMode!=1)
		{
			//Verificar si no se trata del usuario supervisor y entra la primera vez
			if($theUser->UserID==0)
			{
				$theAppUser->updateProfile();
				
				$userRoles["SV_Admin"] = $theAppUser->SV_Admin;
				$userRoles["SV_User"] = $theAppUser->SV_User;
			}
			else 
			{
				setcookie("PABITAM_RepositoryName", $theRepositoryName);
			    setcookie("PABITAM_UserName", "");
				exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=4".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
			}
		}
		else 
		{
			setcookie("PABITAM_RepositoryName", $theRepositoryName);
		    setcookie("PABITAM_UserName", "");
			exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=genericErrorPage.php\">\n</head>\n<body>\n</body>\n</html>\n");
		}
	}
	
	/*Validación de LDAP*/
	if($theUser->intConfigSecurity == 1)
	{
		//@JAPR 2015-02-09: Agregado soporte para php 5.6
		//session_register("PAthePassword");
		
		$sql = "SELECT CLA_CONFIGURA, REF_CONFIGURA FROM SI_CONFIGURA";
		$sql .= " WHERE CLA_USUARIO = -2 AND CLA_CONFIGURA IN (402, 403) ORDER BY CLA_CONFIGURA";
		
		$aRS = $theRepository->ADOConnection->Execute($sql);
		
		if($aRS === false)
		{
			die( translate("Error accessing")." SI_CONFIGURA ".translate("table").": ".$aRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		else 
		{
			$gsServerAD = "";
			$gsDominioAD = "";
			
			while (!$aRS->EOF){
       			switch (intval($aRS->fields['cla_configura'])){
       				case 402:
       					$gsServerAD = $aRS->fields['ref_configura'];
       					break;
       				case 403:
       					$gsDominioAD = $aRS->fields['ref_configura'];
       					break;
       			}
				$aRS->MoveNext();
		    }
		    
			$bOk = false;
		 	$sEspPwd = $thePassword;
		 	$sEspPwd = BITAMDecode($sEspPwd);
		 	$sEspPwd = BITAMDecryptPassword($sEspPwd);
		 	$nLen = strlen($sEspPwd);
		 	if (substr($sEspPwd,$nLen-5,5)=='CKPWD'){
				$dDate = date('Ymd');
				if (formatDateTime($dDate, "YYYY")==substr($sEspPwd,0,4)){
					$nLen = strlen($theUserName);
					if ($theUserName == strtoupper(substr($sEspPwd,4,$nLen))){
						if (formatDateTime($dDate, "MMDD")==substr($sEspPwd,4+$nLen,4)){
							$bOk = true;
							$_SESSION["PAthePassword"] = $thePassword;
						}
					}
				}
		 	}
		 	if (!$bOk ){
				require_once('ldap.php');
				
				if ($gsServerAD=='' | $gsDominioAD == ''){
					setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    			setcookie("PABITAM_UserName", $theUserName);
					exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=3".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
				}
				else{
					if (!ldapLogin($gsServerAD, $gsDominioAD, $theUserName, $thePassword)){
						setcookie("PABITAM_RepositoryName", $theRepositoryName);
		    			setcookie("PABITAM_UserName", $theUserName);
						exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=3".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
					}
				}				
				$_SESSION["PAthePassword"] =  GetADPwd($theUserName);
		 	}
		 }
	}
	elseif (BITAMDecryptPassword($theUser->Password) != $thePassword)
	{
		setcookie("PABITAM_RepositoryName", $theRepositoryName);
	    setcookie("PABITAM_UserName", $theUserName);
		exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=index.php?error=3".($goto == "" ? "" : "&goto=".$goto)."\">\n</head>\n<body>\n</body>\n</html>\n");
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	/*session_register("PABITAM_RepositoryName");
	//session_register("PABITAM_UserName");
	//session_register("PABITAM_UserRole");
	
	if(!array_key_exists("PABITAM_Password",$_SESSION))	
	{
		session_register("PABITAM_Password");
	}
	*/
	$_SESSION["PABITAM_Password"] = $theUser->Password;
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	/*session_register("PABITAM_UserID");
	//theUserID se utiliza en los archivos ya programados aún no se actualiza BITAM_UserID
	session_register("PAtheUserID");
	//theAppUserID es el ID que se manejan entre los usuarios de ESurvey
	session_register("PAtheAppUserID");
	session_register("PAgenerateLog");*/
	$_SESSION["PAgenerateLog"] = $generateLog;
	
	$_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
	$_SESSION["PABITAM_UserName"] = $theUser->UserName;
	$_SESSION["PABITAM_UserRole"] = $theAppUser->Role;
	
	$_SESSION["PABITAM_UserID"] = $theUser->UserID;
	$_SESSION["PAtheUserID"] = $theUser->UserID;
	$_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
	
	$_SESSION["PAvMenu"]=0;
	
	//@JAPR 2012-06-05: Agregado el código para accesar a FBM000 y así obtener algunos datos que se quedarán como variables de Sesión
	@getSAASDataByEMail($theRepository);
	//@JAPR
	
	//Variables de sesión que guardan los valores de los roles del usuario logueado
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("SV_ADMIN");
	//session_register("SV_USER");
	$_SESSION["SV_ADMIN"] = $userRoles["SV_Admin"];
	$_SESSION["SV_USER"] = $userRoles["SV_User"];
	
	setcookie("PABITAM_RepositoryName", $theRepository->RepositoryName);
	setcookie("PABITAM_UserName", $theUser->UserName);
	
	setcookie("PABITAM_UserID", $theUser->UserID);	
	setcookie("PAtheUserID", $theUser->UserID);	
	
	//@JAPR 2012-07-26: Agregado el uso de la versión recibida como parámetro para controlar los campos/tablas que son consultados de la metadata
	@setAppVersion();
	//@JAPR
	
	require_once("initialize.php");
	
	//Leer Versión de la metadata de Artus
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("PAArtusMDVersion");
	$_SESSION["PAArtusMDVersion"] = (float) $anArtusMDVersion;

	//@JAPR 2008-05-19: Extraido del Administrator Web
	//@EA 2005-06-22 Ahora obtenemos el tipo de repositorio
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("PABITAM_RepositoryType");
	//session_register("PABITAM_ImplementationSecurity");
	$_SESSION["PABITAM_RepositoryType"]=1;
	$_SESSION["PABITAM_ImplementationSecurity"]=0;
	$sql = "SELECT CLA_CONFIGURA, REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_USUARIO = -2 and CLA_CONFIGURA IN (603, 902)";
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false or $aRS->EOF)
	{
		$_SESSION["PABITAM_RepositoryType"]=1;
	}
	else
	{
		while (!$aRS->EOF)
		{
			$intClaConfigura = (int) $aRS->fields["cla_configura"];
			switch ($intClaConfigura)
			{
				case 603:
					$_SESSION["PABITAM_RepositoryType"] = (int) rtrim( $aRS->fields["ref_configura"] );
					break;
				case 902:
					$_SESSION["PABITAM_ImplementationSecurity"] = (int) rtrim( $aRS->fields["ref_configura"] );
					break;
			}
			$aRS->MoveNext();
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	/*
	//@JAPR 2008-11-20: Agregada la limpieza de archivos temporales de la carpeta ".\Log\"
	if(!array_key_exists("PAKeepTempFiles",$_SESSION))	
	{
		session_register("PAKeepTempFiles");
	}
	
	if(!array_key_exists("PAlogLink",$_SESSION))	
	{
		session_register("PAlogLink");
	}
	*/
	$_SESSION["PAKeepTempFiles"] = $keepTempFiles;
	$_SESSION["PAlogLink"] = "";


	//Establecer el lenguaje en una variable de sesión
	//0=DEFAULT, 1=Spanish, 2=English
	
	if($theUser->UserLanguageID<=0 || $theUser->UserLanguageID>8)
	{
		$userLanguageID = 2;
		$userLanguage = 'EN';
	}
	else
	{
		//@JAPR 2014-06-30: Agregado el soporte para multiples idiomas por usuario
		//Se había comentado esta asignación por lo que siempre se estaba cargando en inglés, así que se volverá a habilitar el uso del idioma del usuario
		$userLanguageID = $theUser->UserLanguageID;
		$userLanguage = $theUser->UserLanguageTerminator;
		//$userLanguageID = 2;
		//$userLanguage = 'EN';
		//@JAPR
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	/*session_register("PAuserLanguageID");
	session_register("PAuserLanguage");
	session_register("PAchangeLanguage");
	session_register("PAArtusWebVersion");
	session_register("PAFBM_Mode");*/
	$_SESSION["PAuserLanguageID"]=$userLanguageID;
	$_SESSION["PAuserLanguage"]=$userLanguage;
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
	$_SESSION["PAchangeLanguage"]=false;

	//Obtener la verión de Artus  Web a Utilizar
	$_SESSION["PAArtusWebVersion"] = 6.0;
	
	//Verificar si el producto esta trabajando sobre un repositorio de SAAS
	$_SESSION["PAFBM_Mode"] = 0;
	
	if($valueFBMMode==1)
	{
		$_SESSION["PAFBM_Mode"] = 1;
		setcookie("PAFBM_Mode", 1);
	}
	else 
	{
		setcookie("PAFBM_Mode", 0);
	}

	$url = $url."?LoadMainFrames=1";

	exit("<html>\n<head>\n<meta http-equiv=\"refresh\" content=\"0;url=".$url."\">\n</head>\n<body>\n</body>\n</html>\n");
}
if (array_key_exists("PABITAM_RepositoryName", $_COOKIE))
{
	$theRepositoryName = $_COOKIE["PABITAM_RepositoryName"];
}
else
{
	$theRepositoryName = "";
}
if (array_key_exists("PABITAM_UserName", $_COOKIE))
{
	$theUserName = $_COOKIE["PABITAM_UserName"];
}
else
{
	$theUserName = "";
}

require_once("initialize.php");

$sUserName = '';
$bOkLogin = true;
$bSSO=false;
if (isset($_REQUEST['SSO'])){
	if (isset($_SERVER['PHP_AUTH_USER']) | isset($_SERVER['AUTH_USER'])) {
		if (isset($_SERVER['PHP_AUTH_USER'])){
			$sUserName = $_SERVER['PHP_AUTH_USER'];
		}
		else{
			$sUserName = $_SERVER['AUTH_USER'];
		}
		while (! strpos($sUserName,"\\")===false){
			$x = strpos($sUserName,"\\");
			$sUserName = substr($sUserName,$x+1);
		}
		$sUserName = trim($sUserName);
		if ($sUserName!=''){
			$bSSO = true;
		}
		else{
			$bOkLogin = false;
		}
	}
	else{
		$bOkLogin = false;
	}
}

//@JAPR 2015-10-16: Modificada la redirección en caso de error para que dependa del tipo de servicio donde se encuentra
//Si se redireccionó a la página de index por algún error (lo cual estaría mal, pero era como estaba programado originalmente), entonces redirecciona a la página de login correspondiente
//al tipo de servicio heredando el mismo error recibido para de esa manera no pasar por el login de eForms
if (array_key_exists("error", $_GET)) {
	RedirectSessionConnectionError((int) @$_GET["error"]);
	exit();
}
//@JAPR
?>

<!-- saved from url=(0022)http://internet.e-mail -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?=translate("Home E-Survey")?></title>
<link href='favicon.ico' rel='shortcut icon' type='image/x-icon'/>
<link href='favicon.ico' rel='icon' type='image/x-icon'/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style type="text/css">
<!--
body,td,th {
	color: #000000;
}
body {
	background-color: #CADEE3;
}
.style1 {color: #9C936A}
.style2 {color: #FFFFFF}
.style3 {font-size: 16px}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; }
-->
</style>
<!--@OMMC 2015/09/04 Validación para prevenir ingreso por Safari -->
<!--script src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<script>
	$(document).ready(function() {
		if($.browser.safari == true){
			window.location = "browser_error.html"
		}
	});
</script-->
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
//-->

		// this page should never load inside of another frame
		if (top.location != self.location)
		{
			top.location = self.location;
		}
		
		function getDate()
		{
			var today, months, days;

			months = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
			days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado']
			today = new Date();
			
			dateString = days[today.getDay()] + " " + today.getDate() + " de " + months[today.getMonth()] + " de " + today.getFullYear();
			displayDate.innerHTML = dateString;
		}
		
		function showRepositories()
		{
			arrayInfo=window.showModalDialog('dialog.html', 'main.php?BITAM_SECTION=EKTRepositoryCollection', 'help: no; status: no; scroll: yes; resizable: yes; dialogHeight:465px; dialogWidth:720px;');
			//var strFeaturesWindow = "top=100, left=100, width=800, height=500, toolbar=no, menubar=no, location=no, directories=no";
			//objNewWindow = window.open("main.php?BITAM_SECTION=EKTRepositoryCollection", "Ektos", strFeaturesWindow);
			window.location.reload();
		}
</script>
	<!--<link rel="stylesheet" type="text/css" href="css/default.css">-->
	<!--<link rel="stylesheet" type="text/css" href="css/index.css">-->
</head>

<body onload="javascript: document.getElementById('LoginForm').BITAM_RepositoryName.focus();">
<table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="64" bgcolor="#336699">&nbsp;</td>
  </tr>
</table>
<table width="100%" height="93"  border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  <tr>
    <td><div id="Layer2" style="position:absolute; width:244px; height:28px; z-index:2; left: 638px; top: 135px; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;">
      <div align="right" class="style3"><?=translate("E-Survey")?></div>
    </div>      <span class="style2"><br>
      </span>
      <div id="Layer1" style="position:absolute; width:209px; height:91px; z-index:1; left: 137px; top: 80px;"><img src="images/LogoSurveys.jpg" width="239" height="83"></div>      </td>
  </tr>
</table>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="1">
  <tr>
    <td width="331" valign="top" bgcolor="#7DABB8"><p><img src="images/BarraDeg.gif" width="331" height="23"></p>
      <form id="LoginForm"  name="LoginForm"  method="post"  action="index.php<?= $goto == "" ? "" : "?goto=".$goto ?><?php
      		if(!isset($_GET["SSO"]))
      			echo "";
      		else 
      		{
      			if($goto == "")
      				echo "?SSO=1";
      			else 	
      				echo "&SSO=1";
      			
      		}
      	?>">
        <table width="315" border="0" align="center" cellpadding="0" cellspacing="0">
          <tr>
            <td width="65"><span class="style7"><?=translate("Project")?>:</span></td>
            <td width="250"><p>
             	<select name="BITAM_RepositoryName">
<?
	foreach ($BITAMRepositories as $aRepositoryName => $aRepository)
	{
?>
					<option value="<?= $aRepositoryName ?>" <?= $theRepositoryName == $aRepositoryName ? "selected" : "" ?>><?= $aRepositoryName ?></option>
<?
	}
?>
				</select>
				</p>
			</td>
          </tr>
          <tr>
            <td width="65">&nbsp;</td>
            <td width="250">&nbsp;</td>
          </tr>
          <tr>
          <td width="65"><span class="style7"><?=translate("User")?>:</span></td>
          <td width="250">
          <?php
	        	if ($bSSO){
	        		echo '<input type="text" size="25"  disabled value="'.$sUserName.'">'."\n";
	            	echo '<input name="BITAM_UserName" class="text" type="hidden" size="25"  value="'.$sUserName.'">'."\n";
	        	}
	        	else{
	    			echo  '<input type="text" name="BITAM_UserName" size="25" value="'.$theUserName.'">';
	           	}
          	?>
			</td>
          </tr>
          <tr>
            <td width="65">&nbsp;</td>
            <td width="250">&nbsp;</td>
          </tr>
          <tr>
            <td width="65">
            <?php
            	if(!$bSSO)
            	{
            ?>
            		<span class="style7"><?=translate("Password")?>:</span>
            <?php
            	}
            ?>
            	</td>
           		<td width="250">
           	<?php
                	if ($bSSO){
                		$sPassword = GetADPwd($sUserName);
						if (trim($sPassword)==''){
							$bOkLogin=false;
						}
						echo '<input  name="BITAM_Password"  value="'.$sPassword.'" type="hidden" size="25" >'."\n";
                	}
                	else{
                    	echo '<input  name="BITAM_Password" type="password" size="25" value = "">'."\n";
                    }
            ?>            
           		</td>
          </tr>
          <tr>
            <td width="65">&nbsp;</td>
            <td width="250">&nbsp;</td>
          </tr>

          <tr>
            <td width="" colspan="2">
            	<div align="center">
<?
	if(count($BITAMRepositories)<1)
	{
?>
            		<input name="addRepository" type="button" id="addRepository" value='<?=translate("Repositories")?>' onclick="showRepositories()">
<?
	}
?>	
	           		<input name="login" type="submit" id="login" value='<?=translate("Login")?>' <?=((!$bOkLogin)?'disabled':'')?>>
            	</div>
            </td>
          </tr>
<?
	if (array_key_exists("error", $_GET))
	{
		switch ($_GET["error"])
		{
			case 1:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("Repository does not exists.")?>
			</td>
		</tr>
<?
				break;
			case 2:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("Can not connect to Repository, please contact your System Administrator.")?>
			</td>
		</tr>
<?
				break;
			case 3:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("Your User Name does not exist or your Password is incorrect.")?>
			</td>
		</tr>
<?
				break;
			case 4:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("The page you are trying to access requires authorization.")?>
			</td>
		</tr>
<?
				break;
			case 5:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("The page you are trying to access does not exist.")?>
			</td>
		</tr>
<?
				break;
			case 6:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("You are not authorized to view the page your are trying to access.")?>
			</td>
		</tr>
<?
				break;
			case 8:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("Error checking the BITAM Artus Metadata version.")?>
			</td>
		</tr>
<?
				break;
			case 9:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("The BITAM Artus Metadata version is older than the required version to run this program (%s), please update your repository.", ARTUS_MD_REQUIRED_VERSION)?>
			</td>
		</tr>
<?
				break;
			case 10:
?>
		<tr>
		    <td class="style7" id="LoginMessage" colspan="2">
				<?=translate("You have more than one open session in this web browser. Please shut down your web browser and re-attempt sign in.")?>
			</td>
		</tr>
<?
				break;
			default:
?>
		<tr>
		    <td class="style7" id="LoginMessage">
		    	<?=translate("System error %s found, please contact your System Administrator.", $_GET["error"])?>
			</td>
		</tr>
<?
				break;

		}
	}
?>
        </table>
    </form>      <p>&nbsp;</p></td>
    
    <td width="469" bgcolor="#FFFFFF"><p align="right"><img src="images/Pict-Surveys.jpg" width="466" height="246"></p>    </td>
  </tr>
</table>
<table width="803" border="0" align="center" cellpadding="0" cellspacing="2" bgcolor="#FFFFFF">
  <tr>
    <td width="331" bgcolor="#FFFFFF">&nbsp;</td>
    <td bgcolor="#999999">&nbsp;</td>
    <td bgcolor="#4D5B64">&nbsp;</td>
  </tr>
</table>
<table width="800" border="0" align="center" cellpadding="0" cellspacing="2">
  <tr>
    <td width="570">&nbsp;</td>
    <td width="230"><div align="center"><img src="images/LogoBitam-fa.gif" ></div></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>