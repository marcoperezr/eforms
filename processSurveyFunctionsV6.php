<?php
/* Esta función realiza organiza la información invocando al grabado de datos según los tipos de secciones, además de procesar las acciones de eBavel que 
se generaron durante el proceso de grabado de los registros en las tablas de eForms
Las formas a partir de esta versión ya no se encuentran ligados a un cubo, así que el key generado automáticamente corresponde con el key de la captura únicamente,
además no existen múltiples registros en la tabla de datos principales de eForms (la tabla de la forma), sino que cada tabla de sección es la que contiene
la información múltiple si está configurada de dicha manera
*/
/* MAPR 2017-08-03: Se agrega el requerimiento del archivo que necesitamos cargar el formato excel correspondiente a KPIForms para crear un data destination de correo con el formato de excel por el que no se envían los datos capturados en Forms a Ebavel ni se manda el correo que se esta requiriendo. */
require_once('../phpexcel/Classes/PHPExcel.php');
require_once 'eFormsDataDestinations.inc.php';
function saveAnswersMobileV6() {
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("function saveAnswersMobileV6", 1, 1, "color:purple;");
	}
	
    global $server;
    global $server_user;
    global $server_pwd;
    global $masterdbname;
    global $BITAMRepositories;
    global $generateLog;
	//@JAPR 2012-08-13: Agregada las dimensiones con la fecha y hora de sincronización (campos SynDateDimID y SyncTimeDimID)
	global $syncDate;
	global $syncTime;
	//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
	global $dieOnError;
	//@JAPR 2012-10-29: Corregido un bug, las fotos de secciones Maestro-Detalle no se estaban grabando con referencia al registro
	global $arrMasterDetSections;
	global $arrMasterDetSectionsIDs;
	//@JAPR 2014-05-28: Agregado el tipo de sección Inline
	global $arrInlineSections;
	global $arrInlineSectionsIDs;
	//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
	global $eBavelAppCodeName;
	global $arrayPDF;
	global $arrActionItems;
	global $arrNumericQAnswersColl;
	//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
	global $strPDFFileNamePrefix;
	global $intPDFFilaNamePrefixCntr;
	//@JAPR 2013-04-30: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	global $jsonData;
	//@JAPR 2013-05-03: Agregado el mapeo de campos de eBavel para actualización de datos entre productos
	global $usePhotoPaths;
	global $blnWebMode;
	global $useRecoveryPaths;
	//@JAPR 2013-07-17: Agregado el archivo log de procesamiento de acciones
	global $blnSaveeBavelLogStringFile;
	//@JAPR
	//@MABH20121205
	global $appVersion;
	//@JAPR 2015-02-27: Agregado el Monitor de Operaciones
	global $gbUseOperationsMonitor;
	global $giOperationID;	
	//@JAPR 2015-07-24: Modificado el cubo global de surveys para adaptarlo a V6
	global $arrGPSData;
	if (!isset($arrGPSData)) {
		$arrGPSData = array();
	}
    //Modificamos variable global $gblShowErrorSurvey para que no realice los die y que devuelva los errores
    global $gblShowErrorSurvey;
    $gblShowErrorSurvey = false;
	
	//Variable global que contiene el id de la tarea a reprocesar
	global $taskIDRePro;
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("function saveAnswersMobileV6 taskIDRePro:".$taskIDRePro, 1, 1, "color:purple;");
	}
	global $strDestinationIDs;
	$strDestinationIDs="";
	//Arreglo de los IDs de destinos que se reprocesarán
	global $repDestinationIDs;
	$repDestinationIDs = array();
	global $reprocess;
	$reprocess=0;
	global $saveDataForm;
	$saveDataForm = true;
	//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
	global $dataDestinationsCol;
	global $dataDestinationsComposite;
	$dataDestinationsComposite = '';
	//OMMC 2019-04-12: Modificada la instrucción del PhantomJS para que vuelva a resolver mapas
	global $gbKPIFormsService;
	
	//@JAPR 2016-08-12: Corregido el mapeo de preguntas firma/sketch hacia eBavel, para estas preguntas lo que llega en la propiedad answer.photo es un B64 que no se graba localmente
	//como archivo de imagen en la carpeta de sincronización, sino que se graba directo en la carpeta de imágenes de las capturas ya procesadas, asó que al mandar a eBavel la imagen,
	//lo cual hacía enviando la propiedad answer.photo como si fuera una ruta de imagen, fallaba el copiado. Ahora se sobreescribirá en el $_POST un array por pregunta al momento de
	//generar la imagen para almacenar el nombre de archivo con el que quedó y usar eso durante el proceso de mapeo hacia eBavel (#4L5RSC)
	//Se indexará por QuestionID, seguido por #registro (por si es una sección multi-registro, 0 en caso de ser sección estándar), ejemplo:
	//$_POST["questionB64Images"][8241][0] = c:\inetpub\...\survey_665\photo_665_8241_5_5.jpg
	//Dado a que este proceso se invoca por captura, no hay necesidad de hacer referencia al valor 5 del ejemplo de arriba (que representa el ID de captura) porque siempre se resetea
	//este array, así que en todo momento se hace referencia a la única captura que está procesando. En el ejemplo de arriba no es multi-registro (no va otro número después del 5 y
	//antes del .jpg) así que por eso se usa el registro 0
	$_POST["questionB64Images"] = array();
	//@JAPR
	
	if($taskIDRePro>0)
	{
		$aBITAMConnection = @connectToKPIRepository(); 
		if ($aBITAMConnection)
		{
			$sqlTask = "SELECT DestinationIDs, Reprocess FROM SI_SV_SurveyTasks WHERE SurveyTaskID= {$taskIDRePro}";
			$aRS = $aBITAMConnection->ADOConnection->Execute($sqlTask);
			if ($aRS && !$aRS->EOF)
			{
				$strDestinationIDs = $aRS->fields["destinationids"];
				$reprocess = (int) $aRS->fields["reprocess"];
				if($reprocess)
				{
					if($strDestinationIDs!="")
					{
						$repDestinationIDs = explode(",", $strDestinationIDs);
					}
				}
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("strDestinationIDs:".$strDestinationIDs, 1, 1, "color:purple;");
					ECHOString("reprocess:".$reprocess, 1, 1, "color:purple;");
				}
			}
		}
	}
	if($reprocess)
	{
		//Si se está reprocesando destinos entonces no se grabarán los datos de la forma
		$saveDataForm=false;
	}
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDataForm: ".(int)$saveDataForm, 1, 1, "color:purple;");
	}
	
	//Este es el punto inicial de grabado de las capturas, así que aquí asigna el valor de la fecha/hora de sincronización
	$syncDate = date("Y-m-d");
	$syncTime = date("H:i:s");
	
	$blnDebugSave = getParamValue('debugSave', 'both', "(boolean)");
	if ($blnDebugSave)
	{
		ob_start();
		ECHOString('$POST = ');
		var_export($_POST);
		ECHOString(";");
		ECHOString('$GET =');
		var_export($_GET);
		ECHOString(";");
	    $strData = trim(ob_get_contents());
		ob_end_clean();
  		$statlogfile = "./uploaderror/ESurvey_SyncData_".date('YmdHis').".log";
	  	@error_log($strData, 3, $statlogfile);
	    header("Content-Type: text/plain");
	    ECHOString("Data file was stored in the server");
	    exit();
	}
	elseif (false)
	{
		//Activar sólo si se quiere dejar registro de los datos subidos por los encuestadores, se puede incluso condicionar a sólo ciertas cuentas
		//Es similar al parámetro debugSave con la excepción de que no manda la salida de datos al buffer sino que lo maneja todo como variables
		//para simplemente grabarlas en un archivo y atrapa todos los errores para no interrumpir el proceso de grabado
		try
		{
			$kpiUser = '';
		    if (isset($_POST['UserID']))
		    {
			    $kpiUser = $_POST['UserID'];
		    }

			$thisDate = date("Y-m-d H:i:s");
			if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
			{
				$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
			}
			$thisDate = str_replace(':', '', $thisDate);
			$thisDate = str_replace('-', '', $thisDate);
			$thisDate = str_replace(' ', '', $thisDate);

			$agendaID = 0;
			if (array_key_exists("agendaID", $_POST))
			{
				$agendaID = (int) $_POST["agendaID"];
			}
			
		    $surveyID = 0;
		    if (array_key_exists("surveyID", $_POST)) {
		        $surveyID = (int) $_POST["surveyID"];
		    }
		    
		    if (trim($kpiUser) != '' && $thisDate != '' && $surveyID > 0)
		    {
		    	$strData = "";
				$strData .= '$POST = '.var_export($_POST, true).";\r\n";
				$strData .= '$GET ='.var_export($_GET, true).";\r\n";
			    
		  		$statlogfile = "./uploaderror/SyncData_".$kpiUser."_Ag".$agendaID."_Sv".$surveyID."_Dte".$thisDate.".log";
			  	@error_log($strData, 3, $statlogfile);
		    }
		}
		catch (Exception $e)
		{
			//No es necesario reportar este error
		}
	}
	$statlogfile = "./uploaderror/eforms.log";
	$friendlyDate = date('Y-m-d H:i:s');
	
	$thisDate = date("Y-m-d H:i:s");

	if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
	{
		$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
	}
	
	if(isset($_POST["surveyDate"]) && isset($_POST["surveyHour"]))
	{
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
	} else {
		$lastDate = $thisDate;
	}
	
	$stat_action = "[".$friendlyDate."] [Action: Connect Saving Answers Service First Step] [UserID - Email: ".@$_POST['UserID']."] [SurveyID: ".@$_POST["surveyID"]."] [SurveyDate: ".$lastDate."]\r\n";
	@error_log($stat_action, 3, $statlogfile);

    if (!isset($_POST['UserID'])) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString('You must enter a valid UserID and Password to access this resource');
        exit();
    }
    $kpiUser = $_POST['UserID'];

    if (!isset($_POST['Password'])) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString('You must enter a valid UserID and Password to access this resource');
        exit();
    }
    $kpiPass = $_POST['Password'];
	
    //Se obtiene el nombre corto del usuario de KPI asi como el nombre del repositorio
    $strPrjAndUser = getKPAProjectAndUser($kpiUser, $kpiPass);
	//@JAPR 2019-09-24: Modificados los mensajes de error para no mostrar SQL y dar información mas útil (#RGA23E)
	if ( stripos($strPrjAndUser, '_KPASep_') === false ) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString($strPrjAndUser);
        exit();
    }
	
    $arrayPrjUsr = explode("_KPASep_", $strPrjAndUser);
    $strRepositoryName = $arrayPrjUsr[0];
    $strUserName = $arrayPrjUsr[1];
	
    $theRepositoryName = $strRepositoryName;
    $theUserName = $strUserName;
    $thePassword = $kpiPass;
	
    if (!array_key_exists($theRepositoryName, $BITAMRepositories)) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString("Project does not exists.");
        exit();
    }
	
    $theRepository = $BITAMRepositories[$theRepositoryName];
    if (!$theRepository->open()) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString(translate("Can not connect to Repository, please contact your System Administrator."));
        exit();
    }
	
	//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
    $theUser = BITAMeFormsUser::WithUserName($theRepository, $theUserName);
    if (is_null($theUser)) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString("Your User Name does not exist or your Password is incorrect.");
        exit();
    }
	
    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
    if (is_null($theAppUser)) {
        header('HTTP/1.0 401 Unauthorized');
        ECHOString("Your User Name is not registered in the application.");
        exit();
    }

	//@JAPR 2012-12-10: Corregido un error en la generación de archivos temporales para envio de PDFs
	//Asigna el prefijo que se utilizará como nombre de archivo de PDF
	//@JAPR 2013-02-26: Corregido un bugm, se movió este prefijo a esta parte porque estaba concatenando la cuenta de correo en lugar del CLA_USUARIO
	$strPDFFileNamePrefix = ($theAppUser->UserID).'_'.((string) @$_POST["surveyID"]).'_'.$lastDate;
	if ($lastDate != $thisDate) {
		$strPDFFileNamePrefix .= '_'.$thisDate;
	}
	$strPDFFileNamePrefix = str_replace(array('-', ':', ' '), '', $strPDFFileNamePrefix);
	//@JAPR
    
    //Establecer el lenguaje en una variable de sesión
    //0=DEFAULT, 1=Spanish, 2=English
    if ($theUser->UserLanguageID <= 0 || $theUser->UserLanguageID > 8) {
        $userLanguageID = 2;
        $userLanguage = 'EN';
    } else {
        $userLanguageID = $theUser->UserLanguageID;
        $userLanguage = $theUser->UserLanguageTerminator;
    }

    //Verificar si se encuentra en modo FBM
    $sqlFBM = "SELECT REF_CONFIGURA FROM SI_CONFIGURA WHERE CLA_CONFIGURA = 527 AND CLA_USUARIO = -2";

    $aRSFBM = $theRepository->ADOConnection->Execute($sqlFBM);

    $valueFBMMode = 0;

    if ($aRSFBM && !$aRSFBM->EOF) {
        if ($aRSFBM->fields["ref_configura"] !== null && trim($aRSFBM->fields["ref_configura"]) !== "") {
            $valueFBMMode = (int) trim($aRSFBM->fields["ref_configura"]);
        }
    }
	
    $_SESSION["PABITAM_RepositoryName"] = $theRepository->RepositoryName;
    $_SESSION["PABITAM_UserName"] = $theUser->UserName;
    $_SESSION["PABITAM_UserRole"] = $theUser->Role;
    $_SESSION["PABITAM_Password"] = $theUser->Password;
    $_SESSION["PABITAM_UserID"] = $theUser->UserID;
    $_SESSION["PAtheUserID"] = $theUser->UserID;
    $_SESSION["PAtheAppUserID"] = $theAppUser->UserID;
    $_SESSION["PAgenerateLog"] = $generateLog;
    $_SESSION["SV_ADMIN"] = $theAppUser->SV_Admin;
    $_SESSION["SV_USER"] = $theAppUser->SV_User;
    $_SESSION["PAuserLanguageID"] = $userLanguageID;
    $_SESSION["PAuserLanguage"] = $userLanguage;
    $_SESSION["PAchangeLanguage"] = false;
    $_SESSION["PAArtusWebVersion"] = 6.0;
    $_SESSION["PAFBM_Mode"] = $valueFBMMode;

    require_once("processsurveyfunctions.php");
	require_once("surveyActionMap.inc.php");
	require_once("questionActionMap.inc.php");
	require_once("questionOptionActionMap.inc.php");

    $surveyID = 0;
    if (array_key_exists("surveyID", $_POST)) {
        $surveyID = $_POST["surveyID"];
    }

	//@JAPR 2012-03-14: Agregado el grabado del Status de las Agendas
	$agendaID = 0;
	if (array_key_exists("agendaID", $_POST))
	{
		$agendaID = (int) $_POST["agendaID"];
	}
	//@JAPR
    
	//@MABH20121205
	if($appVersion < esvServerDateTime) {
		if(!isset($_POST["serverSurveyDate"])) {
			$_POST["serverSurveyDate"] = "";
		}
		if(!isset($_POST["serverSurveyHour"])) {
			$_POST["serverSurveyHour"] = "";
		}
		if(!isset($_POST["serverSurveyEndDate"])) {
			$_POST["serverSurveyDate"] = "";
		}
		if(!isset($_POST["serverSurveyEndHour"])) {
			$_POST["serverSurveyEndHour"] = "";
		}
	}
	
    $arrayData = $_POST;
    foreach ($arrayData as $key => $value) {///////AQUI
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
            	//@JAPR 2013-04-10: Agregado un nivel mas de conversión de arrays, si se requiere otro ya mejor cambiar a llamadas recursivas
            	if (is_array($value1)) {
            		foreach ($value1 as $key2 => $value2) {
            			$arrayData[$key][$key1][$key2] = $arrayData[$key][$key1][$key2];
            		}
            	}
            	else {
                	$arrayData[$key][$key1] = $arrayData[$key][$key1];
            	}
            	//@JAPR
            }
        } else {
            $arrayData[$key] = $arrayData[$key];
        }
    }/////AQUI
    
    //Agregamos el usuario de artus y de la aplicacion logueado en el arreglo $arrayData
    $arrayData["userID"] = $_SESSION["PABITAM_UserID"];
    $arrayData["appUserID"] = $_SESSION["PAtheAppUserID"];
	
    require_once('survey.inc.php');
    $surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);

    if (is_null($surveyInstance)) {
        $strStatus = "Survey data was saved successfully.";
        header("Content-Type: text/plain");
        ECHOString($strStatus);
        exit();
    }
	
	//@JAPR 2012-05-23: Agregado el log de proceso de los móviles a una tabla
	//Se movió $logdate para poder usarla durante la generación del log. Sólo se registra un error de log por sesión
	$logdate = date('YmdHis');
	$blnFirstLogError = true;
	$strLogStatus = '';
	$factKeyDimVal = 0;
	
	//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
	//varios requests al mismo tiempo
	$blnIsAgent = getParamValue('isagent', 'both', "(int)", false);
	if ($blnIsAgent || ($handle = @fopen("./uploaderror/forms_" . $theRepository->RepositoryName . "_" . $theUser->UserID . ".lck", "w"))) {
        if ($blnIsAgent || @flock($handle, LOCK_EX)) {
			$sql = "SELECT DateID FROM {$surveyInstance->SurveyTable} 
				WHERE DateID = ".$theRepository->DataADOConnection->DBTimeStamp($lastDate)." AND UserID = ".$arrayData["userID"];
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Revisar si se repite la captura:", 1, 1, "color:purple;");
				ECHOString($sql, 1, 1, "color:purple;");
			}
			
			if ($aRS->EOF) {
				$repeatedDate = false;
			} else {
				$repeatedDate = true;
			}
			
			if(!$repeatedDate || $reprocess) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Entra a IF de grabado de forma", 1, 1, "color:purple;");
				}
				
				//@JAPR 2012-05-23: Agregado el log de proceso de los móviles a una tabla
				if (isset($arrayData['logMobile'])) {
					//@JAPR 2012-06-14: No estaba haciendo el explode por <enter>
					$arrEntries = explode("\r\n", $arrayData['logMobile']);
					$arrLogLine = array();
					//Recorre cada línea del log y al encontrar una cuyo contenido es el texto "***" inserta los campos en la tabla del Log
					foreach ($arrEntries as $strLine) {
						if ($strLine == '***') {
							try {
								$arrLogLine[] = 'survey='.$surveyID;
								$arrLogLine[] = 'agenda='.$agendaID;
								$strLogStatus = insertAppLogLine($theRepository, $arrLogLine);
							}
							catch (Exception $e) {
								$strLogStatus = $e->getMessage();
							}
							
							//En caso de error durante el grabado del log en la tabla, registra la entrada en el propio log de grabado en archivo
							if ($strLogStatus != '' && $blnFirstLogError) {
								$blnFirstLogError = false;
								$thelog = "[".$friendlyDate."] [Action: SaveLog error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving mobile app log entry: " . $strLogStatus . " POST = " . print_r($_POST,true);
								$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
								@error_log($thelog, 3, $thefile);
								
								$stat_action = "[".$friendlyDate."] [Action: SaveLog error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
								@error_log($stat_action, 3, $statlogfile);
							}
							$arrLogLine = array();
						}
						else {
							$arrLogLine[] = $strLine;
						}
					}
				}
				
		        //@JAPR 2012-04-12: Agregadas las secciones Maestro-Detalle
		        $hasMasterDetSections = false;
		        $arrMasterDetSections = BITAMSection::existMasterDetSections($theRepository, $surveyID, true);
		        if (is_array($arrMasterDetSections) && count($arrMasterDetSections) > 0) {
					//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        	$arrMasterDetSectionsIDs = array_flip($arrMasterDetSections);
		        	
		        	//Verifica si realmente se capturaron preguntas de la sección de Maestro detalle, ya que si no es así entonces simplemente
		        	//se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
		        	//por lo que no habría problema en procesarlas
		        	for($i = 0; $i < count($arrMasterDetSections); $i++) {
		        		$intSectionID = $arrMasterDetSections[$i];
		        		$postKey = "masterSectionRecs".$intSectionID;
		        		if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0)) {
		        			//Con una sola sección que si capturara un Maestro - Detalle, se asume que si vienen preguntas como array por lo que
		        			//se usará el método alterno de grabado
		        			$hasMasterDetSections = true;
		        		}
		        		else {
		        			//Si esta sección siendo un Maestro - Detalle NO capturó ningún conjunto de respuestas, en el mejor de los casos
		        			//llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
		        			//pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
		        			//(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
		        			$objQuestionsColl = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				            foreach ($objQuestionsColl->Collection as $questionElement) {
				            	$postKey = "qfield".$questionElement->QuestionID;
				            	if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey])) {
				            		//En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
				            		//"masterSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
				            		//no se conocía el concepto de Maestro - Detalle, en cuyo caso no debió haber llegado entonces un Array sino
				            		//las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
				            		//con el is_array también
				            		$arrayData[$postKey] = '';
				            	}
				            }
		        		}
		        	}
		        }
		        else {
		        	$arrMasterDetSections = array();
		        	$arrMasterDetSectionsIDs = array();
		        }
		        
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//@JAPR 2015-07-08 Agregado el soporte de Formas v6 con un grabado completamente diferente
				//Se desaparece el concepto de sección dinámica, ahora las secciones dinámicas son secciones Inline configuradas sin selector y en modo de
				//paginación, de tal manera que permiten mostrarse tal como lo hacía una dinámica así como tener tantas secciones como se desee de diferentes
				//catálogos si así se desea. Estas secciones disfrazadas pudieran o no depender de alguna pregunta, eso es decisición de quien la configure
		        $hasInlineSections = false;
		        $arrInlineSections = BITAMSection::existInlineSections($theRepository, $surveyID, true);
		        if (is_array($arrInlineSections) && count($arrInlineSections) > 0) {
					//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        	$arrInlineSectionsIDs = array_flip($arrInlineSections);
		        	
		        	//Verifica si realmente se capturaron preguntas de la sección Inline, ya que si no es así entonces simplemente
		        	//se debe hacer un grabado normal pues ninguna de las preguntas cuyas respuestas deberían ser un array vendrían asignadas,
		        	//por lo que no habría problema en procesarlas
		        	for($i = 0; $i < count($arrInlineSections); $i++) {
		        		$intSectionID = $arrInlineSections[$i];
		        		$postKey = "inlineSectionRecs".$intSectionID;
		        		if ((isset($arrayData[$postKey]) && !is_array($arrayData[$postKey]) && (int) $arrayData[$postKey] > 0) || (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]) && (int) count($arrayData[$postKey]) > 0)) {
		        			//Con una sola sección que si capturara una Inline, se asume que si vienen preguntas como array por lo que
		        			//se usará el método alterno de grabado
		        			$hasInlineSections = true;
		        		}
		        		else {
		        			//Si esta sección siendo una Inline NO capturó ningún conjunto de respuestas, en el mejor de los casos
		        			//llegó un Array vacio como parámetro para sus preguntas, o en el peor de los casos ni siquiera llegó el parámetro,
		        			//pero si llegó entonces se limpia para no causar conflicto colocando el valor de No Aplica según el tipo de pregunta
		        			//(por lo pronto se dejará en blanco, la validación real se hará al momento de armar el array para grabar)
		        			$objQuestionsColl = BITAMQuestionCollection::NewInstanceBySection($theRepository, $intSectionID);
				            foreach ($objQuestionsColl->Collection as $questionElement) {
				            	$postKey = "qfield".$questionElement->QuestionID;
				            	if (isset($arrayData[$postKey]) && is_array($arrayData[$postKey]))
				            	{
				            		//En teoría no deberían llegar datos para las preguntas de esta sección si no viene el correspondiente campo
				            		//"inlineSectionRecs", si eso llega a pasar es porque se está intentando grabar en una versión previa donde
				            		//no se conocía el concepto de Inline, en cuyo caso no debió haber llegado entonces un Array sino
				            		//las respuestas directas, si se pudiera dar ese caso entonces se dejarán dichas respuestas, por eso se valida
				            		//con el is_array también
				            		$arrayData[$postKey] = '';
				            	}
				            }
		        		}
		        	}
		        }
		        else {
		        	$arrInlineSections = array();
		        	$arrInlineSectionsIDs = array();
		        }
		        
				//@JAPR 2015-07-25: Modificado el cubo global de surveys para adaptarlo a V6
				//Carga la información del GPS para basado en la posición de la captura. Este proceso se realiza siempre porque ahora esta información es parte del cubo global de Survey,
				//así que ya no depende de una bandera activada sólo por el proceso de acciones
				//@JAPR 2018-08-20: Debido al alto consumo de llamadas al API de Geocoding en eForms, DAbdo autorizó a ya NO traducir las posiciones GPS
				//a direcciones para grabarlas como parte de los datos de cada captura, cubos ni poder mandarlas a ningún destino (#0MESM9)
				//@JAPR 2018-12-05: Se determinó que el uso de Geocoding de Google Maps desde el server no era el problema, así que se regresa esta funcionalidad (#09FEOR)
				if ( true ) {
					try {
						if(isset($arrayData['surveyLatitude'])) 
						{
							$latitude = (double)$arrayData['surveyLatitude'];
						}
						else 
						{
							$latitude = 0;
						}
						
						if(isset($arrayData['surveyLongitude'])) 
						{
							$longitude = (double)$arrayData['surveyLongitude'];
						} 
						else 
						{
							$longitude = 0;
						}
						
						//Verifica si se trata de la pregunta especial con valor dummy para pruebas
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Checking special GPS data");
							ECHOString("Repository: ".((string) @$_SESSION["PABITAM_RepositoryName"]));
							ECHOString("SurveyID: ".((int) @$surveyInstance->SurveyID));
							ECHOString("Question 32 data: ".((string) @$arrayData['qfield32']));
						}
						//@logeBavelString("Checking special GPS data");
						//@logeBavelString("Repository: ".((string) @$_SESSION["PABITAM_RepositoryName"]));
						//@logeBavelString("SurveyID: ".((int) @$surveyInstance->SurveyID));
						
						if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
							$strMessageQuestion = trim((string) @$arrayData['qfield32']);
							if ($strMessageQuestion != '') {
								$arrData = explode(',', $strMessageQuestion);
								if (count($arrData) == 2) {
									$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
									$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
									if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
										$latitude = $strLatitude;
										$longitude = $strLongitude;
									}
								}
							}
						}
						
						//OMMC 2018-12-17: Proceso de obtención de coordenadas para el administrador usando mapkit y phantom
						$mapSetting = @BITAMSettingsVariable::NewInstanceWithName($theRepository, 'MAPTYPE');
						$intMapType = maptGoogle;
						if (!is_null($mapSetting) && trim($mapSetting->SettingValue != '')) {
							$intMapType = (int) @$mapSetting->SettingValue;
							if ( $intMapType < maptGoogle || $intMapType > maptApple ) {
								$intMapType = maptApple;
							}
						}
						switch($intMapType) {
							case maptApple:
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\nRetrieving GPS Data using Phantom for latitude $latitude, longitude $longitude");
								}
								
								//Variables del proceso. Ajustar a conveniencia.
								//OMMC 2019-04-12: Modificada la instrucción del PhantomJS para que vuelva a resolver mapas
								$commandLine = 'phantomjs --web-security=no --ignore-ssl-errors=true --ssl-protocol=any mapkitGeocoding.js "service='.$gbKPIFormsService.'" "timeout='.mapkitTimeout.'" "latitude='.$latitude.'" "longitude='.$longitude.'"';
								$intSleepTime = mapkitSleepLength;
								$intWhile = 0;
								$intCapWhile = mapkitWaitLength;
								$output = "";
								$pipesIn = array(
									0 => array('pipe', 'r'), // 0 is STDIN for process
									1 => array('pipe', 'w'), // 1 is STDOUT for process
								);
								$pipesOut = array();
								
								//Función para matar el proceso en el CMD, diferente para sistemas operativos de windows y unix
								//Mata también subprocesos inicializados por el proceso padre, requiere permisos de administrador
								//Programada como alternativa al proc_terminate ya que esta función de PHP en realidad no termina los procesos en windows
								function killProcessID($ppid){ 
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										echo("<br>\r\n".date("Y-m-d H:i:s")." Matando el proceso: " . $ppid);
									}
									return stripos(php_uname('s'), 'win')>-1  ? exec("taskkill /pid ".$ppid." /t /f") : exec("kill -9 $ppid");
								}
								
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\n".date("Y-m-d H:i:s")." Proceso para el comando: " . $commandLine);
									echo("<br>\r\n".date("Y-m-d H:i:s")." Abriendo el proc... ");
								}
								
								$proc = @proc_open($commandLine, $pipesIn, $pipesOut, getcwd(), NULL);
								$procStatus = @proc_get_status($proc);
								
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\n".date("Y-m-d H:i:s")." Proc abierto... ");
									echo("<br>\r\n ProcessID: " . $procStatus['pid']);
									echo("<br>\r\n".date("Y-m-d H:i:s")." Cerrando pipes... ");
								}
								if (isset($pipesOut[0])) @fclose($pipesOut[0]);

								//Ciclamos mientras el proceso está corriendo
								while( ($intWhile < $intCapWhile) && proc_get_status($proc)['running']) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										echo("<br>\r\n".date("Y-m-d H:i:s")." Resultado ". $intWhile ." de read en el ciclo ");
									}
									sleep($intSleepTime);
									$intWhile++;
								}
								
								//Evaluar la salida si el contador llegó al máximo pero el proceso aún sigue corriendo
								if ($intWhile == $intCapWhile && proc_get_status($proc)['running']) {
									killProcessID($procStatus['pid']);
								} else { //De lo contrario sólo se obtiene el output
									$output = stream_get_contents($pipesOut[1]);
								}
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\n".date("Y-m-d H:i:s")." Contenido del output: " . $output);
									echo("<br>\r\n".date("Y-m-d H:i:s")." Cerrando pipe 1 y cerrando el proc ");
								}
								if (isset($pipesOut[1])) @fclose($pipesOut[1]);
								@proc_close($proc);
								if ($output) {
									if ( stripos($output, "error") ) {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("Error retrieving GPS Data: " );
										}
									} else if ( stripos($output, "formattedAddress") ) {
										$arrGPSData = @getGPSDataFromAppleJson($output);
										if (is_null($arrGPSData) || !is_array($arrGPSData)) {
											$arrGPSData = array();
										}
										
										if (is_array($arrGPSData)) {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												echo("<br>\r\nGPS Data: ");
												PrintMultiArray($arrGPSData);
											}
											if (@$blnSaveeBavelLogStringFile) {
												$strLogText = (string) @var_export(@$arrGPSData, true);
											}
										}
									}
								} else {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("Error retrieving GPS Data: ".(string) $output);
									}
								}
							break;
							case maptGoogle:
							default:
								//@JAPR 2018-03-02: Corregido un bug, faltaba el API Key de google en las llamadas para usar el API, así que excedía el límite de peticiones (#KRZ34J)
								//$strGPSURL = "http://maps.google.com/maps/api/geocode/json?sensor=false".googleAPIKey."&language=es&latlng=$latitude,$longitude";
								//Se cambia el APIKey que era usada para móviles (googleAPIKey) por la versión para llamadas http (googleServerAPIKey), además necesita https
								//@JAPR 2019-01-18: Modificadas nuevamente las llaves, la última que se había asignado era solo para Geocoding así que los mapas dejaron de funcionar, ahora se generará una llave exclusiva
								//para Geocoding (solo servidor) mientras que las otras dos serán para mapas (Servidor y App de captura) (#J1MXYY)
								$strGPSURL = "https://maps.google.com/maps/api/geocode/json?sensor=false".googleServerGeocodingAPIKey."&language=es&latlng=$latitude,$longitude";
								//@JAPR
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\nRetrieving GPS Data for: $strGPSURL");
								}

								$ch = curl_init(); 
								if ($ch) {
									curl_setopt($ch, CURLOPT_URL, $strGPSURL); 
									curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
									curl_setopt($ch, CURLOPT_TIMEOUT, 10); 
									$strJSON = curl_exec($ch);
									if ($strJSON) {
										curl_close($ch);
										
										//Convierte la respuesta a un objeto para manipular el resultado
										$arrGPSData = @getGPSDataFromJson($strJSON);
										if (is_null($arrGPSData) || !is_array($arrGPSData)) {
											$arrGPSData = array();
										}
										
										if (is_array($arrGPSData)) {
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												echo("<br>\r\nGPS Data: ");
												PrintMultiArray($arrGPSData);
											}
											if (@$blnSaveeBavelLogStringFile) {
												$strLogText = (string) @var_export(@$arrGPSData, true);
											}
										}
									} else {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											echo("<br>\r\nError retrieving GPS Data: ".(string) @curl_error($ch));
										}
										curl_close($ch);
									}
								}
							break;
						}//OMMC
					} catch (Exception $e) {
						//Por el momento no reportará este error
						$strGPSException = (string) @$e->getMessage();
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Error retrieving GPS Data: ".$strGPSException);
						}
						//@logeBavelString("Error retrieving GPS Data: ".$strGPSException);
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("No GPS Data will be used");
					}
				}
				//@JAPR
				
				
				//Originalmente por la manera en que se tenían que grabar las diferentes secciones, se respaldaba el array de datos recibidos para restaurarlo
				//antes de iniciar el post-procesamiento (posterior al grabado en las tablas de datos de eForms), sin embargo ahora todas las secciones multi-registro
				//se graban en el mismo proceso independientemente de su tipo, ya que todas tienen el mismo comportamiento, así que se seguirá respaldando y
				//restaurando este array sólo por cuestiones de seguridad, aunque debería ser innecesario totalmente dicho proceso
				$arrayDataOrig = $arrayData;
				
				//Esta variable contendrá el Key asignado a esta captura de encuesta para el único registro en la tabla de la forma, ya que a partir de esta
				//versión cada sección multi-registro grabará en una tabla exclusiva para ella, lo mismo las preguntas múltiple choice, y todas las secciones
				//estándar se grabarán en la misma tabla que es una especial diferente a la tabla de datos de la forma
		        $factKeyDimVal = 0;
				global $dataDestinationsCol;
				global $dataDestinationsComposite;
				//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
				$dataDestinationsCol = BITAMeFormsDataDestinationsCollection::NewInstance( $theRepository, $surveyID);
				foreach ($dataDestinationsCol->Collection as $dataDestinations){
					$dataDestinationsComposite = $dataDestinationsComposite.$dataDestinations->additionaleMailComposite."; ";
				}

				//A partir de esta versión, la tabla de la forma (anteriormente llamada Tabla Paralela) sólo contiene un único registro con la información general
				//de la captura pero sin referencia a las respuestas capturadas, este será el primer registro a grabar y determinará el ID de captura ($factKeyDimVal)
				$strStatus = saveDataInSurveyTableV6($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
				
				//Solo debe continuar si no hubo errores
				if ($strStatus == "" || $strStatus == "OK") {
					//Graba el registro correspondiente a las secciones estándar de la forma, el cual a partir de esta versión se graba en una tabla independiente
					//a la tabla del registro de la captura (la cual ahora se llama Tabla de la Forma), en caso de no haber secciones estándar, simplemente continuará
					//el proceso sin error para grabar el resto de las secciones
					//Graba el registro con las respuestas de secciones estándar (sólo si se procesaron otro tipo de secciones previamente)
					$strStatus = singleSaveStandardDataV6($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
				}
				
				//Solo debe continuar si no hubo errores
				if ($strStatus == "" || $strStatus == "OK") {
					//A partir de esta versión, todo el grabado de las secciones múltiples se hará de la misma manera invocando a un método diferente al grabado
					//de las secciones estándar, este método simplemente se encarga de grabar los registros correspondientes a la sección que se procesa así como
					//los de sus preguntas, los cuales sólo hacen join con la propia tabla de sección, por tanto es un método universal sin importar el tipo debug_backtrace
					//sección del que se trate, la única condición es que debe tener una tabla independiente de sección en lugar de una tabla compartida
					$objSectionsColl = BITAMSectionCollection::NewInstance($theRepository, $surveyID);
					foreach ($objSectionsColl as $objSection) {
						$intSectionID = $objSection->SectionID;
						$strStatus = "";
						switch ($objSection->SectionType) {
							case sectFormatted:
								//Las secciones formateadas y cualquiera que caiga en categorías similares, no requieren grabado de datos, así que se ignoran
								break;
							case sectNormal:
								//Las secciones estándar se graban en un método especial, así que son ignoradas de este ciclo
								break;
							case sectMasterDet:
							case sectInline:
							default:
								//Cualquier otra sección se grabará con el mismo método, el cual exclusivamente procesa los registros de dicha sección, para esto ya
								//debió haber grabado el registro único de sección estándar y tener asignado un ID de captura en $factKeyDimVal
								$strStatus = multipleSaveSectionDataV6($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal, $intSectionID);
								break;
						}
						
						//Si ocurrió un error al grabar la sección procesada, debe terminar el proceso en este punto
						if ($strStatus != "" && $strStatus != "OK") {
							break;
						}
					}
				}
				
				//@JAPR 2014-05-28: Agregado el tipo de sección Inline
				//Restaura el array original de datos antes del proceso de pos-procesamiento
				$arrayData = $arrayDataOrig;
				if($saveDataForm)
				{
				//@JAPR 2012-03-14: Agregado el grabado del Status de las Agendas
		        if ($agendaID > 0 && ($strStatus == "" || $strStatus == "OK")) {
					//Si se envió el ID de la agenda y se logró grabar la captura de encuesta, se actualiza el Status de la misma para que ya no se
					//vuelva a considerar en la descarga de las Agendas hacia los móviles
					//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
					//Cuando se graba una encuesta de Agenda la cual está marcada con un estatus de recalendarización, la propia Agenda queda
					//marcada de esa manera, aunque realmente no hay que cambiar nada en su definición, simplemente se presenta de manera diferente
					//en la lista de Agendas
					$intStatus = agdAnswered;
					if ($surveyInstance->EnableReschedule)
					{
						$intStatus = agdRescheduled;
					}
					//GCRUZ 2016-06-02. Agregar campos nuevos de RealDuration, EntryLatitude, EntryLongitude
					$strAddFields = '';
					if (getMDVersion() >= esvAgendaDetNewModelFields) {
						//Asegurarse de que existan los campos
						$aSQL = "ALTER TABLE SI_SV_SurveyAgendaDet ADD RealDuration DOUBLE NULL";
						$theRepository->DataADOConnection->Execute($aSQL);
						$aSQL = "ALTER TABLE SI_SV_SurveyAgendaDet ADD RealLatitude DOUBLE NULL";
						$theRepository->DataADOConnection->Execute($aSQL);
						$aSQL = "ALTER TABLE SI_SV_SurveyAgendaDet ADD RealLongitude DOUBLE NULL";
						$theRepository->DataADOConnection->Execute($aSQL);

						$startDate = (string) @substr($arrayData["surveyDate"], 0, 10);
						$endDate = substr($thisDate, 0, 10);
						$startTime = (string) @$arrayData["surveyHour"];
						$endTime = substr($thisDate, 11);
						$dblRealDuration = (strtotime($endDate.' '.$endTime) - strtotime($startDate.' '.$startTime)) / 60;
						$dblRealLatitude = (double) @$arrayData["surveyLatitude"];
						$dblRealLongitude = (double) @$arrayData["surveyLongitude"];
						$strAddFields .= ", RealDuration = ".$dblRealDuration.", RealLatitude = ".$dblRealLatitude.", RealLongitude = ".$dblRealLongitude;
					}
					$sql = "UPDATE SI_SV_SurveyAgendaDet SET Status = ".$intStatus.$strAddFields.
						" WHERE AgendaID = ".$agendaID." AND SurveyID = ".$surveyID;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("update si_sv_surveyagendadet: ".$sql, 1, 1, "color:purple;");
					}
					$theRepository->DataADOConnection->Execute($sql);
					
					//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
					//Ahora al grabar cualquier captura de encuesta de agenda, automáticamente se va a marcar la agenda como contestada, excepto
					//si previamente ya se encontraba cancelada o recalendarizada (sería un error que estuviera recalendarizada, pero no se
					//debe cambiar dicho estado, la cancelación ocurre durante el grabado si viene cierto parámetro así que esto tentativamente
					//o estaría ocurriendo aquí porque este es el grabado de la forma de Check-In, no podría haber una captura de alguna otra
					//forma ya con un estado de agenda cancelada)
					$strAdditionalFields = '';
					$strAdditionalValues = '';
					if (getMDVersion() >= esvAgendaRedesign) {
						$intStatus = agdAnswered;
						$strExtraFilters = " AND (Status IS NULL OR Status <= ".agdAnswered.")";
						if ((int) @$_POST['AgendaCancelled']) {
							$intStatus = agdCanceled;
							$strExtraFilters = '';
							//Si se está cancelando la agenda, la única validación que tiene prioridad es si la agenda había sido recalendarizada
							//desde el Administrador, en ese caso NO podría actualizar el status a cancelada porque recalendarizado tiene prioridad
							$strExtraFilters = " AND (Status IS NULL OR Status <> ".agdRescheduled.")";
						}
						else {
							//Si no se está cancelando la agenda, entonces verifica si aún había alguna encuesta que no estuviera terminada,
							//ya que si todas estaban terminadas entonces el status a grabar debería ser terminada en lugar de contestada
							$intNonFinishedAgendas = 0;
							$sql = "SELECT COUNT(*) AS NonFinishedNum 
								FROM SI_SV_SurveyAgendaDet 
								WHERE AgendaID = {$agendaID} AND (Status IS NULL OR Status = ".agdOpen.")";
							$tmpVal = $theRepository->DataADOConnection->Execute($sql);
							if ($tmpVal && !$tmpVal->EOF) {
								$intNonFinishedAgendas = (int) @$tmpVal->fields["nonfinishednum"];
							}
							
							if ($intNonFinishedAgendas == 0) {
								$intStatus = agdFinished;
							}
						}
						
						//Si el estado de esta agenda es cancelada, entonces si debe ejecutar el UPDATE sin restricciones pues va a cambiar
						//el estado, de lo contrario sólo puede cambiar el estado si la agenda no había sido ni terminada ni cancelada o 
						//recalendarizada
						$sql = "UPDATE SI_SV_SurveyAgenda SET Status = {$intStatus} 
							WHERE AgendaID = {$agendaID} ".$strExtraFilters;
						$theRepository->DataADOConnection->Execute($sql);
					}
					//@JAPR
					
					//@JAPR 2012-06-01: Temporalmente se modificará la fecha de actualización de la Agenda para que las Apps piensen que han cambiado
					//y eso las force a descargar las definiciones de nuevo, de lo contrario la App se quedaría con las mismas encuestas por agenda
					//incluso las ya contestadas y las intentaría capturar nuevamente
					$currentDate = date("Y-m-d H:i:s");
					$sql = "UPDATE SI_SV_SurveyAgenda SET LastDateID = ".$theRepository->DataADOConnection->DBTimeStamp($currentDate).
						" WHERE AgendaID = ".$agendaID;
					$theRepository->DataADOConnection->Execute($sql);
					//@JAPR
					
					//Conchita 2013-01-22 Se debe de modificar el versionnum para que se descarguen bien definiciones en el movil
					$sql = "SELECT MAX(VersionNum) AS Num FROM SI_SV_SurveyAgenda WHERE AgendaID = ".$agendaID;
					$tmpVal = $theRepository->DataADOConnection->Execute($sql);
					if ($tmpVal) {
						$oldVersion = (int) @$tmpVal->fields["num"];
						$oldVersion ++;
						$sql = "UPDATE SI_SV_SurveyAgenda SET VersionNum = ".$oldVersion." WHERE AgendaID = ".$agendaID;
						$theRepository->DataADOConnection->Execute($sql);
					}
					//Conchita

					//GCRUZ 2016-06-02. Proceso de cálculo de tiempo de traslado de agendas
					if (getMDVersion() >= esvAgendaDetNewModelFields) {
						calculateSurveyAgendaTransfer($theRepository, $surveyID, $agendaID, $arrayData);
					}
		        }
				}
				else
				{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("No realiza proceso de agendas", 1, 1, "color:purple;");
						}
				}
				//Realiza procesos adicionales posteriores al correcto grabado de los datos de la forma, los cuales no deben ser detenidos en caso de que falle
				//algo en el grabado de datos hacia eBavel
				//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
				//@JAPR 2015-08-25: Corregido un bug, faltaba validar contra errores esta parte
				if($saveDataForm)
				{
				if ($strStatus == "" || $strStatus == "OK") {
					$strStatus = saveDataInGlobalSurveyModelV6($theRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal);
				}
				}
				else
				{
					if (getParamValue('DebugBreak', 'both', '(int)', true)){
						ECHOString("No ejecuta saveDataInGlobalSurveyModelV6", 1, 1, "color:purple;");
					}

				}
				if ($strStatus == "" || $strStatus == "OK") {
					
					if($saveDataForm)
					{
					//@JAPR 2015-09-18: Corregido un bug, en este punto se actualizará el Status de la captura en SI_SV_SurveyLog ya que aquí terminó de grabar todo lo relacionado
					//con eForms, de esta manera si falla durante el grabado de DataDestinations o algún otro proceso, por lo menos se verá la captura en la bitácora (#L7TLOH)
					$UserID = $theRepository->DataADOConnection->Quote(getParamValue('UserID', 'both', "(string)"));
					$sql = "UPDATE SI_SV_SurveyLog SET Status = 1 
						WHERE SurveyID = {$surveyID} AND UserID = {$UserID} AND SurveyDate = ".$theRepository->DataADOConnection->DBTimeStamp($lastDate);
					$theRepository->DataADOConnection->Execute($sql);
					//@JAPR
					}
					
					//El SP se ejecutará sólo si se graban los datos en la forma
					//o si se está reprocesando el destino que es "Model"
					if($saveDataForm || ($reprocess && in_array(-1,$repDestinationIDs)))
					{
					
					//02Dic2015: Validar que sólo se ejecute el SP si hay cubo para la forma
					$sql = "SELECT id_modelartus FROM SI_SV_MODELARTUS WHERE ID_SURVEY={$surveyID} AND id_modelartus>0";
					if (getParamValue('DebugBreak', 'both', '(int)', true)){
						ECHOString("Validar que solamente se ejecute el SP si hay cubo para la forma", 1, 1, "color:purple;");
						ECHOString($sql, 1, 1, "color:purple;");
					}
					
					$aRS = $theRepository->DataADOConnection->Execute($sql);
					if ($aRS && !$aRS->EOF)
					{
						$execSP=true;
					}
					else
					{
						$execSP=false;
						if (getParamValue('DebugBreak', 'both', '(int)', true)){
							ECHOString("No se ejecuta el SP por que no hay cubo para la forma, execSP=".((int)$execSP), 1, 1, "color:purple;");
						}
					}

					if($execSP)
					{
						// @EA 2015-07-29 Se llama al stored procedure que actualiza los modelos asociados a la encuesta
						$sql = "call sp_load_model_for_survey_{$surveyID}({$factKeyDimVal})";
						$message_log = array();
						$message_log['error'] = 0;
						$serr = '';
						if (getParamValue('DebugBreak', 'both', '(int)', true)){
							ECHOString("Ejecutar SP que actualiza los modelos", 1, 1, "color:purple;");
						}
						
						if (!$theRepository->DataADOConnection->Execute($sql))
						{
						  $serr = $theRepository->DataADOConnection->ErrorMsg();
						  @savelogfile($sql.' -> '.$serr);
						  $message_log['msg'] = translate("Model needs to be re-mapped due to delete questions");
						  $message_log['error'] = 1;
						}
						else
						  $message_log['msg'] = translate("Capture loaded into models");
						
						if ($serr != '' && stripos($serr, 'PROCEDURE') !== false) {
						  //@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
						  @insertDataDestinationModelLog($message_log, 'Model', $surveyID, $theUser->UserName.'/'.$theUser->FullName, $factKeyDimVal);
						  //@JAPR
						}
					}
					}
					else
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)){
							ECHOString("No ejecuta el SP que actualiza los modelos", 1, 1, "color:purple;");
						}
					}
					
					if($saveDataForm || ($reprocess && count($repDestinationIDs)>0))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)){
							ECHOString("Preparar instancias para el procesamiento de destinos", 1, 1, "color:purple;");
						}

					//@JAPR 2015-12-03: Movido el código de DataDestinations para que se ejecute posterior al grabado en los cubos, ya que uno de los
					//destinos es ahora el envío de Dashboards o ARS y obviamente necesita que primero se actualicen los datos ahí
					require_once('report.inc.php');
					$instanceiExport = BITAMReport::NewInstanceToFormsLogWithIDV6($theRepository,@$lastDate, $surveyID, @$theUser->UserID,  true, false);
					
					require_once( 'SyncDataDestination.class.php' );
					
					$aSyncDataDestination = SyncDataDestination::NewInstance($theRepository, $surveyInstance, $factKeyDimVal, $jsonData);
					$syncDataDestinationOpt=array(
						'arrayData' =>$arrayData,
						'thisDate' =>$thisDate,
						'syncDate' =>$syncDate,
						'syncTime' =>$syncTime,
						'aSourceActionData' =>@$aSourceActionData,
						'theAppUser'=>$theAppUser,
						'arrGPSData'=>$arrGPSData,
						//@JAPR 2017-02-01: Agregado el mapeo del UUID (#TDYU15)
						'arrJSONData'=>$jsonData
						//@JAPR 2018-01-18: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
						, 'surveyInstance'=>$surveyInstance
						, 'factKeyDimVal'=>$factKeyDimVal
						//@JAPR
						);
					$aSyncDataDestination->options=$syncDataDestinationOpt;
					$aSyncDataDestination->instaceExport = $instanceiExport;
					$aSyncDataDestination->sync();
					//@JAPR
						if (getParamValue('DebugBreak', 'both', '(int)', true)){
							ECHOString("Termina procesamiento de destinos", 1, 1, "color:purple;");
						}
					}
				}
				//@JAPR
				
				//A partir de esta versión sólo se soportará el mapeo a eBavel a través de formas independientes, de hecho el mapeo ya no es parte de cada opción
				//de respuesta o cada pregunta, sino que es un mapeo a nivel de la Forma
				
				//Por default (al menos con el método original de acciones) bastaba con tener una capturada para continuar con el proceso
				$blnCanSaveeBavelActions = (isset($arrActionItems) && is_array($arrActionItems) && count($arrActionItems) > 0);
				//Si ocurrió un error al grabar hasta este punto, debe omitir el grabado de acciones de eBavel
				if ($strStatus != "" && $strStatus != "OK") {
					$blnCanSaveeBavelActions = false;
				}
				
				//Valida que se tengan las configuraciones necesarias para poder interactuar con el eBavel
				if ($blnCanSaveeBavelActions) {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Starting eBavel action saving process: {$blnCanSaveeBavelActions}");
						ECHOString("Number of actions to process: ".count($arrActionItems));
					}
					@logeBavelString("Starting eBavel action saving process: {$blnCanSaveeBavelActions}");
					@logeBavelString("User: ".@$theUser->UserID.", Survey: ".@$surveyID.", Entry date: ".@$lastDate);
					@logeBavelString("Number of actions to process: ".count($arrActionItems));
					
					require_once('settingsvariable.inc.php');
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELURL');
					if (is_null($objSetting) || trim($objSetting->SettingValue == ''))
					{
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("No eBavel path configured: ".$blnCanSaveeBavelActions);
						}
						@logeBavelString("No eBavel path configured: ".$blnCanSaveeBavelActions);
						$blnCanSaveeBavelActions = false;
					}
				}
				else {
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Skipping eBavel action saving process: ".$blnCanSaveeBavelActions);
						if (isset($arrActionItems) && is_array($arrActionItems)) {
							PrintMultiArray($arrActionItems);
						}
						else {
							ECHOString("No actions registered");
						}
					}
					@logeBavelString("Skipping eBavel action saving process: ".$blnCanSaveeBavelActions);
					if (isset($arrActionItems) && is_array($arrActionItems)) {
						
					}
					else {
						@logeBavelString("No actions registered");
					}
				}
				
				if ($blnCanSaveeBavelActions) {
					$eBavelAppCodeName = '';
					$sql = "SELECT codeName FROM SI_FORMS_APPS WHERE id_app = ".$surveyInstance->eBavelAppID;
				    $aRS = $theRepository->ADOConnection->Execute($sql);
				    if ($aRS === false || $aRS->EOF) {
						$blnCanSaveeBavelActions = false;
				    }
				    else 
				    {
				    	$eBavelAppCodeName = (string) $aRS->fields["codename"];
				    	//La propiedad eBavelFormID sólo se usaba para acciones antes de que se creara la forma exclusiva para acciones, así que
				    	//a falta de la misma no hay por qué impedir que se graben las acciones, asumiendo por supuesto que se trata del método
				    	//original. Si se tratara del nuevo método entonces por el contrario se tiene que verificar pero contra el campo de la
				    	//forma especial de acciones
				    	if (trim($eBavelAppCodeName) == '') {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("No eBavel app found: ".$surveyInstance->eBavelAppID);
							}
							@logeBavelString("No eBavel app found: ".$surveyInstance->eBavelAppID);
				    		$blnCanSaveeBavelActions = false;
				    	}
				    }
				}
				
				//Si se utilizará una forma para las acciones, hay que verificar muchas cosas antes de intentar el grabado de las mismas, de tal
				//manera que si no todas se tienen correctamente entonces debe deshabilitar el grabado de acciones por completo
				if ($blnCanSaveeBavelActions) {
					//Independientemente del método de grabado, se verica si el código de eBavel está disponible
				    $strPath = '';
				    $streBavelPath = '';
					$arrURL = parse_url($objSetting->SettingValue);
					$strPath = $_SERVER["DOCUMENT_ROOT"].((string)@$arrURL['path']).'service.php';
					$strPath = str_replace('/', '\\', $strPath);
					$strPath = str_replace('\\\\', '\\', $strPath);
					//@JAPR 2012-07-10: Corregido un bug, si la ruta al eBavel configurada  no existe, a pesar de ignorar el error mediante el uso de @
					//estaba generando un error de compilación interrumpiendo con ello el grabado al primer registro de una captura que debió haber
					//grabado múltiples registros
					if (file_exists($strPath)) {
						@require_once($strPath);
						$streBavelPath = str_ireplace('service.php', '', $strPath);
					}
					
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						if (!function_exists('NotifyError')) {
							ECHOString("No NotifyError function");
						}
						else {
							ECHOString("NotifyError function exists");
						}
					}
					if (!function_exists('NotifyError')) {
						@logeBavelString("No NotifyError function");
					}
					else {
						@logeBavelString("NotifyError function exists");
					}
					//@JAPR
					
					if (!class_exists('Tables')) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("No eBavel code loaded: ".$blnCanSaveeBavelActions);
						}
						@logeBavelString("No eBavel code loaded: ".$blnCanSaveeBavelActions);
						$blnCanSaveeBavelActions = false;
					}
					
					if ($blnCanSaveeBavelActions) {
					    //Verifica si la forma especificada existe
				    	$eBavelFormTable = '';
					    $arreBavelFieldsColl = array();
						require_once('eBavelIntegration.inc.php');
				    	$arreBavelFormInfo = null;
				    	//Sólo cargará la forma de acciones default de la encuesta si esta se encuentra configurada, ya que si no se asume que se
				    	//usará la forma configurada por cada pregunta
				    	if ($surveyInstance->eBavelActionFormID > 0) {
					    	$arreBavelFormColl = @GetEBavelSections($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelActionFormID));
					    	if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[$surveyInstance->eBavelActionFormID])) {
					    		$arreBavelFormInfo = $arreBavelFormColl[$surveyInstance->eBavelActionFormID];
					    		$eBavelFormTable = $arreBavelFormInfo['name'];
					    		
					    		$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, $surveyInstance->eBavelAppID, array($surveyInstance->eBavelActionFormID));
					    		if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl) || count($arreBavelFieldsColl) == 0) {
					    			//No se pudo recuperar la info de los campos o no hay campos de eBavel
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("No default eBavel action fields found: ".$surveyInstance->eBavelActionFormID);
										PrintMultiArray($arreBavelFieldsColl);
									}
									@logeBavelString("No default eBavel action fields found: ".$surveyInstance->eBavelActionFormID);
					    			$blnCanSaveeBavelActions = false;
					    		}
					    	}
					    	else {
					    		//No se pudo recuperar la info de la forma de eBavel
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("No default eBavel action form found: ".$surveyInstance->eBavelActionFormID);
									PrintMultiArray($arreBavelFormColl);
								}
								@logeBavelString("No default eBavel action form found: ".$surveyInstance->eBavelActionFormID);
					    		$blnCanSaveeBavelActions = false;
					    	}
				    	}
				    	
				    	$strDefaulteBavelFormTable = $eBavelFormTable;
				    	$arrDefaulteBavelFieldsColl = $arreBavelFieldsColl;
					}
				}
				
				//Array indexado por el Id de pregunta o nombre específico de un campo del que se deben obtener datos para grabar en eBavel
				$arreBavelEmptyMappedData = array();
				//Array con los IDs mapeados de campos de eBavel indexado por el Id de pregunta o nombre específico de campo de eForms
				$arreBavelMappedIDs = array();
				//Array con las preguntas y secciones indexadas por su ID
				$arrQuestionsColl = array();
				$arrSectionsColl = array();
				
				if ($blnCanSaveeBavelActions) {
					require_once("surveyActionMap.inc.php");
					require_once("questionActionMap.inc.php");
					require_once("questionOptionActionMap.inc.php");
					
					if ($surveyInstance->eBavelActionFormID > 0) {
						$objSurveyFieldMap = BITAMSurveyActionMap::NewInstanceWithSurvey($theRepository, $surveyID);
						if (is_null($objSurveyFieldMap) || !isset($objSurveyFieldMap->ArrayVariables) || count($objSurveyFieldMap->ArrayVariables) == 0) {
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("No fields mapped: ".$blnCanSaveeBavelActions);
							}
							@logeBavelString("No fields mapped: ".$blnCanSaveeBavelActions);
						}
						else {
							foreach($objSurveyFieldMap->ArrayVariables as $intSurveyFieldID => $strVariableName) {
								$intVariableValue = (int) @$objSurveyFieldMap->ArrayVariableValues[$intSurveyFieldID];
								if ($intVariableValue > 0 && isset($arreBavelFieldsColl[$intVariableValue])) {
									$arreBavelEmptyMappedData[$strVariableName] = null;
									$arreBavelMappedIDs[$strVariableName] = $intVariableValue;
								}
							}
						}
					}
				}
				$arrDefaulteBavelEmptyMappedData = $arreBavelEmptyMappedData;
				$arrDefaulteBavelMappedIDs = $arreBavelMappedIDs;
				
				$arrUserNames = array();
				//@JAPR 2012-10-29: Modificado para enviar las acciones hasta que se termine de grabar, incluyendo mayor detalle sobre la captura (PDF)
		        //Si hay definida alguna acción de eBavel según la captura, procede a crear cada una mediante la instancia de eBavel
				//@JAPR 2013-07-01: Agregado el mapeo de una forma de eBavel para acciones
		        if ($blnCanSaveeBavelActions) {
		        	//Si llegó a este punto, quiere decir que o bien se usará el método original para lo cual no requiere de muchas validaciones,
		        	//o bien que se usará el método de grabado en una forma para lo cual ya se verificó que todo estuviera correctamente configurado
					
					//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
					//Ahora se requiere cargar las colecciones de preguntas y secciones para validar ciertas cosas por tipos
					//En este caso se agregan todas las preguntas, no sólo aquellas mapeadas ya que se usará en este punto para procesar
					//en las acciones, no entre los datos mapeados a nivel de encuesta
					$objQuestionsColl = BITAMQuestionCollection::NewInstance($theRepository, $surveyID);
					foreach ($objQuestionsColl->Collection as $aQuestion) {
						$arrQuestionsColl[$aQuestion->QuestionID] = $aQuestion;
					}
					
					$objSectionsColl = BITAMSectionCollection::NewInstance($theRepository, $surveyID);
					foreach ($objSectionsColl->Collection as $sectionKey => $aSection) {
						$arrSectionsColl[$aSection->SectionID] = $aSection;
					}
					//@JAPR
					
					$strDefaultActionStatus = '';
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELDEFAULTACTIONSTATUS');
					if (!(is_null($objSetting) || trim($objSetting->SettingValue == '')))
					{
						$strDefaultActionStatus = $objSetting->SettingValue;
					}
					if (trim($strDefaultActionStatus) == '') {
						$strDefaultActionStatus = "Request";
					}
					
					$strDefaultActionSource = '';
					$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'EBAVELDEFAULTACTIONSOURCE');
					if (!(is_null($objSetting) || trim($objSetting->SettingValue == '')))
					{
						$strDefaultActionSource = $objSetting->SettingValue;
					}
					if (trim($strDefaultActionSource) == '') {
						$strDefaultActionSource = "eForms";
					}
					
					//@JAPR 2013-07-10: Corregido un bug, los datos del GPS se deben obtener ahora en este punto si es que son usados por acciones
					$arreBavelFieldsByForm = array();
					//Este es el método de grabado de acciones a partir de una forma de eBavel
					$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
					foreach ($arrActionItems as $intActionIdx => $aSourceActionData) {
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							ECHOString("Processing action #{".($intActionIdx +1)."}");
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								PrintMultiArray($aSourceActionData);
							}
						}
						
						@logeBavelString("Processing action #{".($intActionIdx +1)."}");
						$blnCanSaveeBavelActions = true;
						try {
							//Antes de comenzar a invocar a eBavel, se tiene que verificar si existe un mapeo de los datos de la acción
							//basado en la pregunta y opción de respuesta que la genera, ya que si no es así entonces significaría que en el
							//administrador se limitó la funcionalidad y esta debe ser una captura viejita que aun no tenía deshabilitadas
							//las acciones, por lo cual sigue enviando información pero ya no se debe grabar porque no habría un mapeo
							$intQuestionID = (int) @$aSourceActionData["QuestionID"];
							if ($intQuestionID <= 0) {
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("No QuestionID specified");
								}
								@logeBavelString("No QuestionID specified");
								continue;
							}
							
							//@JAPR 2013-07-15: Agregado el mapeo de una forma de eBavel para acciones
							//Ya no es necesario que esté asignado el QuestionAnswer, ya que dependerá del tipo de pregunta y pudiera ser de una
							//pregunta tipo acción simplemente, si se quiere validar correctamente se tendría que agregar al IF el tipo de pregunta,
							//pero se supone que desde que generó el array de acciones ya estaba todo perfectamente validado
							$strOptionText = (string) @$aSourceActionData["QuestionAnswer"];
							//@JAPR
							$inteBavelActionFormID = (int) @$aSourceActionData["eBavelActionFormID"];
							if ($inteBavelActionFormID <= 0) {
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("No eBavel form specified");
								}
								@logeBavelString("No eBavel form specified");
								continue;
							}
							
							//Ya identificada la pregunta y la opción de respuesta, obtiene su mapeo específico hacia eBavel, si no hay entonces
							//ignorará esta acción (dado a que la colección de opciones de respuesta no está optimizada para grabarse en caché,
							//se utilizará directamente un elemento del mismo array de datos de la acción para recuperarlo)
							$objQuestionOptionFieldMap = @$aSourceActionData["eBavelFieldMap"];
							if (is_null($objQuestionOptionFieldMap)) {
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									ECHOString("No eBavel action field map specified");
								}
								@logeBavelString("No eBavel action field map specified");
								continue;
							}
							else {
								$arreBavelEmptyMappedData = array();
								$arreBavelMappedIDs = array();
								
								//Verifica si la forma especificada existe
								$eBavelFormTable = '';
								$arreBavelFieldsColl = array();
								require_once('eBavelIntegration.inc.php');
								$arreBavelFormInfo = null;
								//Sólo cargará la forma de acciones default de la encuesta si esta se encuentra configurada, ya que si no se asume que se
								//usará la forma configurada por cada pregunta
								if ($inteBavelActionFormID > 0) {
									$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
									$arreBavelFormColl = @GetEBavelSections($theRepository, true, $surveyInstance->eBavelAppID, array($inteBavelActionFormID));
									$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
									if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[$inteBavelActionFormID])) {
										$arreBavelFormInfo = $arreBavelFormColl[$inteBavelActionFormID];
										$eBavelFormTable = $arreBavelFormInfo['name'];
										
										$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
										$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, $surveyInstance->eBavelAppID, array($inteBavelActionFormID));
										$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
										if (is_null($arreBavelFieldsColl) || !is_array($arreBavelFieldsColl) || count($arreBavelFieldsColl) == 0) {
											//No se pudo recuperar la info de los campos o no hay campos de eBavel
											if (getParamValue('DebugBreak', 'both', '(int)', true)) {
												ECHOString("No eBavel action fields found: ".$inteBavelActionFormID);
												PrintMultiArray($arreBavelFieldsColl);
											}
											@logeBavelString("No eBavel action fields found: ".$inteBavelActionFormID);
											if (@$blnSaveeBavelLogStringFile) {
												$strLogText = (string) @var_export(@$arreBavelFieldsColl, true);
												@logeBavelString($strLogText);
											}
											$blnCanSaveeBavelActions = false;
										}
									}
									else {
										//No se pudo recuperar la info de la forma de eBavel
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("No eBavel action form found: ".$inteBavelActionFormID);
											PrintMultiArray($arreBavelFormColl);
										}
										@logeBavelString("No eBavel action form found: ".$inteBavelActionFormID);
										if (@$blnSaveeBavelLogStringFile) {
											$strLogText = (string) @var_export(@$arreBavelFormColl, true);
											@logeBavelString($strLogText);
										}
										$blnCanSaveeBavelActions = false;
									}
								}
								else {
									//En este caso no hay una forma configurada por pregunta, así que se reutilizarán los datos default de la encuesta
									$arreBavelFieldsColl = $arrDefaulteBavelFieldsColl;
									$eBavelFormTable = $strDefaulteBavelFormTable;
								}
							}
							
							//@JAPR 2013-07-15: Faltaba validar en este punto si hubo algún error al recuperar los datos de eBavel para impedir
							//que se intente generar una acción que va a fallar (el error ya se reportó arriba)
							if (!$blnCanSaveeBavelActions) {
								continue;
							}
							
							//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
							//la clase equivocada
							$strActionMapClassName = (string) @$aSourceActionData["eBavelActionSrcType"];
							switch (strtolower($strActionMapClassName)) {
								case 'question':
									$strActionMapClassName = 'BITAMQuestionActionMap';
									break;
								//case 'answer':
								default:
									$strActionMapClassName = 'BITAMQuestionOptionActionMap';
									break;
							}
							//@JAPR
							
							foreach($objQuestionOptionFieldMap->ArrayVariables as $inteBavelFieldID => $strVariableName) {
								if (!isset($objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) || !$objQuestionOptionFieldMap->ArrayVariablesExist[$inteBavelFieldID]) {
									//En este caso el campo no está mapeado, así que lo omite
									continue;
								}
								
								//Si el campo de eBavel especificado no existe, omite el mapeo
								if (!isset($arreBavelFieldsColl[$inteBavelFieldID])) {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("Mapped field do not exists: {$inteBavelFieldID} => $strVariableName");
									}
									@logeBavelString("Mapped field do not exists: {$inteBavelFieldID} => $strVariableName");
									continue;
								}
								
								$intSurveyFieldID = ((int) @$objQuestionOptionFieldMap->ArrayVariableValues[$inteBavelFieldID]);
								if ($intSurveyFieldID != 0) {
									//Se puede usar la clase BITAMQuestionOptionActionMap indistintamente de si es o no una acción de opción de
									//respuesta, ya que todas estas acciones siguen la misma base y por ende sus clases usarían los mismos IDs
									//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
									//la clase equivocada
									$strSurveyVariableName = call_user_func($strActionMapClassName.'::GetSurveyColumnName', $intSurveyFieldID);
									//@JAPR
									if (trim($strSurveyVariableName) != '' ) {
										$arreBavelEmptyMappedData[$strSurveyVariableName] = null;
										//@JAPR 2013-12-12: Corregido un bug, diferentes campos de eBavel se pueden mapear al mismo valor de
										//eForms, por lo tanto usar la llave del campo de eForms para el array no era correcto, ya que de esa
										//forma sólo el último en la configuración se habría grabado, ahora se manejará un array
										if (!isset($arreBavelMappedIDs[$strSurveyVariableName])) {
											$arreBavelMappedIDs[$strSurveyVariableName] = array();
										}
										$arreBavelMappedIDs[$strSurveyVariableName][] = $inteBavelFieldID;
										//@JAPR
									}
								}
								else {
									if (getParamValue('DebugBreak', 'both', '(int)', true)) {
										ECHOString("Invalid field value: {$inteBavelFieldID} => $strVariableName");
									}
									@logeBavelString("Invalid field value: {$inteBavelFieldID} => $strVariableName");
								}
							}
							
							$anActionData = array();
							//Hay datos que siempre son fijos así que se agregan tal cual
							$anActionData["createdDate"] = @$aSourceActionData["createdDate"];
							$anActionData["modifiedDate"] = @$aSourceActionData["modifiedDate"];
							foreach ($arreBavelMappedIDs as $aneFormsFieldID => $aneBavelFieldArray) {		//$aneBavelFieldID
								//@JAPR 2013-12-12: Corregido un bug, diferentes campos de eBavel se pueden mapear al mismo valor de
								//eForms, por lo tanto usar la llave del campo de eForms para el array no era correcto, ya que de esa
								//forma sólo el último en la configuración se habría grabado, ahora se manejará un array
								if (!is_array($aneBavelFieldArray)) {
									$aneBavelFieldArray = array($aneBavelFieldArray);
								}
								
								//Recorre el ciclo de todos los campos de eBavel que apuntan a este mismo campo de eForms
								foreach ($aneBavelFieldArray as $aneBavelFieldID) {
									$arreBavelFieldInfo = @$arreBavelFieldsColl[$aneBavelFieldID];
									if (is_null($arreBavelFieldInfo) || !is_array($arreBavelFieldInfo)) {
										if (getParamValue('DebugBreak', 'both', '(int)', true)) {
											ECHOString("No eBavel field info: ".$aneBavelFieldID);
										}
										@logeBavelString("No eBavel field info: ".$aneBavelFieldID);
										continue;
									}
									
									$inteBavelQTypeID = convertEBavelToEFormsFieldType($arreBavelFieldInfo['tagname']);
									$streBavelFieldName = (string) @$arreBavelFieldInfo['label'];
									$aData = null;
									$blnSkipField = false;
									if (is_numeric(str_ireplace('qfield', '', $aneFormsFieldID))) {
										//Es una pregunta de la encuesta, así que se busca el valor que previamente se tuvo que haber agregado
										//al array de datos de la acción, utilizando el nombre de variable de preguntas
										//@JAPR 2014-10-29: Agregado el mapeo de Scores
										//En este punto las preguntas pudieran tener diferente valor configurado según el DataID utilizado, así
										//que ahora debe ajustar el elemento que busca en el array pues no siempre será la respuesta de la pregunta,
										//esto es así porque la variable $aneFormsFieldID se genera de forma genérica con la estructura qfield###
										//sin importarle el dato específico a pedir de la pregunta, así que en este punto se valida eso
										$intDataID = (int) @$objQuestionOptionFieldMap->ArrayVariablesDataID[$aneBavelFieldID];
										$strFormsFieldID = $aneFormsFieldID;
										switch ($intDataID) {
											case sdidScore:
												$strFormsFieldID .= "Score";
												break;
										}
										
										if (isset($aSourceActionData[$strFormsFieldID])) {
											//@JAPR 2013-07-19: Corregido un bug, si las preguntas son numéricas y están en secciones estándar,
											//no se puede obtener su valor durante el procesamiento de registros que no sean el único de secciones estándar,
											//Se agregará un array con las respuestas a todas estas preguntas justo cuando se proceso este registro único para reasignarlo mas
											//adelante. Si viene un array con el dummy que así lo indica, entonces este valor se obtendrá del array especial de
											//preguntas numéricas
											$aData = $aSourceActionData[$strFormsFieldID];
											if (is_array($aData)) {
												//No puede venir como respuesta un array, así que automáticamente esta pregunta no es válida a menos que se
												//esté pidiendo el dummy para preguntas numéricas de secciones estandar
												if (((string) @$aData[0]) == 'GetFromStRec') {
													//Se obtiene el valor del dummy si es que existe
													$aData = @$arrNumericQAnswersColl[$aneFormsFieldID];
												}
												else {
													//Definitivamente no es una respuesta válida
													$blnSkipField = true;
												}
											}
											
											//@JAPR 2013-07-24: Agregado el mapeo de fotos durante la generación de acciones
											//@JAPR 2014-10-29: Agregado el mapeo de Scores
											//Estas validaciones sólo aplican si se está pidiendo el valor de la pregunta
											if (!$blnSkipField && $intDataID == sdidValue) {
												//Obtiene el ID de pregunta para validar ciertas cosas dependiendo del tipo
												$intQuestionID = str_ireplace('qfield', '', $aneFormsFieldID);
												$objQuestion = @$arrQuestionsColl[$intQuestionID];
												if (is_null($objQuestion)) {
													if (getParamValue('DebugBreak', 'both', '(int)', true)) {
														ECHOString("QuestionID not found: $aneFormsFieldID");
													}
												}
												else {
													//Verifica por tipo de pregunta para saber como debe grabarla
													switch ($objQuestion->QTypeID) {
														case qtpMulti:
															//En el caso de las multiple choice, eBavel espera un array con las descripciones de
															//las opciones seleccionadas cuando se mapea a un campo multiple-choice, si se mapea
															//a un alfanumerico entonces serán las propias descripciones separadas por ","
															//Por el momento sólo se pueden mapear preguntas multiple-choice tipo CheckBox
															switch ($inteBavelQTypeID) {
																case qtpMulti:
																	//En este caso se espera un array de las opciones seleccionadas
																	if (trim($aData) == '') {
																		$aData = array();
																	}
																	else {
																		$aData = @explode(';', $aData);
																	}
																	break;
																	
																case qtpOpenString:
																case qtpOpenAlpha:
																	//Se convierte a una lista de opciones separada por ","
																	$aData = str_replace(';', ',', $aData);
																	break;
																	
																default:
																	//Tipo de conversión no soportado
																	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																		ECHOString("Type mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
																		PrintMultiArray($objAnswers);
																	}
																	break;
															}
															
															break;
														
														case qtpPhoto:
															$insertImage = $aData;
															//Genera el nombre de la ruta de la imagen según eBavel
															$manager = new Tables();
															@$manager->notifyErrorService = true;
															$manager->applicationCodeName = $eBavelAppCodeName;
															$manager->init($theRepository->ADOConnection);
															$params = array();
															$params['sectionname'] = $eBavelFormTable;
															$params['widgettype'] = 'widget_photo';
															$params['ext'] = 'jpg';
															$aData = $manager->getFilePath($params, null);
															$strCompletePath = $streBavelPath.$aData;
															if ($inteBavelQTypeID == qtpPhoto) {
																//Sólo si el campo de eBavel es un tipo foto es como se copia la imagen
																if ($usePhotoPaths) {
																	//Realiza la copia del archivo de imagen
																	//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
																	if ($blnWebMode) {
																		$path = getcwd()."\\";
																	}
																	else {
																		//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
																		$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".trim($_SESSION["PABITAM_RepositoryName"]);
																	}
																	
																	$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertImage);
																	$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
																	if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
																		$strCompleteSourcePath .= '.jpg';
																	}
																	
																	$status = @copy($strCompleteSourcePath, $strCompletePath);
																	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																		ECHOString("Saving eBavel action photo from $strCompleteSourcePath to $strCompletePath: $status");
																	}
																}
																else {
																	//Esta funcionalidad es exclusiva de v4, así que no se soportará el viejo formato para este proceso
																	$aData = null;
																}
															}
															
															switch ($inteBavelQTypeID) {
																case qtpPhoto:
																case qtpOpenString:
																case qtpOpenAlpha:
																	//En los 3 tipos se graba el dato como la ruta de la imagen
																	break;
																	
																default:
																	//Tipo de conversión no soportado
																	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
																		ECHOString("Type mistmatch for question $aneFormsFieldID mapping data to the field $streBavelFieldName");
																		PrintMultiArray($objAnswers);
																	}
																	$aData = null;
																	break;
															}
															break;
														case qtpSketch:	
														case qtpSignature:
														case qtpAudio:
														case qtpVideo:
														//Conchita agregados audio y video 20140924
														case qtpDocument:
														case qtpOCR:
															//@JAPRPendiente
															break;
													}
												}
											}
											//@JAPR
										}
										else {
											//No venía el dato, por lo tanto se ignorará ya que para las acciones se tiene que haber registrado
											//al momento de registrar la acción, esto por si la acción se encontrara dentro de una sección múltiple
											//por lo que no se hubiera podido determinar fácilmente su valor en este punto
											$blnSkipField = true;
										}
									}
									else {
										//Es un campo fijo de datos de la encuesta
										//Se puede usar la clase BITAMQuestionOptionActionMap indistintamente de si es o no una acción de opción de
										//respuesta, ya que todas estas acciones siguen la misma base y por ende sus clases usarían los mismos IDs
										//@JAPR 2014-10-14: Corregido un bug, no estaba grabando las acciones de preguntas tipo acción porque estaba utilizando
										//la clase equivocada
										$intSurveyFieldID = call_user_func($strActionMapClassName.'::GetSurveyColumnID', $aneFormsFieldID);
										//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
										$intAttributeID = (int) @$objQuestionOptionFieldMap->ArrayVariablesMemberID[$aneBavelFieldID];
										//@JAPR
										switch ($intSurveyFieldID) {
											//*********************************************************************************************
											//Campos generales de la captura de encuesta
											//*********************************************************************************************
											case sfidStartDate:
												$aData = substr($arrayData["surveyDate"], 0, 10);
												break;
											case sfidStartHour:
												$aData = $arrayData["surveyHour"];
												break;
											case sfidEndDate:
												$aData = substr($thisDate, 0, 10);
												break;
											case sfidEndHour:
												$aData = substr($thisDate, 11);
												break;
											case sfidLocation:
												$blnSkipField = true;
												if(isset($arrayData['surveyLatitude'])) {
													$latitude = (double)$arrayData['surveyLatitude'];
												}
												else {
													$latitude = 0;
												}
												
												if(isset($arrayData['surveyLongitude'])) {
													$longitude = (double)$arrayData['surveyLongitude'];
												} 
												else {
													$longitude = 0;
												}
												
												if (trim($_SESSION["PABITAM_RepositoryName"]) == "fbm_bmd_0713" && $surveyInstance->SurveyID == 4) {
													$strMessageQuestion = trim((string) @$arrayData['qfield32']);
													if ($strMessageQuestion != '') {
														$arrData = explode(',', $strMessageQuestion);
														if (count($arrData) == 2) {
															$strLatitude = trim(str_ireplace('A=', '', $arrData[0]));
															$strLongitude = trim(str_ireplace('N=', '', $arrData[1]));
															if ($strLatitude != '' && $strLongitude != '' && is_numeric($strLatitude) && is_numeric($strLongitude)) {
																$latitude = $strLatitude;
																$longitude = $strLongitude;
															}
														}
													}
												}
												
												$anActionData[$arreBavelFieldInfo['id']] = $latitude.', '.$longitude;
												break;
											//@JAPR 2013-12-02: Agregado el Atributo de Accuracy
											case sfidLocationAcc:
												if(isset($arrayData['surveyAccuracy'])) 
												{
													$accuracy = (double)$arrayData['surveyAccuracy'];
												}
												else 
												{
													$accuracy = 0;
												}
												$aData = $accuracy;
												//@JAPR 2014-05-20: Corregido un bug, faltaba el break
												break;
											//@JAPR
											case sfidSyncDate:
												$aData = $syncDate;
												break;
											case sfidSyncHour:
												$aData = $syncTime;
												break;
											case sfidServerStartDate:
												if (isset($arrayData["serverSurveyDate"])) {
													$aData = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
												}
												break;
											case sfidServerStartHour:
												if (isset($arrayData["serverSurveyHour"])) {
													$aData = (string) @$arrayData["serverSurveyHour"];
												}
												break;
											case sfidServerEndDate:
												if (isset($arrayData["serverSurveyEndDate"])) {
													$aData = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
												}
												break;
											case sfidServerEndHour:
												if (isset($arrayData["serverSurveyEndHour"])) {
													$aData = (string) @$arrayData["serverSurveyEndHour"];
												}
												break;
											case sfidScheduler:
												$aData = null;	//@JAPRPendiente
												break;
											case sfidEMail:
												$aData = null;	//@JAPRPendiente
												break;
											case sfidDuration:
												$aData = null;	//@JAPRPendiente
												break;
											case sfidAnsweredQuestions:
												$aData = null;	//@JAPRPendiente
												break;
											case sfidEntrySection:
												$aData = 0;		//@JAPRPendiente
												break;
											case sfidDynamicPage:
												$aData = null;	//@JAPRPendiente
												break;
											case sfidDynamicOption:
												$aData = null;	//@JAPRPendiente
												break;
											case safidSourceRowKey:
											case sfidFactKey:
												$aData = (int) @$aSourceActionData["sourceRow_key"];
												break;
											case sfidEntryID:
												$aData = (int) $factKeyDimVal;
												break;
											//@JAPR 2013-05-03: Agregados campos extras al mapeo de datos general de la encuesta
											case sfidUserID:
												$aData = (int) @$theUser->UserID;
												break;
											//@JAPR 2013-05-06: Agregados campos extras al mapeo de datos general de la encuesta
											case sfidUserName:
												$aData = (string) @$theAppUser->UserName;
												break;
											//@JAPR 2013-06-10: Agregados los campos para el desglose de la dirección basada en el GPS
											case sfidGPSCountry:
												$aData = (string) @$arrGPSData['Country'];
												break;
											case sfidGPSState:
												$aData = (string) @$arrGPSData['State'];
												break;
											case sfidGPSCity:
												$aData = (string) @$arrGPSData['City'];
												break;
											case sfidGPSZipCode:
												$aData = (string) @$arrGPSData['ZipCode'];
												break;
											case sfidGPSAddress:
												$aData = (string) @$arrGPSData['Address'];
												break;
											case sfidGPSFullAddress:
												$aData = (string) @$arrGPSData['FullAddress'];
												break;
											//*********************************************************************************************
											//Campos exclusivos para acciones
											//*********************************************************************************************
											case safidDescription:
												$aData = (string) @$aSourceActionData["description"];
												break;
											//@JAPR 2014-05-27: Agregado el mapeo del título de la acción
											case safidTitle:
												$aData = (string) @$aSourceActionData["title"];
												break;
											//@JAPR
											case safidResponsible:
												$intUserID = (int) @$aSourceActionData["user"];
												if ($intUserID <= 0) {
													$intUserID = 1;
												}
												
												if ($inteBavelQTypeID == qtpOpenNumeric) {
													//Si es un campo numérico entonces se graba directamente el ID del usuario
													$aData = $intUserID;
												}
												else {
													//En este caso se debe obtener el nombre del usuario
													if (!isset($arrUserNames[$intUserID])) {
														$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
														//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
														$objResponsible = BITAMeFormsUser::NewInstanceWithID($theRepository, $intUserID);
														$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_NUM);
														if (!is_null($objResponsible)) {
															$strResponsibleName = $objResponsible->FullName;
														}
														$arrUserNames[$intUserID] = $strResponsibleName;
													}
													$strResponsibleName = (string) @$arrUserNames[$intUserID];
													$aData = $strResponsibleName;
												}
												break;
											case safidCategory:
												$aData = (string) @$aSourceActionData["category"];
												break;
											case safidDueDate:
												$aData = (string) @$aSourceActionData["deadline"];
												break;
											case safidStatus:
												$aData = $strDefaultActionStatus;
												break;
											case safidSource:
												$aData = $strDefaultActionSource;
												break;
											case safidSourceID:
												if ($inteBavelQTypeID == qtpOpenNumeric) {
													//Si es un campo numérico entonces se graba directamente el ID del usuario
													$aData = $surveyID;
												}
												else {
													//En este caso se debe obtener el nombre del usuario
													$aData = $surveyInstance->SurveyName;
												}
												break;
											//@JAPR 2014-10-27: Agregado el mapeo de Constantes fijas
											case sfidFixedNumExpression:
												$aData = (float) @$aSourceActionData["value".$aneBavelFieldID]; 
												break;
											case sfidFixedStrExpression:
												$aData = (string) @$aSourceActionData["value".$aneBavelFieldID]; 
												break;
											//@JAPR 2014-10-29: Agregado el mapeo de Scores
											case sfidSurveyScore:
												$aData = (int) @$_POST['SurveyScore'];
												break;
											//@JAPR 2014-10-30: Agregado el mapeo de atributos de catálogos
											case sfidCatalogAttribute:
												$aData = (string) @$aSourceActionData["attribValue".$intAttributeID]; 
												break;
											//@JAPR
											default:
												//Campos no conocidos regresarán null para que no se consideren
												if (getParamValue('DebugBreak', 'both', '(int)', true)) {
													ECHOString("Unknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");
												}
												@logeBavelString("Unknown field ({{$intSurveyFieldID}}): {$aneFormsFieldID} => {$aneBavelFieldID}");
												break;
										}
									}
									
									//Solo inserta el dato si no es null el valor o si se saltó por requerir trato especial, en cuyo caso en el switch
									//se habría insertado
									if (!$blnSkipField && !is_null($aData)) {
										$anActionData[$arreBavelFieldInfo['id']] = $aData;
									}
								}
								//@JAPR
							}
							
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("eBavel path: $strPath");
								ECHOString("eBavel app: $eBavelAppCodeName");
								ECHOString("eBavel table: $eBavelFormTable");
							}
							@logeBavelString("eBavel path: $strPath");
							@logeBavelString("eBavel app: $eBavelAppCodeName");
							@logeBavelString("eBavel table: $eBavelFormTable");
							
							//Inicializa datos fijos adicionales
							$anActionData["id_".$eBavelFormTable] = -1;
							
							//Invoca al grabado de eBavel
							$manager = new Tables();
							//@JAPR 2013-05-03: Agregado el control de errores con eBavel para evitar que interrumpa la ejecución del script y
							//simplemente lance una excepción que puede ser atrapada por eForms
							@$manager->notifyErrorService = true;
							//@JAPR
							$manager->applicationCodeName = $eBavelAppCodeName;
							$manager->init($theRepository->ADOConnection);
							$insertData = array();
							$insertData[-1] = $anActionData;
							
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("eBavel insert data in table: $eBavelFormTable");
								PrintMultiArray($insertData);
							}
							@logeBavelString("eBavel insert data in table: $eBavelFormTable");
							if (@$blnSaveeBavelLogStringFile) {
								$strLogText = (string) @var_export(@$insertData, true);
								@logeBavelString($strLogText);
							}
							
							//@JAPR 2014-10-14: Agregada la edición de datos en eBavel
							//@JAPR 2014-11-28: Agregado el parámetro para permitir que los campos de eBavel que utilizan catálogo
							//puedan recibir siempre las descripciones y así no se confunda pensando que lo que recibe es el key
							$arrParams = array('alldescription' => true);
							$manager->insertData($eBavelFormTable, $insertData, array(), $theRepository->ADOConnection, 'overwrite', $arrParams);
						} catch (Exception $e) {
							//Por el momento no reportará este error
							$streBavelException = (string) @$e->getMessage();
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								ECHOString("eBavel exception saving action data: ".$streBavelException);
							}
							@logeBavelString("eBavel exception saving action data: ".$streBavelException);
							$thelog = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $streBavelException . " POST = " . print_r($_POST,true);
							$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
							@error_log($thelog, 3, $thefile);
							
							$stat_action = "[".$friendlyDate."] [Action: eBavel form action saving error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
							@error_log($stat_action, 3, $statlogfile);
						}
					}
					$theRepository->ADOConnection->SetFetchMode(ADODB_FETCH_ASSOC);
		        	
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("Finishing eBavel action saving process");
					}
					@logeBavelString("Finishing eBavel action saving process");
		        }
			} else {
				$strStatus = "OK";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("Fecha repetida", 1, 1, "color:purple;");
				}

			}
			//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
			//varios requests al mismo tiempo
			if (!$blnIsAgent) {
				@flock($handle, LOCK_UN);
			}
			//@JAPR
        }
		//@JAPR 2015-04-01: Validado que si se trata de un request del Agente, no verifique la existencia del Lock para que permita procesar
		//varios requests al mismo tiempo
		if (!$blnIsAgent) {
        	@fclose($handle);
		}
		//@JAPR
	} else {
		$strStatus = translate("Error saving answers.")." ".translate("The writing permissions must be assigned in the log directory for the IIS user.")." ".translate("Please contact your System Administrator");
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Entry saving process finished: '{$strStatus}'");
	}
	
	//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
	//Almacena si realmente hubo o no error, ya que cuando las capturas son desde móviles siempre se regresa que todo estuvo Ok, sin embargo cierta
	//funcionalidad reciente debe realmente regresar un código de error al móvil para impedir continuar
	$blnSavingError = false;
	
	if(!$saveDataForm)
	{	//Cuando no se graba la forma es cuando se eligen ciertos destinos a reprocesar
		$strStatus = "Survey data was saved successfully.";
		if (getParamValue('DebugBreak', 'both', '(int)', true)){
			ECHOString("Survey data was saved successfully.", 1, 1, "color:purple;");
		}
	}
	else
	{
	if ($strStatus == "" || $strStatus == "OK") {
        $strStatus = "Survey data was saved successfully.";
		
		//guardamos en el log
		$stat_action = "[".$friendlyDate."] [Action: Upload] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
		@error_log($stat_action, 3, $statlogfile);

		//@AAL 17/06/2015 Agregado para actualizar el campo Status el cual indica si se grabaron los Datos capturados de la encuesta (0 => No grabado, 1 => grabado)
		$UserID = $theRepository->DataADOConnection->Quote(getParamValue('UserID', 'both', "(string)"));
		$sql = "UPDATE SI_SV_SurveyLog SET Status = 1 WHERE SurveyID = {$surveyID} AND UserID = {$UserID} AND SurveyDate = '{$lastDate}'";
		$theRepository->DataADOConnection->Execute($sql);
    } else {
		//Guardamos en el log de errores
		$thelog = "[".$friendlyDate."] [Action: Upload error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\nError saving survey data: " . $strStatus . " POST = " . print_r($_POST,true);
		$thefile = "./uploaderror/surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
		@error_log($thelog, 3, $thefile);
        
		//guardamos en el log ($statlogfile = "./uploaderror/eforms.log";)
		$stat_action = "[".$friendlyDate."] [Action: Upload error] [UserID: ".$theUser->UserID."] [UserName: ".$theUser->UserName."] [Repository: ".$theRepository->RepositoryName."] [SurveyID: ".$surveyID."] [SurveyDate: ".$lastDate."]\r\n";
		@error_log($stat_action, 3, $statlogfile);
		
		//@JAPR 2013-02-26: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
		//Si el proceso de grabado no debe regresar el error al App, entonces se debe respaldar la información para que posteriormente sea corregida
		//mediante el proceso de ReUpload, para esto se empieza copiando el archivo log que contiene los datos del POST y posteriormente
		//identificará el resto de los archivos de la sincronización en base al outbox recibido
		if (!$dieOnError) {
			//Respalda también el archivo de log
			$strSrcLogFile = getcwd()."\\"."uploaderror\\surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
			$strTrgLogFile = getcwd()."\\"."syncerrors\\{$theRepository->RepositoryName}\\surveyerrorlog_".$theRepository->RepositoryName."_".$theUser->UserID."_".$logdate.".log";
			@copy($strSrcLogFile, $strTrgLogFile);
			
			$dieOnError = !((bool) @backUpSyncOutboxData());
			
			//@JAPR 2014-02-20: Agregado el reporte de error incluso para las capturas Web
			//No se removerá esté código, porque cuando es captura móvil se debe verificar que todo se respalde correctamente, por eso se vuelve a
			//validar contra la variable $dieOnError antes de determinar si va o no a continuar con el grabado
			//Envía la notificación del error vía EMail para que empiece a ser atendido, este proceso no debe generar otro error, así que se
			//atrapa cualquier posible error
			if (!$dieOnError) {
				try {
					@reportSurveyEntryError($theRepository, $strStatus, $theUser);
				}
				catch (Exception $e) {
					//No reporta mas errores, continua para indicar al App que todo estuvo ok pues para este momento ya respaldó los archivos
					//con los que se puede restaurar la captura
				}
			}
		}
		else {
			//@JAPR 2014-02-20: Agregado el reporte de error incluso para las capturas Web
			//Agregado este envió en forma separada, porque si entra aquí es seguro que es captura Web, así que ahora se notificarán también esos
			//errores vía EMail
			try {
				@reportSurveyEntryError($theRepository, $strStatus, $theUser);
			}
			catch (Exception $e) {
				//No reporta mas errores, continua para indicar al App que todo estuvo ok pues para este momento ya respaldó los archivos
				//con los que se puede restaurar la captura
			}
		}
		//@JAPR
		
		//@JAPR 2012-10-23: Agregada la variable para interrumpir el proceso en caso de errores
		if ($dieOnError) {
			$strStatus = "There was an error while saving the outbox, if you do not found it on the reports page, please contact your System Administrator. ".$strStatus;
		}
		else {
			$strStatus = "Survey data was saved successfully.";
		}
		//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
		//Almacena si realmente hubo o no error, ya que cuando las capturas son desde móviles siempre se regresa que todo estuvo Ok, sin embargo cierta
		//funcionalidad reciente debe realmente regresar un código de error al móvil para impedir continuar
		$blnSavingError = true;
    }
	
	//Realiza actualizaciones adicionales de información que se afecta cada vez que se graba correctamente una captura de forma
	//@JAPRWarning: Analizar si se debe mover esto, ya que seguramente este punto es demasiado tarde para ejecutarlo
	if (!$blnSavingError && isset($factKeyDimVal) && $factKeyDimVal > 0) {
		//@JAPR 2014-11-07: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		//Actualiza la agenda indicando que se ha realizado la captura de la forma de CheckIn
		$agendaID = getParamValue('agendaID', 'both', "(int)");
		if ($agendaID > 0) {
			//@JAPR 2015-09-07: Corregido un bug, se estaba enviando el parámetro de repositorio con una variable equivocada que no estaba asignada (#GG2YLO)
			//@JAPR 2016-09-08: Removida la @ pues causaba que se omitieran errores en este proceso
			$objAgenda = BITAMSurveyAgenda::NewInstanceWithID($theRepository, $agendaID);
			if (!is_null($objAgenda)) {
				$intCheckInSurvey = (int) @$objAgenda->CheckinSurvey;
				if ($intCheckInSurvey == $surveyInstance->SurveyID) {
					//Se debe actualizar la fecha de captura de la encuesta de CheckIn de esta captura que es de agenda
					$currentDate = $arrayData["surveyDate"];
					$surveyHour = $arrayData["surveyHour"];
					$currentDate = substr($currentDate, 0, 10)." ".substr($surveyHour, 0, 8);
					//@JAPR 2015-09-07: Corregido un bug, se estaba enviando el parámetro de repositorio con una variable equivocada que no estaba asignada (#GG2YLO)
					$sql = "UPDATE SI_SV_SurveyAgenda SET 
							EntryCheckInStartDate = ".$theRepository->DataADOConnection->DBTimeStamp($currentDate)." 
						WHERE AgendaID = {$agendaID}";
					$theRepository->DataADOConnection->Execute($sql);
					//@JAPR
				}
			}
		}
	}
	
	//@JAPR 2014-03-31: Agregado el envío de datos de la encuesta hacia un servicio externo
	//Verifica si hay un archivo para procesar el grabado de datos habilitado, si lo hay, prepara una perición con el array de respuestas (tal como
	//llegaron desde la captura) para que dicho servicio se encargue de realizar su proceso que es independiente al grabado de eForms
	//Hay un conjunto de parámetros fijos que se mandan (todos ellos como POST), entre ellos:
	//UserID: La cuenta del usuario que realizó la captura
	//SurveyDate: La fecha y hora de la captura
	//SurveyID: El ID de la encuesta capturada
	//Data: El conjunto de respuestas indexadas por el ID de la pregunta (el array como viene del archivo .dat + la propiedad Type agregada abajo)
	require_once('settingsvariable.inc.php');
	//@JAPR 2014-07-09: Corregido un bug, la referencia a la configuración estaba equivocada, así que nunca realizaba este proceso
	$objSetting = @BITAMSettingsVariable::NewInstanceWithName($theRepository, 'CUSTOMIZEDSTORESURVEYSERVICE');
	//@JAPR 2014-11-12: Corregido un bug no reelevante, cuando se intentaba hacer el grabado de una captura que ya se encontraba en el server,
	//intentaba usar la variable $factKeyDimVal la cual nunca se asignaba pues para ello tiene que hacer un grabado y en esos casos se omite
	//dicho proceso por ya encontrarse la captura previamente
	if (!$blnSavingError && isset($factKeyDimVal) && $factKeyDimVal > 0 && !is_null($objSetting) && trim($objSetting->SettingValue) != '') {
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Customized survey data saving service enabled");
		}
		
		//Realiza la invocación al grabado. Si hay alguna respuesta, se asume que es un error así que debe hacer rollback de la captura eliminando
		//el reporte que acaba de ser generado (aunque para este momento, ya se envió el reporte PDF vía EMail)
		try {
			//Agrega el ID de captura por si se utiliza para generar el ID de reporte
			$_POST['factKeyDimVal'] = $factKeyDimVal;
			$blnSavingError = trim((string) @saveAnswersCustomized($theRepository));
			if ($blnSavingError != '') {
				$strStatus = $blnSavingError." ({$objSetting->SettingValue})";
			}
			$blnSavingError = ($blnSavingError != '');
		} catch (Exception $e) {
			$blnSavingError = true;
			$strStatus = $e->getMessage()." ({$objSetting->SettingValue})";
		}
		
		//Si hubo un error durante la captura en este proceso, debe eliminar la captura y notificar al dispositivo que la envió (esto NO está
		//preparado para funcionar con el Agente de eForms, ya que el agente no permite retroalimentación al dispositivo de captura)
		if ($blnSavingError) {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Customized survey saving process finished with errors: {$strStatus}", 2, 1);
			}
			
			//@JAPRWarning: Preparar un método de Rollback que no dependa de FactKey
			//Primero obtiene el FactKey correspondiente con el registro de la sección estándar de la encuesta
			$intFactKey = 0;
			$sql = "SELECT A.FactKey 
				FROM {$surveyInstance->SurveyTable} A 
				WHERE A.FactKeyDimVal = {$factKeyDimVal} AND A.EntrySectionID = 0";
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS && !$aRS->EOF) {
				$intFactKey = (int) @$aRS->fields["factkey"];
			}
			
			if ($intFactKey > 0) {
				require_once("report.inc.php");
				$anInstance = BITAMReport::NewInstanceWithID($theRepository, (int) $intFactKey, $surveyID);
				if (!is_null($anInstance)) {
					$anInstance->remove();
				}
			}
		}
		else {
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Customized survey saving process finished without errors<br>", 2, 1);
			}
		}
	}
	//@JAPR
    }
    @header("Content-Type: text/plain");
	//@JAPR 2015-09-07: Este ECHOString si se debe generar sin validar con el parámetro, ya que el request lo espera
	ECHOString($strStatus, 2, 2, "color:".(($blnSavingError)?"red":"blue"));
	//@JAPR
}

//@JAPR 2016-04-01: Agregado el tipo de pregunta Update Data
/* Graba exclusivamente el destino de datos recibido como parámetro usando los datos de la captura recibida. Originalmente usado sólo para preguntas tipo Update Data, cuando se recibía
el request de dicha pregunta para grabar en eBavel con el destino de datos configurado los datos parciales recibidos de la captura
*/
function saveDataDestinationV6($aRepository, $aUser, $aDataDestinationID) {
	global $jsonData;
	global $syncDate;
	global $syncTime;
	global $arrGPSData;
	if (!isset($arrGPSData)) {
		$arrGPSData = array();
	}
	
    $return = array();
    $return['error'] = false;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing single data destination from eForms entry: DataDestinationID == {$aDataDestinationID}", 1, 1, "color:purple;");
	}
	
	$theRepository = $aRepository;
	$theUser = $aUser;
    $theAppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
	if (is_null($theRepository) || is_null($theUser) || is_null($theAppUser)) {
		$return['errmsg'] = 'Error reading the entry data';
		return $return;
	}
	
	$thisDate = date("Y-m-d H:i:s");
	if(isset($_POST["surveyEndDate"]) && isset($_POST["surveyEndHour"]))
	{
		$thisDate = substr($_POST["surveyEndDate"], 0, 10)." ".substr($_POST["surveyEndHour"], 0, 8);
	}
	
	if(isset($_POST["surveyDate"]) && isset($_POST["surveyHour"]))
	{
		$lastDate = substr($_POST["surveyDate"], 0, 10)." ".substr($_POST["surveyHour"], 0, 8);
	} else {
		$lastDate = $thisDate;
	}
	
	$surveyID = 0;
	if (array_key_exists("surveyID", $_POST)) {
		$surveyID = (int) $_POST["surveyID"];
	}
	
    $surveyInstance = BITAMSurvey::NewInstanceWithID($theRepository, $surveyID);
	if (is_null($surveyInstance)) {
		$return['errmsg'] = 'Error reading the entry data';
		return $return;
	}
	
    $arrayData = $_POST;
    foreach ($arrayData as $key => $value) {///////AQUI
        if (is_array($value)) {
            foreach ($value as $key1 => $value1) {
            	//@JAPR 2013-04-10: Agregado un nivel mas de conversión de arrays, si se requiere otro ya mejor cambiar a llamadas recursivas
            	if (is_array($value1)) {
            		foreach ($value1 as $key2 => $value2) {
            			$arrayData[$key][$key1][$key2] = $arrayData[$key][$key1][$key2];
            		}
            	}
            	else {
                	$arrayData[$key][$key1] = $arrayData[$key][$key1];
            	}
            	//@JAPR
            }
        } else {
            $arrayData[$key] = $arrayData[$key];
        }
    }/////AQUI
    
    //Agregamos el usuario de artus y de la aplicacion logueado en el arreglo $arrayData
    $arrayData["userID"] = $_SESSION["PABITAM_UserID"];
    $arrayData["appUserID"] = $_SESSION["PAtheAppUserID"];
	
	require_once('report.inc.php');
	//$instanceiExport = BITAMReport::NewInstanceToFormsLogWithIDV6($theRepository, @$lastDate, $surveyID, @$theUser->UserID,  true, false);
	//Esta instancia sólo se requiere para destinos de datos que se basan en exportaciones (XLS, HTTP, Google Drive, etc.) así que se genera vacía porque en este punto no hay un captura grabada
	$instanceiExport = BITAMReport::NewInstanceEmpty($theRepository, $surveyID);
	
	require_once( 'SyncDataDestination.class.php' );
	
	//En este caso no hay un ID de captura todavía, así que se asigna -1
	$factKeyDimVal = -1;
	$aSyncDataDestination = SyncDataDestination::NewInstance($theRepository, $surveyInstance, $factKeyDimVal, $jsonData);
	$syncDataDestinationOpt=array(
		'arrayData' =>$arrayData,
		'thisDate' =>$thisDate,
		'syncDate' =>$syncDate,
		'syncTime' =>$syncTime,
		'aSourceActionData' =>@$aSourceActionData,
		'theAppUser'=>$theAppUser,
		'arrGPSData'=>$arrGPSData,
		//@JAPR 2018-01-03: Agregado el mapeo del UUID (#TDYU15)
		'arrJSONData'=>$jsonData
		//@JAPR
		);
	$aSyncDataDestination->options=$syncDataDestinationOpt;
	$aSyncDataDestination->instaceExport = $instanceiExport;
	
	/*require_once('eBavelIntegration.inc.php');
	$inteBavelFormID = 0;
	$arreBavelFormColl = @GetEBavelSections($theRepository, false, null, array('FRM_210725702A0F516DF9'), true);
	if (!is_null($arreBavelFormColl) && is_array($arreBavelFormColl) && isset($arreBavelFormColl[0]) && is_array($arreBavelFormColl[0])) {
		$inteBavelFormID = (int) @$arreBavelFormColl[0]['id_section'];
		if ($inteBavelFormID) {
			$arreBavelFieldsColl = @GetEBavelFieldsForms($theRepository, true, null, array($inteBavelFormID));
		}
	}*/
	
	//@JAPR 2016-04-04: Agregado el tipo de pregunta Update Data
	//Para la pregunta Update Data debe regresar las llaves insertadas a las diferentes formas de eBavel, de tal manera que si es necesario las pueda limpiar en una llamada subsecuente
	//Debido a que constantemente eBavel genera Warnings que impediría la formación de una respuesta JSON, va a utilizar el buffer de salida para ignorar cualquier warning generado por eBavel
	ob_start();
	$arrDataKeys = $aSyncDataDestination->sync(array($aDataDestinationID));
	if (is_null($arrDataKeys) || !is_array($arrDataKeys)) {
		$arrDataKeys = array();
	}
	$streBavelWarnings = trim(ob_get_clean());
	ob_end_clean();
	if ($streBavelWarnings != '') {
		$return['warnings'] = $streBavelWarnings;
	}
	//@JAPR
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Inserted destination keys", 1, 1, "color:purple;");
		if (is_array($arrDataKeys)) {
			PrintMultiArray($arrDataKeys);
		}
	}
	
    //$return['errmsg'] = 'Error getting survey definition list';
	//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
	//Ahora el array $arrDataKeys regresará tanto los Keys como la validación de condición (respuesta)
    $return['answer'] = (isset($arrDataKeys['answer']))?$arrDataKeys['answer']:'False';
	if (isset($arrDataKeys['keys'])) {
		$return['keys'] = $arrDataKeys['keys'];
	}
    
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Finishing single data destination from eForms entry", 1, 1, "color:purple;");
	}
    return $return;
}
//@JAPR

//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
//Agregado el parámetro $factKeyDimVal para grabar el id de la captura en la bitácora
function insertDataDestinationModelLog($success,$destination,$surveyID,$user, $factKeyDimVal){
// @EA 2015-10-15
   $SurveyTaskID=getParamValue('TaskID', 'both', '(int)', true);
   if ($SurveyTaskID > 0) {
	   $creationDateID=date("Y-m-d H:i:s");
	   //@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
	   //En este punto siempre debería haber un Id de captura asignado o no habría llegado hasta aquí
	   $strSQL="INSERT INTO SI_SV_SurveyDestinationLog (`surveytasklogid`,`creationdateid`,`destination`,`description`,`status`,`SurveyID`,`userID`, `destinationid`, `SurveyKey`) 
		VALUES({$SurveyTaskID},'{$creationDateID}','{$destination}','{$success["msg"]}',{$success["error"]},{$surveyID},'{$user}','-1', {$factKeyDimVal})";
	   //@JAPR
	   $aBITAMConnection = @connectToKPIRepository(); 
	   if ($aBITAMConnection)
	   	$aBITAMConnection->ADOConnection->Execute($strSQL);
   }
}

/* Graba el registro único de la Tabla de la Forma, el cual define los datos generales de la captura que se está grabando
Esta función genera el ID que se reutilizará en el resto de las funciones de grabado mediante el parámetro $factKeyDimVal
//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
*/
function saveDataInSurveyTableV6($aRepository, $surveyID, $arrayData, $thisDate, &$factKeyDimVal) {
	global $gblEFormsErrorMessage;
	global $syncDate;
	global $syncTime;
	global $usePhotoPaths;
	global $blnWebMode;
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	global $appVersion;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $arrGPSData;
	global $saveDataForm;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDataInSurveyTableV6 SurveyID == {$surveyID}, SurveyDate == {$thisDate}", 2, 0, "color:darkgreen;");
	}
	
	$strEFormsNA = $aRepository->DataADOConnection->Quote($gblEFormsNA);
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if (is_null($syncDate) || trim((string) $syncDate) == '') {
		$syncDate = date("Y-m-d");
	}
	if (is_null($syncTime) || trim((string) $syncTime) == '') {
		$syncTime = date("H:i:s");
	}
	
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	//Ahora estos datos se obtendrán desde el parámetro, ya que será el Agente quien lo enviará, en caso de no venir asignado se asume que se trata de una captura directa así que
	//utilizará en ese caso la fecha y hora actual
	$strSyncDate = @$arrayData["syncDate"];
	if (is_null($strSyncDate)) {
		$strSyncDate = $syncDate;
		$strSyncTime = $syncTime;
	}
	else {
		$strSyncTime = formatDateTime($strSyncDate, "HH:MM:SS");
	}
	
	$currentDate = (string) @$arrayData["surveyDate"];
	$startDate = (string) @substr($arrayData["surveyDate"], 0, 10);
	$endDate = substr($thisDate, 0, 10);
	$surveyHour = (string) @$arrayData["surveyHour"];
	$startTime = (string) @$arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	$serverStartDate = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
	$serverStartTime = (string) @$arrayData["serverSurveyHour"];
	$serverEndDate = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
	$serverEndTime = (string) @$arrayData["serverSurveyEndHour"];
	$appUserID = (int) @$arrayData["appUserID"];
	$latitude = (double) @$arrayData["surveyLatitude"];
	$longitude = (double) @$arrayData["surveyLongitude"];
	$accuracy = (double) @$arrayData["surveyAccuracy"];
	$dblSyncLat = (double) @$arrayData["syncLat"];
	$dblSyncLong = (double) @$arrayData["syncLong"];
	$dblSyncAcc = (double) @$arrayData["syncAcc"];
	$agendaID = (int) @$arrayData["agendaID"];
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	//Fechas de última edición del outbox local
	$editStartDate = substr((string) @$arrayData["editStartDate"], 0, 10);
	$editStartHour = (string) @$arrayData["editStartHour"];
	$editEndDate = substr((string) @$arrayData["editEndDate"], 0, 10);
	$editEndHour = (string) @$arrayData["editEndHour"];
	//Fechas de última edición del outbox local según el server
	$editServerStartDate = substr((string) @$arrayData["editServerStartDate"], 0, 10);
	$editServerStartHour = (string) @$arrayData["editServerStartHour"];
	$editServerEndDate = substr((string) @$arrayData["editServerEndDate"], 0, 10);
	$editServerEndHour = (string) @$arrayData["editServerEndHour"];
	//@JAPR
	
	//Obtenemos los valores de CaptureVia, CaptureEmail y SchedulerID
	//para verificar si dichos valores los estan dando de alta un correo electronico
	$captureVia = (int) @$arrayData["CaptureVia"];
	$captureEmail = (string) @$arrayData["CaptureEmail"];
	$schedulerID = (int) @$arrayData["SchedulerID"];
	//Si se trata de una captura de Scheduler de EMail, entonces no hay un usuario específico asociado a la captura, sino que se graba el EMail de quien captura
	//y que no necesariamente es una cuenta de usuario de KPIOnline
	if ($captureVia == 1 && trim($captureEmail) != "" && $schedulerID > 0) {
		$userID = -1;
	}
	else {
		$userID = $arrayData["userID"];
	}
	$strUserName = getParamValue('UserID', 'both', '(string)');
	
	//Prepara los datos para el Insert, en su mayoría son keys a la dimensión del cubo global de formas, el cual para este punto ya debe estar creado y haber
	//generado todas las dimensiones correspondientes, pero en caso de no existir, se procederá a continuar con el grabado sin generar error para permitir un
	//reupload mas adelante
	$strAdditionalFields = "";
	$strAdditionalValues = "";
	
	//DateKey- ID de la dimension DimTime equivalente al StartDateID (no se consideran horas, sólo la fecha), se obtiene a partir del cubo global de surveys, ya que
	//es el único cubo que se tiene certeza que debe existir en este punto, y todos los cubos de Model Manager como este utilizan la misma estructura
	//Este es uno de los únicos valores que se encuentran normalizados en la tabla de tiempo, pero no es requerido para consulta como tal
	$strOriginalWD = getcwd();
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");
	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);
	
	//@JAPRWarning: Falta reportar error si no se puede encontrar alguna dimensión, pero ese error no será showStopper sino que se mostrará al final en un log
	if (!is_null($anInstanceModel)) {
		$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $surveyInstance->GblSurveyModelID);
		//En este punto la fecha de la tabla de tiempo sólo corresponde a la porción fecha sin la hora de captura
		$dateKey = getDateKeyV6($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate);
		if ($dateKey === false) {
			return $gblEFormsErrorMessage;
		}
		
		$strAdditionalFields .= ", DateKey";
		$strAdditionalValues .= ", ".$dateKey;
	}
	
	//StartDate- Fecha inicial de la captura (NO se altera durante la edición de la captura)
	$strAdditionalFields .= ", StartDate";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($startDate);
	
	//StartTime- Hora en formato 00:00:00 correspondiente con la hora del inicio de la captura (NO se altera durante la edición de la captura)
	$strAdditionalFields .= ", StartTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($startTime);
	
	//EndDate- Fecha final de la captura (SI se altera durante la edición de la captura)
	$strAdditionalFields .= ", EndDate";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($endDate);
	
	//EndTime- Hora en formato 00:00:00 correspondiente con la hora del final de la captura (SI se altera durante la edición de la captura)
	$strAdditionalFields .= ", EndTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($endTime);
	
	//Estos valores no se asignan durante la captura inicial, sólo durante le proceso de edición vía Web
	//StartDateEdit- Fecha inicial de la última edición de la captura (SI se altera durante la edición de la captura)
	//EndDateEdit- Fecha final de la última edición de la captura (SI se altera durante la edición de la captura)
	//StartTimeEdit- Hora en formato 00:00:00 correspondiente con la hora inicial de la última edición de la captura (SI se altera durante la edición de la captura)
	//EndTimeEdit- Hora en formato 00:00:00 correspondiente con la hora final de la última edición de la captura (SI se altera durante la edición de la captura)
	$strAdditionalFields .= ", StartDateEdit, EndDateEdit, StartTimeEdit, EndTimeEdit";
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($editStartDate)?$editStartDate:$gblEFormsNA).
		", ".$aRepository->DataADOConnection->Quote(($editEndDate)?$editEndDate:$gblEFormsNA).
		", ".$aRepository->DataADOConnection->Quote(($editStartHour)?$editStartHour:$gblEFormsNA).
		", ".$aRepository->DataADOConnection->Quote(($editEndHour)?$editEndHour:$gblEFormsNA);
	
	//@JAPRWarning: Estos valores realmente corresponden al tiempo de Grabado en las tablas de datos, NO de la sincronización de los archivos de esta captura
	//SyncDate- Fecha inicial de la sincronización de la captura
	$strAdditionalFields .= ", SyncDate";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($strSyncDate);
	
	//@JAPRWarning: Estos valores realmente corresponden al tiempo de Grabado en las tablas de datos, NO de la sincronización de los archivos de esta captura
	//SyncTime- Hora en formato 00:00:00 correspondiente con la hora inicial de la sincronización de la captura
	$strAdditionalFields .= ", SyncTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($strSyncTime);
	
	//SyncEndDate- Fecha final de la sincronización de la captura
	//SyncEndTimeSync- Hora en formato 00:00:00 correspondiente con la hora final de la sincronización de la captura
	$strAdditionalFields .= ", SyncEndDate, SyncEndTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($syncDate).", ".$aRepository->DataADOConnection->Quote($syncTime);
	
	//ServerStartDate- Fecha inicial de la captura según el servidor (NO se altera durante la edición de la captura)
	$strAdditionalFields .= ", ServerStartDate";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($serverStartDate)?$serverStartDate:$gblEFormsNA);
	
	//ServerStartTime- Hora en formato 00:00:00 correspondiente con la hora inicial de la captura según el servidor
	$strAdditionalFields .= ", ServerStartTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($serverStartTime)?$serverStartTime:$gblEFormsNA);
	
	//ServerEndDate- Fecha final de la captura según el servidor (NO se altera durante la edición de la captura)
	$strAdditionalFields .= ", ServerEndDate";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($serverEndDate)?$serverEndDate:$gblEFormsNA);
	
	//ServerEndTime- Hora en formato 00:00:00 correspondiente con la hora final de la captura según el servidor
	$strAdditionalFields .= ", ServerEndTime";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($serverEndTime)?$serverEndTime:$gblEFormsNA);
	
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	//ServerStartDateEdit- Fecha de la última edición del outbox local según el servidor
	$strAdditionalFields .= ", ServerStartDateEdit";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($editServerStartDate)?$editServerStartDate:$gblEFormsNA);
	
	//ServerStartTimeEdit- Hora en formato 00:00:00 correspondiente con la hora de la última edición del outbox local según el servidor
	$strAdditionalFields .= ", ServerStartTimeEdit";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($editServerStartHour)?$editServerStartHour:$gblEFormsNA);
	
	//ServerEndDateEdit- Fecha final de la última edición del outbox local según el servidor
	$strAdditionalFields .= ", ServerEndDateEdit";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($editServerEndDate)?$editServerEndDate:$gblEFormsNA);
	
	//ServerEndTimeEdit- Hora en formato 00:00:00 correspondiente con la hora final de la última edición del outbox local según el servidor
	$strAdditionalFields .= ", ServerEndTimeEdit";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(($editServerEndHour)?$editServerEndHour:$gblEFormsNA);
	//@JAPR
	
	//Latitude- Posición GPS (Latitude)
	$strAdditionalFields .= ", Latitude";
	$strAdditionalValues .= ", ".$latitude;
	
	//Longitude- Posición GPS (Longitude)
	$strAdditionalFields .= ", Longitude";
	$strAdditionalValues .= ", ".$longitude;
	
	//Accuracy- Precisión de la posición GPS
	$strAdditionalFields .= ", Accuracy";
	$strAdditionalValues .= ", ".$accuracy;
	
	//Estos datos se deben obtener desde GoogleMaps a partir de la posición de la captura
	//Country- Porción extraida del GPS de la captura
	//State- Porción extraida del GPS de la captura
	//City- Porción extraida del GPS de la captura
	//ZipCode- Porción extraida del GPS de la captura
	//Address- Porción extraida del GPS de la captura
	$strAdditionalFields .= ", Country, State, City, ZipCode, Address";
	$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote((string) @$arrGPSData['Country']).
		", ".$aRepository->DataADOConnection->Quote((string) @$arrGPSData['State']).
		", ".$aRepository->DataADOConnection->Quote((string) @$arrGPSData['City']).
		", ".$aRepository->DataADOConnection->Quote((string) @$arrGPSData['ZipCode']).
		", ".$aRepository->DataADOConnection->Quote((string) @$arrGPSData['FullAddress']);
	
	//SyncLatitude- Posición GPS (Latitude) de la Sincronización
	$strAdditionalFields .= ", SyncLatitude";
	$strAdditionalValues .= ", ".$dblSyncLat;
	
	//SyncLongitude- Posición GPS (Longitude) de la Sincronización
	$strAdditionalFields .= ", SyncLongitude";
	$strAdditionalValues .= ", ".$dblSyncLong;
	
	//SyncAccuracy- Precisión de la posición GPS de la Sincronización
	$strAdditionalFields .= ", SyncAccuracy";
	$strAdditionalValues .= ", ".$dblSyncAcc;
	
	//@JAPR 2015-08-30: Agregado el grabado de la Agenda que realizó la captura
	$strAdditionalFields .= ", AgendaID";
	$strAdditionalValues .= ", ".$agendaID;
	//@JAPR
	
	//Genera el INSERT del registro en la Tabla de la forma, el Key es automáticamente generado y asignado a la variable de retorno
	//En este punto la fecha si es completa, así que incluye la hora de la captura
	$currentDate = substr($currentDate, 0, 10)." ".substr($surveyHour, 0, 8);

	//GCRUZ 2016-05-18. Agregados campos (TransferTime, TransferDistance) para el tiempo y distancia de traslado para una captura
	$transfer = getSurveyTransferTimeDistance($aRepository, $surveyInstance, $strUserName, $arrayData);
	$strAdditionalFields .= ", TransferTime";
	$strAdditionalValues .= ", ".$transfer['Time'];
	$strAdditionalFields .= ", TransferDistance";
	$strAdditionalValues .= ", ".$transfer['Distance'];
	
	if ($saveDataForm) {
		$sql = "INSERT INTO {$surveyInstance->SurveyTable} (DateID, UserID, eFormsVersionNum, UserEMail".$strAdditionalFields.")
			VALUES (".$aRepository->DataADOConnection->DBTimeStamp($currentDate).", {$userID}, ".
				$aRepository->DataADOConnection->Quote(ESURVEY_SERVICE_VERSION).", ".
				$aRepository->DataADOConnection->Quote($strUserName).$strAdditionalValues.")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("saveDataInSurveyTableV6 INSERT: ".$sql, 1, 1, "color:blue;");
		}
		
		//@JAPR 2015-08-30: Agregadas validaciones de campos no existentes, ejecutará el INSERT y si falla intentará modificar la tabla antes de marcar el
		//error, en caso de fallar nuevamente ya generará el error
		for ($intTries = 0; $intTries < 2; $intTries++) {
			if ($aRepository->DataADOConnection->Execute($sql)) {
				//Obtiene el ID autoincremental que identifica a la captura
				$factKeyDimVal = (int) @$aRepository->DataADOConnection->_insertid();
				if ($factKeyDimVal <= 0) {
					return ("(".__METHOD__.") ".translate("Error calculating the auto-incremental fact key")." (".$surveyInstance->SurveyTable.").".translate("Executing").": ".$sql);
				}
				
				//@JAPR 2018-02-02: Agregada la columna con el Id de la captura a la bitácora de tareas y destinos de datos (#E4BMT2)
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					### Entry info ###
					ECHOString("### Entry info ###");
					ECHOString("SurveyKey: {$factKeyDimVal}");
					ECHOString("### Entry info ###");
				}
				//@JAPR
				
				//En este caso no hubo error, así que continua
				break;
			}
			else {
				//Si era el primer intento, antes de generar el error intenta actualizar la tabla por si faltaban campos y lo vuelve a intentar
				if ($intTries == 0) {
					$surveyInstance->createDataTablesAndFields();
				}
				else {
					//Si hubo un error en la insercion de los datos en esta tabla paralela, tambien 
					//borramos los datos de la tabla paralela matriz y de la tabla de comentarios, imagenes y acciones
					$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
					return ("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
				}
			}
		}
	}
	else
	{
		$sql = "SELECT SurveyKey FROM {$surveyInstance->SurveyTable} 
			WHERE DateID={$aRepository->DataADOConnection->DBTimeStamp($currentDate)} AND UserID = {$userID} ";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
			return ("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
		}
		
		$factKeyDimVal = (int) $aRS->fields["surveykey"];
		if ($factKeyDimVal <= 0) {
			return ("(".__METHOD__.") ".translate("Error getting SurveyKey")." (".$surveyInstance->SurveyTable.").".translate("Executing").": ".$sql);
		}
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("saveDataInSurveyTableV6 SELECT: ".$sql, 1, 1, "color:purple;");
			ECHOString("factKeyDimVal: ".$factKeyDimVal, 1, 1, "color:purple;");
		}
	}
	//@JAPR
	
	return "OK";
}

//GCRUZ 2016-05-18. Obtener el tiempo y distancia de traslado para una captura
function getSurveyTransferTimeDistance($aRepository, $aSurveyInstance, $userID, $arrayData, $bUpdateNextCapture = true)
{
	$surveyTransfer = array();
	$surveyTransfer['Time'] = 0;
	$surveyTransfer['Distance'] = 0;
	require_once('google.address.maps.inc.php');
	
	$startDate = (string) @substr($arrayData["surveyDate"], 0, 10);
	$startTime = (string) @$arrayData["surveyHour"];
	$latitude = (double) @$arrayData["surveyLatitude"];
	$longitude = (double) @$arrayData["surveyLongitude"];
	//Obtener el tiempo, lat, lng de la captura anterior
	$aSQL = "SELECT SurveyDate, Latitude, Longitude FROM SI_SV_SurveyLog WHERE Repository = '".$_SESSION["PABITAM_RepositoryName"]."' AND SurveyDate > ".$aRepository->DataADOConnection->Quote($startDate.' 00:00:00')." AND SurveyDate < ".$aRepository->DataADOConnection->Quote($startDate.' '.$startTime)." AND UserID = ".$aRepository->DataADOConnection->Quote($userID)." AND Deleted = 0 AND Status = 1 ORDER BY SurveyDate DESC LIMIT 1";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
	}
	$aRSLastSurvey = $aRepository->DataADOConnection->Execute($aSQL);
	if ($aRSLastSurvey && $aRSLastSurvey->_numOfRows != 0)
	{
		$LastSurveyDate = (string) @$aRSLastSurvey->fields['surveydate'];
		$timeLastSurvey = strtotime($LastSurveyDate);
		$timeThisSurvey = strtotime($startDate.' '.$startTime);
		$surveyTransfer['Time'] = ($timeThisSurvey - $timeLastSurvey) / 60;

		$LastSurveyLat = (double) @$aRSLastSurvey->fields['latitude'];
		$LastSurveyLng = (double) @$aRSLastSurvey->fields['longitude'];
		if ($LastSurveyLat != 0 && $LastSurveyLng != 0 && $latitude != 0 && $longitude != 0)
			$surveyTransfer['Distance'] = getlatlngDifference($LastSurveyLat, $LastSurveyLng, $latitude, $longitude);
	}

	if ($bUpdateNextCapture)
	{
		//Actualizar siguiente captura, si es que existe
		//Antes de actualizar, asegurar de que las tablas estén creadas
		$aSurveyInstance->createDataTablesAndFields();
		$aSQL = "SELECT SurveyDate, SurveyID, Latitude, Longitude FROM SI_SV_SurveyLog WHERE Repository = '".$_SESSION["PABITAM_RepositoryName"]."' AND SurveyDate > ".$aRepository->DataADOConnection->Quote($startDate.' '.$startTime)." AND SurveyDate < ".$aRepository->DataADOConnection->Quote($startDate.' 23:59:59')." AND UserID = ".$aRepository->DataADOConnection->Quote($userID)." AND Deleted = 0 AND Status = 1 ORDER BY SurveyDate LIMIT 1";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
		}
		$aRSNextSurvey = $aRepository->DataADOConnection->Execute($aSQL);
		if ($aRSNextSurvey && $aRSNextSurvey->_numOfRows != 0)
		{
			$NextSurveyTransferTime = 0;
			$NextSurveyTransferDistance = 0;
			$NextSurveyID = (int) @$aRSNextSurvey->fields['surveyid'];
			$NextSurveyDate = (string) @$aRSNextSurvey->fields['surveydate'];
			$timeNextSurvey = strtotime($NextSurveyDate);
			$timeThisSurvey = strtotime($startDate.' '.$startTime);
			$NextSurveyTransferTime = ($timeNextSurvey - $timeThisSurvey) / 60;

			$NextSurveyLat = (double) @$aRSNextSurvey->fields['latitude'];
			$NextSurveyLng = (double) @$aRSNextSurvey->fields['longitude'];
			if ($NextSurveyLat != 0 && $NextSurveyLng != 0 && $latitude != 0 && $longitude != 0)
				$NextSurveyTransferDistance = getlatlngDifference($latitude, $longitude, $NextSurveyLat, $NextSurveyLng);

			if ($NextSurveyID != $aSurveyInstance->SurveyID)
				$aNextSurveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $NextSurveyID);
			else
				$aNextSurveyInstance = $aSurveyInstance;
			//Get SurveyKey
			$arrNextSurveyDate = explode(' ', $NextSurveyDate);
			$NextSurveyStartDate = @$arrNextSurveyDate[0];
			$NextSurveyStartTime = @$arrNextSurveyDate[1];
			$aSQL = "SELECT SurveyKey FROM {$aNextSurveyInstance->SurveyTable} WHERE UserEMail = ".$aRepository->DataADOConnection->Quote($userID)." AND StartDate = '{$NextSurveyStartDate}' AND StartTime = '{$NextSurveyStartTime}'";
			$aRSNextSurveyKey = $aRepository->DataADOConnection->Execute($aSQL);
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
			}
			if ($aRSNextSurveyKey && $aRSNextSurveyKey->_numOfRows != 0)
			{
				$aSQL = "UPDATE {$aNextSurveyInstance->SurveyTable} SET TransferTime = ".$NextSurveyTransferTime.", TransferDistance = ".$NextSurveyTransferDistance." WHERE SurveyKey = ".@$aRSNextSurveyKey->fields['surveykey'];
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
				}
				$aRepository->DataADOConnection->Execute($aSQL);

				//Guardar en el cubo global de Surveys
				//Primero obtener el FactKey correspondiente
				$aSQL = "SELECT FactKey FROM RIFACT_{$aNextSurveyInstance->GblSurveyModelID} Fact ";
				$aSQL .= "JOIN RIDIM_{$aNextSurveyInstance->GblUserDimID} DUser ON Fact.RIDIM_{$aNextSurveyInstance->GblUserDimID}KEY = DUser.RIDIM_{$aNextSurveyInstance->GblUserDimID}KEY ";
				$aSQL .= "JOIN RIDIM_{$aNextSurveyInstance->GblSurveyDimID} DSurvey ON Fact.RIDIM_{$aNextSurveyInstance->GblSurveyDimID}KEY = DSurvey.RIDIM_{$aNextSurveyInstance->GblSurveyDimID}KEY ";
				$aSQL .= "JOIN RIDIM_{$aNextSurveyInstance->GblStartDateDimID} DSurveyStartDate ON Fact.RIDIM_{$aNextSurveyInstance->GblStartDateDimID}KEY = DSurveyStartDate.RIDIM_{$aNextSurveyInstance->GblStartDateDimID}KEY ";
				$aSQL .= "JOIN RIDIM_{$aNextSurveyInstance->GblStartTimeDimID} DSurveyStartTime ON Fact.RIDIM_{$aNextSurveyInstance->GblStartTimeDimID}KEY = DSurveyStartTime.RIDIM_{$aNextSurveyInstance->GblStartTimeDimID}KEY ";
				$aSQL .= " WHERE DUser.DSC_{$aNextSurveyInstance->GblUserDimID} = ".$aRepository->DataADOConnection->Quote($userID);
				$aSQL .= " AND DSurvey.DSC_{$aNextSurveyInstance->GblSurveyDimID} = ".$aRepository->DataADOConnection->Quote($aNextSurveyInstance->SurveyName);
				$aSQL .= " AND DSurveyStartDate.DSC_{$aNextSurveyInstance->GblStartDateDimID} = '{$NextSurveyStartDate}'";
				$aSQL .= " AND DSurveyStartTime.DSC_{$aNextSurveyInstance->GblStartTimeDimID} = '{$NextSurveyStartTime}'";
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
				}
				$aRSNextSurveyFactKey = $aRepository->DataADOConnection->Execute($aSQL);
				if ($aRSNextSurveyFactKey && $aRSNextSurveyFactKey->_numOfRows != 0)
				{
					$FactKey = @$aRSNextSurveyFactKey->fields['factkey'];
					$aSQL = "UPDATE RIFACT_{$aNextSurveyInstance->GblSurveyModelID} SET IND_{$aNextSurveyInstance->GblTransferTimeIndID} = ".$NextSurveyTransferTime.", IND_{$aNextSurveyInstance->GblTransferDistanceIndID} = ".$NextSurveyTransferDistance." WHERE FactKey = ".$FactKey;
					if (getParamValue('DebugBreak', 'both', '(int)', true)) {
						ECHOString("getSurveyTransferTimeDistance :".$aSQL, 1, 1, "color:purple;");
					}
					$aRepository->DataADOConnection->Execute($aSQL);
				}

			}
		}
	}

	return $surveyTransfer;
}

//GCRUZ 2016-06-02. Proceso de cálculo de tiempo de traslado de agendas
function calculateSurveyAgendaTransfer($aRepository, $surveyID, $agendaID, $arrayData)
{
	//Obtener tiempo de inicio y fin de la propia agenda
	$surveyDate = (string) @substr($arrayData["surveyDate"], 0, 10);
	$aSQL = "SELECT MAX(SurveyDate) AS 'maxtime', MIN(SurveyDate) AS 'mintime', UserID FROM SI_SV_SurveyLog WHERE AgendaID = ".$agendaID." AND Deleted = 0 AND SurveyDate BETWEEN ".$aRepository->DataADOConnection->Quote($surveyDate.' 00:00:00')." AND ".$aRepository->DataADOConnection->Quote($surveyDate.' 23:59:59');
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
	}
	$rsMaxMinTime = $aRepository->DataADOConnection->Execute($aSQL);
	if ($rsMaxMinTime && $rsMaxMinTime->_numOfRows != 0)
	{
		$thisAgendaMinTime = $rsMaxMinTime->fields['mintime'];
		$thisAgendaMaxTime = $rsMaxMinTime->fields['maxtime'];
		$userID = $rsMaxMinTime->fields['userid'];
	}
	else
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("calculateSurveyAgendaTransfer :".'Error al obtener datos de inicio y finalización de la agenda', 1, 1, "color:red;");
		}
		return false;
	}
	
	//Buscar la agenda inmediata anterior y calcular el traslado para esta agenda
	//Validar existencia de campos
	$aSQL = "ALTER TABLE SI_SV_SurveyAgenda ADD TrasladeTime DOUBLE NULL";
	$aRepository->DataADOConnection->Execute($aSQL);
	$aSQL = "SELECT SurveyDate FROM SI_SV_SurveyLog WHERE AgendaID <> ".$agendaID." AND AgendaID <> 0 AND Deleted = 0 AND Status = 1 AND UserID = ".$aRepository->DataADOConnection->Quote($userID)." AND SurveyDate BETWEEN ".$aRepository->DataADOConnection->Quote($surveyDate.' 00:00:00')." AND ".$aRepository->DataADOConnection->Quote($thisAgendaMinTime)." ORDER BY SurveyDate DESC LIMIT 1";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
	}
	$rsPreviousAgenda = $aRepository->DataADOConnection->Execute($aSQL);
	if ($rsPreviousAgenda && $rsPreviousAgenda->_numOfRows != 0)
	{
		//Existe agenda anterior, calcular traslado y guardar en SI_SV_SurveyAgenda
		$transferTime = (strtotime($thisAgendaMinTime) - strtotime($rsPreviousAgenda->fields['surveydate'])) / 60;
		$aSQL = "UPDATE SI_SV_SurveyAgenda SET TrasladeTime = ".$transferTime." WHERE AgendaID = ".$agendaID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
		}
		$aRepository->DataADOConnection->Execute($aSQL);
	}
	else
	{
		//No existe agenda anterior, por lo tanto el tiempo el traslado es 0
		$aSQL = "UPDATE SI_SV_SurveyAgenda SET TrasladeTime = 0 WHERE AgendaID = ".$agendaID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
		}
		$aRepository->DataADOConnection->Execute($aSQL);
	}

	//Buscar la agenda inmediata siguiente y calcular el traslado de ella
	$aSQL = "SELECT AgendaID, SurveyDate FROM SI_SV_SurveyLog WHERE AgendaID <> ".$agendaID." AND AgendaID <> 0 AND Deleted = 0 AND Status = 1 AND UserID = ".$aRepository->DataADOConnection->Quote($userID)." AND SurveyDate BETWEEN ".$aRepository->DataADOConnection->Quote($thisAgendaMaxTime)." AND ".$aRepository->DataADOConnection->Quote($surveyDate.' 23:59:59')." ORDER BY SurveyDate LIMIT 1";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
	}
	$rsNextAgenda = $aRepository->DataADOConnection->Execute($aSQL);
	if ($rsNextAgenda && $rsNextAgenda->_numOfRows != 0)
	{
		//Existe agenda siguiente, calcular su traslado y guardar en SI_SV_SurveyAgenda
		$nextAgendaID = $rsNextAgenda->fields['agendaid'];
		$transferTime = (strtotime($rsNextAgenda->fields['surveydate']) - strtotime($thisAgendaMaxTime)) / 60;
		$aSQL = "UPDATE SI_SV_SurveyAgenda SET TrasladeTime = ".$transferTime." WHERE AgendaID = ".$nextAgendaID;
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("calculateSurveyAgendaTransfer :".$aSQL, 1, 1, "color:blue;");
		}
		$aRepository->DataADOConnection->Execute($aSQL);
	}

	return true;
}

/* Almacena el registro en el cubo global de capturas (Surveys), el cual a partir de esta versión almacenará un único registro tal como lo hace la tabla de los datos de la Forma
En este caso las columnas son sólo campos subrrogados de dimensiones, asi que todas las tablas están normalizadas por definición propia
*/
function saveDataInGlobalSurveyModelV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal) {
	global $gblEFormsErrorMessage;
	global $syncDate;
	global $syncTime;
	global $usePhotoPaths;
	global $blnWebMode;
	global $arrSectionDescByID;
	global $arrSectionDescStrByID;
	global $appVersion;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $arrDefDimValues;
	global $arrGPSData;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDataInGlobalSurveyModelV6 SurveyID == {$surveyID}, SurveyDate == {$thisDate}", 2, 0, "color:darkgreen;");
	}
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	if (is_null($syncDate) || trim((string) $syncDate) == '') {
		$syncDate = date("Y-m-d");
	}
	if (is_null($syncTime) || trim((string) $syncTime) == '') {
		$syncTime = date("H:i:s");
	}
	
	//@JAPR 2015-07-25: Agregado el grabado en el cubo global de Surveys
	//Ahora estos datos se obtendrán desde el parámetro, ya que será el Agente quien lo enviará, en caso de no venir asignado se asume que se trata de una captura directa así que
	//utilizará en ese caso la fecha y hora actual
	$strSyncDate = @$arrayData["syncDate"];
	if (is_null($strSyncDate)) {
		$strSyncDate = $syncDate;
		$strSyncTime = $syncTime;
	}
	else {
		$strSyncTime = formatDateTime($strSyncDate, "HH:MM:SS");
	}
	
	$startDate = (string) @substr($arrayData["surveyDate"], 0, 10);
	$endDate = substr($thisDate, 0, 10);
	$surveyHour = (string) @$arrayData["surveyHour"];
	$startTime = (string) @$arrayData["surveyHour"];
	$endTime = substr($thisDate, 11);
	$currentDate = (string) @$arrayData["surveyDate"];
	$initialTime = strtotime((substr($currentDate, 0, 10)." ".$startTime));
	$finalTime = strtotime((substr($thisDate, 0, 10)." ".$endTime));
	$totalSeconds = $finalTime - $initialTime;
	$totalMinutes = $totalSeconds/60;
	$serverStartDate = substr((string) @$arrayData["serverSurveyDate"], 0, 10);
	$serverStartTime = (string) @$arrayData["serverSurveyHour"];
	$serverEndDate = substr((string) @$arrayData["serverSurveyEndDate"], 0, 10);
	$serverEndTime = (string) @$arrayData["serverSurveyEndHour"];
	$appUserID = (int) @$arrayData["appUserID"];
	$latitude = (double) @$arrayData["surveyLatitude"];
	$longitude = (double) @$arrayData["surveyLongitude"];
	$accuracy = (double) @$arrayData["surveyAccuracy"];
	$dblSyncLat = (double) @$arrayData["syncLat"];
	$dblSyncLong = (double) @$arrayData["syncLong"];
	$dblSyncAcc = (double) @$arrayData["syncAcc"];
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	//Fechas de última edición del outbox local
	$editStartDate = substr((string) @$arrayData["editStartDate"], 0, 10);
	$editStartHour = (string) @$arrayData["editStartHour"];
	$editEndDate = substr((string) @$arrayData["editEndDate"], 0, 10);
	$editEndHour = (string) @$arrayData["editEndHour"];
	//Fechas de última edición del outbox local según el server
	$editServerStartDate = substr((string) @$arrayData["editServerStartDate"], 0, 10);
	$editServerStartHour = (string) @$arrayData["editServerStartHour"];
	$editServerEndDate = substr((string) @$arrayData["editServerEndDate"], 0, 10);
	$editServerEndHour = (string) @$arrayData["editServerEndHour"];
	//@JAPR
	$answeredQuestions = getSurveyAnsweredQuestionsV6($arrayData, $aRepository, $surveyID);

	//GCRUZ 2016-05-30. Agregados campos (TransferTime, TransferDistance) para el tiempo y distancia de traslado para una captura
	$strUserName = getParamValue('UserID', 'both', '(string)');
	$transfer = getSurveyTransferTimeDistance($aRepository, $surveyInstance, $strUserName, $arrayData, false);
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDataInGlobalSurveyModelV6 TRANSFER == ".print_r($transfer, true), 2, 0, "color:darkgreen;");
	}
	//GCRUZ 2016-05-30. Validar integridad del cubo de surveys
	$surveyInstance->checkIntegrity();
	
	//Obtenemos los valores de CaptureVia, CaptureEmail y SchedulerID
	//para verificar si dichos valores los estan dando de alta un correo electronico
	$captureVia = (int) @$arrayData["CaptureVia"];
	$captureEmail = (string) @$arrayData["CaptureEmail"];
	$schedulerID = (int) @$arrayData["SchedulerID"];
	//Pare el modelo global de Surveys siempre hay un usuario, incluso si se trata del Administrador en caso de ser inválido o de una captura vía EMail, así que se valida
	$userID = (int) @$arrayData["userID"];
	if ($userID <= 0) {
		$userID = 1;
	}
	
	//Prepara los datos para el Insert, en su mayoría son keys a la dimensión del cubo global de formas, el cual para este punto ya debe estar creado y haber
	//generado todas las dimensiones correspondientes, pero en caso de no existir, se procederá a continuar con el grabado sin generar error para permitir un
	//reupload mas adelante
	$strAdditionalFields = "";
	$strAdditionalValues = "";
	
	//DateKey- ID de la dimension DimTime equivalente al StartDateID (no se consideran horas, sólo la fecha), se obtiene a partir del cubo global de surveys, ya que
	//es el único cubo que se tiene certeza que debe existir en este punto, y todos los cubos de Model Manager como este utilizan la misma estructura
	$strOriginalWD = getcwd();
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];
	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");
	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);
	if (is_null($anInstanceModel)) {
		return translate("There was an error loading the surveys model, please contact Technical Support");
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field DateKey");
	}
	$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $surveyInstance->GblSurveyModelID);
	//En este punto la fecha de la tabla de tiempo sólo corresponde a la porción fecha sin la hora de captura
	$dateKey = getDateKeyV6($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate);
	if ($dateKey === false) {
		return $gblEFormsErrorMessage;
	}
	
	$strAdditionalFields .= "DateKey";
	$strAdditionalValues .= $dateKey;
	
	//UserKey- EMail del usuario que hizo la captura (en caso de capturas tipo EMail es la cuenta Maestra). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "User";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblUserDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";;
		$strAdditionalValues .= ", ".$userID;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SurveyKey- Nombre de la forma que hizo la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "Survey";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	$surveyKey = -1;
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$surveyKey = getSurveyDimKey($aRepository, $anInstanceModelDim, $surveyInstance);
		if ($surveyKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";;
		$strAdditionalValues .= ", ".$surveyKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SurveyGlobalKey- Combinación del SurveyKey-AutoIncrForma, es decir, el Key de la dimensión Survey + el Key de la captura en la tabla de la forma.  ID del valor de la dimensión 
	//de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "SurveyGlobalKey";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyGlobalDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$surveyGlobalKey = getSurveyGlobalDimKey($aRepository, $anInstanceModelDim, $surveyInstance, $surveyKey, $factKeyDimVal, $latitude, $longitude, $accuracy);
		if ($surveyGlobalKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";;
		$strAdditionalValues .= ", ".$surveyGlobalKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SurveySingleRecordKey- Combinación del SurveyKey-AutoIncrForma-AutoIncrForma, es decir, el Key de la dimensión Survey + el Key de la captura en la tabla de la forma 2 veces, ya 
	//que esta dimensión no tiene aplicación prática en V6, pues originalmente el 3er ID representaba el registo específico de entre los "n" registros por captura (FactKey) mientras que 
	//el 2do ID representaba el ID de la captura (FactKeyDimVal), pero a partir de v6 toda captura tiene un único ID en la tabla de datos de la forma, así que se repitió el mismo valor
	//en el 2do y 3er ID.  ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "SurveySingleRecordKey";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveySingleRecordDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$surveySingleRecordKey = getSurveySingleRecordDimKey($aRepository, $anInstanceModelDim, $surveyInstance, $surveyKey, $factKeyDimVal, $factKeyDimVal);
		if ($surveySingleRecordKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";;
		$strAdditionalValues .= ", ".$surveySingleRecordKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//AgendaKey- Nombre de la agenta que hizo la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$aDimDesc = (string) @$arrDefDimValues['AgendaFilter'];
	if ($aDimDesc == '') {
		$aDimDesc = $gblEFormsNA;
	}
	$strDimensionName = "Agenda";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblAgendaDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $aDimDesc);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//StartDateKey- Fecha inicial de la captura (NO se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Start date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $startDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//StartTimeKey- Hora en formato 00:00:00 correspondiente con la hora del inicio de la captura (NO se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Start time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $startTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//EndDateKey- Fecha final de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "End date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $endDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//EndTimeKey- Hora en formato 00:00:00 correspondiente con la hora del final de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "End time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $endTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//Estos valores no se asignan durante la captura inicial, sólo durante le proceso de edición vía Web
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	//A partir de V6 el día de esta implementación, se modificó el sentido de estas dimensiones para que contengan las fechas de edición locales de los Outbox, en caso de agregar
	//fechas de edición directas en browser mas adelante, se tendría que agregar un nuevo conjunto de dimensiones para tal fin. En los pocos casos donde un repositorio viniera de
	//versiones anteriores a V6, habría que considerar que en el cubo global esta dimensión tendría un antes y un después o mejor dicho según la versión de la forma significarían
	//edición en browser (v5) o en outbox local (v6)
	//StartDateEditKey- Fecha inicial de la última edición de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Start date edit";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartDateEditDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editStartDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//EndDateEditKey- Fecha final de la última edición de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "End date edit";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndDateEditDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editEndDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//StartTimeEditKey- Hora en formato 00:00:00 correspondiente con la hora inicial de la última edición de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Start time edit";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartTimeEditDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editStartHour);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//EndTimeEditKey- Hora en formato 00:00:00 correspondiente con la hora final de la última edición de la captura (SI se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "End time edit";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndTimeEditDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editEndHour);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SyncDateKey- Fecha inicial de la sincronización de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $strSyncDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SyncTimeKey- Hora en formato 00:00:00 correspondiente con la hora inicial de la sincronización de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $strSyncTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//@JAPRWarning: Estos valores realmente corresponden al tiempo de Grabado en las tablas de datos, así que obteniendo la diferencia en tiempo entre la fecha de sincronización (si es
	//que se recibió correctamente desde un App que ya la envíe) y este momento se puede determinar el tiempo que duró el grabado si se usa el esquema en línea sin agente, o bien el tiempo
	//que tardó el agente en procesar la tarea considerando como el inicio el momento en que fue creada
	//SyncEndDateKey- Fecha final de la sincronización de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync end date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncEndDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $syncDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SyncEndTimeSync- Hora en formato 00:00:00 correspondiente con la hora final de la sincronización de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync end time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncEndTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $syncTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//ServerStartDateKey- Fecha inicial de la captura según el servidor (NO se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Server start date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $serverStartDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//ServerStartTimeKey- Hora en formato 00:00:00 correspondiente con la hora inicial de la captura según el servidor. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Server start time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $serverStartTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//ServerEndDateKey- Fecha final de la captura según el servidor (NO se altera durante la edición de la captura). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Server end date";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndDateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $serverEndDate);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//ServerEndTimeKey- Hora en formato 00:00:00 correspondiente con la hora final de la captura según el servidor. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Server end time";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndTimeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $serverEndTime);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//@JAPR 2016-12-29: Agregadas las fechas de edición según el servidor (#44O0VU)
	if (getMDVersion() >= esvServerEditDates) {
		//Todas estas dimensiones al ser creadas posterior a la liberación de eForms v6, y debido a que la revisión de integridad se realiza sólo 1 vez por día por repositorio, no
		//generarán error inmediatamente a menos que no se logre encontrar la dimensión siendo que si tiene asignado un ID, a diferencia de las dimensiones pre-existentes que incluso
		//sin un ID asignado habrían generado un error
		
		if ($surveyInstance->GblServerStartDateEditDimID > 0) {
			//ServerStartDateEditKey- Fecha inicial de edición según el server. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
			$aDimDescKey = $gblEFormsNAKey;
			$strDimensionName = "Server Start Date Edit";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Processing global model field {$strDimensionName}");
			}
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartDateEditDimID);
			if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
				$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editServerStartDate);
				if ($aDimDescKey === false) {
					return $gblEFormsErrorMessage;
				}
				$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
				$strAdditionalValues .= ", ".$aDimDescKey;
			}
			else {
				return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
			}
		}
		
		if ($surveyInstance->GblServerStartTimeEditDimID > 0) {
			//ServerStartTimeEditKey- Hora en formato 00:00:00 correspondiente con la Hora inicial de edición según el server. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
			$aDimDescKey = $gblEFormsNAKey;
			$strDimensionName = "Server Start Time Edit";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Processing global model field {$strDimensionName}");
			}
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerStartTimeEditDimID);
			if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
				$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editServerStartHour);
				if ($aDimDescKey === false) {
					return $gblEFormsErrorMessage;
				}
				$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
				$strAdditionalValues .= ", ".$aDimDescKey;
			}
			else {
				return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
			}
		}
		
		if ($surveyInstance->GblServerEndDateEditDimID > 0) {
			//ServerEndDateKey- Fecha final de edición según el server. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
			$aDimDescKey = $gblEFormsNAKey;
			$strDimensionName = "Server End Date Edit";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Processing global model field {$strDimensionName}");
			}
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndDateEditDimID);
			if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
				$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editServerEndDate);
				if ($aDimDescKey === false) {
					return $gblEFormsErrorMessage;
				}
				$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
				$strAdditionalValues .= ", ".$aDimDescKey;
			}
			else {
				return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
			}
		}
		
		if ($surveyInstance->GblServerEndTimeEditDimID > 0) {
			//ServerEndTimeKey- Hora en formato 00:00:00 correspondiente con la Hora final de edición según el server. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
			$aDimDescKey = $gblEFormsNAKey;
			$strDimensionName = "Server End Time Edit";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("Processing global model field {$strDimensionName}");
			}
			$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblServerEndTimeEditDimID);
			if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
				$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $editServerEndHour);
				if ($aDimDescKey === false) {
					return $gblEFormsErrorMessage;
				}
				$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
				$strAdditionalValues .= ", ".$aDimDescKey;
			}
			else {
				return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
			}
		}
	}
	//@JAPR
	
	//Estos datos se deben obtener desde GoogleMaps a partir de la posición de la captura
	//CountryKey- Porción extraida del GPS de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Country";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblCountryDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, (string) @$arrGPSData['Country']);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//StateKey- Porción extraida del GPS de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "State";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStateDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, (string) @$arrGPSData['State']);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//CityKey- Porción extraida del GPS de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "City";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblCityDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, (string) @$arrGPSData['City']);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//ZipCodeKey- Porción extraida del GPS de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "ZipCode";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblZipCodeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, (string) @$arrGPSData['ZipCode']);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//AddressKey- Porción extraida del GPS de la captura. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Address";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblAddressDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, (string) @$arrGPSData['FullAddress']);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
	//$strAdditionalValues .= ", {$gblEFormsNAKey}, {$gblEFormsNAKey}, {$gblEFormsNAKey}, {$gblEFormsNAKey}, {$gblEFormsNAKey}";
	
	//SyncLatitudeKey- Posición GPS (Latitude) de la Sincronización. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync latitude";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncLatitudeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $dblSyncLat);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SyncLongitudeKey- Posición GPS (Longitude) de la Sincronización. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync longitude";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncLongitudeDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $dblSyncLong);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//SyncAccuracyKey- Precisión de la posición GPS de la Sincronización. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$aDimDescKey = $gblEFormsNAKey;
	$strDimensionName = "Sync accuracy";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceModelDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSyncAccuracyDimID);
	if (!is_null($anInstanceModelDim) && $anInstanceModelDim->Dimension->DimensionID > 0) {
		$aDimDescKey = getInsertDimValueKeyV6($aRepository, $anInstanceModelDim, $dblSyncAcc);
		if ($aDimDescKey === false) {
			return $gblEFormsErrorMessage;
		}
		$strAdditionalFields .= ", ".$anInstanceModelDim->Dimension->TableName."KEY";
		$strAdditionalValues .= ", ".$aDimDescKey;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Dimension").": {$strDimensionName})";
	}
	
	//@JAPR 2015-07-24: Finalmente NO se agregaron dimensiones independientes para Latitude, Longitude ni Accuracy de la captura, porque la idea original de tener toas las tablas normalizadas
	//ya no aplicó debido a limitantes en la cantidad de joins, adicionalmente EArteaga utilizó dimensiones-atributo para estos valores en los modelos mapeados a partir de las nuevas tablas
	//de eForms v6, por lo que haber creado una nueva tercia de dimensiones habría entrado en conflicto con las dimensiones-atributos existentes del cubo global, así que se optó por
	//seguir utilizando las ya existentes
	//LatitudeInd- Posición GPS (Latitude). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "Latitude";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLatitudeIndID);
	if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
		$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
		$strAdditionalValues .= ", ".$latitude;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
	}
	
	//LongitudeInd- Posición GPS (Longitude). ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "Latitude";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLongitudeIndID);
	if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
		$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
		$strAdditionalValues .= ", ".$longitude;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
	}
	
	//AccuracyKey- Precisión de la posición GPS. ID del valor de la dimensión de dicho nombre, la cual está definida en el cubo global de Surveys
	$strDimensionName = "Accuracy";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblAccuracyIndID);
	if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
		$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
		$strAdditionalValues .= ", ".$accuracy;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
	}
	
	//DurationInd- Diferencia en minutos entre la hora inicial y la hora final de la captura 
	$strDimensionName = "Duration";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblDurationIndID);
	if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
		$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
		$strAdditionalValues .= ", ".$totalMinutes;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
	}
	
	//AnsweredQuestionsInd- Total de preguntas que fueron capturadas (se cuenta por instancia de pregunta, no por pregunta de cada posible registro)
	$strDimensionName = "Answered questions";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Processing global model field {$strDimensionName}");
	}
	$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblAnsweredQuestionsIndID);
	if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
		$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
		$strAdditionalValues .= ", ".$answeredQuestions;
	}
	else {
		return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
	}

	if (getMDVersion() >= esvCaptureTransfer) {
		//GCRUZ 2016-05-30. Tiempo de traslado
		//TrasnferTimeInd- Tiempo de traslado de la captura
		$strDimensionName = "TransferTime";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Processing global model field {$strDimensionName}");
		}
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblTransferTimeIndID);
		if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
			$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
			$strAdditionalValues .= ", ".$transfer['Time'];
		}
		else {
			return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
		}

		//GCRUZ 2016-05-30. Distancia de traslado
		//TrasnferTimeInd- Distancia de traslado de la captura
		$strDimensionName = "TransferDistance";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("Processing global model field {$strDimensionName}");
		}
		$anInstanceIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblTransferDistanceIndID);
		if (!is_null($anInstanceIndicator) && $anInstanceIndicator->IndicatorID > 0) {
			$strAdditionalFields .= ", ".$anInstanceIndicator->field_name.$anInstanceIndicator->IndicatorID;
			$strAdditionalValues .= ", ".$transfer['Distance'];
		}
		else {
			return translate("There was an error loading the surveys model, please contact Technical Support")." (".translate("Indicator").": {$strDimensionName})";
		}
	}
	
	//Genera el INSERT del registro en la Tabla del modelo global de Surveys, el FactKey es automáticamente y previamente ya se obtuvieron/generaron todos los keys de las dimensiones
	//utilizadas
	$factTable = $anInstanceModel->nom_tabla;
	$sql = "INSERT INTO {$factTable} ({$strAdditionalFields}) 
		VALUES ({$strAdditionalValues})";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDataInGlobalSurveyModelV6 INSERT: ".$sql, 1, 1, "color:blue;");
	}
	if (!$aRepository->DataADOConnection->Execute($sql)) {
		//Si hubo un error en la insercion de los datos en esta tabla paralela, tambien 
		//borramos los datos de la tabla paralela matriz y de la tabla de comentarios, imagenes y acciones
		$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
		return ("(".__METHOD__.") ".translate("Error accessing")." ".$surveyInstance->SurveyTable." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
	}
	
	return "OK";
}

/* Graba un sólo registro que contiene a todas las preguntas de secciones estándar de la encuesta (incluso si estas vienen vacias), sólo no 
grabaría el registros si no existíeran preguntas de este tipo (por ejemplo una encuesta con puras secciones formateadas o maestro-detalle, etc.). El registro
se graba en una tabla especial exclusiva para este tipo de secciones
*/
function singleSaveStandardDataV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal) {
	global $gblEFormsErrorMessage;
	
	//EVEG array que contiene las respuestas de las capturas y el array que llenara con los correos adicionales capturados en la encuesta
	global $arrAnswers;
	global $arrAdditionalEMails;
	
	global $saveDataForm;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("singleSaveStandardDataV6 SurveyID == {$surveyID}, SurveyDate == {$thisDate}", 2, 0, "color:darkgreen;");
	}
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	$objQuestionsColl = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	
	//Primero verifica si hay o no necesidad de grabar el registro, comprobando la existencia de preguntas estándar
	$blnHasStandardQuestions = false;
	foreach ($objQuestionsColl->Collection as $objQuestion) {
		if ($objQuestion->SectionType == sectNormal) {
			$blnHasStandardQuestions = true;
			break;
		}
	}
	
	//Si no hay preguntas estándar, simplemente sale sin insertar registro para esta captura pero sin generar un error
	if (!$blnHasStandardQuestions) {
		return "OK";
	}
	
	//En este caso si hay secciones estándar, así que tiene que cargar y procesar todas las que existan para generar el String de inserción
	$strTableName = $surveyInstance->SurveyStdTable;
	$strAdditionalFields = "";
	$strAdditionalValues = "";
	$objSectionsColl = BITAMSectionCollection::NewInstance($aRepository, $surveyID);
	foreach ($objSectionsColl->Collection as $objSection) {
		if ($objSection->SectionType != sectNormal) {
			continue;
		}
		
		//Genera la porción de INSERT para esta sección y la concatena a la que se lleva armada hasta este punto
		$arrFields = saveSectionDataV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal, $objSection->SectionID);
		if ($arrFields === false) {
			return $gblEFormsErrorMessage;
		}
		
		if (is_array($arrFields) && count($arrFields) >= 2) {
			$strFields = (string) @$arrFields['fields'];
			$strValues = (string) @$arrFields['values'];
			if (trim($strFields) != '' && trim($strValues) != '') {
				$strAdditionalFields .= $strFields;
				$strAdditionalValues .= $strValues;
			}
		}
	}
	
	//Genera el INSERT del registro en la Tabla de la forma, el Key es automáticamente generado y asignado a la variable de retorno
	$sectionKeyDimVal = 0;
	//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
	if($saveDataForm)
	{
	$sql = "INSERT INTO {$strTableName} (".BITAMSurvey::$KeyField." ".$strAdditionalFields.")
		VALUES ({$factKeyDimVal} ".$strAdditionalValues.")";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("singleSaveStandardDataV6 INSERT: ".$sql, 1, 1, "color:blue;");
	}
	if ($aRepository->DataADOConnection->Execute($sql)) {
		//Obtiene el ID autoincremental que identifica a la captura
		$sectionKeyDimVal = (int) @$aRepository->DataADOConnection->_insertid();
		if ($sectionKeyDimVal <= 0) {
			return ("(".__METHOD__.") ".translate("Error calculating the auto-incremental standard section key")." (".$strTableName."). ".translate("Executing").": ".$sql);
		}
	}
	else {
		$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
		return ("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
	}
	}
	//Si llega a este punto es que el registro se insertó correctamente, así que ahora procesa las preguntas que dependen del Key autoincremental de la sección
	$strAnd = "";
	$strAdditionalValues = "";
	foreach ($objSectionsColl->Collection as $objSection) {
		if ($objSection->SectionType != sectNormal) {
			continue;
		}
		
		$objQuestionsColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $objSection->SectionID);
		foreach ($objQuestionsColl->Collection as $objQuestion) {
			switch ($objQuestion->QTypeID) {
				case qtpMulti:
				case qtpSingle:
					$postKey = "qfield".$objQuestion->QuestionID;
					$strValue = "";
					//Se permitirá que en caso de no existir el parámetro se utilice el valor null, ya que ese se valida dentro de la función para ser ignorado
					//if (isset($arrayData[$postKey])) {
					$strValue = @$arrayData[$postKey];
					//}
					
					if ($objQuestion->ValuesSourceType == tofCatalog) {
						//Las preguntas tipo Simple Choice de catálogo deben buscar la combinación exacta de todos los atributos recibidos, así que es una función diferente
						//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
						$arrQuestionKeyDimVal = getInsertQCatFieldKeyV6($aRepository, $objQuestion, $strValue, $arrayData, $sectionKeyDimVal, $factKeyDimVal);
					}
					else {
						//Si es una simple choice normal, entonces tiene una tabla normalizada para sus valores de tal manera que se pueda convertir mas fácilmente a una múltiple choice
						//Identifica el Key para esta respuesta y lo agrega si no existía
						//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
						$arrQuestionKeyDimVal = getInsertQOptsFieldKeyV6($aRepository, $objQuestion, $strValue, $arrayData, $sectionKeyDimVal, $factKeyDimVal);
					}
					
					if ($arrQuestionKeyDimVal === false) {
						return $gblEFormsErrorMessage;
					}
					break;
			}
			
			//Graba la imagen asociada a la pregunta si es de un tipo que la contenga o está configurada con imagen adicional
			if ($objQuestion->HasReqPhoto > 0) {
				$postKey = "qimageencode".$objQuestion->QuestionID;
				$insertFile = @$arrayData[$postKey];
				$strFilePath = savePhotoV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertFile);
				if ($strFilePath === false) {
					return $gblEFormsErrorMessage;
				}
				
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote($strFilePath);
				$strAnd = ", ";
				
				//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				//En el caso de las preguntas Sketch+ se necesita un campo adicional para almacenar la imagen original sin los trazos
				if ( $objQuestion->QTypeID == qtpSketchPlus ) {
					$postKey = "qimageencode".$objQuestion->QuestionID.'Stored';
					$insertFile = @$arrayData[$postKey];
					//Se manda el parámetro para indicar que es la imagen adicional y NO la principal de la pregunta (prefijo 'Src')
					$strFilePath = savePhotoV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertFile, 0, 'Src');
					if ($strFilePath === false) {
						return $gblEFormsErrorMessage;
					}
					
					$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldImg}Src_{$objQuestion->QuestionID} = ".
						$aRepository->DataADOConnection->Quote($strFilePath);
					$strAnd = ", ";
				}
				//@JAPR
			}
			
			//Graba el documento asociado a la pregunta si es de un tipo que permita asociar o que genere archivos
			if ($objQuestion->HasReqDocument > 0) {
				$postKey = "qdocument".$objQuestion->QuestionID;
				$insertFile = @$arrayData[$postKey];
				$strFilePath = saveDocumentV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertFile);
				if ($strFilePath === false) {
					return $gblEFormsErrorMessage;
				}
				
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote($strFilePath);
				$strAnd = ", ";
			}
			
			//Graba el comentario asociado a la pregunta si estaba configurada para ello
			if ($objQuestion->HasReqComment > 0) {
				$postKey = "qcomment".$objQuestion->QuestionID;
				$insertComm = @$arrayData[$postKey];
				$strDataVal = saveCommentV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertComm);
				if ($strDataVal === false) {
					return $gblEFormsErrorMessage;
				}
				
				//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
				//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strDataVal));
				//@JAPR
				$strAnd = ", ";
			}
		}
	}
	
	if($saveDataForm)
	{
	if ($strAdditionalValues != '') {
		//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
		$sql = "UPDATE {$strTableName} SET {$strAdditionalValues} 
			WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal} AND SectionKey = {$sectionKeyDimVal}";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("singleSaveStandardDataV6 UPDATE: ".$sql, 1, 1, "color:blue;");
		}
		if (!$aRepository->DataADOConnection->Execute($sql)) {
			$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
			return ("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
		}
	}
	}
	return "OK";
}

/* Graba varios registros que contienen a todas las preguntas de secciones Maestro-Detalle o Inline de la encuesta (no vendrían vacias o no hubiera llegado aquí), sólo no 
grabaría el registros si no existíeran preguntas de este tipo (por ejemplo una encuesta con puras secciones formateadas o estándar). Los registros se graban en la tabla de la sección
correspondiente, la cual tiene una estructura ligeramente diferente según el tipo de sección, sin embargo las preguntas se graban de la misma manera en todas ellas
*/
function multipleSaveSectionDataV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal, $sectionID) {
	global $gblEFormsErrorMessage;
	
	global $saveDataForm;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("multipleSaveSectionDataV6 SurveyID == {$surveyID}, SectionID == {$sectionID}, SurveyDate == {$thisDate}", 2, 0, "color:darkgreen;");
	}
	
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
	$objSection = BITAMSection::NewInstanceWithID($aRepository, $sectionID);
	$objQuestionsColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $sectionID);
	
	//Primero verifica si hay o no necesidad de grabar el registro, comprobando la existencia de registros y preguntas
	$blnHasQuestions = false;
	foreach ($objQuestionsColl->Collection as $objQuestion) {
		$blnHasQuestions = true;
	}
	
	$postKey = "";
	switch ($objSection->SectionType) {
		case sectMasterDet:
			$postKey = "masterSectionRecs".$sectionID;
			break;
		case sectInline:
			$postKey = "inlineSectionRecs".$sectionID;
			break;
	}
	
	$intNumRecords = (int) @$arrayData[$postKey];
	
	//Si no hay preguntas de esta sección o no hay registros, simplemente sale sin insertar registro para esta captura pero sin generar un error
	if (!$blnHasQuestions || $intNumRecords <= 0) {
		return "OK";
	}
	
	//En este caso si hay registros para esta sección, así que tiene que grabar sus datos invocando al método para procesar las preguntas pero registro a registro de la captura
	$strTableName = $objSection->SectionTable;
	for ($intRecNum = 0; $intRecNum < $intNumRecords; $intRecNum++) {
		//@JAPR 2015-08-29: Corregido un bug, siempre que se deba concatenar campos, se debe concatener el separador (#HE8WBH)
		//Realmente este And no debe tener "," ni cambiar a dicho valor aquí, pues las funciones que generan los Fields ya la traerían si era necesario
		$strAnd = "";
		//@JAPR
		$strAdditionalFields = "";
		$strAdditionalValues = "";
		
		//Genera el array específico de esa sección para que se procese en la función genérica
		$arrayDataNew = array();
		foreach ($objQuestionsColl->Collection as $objQuestion) {
			$intQuestionID = $objQuestion->QuestionID;
			$postKey = "qfield".$intQuestionID;
			$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			$postKey = "qoptions".$intQuestionID;
			$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			$postKey = "qimageencode".$intQuestionID;
			$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			$postKey = "qcomment".$intQuestionID;
			$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			$postKey = "qdocument".$intQuestionID;
			$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			//@JAPR 2016-05-19: Corregido un bug, no estaba generando los datos para las preguntas GPS así que en multi-registro no se grababan
			if ($objQuestion->QTypeID == qtpGPS || $objQuestion->QTypeID == qtpMyLocation) {
				$postKey = "qlat".$intQuestionID;
				$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
				$postKey = "qlong".$intQuestionID;
				$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
				$postKey = "qacc".$intQuestionID;
				$arrayDataNew[$postKey] = @$arrayData[$postKey][$intRecNum];
			}
			//@JAPR
		}
		
		//Genera la porción de INSERT para esta sección y la concatena a la que se lleva armada hasta este punto
		$arrFields = saveSectionDataV6($aRepository, $surveyID, $arrayDataNew, $thisDate, $factKeyDimVal, $sectionID,$intRecNum);
		if ($arrFields === false) {
			return $gblEFormsErrorMessage;
		}
		
		if (is_array($arrFields) && count($arrFields) >= 2) {
			$strFields = (string) @$arrFields['fields'];
			$strValues = (string) @$arrFields['values'];
			if (trim($strFields) != '' && trim($strValues) != '') {
				$strAdditionalFields .= $strAnd.$strFields;
				$strAdditionalValues .= $strAnd.$strValues;
				//@JAPR 2015-08-29: Corregido un bug, siempre que se deba concatenar campos, se debe concatener el separador (#HE8WBH)
				//Realmente este And no debe tener "," ni cambiar a dicho valor aquí, pues las funciones que generan los Fields ya la traerían si era necesario
				$strAnd = "";
				//@JAPR
			}
		}
		
		//@JAPR 2015-08-26: Agregadas las columnas para las secciones Inline, ya que debido a que finalmente no se normalizaron todas las tablas como estaba pensado, se había
		//olvidado hacer este paso y no se estaban grabando los datos de las secciones
		if ($objSection->SectionType == sectInline) {
			//Genera la porción de los campos extras que se grabarán para esta sección inline basada en el número de registro que se está procesando, dependiendo de si es o no de catálogo
			//sería sólo el campo descripción, o dicho campo + todos los de atributos
			$arrFields = saveSectionDescDataV6($aRepository, $surveyID, $arrayDataNew, $thisDate, $factKeyDimVal, $sectionID, $intRecNum);
			if ($arrFields === false) {
				return $gblEFormsErrorMessage;
			}
			
			if (is_array($arrFields) && count($arrFields) >= 2) {
				$strFields = (string) @$arrFields['fields'];
				$strValues = (string) @$arrFields['values'];
				if (trim($strFields) != '' && trim($strValues) != '') {
					$strAdditionalFields .= $strAnd.$strFields;
					$strAdditionalValues .= $strAnd.$strValues;
					//@JAPR 2015-08-29: Corregido un bug, siempre que se deba concatenar campos, se debe concatener el separador (#HE8WBH)
					//Realmente este And no debe tener "," ni cambiar a dicho valor aquí, pues las funciones que generan los Fields ya la traerían si era necesario
					$strAnd = "";
					//@JAPR
				}
			}
		}
		//@JAPR
		
		//if ($strAdditionalFields != '') {
		//Genera el INSERT del registro en la Tabla de la sección, el Key es automáticamente generado y asignado para uso en grabado de datos adicionales
		$sectionKeyDimVal = 0;
		//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
		if($saveDataForm)
		{
		$sql = "INSERT INTO {$strTableName} (".BITAMSurvey::$KeyField." ".$strAdditionalFields.")
			VALUES ({$factKeyDimVal} ".$strAdditionalValues.")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("multipleSaveSectionDataV6 INSERT: ".$sql, 1, 1, "color:blue;");
		}
		if ($aRepository->DataADOConnection->Execute($sql)) {
			//Obtiene el ID autoincremental que identifica a la captura
			$sectionKeyDimVal = (int) @$aRepository->DataADOConnection->_insertid();
			if ($sectionKeyDimVal <= 0) {
				return ("(".__METHOD__.") ".translate("Error calculating the auto-incremental multiple section key")." (".$strTableName.") .".translate("Executing").": ".$sql);
			}
		}
		else {
			//Si hubo un error en la insercion de los datos en esta tabla paralela, tambien 
			//borramos los datos de la tabla paralela matriz y de la tabla de comentarios, imagenes y acciones
			$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
			return ("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
		}
		}
		//}
		
		//Si llega a este punto es que el registro se insertó correctamente, así que ahora procesa las preguntas que dependen del Key autoincremental de la sección
		$strAnd = "";
		$strAdditionalValues = "";
		foreach ($objQuestionsColl->Collection as $objQuestion) {
			switch ($objQuestion->QTypeID) {
				case qtpMulti:
				case qtpSingle:
					$postKey = "qfield".$objQuestion->QuestionID;
					$strValue = "";
					//Se permitirá que en caso de no existir el parámetro se utilice el valor null, ya que ese se valida dentro de la función para ser ignorado
					//if (isset($arrayData[$postKey][$intRecNum])) {
					$strValue = @$arrayData[$postKey][$intRecNum];
					//}
					
					if ($objQuestion->ValuesSourceType == tofCatalog) {
						//Las preguntas tipo Simple Choice de catálogo deben buscar la combinación exacta de todos los atributos recibidos, así que es una función diferente
						//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
						//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
						//Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
						$arrQuestionKeyDimVal = getInsertQCatFieldKeyV6($aRepository, $objQuestion, $strValue, $arrayData, $sectionKeyDimVal, $factKeyDimVal, $intRecNum);
					}
					else {
						//Si es una simple choice normal, entonces tiene una tabla normalizada para sus valores de tal manera que se pueda convertir mas fácilmente a una múltiple choice
						//Identifica el Key para esta respuesta y lo agrega si no existía
						//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
						//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
						//Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
						$arrQuestionKeyDimVal = getInsertQOptsFieldKeyV6($aRepository, $objQuestion, $strValue, $arrayData, $sectionKeyDimVal, $factKeyDimVal, $intRecNum);
					}
					
					if ($arrQuestionKeyDimVal === false) {
						return $gblEFormsErrorMessage;
					}
					break;
			}
			
			//Graba la imagen asociada a la pregunta si es de un tipo que la contenga o está configurada con imagen adicional
			if ($objQuestion->HasReqPhoto > 0) {
				$postKey = "qimageencode".$objQuestion->QuestionID;
				$insertImage = @$arrayData[$postKey][$intRecNum];
				//@JAPR 2016-08-12: Corregido el mapeo de preguntas firma/sketch hacia eBavel, para estas preguntas lo que llega en la propiedad answer.photo es un B64 que no se graba localmente
				//como archivo de imagen en la carpeta de sincronización, sino que se graba directo en la carpeta de imágenes de las capturas ya procesadas, así que al mandar a eBavel la imagen,
				//lo cual hacía enviando la propiedad answer.photo como si fuera una ruta de imagen, fallaba el copiado. Ahora se sobreescribirá en el $_POST un array por pregunta al momento de
				//generar la imagen para almacenar el nombre de archivo con el que quedó y usar eso durante el proceso de mapeo hacia eBavel (#4L5RSC)
				$strFilePath = savePhotoV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertImage, $intRecNum);
				//@JAPR
				if ($strFilePath === false) {
					return $gblEFormsErrorMessage;
				}
				
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldImg}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote($strFilePath);
				$strAnd = ", ";
				
				//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
				//En el caso de las preguntas Sketch+ se necesita un campo adicional para almacenar la imagen original sin los trazos
				if ( $objQuestion->QTypeID == qtpSketchPlus ) {
					$postKey = "qimageencode".$objQuestion->QuestionID.'Stored';
					$insertImage = @$arrayData[$postKey][$intRecNum];
					//Se manda el parámetro para indicar que es la imagen adicional y NO la principal de la pregunta (prefijo 'Src')
					$strFilePath = savePhotoV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertImage, $intRecNum, 'Src');
					if ($strFilePath === false) {
						return $gblEFormsErrorMessage;
					}
					
					$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldImg}Src_{$objQuestion->QuestionID} = ".
						$aRepository->DataADOConnection->Quote($strFilePath);
					$strAnd = ", ";
				}
				//@JAPR
			}
			
			//Graba el documento asociado a la pregunta si es de un tipo que permita asociar o que genere archivos
			if ($objQuestion->HasReqDocument > 0) {
				$postKey = "qdocument".$objQuestion->QuestionID;
				$insertFile = @$arrayData[$postKey][$intRecNum];
				$strFilePath = saveDocumentV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertFile);
				if ($strFilePath === false) {
					return $gblEFormsErrorMessage;
				}
				
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldDoc}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote($strFilePath);
				$strAnd = ", ";
			}
			
			//Graba el comentario asociado a la pregunta si estaba configurada para ello
			if ($objQuestion->HasReqComment > 0) {
				$postKey = "qcomment".$objQuestion->QuestionID;
				$insertComm = @$arrayData[$postKey][$intRecNum];
				$strDataVal = saveCommentV6($aRepository, $objQuestion, $sectionKeyDimVal, $factKeyDimVal, $insertComm);
				if ($strDataVal === false) {
					return $gblEFormsErrorMessage;
				}
				
				//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
				//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
				$strAdditionalValues .= $strAnd."{$objQuestion->DescFieldComm}_{$objQuestion->QuestionID} = ".
					$aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strDataVal));
				//@JAPR
				$strAnd = ", ";
			}
		}
		
		if($saveDataForm)
		{
		if ($strAdditionalValues != '') {
			//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
			$sql = "UPDATE {$strTableName} SET {$strAdditionalValues} 
				WHERE ".BITAMSurvey::$KeyField." = {$factKeyDimVal} AND SectionKey = {$sectionKeyDimVal}";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("multipleSaveSectionDataV6 UPDATE: ".$sql, 1, 1, "color:blue;");
			}
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				$strSQLErrMsg = (string) @$aRepository->DataADOConnection->ErrorMsg();
				return ("(".__METHOD__.") ".translate("Error accessing")." ".$strTableName." ".translate("table").": ".$strSQLErrMsg.". ".translate("Executing").": ".$sql);
			}
		}
		}
	}
	
	return "OK";
}

/* Genera el String de campos con los valores a insertar para todas las preguntas que apliquen en la sección especificada
Se creó esta función para tener un único lugar que permita obtener los keys subrrogados e insertar las nuevas respuestas a cada tipo de pregunta, sin que
realmente importe en que tipo de sección está, ya que no importar si es o no multi-registro, el comportamiento de los datos de las preguntas es el mismo,
así que simplemente se puede invocar a esta función tantas veces como registros existan en una captura de sección múltiple o bien una vez por cada secciónatcasesort
estándar que se deba grabar
Los posibles resultados de esta función son:
- Array Vacío si no hay preguntas (en ese caso quien invoca simplemente continua el proceso)
- false si hay un error (en ese caso el error específico se regresa en la variable $gblEFormsErrorMessage)
- Un array con dos elementos: ('fields' => lista de campos del INSERT, 'values' => Valores correspondientes a los campos del VALUES)
EVEG se agrego el parametro intRecNum que es el numero de registro
*/

function saveSectionDataV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal, $sectionID,$intRecNum=0) {
	global $gblEFormsErrorMessage;
	//@JAPR 2015-12-09: Corregido un bug, no se había obtenido la referencia global de $arrAnswers, así que el proceso de eMails no podía reemplazar las variables (#TI6HON)
	//EVEG array que contiene las respuestas de las capturas y el array que llenara con los correos adicionales capturados en la encuesta
	global $arrAnswers;
	global $arrAdditionalEMails;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveSectionDataV6 SurveyID == {$surveyID}, SectionID == {$sectionID}, SurveyDate == {$thisDate}", 2, 0, "color:darkgreen;");
	}
	
	//@JAPR 2015-10-07: Corregido un bug, al introducir el cambio de $arrAdditionalEMails, se empezó a usar la instancia de Survey sin estar
	//asignada, así que generaba un error en todo el proceso
	$surveyInstance = BITAMSurvey::NewInstanceWithID($aRepository, $surveyID);
  
	$arrReturnValue = array();
	$objQuestionsColl = BITAMQuestionCollection::NewInstanceBySection($aRepository, $sectionID);
	
	//Recorre la colección de preguntas y sólo procesa aquellas que son de secciones estándar
	$strAdditionalFields = "";
	$strAdditionalValues = "";
	foreach ($objQuestionsColl->Collection as $objQuestion) {
		//Valida si la pregunta tiene o no un valor que capturar como respuesta
		$blnValidQuestion = true;
		switch ($objQuestion->QTypeID) {
			case qtpPhoto:
			case qtpDocument:
			case qtpSignature:
			case qtpSketch:
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//case qtpSketchPlus:
			//@JAPR
			case qtpSkipSection:
			case qtpMessage:
			case qtpSync:
			case qtpPassword:
			case qtpAudio:
			case qtpVideo:
			case qtpSection:
			case qtpOCR:
			//@JAPR 2016-01-28: Agregado el tipo de pregunta Salir sin grabar (#MJS6SY)
			case qtpExit:
			//@JAPR 2016-04-06: Agregado el tipo de pregunta Update Data
			case qtpUpdateDest:
				$blnValidQuestion = false;
				break;
		}
		
		//Graba la respuesta de la pregunta
		if ($blnValidQuestion) {
			switch ($objQuestion->QTypeID) {
				//OMMC 2016-04-18: Agregado el tipo de pregunta Mi localización
				case qtpMyLocation:
				case qtpGPS:
					//El valor de la pregunta está en formato Latitude, Longitude, así que además del campo descripción base con ese valor, graba campos
					//adicionales donde viene el desgloce de ambos
					
					$postKey = "qlat".$objQuestion->QuestionID;
					$strValue = "";
					if (isset($arrayData[$postKey])) {
						$strValue = $arrayData[$postKey];
					}
					$strAdditionalFields .= ", QuestionLat_".$objQuestion->QuestionID;
					//@JAPR 2015-10-28: Corregido un bug, este campo es DOUBLE así que no se debe tratar como String, cadena vacía se convierte a 0 (#WNBLAJ)
					$strAdditionalValues .= ", ".((float) $strValue);
					
					$postKey = "qlong".$objQuestion->QuestionID;
					$strValue = "";
					if (isset($arrayData[$postKey])) {
						$strValue = $arrayData[$postKey];
					}
					$strAdditionalFields .= ", QuestionLong_".$objQuestion->QuestionID;
					//@JAPR 2015-10-28: Corregido un bug, este campo es DOUBLE así que no se debe tratar como String, cadena vacía se convierte a 0 (#WNBLAJ)
					$strAdditionalValues .= ", ".((float) $strValue);
					
					$postKey = "qacc".$objQuestion->QuestionID;
					$strValue = "";
					$questionAcc = "";
					if (isset($arrayData[$postKey])) {
						$strValue = $arrayData[$postKey];
						$questionAcc = " (".$strValue.")";
					}
					$strAdditionalFields .= ", QuestionAcc_".$objQuestion->QuestionID;
					//@JAPR 2015-10-28: Corregido un bug, este campo es DOUBLE así que no se debe tratar como String, cadena vacía se convierte a 0 (#WNBLAJ)
					$strAdditionalValues .= ", ".((float) $strValue);
					
					$postKey = "qfield".$objQuestion->QuestionID;
					$strValue = "";
					if (isset($arrayData[$postKey])) {
						$strValue = $arrayData[$postKey];
					}
					$strAdditionalFields .= ", ".$objQuestion->DescFieldAttr;
					$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote($strValue.$questionAcc);

					break;
				
				case qtpSingle:
				case qtpMulti:
					//Si es una multiple choice tiene una tabla normalizada con un registro por cada valor marcado, en donde a su vez como campo no normalizado estaría la descripción
					//Es similar a una Simple choice, sólo que en la simple choice no se repetirían descripciones
					//En ambos casos, se deben grabar hasta que se insertó el registro de la sección, ya que como llave foránea deben usar ticho Key, por tanto no se graban en este ciclo
					break;
				
				default:
					//El resto de las preguntas que no reciben tratamiento especial sólo tienen una respuesta directa
					$postKey = "qfield".$objQuestion->QuestionID;
					$strValue = "";
					$dblValue = null;
					if (isset($arrayData[$postKey])) {
						$strValue = $arrayData[$postKey];
						$dblValue = $strValue;
					}
					
					//Graba la respuesta directa, la cual siempre es una representación String aunque el tipo sea diferente
					$strAdditionalFields .= ", ".$objQuestion->DescFieldAttr;
					//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
					//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
					$strAdditionalValues .= ", ".$aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strValue));
					//@JAPR
					
					//Si la pregunta se resuelve a un número, adicionalmente graba la representación numérica de la misma, lo cual si permite NULL
					switch ($objQuestion->QTypeID) {
						case qtpOpenNumeric:
						case qtpCalc:
							if (!is_null($dblValue) && is_numeric($dblValue)) {
								$strAdditionalFields .= ", ".$objQuestion->ValueField;
								$strAdditionalValues .= ", ".$dblValue;
							}
							break;
					}
					
					break;
			}
			
			//Procesa valores adicionales de la pregunta según su tipo
			switch ($objQuestion->QTypeID) {
				case qtpSingle:
				case qtpMulti:
					//Si la pregunta es de selección pero no de catálogo, procesa el Score de las opciones seleccionadas
					if ($objQuestion->ValuesSourceType != tofCatalog) {
					}
					break;
			}
		}
		
		//Valida si la pregunta tiene o no una imagen que grabar como parte de la respuesta
		$blnValidQuestion = false;
		switch ($objQuestion->QTypeID) {
			case qtpPhoto:
			case qtpSignature:
			case qtpSketch:
			//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case qtpSketchPlus:
			//@JAPR
			case qtpOCR:
				$blnValidQuestion = true;
				break;
			
			default:
				$blnValidQuestion = ($objQuestion->HasReqPhoto > 0);
				break;
		}
		
		//Graba la foto/imagen/dibujo de la pregunta
		if ($blnValidQuestion) {
			$postKey = "qimageencode".$objQuestion->QuestionID;
			$strValue = "";
			if (isset($arrayData[$postKey])) {
				$strValue = $arrayData[$postKey];
			}
			
			//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
			/*
			//Identifica el Key para esta imagen
			$aQDescKey = getInsertQFieldKeyV6($aRepository, $objQuestion, $strValue, "image");
			if ($aQDescKey === false) {
				return false;
			}
			$strAdditionalFields .= ", ".$objQuestion->KeyFieldImgFK;
			$strAdditionalValues .= ", ".$aQDescKey;
			*/
		}
		
		$blnValidQuestion = ($objQuestion->HasReqComment > 0);
		//Graba el comentario de la pregunta
		if ($blnValidQuestion) {
			$postKey = "qcomment".$objQuestion->QuestionID;
			$strValue = "";
			if (isset($arrayData[$postKey])) {
				$strValue = $arrayData[$postKey];
			}
			
			//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
			/*
			//Identifica el Key para este comentario
			$aQDescKey = getInsertQFieldKeyV6($aRepository, $objQuestion, $strValue, "comment");
			if ($aQDescKey === false) {
				return false;
			}
			$strAdditionalFields .= ", ".$objQuestion->KeyFieldCommFK;
			$strAdditionalValues .= ", ".$aQDescKey;
			*/
		}
		
		//Valida si la pregunta tiene o no un documento que grabar como parte de la respuesta
		$blnValidQuestion = false;
		switch ($objQuestion->QTypeID) {
			case qtpDocument:
			case qtpAudio:
			case qtpVideo:
				$blnValidQuestion = true;
				break;
			
			default:
				$blnValidQuestion = ($objQuestion->HasReqDocument > 0);
				break;
		}
		
		//Graba el documento de la pregunta
		if ($blnValidQuestion) {
			$postKey = "qdocument".$objQuestion->QuestionID;
			$strValue = "";
			if (isset($arrayData[$postKey])) {
				$strValue = $arrayData[$postKey];
			}
			
			//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
			/*
			//Identifica el Key para esta imagen
			$aQDescKey = getInsertQFieldKeyV6($aRepository, $objQuestion, $strValue, "document");
			if ($aQDescKey === false) {
				return false;
			}
			$strAdditionalFields .= ", ".$objQuestion->KeyFieldDocFK;
			$strAdditionalValues .= ", ".$aQDescKey;
			*/
		}
	}
	
	//Si llegó hasta este punto quiere decir que o no había preguntas, así que regresará un array vacio, o bien generó el string correctamente, por lo que debe
	//regresarlo con la estructura esperada
	if (trim($strAdditionalFields) != '' && trim($strAdditionalValues) != '') {
		$arrReturnValue['fields'] = $strAdditionalFields;
		$arrReturnValue['values'] = $strAdditionalValues;
	}
	//EVEG se llena el array de los correos adicionales
	if (getMDVersion() >= esvReportEMailsByCatalog && trim($surveyInstance->AdditionalEMails) != '') {
		$strAdditionalEMails = $surveyInstance->AdditionalEMails;
		//@JAPR 2015-12-09: Corregido un bug, no estaba validando contra el formato nuevo de $, y no para variables de sección (#TI6HON)
		if (stripos($surveyInstance->AdditionalEMails, '$Q') !== false || stripos($surveyInstance->AdditionalEMails, '$S') !== false) {
			//En este caso se debe traducir el texto para reemplazar las variables que utiliza
			//$strAdditionalEMails = (string) @replaceQuestionVariables($strAdditionalEMails, $arrFieldValues, true, $aRepository, $surveyID, false, true);
			$arrInfo = array();
			//@JAPR 2015-12-01: Corregido un bug, no se debe enviar el parámetro de Quote porque las comillas sólo son requeridas en casos muy específicos (#9IG3DH)
			$strAdditionalEMails = (string) @replaceQuestionVariablesAdvV6($strAdditionalEMails, $arrAnswers, true, $aRepository, $surveyID, false, false, false, $arrInfo,$intRecNum);
			//@JAPR
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Additional EMails: {$strAdditionalEMails}");
			}
		}
		
		//@JAPR 2014-01-27: Corregido un bug, debe hacer este proceso no sólo si hay atributos, por si vienen EMails fijos
		if (trim($strAdditionalEMails) != '') {
			$arrNewEMails = explode(';', $strAdditionalEMails);
			foreach ($arrNewEMails as $strEMail) {
				//Verifica que el EMail sea válido antes de intentar utilizarlo
				if (trim($strEMail) != '' && strpos($strEMail, '@') !== false && strlen($strEMail) >= 3) {
					if (!isset($arrAdditionalEMails[$strEMail])) {
						$arrAdditionalEMails[$strEMail] = $strEMail;
					}
				}
			}
		}
	}
	global $dataDestinationsComposite;
	//MAPR 2019-07-22 Se añade la funcionalidad para utilizar las nuevas variables de correo en destino de Datos. (#280RV6)
	if (getMDVersion() >= esvDataDestEmailComposite && $dataDestinationsComposite != '') {
		$strAdditionalEMails = $dataDestinationsComposite;
		//@JAPR 2015-12-09: Corregido un bug, no estaba validando contra el formato nuevo de $, y no para variables de sección (#TI6HON)
		if (stripos($strAdditionalEMails, '$Q') !== false || stripos($strAdditionalEMails, '$S') !== false) {
			//En este caso se debe traducir el texto para reemplazar las variables que utiliza
			//$strAdditionalEMails = (string) @replaceQuestionVariables($strAdditionalEMails, $arrFieldValues, true, $aRepository, $surveyID, false, true);
			$arrInfo = array();
			//@JAPR 2015-12-01: Corregido un bug, no se debe enviar el parámetro de Quote porque las comillas sólo son requeridas en casos muy específicos (#9IG3DH)
			$strAdditionalEMails = (string) @replaceQuestionVariablesAdvV6($strAdditionalEMails, $arrAnswers, true, $aRepository, $surveyID, false, false, false, $arrInfo,$intRecNum);
			//@JAPR
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				echo("<br>\r\n Additional EMails: {$strAdditionalEMails}");
			}
		}
		
		//@JAPR 2014-01-27: Corregido un bug, debe hacer este proceso no sólo si hay atributos, por si vienen EMails fijos
		if (trim($strAdditionalEMails) != '') {
			$arrNewEMails = explode(';', $strAdditionalEMails);
			foreach ($arrNewEMails as $strEMail) {
				//Verifica que el EMail sea válido antes de intentar utilizarlo
				if (trim($strEMail) != '' && strpos($strEMail, '@') !== false && strlen($strEMail) >= 3) {
					if (!isset($arrAdditionalEMails[$strEMail])) {
						$arrAdditionalEMails[$strEMail] = $strEMail;
					}
				}
			}
		}
	}
	return $arrReturnValue;
}

/* Dada la instancia de una pregunta y un valor que corresponde a un catálogo en formato "key_id_SVSep_ValorAttr_SVElem_", obtiene el key correspondiente al valor dentro de la tabla 
del detalle de la pregunta siempre y cuando exista un registro que contenga toda la combinación de los atributos recibidos con los valores exactos, en caso contrario, se insertará un nuevo
registro para esa combinación. Dado a que este método se puede invocar en tiempos distintos entre la definición y la captura correspondiente a esa definición, se preparará para recibir
en el propio archivo de datos la estructura del catálogo que había en el momento de la captura, en caso de no recibirla (como lo será en las versiones iniciales), se asumirá que la
estructura del catálogo en la definición es la misma usada durante la captura, así que se generará en este preciso momento utilizando el orden definido actualmente en la pregunta
Esta función se debe utilizar sólo para preguntas que están relacionadas a un catálogo
//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
*/
function saveSectionDescDataV6($aRepository, $surveyID, $arrayData, $thisDate, $factKeyDimVal, $sectionID, $aRecNum = 0) {
	global $gblEFormsErrorMessage;
	global $arrSectionDescByID;
	global $strNewAttribSep;
	global $strNewDataSep;
	global $gblEFormsNA;
	//Array global con las definiciones de catálogo recibidas, si no está asignado el catálogo de esta pregunta, deberá generarlo en este instante para continuar el grabado, usando la
	//definición actual del catálogo, lo cual sólo sirve en caso de capturas recientes a la fecha de definición, pero no servirá para ediciones futuras necesariamente
	global $garrCatalogDefs;
	if (!isset($garrCatalogDefs)) {
		$garrCatalogDefs = array();
	}
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App, este array contiene la lista completa de valores en el mismo orden de atributos definidos en garrCatalogDefs, la
	//manera de indexar es "q"+QuestionID (o "s"+SectionID), luego el índice de registro (o 0 si es estándar), y finalmente contiene el array de atributos en el mismo orden de garrCatalogDefs
	global $garrCatalogData;
	if (!isset($garrCatalogData)) {
		$garrCatalogData = array();
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveSectionDescDataV6 SurveyID == {$surveyID}, SectionID == {$sectionID}, SurveyDate == {$thisDate}, aRecNum == {$aRecNum}", 2, 0, "color:darkgreen;");
	}
	
	$arrReturnValue = array();
	
	$objSection = BITAMSection::NewInstanceWithID($aRepository, $sectionID);
	//Este proceso sólo aplica para secciones Inline
	if ($objSection->SectionType != sectInline) {
		return $arrReturnValue;
	}
	
	//Obtiene el valor de la etiqueta del registro que se está procesando, este es independiente de si es o no una sección de catálogo
	$arrSectionDesc = @$arrSectionDescByID[$sectionID][$aRecNum];
	if (is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
		$strDynamicOptionDSC = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
		//@JAPR 2015-08-29: Corregido un bug, siempre que se deba concatenar campos, se debe concatener el separador (#HE8WBH)
		$arrReturnValue['fields'] = ", ".$objSection->DescField;
		$arrReturnValue['values'] = ", ".$aRepository->DataADOConnection->Quote($strDynamicOptionDSC);
		//@JAPR
	}
	
	//Si el tipo de llenado de la sección no es de catálogo, entonces sólo hay un único valor a utilizar el cual se llena en el campo de descripción y corresponde al valor mostrado
	//en la columna de etiqueta de la sección, así que se regresa directamente dicho valor
	if ($objSection->ValuesSourceType != tofCatalog) {
		return $arrReturnValue;
	}
	
	if ($objSection->DataSourceID <= 0 || $objSection->DataSourceMemberID <= 0) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Catalog").")";
		return false;
	}
	
	//En este caso la sección se basa en un catálogo, así que debe grabar los valores de todos los atributos tal como se recibieron
	$intDataSourceID = $objSection->DataSourceID;
	$arrUsedDataMembers = array();
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$arrDataSourceDefColl = @$garrCatalogDefs["s".$objSection->SectionID];
	//$arrDataSourceDefColl = @$garrCatalogDefs[$intDataSourceID];
	//Si no se recibió la definición del DataSource desde el App, entonces lo genera en este punto a partir de la definición actual de la sección y del DataSource
	if (is_null($arrDataSourceDefColl) || !is_array($arrDataSourceDefColl) || count($arrDataSourceDefColl) == 0) {
		$arrDataSourceDefColl = array();
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $objSection->DataSourceID);
		if (is_null($objDataSourceMembersColl)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Catalog")."-".translate("Source").")";
			return false;
		}
		
		$arrDataSourceMembersByIDs = array();
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
		}
		
		//Para secciones se pueden configurar varios atributos, sin embargo todos se deben agregar así que no importa el orden en realidad
		if ($objSection->CatalogID <= 0 || $objSection->CatMemberID <= 0) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Catalog").")";
			return false;
		}
		
		//En este caso se usarán los atributos según el marcado como seleccionado para la pregunta
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $objSection->CatalogID);
		if (is_null($objCatalogMembersColl)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Catalog").")";
			return false;
		}
		
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			$intDataSourceMemberID = $objCatalogMember->DataSourceMemberID;
			if ($intDataSourceMemberID <= 0) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Attribute")." - ".$objCatalogMember->MemberName.")";
				return false;
			}
			$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $objCatalogMember->MemberName);
			$arrUsedDataMembers[$intDataSourceMemberID] = $intDataSourceMemberID;
		}
	}
	
	//Si no hubiera atributos para procesar, debe generar un error pues no podrá grabar la pregunta
	if (count($arrDataSourceDefColl) == 0) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Attributes").")";
		return false;
	}
	
	//En este punto ya tiene lo necesario para procesr el grabado de la combinación del catálogo correspondiente
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$blnValueSet = true;
	$arrAllValues = array();
	$arrAllValues = @$garrCatalogData['s'.$objSection->SectionID][$aRecNum];
	if (is_null($arrAllValues)) {
		$arrAllValues = array();
	}
	
	$intIndex = 0;
	$strAdditionalFields = '';
	$strAdditionalValues = '';
	$strCatStructValues = '';
	$strAndStruct = '';
	$arrValues = array();
	//Arma el filtro en el orden en que teóricamente recibió los atributos, basado en el valor indicado
	foreach ($arrDataSourceDefColl as $arrDataSourceDef) {
		$intDataSourceMemberID = (int) @$arrDataSourceDef['key'];
		$strDataSourceMemberName = (string) @$arrDataSourceDef['name'];
		if ($intDataSourceMemberID <= 0) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the section's data")." (".$objSection->SectionID."), ".translate("the section has an invalid configuration")." (".translate("Attribute")." - #".$intIndex.")";
			return false;
		}
		
		$strValue = @$arrValues[$intIndex];
		if (is_null($strValue) || trim($strValue) == '') {
			$strValue = $gblEFormsNA;
			//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
			//Si se asignó el valor de respuesta de esta pregunta pero ya no viene capturado este registro, utiliza el array de valores completos del catálogo que se recibió desde el App
			//para complementar con el primer valor de los atributos no usados directamente por la pregunta
			if ($blnValueSet) {
				if (isset($arrAllValues[$intIndex]) && trim((string) @$arrAllValues[$intIndex]) != '') {
					$strValue = (string) @$arrAllValues[$intIndex];
				}
			}
			//@JAPR
		}
		else {
			$arrData = explode($strNewDataSep, $strValue);
			$strValue = @$arrData[((count($arrData) > 1)?1:0)];
			if (is_null($strValue) || trim($strValue) == '') {
				$strValue = $gblEFormsNA;
			}
		}
		
		$strValue = $aRepository->DataADOConnection->Quote($strValue);
		$strFieldName = BITAMQuestion::$DescFieldCatMember.$intDataSourceMemberID;
		$strAdditionalFields .= ", ".$strFieldName;
		$strAdditionalValues .= ", ".$strValue;
		$strOptKey = "";
		if ($intDataSourceMemberID == $objSection->DataSourceMemberID) {
			$strOptKey = "*";
		}
		$strCatStructValues .= $strAndStruct."key".$strOptKey."_{$intDataSourceMemberID}_{$strDataSourceMemberName}";
		$strAndStruct = $strNewAttribSep;
		$intIndex++;
	}
	
	//Llegado a este punto concatena los campos adicionales de atributos al default de la descripción
	if (trim($strAdditionalFields) != '' && trim($strAdditionalValues) != '') {
		$arrReturnValue['fields'] .= $strAdditionalFields.', AttributeDesc';
		$arrReturnValue['values'] .= $strAdditionalValues.', '.$aRepository->DataADOConnection->Quote($strCatStructValues);
	}
	return $arrReturnValue;
}

/* Obtiene la llave subrrogada en la tabla de tiempo del modelo especificado para la fecha indicada, en caso de no existir, la agrega 
formateando la fecha según cada dimensión tiempo definida en el modelo
*/
function getDateKeyV6($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate) {
	global $gblEFormsErrorMessage;
	
	$periodDims = BITAMDimensionCollection::NewInstancePeriodDims($aRepository, $anInstanceModel->ModelID, false);
	//Valida que se pueda ejecutar el query en lugar de generar un error mas complejo de identificar
	if (is_null($periodDims) || !is_object($periodDims) && property_exists($periodDims, 'Collection')) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Unable to get the DateKey from the surveys model, there's some missing settings in the model definition").": ".translate("Periods");
		return false;
	}
	
	$numPeriodDims = count($periodDims->Collection);
	$formatedDate = $currentDate." 00:00:00";
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$dateDescField = $anInstanceModel->fecha;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, false);
	
	//Valida que se pueda ejecutar el query en lugar de generar un error mas complejo de identificar
	if (!$dateKeyField || !$dimPeriodo || !$dateDescField) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Unable to get the DateKey from the surveys model, there's some missing settings in the model definition").": dateKeyField == {$dateKeyField}, dimPeriodo == {$dimPeriodo}, dateDescField == {$dateDescField}";
		return false;
	}
	
	$sql = "SELECT ".$dateKeyField." FROM ".$dimPeriodo." WHERE ".$dateDescField." = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	
	if (!$aRS->EOF) {
		//Ya existe esa fecha en la tabla especificada en dim_periodo, se retorna su clave
		$dateKey =  $aRS->fields[strtolower($dateKeyField)];
	}
	else {
		$dateKey = substr($currentDate, 0, 10);
		$dateKey = str_replace("-", "", $dateKey);
		
		$sql = "INSERT INTO ".$dimPeriodo." (".$dateKeyField.", ".$dateDescField;
		for ($i = 0; $i < $numPeriodDims; $i++) {
			$sql.= ", ".$periodDims->Collection[$i]->nom_fisicok_bd;
		}
		$sql.= ") VALUES (".$dateKey.", ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
		
		for ($i = 0; $i < $numPeriodDims; $i++) {
			$formatedDateField = formatADateTime($formatedDate, $periodDims->Collection[$i]->fmt_periodo);
			$formatedDateField = $aRepository->DataADOConnection->Quote($formatedDateField);
			$sql.= ", ".$formatedDateField;
		}
		$sql .= ")";
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	return $dateKey;
}

/* Dadas una dimensión que debe ser válida en este punto, mas un valor que debería ser un miembro de dicha dimensión, obtiene el key correspondiente al valor,
pero en caso de no encontrarlo se insertará en la tabla de la dimensión, bajo el supuesto que todas las dimensiones contienen valores VarChar exclusivamente
@JAPR 2015-08-13: Agregado el parámetro $bAddMember para indicar si se desea o no hacer la inserción (por default se hará ya que se usa durante el proceso de grabado de datos)
*/
function getInsertDimValueKeyV6($aRepository, $anInstanceIndDim, $value, $bAddMember = true) {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	
	$valueKey = $gblEFormsNAKey;
	if ((string) $value == '') {
		$value = $gblEFormsNA;
	}
	
	$tableName = $anInstanceIndDim->Dimension->TableName;
	$fieldKey = $tableName."KEY";
	$fieldDesc = $anInstanceIndDim->Dimension->FieldDescription;
	$strDimName = $anInstanceIndDim->Dimension->DimensionName;
	
	$sql = "SELECT {$fieldKey}, {$fieldDesc} 
		FROM {$tableName} 
		WHERE {$fieldDesc} = ".$aRepository->DataADOConnection->Quote($value);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table")." (".translate("Dimension").": ".$strDimName."): ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	if (!$aRS->EOF) {
		//Ya existe ese valor en la tabla de la dimension, se retorna su clave
		$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
	}
	//@JAPR 2015-08-13: Agregado el parámetro $bAddMember para indicar si se desea o no hacer la inserción (por default se hará ya que se usa durante el proceso de grabado de datos)
	elseif ($bAddMember) {
		//Se debe insertar dicho valor en la tabla de dimension
		$sql = "INSERT INTO {$tableName} ({$fieldDesc}) VALUES (".$aRepository->DataADOConnection->Quote($value).")";
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table")." (".translate("Dimension").": ".$strDimName."): ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT {$fieldKey}, {$fieldDesc} 
			FROM {$tableName} 
			WHERE {$fieldDesc} = ".$aRepository->DataADOConnection->Quote($value);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table")." (".translate("Dimension").": ".$strDimName."): ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
	}
	else {
		//Si no se optó por agregar un valor, entonces se debe regresar un key inválido ya que no se trataría del proceso de grabado y 1 es un Key válido para algunas dimensiones
		//de eForms que originalmente se generaron sin valor de NA en el cubo Global surveys
		$valueKey = false;
	}
	
	return $valueKey;
}

/* Dada la instancia de una pregunta y un valor, obtiene el key correspondiente al valor dentro de la tabla donde la pregunta guarda sus datos, considerando
el tipo de dato del campo descripción según el tipo de la pregunta. Esta función se debe utilizar sólo para preguntas que contienen una única respuesta
El parámetro $sPropertyName permite indicar si se está consultando la tabla de la respuesta ("value" - Default), la imagen ("image"), el comentario ("comment"),
Documento ("document"), Score ("score") o algún valor adicional ("extra")
//@JAPR 2015-07-10: Dada la limitante de tablas en un join en MySQL, no se normalizarán los datos adicionales sino que tendrán una tabla única tal como en v5-
//@JAPRDescontinuada: Esta función finalmente no se usó por la limitante de la cantidad de tablas en un join, así que no se pudieron usar tablas independientes para cada elemento
//de la pregunta sino que se dejaron columnas independientes dentro de la misma tabla
*/
function getInsertQFieldKeyV6($aRepository, $aQuestion, $value, $sPropertyName = "value") {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	
	$valueKey = $gblEFormsNAKey;
	if ((string) $value == '') {
		$value = $gblEFormsNA;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQFieldKeyV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sPropertyName == {$sPropertyName}, value == {$value}", 2, 0, "color:darkgreen;");
	}
	
	$tableName = "";
	$fieldKey = "";
	$fieldDesc = "";
	switch ($sPropertyName) {
		case "image":
			$tableName = $aQuestion->QuestionImgTable;
			$fieldKey = $aQuestion->KeyFieldImg;
			$fieldDesc = $aQuestion->DescFieldImg;
			break;
		case "comment":
			$tableName = $aQuestion->QuestionCommTable;
			$fieldKey = $aQuestion->KeyFieldComm;
			$fieldDesc = $aQuestion->DescFieldComm;
			break;
		case "document":
			$tableName = $aQuestion->QuestionDocTable;
			$fieldKey = $aQuestion->KeyFieldDoc;
			$fieldDesc = $aQuestion->DescFieldDoc;
			break;
		case "score":
			$tableName = $aQuestion->QuestionPtsTable;
			$fieldKey = $aQuestion->KeyFieldPts;
			$fieldDesc = $aQuestion->DescFieldPts;
			break;
		case "extra":
			$tableName = $aQuestion->QuestionValTable;
			$fieldKey = $aQuestion->KeyFieldVal;
			$fieldDesc = $aQuestion->DescFieldVal;
			break;
		case "value":
		default:
			$tableName = $aQuestion->QuestionTable;
			$fieldKey = $aQuestion->DescKeyField;
			$fieldDesc = $aQuestion->DescField;
			break;
	}
	
	$sql = "SELECT {$fieldKey}, {$fieldDesc} 
		FROM {$tableName} 
		WHERE {$fieldDesc} = ".$aRepository->DataADOConnection->Quote($value);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table")." (".translate("Property").": ".$sPropertyName."): ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	
	if (!$aRS->EOF) {
		//Ya existe ese valor en la tabla de la pregunta, se retorna su clave
		$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
	}
	else {
		//Se debe insertar dicho valor en la tabla de la pregunta
		$sql = "INSERT INTO {$tableName} ({$fieldDesc}) VALUES (".$aRepository->DataADOConnection->Quote($value).")";
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha pregunta
		$sql = "SELECT {$fieldKey}, {$fieldDesc} 
			FROM {$tableName} 
			WHERE {$fieldDesc} = ".$aRepository->DataADOConnection->Quote($value);
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if (!$aRS) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
	}
	
	return $valueKey;
}

/* Dada la instancia de una pregunta y un valor que corresponde a un catálogo en formato "key_id_SVSep_ValorAttr_SVElem_", obtiene el key correspondiente al valor dentro de la tabla 
del detalle de la pregunta siempre y cuando exista un registro que contenga toda la combinación de los atributos recibidos con los valores exactos, en caso contrario, se insertará un nuevo
registro para esa combinación. Dado a que este método se puede invocar en tiempos distintos entre la definición y la captura correspondiente a esa definición, se preparará para recibir
en el propio archivo de datos la estructura del catálogo que había en el momento de la captura, en caso de no recibirla (como lo será en las versiones iniciales), se asumirá que la
estructura del catálogo en la definición es la misma usada durante la captura, así que se generará en este preciso momento utilizando el orden definido actualmente en la pregunta
Esta función se debe utilizar sólo para preguntas que están relacionadas a un catálogo
//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
*/
function getInsertQCatFieldKeyV6($aRepository, $aQuestion, $value, $arrayData, $sectionKey, $factKeyDimVal, $aRecNum = 0) {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $strNewAttribSep;
	global $strNewDataSep;
	//Array global con las definiciones de catálogo recibidas, si no está asignado el catálogo de esta pregunta, deberá generarlo en este instante para continuar el grabado, usando la
	//definición actual del catálogo, lo cual sólo sirve en caso de capturas recientes a la fecha de definición, pero no servirá para ediciones futuras necesariamente
	global $garrCatalogDefs;
	if (!isset($garrCatalogDefs)) {
		$garrCatalogDefs = array();
	}
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App, este array contiene la lista completa de valores en el mismo orden de atributos definidos en garrCatalogDefs, la
	//manera de indexar es "q"+QuestionID (o "s"+SectionID), luego el índice de registro (o 0 si es estándar), y finalmente contiene el array de atributos en el mismo orden de garrCatalogDefs
	global $garrCatalogData;
	if (!isset($garrCatalogData)) {
		$garrCatalogData = array();
	}
	
	global $saveDataForm;
	
	$arrValueKey = array();
	//Si el valor no se hubiera capturado, simplemente no insertará el registro de respuesta
	if (is_null($value)) {
		return $arrValueKey;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQCatFieldKeyV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$value}", 2, 0, "color:darkgreen;");
	}
	
	//JAPR 2016-04-27: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
	//Si la pregunta es de tipo múltiple choice de catálogo, entonces $value contiene los valores de las múltiples opciones respondidas según el tipo específico de múltiple choice de
	//la que se trate, si es checkbox entonces contiene el nombre de la opción respondida, pero si es tipo numérica o texto entonces contiene el valor del input de dicha opción, pero no
	//el nombre de la opción, por tanto se usará $arrAllValues directamente para recorrer e insertar los valores en lugar de hacerlo como si fuera una simple choice
	//Se invocará a una función especializada para tratar este caso
	if ($aQuestion->QTypeID == qtpMulti) {
		return getInsertQMCCatFieldKeyV6($aRepository, $aQuestion, $value, $arrayData, $sectionKey, $factKeyDimVal, $aRecNum);
	}
	//@JAPR
	
	if ($aQuestion->ValuesSourceType != tofCatalog) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("this is not a valid catalog question");
		return false;
	}
	if ($aQuestion->DataSourceID <= 0 || $aQuestion->DataSourceMemberID <= 0 || (count($aQuestion->CatMembersList) == 0 && $aQuestion->QDisplayMode == dspMenu)) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
		return false;
	}
	
	$intDataSourceID = $aQuestion->DataSourceID;
	$arrUsedDataMembers = array();
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$arrDataSourceDefColl = @$garrCatalogDefs["q".$aQuestion->QuestionID];
	//$arrDataSourceDefColl = @$garrCatalogDefs[$intDataSourceID];
	//Si no se recibió la definición del DataSource desde el App, entonces lo genera en este punto a partir de la definición actual de la pregunta y del DataSource
	if (is_null($arrDataSourceDefColl) || !is_array($arrDataSourceDefColl) || count($arrDataSourceDefColl) == 0) {
		$arrDataSourceDefColl = array();
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aQuestion->DataSourceID);
		if (is_null($objDataSourceMembersColl)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog")."-".translate("Source").")";
			return false;
		}
		
		$arrDataSourceMembersByIDs = array();
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
		}
		
		//Primero agrega los DataSourceMembers que están configurados para la pregunta en el mismo orden que están definidos. Si la pregunta no es tipo Menu, el orden no aplica pero se
		//deben agregar en la secuencia de todos los padres hasta llegar al atributo seleccionado en la pregunta
		if ($aQuestion->QDisplayMode == dspMenu) {
			foreach ($aQuestion->CatMembersList as $intDataSourceMemberID) {
				$strDataSourceMemberName = '';
				$objDataSourceMember = @$arrDataSourceMembersByIDs[$intDataSourceMemberID];
				if (!is_null($objDataSourceMember)) {
					$strDataSourceMemberName = $objDataSourceMember->MemberName;
				}
				
				$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $strDataSourceMemberName);
				$arrUsedDataMembers[$intDataSourceMemberID] = $intDataSourceMemberID;
			}
		}
		else {
			if ($aQuestion->CatalogID <= 0 || $aQuestion->CatMemberID <= 0) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
				return false;
			}
			
			//En este caso se usarán los atributos según el marcado como seleccionado para la pregunta
			$intCatMemberID = $aQuestion->CatMemberID;
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
			if (is_null($objCatalogMembersColl)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
				return false;
			}
			
			foreach ($objCatalogMembersColl as $objCatalogMember) {
				$intDataSourceMemberID = $objCatalogMember->DataSourceMemberID;
				if ($intDataSourceMemberID <= 0) {
					$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attribute")." - ".$objCatalogMember->MemberName.")";
					return false;
				}
				$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $objCatalogMember->MemberName);
				$arrUsedDataMembers[$intDataSourceMemberID] = $intDataSourceMemberID;
			}
		}
		
		//Independientemente del tipo de pregunta, complementa con los DataSourceMembers que no se consideren usados pero que actualmente existan en el DataSource
		foreach ($objDataSourceMembersColl as $objDataSourceMember) {
			$intDataSourceMemberID = $objDataSourceMember->MemberID;
			if (isset($arrUsedDataMembers[$intDataSourceMemberID])) {
				continue;
			}
			
			$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $objDataSourceMember->MemberName);
		}
	}
	
	//Si no hubiera atributos para procesar, debe generar un error pues no podrá grabar la pregunta
	if (count($arrDataSourceDefColl) == 0) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attributes").")";
		return false;
	}
	
	//En este punto ya tiene lo necesario para procesr el grabado de la combinación del catálogo correspondiente, los atributos que no se encuentren entre los valores recibidos por
	//posición se grabarán como NA
	$valueKey = $gblEFormsNAKey;
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$blnValueSet = true;
	$arrAllValues = array();
	if ((string) $value == '') {
		$value = $gblEFormsNA;
		$blnValueSet = false;
	}
	else {
		$arrAllValues = @$garrCatalogData['q'.$aQuestion->QuestionID][$aRecNum];
		if (is_null($arrAllValues)) {
			$arrAllValues = array();
		}
	}
	
	$intIndex = 0;
	$strAdditionalFields = '';
	$strAdditionalValues = '';
	$strCatStructValues = '';
	$strWhere = '';
	$strAnd = '';
	$strAndStruct = '';
	$arrValues = explode($strNewAttribSep, $value);
	//GCRUZ 2016-05-11. Obtener memberID de pregunta tipo metro para grabar correctamente en la tabla
	$intMetroMemberID = -1;
	if ($aQuestion->QDisplayMode == dspMetro)
	{
		$aQuestion->getCatMembersIDs();
		$intMetroMemberID = $aQuestion->CatMembersList[count($aQuestion->CatMembersList)-1];
	}
	//Arma el filtro en el orden en que teóricamente recibió los atributos, basado en el valor indicado
	foreach ($arrDataSourceDefColl as $arrDataSourceDef) {
		$intDataSourceMemberID = (int) @$arrDataSourceDef['key'];
		$strDataSourceMemberName = (string) @$arrDataSourceDef['name'];
		if ($intDataSourceMemberID <= 0) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attribute")." - #".$intIndex.")";
			return false;
		}
		
		$strValue = @$arrValues[$intIndex];
		if (is_null($strValue) || trim($strValue) == '') {
			$strValue = $gblEFormsNA;
			//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
			//Si se asignó el valor de respuesta de esta pregunta pero ya no viene capturado este registro, utiliza el array de valores completos del catálogo que se recibió desde el App
			//para complementar con el primer valor de los atributos no usados directamente por la pregunta
			if ($blnValueSet) {
				if (isset($arrAllValues[$intIndex]) && trim((string) @$arrAllValues[$intIndex]) != '') {
					$strValue = (string) @$arrAllValues[$intIndex];
				}
			}
			//@JAPR
		}
		else {
			$arrData = explode($strNewDataSep, $strValue);
			$strValue = @$arrData[((count($arrData) > 1)?1:0)];
			if (is_null($strValue) || trim($strValue) == '') {
				$strValue = $gblEFormsNA;
			}
		}
		
		$strValue = $aRepository->DataADOConnection->Quote($strValue);
		$strFieldName = BITAMQuestion::$DescFieldCatMember.$intDataSourceMemberID;
		$strWhere .= $strAnd.$strFieldName." = ".$strValue;
		$strAdditionalFields .= ", ".$strFieldName;
		$strAdditionalValues .= ", ".$strValue;
		$strOptKey = "";
		//GCRUZ 2016-05-11. Grabar el "*" dependiendo del último memberID de la pregunta tipo metro
		if ($aQuestion->QDisplayMode == dspMetro)
		{
			if ($intDataSourceMemberID == $intMetroMemberID) {
				$strOptKey = "*";
			}
		}
		else
		{
			if ($intDataSourceMemberID == $aQuestion->DataSourceMemberID) {
				$strOptKey = "*";
			}
		}
		$strCatStructValues .= $strAndStruct."key".$strOptKey."_{$intDataSourceMemberID}_{$strDataSourceMemberName}";
		$strAnd = " AND ";
		$strAndStruct = $strNewAttribSep;
		$intIndex++;
	}
	
	$intQuestionID = $aQuestion->QuestionID;
	$strQuestionID = strval($intQuestionID);
	$tableName = $aQuestion->QuestionDetTable;
	//Por ahora no se requiere, si a este tipo de preguntas se les configura un score, entonces se podrá emplear esta funcionalidad
	$fieldPts = "{$aQuestion->DescFieldPts}_{$intQuestionID}";
	//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
	$fieldKey = BITAMQuestion::$KeyField;
	$fieldStruct = BITAMQuestion::$CatStructField;
	
	//@JAPR 2015-07-22: LRoux determinó que no tenía caso complicar el grabado, así que en lugar de una FK a un catálogo de estructuras, en cada registro se grabará directamente la 
	//estructura como estaba originalmente planeado
	//Obtiene el Key subrrogado de la combinación de atributos que estan grabando este valor específico, insertandolo si era la primera vez que se usaba
	//$intCatStructKey = getInsertQCatStructV6($aRepository, $aQuestion->DataSourceID, $strCatStructValues);
	$intCatStructKey = $aRepository->DataADOConnection->Quote($strCatStructValues);
	
	//Realmente no es una tabla de "catálogo" de descripciones, así que no hay por qué consultar la combinación, siempre se insertará como nueva ya que no hay un campo subrrogados
	//desde esta tabla hacia la de la forma/sección, sino que el campo key de la sección se hereda a esta para poder tener en el caso de múltiple choice múltiples valores, eso es así
	//porque en caso de requerirse un score, si de esta tabla se generara el subrrogado a utilizar en la tabla de forma/sección, se tendría que incluir el score y cualquier otro valor
	//capturable adicional (como los valores extras de múltiple choice) para generar un nuevo key por cada combinación única, y no es práctica esa situación
	/*
	//Primero consulta la tabla en busca de una combinación exacta de estos valores, si la encuentra entonces eso es el key a regresar
	$sql = "SELECT {$fieldKey} 
		FROM {$tableName} 
		WHERE {$strWhere}";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQCatFieldKeyV6 INSERT: ".$sql, 1, 1, "color:blue;");
	}
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	
	if (!$aRS->EOF) {
		//Ya existe ese valor en la tabla de la pregunta, se retorna su clave
		$valueKey =  (int) @$aRS->fields[strtolower($fieldKey)];
	}
	else {
	*/
	
	if($saveDataForm)
	{
	//Se debe insertar dicho valor en la tabla de la pregunta
	//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
	$sql = "INSERT INTO {$tableName} (".BITAMSection::$KeyField.", ".BITAMSurvey::$KeyField.", {$fieldStruct} {$strAdditionalFields}) 
		VALUES ({$sectionKey}, {$factKeyDimVal}, {$intCatStructKey} {$strAdditionalValues})";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQCatFieldKeyV6 INSERT: ".$sql, 1, 1, "color:blue;");
	}
	if (!$aRepository->DataADOConnection->Execute($sql)) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	
	$valueKey = (int) @$aRepository->DataADOConnection->_insertid();
	}
	//}
	
	$arrValueKey[] = $valueKey;
	
	return $valueKey;
}

/* Dada la instancia de una pregunta múltiple choice y un valor que corresponde a las opciones contestadas o al valor de cada opción contestada en formato "opt/val1_SVSep_opt/val2...", 
obtiene el key correspondiente al valor dentro de la tabla del detalle de la pregunta siempre y cuando exista un registro que contenga toda la combinación de los atributos recibidos con 
los valores exactos, en caso contrario, se insertará un nuevo registro para esa combinación. Dado a que este método se puede invocar en tiempos distintos entre la definición y la captura 
correspondiente a esa definición, se preparará para recibir en el propio archivo de datos la estructura del catálogo que había en el momento de la captura, en caso de no recibirla, se 
asumirá que la estructura del catálogo en la definición es la misma usada durante la captura, así que se generará en este preciso momento utilizando el orden definido actualmente en la 
pregunta. Para que esta función haga su labor correctamente, dado a que el App no envía la estructura de valores como atributos del catálogo sino el valor del input o bien sólo la opción
respondida, se dependerá totalmente del array adicional recibido que contiene el orden de los atributos en la secuencia que se respondieron las preguntas, de tal forma que el $value
recibido no sirve para identificar en todos los casos la opción respondida, por tanto no se usará para tal fin sino sólo para extraer el valor adicional cuando no es tipo checkbox
Esta función se debe utilizar sólo para preguntas múltiple choice que están relacionadas a un catálogo
//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
*/
function getInsertQMCCatFieldKeyV6($aRepository, $aQuestion, $value, $arrayData, $sectionKey, $factKeyDimVal, $aRecNum = 0) {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $strNewAttribSep;
	global $strNewDataSep;
	//Array global con las definiciones de catálogo recibidas, si no está asignado el catálogo de esta pregunta, deberá generarlo en este instante para continuar el grabado, usando la
	//definición actual del catálogo, lo cual sólo sirve en caso de capturas recientes a la fecha de definición, pero no servirá para ediciones futuras necesariamente
	global $garrCatalogDefs;
	if (!isset($garrCatalogDefs)) {
		$garrCatalogDefs = array();
	}
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App, este array contiene la lista completa de valores en el mismo orden de atributos definidos en garrCatalogDefs, la
	//manera de indexar es "q"+QuestionID (o "s"+SectionID), luego el índice de registro (o 0 si es estándar), y finalmente contiene el array de atributos en el mismo orden de garrCatalogDefs
	global $garrCatalogData;
	if (!isset($garrCatalogData)) {
		$garrCatalogData = array();
	}
	
	global $saveDataForm;
	
	$arrValueKey = array();
	//Si el valor no se hubiera capturado, simplemente no insertará el registro de respuesta
	if (is_null($value) || (string) $value == '') {
		return $arrValueKey;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQMCCatFieldKeyV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$value}", 2, 0, "color:darkgreen;");
	}
	
	if ($aQuestion->ValuesSourceType != tofCatalog) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("this is not a valid catalog question");
		return false;
	}
	
	//@JAPR 2016-04-26: Agregadas las preguntas tipo múltiple choice de catálogo (un solo atributo) (#VOPSK0)
	//Las preguntas tipo múltiple choice de catálogo con despliegue menú no necesitan una lista de atributos, así que se excluyen
	if ($aQuestion->DataSourceID <= 0 || $aQuestion->DataSourceMemberID <= 0) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
		return false;
	}
	
	$intDataSourceID = $aQuestion->DataSourceID;
	$arrUsedDataMembers = array();
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$arrDataSourceDefColl = @$garrCatalogDefs["q".$aQuestion->QuestionID];
	//$arrDataSourceDefColl = @$garrCatalogDefs[$intDataSourceID];
	//Si no se recibió la definición del DataSource desde el App, entonces lo genera en este punto a partir de la definición actual de la pregunta y del DataSource
	if (is_null($arrDataSourceDefColl) || !is_array($arrDataSourceDefColl) || count($arrDataSourceDefColl) == 0) {
		$arrDataSourceDefColl = array();
		$objDataSourceMembersColl = BITAMDataSourceMemberCollection::NewInstance($aRepository, $aQuestion->DataSourceID);
		if (is_null($objDataSourceMembersColl)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog")."-".translate("Source").")";
			return false;
		}
		
		$arrDataSourceMembersByIDs = array();
		foreach ($objDataSourceMembersColl->Collection as $objDataSourceMember) {
			$arrDataSourceMembersByIDs[$objDataSourceMember->MemberID] = $objDataSourceMember;
		}
		
		//Primero agrega los DataSourceMembers que están configurados para la pregunta en el mismo orden que están definidos. Si la pregunta no es tipo Menu, el orden no aplica pero se
		//deben agregar en la secuencia de todos los padres hasta llegar al atributo seleccionado en la pregunta
		if ($aQuestion->CatalogID <= 0 || $aQuestion->CatMemberID <= 0) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
			return false;
		}
		
		//En este caso se usarán los atributos según el marcado como seleccionado para la pregunta
		$intCatMemberID = $aQuestion->CatMemberID;
		$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
		if (is_null($objCatalogMembersColl)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Catalog").")";
			return false;
		}
		
		foreach ($objCatalogMembersColl as $objCatalogMember) {
			$intDataSourceMemberID = $objCatalogMember->DataSourceMemberID;
			if ($intDataSourceMemberID <= 0) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attribute")." - ".$objCatalogMember->MemberName.")";
				return false;
			}
			$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $objCatalogMember->MemberName);
			$arrUsedDataMembers[$intDataSourceMemberID] = $intDataSourceMemberID;
		}
		
		//Independientemente del tipo de pregunta, complementa con los DataSourceMembers que no se consideren usados pero que actualmente existan en el DataSource
		foreach ($objDataSourceMembersColl as $objDataSourceMember) {
			$intDataSourceMemberID = $objDataSourceMember->MemberID;
			if (isset($arrUsedDataMembers[$intDataSourceMemberID])) {
				continue;
			}
			
			$arrDataSourceDefColl[] = array('key' => $intDataSourceMemberID, 'name' => $objDataSourceMember->MemberName);
		}
	}
	
	//Si no hubiera atributos para procesar, debe generar un error pues no podrá grabar la pregunta
	if (count($arrDataSourceDefColl) == 0) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attributes").")";
		return false;
	}
	
	//En este punto ya tiene lo necesario para procesr el grabado de la combinación del catálogo correspondiente, los atributos que no se encuentren entre los valores recibidos por
	//posición se grabarán como NA
	$valueKey = $gblEFormsNAKey;
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
	$arrAllValues = @$garrCatalogData['q'.$aQuestion->QuestionID][$aRecNum];
	if (is_null($arrAllValues)) {
		$arrAllValues = array();
	}
	
	$arrValues = explode($strNewDataSep, $value);
	//Para este tipo de preguntas se asume que $arrAllValues contiene todos los valores de los atributos existentes indexados en el orden de las respuestas alfabético, ya que el objeto
	//generado en JScript para mandar estos valores tendría por fuerza que haber estado indexado de esa manera ya que así lo maneja JScript, y como el $value tiene que venir en el orden
	//en que son mostrados los elementos en pantalla (que es alfabético), es consistente y se pueden extraer los valores adicionales numéricos/texto en la misma secuencia, sin embargo
	//si la pregunta fuera tipo Ranking (a la fecha de implementación sólo puede ser de tipo de captura CheckBox), el $arrAllValues seguiría estando indexado alfabéticamente pero el
	//$value habría sido reordenado durante el pre-procesamiento según el ranking capturado, así que sería inconsistente asumir que se puede recorrer $arrAllValues, por lo que en ese
	//único caso se tendrá que hacer un reordenamiento de $arrAllValues en base a $value
	$arrAllValuesSorted = $arrAllValues;
	if ($aQuestion->QDisplayMode == dspLabelnum) {
		$arrAllValuesSorted = array();
		foreach ($arrValues as $strOptionText) {
			if (!isset($arrAllValues[$strOptionText])) {
				continue;
			}
			
			$arrAllValuesSorted[$strOptionText] = $arrAllValues[$strOptionText];
		}
	}
	
	$blnAddValue = false;
	if ($aQuestion->QTypeID == qtpMulti && $aQuestion->MCInputType != mpcCheckBox) {
		$blnAddValue = true;
	}
	
	$intQuestionID = $aQuestion->QuestionID;
	$strQuestionID = strval($intQuestionID);
	$tableName = $aQuestion->QuestionDetTable;
	//Por ahora no se requiere, si a este tipo de preguntas se les configura un score, entonces se podrá emplear esta funcionalidad
	$fieldPts = "{$aQuestion->DescFieldPts}_{$intQuestionID}";
	//@JAPR 2015-08-12: Convertidas a variables estáticas para no depender de una instancia
	$fieldKey = BITAMQuestion::$KeyField;
	$fieldStruct = BITAMQuestion::$CatStructField;
	$fieldVal = $aQuestion->ValueField;
	
	//Recorre todas las opciones de respuesta seleccionadas (el índice del $arrAllValues) y por cada una generará un registro diferente
	$intOptionNum = 0;
	foreach ($arrAllValuesSorted as $strOptionText => $arrAllValues) {
		$intIndex = 0;
		$strAdditionalFields = '';
		$strAdditionalValues = '';
		$strCatStructValues = '';
		$strWhere = '';
		$strAnd = '';
		$strAndStruct = '';
		foreach ($arrDataSourceDefColl as $arrDataSourceDef) {
			$intDataSourceMemberID = (int) @$arrDataSourceDef['key'];
			$strDataSourceMemberName = (string) @$arrDataSourceDef['name'];
			if ($intDataSourceMemberID <= 0) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("the question has an invalid configuration")." (".translate("Attribute")." - #".$intIndex.")";
				return false;
			}
			
			$strValue = $gblEFormsNA;
			//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
			//Si se asignó el valor de respuesta de esta pregunta pero ya no viene capturado este registro, utiliza el array de valores completos del catálogo que se recibió desde el App
			//para complementar con el primer valor de los atributos no usados directamente por la pregunta
			if (isset($arrAllValues[$intIndex]) && trim((string) @$arrAllValues[$intIndex]) != '') {
				$strValue = (string) @$arrAllValues[$intIndex];
			}
			
			$strValue = $aRepository->DataADOConnection->Quote($strValue);
			$strFieldName = BITAMQuestion::$DescFieldCatMember.$intDataSourceMemberID;
			$strWhere .= $strAnd.$strFieldName." = ".$strValue;
			$strAdditionalFields .= ", ".$strFieldName;
			$strAdditionalValues .= ", ".$strValue;
			$strOptKey = "";
			if ($intDataSourceMemberID == $aQuestion->DataSourceMemberID) {
				$strOptKey = "*";
			}
			$strCatStructValues .= $strAndStruct."key".$strOptKey."_{$intDataSourceMemberID}_{$strDataSourceMemberName}";
			$strAnd = " AND ";
			$strAndStruct = $strNewAttribSep;
			$intIndex++;
		}
		
		//@JAPR 2015-07-22: LRoux determinó que no tenía caso complicar el grabado, así que en lugar de una FK a un catálogo de estructuras, en cada registro se grabará directamente la 
		//estructura como estaba originalmente planeado
		//Obtiene el Key subrrogado de la combinación de atributos que estan grabando este valor específico, insertandolo si era la primera vez que se usaba
		//$intCatStructKey = getInsertQCatStructV6($aRepository, $aQuestion->DataSourceID, $strCatStructValues);
		$intCatStructKey = $aRepository->DataADOConnection->Quote($strCatStructValues);
		
		//En este punto el valor de este array corresponde exactamente a la opción de respuesta $strOptionText si es una tipo checkbox, o bien es el valor adicional capturado para
		//dicha opción en cualquier otro caso, sólo es útil cuando no es tipo checkbox
		if ($blnAddValue) {
			$strValue = (string) @$arrValues[$intOptionNum];
			$strAdditionalFields .= ", {$fieldVal}";
			if ($aQuestion->MCInputType == mpcText) {
				//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
				//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
				$strValue = $aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strValue));
				//@JAPR
			}
			else {
				//Si siendo un tipo de capturan numérico no hay un valor, para evitar que falle el INSERT se convertirá a NULL (aunque ese caso no debería ser posible debido a que sólo
				//habría llegado este multiAnswer precisamente si hubiera tenido algún valor)
				if (trim($strValue) == '') {
					$strValue = 'NULL';
				}
			}
			$strAdditionalValues .= ", ".$strValue;
		}
		
		if($saveDataForm)
		{
			//Se debe insertar dicho valor en la tabla de la pregunta
			//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
			$sql = "INSERT INTO {$tableName} (".BITAMSection::$KeyField.", ".BITAMSurvey::$KeyField.", {$fieldStruct} {$strAdditionalFields}) 
				VALUES ({$sectionKey}, {$factKeyDimVal}, {$intCatStructKey} {$strAdditionalValues})";
			if (getParamValue('DebugBreak', 'both', '(int)', true)) {
				ECHOString("getInsertQMCCatFieldKeyV6 INSERT: ".$sql, 1, 1, "color:blue;");
			}
			if (!$aRepository->DataADOConnection->Execute($sql)) {
				$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
				return false;
			}
			
			$valueKey = (int) @$aRepository->DataADOConnection->_insertid();
		}
		
		$arrValueKey[] = $valueKey;
		$intOptionNum++;
	}
	
	return $arrValueKey;
}

/* Tabla única que almacena una estructura de atributos de un catálogo cualquiera, para permitir recontruir la captura en un futuro. Esta tabla está ligada a la tabla de datos
de las preguntas de catálogo, es única porque funcionará como un tipo de catálogo de estructuras, permitiendo reutilizar un key subrrogado cuando se repita la combinación
*/
function getInsertQCatStructV6($aRepository, $aDataSourceID, $value) {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $strNewAttribSep;
	global $strNewDataSep;
	
	if (is_null($value) || trim($value) == '') {
		return 0;
	}
	
	//if (getParamValue('DebugBreak', 'both', '(int)', true)) {
	//	ECHOString("getInsertQCatStructV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$value}", 2, 0, "color:darkgreen;");
	//}
	
	$value = $aRepository->DataADOConnection->Quote($value);
	//Primero consulta la tabla en busca de una combinación exacta de estos valores, si la encuentra entonces eso es el key a regresar
	$sql = "SELECT A.DataSourceStructID 
		FROM SI_SV_DataSourceStructs A 
		WHERE A.DataSourceID = {$aDataSourceID} AND A.DataSourceStruct = {$value}";
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQCatStructV6 INSERT: ".$sql, 1, 1, "color:blue;");
	}
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	if (!$aRS) {
		//Crea la estructura por si se había eliminado anteriormente
		//@JAPR 2016-12-15: Modificado el tipo de dato TEXT por LONGTEXT ya que es mas compatible con el TEXT de SQL Server (#51B4HY)
		$sql = "CREATE TABLE SI_SV_DataSourceStructs (
			DataSourceStructID INTEGER NOT NULL AUTO_INCREMENT, 
			DataSourceID INTEGER NOT NULL,
			DataSourceStruct LONGTEXT NULL,
			PRIMARY KEY (`DataSourceStructID`)
		) AUTO_INCREMENT = 1";
		$aRepository->DataADOConnection->Execute($sql);
	}
	
	if ($aRS && !$aRS->EOF) {
		//Ya existe ese valor en la tabla de la pregunta, se retorna su clave
		$valueKey =  (int) @$aRS->fields["datasourcestructid"];
	}
	else {
		//Se debe insertar dicho valor en la tabla de la pregunta
		$sql = "INSERT INTO SI_SV_DataSourceStructs (DataSourceID, DataSourceStruct) 
			VALUES ({$aDataSourceID}, {$value})";
		if (!$aRepository->DataADOConnection->Execute($sql)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return 0;
		}
		
		$valueKey = (int) @$aRepository->DataADOConnection->_insertid();
	}
	
	return $valueKey;
}

/* Dada la instancia de una pregunta y el array de valores, genera el registro correspondiente al valor dentro de la tabla de la pregunta, incluyendo todos sus datos adicionales
según el tipo de pregunta Simple o Múltiple choice de la que se trate (NO aplica para los casos de catálogo, sólo para las de opciones fijas). El parámetro Value sólo está como apoyo
del log, ya que en realidad se graba mucho mas que sólo un valor, pues hay que procesar el Score o el dato adicional en caso de múltiple choice
Para que se grabe este dato primero tiene que estar generado el registro de la sección a la que pertenecerá, ya que se usa como llave foránea ($sectionKey)
//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
*/
function getInsertQOptsFieldKeyV6($aRepository, $aQuestion, $value, $arrayData, $sectionKey, $factKeyDimVal, $aRecNum = 0) {
	global $gblEFormsErrorMessage;
	global $gblEFormsNA;
	global $gblEFormsNAKey;
	global $saveDataForm;
	
	$arrValueKey = array();
	//Si el valor no se hubiera capturado, simplemente no insertará el registro de respuesta
	if (is_null($value)) {
		return $arrValueKey;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("getInsertQOptsFieldKeyV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$value}", 2, 0, "color:darkgreen;");
	}
	
	$strSep = "_SVSep_";
	//Si la pregunta hubiera sido SimpleChoice, aquí habría llegado un solo string, de lo contrario habría llegado una lista separada por $strSep, en todo caso transforma a un
	//Array lo que recibe para procesarlo por igual, ya que sea del tipo que sea la pregunta, graba casi lo mismo de la misma manera. El único ajuste que se hace es si la pregunta fuera
	//múltiple choice con captura diferente de CheckBox, ya que en ese caso lo que llega es el valor adicional capturado y no la lista de opciones seleccionadas, así que en ese único
	//supuesto sobreescribe este valor con el parámetro del array de datos recibido
	$strOptions = $value;
	$blnAddValue = false;
	if ($aQuestion->QTypeID == qtpMulti && $aQuestion->MCInputType != mpcCheckBox) {
		//@JAPR 2016-12-22: Corregido un bug, no se estaba considerando si la sección era o no multi-registro, por tanto cuando si lo era, tomaba el valor sin considerar el número de
		//registro recibido lo cual le regresaba un array en lugar del string con separador, por tanto se grababa equivocada la información (#2NL1XB)
		if ($aQuestion->SectionType == sectNormal) {
			$strOptions = (string) @$arrayData['qoptions'.$aQuestion->QuestionID];
		}
		else {
			$strOptions = (string) @$arrayData['qoptions'.$aQuestion->QuestionID][$aRecNum];
		}
		//@JAPr
		$blnAddValue = true;
	}
	$arrOptions = @explode($strSep, $strOptions);
	if (!is_array($arrOptions)) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("there was an error processing the options list");
		return false;
	}
	
	//Valores para la pregunta múltiple choice que no es tipo de captura CheckBox
	$arrValues = @explode($strSep, $value);
	if (!is_array($arrValues)) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's data")." (".$aQuestion->QuestionID."), ".translate("there was an error processing the values list");
		return false;
	}
	
	//Si no hubiera valores, simplemente sale de la función sin generar error, pero esto no podría ser ya que se validó arriba por posibles errores. Una descripción vacía grabará un NA
	if (count($arrOptions) == 0 || count($arrValues) == 0) {
		return arrValueKey;
	}
	
	//Obtiene la referencia a los scores
	$arrOptionScores = getParamValue('optionScores', 'both', "(array)");
	if (!is_array($arrOptionScores)) {
		$arrOptionScores = array();
	}
	
	//En esta tabla de detalle no se hace un SELECT para buscar la descripción de cada opcion a grabar, simplemente se agrega un registro por cada una
	$intQuestionID = $aQuestion->QuestionID;
	$strQuestionID = strval($intQuestionID);
	$tableName = $aQuestion->QuestionDetTable;
	$fieldDesc = $aQuestion->DescFieldAttr;
	$fieldPts = "{$aQuestion->DescFieldPts}_{$intQuestionID}";
	$fieldVal = $aQuestion->ValueField;
	foreach ($arrOptions as $intOptionNum => $strOptionText) {
		$strValue = (string) @$arrValues[$intOptionNum];
		$intScore = (int) @$arrOptionScores[$strQuestionID][$strOptionText];
		$strAdditionalFields = "";
		$strAdditionalValues = "";
		if ($blnAddValue) {
			$strAdditionalFields .= ", {$fieldVal}";
			if ($aQuestion->MCInputType == mpcText) {
				//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
				//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
				$strValue = $aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strValue));
				//@JAPR
			}
			$strAdditionalValues .= ", ".$strValue;
		}
		
		//@JAPR 2015-08-12: Agregado el parámetro $factKeyDimVal para grabar el Key de la captura en la tabla de las preguntas
		//@JAPR 2015-12-02: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
		//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
		$valueKey=0;
		if($saveDataForm)
		{
		$sql = "INSERT INTO {$tableName} (".BITAMSection::$KeyField.", ".BITAMSurvey::$KeyField.", {$fieldDesc}, {$fieldPts} {$strAdditionalFields}) 
			VALUES ({$sectionKey}, {$factKeyDimVal}, ".$aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($strOptionText)).", {$intScore} {$strAdditionalValues})";
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("getInsertQOptsFieldKeyV6 INSERT: ".$sql, 1, 1, "color:blue;");
		}
		if ($aRepository->DataADOConnection->Execute($sql) === false) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		$valueKey = (int) @$aRepository->DataADOConnection->_insertid();
		}
		$arrValueKey[] = $valueKey;
	}
	
	//El resultado es intrascendente, sin embargo en caso de ocuparse, sería la lista de los Keys insertados en el proceso
	return $arrValueKey;
}

/* Mueve la imagen de la pregunta indicada grabándola en el campo correspondiente para el registro de la tabla de sección de esta pregunta
referenciado por $sectionKey. Adicionalmente guardará la referencia de la imagen en la tabla de eForms, la cual sirve para obtener todas las
imagenes aplicables por captura sin ser un join (usado para los reportes)
//@JAPR 2016-08-12: Corregido el mapeo de preguntas firma/sketch hacia eBavel, para estas preguntas lo que llega en la propiedad answer.photo es un B64 que no se graba localmente
//como archivo de imagen en la carpeta de sincronización, sino que se graba directo en la carpeta de imágenes de las capturas ya procesadas, asó que al mandar a eBavel la imagen,
//lo cual hacía enviando la propiedad answer.photo como si fuera una ruta de imagen, fallaba el copiado. Ahora se sobreescribirá en el $_POST un array por pregunta al momento de
//generar la imagen para almacenar el nombre de archivo con el que quedó y usar eso durante el proceso de mapeo hacia eBavel (#4L5RSC)
Agregado el parámetro $aRecNum para indicar el registro de respuesta específico de esta pregunta que se está procesando (corresponde al registro de la sección multi-registro de la pregunta)
//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
//Se manda el parámetro para indicar que es la imagen adicional y NO la principal de la pregunta (prefijo 'Src'), con este parámetro se modificará el nombre de archivo para no sobrescribir
//el de la respuesta con los trazos sino generarlo como una imagen independiente
*/
function savePhotoV6($aRepository, $aQuestion, $sectionKey, $factKeyDimVal, $insertImage, $aRecNum = 0, $sPrefix = '') {
	global $gblEFormsErrorMessage;
	global $usePhotoPaths;
	global $appVersion;
	global $blnWebMode;
	global $useRecoveryPaths;
	global $gblEFormsNA;
	global $saveDataForm;
	
	$strFieldDocumentName="";
	if(!$saveDataForm)
	{
		return $strFieldDocumentName;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("savePhotoV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, sPrefix == {$sPrefix}, value == {$insertImage}", 2, 0, "color:green;");
	}
	
	//Si no hay imagen que grabar, simplemente regresa el valor de NA
	if (is_null($insertImage) || trim($insertImage) == '') {
		return $gblEFormsNA;
	}
	
	$intSurveyID = $aQuestion->SurveyID;
	//Genera la ruta donde se grabará el archivo, reportando el error en caso de no lograrlo
	$script = $_SERVER["REQUEST_URI"];
	$position = strrpos($script, "/");
	$dir = substr($script, 0, $position);
	$position = strrpos($dir, "/");
	$mainDir = substr($dir, ($position+1));
	
	$DocsDir = getcwd()."\\"."surveyimages";
	$SurveyImagesPath = "\\"."surveyimages";
	if (!file_exists($DocsDir)) {
		if (!mkdir($DocsDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's image")." (".$aQuestion->QuestionID."), ".translate("Unable to create the image's path").": ".$DocsDir;
			return false;
		}
	}
	
	$strRepositoryName = trim($_SESSION["PABITAM_RepositoryName"]);
	$RepositoryDir = $DocsDir."\\".$strRepositoryName;
	$SurveyImagesPath .= "\\".$strRepositoryName;
	if (!file_exists($RepositoryDir)) {
		if (!mkdir($RepositoryDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's image")." (".$aQuestion->QuestionID."), ".translate("Unable to create the image's path").": ".$RepositoryDir;
			return false;
		}
	}
	
	$ProjectDir = $RepositoryDir."\\"."survey_".$intSurveyID;
	$SurveyImagesPath .= "\\"."survey_".$intSurveyID;
	if (!file_exists($ProjectDir)) {
		if (!mkdir($ProjectDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's image")." (".$aQuestion->QuestionID."), ".translate("Unable to create the image's path").": ".$ProjectDir;
			return false;
		}
	}
	
	//Genera el archivo de imagen en el directorio correspondiente
	//survey_10/photo_survey_question_formKey_sectionKey.jpg
	//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	//Se le concatenará el prefijo especificado para no sobrescribir el archivo de imagen normal de la pregunta (solo aplica en preguntas especiales que de alguna manera necesiten grabar
	//múltiples imagenes por cada respuesta)
	if ( !is_string($sPrefix) || trim($sPrefix) == '' ) {
		//Si no es un string o no viene asignado (incluyendo espacios vacios) se eliminará el sufijo para guardar como la imagen normal
		$sPrefix = '';
	}
	$strDocumentName = $sPrefix."photo_{$intSurveyID}_{$aQuestion->QuestionID}_{$factKeyDimVal}_{$sectionKey}.jpg";
	//JAPR
	$strFieldDocumentName = "survey_".$intSurveyID."/".$strDocumentName;
	//$strFieldDocumentName2 = $mainDir."/"."surveyimages"."/".$strRepositoryName."/"."survey_".$intSurveyID."/".$strDocumentName;
	$strCompletePath = $ProjectDir."\\".$strDocumentName;
	
	//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
	$blnIsPhotoName = true;
	//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch || $aQuestion->QTypeID == qtpSketchPlus) {
		if ($usePhotoPaths) {
			$blnIsPhotoName = false;
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			if (substr($insertImage, 0, 5) == "photo" || substr($insertImage, 0, 5) == "canva" || substr($insertImage, 0, 6) == "sketch" ) {
				$blnIsPhotoName = true;
			}
			//@JAPR 2019-03-04: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			elseif ( $sPrefix ) {
				//En caso de no ser un archivo generado por el App sino una imagen directa, la debe considerar como tal siempre y cuando se reciba un prefijo, pues en ese caso no se
				//tiene control sobre que tipo de imagen fue utilizada desde el dispositivo / browser ni el nombre que se le asignó
				$blnIsPhotoName = true;
			}
		}
	}
	
	//Conchita 2015-01-14
	//Si es webmode validamos si trae la palabra canvas o photo
	if($blnWebMode){
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch || $aQuestion->QTypeID == qtpSketchPlus) {
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			if (strpos($insertImage, "photo") || strpos($insertImage, "canvas") || strpos($insertImage, "sketch")) {
				$blnIsPhotoName = true;
			}
		}
	}
	//Conchita
	
	if ($usePhotoPaths && $blnIsPhotoName) {
		//Realiza la copia del archivo de imagen
		//@JAPR 2012-11-12: Identificado si la carga se hace desde un browser o móvil
		if ($blnWebMode) {
			$path = getcwd()."\\";
		}
		else {
			//@JAPR 2013-02-27: DAbdo solicitó que no se regresaran errores al App así que volverá a funcionar como en v3 dejando un log de error
			$path = getcwd()."\\".(($useRecoveryPaths)?"syncerrors":"syncdata")."\\".$strRepositoryName;
		}
		
		//@JAPR 2018-11-21: Corregido un bug con el grabado de archivos que contiene caracteres fuera del rango ASCII, se identificó que Windows graba los archivos en ASCII o utf16, por tanto
		//al recibir los nombres como utf8, los estaba convirtiendo a la representación ASCII y no eran encontrados por el tag IMG del componente del upload, así que se modificó el código
		//para que ahora grabe los nombres de archivos en ASCII y así ya no habrá problema al buscarlos (#R45F5N)
		$strCompleteSourcePath = str_replace("/", "\\", $path."\\".utf8_decode($insertImage));
		//@JAPR
		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
		if (strpos(strtolower($strCompleteSourcePath), '.jpg') === false) {
			$strCompleteSourcePath .= '.jpg';
		}
		$status = @copy($strCompleteSourcePath, $strCompletePath);
	}
	else {
		if ($insertImage == "photoimage") {
			$tmpfilename = $_FILES['photoimage'.$aQuestion->QuestionID]['tmp_name'];
			$status = move_uploaded_file($tmpfilename, $strCompletePath);
		} else {
			//@JAPR 2016-04-29: Corregido un bug, faltaba decodificar el B64 que es lo que se recibe como imagen de firma desde web (#CD1OMS)
			$status = file_put_contents ($strCompletePath, base64_decode($insertImage));
			//@JAPR
		}
	}
	
	if ($status) {
		//Conchita 2-nov-2011 si es android la imagen no se debe de rotar
		//@JAPR 2013-02-25: Modificada la forma de validar para usar una función genérica que ya se tenía (caso de curl donde HTTP_USER_AGENT no existe)
		$nav = (isset($_SERVER['HTTP_USER_AGENT']))?$_SERVER['HTTP_USER_AGENT']:'';
		//@JAPR 2012-05-22: Agregadas las preguntas tipo Firma
		//@JAPR 2019-02-05: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		if ($aQuestion->QTypeID == qtpSignature || $aQuestion->QTypeID == qtpSketch || $aQuestion->QTypeID == qtpSketchPlus) {
			//Si la pregunta es tipo firma, realmente lo que llega es un .png, pero se disfraza como .jpg
			//En este caso NO se va a renombrar el archivo a Signature_####_###, se dejará como si fuera una Foto de cualquier pregunta
			$status = true;
		}
		//@JAPR
		else
		{
			//@JAPR 2012-10-22: Agregado el grabado de fotos a partir de la ruta donde se subieron
			if ($usePhotoPaths) {
				//En este caso la imagen ya estaba rotada, así que no hay que hacer procesamiento especial
			}
			else {
				//Conchita corregido para la 3.00018 no debe de rotar
				if (!preg_match('/android/i',$nav) && $insertImage != "photoimage" && $appVersion < 3.00018) {
					//la imagen se guarda rotada, se debe volver a rotar en el servidor
					$origen = imagecreatefromjpeg($strCompletePath);
					$rotar = imagerotate($origen, -90, 0);
					$status = imagejpeg($rotar, $strCompletePath);
				}
			}
		}
	}
	else {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's image")." (".$aQuestion->QuestionID."), ".translate("Unable to create the image file");
		return false;
	}
	
	//Por ahora en V6 no habrá compatibilidad hacia atrás, ya que el nombre del archivo de imagen cambió y no se generan los registros de la misma manera
	/*
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar imagenes a otra ruta
	$blnCopyToAltPath = false;
	$strDestPath = '';
	require_once('settingsvariable.inc.php');
	$objSettingReqAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'REQUIREDVERSIONFORALTSERVER');
	if (!is_null($objSettingReqAltVersion) && trim($objSettingReqAltVersion->SettingValue != ''))
	{
		$objSettingAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICEVERSION');
		//Si la app que se conecta es menor que el servicio alterno (si existe) entonces se usa este
		//sino entonces debe ocupar la config de mirror
		if($appVersion < $objSettingAltVersion->SettingValue) {
			$objSettingAltService = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICE');
			if(!is_null($objSettingAltService) && trim($objSettingAltService->SettingValue != '')) {
				$blnCopyToAltPath = true;
				$tmpAltService = substr($objSettingAltService->SettingValue, strpos($objSettingAltService->SettingValue, "/"));
				$strDestPath = $tmpAltService;
			}
		} else {
			$objSettingMirror = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'SURVEYIMAGESMIRRORFOLDER');
			if(!is_null($objSettingMirror) && trim($objSettingMirror->SettingValue != '')) {
				$blnCopyToAltPath = true;
				$strDestPath = $objSettingMirror->SettingValue;
			}
		}
	}
	
	if ($blnCopyToAltPath && trim($strDestPath) != '') {
		//Si el path configurado es vía http, se debe hacer una transferencia de archivo a otro server
		if (strtolower(substr($strDestPath, 0, 4)) == 'http') {
			
		}
		else {
			//En este caso es una ruta relativa, se asume que está en el mismo server por lo que
			//simplemente de la ruta de $strCompletePath elimina la referencia al servicio actual
			//y pone la ruta especificada, pero en el mismo directorio base
			$strBasePath = getcwd();	//Ruta completa de este script desde la unidad C:\, incluyendo el path del servicio
			$strCurrServicePath = substr($_SERVER["REQUEST_URI"], 0, strrpos($_SERVER["REQUEST_URI"], "/"));	//Solo el path del servicio empezando por "/" sin "/" al final
			$strCurrServicePath = str_ireplace("/", "\\", $strCurrServicePath);
			$strBasePath = str_ireplace($strCurrServicePath, "", $strBasePath);	//Sólo la ruta de la raíz sin el path del servicio
			//Concatena a la ruta raiz base, la ruta alternativa de servicio configurada
			$strDestPath = $strBasePath.$strDestPath;
		}
		
		//Finalmente a la ruta obtenida le concatena el nombre de archivo, el cual empieza desde el path SurveyImages
		$strDestPath .= "\\".$SurveyImagesPath;
		$strDestPath = str_ireplace('/', "\\", $strDestPath);
		$strDestPath = str_ireplace("\\\\", "\\", $strDestPath);
		//Primero intenta crear la ruta generada
		@mkdir($strDestPath, 0777, true);
		//Luego copia el archivo a la nueva ruta
		@copy($strCompletePath, $strDestPath."\\".$strDocumentName);
	}
	*/
	//@JAPR
	
	//Graba el resgistro en la tabla de imagenes global de eForms. Se conservará la misma tabla aunque el campo MainFactKey ahora representa el sectionKey
	//de la tabla donde se encuentra la pregunta
	if ($status) {
		//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//La tabla a utilizar debe considerar el prefijo correspondiente para no sobrescribir la respuesta normal de esta pregunta (en este caso en el nombre de la tabla NO es un prefijo
		//sino un sufijo para facilidad de manejo)
		$sql = "INSERT INTO SI_SV_SurveyAnswerImage{$sPrefix} (SurveyID, QuestionID, MainFactKey, FactKey, PathImage) 
			VALUES ({$intSurveyID}, {$aQuestion->QuestionID}, {$sectionKey}, {$factKeyDimVal}, ".
				$aRepository->DataADOConnection->Quote($strFieldDocumentName).
			")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("savePhotoV6 INSERT: ".$sql, 1, 1, "color:royalblue;");
		}
		if (!$aRepository->DataADOConnection->Execute($sql)) {
			//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			//La tabla a utilizar debe considerar el prefijo correspondiente para no sobrescribir la respuesta normal de esta pregunta (en este caso en el nombre de la tabla NO es un prefijo
			//sino un sufijo para facilidad de manejo)
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerImage{$sPrefix} ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
		
		//@JAPR 2016-08-12: Corregido el mapeo de preguntas firma/sketch hacia eBavel, para estas preguntas lo que llega en la propiedad answer.photo es un B64 que no se graba localmente
		//como archivo de imagen en la carpeta de sincronización, sino que se graba directo en la carpeta de imágenes de las capturas ya procesadas, asó que al mandar a eBavel la imagen,
		//lo cual hacía enviando la propiedad answer.photo como si fuera una ruta de imagen, fallaba el copiado. Ahora se sobreescribirá en el $_POST un array por pregunta al momento de
		//generar la imagen para almacenar el nombre de archivo con el que quedó y usar eso durante el proceso de mapeo hacia eBavel (#4L5RSC)
		//@JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Si se está procesando una imagen adicional, NO se sobrescribe el valor de la imagen normal a enviar a eBavel como respuesta
		if ( !$sPrefix) {
			if (!isset($_POST["questionB64Images"][$aQuestion->QuestionID])) {
				$_POST["questionB64Images"][$aQuestion->QuestionID] = array();
			}
			
			//Se debe remover getcwd(), ya que el proceso que moverá la imagen a eBavel lo vuelve a incluir
			$_POST["questionB64Images"][$aQuestion->QuestionID][$aRecNum] = str_ireplace(getcwd(), '', $strCompletePath);
		}
		//@JAPR
	}
	
	//Regresa la ruta del archivo si todo estuvo bien
	return $strFieldDocumentName;
}

/* Mueve el documento de la pregunta indicada grabándolo en el campo correspondiente para el registro de la tabla de sección de esta pregunta
referenciado por $sectionKey. Adicionalmente guardará la referencia del documento en la tabla de eForms, la cual sirve para obtener todos los
documentos aplicables por captura sin ser un join (usado para los reportes)
*/
function saveDocumentV6($aRepository, $aQuestion, $sectionKey, $factKeyDimVal, $insertDocument) {
	global $gblEFormsErrorMessage;
	global $usePhotoPaths;
	global $appVersion;
	global $blnWebMode;
	global $useRecoveryPaths;
	global $gblEFormsNA;
	global $saveDataForm;
	
	$strFieldDocumentName="";
	
	if(!$saveDataForm)
	{
		return $strFieldDocumentName;
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveDocumentV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$insertDocument}", 2, 0, "color:green;");
	}
	
	//Si no hay documento que grabar, simplemente regresa el valor de NA
	if (is_null($insertDocument) || trim($insertDocument) == '') {
		return $gblEFormsNA;
	}
	
	//si es movil
	//buscamos el nombre del archivo ya que en movil viene la ruta del dispositivo
	//movil
	if (!$blnWebMode) {
		//obtenemos el nombre del archivo de audio y video (en android y en el caso de video de ios viene con / y una ruta del dispositivo)
		$x = strrpos($insertDocument,'/');
		if ($x !== false) {
			$insertDocument = substr($insertDocument, $x+1);	
		}
	}
	//continua normal
	
	$intSurveyID = $aQuestion->SurveyID;
	//Genera la ruta donde se grabará el archivo, reportando el error en caso de no lograrlo
	$script = $_SERVER["REQUEST_URI"];
	$position = strrpos($script, "/");
	$dir = substr($script, 0, $position);
	$position = strrpos($dir, "/");
	$mainDir = substr($dir, ($position+1));
	
	//validar si es movil debe de buscar en syncdata no en surveydocuments
	//@JAPR 2015-08-11: Corregido un bug con el grabado de documentos desde dispositivo, estaba usando la misma ruta destino que fuente, y al final terminaba eliminando el documento por 
	//completo impidiendo que se pudiera hacer el proceso de Reupload
	/*if (!$blnWebMode) {
		$DocsDir = getcwd()."\\"."syncdata";

	} else {*/
		$DocsDir = getcwd()."\\"."surveydocuments";
	//}
	//@JAPR
	
	//$SurveyImagesPath = "\\"."surveydocuments";
	if (!file_exists($DocsDir)) {
		if (!mkdir($DocsDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's document")." (".$aQuestion->QuestionID."), ".translate("Unable to create the document's path").": ".$DocsDir;
			return false;
		}
	}
	
	$strRepositoryName = trim($_SESSION["PABITAM_RepositoryName"]);
	$RepositoryDir = $DocsDir."\\".$strRepositoryName;
	//$SurveyImagesPath .= "\\".$strRepositoryName;
	if (!file_exists($RepositoryDir)) {
		if (!mkdir($RepositoryDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's document")." (".$aQuestion->QuestionID."), ".translate("Unable to create the document's path").": ".$RepositoryDir;
			return false;
		}
	}
	
	$ProjectDir = $RepositoryDir."\\"."survey_".$intSurveyID;
	//$SurveyImagesPath .= "\\"."survey_".$intSurveyID;
	if (!file_exists($ProjectDir)) {
		if (!mkdir($ProjectDir)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error saving the question's document")." (".$aQuestion->QuestionID."), ".translate("Unable to create the document's path").": ".$ProjectDir;
			return false;
		}
	}
	
	//@JAPR 2015-08-13: Corregido un bug, los archivos de audio y video deben tener otro prefijo, se estaban identificando todos como document
	$strPrefix = "";
	switch ($aQuestion->QTypeID) {
		case qtpAudio:
			$strPrefix = "audio";
			break;
		case qtpVideo:
			$strPrefix = "video";
			break;
		default:
			$strPrefix = "document";
			break;
	}
	
	//@JAPR 2015-08-14: Corregido un bug, en Android los archivos de audio se graban como .amr pero incorrectamente se estaban enviando con extensión .mp3 y no funcionaban
	$strExtension = "";
	$intDeviceID = identifyDeviceType();
	switch ($intDeviceID) {
		case dvciPod:
		case dvciPad:
		case dvciPadMini:
		case dvciPhone:
			//En IOs la extensión correcta es .wav
			$strExtension = ".wav";
			break;
		case dvcCel:
		case dvcTablet:
			//En Android la extensión correcta es .amr
			$strExtension = ".amr";
			break;
		default:
			//Cualquier otro dispositivo, o básicamente browser, no se tiene identificada la extensión, se dejará la que se recibió
			break;
	}
	
	//Genera el archivo de documento en el directorio correspondiente
	//survey_10/document_survey_question_formKey_sectionKey.ext
	//los archivos temporales se guardan como tmpsurveydocuments/yyyyMMddhhmmss/bftxxxx.FILE.ext, ej.
	//tmpsurveydocuments/20121127090437/bft43dd.FormsV4.xlsx 
	if (!$blnWebMode) {
		//si es movil, entonces el nombre es el insertdocument previamente limpiado
		$documentOriginalName = $insertDocument;
		$strDocumentName = $documentOriginalName;
		//@JAPR 2015-08-14: Corregida la extensión del archivo recibido desde Android, ahora se usará $documentOriginalName para el archivo recibido y $strDocumentName para el local
		if ($intDeviceID == dvcCel || $intDeviceID == dvcTablet) {
			$strDocumentName = str_ireplace(".mp3", $strExtension, $strDocumentName);
		}
		//@JAPR 2015-08-11: Corregido el grabado de documentos desde dispositivo, no estaba dejando la ruta a partir del survey
		//$strFieldDocumentName = $strDocumentName;
		$strFieldDocumentName = "survey_".$intSurveyID."/".$strDocumentName;
		//@JAPR
		//$strFieldDocumentName2 = $mainDir."/"."syncdata"."/".trim($strRepositoryName)."/"."survey_".$intSurveyID."/".$strDocumentName;
	}
	else {
		//es web entonces se hace como antes
		//@JAPR 2015-07-13: Aparentemente sólo los documentos de preguntas tipo documento se subían con el nombre adecuado estilo Web, las tipo Audio/Video se subían con una ruta
		//dentro de syncData, así que hay que modificar el algoritmo para identificar el nombre del archivo
		if ($aQuestion->QTypeID == qtpDocument) {
			$documentOriginalName = substr($insertDocument,42);
		}
		else {
			$arrPaths = explode("\\", $insertDocument);
			if (count($arrPaths) > 0) {
				$strInsertDocument = $arrPaths[count($arrPaths)-1];
			}
			else {
				$strInsertDocument = $insertDocument;
			}
			
			//Obtiene el nombre del archivo asumiendo que se generó con un nombre que usa un prefijo aleatorio
			$arrFileParts = explode(".", $strInsertDocument);
			if (count($arrFileParts) > 0) {
				unset($arrFileParts[0]);
				$documentOriginalName = implode(".", $arrFileParts);
			}
			else {
				$documentOriginalName = $strInsertDocument;
			}
		}
		
		$strDocumentName = "{$strPrefix}_{$intSurveyID}_{$aQuestion->QuestionID}_{$factKeyDimVal}_{$sectionKey}_{$documentOriginalName}";
		//@JAPR
		$strFieldDocumentName = "survey_".$intSurveyID."/".$strDocumentName;
		//$strFieldDocumentName2 = $mainDir."/"."surveydocuments"."/".trim($strRepositoryName)."/"."survey_".$intSurveyID."/".$strDocumentName;
	}
	
	//Esta variable contiene el lugar a donde se copiará el archivo finalmente
	$strCompletePath = $ProjectDir."\\".$strDocumentName;
	
	//Realiza la copia del archivo de documento
	$path = getcwd()."\\";
	
	//Esta variable contiene el lugar donde el archivo se subió originalmente para procesarse, según si es Web o no, es una ruta temporal o syncData
	if (!$blnWebMode) {
		//si es movil se graba en syncdata
		//@JAPR 2015-08-14: Corregida la extensión del archivo recibido desde Android, ahora se usará $documentOriginalName para el archivo recibido y $strDocumentName para el local
		$strCompleteSourcePath = $path."syncdata"."/".trim($strRepositoryName)."/".$documentOriginalName;
		$strCompleteSourcePath = str_replace("/", "\\", $strCompleteSourcePath);
		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
	} else {
		//Si es Web se graba en una carpeta temporal y el propio nombre de archivo original contiene dicha ruta
		$strCompleteSourcePath = str_replace("/", "\\", $path."\\".$insertDocument);
		$strCompleteSourcePath = str_replace("\\\\", "\\", $strCompleteSourcePath);
	}
	
	//Mueve el archivo al lugar donde finalmente se almacena como parte de la captura ya procesada
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("Copying {$strPrefix} from {$strCompleteSourcePath} to {$strCompletePath}");
	}
	$status = @copy($strCompleteSourcePath, $strCompletePath);
	
	//Conchita 2014-11-27 Si es movil, borramos el archivo anterior ya que se estaba guardando dos veces
	if (!$blnWebMode && $status) {
		//@JAPR 2015-08-11: Corregido un bug con el grabado de documentos desde dispositivo, estaba usando la misma ruta destino que fuente, y al final terminaba eliminando el documento por 
		//completo impidiendo que se pudiera hacer el proceso de Reupload
		//@JAPR 2015-08-11: Temporalmente se evita eliminar el archivo original para permitir reprocesarlo, en lo que se estabiliza el proceso y se decide como proceder
		//@unlink($strCompleteSourcePath);
		//@JAPR
	}
	
	//Por ahora en V6 no habrá compatibilidad hacia atrás, ya que el nombre del archivo de documento cambió y no se generan los registros de la misma manera
	/*
	//@JAPR 2012-10-26: Agregada la posibilidad de trabajar con 2 servicios simultáneamente y copiar documentos a otra ruta
	$blnCopyToAltPath = false;
	$strDestPath = '';
	require_once('settingsvariable.inc.php');
	$objSettingReqAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'REQUIREDVERSIONFORALTSERVER');
	if (!is_null($objSettingReqAltVersion) && trim($objSettingReqAltVersion->SettingValue != ''))
	{
		$objSettingAltVersion = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICEVERSION');
		//Si la app que se conecta es menor que el servicio alterno (si existe) entonces se usa este
		//sino entonces debe ocupar la config de mirror
		if($appVersion < $objSettingAltVersion->SettingValue) {
			$objSettingAltService = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'ALTEFORMSSERVICE');
			if(!is_null($objSettingAltService) && trim($objSettingAltService->SettingValue != '')) {
				$blnCopyToAltPath = true;
				$tmpAltService = substr($objSettingAltService->SettingValue, strpos($objSettingAltService->SettingValue, "/"));
				$strDestPath = $tmpAltService;
			}
		} else {
			$objSettingMirror = BITAMSettingsVariable::NewInstanceWithName($aRepository, 'SURVEYIMAGESMIRRORFOLDER');
			if(!is_null($objSettingMirror) && trim($objSettingMirror->SettingValue != '')) {
				$blnCopyToAltPath = true;
				$strDestPath = $objSettingMirror->SettingValue;
			}
		}
	}
	
	if ($blnCopyToAltPath && trim($strDestPath) != '') {
		//Si el path configurado es vía http, se debe hacer una transferencia de archivo a otro server
		if (strtolower(substr($strDestPath, 0, 4)) == 'http') {
			
		}
		else {
			//En este caso es una ruta relativa, se asume que está en el mismo server por lo que
			//simplemente de la ruta de $strCompletePath elimina la referencia al servicio actual
			//y pone la ruta especificada, pero en el mismo directorio base
			$strBasePath = getcwd();	//Ruta completa de este script desde la unidad C:\, incluyendo el path del servicio
			$strCurrServicePath = substr($_SERVER["REQUEST_URI"], 0, strrpos($_SERVER["REQUEST_URI"], "/"));	//Solo el path del servicio empezando por "/" sin "/" al final
			$strCurrServicePath = str_ireplace("/", "\\", $strCurrServicePath);
			$strBasePath = str_ireplace($strCurrServicePath, "", $strBasePath);	//Sólo la ruta de la raíz sin el path del servicio
			//Concatena a la ruta raiz base, la ruta alternativa de servicio configurada
			$strDestPath = $strBasePath.$strDestPath;
		}
		
		//Finalmente a la ruta obtenida le concatena el nombre de archivo, el cual empieza desde el path SurveyImages
		$strDestPath .= "\\".$SurveyImagesPath;
		$strDestPath = str_ireplace('/', "\\", $strDestPath);
		$strDestPath = str_ireplace("\\\\", "\\", $strDestPath);
		//Primero intenta crear la ruta generada
		@mkdir($strDestPath, 0777, true);
		//Luego copia el archivo a la nueva ruta
		@copy($strCompletePath, $strDestPath."\\".$strDocumentName);
	}
	*/
	//@JAPR
	
	//Graba el resgistro en la tabla de documentos global de eForms. Se conservará la misma tabla aunque el campo MainFactKey ahora representa el sectionKey
	//de la tabla donde se encuentra la pregunta
	if ($status) {
		$sql = "INSERT INTO SI_SV_SurveyAnswerDocument (SurveyID, QuestionID, MainFactKey, FactKey, PathDocument) 
			VALUES ({$intSurveyID}, {$aQuestion->QuestionID}, {$sectionKey}, {$factKeyDimVal}, ".
				$aRepository->DataADOConnection->Quote($strFieldDocumentName).
			")";
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			ECHOString("saveDocumentV6 INSERT: ".$sql, 1, 1, "color:royalblue;");
		}
		if (!$aRepository->DataADOConnection->Execute($sql)) {
			$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
			return false;
		}
	}
	
	//Regresa la ruta del archivo si todo estuvo bien
	return $strFieldDocumentName;
}

/* Guarda el comentario de la pregunta indicada grabándolo en el campo correspondiente para el registro de la tabla de sección de esta pregunta
referenciado por $sectionKey. Adicionalmente guardará la referencia del comentario en la tabla de eForms, la cual sirve para obtener todos los
comentarios aplicables por captura sin ser un join (usado para los reportes)
*/
function saveCommentV6($aRepository, $aQuestion, $sectionKey, $factKeyDimVal, $insertComment) {
	global $gblEFormsErrorMessage;
	global $usePhotoPaths;
	global $appVersion;
	global $blnWebMode;
	global $useRecoveryPaths;
	global $gblEFormsNA;
	
	global $saveDataForm;
	
	if(!$saveDataForm)
	{
		return "";
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveCommentV6 QuestionID == ".((int) @$aQuestion->QuestionID).", sectionKey == {$sectionKey}, value == {$insertComment}", 2, 0, "color:green;");
	}
	
	//Si no hay comentario que grabar, en este caso se inserta vacío el campo, no con un valor de NA ya que cualquier tipo de texto es válido como comentario
	if (is_null($insertComment) || trim($insertComment) == '') {
		return '';
	}
	
	$intSurveyID = $aQuestion->SurveyID;
	//@JAPR 2015-12-01: Validado que al hacer una inserción de texto a las tablas, se remuevan los caracteres UNICODE posteriores al rango U-FFFF, ya que los campos utf8 
	//de MySQL sólo soportan un subconjunto de 3 bytes de UNICODE, así que no los puede almacenar. Se reemplazarán por "?" para permitir el grabado (#JHSE3G)
	$sql = "INSERT INTO SI_SV_SurveyAnswerComment (SurveyID, QuestionID, MainFactKey, FactKey, StrComment) 
		VALUES ({$intSurveyID}, {$aQuestion->QuestionID}, {$sectionKey}, {$factKeyDimVal}, ".
			$aRepository->DataADOConnection->Quote(RemoveUNICODEFromUtf8Str($insertComment)).
		")";
	//@JAPR
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		ECHOString("saveCommentV6 INSERT: ".$sql, 1, 1, "color:royalblue;");
	}
	if (!$aRepository->DataADOConnection->Execute($sql)) {
		$gblEFormsErrorMessage = "(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAnswerDocument ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql;
		return false;
	}
	
	//Regresa la ruta del archivo si todo estuvo bien
	return $insertComment;
}

function getSurveyAnsweredQuestionsV6($arrayData, $aRepository, $surveyID) {
	//conteo de preguntas contestadas
	$answeredQuestionsCount = 0;
	$questionCollection = BITAMQuestionCollection::NewInstance($aRepository, $surveyID);
	//@JAPRWarning: Falta considerar las preguntas de tipo Foto y firma, esas no contienen un qField pero se debe validar la foto que pudieran haber grabado
	foreach ($questionCollection->Collection as $questionKey => $aQuestion)
	{
		$postKey = "qfield".$aQuestion->QuestionID;
		if(isset($arrayData[$postKey]))
		{
			if($arrayData[$postKey] != '') {
				$answeredQuestionsCount++;
			}
		}
	}
	return $answeredQuestionsCount;
}

//@JAPR 2015-03-05: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
/* Función equivalente a replaceQuestionVariables pero con variables en el nuevo formato extendido. Realiza los mismos reemplazos que el server
había realizado hasta el día de hoy, los cuales no corresponden exactamente a los que son posible realizar en el App dado al cambio de contexto,
pero debe ser completamente compatible con la otra función de reemplazo de variables en el viejo formato
Esta función se utilizará para resolver filtros dinámicos generados para agendas durante la obtención de definiciones, simulando el array de
datos de un grabado de captura, esto para permitir generar el array de valores de todos los catálogos que pudieran estar filtrados a partir de
alguna pregunta que corresponda con el propio catálogo de la agenda
//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
//@JAPR 2018-01-18: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
Agregado el parámetro $syncDataDestinationOpt generado originalmente para ser enviado a la clase SyncDataDestination de manera que los procesos de destino de datos
tuvieran accceso a todos los datos de las capturas, con este parámetro se pueden resolver variables de la captura usando esta función, así que será el nuevo estándar
armarlo como se hace para SyncDataDestination en caso de requerir algo como esto en futuras implementaciones, así no se obtendrán directo del $_POST en cada caso
*/
function replaceQuestionVariablesAdvV6($sText, $arrFieldValues, $bReplaceByNumber = true, $aRepository = null, $aSurveyID = 0, $bQuoteText = true, $bForceGetAttribs = false, $bReturnExtendedInfo = false, &$arrInfo = array(), $nRow = 0, $syncDataDestinationOpt = null)
{
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	global $aSectionEvalRecord;
	global $arrQuestionScoresFromApp;
	global $garrCatalogData;
	global $garrCatalogDefs;
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariablesAdvV6 expression: {$sText}");
	}
	
	if (!isset($arrQuestionScoresFromApp) || is_null($arrQuestionScoresFromApp) || !is_array($arrQuestionScoresFromApp)) {
		$arrQuestionScoresFromApp = array();
	}
	
	//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
	if ($bReturnExtendedInfo) {
		if (!is_array($arrInfo)) {
			$arrInfo = array();
		}
		if (!isset($arrInfo['elements'])) {
			$arrInfo['elements'] = array();
		}
	}
	
	//@JAPR 2018-01-18: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
	//Agrega el reemplazo de las variables correspondientes al App de captura, mismas que originalmente no estaban soportadas por el server
	$sText = replaceEntryVariables($sText, $syncDataDestinationOpt);
	
	//@JAPR 2015-10-14: Agregadas variables para las secciones en v6 (#TI6HON)
	//Antes de iniciar el reemplazo de las variables de preguntas, invoca al reemplazo de las variables de sección
	$strText = @replaceSectionVariablesAdvV6($sText, $arrFieldValues, true, $aRepository, $aSurveyID, $bQuoteText, $bForceGetAttribs, $bReturnExtendedInfo, $arrInfo, $nRow);
	if (!is_null($strText)) {
		$sText = $strText;
	}
	//@JAPR
	
	$intOffSet = 0;
	$intPos = stripos($sText, '{$Q', $intOffSet);
	if ($intPos === false || is_null($arrFieldValues) || !is_array($arrFieldValues))
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n replaceQuestionVariablesAdvV6 no variables found");
		}
		return $sText;
	}
	
	//@JAPR 2014-10-29: Agregado el mapeo de Scores
	if (is_null($aSectionEvalRecord) || !is_array($aSectionEvalRecord)) {
		$aSectionEvalRecord = array();
	}
	
	//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
	//o en el ID dentro del texto especificado
	$arrQuestionsByID = array();
	$arrQuestionObjsByNum = array();
	$arrQuestionObjsByID = array();
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Ahora siempre se debe cargar la colección de preguntas
	//if (!$bReplaceByNumber) {
		//Carga la colección completa de preguntas de la encuesta (esta llamada debería estar en el caché, así que sólo la primera vez sería
		//tardada teoricamente)
		if (!is_null($aRepository)) {
			$aQuestionsColl = BITAMQuestionCollection::NewInstance($aRepository, $aSurveyID);
			if (!is_null($aQuestionsColl)) {
				foreach ($aQuestionsColl->Collection as $aQuestion) {
					$arrQuestionsByID[$aQuestion->QuestionID] = $aQuestion->QuestionNumber;
					$arrQuestionObjsByNum[$aQuestion->QuestionNumber] = $aQuestion;
					$arrQuestionObjsByID[$aQuestion->QuestionID] = $aQuestion;
				}
			}
		}
	//}
	//@JAPR
	
	$strText = $sText;
	$pattern = '/[\{]{1}[$]{1}[Q|q]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
	preg_match_all($pattern, $strText, $arrayEvalQuestionsAdv);
	$arrayQuestionsAdv = array();
	if (count($arrayEvalQuestionsAdv[0]) > 0) {
		$arrayQuestionsAdv = array_unique($arrayEvalQuestionsAdv[0]);
	}
	
	foreach ($arrayQuestionsAdv as $strExpr) {
		//Primero intentará separar por el total de propiedades/funciones especificadas, esto es las cadenas separadas por '.'. Siempre la última es la
		//única que podría ser una función así que se valida si está o no definda así como su parámetro en caso de haber alguno
		//Sólo puede haber una propiedad, una función, o una propiedad + una función, todo aplicando a una misma pregunta y/o sección
		$strProperty = '';
		$strFunction = '';
		$strParam = '';
		$arrParts = explode('.', $strExpr);
		$strQuestionID = $arrParts[0];
		$blnValid = true;
		
		$strQuestionID = (int) @str_ireplace(array('{', '}', '$', 'S', 'Q'), '', $strQuestionID);
		if ($strQuestionID <= 0) {
			continue;
		}
		
		//Identifica el resto de los componentes de la variable
		switch (count($arrParts)) {
			case 1:	//Sólo es la definición de la variable, así que la propiedad default es value
				$strProperty = 'value';
				break;
			case 2: //Es una propiedad o función basado en si la segunda parte contiene o no ()
				$strFunction = str_ireplace('}', '', $arrParts[1]);
				$arrParams = explode('(', $strFunction);
				switch (count($arrParams)) {
					case 1: //Es sólo una propiedad
						$strProperty = $strFunction;
						$strFunction = '';
						break;
					default: //Sólo debería haber un único paréntesis y entonces significa que es una función
						$strFunction = $arrParams[0];
						$strProperty = $strFunction;
						$strParam = str_ireplace(')', '', $arrParams[1]);
						break;
				}
				break;
			case 3: //Es una propiedad + función (caso no soportado con la actual expresión regular)
				$strProperty = str_ireplace('}', '', $arrParts[1]);
				$strFunction = str_ireplace('}', '', $arrParts[2]);
				$arrParams = explode('(', $strFunction);
				$strFunction = $arrParams[0];
				if (count($arrParams) > 1) {
					$strParam = str_ireplace(')', '', $arrParams[1]);
				}
				break;
			default: //No se soporta esta expresión, aunque no debería haber aceptado nada similar a este caso
				$blnValid = false;
				break;
		}
		
		if (!$blnValid) {
			continue;
		}
		
		$strPropertyCS = $strProperty;
		$strProperty = strtolower($strProperty);
		$strFunction = strtolower($strFunction);
		$strParam = strtolower($strParam);
		$strParam = str_ireplace("\'\"", '', $strParam);
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr] = array();
			$arrInfo['elements'][$strExpr]['type'] = otyQuestion;
			$arrInfo['elements'][$strExpr]['id'] = $strQuestionID;
			$arrInfo['elements'][$strExpr]['expr'] = $strExpr;
			$arrInfo['elements'][$strExpr]['prop'] = $strProperty;
			$arrInfo['elements'][$strExpr]['fn'] = $strFunction;
			$arrInfo['elements'][$strExpr]['param'] = $strParam;
		}
		//@JAPR
		
		//Hasta este punto ya se tienen identificados los diferentes componentes de la variable, ahora deber ejecutar procesos diferentes según tu tipo
		$anAnswer = null;
		$aValue = '';
		//@JAPR 2015-03-23: Corregido un bug, no estaba usando el parámetro $bReplaceByNumber correctamente, a partir de ahora ya será usado como
		//debía, sin embargo los casos donde se hubieran grabado mal los mapeos tendrían que ser re-mapeados
		if (!$bReplaceByNumber) {
			//Obtiene el número de pregunta basado en el ID (si ya no existe, se regresará 0 así que termina reemplazando por vacio)
			$strQuestionID = (int) @$arrQuestionsByID[$strQuestionID];
		}
		$aQuestion = @$arrQuestionObjsByNum[ $arrQuestionsByID[ $strQuestionID ] ];
		//$aQuestion = @$arrQuestionObjsByID[$strQuestionID];
		//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		$intQTypeID = qtpOpen;
		if ( !is_null($aQuestion) ) {
			$intQTypeID = $aQuestion->QTypeID;
		}
		
		//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Se removió este código de cada case porque finalmente era exactamenete el mismo en todos los casos, de esta manera ya quedará como algo general
		//Este es un array multidimensional con el valor por cada registro posible de esa pregunta si es que pertenecía a una sección
		//multi-registro (dinámica, Inline o maestro-detalle), así que se debe obtener el valor correcto según el parámetro $aSectionEvalRecord
		//Este array siempre viene indexado por el número de pregunta, así que la variable ya debe estar ajustada
		$anAnswer = @$arrFieldValues[$strQuestionID];
		//@JAPR 2015-12-04: Corregido un bug, este objeto es la colección de respuestas del archivo .dat, así que es un array indexado por QuestionID, pero cuyo contenido
		//depende del tipo de pregunta/sección, cuando es una pregunta de secciones estándar siempre trae las propiedades directas ya que no es un array, cuando es una pregunta
		//de una sección multi-registro, siempre será un array (aunque sólo tenga una posible respuesta) así que hay un nivel antes de las propiedades, sin embargo esta función
		//al recibir el número de row ($nRow) siempre toma un valor entero con el default 0, lo cual significa el primer registro, así que en el caso de que se intentara aplicar
		//sobre una pregunta de sección estándar habría generado un error al no contener un array, y visceversa, si como originalmente estaba este código nunca interpretaba que
		//la pregunta podía tener un array que agrupa, hubiera intentado pedir una propiedad al array de índices y habría generado un error, por tanto se agregó el paso extra de
		//identificar si es o no un array de respuestas basado siempre en el $nRow, asumiendo que cuando no se mande, usará la única posible respuesta que tenía, que por orden
		//lógico hubiera sido la respuesta 0 (#TI6HON)
		$objAnswer = $anAnswer;
		if (isset($anAnswer[$nRow])) {
			//En este caso si existe el índice especificado, como es un número, nunca ninguna propiedad de una respuesta pudo tener como nombre un número, así que es válida esta pregunta
			$objAnswer = $anAnswer[$nRow];
		}
		//@JAPR
		
		switch ($strProperty) {
			case 'comment':
				//$anAnswer = objAnswer.comment;
				break;
			case 'days':
				//$anAnswer = objAnswer.daysDueDate;
				break;
			case 'value':
				if (!is_null($objAnswer) && is_array($objAnswer)) {
					if (isset($objAnswer['multiAnswer']) && is_array($objAnswer['multiAnswer']) && count( $objAnswer['multiAnswer'] )) {
						$aValue = $objAnswer['multiAnswer'];
					} 
					else {
						//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						if ( $intQTypeID == qtpSketchPlus ) {
							//Para Sketch+ en el server no existe el valor como variable (solo existe en móvil donde si podría tener uso) ya que el valor recibido del móvil se transforma
							//a un path y nombre de archivo distinto, así que no sería consistente con el valor del móvil, además que cuando se usa la pregunta directamente como mapeo (por
							//ende eso sería su "valor") lo que se obtiene es la imagen con los trazos, en todo caso si algún día se implementa, eso es lo que se tendría que regresar en
							//este caso
						}
						else {
							$aValue = @$objAnswer['answer'];					
						}
						//@JAPR
					}
				}
				//@JAPR
				break;
				
			//@JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
			case 'strokes':
				if ( $intQTypeID == qtpSketchPlus ) {
					if (!is_null($objAnswer) && is_array($objAnswer)) {
						$aValue = (string) @$objAnswer['answer'];
					}
				}
				break;
			
			case 'image':
				//Para Sketch+ en el server no existe el valor como variable (solo existe en móvil donde si podría tener uso) ya que el valor recibido del móvil se transforma
				//a un path y nombre de archivo distinto, así que no sería consistente con el valor del móvil, en todo caso si algún día se implementa, esta tendría que ser la imagen
				//original utilizada para generar el Sketch, ya sea una foto o la imagen default de la pregunta
				break;
			//@JAPR
				
			case 'score':
				if (!is_null($objAnswer) && is_array($objAnswer)) {
					$aValue = (string) @$objAnswer['score'];
				}
				//@JAPR
				break;
				
			case 'attr':
			//@JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
			//OMMC 2016-03-01: Agregada la propiedad de attrDID
			case 'attrid':
			case 'attrdid':
				$blnLookForID = false;
				if ($strProperty == 'attrid' || $strProperty == 'attrdid') {
					$blnLookForID = true;
				}
				//@JAPR
				
				if (!is_null($objAnswer) && is_array($objAnswer)) {
					if (!is_null($aQuestion) && !is_null($aRepository) && $aQuestion->QTypeID == qtpSingle && $aQuestion->CatalogID > 0) {
						$anAnswerCatalogFilter = [];
						//Esta validación ya no aplica, porque ahora $objAnswer ya es un elemento del array de respuestas, así que no debe volver a preguntar por $nRow
						if (isset($objAnswer['catalogFilter']) && is_array($objAnswer['catalogFilter'])) {
							$anAnswerCatalogFilter = $objAnswer['catalogFilter'];
						}
						/*if( $nRow )
						{
							if( @$anAnswer[ $nRow ] && @$anAnswer[ $nRow ]['catalogFilter'] && is_array( $anAnswer[ $nRow ]['catalogFilter'] )  )
								$anAnswerCatalogFilter = $anAnswer[ $nRow ]['catalogFilter'];
						} else if ( is_array( $anAnswer['catalogFilter'] ) )
						{
							$anAnswerCatalogFilter = $anAnswer['catalogFilter'];
						}*/					
						
						$intNumAttrs = count( $anAnswerCatalogFilter );
						//@JAPR 2016-01-15: Corregido un bug, la variable $intAttributeNum no estaba inicializada
						$intAttributeNum = -1;
						if ((int) $strParam > 0) {
							//Se asigna un número de atributo directo
							$intAttributeNum = (int) $strParam;
							//@JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
							$blnValidAttribute = true;
							if ($blnLookForID) {
								//En este caso el número realmente corresponde a un ID de atributo, así que obtiene la posición antes de continuar
								//En este caso intAttributeNum representa un ID de atributo en este punto, así que se busca la referencia directa por él para obtener el número
								$blnValidAttribute = false;
								$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
								if (!is_null($objCatalogMembersColl)) {
									foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
										//OMMC 2016-03-01: Agregados los datasourcememberID
										if($strProperty == 'attrdid'){
											if (strtolower($objCatalogMember->DataSourceMemberID) == $intAttributeNum) {
												$intAttributeNum = $objCatalogMember->MemberOrder;
												//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
												$blnValidAttribute = true;
												break;
											}
										}else{
											if (strtolower($objCatalogMember->MemberID) == $intAttributeNum) {
												$intAttributeNum = $objCatalogMember->MemberOrder;
												//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
												$blnValidAttribute = true;
												break;
											}
										}
									}
								}
							}
							
							//Si no se encontró el atributo, limpia el número de atributo para que no se procese mas abajo
							if (!$blnValidAttribute) {
								$intAttributeNum = -1;
							}
							//@JAPR
						}
						else {
							$objCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aQuestion->CatalogID);
							if (!is_null($objCatalog)) {
								$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $aQuestion->CatalogID);
								if (!is_null($objCatalogMembersColl)) {
									foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
										if (strtolower($objCatalogMember->MemberName) == $strParam) {
											$intAttributeNum = $objCatalogMember->MemberOrder;
											break;
										}
									}
								}
							}
						}
						
						$strAttributePos = (int) $intAttributeNum;
						if ($strAttributePos > 0 && $strAttributePos <= $intNumAttrs)
						{
							$aValue = (string) @$anAnswerCatalogFilter[$strAttributePos -1];
						}
						else {
							//@JAPR 2016-01-15: Corregido un bug, la variable $intPosValueAttr no estaba inicializada
							$intPosValueAttr = null;
							//EVEG Buscamos dentro del array de atributos el que se esta evaluando para sacar su posicion
							foreach ($garrCatalogDefs['q'.$aQuestion->QuestionID] as $keyAttr => &$anAttr) {
								//@JAPR 2016-06-01: Corregido un bug, no estaba correctamente implementada la propiedad attrdid, seguía buscando en este punto por nombre de atributo (#MKM198)
								if ($strProperty == 'attrdid') {
									$intPosValueAttr = $strAttributePos -1;
									break;
								}
								else {
									//@JAPR 2016-11-01: Corregido un bug, estaba haciendo la comparación del nombre sin aplicar minúsculas que es como $strParam funciona
									if(strtolower($anAttr["name"]) == str_replace("'", "", $strParam)){
										$intPosValueAttr=(integer)$keyAttr;
										//@JAPR 2016-06-01: Agregado el break pues una vez encontrado no tiene caso continuar con el ciclo
										break;
									}
								}
								//@JAPR
							}
							
							//Usará el último elemento (también entra aquí si no se pide un atributo sino simplemente se quiere el valor normal)
							//EVEG se busca el atributo dentro del array de datos de catalgoo
							//@JAPR 2016-01-15: Corregido un bug, la variable $intPosValueAttr no estaba inicializada
							if (isset($intPosValueAttr) && !is_null($intPosValueAttr)) {
								$nRow=!isset($nRow)||$nRow==null?0:$nRow;
								$anAnswerCatalogFilter = @$garrCatalogData['q'.$aQuestion->QuestionID][$nRow];
								$aValue = (string) @$anAnswerCatalogFilter[$intPosValueAttr];
							}
						}
					}
				}
				break;
		}

		if( is_array( $aValue ) )
		{
			$strAnswer = implode(',', $aValue);
				
		} else 
		{
			$strAnswer = $aValue;
		}
		
		
		//$strAnswer = (string) @$arrFieldValues['qfield'.$strQuestionID];
		if (!is_numeric($strAnswer) && $bQuoteText)
		{
			$strAnswer = "'".$strAnswer."'";
		}
		
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr]['value'] = $strAnswer;
		}
		//@JAPR
		
		$strText = str_ireplace($strExpr, $strAnswer, $strText);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceQuestionVariablesAdvV6 result: {$strText}");
	}
	return $strText;
}

//@JAPR 2015-10-14: Agregadas variables para las secciones en v6 (#TI6HON)
/* Reemplaza las variables de los textos de acciones por el correspondiente valor de la sección a la que se hace referencia. Es equivalente a la
función replaceQuestionVariables en cuanto al funcionamiento de los parámetros, y de hecho es invocada antes de ejecutar dicha función
//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
*/
function replaceSectionVariablesAdvV6($sText, $arrFieldValues = null, $bReplaceByNumber = true, $aRepository = null, $aSurveyID = 0, $bQuoteText = true, $bForceGetAttribs = false, $bReturnExtendedInfo = false, &$arrInfo = array(), $nRow = 0)
{
	global $arrSectionDescByID;
	global $strNewDataSep;
	global $gblEFormsNA;
	//Array global con las definiciones de catálogo recibidas, si no está asignado el catálogo de esta pregunta, deberá generarlo en este instante para continuar el grabado, usando la
	//definición actual del catálogo, lo cual sólo sirve en caso de capturas recientes a la fecha de definición, pero no servirá para ediciones futuras necesariamente
	global $garrCatalogDefs;
	if (!isset($garrCatalogDefs)) {
		$garrCatalogDefs = array();
	}
	//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App, este array contiene la lista completa de valores en el mismo orden de atributos definidos en garrCatalogDefs, la
	//manera de indexar es "q"+QuestionID (o "s"+SectionID), luego el índice de registro (o 0 si es estándar), y finalmente contiene el array de atributos en el mismo orden de garrCatalogDefs
	global $garrCatalogData;
	if (!isset($garrCatalogData)) {
		$garrCatalogData = array();
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceSectionVariablesAdvV6 expression: {$sText}");
	}
	
	$intOffSet = 0;
	$intPos = stripos($sText, '{$S', $intOffSet);
	if ($intPos === false)
	{
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n replaceSectionVariablesAdvV6 no variables found");
		}
		return $sText;
	}
	
	//@JAPR 2012-10-30: Agregado el parámetro $bReplaceByNumber para controlar si se debe reemplazar la respuesta basado en el número de pregunta
	//o en el ID dentro del texto especificado
	$arrSectionsByID = array();
	$arrSectionObjsByNum = array();
	//@JAPR 2014-01-24: Agregada la opción para enviar los reportes a cuentas de correo adicionales
	//Ahora siempre se debe cargar la colección de secciones
	if (!is_null($aRepository)) {
		$aSectionsColl = BITAMSectionCollection::NewInstance($aRepository, $aSurveyID);
		if (!is_null($aSectionsColl)) {
			foreach ($aSectionsColl->Collection as $aSection) {
				$arrSectionsByID[$aSection->SectionID] = $aSection->SectionNumber;
				$arrSectionObjsByNum[$aSection->SectionNumber] = $aSection;
			}
		}
	}
	//@JAPR
	
	$strText = $sText;
	$pattern = '/[\{]{1}[$]{1}[S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/';
	preg_match_all($pattern, $strText, $arrayEvalSectionsAdv);
	$arraySectionsAdv = array();
	if (count($arrayEvalSectionsAdv[0]) > 0) {
		$arraySectionsAdv = array_unique($arrayEvalSectionsAdv[0]);
	}
	
	foreach ($arraySectionsAdv as $strExpr) {
		//Primero intentará separar por el total de propiedades/funciones especificadas, esto es las cadenas separadas por '.'. Siempre la última es la
		//única que podría ser una función así que se valida si está o no definda así como su parámetro en caso de haber alguno
		//Sólo puede haber una propiedad, una función, o una propiedad + una función, todo aplicando a una misma pregunta y/o sección
		$strProperty = '';
		$strFunction = '';
		$strParam = '';
		$arrParts = explode('.', $strExpr);
		$strSectionID = $arrParts[0];
		$blnValid = true;
		
		$strSectionID = (int) @str_ireplace(array('{', '}', '$', 'S', 'Q'), '', $strSectionID);
		if ($strSectionID <= 0) {
			continue;
		}
		
		//Identifica el resto de los componentes de la variable
		switch (count($arrParts)) {
			case 1:	//Sólo es la definición de la variable, así que la propiedad default es name
				$strProperty = 'name';
				break;
			case 2: //Es una propiedad o función basado en si la segunda parte contiene o no ()
				$strFunction = str_ireplace('}', '', $arrParts[1]);
				$arrParams = explode('(', $strFunction);
				switch (count($arrParams)) {
					case 1: //Es sólo una propiedad
						$strProperty = $strFunction;
						$strFunction = '';
						break;
					default: //Sólo debería haber un único paréntesis y entonces significa que es una función
						$strFunction = $arrParams[0];
						$strProperty = $strFunction;
						$strParam = str_ireplace(')', '', $arrParams[1]);
						break;
				}
				break;
			case 3: //Es una propiedad + función (caso no soportado con la actual expresión regular)
				$strProperty = str_ireplace('}', '', $arrParts[1]);
				$strFunction = str_ireplace('}', '', $arrParts[2]);
				$arrParams = explode('(', $strFunction);
				$strFunction = $arrParams[0];
				if (count($arrParams) > 1) {
					$strParam = str_ireplace(')', '', $arrParams[1]);
				}
				break;
			default: //No se soporta esta expresión, aunque no debería haber aceptado nada similar a este caso
				$blnValid = false;
				break;
		}
		
		if (!$blnValid) {
			continue;
		}
		
		$strPropertyCS = $strProperty;
		$strProperty = strtolower($strProperty);
		$strFunction = strtolower($strFunction);
		$strParam = strtolower($strParam);
		$strParam = str_ireplace("\'\"", '', $strParam);
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr] = array();
			$arrInfo['elements'][$strExpr]['type'] = otySection;
			$arrInfo['elements'][$strExpr]['id'] = $strSectionID;
			$arrInfo['elements'][$strExpr]['expr'] = $strExpr;
			$arrInfo['elements'][$strExpr]['prop'] = $strProperty;
			$arrInfo['elements'][$strExpr]['fn'] = $strFunction;
			$arrInfo['elements'][$strExpr]['param'] = $strParam;
		}
		//@JAPR
		
		//Hasta este punto ya se tienen identificados los diferentes componentes de la variable, ahora debe ejecutar procesos diferentes según tu tipo
		$aValue = '';
		//@JAPR 2015-03-23: Corregido un bug, no estaba usando el parámetro $bReplaceByNumber correctamente, a partir de ahora ya será usado como
		//debía, sin embargo los casos donde se hubieran grabado mal los mapeos tendrían que ser re-mapeados
		if (!$bReplaceByNumber) {
			//Obtiene el número de sección basado en el ID (si ya no existe, se regresará 0 así que termina reemplazando por vacio)
			$strSectionID = (int) @$arrSectionsByID[$strSectionID];
		}
		$objSection = @$arrSectionObjsByNum[ $arrSectionsByID[ $strSectionID ] ];
		//@JAPR
		
		if (!is_null($objSection)) {
			switch ($strProperty) {
				case 'name':
					$aValue = $objSection->SurveyName;
					break;
				case 'number':
					$aValue = $objSection->SectionNumber;
					break;
				case 'pagename':
					//Este valor sólo se puede intentar utilizar si se trata de una sección Inline que utiliza catálogo, sin embargo en este caso es fácil
					//validar ignorando el error y regresando un valor vacio, ya que si no es de ese tipo no vendría un dato en este array
					$arrSectionDesc = @$arrSectionDescByID[$objSection->SectionID][$nRow];
					if (is_array($arrSectionDesc) && count($arrSectionDesc) > 0) {
						$aValue = (string) @$arrSectionDesc[count($arrSectionDesc) -1];
					}
					break;
				case 'pagenumber':
					//El número de página realmente es el número de registro procesado, así que se regresa el mismo valor recibido en dicho parámetro
					$aValue = $nRow;
					break;
				case 'score':
					//En el caso de las secciones, actualmente el App no envía el Score de cada una, así que este valor no se puede obtener desde las respuestas recibidas, se tendría que
					//calcular en el server e base al array de respuestas simulando lo mismo que haría el App
					//$aValue = (string) @$anAnswer['score'];
					break;
				case 'attr':
				//@JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
				//OMMC 2016-03-02: Agregada la propiedad attrDID
				case 'attrid':
				//@JAPR 2018-11-30: Corregido un bug, aunque estaba programado el soporte, nunca entraba porque faltaba este valor en el switch (#9TMPFM)
				case 'attrdid':
					$blnLookForID = false;
					if ($strProperty == 'attrid' || $strProperty == 'attrdid') {
						$blnLookForID = true;
					}
					//@JAPR
					
					//Este valor sólo se puede intentar utilizar si se trata de una sección Inline que utiliza catálogo, así que se valida que se cumpla la
					//condición para no intentar procesar un ciclo sobre arrays no asignados, se regresará vacío si es una variable de sección inválida
					if ($objSection->SectionType == sectInline && $objSection->ValuesSourceType == tofCatalog) {				
						//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
						$arrDataSourceDefColl = @$garrCatalogDefs["s".$objSection->SectionID];
						if (is_array($arrDataSourceDefColl)) {
							//@JAPR 2015-08-24: Enviada la definición del catálogo desde el App
							$blnValueSet = true;
							$arrAllValues = array();
							$arrAllValues = @$garrCatalogData['s'.$objSection->SectionID][$nRow];
							if (is_null($arrAllValues)) {
								$arrAllValues = array();
							}
							
							//Esta variable puede recibir el número de atributo (si es numérico) o el nombre, en cualquier caso identifica el número de atributo
							//para obtener el valor en dicha posición
							$intAttributeNum = -1;
							if ((int) $strParam > 0) {
								//@JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
								if ($blnLookForID) {
									//En este caso el número realmente corresponde a un ID de atributo, así que obtiene la posición antes de continuar
									//En este caso intAttributeNum representa un ID de atributo en este punto, así que se busca la referencia directa por él para obtener el número
									$intAttributeNum = ((int) $strParam);
									$blnValidAttribute = false;
									$objCatalogMembersColl = @BITAMCatalogMemberCollection::NewInstance($aRepository, $objSection->CatalogID);
									if (!is_null($objCatalogMembersColl)) {
										foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
											//OMMC 2016-03-01: Agregados los datasourcememberID
											if($strProperty == 'attrdid'){
												if (strtolower($objCatalogMember->DataSourceMemberID) == $intAttributeNum) {
													//@JAPR 2016-04-28: Corregido un bug, esta asignación debe ser con el número de atributo, no con el ID, adicionalmente en este punto
													//se debe restar 1 al número de atributo, porque donde se usará se busca directamente como índice, así que se debe cuidar si se copia
													//este código a la función del archivo replaceQuestionVariables de processSurveyFunctions o visceversa, porque esa otra función maneja
													//los índices desde 1 mientras esta ya lo hace desde 0
													$intAttributeNum = $objCatalogMember->MemberOrder -1;
													//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
													$blnValidAttribute = true;
													break;
												}
											}else{
												if (strtolower($objCatalogMember->MemberID) == $intAttributeNum) {
													$intAttributeNum = $objCatalogMember->MemberOrder -1;
													//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
													$blnValidAttribute = true;
													break;
												}
											}
										}
									}
									
									//Si no se encontró el atributo, limpia el número de atributo para que no se procese mas abajo
									if (!$blnValidAttribute) {
										$intAttributeNum = -1;
									}
								}
								else {
									//Se asigna un número de atributo directo, en este caso el valor definido por el usuario inicia en 1, pero en el array se inicia
									//en 0 así que decrementa la posición para hacer el match adecuado en el array
									$intAttributeNum = ((int) $strParam) -1;
								}
								//@JAPR
							}
							else {
								//EVEG Buscamos dentro del array de atributos el que se esta evaluando para sacar su posicion
								foreach ($arrDataSourceDefColl as $keyAttr => $anAttr) {
									if ($anAttr["name"] == str_replace("'", "", $strParam)) {
										$intAttributeNum = (integer) $keyAttr;
										break;
									}
								}
							}
							
							//Obtiene directamente el valor del atributo por medio de su posición en el catálogo recibido
							if (isset($arrAllValues[$intAttributeNum]) && trim((string) @$arrAllValues[$intAttributeNum]) != '') {
								$aValue = (string) @$arrAllValues[$intAttributeNum];
							}
						}
					}
					break;
			}
		}
		
		if( is_array( $aValue ) )
		{
			$strAnswer = implode(',', $aValue);
		} else 
		{
			$strAnswer = $aValue;
		}
		
		//$strAnswer = (string) @$arrFieldValues['qfield'.$strSectionID];
		if (!is_numeric($strAnswer) && $bQuoteText)
		{
			$strAnswer = "'".$strAnswer."'";
		}
		
		//@JAPR 2015-03-12: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
		//Agregado el parámetro bReturnExtendedInfo y arrInfo, ver comentario de la función replaceQuestionVariables para mas detalles
		if ($bReturnExtendedInfo) {
			$arrInfo['elements'][$strExpr]['value'] = $strAnswer;
		}
		//@JAPR
		
		$strText = str_ireplace($strExpr, $strAnswer, $strText);
	}
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceSectionVariablesAdvV6 result: {$strText}");
	}
	
	return $strText;
}

//@JAPR 2018-01-18: Agregada la configuración para el subject y body de los destinos tipo EMail (#UTOK8K)
function replaceEntryVariables($sText, $syncDataDestinationOpt = null) {
	$strText = $sText;
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\n replaceEntryVariables expression: {$sText}");
	}
	
	if ( is_null($syncDataDestinationOpt) ) {
		return $strText;
	}
	
	//Inicia el reemplazo de las posibles variables de la captura que se pueden configurar para el App en la forma selSurvey.['property'] o las de @gps('property')
	//entre otras, es decir, todas aquellas que no están directamente relacionadas con las preguntas y/o secciones, las cuales tienen sus propias funciones y si eran
	//compatibles con el reemplazo de variables en el servidor hasta antes de esta nueva función
	try {
		$arrAppVariables = array(); $arrAppVariableValues = array();
		$arrAppVariables[] = '{selSurvey.[\'id\']}'; $arrAppVariableValues[] = (int) @$syncDataDestinationOpt['surveyID'];
		if ( isset($syncDataDestinationOpt['surveyInstance']) && is_object($syncDataDestinationOpt['surveyInstance']) ) {
			$arrAppVariables[] = '{selSurvey.[\'name\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['surveyInstance']->SurveyName;
		}
		$arrAppVariables[] = '{selSurvey.[\'surveyDate\']}'; $arrAppVariableValues[] = substr((string) @$syncDataDestinationOpt['arrayData']['surveyDate'], 0, 10);
		$arrAppVariables[] = '{selSurvey.[\'surveyHour\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrayData']['surveyHour'];
		if ( isset($syncDataDestinationOpt['theAppUser']) && is_object($syncDataDestinationOpt['theAppUser']) ) {
			$arrAppVariables[] = '{objSettings.[\'user\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['theAppUser']->UserName;
			$arrAppVariables[] = '{objSettings.[\'userLogin\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['theAppUser']->ArtusUser;
		}
		$arrAppVariables[] = '{objSettings.[\'project\']}'; $arrAppVariableValues[] = (string) @$_SESSION["PABITAM_RepositoryName"];
		$arrAppVariables[] = '{objSettings.[\'device\'].[\'uuid\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrJSONData']['device']['uuid'];
		//NO soportada para envío de EMail por seguridad
		//$arrAppVariables[] = '{objSettings.[\'passwordEnc\']}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrJSONData']['device']['uuid'];
		$arrAppVariables[] = '{$gps.accuracy}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrJSONData']['accuracy'];
		$arrAppVariables[] = '{$gps.latitude}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrJSONData']['latitude'];
		$arrAppVariables[] = '{$gps.longitude}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrJSONData']['longitude'];
		$arrAppVariables[] = '{$gps.country}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrGPSData']['Country'];
		$arrAppVariables[] = '{$gps.state}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrGPSData']['State'];
		$arrAppVariables[] = '{$gps.city}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrGPSData']['City'];
		$arrAppVariables[] = '{$gps.zipcode}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrGPSData']['ZipCode'];
		$arrAppVariables[] = '{$gps.address}'; $arrAppVariableValues[] = (string) @$syncDataDestinationOpt['arrGPSData']['Address'];
		$arrAppVariables[] = '{$Score}'; $arrAppVariableValues[] = (int) @$_POST['SurveyScore'];
	} catch (Exception $e) {
		//die("<br>\r\nException replacing variables: ".$e->getMessage());
	}
	
	//Realiza el reemplazo de los arrays de variables / valores sobre el texto proporcionado
	$strText = str_replace($arrAppVariables, $arrAppVariableValues, $strText);
	
	if (getParamValue('DebugBreak', 'both', '(int)', true)) {
		echo("<br>\r\nreplaceEntryVariables result ".$sText);
	}
	
	return $strText;
}
//@JAPR
?>