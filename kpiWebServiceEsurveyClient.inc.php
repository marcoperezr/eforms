<?php

class getKpiWebServiceClient
{
	private $client;
	public $errorMsg;

	function __construct()
	{
	
		ini_set('soap.wsdl_cache_enabled', '0');

		$serverUrl = 'http://kpionline5.bitam.com/fbm/kpiWebService.php?wsdl'; //cambiar aquí el nombre del servidor 
//		$serverUrl = 'http://kpionline5.bitam.com/fbm/kpiWebServiceTempo/kpiWebService.php?wsdl'; //cambiar aquí el nombre del servidor 
		$wsdl_shutdown_error = '';

		function ob_callback($buffer)
		{
			$buffer = "kpiWebService.Error: Failed to connect to kpiWebService!";
			return $buffer;
		}
		ob_start('ob_callback');
		$this->client = new SoapClient($serverUrl, array('exceptions' => 0, 'encoding' => 'ISO-8859-1', 'cache_wsdl' => 0));
		ob_end_clean();
		
	}

	public function registerUser($arrUserData)
	{
		$result = $this->client->registerGuest($arrUserData);
		if (is_soap_fault($result))
		{
			printf('kpiWebService.Error: SOAP Fault: (faultcode: %s, faultstring: %s)', $result->faultcode, $result->faultstring);
			exit;
		}
	}

	public function getUsers($adminEmail, $productID)
	{
		$objParams = (object) array('adminEmail' => $adminEmail, 'productIDs' => json_encode(array($productID)));
		$res = $this->client->getAllowedUsers($objParams);
		if (strpos($res, 'ERROR:') !== false)
			return str_replace('ERROR: ', '', $res);
		$arrResponse = json_decode($res, true);
		if (!is_array($arrResponse))
			return 'Bad response';
		return $arrResponse;
	}

	public function suscribeUsers($adminEmail, $productID, $arrUsers)
	{
		$objParams = (object) array('adminEmail' => $adminEmail, 'newRoles' => json_encode(array($productID => $arrUsers)));
		$res = $this->client->addRmvUserRole($objParams);
		if (strpos($res, 'ERROR:') !== false)
			return str_replace('ERROR: ', '', $res);
		return 'kpiWebService.Success:';
	}
}



?>