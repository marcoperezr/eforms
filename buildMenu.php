<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

$numOptions = 16;
/*
//Arreglo que contiene los nombres de los menus utilizados en Ektos
$arrayEktosMenusNames = array();
$arrayEktosMenusNames[0]="projects";
$arrayEktosMenusNames[1]="administration";
*/

include("loadMenuSettings.php");

$arrayEktosMenusNames = array
(
	0=>array
	(
		"name" => "projects",
		"isDisplay" => $arrayMenusDesc["PRJ"]
	),
	
	1=>array
	(
		"name" => "timesheets",
		"isDisplay" => true
	),
	
	2=>array
	(
		"name" => "administration",
		"isDisplay" => $arrayMenusDesc["ADM"]
	)
);

$arrayEktosMenusTabs = array
(
	"projects"=>array
	(
		"label" => translate("Projects"),
		"onclick" => "javascript:changeTab(this, '');",
		"numOptions" => $numOptions,
		"firstDisplay" => true
	),

	"timesheets"=>array
	(
		"label" => translate("Timesheets"),
		"onclick" => "javascript:changeTab(this, '');",
		"numOptions" => $numOptions,
		"firstDisplay" => false
	),
	
	"administration"=>array
	(
		"label" => translate("Administration"),
		"onclick" => "javascript:changeTab(this, '');",
		"numOptions" => $numOptions,
		"firstDisplay" => false
	)
);

//Arreglo que contiene informacion de las opciones de los menus
$arrayEktosMenusOptions = array
(
	"projects"=>array
	(
		0=>array
		(
			"href" => "main.php?BITAM_SECTION=ProjectCollection",
			"label" => translate("Projects Definitions"),
			"isDisplay" => $arrayMenuOptionsDesc["PRJ_PROJECTDEFINITIONS"],
			"target" => "body"
		)
		
	),
	
	"timesheets"=>array
	(
		0=>array
		(
			"href" => "main.php?BITAM_PAGE=TimeSheet&IsMyCurrentTS=1",
			"label" => translate("My TimeSheet"),
			"isDisplay" => true,
			"target" => "body"
		),
		
		1=>array
		(
			"href" => "main.php?BITAM_SECTION=TimesheetCollection&IsMyCollectionTS=1",
			"label" => translate("Pending Timesheets"),
			"isDisplay" => true,
			"target" => "body"
		),
		
		2=>array
		(
			"href" => "main.php?BITAM_SECTION=TimesheetCollection",
			"label" => translate("Timesheets List"),
			"isDisplay" => true,
			"target" => "body"
		),		
	),
	
	"administration"=>array
	(
		0=>array
		(
			"href" => "main.php?BITAM_SECTION=ResourceCollection",
			"label" => translate("Resources"),
			"isDisplay" => $arrayMenuOptionsDesc["ADM_RESOURCES"],
			"target" => "body"
		),
		1=>array
		(
			"href" => "main.php?BITAM_SECTION=NonProjectTaskCollection",
			"label" => translate("Non Project Tasks"),
			"isDisplay" => true,
			"target" => "body"
		)
	)
);
?>