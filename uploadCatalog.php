<?
define("FBM_PATH", "e:\\inetpub\\wwwroot\\fbm");
define("FBM_ROOT_PATH", "/fbm");

include ("../error.inc.php");

//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("dimension.inc.php");
global $gblEFormsNA;

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

// session stuff
if (!array_key_exists("saasuser", $_SESSION) || !array_key_exists("dbid", $_SESSION))
{
	//header("Location: "."/".FBM_PATH."/session_lost.html");
	$url = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https://' : 'http://').$_SERVER['HTTP_HOST']."/".FBM_ROOT_PATH."/session_lost.html";
	header("Location: ".$url);
	exit;
}

// get admin user id
$intAdminUserID = $_SESSION["saasuser"];

// set a timestamp for file names
$strTimeStamp = date("YmdHis");

// set global log file name
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strLogFile = GeteFormsLogPath()."tmpuid".$intAdminUserID."_".$strTimeStamp.".txt";
//@JAPR
// create connection
include(FBM_PATH."\\".'conns.inc.php');
////global $server, $server_user, $server_pwd, $masterdbname;
$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
if (!$mysql_Connection) 
{
	reportErrorToClient("Could not establish connection to the admin database");
	exit;
}
$db_selected = mysql_select_db($masterdbname, $mysql_Connection);
if (!$db_selected) 
{
	reportErrorToClient("Could not select the admin database");
	exit;
}

require_once(FBM_PATH."\\".'ESurvey.class.php');

// instantiate the host user
$hostUser = SAASUser::getSAASUserWithID($intAdminUserID, $mysql_Connection);
if (is_null($hostUser))
{
	reportErrorToClient("Could not get host info");
	exit;
}
/*
// check his main project
if (is_null($hostUser->MainProject))
{
reportErrorToClient("Could not get host main project info");
exit;
}


// verify that host user main repository is equal to the one on the PABITAM_RepositoryName session var
if ($hostUser->MainProject["DatabaseBMD"] !== $_SESSION["PABITAM_RepositoryName"])  
{
header("Location: "."/".FBM_PATH."/session_lost.html"); //the session vars have changed; this is just validation
exit;
}
*/
$blnUploadedSuccessfully = (array_key_exists("xlsfile", $_FILES) && $_FILES['xlsfile']['error'] === UPLOAD_ERR_OK);

if (!$blnUploadedSuccessfully)
{
	reportErrorToClient("Upload error code: ".$_FILES['xlsfile']['error']);
	exit;
}

$type = strtolower(substr($_FILES['xlsfile']['name'], -4));
$blnExcel5 = ($type == ".xls");
$blnExcel2007 = ($type == "xlsx");
if (!($blnExcel5 || $blnExcel2007))
{
	reportErrorToClient("Invalid xls file");
	exit;
}

//verificamos que el nombre del archivo tenga la estructura adecuada.
//@JAPR 2012-06-20: Corregido un bug, el nombre de archivo por fuerza debía seguir cierta sintaxis para poderse importar
$intCatalogKey = null;
if (isset($_POST['catalogid']) && (int) $_POST['catalogid'] > 0)
{
	$intCatalogKey = (int) $_POST['catalogid'];
}
elseif (isset($_GET['catalogid']) && (int) $_GET['catalogid'] > 0)
{
	$intCatalogKey = (int) $_GET['catalogid'];
}

if (is_null($intCatalogKey))
{
	$basename = basename($_FILES['xlsfile']['name']);
	$filename = pathinfo ($_FILES['xlsfile']['name'], PATHINFO_FILENAME);
	$arrFilenameTMP = explode('_', $filename);
	$arrFilename[] = @$arrFilenameTMP[0];
	$arrFilename[] = @$arrFilenameTMP[1];
	$arrFilename[] = @$arrFilenameTMP[2];
	
	$intCatalogKey = (int) $arrFilename[1];

	if(strtolower($arrFilename[0]) != "catalog")
	{
		reportErrorToClient("Invalid File. A catalog for this file has not been found : '{$filename}'.");
		exit;
	}
}
//@JAPR
	
/*
Catalog_1165_Empresa (2).xls
*/

//Si el tipo de carga es nueva, entonces se hace truncate a la tabla del catalogo
//y con ello el $typeLoading = 0, pero si viene el parametro appendFile en el GET
//entonces el valor de $typeLoading = 1 y con ello no se hara truncate solo se 
//agregaran mas valores
$typeLoading = 0;
//Obtenemos el tipo de carga, verificamos si viene el parametro appendFile en el GET
if(isset($_GET["appendFile"]))
{
	$typeLoading = (int)$_GET["appendFile"];
}

//obtenemos la demas informacion
$aSQL = "select default_key, nom_tabla, nom_fisico FROM SI_DESCRIP_ENC WHERE CLA_DESCRIP = " . $intCatalogKey;
$rs = $theRepository->ADOConnection->Execute($aSQL);
$defaultKey = $gblEFormsNA;
$nomTabla = '';
$nomFisico = '';
if($rs)
{
  	//@JAPR 2012-06-20: Corregido un bug, el nombre de archivo por fuerza debía seguir cierta sintaxis para poderse importar
  	//Se agregó esta validación porque en caso de no encontrar el catálogo indicado, hubiera generado un error sin descripción ya que un EOF
  	//no genera texto de error en ADODB entrando por este Else
	if ($rs->EOF)
	{
		reportErrorToClient("Invalid File. A catalog for this file has not been found : '{$filename} (CatalogID #{$intCatalogKey})'.");
		exit;
	}
	//@JAPR
	
	$defaultKey = $rs->fields['default_key'];
	$nomTabla = $rs->fields['nom_tabla'];
	$nomFisico = $rs->fields['nom_fisico'];
	$rs->Close();
}
else 
{
	reportErrorToClient($theRepository->ADOConnection->ErrorNo() . ": " . $theRepository->ADOConnection->ErrorMsg());
	exit();
}

$isEmpty = true;
//@JAPR 2012-04-25: Corregido un bug, a partir de cierta versión de Model Manager se modificó el índice de la columna de la dimensión
//base para ser UNIQUE, por lo que insertar repetido el valor de *NA ya no funcionaba, así que ahora se insertará concatenando
//un consecutivo único bajo el entendido de que al terminar el proceso normal se debería reemplazar con el KEY asignado así queno
//deberían darse situaciones en las que se repirta este valor
//@JAPR 2012-05-02: Corregido un bug, se había dejado una referencia a $this->repository
$aSQL = "select count(*) as total, ".$theRepository->DataADOConnection->IfNull("MAX({$nomTabla}KEY)", "0")." + 1 AS lastkeyval from $nomTabla";
$rs = $theRepository->DataADOConnection->Execute($aSQL);
$nextIdx = (int) @$rs->fields['lastkeyval'];
//@JAPR
$totalRegisters = (int)$rs->fields['total'];
$isEmpty = ($rs->fields['total'] > 1) ? false : true;
$rs->close();

/*
if(!$isEmpty)
{
	//verificamos que el catalogo no este ligado a ningun modelo
	$aSQL = "select count(*) as total from SI_CPTO_LLAVE  t1, SI_CONCEPTO t2 where t2.cla_concepto = t1.cla_concepto and t2.nom_tabla <> '{$nomTabla}' and t1.CLA_DESCRIP = " . $intCatalogKey;
	$rs = $theRepository->ADOConnection->Execute($aSQL);
	$isValidCatalog = ($rs->fields['total'] > 0) ? false : true;
	$rs->close();
	if(!$isValidCatalog)
	{		
		reportErrorToClient("Invalid catalog:{ key:{$intCatalogKey} }");
		exit();
	}
}
*/
//obtenemos los nombres de la columna de excel vs los fisicos de la tabla
//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
$aSQL = "SELECT CLA_DESCRIP, NOM_LOGICO, NOM_FISICO 
	FROM SI_DESCRIP_ENC 
	WHERE CLA_DESCRIP IN (
		SELECT CLA_DESCRIP 
		FROM SI_DESCRIP_DET 
		WHERE CLA_PADRE = {$intCatalogKey})";
$rs = $theRepository->ADOConnection->Execute($aSQL);
$arrExcelColumns = array();
$arrExcelColumnsFile = array();
//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
$arrExcelColumnsDescripID = array();	//Contiene los CLA_DESCRIP correspondientes con cada dimensión para poder hacer match con los atributos
$strErrors = '';
//@JAPR
if($rs)
{
	while(!$rs->EOF)
	{
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		$intClaDescrip = (int) @$rs->fields['cla_descrip'];
		$nom_logico = (string) @$rs->fields['nom_logico'];
		$nom_fisico = (string) @$rs->fields['nom_fisico'];
		$nom_logico = strtolower($nom_logico);
		$nom_logico = str_replace(' ', '', $nom_logico);
		$arrExcelColumns[$nom_logico] = $nom_fisico;
		//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
		$arrExcelColumnsDescripID[$nom_logico] = $intClaDescrip;
		$arrExcelColumnsFile[] = $rs->fields['nom_logico'];
		$rs->MoveNext();
	}
	$rs->close();
}
else 
{		
	reportErrorToClient($theRepository->ADOConnection->ErrorNo() . ": " . $theRepository->ADOConnection->ErrorMsg());
	exit();
}

//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
$arrExcelColumnsDescripIDFlipped = array_flip($arrExcelColumnsDescripID);
//@JAPR

// move xls file
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
$strXLSFileName = GeteFormsLogPath()."tmpuid_".$intAdminUserID."_".$strTimeStamp."_".$_FILES['xlsfile']['name'];
//@JAPR
$strXLSFileTempName = $_FILES['xlsfile']['tmp_name'];
move_uploaded_file($strXLSFileTempName, $strXLSFileName);

// load xls file
include_once('excel.class.php');
$excel = new Excel($strXLSFileName);
$excel->open();

//obtenemos las hojas del excel
$error = '';
$sheets = $excel->getSheets($error);

if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}
	
$sheet = $sheets[0];

$error = '';
$excelColumns = $excel->getColumns($sheet, $error);
if($error)
{
	reportErrorToClient('There was an error while trying to obtain the excel columns. We recommend to donwload the catalog template and fill it with the information you are tying to upload, and try again.');
	exit();
	
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}
for($i=0; $i<count($excelColumns); $i++)
{
	$excelColumns[$i] = str_replace(' ', '', strtolower($excelColumns[$i]));
}

//validamos que todas las columnas esten en el excel
$excelColumns = array_flip($excelColumns);

foreach ($arrExcelColumns as $key => $value)
{
	if(!isset($excelColumns[$key]))
	{
		reportErrorToClient("Invalid excel columns: [{$key}]");
		exit();
	}
}

$excelColumns = array_flip($excelColumns);

//$aQuery = "SELECT ". implode(", ", $excelColumns) ." FROM [%s]";
$aQuery = "SELECT [". implode("], [", $arrExcelColumnsFile) ."] FROM [%s]";

$error = "";
$rs = $excel->execute($aQuery, $sheet, $error);
if($error)
{
	if(is_object($error))
		reportErrorToClient($error->getMessage());
	else 
		reportErrorToClient($error);
	exit();
}

if(!$rs)
{
	reportErrorToClient("Invalid query". ": {$aQuery}");
	exit();		
}

//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
$aDimension = BITAMDimension::NewInstanceWithDescripID($theRepository, $intCatalogKey, -1, false);
$intVersionNum = $aDimension->getCatalogVersion();
$intVersionNum = ($intVersionNum >= 0)?$intVersionNum+1:null;
$strVersionFieldName = $aDimension->VersionFieldName;

if (!is_null($intVersionNum))
{
	$strVersionFieldName = ', '.$strVersionFieldName;
	$intVersionNum = ", ".$intVersionNum;
}
else 
{
	$intVersionNum = '';
}
//@JAPR

//Si la carga es nueva entonces se hace truncate a la tabla y se borran los elementos
//GCRUZ 2013-03-27. || true a la condición para que SIEMPRE entre por aquí... ya no hay distinción entre si es "upload" o "append"
//GCRUZ 2013-05-27. Agregada condicion de catálogo formato de $intDissociateCatDimens para usar nuevo método o anterior

//GCRUZ 2019-03-26. Validaciones iniciales antes de empezar a cargar
if(!file_exists(getcwd().'/logETL/'))
	mkdir(getcwd().'/logETL/') or die('Error al crear la carpeta de usuario');
$logFileETLUpdate = getcwd().'/logETL/'.date('YmdHis').'_'.$_SESSION["saasuser"].'_'.'ETLCatalogUpdateLog.html';
writeLogDetail('Validaciones iniciales antes de empezar a cargar', $logFileETLUpdate);

//Obtenemos el CatalogID a partir del cla_descrip que se recibio
$sql = "SELECT CatalogID FROM SI_SV_Catalog WHERE ParentID = ".$intCatalogKey;
$aRS = $theRepository->DataADOConnection->Execute($sql);

if($aRS!==false && !$aRS->EOF)
{
	$svCatalogID = (int)$aRS->fields["catalogid"];
}

$intDissociateCatDimens = 0;
$sql = "SELECT DissociateCatDimens FROM SI_SV_Catalog WHERE CatalogID = ".$svCatalogID;
writeLogDetail($sql, $logFileETLUpdate);
$rsETLUpdate = $theRepository->DataADOConnection->Execute($sql);
if ($rsETLUpdate && $rsETLUpdate->_numOfRows != 0)
{
	$DissociateCatDimens = intval($rsETLUpdate->fields['DissociateCatDimens']);
	writeLogDetail('DissociateCatDimens='.$DissociateCatDimens, $logFileETLUpdate);
	if ($DissociateCatDimens == 0) {
		$intDissociateCatDimens = 0;
	}
	else {
		$intDissociateCatDimens = 1;
		writeLogDetail('(getMDVersion()='.getMDVersion().') >= (esvCatalogDissociation='.esvCatalogDissociation.')', $logFileETLUpdate);
		if (getMDVersion() >= esvCatalogDissociation) {
			$sql = "SELECT DISTINCT A.DissociateCatDimens 
			FROM SI_SV_Survey A, SI_SV_Question B 
			WHERE A.SurveyID = B.SurveyID AND B.QTypeID = ".qtpSingle." AND B.CatalogID = ".$svCatalogID;
			writeLogDetail($sql, $logFileETLUpdate);
			$aRS = $theRepository->DataADOConnection->Execute($sql);
			if ($aRS !== false) {
				while(!$aRS->EOF) {
					$intDissociateCatDimens = (int) @$aRS->fields["dissociatecatdimens"];
					if ($intDissociateCatDimens == 0) {
						break;
					}
					$aRS->MoveNext();
				} 
			}
		}
	}
}
writeLogDetail('intDissociateCatDimens='.$intDissociateCatDimens, $logFileETLUpdate);
//GCRUZ 2013-05-27. Si $intDissociateCatDimens == 0, usar método nuevo, de lo contrario, usar método anterior.
if($intDissociateCatDimens == 0)
{
	$aSQLTruncate = "truncate table " . $nomTabla;
	//GCRUZ 2013-03-26. Comentado código de ejecución del truncate.
	//$theRepository->DataADOConnection->Execute($aSQLTruncate);
	//GCRUZ
	$unquoteddefaultKey = $defaultKey;
	$defaultKey = $theRepository->DataADOConnection->Quote($defaultKey);
	$bFirst = true;
	$numRow = 0;

	writeLogDetail('UserID='.$_SESSION["saasuser"], $logFileETLUpdate);
	$countRowsInXLS = 0;
	while (!$rs->EOF)
	{
		$countRowsInXLS++;
		$rs->MoveNext();
	}
	if ($countRowsInXLS == 0)
	{
		writeLogDetail('XLS is empty. No records found.', $logFileETLUpdate);
		reportErrorToClient("XLS is empty. No records found.");
		exit;
	}
	else
		$rs->MoveFirst();
	$rowsInCatalog = 0;
	$aSQLETLUpdate = "SELECT COUNT(*) AS 'count' FROM {$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	else
		$rowsInCatalog = intval($rsETLUpdate->fields['count']);
	if ($rowsInCatalog == 0)
		$bFirst = true;
	else
		$bFirst = false;
	
	//GCRUZ 2013-03-26. Código para actualizar catálogo sólo con los elementos nuevos.
	//Get keys from first record
	$valuesExcel =array();
	for($i=0; $i<count($excelColumns); $i++)
	{
		$sName = strtolower($rs->Fields($i)->Name);
		$sName = str_replace(' ', '', $sName);
		$valuesExcel[$arrExcelColumns[$sName]] = $theRepository->DataADOConnection->Quote($rs->Fields($i)->Value);
	}
	$arrKeys = array_keys($valuesExcel);

	//Updates para reemplazar el NULL por 'NA'
	writeLogDetail('Updates para reemplazar el NULL por NA', $logFileETLUpdate);
	foreach ($arrKeys as $aDimDesc)
	{
		$aSQLETLUpdate = "UPDATE {$nomTabla} SET {$aDimDesc} = 'NA' WHERE {$aDimDesc} IS NULL";
		writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
		$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
		if (!$rsETLUpdate)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
	}
	//Crear Tablas temporales para el proceso
	writeLogDetail('Crear Tablas temporales para el proceso', $logFileETLUpdate);
	//GCRUZ 2013-05-22. Quitar el temporary temporalmente para revisar caso de Rich
	//$aSQLETLUpdate = "CREATE TABLE {$nomTabla}_xlslayout ({$nomFisico} VARCHAR(255) {$strVersionFieldName} DOUBLE, ".implode(" VARCHAR(255),", $arrKeys)." VARCHAR(255) )";
	$aSQLETLUpdate = "CREATE TEMPORARY TABLE {$nomTabla}_xlslayout ({$nomFisico} VARCHAR(255) {$strVersionFieldName} DOUBLE, ".implode(" VARCHAR(255),", $arrKeys)." VARCHAR(255) )";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "CREATE TEMPORARY TABLE TEMP1{$nomTabla} ({$nomTabla}KEY INT(11), {$nomFisico} VARCHAR(255) {$strVersionFieldName} DOUBLE, ".implode(" VARCHAR(255),", $arrKeys)." VARCHAR(255) )";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "CREATE TEMPORARY TABLE TEMP{$nomTabla} ({$nomTabla}KEY INT(11), {$nomFisico} VARCHAR(255) {$strVersionFieldName} DOUBLE, ".implode(" VARCHAR(255),", $arrKeys)." VARCHAR(255) )";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "DELETE FROM {$nomTabla}_xlslayout";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	$aSQLETLUpdate = "DELETE FROM TEMP1{$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	$aSQLETLUpdate = "DELETE FROM TEMP{$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "CREATE INDEX idx_TEMP1{$nomTabla} ON TEMP1{$nomTabla} ({$nomTabla}KEY)";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	$aSQLETLUpdate = "CREATE INDEX idx_TEMP{$nomTabla} ON TEMP{$nomTabla} ({$nomTabla}KEY)";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$index_keys = array();
	$index_key_count = 0;
	foreach ($arrKeys as $eachKey)
	{
		$aSQLETLUpdate = "CREATE INDEX idx_TEMP1{$nomTabla}_{$eachKey} ON TEMP1{$nomTabla} ($eachKey)";
		writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
		$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
		if (!$rsETLUpdate)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
		$aSQLETLUpdate = "CREATE INDEX idx_TEMP{$nomTabla}_{$eachKey} ON TEMP{$nomTabla} ($eachKey)";
		writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
		$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
		if (!$rsETLUpdate)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
		
		//GCRUZ. Crear el índeice de la tabla de dimension, ese marcará error si ya existe.
		$aSQLETLUpdate = "CREATE INDEX idx_{$nomTabla}_{$eachKey} ON {$nomTabla} ($eachKey)";
		writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
		$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
		if (!$rsETLUpdate)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
	}
	
	//Llenar la temporal del layout de xls
	writeLogDetail('Llenar la temporal del layout de xls', $logFileETLUpdate);
	//GCRUZ 2013-03-27. Agregado elemento "dummy" para evitar errores al insertar llaves duplicadas en el catálogo para el campo descripcion de la llave
	$nomFisicoUniqueInsertKey = -1;
	while (!$rs->EOF)
	{
		$valuesExcel =array();
		for($i=0; $i<count($excelColumns); $i++)
		{
			$sName = strtolower($rs->Fields($i)->Name);
			$sName = str_replace(' ', '', $sName);
			//GCRUZ 2013-05-22. Agregado trim a los valores del xls.
			$valuesExcel[$arrExcelColumns[$sName]] = $theRepository->DataADOConnection->Quote(trim($rs->Fields($i)->Value));
		}
		
		$rs->MoveNext();
		$arrKeys = array_keys($valuesExcel);

		if($bFirst)	
		{
			$nCount = count($valuesExcel);
			$values = array();
			for($i=0; $i<$nCount; $i++)
			{
				$values[$i] = $defaultKey;
			}
			$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomTabla}KEY, {$nomFisico}, ". implode(", ", $arrKeys) .") VALUES (1, {$defaultKey}, ". implode(", ", $values) .")";
			//writeLogDetail($aSQLInsert, $logFileETLUpdate);
			$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
			if (!$rsInsert)
			{
				writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
			}
			$bFirst = false;
		}
		//else 
		//{
			//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
			//@JAPR 2012-04-25: Corregido un bug, a partir de cierta versión de Model Manager se modificó el índice de la columna de la dimensión
			//base para ser UNIQUE, por lo que insertar repetido el valor de *NA ya no funcionaba, así que ahora se insertará concatenando
			//un consecutivo único bajo el entendido de que al terminar el proceso normal se debería reemplazar con el KEY asignado así queno
			//deberían darse situaciones en las que se repirta este valor
			//$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$defaultKey}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
			//GCRUZ 2013-03-26. Comentado este instert para usar el de abajo.
			//$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$theRepository->DataADOConnection->Quote($unquoteddefaultKey.$nextIdx++)}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
			//@JAPR
			//GCRUZ 2013-03-26. Modificado INSERT para llenar el layout temporal de xls en lugar de directamente el catalogo
			$aSQLInsert = "INSERT INTO {$nomTabla}_xlslayout ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$theRepository->DataADOConnection->Quote($nomFisicoUniqueInsertKey)}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
			$numRow = $numRow + 1;
			$nomFisicoUniqueInsertKey--;
		//}
		//writeLogDetail($aSQLInsert, $logFileETLUpdate);
		$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
		if (!$rsInsert)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
	}
	
	//Cargar catalogos en las temporales
	writeLogDetail('Cargar catalogos en las temporales', $logFileETLUpdate);
	$aSQLETLUpdate = "INSERT INTO TEMP1{$nomTabla} ({$nomTabla}KEY, {$nomFisico} {$strVersionFieldName}, ".implode(",", $arrKeys).") ";
	$aSQLETLUpdate .= "SELECT DISTINCT 0, COALESCE(src.{$nomFisico}, 'NA'), COALESCE(src.".str_replace(', ', '', $strVersionFieldName).", 'NA') ";
	$desc_attrib = array();
	$count_attrib = 1;
	foreach ($arrKeys as $aKeyDesc)
	{
		$desc_attrib[] = 'attrib'.$count_attrib;
        $aSQLETLUpdate .= ", COALESCE(src.".$aKeyDesc.", 'NA') AS attrib{$count_attrib}";
        $count_attrib++;
	}
	$aSQLETLUpdate .= " FROM {$nomTabla}_xlslayout src GROUP BY ".implode(',', $desc_attrib);
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "INSERT INTO TEMP{$nomTabla} ({$nomTabla}KEY, {$nomFisico} {$strVersionFieldName}, ".implode(",", $arrKeys).") ";
	$aSQLETLUpdate .= "SELECT DISTINCT COALESCE(dim.{$nomTabla}KEY, 0), COALESCE(src.{$nomFisico}, 'NA'), COALESCE(src.".str_replace(', ', '', $strVersionFieldName).", 'NA') ";
	foreach ($arrKeys as $aKeyDesc)
	{
		$aSQLETLUpdate .= ", COALESCE(src.".$aKeyDesc.", 'NA') ";
	}
	$aSQLETLUpdate .= "FROM TEMP1{$nomTabla} src LEFT JOIN {$nomTabla} dim ON ";
	$bFirstKey = true;
	foreach ($arrKeys as $aKeyDesc)
	{
		if ($bFirstKey)
			$bFirstKey = false;
		else
			$aSQLETLUpdate .= " AND ";
		$aSQLETLUpdate .= " src.".$aKeyDesc." = dim.".$aKeyDesc." ";
	}
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "SELECT MAX({$nomTabla}KEY) AS 'max' FROM {$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	$maxKey = 0;
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	else
		$maxKey = intval($rsETLUpdate->fields['max']) + 1;
	
	writeLogDetail("MaxKey=".$maxKey, $logFileETLUpdate);
	if ($maxKey != 0)
	{
		$aSQLETLUpdate = "ALTER TABLE {$nomTabla} AUTO_INCREMENT={$maxKey}";
		writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
		$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
		if (!$rsETLUpdate)
		{
			writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
		}
	}
	
	//Cargar catalogos en la tabla de dimension
	writeLogDetail('Cargar catalogos en la tabla de dimension', $logFileETLUpdate);
	$aSQLETLUpdate = "INSERT INTO {$nomTabla} ({$nomFisico} {$strVersionFieldName}, ".implode(",", $arrKeys).") ";
	$aSQLETLUpdate .= "SELECT {$nomFisico} {$strVersionFieldName}, ".implode(",", $arrKeys)." ";
	$aSQLETLUpdate .= "FROM TEMP{$nomTabla} WHERE {$nomTabla}KEY = 0";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	//Borrar temporales y actualizar catalogo
	writeLogDetail('Borrar temporales y actualizar catalogo', $logFileETLUpdate);
	$aSQLETLUpdate = "DROP TABLE TEMP1{$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	$aSQLETLUpdate = "DROP TABLE TEMP{$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	$aSQLETLUpdate = "DROP TABLE {$nomTabla}_xlslayout";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$aSQLETLUpdate = "UPDATE {$nomTabla} SET {$nomFisico} = {$nomTabla}KEY WHERE {$nomTabla}KEY <> 1";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	
	$rowsAfterUpdate = 0;
	$aSQLETLUpdate = "SELECT COUNT(*) AS 'count' FROM {$nomTabla}";
	writeLogDetail($aSQLETLUpdate, $logFileETLUpdate);
	$rsETLUpdate = $theRepository->DataADOConnection->Execute($aSQLETLUpdate);
	if (!$rsETLUpdate)
	{
		writeLogDetail('<div style="color:red;"><b style="color=red;">Query Error: ' . $theRepository->DataADOConnection->ErrorNo() . ": " . $theRepository->DataADOConnection->ErrorMsg() . '</b></div>', $logFileETLUpdate);
	}
	else
		$rowsAfterUpdate = intval($rsETLUpdate->fields['count']);
	$newRowsUpdated = $rowsAfterUpdate - $rowsInCatalog;
	$alertResultMsg = $numRow.' records found to upload.\n '.$newRowsUpdated.' new records were updated.';
	//GCRUZ
	
	//Despues de esta carga realizamos el update de la dimension dummy o agrupadora con el valor del ID(consecutivo)
	$aSQLUpdate = "UPDATE ".$nomTabla." SET ".$nomFisico." = ".$nomTabla."KEY WHERE ".$nomTabla."KEY <> 1";
	
	$rsUpdate = $theRepository->DataADOConnection->Execute($aSQLUpdate);
}
elseif (true)
{
	$aSQLTruncate = "truncate table " . $nomTabla;
	$theRepository->DataADOConnection->Execute($aSQLTruncate);
	$unquoteddefaultKey = $defaultKey;
	$defaultKey = $theRepository->DataADOConnection->Quote($defaultKey);
	$bFirst = true;
	$numRow = 0;
	//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
	//Identifica los atributos imagen para procesar las URLs que pudieran venir como valores
	$arrImageAttributes = array();
	if (getMDVersion() >= esvCatalogAttributeImage && !$rs->EOF) {
		$aCatalog = BITAMCatalog::NewInstanceWithParentID($theRepository, $intCatalogKey);
		if (!is_null($aCatalog)) {
			$catalogID = $aCatalog->CatalogID;
			$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($theRepository, $catalogID); 
			if (!is_null($objCatalogMembersColl)) {
				foreach ($objCatalogMembersColl->Collection as $objCatalogMember) {
					if ($objCatalogMember->IsImage) {
						//Si es un atributo imagen, utiliza el CLA_DESCRIP para identificar el nombre de columna (dimensión) correspondiente
						//ya que no necesariamente las dimensiones se llaman igual que los atributos (si debería ser, pero han sucedido casos
						//donde hay errores o algún otro producto podría renombrarla)
						if (isset($arrExcelColumnsDescripIDFlipped[$objCatalogMember->ClaDescrip])) {
							//$arrImageAttributes[$objCatalogMember->MemberName] = $objCatalogMember->MemberID;
							$arrImageAttributes[$arrExcelColumnsDescripIDFlipped[$objCatalogMember->ClaDescrip]] = $objCatalogMember->MemberID;
						}
					}
				}
			}
		}
	}
	
	while (!$rs->EOF)
	{
		$valuesExcel =array();
		for ($i = 0; $i < count($excelColumns); $i++)
		{
			$sName = strtolower($rs->Fields($i)->Name);
			$sName = str_replace(' ', '', $sName);
			$excelColumnName = strtolower($arrExcelColumnsFile[$i]);
			$value = $rs->Fields($i)->Value;
			//@JAPR 2014-12-09: Agregado el soporte de imagenes en los atributos de los catálogo
			//Si el atributo está marcado como imagen, entonces se deben descargar las imagenes a las que apuntan sus valores, las cuales
			//se podrán interpretar de varias formas:
			//- Imágenes relativas a la carpeta "images" del servicio (en ese caso, no contendrán path sino sólo un nombre de archivo)
			//- Imágenes subidas por el editor HTML / TinyMCE (en ese caso la ruta sería o relativa o absoluta a customerImages del servicio)
			//- Imágenes de cualquier URL en internet (en ese caso hay que descargarla a customerImages primero)
			if (isset($arrImageAttributes[$excelColumnName])) {
				//Se obtiene el valor de URL ajustado a como finalmente se usará para sobreescribirlo, además de descargar la imagen si
				//es necesario
				$value = str_replace("\\", "/", $value);
				$arrURLData = @IdentifyAndAdjustAttributeImageToDownload($value);
				if (!is_null($arrURLData)) {
					//En este caso la URL necesita algún ajuste y posiblemente se debe descargar la imagen, según el objeto regresado
					if (is_array($arrURLData)) {
						//Se debe descargar la imagen, además de actualizar el valor a grabar
						$value = (string) @$arrURLData['path'];
						$fileName = (string) @$arrURLData['name'];
						//Antes de hacer la copia, primero crea la ruta destino
						$path = str_ireplace($fileName, '', $value);
						if (!file_exists($path)) {
							@mkdir($path, null, true);
						}
						if (!@copy($arrURLData['url'], $value)) {
							$errors = error_get_last();
							$strErrors .= "\r\n({$fileName}) ".(string) @$errors['message'];
						}
					}
					else {
						//En este caso simplemente se ajusta la URL a grabar
						$value = $arrURLData;
					}
				}
				/*
				$fileInfo = pathinfo($value);
				if (isset($fileInfo['extension'])) {
					$fileExt = strtolower($fileInfo['extension']);
					$arrImageExt = array('jpg', 'png', 'gif');
					if (in_array($fileExt, $arrImageExt)) {
						if (isset($fileInfo['basename']) && $fileInfo['basename']) {
							$fileName = $fileInfo['basename'];
							$customerImageFile = "customerimages/".$theRepositoryName.'/'.$fileName;
							if (!copy($value, $customerImageFile)) {
								echo "Error al copiar $fileName...\n";
							} else {
								$value = $customerImageFile;
							}
						}
					}
				}
				*/
			}
			//@JAPR
			$realValue = $theRepository->DataADOConnection->Quote($value);
			$valuesExcel[$arrExcelColumns[$sName]] = $realValue;
		}
		
		$rs->MoveNext();
		$arrKeys = array_keys($valuesExcel);

		if($bFirst)	
		{
			$nCount = count($valuesExcel);
			$values = array();
			for($i=0; $i<$nCount; $i++)
			{
				$values[$i] = $defaultKey;
			}
			$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomTabla}KEY, {$nomFisico}, ". implode(", ", $arrKeys) .") VALUES (1, {$defaultKey}, ". implode(", ", $values) .")";
			$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
			$bFirst = false;
		}
		//else 
		//{
			//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
			//@JAPR 2012-04-25: Corregido un bug, a partir de cierta versión de Model Manager se modificó el índice de la columna de la dimensión
			//base para ser UNIQUE, por lo que insertar repetido el valor de *NA ya no funcionaba, así que ahora se insertará concatenando
			//un consecutivo único bajo el entendido de que al terminar el proceso normal se debería reemplazar con el KEY asignado así queno
			//deberían darse situaciones en las que se repirta este valor
			//$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$defaultKey}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
			$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$theRepository->DataADOConnection->Quote($unquoteddefaultKey.$nextIdx++)}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
			//@JAPR
			$numRow = $numRow + 1;
		//}
		$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
	}
	
	//Despues de esta carga realizamos el update de la dimension dummy o agrupadora con el valor del ID(consecutivo)
	$aSQLUpdate = "UPDATE ".$nomTabla." SET ".$nomFisico." = ".$nomTabla."KEY WHERE ".$nomTabla."KEY <> 1";
	$rsUpdate = $theRepository->DataADOConnection->Execute($aSQLUpdate);
}
else 
{
	//Aqui se realiza un Append pero si el catalogo esta vacío
	//entonces lo manejaremos como una carga nueva
	//porque sino lo hacemos asi, entonces no se insertara
	//el valor de No Aplica 1
	$bFirst = false;
	if($totalRegisters==0)
	{
		$aSQLTruncate = "truncate table " . $nomTabla;
		$theRepository->DataADOConnection->Execute($aSQLTruncate);
		$bFirst = true;
	}
	
	$unquoteddefaultKey = $defaultKey;
	$defaultKey = $theRepository->DataADOConnection->Quote($defaultKey);
	$numRow = 0;
	while (!$rs->EOF)
	{
		$valuesExcel =array();
		for($i=0; $i<count($excelColumns); $i++)
		{
			$sName = strtolower($rs->Fields($i)->Name);
			$sName = str_replace(' ', '', $sName);
			$valuesExcel[$arrExcelColumns[$sName]] = $theRepository->DataADOConnection->Quote($rs->Fields($i)->Value);
		}
		
		$rs->MoveNext();
		$arrKeys = array_keys($valuesExcel);
		
		//Se verifica si esta activado primer valor de No Aplica 1 
		//y que el catalogo haya estado vacio desde el inicio
		//para proceder a insertarlo, en caso contrario entonces 
		//se insertan los registros en el orden en que vengan
		if($bFirst && $totalRegisters==0)	
		{
			$nCount = count($valuesExcel);
			$values = array();
			for($i=0; $i<$nCount; $i++)
			{
				$values[$i] = $defaultKey;
			}
			$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomTabla}KEY, {$nomFisico}, ". implode(", ", $arrKeys) .") VALUES (1, {$defaultKey}, ". implode(", ", $values) .")";
			$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
			$bFirst = false;
		}

		//@JAPR 2012-03-07: Agregado el indicador de versión por registro para descarga inteligente de catálogos
		//@JAPR 2012-04-25: Corregido un bug, a partir de cierta versión de Model Manager se modificó el índice de la columna de la dimensión
		//base para ser UNIQUE, por lo que insertar repetido el valor de *NA ya no funcionaba, así que ahora se insertará concatenando
		//un consecutivo único bajo el entendido de que al terminar el proceso normal se debería reemplazar con el KEY asignado así queno
		//deberían darse situaciones en las que se repirta este valor
		//$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$defaultKey}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
		$aSQLInsert = "INSERT INTO {$nomTabla} ({$nomFisico}, ". implode(", ", $arrKeys) .$strVersionFieldName.") VALUES ({$theRepository->DataADOConnection->Quote($unquoteddefaultKey.$nextIdx++)}, ". implode(", ", $valuesExcel) .$intVersionNum.")";
		//@JAPR
		$numRow = $numRow + 1;
		$rsInsert = $theRepository->DataADOConnection->Execute($aSQLInsert);
	}
	
	//Despues de esta carga realizamos el update de la dimension dummy o agrupadora con el valor del ID(consecutivo)
	$aSQLUpdate = "UPDATE ".$nomTabla." SET ".$nomFisico." = ".$nomTabla."KEY WHERE ".$nomTabla."KEY <> 1";
	$rsUpdate = $theRepository->DataADOConnection->Execute($aSQLUpdate);
	
	//GCRUZ 2013-03-26. Agregado mensaje diferente de alert al finalizar la carga
	$alertResultMsg = $numRow.' records loaded';
}

//Obtenemos el CatalogID a partir del cla_descrip que se recibio
$sql = "SELECT CatalogID FROM SI_SV_Catalog WHERE ParentID = ".$intCatalogKey;
$aRS = $theRepository->DataADOConnection->Execute($sql);

if($aRS!==false && !$aRS->EOF)
{
	require_once("survey.inc.php");
	$svCatalogID = (int)$aRS->fields["catalogid"];
	
	require_once("catalog.inc.php");
	$aCatalog = BITAMCatalog::NewInstanceWithID($theRepository, $svCatalogID);
	if (!is_null($aCatalog))
	{
		$aCatalog->save();
	}
	
	BITAMSurvey::updateAllSurveyLastUserAndDateFromCatalog($theRepository, $svCatalogID);
}

$theRepository->close();

// delete the xls temp file
@unlink($strXLSFileTempName);
	
//@JAPR 2014-12-11: Agregado el soporte de imagenes en los atributos de los catálogo
if ($strErrors != '') {
	reportErrorToClient($strErrors);
	exit;
}
else {
	// everything's right!
	processTerminatedSuccessfully($numRow);
}
//@JAPR
	
function processTerminatedSuccessfully($numRow)
{
	global $strLogFile;
	$strLogPath = "";
	if (file_exists($strLogFile))
	{
		$arrLogFileName = explode("\\", $strLogFile);
		$strLogFileName = end($arrLogFileName);
		//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
		$strLogPath = GeteFormsLogPath().$strLogFileName;
		//@JAPR
	}
?>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		  function pageloaded()
		  {	//main.php?BITAM_PAGE=Catalog&CatalogID=<?php echo @$intCatalogKey; ?>
			//window.parent.refreshpage();
			alert('<?=$numRow?> records loaded');
			document.location.href = 'main.php?BITAM_PAGE=Catalog&CatalogID=<?php echo @$intCatalogKey; ?>';
		  }
		</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?    
}
  
function reportErrorToClient($errdesc)
{
?>    
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script language="javascript">
		  function pageloaded()
		  {
			alert("There was an error during the import process: " + "<?=str_replace("\\", "\\\\", str_replace("\n", "", $errdesc))?>")
		  }
		</script>
	</head>
<body onload="pageloaded()">
</body>
</html>
<?
}
  
function checkEmail($email) {
	if(eregi("^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$" , $email)){
		//@JAPR 2015-02-06: Agregado soporte para php 5.6
		list($username,$domain)=explode('@',$email);
		//if(!customCheckDnsrr($domain)){
		 //return false;
		//}
		return true;
	}
	return false;
}
  
function myErrorLog($errDesc)
{
	global $strLogFile;
	$dtNow = date("Y-m-d H:i:s");
	error_log($dtNow." - ".$errDesc."\r\n", 3, $strLogFile);
}

function writeLogDetail($msg, $logFile, $time = true)
{
	if ($time)
	{
		$msg = '<div style="color:#666666; float:left">'.date('Y-m-d h:i:s').' ::: </div><div style="margin-left:150px">' .$msg.'</div>';
	}
	
	$modo = 'a';
	$frsupdate = fopen($logFile, $modo);
	if ($frsupdate)
	{
		fwrite($frsupdate, '<div class="row">'.$msg.'</div>');
		fclose($frsupdate);
	}
}
?>