var system = require('system');
var fs = require('fs');

var ProgramName = system.args[0];
if (system.args.length < 2) {
    if (DebugMode) { console.log('Missing report name!'); }
	phantom.exit(1);
}
var DebugMode = false;
var latitude = null;
var longitude = null;
var aTimeout = null;
var aService = null;
for (var i = 1; i < system.args.length; i++) {
	var arg = system.args[i];
	switch (arg) {
		case "-debug":
			DebugMode = true;
		break;
		default:
			var param = arg.split('=');
			if (param.length == 2) {
				switch(param[0]) {
					case 'latitude':
						latitude = param[1];
					break;
					case 'longitude':
						longitude = param[1];
					break;
					case 'timeout':
						aTimeout = param[1];
					break;
					case 'service':
						aService = param[1];
					break;
					//algún otro caso
					default:
					break;
				}
			}
		break;
	}
}

if (!aService) {
 	if (DebugMode) { console.log("No service provided on the request."); }
 	phantom.exit(1);
}

//poner la URL de mi php
var Url = aService+"mapkitGeocoding.php";
var Page = require('webpage').create();

Page.settings.webSecurityEnabled = false;
Page.settings.userAgent = 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.103 Safari/537.36';
Page.settings.loadImages = false;
var PageLoaded = false;
var FileDownloaded = false;

Page.onConsoleMessage = function(msg) {
	if (DebugMode) { console.log('Inner console message:' + msg); }
};

Page.onLoadStarted = function() {
	PageLoaded = false;
	if (DebugMode) {
		console.log('= onLoadStarted()');
		var currentUrl = Page.evaluate(function() {
			return window.location.href;
		});
		console.log('  leaving url: ' + currentUrl);
	}
};
 
Page.onLoadFinished = function(status) {
	PageLoaded = true;
	if (DebugMode) {
		console.log('= onLoadFinished()');
		console.log('  status: ' + status);
	}
};

Page.onResourceError = function(resourceError) {
	if (DebugMode) {
		console.log('= onResourceError()');
		console.log('  - unable to load url: "' + resourceError.url + '"');
		console.log('  - error code: ' + resourceError.errorCode + ', description: ' + resourceError.errorString );
	}
};

Page.onError = function(msg, trace) {
    if (DebugMode) { console.log('= onError()'); }
    var msgStack = ['  ERROR: ' + msg];
    if (trace) {
        msgStack.push('  TRACE:');
        trace.forEach(function(t) {
            msgStack.push('    -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
        });
    }
    if (DebugMode) { console.log(msgStack.join('\n')); }
};

function WaitFor(testFx, onReady) {
    var maxtimeOutMillis = aTimeout ? aTimeout : 15000; //< Default Max Timout is 15s
	var start = new Date().getTime();
	var condition = false;
	var interval = setInterval(function() {
		if ( (new Date().getTime() - start < maxtimeOutMillis) && !condition ) {
			// If not time-out yet and condition not yet fulfilled
			condition = (typeof(testFx) === "string" ? eval(testFx) : testFx()); //< defensive code
		} else {
			if(!condition) {
				// If condition still not fulfilled (timeout but condition is 'false')
				if (DebugMode) {
					console.log("'WaitFor()' timeout");
					var content = Page.evaluate(function () {
							return document.all[0].outerHTML;
					});
					try {
						fs.write("downloadReportDebug.html", content, 'w');
					} catch(e) {
						console.log(e);
					}
				}
				clearInterval(interval); //< Stop this interval
				Page.stop();
				phantom.exit(1);
			} else {
				// Condition fulfilled (timeout and/or condition is 'true')
				if (DebugMode) {
					console.log("'WaitFor()' finished in " + (new Date().getTime() - start) + "ms.");
				}
				if (DebugMode) {
					var content = Page.evaluate(function () {
							return document.all[0].outerHTML;
					});
					try {
						fs.write("downloadReportDebug.html", content, 'w');
					} catch(e) {
						console.log(e);
					}
				}
				clearInterval(interval); //< Stop this interval
				typeof(onReady) === "string" ? eval(onReady) : onReady(); //< Do what it's supposed to do once the condition is fulfilled
			}
		}
	}, 250); //< repeat check every 250ms
};

function returnValue() {
	var aPage = Page.evaluate(function() {
        return document.getElementById('mapkitResponse').value;
    });
    console.log(aPage);
    phantom.exit(1);
}

if (DebugMode) console.log('Connecting to ', Url, ' ...');
PageLoaded = false;
Page.open(Url+"?latitude="+latitude+"&longitude="+longitude, function(status) {
	var result, data;
	if (status !== 'success') {
		if (DebugMode) {
			console.log(Url);
			console.log('Error: Unable to access network!');
		}
		phantom.exit(1);
	} else {
		WaitFor(function() {
			// Check in the page does not contain a form
			return PageLoaded && Page.evaluate(function() {
				return document.getElementById('mapkitResponse').value != "";
			});
		}, returnValue);
	}
});
