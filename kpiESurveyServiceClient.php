<?
// loggear tiempos?
$blnLogTimes = true;
$dtInitialTime = microtime(true);
$dtLastTime = $dtInitialTime;

if ($blnLogTimes) { logTime("Starting process"); }
// loggear tiempos (end)

ini_set('soap.wsdl_cache_enabled', '0');
define("FORMS_TEMPLATE_ID", 12); // no se usa, por lo pronto

if ($blnLogTimes) { logTime("Logging request data"); }

@error_log(date("Y-m-d H:i:s")." - ".print_r($_REQUEST, true), 3, "fb.log");

if ($blnLogTimes) { logTime("Logged request data"); }

$intDefaultLanguage = 1; //español // no se usa, por lo pronto

// arreglo de errorCodes
$arrErrorCodes = array();
$arrErrorCodes["eSurveyServiceErrCode_0100"] = "None of the parameters appname, email or password can be an empty string";
$arrErrorCodes["eSurveyServiceErrCode_0101"] = "Could not establish connection to the master db";
$arrErrorCodes["eSurveyServiceErrCode_0102"] = "Could not select the master db";
$arrErrorCodes["eSurveyServiceErrCode_0103"] = "There was an error while retrieving info from master db";
$arrErrorCodes["eSurveyServiceErrCode_0104"] = "Could not found appname - account info";
$arrErrorCodes["eSurveyServiceErrCode_0105"] = "The account is currently set to an invalid status";
$arrErrorCodes["eSurveyServiceErrCode_0106"] = "Missing parameter 'data'";

$arrWarningCodes["eSurveyServiceWarningCode_0100"] = "The user already exists";
$arrWarningCodes["eSurveyServiceWarningCode_0101"] = "Could not establish connection to the repository db";
$arrWarningCodes["eSurveyServiceWarningCode_0102"] = "Could not select the repository db";
$arrWarningCodes["eSurveyServiceWarningCode_0103"] = "There was an error while retrieving guests info from repository";
$arrWarningCodes["eSurveyServiceWarningCode_0104"] = "There was an error while trying to update eForms permissions";
$arrWarningCodes["eSurveyServiceWarningCode_0105"] = "This operation must be authorized by the administrator user";


// arreglo default de retorno
$arrReturn = array(
  "error" => 0,
  "errorCode" => 0,
  "errorMsg" => "",
  "warning" => 0,
  "warningCode" => 0,
  "warningMsg" => "",
  "validAccount" => 1,
  "availableLicenses" => 0,
  "regType" => 0,
  "user" => "",
  "password" => "",
  "passwordEnc" => "",
  "encrypted" => 0
);

// build >= 20160630000000
//@JAPR 2016-08-23: Corregido un bug, no se había actualizado la versión del uso de passwords encriptados, la correcta es a partir de 6.0300
// appVersion >= 6.03000

$sdata = (isset($_REQUEST["data"]) ? $_REQUEST["data"] : "");

if (strlen($sdata) == 0)
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0106";
  returnResult($arrReturn);
}

$data = json_decode($sdata, true);

$registerData = $data["registerData"];

// asignar nuevos parámetros de eFormsV6 para registro de un invitado
$appName = (isset($data["appName"]) ? $data["appName"] : "");

$register_name = (isset($registerData["register_name"]) ? $registerData["register_name"] : "");
$register_firstname = (isset($registerData["register_firstname"]) ? $registerData["register_firstname"] : "");
$register_middlename = (isset($registerData["register_middlename"]) ? $registerData["register_middlename"] : "");
$register_lastname = (isset($registerData["register_lastname"]) ? $registerData["register_lastname"] : "");
$register_email = (isset($registerData["register_email"]) ? $registerData["register_email"] : "");
$register_password = (isset($registerData["register_password"]) ? $registerData["register_password"] : "");
$register_birthday = (isset($registerData["register_birthday"]) ? $registerData["register_birthday"] : "");
$register_gender = (isset($registerData["register_gender"]) ? $registerData["register_gender"] : "");
$register_hometown = (isset($registerData["register_hometown"]) ? $registerData["register_hometown"] : "");
$register_work = (isset($registerData["register_work"]) ? $registerData["register_work"] : "");
$register_telephone = (isset($registerData["register_telephone"]) ? $registerData["register_telephone"] : "");
$register_language = (isset($registerData["register_language"]) ? $registerData["register_language"] : "");
$register_id = (isset($registerData["register_id"]) ? $registerData["register_id"] : "");
$register_type = (isset($registerData["register_type"]) ? $registerData["register_type"] : "");

if (strlen($appName) == 0 || strlen($register_email) == 0 || strlen($register_password) == 0)
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0100";
  returnResult($arrReturn);
}

// --> obtener el hostEmail en base al appName
// get connection info

if ($blnLogTimes) { logTime("Bout to require utils & conns"); }

require_once("utils.inc.php");
require_once("conns.inc.php");

if ($blnLogTimes) { logTime("Just required utils & conns"); }

if ($blnLogTimes) { logTime("Attempting connection to fbm000"); }

// connect
$mysql_Connection = mysql_connect($server, $server_user, $server_pwd);
if (!$mysql_Connection) 
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0101";
  returnResult($arrReturn);
}

// select fbm000 saas admin tables schema
$db_selected = mysql_select_db($masterdbname, $mysql_Connection);
if (!$db_selected) 
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0102";
  returnResult($arrReturn);
}

if ($blnLogTimes) { logTime("Connected to fbm000"); }

$aSQL = <<<EOS
SELECT b.email, a.regType, DATE_ADD(d.confirmDate, INTERVAL 30 DAY) AS expirationDate, DATEDIFF(DATE_ADD(d.confirmDate, INTERVAL 30 DAY), NOW()) AS daysLeft, c.DatabaseID, c.PaymentType, 
    c.CustomerType, COALESCE(c.eFormsMaxGuests, 0) AS eFormsMaxGuests, c.DBServer, c.DBServerUsr, c.DBServerPwd, c.DBType, c.Repository
FROM saas_eforms_apps a, saas_users b, saas_databases c, saas_regs d
WHERE a.appName = '$appName'
AND a.masterAccount = b.Email
AND b.userID = c.UserID
AND c.UserID = d.UserID
AND d.HostID = 0
AND c.status = 1
AND d.status = 1
EOS;

$result = mysql_query($aSQL, $mysql_Connection);

if ($blnLogTimes) { logTime("Thrown query: $aSQL"); }

// hubo algún error?
if (!$result)
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0103";
  returnResult($arrReturn);
}

// no trajo datos?
if (mysql_num_rows($result) == 0)
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0104";
  returnResult($arrReturn);
}

$row = mysql_fetch_assoc($result);
$adminEmail = $row["email"];
$regType = $row["regType"];
$expirationDate = $row["expirationDate"];
$daysLeft = $row["daysLeft"];
$databaseID = $row["DatabaseID"];
$paymentType = $row["PaymentType"];
$customerType = $row["CustomerType"];
$eFormsMaxGuests = $row["eFormsMaxGuests"];
if ($eFormsMaxGuests == 0 && $customerType != 1)
{
  $eFormsMaxGuests = 10;
}
$dbServer = $row["DBServer"];
$dbUser = $row["DBServerUsr"];
$dbPwd = BITAMDecryptPassword($row["DBServerPwd"]);
$dbType = $row["DBType"];
$repository = $row["Repository"];

// cuenta "banneada"?
$blnBannedDB = false;
if (file_exists("banned_dbs.php"))
{
  if (function_exists("checkBannedDB"))
  {
    $blnBannedDB = checkBannedDB($databaseID);
  }
}

// cuenta válida?
if ($blnBannedDB || ($daysLeft < 0 && $paymentType != -1))
{
  $arrReturn["errorCode"] = "eSurveyServiceErrCode_0105";
  $arrReturn["validAccount"] = 0;
  returnResult($arrReturn);
}

// parámetros originales para registro de un invitado:
$guestEmail = $register_email;
$guestPassword = $register_password;
$hostEmail = $adminEmail;
$productID = 1; // Artus, por lo pronto
$sendInternalEmail = 1;

$arrGuestData = array(
    "guestEmail" => $guestEmail,
    "guestPassword" => $guestPassword,
    "hostEmail" => $hostEmail,
    "productID" => "1",
    "sendInternalEmail" => 1,
    // nuevos parámetros:
    "register_name" => $register_name,
    "register_firstname" => $register_firstname,
    "register_middlename" => $register_middlename,
    "register_lastname" => $register_lastname,
    "register_email" => $register_email,
    "register_password" => BITAMencryptPassword($register_password),
    "register_birthday" => $register_birthday,
    "register_gender" => $register_gender,
    "register_hometown" => $register_hometown,
    "register_work" => $register_work,
    "register_telephone" => $register_telephone,
    "register_language" => $register_language,
    "register_id" => $register_id,
    "register_type" => $register_type
);

$serverUrl = 'http://localhost/fbm/kpiWebService.php?wsdl'; //cambiar aquí el nombre del servidor 

if ($blnLogTimes) { logTime("Bout to create object SoapClient"); }

$client = new SoapClient($serverUrl, array('exceptions' => 0, 'encoding' => 'ISO-8859-1', 'cache_wsdl' => 0));

if ($blnLogTimes) { logTime("Just created object SoapClient"); }

if ($blnLogTimes) { logTime("Bout to call \$client->registerGuest(\$arrGuestData)"); }

// register the guest!
$result = $client->registerGuest($arrGuestData);

if ($blnLogTimes) { logTime("Returned from \$client->registerGuest(\$arrGuestData)"); }

// inicializar
$availableLicenses = 0;

// was there an error?
if (is_soap_fault($result))
{
  $arrReturn["errorCode"] = $result->faultcode;
  $arrReturn["errorMsg"] = $result->faultstring;
  returnResult($arrReturn);
}
else
{
  // checar si hay licencias disponibles
  // connect
  
if ($blnLogTimes) { logTime("Attempting connection to rep to check if there are free licenses"); }
  
  $mysql_RepConnection = mysql_connect($dbServer, $dbUser, $dbPwd);
  if (!$mysql_RepConnection) 
  {
    $arrReturn["warningCode"] = "eSurveyServiceWarningCode_0101";
  }
  else
  {
    // select the repository
    $db_selected_rep = mysql_select_db($repository, $mysql_RepConnection);
    if (!$db_selected_rep) 
    {
      $arrReturn["warningCode"] = "eSurveyServiceWarningCode_0102";
    }

if ($blnLogTimes) { logTime("Just connected to rep"); }
    
	$aSQLRep = "SELECT COUNT(cla_usuario) AS currentNumberOfGuests FROM $repository.si_sv_users WHERE sv_user = 1";
    $resultRep = mysql_query($aSQLRep, $mysql_RepConnection);

if ($blnLogTimes) { logTime("Queried $aSQLRep"); }

    if (!$resultRep)
    {
      $arrReturn["warningCode"] = "eSurveyServiceWarningCode_0103";
    }
    else
    {
      $row = mysql_fetch_assoc($resultRep);
      $currentNumberOfGuests = $row["currentNumberOfGuests"];
      //$availableLicenses = 0; // inicializada más arriba
      if ($eFormsMaxGuests != -1)
      {
        $availableLicenses = -1;
      }
      elseif ($currentNumberOfGuests < $eFormsMaxGuests)
      {
        $availableLicenses = ($eFormsMaxGuests - $currentNumberOfGuests);
      }
    }
  }

  // si no hay licencias o el admin tiene que confirmar el registro, quitar el permiso de eForms al usuario
  if ($regType == 1) //($availableLicenses <= 0 || $regType == 1)
  {
    // quitar el permiso al nuevo guest en la si_sv_users
    $aSQLRepUpdate = "update si_usuario a, si_sv_users b set b.sv_user = 0 where a.cuenta_correo = '$guestEmail' and a.cla_usuario = b.cla_usuario";
    $result = mysql_query($aSQLRepUpdate, $mysql_RepConnection);
    if (!$result)
    {
      $arrReturn["warningCode"] = "eSurveyServiceWarningCode_0104";
    }
  }
  
  // armar el return
  $arrReturn["errorMsg"] = "success";
  // asignar el regType
  $arrReturn["regType"] = $regType;
  // asignar el número de licencias disponibles
  $arrReturn["availableLicenses"] = $availableLicenses;

  // regresar el password
  // build >= 20160630000000
  //@JAPR 2016-08-23: Corregido un bug, no se había actualizado la versión del uso de passwords encriptados, la correcta es a partir de 6.0300
  // appVersion >= 6.03000

  // enviar user y pass!
  $arrReturn["user"] = $guestEmail;
  $blnPassEncrypted =  (isset($data["encrypted"]) && intval($data["encrypted"]) == 1);
  //@JAPR 2016-08-23: Corregido un bug, no se había actualizado la versión del uso de passwords encriptados, la correcta es a partir de 6.0300
  if ($blnPassEncrypted || (floatval($data["build"]) >= 20160630000000 && floatval($data["appVersion"]) >= 6.03000))
  {
    $arrReturn["passwordEnc"] = BITAMEncode(BITAMEncryptPassword($guestPassword));
    $arrReturn["encrypted"] = 1;
  }
  else
  {
    $arrReturn["password"] = $guestPassword;
  }

  // regresar resultado success
  returnResult($arrReturn, false);

  // regType = 0 ? => (registro automático); enviar email de bienvenida al nuevo invitado; regType = 1? (requiere autorización; enviar email avisándole al host del nuevo registro)
  $arrInfo = array();
  if ($regType == 0) // es un registro automática, enviar email de bienvenida al nuevo invitado
  {
    $arrInfo["target"] = $guestEmail;
    $arrInfo["replace"] = array("{guestEmail}" => $guestEmail, "{guestPassword}" => $guestPassword);
    $arrInfo["mailPath"] = "./eFormsV6/";
    $arrInfo["mailFileName"] = "bienvenido.html";
	// sbrande_27jul2016: avisar en éste punto al admin, por medio de un correo, que ha sido registrado un nuevo usuario;
	// checar si tiene configurado un si_sv_users.emailAlt, para enviarle copia
	$strAltEmail = "";
    $aSQLRepEmail = "select AltEmail from si_sv_users where cla_usuario = 1";
    $result = mysql_query($aSQLRepEmail, $mysql_RepConnection);

if ($blnLogTimes) { logTime("Queried $aSQLRepEmail"); }

    if ($result && mysql_num_rows($result) > 0)
    {
	  $row = mysql_fetch_assoc($result);
      $strAltEmail = (is_null($row["AltEmail"]) || strlen($row["AltEmail"]) == 0 ? "" : $row["AltEmail"]);
    }
	// enviar correo al admin
    $arrAdminInfo["target"] = $hostEmail;
	if (strlen($strAltEmail) > 0)
	{
	  $arrAdminInfo["cc"] = $strAltEmail;
	}
    $arrAdminInfo["replace"] = array("{guestEmail}" => $guestEmail, "{hostEmail}" => $hostEmail);
    $arrAdminInfo["mailPath"] = "./eFormsV6/";
    $arrAdminInfo["mailFileName"] = "nuevoReg.html";

if ($blnLogTimes) { logTime("Bout to call getMailInfo"); }

	$arrAdminMailInfo = getMailInfo($arrAdminInfo);
	
if ($blnLogTimes) { logTime("Just called getMailInfo"); }
	
if ($blnLogTimes) { logTime("Bout to call sendMailToClient"); }

	sendMailToClient($arrAdminMailInfo);

if ($blnLogTimes) { logTime("Just called sendMailToClient"); }

  }
  elseif ($regType == 1)
  {
    // set params
    $params = BITAMEncode("hostEmail=$hostEmail&guestEmail=$guestEmail&guestPassword=$guestPassword");
    $arrInfo["target"] = $hostEmail;
    $arrInfo["replace"] = array("{hostEmail}" => $hostEmail, "{guestEmail}" => $guestEmail, "{params}" => $params);
    $arrInfo["mailPath"] = "./eFormsV6/";
    $arrInfo["mailFileName"] = "autorizacion.html";
  }
if ($blnLogTimes) { logTime("Bout to call getMailInfo (2nd)"); }

  $arrMailInfo = getMailInfo($arrInfo);

if ($blnLogTimes) { logTime("Just called getMailInfo (2nd)"); }

if ($blnLogTimes) { logTime("Bout to call sendMailToClient (2nd)"); }

  sendMailToClient($arrMailInfo);

if ($blnLogTimes) { logTime("Just called sendMailToClient (2nd)"); }
}

/******************************** funciones de correos ***********************************/
function sendMailToClient($arrMailInfo)
{
  //require_once('./mail/class.phpmailer.php');
  //require_once('./mail/class.smtp.php');
  require_once('class.phpmailer.php');
  include("project_state.php");
  //global $IsInProduction, $Testing, $arrTestingAccounts;
  if ($IsInProduction && !$Testing)   
  {
    $server = 'localhost';
    $port = 25;
    //$username = 'saas';
    //$password = '0517';
    $username = 'kpionline';
    $password = 'b1t4mkp1';
    $source = 'kpionline@bitam.com';
    
    // direcciones
    //$target = "aceballos@bitam.com";
    $arrBCC = array("lroux@bitam.com", "magutierrez@bitam.com", "sbrande@bitam.com", "cgil@bitam.com");
  }
  else 
  {   
    $server = 'mail.bitam.com';
    $port = 25;
    $username = 'testkpi';
    $password = 'bitam';
    $source = 'testkpi@bitam.com';

    // direcciones
    //$target = "sbrande@bitam.com";
    $arrBCC = array("magutierrez@bitam.com", "sbrande@bitam.com");
  }
  
  $target = $arrMailInfo["target"];
  $subject = $arrMailInfo["subject"];
  $body = $arrMailInfo["body"];

  //Send the mail notification
  try
  {
    $mail = new PHPMailer();
    $mail->AddAddress($target);
	
	// sbrande_27jul2016: add cc?
	if (isset($arrMailInfo["cc"]))
	{
	  $arrCC = (is_array($arrMailInfo["cc"]) ? $arrMailInfo["cc"] : array($arrMailInfo["cc"]));
	  foreach ($arrCC as $dir)
	  {
	    $mail->AddCC($dir);
      }
	}

    foreach($arrBCC as $dir)
    {
      $mail->AddBCC($dir);
    }
    
    if (isset($arrMailInfo["imgs"]))
    {
      $imgs = $arrMailInfo["imgs"];
      foreach ($imgs as $img)
      {
        $mail->AddEmbeddedImage($img["src"], $img["onlyname"], $img["filename"]);
      }
    }
//@error_log("body = $body", 3, "testingzzz.log")    ;
    $mail->Mailer = "smtp";
    $mail->SMTPAuth = true;
	//@JAPR 2016-09-27: Corregido un bug, por alguna razón en KPI10 no funcionaba el mismo código para envío de EMails, así que se se comparó contra la manera en que eBavel lo hace
	//y se detectó que esta línea era la que se requería para que funcionara el envío de los correos, se dejaron el resto de las diferencias como referencia (#C601QN)
	$mail->SMTPAutoTLS = false;
	//$mail->SMTPDebug = false;
	//$mail->Debugoutput = 'echo';
	//$mail->SMTPSecure = '';
	//$mail->CharSet = 'UTF-8';
	//@JAPR
    $mail->SMTPDebug = false;
    $mail->Host = $server;
    $mail->Helo = $server;
    $mail->Port = $port;
    $mail->Username = $username;
    $mail->Password = $password;
    $mail->From = $source;
    $mail->FromName = $source;
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->IsHTML(true);
//ob_start("myfn");    
    $res = $mail->Send();
//ob_end_flush();
  }
  catch (Exception $e)
  {
    $result = "Error: " . $e->getMessage();
  }
}

function getMailInfo($arrInfo)
{
//error_log("arrInfo: ".print_r($arrInfo, true), 3, "logging10.log");
  $strHTMLMailPath = (isset($arrInfo["mailPath"]) ? realpath($arrInfo["mailPath"]) : realpath("."));
  $strLang = (isset($arrInfo["lang"]) ? "_".$arrInfo["lang"] : "");
  $arrMailInfo = array();
  $strSubject = (isset($arrInfo["subject"]) ? $arrInfo["subject"] : "KPI Online");
  $strBody = (isset($arrInfo["body"]) ? $arrInfo["body"] : "");
  $strHTMLFile = $arrInfo["mailFileName"];
//error_log("file_exists($strHTMLMailPath/$strHTMLFile)" , 3, "logging10.log");
  if (strlen($strHTMLFile) > 0 && file_exists("$strHTMLMailPath/$strHTMLFile"))
  {
    require_once("simple_html_dom.php");
    //require_once("simple_html_dom.php");
    $html = file_get_html("$strHTMLMailPath/$strHTMLFile");
//error_log("dentro: $html" , 3, "logging10.log");
    $oTitle = $html->find("title", 0);
    if (!is_null($oTitle))
    {
      $strSubject = utf8_decode(html_entity_decode($oTitle->innertext, ENT_COMPAT, 'UTF-8'));
    }
    $imgs = $html->find("img");
    foreach ($imgs as $index => $img)
    {
      $strSrc = $img->getAttribute("src");
      $strFileName = basename($strSrc);
      $strOnlyName = preg_replace('/\..*/', "", $strFileName);
      $arrMailInfo["imgs"][$strFileName]["filename"] = $strFileName;
      $arrMailInfo["imgs"][$strFileName]["src"] = "$strHTMLMailPath/images/$strFileName";
      $arrMailInfo["imgs"][$strFileName]["onlyname"] = $strOnlyName;
      $imgs[$index]->setAttribute("src", "cid:$strOnlyName");
    }
    //$strBody = iconv("UTF-8", "ISO-8859-1", $html->find("html",0).outertext);
    $strBody = $html->find("html",0)->outertext;
    $strBody = str_replace(array_keys($arrInfo["replace"]), array_values($arrInfo["replace"]), $strBody);
  }
  $arrMailInfo["target"] = $arrInfo["target"];
  $arrMailInfo["subject"] = $strSubject;
  $arrMailInfo["body"] = $strBody;
  return $arrMailInfo;
}

function returnResult($return, $blnExit = true)
{
  global $data, $arrErrorCodes, $arrWarningCodes, $regType;
  // convertir a enter el errorCode y el warningCode!
  if (is_string($return["errorCode"]))
  {
    // checar si el error es de usuario duplicado
    $strErrorCode = $return["errorCode"];
    $intErrorCode = intval(preg_replace("/^.*_/", "", $strErrorCode));
    if ($intErrorCode == 14 && $return["errorMsg"] == "The supplied email-address is already registered to the requested template")
    {
      // manejarlo como warning
      $return["errorCode"] = 0;
      $return["errorMsg"] = "";
      $return["warningCode"] = "eSurveyServiceWarningCode_0100";
    }
    else
    {
      $return["error"] = 1;
      $return["errorCode"] = $intErrorCode;
      $return["errorMsg"] = (isset($return["errorMsg"]) && strlen($return["errorMsg"]) > 0 ? $return["errorMsg"] : (string)@$arrErrorCodes[$strErrorCode]);
    }
  }
  if (is_string($return["warningCode"]))
  {
    $strWarningCode = $return["warningCode"];
    $intWarningCode = intval(preg_replace("/^.*_/", "", $strWarningCode));
    $return["warning"] = 1;
    $return["warningCode"] = $intWarningCode;
    $return["warningMsg"] = $arrWarningCodes[$strWarningCode];
  }
  // waiting for authorization?
  if ($return["error"] == 0 && $return["warning"] == 0 && $regType == 1) // requiere autorización? manejarlo como warning
  {
    $strWarningCode = "eSurveyServiceWarningCode_0105";
    $return["warning"] = 1;
    $return["warningCode"] = 105;
    $return["warningMsg"] = $arrWarningCodes[$strWarningCode];
  }
  
  header("Content-Type: text/javascript");
  if (isset($data['jsonpCallback'])) 
  {
    @error_log(date("Y-m-d H:i:s")." - (case1) - ".print_r($data, true), 3, "fb.log");
    echo($data['jsonpCallback'] . '(' . json_encode($return) . ');');
  } 
  else 
  {
    @error_log(date("Y-m-d H:i:s")." - (case2) - ".print_r($data, true), 3, "fb.log");
    echo(json_encode($return));
  }
  if ($blnExit) { exit(); }
}

function logTime($msg)
{
	global $blnLogTimes, $dtInitialTime, $dtLastTime;
	if (!isset($blnLogTimes) || !isset($dtLastTime) || !$blnLogTimes) { return; }
	$dtCurrentTime = microtime(true);
	$dtTotalTime = round($dtCurrentTime - $dtLastTime, 4);
	$dtSuperTotalTime = round($dtCurrentTime - $dtInitialTime, 4);
	$dtLastTime = $dtCurrentTime;
	@error_log(date("Y-m-d H:i:s")." - $msg => Last step time = $dtTotalTime; Total time = $dtSuperTotalTime".PHP_EOL, 3, "kpiESurveyLogTimes.log");
}
?>