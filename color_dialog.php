<?PHP
session_start();
require_once($_SESSION["PAuserLanguage"].".inc.php");
global $ML;
?>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?=$ML['CHOOSE_COLOR']?></title>
<style>
input {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	padding-top: 1px;
	padding-bottom: 1px;
	padding-left: 3px;
	padding-right: 3px;
	color: #666666;
	border: 1px solid #666666;
}
span {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
	padding-top: 1px;
	padding-bottom: 1px;
	padding-left: 3px;
	padding-right: 3px;
	color: #666666;
}
.NBody {
    margin: 0px 0px 0px 0px;
    font-family: Calibri, Verdana, Arial, Helvetica, sans-serif;
    color: black;
    font-size: 12px;
    background-color: #f4f5f7;
}
.Nbutton {
    width: 70px;
    border-left: 1px solid #f8f9fb;
    border-right: 1px solid #707c8a;
    border-bottom: 1px solid #707c8a;
    border-top: 1px solid #f8f9fb;
    background-color: #f4f5f7;
    font-size: 10px;
    color: #213b63;
    font-weight: normal;
    background-image: url(images/bgboton.gif);
    background-repeat: repeat-x;
}
</style>
<script language="javascript" src="js/EditorHTML/<?=$_SESSION["PAuserLanguage"]?>.js"></script>
<script language="javascript" src="js/EditorHTML/utils.js"></script>
<script language="javascript" src="js/EditorHTML/color_conv.js"></script>
<script language="javascript">
<?php
$arr_color = "var arr_colors = [";
$arr_color .= "];\n";
$bhave_colors = "var bhave_colors = false;\n";
echo $arr_color;
echo $bhave_colors;
?>
</script>
</head>
<body onLoad="fnOnLoad();" class="NBody" style="overflow:hidden;">
<iframe name="FrameColors" id="FrameColors" style="display:none; visibility:hidden; position:absolute; width:1px; height:1px; top:0px; left:0px;" src=""></iframe>
<div id='div_colors' style="position:absolute; display:none;">
<span><script language="javascript">document.write(ML[414])</script>:</span>
<br/>
</div>


<div id='div_cust_colors' style="position:absolute; display:none;">
<span><script language="javascript">document.write(ML[415])</script>:</span>
<br/>
</div>

<div id='div_transp_color' style="position:absolute; display:inline;">
<span><script language="javascript">document.write(ML[416])</script>:</span>&nbsp;&nbsp;<input type="checkbox" name="nTransparent" id="id_Transparent" style="background-color:transparent;border:none;">
<br/>
</div>

<div id='div_buttons' style="position:absolute; top:300px; display:none;">
	<table>
		<tr>
			<td align = "center" colspan = "2">
			<input style="WIDTH: 215px" type="button" class="Nbutton" id="DefineCustomColors" name="DefineCustomColors" value = "<?=$ML['DEF_CUST_COLOR']?> >>"
				onClick="ShowHideCustColors();"
			>
			</td>
		</tr>
		<tr>
			<td align = "center">
				<input style="width:70px" type="button" class="Nbutton" id="btn_OK" name="btn_OK" value = "<?=$ML['OK']?>" onClick="EndDialog('OK');" >
			</td>
			<td align = "center">
				<input style="width:70px"  type="button" class="Nbutton" id="btn_Cancel" name="btn_Cancel" value = "<?=$ML['CANCEL']?>" onClick="EndDialog('Cancel');" >
			</td>
		</tr>
	</table>
</div>



<div id='div_graphics' style="position:absolute; display:none">
	<div id = "HSTable" style="position:absolute; " onclick="CalcSelectPos(event);">
		<div  id = "div_select" style="position:absolute; padding:0px; margin:0px; outline:0px" >
			<img id = "img_select" src="images/select.GIF"></img>
		</div>
	</div>

	<div id = "LTable" style="position:absolute;" onClick="CalcSliderPos(event);">
		<div  id = "div_slider" style="position:absolute; padding:0px; margin:0px; outline:0px;">
			<img id = "img_slide" src="images/slide.gif"></img>
		</div>
	</div>
</div>



<div id="div_values" style="position:absolute; display:none;">
<table cellpadding="0" cellspacing="0">
<tr>
<!------------------------------------------------------------->
	<td>
		<table cellpadding="0" cellspacing="0" >
			<tr height="40" >
				<td id="previewColor">
				</td>
			</tr>
			<tr>
				<td>
				<span><script language="javascript">document.write(ML[417])</script></span>
				</td>
			</tr>
		</table>
	</td>
<!------------------------------------------------------------->
	<td>
		<table>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[418])</script>:</span></td>
				<td><input maxlength="3" size="3" type="text" id="H_id" name="H_id" value = "2" onKeyUp="fnVerifyNumber('H_id',240);CalcPosByHue();"/></td>
			</tr>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[419])</script>:</span></td>
				<td><input maxlength="3"size="3" type="text" id="S_id" name="S_id" value = "2" onKeyUp="fnVerifyNumber('S_id',240);CalcPosBySat();"/></td>
			</tr>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[420])</script>:</span></td>
				<td><input maxlength="3" size="3" type="text" id="L_id" name="L_id" value = "2" onKeyUp="fnVerifyNumber('L_id',240);CalcPosByLum();"/></td>
			</tr>
		</table>
	</td>
<!------------------------------------------------------------->
	<td>
		<table>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[392])</script>:</span></td>
				<td><input maxlength="3" size="3" type="text" id="R_id" name="R_id" value = "1" onKeyUp="fnVerifyNumber('R_id',255);OnChangeRGB();"/></td>
			</tr>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[394])</script>:</span></td>
				<td><input maxlength="3" size="3" type="text" id="G_id" name="G_id" value = "1" onKeyUp="fnVerifyNumber('G_id',255);OnChangeRGB();"/></td>
			</tr>
			<tr>
				<td align="right"><span><script language="javascript">document.write(ML[421])</script>:</span></td>
				<td><input maxlength="3" size="3" type="text" id="B_id" name="B_id" value = "1" onKeyUp="fnVerifyNumber('B_id',255);OnChangeRGB();"/></td>
			</tr>
		</table>
	</td>

</tr>
<tr>
	<td colspan="3" align="center">
		<input type="button" class="Nbutton" style="width:150px" id="AddCustomColor" name="AddCustomColor" value = "<?=$ML['ADD_CUST_COLOR']?>"
			onClick="
				R = parseInt(document.getElementById('R_id').value);
				G = parseInt(document.getElementById('G_id').value);
				B = parseInt(document.getElementById('B_id').value);
				fnAddCustColorClick(R,G,B);
			"
		/>
	</td>
</tr>
</table>
</div>
</body>
</html>