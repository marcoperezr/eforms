  var intCmbMultiSelCounter = 0
  var CmbMultiSelArray = new Array()
  
  function cmbMultiSel_change(scmbid)
  {
    var ocmb = document.getElementById(scmbid)
    if (ocmb == null) { return }
    if (ocmb.options[ocmb.selectedIndex].value == ocmb.getAttribute("selvalue"))
    {
      var odiv = document.getElementById("div_" + scmbid)
      if (odiv == undefined)
      {
        // create the container div
        var odiv = document.createElement("DIV")
        odiv.id = "div_" + scmbid
        odiv.style.position = "absolute"
        odiv.style.backgroundColor = "transparent"
        //odiv.style.posLeft = ocmb.offsetLeft
        //odiv.style.posTop = ocmb.offsetTop + ocmb.offsetHeight
        odiv.style.posLeft = GetAbsoluteLeft(ocmb)
        odiv.style.posTop = GetAbsoluteTop(ocmb) + ocmb.offsetHeight
        odiv.style.posWidth = ocmb.offsetWidth
        
        // create the multiple select 
        olist = document.createElement("SELECT")
        olist.id = "lst_" + scmbid
        olist.style.fontFamily = ocmb.style.fontFamily
        olist.style.fontSize = ocmb.style.fontSize
        olist.style.width = "100%"
        olist.setAttribute("multiple", true)
        if (ocmb.options.length <= 11)
          olist.size = ocmb.options.length - 2
        else
          olist.size = 9
        var ncounter
        for (ncounter = 2; ncounter < ocmb.options.length; ncounter++)
        {
          var ooption = document.createElement("OPTION")
          ooption.value = ocmb.options[ncounter].value
          ooption.text = ocmb.options[ncounter].text
          olist.options.add(ooption)
        }
        olist = odiv.appendChild(olist)
          
        // create the imgs container table
        var otable = document.createElement("TABLE")
        otable.id = "tbl_" + scmbid
        otable.cellpadding = 1
        otable.cellspacing = 0
        otable.style.backgroundColor = "#efefef"
        otable.style.borderStyle = "ridge"
        otable.style.borderWidth = "1px"
        otable.style.width = "100%"
        var orow = document.createElement("TR")
        orow = otable.appendChild(orow)
        var otd = document.createElement("TD")
        otd.innerHTML = "&nbsp;"
        otd.setAttribute("align", "left")
        otd = orow.appendChild(otd)
        var otd = document.createElement("TD")
        otd.innerHTML = '<img id=imgok src=images/ok.png style="cursor:pointer" onclick="cmbMultiSel_ok(\'' + scmbid + '\',true,true)">'
        otd.setAttribute("align", "right")
        otd.setAttribute("width", "20px")
        otd = orow.appendChild(otd)
        var otd = document.createElement("TD")
        otd.innerHTML = '<img src=images/cancel.png style="cursor:pointer" onclick="cmbMultiSel_closepopup(\'' + scmbid + '\')">'
        otd.setAttribute("align", "right")
        otd.setAttribute("width", "20px")
        otd = orow.appendChild(otd)
        otable = odiv.appendChild(otable)
        
        // add the new div to the doc
        document.body.insertAdjacentHTML("beforeEnd", odiv.outerHTML)
        
        // attach event to the div
        odiv = document.getElementById("div_" + scmbid)
        odiv.onclick = function () { window.event.cancelBubble = true } 

        // attach onselok event
        sselok = ocmb.getAttribute("onselok")
        if (sselok != undefined)
        {
          var oimg = odiv.all("imgok")
          if (oimg != null)
          {
            oimg.attachEvent("onclick", function() {eval(sselok + '(\'' + scmbid + '\')')})
          }
        }

        // attach events to the body
        document.body.attachEvent("onclick", function () { cmbMultiSel_closepopup(scmbid) })
        document.body.attachEvent("onkeypress", function () { if (window.event.keyCode == 27) { cmbMultiSel_closepopup(scmbid) } })
      }
      else
      {
        // ya existe el div... mostrarlo!
        cmbMultiSel_showpopup(scmbid)
      }
      cmbMultiSel_markselected(scmbid)
    }
    else
    {
      ocmb.setAttribute("sellist", "")
      cmbMultiSel_closepopup(scmbid)
    }    
  }
  
  function cmbMultiSel_showpopup(scmbid)
  {
    var odiv = document.getElementById("div_" + scmbid)
    if (odiv == null) { return }
    odiv.style.display = "inline"
  }
  
  function cmbMultiSel_markselected(scmbid)
  {
    // mark the selected options
    var sselected
    var ocmb = document.getElementById(scmbid)
    if (ocmb == null) { return }
    var olist = document.getElementById("div_" + scmbid).all("lst_" + scmbid)
    if (olist != undefined)
    {
      sselected = ocmb.getAttribute("sellist")
      if (sselected == null)
      {
        sselected = ""
      }
      var ncounter
      sselected = "," + sselected + ","
      for (ncounter = 0; ncounter < olist.options.length; ncounter++)
      {
        if (sselected.indexOf("," + olist.options[ncounter].value + ",") >= 0)
        {
          olist.options[ncounter].selected = true
        }
        else
        {
          olist.options[ncounter].selected = false
        }
      }
    }
  }
  
  function cmbMultiSel_mouseover(scmbid)
  {
    var ocmb = document.getElementById(scmbid)
    if (ocmb == null) { return }
    if (ocmb.options[ocmb.selectedIndex].value == ocmb.getAttribute("selvalue"))
    {
      var odiv = document.getElementById("div_" + ocmb.id)
      if (odiv == null) { return }
      if (odiv.style.display != "inline" && odiv.style.display != "")
      {
        cmbMultiSel_hideotherpopups(scmbid)
        cmbMultiSel_showpopup(scmbid)
      }
    }
  }

  function cmbMultiSel_ok(scmbid)
  {
    var ocmb = document.getElementById(scmbid)
    if (ocmb == null) { return }
    var ncounter, olst, simgbttitle, nselected, sselected
    nselected = 0
    simgbttitle = ""
    olst = document.getElementById("div_" + scmbid).all("lst_" + scmbid)
    sselected = ""
    for (ncounter = 0; ncounter < olst.options.length; ncounter++)
    {
      if (olst.options[ncounter].selected)
      {
        sselected += "," + olst.options[ncounter].value
        simgbttitle += "\n" + "  " + olst.options[ncounter].text + "  "
        nselected++
      }
    }
    sselected = sselected.substr(1)
    if (sselected.length == 0 || nselected == olst.options.length)
    {
      ocmb.selectedIndex = 0
      ocmb.setAttribute("sellist", "")
    }
    else if (nselected == 1)
    {
      for (ncounter = 2; ncounter < ocmb.length; ncounter++)
      {
        if (parseInt(ocmb.options[ncounter].value) == parseInt(sselected))
        {
          ocmb.selectedIndex = ncounter
          ocmb.setAttribute("sellist", "")
          break
        }
      }
    }
    else
    {
      ocmb.setAttribute("sellist", sselected)
    }
    cmbMultiSel_closepopup(scmbid)
  }
  
  function cmbMultiSel_closepopup(scmbid)
  {
    var odiv = document.getElementById("div_" + scmbid)
    if (odiv == null) { return }
    odiv.style.display = "none"
    var ocmb = document.getElementById(scmbid)
    if (ocmb == null) { return }
    if (ocmb.options[ocmb.selectedIndex].value == ocmb.getAttribute("selvalue"))
    {
      var sselected = ocmb.getAttribute("sellist")
      if (sselected == null || sselected == "")
      {
        ocmb.selectedIndex = 0
      }
    }
  }
  
  function cmbMultiSel_click(scmbid)
  {
    window.event.cancelBubble = true
    cmbMultiSel_hideotherpopups(scmbid)
  }
  
  function cmbMultiSel_hideotherpopups(scmbid)
  {
    // hide the other popups
    var ncounter, stempid
    for (ncounter = 0; ncounter < CmbMultiSelArray.length; ncounter++)
    {
      stempid = CmbMultiSelArray[ncounter]
      if (stempid != scmbid)
      {
        cmbMultiSel_closepopup(stempid)
      }
    }
  }
  
  function AddCmbMultiSel(ocmb, AllText, SelText, AllValue, SelValue)
  {
    if (ocmb == null || ocmb == undefined) { return false }
    if (ocmb.options.length < 2) { return false }
    
    var ooption, sAllText, sSelText, sAllValue, sSelValue
    
    sAllText = typeof AllText != "undefined" ? AllText : "(Todos)"
    sSelText = typeof SelText != "undefined" ? SelText : "(Selección)"
    sAllValue = typeof AllValue != "undefined" ? AllValue : "_all_"
    sSelValue = typeof SelValue != "undefined" ? SelValue : "_sel_"
    
    if (ocmb.options.length >= 3)
    {
      // sel option
      ooption = document.createElement("OPTION")
      ooption.value = sSelValue
      ooption.text = sSelText
      ocmb.add(ooption, 0)
      ocmb.setAttribute("selvalue", sSelValue)
    }
    // all option
    ooption = document.createElement("OPTION")
    ooption.value = sAllValue
    ooption.text = sAllText
    ocmb.add(ooption, 0)
    ocmb.setAttribute("allvalue", sAllValue)
    // events
    ocmb.attachEvent("onchange", function () { cmbMultiSel_change(ocmb.id) })
    ocmb.attachEvent("onmouseover", function () { cmbMultiSel_mouseover(ocmb.id) })
    ocmb.attachEvent("onclick", function () { cmbMultiSel_click(ocmb.id) })
    // select first
    ocmb.selectedIndex = 0
    // add it to the array
    CmbMultiSelArray[intCmbMultiSelCounter] = ocmb.id
    intCmbMultiSelCounter++
    return true
  }

  function GetAbsoluteLeft(oNode)
  {
    var nLeft, oNodeTemp
    nLeft = 0
    oNodeTemp = oNode
    while (oNodeTemp.tagName != "BODY")
    {
      nLeft += oNodeTemp.offsetLeft
      oNodeTemp = oNodeTemp.offsetParent
    }
    return nLeft
  }

  function GetAbsoluteTop(oNode)
  {
    var nTop, oNodeTemp
    nTop = 0
    oNodeTemp = oNode
    while (oNodeTemp.tagName != "BODY")
    {
      nTop += oNodeTemp.offsetTop
      oNodeTemp = oNodeTemp.offsetParent
    }
    return nTop
  }  