dim oIE
set oIE = WScript.CreateObject("InternetExplorer.Application")
oIE.Visible = false
set objArgs = WScript.Arguments
if not objArgs is nothing then
  oIE.Navigate objArgs(0)
end if

do while oIE.Busy or (oIE.READYSTATE <> 4)
    WScript.Sleep 100
loop
oIE.Quit
set oIE = nothing
