<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

//Lee los parámetros con la versión desde la que se quiere actualizar hasta la que se deberá actualizar, hará un Require_once de cada archvo
$dblFromVersion = getParamValue('from', 'both', "(float)", true);
//La primer versión que se soportó con este PhP es la esveFormsV41Func, por lo que por default se coloca en la versión inmediata
if (is_null($dblFromVersion)) {
	$dblFromVersion = esvMultiDynamicAndStandarization;
}

//Si no se especifica la versión destino entonces se asume que se actualizará a la última del servicio
$dblToVersion = getParamValue('to', 'both', "(float)", true);

//Agrega las columnas con el ID de sección y ID de pregunta categoría de dimensión para la tabla paralela, con lo cual se podrá identificar
//si un registro capturado corresponde con alguno de esos elementos
//Campos EntrySectionID y EntryCatDimID
if (!is_null($dblToVersion) && $dblToVersion < esveFormsV41Func) {
	die();
}
if ($dblFromVersion < esveFormsV41Func) {
	require_once('createSurveyRecordSourceFields.php');
	require_once('createServerDateTimeDims.php');
}
if ($dblFromVersion < esvCatalogDissociation) {
//No habilitar este proceso nunca en este script, ya que es demasiado riesgozo y tardado como para ejecutarlo a la ligera, si realmente se quiere
//ejecutar entonces primero se requiere un BackUp de la BD, tentativamente que nadie haga capturas mientras dura el proceso, y revisar el log
//al terminar para verificar que no existan errores críticos o se hubiera dañado algo
//	require_once('dissociateCatalogs.php');
}
?>