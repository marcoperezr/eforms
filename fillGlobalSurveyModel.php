<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");
require_once("question.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

//Obtenemos los datos del cubo global de encuestas a partir de la primera encuesta de la coleccion
if(isset($surveyCollection->Collection[0]))
{
	$surveyInstance = $surveyCollection->Collection[0];
	$strOriginalWD = getcwd();
	
	//Obtener el DateKey
	require_once("cubeClasses.php");
	require_once("dimension.inc.php");
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	//session_register("BITAM_UserID");
	//session_register("BITAM_UserName");
	//session_register("BITAM_RepositoryName");
	$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
	$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
	$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

	require_once("../model_manager/model.inc.php");
	require_once("../model_manager/modeldata.inc.php");
	require_once("../model_manager/modeldimension.inc.php");
	require_once("../model_manager/indicatorkpi.inc.php");

	$anInstanceModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->GblSurveyModelID);
	chdir($strOriginalWD);

	$descriptKeysCollection = BITAM_SI_DESCRIPT_KEYSCollection::NewInstance($aRepository, $anInstanceModel->ModelID);
	
	$aSurveyInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblSurveyDimID);
	chdir($strOriginalWD);
	$aStartDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartDateDimID);
	chdir($strOriginalWD);
	$anEndDateInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndDateDimID);
	chdir($strOriginalWD);
	$aStartTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblStartTimeDimID);
	chdir($strOriginalWD);
	$anEndTimeInstanceDim = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblEndTimeDimID);
	chdir($strOriginalWD);
	
	//Obtenemos la instancia de la dimension User
	$userDimension = BITAMModelDimension::NewModelDimensionWithDimensionID($aRepository, $surveyInstance->GblSurveyModelID, -1, $surveyInstance->GblUserDimID);
	chdir($strOriginalWD);
	
	//Obtenemos las instancias de los indicadores Duration, Latitud y Longitud
	$durationIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblDurationIndID);
	chdir($strOriginalWD);
	$latitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLatitudeIndID);
	chdir($strOriginalWD);
	$longitudeIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->GblLongitudeIndID);
	chdir($strOriginalWD);
	
	$durationIndField = $durationIndicator->field_name.$durationIndicator->IndicatorID;
	$latitudeIndField = $latitudeIndicator->field_name.$latitudeIndicator->IndicatorID;
	$longitudeIndField = $longitudeIndicator->field_name.$longitudeIndicator->IndicatorID;

	//Obtenemos los campos del INSERT a la tabla de hechos de encuestas globales
	$factTable = $anInstanceModel->nom_tabla;
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, true);
	$surveyDimField = $aSurveyInstanceDim->Dimension->TableName."KEY";
	$userDimField = $userDimension->Dimension->TableName."KEY";
	$startDateDimField = $aStartDateInstanceDim->Dimension->TableName."KEY";
	$endDateDimField = $anEndDateInstanceDim->Dimension->TableName."KEY";
	$startTimeDimField = $aStartTimeInstanceDim->Dimension->TableName."KEY";
	$endTimeDimField = $anEndTimeInstanceDim->Dimension->TableName."KEY";
}

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	$aModel = BITAMModel::NewModelWithModelID($aRepository, $surveyInstance->ModelID);
	chdir($strOriginalWD);
	//Obtenemos instancia del indicador Duration, Latitude y Longitude
	$durIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->DurationIndID);
	$latIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LatitudeIndID);
	$longIndicator = BITAMIndicatorKPI::NewIndicatorWithID($aRepository, $surveyInstance->LongitudeIndID);
	chdir($strOriginalWD);

	//Tablas de Hechos y Paralela
	$tablaHechos = $aModel->nom_tabla;
	$tablaParalela = $surveyInstance->SurveyTable;
	
	//Duracion, Latitude y Longitude
	$fieldDurationInd = $durIndicator->field_name.$durIndicator->IndicatorID;
	$fieldLatitudeInd = $latIndicator->field_name.$latIndicator->IndicatorID;
	$fieldLongitudeInd = $longIndicator->field_name.$longIndicator->IndicatorID;

	
	$sql = "SELECT A.DateID, A.UserID, A.StartTime, A.EndTime, B.".$fieldDurationInd.", B.".$fieldLatitudeInd.", B.".$fieldLongitudeInd." 
			FROM ".$tablaParalela." A, ".$tablaHechos." B WHERE A.FactKey = B.FactKey ORDER BY A.FactKey";
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if(!$aRS)
	{
		echo('Error en consulta de datos de Survey "'.$surveyInstance->SurveyName.'"(ID='.$surveyInstance->SurveyID.'). <br>SQL = '.$sql.'<br>');
		echo(translate("Error accessing")." ".$tablaParalela.", ".$tablaHechos." ".translate("tables").": ".$aRepository->DataADOConnection->ErrorMsg().'<br>');
		continue;
	}
	
	while(!$aRS->EOF)
	{
		$userID = (int)$aRS->fields["userid"];
		
		$currentDate = substr($aRS->fields["dateid"], 0, 10);
		$startDate = $currentDate;
		$endDate = $currentDate;

		$startTime = $aRS->fields["starttime"];
		$endTime = $aRS->fields["endtime"];

		$totalMinutes = (double)$aRS->fields[strtolower($fieldDurationInd)];
		$latitude = (double)$aRS->fields[strtolower($fieldLatitudeInd)];
		$longitude = (double)$aRS->fields[strtolower($fieldLongitudeInd)];

		//Obtenemos el DateKey
		$dateKey = getDateKey($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate);
		//Obtenemos el SurveyKey
		$surveyKey = getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance);
		//Obtenemos el StartDateKey
		$startDateKey = getDateTimeDimKey($aRepository, $aStartDateInstanceDim, $startDate);
		//Obtenemos el EndDateKey
		$endDateKey = getDateTimeDimKey($aRepository, $anEndDateInstanceDim, $endDate);
		//Obtenemos el StartTimeKey
		$startTimeKey = getDateTimeDimKey($aRepository, $aStartTimeInstanceDim, $startTime);
		//Obtenemos el EndTimeKey
		$endTimeKey = getDateTimeDimKey($aRepository, $anEndTimeInstanceDim, $endTime);
		
		//Se procede a insertar dicho registro en el cubo global de encuestas
		$sql = "INSERT INTO ".$factTable." ( ".
		$dateKeyField.
		", ".$surveyDimField.
		", ".$userDimField.
		", ".$startDateDimField.
		", ".$endDateDimField.
		", ".$startTimeDimField.
		", ".$endTimeDimField.
		", ".$durationIndField.
		", ".$latitudeIndField.
		", ".$longitudeIndField.
		" ) VALUES ( ".
		$dateKey.
		",".$surveyKey.
		",".$userID.
		",".$startDateKey.
		",".$endDateKey.
		",".$startTimeKey.
		",".$endTimeKey.
		",".$totalMinutes.
		",".$latitude.
		",".$longitude.
		" )";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			echo(translate("Error accessing")." ".$factTable." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql.'<br>');
		}
		else
		{
			echo('INSERT: '.$sql.'<br>');
		}

		$aRS->MoveNext();
	}
}

function getSurveyDimKey($aRepository, $aSurveyInstanceDim, $surveyInstance)
{
	$tableName = $aSurveyInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	$fieldKey = $aSurveyInstanceDim->Dimension->FieldKey;
	$fieldDesc = $aSurveyInstanceDim->Dimension->FieldDescription;
	
	$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$surveyInstance->SurveyID;
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$sql = "INSERT INTO ".$tableName." ( ".$fieldKey.", ".$fieldDesc." ) VALUES ( ".$surveyInstance->SurveyID.", ".$aRepository->DataADOConnection->Quote($surveyInstance->SurveyName).")";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldKey." = ".$surveyInstance->SurveyID;
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

function getDateTimeDimKey($aRepository, $anInstanceDim, $value)
{
	$tableName = $anInstanceDim->Dimension->TableName;
	$fieldSurrogateKey = $tableName."KEY";
	$fieldDesc = $anInstanceDim->Dimension->FieldDescription;
	
	$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($value);
	
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	//Considerando que es un solo valor el que se esta procesando
	if (!$aRS->EOF)
	{	
		//Ya existe ese valor en la tabla de la dimension de Surveys, se retorna su clave
		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	else 
	{
		//Se debe insertar dicho valor en la tabla de dimension de Surveys
		$sql = "INSERT INTO ".$tableName." ( ".$fieldDesc." ) VALUES ( ".$aRepository->DataADOConnection->Quote($value).")";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//Volvemos a consultar el valueKey del nuevo registro insertado en dicha dimension
		$sql = "SELECT ".$fieldSurrogateKey." FROM ".$tableName." WHERE ".$fieldDesc." = ".$aRepository->DataADOConnection->Quote($value);
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die( translate("Error accessing")." ".$tableName." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}

		$valueKey =  $aRS->fields[strtolower($fieldSurrogateKey)];
	}
	
	return $valueKey;
}

function getDateKey($aRepository, $anInstanceModel, $descriptKeysCollection, $currentDate)
{
	$periodDims = BITAMDimensionCollection::NewInstancePeriodDims($aRepository, $anInstanceModel->ModelID, false);
	$numPeriodDims=count($periodDims->Collection);
	
	$formatedDate = $currentDate." 00:00:00";
	$dimPeriodo = $anInstanceModel->dim_periodo;
	$fechaField = $anInstanceModel->fecha;
	$dateKeyField = getJoinField($dimPeriodo, "", $descriptKeysCollection, false);
	
	$sql = "SELECT ".$dateKeyField." FROM ".$dimPeriodo." WHERE ".$fechaField." = ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
	$aRS = $aRepository->DataADOConnection->Execute($sql);
	
	if ($aRS === false)
	{
		die( translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}
	
	if (!$aRS->EOF)
	{	
		//Ya existe esa fecha en la tabla especificada en dim_periodo, se retorna su clave
		$dateKey =  $aRS->fields[strtolower($dateKeyField)];
	}
	else 
	{
		$dateKey = substr($currentDate, 0, 10);
		$dateKey = str_replace("-", "", $dateKey);
		
		$sql = "INSERT INTO ".$dimPeriodo." ( ".$dateKeyField.", ".$fechaField;
					
		for($i=0; $i<$numPeriodDims; $i++)
		{
			$sql.= ", ".$periodDims->Collection[$i]->nom_fisicok_bd;
		}
		
		$sql.= " ) VALUES ( ".$dateKey.", ".$aRepository->DataADOConnection->DBTimeStamp($currentDate);
		
		for($i=0; $i<$numPeriodDims; $i++)
		{	
			$formatedDateField = formatADateTime($formatedDate, $periodDims->Collection[$i]->fmt_periodo);
			$formatedDateField = $aRepository->DataADOConnection->Quote($formatedDateField);
			$sql.= ", ".$formatedDateField;
		}
		
		$sql.=" )";
		
		if($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die( translate("Error accessing")." ".$dimPeriodo." ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	return $dateKey;
}
?>