/* Definición de métodos para interfaz o procesos generales que se salen de las clases implementadas en eSurveyServiceMod.js, pero que son
invocados por esas clases
*/

//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Eventos del App */
//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Cuando el dispositivo está listo para ser utilizado, invoca al método que muestra el login o realiza el autologin, además de preparar
ciertos datos o eliminar información ya no necesaria
*/

window.onorientationchange = function () {
	//Ajusta el footer para que siempre permanezca en la parte inferior sin importar el contenido de la pantalla
	//JAPR 2015-08-10: Corregido un bug, el footer quedaba ligeramente sobre la última porción de la ventana ya que no estaba considerando su propio height
	//JAPR 2016-09-19: Corregido un bug, la implementación original corregía el problema, pero en IOs solamente provocaba que al mostrar el teclado se moviera el footer dentro
	//del área visible sobre el teclado, ya que internamente lanzaba el evento resize el cual invoca a orientationchange o algo así, por lo que este código provocaba el desajuste
	//del footer hasta que se cerrara el teclado. Ahora se dejará fijo el footer mediante JQuery directamente 
	$.each($('[data-role="footer"]'), function(idx) {
		var objFooter = $(this);
		if (objFooter && objFooter.length && objFooter.height) {
			var objPage = undefined;
			if (objFooter[0].id) {
				var objPage = $('#' + objFooter[0].id.replace('_footer', ''));
			}
			
			//Si el contenido de la página es mas grande del área visible, el footer se debe recorrer su tamaño hacia abajo para que no corte el contenido
			if (objPage && objPage.length && objPage.height && objPage.height() > $(window).height()) {
				//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
				//Corregido el footer fijo en la parte inferior de cada página
				//objFooter.css('bottom', '-' + objFooter.height() + 'px')
				if(objSettings.webMode){
					objFooter.css({position : "fixed",width: "100%",bottom:"0px"})
				}
			}
			else {
				//Si el footer si cabe correctamente porque el tamaño de la página no es mas grande que el área visible, entonces sólo se coloca hasta abajo
				objFooter.css('bottom', '0px')
			}
		}
	})
	//JAPR	
	//$('[data-role="footer"]').css('bottom', '0px');
	
	//Ajusta la imagen para que se utilice la de la orientación a la que se acaba de cambiar
	var orientationScreen = window.orientation;
	var divWidth = String(objSettings.backgroundWidth);
	var divHeight = String(objSettings.backgroundHeight);
	if (!$.isNumeric(divHeight.replace('%', '')) && !$.isNumeric(divHeight.replace('px', ''))) {
		divHeight = window.innerHeight/2 + "px";
	}
	else {
		divHeight = divHeight.replace('px', '') + 'px';
	}
	if (!$.isNumeric(divWidth.replace('%', '')) && !$.isNumeric(divWidth.replace('px', ''))) {
		divWidth = window.innerWidth + "px"; // /2 + "px";
	}
	else {
		divWidth = divWidth.replace('px', '') + 'px';
	}
	
	//JAPR 2015-08-07: Corregido el uso de la imagen de fondo ya que no se estaba aplicando
	var strBackGroundImage = String(objSettings.backgroundImage);
	if (strBackGroundImage) {
		//Si hay imagen de fondo, utiliza directamente esa imagen, por ahora en ambas orientaciones, aunque la imagen se graba localmente como background.jpg, en modo Web
		//se usa la ruta relativa que ya trae directamente esta propiedad
		if (!objSettings.webMode) {
			var strProjectFolder = '';
			if (objApp.useProjectForBGIMG && objSettings.project) {
				strProjectFolder = objSettings.project + "/";
			}
			
			strBackGroundImage = objSettings.prefixTrans + "/images/" + strProjectFolder + "background.jpg" + "?dte=" + getFullDate(undefined, true);
		}
	} else {
		//OMMC 2016-05-26: Cambiado el banner del menú de formas.
		//Si no hay imagen de fondo, pone la default propia del App
		if (objSettings && objSettings.webMode) {
			strBackGroundImage = "images/formsMenu_banner.jpg";
		} else {
			//Se deja por si se quisiera hacer algo especial para móviles debido a tablets / celulares u orientaciones
			//Estaba así anteriormente donde validaba orientationScreen para ajustar la imagen adecuada. Ahora es la misma imagen.
			strBackGroundImage = "images/formsMenu_banner.jpg";
		}
	}
	
	//JAPR 2015-08-07: Removido el max-height para que crezca al máximo posible horizontalmente
	//$('#backDiv').css('max-height', divHeight);
	$('#backDiv').css('max-width', divWidth);
	$('#backDiv').attr('src', strBackGroundImage);
	
	//JAPR 2016-02-10: Modificada la lista de formas para permitir Layouts personalizados y diferentes tamaños por cada forma (#FSMUZC)
	//Cuando ocurre el cambio de orientación del dispositivo, se debe invocar de nuevo a la reconstrucción del Masonry, ya que con el cambio de ancho se pudieron haber reajustado los
	//elementos, así que hay que revisar de nuevo si es posible centrar el componente. Debido a que hay un evento cuando el Masonry termina de hacer el ajuste, se debe forzar a la 
	//destrucción completa del Masonry para no agregar eventos, esto a su vez regresará el div de las formas al width=100% y se podrá recalcular el tamaño final en base al left + width
	//de cada elemento individualmente
	//En modo web este evento entra en cada Resize que se realiza, mientras que en dispositivo sólo debe entrar cuando en realidad hay un cambio de orientación, el comportamiento es igual
	if (objApp && objApp.currentPageID == "mainpage") {
		objApp.buildNavigation();
		objApp.refreshFormsList(undefined, true);
	}
	//JAPR
}

$.urlParam = function(name){
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results)
    { 
        return 0; 
    }
    return results[1] || 0;
}
function goToApp(){
	if(objSettings.isiOS()){
		return;
	}

	if(fromURL == true && globalData != ''){
		var cadenaTmp="gncapp://"+objSettings.user+"_AWSEP_"+objSettings.password;
		navigator.app.loadUrl(cadenaTmp, {openExternal: true});
	}
}

//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
//Funcion para darle resize dinamico al login dependiendo del tamaño de screen//
function screensize(){
	if( $(window).width() <= 650 || $(window).height() <= 1000){
		$("#loginpage_body").css({"padding-top":"5px", "margin-left":"18%","margin-top":"18%"});
	 }
	if($(window).width() >= 649 || $(window).height() >= 999){
		$("#loginpage_body").css({"padding-top":"30px", "margin-left":"14%","margin-top":"14%"});
	 }
	if(	$(window).width() <= 650 || $(window).height() <= 1000){
		}
}

//activar menu solo por primera vez (se usa la variable global
function activateMenu(){
	try {
		if (fmenu==true) {
			//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
			//OMMC 2016-07-08: Agregados el loader y el emptypage para que el menú no les hiciera wrap.
			jPM = $.jPanelMenu({
				animated : true,
				excludedPanelContent : "style, script, #emptyPage, .ui-loader"
			});
			//JAPR
			jPM.on();
			fmenu = false;
		}
    }
    catch(e) {
		alert(e);
    }
}

function onDeviceReady() {
	try {
		var invokeString = $.urlParam('invokeString');
	}
	catch(e){
	}

	Debugger.register('onDeviceReady ');
    
	//ESFile.deleteFile('log.txt',function(){},function(){});
	//Conchita  Se movio este listener para onDeviceReady ya que no lo agregaba bien antes donde se encontraba
    try{
    	if(invokeString){
        	handleOpenURL(invokeString);
    	}
    }
    catch(e){
    }
	
    document.addEventListener('online', onOnline, false);
    document.addEventListener('offline', onOffline, false);
    document.addEventListener('resume', onResume, false);
	document.addEventListener("backbutton", onBackKeyDown, false);
	window.addEventListener("orientationchange", onUpdateOrientation);
    if (objSettings.device.phonegap == '0.9.5') {
    	//Forza a indicar que se encuentra Online el App
        onOnline();
        
        initForms();
    }
	if (true) { //objSettings.isAndroid()
		//fix conchita
		if(navigator.onLine==true){
			onOnline();
		}
		else {
			onOffline();
		}
	}
}

//funciones para push notifications Conchita 2013-05-14
function GCM_Event(e)
{
	switch( e.event ) {
		case 'registered':
		    // the definition of the e variable is json return defined in GCMReceiver.java
		    // In my case on registered I have EVENT and REGID defined
		    gApp.gcmregid = e.regid;
		    if ( gApp.gcmregid.length > 0 )
		    {
				Debugger.register("El id:" + e.regid);
		    }
		    
		    //JAPR 2014-04-07: Actualiza el regID en el server, ya que cuando se lanza el evento la primera vez, es probable que ya se hubiera
		    //lanzado el login así que ya grabó un RegID vacio en el servicio, y no se puede condicionar el login a si logra o no obtener este dato
		    //No le envía función de callback para simplemente hacer la petición del servicio, que es el punto donde se envía el RegID al server
		    if (!objApp.regIDSent && objApp.logged) {
		    	objApp.checkUserServiceFolder(undefined, undefined, false);
		    	objApp.regIDSent = true;
		    }
		    break;
		
		case 'message':
		    // the definition of the e variable is json return defined in GCMIntentService.java
		    // In my case on registered I have EVENT, MSG and MSGCNT defined
			
		    // You will NOT receive any messages unless you build a HOST server application to send
		    // Messages to you, This is just here to show you how it might work
			break;
	  	case 'error':
			break;
		default:
	    	
	    	break;
  	}
}

function GCM_Success(e)
{
	Debugger.register("registro sucess");
}

function GCM_Fail(e)
{
	Debugger.register("error registro: " + e.msg);
}

//conchita funciones para IOS para push notification
//inicio
// result contains any message sent from the plugin call
function successHandler (result) {
    Debugger.register('result = ' + result);
}
// result contains any error description text returned from the plugin call
function errorHandler (error) {
    Debugger.register('error = ' + error);
}
function tokenHandler (result) {
    // Your iOS push server needs to know the token before it can push to this device
    // here is where you might want to send it the token for later use.
    gApp.gcmregid = result;
    if ( gApp.gcmregid.length > 0 )
    {
		Debugger.register("El id:" + e.regid);
		
	    //JAPR 2014-04-07: Actualiza el regID en el server, ya que cuando se lanza el evento la primera vez, es probable que ya se hubiera
	    //lanzado el login así que ya grabó un RegID vacio en el servicio, y no se puede condicionar el login a si logra o no obtener este dato
	    //No le envía función de callback para simplemente hacer la petición del servicio, que es el punto donde se envía el RegID al server
	    if (!objApp.regIDSent && objApp.logged) {
	    	objApp.checkUserServiceFolder(undefined, undefined, false);
	    	objApp.regIDSent = true;
	    }
    }
}
function onNotificationAPN (event) {
    if ( event.alert )
    {
        navigator.notification.alert(event.alert);
    }
	
    if ( event.sound )
    {
        var snd = new Media(event.sound);
        snd.play();
    }
	
    if ( event.badge )
    {
        pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
    }
}
//fin

//funciones para push
function checkConnectionType() {
	if (objSettings.webMode) {
		return;
	}
	
	try {
        var networkState = navigator.network.connection.type;
	    var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = '2G connection';
        states[Connection.CELL_3G]  = '3G connection';
        states[Connection.CELL_4G]  = '4G connection';
        states[Connection.NONE]     = 'No network connection';
		
        return states[networkState];
    }
    catch(e) {
      	return "Can not check connection type";
	}
}

function initForms() {
    //Inicializa propiedades y objetos que son exclusivos de móviles y que se deben hacer hasta que el dispositivo está "listo"
    objApp.initMobile();
    
	//JAPR 2014-04-28: Modificado el manejo de log para permitir grabar en localStorage en caso de caerse el App
	//En este punto ya asignó la ruta donde se grabarán archivos, así que invoca al método que graba el log de depuración si es que había alguna
	//entrada almacenada en el localStorage
	try {
		Debugger.saveLocalStorageLog();
	} catch(e) {
		//No registra este error
	}
    //JAPR
    
    //Invoca a la ventana inicial pidiendo el Auto-login si es que hay alguno registrado
	objApp.login(true);
}

/*Funcion para atrapar cuando venga de una url (osea que se invoca desde la otra app*/

function handleOpenURL(url){
	Debugger.register('handleOpenURL');
	globalData = url;
	fromURL = true;
}

/* Evento que se lanza (a partir de Phonegap 1.2 nada mas) cuando el dispositivo vuelve a estar online. Simplemente actualiza la variable
de Status
*/
function onOnline() {
	Debugger.register('onOnline');
	if (objSettings) {
		objSettings.online = true;
	}
    if (gblPhonegapStarted) {
        gblPhonegapStarted = false;
        initForms();
    }
}

/* Evento que se lanza (a partir de Phonegap 1.2 nada mas) cuando el dispositivo vuelve a estar offline. En este caso además de actualizar la 
variable de Status, cancela cualquier ajax que se estuviera ejecutando en este instante
*/
function onOffline() {
	Debugger.register('onOffline');
	if (objSettings) {
		objSettings.online = false;
	}
    if (gblPhonegapStarted) {
        gblPhonegapStarted = false;
        initForms();
    }
	
	//JAPRWarning: Habilitar
	/*
    if (lastXHR != undefined) {
        lastXHR.abort();
    }
    */
}

function onResume() {
	Debugger.register('onResume');

	//OMMC 2016-10-20: Cambiado el tiempo del timeout de 1 segundo a nada.
	setTimeout(function() {
		if (objApp && objApp.logged && appPush) {
			//OMMCWarning: Esta parte se programó de este modo debido al cambio de la versión del plugin v1.6.0 a v1.8.2 y la inclusión de iOS 10
			//Esta porción del código no debió quedar así ya que de que el plugin y operativo sirvieran de manera apropiada el evento se resolvería
			// en el .on('notification') del JS principal que es como estaba para iOS 9 y Android, el plugin hacía diferencia entre el foreground y background
			// por lo que al aceptar la notificación todo se procesaba de manera normal, mandando o no a la ventana de notficaciones en caso de existir una captura.

			//OMMC 2016-10-26: Agregada la obtención de mensajes sin leer
			//Se obtiene el badge actual del app y los mensajes sin leer
			if (objSettings && objSettings.isiOS()) {
				objApp.manageNotifications('getBadge', undefined, function(){
					objApp.deviceUnreadCount = objApp.managePushNotifications('getUnreadMessages', {pushObject: objePush});
					//Para este punto no sería posible que estas 2 propiedades no existieran, se dejan en cero para evitar ir al server de manera innecesaria.
					if (objApp.badgeNumber === undefined) {
						objApp.badgeNumber = 0;
					}

					if (objApp.badgeNumber != objApp.deviceUnreadCount) {
						//OMMC 2016-10-20: Agregado changePage para que al momento de reanudar forms e ir por las notificaciones al server, no mande a la pantalla de push
						//OMMC 2016-10-26: Ahora hará diferencia si está o no en una captura, para hacer el cambio de pantalla, adicional al hecho de revisar el badge.
						if (selSurvey) {
							objApp.managePushNotifications('requestMessages', {changePage: false});
						} else {
							objApp.managePushNotifications('requestMessages', {changePage: true});	
						}
					}
				});
			} else if (objSettings && objSettings.isAndroid()) {
				objApp.deviceUnreadCount = objApp.managePushNotifications('getUnreadMessages', {pushObject: objePush});	

				if (selSurvey) {
					objApp.managePushNotifications('requestMessages', {changePage: false});
				} else {
					objApp.managePushNotifications('requestMessages', {changePage: true});	
				}
			}
			
		}
	}, 0)
}

/*  Evento que se lanza (sólo para Android) al forzar el cierre del App para remover la memoria usada por la misma
*/
function onBackKeyDown()
{
	if (selSurvey) {
		objSectionBack = selSurvey.currentCapture.section
	   if (objSectionBack.number == 1) {
		  objApp.backToFormsList();
	   } else {
	   	//OMMC 2016-06-07: Agregada la validación para negar el back con el botón del teléfono cuando los botones de navegación están ocultos.
	   	if (!selSurvey.hideNavButtons) {
	   		objApp.changeSection(-1);
	   	}
	   }
	}
	else {
		Debugger.register('onBackKeyDown');
		var strMsg = translate("Do you want to exit the application?");
		navigator.notification.confirm(strMsg, function(answer) {
			if (answer == 1) {
				shutDownApp();
			}
		});
	}
	/*Debugger.register('onBackKeyDown');
	var strMsg = translate("Do you want to exit the application?");
	if (objSettings && objSettings.webMode) {
		var answer = confirm(strMsg);
		if (answer) {
			shutDownApp();
		}
	}
	else {
		Debugger.register('onBackKeyDown');
		var strMsg = translate("Do you want to exit the application?");
		navigator.notification.confirm(strMsg, function(answer) {
			if (answer == 1) {
				shutDownApp();
			}
		});
	}*/
    
}

/* Evento que se lanza al cambiar la orientación de la aplicación para ajustes en ciertas ventanas */
function onUpdateOrientation() {
	
}

/* Funciones de callback cuando no son especificadas funciones reales para ciertos procesos */
function noCallbackSuccess() {
	Debugger.register('noCallbackSuccess');
}

function noCallbackError(anXMLHttpRequest, aTextStatus, anErrorThrown) {
	Debugger.register('noCallbackError');
	Debugger.message('arguments.length: ' + arguments.length);
	if (arguments.length >= 1) {
		var anEvt = undefined;
		if (arguments.length == 1) {
			anEvt = arguments[0];
		}
		if (anEvt && anEvt.errCode) {
			Debugger.message('ErrCode: ' + anEvt.errCode + ', ErrDesc: ' + anEvt.errDesc);
		}
		else {
			//Debe ser un error por un Callback
			if (anXMLHttpRequest && anXMLHttpRequest.state) {
				//JAPR 2014-08-11: Modificado el reporte de errores para hacerlos mas amigables
				//Esta función no necesariamente viene de un Ajax, pero si tiene esta estructura de reporte entonces se considerará que si lo hizo
				var objErrDesc = objAjaxDwnld.getFriendlyErrorDesc(anXMLHttpRequest, aTextStatus, anErrorThrown);
				//JAPR 2014-08-15: Agregado el detalle de error exacto para el log del usuario
				var strDetail = translate("There was an error in the request to the Web service") + ': (' + aTextStatus + ') ' + ((anXMLHttpRequest.state)?anXMLHttpRequest.state():'') + ' ' + anErrorThrown;
				//JAPR 2016-11-07: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
				throwError({code:2, desc:objErrDesc.errdesc, detail:strDetail, fnName:objApp.lastRequestMngr.lastFunction, type:UserLogEvents.uleSynchronization, callSupportMsg:objErrDesc.callSupportMsg});
			}
		}
	}
}

//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Funciones para realizar ciertos procesos en el App */
//****************************************************************************************************************************************
//****************************************************************************************************************************************
//Calcular memoria disponible iOS
function calculateFreeMemIOS() {
	//document.location.href = "myapp://fake";
    return '0';
	$('<iframe style="display:none;" id="myFrame" name="myFrame" src="myapp://fake">').appendTo('body');
	var val1 = document.getElementById('test01').value;
	$('#myFrame').remove();
	return val1;
}

function cancelSpinningWheel() {
	Debugger.register('cancelSpinningWheel');
	//Conchita 2015-03-04 comentado codigo de carlos espinoza
    //2015.02.24 JCEM #NAJ96E
	//SpinningWheel.destroy();
}

//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
//JAPR 2016-03-03: Se había comentado esta función, pero hasta la fecha no hay indicación acerca de no validar por @ en los nombres de cuentas, salvo para GeoControl como ya está
//validado aquí, así que se regresará la función como estaba originalmente
/* Verifica si el nombre de usuario introducido es válido según las propias reglas del producto del que se trate */
function checkLoginData(inputtext) {
	//MAPR 2017-05-05: Se desactivo el botón para aplicaciones iOS por un problema reportado por el equipo de QA de Apple con redes IPv6
    if(gsProductName != "Geocontrol" && !objSettings.isiOS())
    {
		//JAPR 2016-03-03: Rediseñados los estilos para dejar look mas parecido a IOs9
		//Removido estilo de gradiente directo
		//JAPR 2016-03-03: Integrada nueva versión de JQ1.11.3/JQM1.45
		//Cambiada la clase ui-disabled por ui-state-disabled ya que desde JQM 1.4 está deprecado (se modificó el tema default jquery.ui.theme.css para este cambio)
		if ($('#myUserKPA').val() == '' || $('#myPassKPA').val() == '' || (gsByUserMail && ($('#myUserKPA').val().indexOf('@') == -1 || $('#myUserKPA').val().split('@')[1] == ''))){
				//$('#btnloginsurvey').css('background', '-webkit-gradient(linear, 0 0, 0 60%, from(black),  to(' + myBlack + '))');
				$('#btnloginsurvey').addClass('ui-state-disabled');
			} else {
				//$('#btnloginsurvey').css('background', '-webkit-gradient(linear, 0 0, 0 60%, from(black),  to(' + myBlack + '))');
				$('#btnloginsurvey').removeClass('ui-state-disabled');
			}
			//JAPR
		}
    $('#btnloginsurvey .ui-btn-text').html('Sign in');
}

function checkEnter(key, anElement) {
	//JAPREliminar: 
	/*
	if(signinXHR != null) {
		signinXHR.abort();
		return;
	}
	*/
    if(key == 13) {
        $(anElement).blur();
        if (objApp && objApp.doSignIn) {
			objApp.doSignIn();
        }
    }
}

function checkUserName(inputtext) {
	var strID = '#'+inputtext.id;
	var strValue = $(strID).val();
    if(gsProductName != "Geocontrol" && gsByUserMail)
    {
        if (strValue != '' && strValue.indexOf('@') == -1) {
            if (objSettings && objSettings.webMode) {
                alert('Not a valid mail');
            }
            else {
                navigator.notification.alert('Not a valid mail', null, 'Invalid Mail');
            }
        }
    }
}

//Terminó de procesar una fecha así que asigna el valor en la pregunta correspondiente
function doneSpinningWheel() {
	Debugger.register('doneSpinningWheel');
	if (!selSurvey || !selSurvey.currentCapture || !selSurvey.currentCapture.question) {
		return;
	}
	
	var objQuestion = selSurvey.currentCapture.question;
    var newDate;
    var newyear;
    var newmonth;
    var newday;
    var swFilterDate;
	var swvalues = new Array();
	
	SpinningWheel.getSelectedValues();
	swvalues = SpinningWheel.getSelectedValues();
	if (objQuestion.type == Type.date || objQuestion.type == Type.action) {
	    if(objQuestion.format) {
	    	var formato = objQuestion.format.split('/');
	    }
    	else {
	    	var formato = msDefaultDateFmt.split('/');
    	}
	    for (var k = 0; k < 3; k++) {
	        if (formato[k].indexOf("d") != -1) {
	            newday = swvalues.values[k];
	        }
	        if (formato[k].indexOf("M") != -1) {
	            newmonth = swvalues.keys[k] < 10 ? "0" + swvalues.keys[k] : swvalues.keys[k];
	        }
	        if (formato[k].indexOf("y") != -1) {
	            newyear = swvalues.values[k];
	        }
	    }
		/*
		newyear = swvalues.values[0];
	    newmonth = swvalues.keys[1] < 10 ? "0" + swvalues.keys[1] : swvalues.keys[1];
	    newday = swvalues.values[2];
	    */
	    
		var datetovalidate = '';
	    datetovalidate = newmonth + '/' + newday + '/' + newyear;
	    //validar aqui la fecha
	    if (!isDate(datetovalidate)) {
			throwError(-1, translate("Please enter a valid date"), translate("Invalid Date"), 'doneSpinningWheel');
			//JAPRWarning: Falta implementar este método para forzar el foco en la pregunta indicada
			//JAPREliminar: Realmente es necesario esto? la pregunta es la activa de todas formas, y el foco no es tan importante pues como sea
			//no actualizó el valor así que se quedó con el anterior o vacio de todas formas
			if (objQuestion.setFocus) {
				objQuestion.setFocus();
			}
			return;
	    }
	    
	    //Actualiza el valor de la pregunta
	    swFilterDate = newyear + "-" + newmonth + "-" + newday;
	    
	    //JAPRWarning: Validar como resolver para asignar el campo daysDueDate, por lo pronto invocar al setValue
	    if(objQuestion.type == Type.action) {
	    	objQuestion.answer.setProperty('daysDueDate', swFilterDate);
		} else {
	    	objQuestion.answer.setValue(swFilterDate);
		}
	}
	else {
	    //tipo time
	    var time = '';
	    time = swvalues.values[0] + ':' + swvalues.values[1];// + ' ' + swvalues.values[2];
	    objQuestion.answer.setValue(time);
	}
	
	//JAPR 2015-09-04: Agregada la actualización de valores default y fórmulas a tipos de preguntas que no lo contemplaban (#ASX4C6)
	objQuestion.updateValue();
	//JAPR
	objQuestion.refreshValue();
	//Conchita 2015-03-04 comentado codigo de carlos espinoza
    //2015.02.24 JCEM #NAJ96E
    //SpinningWheel.destroy();
}

//Obtiene el número total de aplicaciones corriendo a la vez en el móvil (-1 quiere decir que no fué posible determinarlo)
function getAppsRunning() {	
	var intAppsRunning = -1;
	
	if (objSettings) {
		if (objSettings.isAndroid()) {
		try{
	        var intAppsRunning = window.MyCls.checkRunningProcesses();
		}
	   	catch(e){
	   	Debugger.message('En getAppsRunning ' +e);
	   	}
		}
	}
	
    return intAppsRunning;
}

//Calcular memoria disponible
function getAvailableMemory() {
	var intAvailMem = -1;
	
	try {
		if (objSettings) {
			if(objSettings.isAndroid()) {
	            var intAvailMem = window.MyCls.checkMemoryInformation();
		    }
		    else if (objSettings.isiOS()) {
		        try{
		            //si es iOS
		            var intAvailMem = calculateFreeMemIOS();
		        }
		        catch(e)
		        {
		            var intAvailMem = -1;
		        }
		    }
		}
	} catch(e) {
		Debugger.message('Unable to calculate the available memory: ' + e);
	}
	
	return intAvailMem;
}

//AAL 06/05/2015: Función agregada para leer código de barras. Empleada en preguntas tipo BarCode.
function getBarCode(aQuestionID) {
	Debugger.register('getBarCode');
	
	cordova.plugins.barcodeScanner.scan(
		function BarCodeSuccess(CodeResult) {
			var objQuestion = selSurvey.questions[aQuestionID];
			if(!objQuestion || CodeResult.cancelled)
				return;
			objQuestion.answer.setValue(CodeResult.text);
			objQuestion.refreshValue();
			
			//JAPR 2019-08-21: Agregados los eventos de fin de captura de preguntas multi-media o que dependen del dispositivo (#DB1WXK)
			if ( objQuestion.onAfterCapture ) {
				objQuestion.onAfterCapture(CodeResult.text);
			}
			
			//JAPR 2016-02-08: Corregido un bug, la pregunta tipo BarCode en dispositivo no estaba lanzando los eventos que encadenaban la actualización de otras preguntas (#B59I3K)
			//Dado a que updateValue lee el valor del componente, primero ejecutará el refreshValue para actualizar el valor asignado arriba, así updateValue ya tomará el nuevo
			//valor para encadenar el refresh al resto de las preguntas que se requieran
			objQuestion.updateValue();
			//JAPR
			//selSurvey.currentCapture.question.answer.setValue(CodeResult.text);
			//objApp.BarCodeTook(aQuestionID, CodeResult);
			/*MAPR 2017-02-03: Se modificó setFocusToNextQuestion para aceptar la variable noEvent y poder mandar un Evento indefinido, así como un callback para poder seguir con 
			el procedimiento de captura del siguiente código en caso de que la pregunta fuese de tipo Barcode.*/
			var intRecordNumber = objQuestion.section.recordNum;
			selSurvey.setFocusToNextQuestion(undefined, true, function() {
				var objCurrQuestion = selSurvey.currentCapture.question;
				//La condición pregunta para casos del manejo de secciones tabla y número de registro, se espera que siempre vaya definido objCurrQuestion, pero se agrega dentro de la confición
				if(objCurrQuestion && objCurrQuestion.id != aQuestionID || (objCurrQuestion.section.type == SectionType.Inline && objCurrQuestion.section.displayMode == SectionDisplayMode.sdspInline && objCurrQuestion.section.recordNum != intRecordNumber)){
					if (objCurrQuestion.type == Type.photo){
						getPicture(objCurrQuestion.allowGallery)
					} else if (objCurrQuestion.type == Type.barcode){
						getBarCode(objCurrQuestion.id);
					}

				}
			});	
		},
		function BarCodeFail(message) {
			throwError(-1, translate("Barcode scanner is not supported in your device") + ': ' + message, translate("Barcode scanner error"), 'getBarCode');
		}
	);
}
//AAL

//Toma una foto desde un dispositivo móvil y devuelve su contenido en B64
function getPicture_old() {
	Debugger.register('getPicture');
	var pictureSource = navigator.camera.PictureSourceType;
	var destinationType = navigator.camera.DestinationType;
	if (objSettings.isiPad() || objSettings.isTabletAndroid()) {
		var photoOptions = { quality: 40 };
	} else {
		var photoOptions = { quality: 10 };
	}
	
	//Modifica la configuración de calidad y tamaño de las fotos (con vacio usará los valores default del propio App, excepto la calidad la
	//cual siempre se especifica)
	if (objSettings.photoQuality) {
		var intCamQuality = parseInt(objSettings.photoQuality);
		if (intCamQuality >= 0 && intCamQuality <= 100) {
			photoOptions = { quality: intCamQuality };
		}
	}
	
	if (objSettings.photoWidth && parseInt(objSettings.photoWidth) > 0) {
		$.extend(photoOptions, {targetWidth:parseInt(objSettings.photoWidth)});
	}

	if (objSettings.photoHeight && parseInt(objSettings.photoHeight) > 0) {
		$.extend(photoOptions, {targetHeight:parseInt(objSettings.photoHeight)});
	}
	
	$.extend(photoOptions, {destinationType: Camera.DestinationType.DATA_URL});
	
	navigator.camera.getPicture(function onPhotoDataSuccess(imageData) {
			objApp.photoTook(imageData);
		},
		function onFail(message) {
			if (message != 'Camera cancelled.' && message != 'no image selected' && message != '3') {
				throwError(-1, translate("Capture photo is not supported in your device") + ': ' + message, translate("Camera error"), 'getPicture');
			}              
		}, photoOptions);
}

//Toma una foto desde un dispositivo móvil y devuelve su contenido en B64
//OMMC 2016-01-11: Cambiada la función de getPicture para que pueda soportar imágenes de la biblioteca.
//Debido a que no se podía reproducir el menú de opciones nativo, se despliega un confirm.
function getPicture(allowGallery) {
	Debugger.register('getPicture');

	//OMMCWarning: LRoux comentó la posibilidad de deshabilitar la elección de galería, se deja estática. Habría que mandarla por parámetro.
	//var allowGallery = true;

	//Inicio del llenado del objeto de las propiedades.
	var photoOptions = new Object();
	if (objSettings.isiPad() || objSettings.isTabletAndroid()) {
		$.extend(photoOptions, {quality: 40});
	} else {
		$.extend(photoOptions, {quality: 10});
	}

	if (objSettings.photoQuality) {
		var intCamQuality = parseInt(objSettings.photoQuality);
		if (intCamQuality >= 0 && intCamQuality <= 100) {
			$.extend(photoOptions, {quality: intCamQuality});
		}
	}
	
	if (objSettings.photoWidth && parseInt(objSettings.photoWidth) > 0) {
		$.extend(photoOptions, {targetWidth: parseInt(objSettings.photoWidth)});
	}

	if (objSettings.photoHeight && parseInt(objSettings.photoHeight) > 0) {
		$.extend(photoOptions, {targetHeight: parseInt(objSettings.photoHeight)});
	}
	$.extend(photoOptions, {correctOrientation: true});
	//Fin del llenado del objeto de propiedades

	if(allowGallery){
		navigator.notification.confirm(translate('Choose an option: '), function(answer){
			switch(answer){
				//Cámara
				case 1:
					$.extend(photoOptions, {sourceType: Camera.PictureSourceType.CAMERA});
					$.extend(photoOptions, {destinationType: Camera.DestinationType.DATA_URL});
				break;
				//Galería
				case 2:
					$.extend(photoOptions, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
					$.extend(photoOptions, {destinationType: Camera.DestinationType.DATA_URL});
				break;
				//Cancelar
				case 3:
				default:
					return;
				break;
			}

			navigator.camera.getPicture(function onPhotoDataSuccess(imageData) {
				objApp.photoTook(imageData);
			},
			function onFail(message) {
				if (message != 'Camera cancelled.' && message != 'no image selected' && message != '3' && message != 'Selection cancelled.') {
					throwError(-1, translate("Capture photo is not supported in your device") + ': ' + message, translate("Camera error"), 'getPicture');
				}              
			}, photoOptions);
		},' ',[translate('Camera'),translate('Gallery'),translate('Cancel')]);
	}else{
		//Si se encuentra bloqueado el acceso a la galería, la cámara se abrirá de manera normal para tomar la foto, esto es el getPicture() original

		$.extend(photoOptions, {sourceType: Camera.PictureSourceType.CAMERA});
		$.extend(photoOptions, {destinationType: Camera.DestinationType.DATA_URL});

		navigator.camera.getPicture(function onPhotoDataSuccess(imageData) {
			objApp.photoTook(imageData);
		},
		function onFail(message) {
			if (message != 'Camera cancelled.' && message != 'no image selected' && message != '3' && message != 'has no access to assets') {
				throwError(-1, translate("Capture photo is not supported in your device") + ': ' + message, translate("Camera error"), 'getPicture');
			}              
		}, photoOptions);
	}
}

//Conchita funciones grabado de video para movil
function recordVideo() {
	Debugger.register('recordVideo');
	
	var options = { limit: 1 }; //opcion para que solo grabe 1 video y regrese a la app (solo para android, en IOS lo ignora)
	try {
		navigator.device.capture.captureVideo(captureVideoSuccess, captureVideoError, options);
	}
	catch(e){
		Debugger.message('Error in recordVideo: ' + e);
	}
}

//Renombrada la función ya que no sólo graba sino recupera de la librería.
//OMMC 2016-01-11: Cambiada la función de recordVideo para que pueda soportar videos de la biblioteca.
//Debido a que no se podía reproducir el menú de opciones nativo, se despliega un confirm.
function getVideo(allowGallery){
	Debugger.register('getVideo');

	//OMMCWarning: LRoux comentó la posibilidad de deshabilitar la elección de galería, se deja estática. Habría que mandarla por parámetro desde el getHTML().
	//var allowGallery = false;
	
	var videoOptions = new Object();
	$.extend(videoOptions, { limit: 1 });

	if(allowGallery){
		navigator.notification.confirm(translate('Choose an option: '), function(answer){
			switch(answer){
				//Seleccionó cámara
				case 1:
					try {
						navigator.device.capture.captureVideo(captureVideoSuccess, captureVideoError, videoOptions);
					}
					catch(e){
						Debugger.message('Error in getVideo-camera: ' + e);
					}
				break;
				//Seleccionó galería
				case 2:
					$.extend(videoOptions, {mediaType: Camera.MediaType.VIDEO});
					$.extend(videoOptions, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
					$.extend(videoOptions, {destinationType: Camera.DestinationType.FILE_URI});
                    
                    try{
                        navigator.camera.getPicture(captureVideoSuccess, captureVideoError, videoOptions);
                    }catch(e){
                        Debugger.message('Error in getVideo-gallery: ' + e);
                    }
					
				break;
				//Seleccionó cancelar
				case 3:
				default:
					return;
				break;
			}
		},' ',[translate('Camera'),translate('Gallery'),translate('Cancel')]);
	}else{
		//Si no está habilitada la galería, sólo se abre la cámara de manera habitual para la captura de video.
		//Esta porción de código es el recordVideo() original.
		try {
			navigator.device.capture.captureVideo(captureVideoSuccess, captureVideoError, videoOptions);
		}
		catch(e){
			Debugger.message('Error in recordVideo: ' + e);
		}
	}
}

function captureVideoSuccess(mediaFiles) {
	// recibe los archivos en un array
	Debugger.register('captureVideoSuccess ');
    var isFile;
	
    //Sólo viene la ruta de un archivo
	//JAPR 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//JAPRWarning: Esta asignación de la propiedad document es temporal, durante la programación original lo hicieron para poder heredar el valor a los callbacks, pero estuvo mal
	//ya que la propiedad document sólo debería contener la referencia del nombre de archivo y no un path nativo del dispositivo, mas adelante en los callbacks se debería hacer
	//el ajuste aunque existe la posibilidad que estuviera llegando al server con un path local, en cuyo caso debe ser simplemente ignorado y conservar el nombre de archivo nada mas
    if(typeof mediaFiles == "string"){
    	//mediaFiles (string), cuando es un archivo de galería sólo viene la ruta donde se encuentra por lo que es como partir el fullPath.
    	selSurvey.currentCapture.question.answer.document =	mediaFiles;
    	isFile = true;    	
    }else{
    	//mediaFiles (array), en su [0] es un objeto que contiene: end, start, fullPath, lastModifiedDate, localURL, name, size, type.
    	//Quiere decir que viene de la captura de un video, por lo que es un objeto
    	selSurvey.currentCapture.question.answer.document =	mediaFiles[0].fullPath;
    	isFile = false;
    }
	
	try {
		Debugger.message("RUTA: " + selSurvey.currentCapture.question.answer.document);
		//tenemos que mover el archivo al directorio de forms y despues reemplazarlo en el campo document
		var strFILEprefix = "";
		//OMMCWarning: Esto tiene razón de ser en iOS??
		if (objSettings.isiOS()) {
			var strFILEprefix = "file:///";
		}
		var strSurveyDate = selSurvey.surveyDate;
		var strSurveyHour = selSurvey.surveyHour;
		strSurveyDate = strSurveyDate.replace(/-/gi, '');
		strSurveyHour = strSurveyHour.replace(/:/gi, '');

		//OMMC 2016-04-18: Agregado para las preguntas tipo documento
		if (selSurvey.currentCapture.question.type == Type.document) {
			var strFileName = 'document_' + objSettings.user + '_' + strSurveyDate + '_' + strSurveyHour + '_' + selSurvey.id + '_' + selSurvey.currentCapture.question.id + '_' + selSurvey.currentCapture.recordNum + '_video';
		} else {
			var strFileName = 'video_' + objSettings.user + '_' + strSurveyDate + '_' + strSurveyHour + '_' + selSurvey.id + '_' + selSurvey.currentCapture.question.id + '_' + selSurvey.currentCapture.recordNum;
		}
		
		//Se revisa de nuevo si es un objeto o si sólo viene la ruta del archivo.
		//En caso de ser un archivo, sólo viene la ruta absoluta de su ubicación mientras que cuando es de cámara se tiene que accesar a la propiedad fullPath
		//En cualquiera de los 2 casos el resultado de pieces será algo así:
		/* 
			pieces[0] = ""
			pieces[1] = "private"
			pieces[2] = "var"
			pieces[3] = "mobile"
			pieces[4] = "container"
			pieces[5] = "Data"
			pieces[6] = "Application"
			pieces[7] = "AA742349S-2342-2SDF-WELK3-324LJ324LKJ2"
			pieces[8] = "tmp"
			pieces[9] = "24352345345__2345LKJH345-2345-4TE4-43AER34A.MOV"
		*/
		if(isFile){
			var pieces = mediaFiles.split("/");
		}else{
			var pieces = mediaFiles[0].fullPath.split("/");	
		}
        
		if (objSettings.isiOS()) {
			//OMMC 2016-10-28: Agregada la validación y fix para videos en iOS 10, había un problema con el sistema de carpetas, se deja documentado el código para futuros cambios, también se agregó el reporte de errores del plugin con Debugger msgs ya que eso no se estaba reportando.
			//Sitio: https://github.com/apache/cordova-plugin-file		(List of Error Codes and meanings)
			var iOSVersion = objSettings.getOSVersion();
			if (iOSVersion >= 10) {
				window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(tmpFileSystem) {
					//En base al sistema de archivos temporal, buscamos el archivo con su nombre y extensión
					tmpFileSystem.root.getFile(pieces[pieces.length - 1], {
						create: true,
						exclusive: false
					}, function(file){
						//Una vez obtenida la instancia del archivo, generamos un sistema de archivos que apunta la carpeta del App
						//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
						//MAPR 2017-08-11: El parametro del requestFileSystem no es del tipo esperado, espera un númerico. Se cambio por resolveLocalFileSystemURL que recibe una ruta
						//window.requestFileSystem(LocalFileSystem.PERSISTENT + "/" + ESFile.getRootFilesPath(), 0, function(perFileSystem){
						window.resolveLocalFileSystemURL(objSettings.prefixTrans + "/" + ESFile.getRootFilesPath(), function(perFileSystem){
							perFileSystem.getDirectory("", {
								create: true,
								exclusive: false
							}, function(destination){
								//El archivo se mueve a la carpeta de destino con el nuevo nombre.
								file.moveTo(destination, strFileName + '.' + objSettings.videoExt, successMoveVideo, failMove);
							}, function(moveErr){
								Debugger.message('Error moving the file, code: ' + moveErr);
							});
						}, function(destErr){
							Debugger.message('Error generating perFileSystem, code: ' + destErr);
						});
					}, function(fileErr){
						Debugger.message('Error getting the file, code: ' + fileErr);	
					});
				}, function(sysErr) {
					Debugger.message('Error generating tmpFileSystem, code: ' + sysErr);
				});
			} else {
				window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(fileSystem) {
					fileSystem.root.getDirectory((isFile ? '' : pieces[pieces.length - 2]), {
	                    create: true,
	                    exclusive: false
	                }, function(tmpEntry) {
	                	window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(fileSystemDestination) {
							fileSystemDestination.root.getFile(tmpEntry.fullPath + "/" + (isFile ? pieces[pieces.length - 1] : mediaFiles[0].name), {
			                    create: true,
			                    exclusive: false
			                }, function(file) {
								//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
					            //MAPR 2017-08-11: El parametro del requestFileSystem no es del tipo esperado, espera un númerico. Se cambio por resolveLocalFileSystemURL que recibe una ruta
					            //window.requestFileSystem(LocalFileSystem.PERSISTENT + "/" + ESFile.getRootFilesPath(), 0, function(fileSystemDestination) {
					            window.resolveLocalFileSystemURL(objSettings.prefixTrans + "/" + ESFile.getRootFilesPath(), function(fileSystemDestination) {
									fileSystemDestination.getDirectory("", {
					                    create: true,
					                    exclusive: false
					                }, function(Destination) {
					                	file.moveTo(Destination,strFileName + '.' + objSettings.videoExt, successMoveVideo, failMove);
					                }, function(){alert("error del getFile");}); //of getFile
					            }, function(){alert("error del fileSystem");});
			                }, function(){alert("error del getFile");}); //of getFile
			            }, function(){alert("error del fileSystem");}); //of requestFileSystem
	                }, function(){alert("error del getFile");}); //of getFile
	            }, function(){alert("error del fileSystem");}); //of requestFileSystem
			}
		} else {
			//OMMC 2016-04-11: Agregado para archivos desde memoria en Android
			//Continua el desarrollo en la V6.02000 debido a que se tienen que actualizar librerìas del FileSystem para que funcione
			//Nunca entrará por aquí hasta que se haga la actualización del plugin.
			if(isFile) {
				//resolveNativePath convierte una URI en una URL para que Android pueda entenderla, el success devuelve esa URL
				//Se debe agregar el plugin para llamar a resolveNativePath que convierte "content:///" a "file:///"
				//Después se procesa el archivo de la misma manera, anteriormente no se podía hacer esto ya que Android sólo
				//consideraba archivos en la memoria interna y no el SDcard
				window.FilePath.resolveNativePath(selSurvey.currentCapture.question.answer.document, 
					function(resultSuccess){
						window.resolveLocalFileSystemURL(resultSuccess, function(entry) {
						//Conchita 2015-05-04 se cambio window.fullpath por cordova.file.applicationDirectory
							//Debugger.message("Funciona el cambio de URL - File prefix: " + strFILEprefix + " Prefixtrans: " + objSettings.prefixTrans + " File name: " + selSurvey.currentCapture.question.answer.document);
							//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
							window.resolveLocalFileSystemURL(strFILEprefix + objSettings.prefixTrans + "/" + ESFile.getRootFilesPath(), function(destination) {
								Debugger.message('En el destination : ' + destination);
								if (destination && destination.fullPath) {
									Debugger.message('En el destination fullPath isFile: ' + destination.fullPath);
								}
								entry.moveTo(destination,strFileName + '.' + objSettings.videoExt, successMoveVideo, failMove);
							},function fail1(err){
								   Debugger.message('error en resolveLocalFileSystemURL strFILEprefix+ objSettings.prefixTrans : ' + JSON.stringify(err));
							   }
							);
						}, function fail1(err){
							//Debugger.message("Falla el cambio de URL - File prefix: " + strFILEprefix + " Prefixtrans: " + objSettings.prefixTrans + " File name: " + selSurvey.currentCapture.question.answer.document + " File name parsed: " + algo);
						   	Debugger.message('error en resolveLocalFileSystemURL strFILEprefix + selSurvey.currentCapture.question.answer.document : ' + JSON.stringify(err));
						});
				}, function(resultError){
						Debugger.message(resultError);
				});
			} else {
				//Este es el comportamiento default de la cámara para Android, donde se resuelve un archivo temporal y se copia a la carpeta de eForms del dispositivo.
				window.resolveLocalFileSystemURL(strFILEprefix + selSurvey.currentCapture.question.answer.document, function(entry) {
				 //Conchita 2015-05-04 se cambio window.fullpath por cordova.file.applicationDirectory
					//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
					window.resolveLocalFileSystemURL(strFILEprefix + objSettings.prefixTrans + "/" + ESFile.getRootFilesPath(), function(destination) {
						Debugger.message('En el destination : ' + destination);
						if (destination && destination.fullPath) {
							Debugger.message('En el destination fullPath: ' + destination.fullPath);
						}
						entry.moveTo(destination,strFileName + '.' + objSettings.videoExt, successMoveVideo, failMove);
					},function fail1(err){
						   Debugger.message('error en resolveLocalFileSystemURL strFILEprefix+ objSettings.prefixTrans : ' + JSON.stringify(err));
					   }
					);
				}, function fail1(err){
				   	Debugger.message('error en resolveLocalFileSystemURL strFILEprefix + selSurvey.currentCapture.question.answer.document : ' + JSON.stringify(err));
				});
			}
		}
		
	}
	catch(e) {
		Debugger.message('Error in captureVideoSuccess ' + e);
	}
}

function captureVideoError(error) {
    Debugger.message('Error capturing video, code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
	selSurvey.currentCapture.question.answer.document = ''; 
	//como hubo error se borra la referencia
}

function successMoveVideo(entry) {
    Debugger.message('se movio ok video: ' + entry.fullPath);
	
	//JAPR 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//JAPRWarning: Esta asignación de la propiedad document es la definitiva, aquí cualquier ruta local debió haber sido removida y dejar sólo el nombre de archivo correspondiente,
	//en el peor de los casos podría venir únicamente la ruta de la subcarpeta de proyecto a partir de la versión que introdujo esa funcionalidad, pero en dicha situación al llegar
	//al server debería poder ser removida o causará problemas
    selSurvey.currentCapture.question.answer.document = entry.fullPath; 
    var strFieldName = selSurvey.currentCapture.question.fieldName;

    //OMMC 2016-05-23: Para los documentos sólo se despliega el nombre.
    if (selSurvey.currentCapture.question.type == Type.document) {
    	selSurvey.currentCapture.question.answer.documentType = 'video';
    	//OMMC 2016-05-20: Se limpia el nombre del archivo antes de agregar algo
		if ($('#'+ strFieldName +' .documentFileName').length > 0 && $('#'+ objQuestion.fieldName +' .documentFileName').html() != "") {
			$('#'+ strFieldName +' .documentFileName').html("");
		}
		//Se agrega el nombre del archivo
		if ($('#'+ strFieldName +' .documentFileName').length > 0) {
			$('#'+ strFieldName +' .documentFileName').html("<span>" + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document) + "</span>");
		}
		
    } else {
    	//JAPR 2016-03-03: Integrada nueva versión de JQ1.11.3/JQM1.45
		//Cambiada la clase ui-disabled por ui-state-disabled ya que desde JQM 1.4 está deprecado (se modificó el tema default jquery.ui.theme.css para este cambio)
	    $("#"+strFieldName+'_button').removeClass('ui-state-disabled');
		//JAPR
		
	    //OMMC 2016-03-02: Se elimina correctamente el reproductor antes de agregar otro. Causaba errores en drafts
	    if($("#"+strFieldName+"_area .videoPlay")){
	    	$("#"+strFieldName+"_area .videoPlay").remove();	
	    }
		
		if(objSettings.isiOS())
		{
			//OMMC 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	        $("#"+strFieldName+"_video").append("<video class=\"videoPlay\" width='"+"200px'"+" controls = 'true' webkit-playsinline='true' ><source src='"+ objSettings.prefixTrans + "/" + ESFile.getRootFilesPath() + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document) +"'"+" type="+"'"+"video/quicktime"+"'"+ "></video>");
			
		} else {
			//OMMC 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			$("#"+strFieldName+"_video").append("<video class=\"videoPlay\" width='"+"200px'"+" controls><source src='"+ objSettings.prefixTrans + "/" + ESFile.getRootFilesPath() + "/" + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document) +"'"+" type="+"'"+"video/mp4"+"'"+ "></video>");
		}
    }
	
	//JAPR 2019-08-21: Agregados los eventos de fin de captura de preguntas multi-media o que dependen del dispositivo (#DB1WXK)
	if ( objQuestion.onAfterCapture ) {
		objQuestion.onAfterCapture(objQuestion.answer.document);
	}
	//JAPR
}

//OMMC 2018-11-22: Agregado para el rediseño de pregunta documento
function moveDocumentSuccess(entry) {
	Debugger.message('Document move success: ' + entry.fullPath);

	var objQuestion = selSurvey.currentCapture.question;

	var strFieldName = objQuestion.getFieldName(selSurvey.currentCapture.recordNum);
	objQuestion.answers[selSurvey.currentCapture.recordNum].document = entry.fullPath;

	var extension = entry.fullPath.split(".")[entry.fullPath.split(".").length - 1];
    objQuestion.answers[selSurvey.currentCapture.recordNum].documentType = extension.toUpperCase();

	//OMMC 2016-05-20: Se limpia el nombre del archivo antes de agregar algo
	if ($('#'+ strFieldName + '_area .documentFileName').length > 0 && $('#'+ objQuestion.fieldName +' .documentFileName').html() != "") {
		$('#'+ strFieldName + '_area .documentFileName').html("");
	}
	//Se agrega el nombre del archivo
	if ($('#'+ strFieldName + '_area .documentFileName').length > 0) {
		$('#'+ strFieldName + '_area .documentFileName').html("<span>" + GetFileNameFromFilePath(objQuestion.answers[selSurvey.currentCapture.recordNum].document) + "</span>");
	}
	
	//JAPR 2019-08-21: Agregados los eventos de fin de captura de preguntas multi-media o que dependen del dispositivo (#DB1WXK)
	if ( objQuestion.onAfterCapture ) {
		objQuestion.onAfterCapture(objQuestion.answers[selSurvey.currentCapture.recordNum].document);
	}
	//JAPR
}

//OMMC 2018-11-22: Agregado para el rediseño de pregunta documento
function moveDocumentError(err) {
	Debugger.message('Error moving the document, code: ' + err.code + '\n' + 'message: ' + err.message + '\n');
	selSurvey.currentCapture.question.answers[selSurvey.currentCapture.recordNum].document = '';
	selSurvey.currentCapture.question.answers[selSurvey.currentCapture.recordNum].documentType = '';
	selSurvey.currentCapture.question.answers[selSurvey.currentCapture.recordNum].filePath = undefined;
}

//Conchita funciones grabado de audio para movil
function recordAudio() {
	//deshabilitar btn de grabar audio
	 var field = selSurvey.currentCapture.question.fieldName;
	//JAPR 2016-03-03: Integrada nueva versión de JQ1.11.3/JQM1.45
	//Cambiada la clase ui-disabled por ui-state-disabled ya que desde JQM 1.4 está deprecado (se modificó el tema default jquery.ui.theme.css para este cambio)
     $("#"+field+'_audiobutton').addClass('ui-state-disabled');
	 //JAPR
     //$("#"+field+'_audiobutton').text(translate("Recording"));
     $("#"+field+'_audiobutton .ui-btn-text').html(translate('Recording'));
	//generamos el nombre del archivo
	var strSurveyDate = selSurvey.surveyDate;
	var strSurveyHour = selSurvey.surveyHour;
	strSurveyDate = strSurveyDate.replace(/-/gi, '');
	strSurveyHour = strSurveyHour.replace(/:/gi, '');
	var strFileName = 'audio_' + objSettings.user + '_' + strSurveyDate + '_' + strSurveyHour + '_' + selSurvey.id + '_' + selSurvey.currentCapture.question.id + '_' + selSurvey.currentCapture.recordNum;
	if(objSettings.isAndroid()){
		var src = strFileName + ".mp3"; //"myrecording.mp3"; //objSettings.prefixFile +
   	}
   	else {
		var  src = strFileName + ".wav"; 
   	}
   	
	//JAPR 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	//JAPRWarning: Esta asignación de la propiedad document es temporal, durante la programación original lo hicieron para poder heredar el valor a los callbacks, pero estuvo mal
	//ya que la propiedad document sólo debería contener la referencia del nombre de archivo y no un path nativo del dispositivo, mas adelante en los callbacks se debería hacer
	//el ajuste aunque existe la posibilidad que estuviera llegando al server con un path local, en cuyo caso debe ser simplemente ignorado y conservar el nombre de archivo nada mas
	//En este caso por como se graban los audios, aquí solo se asigna el nombre del archivo sin referencia a paths locales, pero sigue siendo un nombre temporal sólo para Android, puesto
	//que mas adelante se va a sobreescribir por el nombre final
	selSurvey.currentCapture.question.answer.document = src;
	if (objSettings.isiOS()) {
		/*objAudio = new Object();
		var captureError = function(e) {
			console.log('captureError' ,e);
		}

		var captureSuccess = function(e) {
			console.log('captureSuccess');console.dir(e);
			objAudio.file = e[0].localURL;
			objAudio.filePath = e[0].fullPath;
		}

		navigator.device.capture.captureAudio(captureSuccess,captureError,{duration:10});*/
		//si es ios se hace de otra manera
		//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//MAPR 2017-08-11: El parametro del requestFileSystem no es del tipo esperado, espera un númerico.
		//window.requestFileSystem(LocalFileSystem.PERSISTENT + "/" + ESFile.getRootFilesPath(), 0, function(fileSystem) {
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
				fileSystem.root.getFile(src, {
                    create: true,
                    exclusive: false
                }, function(fileEntry) {
                    mediaVar = new Media(fileEntry.fullPath , function(e) {mediaVar.release();}, onAudioError, null); //of new Media
                    onAudioSuccess(mediaVar);
                }, onAudioError); //of getFile
            }, onAudioError); //of requestFileSystem
		//fin si es ios
	}
    // Record audio
   	
   	if (objSettings.isAndroid()) {
		//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		Debugger.message('ENTRO AL RECORD AUDIO '+ objSettings.prefixTrans + ESFile.getRootFilesPath() + src);
	   	try {
			//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
	   		var mediaRec = new Media(objSettings.prefixTrans + ESFile.getRootFilesPath() + src, onAudioSuccess, onAudioError);
	        mediaRec.startRecord();
	        // Stop recording after 10 sec
	        var recTime = 0;
	        var recInterval = setInterval(function() {
	            recTime = recTime + 1;
	        	//setAudioPosition(recTime + " sec");
	            if (recTime >= 10) {
		            clearInterval(recInterval);
		            mediaRec.stopRecord();
	            }
	        }, 1000);
	    }
	    catch(e){
	    	Debugger.message('Error in recordAudio: ' + e);
	    }
    }
}

// onSuccess Callback
//
function onAudioSuccess(mediaRec) {
    Debugger.message('onAudioSuccess');
	
	if (objSettings.isiOS()) {
	    mediaRec.startRecord();
		// Stop recording after 10 sec
	  	setTimeout(function() {
			mediaRec.stopRecord();
			window.requestFileSystem(LocalFileSystem.TEMPORARY, 0, function(fileSystemDestination) {
				fileSystemDestination.root.getFile(selSurvey.currentCapture.question.answer.document, {
	                create: true,
	                exclusive: false
	            }, function(file) {
					//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
					//MAPR 2017-08-11: El parametro del requestFileSystem no es del tipo esperado, espera un númerico. Se cambio por resolveLocalFileSystemURL que recibe una ruta
					//window.requestFileSystem(LocalFileSystem.PERSISTENT + "/" + ESFile.getRootFilesPath(), 0, function(fileSystemDestination) {
		            window.resolveLocalFileSystemURL(objSettings.prefixTrans + "/" + ESFile.getRootFilesPath(), function(fileSystemDestination) {
						fileSystemDestination.getDirectory("", {
		                    create: true,
		                    exclusive: false
		                }, function(Destination) {
		                	file.moveTo(Destination, selSurvey.currentCapture.question.answer.document, successMoveAudio, failMove);
		                }, function(){alert("error del getFile");}); //of getFile
		            }, function(){alert("error del fileSystem");});
	            }, function(){alert("error del getFile");}); //of getFile
	        	}, function(){alert("error del fileSystem");}
		    );
	    }, 10000);

	}
	else {
		try {
		    Debugger.message('en el try');
		    //tenemos que mover el archivo al directorio de forms y despues reemplazarlo en el campo document
			//conchita 2015-05-04 se cambio a cordova.file.applicationDirectory
			//JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
			//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
			//window.resolveLocalFileSystemURL(strFILEprefix + selSurvey.currentCapture.question.answer.document, function(entry) {
			window.resolveLocalFileSystemURL(objSettings.prefixTrans + "/" + ESFile.getRootFilesPath() + selSurvey.currentCapture.question.answer.document,
				successMoveAudio, function failResolve1(err) {
					Debugger.message('fail resolve1: ' + + JSON.stringify(err));
				});
		}
		catch(e) {
			Debugger.message('Error in onAudioSuccess ' + e);
		}
	}
}

// onError Callback 
function onAudioError(error) {
    Debugger.message('code: ' + error.code + '\n' + 'message: ' + error.message + '\n');
	selSurvey.currentCapture.question.answer.document = ''; 
	//como hubo error se borra la referencia
}

function successMoveAudio(entry) {
    Debugger.message('se movio ok audio: ' + entry.fullPath);
	
	var strFieldName = selSurvey.currentCapture.question.fieldName;
	//JAPR 2016-03-03: Integrada nueva versión de JQ1.11.3/JQM1.45
	//Cambiada la clase ui-disabled por ui-state-disabled ya que desde JQM 1.4 está deprecado (se modificó el tema default jquery.ui.theme.css para este cambio)
	$("#"+field+'_audiobutton').removeClass('ui-state-disabled');
	//JAPR
	// $("#"+field+'_audiobutton').text(translate("Capture Audio"));
	$("#"+field+'_audiobutton .ui-btn-text').html("<img width='28px' height='28px'  src='images/audio.png'>");
	//si es android hay que poner al campo el fullpath
	if (!objSettings.isiOS()) {
		//JAPR 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//JAPRWarning: Esta asignación de la propiedad document es la definitiva, aquí cualquier ruta local debió haber sido removida y dejar sólo el nombre de archivo correspondiente,
		//en el peor de los casos podría venir únicamente la ruta de la subcarpeta de proyecto a partir de la versión que introdujo esa funcionalidad, pero en dicha situación al llegar
		//al server debería poder ser removida o causará problemas
    	selSurvey.currentCapture.question.answer.document = entry.fullPath; 
	}
	//OMMC 2016-03-02: Se elimina correctamente el reproductor antes de agregar otro. Causaba errores en drafts
	if ($("#"+strFieldName+"_area .audioPlay")) {
		$("#"+strFieldName+"_area .audioPlay").remove();	
	}
	if (objSettings.isiOS()) {
	    //JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
		var pref = objSettings.prefixTrans + "/";
	    //agregado nuevo para mini reproductor
		var field = selSurvey.currentCapture.question.fieldName+"_audio";
		//JAPR 2016-03-15: Agregada la variable/constante para generación de previews (sólo puede estar activada desde la consola para generación de HTML que será reemplazado como preview
		var strPreviewSel = "";
		//OMMC 2016-12-08: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//Aquí se supone que como el parámetro entry ya viene con la subcarpeta desde el callback anterior, el fullpath que es lo que está en el document de esta pregunta está bien, ahora... aquí hay un currentCapture que no debería de estar, y anteriormente estaba que sólo entraba SI Y SÓLO SI estaba en modo de diseño.
		//Le agregué la negación, sino no sale el botón de play.
		if (!gbGeneratePreview) {
			strPreviewSel = " href=\"javascript:selSurvey.changeQuestion(" + selSurvey.currentCapture.question.number + "); playAudio('" + selSurvey.currentCapture.question.answer.document + "');\" ";
		}
	    var tmp = "\ <a class=\"audioPlay\" id=\"" + field + "_play\" data-theme=\"b\" data-role=\"button\" "+strPreviewSel+">"+translate("Play")+"</a>";
		//JAPR
	    $("#"+strFieldName+"_audio").append(tmp);
		
		//var strHTML = "<audio controls><source src="+"'"+pref+selSurvey.currentCapture.question.answer.document +"'"+" type="+"'"+"audio/mpeg"+"'"+ "/></audio>";
		/*var strHTML = '<audio controls><source src="'+pref+selSurvey.currentCapture.question.answer.document +'" type="audio/wav"/></audio>';
		$("#"+field).append(strHTML);*/
	}
	else {
		//JAPR 2016-12-07: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//JAPR 2016-12-08: Modificado el método de reproducción de audio para hacerlo igual que IOs, ya que se detectó que el HTML5 audio dejó de funcionar en Android (#E10U00)
		/*Debugger.message('audio path: ' + objSettings.prefixTrans + ESFile.getRootFilesPath() + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document));
		$("#"+strFieldName+"_audio").append("<audio class=\"audioPlay\" controls=\"controls\"><source src='"+ objSettings.prefixTrans + ESFile.getRootFilesPath() + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document) +"'"+" type="+"'"+"audio/mpeg"+"'"+ "></audio>");*/
		var field = selSurvey.currentCapture.question.fieldName+"_audio";
		var strPreviewSel = "";
		if (!gbGeneratePreview) {
			strPreviewSel = " href=\"javascript:selSurvey.changeQuestion(" + selSurvey.currentCapture.question.number + "); playAudio('" + objSettings.prefixTrans + "/" + ESFile.getRootFilesPath() + GetFileNameFromFilePath(selSurvey.currentCapture.question.answer.document) + "');\" ";
		}
	    var tmp = "\ <a class=\"audioPlay\" id=\"" + field + "_play\" data-theme=\"b\" data-role=\"button\" "+strPreviewSel+">"+translate("Play")+"</a>";
		//JAPR
	    $("#"+strFieldName+"_audio").append(tmp);
	}
	
	//JAPR 2019-08-21: Agregados los eventos de fin de captura de preguntas multi-media o que dependen del dispositivo (#DB1WXK)
	if ( selSurvey.currentCapture.question.onAfterCapture ) {
		selSurvey.currentCapture.question.onAfterCapture(selSurvey.currentCapture.question.answer.document);
	}
	//JAPR
}

//funcion para reproducir audio en IOS (no funciona el reproductor de html5)
//se cambio a cordova.file.applicationDirectory
function playAudio(audio) {
	var audioRoute = '';
	//JAPR 2016-12-08: Modificado el método de reproducción de audio para hacerlo igual que IOs, ya que se detectó que el HTML5 audio dejó de funcionar en Android (#E10U00)
	if (objSettings.isiOS()) {
		audioRoute = "documents://"+audio;
	}
	else {
		audioRoute = audio;
	}
	//JAPR
	
	var mediaRec = new Media(audioRoute,function(e) {
			mediaRec.release();
            Debugger.register("playAudio():Audio Success");
        },
        // error callback
        function(err) {
            Debugger.register("playAudio():Audio Error: "+err);
    });
	mediaRec.play();
}
function failMove(error) {
    Debugger.message('fail al mover: ' + error.code);
    selSurvey.currentCapture.question.answer.document = ''; 
}

//OMMC 2016-01-13: Cambiada la función de getDocument para tomar achivos tipo foto o video de la biblioteca
function getDocument() {
	Debugger.register('getDocument');
	
	var optionChosen = 0;
	var photoOptions = new Object();
	var videoOptions = new Object();

	navigator.notification.confirm(translate('Choose an option: '), function(answer){
		switch(answer){
			//Foto
			case 1:
				//Inicio del llenado del objeto de las propiedades.
				if (objSettings.isiPad() || objSettings.isTabletAndroid()) {
					$.extend(photoOptions, {quality: 40});
				} else {
					$.extend(photoOptions, {quality: 10});
				}

				if (objSettings.photoQuality) {
					var intCamQuality = parseInt(objSettings.photoQuality);
					if (intCamQuality >= 0 && intCamQuality <= 100) {
						$.extend(photoOptions, {quality: intCamQuality});
					}
				}
				
				if (objSettings.photoWidth && parseInt(objSettings.photoWidth) > 0) {
					$.extend(photoOptions, {targetWidth: parseInt(objSettings.photoWidth)});
				}

				if (objSettings.photoHeight && parseInt(objSettings.photoHeight) > 0) {
					$.extend(photoOptions, {targetHeight: parseInt(objSettings.photoHeight)});
				}
				//Fin del llenado del objeto de propiedades

				$.extend(photoOptions, {mediaType: Camera.MediaType.PICTURE});
				$.extend(photoOptions, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
				$.extend(photoOptions, {destinationType: Camera.DestinationType.DATA_URL});

				optionChosen = answer;
			break;
			//Video
			case 2:
				$.extend(videoOptions, { limit: 1 });

				$.extend(videoOptions, {mediaType: Camera.MediaType.VIDEO});
				$.extend(videoOptions, {sourceType: Camera.PictureSourceType.PHOTOLIBRARY});
				$.extend(videoOptions, {destinationType: Camera.DestinationType.DATA_URL});

				optionChosen = answer;
			break;
			//Cancelar
			case 3:
			default:
				return;
			break;
		}

		switch(optionChosen){
			case 1:
				navigator.camera.getPicture(function onPhotoDataSuccess(imageData) {
					objApp.photoTook(imageData);
				},
				function onFail(message) {
					if (message != 'Camera cancelled.' && message != 'no image selected' && message != '3' && message != 'Selection cancelled.') {
						throwError(-1, translate("Capture photo is not supported in your device") + ': ' + message, translate("Camera error"), 'getPicture');
					}              
				}, photoOptions);
			break;
			case 2:
				navigator.camera.getPicture(captureVideoSuccess, captureVideoError, videoOptions);
			break;
			default:
			break;
		}
	},' ',['Photo','Video',translate('Cancel')]);
}

//Cargar mapa
function mapsLoaded() {
	Debugger.register('mapsLoaded');
	setDebugHeaderText('mapsLoaded');
	lastGoogleAPIRequestTime = null;
	
	//JAPR 2014-09-01: Agregada la validación para impedir que se intente cargar múltiples veces el API de Google durante la primera corrida
    if (xmlGoogleAPITimeOut !== null) {
    	try {
    		clearTimeout(xmlGoogleAPITimeOut);
    	} catch (e) {
    		//Ignora este error
    	}
    	xmlGoogleAPITimeOut = null;
    }
    //JAPR
	
	//JAPR 2014-05-09: Agregada la identificación de la posición a partir del API de JScript
	//Marcará el API de Google como cargada, ya que es lo mismo que se tiene que invocar para pedir la dirección a partir del GPS
	if (objApp.position) {
		objApp.position.googleAPILoaded = true;
	}
	//JAPR
	objApp.loadPage('googlemaps');
}

/*Conchita 2012-11-29 funcion obsoleta para el nuevo plugin
function loadMaps() {
	google.load("maps", "2", {"callback" : mapsLoaded});
}
*/

//Invoca a la ventana de Google maps con la posición actual del GPS
function googleMapLastPosition() {
	//JAPR 2014-09-01: Agregada la validación para impedir que se intente cargar múltiples veces el API de Google durante la primera corrida
	//JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM) (OMMC, MAPR)
	if (!objApp.position.googleAPILoaded) {
		objApp.loadGoogleMaps('mapsLoaded');
	}
	else {
		//Si ya está cargado, genera directamente el mapa
		mapsLoaded();
	}
	//JAPR
}

/* Verifica si el device está o no offline y ejecuta las funciones de callback especificadas de acuerdo a el status (en el caso de webMode
simplemente invoca a la función directa ya que ahí no podría detectarse el offline, cuando se realice el Ajax este fallará y directamente en el
callback de error se aplicará el mismo criterio que en la función de error de esta validación)
*/
function makeAjaxProcess(success, error) {
	Debugger.register('makeAjaxProcess');
	
	if (objSettings && objSettings.webMode) {
		//En webMode es un llamado directo a success
		//Debugger.message('checking online for web');
		if (objSettings.isOnline()) {
	    	success();
		}
		else {
			error();
		}
	}
	else {
		//En un móvil se tiene que verificar si está o no usando el phoneGap que permite la validación asíncrona de status online
	    if(objSettings && objSettings.device.phonegap == '0.9.5') {
			//Debugger.message('checking reachable');
	    	//Con este phoneGap la validación no era asíncrona, así que se verifica de esta manera y invocará al success o error en el propio
	    	//callback de esta validación
	        navigator.network.isReachable('kpionline.com', function reachableCallback(reachability) {
	        	Debugger.register('reachableCallback');
	        	
	        	//Como ya se identificó que si está online, sobreescribe esta variable
	        	objSettings.online = true;
	        	
	            var networkState = reachability.code || reachability;
	            var states = {};
	            states[NetworkStatus.NOT_REACHABLE] = 'No network connection';
	            states[NetworkStatus.REACHABLE_VIA_CARRIER_DATA_NETWORK] = 'Carrier data connection';
	            states[NetworkStatus.REACHABLE_VIA_WIFI_NETWORK] = 'WiFi connection';
	            if(states[networkState] != 'No network connection') {
	                success();
	            } else {
	                error();
	            }
	        }, function() {});
	    } else {
	    	//En este caso hay una validación para el status de online asíncrona, por lo que se pregunta directamente a la variable que la contiene
			//Debugger.message('checking online for movil if not phonegap 0.9.5');
	        if(objSettings && objSettings.isOnline()) {
				//Debugger.message('onOnline success');
	            success();
	        } else {
				//Debugger.message('onOnline fail');
	            error();
	        }
	    }
	}
}

/* Invoca al proceso que actualiza las definiciones de todos los objetos, eliminando aquello ya no utilizado (no hace una sincronización de
las capturas)
*/
function refreshFromServer(tmpSyncAndBack) { //conchita cambio dabdo
	Debugger.register('refreshFromServer tmpSyncAndBack = ' + tmpSyncAndBack);
	
	selSurvey = undefined;
	selAgenda = undefined;
	if(tmpSyncAndBack){ //conchita cambio dabdo
		objApp.loadPage('mainpage');
		return;
	}
	
	//JAPR 2012-09-18: Validado que si se encuentra offline se ignore la parte del Ajax y se trabaje solo con las definiciones en disco
	//Ahora al intentar refrescar definiciones desde el server, dado a que internamente getLocalFiles verifica la propiedad objSettings.online
	//para determinar si debe o no ejecutar los Ajaxs, se tiene que hacer una verificación previa de disponibilidad en este punto antes de
	//invocar a getLocalFiles, ya que de lo contrario los Ajax no se ejecutarían
	makeAjaxProcess (
		function() {
			//JAPR 2013-02-21: Agregado el método para refrescar exclusivamente la ruta del servicio justo antes de cada petición al server
			//Se invocará primero al método que actualiza la ruta a usar y posteriormente como callback se invocará al proceso específico que ya
			//usará la nueva ruta obtenida
			//JAPR 2014-08-26: Validación para verificar si existe aún el archivo de definición de alguno de los objetos
			//Se invocará a otra función distinta de callback para tratar de evitar un error de lectura de archivos mientras se escribían nuevas
			//definiciones, revisar el comentario para entender la razón
			objApp.checkUserServiceFolder('objApp.getLocalFilesAfterFolderFiles');
			//JAPR
		},
		function() {objApp.offlineErrorDialog('refreshFromServer');}
	);
}

//JAPRDescontinuada en V6, ya no se usa el Spinning wheel para preguntas fecha
//Muestra el componente de Spin para fechas
function setSpinningWheel(aQuestion) {
    try {
    
        if(gblBuildingSpinner) {
            return;
        }
        
        gblBuildingSpinner = true;
        
	Debugger.register('setSpinningWheel');
	if (!aQuestion || !aQuestion.answer) {
		return;
	}
	
	var strDate = '';
	if (aQuestion.answer) {
		if(aQuestion.type == Type.action) {
			if (aQuestion.answer.daysDueDate !== undefined) {
				//Sólo se asigna este dato si no venía un default del server, así que debe ser una fecha en este punto
				if (aQuestion.answer.daysDueDate) {
					strDate = aQuestion.answer.daysDueDate;
				}
			}
		} else {
			//Es una pregunta fecha directamente, por lo tanto la respuesta tal cual es siempre una fecha
			//JAPRWarning: ¿Se deben aplicar defaults a esto?
			if (aQuestion.answer.answer) {
				strDate = aQuestion.answer.answer;
			}
		}
	}
	
    if (aQuestion.type == Type.date || aQuestion.type == Type.action) {
		//Si venía asignada la fecha entonces la divide, de lo contrario se usa el valor defult que es la fecha actual
		var now = new Date();
		
		if (strDate) {
			strDate = strDate.replace(new RegExp('/',"g"),"-");
			var date = strDate.split('-');
		}
		else {
			strDate = getDate(now);
			var date = strDate.split('-');
		}
		
	    var iniYear = parseInt(date[0],10);
		var iniMonth = parseInt(date[1],10);
		var iniDay = parseInt(date[2],10);
	    var days = {};
	    var years = {};
	    //JP_ML
	    var months = {
	        1: 'January',
	        2: 'February',
	        3: 'March',
	        4: 'April',
	        5: 'May',
	        6: 'June',
	        7: 'July',
	        8: 'August',
	        9: 'September',
	        10: 'October',
	        11: 'November',
	        12: 'December'
	    };
        var months2 = {
            1: 'Jan',
            2: 'Feb',
            3: 'Mar',
            4: 'Apr',
            5: 'May',
            6: 'Jun',
            7: 'Jul',
            8: 'Aug',
            9: 'Sep',
            10: 'Oct',
            11: 'Nov',
            12: 'Dec'
        };
        var months3 = {};
        for (var i = 1; i <= 12; i += 1) {
            if (i < 10) {
                months3[i] = '0' + i;
            } else {
                months3[i] = i;
            }
        }
        
	    for (var i = 1; i < 32; i += 1) {
	        if (i < 10) {
	            days[i] = '0' + i;
	        } else {
	            days[i] = i;
	        }
	    }
	    for (var i = now.getFullYear() - 100; i < now.getFullYear() + 100 + 1; i += 1) {
	        years[i] = i;
	    }
	    
	    if(aQuestion.format)
	    {
	        var formato = aQuestion.format.split('/');
	    }
	    else {
			var formato = msDefaultDateFmt.split('/');
	    }

        for (var k = 0; k < 3; k++) {
            if (formato[k].indexOf("d") != -1) {
                SpinningWheel.addSlot(days, 'center', iniDay);
            }
            if (formato[k].indexOf("M") != -1) {
                switch (formato[k]) {
                    case 'MMM':
                        SpinningWheel.addSlot(months2, 'center', iniMonth);
                        break;
                    case 'MMMM':
                        SpinningWheel.addSlot(months, 'center', iniMonth);
                        break;

                    case 'MM':
                        SpinningWheel.addSlot(months3, 'center', iniMonth);
                        break;

                }
            }
            if (formato[k].indexOf("y") != -1) {
                SpinningWheel.addSlot(years, 'center', iniYear);
            }
        }
	    /*
		SpinningWheel.addSlot(years, 'center', iniYear);
	    SpinningWheel.addSlot(months, 'center', iniMonth);
	    SpinningWheel.addSlot(days, 'center', iniDay);
	    */
	    SpinningWheel.setCancelAction(cancelSpinningWheel);
		SpinningWheel.setDoneAction(doneSpinningWheel);
    }
    else {
        //tipo time
        var hours = {};
        var minutes = {};
        //JP_ML
        var ampm = {
            1: 'HRS'
        };
        
        for (var i = 0; i < 24; i += 1) {
            if (i < 10) {
                hours[i] = '0' + i;
            } else {
                hours[i] = i;
            }
        }
        
        for (var i = 0; i < 60; i += 1) {
            if (i < 10) {
                minutes[i] = '0' + i;
            } else {
                minutes[i] = i;
            }
        }
        
		//JAPRWarning: ¿Se deben aplicar defaults a esto?
    	if (aQuestion.answer && aQuestion.answer.answer && aQuestion.answer.answer.indexOf(':') > 0) {
	    	var tmpTime = (aQuestion.answer.answer).split(':');
	    	var inihour = parseInt(tmpTime[0],10);
	        var iniminute = parseInt(tmpTime[1],10);
    	}
    	else {
    		//poner la hr actual
    		var timeNow=new Date();
			var hoursNow=timeNow.getHours();
			var minutesNow=timeNow.getMinutes(); 
			try{
			var inihour = hoursNow;
	        var iniminute = minutesNow;
			}
			catch(e){
	        var inihour = hours[1];
	        var iniminute = minutes[1];
    	}
    	}
    	
        var iniampm = ampm[1];
        SpinningWheel.addSlot(hours, 'center', inihour);
        SpinningWheel.addSlot(minutes, 'center', iniminute);
        SpinningWheel.addSlot(ampm, 'center', iniampm);
        SpinningWheel.setCancelAction(cancelSpinningWheel);
        SpinningWheel.setDoneAction(doneSpinningWheel);
    }
        
        gblBuildingSpinner = false;
    } catch (e) {
        //2015.02.24 JCEM #NAJ96E
		Debugger.register('Error setSpinningWheel: ' + e);
        gblBuildingSpinner = false;
    }
}

/* Función de callback para cargar los datos recuperados de un Draft, ya que usará el SurveyId almacenado en los datos del mismo
*/
function showSurveyFromDraftData() {
	Debugger.register('showSurveyFromDraftData');
	
	if (!objApp.draftData || !objApp.draftData.surveyID) {
		objApp.clearLocalStorage();
		objApp.loadPage('mainPage');
	}
	
	//JAPR 2014-05-20: Corregido un bug con el refresh previo a carga de encuestas, si se trata del modo Web y está cargando para edición, ya no
	//hay necesidad de solicitar que se haga un refreshFromServer
	var blnRefreshFromServer = undefined;
	if (objSettings && objSettings.webMode && objSettings.singleDraftEntry) {
		blnRefreshFromServer = false;
	}
	objApp.showSurvey(objApp.draftData.surveyID, blnRefreshFromServer);
	//JAPR
}

/* Función de callback del grabado de la encuesta cuando es recuperada de un Draft automáticamente
*/
function surveyRecovered() {
	Debugger.register('surveyRecovered');
	
	if (objeDrafts && objApp.recoveredFileName) {
		var arrFileStruct = unformatFileName(objApp.recoveredFileName);
		if (arrFileStruct === false) {
			//No se pudo identificar el nombre de archivo así que no puede grabar nada
			return;
		}
		
		//Agrega el registro del Draft
		var strSurveyDate = arrFileStruct['date'];
		var strSurveyHour = arrFileStruct['hour'];
		var intDraftKey = strSurveyDate + strSurveyHour + arrFileStruct['surveyID'];
		Debugger.message('Draft saved ' + intDraftKey);
		objeDrafts[intDraftKey] = objApp.recoveredFileName;
		//JAPR 2017-04-19: Agregadas las descripciones de las capturas basadas en una pregunta o expresión de la forma (#UO4Q4M)
		objeDraftsDesc[intDraftKey] = objApp.recoveredEntryDesc;
		objApp.saveDraftsDescriptions();
		//JAPR
	}
	
	//No redireccionará a la página principal porque originalmente era el pintado de la página principal el que redireccionaba aquí, así que sería
	//una llamada ciclica redireccionar de nuevo en este punto, simplemente limpia el LocalStorage de cualquier dato de eForms
	objApp.draftData = undefined;
	objApp.clearLocalStorage();
	//despues de crear el draft debe de actualizar el contador en mainpage
	if (!objSettings.webMode) {
		//ponerlo en color si es mas de 1
		if(objeDrafts && objeDrafts.length() > 0) {
			$('#countermydrafts').css('background', myOrange);
			$('#countermydrafts').css('color', 'white');
		}
		$('#countermydrafts').html(objeDrafts.length());
		objApp.showDraft(objApp.recoveredFileName);;
	}
}

/* Inicia el proceso de sincronización de los Outbox al servidor
*/
function syncToServer() {
	Debugger.register('syncToServer');
	
	//JAPR 2015-07-14: Agregado el identificador único de dispositivo a cada request, así como el nombre de dispositivo y el ID de sessión de sincronización
	if (objSettings) {
		objSettings.sessionID = getFullDate(undefined, true);
	}
	//JAPR
    makeAjaxProcess (
    	function() {
	    	var intOutboxLength = 0;
	        if (objeOutbox && objeOutbox.length() > 0) {
	        	intOutboxLength = objeOutbox.length();
	        }
	        
	        if (intOutboxLength > 0) {
	            var strMsg = '';
	            if (intOutboxLength == 1) {
	                strMsg = translate("{var1} entry will be sent to the server. Proceed?").replace("{var1}", intOutboxLength);
	            } else {
	                strMsg = translate("{var1} entries will be sent to the server. Proceed?").replace("{var1}", intOutboxLength);
	            }
	            navigator.notification.confirm(strMsg, function (answer) {
	                if (answer == 1) {
	                    mobileLoading(translate("Sending captured data to server"));
	                    setTimeout(function () {
							//JAPR 2013-02-21: Agregado el método para refrescar exclusivamente la ruta del servicio justo antes de cada petición al server
							//Se invocará primero al método que actualiza la ruta a usar y posteriormente como callback se invocará al proceso específico que ya
							//usará la nueva ruta obtenida
							objApp.checkUserServiceFolder('objApp.identifySynchronizationFiles');
	                    	//objApp.identifySynchronizationFiles();
	                    	//JAPR
	                    }, 100);
                        $('#syncButtonColor').css('background-color','initial');
                        $('#syncButtonColor').css('background-image', 'initial');
	                }
	            }, 'Data Sync');
	        }
	        else {
	        	refreshFromServer();
	        }
	    },
		function() {objApp.offlineErrorDialog('refreshFromServer');}
    );
}

//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Funciones de uso general */
//****************************************************************************************************************************************
//****************************************************************************************************************************************
/* Regresa un valor dentro del rango entero especificado en los parámetro
*/
function getIntegerBetween(aValue, aMinVal, aMaxVal, aDefault, bAllowEmpty) {
	if ((aValue === undefined || aValue == '') && !bAllowEmpty) {
		aValue = aDefault;
	}
	if (aMinVal == '') {
		aMinVal = undefined;
	}
	if (aMaxVal == '') {
		aMaxVal = undefined;
	}
	
	if (aValue != '' || !bAllowEmpty) {
		aValue = parseInt(aValue);
	}
	if (isNaN(aValue)) {
		aValue = 0;
	}
	if (aMinVal !== undefined && aValue < aMinVal) {
		aValue = aMinVal;
	}
	else if (aMaxVal !== undefined && aValue > aMaxVal) {
		aValue = aMaxVal;
	}
	
	return aValue;
}

/* Identifica un error en la respuesta de un Request de sincronización, la cual dependiendo de la sección en la que se generó pudiera venir con 
diferente formato. Si identifica algún error regresa un objeto con la estructura del error, de lo contrario regresa false
El parámetro bCheckForOk sólo se debe usar en la última respuesta esperada del proceso de sincronización, si se activa, adicional a las verificaciones
estándar de códigos de error que son esperadas, se verifica que la respuesta en si sea el texto de que todo fué sincronizado correctamente, de lo
contrario cualquier cosa que se regresara se consideraría un reporte de error sin estructura (errores de PhP por ejemplo)
*/
function identifySyncError(aResponseText, bCheckForOk) {
	var objError = false;
	
	var intErrIdx = aResponseText.indexOf('errmsg=');
	if (intErrIdx >= 0) {
		objError = new Object();
		objError.errCode = -1;
		objError.errDesc = aResponseText.substr(intErrIdx + 7);
	}
	else {
		var intErrIdx = aResponseText.indexOf('_SV_ERR_');
		if (intErrIdx >= 0) {
			var strMsg = aResponseText.substr(0, intErrIdx);
			objError = new Object();
			objError.errCode = parseInt(strMsg);
			if (isNaN(objError.errCode)) {
				objError.errCode = -1;
			}
			
			strMsg = aResponseText.substr(intErrIdx + 8);
			intErrIdx = strMsg.indexOf('_SV_FLD_');
			if (intErrIdx >= 0) {
				strMsg = strMsg.substr(0, intErrIdx);
			}
			objError.errDesc = strMsg;
		}
		else {
			if (bCheckForOk) {
				//Si no se recibió exactamente el texto de que todo estuvo ok, se considerará un error
				if (aResponseText.indexOf('Survey data was saved successfully.') == -1) {
					objError = new Object();
					objError.errCode = -1;
					//JAPR 2018-09-26: Validado que todo upload realizado al server busque explícitamente la respuesta de que todo ok, de lo contrario se asumirá que hubo algún tipo de error, esto
					//debido a que se habían perdido imágenes al realizar uploads con display_errors = 0, cuando ocurrían errores de php pero aún así regresaban un http code 200 en lugar de un 500 (#961S7M)
					//Modificada la propiedad de error para solo desplegar el error completo en el log de depuración, no en pantalla para el usuario
					objError.errExtended = aResponseText;
					objError.errDesc = '';
					//JAPR
				}
			}
		}
	}
	return objError;
}

/* Obtiene los valores de todas las propiedades de un objeto tal como si fuera la función join de un array, sin embargo el objeto no debe contener
funciones o estas podrían interpretarse como propiedades y alterar el resultado. Se puede agregar un parámetro con un objeto que contenga una
lista de propiedades a excluir
*/
function joinObject(anInstance, aGlue, aSkipPropColl) {
	Debugger.register('joinObject');
	if (!aSkipPropColl) {
		aSkipPropColl = new Object();
	}
	if (!aGlue) {
		aGlue = ',';
	}
	
	var arrKeys = Object.keys(anInstance);
	var strGlue = '';
	strResult = '';
	
	for (var strKey in arrKeys) {
		if (!aSkipPropColl[arrKeys[strKey]]) {
			strResult += strGlue + anInstance[arrKeys[strKey]];
			strGlue = aGlue;
		}
	}
	
	return strResult;
}

/* Obtiene la referencia a la función solicitada a partir del nombre de la función */
function getFunctionRefFromName(aFunctionName) {
	Debugger.register('getFunctionRefFromName ' + ((typeof(aFunctionName) == 'string')?aFunctionName:'function'));
	if (!aFunctionName || aFunctionName == '') {
		return undefined;
	}
	
	try {
		//JAPR 2013-02-21: Agregada la validación para detectar si el parámetro recibido ya es o no una función y simplemente regresarlo
		if (typeof(aFunctionName) == 'function' || typeof(aFunctionName) == 'object') {
			return aFunctionName;
		}
		//JAPR
		
		if (aFunctionName.indexOf('.') == -1) {
			return window[aFunctionName];
		}
		else {
			//Hay que hacer referencia al namedSpace donde está definida la función
			var arrFnName = aFunctionName.split('.');
			return window[arrFnName[0]][arrFnName[1]];
		}
	} catch(e) {
		return undefined;
	}
}

//JAPR 2015-03-27: Corregido un bug, no permitía editar outbox con secciones Inline con filtros dinámicos/agendas (#7RZL4A)
/* Obtiene la referencia a la función solicitada a partir del nombre de la función
A diferencia de getFunctionRefFromName que cuando se mandaba un nombre de función o directamente una función de un objeto y que perdía el
contexto de this en esos casos, esta función se asegurará de respetar el contexto del objeto al que se hace referencia en la función. Para los
casos donde lo que se recibe es una función directamente, no hay diferencia con la función getFunctionRefFromName, así que no se deberia usar
en esas situaciones. Se separó en dos funciones para evitar problemas de compatibilidad al incluir un cambio de este tipo con poco tiempo de
prueba del producto antes de liberarlo
*/
function getFunctionRefFromNameProxy(aFunctionName) {
	Debugger.register('getFunctionRefFromNameProxy ' + ((typeof(aFunctionName) == 'string')?aFunctionName:'function'));
	if (!aFunctionName || aFunctionName == '') {
		return undefined;
	}
	
	try {
		//JAPR 2013-02-21: Agregada la validación para detectar si el parámetro recibido ya es o no una función y simplemente regresarlo
		if (typeof(aFunctionName) == 'function' || typeof(aFunctionName) == 'object') {
			return aFunctionName;
		}
		//JAPR
		
		if (aFunctionName.indexOf('.') == -1) {
			return window[aFunctionName];
		}
		else {
			//Hay que hacer referencia al namedSpace donde está definida la función
			var arrFnName = aFunctionName.split('.');
			//En caso de que el string recibido sea una función de un objeto (que debe ser global), se asegura de mantener el contexto del objeto
			return function() {window[arrFnName[0]][arrFnName[1]]()};
		}
	} catch(e) {
		return undefined;
	}
}
//JAPR

/* Muestra la ventana de carga del JQueryMobile */
//OMMC 2016-06-08: Cambiados los mensajes de loading, siempre se planchaba con el mensaje "Cargando", ahora ya recibe el mensaje que se le envíe.
function mobileLoading(aMsg) {
	Debugger.register('mobileLoading ' + aMsg);
	//JAPR 2015-02-24: Rediseñado el Admin con DHTMLX
	if (gbDesignMode) {
		return;
	}
	//JAPR
	
	if (aMsg === undefined) {
		aMsg = translate("Loading");
	}
	//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
    /*$.mobile.showPageLoadingMsg();*/ 
    //version pasada de loading
    $.mobile.loading("show", {text: aMsg, textonly:false, theme:'b', textVisible:true});
	//JAPR
}

//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
/* en caso de que la funcion loading('hide') cambie en un futuro modificar esta funcion*/
function mobileHide() {
	Debugger.register('mobileHide');
	if (gbDesignMode) {
		return;
	}
	$.mobile.loading("hide");
}

//OMMC 2016-04-01
//Muestra una notificación (alerta) con algun mensaje. No es un mensaje de error como throwError, sino un aviso para el usuario de que algo se ejecutó.
/*
    notMsg: (string) Mensaje que va a mostrar en la alerta.
    notTitle: (string) Título del mensaje de alerta.
    notButotn: (string) Texto que estará en el botón.
    aHideLoadingScreen: (bool) Si tiene que ocultar un loading screen en caso que haya alguno.
*/
function throwNotification(notMsg, notTitle, notButton, notHideLoadingScreen){
	oNotification = {
		nMsg: notMsg, 
		nTitle: notTitle, 
		nButton: notButton, 
		nHideLoadingScreen: notHideLoadingScreen
	};
	
    if(!oNotification.nMsg){
        return;
    }
    if(!oNotification.nTitle){
        oNotification.nTitle = '';
    }

    if(!oNotification.nButton){
        oNotification.nButton = translate("Ok");
    }

    if (oNotification.nHideLoadingScreen === undefined || oNotification.nHideLoadingScreen) {
    	//OMMC 2016-05-23: Cambiada la llamada del hide al nuevo jquery
        mobileHide();
    }

    if(objSettings.webMode){
        alert(oNotification.nMsg);
    }else{
        if(oNotification.nTitle){
            navigator.notification.alert(oNotification.nMsg, null, oNotification.nTitle, oNotification.nButton);
        }else{
            navigator.notification.alert(oNotification.nMsg, null, ' ', oNotification.nButton);
        }
    }
}

/* Muestra el error especficado verificando si está o no cargado el objeto para este fin y permite realizar acciones adicionales
//JAPR 2014-08-08: Modificado el reporte de errores para hacerlos mas amigables
Agregado el parámetro iErrType para indicar cuando se trate de un error que request o algún otro que requiera tratamiento especial, como por
ejemplo grabar en el log de errores específico
*/
function throwError(anErrNum, anErrMsg, anErrTitle, aFunctionName, aHideLoadingScreen, bAskConfirmation, sConfirmationText, aConfirmCallbackFn, iErrType) {
	var strLastFnCall = '';
	var answer = undefined;
	
	//Ahora se manejará como un objeto la lista de parámetros
	oErrDef = anErrNum;
	if (!$.isPlainObject(oErrDef)) {
		oErrDef = {code:anErrNum, desc:anErrMsg, title:anErrTitle, fnName:aFunctionName, 
			hideLoadingScreen:aHideLoadingScreen, askConfirmation:bAskConfirmation,
			confirmationText:sConfirmationText, confirmCallbackFn:aConfirmCallbackFn,
			type:iErrType};
	}
	
	//JAPR 2016-10-25: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
	//Ahora el título de la ventana de mensajes será simplemente el nombre del App, ya no mas mensajes que pudieran ser confusos o repetir la palabra "Error"
	if (!oErrDef.title) {
		oErrDef.title = '';
	}
	
	oErrDef.title = (gsAppName)?gsAppName:"KPIOnline Forms";
	//JAPR
	if (!oErrDef.confirmationText) {
		oErrDef.confirmationText = '';
	}
	
	//JAPR 2014-06-25: Validado que si se está cargando con un login automático manual, no muestre la ventana de login
	if ($('#loginDataManual').length) {
		$('#loginpage_header').show();
		$('#loginpage_body').show();
		$('#loginpage_footer').show();
	}
	//JAPR
	
	if (oErrDef.hideLoadingScreen == undefined || oErrDef.hideLoadingScreen) {
		//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
        //hidepageloadingmsg() -> loadmsg() 
		mobileHide();
		//JAPR
	}
	
	if (!oErrDef.fnName) {
		oErrDef.fnName = '';
	}
	if (Debugger) {
		strLastFnCall = Debugger.getLastFnCall();
		if (strLastFnCall) {
			oErrDef.fnName += '-> ' + strLastFnCall;
		}
	}
	
	if (objApp && objApp.setError) {
		//JAPR 2014-08-08: Modificado el reporte de errores para hacerlos mas amigables
		//JAPR 2014-08-15: Agregado el detalle de error exacto para el log del usuario
		//JAPR 2016-11-02: Modificado el reporte de errores nuevamente para hacerlos todavía mas amigables
		//No había necesidad de copiar manualmente este objeto, se puede heredar directamente para permitir agregar propiedades desde la llamada a esta función sin replicar aquí
		var objErrorDef = $.extend({}, oErrDef);	//{code:oErrDef.code, desc:oErrDef.desc, detail:oErrDef.detail, title:oErrDef.title, fnName:oErrDef.fnName, type:oErrDef.type};
		if (oErrDef.askConfirmation) {
			//En este caso registra el error (pero no lo muestra en setError) y posteriormente muestra el diálogo para confirmar la respuesta
			//JAPR 2014-08-08: Modificado el reporte de errores para hacerlos mas amigables
			$.extend(objErrorDef, {showFlag:false});
			objApp.setError(objErrorDef);
			var strError = '';
			//JAPR 2014-08-08: Modificado el reporte de errores para hacerlos mas amigables
			/*
			if (oErrDef.fnName != '') {
				strError += '(' + oErrDef.fnName + ') ';
			}
			*/
			strError += "Error #" + oErrDef.code + ": " + oErrDef.desc;
			
		    if (objSettings && objSettings.webMode) {
		    	answer = confirm(strError + "\r\n" + oErrDef.confirmationText);
		    }
		    else {
		    	var objCallBackFn = getFunctionRefFromName(oErrDef.confirmCallbackFn);
			    navigator.notification.confirm(strError + "\r\n" + oErrDef.confirmationText, function(answer) {
			        if (answer == 1) {
			        	if (objCallBackFn) {
			        		objCallBackFn();
			        	}
			        }
			    });
		    }
		}
		else {
			//Sólo se despliega el error sin esperar respuesta
			//JAPR 2014-08-08: Modificado el reporte de errores para hacerlos mas amigables
			objApp.setError(objErrorDef);
		}
	}
	else {
		//JP_ML: Traducción
		Debugger.message('Error #' + oErrDef.code + ': ' + oErrDef.desc);
	    if (objSettings && objSettings.webMode) {
			alert('Error #' + oErrDef.code + ': ' + oErrDef.desc);
	    }
	    else {
	    	navigator.notification.alert('Error #' + oErrDef.code + ': ' + oErrDef.desc, null, oErrDef.title);
	    }
	}
	
	//JAPR 2018-07-20 Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	if ( oErrDef.detail ) {
		Debugger.message('Error detail: ' + oErrDef.detail);
	}
	//JAPR
	
	return answer;
}

/* Identifica el tipo de nombre de archivo basado en el reconocimiento de patrones. Si el archivo no corresponde con un archivo del usuario
logeado regresará false, de lo contrario regresa un objeto con las partes identificadas del nombre de archivo
*/
function unformatFileName(aFileName) {
	Debugger.register('unformatFileName aFileName == ' + aFileName);
	if (!objSettings) {
		return false;
	}
	
	var strFileName = aFileName;
	if (strFileName.indexOf(objSettings.user) < 0) {
		return false;
	}
	
	//Remueve la parte del usario
	strFileName = strFileName.replace(new RegExp(RegExp.quote(objSettings.user + '_'), "gi"), '');
	var arrParts = strFileName.split('_');
	var objFileStruct = new Object();
	switch (arrParts[0]) {
		case 'surveyanswers':
			//Puede ser un Draft u Outbox, así que verifica buscando la palabra que los identifica
			var blnOutbox = false;
			for (var intIndex in arrParts) {
				if (arrParts[intIndex].indexOf('finished.dat') >= 0) {
					blnOutbox = true
					break;
				}
			}
			
			if (blnOutbox) {
				objFileStruct['typeid'] = ObjectType.otyOutbox;
				objFileStruct['type'] = 'Outbox';
			}
			else {
				objFileStruct['typeid'] = ObjectType.otyDraft;
				objFileStruct['type'] = 'Draft';
			}
			
			if (arrParts.length > 1) {
				objFileStruct['date'] = arrParts[1].replace('.dat', '');
				objFileStruct['fmtdate'] = formatNumericDate(objFileStruct['date']);
			}
			if (arrParts.length > 2) {
				objFileStruct['hour'] = arrParts[2].replace('.dat', '');
				objFileStruct['fmthour'] = formatNumericTime(objFileStruct['hour']);
			}
			if (arrParts.length > 3) {
				objFileStruct['surveyID'] = arrParts[3].replace('.dat', '');
			}
			if (arrParts.length > 4) {
				objFileStruct['agendaID'] = arrParts[4].replace('.dat', '');
				if (!$.isNumeric(objFileStruct['agendaID'])) {
					objFileStruct['agendaID'] = undefined;
				}				
			}
			break;
		
		case 'survey':
			//Definición de una encuesta
			objFileStruct['typeid'] = ObjectType.otySurvey;
			objFileStruct['type'] = 'Survey';
			if (arrParts.length > 1) {
				objFileStruct['surveyID'] = arrParts[1];
			}
			break;
			
		case 'catalog':
			//Definición de un catálogo
			objFileStruct['typeid'] = ObjectType.otyCatalog;
			objFileStruct['type'] = 'Catalog';
			if (arrParts.length > 1) {
				objFileStruct['catalogID'] = arrParts[1];
			}
			break;
		//JAPR 2016-12-01: Agregado el soporte de múltiples passwords para usuarios en distintos espacios de trabajo (#XZ9KKC)
		//Agregados arrays faltantes a estos procesos
		case 'menu':
			//Definición de un menú
			objFileStruct['typeid'] = ObjectType.otyMenu;
			objFileStruct['type'] = 'Menu';
			if (arrParts.length > 1) {
				objFileStruct['menuID'] = arrParts[1];
			}
			break;
		//JAPR 2019-02-20: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		case 'sketch':
		//JAPR
		case 'canvas':	
		case 'photo':
			//Definición de una foto
			objFileStruct['typeid'] = ObjectType.otyPhoto;
			objFileStruct['type'] = 'Photo';
			if (arrParts.length > 1) {
				objFileStruct['surveyID'] = arrParts[1];
			}
			if (arrParts.length > 2) {
				objFileStruct['date'] = arrParts[2];
				objFileStruct['fmtdate'] = formatNumericDate(objFileStruct['date']);
			}
			if (arrParts.length > 3) {
				objFileStruct['hour'] = arrParts[3];
				objFileStruct['fmthour'] = formatNumericTime(objFileStruct['hour']);
			}
			if (arrParts.length > 4) {
				objFileStruct['questionID'] = arrParts[4];
			}
			if (arrParts.length > 5) {
				objFileStruct['recordNum'] = arrParts[5];
			}
			break;
			
		case 'audio':
			//Definición de un audio
			
			objFileStruct['typeid'] = ObjectType.otyAudio;
			objFileStruct['type'] = 'Audio';
			if (arrParts.length > 1) {
				objFileStruct['surveyID'] = arrParts[1];
				
			}
			if (arrParts.length > 2) {
				objFileStruct['date'] = arrParts[2];
				objFileStruct['fmtdate'] = formatNumericDate(objFileStruct['date']);
				
			}
			if (arrParts.length > 3) {
				objFileStruct['hour'] = arrParts[3];
				objFileStruct['fmthour'] = formatNumericTime(objFileStruct['hour']);
				
			}
			if (arrParts.length > 4) {
				objFileStruct['questionID'] = arrParts[4];
				
			}
			if (arrParts.length > 5) {
				objFileStruct['recordNum'] = arrParts[5];
				
			}
			break;
						
		case 'video':
			//Definición de un video
			
			objFileStruct['typeid'] = ObjectType.otyVideo;
			objFileStruct['type'] = 'Video';
			if (arrParts.length > 1) {
				objFileStruct['surveyID'] = arrParts[1];
				
			}
			if (arrParts.length > 2) {
				objFileStruct['date'] = arrParts[2];
				objFileStruct['fmtdate'] = formatNumericDate(objFileStruct['date']);
				
			}
			if (arrParts.length > 3) {
				objFileStruct['hour'] = arrParts[3];
				objFileStruct['fmthour'] = formatNumericTime(objFileStruct['hour']);
				
			}
			if (arrParts.length > 4) {
				objFileStruct['questionID'] = arrParts[4];
				
			}
			if (arrParts.length > 5) {
				objFileStruct['recordNum'] = arrParts[5];
				
			}
			break;

		case 'document':
			//Definición de un documento

			objFileStruct['typeid'] = ObjectType.otyDocument;
			objFileStruct['type'] = 'Document';
			if (arrParts.length > 1) {
				objFileStruct['surveyID'] = arrParts[1];
				
			}
			if (arrParts.length > 2) {
				objFileStruct['date'] = arrParts[2];
				objFileStruct['fmtdate'] = formatNumericDate(objFileStruct['date']);
				
			}
			if (arrParts.length > 3) {
				objFileStruct['hour'] = arrParts[3];
				objFileStruct['fmthour'] = formatNumericTime(objFileStruct['hour']);
				
			}
			if (arrParts.length > 4) {
				objFileStruct['questionID'] = arrParts[4];
				
			}
			if (arrParts.length > 5) {
				objFileStruct['recordNum'] = arrParts[5];
				
			}
			if (arrParts.length > 6) {
				objFileStruct['fileType'] = arrParts[6];
				
				if (objFileStruct['fileType'].indexOf('.') != -1) {
					//Si es que tiene una extensión, se separan los nombres para obtener el tipo de archivo y su extensión
					objFileStruct['fileExt'] = objFileStruct['fileType'].substr(objFileStruct['fileType'].indexOf('.') + 1, objFileStruct['fileType'].length);
					objFileStruct['fileType'] = objFileStruct['fileType'].substr(objFileStruct['fileType'][0], objFileStruct['fileType'].indexOf('.'));
				} else {
					objFileStruct['fileExt'] = '';
				}
			}
			break;
				
		case 'agenda':
			//Definición de una agenda, pero pude estar marcada como completada o no, así que busca la palabra clave
			var blnCompleted = false;
			for (var intIndex in arrParts) {
				if (arrParts[intIndex].indexOf('completed') >= 0) {
					blnCompleted = true
					break;
				}
			}
			
			objFileStruct['typeid'] = ObjectType.otyAgenda;
			objFileStruct['type'] = 'Agenda';
			if (blnCompleted) {
				objFileStruct['completed'] = true;
			}
			
			if (arrParts.length > 1 && arrParts[1].indexOf('completed') < 0) {
				objFileStruct['agendaID'] = arrParts[1];
			}
			break;
	}
	
	objFileStruct['fileName'] = aFileName;
	
	return objFileStruct;
}

//Valida si el número se encuentra entre el rango especificado y muestra un mensaje de advertencia si no lo está regresando un valor default, o bien
//regresando el propio número si es válido
function validateNumber(aValue, aMinValue, aMaxValue, aDefaultValue, aShowMsg, aStrictNumberFlag) {
	var dblValue = aValue;
	
	if (aStrictNumberFlag) {
		if (isNaN(dblValue)) {
			return aDefaultValue;
		}
	}
	
	if (aMinValue === undefined) {
		aMinValue = '';
	}
	if (aMaxValue === undefined) {
		aMaxValue = '';
	}
	if (aDefaultValue === undefined) {
		aDefaultValue = '';
	}
	
	aMinValue = parseFloat(Trim(aMinValue));
	if (isNaN(aMinValue)) {
		aMinValue = '';
	}
	aMaxValue = parseFloat(Trim(aMaxValue));
	if (isNaN(aMaxValue)) {
		aMaxValue = '';
	}
	
	if (aMinValue !== '' && aMaxValue !== '') {
		//JAPR 2016-06-06: Ajustada traducciones faltantes (#HC4SZT)
		strMsg = translate("Please enter a value between {var1} and {var2}").replace("{var1}", aMinValue).replace("{var2}", aMaxValue);
	}
	else {
		if (aMinValue !== '') {
			//JAPR 2016-06-06: Ajustada traducciones faltantes (#HC4SZT)
			strMsg = translate("Please enter a value greater than or equal to {var1}").replace("{var1}", aMinValue);
		}
		else if (aMaxValue !== '') {
			//JAPR 2016-06-06: Ajustada traducciones faltantes (#HC4SZT)
			strMsg = translate("Please enter a value less than or equal to {var1}").replace("{var1}", aMaxValue);
		}
	}
	
	//OMMC 2016-07-26: Cambiada la forma en la que se arrojan las notificaciones, agregada traducción.
	if (aMinValue !== '') {
		if (dblValue < aMinValue) {
			if (aShowMsg) {
				throwNotification(strMsg, translate('Check value'));
			}
			dblValue = aDefaultValue;
		}
	}
	if (aMaxValue !== '') {
		if (dblValue > aMaxValue) {
			if (aShowMsg) {
				throwNotification(strMsg, translate('Check value'));
			}
			dblValue = aDefaultValue;
		}
	}
	
	return dblValue;
}

//JAPR 2013-01-18: Corregido un bug, se deben reemplazar las htmlEntities de los inputs cuando se genera el HTML
function replaceHTMLEntities(sText) {
	if (sText === undefined) return '';
	
	return sText.replace(new RegExp('"', "gi"), '&quot;')
}
//JAPR

//JAPR 2016-05-26: Modificado el esquema de visibilidad de preguntas a partir de una condición (#NMW148)
/* Dada una expresión que puede contener variables de preguntas o sección, parsea el string e identifica todas, regresando un objeto con cada variable identificada única, con todo
el detalle de lo que representa separado en subobjetos para su fácil manipulación por medio de otras funciones
Sólo soporta el formato extendido de variables
*/
function identifyVariables(sText) {
	var objVariables = new Object();
	var objInvalidVariables = new Object();
	
	var strText = sText;
	var objRegExp = /[\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/g;
	var arrVars = strText.match(objRegExp);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		//No reprocesa variables ya identificadas
		if (objVariables[strExpr] || objInvalidVariables[strExpr]) {
			continue;
		}
		
		objInvalidVariables[strExpr] = {};
		//Primero intentará separar por el total de propiedades/funciones especificadas, esto es las cadenas separadas por '.'. Siempre la última es la
		//única que podría ser una función así que se valida si está o no definda así como su parámetro en caso de haber alguno
		//Sólo puede haber una propiedad, una función, o una propiedad + una función, todo aplicando a una misma pregunta y/o sección
		var strProperty = '';
		var strFunction = '';
		var strParam = '';
		var intType = ObjectType.otyQuestion;
		var arrParts = strExpr.split('.');
		var strSectionNum = undefined;
		var strQuestionNum = arrParts[0];
		var blnValid = true;
		//Identifica el número de pregunta y tipo de variable
		var arrType = strQuestionNum.match(/[SsQq]/g);
		
		if (!arrType.length) {
			continue;
		}
		
		switch (arrType[0].toLowerCase()) {
			case 's':
				var intType = ObjectType.otySection;
				break;
			case 'q':
				var intType = ObjectType.otyQuestion;
				break;
			default:
				blnValid = false;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		
		strQuestionNum = strQuestionNum.replace(/[{}$SsQq]/g, '');
		if (isNaN(strQuestionNum)) {
			continue;
		}
		
		strQuestionNum = parseInt(strQuestionNum);
		//Identifica el resto de los componentes de la variable
		switch (arrParts.length) {
			case 1:	//Sólo es la definición de la variable, así que la propiedad default es value
				strProperty = 'value';
				break;
			case 2: //Es una propiedad o función basado en si la segunda parte contiene o no ()
				strFunction = arrParts[1].replace('}', '');
				arrParams = strFunction.split('(');
				switch (arrParams.length) {
					case 1: //Es sólo una propiedad
						strProperty = strFunction;
						strFunction = '';
						break;
					default: //Sólo debería haber un único paréntesis y entonces significa que es una función
						strFunction = arrParams[0];
						strProperty = strFunction;
						strParam = arrParams[1].replace(')', '');
						break;
				}
				break;
			case 3: //Es una propiedad + función (caso no soportado con la actual expresión regular)
				strProperty = arrParts[1].replace('}', '');
				strFunction = arrParts[2].replace('}', '');
				arrParams = strFunction.split('(');
				strFunction = arrParams[0];
				if (arrParams.length > 1) {
					strParam = arrParams[1].replace(')', '');
				}
				break;
			default: //No se soporta esta expresión, aunque no debería haber aceptado nada similar a este caso
				blnValid = false;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		var strPropertyCS = strProperty;
		strProperty = strProperty.toLowerCase();
		strFunction = strFunction.toLowerCase();
		strParam = strParam.toLowerCase();
		strParam = strParam.replace(/[\'\"]/gi, '');
		
		//Hasta este punto ya se tienen identificados los diferentes componentes de la variable, ahora deber ejecutar procesos diferentes según tu tipo
		//Primero obtiene la referencia al objeto del que se trata
		var objQuestion = undefined;
		var objSection = undefined;
		//Utilizada como objeto polimórfico para obtener las propiedades dinámicamente según el tipo de variable, cuando se traten de propiedades
		//de la definición que no requieran tratamiento especial
		var objObject = undefined;
		switch (intType) {
			case ObjectType.otyQuestion:
				if (!selSurvey || !selSurvey.questions || !selSurvey.questionsOrder) {
					blnValid = false;
					break;
				}
				
				var objQuestion = selSurvey.questions[selSurvey.questionsOrder[strQuestionNum-1]];
				if (!objQuestion || !objQuestion.answer || !objQuestion.section) {
					//Si no se encontró la pregunta, continua con la siguiente expresión identificada
					blnValid = false;
					break;
				}
				
				objSection = objQuestion.section;
				strSectionNum = objSection.number;
				
				objObject = objQuestion;
				break;
			case ObjectType.otySection:
				if (!selSurvey || !selSurvey.sections || !selSurvey.sectionsOrder) {
					blnValid = false;
					break;
				}
				
				strSectionNum = strQuestionNum;
				var objSection = selSurvey.sections[selSurvey.sectionsOrder[strSectionNum-1]];
				if (!objSection) {
					blnValid = false;
					break;
				}
				
				objObject = objSection;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		
		//En este punto siempre estará asignada objSection independientemente de si es una variable de pregunta o sección, pero por el contrario
		//objQuestion sólo estaría asignada si fueran variables de preguntas
		//Agrega la identificación de la variable en el objeto resultado
		objVariable = new Object();
		objVariable.type = intType;
		objVariable.id = objObject.id;
		objVariable.expr = strExpr;
		objVariable.prop = strProperty;
		objVariable.fn = strFunction;
		objVariable.param = strParam;
		objVariable.number = objObject.number;
		objVariables[strExpr] = objVariable;
		
		//Remueve la variable de la lista de inválidas si llegó hasta este punto
		delete objInvalidVariables[strExpr];
	}
	
	return objVariables;
}

//OMMC 2016-07-28: Agregada para un posible reemplazo de variables de usuario
/*
	sText 			(string) 	Texto a reemplazar
*/
function replaceVarsUser(sText) {
	Debugger.register('replaceVarsUser');

	if (!sText) {
		sText = '';
	}

	return sText;
}

//JAPR 2014-10-15: Agregado el nuevo esquema de variables avanzado (con scores)
/* Esta función se lleva a cabo antes del replaceVars original (se puede invocar de manera independiente pero no reemplazará la totalidad de posibles 
variables de esa forma). Es utilizada para reemplazar las variables avanzadas con la nueva sintaxis mas parecida a php/javascript, la cual no reemplaza
a la sintaxis anterior sino que la complementa. Para nuevas funcionalidades se deberán programar exclusivamente con esta nueva sintaxis
//JAPR 2015-09-22: Modificado el comportamiento de bQuoteAnswers para que la decisión de delimitar o no la variable dependa del tipo de pregunta
Por autorización de Luis, todas las variables que se resuelvan a String se van a procesar de la siguiente manera:
	- Si están en un defaultValue o HTML o cualquier String que NO sea una expresión, se reemplazarán siempre SIN delimitarse
	- Si están en un string que representa una expresión, se tienen que delimitar siempre y cuando NO se encontraran previamente delimitadas (se usará una expresión regular)
	- Si están en un HTML que puede tener un script, se delimitarán siempre como el caso 2 cuando estén en el script, pero como el caso 1 cuando estén fuera de él
Debido a este cambio, el parámetro bQuoteAnswers cambia su sentido y ahora indicará si el string recibido corresponde a una expresión (true) o a un valor fijo (false) para determinar
que caso de reemplazos se va a utilizar
*/
function replaceVarsAdv(sText, anIgnoreCol, anApplyFormat, initBeforeShow, bReturnNumericDefault, aSectionEvalRecord, bQuoteAnswers) {
	Debugger.register('replaceVarsAdv');
	if (aSectionEvalRecord === undefined) {
		aSectionEvalRecord = {};
	}
	
	if (!sText) {
		sText = '';
	}
	
	if (!anIgnoreCol) {
		anIgnoreCol = new Object();
	}
	
	var intStart = sText.indexOf('{');
	if (intStart == -1) {
		//Si no hay variables, simplemente regresa el texto recibido
		return sText;
	}
	
	//Inicia el reemplazo de los valores de las preguntas en formato avanzado:
	//{$Q###.property} -> Obtiene el dato específicado por el nombre de propiedad para una pregunta
	//Algunos posibles datos son: value, comment, photo, score, atributo de catálogo, etc.
	//{$Q###.function(data)} -> Obtiene el resultado de aplicar la función especificada al dato indicado para esta pregunta (aplica el contexto donde
	//se utilice, si se invoca como parte de una fórmula en una sección con múltiples registros, regresará el dato del registro actual, de lo
	//contrario si se pide en una fórmula de una sección estándar, regresará la función aplicada a la totalidad de registros de la pregunta
	//Utiliza los mismos tipos de dato de la pregunta aplicables en el property de la sintaxis básica
	var strText = sText;
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	//var arrVars = strText.match(/[\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\(([0-9|a-z|A-Z\'\"]*)\)))|){1}[}]{1}/g);
	//JAPR 2014-10-28: Corregido un bug, se cambió la expresión para que el parámetro de la función sea cualquier caracter
	//JAPR 2015-09-22: Modificado el comportamiento de bQuoteAnswers para que la decisión de delimitar o no la variable dependa del tipo de pregunta
	//Si se está reemplazando una expresión, debe excluir variables ya delimitadas, de lo contrario si se incluirán
	var objRegExp = /[\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/g;
	var objRegExpQuoted = /"(?:[^"]|\\")*"|\\"|'(?:[^']|\\')*'|\\'|([\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1})/g;
	//JAPR 2015-09-24: Finalmente luego de analizar las posibilidades, todas las variables se reemplazarán, sólo variará el cuando son o no delimitadas, así que se usará la expresión
	//regular original para no descartar ninguna
	/*if (bQuoteAnswers) {
		//En este caso regresa todos los string delimitados además de la propia variable, los strings delimitados serán filtrados para excluirse, si no resulta ninguna variable
		//no delimitada, entonces no hay necesidad de realizar reemplazos
		arrVars = strText.match(objRegExpQuoted);
		if (arrVars) {
			if ($.isArray(arrVars)) {
				arrVars = arrVars.filter(function (elem) {
					return elem[0] != '"' && elem[0] != "'";
				});
			}
		}
	}
	else {
		//En este caso todas las variables delimitadas o no son válidas para reemplazar
	*/
	var arrVars = strText.match(objRegExp);
	//}
	//JAPR
	
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strValue = '';
		var strExpr = arrVars[intCont];
		//Primero intentará separar por el total de propiedades/funciones especificadas, esto es las cadenas separadas por '.'. Siempre la última es la
		//única que podría ser una función así que se valida si está o no definda así como su parámetro en caso de haber alguno
		//Sólo puede haber una propiedad, una función, o una propiedad + una función, todo aplicando a una misma pregunta y/o sección
		var strProperty = '';
		var strFunction = '';
		var strParam = '';
		var intType = ObjectType.otyQuestion;
		var arrParts = strExpr.split('.');
		var strSectionNum = undefined;
		var strQuestionNum = arrParts[0];
		var blnValid = true;
		//Identifica el número de pregunta y tipo de variable
		var arrType = strQuestionNum.match(/[SsQq]/g);
		
		if (!arrType.length) {
			continue;
		}
		
		switch (arrType[0].toLowerCase()) {
			case 's':
				var intType = ObjectType.otySection;
				break;
			case 'q':
				var intType = ObjectType.otyQuestion;
				break;
			default:
				blnValid = false;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		
		strQuestionNum = strQuestionNum.replace(/[{}$SsQq]/g, '');
		if (isNaN(strQuestionNum)) {
			continue;
		}
		
		strQuestionNum = parseInt(strQuestionNum);
		//Identifica el resto de los componentes de la variable
		switch (arrParts.length) {
			case 1:	//Sólo es la definición de la variable, así que la propiedad default es value
				strProperty = 'value';
				break;
			case 2: //Es una propiedad o función basado en si la segunda parte contiene o no ()
				strFunction = arrParts[1].replace('}', '');
				arrParams = strFunction.split('(');
				switch (arrParams.length) {
					case 1: //Es sólo una propiedad
						strProperty = strFunction;
						strFunction = '';
						break;
					default: //Sólo debería haber un único paréntesis y entonces significa que es una función
						strFunction = arrParams[0];
						strProperty = strFunction;
						strParam = arrParams[1].replace(')', '');
						break;
				}
				break;
			case 3: //Es una propiedad + función (caso no soportado con la actual expresión regular)
				strProperty = arrParts[1].replace('}', '');
				strFunction = arrParts[2].replace('}', '');
				arrParams = strFunction.split('(');
				strFunction = arrParams[0];
				if (arrParams.length > 1) {
					strParam = arrParams[1].replace(')', '');
				}
				break;
			default: //No se soporta esta expresión, aunque no debería haber aceptado nada similar a este caso
				blnValid = false;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		var strPropertyCS = strProperty;
		strProperty = strProperty.toLowerCase();
		strFunction = strFunction.toLowerCase();
		var strParamCS = strParam;
		strParam = strParam.toLowerCase();
		strParam = strParam.replace(/[\'\"]/gi, '');
		//JAPR 2014-12-09: Corregido un bug, faltaba remover las comillas de este parámetro
		strParamCS = strParamCS.replace(/[\'\"]/gi, '');
		//JAPR
		
		//Hasta este punto ya se tienen identificados los diferentes componentes de la variable, ahora deber ejecutar procesos diferentes según tu tipo
		//Primero obtiene la referencia al objeto del que se trata
		var objQuestion = undefined;
		var objSection = undefined;
		var objAnswer = undefined;
		//Utilizada como objeto polimórfico para obtener las propiedades dinámicamente según el tipo de variable, cuando se traten de propiedades
		//de la definición que no requieran tratamiento especial
		var objObject = undefined;
		var intRecordNumber = 0;
		var intEvaluatedRecordNumber = undefined;
		switch (intType) {
			case ObjectType.otyQuestion:
				if (!selSurvey || !selSurvey.questions || !selSurvey.questionsOrder) {
					blnValid = false;
					break;
				}
				
				var objQuestion = selSurvey.questions[selSurvey.questionsOrder[strQuestionNum-1]];
				if (!objQuestion || !objQuestion.answer || !objQuestion.section) {
					//Si no se encontró la pregunta, continua con la siguiente expresión identificada
					blnValid = false;
					break;
				}
				
				//Si es una pregunta que se debe ignorar, continua con el reemplazo de la siguiente variable
				if (anIgnoreCol[objQuestion.number]) {
					blnValid = false;
					break;
				}
				
				//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
				//Ahora la respuesta a utilizar dependerá del registro indicado si es que se especificó alguno, de lo contrario usa el "actual"
				objSection = objQuestion.section;
				objAnswer = objQuestion.answer;
				strSectionNum = objSection.number;
				
				//JAPR 2014-05-20: Agregado el tipo de sección Inline
				//JAPR 2018-12-10: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
				if (objSection.buildOptions.isMultiRecord && aSectionEvalRecord && aSectionEvalRecord[objSection.id] !== undefined) {
					intRecordNumber = parseInt(aSectionEvalRecord[objSection.id]);
					if (!isNaN(intRecordNumber) && objQuestion.answers[intRecordNumber]) {
						intEvaluatedRecordNumber = intRecordNumber;
						objAnswer = objQuestion.answers[intRecordNumber];
					}
				}
				
				if (!objAnswer) {
					blnValid = false;
					break;
				}
				
				objObject = objQuestion;
				break;
			case ObjectType.otySection:
				if (!selSurvey || !selSurvey.sections || !selSurvey.sectionsOrder) {
					blnValid = false;
					break;
				}
				
				strSectionNum = strQuestionNum;
				var objSection = selSurvey.sections[selSurvey.sectionsOrder[strSectionNum-1]];
				if (!objSection) {
					blnValid = false;
					break;
				}
				
				//JAPR 2014-10-15: Corregido un bug, en el método de reemplazo de variables original, si no estaba visible la sección simplemente
				//continuaba con la siguiente variable, siendo que lo correcto es que regrese vacio por consistencia con el comportamiento de las
				//preguntas que están ocultas
				//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
				//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
				if (!objSection.isVisible(true)) {
					strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), '');
					blnValid = false;
					break;
				}
				
				//JAPR 2018-12-10: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
				if (objSection.buildOptions.isMultiRecord && aSectionEvalRecord && aSectionEvalRecord[objSection.id] !== undefined) {
					intRecordNumber = parseInt(aSectionEvalRecord[objSection.id]);
					if (!isNaN(intRecordNumber) && intRecordNumber < objSection.maxRecordNum) {
						intEvaluatedRecordNumber = intRecordNumber;
					}
				}
				objObject = objSection;
				break;
		}
		
		if (!blnValid) {
			continue;
		}
		
		//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
		//Inicia el procesamiento del dato solicitado para la variable
		//En este punto siempre estará asignada objSection independientemente de si es una variable de pregunta o sección, pero por el contrario
		//objQuestion y objAnswer sólo estarían asignadas si fueran variables de preguntas
		var intEmpty = 0;
		var intSectionType = objSection.type;
		//JAPR 2012-11-21: Corregido un bug, no estaba tomando correctamente el valor de las preguntas abiertas numéricas de catálogo
		//en secciones dinámicas
		var blnDinamicCatalogQuestion = false;
		var blnMasterDetSection = false;
		var blnDinamicSection = false;
		var intSectionMaxAttribNum = 0;
		var intSectionRecordNumber = 0;
		var strPageName = '';
		var objSectionCatalogFilter = {};
		switch (intSectionType) {
			case SectionType.Multiple:
				blnMasterDetSection = true;
				intEmpty = objSection.empty;
				//JAPR 2014-01-16: Corregido un bug, si se está pidiendo el valor de una pregunta de una sección maestro-detalle y se está
				//en ella misma capturando en ese momento, entonces aunque se considere vacía la sección si se debe devolver el valor, ya
				//que se estaría precisamente realizando la captura del primer registro
				if (intEmpty && selSurvey.currentCapture && selSurvey.currentCapture.section) {
					if (selSurvey.currentCapture.section.id == objSection.id) {
						intEmpty = 0;
					}
				}
				
				//JAPR 2014-10-15: Modificado este código porque ahora se puede recibir un registro específico como parámetro
				if (intEvaluatedRecordNumber !== undefined) {
					intSectionRecordNumber = intEvaluatedRecordNumber;
				}
				else {
					intSectionRecordNumber = objSection.recordNum +1;
				}
				//JAPR
				break;
			
			case SectionType.Dynamic:
				//Este proceso es exclusivo para preguntas en secciones dinámicas
				//Se debe validar que sea una variable de pregunta pues sólo aplica para ellas
				if (objQuestion && objQuestion.catalogID == objSection.catalogID && objQuestion.catMemberID) {
					//Por ahora sólo se soportan este tipo de preguntas que provienen del catálogo de la sección dinámica
					if (objQuestion.type = Type.number) {
						blnDinamicCatalogQuestion = true;
					}
				}
				//No hay un break para que continue con el resto de las asignaciones en el case de la inline
				
			//JAPR 2014-05-20: Agregado el tipo de sección Inline
			case SectionType.Inline:
				//Las secciones Inline se consideran como dinámicas para el caso de las variables de sección, así que esta bandera se asigna igual
				//en ambos casos
				blnDinamicSection = true;
				
				//Si se aplicará el filtro de sección dinámica, tiene que obtener el valor de la página que se está procesando para agregarlo
				if (objSection.catalog && objSection.catMemberID) {
					if (objSection.catalog.attributes && objSection.catalog.attributes[objSection.catMemberID]) {
						intSectionMaxAttribNum = objSection.catalog.attributes[objSection.catMemberID].order -1;
					}
				}
				
				//La página dinámica no puede usar el primer atributo, así que se valida que sea por lo menos el 2do en adelante
				//JAPR 2014-05-20: Agregado el tipo de sección Inline
				//Las secciones Inline si pudieran usar el primer atributo, así que se modificó la condición para ese caso
				if (intSectionMaxAttribNum || intSectionType == SectionType.Inline) {
					//Genera el filtro a partir de la página dinámica que se estuviera pintando (si hay alguna), concatenando el valor dinámica
					//específico
					//JAPR 2014-10-15: Modificado este código porque ahora se puede recibir un registro específico como parámetro
					if (intEvaluatedRecordNumber !== undefined) {
						intSectionRecordNumber = intEvaluatedRecordNumber;
					}
					else {
						intSectionRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
					}
					
					objSectionCatalogFilter = $.extend({}, objSection.catalogFilter);
					//Concatena el atributo de la página que se está procesando
					if (objSection.dynamicValues && objSection.dynamicValues[intSectionRecordNumber]) {
						strPageName = objSection.dynamicValues[intSectionRecordNumber].value;
						//JAPR 2014-11-12: Corregido un bug, si en este punto la sección inline (o dinámica, aunque con estás últimas no se
						//podría dar porque siempre dependen de una pregunta inicial) hubiese sido filtrada dinámicamente y de hecho no depende
						//de ninguna pregunta previa del mismo catálogo, no trae un objSection.catalogFilter por lo que al asignar únicamente
						//el atributo específico del dynamicValues solicitado, estaría dejando huecos en el resto de los filtros así que al
						//invocar a la función getFirstValueForAttributeNum para obtener un atributo específico, se iría por la primer rama
						//posible y al llegar a este atributo no coincidiría el valor, así que termina regresando vacio. Lo que se tiene que hacer
						///es completar todos los atributos que pudieran estar faltando según la rama del dynamicValues solicitado
						if (objSection.dynamicFilters && objSection.dynamicFilters[intSectionRecordNumber]) {
							var arrKeys = Object.keys(objSection.dynamicFilters[intSectionRecordNumber]);
							var intNumAttribs = arrKeys.length;
							//JAPR 2014-12-09: Corregido un bug, se estaba usando la posición dentro del array del filtro, siendo que debe
							//ser la posición del atributo que se usó en esa posición dentro del array
							for (var intAttribPos = 0; intAttribPos < intNumAttribs && intAttribPos < intSectionMaxAttribNum; intAttribPos++) {
								var intAttribNum = arrKeys[intAttribPos];
								//JAPR
								if (objSectionCatalogFilter[intAttribNum] === undefined) {
									//En este caso este atributo no tenía un filtro, por lo tanto debe tomar exactamente el filtro del mismo
									//atributo del registro que se está procesando (sólo podría no tener un filtro para el atributo si se
									//hubieran saltado registros o bien no hubiera pregunta generadora de la sección)
									objSectionCatalogFilter[intAttribNum] = objSection.dynamicFilters[intSectionRecordNumber][intAttribNum];
								}
							}
						}
						//JAPR
						objSectionCatalogFilter[intSectionMaxAttribNum] = strPageName;
					}
					intSectionRecordNumber++;
				}
				break;
		}
		
		//Si se aplicará el filtro de sección dinámica, tiene que obtener el valor de la página que se está procesando para agregarlo
		var intMaxAttribNum = 0;
		//Esta es la colección de filtros que se debe usar, ya está ajustada al tipo de objeto de la variable
		var objObjectCatalogFilter = {};
		//Se debe validar que sea una variable de pregunta pues sólo aplica para ellas
		if (objQuestion) {
			//En este caso tiene que ser una variable de pregunta, así que se usa el filtro específico de la pregunta
			if (blnDinamicCatalogQuestion && objQuestion.catalog && objSection.catMemberID) {
				if (objQuestion.catalog.attributes && objQuestion.catalog.attributes[objSection.catMemberID]) {
					intMaxAttribNum = objQuestion.catalog.attributes[objSection.catMemberID].order -1;
				}
			}
			
			//La página dinámica no puede usar el primer atributo, así que se valida que sea por lo menos el 2do en adelante
			var objCatalogFilter = undefined;
			if (blnDinamicCatalogQuestion && intMaxAttribNum) {
				//Genera el filtro a partir de la página dinámica que se estuviera pintando (si hay alguna), concatenando el valor dinámica
				//específico
				//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
				if (intEvaluatedRecordNumber !== undefined) {
					//Se usará el registro especificado en caso de haber información de contexto de evaluación
					var intRecordNumber = intEvaluatedRecordNumber;
				}
				else {
					//De lo contrario se usará el registro considerado como el "actual"
					var intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
				}
				//JPAR
				
				var objCatalogFilter = $.extend({}, objSection.catalogFilter);
				//Concatena el atributo de la página que se está procesando
				if (objSection.dynamicValues && objSection.dynamicValues[intRecordNumber]) {
					objCatalogFilter[intMaxAttribNum] = objSection.dynamicValues[intRecordNumber].value;
				}
			}
			else {
				//En este caso utiliza directamente el filtro de la pregunta
				var objCatalogFilter = $.extend({}, objAnswer.catalogFilter);
			}
			objObjectCatalogFilter = objCatalogFilter;
		}
		else {
			//En este caso tiene que ser una variable de sección, así que se usa el filtro de sección si es que hay alguno
			objObjectCatalogFilter = objSectionCatalogFilter;
		}
		
		//Obtiene el valor solicitado
		//Se basará en el dato especificado en strProperty, si se hubiera solicitado una función entonces strProperty se hubiera igualado a
		//strFunction y strParam sería el dato solicitado en strProperty como la información a obtener con esa función
		//Esta variable indica si el dato proviene de la pregunta y por tanto se puede hacer quote mediante su propio método, o bien si es un dato
		//obtenido a partir de un cálculo, atributo (del cual no sabemos el tipo) u otro origen y por tanto el quote depende sólo de si es o no
		//un valor numérico o string
		var blnIsCalculatedValue = false;
		//JAPR 2015-09-29: Corregido un bug, los scores no se estaban considerando numéricos al aplicar bQuoteAnswers (#X7ZU1E)
		var blnIsAttribute = false;
		try {
			switch (strProperty) {
				//JAPR 2016-01-22: Corregido un bug del count para preguntas, después de analizar el tema con LRoux y CGil, se determinó que si se pide el
				//count de una pregunta, se está refiriendo a las rows con respuesta de dicha pregunta, mientras que si se pide el count de una sección, se refiere
				//a las rows marcadas con el selector (en los casos donde esté visible) de dicha sección, y se agregará rows para determinar la cantidad de rows
				//de la sección sin importar si están o no habilitadas con el selector
				case 'rows':
					break;
				//JAPR
				case 'sum':
				//JAPR 2013-08-30: Agregados mas tipos de cálculos
				case 'avg':
				case 'max':
				case 'min':
				case 'count':
					//En todos los cálculos se asume que se trata de una sección multi-registro, así que la visibilidad es por cada registro
					blnIsCalculatedValue = true;
					
					//JAPR 2016-01-22: Corregido un bug del count para preguntas, después de analizar el tema con LRoux y CGil, se determinó que si se pide el
					//count de una pregunta, se está refiriendo a las rows con respuesta de dicha pregunta, mientras que si se pide el count de una sección, se refiere
					//a las rows marcadas con el selector (en los casos donde esté visible) de dicha sección, y se agregará rows para determinar la cantidad de rows
					//de la sección sin importar si están o no habilitadas con el selector
					/*if (intType == ObjectType.otySection && strProperty == 'count') {
						//Si el objeto es una sección y la función es count, el comportamiento es ligeramente diferente
						break;
					}
					*/
					
					//JAPR 2014-11-11: Corregido un bug, si no viene un parámetro indicado, el default es value cuando se pide una función
					if (!strParam) {
						strParam = 'value';
					}
					//JAPR
					
					//Se pueden aplicar funciones a datos específicos de las preguntas o atributos, así que dependiendo de que se pida es lo que se
					//va a procesar
					switch (strParam) {
						case 'value':
							//Se debe validar que sea una variable de pregunta pues sólo aplica para ellas
							if (!objQuestion) {
								break;
							}
							//No hace un break para que continue con el código, ya que es el mismo cálculo para score que para value
							
						case 'score':
							//La suma del score puede ser calculado tanto para preguntas como para secciones, en el caso de preguntas suma el score
							//de esta para todos los registros, en el caso de las secciones suma el score de todas sus preguntas para todos sus
							//registros
							
							//Si se pide calcular el score y no es una pregunta que lo soporte, entonces no tiene razón para continuar
							var blnGetScore = (strParam == 'score');
							if (blnGetScore && objQuestion && objQuestion.type != Type.simplechoice && objQuestion.type != Type.multiplechoice) {
								break;
							}
							
							var dblValue = 0;
							var dblMax = undefined;
							var dblMin = undefined;
							var intNumValues = 0;
							//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
							//En este caso cualquiera de estos parámetros sólo aplica para secciones con múltiples registros, así que se
							//valida directamente la visibilidad por registro, previamente verificando que la sección esté visible
							//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
							//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
							if (objSection.isVisible && objSection.isVisible(true) && !intEmpty) {
								//JAPR 2014-05-20: Agregado el tipo de sección Inline
								//JAPR 2014-10-15: No tiene caso validar si es o no multi-record, simplemente recorrerá la única respuesta si no lo es
								for (var intRecordNumber = 0; intRecordNumber < objSection.maxRecordNum; intRecordNumber++) {
									if (objQuestion && !objQuestion.isVisible(intRecordNumber)) {
										continue;
									}
									
									//JAPR 2013-08-30: Corregido un bug, si no se puede resolver a un número, debe sumar 0
									//JAPR 2014-10-11: Corregido un bug, no se debe usar el answer directo sino la función getAnswer para
									//permitir aplicar valores default encadenados
									//JAPR 2014-10-15: Agregado el cálculo en base al score
									var strAnswer = undefined;
									if (blnGetScore) {
										if (objObject.getScore) {
											var strAnswer = objObject.getScore(intRecordNumber);
										}
									}
									else {
										var strAnswer = objQuestion.getAnswer(intRecordNumber);
									}
									
									var dblTempVal = parseFloat(strAnswer);
									//JAPR
									if (isNaN(dblTempVal)) {
										dblTempVal = 0;
										//JAPR 2015-04-23: Corregido un bug, no debe hacer un continue cuando se trata de count porque debe
										//considerar el total de opciones posibles independientemente de su valor, ya que para llegar aquí ya
										//se comprobó que si está contestada, simplemente no es un número pero eso no importa
										//JAPR 2016-01-22: Corregido un bug del count para preguntas, después de analizar el tema con LRoux y CGil, se determinó que si se pide el
										//count de una pregunta, se está refiriendo a las rows con respuesta de dicha pregunta, mientras que si se pide el count de una sección, se refiere
										//a las rows marcadas con el selector (en los casos donde esté visible) de dicha sección, y se agregará rows para determinar la cantidad de rows
										//de la sección sin importar si están o no habilitadas con el selector
										//if (strProperty != 'count') {
										//JAPR 2016-01-22: Si el objeto es una pregunta y esta NO es numérica o calculada y se pidió el count significa que se puede continuar para que
										//contabilice el registro con cualquier valor que este tenga, no se tiene que salir en caso de que el valor sea NaN
										if (intType == ObjectType.otyQuestion && strProperty == 'count') {
											if (objQuestion.type == Type.number || objQuestion.type == Type.calc) {
												//Como es una pregunta que requiere número, se está pidiendo el count y el valor que tiene no es número, no puede contabilizar así que
												//sale del ciclo
												continue;
											}
											else {
												//En cualquier otro caso sólo contabilizaría si la respuesta no hubiera sido vacía, así que si lo fue también sale del ciclo (un valor
												//de 0 NO cuenta como respuesta vacía, ya que pudo ser un default que se resolvió a 0 en una alfanumérica o algo así)
												if (!strAnswer && (strAnswer === undefined || strAnswer === '' || strAnswer === null)) {
													continue;
												}
											}
										}
										else {
											//Cualquier otro caso (no es una pregunta o no se pidió el count) por ahora requiere un número, así que al no ser un número no puede
											//contabilizar y simplemente brincará a la siguiente iteración
											continue;
										}
										//JAPR
									}
									
									dblValue += dblTempVal;
									//JAPR 2013-08-30: Agregados mas tipos de cálculos
									if (dblMax === undefined || dblTempVal > dblMax) {
										dblMax = dblTempVal;
									}
									if (dblMin === undefined || dblTempVal < dblMin) {
										dblMin = dblTempVal;
									}
									intNumValues++;
									//JAPR
								}
							}
							
							//JAPR 2013-08-30: Agregados mas tipos de cálculos
							switch (strProperty) {
								case 'avg':
									if (intNumValues) {
										strValue = dblValue / intNumValues;
									}
									else {
										strValue = 0;
									}
									break;
									
								case 'max':
									strValue = (dblMax === undefined)?'':dblMax;
									break;
									
								case 'min':
									strValue = (dblMin === undefined)?'':dblMin;
									break;
								
								case 'count':
									strValue = intNumValues;
									break;	
								
								case 'sum':
								default:
									strValue = dblValue;
									break;
							}
							//JAPR
							break;
						
						default:
							//JAPRWarning: Falta implementar esto:
							//En este caso se asumirá que se trata de algún atributo del catálogo si es que es una pregunta de catálogo o bien es una
							//sección dinámica o Inline, ya que de lo contrario no aplicaría
							break;
					}
					break;
					
				default:
					//En este caso cuando se pide un dato específico, la visibilidad es a nivel de pregunta en el registro solicitado explícitamente o
					//bien en el registro activo, así que se verifica si está o no visible antes de asignar el dato
					//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
					//Dependiendo de la sección, obtiene la visibilidad del registro actual o simplemente de la pregunta, si no está
					//visible entonces no se obtendrá su valor
					var blnVisible = true;
					//Se debe validar que sea una variable de pregunta pues sólo aplica para ellas
					//en este caso si fuera una variable de sección ya se habría validado su visibilidad desde mucho antes
					if (objQuestion) {
						switch (intSectionType) {
							case SectionType.Dynamic:
							case SectionType.Multiple:
							//JAPR 2014-05-20: Agregado el tipo de sección Inline
							case SectionType.Inline:
								//Estas secciones validarán primero que sean ellas mismas visibles y tengan por lo menos un registro, si es así
								//posteriormente valida la visibilidad de la pregunta en el registro actual, ya que la función no permite
								//especificar un registro y pedir por estas preguntas desde secciones estándar resulta ilógico, así que sólo se
								//pueden procesar realmente bien si se piden del registro actualmente evaluado
								//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
								//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
								if (!objSection.isVisible || !objSection.isVisible(true) || intEmpty) {
									blnVisible = false;
								}
								else {
									//La sección es visible, pero ahora se basa en la visibilidad de la pregunta dentro del registro actual en la
									//sección para determinar si debe o no usar la respuesta
									//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
									if (intEvaluatedRecordNumber !== undefined) {
										//Se usará el registro especificado en caso de haber información de contexto de evaluación
										var intRecordNumber = intEvaluatedRecordNumber;
									}
									else {
										//De lo contrario se usará el registro considerado como el "actual"
										var intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
									}
									//JAPR
									
									if (!objQuestion.isVisible || !objQuestion.isVisible(intRecordNumber)) {
										blnVisible = false;
									}
									//JAPR 2015-01-15: Validado que las preguntas simple choice de catálogo, las cuales deben tener una respuesta
									//siempre capturada, se encuentren efectivamente contestadas ya que el proceso que extrae sus atributos no
									//se basa en su respuesta sino en su filtro, lo cual no siempre se asigna a la par (#2X267B)
									else {
										if (objQuestion.type == Type.simplechoice && objQuestion.catalogID > 0 && objQuestion.answers) {
											//Esta validación sólo aplica para cuando se pide un atributo, ya que la respuesta regresaría vacía
											//de todas formas si lo estuviera así que puede continuar, y si podemos pedir comentarios u otros datos
											//JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
											//OMMC 2016-05-26: Agregado attrdid para que el reemplazo sólo ocurra cuando la pregunta no esté vacía
											if ((strProperty == 'attr' || strProperty == 'attrid' || strProperty == 'attrdid') && objQuestion.answers[intRecordNumber] && objQuestion.answers[intRecordNumber].isEmpty) {
												//JAPR 2019-09-23: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
												//Corregido un bug, con el cambio de comportamiento, la pregunta ahora puede tener internamente un valor vacío pero estar considerada contestada debido
												//a su defaultValue o bien a la configuración de auto-seleccionar el primer valor, por lo que debe validar no solo contra el isEmpty sino contra esa
												//función pero considerando el defaultValue
												if (objQuestion.answers[intRecordNumber].isEmpty(undefined,undefined,true)) {
													//Las preguntas simple choice de catálogo originalmente no podían tener un valor default, así
													//que es seguro preguntar directamente si está vacía sin considerar defaults
													blnVisible = false;
												}
											}
										}
									}
									//JAPR
								}
								break;
							
							default:
								//Cualquier otra sección sólo contiene una respuesta, por lo que valida directamente con el Question para saber
								//si debe o no agregarla
								if (!objQuestion.isVisible || !objQuestion.isVisible()) {
									blnVisible = false;
								}
								//JAPR 2015-01-15: Validado que las preguntas simple choice de catálogo, las cuales deben tener una respuesta
								//siempre capturada, se encuentren efectivamente contestadas ya que el proceso que extrae sus atributos no
								//se basa en su respuesta sino en su filtro, lo cual no siempre se asigna a la par (#2X267B)
								else {
									if (objQuestion.type == Type.simplechoice && objQuestion.catalogID > 0 && objQuestion.answer) {
										//Esta validación sólo aplica para cuando se pide un atributo, ya que la respuesta regresaría vacía
										//de todas formas si lo estuviera así que puede continuar, y si podemos pedir comentarios u otros datos
										//JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
										//OMMC 2016-05-26: Agregado attrdid para que el reemplazo sólo ocurra cuando la pregunta no esté vacía
										if ((strProperty == 'attr' || strProperty == 'attrid' || strProperty == 'attrdid') && objQuestion.answer.isEmpty) {
											//JAPR 2019-09-23: Modificado el comportamiento de las preguntas SChoice Menú, ahora ya podrán generarse sin valor automáticamente seleccionado (#86JRZY)
											//Corregido un bug, con el cambio de comportamiento, la pregunta ahora puede tener internamente un valor vacío pero estar considerada contestada debido
											//a su defaultValue o bien a la configuración de auto-seleccionar el primer valor, por lo que debe validar no solo contra el isEmpty sino contra esa
											//función pero considerando el defaultValue
											if (objQuestion.answer.isEmpty(undefined,undefined,true)) {
												//Las preguntas simple choice de catálogo originalmente no podían tener un valor default, así
												//que es seguro preguntar directamente si está vacía sin considerar defaults
												blnVisible = false;
											}
										}
									}
								}
								//JAPR
								break;
						}
					}
					
					//Si no está visible la pregunta, entonces no puede devolver un valor
					if (!blnVisible) {
						break;
					}
					
					//En este caso si es visible así que obtiene el dato especificado
					//Se debe validar que sea una variable de pregunta pues sólo aplica para ellas en algunas propiedades, las que son compartidas
					//utilizarán al objObject
					switch (strProperty) {
						case 'comment':
						case 'days':
						case 'value':
						//case 'score':
							//En todos estos casos debe ser una variable de pregunta para que apliquen, así que se limpia strProperty si no lo es
							if (!objQuestion) {
								strProperty = 'undefined';
							}
							break;
					}
					
					switch (strProperty) {
						case 'undefined':
							//En este caso se limpió la propiedad por ser una variable que no logró cargar la instancia necesaria, así que al entrar
							//aquí simplemente no regresará valor, esto es para no tener que validar contra la instancia en cada uno de los case
							//posteriores
							break;
							
						case 'comment':
							strValue = objAnswer.comment;
							break;
							
						case 'days':
							strValue = objAnswer.daysDueDate;
							break;
							
						case 'value':
							//JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
							if ( objQuestion ) {
								if ( objQuestion.type == Type.sketchPlus ) {
									//JAPR 2019-04-17: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
									//Corregido un bug, se documentó que el value de esta pregunta sería la imagen, así que estaba mal resuelta esta variable
									//Sólo aplicable para Sketch+, la imagen que contiene los trazos realizados
									strValue = objAnswer.canvasPhoto;
								}
								else {
									//JAPR 2012-10-16: Agregado el valor default hasta que se edita la pregunta
									if (objQuestion.type == Type.number && objQuestion.catMemberID && initBeforeShow) {
										var strAttribNum = objQuestion.catalog.attributes[objQuestion.catMemberID].order;
										var strValueTemp = "{@Q" + objQuestion.number + "(" + strAttribNum + ")}";
										objQuestion.defaultValue = strValueTemp;
									}
									
									//No solicita que regrese 0s en caso de numéricas ni tampoco pide formatos ya que esta misma función va
									//a aplicarlo al final de ser necesario
									//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba
									//siempre el último registro navegado
									//JAPR 2014-10-15: Por alguna razón se había utilizado getDefaultValue, pero lo correcto es utilizar getAnswer
									//strValue = objQuestion.getDefaultValue(intEvaluatedRecordNumber);
									strValue = objQuestion.getAnswer(intEvaluatedRecordNumber);
								}
							}
							break;
							
						case 'score':
							//JAPR 2015-09-29: Corregido un bug, los scores no se estaban considerando numéricos al aplicar bQuoteAnswers (#X7ZU1E)
							blnIsCalculatedValue = true;
							//JAPR
							//No solicita que regrese 0s en caso de numéricas ni tampoco pide formatos ya que esta misma función va
							//a aplicarlo al final de ser necesario
							if (objObject.getScore) {
								var strAnswer = objObject.getScore(intEvaluatedRecordNumber);
								var dblTempVal = parseFloat(strAnswer);
								if (isNaN(dblTempVal)) {
									dblTempVal = 0;
								}
								
								strValue = dblTempVal;
							}
							break;
							
						case 'attr':
						//JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
						//Agregados los attrDID que vienen siendo los DataSourceMemberIDs
						case 'attrid':
						case 'attrdid':
							var blnLookForID = false;
							if (strProperty == 'attrid' || strProperty == 'attrdid') {
								var blnLookForID = true;
							}
							//JAPR
						
							//Esta propiedad se puede pedir tanto a preguntas como a secciones, así que se debe usar objObject
							blnIsCalculatedValue = true;
							blnIsAttribute = true;
							//Se está solicitando un atributo específico de la pregunta, por lo que utiliza el strParam
							//Verifica si se trata de un número, en ese caso hace referencia al número de atributo de esta pregunta
							if (objObject.catalog && objObject.catalog.attributes && objObject.catalog.attributesOrder) {
								if (isNaN(strParam)) {
									//Se está definiendo un nombre de atributo, así que lo busca por su etiqueta
									var intAttributeNum = undefined;
									for (var intAttribNum in objObject.catalog.attributesOrder) {
										var intAttribID = objObject.catalog.attributesOrder[intAttribNum];
										var objAttribute = objObject.catalog.attributes[intAttribID];
										if (objAttribute && objAttribute.name == strParamCS) {
											intAttributeNum = objAttribute.order;
											break;
										}
									}
									
									//Regresa el valor del atributo indicado de esta respuesta
									if (intAttributeNum) {
										strValue = objObject.catalog.getFirstValueForAttributeNum(undefined, 0, objObjectCatalogFilter, intAttributeNum);
									}
								}
								else {
									//Se está definiendo como un número de atributo, así que utiliza directamente esa posición de la colección de atributos
									var intAttributeNum = parseInt(strParam);
									//JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
									var blnValidAttribute = true;
									if (blnLookForID) {
										//En este caso el número realmente corresponde a un ID de atributo, así que obtiene la posición antes de continuar
										//En este caso intAttributeNum representa un ID de atributo en este punto, así que se busca la referencia directa por él para obtener el número
										//Agregados los DataSourceMemberIDs
										if(strProperty == 'attrdid'){
											var blnAttrFound = false;
  											for(var intAttrNum in objObject.catalog.attributes) {
  												var objAttribute = objObject.catalog.attributes[intAttrNum];
											  	if (objAttribute && objAttribute.sourceMemberID == intAttributeNum) {
												   	blnAttrFound = true;
												   	break;
											  	}
  											}
  											if(!blnAttrFound){
  												objAttribute = undefined;
  											}
										}else{
											var objAttribute = objObject.catalog.attributes[intAttributeNum];
										}
										//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
										if (objAttribute) {
											intAttributeNum = objAttribute.order;
										}
										else {
											//No se encontró el atributo por ID, así que no regresará valor para esta variable
											blnValidAttribute = false;
										}
									}
									
									//Regresa el valor del atributo indicado de esta respuesta
									//JAPR 2016-01-15: Agregada la propiedad attrID para buscar atributos por ID en lugar de posición o nombre (#MKM198)
									//Ahora verificará si el atributo es válido para cuando se pida por ID y el ID especificado ya no esté en el catálogo
									if (blnValidAttribute && objObject.catalog && objObject.catalog.getFirstValueForAttributeNum) {
										strValue = objObject.catalog.getFirstValueForAttributeNum(undefined, 0, objObjectCatalogFilter, intAttributeNum);
									}
									//JAPR
								}
							}
							break;
							
						case 'pagename':
							strValue = strPageName;
							break;
							
						case 'pagenumber':
							//JAPR 2015-09-29: Corregido un bug, los scores no se estaban considerando numéricos al aplicar bQuoteAnswers (#X7ZU1E)
							blnIsCalculatedValue = true;
							//JAPR
							if (blnMasterDetSection || blnDinamicSection) {
								strValue = intSectionRecordNumber;
							}
							else {
								strValue = 0;
							}
							break;
						
						//JAPR 2019-03-08: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
						//Se habilitarán variables para permitir obtener la ruta y nombre del archivo de imagen asociado a Sketch+
						case 'image':
							//Sólo aplicable para Sketch+, la imagen original usada para generar el Sketch
							//JAPR 2019-04-17: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
							//Corregido un bug, cuando se utiliza la propiedad imageSketch, viene preparada como un <img> así que no se puede utilizar directamente en browser sin reemplazar
							//la porción correspondiente al <img>
							if (objAnswer.photo) {
								strValue = objAnswer.photo;
							}
							else {
								var tmp = ("'"+objQuestion.imageSketch+"'").match(/(img|src)\=(\"|\')[^\"\'\>]+/g);
								if ( tmp ) {
									tmp = tmp + '"'; //agregamos la ultima comilla ya que la quita el match
									var tmp2 = tmp.replace("src=","");
									tmp2 = tmp2.replace('"',''); //quitamos las comillas
									tmp2 = tmp2.replace('"',''); 
									strValue = tmp2;
								}
							}
							//JAPR
							break;
							
						case 'sketch':
							//Sólo aplicable para Sketch+, la imagen que contiene los trazos realizados
							strValue = objAnswer.canvasPhoto;
							break;
							
						case 'strokes':
							//Sólo aplicable para Sketch+, los trazos como vectores, en caso de no estar contestada siempre será vacío. No se usará getAnswer como con otras preguntas
							//para obtener el valor, porque para el Sketch original se podía configurar un defaultValue que se resolvía a la imagen que debía usar, y getAnswer regresaría
							//el defaultValue en caso de que eventualmente se configurara algo similar para Sketch+
							strValue = objAnswer.answer;
							break;
						//JAPR
						//MAPR 2019-08-19: Se añaden atributos de id y number para ser usadas en preguntas y secciones en el editor de formulas (#DW8IC2)
						case 'id':
							switch (intType) {
								case ObjectType.otyQuestion:
									strValue = objAnswer.id;
									break;
								case ObjectType.otySection:
									strValue = objObject[strPropertyCS];
									if (strValue === undefined) {
										strValue = '';
									}
									break;
							}
							break;
						case 'number':
							switch (intType) {
								case ObjectType.otyQuestion:
									strValue = objAnswer.number;
									break;
								case ObjectType.otySection:
									strValue = objObject[strPropertyCS];
									if (strValue === undefined) {
										strValue = '';
									}
									break;
							}
							break;
						default:
							//En este caso va a pedir alguna propiedad directamente de la instancia del objeto que corresponde con la variable en
							//lugar de pedir una propiedad de su respuesta o página o algo que se genere en forma dinámica, por tanto se pide directo
							//al objeto pero en caso de no encontrarse se regresará vacio
							strValue = objObject[strPropertyCS];
							if (strValue === undefined) {
								strValue = '';
							}
							break;
					}
					break;
			}
			
			//JAPR 2014-10-10: Agregada la evaluación del valor default para permitir datos resultado de fórmulas complejas
			//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
			//Si la pregunta no estaba visible entonces debería venir completamente vacio, pero si fuera una pregunta dentro de sección
			//con múltiples registros y se pidió un valor de función, entonces traera un 0 (aunque técnicamente hablando no esté visible
			//la pregunta, ya que esos valores generalmente se utilizaban para cálculos así que se regresaba siempre el 0), además habrá
			//casos en los que se pidan datos de atributos del catálogo por lo que no sabemos si esperamos o no un número, por lo tanto
			//la validación se reducirá a verificar si viene vacio o no, si lo está entonces se delimitará (si es que así se pidió), de
			//lo contrario se verificará si el valor es o no un número, si lo es entonces NO se delimitará porque no se sabe si realmente
			//debía ser o no un valor numérico o si es un comentario donde sólo capturaron un número (por tanto debería ser un texto), pero
			//como javascript aplica conversiones de tipos automáticas según los operandos, si se esperaba un texto en lugar del número y
			//se utiliza el operador + entonces si hará la concatenación incluso si aquí se regresa como número, contrario a que si lo
			//convertimos a cadena cuando el otro operando era un número, ya que en ese caso no se sumarían
			//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
			//JAPR 2015-09-23: Modificada la función para aplicar correctamente los parámetros recibidos, ya que algunas combinaciones no se estaban considerando
			//if (bQuoteAnswers) {
			//Si no hay valor, entonces depende de si se pidió valor numérico (se regresará 0) o si se pidió delimitar (regresará ''). Sólo aplica conversión a número si la pregunta
			//es de un tipo que así lo permita siempre, en caso contrario regresará vacio (sin comillas por haberse pedido como número)
			//strValue 											//Valor delimitado y formateado
			var strUnformattedValue = strValue;					//Valor delimitado pero no formateado
			var strNonDelimitedValue = strValue;				//Valor no delimitado pero formateado
			var strNonDelimitedUnformattedValue = strValue;		//Valor no delimitado ni formateado
			if (Trim(strValue) == '') {
				//JAPR 2015-09-29: Corregido un bug, los scores no se estaban considerando numéricos al aplicar bQuoteAnswers (#X7ZU1E)
				//Se agregó a la condición el caso en el que se trata de un valor numérico pero que no sea un atributo, ya que si se esperaba un número entonces no puede utilizar vacío
				//porque intentaría concatenación en lugar de suma en ciertos casos, sin embargo si era un atributo, el default es texto a menos que explícitamente se trate de un
				//número, y en este caso está vacío así que se considera un texto
				if (bReturnNumericDefault || (blnIsCalculatedValue && !blnIsAttribute)) {
					//Si se pide un valor numérico, lo regresa como 0 en caso de ser una pregunta que se resuelve a números, vacío (no delimitado) en cualquier otro caso
					if (objQuestion) {
						switch (objQuestion.type) {
							case Type.number:
							case Type.calc:
								strValue = 0;
								strUnformattedValue = 0;
								strNonDelimitedValue = 0;
								strNonDelimitedUnformattedValue = 0;
								break;
							
							//JAPR 2016-05-12: Corregido un bug, si se activaba el parámetro bReturnNumericDefault, el cual generalmente (aunque no siempre) viene activado con el parámetro
							//bQuoteAnswers, al entrar primero por bReturnNumericDefault estaba impidiendo que se pudiera hacer un quote de los textos que eran necesarios, por lo que
							//literalmente quedaba un espacio vacío para ciertas variables que debían haber regresado un texto vacío como '', así que si no es una pregunta numérica cuando
							//se trata de este caso, se verificará si es o no necesario el quote del vacío
							default:
								if (bQuoteAnswers) {
									if (objQuestion && objQuestion.quote) {
										//En este caso se delimita sólo si el tipo de pregunta así lo requiere
										strValue = objQuestion.quote(strValue);
									}
									else {
										//No es una variable de pregunta sino de sección, todo se puede delimitar directamente
										strValue = "''";
									}
									strUnformattedValue = strValue;
								}
								break;
							//JAPR
						}
					}
				}
				else if (bQuoteAnswers) {
					if (objQuestion && objQuestion.quote) {
						//En este caso se delimita sólo si el tipo de pregunta así lo requiere
						strValue = objQuestion.quote(strValue);
					}
					else {
						//No es una variable de pregunta sino de sección, todo se puede delimitar directamente
						strValue = "''";
					}
					strUnformattedValue = strValue;
				}
			}
			else {
				//Si se aplicó un formato y se pidió hacer quote de las respuestas, entonces independientemente del tipo de pregunta
				//se debe forzar a que se haga el quote, por lo que manualmente se agregan los delimitadores (se espera que todo número formateado es un string, así que es requerido)
				var isAlreadyFormated = false;
				if (anApplyFormat && objQuestion && objQuestion.format) {
					//JAPR 2016-01-21: Corregidas las variables, strNonDelimitedValue en efecto no estaba delimitada pero no se estaba formateando, además strNonDelimitedUnformattedValue 
					//en efecto no estaba delimitada pero si formateada
					//JAPR 2016-06-17: Corregido un bug, no estaba aplicando el formateo de la fecha en ese tipo de preguntas, sino siempre numérica (#6TC5Q7)
					if (objQuestion.type == Type.date) {
						strValue = formatDate(strValue.replace(/-/g,""), objQuestion.format);
					}
					else {
						strValue = formatNumber(strValue, objQuestion.format);
					}
					//JAPR
					strNonDelimitedValue = strValue;
					//strNonDelimitedUnformattedValue = strValue;
					//JAPR
					isAlreadyFormated = true;
					if (bQuoteAnswers) {
						strValue = "'" + strValue + "'";
					}
					//JAPR 2016-01-21: Corregidas las variables, strNonDelimitedValue en efecto no estaba delimitada pero no se estaba formateando, además strNonDelimitedUnformattedValue 
					//en efecto no estaba delimitada pero si formateada
					//Debido a la corrección de arriba, ya no puede venir de la misma variable que antes (strNonDelimitedValue) porque ya estaría formateada
					strUnformattedValue = "'" + strUnformattedValue + "'";
					//JAPR
				}
				else if (blnIsCalculatedValue) {
					if ($.isNumeric(strValue)) {
						//Es un número, no aplicará delimitación
					}
					else {
						//No es un número, así que delimitará lo que sea que se regresó (en este punto debería generarse un error realmente, pues se pidió como número)
						//JAPR 2015-09-29: Corregido un bug, los scores no se estaban considerando numéricos al aplicar bQuoteAnswers (#X7ZU1E)
						//Los casos de atributos entrarían por este else, en ese caso NO se trata de un error, así que se puede validar con blnIsAttribute para descartarlos
						strValue = "'" + strValue + "'";
						strUnformattedValue = strValue;
					}
				}
				else if (bQuoteAnswers) {
					if (objQuestion && objQuestion.quote) {
						//En este caso se delimita sólo si el tipo de pregunta así lo requiere
						strValue = objQuestion.quote(strValue);
					}
					else {
						//No es una variable de pregunta sino de sección, todo se puede delimitar directamente
						strValue = "'" + strValue + "'";
					}
					strUnformattedValue = strValue;
				}
			}
			//}
		}
		catch(e) {
			//No importa el error, pero si ocurre debe dejar el string original
			var strValue = strExpr;
			var strUnformattedValue = strExpr;
			var strNonDelimitedValue = strExpr;
			var strNonDelimitedUnformattedValue = strExpr;
		}
		//JAPR
		
		if (strValue != strExpr) {
			//JAPR 2015-09-22: Modificado el comportamiento de bQuoteAnswers para que la decisión de delimitar o no la variable dependa del tipo de pregunta
			//En este punto pueden suceder una de dos cosas:
			if (bQuoteAnswers) {
				/*
				1- Si se pidió auto-delimitar variables, significaría que es un string de una fórmula o expresión (esto incluye defaults que utilizan el operador "=" al inicio para definirlos
					como expresión), así que se deben reemplazar las variables sólo delimitándolas siempre y cuando no se encuentren previamente delimitadas, las que ya lo estaban se
					reemplazarán sin delimitación, por ejemplo:
							=({$Q1.value} == 'Si')?'El valor de la variable es: $Q1.value':'No aplica'
						En ese caso la parte entre () se auto-delimitará (asumiendo que es un tipo de pregunta que lo requiere) prorque no está delimitada, pero la parte del ? no se 
						auto-delimitará porque intencionalmente ya se delimitó en la expresión
					En ninguno de estos casos se usarán variables formateadas, ya que los cálculos se deberían hacer sobre los valores reales de los números / atributos
				*/
				//Remplaza primero las NO delimitadas por el valor delimitado
				strText = strText.replace(objRegExpQuoted, function(sMatch, sGroup, iPos) {
					var strResult = '';
					if (sGroup == undefined ) {
						strResult = sMatch;
					}
					else {
						//Tiene que usar una expresión regular porque no necesariamente corresponde con la variable que se está procesando en este punto, si se identifica una manera
						//mas eficiente de reutilizar objRegExpQuoted para cambiar la última parte por una variable específica, eso sería óptimo para reducir reemplazos
						strResult = sGroup.replace(new RegExp(RegExp.quote(strExpr), "gi"), strUnformattedValue);
					}
					
					return strResult;
				});
				
				//A continuación reemplaza las delimitadas por el valor no delimitado, esto es, todas las que quedaran aún que no se hubieran reemplazado en la expresión previa
				strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strNonDelimitedUnformattedValue);
			}
			else {
				/*
				2- Si no se pidió auto-delimitar variables, significaría que es un String de un defaultValue, nombre o cualquier cosa que NO es una expresión, pero también puede ser un String
					que represente un HTML, así que debe buscar por tags <script></script> para aplicar auto-delimitación en dichos tags y sólo reemplazar sin delimitar todas las variables 
					que estén fuera de ellos, por ejemplo:
							Esto es un HTML que generará una tabla para la tienda {$Q1.value}
							<script>
							//Esta es la parte que genera la tabla, una porción del código es esto
							function GetProductName() {
								var strProductName = {$Q2.value};
								return strProductName + 'Has a price in {$Q3.value}';
							}
							</script>
						En ese caso la primera línea es un texto plano, reemplaza sin delimitar pues realmente no importa, sin embargo toda la parte dentro del tag <script> se verificaráabove
						para auto-delimitación, siendo necesario delimitar sólo la variable $Q2 ya que esa no estaba previamente delimitada, mientras que $Q3 si lo estaba, si no se delimitara
						$Q2 generaría un error de JavaScript
				*/
				var arrScripts = strText.match(/(<[\s\S]*script[\s\S]*?>[\s\S]*?<\/[\s\S]*script[\s\S]*>)/gi);
				if (arrScripts && $.isArray(arrScripts)) {
					//En este caso si existen substrings de scripts, debe reemplazar en cada uno las variables verificando su delimitación
					//En ninguno de estos casos se usarán variables formateadas, ya que los cálculos se deberían hacer sobre los valores reales de los números / atributos
					for (var intScriptNum in arrScripts) {
						var strScriptExp = arrScripts[intScriptNum];
						//Remplaza primero las NO delimitadas por el valor delimitado
						var strScriptReplaced = strScriptExp.replace(objRegExpQuoted, function(sMatch, sGroup, iPos) {
							var strResult = '';
							if (sGroup == undefined ) {
								strResult = sMatch;
							}
							else {
								//Tiene que usar una expresión regular porque no necesariamente corresponde con la variable que se está procesando en este punto, si se identifica una manera
								//mas eficiente de reutilizar objRegExpQuoted para cambiar la última parte por una variable específica, eso sería óptimo para reducir reemplazos
								strResult = sGroup.replace(new RegExp(RegExp.quote(strExpr), "gi"), strUnformattedValue);
							}
							
							return strResult;
						});
						
						//A continuación reemplaza las delimitadas por el valor no delimitado, esto es, todas las que quedaran aún que no se hubieran reemplazado en la expresión previa
						strScriptReplaced = strScriptReplaced.replace(new RegExp(RegExp.quote(strExpr), "gi"), strNonDelimitedUnformattedValue);
						
						//Reemplaza esta porción de script en el string original (no debería haber 2 o mas porciones iguales, si las hubiera, se habrían generado como expresiones
						//diferentes en el array y se reprocesarán individualmente, así que sigue siendo válido reemplazar la primer ocurrencia del script solamente. En este punto nodeName
						//importarían diferencias de case sensitive ya que la expresión regular que obtuvo este string lo regresaría tal como existe, así que puede ser reemplazo directo
						strText = strText.replace(strScriptExp, strScriptReplaced);
					}
				}
				
				//Una vez que se hubieran procesado todos los scripts (si es que hubo alguno), ya se puede hacer el reemplazo directo de las variables por su valor sin delimitar, ya que
				//en este punto el texto no debería ser evaluado (no se marcó como tal) y por tanto se esperan valores sin delimitar solamente, cualquier escript que tuviera ya fue
				//resuelto correctamente y no podría quedar ninguna variable de este tipo en su porción de texto. En este caso el String estará o no formateado según como se indicó
				//en el parámetro correspondiente
				strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strNonDelimitedValue);
			}
			//JAPR
		}
	}
	
	//JAPR 2014-10-16: Agregadas la variable del score total de la captura. Este score es el total de la suma de los scores de cada sección, el cual
	//a su vez es el total de la suma de los scores de sus preguntas
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/{\$score}/gi);
	var intLength = (arrVars === null)?0:arrVars.length;
	//for (var intCont = 0; intCont < intLength; intCont++) {
	if (intLength) {
		//Esta variable se resolverá con el método original ya que no se le puede aplicar funciones ni pedir propiedades
		var strExpr = '{\$score}';
		var strValue = '{@score}';
		strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
	}
	
	//Inicia el reemplazo de valores del GPS {gps(data)}, en este caso se delegará al método original obtener su respuesta para centralizar el código,
	//aquí simplemente se cambiará el formato del nuevo al original
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/\{\$gps(\.([0-9|a-z|A-Z]+){1})?}/gi);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strValue = '';
		var strExpr = arrVars[intCont];
		var strProperty = '';
		var arrParts = strExpr.split('.');
		if (arrParts.length > 1) {
			strProperty = "(" + arrParts[1].replace('}', '') + ")";
		}
		
		strValue = "{@gps" + strProperty + "}";
		strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
	}
	
	return strText;
}

/* Reemplaza las variables que se encuentren dentro del html para regenerar la página (regresa el nuevo html)
El parámetro anIgnoreCol contiene una colección de preguntas que deberán ignorarse en el reemplazo de variables (originalmente usado para el
reemplazo dentro de valores default, de manera que no se pudieran crear referencias a si mismas que provocaran ciclos infinitos)
El parámetro anApplyFormat indica si se debe o no aplicar el formato del valor de esta pregunta (originalmente aplicaba sólo para numéricas)
//JAPR 2012-11-29: Modificado para agregar el parámetro bReturnNumericDefault de manera que en fórmulas que se van a evaluar,
se utilice un valor default de 0 para las preguntas numéricas, pero al asignar valores default como respuesta sin eval simplemente
se regrese la respuesta obtenida incluso si esta es vacía
//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
//JAPR 2013-12-16: Corregido un bug, se agregó el parámetro aSectionEvalRecord porque en la revisión final antes de grabar para reasignar respuestas
basadas en el default de las preguntas cuando estas NO se contestan, si dicho default fuera de secciones dinámicas y/o maestro-detalle y además se
combinara con Show Questions, el valor default a utilizar así como la visibilidad a evaluar corresponderían con el último registro por el que se 
hubiera navegado en cada sección, en lugar de por el registro que se estaba analizando. El parámetro aSectionEvalRecord contendrá (basado en la
pregunta para la cual se invoque la evaluación de defaults) un objeto indexado con el número de sección y como contenido el número de registro
evaluado, de tal forma que si se tiene que procesar una pregunta de dicha sección y si viene en este objeto referencia a esa sección, entonces
se usará el número de registro indicado en el objeto en lugar del número de registro interno de dicha sección (ni siquiera el número de registro
usado sólo durante la generación de las secciones dinámicas). La teoría de esto es que replaceVars automáticamente usa el registro actualmente 
evaluado en las secciones múltiples, pero si se manda este objeto, entonces se forzará a usar un registro específico, a sabiendas que una pregunta
dentro de una sección múltiple sólo debería usar preguntas de secciones estándars o de ella misma para sus fórmulas, o bien sumarizados o funciones
aplicadas a preguntas de otras secciones múltiples, pero nunca valores directos de otros registros en otras secciones múltiples
//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string (como las alfanuméricas,
fecha, hora, etc.) entre comillas sencillas de tal manera que el posible eval que se utilice posterior al resultado de este reemplazo pueda resolverse
correctamente (este parámetro inicialmente sólo se usaría dentro de getDefaultValue, que es la función que anteriormente hacía un replace exclusivamente
de variables sin evaluar su fórmula, pero debido a un cambio/corrección este día, se tendrá que realizar ahora un eval posterior al reemplazo, así
que sin delimitar estas variables no se podría llevar a cabo en muchas instancias pues trataría de usar los textos como variables globales)
//JAPR 2015-09-22: Modificado el comportamiento de bQuoteAnswers para que la decisión de delimitar o no la variable dependa del tipo de pregunta
Por autorización de Luis, todas las variables que se resuelvan a String se van a procesar de la siguiente manera:
	- Si están en un defaultValue o HTML o cualquier String que NO sea una expresión, se reemplazarán siempre SIN delimitarse
	- Si están en un string que representa una expresión, se tienen que delimitar siempre y cuando NO se encontraran previamente delimitadas (se usará una expresión regular)
	- Si están en un HTML que puede tener un script, se delimitarán siempre como el caso 2 cuando estén en el script, pero como el caso 1 cuando estén fuera de él
Debido a este cambio, el parámetro bQuoteAnswers cambia su sentido y ahora indicará si el string recibido corresponde a una expresión (true) o a un valor fijo (false) para determinar
que caso de reemplazos se va a utilizar
Esto sólo es aplicable para las variables del formato extendido, así que en esta función NO se realizará esta lógica para preguntas, se recibe el parámetro para heredarlo a replaceVarsAdv,
sólo se aplicará la lógica para las variables que son de sistema y/o las de sección
*/
function replaceVars(sText, anIgnoreCol, anApplyFormat, initBeforeShow, bReturnNumericDefault, aSectionEvalRecord, bQuoteAnswers) {
	//JAPR 2018-12-10: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
	Debugger.register('replaceVars: ' + String(sText).substr(0, 100));
	
	if (aSectionEvalRecord === undefined) {
		aSectionEvalRecord = {};
	}
	
	if (!sText) {
		sText = '';
	}
	
	if (!anIgnoreCol) {
		anIgnoreCol = new Object();
	}
	
	var intStart = sText.indexOf('{');
	
	if (intStart == -1) {
		//Si no hay variables, simplemente regresa el texto recibido
		return sText;
	}
	
	//JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
	//Debido a que para IOs es diferente la ruta de archivos que para Android, y que las referencias a imagenes tienen que incluir esta diferencia
	//cuando se utilizan localmente en un tag <IMG src=""> o URL(), se tiene que agregar una constante de ruta de imagen que el propio server tendrá
	//que incluir en el Path cuando se descargan definiciones para dispositivos, ya que el server no puede anticipar correctamente la ruta que se 
	//utilizará pues depende del dispositivo, así que esta variable especial funcionará para ajustarla dinámicamente {@IMGPATH}
	//Este reemplazo adicional no aplica vía Web
	var strText = sText;
	
	//OMMC 2016-07-28: Agregada para un posible reemplazo de variables de usuario
	strText = replaceVarsUser(strText);

	//JAPR 2014-10-15: Agregado el nuevo esquema de variables avanzado (con scores)
	strText = replaceVarsAdv(strText, anIgnoreCol, anApplyFormat, initBeforeShow, bReturnNumericDefault, aSectionEvalRecord, bQuoteAnswers);
	//JAPR
	
	//Inicia el reemplazo de propiedades de objetos {object.['property']} ó {object.['function()']}
	//var strText = sText;
	//JAPR 2017-02-02: Modificada la expresión regular para permitir múltiples niveles de objetos / funciones llamados de manera recursiva (#0A62YZ)
	//Ahora se permitirán variables del tipo {object.['subobject'].['property']} ó {object.['firstfunction()'].['function()']} y cualquier combinación válida de los mismos
	//var arrVars = strText.match(/[\{]{1}[a-zA-Z0-9]+[\.]{1}(\['){1}[a-zA-Z0-9]+[\()]*(']){1}[}]{1}/g);
	var arrVars = strText.match(/[\{]{1}[a-zA-Z0-9]+([\.]{1}(\[)'{1}[a-zA-Z0-9]+[\()]*(']){1})+[}]{1}/g);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		var arrParts = strExpr.split('.');
		//JAPR 2017-02-02: Modificada la expresión regular para permitir múltiples niveles de objetos / funciones llamados de manera recursiva (#0A62YZ)
		//strObject representará el objeto base sobre el que se pide la cadena de propiedades o funciones
		var strObject = arrParts[0].replace('{', '');
		//strProp representará el último elemento del array, el cual manualmente se ejecutará como propiedad o función (por lo menos existe un . así que es válido pedir length -1)
		var strProp = arrParts[arrParts.length -1].replace(/['\[\]}]+/gi, '');
		//strSubObject representará cualquier combinación de objetos o funciones previas, sólo se removerán los delimitadores intermedios pero no los paréntesis (aún con este cambio
		//no se permite el paso de parámetros en las funciones)
		if (arrParts.length > 2) {
			for (var intIndex = 1; intIndex < arrParts.length -1; intIndex++) {
				var strSubObject = arrParts[intIndex].replace(/['\[\]}]+/gi, '');
				//Cada subobjecto o subfunción se va concatenando al objeto principal
				strObject += '.' + strSubObject;
			}
		}
		//JAPR
		
		var strFn = '';
		if (strProp.indexOf('()') > 0) {
			strFn = strProp.replace(/['\[\]}\()]+/gi, '');
		}
		var strValue = '';
		try {
			if (strFn != '') {
				eval("strValue="+strObject+"."+strFn+"();");
			}
			else {
				eval("strValue="+strObject+"['"+strProp+"'];");
			}
		}
		catch(e) {
			//No importa el error, pero si ocurre debe dejar el string original
			strValue = strExpr;
		}
		
		//OMMC 2016-07-28: Agregado que el replaceVars permita el nombre de usuario y password en blanco cuando es app Personalizada
		//OMMC 2016-10-21: Cambiada la propiedad para permitir login en blanco aún cuando sea app de Forms.
		if (objApp && objApp.blankLogin) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		} else {
			if (strValue !== '' && strValue != strExpr) {
				strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
			}
		}
	}
	
	//Inicia el reemplazo de valores de las preguntas {Q###(data)}
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/g);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		var arrParts = strExpr.split('(');
		if (arrParts.length == 1) {
			var strQuestionNum = arrParts[0].replace(/[\{@qQ\}]+/gi, '');
		}
		else {
			var strQuestionNum = arrParts[0].replace(/[\{@qQ]+/gi, '');
		}
		var strValue = '';
		var objAnswer = undefined;
		
		if (!isNaN(strQuestionNum)) {
			strQuestionNum = parseInt(strQuestionNum);
			//Obtiene la referencia de la pregunta especificada
			//Se optó por no usar el array de respuestas ya que ese contiene para las secciones Dinámica y Maestro-Detalle un array de respuestas
			//por lo que no se sabría además a que registro se estaría haciendo referencia
			//if (selSurvey && selSurvey.answers && selSurvey.answers[strQuestionNum]) {
			if (selSurvey && selSurvey.questions && selSurvey.questionsOrder) {
				var objQuestion = selSurvey.questions[selSurvey.questionsOrder[strQuestionNum-1]];
				if (!objQuestion || !objQuestion.answer) {
					//Si no se encontró la pregunta, continua con la siguiente expresión identificada
					continue;
				}
				
				//Si es una pregunta que se debe ignorar, continua con el reemplazo de la siguiente variable
				if (anIgnoreCol[objQuestion.number]) {
					continue;
				}
				
				//objAnswer = selSurvey.answers[strQuestionNum];
				//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
				//Ahora la respuesta a utilizar dependerá del registro indicado si es que se especificó alguno, de lo contrario usa el "actual"
				objSection = objQuestion.section;
				objAnswer = objQuestion.answer;
				
				var intRecordNumber = 0;
				var intEvaluatedRecordNumber = undefined;
				//JAPR 2014-05-20: Agregado el tipo de sección Inline
				//JAPR 2018-12-10: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
				if (objSection && objSection.buildOptions.isMultiRecord && aSectionEvalRecord && aSectionEvalRecord[objSection.id] !== undefined) {
					intRecordNumber = parseInt(aSectionEvalRecord[objSection.id]);
					if (!isNaN(intRecordNumber) && objQuestion.answers[intRecordNumber]) {
						intEvaluatedRecordNumber = intRecordNumber;
						objAnswer = objQuestion.answers[intRecordNumber];
					}
				}
				
				//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
				var intSectionType = SectionType.Standard;
				var intEmpty = 0;
				if (objSection) {
					intSectionType = objQuestion.section.type;
					if (intSectionType == SectionType.Multiple) {
						intEmpty = objSection.empty;
						//JAPR 2014-01-16: Corregido un bug, si se está pidiendo el valor de una pregunta de una sección maestro-detalle y se está
						//en ella misma capturando en ese momento, entonces aunque se considere vacía la sección si se debe devolver el valor, ya
						//que se estaría precisamente realizando la captura del primer registro
						if (intEmpty && selSurvey.currentCapture && selSurvey.currentCapture.section) {
							if (selSurvey.currentCapture.section.id == objSection.id) {
								intEmpty = 0;
							}
						}
						//JAPR
					}
				}
				
				//JAPR 2012-11-21: Corregido un bug, no estaba tomando correctamente el valor de las preguntas abiertas numéricas de catálogo
				//en secciones dinámicas
				var blnDinamicCatalogQuestion = false;
				if (objSection && objSection.type == SectionType.Dynamic) {
					if (objQuestion.catalogID == objSection.catalogID && objQuestion.catMemberID) {
						//Por ahora sólo se soportan este tipo de preguntas que provienen del catálogo de la sección dinámica
						if (objQuestion.type = Type.number) {
							blnDinamicCatalogQuestion = true;
						}
					}
				}
				
				//Si se aplicará el filtro de sección dinámica, tiene que obtener el valor de la página que se está procesando para agregarlo
				var intMaxAttribNum = 0;
				if (blnDinamicCatalogQuestion && objQuestion.catalog && objSection.catMemberID) {
					if (objQuestion.catalog.attributes && objQuestion.catalog.attributes[objSection.catMemberID]) {
						intMaxAttribNum = objQuestion.catalog.attributes[objSection.catMemberID].order -1;
					}
				}
				
				//La página dinámica no puede usar el primer atributo, así que se valida que sea por lo menos el 2do en adelante
				if (blnDinamicCatalogQuestion && intMaxAttribNum) {
					//Genera el filtro a partir de la página dinámica que se estuviera pintando (si hay alguna), concatenando el valor dinámica
					//específico
					//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
					if (intEvaluatedRecordNumber !== undefined) {
						//Se usará el registro especificado en caso de haber información de contexto de evaluación
						var intRecordNumber = intEvaluatedRecordNumber;
					}
					else {
						//De lo contrario se usará el registro considerado como el "actual"
						var intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
					}
					//JPAR
					
					var objCatalogFilter = $.extend({}, objSection.catalogFilter);
					//Concatena el atributo de la página que se está procesando
					if (objSection.dynamicValues && objSection.dynamicValues[intRecordNumber]) {
						objCatalogFilter[intMaxAttribNum] = objSection.dynamicValues[intRecordNumber].value;
					}
				}
				else {
					//En este caso utiliza directamente el filtro de la pregunta
					var objCatalogFilter = $.extend({}, objAnswer.catalogFilter);
				}
				
				var strData = '';
				if (arrParts.length > 1) {
					strData = arrParts[1].replace(/[}\)]+/gi, '');
				}
				try {
					if (strData != '') {
						//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
						//Dependiendo de la sección, obtiene la visibilidad del registro actual o simplemente de la pregunta, si no está
						//visible entonces no se obtendrá su valor
						var blnVisible = true;
						switch (intSectionType) {
							case SectionType.Dynamic:
							case SectionType.Multiple:
							//JAPR 2014-05-20: Agregado el tipo de sección Inline
							case SectionType.Inline:
								//Estas secciones validarán primero que sean ellas mismas visibles y tengan por lo menos un registro, si es así
								//posteriormente valida la visibilidad de la pregunta en el registro actual, ya que la función no permite
								//especificar un registro y pedir por estas preguntas desde secciones estándar resulta ilógico, así que sólo se
								//pueden procesar realmente bien si se piden del registro actualmente evaluado
								//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
								//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
								if (!objSection || !objSection.isVisible || !objSection.isVisible(true) || intEmpty) {
									blnVisible = false;
								}
								else {
									//La sección es visible, pero ahora se basa en la visibilidad de la pregunta dentro del registro actual en la
									//sección para determinar si debe o no usar la respuesta
									//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
									if (intEvaluatedRecordNumber !== undefined) {
										//Se usará el registro especificado en caso de haber información de contexto de evaluación
										var intRecordNumber = intEvaluatedRecordNumber;
									}
									else {
										//De lo contrario se usará el registro considerado como el "actual"
										var intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
									}
									//JAPR
									
									if (!objQuestion.isVisible || !objQuestion.isVisible(intRecordNumber)) {
										blnVisible = false;
									}
									//JAPR 2015-01-15: Validado que las preguntas simple choice de catálogo, las cuales deben tener una respuesta
									//siempre capturada, se encuentren efectivamente contestadas ya que el proceso que extrae sus atributos no
									//se basa en su respuesta sino en su filtro, lo cual no siempre se asigna a la par (#2X267B)
									else {
										if (objQuestion.type == Type.simplechoice && objQuestion.catalogID > 0 && objQuestion.answers) {
											//Esta validación sólo aplica para cuando se pide un atributo, ya que la respuesta regresaría vacía
											//de todas formas si lo estuviera así que puede continuar, y si podemos pedir comentarios u otros datos
											var strProperty = strData.toLowerCase();
											if (!isNaN(strProperty) && objQuestion.answers[intRecordNumber] && objQuestion.answers[intRecordNumber].isEmpty) {
												if (objQuestion.answers[intRecordNumber].isEmpty()) {
													//Las preguntas simple choice de catálogo originalmente no podían tener un valor default, así
													//que es seguro preguntar directamente si está vacía sin considerar defaults
													blnVisible = false;
												}
											}
										}
									}
									//JAPR
								}
								break;
							
							default:
								//Cualquier otra sección sólo contiene una respuesta, por lo que valida directamente con el Question para saber
								//si debe o no agregarla
								if (!objQuestion.isVisible || !objQuestion.isVisible()) {
									blnVisible = false;
								}
								//JAPR 2015-01-15: Validado que las preguntas simple choice de catálogo, las cuales deben tener una respuesta
								//siempre capturada, se encuentren efectivamente contestadas ya que el proceso que extrae sus atributos no
								//se basa en su respuesta sino en su filtro, lo cual no siempre se asigna a la par (#2X267B)
								else {
									if (objQuestion.type == Type.simplechoice && objQuestion.catalogID > 0 && objQuestion.answer) {
										//Esta validación sólo aplica para cuando se pide un atributo, ya que la respuesta regresaría vacía
										//de todas formas si lo estuviera así que puede continuar, y si podemos pedir comentarios u otros datos
										var strProperty = strData.toLowerCase();
										if (!isNaN(strProperty) && objQuestion.answer.isEmpty) {
											if (objQuestion.answer.isEmpty()) {
												//Las preguntas simple choice de catálogo originalmente no podían tener un valor default, así
												//que es seguro preguntar directamente si está vacía sin considerar defaults
												blnVisible = false;
											}
										}
									}
								}
								//JAPR
								break;
						}
						//JAPR
						
						strData = strData.toLowerCase();
						switch (strData.toLowerCase()) {
							case 'comment':
								//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
								if (blnVisible) {
									strValue = objAnswer.comment;
								}
								//JAPR
								break;
							
							case 'days':
								//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
								if (blnVisible) {
									strValue = objAnswer.daysDueDate;
								}
								//JAPR
								break;
							
							case 'sum':
							//JAPR 2013-08-30: Agregados mas tipos de cálculos
							case 'avg':
							case 'max':
							case 'min':
							case 'count':
								var dblValue = 0;
								var dblMax = undefined;
								var dblMin = undefined;
								var intNumValues = 0;
								//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
								//En este caso cualquiera de estos parámetros sólo aplica para secciones con múltiples registros, así que se
								//valida directamente la visibilidad por registro, previamente verificando que la sección esté visible
								//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
								//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
								if (objSection && objSection.isVisible && objSection.isVisible(true) && !intEmpty) {
									//JAPR 2014-05-20: Agregado el tipo de sección Inline
									//JAPR 2018-12-10: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
									if (objSection.buildOptions.isMultiRecord) {
										for (var intRecordNumber = 0; intRecordNumber < objSection.maxRecordNum; intRecordNumber++) {
											if (!objQuestion.isVisible(intRecordNumber)) {
												continue;
											}
											
											//JAPR 2013-08-30: Corregido un bug, si no se puede resolver a un número, debe sumar 0
											//JAPR 2014-10-11: Corregido un bug, no se debe usar el answer directo sino la función getAnswer para
											//permitir aplicar valores default encadenados
											//var dblTempVal = parseFloat(objQuestion.answers[intRecordNumber].answer);
											var strAnswer = objQuestion.getAnswer(intRecordNumber);
											var dblTempVal = parseFloat(strAnswer);
											//JAPR
											if (isNaN(dblTempVal)) {
												dblTempVal = 0;
												continue;
											}
											
											dblValue += dblTempVal;
											//dblValue += parseFloat(objQuestion.answers[intRecordNumber].answer);
											//JAPR 2013-08-30: Agregados mas tipos de cálculos
											if (dblMax === undefined || dblTempVal > dblMax) {
												dblMax = dblTempVal;
											}
											if (dblMin === undefined || dblTempVal < dblMin) {
												dblMin = dblTempVal;
											}
											intNumValues++;
											//JAPR
										}
									}
								}
								
								//JAPR 2013-08-30: Agregados mas tipos de cálculos
								switch (strData.toLowerCase()) {
									case 'avg':
										if (intNumValues) {
											strValue = dblValue / intNumValues;
										}
										else {
											strValue = 0;
										}
										break;
										
									case 'max':
										strValue = (dblMax === undefined)?'':dblMax;
										break;
										
									case 'min':
										strValue = (dblMin === undefined)?'':dblMin;
										break;
									
									case 'count':
										strValue = intNumValues;
										break;	
									
									case 'sum':
									default:
										strValue = dblValue;
										break;
								}
								//JAPR
								//strValue = dblValue;
								break;
								
							default:
								//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
								if (blnVisible) {
									//Verifica si se trata de un número, en ese caso hace referencia al número de atributo de esta pregunta
									if (!isNaN(strData)) {
										var intAttributeNum = parseInt(strData);
										//Regresa el valor del atributo indicado de esta respuesta
										if (objQuestion.catalog && objQuestion.catalog.getFirstValueForAttributeNum) {
											strValue = objQuestion.catalog.getFirstValueForAttributeNum(undefined, 0, objCatalogFilter, intAttributeNum);
										}
									}
									else {
										//No se entiende el parámetro solicitado, así que regresa vacío
									}
								}
								//JAPR
								break;
						}
						
						//JAPR 2014-10-10: Agregada la evaluación del valor default para permitir datos resultado de fórmulas complejas
						//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
						//Si la pregunta no estaba visible entonces debería venir completamente vacio, pero si fuera una pregunta dentro de sección
						//con múltiples registros y se pidió un valor de función, entonces traera un 0 (aunque técnicamente hablando no esté visible
						//la pregunta, ya que esos valores generalmente se utilizaban para cálculos así que se regresaba siempre el 0), además habrá
						//casos en los que se pidan datos de atributos del catálogo por lo que no sabemos si esperamos o no un número, por lo tanto
						//la validación se reducirá a verificar si viene vacio o no, si lo está entonces se delimitará (si es que así se pidió), de
						//lo contrario se verificará si el valor es o no un número, si lo es entonces NO se delimitará porque no se sabe si realmente
						//debía ser o no un valor numérico o si es un comentario donde sólo capturaron un número (por tanto debería ser un texto), pero
						//como javascript aplica conversiones de tipos automáticas según los operandos, si se esperaba un texto en lugar del número y
						//se utiliza el operador + entonces si hará la concatenación incluso si aquí se regresa como número, contrario a que si lo
						//convertimos a cadena cuando el otro operando era un número, ya que en ese caso no se sumarían
						if (bQuoteAnswers) {
							if (Trim(strValue) == '') {
								//Está vacío, así que se regresa simplemente delimitado el vacio
								strValue = "''";
							}
							else {
								if ($.isNumeric(strValue)) {
									//Es un número, no aplicará delimitación
								}
								else {
									//No es un número, así que delimitará lo que sea que se regresó
									strValue = "'" + strValue + "'";
								}
							}
						}
						//JAPR
					}
					else {
						//No hay un parámetro que especifique lo que se desea obtener, así que se regresa directamente el valor
						//JAPR 2012-10-16: Agregado el valor default hasta que se edita la pregunta
						//strValue = objAnswer.answer;
						if (objQuestion.type == Type.number && objQuestion.catMemberID && initBeforeShow) {
							var strAttribNum = objQuestion.catalog.attributes[objQuestion.catMemberID].order;
							var strValueTemp = "{@Q" + objQuestion.number + "(" + strAttribNum + ")}";
							objQuestion.defaultValue = strValueTemp;
						}
						
						//JAPR 2013-08-30: Corregido un bug, no estaba aplicando visibilidad de preguntas en los cálculos de variables
						//Dependiendo de la sección, obtiene la visibilidad del registro actual o simplemente de la pregunta, si no está
						//visible entonces no se obtendrá su valor
						var blnVisible = true;
						switch (intSectionType) {
							case SectionType.Dynamic:
							case SectionType.Multiple:
							//JAPR 2014-05-20: Agregado el tipo de sección Inline
							case SectionType.Inline:
								//Estas secciones validarán primero que sean ellas mismas visibles y tengan por lo menos un registro, si es así
								//posteriormente valida la visibilidad de la pregunta en el registro actual, ya que la función no permite
								//especificar un registro y pedir por estas preguntas desde secciones estándar resulta ilógico, así que sólo se
								//pueden procesar realmente bien si se piden del registro actualmente evaluado
								//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
								//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
								if (!objSection || !objSection.isVisible || !objSection.isVisible(true) || intEmpty) {
									blnVisible = false;
								}
								else {
									//La sección es visible, pero ahora se basa en la visibilidad de la pregunta dentro del registro actual en la
									//sección para determinar si debe o no usar la respuesta
									//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
									if (intEvaluatedRecordNumber !== undefined) {
										//Se usará el registro especificado en caso de haber información de contexto de evaluación
										var intRecordNumber = intEvaluatedRecordNumber;
									}
									else {
										//De lo contrario se usará el registro considerado como el "actual"
										var intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
									}
									//JAPR
									
									if (!objQuestion.isVisible || !objQuestion.isVisible(intRecordNumber)) {
										blnVisible = false;
									}
								}
								break;
							
							default:
								//Cualquier otra sección sólo contiene una respuesta, por lo que valida directamente con el Question para saber
								//si debe o no agregarla
								if (!objQuestion.isVisible || !objQuestion.isVisible()) {
									blnVisible = false;
								}
								break;
						}
						
						if (blnVisible) {
							//No solicita que regrese 0s en caso de numéricas ni tampoco pide formatos ya que esta misma función va
							//a aplicarlo al final de ser necesario
							//JAPR 2013-12-16: Corregido un bug, la revisión final antes de grabar para reasignar respuestas a partir de defaults usaba siempre el último registro navegado
							//JAPR 2019-05-02: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
							//strValue = objQuestion.getDefaultValue(intEvaluatedRecordNumber);
							strValue = objQuestion.getAnswer(intEvaluatedRecordNumber);
							//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
							if (bQuoteAnswers && objQuestion.quote) {
								strValue = objQuestion.quote(strValue);
							}
							//JAPR
						}
						//JAPR 2014-10-10: Agregada la evaluación del valor default para permitir datos resultado de fórmulas complejas
						//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
						else {
							//Si la pregunta no está visible pero además se pidió delimitar las respuestas, tiene que regresar vacio delimitado o
							//de lo contrario los evals fallarán
							if (bQuoteAnswers) {
								strValue = "''";
							}
						}
						//JAPR
						var isAlreadyFormated = false;
						if (strValue !== '' && anApplyFormat && objQuestion.format) {
							//JAPR 2016-06-17: Corregido un bug, no estaba aplicando el formateo de la fecha en ese tipo de preguntas, sino siempre numérica (#6TC5Q7)
							if (objQuestion.type == Type.date) {
								strValue = formatDate(strValue.replace(/-/g,""), objQuestion.format);
							}
							else {
								strValue = formatNumber(strValue, objQuestion.format);
							}
							//JAPR
							isAlreadyFormated = true;
							//JAPR 2014-10-10: Agregado el parámetro bQuoteAnswers para delimitar las variables de preguntas que se resuelvan a un string
							//Si se aplicó un formato y se pidió hacer quote de las respuestas, entonces independientemente del tipo de pregunta
							//se debe forzar a que se haga el quote, por lo que manualmente se agregan los delimitadores
							if (bQuoteAnswers) {
								strValue = "'" + strValue + "'";
							}
							//JAPR
						}
						else {
							//En caso de un valor vacio, aplica una conversión a número si la pregunta se resuelve como numérica
							//JAPR 2012-11-30: Agregado el parámetro bReturnNumericDefault para forzar a regresar números en
							//reemplazos para cálculos
							if (bReturnNumericDefault && Trim(strValue) == '') {
								switch (objQuestion.type) {
									case Type.number:
									case Type.calc:
										strValue = 0;
										break;
								}
							}
						}
					}
				}
				catch(e) {
					//No importa el error, pero si ocurre debe dejar el string original
					strValue = strExpr;
				}
			}
		}
        //2014.11.21 JCEM Se agrega formato a summary
        //JAPR 2015-01-28: Corregido un bug, el formato no siempre se debe aplicar, ya que según el uso de replaceVars, a veces es para cálculo
        //y a veces es para despliegue, además en este punto podría no existir una instancia de QuestionCls asignada, así que no se puede usar
        //directamente una propiedad de ella sin comprobar su existencia
		if (strValue !== '' && anApplyFormat && objQuestion && objQuestion.format && !isAlreadyFormated) {
			//JAPR 2016-06-17: Corregido un bug, no estaba aplicando el formateo de la fecha en ese tipo de preguntas, sino siempre numérica (#6TC5Q7)
			if (objQuestion.type == Type.date) {
				strValue = formatDate(strValue.replace(/-/g,""), objQuestion.format);
			}
			else {
				strValue = formatNumber(strValue, objQuestion.format);
			}
			//JAPR
        }
        //JAPR
		if (strValue != strExpr) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		}
	}
	
	//JAPR 2013-12-06: Agregadas variables para las secciones
	//Inicia el reemplazo de valores de las secciones {S###(data)}
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/[\{]{1}[@]{1}[S|s]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/g);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		var arrParts = strExpr.split('(');
		if (arrParts.length == 1) {
			var strSectionNum = arrParts[0].replace(/[\{@sS\}]+/gi, '');
		}
		else {
			var strSectionNum = arrParts[0].replace(/[\{@sS]+/gi, '');
		}
		var strValue = '';
		
		if (!isNaN(strSectionNum)) {
			strSectionNum = parseInt(strSectionNum);
			//Obtiene la referencia de la sección especificada
			if (selSurvey && selSurvey.sections && selSurvey.sectionsOrder) {
				var objSection = selSurvey.sections[selSurvey.sectionsOrder[strSectionNum-1]];
				if (!objSection) {
					//Si no se encontró la sección, continua con la siguiente expresión identificada
					continue;
				}
				//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
				//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
				if (!objSection.isVisible(true)) {
					continue;
				}
				
				var intEmpty = 0;
				var intSectionType = objSection.type;
				var intRecordNumber = 0;
				var intMaxAttribNum = 0;
				var blnMasterDetSection = false;
				var blnDinamicSection = false;
				var strPageName = '';
				var objCatalogFilter = {};
				switch (intSectionType) {
					case SectionType.Multiple:
						intRecordNumber = objSection.recordNum +1;
						blnMasterDetSection = true;
						intEmpty = objSection.empty;
						break;
					
					case SectionType.Dynamic:
					//JAPR 2014-05-20: Agregado el tipo de sección Inline
					case SectionType.Inline:
						blnDinamicSection = true;
						//Si se aplicará el filtro de sección dinámica, tiene que obtener el valor de la página que se está procesando para agregarlo
						if (objSection.catalog && objSection.catMemberID) {
							if (objSection.catalog.attributes && objSection.catalog.attributes[objSection.catMemberID]) {
								intMaxAttribNum = objSection.catalog.attributes[objSection.catMemberID].order -1;
							}
						}
						
						//La página dinámica no puede usar el primer atributo, así que se valida que sea por lo menos el 2do en adelante
						//JAPR 2014-05-20: Agregado el tipo de sección Inline
						//Las secciones Inline si pudieran usar el primer atributo, así que se modificó la condición para ese caso
						if (intMaxAttribNum || intSectionType == SectionType.Inline) {
							//Genera el filtro a partir de la página dinámica que se estuviera pintando (si hay alguna), concatenando el valor dinámica
							//específico
							intRecordNumber = (objSection.recordNumForGen !== undefined)?objSection.recordNumForGen:objSection.recordNum;
							objCatalogFilter = $.extend({}, objSection.catalogFilter);
							//Concatena el atributo de la página que se está procesando
							if (objSection.dynamicValues && objSection.dynamicValues[intRecordNumber]) {
								strPageName = objSection.dynamicValues[intRecordNumber].value;
								objCatalogFilter[intMaxAttribNum] = strPageName;
							}
							intRecordNumber++;
						}
						break;
				}
				
				var strData = '';
				if (arrParts.length > 1) {
					strData = arrParts[1].replace(/[}\)]+/gi, '');
				}
				try {
					if (strData != '') {
						//Dependiendo del tipo de la sección y el parámetro pedido, obtiene un dato específico dentro de la página dinámica/maestro detalle, o solo datos generales
						//de la sección
						strData = strData.toLowerCase();
						switch (strData.toLowerCase()) {
							case 'name':
								strValue = objSection.name;
								break;
								
							case 'number':
								strValue = objSection.number;
								break;
							
							//MAPR 2019-08-22: Se añade la propiedad id de la sección en el editor de formulas
							case 'id':
								strValue = objSection.id;
								break;
							
							case 'pagename':
								strValue = strPageName;
								break;
								
							case 'pagenumber':
								if (blnMasterDetSection || blnDinamicSection) {
									strValue = intRecordNumber;
								}
								else {
									strValue = 0;
								}
								break;
							
							default:
								//Verifica si se trata de un número, en ese caso hace referencia al número de atributo del catálogo de esta sección que debe ser dinámica
								if (!isNaN(strData) && blnDinamicSection) {
									var intAttributeNum = parseInt(strData);
									//Regresa el valor del atributo indicado de esta sección
									if (objSection.catalog && objSection.catalog.getFirstValueForAttributeNum) {
										strValue = objSection.catalog.getFirstValueForAttributeNum(undefined, 0, objCatalogFilter, intAttributeNum);
									}
								}
								else {
									//No se entiende el parámetro solicitado, así que regresa vacío
								}
								break;
						}
					}
					else {
						//No hay un parámetro que especifique lo que se desea obtener, así que se regresa directamente el nombre
						strValue = objSection.name;
					}
				}
				catch(e) {
					//No importa el error, pero si ocurre debe dejar el string original
					strValue = strExpr;
				}
			}
		}
		
		if (strValue != strExpr) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		}
	}
	
	//JAPR 2014-02-11: Agregadas las variables con los datos extendidos de GPS
	//Inicia el reemplazo de valores del GPS {gps(data)}
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/[\{]{1}[@]{1}[gps]+(\([0-9|a-z|A-Z]*\))?[}]{1}/gi);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		var arrParts = strExpr.split('(');
		var strValue = '';
		var strData = '';
		if (arrParts.length > 1) {
			strData = arrParts[1].replace(/[}\)]+/gi, '');
			if (!strData) {
				strData = '';
			}
		}
		
		if (objApp && objApp.position && objApp.position.coords) {
			try {
				//Dependiendo del tipo de la sección y el parámetro pedido, obtiene un dato específico dentro de la página dinámica/maestro detalle, o solo datos generales
				//de la sección
				strData = strData.toLowerCase();
				switch (strData.toLowerCase()) {
					case 'accuracy':
						strValue = objApp.position.coords.getGPSData('accuracy');
						break;
					case 'latitude':
					case 'lat':
						strValue = objApp.position.coords.getGPSData('latitude');
						break;
					case 'longitude':
					case 'long':
						strValue = objApp.position.coords.getGPSData('longitude');
						break;
					case 'country':
						strValue = objApp.position.coords.getGPSData('Country');
						break;
					case 'state':
						strValue = objApp.position.coords.getGPSData('State');
						break;
					case 'city':
						strValue = objApp.position.coords.getGPSData('City');
						break;
					case 'zipcode':
					case 'zip':
					case 'code':
						strValue = objApp.position.coords.getGPSData('ZipCode');
						break;
					case 'address':
					case 'addr':
					case 'street':
					case 'colony':
						strValue = objApp.position.coords.getGPSData('Address');
						break;
					case 'fulladdress':
					case 'full':
					case 'fulladdr':
						strValue = objApp.position.coords.getGPSData('FullAddress');
						break;
					default:	//Vacio
						//La posición GPS completa sólo como Lat,Long
						strValue = objApp.position.coords.getGPSData('latitude').toString() + ',' + objApp.position.coords.getGPSData('longitude').toString();
						break;
				}
			}
			catch(e) {
				//No importa el error, pero si ocurre debe dejar el string original
				strValue = strExpr;
			}
		}
		
		if (strValue != strExpr) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		}
	}
	
	//JAPR 2014-10-16: Agregadas la variable del score total de la captura. Este score es el total de la suma de los scores de cada sección, el cual
	//a su vez es el total de la suma de los scores de sus preguntas
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/{@score}/gi);
	var intLength = (arrVars === null)?0:arrVars.length;
	//for (var intCont = 0; intCont < intLength; intCont++) {
	if (intLength) {
		var strExpr = '{@score}';
		var strValue = '';
		
		if (selSurvey && selSurvey.sections && selSurvey.sectionsOrder) {
			try {
				var dblScore = 0;
				for (var intSectionNum in selSurvey.sectionsOrder) {
					var objSection = selSurvey.sections[selSurvey.sectionsOrder[intSectionNum]];
					if (!objSection || objSection.type == SectionType.Formatted || !objSection.getScore) {
						continue;
					}
					
					//JAPR 2016-12-14: Corregido un bug con la visibilidad de secciones tabla cuando se encuentran dentro de preguntas tabla (#LVK6J0)
					//Se tuvo que agregar el parámetro bCheckContainerVisibility para forzar a revisar la visibilidad de las preguntas tabla cuando se trata de una sección tabla
					if (!objSection.isVisible || !objSection.isVisible(true) || (objSection.type == SectionType.Multiple && objSection.empty)) {
						continue;
					}
					
					var intMaxRecordNum = objSection.maxRecordNum;
					for (var intRecordNum = 0; intRecordNum < intMaxRecordNum; intRecordNum++) {
						var strAnswer = objSection.getScore(intRecordNum);
						var dblTempVal = parseFloat(strAnswer);
						if (isNaN(dblTempVal)) {
							dblTempVal = 0;
						}
						
						dblScore += dblTempVal;
					}
				}
				
				strValue = dblScore;
			}
			catch(e) {
				//No importa el error, pero si ocurre debe dejar el string original
				strValue = strExpr;
			}
		}
		
		if (strValue != strExpr) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		}
	}
		
    //JAPR 2014-09-10: Modificado el método de descarga de definiciones para descargar en forma independiente las imagenes
	//Debido a que para IOs es diferente la ruta de archivos que para Android, y que las referencias a imagenes tienen que incluir esta diferencia
	//cuando se utilizan localmente en un tag <IMG src=""> o URL(), se tiene que agregar una constante de ruta de imagen que el propio server tendrá
	//que incluir en el Path cuando se descargan definiciones para dispositivos, ya que el server no puede anticipar correctamente la ruta que se 
	//utilizará pues depende del dispositivo, así que esta variable especial funcionará para ajustarla dinámicamente {@IMGPATH}
	//Este reemplazo adicional no aplica vía Web
	//JAPR 2016-01-11: Agregado el defaultValue para preguntas tipo Photo
	//Ahora se reemplazará la constante {@IMGPATH} cuando se trata de captura vía Web para hacer referencia al directorio CustomerImages/fbm_bmd_#### como el
	//path de las imagenes, ya que sólo así se podrían usar imagenes con rutas relativas, el resto del tiempo tendrán que ser rutas absolutas si corresponden
	//a un defaultValue que no viene de un atributo tipo imagen de catálogo
	//OMMC 2016-05-09: Agregada la variable {@IMGFULLPATH} para resolver imagenes en dispositivo sin la necesidad de concatenar las subcarpetas
	//JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
	//Ahora se invocará a la nueva función para no tener el mismo código en dos lugares
	//JAPR 2017-03-03: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
	//En este caso se hace el replace sobre un HTML completo, no se puede determinar si la variable está ahí o no por un catálogo cargado dinámicamente, pero no debería ya que todas
	//las referencias de atributos imágenes en ese caso se habrían convertido desde que se cargaron los datos dinámicos, así que no manda el parámetro bFullServerReference
	strText = replaceIMGPathVars(strText);
	/*if (objSettings) {
		var strImgPath = '';
		var strFullImgPath = '';
		if (objSettings.webMode) {
			strImgPath = 'customerimages/' + objSettings.project + '/';
		}
		else {
			//JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
			strImgPath = objSettings.prefixTrans + "/";
			strFullImgPath = objSettings.prefixTrans + "/images/" + objSettings.project + "/";
			//JAPR
		}
		
		strText = strText.replace(new RegExp(RegExp.quote("{@IMGPATH}"), "gi"), strImgPath);
		strText = strText.replace(new RegExp(RegExp.quote("{@IMGFULLPATH}"), "gi"), (objSettings.webMode)?strImgPath:strFullImgPath);
	}
	*/
	
	return strText;
}

//JAPR 2016-05-20: Integrado cambio de RVega para permitir especificar imagen de fondo para los botones de Next y Back de las secciones
/* Esta función exclusivamente reemplazará las variables {@IMGPATH} y {@IMGFULLPATH} (o cualquier otra que se pudiera agregar con uso similar) ya que para el uso de imagenes personalizadas
durante la inyección de los templates de estilos, no tiene caso intentar reemplazar todas las demás variables existentes, ya que sólo estas podrían ser necesarias por las referencias
a imagenes que pudieran existir en propiedades background-image, así que se extrajo únicamente esta porción
//JAPR 2017-03-03: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
Agregado el parámetro bFullServerReference para indicar si la función a pesar de estar siendo ejecutada en un dispositivo móvil, deberá o no reemplazar el path por la referencia de la
imagen tal como existiría en el servidor de KPIOnline directamente
//JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
Agregado el parámetro oCatalog para obtener la referencia al catálogo que está solicitando esta función (sólo aplica si bFullServerReference)
*/
function replaceIMGPathVars(sText, bFullServerReference, oCatalog) {
	strText = sText;
	
	if (objSettings) {
		var strImgPath = '';
		var strFullImgPath = '';
		if (objSettings.webMode) {
			strImgPath = 'customerimages/' + objSettings.project + '/';
		}
		else {
			//JAPR 2017-03-03: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
			if (bFullServerReference) {
				//En este caso a pesar de que se trata de un App, se forzará a ir a KPIOnline por la imagen por lo que sólo genera una referencia a ella
				var blnChangeRoot = true;
				/*if (strDefaultValue.toLowerCase().indexOf('http') == 0) {
					blnChangeRoot = false;
				}*/
				
				if (blnChangeRoot) {
					var strRoot = "http://" + objSettings.esurveyFolder;
					if (objSettings.protocol) {
						strRoot = strRoot.replace('http:', objSettings.protocol);
					}
				}
				else {
					//En este caso el propio valor de imagen ya trae una ruta absoluta
					var strRoot = '';
				}
				
				//JAPR 2017-03-06: Corregido un bug, no se estaban mostrando las imágenes cargadas dinámicamente, ahora usará referencia directa al servidor de KPIOnline (#6ZKO4O)
				//Se requiere cambiar el contenido regresado como valor de este atributo a una URL que haga un request de la imagen vía PhP para evitar problemas con CORS
				if (oCatalog && oCatalog.dataSourceID) {
					if (oCatalog.dataSourceType == DataSourceTypes.eBavel) {
						//Si el catálogo es de tipo eBavel, entonces al tratarse de un valor dinámico ya debería venir directamente la URL de la imagen desde el servicio de eBavel
						strImgURL = strText;
					}
					else {
						//Con cualquier otro catálogo sólo viene la ruta de la imagen en forma relativa, se debe ir al server para descargar la imagen binaria de manera segura para CORs
						var urlService = "http://" + objSettings.esurveyFolder + objSettings.esurveyservice;
						if (objSettings && objSettings.protocol) {
							urlService = urlService.replace('http:', objSettings.protocol);
						}
						var strImgURL = urlService + "?action=getimage" + 
							"&dataSourceID=" + oCatalog.dataSourceID + 
							"&projectName=" + objSettings.project + 
							"&appVersion=" + objSettings.appFormsVersion + 
							"&fileName=" + encodeURIComponent(strText);
					}
					
					Debugger.message("replaceIMGPathVars CORS Safe ImgURL: " + strImgURL);
					return strImgURL;
				}
				
				//En este punto la imagen del servidor ya incluía los folders "images/fbm_bmd_####" debido a que se había preparado para dispositivo, así que no se agregarán los folders
				//de la ruta real en el servidor sino sólo la palabra "customer" lo cual complementará la ruta del servidor automáticamente
				//strImgPath = strRoot + 'customerimages/' + objSettings.project + '/';
				strImgPath = strRoot + 'customer';
				//En este caso no podrá diferenciar entre usar el path o el fullpath, porque tiene que ir al server por la imagen y esa está siempre en el mismo path
				strFullImgPath = strImgPath;
			}
			else {
				//En este caso la imagen ya debería estar descargada en el dispositivo, así que simplemente obtiene la referencia local a ella
				//JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
				strImgPath = objSettings.prefixTrans + "/";
				strFullImgPath = objSettings.prefixTrans + "/images/" + objSettings.project + "/";
				//JAPR
			}
		}
		
		strText = strText.replace(new RegExp(RegExp.quote("{@IMGPATH}"), "gi"), strImgPath);
		strText = strText.replace(new RegExp(RegExp.quote("{@IMGFULLPATH}"), "gi"), (objSettings.webMode)?strImgPath:strFullImgPath);
	}
	
	return strText;
}

/* Reemplaza las variables de atributos de catálogos (@CMID#) del catálogo especificado en el parámetro, por los valores correspondientes en el array
de valores aCatalogValuesColl, en caso de no recibir un array de valores entonces se utilizarán los valores totales del catálogo, adicionalmente se
aplicará el filtro especificado con aParentFilter en caso de ser recibido, de lo contrario se tomará el primer valor de cada atributo por lo que el
parámetro aCatalogValuesColl debería ser exclusivamente un valor nada mas
*/
function replaceCatVarsByValues(sText, aCatalog, aCatalogValuesColl, aParentFilter) {
	Debugger.register('replaceCatVarsByValues');
	
	if (!sText) {
		sText = '';
	}
	
	if (!aCatalog || !aCatalog.attributes || !aCatalog.attributeValues) {
		return sText;
	}
	
	if (!aCatalogValuesColl) {
		aCatalogValuesColl = aCatalog.attributeValues;
		//return sText;
	}
	
	var intStart = sText.indexOf('{@CM');
	
	if (intStart == -1) {
		//Si no hay variables, simplemente regresa el texto recibido
		return sText;
	}
	
	var strText = sText;
	//Inicia el reemplazo de valores de las preguntas {CM###}
	//JAPR 2014-10-27: Corregido un bug, no se estaba definiendo la variable como local así que al emplear reemplazos en cadena se sobreescribía
	var arrVars = strText.match(/[\{]{1}[@]{1}[C|c]{1}[M|m]{1}[0-9]+[}]{1}/g);
	var intLength = (arrVars === null)?0:arrVars.length;
	for (var intCont = 0; intCont < intLength; intCont++) {
		var strExpr = arrVars[intCont];
		var arrParts = strExpr.split('(');
		if (arrParts.length > 1) {
			continue;
		}
		
		var strAttribNum = arrParts[0].replace(/[\{@CcMm\}]+/gi, '');
		var strValue = '';
		var objAnswer = undefined;
		
		if (!isNaN(strAttribNum)) {
			strAttribNum = parseInt(strAttribNum);
			try {
				strValue = aCatalog.getFirstValueForAttributeNum(aCatalogValuesColl, 0, aParentFilter, strAttribNum);
			}
			catch(e) {
				//No importa el error, pero si ocurre debe dejar el string original
				strValue = strExpr;
			}
		}
		
		if (strValue != strExpr) {
			strText = strText.replace(new RegExp(RegExp.quote(strExpr), "gi"), strValue);
		}
	}
	
	return strText;
}

function changeInkColor(inkColor,qID){
	//quitamos el color seleccionado
	$('.colorSquare').css('border','none');
	var colorTap = undefined;
	switch(inkColor){
		case '0060ff':
		colorTap = 'azul';
		break;
		case '000000':
		colorTap = 'negro';
		break;
		case 'ffffff':
		colorTap = 'blanco';
		break;
		case 'fe0000':
		colorTap = 'rojo';
		break;
		case 'ffde00':
		colorTap = 'amarillo';
		break;
		case '727272':
		colorTap = 'gris';
		break;
		case 'ff8a00':
		colorTap = 'naranja';
		break;
		case '2fc501':
		colorTap = 'verde';
		break;
		default: 
			break;
	}
	if(colorTap){
		$('#'+colorTap+'_'+qID).css('border','5px solid black');
	}
	//Para cambiar el color de la tinta del canvas necesitamos obtener primero el contexto
	var objQuestion = selSurvey.questions[qID];
	var strFieldName = objQuestion.getFieldName();
	var elID = "padsignature"+strFieldName;

	var objByNames = document.getElementsByName(elID);
	if (objByNames)
	{
		var ntopobjs = objByNames.length;
		for (var iobj=0; iobj<ntopobjs; iobj++)
		{
			var canvas = objByNames[iobj];
	var context = canvas.getContext('2d');
	context.strokeStyle = "#"+inkColor;
}
	}

}

//Refresca el canvas de fullscreen
//JAPR 2014-12-16: Agregado el valor default para preguntas tipo Foto y Sketch
/* Agregado el parametro sDefaultImageFileName por simplicidad, ya que ese dato se calculó al entrar al diálogo de captura del Sketch y hay
una serie de condiciones que están validadas no de una forma muy óptima en esta función, así que para no complicar mas el asunto, si se
recibió este parámetro quiere decir que se va a usar la imagen recibida tal cual para restablecer el canvas, de lo contrario entonces si aplicará
la lógica que consiste básicamente en:
- Si tiene imagen default de fondo definida, usa dicha imagen
- Si tiene una foto (que sólo puede ser si no hay imagen default definida, ya que si la hubiera, el botón de fotos no aparecería) entonces usa la foto
*/
function restoreFullScreenCanvasBg(qID, sDefaultImageFileName) {
	var objQuestion = selSurvey.questions[qID];
	//si es firma no entra a esta funcion
	if(objQuestion.type != Type.sketch){
		return;
	}
	
	var strFieldName = objQuestion.getFieldName();
	var elID = "padsignature"+strFieldName;
	var objByNames = document.getElementsByName(elID);
	if (objByNames)
	{
		var ntopobjs = objByNames.length-1;
		var canvas = objByNames[ntopobjs];
	}
	else{
		return;
	}
	
	var image = undefined;
	//JAPR 2014-12-16: Agregado el valor default para preguntas tipo Foto y Sketch
	//Agregado el parametro sDefaultImageFileName por simplicidad para restablecer en base a él (leer comentario en la cabecera de la función)
	if (sDefaultImageFileName) {
		//En este caso la imagen tiene que estar previamente grabada localmente si es un dispositivo, y si es web entonces se puede usar su
		//URL directa, así que realmente no afecta y en ambos casos se puede usar la ruta de imagen tal como se reciba
		image = new Image();
		image.src = replaceVars(sDefaultImageFileName);
		
		var context;
		//canvas = document.getElementById(elID); //document.getElementById(strFieldName);
		context = canvas.getContext('2d');
		image.onload = function() {
			context.drawImage(image, 0, 0); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
		};
		return;
	}
	//JAPR
	
	if (objQuestion.imageSketch) {
		//limpiamos la ruta de la imagen 
		var tmp = ("'"+objQuestion.imageSketch+"'").match(/(img|src)\=(\"|\')[^\"\'\>]+/g);
		tmp = tmp + '"'; //agregamos la ultima comilla ya que la quita el match
		
		var tmp2 = tmp.replace("src=","");
		tmp2 = tmp2.replace('"',''); //quitamos las comillas
		tmp2 = tmp2.replace('"',''); 
		
		var strFileName = tmp2;//objQuestion.imageSketch; //verificar que tenga algo de foto (ruta)
		if (strFileName != '') {
			//generamos un objeto imagen, le asignamos a su propiedad src la ruta de la imagen
			image = new Image();
			image.src = replaceVars(strFileName);
					
			var context;
			//canvas = document.getElementById(elID); //document.getElementById(strFieldName);
			
			context = canvas.getContext('2d');
			image.onload = function() {
				context.drawImage(image, 0, 0,canvas.width,canvas.height); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
			};
		}
		return;
	}
	
	var image = undefined;
	//vemos si tiene foto tomada (se guardo en esta propiedad ya que la de photo tiene lo ultimo rayado)	
	if (objQuestion.answer.photo) {
		//tiene foto tomada x el usuario hay que ponerla 
		var strFileName = objQuestion.answer.photo;//objQuestion.imageSketch; //verificar que tenga algo de foto (ruta)
		if (strFileName != '') {
			//generamos un objeto imagen, le asignamos a su propiedad src la ruta de la imagen
			image = new Image();
			if (objSettings.webMode) {
				image.src = replaceVars(strFileName);
				var context;
				//canvas = document.getElementById(elID); //document.getElementById(strFieldName);
				context = canvas.getContext('2d');
				image.onload = function() {
					context.drawImage(image, 0, 0); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
				};
			}
			else {
				ESFile.readFile(strFileName,$.proxy(function(evt) {
					this.image.src = 'data:image/jpeg;base64,' + evt.target.result;
					var context;
					//canvas = document.getElementById(this.elID); //document.getElementById(strFieldName);
					context = this.canvas.getContext('2d');
					/*MAPR 2017-05-17: Se intentaba dibujar antes de que estuviera lista la imagen
					En todos los casos el dibujo debe ir dentro del onload de la imagen */
					var objTempImage = this.image;
					this.image.onload = function() {
						context.drawImage(objTempImage, 0, 0); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
					};
				},{image:image,canvas:canvas,elID:elID}));
			}
		}
	}
}

//Cierra el dialogo de pantalla completa con el sketch y refresca el sketch en miniatura en la pantalla
function closeAndRefresh(qID) {
	/*if(objSettings.isiOS()){
	window.location.href="zoom://reset";
	    $('head').append("<meta name='viewport' id='devicezoom' content='user-scalable=no, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0'>");
	
	}*/
	//window.location.href="http://xxx";
	$('head').append("<meta name='viewport' id='devicezoom' content='user-scalable=no, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0'>");
	/*if (objSettings.isAndroid()) {
		var tmzoom = window.MyCls.shutDownZoom();
	}*/
	try {
		//document.body.style.zoom = "1.0";
		//document.body.style.zoom='1.0';document.blur();
		document.body.style.webkitTransform = '100%';
		document.body.style.msTransform =  '100%';
		$('body').css("webkitTransform","100%");
		$('body').css("msTransform","100%");
		fromFullScreen = true; //viene de pantalla completa
		
		var objQuestion = selSurvey.questions[qID];
		var elID = 'output'+objQuestion.getFieldName();
		var objByNames = document.getElementsByName(elID);
		var strFieldName = objQuestion.fieldName;
		var coords ='';
		var iobj=0;
		if (objByNames) {
			var ntopobjs = objByNames.length;
			for (iobj=0; iobj<ntopobjs; iobj++) {
				var output = objByNames[iobj];
				coords = output.value;
			}
		}
		
		//tenemos que pintar las coordenadas en el canvas pequeño
		//estas coordenadas estan guardadas en coords
		var canvasPad = $("#" + strFieldName).signaturePad({});
		//no se puede regenerar ya que no respetaria los colores, entonces tenemos que convertirlo a imagen y dibujarla en el canvas pequeño
		//guardamos las coordenadas en el answer de la pregunta
		//objQuestion.answer.setValue(coords);
		 //      canvasPad.regenerate(coords);  
		//obtenemos las referencias para convertir lo del canvas en base 64
		strFieldName = objQuestion.getFieldName();
		elID = "padsignature"+strFieldName;
		var mycanvas = document.getElementById(elID);
		var strSignature = undefined;
		if (objSettings.webMode) {
			//se obtiene la imagen en base 64
			var strSignature = mycanvas.toDataURL('image/jpeg');
			if (strSignature) {
				var comaIndex = strSignature.indexOf(",");
				if (comaIndex > -1) {
					strSignature = strSignature.slice(comaIndex + 1, strSignature.length);
				}
				//strSignature = strSignature.substring(22);
			}
			//objQuestion.answer.photo = strSignature;
		}
		else {
			//es un dispositivo movil
			objQuestion.answer.setValue(coords);
			if (objSettings.isAndroid()) {
				var context = mycanvas.getContext("2d");
				var imageData = context.getImageData(0, 0, mycanvas.width, mycanvas.height);
				var data = imageData; //.data;
				var myEncoder = new JPEGEncoder(50);
				var strSignature = myEncoder.encode(data, 50);
			}
			else {
				var strSignature = mycanvas.toDataURL('image/jpeg');
			}
			if (strSignature) {
				strSignature = strSignature.substring(22);
			}
			//objApp.photoTook(strSignature, undefined, objQuestion.id);
		}
			
		//Ponemos la imagen en el thumbnail de la encuesta
		//strSignature contiene la imagen de lo firmado en base 64
		if (strSignature) {
			//JAPR 2014-12-17: Agregado el upload de la imagen en modo Web
			if (objSettings && objSettings.webMode) {
				//Sube la imagen del sketch al server y recupera la ruta que le fue asignada
				xmlObj = getXMLObject();
				if (xmlObjSketchTimeOut !== null) {
					try {
						clearTimeout(xmlObjSketchTimeOut);
					} catch (e) {
						//Ignora este error
					}
					xmlObjSketchTimeOut = null;
				}
				
				if (xmlObj == null) {
					return;
				}
				
				var urlService = "http://" + objSettings.esurveyFolder + objSettings.esurveyservice;
				//JAPR 2012-11-28: Corregido el protocolo de acceso desde el browser
				if (objSettings && objSettings.webMode && objSettings.protocol) {
					urlService = urlService.replace('http:', objSettings.protocol);
				}
				
				//Prepara el llamado sincrono de la carga del archivo de sketch
				//JAPR 2012-12-17: Modificado para hacer una llamada asíncrona
				xmlObj.open('POST', urlService, true);
				//xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
				xmlObj.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				xmlObj.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
				
				//RTORRES 2015-11-27 Se comenta el timeuot por que realmente no se nesesitaba. 
                /*
                if (xmlObj.timeout) {
                    xmlObj.timeout = (objSettings.uploadTimeOut +1) * 1000;
                }*/
				xmlObjSketchTimeOut = setTimeout(function () {
					//Termina la llamada asíncrona aunque no hubiera regresado
					if (xmlObjSketchTimeOut != null) {
						try {
							clearTimeout(xmlObjSketchTimeOut);
						} catch (e) {
							//Ignora este error
						}
						xmlObjSketchTimeOut = null;
					}
					xmlObj.abort();
					var strErrDesc = '(closeAndRefresh) You have lost your connection, try to connect again later. '
					objApp.abortFileUpload(strErrDesc);
					
					//Hay que cerrar el diálogo aunque no hubiera grabado el contenido del Sketch debido al error de Upload
					fromFullScreen = true; //viene de pantalla completa
					$("#SketchFullScreen_"+qID).dialog('close');
					//si es modo web no se hace el change page
					//selSurvey.changeSection(selSurvey.currentCapture.section.number);
				}, (objSettings.uploadTimeOut * 1000));
				
				//Función que será invocada al terminar el request asíncrono
				xmlObj.onreadystatechange = objApp.sketchUploadCallback;
				
				//Genera los parámetros del request
				var strParams = objSettings.getHttpReqParamsStr() + '&action=uploadfile&ftype=sketch' +
					'&filetype=sketch&data=' + encodeURIComponent(strSignature) + '&responseformat=json';
				var strFileName = '';
				if (selSurvey && objQuestion.section) {
					var intRecordNum = objQuestion.section.recordNum;
					var strSurveyDate = selSurvey.surveyDate;
					var strSurveyHour = selSurvey.surveyHour;
					strSurveyDate = strSurveyDate.replace(/-/gi, '');
					strSurveyHour = strSurveyHour.replace(/:/gi, '');
					var strFileName = 'canvas_' + objSettings.user + '_' + strSurveyDate + '_' + strSurveyHour + '_' + selSurvey.id + '_' + objQuestion.id + '_' + intRecordNum;
					strParams += '&surveyID=' + selSurvey.id + '&questionID=' + objQuestion.id + 
						'&recordNum=' + intRecordNum + '&surveyDate=' + selSurvey.surveyDate + '&surveyHour=' + selSurvey.surveyHour +
						'&filename=' + encodeURIComponent(strFileName);
				}
				//Inicia el llamado síncrono para subir el outbox
				try {
					//Debugger.message(strParams);
					xmlObj.send(strParams);
				} catch (e) {
					//JAPREliminar: Estos errores se deben almacenar en un log, no deben terminar con el proceso
					var strErrDesc = 'There was an error sending the sketch image to the server ' + strFileName + ', error: ' + e;
					objApp.abortFileUpload(strErrDesc);
					
					//Hay que cerrar el diálogo aunque no hubiera grabado el contenido del Sketch debido al error de Upload
					fromFullScreen = true; //viene de pantalla completa
					$("#SketchFullScreen_"+qID).dialog('close');
					//Conchita 2015-01-21 en modo web no se tiene q hacer el changepage puesto que no hay zoom
				   // selSurvey.changeSection(selSurvey.currentCapture.section.number);
				}
				//JAPR
				
				//No continua con el resto de la función para no permitir el changeSection mas abajo, ya que necesita la referencia a la
				//pregunta activa tal como lo haría la toma de una foto
				return;
			}
			//JAPR
			
			//guardamos el strSignature (la imagen del canvas) en el answer.photo de esa pregunta
			//y seteamos la propiedad de esa pregunta para indicar que se debe de guardar lo de fullscreen y no lo del canvas en miniatura
			//objQuestion.answer.photo = strSignature; //esto ya no sera asi porque photo contiene ahora la foto tomada
			objApp.photoSketch(strSignature, undefined, objQuestion.id);
			//objQuestion.answer.canvasPhoto = strSignature; //<---falta grabar como archivo y se graba la ruta 
			//se continua con el proceso normal para pintar el thumbnail con lo rayado en el canvas
			
			objQuestion.answer.fullScreenChange = true;
			/*MAPR 2017-05-15 solucionando problemas con el Sketch que no hacía un limpiado correcto de la pregunta. 
			Si no hay coordenadas no se asigna a la respuesta */
			if (coords){
				objQuestion.answer.setValue(coords); //se asignan las coordenadas calculadas arriba
			}
			///MAPR
			mycanvas = document.getElementById(elID); //obtenemos la referencia al canvas original
			/* MAPR 2017-05-17: Se hacía esta extraña asignación mycanvas = objByNames[0]; que no era necesaria */
			var image = new Image();
			var context = mycanvas.getContext('2d');
			image.src = '';
			try {
				image.onload = function() {
					//ahora se dibuja la imagen en el thumbnail del sketch
					//le asignamos al src del mismo
					var objPhoto = $('#' + strFieldName + '_minisketch');
					if (objPhoto.length > 0) {
						objPhoto.attr('src',image.src);
						objPhoto.css('display', 'block');
					}
				};
			}
			catch(e) {
				Debugger.message(e);
			}
			image.src = 'data:image/jpeg;base64' + strSignature;
		}
		/*MAPR 2017-05-15 solucionando problemas con el Sketch que no hacía un limpiado correcto de la pregunta. 
		Se añade una validación más en el caso de que no tenga una firma en el canvas para eliminar la respuesta (canvasPhoto) */
		else{
			if(objQuestion.answer && objQuestion.answer.canvasPhoto){
				ESFile.deleteFile(objQuestion.answer.canvasPhoto);
				objQuestion.answer.canvasPhoto = '';
			}
		}
		//MAPR
		//window.localStorage.setItem('tmpcanvas', image.src);
		//se cierra el dialogo
		fromFullScreen = true; //viene de pantalla completa
		//JAPR 2016-08-18: Movido este código fuera del try, ya que independientemente de si terminaba bien u ocurriá un error, se ejecuta este mismo bloque así que no tiene caso
		//manterlo repetido en el try y en el catch
	}
	catch(e) {
		//si ocurre algun error al cerrar el dialogo y al guardar se hace el changesection
		//JAPR 2016-08-18: Movido este código fuera del try, ya que independientemente de si terminaba bien u ocurriá un error, se ejecuta este mismo bloque así que no tiene caso
		//manterlo repetido en el try y en el catch
	}
	/* MAPR 2017-05-17: Implementando variable que lista las preguntas Sketch, para evitar volver a dibujar 
	la imagen de la pregunta, porque esta función se encarga de dibujar la vista previa. */
	selSurvey.skipShowPhotosColl[objQuestion.id] = objQuestion.id;
	//MAPR
	//JAPR 2016-08-18: Movido este código fuera del try, ya que independientemente de si terminaba bien u ocurriá un error, se ejecuta este mismo bloque así que no tiene caso
	//manterlo repetido en el try y en el catch
	$("#SketchFullScreen_"+qID).dialog('close');
	//se tiene que hacer 1 change page para que el zoom se resetee solo en movil
	if (objSettings && !objSettings.webMode) {
		//Conchita 2015-01-23 corregido un bug, en las maestro detalle se iba al primer registro al cerrar el dialogo
		//JAPR 2016-08-18: Modificado el esquema de saltos para que se controle el regreso con una pila (#WWIJLY)
		//Estos saltos en realidad son un cambio de página del sketch en ventana completa a la sección donde aún estamos, por tanto no es un salto de sección y no deberá agregar
		//a la pila de historial ni se considera un salto hacia atrás
		if (selSurvey && selSurvey.currentCapture.section.type == SectionType.Multiple && selSurvey.currentCapture.section.recordNum) {
			selSurvey.changeSection(selSurvey.currentCapture.section.number, selSurvey.currentCapture.section.recordNum, undefined, undefined, undefined, undefined, undefined, false, false);
		}
		else {
			selSurvey.changeSection(selSurvey.currentCapture.section.number, undefined, undefined, undefined, undefined, undefined, undefined, false, false);
		}
	}
	//JAPR
}

//cierra los dialogos, cuando se tiene abierto un dialogo de sketch y por cualquier razon marque un error, se manda a llamar a esta funcion
function closeDialogs(){
	//JAPR 2016-03-31: Corregido un bug, el selector estaba obteniendo referencias a todos los diálogos, así que al cerrar todos, se perdía la referencia histórica de navegación y
	//terminaba regresando al main o al login o a alguna sección de la que se hubiera brincado a este punto
	$('[data-role=dialog][id^="SketchFullScreen_"]').dialog("close");
	//JAPR 2019-02-18: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
	//Agregado el cambio de sección debido a las transiciones del Sketch+, ya que se estaba quedando cargado el diálogo de Sketch pero sin regresar a la sección que lo había invocado, como
	//si se hubiera perdido la referencia, esto únicamente cuando se había ingresado por una captura de registros de datos del Sketch
	if ( selSurvey.currentCapture.question && selSurvey.currentCapture.question.type == Type.sketchPlus ) {
		//Antes de realizar el salto, debe limpiar la bandera que indica que el Sketch se encontraba construido o de lo contrario al volver a entrar a la pregunta no aparecerá el OpenLayer,
		//se hace en este punto porque closeDialogs se invoca tanto cuando termina correctamente el proceso como cuando falla por alguna razón, así que siempre pasará por aquí al estar
		//cerrando la ventana de Sketch, que es precisamente lo que debería provocar que se limpie esta bandera
		selSurvey.currentCapture.question.sketchGenerated = false;
		//JAPR 2019-03-01: Agregado el tipo de pregunta Sketch+ (#7WMZIV)
		//Corregido un bug, estaba perdiendo la referencia del registro de la sección que estaba siendo capturada, especificamente con Maestro-detalle, así que ahora se enviará el número
		//de registro que tenía seleccionado, bajo el entendido que ninguna interacción de setActive de las secciones de captura pudo haber sido hacia la propia sección que estaba
		//capturando el sketch, así que aunque currentCapture pudiera haber cambiado el registro activo para capturar datos de los elementos del sketch, la sección de la pregunta sketch
		//no podría haber cambiado el registro activo, así que eso es lo que se usará
		selSurvey.changeSection(selSurvey.currentCapture.question.section.number, selSurvey.currentCapture.question.section.recordNum, undefined, undefined, undefined, undefined, undefined, false);
	}
	//JAPR
	fromFullScreen = true; //viene de pantalla completa
}

//genera el dialogo para desplegar el sketch en tamaño completo independiente de la pantalla principal de la app
function fullScreenSketch(qID) {
	//JAPR 2015-09-09: Validado que en modo diseño no realice el proceso de Sketch
	if (gbDesignMode) {
		return;
	}
	//JAPR
	/*MAPR 2017-05-15 solucionando problemas con el Sketch que no hacía un limpiado correcto de la pregunta. 
	Se eliminan los elementos cargados anteriormente porque al parecer que empalma con canvas anteriores. */
	$('[id^="SketchFullScreen_"]').remove();
	var elID;
	$('head').append("<meta name='viewport' id='devicezoom' content='user-scalable=yes, minimum-scale=1.0, maximum-scale=2.0, initial-scale=1.0'>");
	/*if (objSettings.isAndroid()) {
		var tmzoom = window.MyCls.activateZoom();
	}*/
	
	fromFullScreen = true; //viene de pantalla completa
	var tmpWidth = window.screen.availWidth + 'px';
	var tmpHeight = window.screen.availHeight + 'px';
	var objQuestion = selSurvey.questions[qID];
	var strFieldName = objQuestion.getFieldName(); //objQuestion.fieldName;
	var strDataRole = " style='width:95%;height:95%' data-role=\"dialog\" data-close-btn='none'";
	var btnClose = "<a href='javascript:closeAndRefresh("+objQuestion.id+");'"+" data-inline=\"true\" data-mini=\"true\" data-theme=\"c\" id=\"" + strFieldName + "closefullscreenbutton" +  "\" name=\"" + strFieldName +"\" data-role=\"none\"><img src='images/closesketch.png'/></a>";
	var strHTML = "<div " + strDataRole + " id=\"SketchFullScreen_" + qID+ "\" data-theme=\"c\">\r\n";
	var strBody = "\
		<div id=\"SketchFullScreen_" + qID + "_header\" data-role=\"header\" style='/*max-width:"+tmpWidth+";*/ max-width: none !important; margin:0% 0% 5px 0%; width:100%;' >"+btnClose+"<br><br></div>\r\n\
		<div id=\"SketchFullScreen_" + qID + "_body\" data-role=\"content\" style='height:"+tmpHeight+";/*max-width:"+tmpWidth+";*/ max-width: none !important; margin:0% 0% 5px 0%; width:100%;' data-theme=\"c\">\r\n" ;
	strHTML += strBody;
	
	//hay que dibujar el fondo si tiene o la foto que trae en el nuevo canvas
	//JAPR 2016-03-15: Agregada la variable/constante para generación de previews (sólo puede estar activada desde la consola para generación de HTML que será reemplazado como preview
	var strOnChange = '';
	var strOnMouseOut = '';
	if (!gbGeneratePreview) {
		strOnChange = "onclick=\"selSurvey.questions[" + objQuestion.id + "].updateValue();\" ";
		if (objSettings.webMode) {
			strOnMouseOut = " onmouseout=\"selSurvey.questions[" + objQuestion.id + "].updateValue();\" ";
		}
		else {
			strOnMouseOut = " onchange=\"selSurvey.questions[" + objQuestion.id + "].updateValue();\" ";
		}
	}
	//JAPR
	
    if (objQuestion.answer.photo && !objQuestion.answer.canvasPhoto) {// !objQuestion.answer.fullScreenChange){
	    //seteamos el width y height al tamao configurado para las fotos
	    tmpWidth = objSettings.photoWidth;
	    tmpHeight = objSettings.photoHeight;
    }
    
	//JAPR 2014-12-16: Agregado el valor default para preguntas tipo Foto y Sketch
	var strRoot = '';
	var strDefaultImageFileName = '';
	var image = undefined;
    //JAPR
    
   	//calcular el tamaño del canvas (width y height) si trae imagen de fondo
	if (objQuestion.imageSketch && !objQuestion.answer.canvasPhoto) {
		//limpiamos la ruta de la imagen 
		var tmp = ("'"+objQuestion.imageSketch+"'").match(/(img|src)\=(\"|\')[^\"\'\>]+/g);
		tmp = tmp + '"'; //agregamos la ultima comilla ya que la quita el match
		
		var tmp2 = tmp.replace("src=","");
		tmp2 = tmp2.replace('"',''); //quitamos las comillas
		tmp2 = tmp2.replace('"',''); 
		
		var strFileName = tmp2;	//verificar que tenga algo de foto (ruta)
		if (strFileName != '') {
			//generamos un objeto imagen, le asignamos a su propiedad src la ruta de la imagen
			var image = new Image();
			image.src = replaceVars(strFileName);
			tmpWidth = image.width+'px';
			tmpHeight = image.height+'px';
		}
    }
	//JAPR 2014-12-16: Agregado el valor default para preguntas tipo Foto y Sketch
	else if (!objQuestion.answer.canvasPhoto && !objQuestion.imageSketch) {
		//Si entra a esta parte significa que:
		// !objQuestion.answer.canvasPhoto, por tanto no había sido capturada hasta ahora esta pregunta sketch
		// !objQuestion.imageSketch, por tanto no tiene una imagen default para el fondo, pero en este caso por si sólo podría tener o no una 
		//foto, sin embargo de la manera en que lo programó Conchita, al tomar la foto se habría asignado canvasPhoto por lo que ya no
		//es posible haber entrado aquí dada la primer condición, así que simplemente no hay foto si llegó aquí
		//Dadas estas condiciones, quiere decir que si se puede utilizar el posible valor default de la pregunta para generar el canvas, el
		//cual dado a la manera en que lo programaron, no podrá salir de este diálogo sin asignar canvasPhoto, así que sólo una vez es posible
		//asignar el defaultValue como imagen de fondo del sketch habiendo entrado a la captura
		//En este caso no importa mucho si es o no modo Web, en ambos casos pasará por el mismo código, lo único importante es que si es modo
		//Web entonces la ruta obtenida debe ser validada en busca de absolutos, lo cual no aplica para móviles
		var blnChangeRoot = (objSettings && objSettings.webMode);
		var objSectionEvalRecord = undefined;
		var blnEvaluateDefaultValue = false;
		var strFileName = Trim(replaceVars(objQuestion.defaultValue, undefined, undefined, undefined, undefined, objSectionEvalRecord, blnEvaluateDefaultValue));
		if (strFileName) {
			strDefaultImageFileName = strFileName;
			//Si el valor default devuelto (que tentativamente es una ruta de imagen) inicia con http, se asume una ruta absoluta por lo
			//que se configura la variable para no forzar a utilizar el Web Service de eForms tal como lo harían las fotos normales, de
			//lo contrario al asumir una ruta relativa si se tendrá que usar
			if (strFileName.toLowerCase().indexOf('http') == 0) {
				blnChangeRoot = false;
			}
			
			//JAPR 2014-12-17: En modo Web el canvas genera un error del tipo
			//Uncaught SecurityError: Failed to execute 'toDataURL' on 'HTMLCanvasElement': Tainted canvases may not be exported.
			//cuando se utiliza una URL absoluta como imagen del canvas, por tanto no se podrán ajustar las URLs para agregar el protocolo, y
			//de hecho si la URL venía de eBavel y por ende contenía ya el protocolo http, no se podrá usar en absoluto (por el momento no hay
			//manera de evitar eso, ya que se tendría que abrir la imagen y obtener su B64 para usarlo en lugar de la URL). Se agregó el false
			//para dejar las rutas como relativas forzando a entrar al else en lo que se resuelve correctamente
			if (blnChangeRoot && false) {
		    	//JAPR 2012-12-13: Faltaba agregar el path absoluto a la imagen, así que realmente la buscaba en el root de entrada y no
		    	//necesariamente al del servicio donde finalmente la graba
				var strRoot = "http://" + objSettings.esurveyFolder;
				if (objSettings.protocol) {
					strRoot = strRoot.replace('http:', objSettings.protocol);
				}
			}
			else {
				//En este caso el propio valor de imagen ya trae una ruta absoluta
				var strRoot = '';
				//JAPR 2014-12-17: En modo Web el canvas genera un error del tipo
				//Uncaught SecurityError: Failed to execute 'toDataURL' on 'HTMLCanvasElement': Tainted canvases may not be exported.
				//cuando se utiliza una URL absoluta como imagen del canvas, por tanto no se podrán ajustar las URLs para agregar el protocolo, y
				//de hecho si la URL venía de eBavel y por ende contenía ya el protocolo http, no se podrá usar en absoluto (por el momento no hay
				//manera de evitar eso, ya que se tendría que abrir la imagen y obtener su B64 para usarlo en lugar de la URL)
				if (objSettings && objSettings.webMode && strFileName.toLowerCase().indexOf('http') == 0) {
					//Si la ruta es absoluta, por ahora no se podrá utilizar esta imagen en modo Web
					strDefaultImageFileName = '';
				}
			}
		}
		
		//Genera la imagen y asigna los tamaños, ya que con la imagen mas adelante se configurará un onload y con los tamaños se preparará
		//el HTML (JAPRWarning:¿Realmente aquí ya se determinó el tamaño, seguramente esto es un error, pero así lo había programado Conchita
		//así que se dejó por consistencia, si se corrige algún día hay que corregir también esta parte)
		if (strDefaultImageFileName) {
			var image = new Image();
			image.src = strRoot + strDefaultImageFileName;
			tmpWidth = image.width+'px';
			tmpHeight = image.height+'px';
		}
	}
	//JAPR
	
    if (!tmpWidth && !tmpHeight || tmpWidth == '0px') {
        tmpWidth = window.screen.availWidth + 'px';
        tmpHeight = window.screen.availHeight + 'px';
    }
	if (objQuestion.answer.canvasPhoto && objQuestion.answer.photo == objQuestion.answer.canvasPhoto && objQuestion.answer.canvasPhoto!='') {// !objQuestion.answer.fullScreenChange){
	    //seteamos el width y height al tamao configurado para las fotos
	    tmpWidth = objSettings.photoWidth;
	    tmpHeight = objSettings.photoHeight;
    }
    
    if (!objQuestion.answer.canvasPhoto) {
		var inkColors = '';
		var fullScreenButton = '';
		var divWrapperSize = tmpWidth;
		var displayStr = " style='display:none'";
		var inkColors = '<a href="javascript:changeInkColor(\'0060ff\','+objQuestion.id+');"><img class="colorSquare" src="images/azul.png" id="azul_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'000000\','+objQuestion.id+');"><img class="colorSquare" src="images/negro.png" id="negro_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffffff\','+objQuestion.id+');"><img class="colorSquare" src="images/blanco.png" id="blanco_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'fe0000\','+objQuestion.id+');"><img class="colorSquare" src="images/rojo.png" id="rojo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffde00\','+objQuestion.id+');"><img class="colorSquare" src="images/amarillo.png" id="amarillo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'727272\','+objQuestion.id+');"><img class="colorSquare" src="images/gris.png" id="gris_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ff8a00\','+objQuestion.id+');"><img class="colorSquare" src="images/naranja.png" id="naranja_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'2fc501\','+objQuestion.id+');"><img class="colorSquare" src="images/verde.png" id="verde_'+ objQuestion.id +'"/></a>';
		strHTML += "\
					<div " + " class=\"divwrapper\">\r\n"+inkColors;			
		var signaturepad_html = '';
		var sigpad = '<div id="' + strFieldName + '" class="sigPad sigPadforms' + objQuestion.getFieldName() + '">\r\n'; //conchita antes era sigpad
		var signaturefieldcontainStyle = '';
		var signaturefieldcontain = '<div data-inline="true"' + signaturefieldcontainStyle + '>';
		var signaturefield_ul = '<ul class="sigNav" '+'>';
		var signaturefield_drawit = '<li class="drawIt"'+displayStr+'><a href="" >'+translate("Draw your signature")+'</a></li>';
		var signaturefield_clear = '<li class="clearButton" onclick="restoreFullScreenCanvasBg('+objQuestion.id+',\''+strDefaultImageFileName+'\');"><a href="">'+translate("Clear")+'</a></li>'; //eraseCanvas('+this.getFieldName()+')
		var signaturefield_ul_close = '</ul>';
		var signaturefield_ul_html = signaturefield_ul + signaturefield_drawit + signaturefield_clear + signaturefield_ul_close;
		//MAPR 2017-05-17: Las variables ya tenían asignado los px, no había necesidad de agregar en esta instancia
		var signaturefield_sigwrapper = '<div  class="sig sigWrapper" style="width:'+divWrapperSize+'; height:'+tmpHeight+ ' !important; overflow:hidden;">';
		var signaturefield_typed = '<div class="typed"></div>';
		var signaturefield_canvas = '<canvas width="'+tmpWidth+'"'+strOnChange+strOnMouseOut +' name="padsignature'+strFieldName+'"'+' id="padsignature'+strFieldName+'" class="pad"  height="'+tmpHeight+'"></canvas>';
		var signaturefield_output = '<input name="output'+objQuestion.getFieldName() +'" type="hidden"'+strOnChange+ 'name="output" class="output" id="output'+objQuestion.getFieldName() +'"/>';
		var signaturefield_sigwrapper_close = '</div>';
		var signaturefield_sigwrapper_html = signaturefield_sigwrapper + signaturefield_typed +	signaturefield_canvas +	signaturefield_output +	signaturefield_sigwrapper_close;
		var signaturefield_inputhidden = '<input type="hidden" id="qsignature'+objQuestion.getFieldName()+'" name="qimageencode'+objQuestion.getFieldName()+'" value=""/>';
		//Conchita 2012-05-22 Se cambio qsignature por qimageenconde en el name
		var signaturefieldcontain_close = '</div>';
		var signaturefieldcontain_html = signaturefieldcontain + signaturefield_ul_html + signaturefield_sigwrapper_html + signaturefield_inputhidden + signaturefieldcontain_close;
		var sigpadclose = '</div></div>';
		signaturepad_html += sigpad + signaturefieldcontain_html + sigpadclose;
		
		strHTML += signaturepad_html;
		strHTML += '</div></div>';
		$(document.body).append(strHTML);
		//$('#SketchFullScreen_' + qID).page();
		//$.mobile.changePage('#SketchFullScreen_' + qID);
        $.mobile.changePage($('#SketchFullScreen_' + qID), {
            transition : 'pop',
            reverse    : true,
            changeHash : true
        });
		//tenemos que obtener lo dibujado en el sketch pequeño y pasarlo al de full screen
		strFieldName = objQuestion.getFieldName();
		elID = "padsignature"+strFieldName;
		
		//strSignature contiene el base 64 de lo que hay pintado en el canvas
		//tenemos que ponerlo en el canvas grande
		if (objQuestion.imageSketch) { //|| (objQuestion.answer.photo && objQuestion.answer.fullScreenChange)){
			var objByNames = document.getElementsByName(elID);
			if (objByNames)
			{
				var ntopobjs = objByNames.length;
				var canvas = objByNames[ntopobjs-1];
				var context = canvas.getContext('2d');
				//var image = new Image();
				//image.src = 'data:image/jpeg;base64' + strSignature;
		        //solo para dos casos dibujamos antes en el or && objQuestion.answer.fullScreenChange
		        //JAPR 2014-12-16: Esta validación estaba mal planteada, al haber entrado aquí ambas condiciones ya eran verdaderas pues se
		        //validaron en los Ifs anteriores, la verdadera pregunta es si está o no asignado el objeto image
		        //if ((objQuestion.imageSketch && !objQuestion.answer.canvasPhoto)) {
		        if (image) {
		        	image.onload = function() {
						context.drawImage(image, 0, 0,canvas.width,canvas.height);
		            }
				}
			}
		}
		//JAPR 2014-12-16: Agregado el valor default para preguntas tipo Foto y Sketch
		else {
			//Si entra a esta parte significa que:
			// !objQuestion.answer.canvasPhoto, por tanto no había sido capturada hasta ahora esta pregunta sketch
			// !objQuestion.imageSketch, por tanto no tiene una imagen default para el fondo, pero en este caso por si sólo podría tener o no una 
			//foto, sin embargo de la manera en que lo programó Conchita, al tomar la foto se habría asignado canvasPhoto por lo que ya no
			//es posible haber entrado aquí dada la primer condición, así que simplemente no hay foto si llegó aquí
			//Dadas estas condiciones, quiere decir que si se puede utilizar el posible valor default de la pregunta para generar el canvas, el
			//cual dado a la manera en que lo programaron, no podrá salir de este diálogo sin asignar canvasPhoto, así que sól una vez es posible
			//asignar el defaultValue como imagen de fondo del sketch habiendo entrado a la captura
			//Para este momento ya se había creado el objeto image y se tiene en la variable strDefaultImageFileName el nombre de archivo de
			//imagen default que finalmente se utilizó
			//if (objSettings) { // && objSettings.webMode
				if (strDefaultImageFileName) {
					var objByNames = document.getElementsByName(elID);
					if (objByNames)
					{
						var ntopobjs = objByNames.length;
						var canvas = objByNames[ntopobjs-1];
						var context = canvas.getContext('2d');
						//var image = new Image();
						//image.src = 'data:image/jpeg;base64' + strSignature;
				        //solo para dos casos dibujamos antes en el or && objQuestion.answer.fullScreenChange
				        if (image) {
				        	image.onload = function() {
								context.drawImage(image, 0, 0,canvas.width,canvas.height);
				            }
						}
					}
				}
			/*}
			else {
				
			}*/
		}
		//JAPR
	}//fin de si no if(!objQuestion.answer.canvasPhoto )
	
	//se cambia antes era la photo ahora seria el canvasPhoto
	//si hay una imagen grabada
	//si se tomo la foto hay que ponerla en el canvas grande && !objQuestion.answer.fullScreenChange
	try {
		if (objQuestion.answer.canvasPhoto) {
	     	var strFileName = objQuestion.answer.canvasPhoto;
			//JAPR 2014-12-17: Agregado el valor default para preguntas tipo Foto y Sketch
			//JAPRWarning: Esto se debería optimizar almacenando las dimensiones de la imagen del sketch en lugar de generarlas cada vez con
			//un tag Image dinámico
	     	if (objSettings && objSettings.webMode) {
	            var image = new Image();
	            var elID;
                //2015.01.20 JCEM absoluto
                var strRoot = "http://" + objSettings.esurveyFolder;
				if (objSettings.protocol) {
					strRoot = strRoot.replace('http:', objSettings.protocol);
				}
                
				//JAPR 2016-03-31: Corregido un bug en Web, si la imagen cambia, se estaba tomando de cache ya que conserva el mismo nombre, así que se agrega algo para que no use cache
				//JAPR 2018-02-28: Se regresó el código para impedir que la imagen fuera tomada de caché, ya que alguien lo había comentado y estaba provocando el mismo error con Sketch
				//editado desde browser que el error original, pero no mencionaron por qué lo comentaron (#6MLZ2L)
				image.src = strRoot + strFileName + ((objSettings.webMode)?"?dte="+((CurrentDate() + CurrentTime()).replace(/-/gi, '').replace(/:/gi, '')):"");
				//image.src = strRoot + strFileName;
	            //JAPR
	            try {
				image.onload = function() {
					var objQuestion = selSurvey.currentCapture.question;
		            //despues de leer tenemos que dibujar el dialog con el canvas con el tmpwidth y tmpheight correspondientes
		            //a la imagen que se saco
		            var tmpWidth = image.width;
					//Conchita 2015-01-21 se agrego px al height ya que sino no lo tomaba
		            var tmpHeight = image.height + 'px';
				
		            var inkColors = '';
					var fullScreenButton = '';
					var divWrapperSize = tmpWidth;
					var displayStr = " style='display:none'";
					var inkColors = '<a href="javascript:changeInkColor(\'0060ff\','+objQuestion.id+');"><img class="colorSquare" src="images/azul.png" id="azul_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'000000\','+objQuestion.id+');"><img class="colorSquare" src="images/negro.png" id="negro_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffffff\','+objQuestion.id+');"><img class="colorSquare" src="images/blanco.png" id="blanco_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'fe0000\','+objQuestion.id+');"><img class="colorSquare" src="images/rojo.png" id="rojo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffde00\','+objQuestion.id+');"><img class="colorSquare" src="images/amarillo.png" id="amarillo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'727272\','+objQuestion.id+');"><img class="colorSquare" src="images/gris.png" id="gris_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ff8a00\','+objQuestion.id+');"><img class="colorSquare" src="images/naranja.png" id="naranja_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'2fc501\','+objQuestion.id+');"><img class="colorSquare" src="images/verde.png" id="verde_'+ objQuestion.id +'"/></a>';
					strHTML += "\
								<div " + " class=\"divwrapper\">\r\n"+inkColors;			
					var signaturepad_html = '';
					var sigpad = '<div id="' + strFieldName + '" class="sigPad sigPadforms' + objQuestion.getFieldName() + '">\r\n'; //conchita antes era sigpad
					var signaturefieldcontainStyle = '';
					var signaturefieldcontain = '<div data-inline="true"' + signaturefieldcontainStyle + '>';
					var signaturefield_ul = '<ul class="sigNav" '+'>';
					var signaturefield_drawit = '<li class="drawIt"'+displayStr+'><a href="" >'+translate("Draw your signature")+'</a></li>';
					var signaturefield_clear = '<li class="clearButton" onclick="restoreFullScreenCanvasBg('+objQuestion.id+',\''+strDefaultImageFileName+'\');"><a href="">'+translate("Clear")+'</a></li>'; //eraseCanvas('+this.getFieldName()+')
					var signaturefield_ul_close = '</ul>';
					var signaturefield_ul_html = signaturefield_ul + signaturefield_drawit + signaturefield_clear + signaturefield_ul_close;
					var signaturefield_sigwrapper = '<div  class="sig sigWrapper" style="width:'+divWrapperSize+'px; height:'+tmpHeight+ 'px !important; overflow:hidden;">';
					var signaturefield_typed = '<div class="typed"></div>';
					var signaturefield_canvas = '<canvas width="'+tmpWidth+'"'+strOnChange+strOnMouseOut +' name="padsignature'+strFieldName+'"'+' id="padsignature'+strFieldName+'" class="pad"  height="'+tmpHeight+'"></canvas>';
					var signaturefield_output = '<input name="output'+objQuestion.getFieldName() +'" type="hidden"'+strOnChange+ 'name="output" class="output" id="output'+objQuestion.getFieldName() +'"/>';
					var signaturefield_sigwrapper_close = '</div>';
					var signaturefield_sigwrapper_html = signaturefield_sigwrapper + signaturefield_typed +	signaturefield_canvas +	signaturefield_output +	signaturefield_sigwrapper_close;
					var signaturefield_inputhidden = '<input type="hidden" id="qsignature'+objQuestion.getFieldName()+'" name="qimageencode'+objQuestion.getFieldName()+'" value=""/>';
					//Conchita 2012-05-22 Se cambio qsignature por qimageenconde en el name
					var signaturefieldcontain_close = '</div>';
					var signaturefieldcontain_html = signaturefieldcontain + signaturefield_ul_html + signaturefield_sigwrapper_html + signaturefield_inputhidden + signaturefieldcontain_close;
					var sigpadclose = '</div></div>';
					signaturepad_html += sigpad + signaturefieldcontain_html + sigpadclose;
					
					strHTML += signaturepad_html;
					strHTML += '</div></div>';
					$(document.body).append(strHTML);
					//$('#SketchFullScreen_' + qID).page();
					$.mobile.changePage('#SketchFullScreen_' + objQuestion.id);
		            
					//tenemos que obtener lo dibujado en el sketch pequeño y pasarlo al de full screen
					strFieldName = objQuestion.getFieldName();
					elID = "padsignature"+strFieldName;
		            
		            //fin de despues de leer el archivo
					var canvas = document.getElementById(elID);//objByNames[ntopobjs-1];
					var context = canvas.getContext('2d');
         			context.drawImage(image, 0, 0,image.width,image.height); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
	   			}
				}
	            catch(e) {
	            	Debugger.message(e);
	            }
	     	}
	     	else {
		     	ESFile.readFile(strFileName,$.proxy(function(evt) {
		            var image = new Image();
		            var dataIm = evt.target.result;
		            var elID;
		            
		            if (dataIm.substring(0,1)==',') {
						image.src = 'data:image/jpeg;base64' + dataIm;
		            }
		            else {
		            	image.src = 'data:image/jpeg;base64,' + dataIm;
		            }
		            /*MAPR 2017-05-17: Se intentaba dibujar antes de que estuviera lista la imagen
					En todos los casos el dibujo debe ir dentro del onload de la imagen */
		            image.onload = function() {
			            try {
							var objQuestion = selSurvey.currentCapture.question;
				            //despues de leer tenemos que dibujar el dialog con el canvas con el tmpwidth y tmpheight correspondientes
				            //a la imagen que se saco
				            var tmpWidth = image.width;
				            var tmpHeight = image.height;
				            var inkColors = '';
							var fullScreenButton = '';
							var divWrapperSize = tmpWidth;
							var displayStr = " style='display:none'";
							var inkColors = '<a href="javascript:changeInkColor(\'0060ff\','+objQuestion.id+');"><img class="colorSquare" src="images/azul.png" id="azul_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'000000\','+objQuestion.id+');"><img class="colorSquare" src="images/negro.png" id="negro_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffffff\','+objQuestion.id+');"><img class="colorSquare" src="images/blanco.png" id="blanco_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'fe0000\','+objQuestion.id+');"><img class="colorSquare" src="images/rojo.png" id="rojo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ffde00\','+objQuestion.id+');"><img class="colorSquare" src="images/amarillo.png" id="amarillo_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'727272\','+objQuestion.id+');"><img class="colorSquare" src="images/gris.png" id="gris_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'ff8a00\','+objQuestion.id+');"><img class="colorSquare" src="images/naranja.png" id="naranja_'+ objQuestion.id +'"/></a> <a href="javascript:changeInkColor(\'2fc501\','+objQuestion.id+');"><img class="colorSquare" src="images/verde.png" id="verde_'+ objQuestion.id +'"/></a>';
							strHTML += "\
										<div " + " class=\"divwrapper\">\r\n"+inkColors;			
							var signaturepad_html = '';
							var sigpad = '<div id="' + strFieldName + '" class="sigPad sigPadforms' + objQuestion.getFieldName() + '">\r\n'; //conchita antes era sigpad
							var signaturefieldcontainStyle = '';
							var signaturefieldcontain = '<div data-inline="true"' + signaturefieldcontainStyle + '>';
							var signaturefield_ul = '<ul class="sigNav" '+'>';
							var signaturefield_drawit = '<li class="drawIt"'+displayStr+'><a href="" >'+translate("Draw your signature")+'</a></li>';
							var signaturefield_clear = '<li class="clearButton" onclick="restoreFullScreenCanvasBg('+objQuestion.id+',\''+strDefaultImageFileName+'\');"><a href="">'+translate("Clear")+'</a></li>'; //eraseCanvas('+this.getFieldName()+')
							var signaturefield_ul_close = '</ul>';
							var signaturefield_ul_html = signaturefield_ul + signaturefield_drawit + signaturefield_clear + signaturefield_ul_close;
							var signaturefield_sigwrapper = '<div  class="sig sigWrapper" style="width:'+divWrapperSize+'px; height:'+tmpHeight+ 'px !important; overflow:hidden;">';
							var signaturefield_typed = '<div class="typed"></div>';
							var signaturefield_canvas = '<canvas width="'+tmpWidth+'"'+strOnChange+strOnMouseOut +' name="padsignature'+strFieldName+'"'+' id="padsignature'+strFieldName+'" class="pad"  height="'+tmpHeight+'"></canvas>';
							var signaturefield_output = '<input name="output'+objQuestion.getFieldName() +'" type="hidden"'+strOnChange+ 'name="output" class="output" id="output'+objQuestion.getFieldName() +'"/>';
							var signaturefield_sigwrapper_close = '</div>';
							var signaturefield_sigwrapper_html = signaturefield_sigwrapper + signaturefield_typed +	signaturefield_canvas +	signaturefield_output +	signaturefield_sigwrapper_close;
							var signaturefield_inputhidden = '<input type="hidden" id="qsignature'+objQuestion.getFieldName()+'" name="qimageencode'+objQuestion.getFieldName()+'" value=""/>';
							//Conchita 2012-05-22 Se cambio qsignature por qimageenconde en el name
							var signaturefieldcontain_close = '</div>';
							var signaturefieldcontain_html = signaturefieldcontain + signaturefield_ul_html + signaturefield_sigwrapper_html + signaturefield_inputhidden + signaturefieldcontain_close;
							var sigpadclose = '</div></div>';
							signaturepad_html += sigpad + signaturefieldcontain_html + sigpadclose;
							
							strHTML += signaturepad_html;
							strHTML += '</div></div>';
							$(document.body).append(strHTML);
							//$('#SketchFullScreen_' + qID).page();
							$.mobile.changePage('#SketchFullScreen_' + objQuestion.id);
				            
							//tenemos que obtener lo dibujado en el sketch pequeño y pasarlo al de full screen
							strFieldName = objQuestion.getFieldName();
							elID = "padsignature"+strFieldName;
				            
				            //fin de despues de leer el archivo
							var canvas = document.getElementById(elID);//objByNames[ntopobjs-1];
							var context = canvas.getContext('2d');
		         			context.drawImage(image, 0, 0,image.width,image.height); //hay que esperar hasta q se cargue la imagen para ponerla en el canvas
			   			}
			            catch(e) {
			            	Debugger.message(e);
			            }
			        };
				}, {elID:elID,qID:qID}));
	     	}
	    }
	} catch(e) {Debugger.message(e);}
}

//fix borde de la firma se sale
//Conchita 2014-12-29
//JAPR 2016-03-02: Rediseñados los estilos para dejar look mas parecido a IOs9 (JIRH)
//jirh cambio de bind por .on
$(window).on('orientationchange resize', function(event) {
	//JIRH active page cambia a getActivepage por jquery updates
	//var currPage = $.mobile.activePage.attr('id');
	var currPage = $(":mobile-pagecontainer").pagecontainer('getActivePage').attr('id');
	if (currPage && currPage.indexOf('section')>=0) {
		//$('.sigWrapper').css('width', '400px');
	}
	return;
	//JAPR
	
    var tmp = document.width*0.68;
    if (objSettings.isiPad()){
        if (window.orientation == 90 || window.orientation == -90) {
			//$('.divwrapper').css('width', '100%');
			$('.divwrapper').css('width', '900px');
        } else {
			//$('.divwrapper').css('width', '100%');
			$('.sigWrapper').css('width', '700px');
			$('.divwrapper').css('width', '700px');
        }
    } else {
        if (window.orientation == 90 || window.orientation == -90) {
			// $('.divwrapper').css('width', '60%');
			$('.divwrapper').css('width', '320px');
        } else {
			//$('.divwrapper').css('width', '100%');
			$('.divwrapper').css('width', '285px');
			$('.sigWrapper').css('width', '285px');
        }
    }
});

//JAPR 2015-01-14: Corregido un bug, se estaban regenerando los markers con cada refresh y además era un array así que buscaba los markers
//por la etiqueta en lugar de haber usado un objeto para buscar por la combinación de padres directamente (#2X267B)
/* Regresa todos los valores de un Objeto en el orden de sus propiedades/llaves como un string separado por el parámetro sSep
Esta función está pensada exclusivamente para objetos que representan filtros de preguntas, por tanto sus llaves son numéricas iniciando en 0
hasta el máximo número de atributo usado en el filtro, no se debería utilizar para otro tipo de objetos
*/
function getCatalogFilterString(oCatalogFilter, sSep) {
	if (!oCatalogFilter) {
		return '';
	}
	
	if (sSep === undefined) {
		sSep = msCFSep;
	}
	
	var strCatalogFilter = '';
	try {
		var arrValues = new Array();
		for (var intAttribNum in oCatalogFilter) {
			arrValues.push(oCatalogFilter[intAttribNum]);
		}
		strCatalogFilter = arrValues.join(sSep);
	}
	catch (e) {}
	
	return strCatalogFilter;
}

//JAPR 2015-05-26: Rediseñado el Admin con DHTMLX
/* Muestra el border de selección alrededor del div de una pregunta (sólo en modo de diseño) */
function showSelectionBorder(oTarget) {
	if (!oTarget || !oTarget.id) {
		return;
	}
	
	var objElement = $('#' + oTarget.id);
	if (!objElement || !objElement.length) {
		return;
	}
	
	objElement.css('border-style', 'dotted');
	objElement.css('border-width', '2')
	objElement.css('border-color', 'blue');
}

/* Oculta el border de selección alrededor del div de una pregunta (sólo en modo de diseño) */
function hideSelectionBorder(oTarget) {
	if (!oTarget || !oTarget.id) {
		return;
	}
	
	var objElement = $('#' + oTarget.id);
	if (!objElement || !objElement.length) {
		return;
	}
	
	objElement.css('border-style', 'initial');
	objElement.css('border-width', 'initial')
	objElement.css('border-color', 'initial');
}

/* Resalta la pregunta indicada con un color en toda su área visible */
function setSelectedQuestion(oTarget) {
	if (!oTarget || !oTarget.id) {
		return;
	}
	
	var strId = oTarget.id;
	if (oTarget.getFieldName) {
		strId = oTarget.getFieldName() + '_area';
	}
	
	var objElement = $('#' + strId);
	if (!objElement || !objElement.length) {
		return;
	}
	
	objElement.css('background-color', myQuestionSelCol);
}

/* Elimina el resaltado de la pregunta indicada restableciendo el color original en toda su área visible, o bien lo hace en todas las preguntas si no se 
especifica alguna directamente */
function unsetSelectedQuestion(oTarget) {
	if (!oTarget || !oTarget.id) {
		//En este caso limpiará el estado de selección de todas las preguntas
		$("div[id$='_area']").css('background-color', 'initial');
		return;
	}
	
	var strId = oTarget.id;
	if (oTarget.getFieldName) {
		strId = oTarget.getFieldName() + '_area';
	}
	
	var objElement = $('#' + strId);
	if (!objElement || !objElement.length) {
		return;
	}
	
	objElement.css('background-color', 'initial');
}

//JAPR 2015-06-23: Agregado el Drag & Drop de preguntas para reordenar
/* Dado un Id de un div que se asume es un div _area de una pregunta, identifica el Id de la pregunta y lo regresa como resultado de la función. Se consideran
casos donde el div pudiera estar generado dentro de una sección multi-registro
*/
function getQuestionIDFromDivArea(sDivID) {
	var intQuestionNum = "";
	
	if (!sDivID) {
		sDivID = "";
	}
	try {
		intQuestionNum = sDivID.substr(0, sDivID.indexOf("_")).replace("qfield", '');
	}
	catch (e) {
		//En caso de error simplemente regresará un 0
	}
	
	if (!$.isNumeric(intQuestionNum)) {
		intQuestionNum = 0;
	}
	else {
		intQuestionNum = parseInt(intQuestionNum);
	}
	
	var intQuestionID = 0;
	if (selSurvey && selSurvey.questionsOrder) {
		intQuestionID = selSurvey.questionsOrder[intQuestionNum-1];
	}
	
	return intQuestionID;
}

//JAPR 2015-08-02: Agregadas las funciones del editor de fórmulas

//OMMC 2015-10-15: Función de geolocalización

var rad = function(x) {
  return x * Math.PI / 180;
};

function Dist(latitudeO, longitudeO, latitudeD, longitudeD){
	//Validamos si son nulas o permitidas
	latO = (isNaN(latitudeO))?0:latitudeO;
	lonO = (isNaN(longitudeO))?0:longitudeO;
	latD = (isNaN(latitudeD))?0:latitudeD;
	lonD = (isNaN(longitudeD))?0:longitudeD;

	var R = 6371000; //Radio promedio de la tierra del centro a la superficie en metros
	var dLat = rad(latD - latO);
	var dLong = rad(lonD - lonO);
	var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(latO)) * Math.cos(rad(latD)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
	var b = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	var dist = R * b;
	return dist;
}


//OMMC 2015/08/28: Genera una cláusula IF
function If(conditional, trueState, falseState){
	return (conditional) ? trueState: falseState;
}

//Genera un string con todos los parámetros proporcionados concatenados
function Concat() {
	var intLength = arguments.length;
	var strResult = '';
	for (var intIdx = 0; intIdx < intLength; intIdx++) {
		strResult += arguments[intIdx];
	}
	
	return strResult;
}

//Obtiene los primeros n caracteres del string indicado
function Left(sText, nLength) {
	var strResult = String(sText);
	
	if (!$.isNumeric(nLength)) {
		nLength = 0;
	}
	if (nLength < 0) {
		nLength *= -1;
	}
	return strResult.substr(0, nLength);
}

//Obtiene los últimos n caracteres del string indicado
function Right(sText, nLength) {
	var strResult = String(sText);
	
	if (!$.isNumeric(nLength)) {
		nLength = 0;
	}
	if (nLength < 0) {
		nLength *= -1;
	}
	return strResult.substr(nLength * -1);
}

//Extrae la porción indicada del string proporcionado, comenzando en el catacter iPos hasta la nLength
function Mid(sText, iPos, nLength) {
	var strResult = String(sText);
	
	if (!$.isNumeric(iPos)) {
		iPos = 0;
	}
	if (iPos < 0) {
		iPos *= -1;
	}
	if (nLength !== undefined) {
		if (!$.isNumeric(nLength)) {
			nLength = 0;
		}
		if (nLength < 0) {
			nLength *= -1;
		}
	}
	return strResult.substr(iPos, nLength);
}

//Regresa la fecha actual en formato universal
function CurrentDate() {
	return getDate(new Date());
}

//Regresa la hora en formato universal
function CurrentTime() {
	return getTime(new Date());
}

//Obtiene el año de la fecha indicada
function Year(oDate) {
	if (!oDate) {
		return '';
	}
	
	try {
		if ((typeof oDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oDate = myToDate(oDate + '' + ' 00:00:00');
		}
		
		return getFormattedDate(oDate, "yyyy");
	} catch(e) {
		return '';
	}
}

//Obtiene el mes de la fecha indicada
function Month(oDate, bLeadZ) {
	if (!oDate) {
		return '';
	}
	
	try {
		if ((typeof oDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oDate = myToDate(oDate + '' + ' 00:00:00');
		}
		
		return getFormattedDate(oDate, "m" + ((!bLeadZ)?'':'m'));
	} catch(e) {
		return '';
	}
}

//Obtiene el día de la fecha indicada
function Day(oDate, bLeadZ) {
	if (!oDate) {
		return '';
	}
	
	try {
		if ((typeof oDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oDate = myToDate(oDate + '' + ' 00:00:00');
		}
		
		return getFormattedDate(oDate, "d" + ((!bLeadZ)?'':'d'));
	} catch(e) {
		return '';
	}
}

//Obtiene el año de la fecha indicada
function CurrentYear() {
	try {
		return Year(CurrentDate());
	} catch(e) {
		return '';
	}
}

//Obtiene el mes de la fecha indicada
function CurrentMonth(bLeadZ) {
	try {
		return Month(CurrentDate(), bLeadZ);
	} catch(e) {
		return '';
	}
}

//Obtiene el día de la fecha indicada
function CurrentDay(bLeadZ) {
	try {
		return Day(CurrentDate(), bLeadZ);
	} catch(e) {
		return '';
	}
}

/* Extraída de eBavel como apoyo de DateDiff */
function eBavelDifDate(pFechaUno,pFechaDos) {
	var vDiasMes =[31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
	var vFechaOrigen;
	var vFechaDestino;
	var vAnio;
	var vMes;
	var vDia;
	var vIncremento;
	//Determinamos cual es la fecha menor
	if (pFechaUno > pFechaDos)
	{
		vFechaOrigen = pFechaDos;
		vFechaDestino = pFechaUno;
	}
	else
	{
		vFechaOrigen = pFechaUno;
		vFechaDestino = pFechaDos;
	}

	// Calculamos los dias
	vIncremento = 0;


	if (vFechaOrigen.getDate() > vFechaDestino.getDate())
	{
		vIncremento = vDiasMes[vFechaOrigen.getMonth()];

	}
	if (vIncremento == -1)
	{
		//if (DateTime.IsLeapYear(vFechaOrigen.getFullYear()))
	if ((vFechaOrigen.getFullYear() % 4 == 0) && ((vFechaOrigen.getFullYear() % 100 != 0) || (vFechaOrigen.getFullYear() % 400 == 0)))
		{
			// Para los años bisiestos
			vIncremento = 29;
		}
		else
		{
			vIncremento = 28;
		}
	}
	if (vIncremento != 0)
	{
		vDia = (vFechaDestino.getDate() + vIncremento) - vFechaOrigen.getDate();
		vIncremento = 1;
	}
	else
	{
		vDia = vFechaDestino.getDate() - vFechaOrigen.getDate();
	}

	//Calculamos los meses
	if ((vFechaOrigen.getMonth() + vIncremento) > vFechaDestino.getMonth())
	{
		vMes = (vFechaDestino.getMonth() + 12) - (vFechaOrigen.getMonth() + vIncremento);
		vIncremento = 1;
	}
	else
	{
		vMes = (vFechaDestino.getMonth()) - (vFechaOrigen.getMonth() + vIncremento);
		vIncremento = 0;
	}
	//Calculamos los años
	vAnio = vFechaDestino.getFullYear() - (vFechaOrigen.getFullYear() + vIncremento);
	var numMes = (vAnio*12)+ vMes;
	return [vAnio , vMes, vDia, numMes]; 
}

//Obtiene la diferencia del sType correspondiente entre las fechas final e inicial proporcionadas
function DateDiff(oEndDate, oStartDate, sType) {
	try {
		sType = String(sType).toLowerCase();
		var arrTypes = ["day", "month", "year"];
		if (arrTypes.indexOf(sType) == -1) {
			sType = "Day";
		}
		
		if ((typeof oEndDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oEndDate = myToDate(oEndDate + '' + ' 00:00:00');
		}
		if ((typeof oStartDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oStartDate = myToDate(oStartDate + '' + ' 00:00:00');
		}
		
		switch (sType) {
			case "day":
				var dif = oEndDate - oStartDate;
				dif = (((dif/1000)/60)/60)/24;
				break;
			case "month":
				var test  = eBavelDifDate(oEndDate, oStartDate);
				dif = test[3];
				break;
			case "year":
				dif = (oEndDate - oStartDate);
				//dif = dif / 31557600000;
				//dif = Math.floor(dif);
				dif = oEndDate.getFullYear() - oStartDate.getFullYear();
				break;
		}
		
		if (dif === undefined || dif == "undefined") {
			dif = '';
		}
		if ($.isNumeric(dif)) {
			dif = Math.abs(dif);
		}
		
		return dif;
	} catch(e) {
		return '';
	}
}

//Realiza el incremento / substracción de una cantidad de tipos de componente de una fecha de la fecha especificada
function DateAdd(sType, iInc, oDate) {
	try {
		sType = String(sType).toLowerCase();
		var arrTypes = ["day", "month", "year"];
		if (arrTypes.indexOf(sType) == -1) {
			sType = "Day";
		}
		
		if ((typeof oDate) == "string") {
			//Realiza la conversión a un objeto Date asumiendo que es un string sólo con fecha, si ya viniera con hora, no debería haber problema por concatenar el extra que se ignorará
			oDate = myToDate(oDate + '' + ' 00:00:00');
		}
		
		if (!$.isNumeric(iInc)) {
			iInc = 1;
		}
		iInc = parseInt(iInc);
		
		var blnAdd = true;
		if (iInc < 0) {
			blnAdd = false;
			iInc = Math.abs(iInc);
		}
		
		var objNewDate = oDate;
		switch (sType) {
			case "day":
				if (blnAdd) {
					objNewDate.setDate(objNewDate.getDate() + iInc);
				}
				else {
					objNewDate.setDate(objNewDate.getDate() - iInc);
				}
				break;
			case "month":
				if (blnAdd) {
					objNewDate.setMonth(objNewDate.getMonth() + iInc);
				}
				else { 
					objNewDate.setMonth(objNewDate.getMonth() - iInc);
				}
				break;
			case "year":
				if (blnAdd) {
					objNewDate.setFullYear(objNewDate.getFullYear() + iInc);
				}
				else {
					objNewDate.setFullYear(objNewDate.getFullYear() - iInc); 
				}
				break;
			default:
				break;
		}
		
		return getDate(objNewDate);
	} catch(e) {
		return '';
	}
}
//MAPR 2019-08-19: Función getQuestion para regresar una instancia de una pregunta (#DW8IC2)
function GetQuestion(id){
	if (selSurvey && selSurvey.questions && !isNaN(parseInt(id))){
		return selSurvey.questions[id];
	} else {
		return undefined;
	}
}
//MAPR 2019-08-22: Función getQuestion para regresar una instancia de una pregunta (#DW8IC2)
function GetSection(id) {
	if (selSurvey && selSurvey.sections && !isNaN(parseInt(id))) {
		return selSurvey.sections[id];
	} else {
		return undefined;
	}
}
//MAPR 2019-08-20: Función que llama el webservice y regresa un arreglo
function WebServiceObj(oObject, oSucess, oError, sAction) {
	return objApp.callWebServiceFromObject(oObject, oSucess, oError, sAction);
}
//MAPR 2019-08-20: Función que llama el web service y regresa un objeto
function WebServiceArr(anArray, oSucess, oError, sAction) {
	return objApp.callWebServiceFromArray(anArray, oSucess, oError, sAction);
}
//MAPR 2019-08-20:
function Loader(show, text) {
	if (show){
		mobileLoading(text);
	} else {
		mobileHide();
	}
}

function GetFileNameFromFilePath(sFilePath) {
	if (sFilePath == null) {
		return '';
	}
	
	var arrParts = String(sFilePath).split("/");
	return arrParts[arrParts.length-1];
}

/* Reemplaza el texto indicado tal como la función String.replace pero usando una expresión regular para reemplazar todas las ocurrencias
El parámetro bCase indica si la búsqueda es o no case sensitive
*/
function Replace(sSearch, sReplace, sSource, bCase) {
	var strText = String(sSource);
	var strSearch = String(sSearch);
	var strReplace = String(sReplace)
	
	return strText.replace(new RegExp(RegExp.quote(strSearch), ((bCase)?"g":"gi")), strReplace);
}

function ReplaceScriptTag(sText) {
	try {
		var strRegEx = /<[\s]*script\b[^<]*(?:(?!<\/[\s]*script[\s]*>)<[^<]*)*<\/[\s]*script[\s]*>/gi;
		var strResult = sText.replace(strRegEx, "");
	} catch(e) {
		return sText;
	}
	
	return strResult;
}
//JAPR
//RTORRES
/*
	Funcion para manejar la notifiacaion push de iOS
*/

function onNotificationAPN (event) {
    if (event.alert) {

       alert(event.alert);
	   
    }

    if (event.sound) {
        var snd = new Media(event.sound);
        snd.play();
    }

    if (event.badge) {
        window.plugins.pushNotification.setApplicationIconBadgeNumber(function(){}, function(){}, event.badge);
    }
}
function onNotificationGCM(e) {
    switch(e.event){
        case 'registered':
            if (e.regid.length > 0){
				gApp.gcmregid = e.regid;
				Debugger.message('RegId : ' + gApp.gcmregid);
            }
        break;

        case 'message':
            Debugger.message('Push notification received on Android: ' + JSON.stringify(e));
            //OMMC 2016-09-15: Agregado el registro de notificaciones push
            //OMMC 2016-11-07: Agregado el coldstart para Android
			if (objApp && objApp.managePushNotifications && e) { 
				if (e.coldstart) {
					if (gbColdStartInit !== undefined) {
						gbColdStartInit = true;
					}
				} else {
					if (e.foreground) {
						if (!selSurvey) {
							var strMsg = translate('You have a new message, do you want to view it?');
							navigator.notification.confirm(strMsg, function(answer){
								if (answer == 1) {
									mobileLoading();
									objApp.managePushNotifications('requestMessages', {changePage: true});			
								} else {
									objApp.managePushNotifications('requestMessages', {changePage: false});
								}
							}, translate("Push Notifications"));
						} else {
							objApp.managePushNotifications('requestMessages', {changePage: false});
						}
					} else {
						objApp.managePushNotifications('requestMessages', {changePage: true});
					}
				}
			}
        break;

        case 'error':
            Debugger.message('Error: ' + e.msg);
        break;

        default:
          Debugger.message('An unknown event was received');
          break;
    }
}
/**
 * Id -> Id de question
 * aRecordNumber 
 * Evento de OnChange de time y date 
 */
/* Evento de cambio de valor para el nuevo componente de preguntas tipo fecha
//JAPR 2016-06-02: Corregido un bug de encadenamiento de preguntas fecha, debido a por como funciona la pregunta OCR, este refresh si estaba cambiando el valor de esos tipos
de preguntas, así que indirectamente se le afectó al encadenamiento de defaults ya que cuando hay defaults se invoca a esta función al entrar a cada sección, y como se lanzaba
la actualización de respueta, al ya no considerarse no contestada ya nunca tomaba el default, por lo tanto ahora se agregará el parámetro bForceUpdate para indicar que sólo
en el evento de la pregunta OCR se mande en true lo cual significaría que si deberá actualizar el valor, los demás casos sólo actualizarán el componente visualmente (#B59I3K)
*/
function dateTimeEvent(id, aRecordNumber, bForceUpdate) {
	var objDate = $("#" + selSurvey.questions[id].getFieldName(aRecordNumber))[0];
	var objQuestion = selSurvey.questions[id];
	//JAPR 2016-06-02: Corregido un bug de encadenamiento de preguntas fecha, debido a por como funciona la pregunta OCR, este refresh si estaba cambiando el valor de esos tipos
	//de preguntas, así que indirectamente se le afectó al encadenamiento de defaults ya que cuando hay defaults se invoca a esta función al entrar a cada sección, y como se lanzaba
	//la actualización de respueta, al ya no considerarse no contestada ya nunca tomaba el default, por lo tanto ahora se agregará el parámetro bForceUpdate para indicar que sólo
	//en el evento de la pregunta OCR se mande en true lo cual significaría que si deberá actualizar el valor, los demás casos sólo actualizarán el componente visualmente (#B59I3K)
	if (bForceUpdate) {
		objQuestion.updateValue();
	}
	//JAPR
	/*MAPR 2018-10-19: Ocurría un problema con las preguntas tipo fecha con formato, debido a que al seleccionar un valor 
	este aparecía 2 veces, una encima de la otra, era debido a una transparencia del valor original	#AHBA8Q*/
	objDate.setAttribute("data-date", selSurvey.questions[id].getAnswer(aRecordNumber, true));
	if (!objSettings.webMode) {
		if (selSurvey.questions[id].getAnswer(aRecordNumber, true)) {
			$(objDate).addClass('has-value');
		} else {
			$(objDate).removeClass('has-value');
		}
	}
}

//OMMC 2016-06-14: Agregada función para refrescar valor de un input tipo fecha.
function dateChange(field) {
	var objField = $("#" + field.id);

	if (objField && objField.attr('data-date')) {
		objField.attr('data-date', objField.val());
	}
}

function arrayObjectIndexOf  (myArray, searchTerm) {
    for(var i = 0, len = myArray.length; i < len; i++) {
    	//debugger;
    	/*var normalize = (function() {
		  var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇḉ̈", 
		      to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
		      mapping = {};
		 
		  for(var i = 0, j = from.length; i < j; i++ )
		      mapping[ from.charAt( i ) ] = to.charAt( i );
		 
		  return function( str ) {
		      var ret = [];
		      for( var i = 0, j = str.length; i < j; i++ ) {
		          var c = str.charAt( i );
		          if( mapping.hasOwnProperty( str.charAt( i ) ) )
		              ret.push( mapping[ c ] );
		          else
		              ret.push( c );
		      }      
		      return ret.join( '' );
		  }
		 
		})();*/
    	var one = removeDiacritics(myArray[i]);
    	var two = removeDiacritics(searchTerm);
    	//Debugger.message(two +" COMPARE "+one);

        if (one == two ) return i;
    }

    return -1;
}
var defaultDiacriticsRemovalMap = [
    {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
    {'base':'AA','letters':/[\uA732]/g},
    {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
    {'base':'AO','letters':/[\uA734]/g},
    {'base':'AU','letters':/[\uA736]/g},
    {'base':'AV','letters':/[\uA738\uA73A]/g},
    {'base':'AY','letters':/[\uA73C]/g},
    {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
    {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
    {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
    {'base':'DZ','letters':/[\u01F1\u01C4]/g},
    {'base':'Dz','letters':/[\u01F2\u01C5]/g},
    {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
    {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
    {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
    {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
    {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
    {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
    {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
    {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
    {'base':'LJ','letters':/[\u01C7]/g},
    {'base':'Lj','letters':/[\u01C8]/g},
    {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
    {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
    {'base':'NJ','letters':/[\u01CA]/g},
    {'base':'Nj','letters':/[\u01CB]/g},
    {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
    {'base':'OI','letters':/[\u01A2]/g},
    {'base':'OO','letters':/[\uA74E]/g},
    {'base':'OU','letters':/[\u0222]/g},
    {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
    {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
    {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
    {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
    {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
    {'base':'TZ','letters':/[\uA728]/g},
    {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
    {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
    {'base':'VY','letters':/[\uA760]/g},
    {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
    {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
    {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
    {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
    {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
    {'base':'aa','letters':/[\uA733]/g},
    {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
    {'base':'ao','letters':/[\uA735]/g},
    {'base':'au','letters':/[\uA737]/g},
    {'base':'av','letters':/[\uA739\uA73B]/g},
    {'base':'ay','letters':/[\uA73D]/g},
    {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
    {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
    {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
    {'base':'dz','letters':/[\u01F3\u01C6]/g},
    {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
    {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
    {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
    {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
    {'base':'hv','letters':/[\u0195]/g},
    {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
    {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
    {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
    {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
    {'base':'lj','letters':/[\u01C9]/g},
    {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
    {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
    {'base':'nj','letters':/[\u01CC]/g},
    {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
    {'base':'oi','letters':/[\u01A3]/g},
    {'base':'ou','letters':/[\u0223]/g},
    {'base':'oo','letters':/[\uA74F]/g},
    {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
    {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
    {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
    {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
    {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
    {'base':'tz','letters':/[\uA729]/g},
    {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
    {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
    {'base':'vy','letters':/[\uA761]/g},
    {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
    {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
    {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
    {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}


];
var changes;
function removeDiacritics (str) {
	var find = '́';
	var re = new RegExp(find, 'g');
	str=str.replace(re, '');
	var find = '̈';
	var re = new RegExp(find, 'g');
	str=str.replace(re, '');
	var find = '̂';
	var re = new RegExp(find, 'g');
	str=str.replace(re, '');
	var find = '̀';
	var re = new RegExp(find, 'g');
	str=str.replace(re, '');
	var find = '̃';
	var re = new RegExp(find, 'g');
	str=str.replace(re, '');
    if(!changes) {
        changes = defaultDiacriticsRemovalMap;
    }
    for(var i=0; i<changes.length; i++) {
        str = str.replace(changes[i].letters, changes[i].base);
    }
    return str;
}

function ConvertDateFromString(dateString) {
	var esDateFormats = [ 
		'dddd, D [de] MMMM [de] YYYY h:mm:ss a Z',
		'dddd, D [de] MMMM [de] YYYY h:mm:ss a', 
		'dddd, D [de] MMMM [de] YYYY h:mm a Z', 
		'dddd, D [de] MMMM [de] YYYY h:mm a', 
		'dddd, D [de] MMMM [de] YYYY H:mm:ssZ', 
		'dddd, D [de] MMMM [de] YYYY H:mm:ss', 
		'dddd, D [de] MMMM [de] YYYY H:mmZ', 
		'dddd, D [de] MMMM [de] YYYY H:mm', 
		'dddd, D [de] MMMM [de] YYYY', 
		'D [de] MMMM [de] YYYY h:mm:ss a Z', 
		'D [de] MMMM [de] YYYY h:mm:ss a', 
		'D [de] MMMM [de] YYYY h:mm a Z', 
		'D [de] MMMM [de] YYYY h:mm a', 
		'D [de] MMMM [de] YYYY H:mm:ssZ', 
		'D [de] MMMM [de] YYYY H:mm:ss', 
		'D [de] MMMM [de] YYYY H:mmZ', 
		'D [de] MMMM [de] YYYY H:mm', 
		'D [de] MMMM [de] YYYY', 
		'D [de] MMM [de] YYYY h:mm:ss a Z', 
		'D [de] MMM [de] YYYY h:mm:ss a', 
		'D [de] MMM [de] YYYY h:mm a Z', 
		'D [de] MMM [de] YYYY h:mm a', 
		'D [de] MMM [de] YYYY H:mm:ssZ', 
		'D [de] MMM [de] YYYY H:mm:ss', 
		'D [de] MMM [de] YYYY H:mmZ', 
		'D [de] MMM [de] YYYY H:mm', 
		'D [de] MMM [de] YYYY', 
		'DD/MM/YYYY h:mm:ss a', 
		'DD/MM/YYYY h:mm a', 
		'DD/MM/YYYY H:mm:ss', 
		'DD/MM/YYYY H:mm', 
		'DD/MM/YYYY', 
		'DD/MM/YY h:mm:ss a',
		'DD/MM/YY h:mm a',
		'DD/MM/YY H:mm:ss',
		'DD/MM/YY H:mm',
		'DD/MM/YY',
		'YYYY-MM-DDTHH:mm:ssZ',
		'YYYY-MM-DDTHH:mmZ',
		'YYYY-MM-DD HH:mm:ssZ',
		'YYYY-MM-DD HH:mm:ss',
		'YYYY-MM-DD HH:mmZ',
		'YYYY-MM-DD HH:mm',
		'YYYY-MM-DD',
		'YYYY/MM/DD HH:mm:ssZ',
		'YYYY/MM/DD HH:mm:ss',
		'YYYY/MM/DD HH:mmZ',
		'YYYY/MM/DD HH:mm',
		'YYYY/MM/DD',
		'YYYYMMDDHHmmss',
		'YYYYMMDDHHmm',
		'YYYYMMDD'
	];
	var aMoment = moment(dateString, esDateFormats, 'es');
	if (!aMoment.isValid()) {
		var enDateFormats = [ 
			'dddd, MMMM D, YYYY h:mm:ss a Z',
			'dddd, MMMM D, YYYY h:mm:ss a',
			'dddd, MMMM D, YYYY h:mm a Z',
			'dddd, MMMM D, YYYY h:mm a',
			'dddd, MMMM D, YYYY H:mm:ssZ',
			'dddd, MMMM D, YYYY H:mm:ss',
			'dddd, MMMM D, YYYY H:mmZ',
			'dddd, MMMM D, YYYY H:mm',
			'dddd, MMMM D, YYYY',
			'MMMM D[,] YYYY h:mm:ss a Z',
			'MMMM D[,] YYYY h:mm:ss a',
			'MMMM D[,] YYYY h:mm a Z',
			'MMMM D[,] YYYY h:mm a',
			'MMMM D[,] YYYY H:mm:ssZ',
			'MMMM D[,] YYYY H:mm:ss',
			'MMMM D[,] YYYY H:mmZ',
			'MMMM D[,] YYYY H:mm',
			'MMMM D[,] YYYY',
			'MMM D[,] YYYY h:mm:ss a Z',
			'MMM D[,] YYYY h:mm:ss a',
			'MMM D[,] YYYY h:mm a Z',
			'MMM D[,] YYYY h:mm a',
			'MMM D[,] YYYY H:mm:ssZ',
			'MMM D[,] YYYY H:mm:ss',
			'MMM D[,] YYYY H:mmZ',
			'MMM D[,] YYYY H:mm',
			'MMM D[,] YYYY',
			'MM D YYYY h:mm:ss a Z',
			'MM D YYYY h:mm:ss a',
			'MM D YYYY h:mm a Z',
			'MM D YYYY h:mm a',
			'MM D YYYY H:mm:ssZ',
			'MM D YYYY H:mm:ss',
			'MM D YYYY H:mmZ',
			'MM D YYYY H:mm',
			'MM D YYYY',
			'MM/DD/YYYY h:mm:ss a Z',
			'MM/DD/YYYY h:mm:ss a',
			'MM/DD/YYYY h:mm a Z',
			'MM/DD/YYYY h:mm a',
			'MM/DD/YYYY H:mm:ssZ',
			'MM/DD/YYYY H:mm:ss',
			'MM/DD/YYYY H:mmZ',
			'MM/DD/YYYY H:mm',
			'MM/DD/YYYY',
			'MM-DD-YYYY h:mm:ss a Z',
			'MM-DD-YYYY h:mm:ss a',
			'MM-DD-YYYY h:mm a Z',
			'MM-DD-YYYY h:mm a',
			'MM-DD-YYYY H:mm:ssZ',
			'MM-DD-YYYY H:mm:ss',
			'MM-DD-YYYY H:mmZ',
			'MM-DD-YYYY H:mm',
			'MM-DD-YYYY',
			'MM D YY h:mm:ss a Z',
			'MM D YY h:mm:ss a',
			'MM D YY h:mm a Z',
			'MM D YY h:mm a',
			'MM D YY H:mm:ssZ',
			'MM D YY H:mm:ss',
			'MM D YY H:mmZ',
			'MM D YY H:mm',
			'MM D YY',
			'MM/DD/YY h:mm:ss a Z',
			'MM/DD/YY h:mm:ss a',
			'MM/DD/YY h:mm a Z',
			'MM/DD/YY h:mm a',
			'MM/DD/YY H:mm:ssZ',
			'MM/DD/YY H:mm:ss',
			'MM/DD/YY H:mmZ',
			'MM/DD/YY H:mm',
			'MM/DD/YY',
			'MM-DD-YY h:mm:ss a Z',
			'MM-DD-YY h:mm:ss a',
			'MM-DD-YY h:mm a Z',
			'MM-DD-YY h:mm a',
			'MM-DD-YY H:mm:ssZ',
			'MM-DD-YY H:mm:ss',
			'MM-DD-YY H:mmZ',
			'MM-DD-YY H:mm',
			'MM-DD-YY'
		];
		aMoment = moment(dateString, enDateFormats, 'en');
	}
	return aMoment.isValid() ? aMoment.format("YYYY-MM-DD") : '';
}

//OMMC 2015-12-02: Agregada para retornar un nuevo elemento de DHTMLX
/* Devuelve un objeto con la siguiente estructura:
	{type:'iType', name:'iName', value:'iValue', label:'iLabel'}

	OMMC 2015-12-02: Originalmente creada para crear input=radio de manera dinámica con los atributos de varias preguntas, para generar el listado de opciones de una pregunta OCR.
*/
function DHTMLXInput(iType, iName, iValue, iLabel){
	if(iType === "undefined"){
		iType = "input";
	}
	if(iName === "undefined"){
		iName = "";
	}
	if(iValue === "undefined"){
		iValue = "";
	}
	if(iLabel === "undefined"){
		iLabel = "";
	}

	switch(iType){
		case "input":
			this.type = iType;
			this.name = iName;
			this.value = iValue;
			this.label = iLabel;
		break;
		case "option":
			this.value = iValue;
			this.label = iLabel;
		break;
	}
	
}

//OMMC 2015-12-02: Parte de la implementación de la pregunta OCR, crea el canvas de acuerdo a las dimensiones de la imagen pintada en el div
function GetDataURL(img) {
	var canvas = document.createElement("canvas");
	canvas.width = img.naturalWidth;
	canvas.height = img.naturalHeight;

	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0);

	var dataURL = canvas.toDataURL("image/jpeg");

	return dataURL;
}

//OMMC 2015-12-02: Parte de la implementación de la pregunta OCR, crea el blob de la imagen.
function DataURLToBlob(dataURL) {
	var BASE64_MARKER = ';base64,';
	if (dataURL.indexOf(BASE64_MARKER) == -1) {
		var parts = dataURL.split(',');
		var contentType = parts[0].split(':')[1];
		var raw = parts[1];
		
		return new Blob([raw], {type: contentType});
	} else {
		var parts = dataURL.split(BASE64_MARKER);
		var contentType = parts[0].split(':')[1];
		var raw = window.atob(parts[1]);
		var rawLength = raw.length;
		
		var uInt8Array = new Uint8Array(rawLength);
		
		for (var i = 0; i < rawLength; ++i) {
			uInt8Array[i] = raw.charCodeAt(i);
		}
		
		return new Blob([uInt8Array], {type: contentType});
	}
}

//OMMC 2015-12-10: Agregado para formatear el tiempo a formato universal quitando segundos
function ConvertTimeFromString (timeString){
	var timeFormats = [ 
		'h:mm:ss a',
		'h:mm:ss A',
		'h:mm a',
		'h:mm A',
		'hh:mm:ss a',
		'hh:mm:ss A',
		'hh:mm a',
		'hh:mm A',
		'H:mm:ss',
		'H:mm',
	];
	aMoment = moment(timeString, timeFormats);
	return aMoment.isValid() ? aMoment.format("HH:mm") : '';
}

//OMMC 2016-06-04: Agregada función para retornar uniqueIDs
/*
	strPrefix (string) Prefijo del uniqueID
	nKeys		(int) Número de llaves a generar (Default = 1)
	nRadix		(int [2-36]) 2 - Binario, 8 - Octal, 16 - Hexadecimal, etc. (Default = 14)
				Puede que debido al radix se obtengan keys menores a 13 caracteres.
	keyLen		(int) Longitud de elementos del key adicionales al prefix. (Default = 13)
*/
function GenerateKeys(strPrefix, nKeys, nRadix, keyLen){
	//Validado para admitir prefijos vacíos
	if (!strPrefix && strPrefix !== '') {
		strPrefix = "RND";
	}

	//Validado para pedir números negativos o 0
	if (!nKeys || nKeys <= 1) {
		nKeys = 1;
	}
	//Validados sólo números válidos para nRadix
	if (!nRadix || nRadix < 2 || nRadix > 36) {
		nRadix = 14;
	}
	//Validada la longitud de la cadena
	if (!keyLen || keyLen <= 2) {
		keyLen = 13;
	}
	
	//Para probar un nRadix
	if (strPrefix === 'DUMMY') {
		return Math.random().toString(nRadix);
	} else {
		//Para generar keys
		var i, key, generated, tmpString;

		//Validación de longitud para evitar problemas al generar las llaves
		//Genera una key muestra para ver si cumple los requisitos de longitud
		tmpString = Math.random().toString(nRadix);
		tmpString = tmpString.substr(2, tmpString.length);
		if(tmpString.length < keyLen){
			keyLen = tmpString.length - 1;
			if(keyLen <= 0){
				return false;
			}
		}

		//Regresa una key
		if (nKeys == 1) {
			generated = Math.random().toString(nRadix).substr(2, keyLen);
			key = strPrefix + generated.toUpperCase();
			return key;
		} else {
			//Regresa un array de keys
			var arrayKeys = [];
			for (i=0; i<nKeys; i++) {
				generated = Math.random().toString(nRadix).substr(2, keyLen);
				key = strPrefix + generated.toUpperCase();
				arrayKeys.push(key);
			}
			return arrayKeys;
		}
	}	
}

//OMMC 2016-04-08: Agregada la función para sobreescribir el comportamiento de los hipervínculos en el app.
/*
	hArray 			(array) Array de tags <a> que se van a mostrar en pantalla
	oOptions		(object) Objeto con opciones adicionales para la función
*/
function ManageHyperlinks(hArray, oOptions){
	if (!hArray || hArray.length <= 0) {
		return;
	}

	if (oOptions === undefined) {
		oOptions = {};
	} else {
		if (typeof oOptions !== 'object') {
			return;
		}
	}

	var browserOptions = 'location=yes';
	if (oOptions && oOptions.bOptions) {
		browserOptions = oOptions.bOptions;
	}

	$.each (hArray, function(index, value) {
		if (!$(value).hasClass("customLink")) {
			$(value).bind("click", function() {
				event.preventDefault();
				//Si hay un link válido
				if ($(value).attr("href") && ($(value).attr("href") != '' || $(value).attr("href") != '#')) {
					//Si tiene protocolo HTTP o HTTPS
					if ($(value).attr("href").indexOf("http://") > -1 || $(value).attr("href").indexOf("https://") > -1) {
						if (objSettings.webMode) {
							window.top.open($(value).attr("href"));	
						} else {
							window.open($(value).attr("href"), '_blank', browserOptions);	
						}
					//Si no tiene el protocolo
					} else {
						if (objSettings.webMode) {
							window.top.open("http://" + $(value).attr("href"));	
						} else {
							window.open("http://" + $(value).attr("href"), '_blank', browserOptions);	
						}
					}
				//De lo contrario enviamos a la página de BITAM
				} else {
					if (objSettings.webMode) {
						window.top.open("http://www.bitam.com");	
					} else {
						window.open("http://www.bitam.com", '_blank', browserOptions);		
					}
					
				}
			});
			//Añade la clase customLink para saber que el evento ya está adjunto a ese elemento
			$(value).addClass("customLink");
		}
	});
}

//OMMC 2016-11-09: Agregada la capacidad de reordenar objectos o arrays por medio de una propiedad, ya sea de manera ascendente o descendente. Regresa un array ordenado
/*
	aElement 	(Object || Array) 	Elemento que va a ordenar
	aProperty 	(String) 			Propiedad por la cual va a ordenar
	aOrder 		(String) 			Forma en la que va ordenar (asc || desc) default: asc
*/
function elementSorter(aElement, aProperty, aOrder) {
	Debugger.register('Sorting object by ' + aProperty + 'in ' + aOrder + ' order');

	if (aElement === undefined) { return; }
	if (aProperty === undefined) { aProperty = 'key'; }
	if (aOrder === undefined) { aOrder = 'asc'; }

	var aArray = [];
	if (typeof aElement === 'object') {
		aArray = $.map(aElement, function(value, index) {
		    return [value];
		});
	} else if (Array.isArray(aElement)) {
		aArray = aElement;
	}

	if (aArray.length > 1) {
		aArray.sort(function(a, b) {
			if (a.hasOwnProperty(aProperty) && b.hasOwnProperty(aProperty)) {
				if (aOrder == 'asc') {
					if (a[aProperty] < b[aProperty]) {
						return -1;
					} else if (a[aProperty] > b[aProperty]) {
						return 1;
					} else {
						return 0;				
					}
				} else if (aOrder == 'desc') {
					if (a[aProperty] > b[aProperty]) {
						return -1;
					} else if (a[aProperty] < b[aProperty]) {
						return 1;
					} else {
						return 0;				
					}
				}
			}
		});
	}
	return aArray;
}

//OMMC 2016-11-24: Agregado para ver si un elemento (selector) de jQuery existe ya que se tenía que preguntar por su length
jQuery.fn.ifExists = function() {
	return this.length > 0;
}

//JAPR 2018-08-29: Optimizado el uso del servicio de GoogleMaps Geocoder para solo invocarlo si realmente hay en la forma variables de GPS que lo requieran (#9BTTAA)
/* Verifica si el texto proporcionado contiene algún tipo de variable que requiera Geocoding, en cuyo caso se deberá habilitar el uso de Geocoding de Google Maps
para su posterior traducción a dirección */
function checkGeocoderGPSVars(sText) {
	var blnHasGPSVars = false;
	
	/*Evaluación mejorada para determinar el uso de variables de GPS que requieren Geocoding*/
	var strGPSVarsRegEx = /{\$gps\.(?:country|state|city|zipcode|address)}/i;
	if ( strGPSVarsRegEx.test(sText) ) {
		blnHasGPSVars = true;
	}
	
	return blnHasGPSVars;
}
//JAPR

//OMMC 2018-09-06: Agregado el objeto para almacenar módulos de scripts (#64ISFM)
function getModuleFiles() {
	//Carga los módules de mapas de openLayers
	var openLayers = [
		{alias: "openLayers_CSS", type: "CSS", localFile: "css/ol.min.css", externalFile: "https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.2.0/css/ol.css", loaded: false},
		{alias: "geocoder_CSS", type: "CSS", localFile: "css/ol-geocoder.min.css", externalFile: "https://unpkg.com/ol-geocoder@3.2.0/dist/ol-geocoder.min.css", loaded: false},
		{alias: "openLayers_JS", type: "JS", localFile: "js/ol.min.js", externalFile: "https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.2.0/build/ol.js", loaded: false},
		{alias: "openLayers_Legacy", type: "JS", localFile: "js/polyfill.min.js", externalFile: "https://cdn.polyfill.io/v2/polyfill.min.js", loaded: false},
		{alias: "geocoder", type: "JS", localFile: "js/ol-geocoder.min.js", externalFile: "https://unpkg.com/ol-geocoder@3.2.0/dist/ol-geocoder.js", loaded: false},
	];
	var mapKit = [
		{alias: "mapKit", type: "JS", localFile: "js/mapkit.min.js", externalFile: "https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js", loaded: false}
	];

	return {mapKit: mapKit};
}

//MAPR 2019-03-28: Agregado el nuevo tipo de pregunta AR (#4HNJD9)
function doGetRackPictureAR(id) {
	var objQuestion = selSurvey.questions[id],
		objPhotoOptions = {};

	selSurvey.changeQuestion(objQuestion.number); /*Invocar a la cámara con el formato JPG que es el soportado por eForms por default (sólo si se requiere hacer upload de la foto como parte de la captura,		en caso contrario se puede asignar la opción objPhotoOptions.encodingType = Camera.EncodingType.PNG; para que se pueda usar la foto directamente*/
	/* No hay soporte de options por ahora, este código será ignorado */
	if (typeof Camera != "undefined") {
		objPhotoOptions.sourceType = Camera.PictureSourceType.CAMERA; /*objPhotoOptions.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;*/
		objPhotoOptions.destinationType = Camera.DestinationType.DATA_URL; /*Configuraciones default para tomar la foto*/
		objPhotoOptions.targetWidth = 1000;
		objPhotoOptions.targetHeight = 1000;
		objPhotoOptions.quality = 50; /*Permite variar el tamaño de las fotos en base a las configuraciones de eForms*/
		if (objSettings) {
			if (objSettings.photoWidth && parseInt(objSettings.photoWidth) > 0) {
				$.extend(objPhotoOptions, {
					targetWidth: parseInt(objSettings.photoWidth)
				});
			}
			if (objSettings.photoHeight && parseInt(objSettings.photoHeight) > 0) {
				$.extend(objPhotoOptions, {
					targetHeight: parseInt(objSettings.photoHeight)
				});
			}
			if (objSettings.photoQuality) {
				var intCamQuality = parseInt(objSettings.photoQuality);
				if (intCamQuality >= 0 && intCamQuality <= 100) {
					$.extend(objPhotoOptions, {
						quality: intCamQuality
					});
				}
			}
		}
		$.extend(objPhotoOptions, { correctOrientation: true });
	}
	/* No hay soporte de options por ahora, este código será ignorado */

	var objARPlugIn = undefined;
	if (typeof BarcelGSEARPlugin == "undefined") {
		if (typeof ARPlugin == "undefined") {
			alert("AR Plugin not detected");
			return;
		}
		objARPlugIn = ARPlugin;
	}
	else {
		objARPlugIn = BarcelGSEARPlugin;
	}
	//MAPR 2019-04-25: Se modifica el ultimo parametro en el plugin para que reciba la calidad de imagen que desea.
	var intCamQuality = 50;
	if (objSettings.photoQuality) {
		var intCamQuality = parseInt(objSettings.photoQuality);
		if (intCamQuality >= 0 && intCamQuality <= 100) {
			var intCamQuality = 50;
		}
	}
	/*MAPR armado de la ruta de los modelos*/
	var nameModel = objQuestion.getDefaultValue();
	var objModelData = _.findWhere(objeModels, { name: nameModel });
	var jsonString = '';
	if (objModelData !== undefined) {
		//JAPR 2015-08-05: Estandarizadas las variables independientemente del operativo utilizado
		var sProjectFolder = ESFile.getRootFilesPath(((objSettings.project) ? objSettings.project + "/" : ''));
		var modelFilepath = objSettings.prefixTrans + sProjectFolder + "models/catalog_" + objQuestion.dataSourceID + "/" + objModelData.name + '_' + objModelData.version + "/" + objModelData.name + (objSettings.isAndroid() ? gsARextAndroid : gsARextiOS);
		var title = objModelData.name;
		jsonString = '[{"modelFilepath":"' + modelFilepath + '", "title": "' + title + '"}]';
	} else {
		if (objeModels && objeModels.length && objeModels.length() > 0 ) {
			var jsonString = '[';
			var sProjectFolder = ESFile.getRootFilesPath(((objSettings.project) ? objSettings.project + "/" : ''));
			for (var objModel in objeModels) {
				var objModelData = objeModels[objModel];
				if (objModelData.name == "") {
					break;
				}
				var modelFilepath = objSettings.prefixTrans + sProjectFolder + "models/catalog_" + objQuestion.dataSourceID + "/" + objModelData.name + '_' + objModelData.version + "/" + objModelData.name + (objSettings.isAndroid() ? gsARextAndroid : gsARextiOS);
				if (objSettings.isiOS()) {
					modelFilepath = modelFilepath.replace('file:///', '');
				}
				var title = objModelData.name;
				if (jsonString !== '[') {
	                jsonString += ","
	            }
				jsonString += '{"modelFilepath":"' + modelFilepath + '", "title": "' + title + '"}';
			}
			jsonString += ']';
		}
	}
	//MAPR 2019-04-25: Se modifica el ultimo parametro en el plugin para que reciba la calidad de imagen que desea.
	objARPlugIn.getPicture(function (imageData) {
		var objQuestion = selSurvey.currentCapture.question;
		objApp.photoTook(imageData, undefined, objQuestion.id, selSurvey.currentCapture.section.recordNum); /*Carga la imagen en formato JPG a un elemento IMG para convertirla a PNG*/
	}, function (message) { /*En caso de que se cancele la toma de la foto o bien ocurra algún tipo de error al tomarla, entrará a este código*/
		alert("No se tomó la foto: " + message);
	}, jsonString, intCamQuality);
}

//JAPR 2018-12-07: Optimizada la carga de múltiples secciones tabla en la misma forma (#EZ06TY)
/* Verifica si el texto proporcionado contiene o no alguna referencia a variables de algún tipo que sean válidas en eForms */
function checkHasVariables(sText) {
	var strExpression = String(sText);
	var intStart = strExpression.indexOf('{');
	var blnHasVars = true;
	//Si no hay variables, simplemente regresa el texto recibido
	if (intStart == -1) {
		blnHasVars = false;
	}
	if ( blnHasVars ) {
		//Desactivará la bandera y empezará a revisar con las variables mas comunes para que en caso de encontrar alguna ya no siga buscando
		blnHasVars = false;
		//Verifica si realmente existen variables de eForms en este valor default
		//Todas estas expresiones regulares se encuentran también en eSurveyServiceModFns.js, si se necesita modificar alguna, se debe realizar en ambos lados (crea una constante)
		var arrVars = strExpression.match(/[\{]{1}[a-zA-Z0-9]+([\.]{1}(\[)'{1}[a-zA-Z0-9]+[\()]*(']){1})+[}]{1}/g);
		blnHasVars = (arrVars === null)?0:arrVars.length;
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/[\{]{1}[@]{1}[Q|q]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/g);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/[\{]{1}[@]{1}[S|s]{1}[0-9]+(\([0-9|a-z|A-Z]*\))?[}]{1}/g);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/[\{]{1}[$]{1}[Q|q|S|s]{1}[0-9]+(\.(([0-9|a-z|A-Z]+){1})|(\.([0-9|a-z|A-Z]+)(\([^();]*\)))|){1}[}]{1}/g);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/[\{]{1}[@]{1}[gps]+(\([0-9|a-z|A-Z]*\))?[}]{1}/gi);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/\{\$gps(\.([0-9|a-z|A-Z]+){1})?}/gi);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/{@IMGPATH}|{@IMGFULLPATH}/gi);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/{@score}/gi);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
		
		if ( !blnHasVars ) {
			var arrVars = strExpression.match(/{\$score}/gi);
			blnHasVars = (arrVars === null)?0:arrVars.length;
		}
	}
	
	return blnHasVars;
}
//JAPR
