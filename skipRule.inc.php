<?php
require_once("repository.inc.php");
require_once("initialize.php");
//EXT
require_once("object.trait.php");

class BITAMSkipRule extends BITAMObject
{
	//EXT
	use BITAMObjectExt;
	
	public $SkipRuleID;
	public $SurveyID;
	public $QuestionID;
	public $RuleName;
	public $SkipCondition;
	public $NextSectionID;
	//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
	
  	function __construct($aRepository, $aQuestionID)
	{
		BITAMObject::__construct($aRepository);
		
		$this->QuestionID = $aQuestionID;
		$this->SurveyID = -1;
		$this->SkipRuleID = -1;
		$this->RuleName = '';
		$this->SkipCondition = '';
		$this->NextSectionID = 0;
		
		//EXT
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
	}
	
	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	public static function getMainQuery() {
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$strAdditionalFields = '';
		if (getMDVersion() >= esvPartialIDsReplaceFix) {
			$strAdditionalFields .= ', A.CreationUserID, A.CreationDateID, A.CreationVersion, A.LastUserID, A.LastDateID, A.LastVersion ';
		}
		//@JAPR
		$sql = "SELECT A.SkipRuleID, A.QuestionID, A.RuleName, A.SkipCondition, A.NextSectionID, B.SurveyID $strAdditionalFields 
			FROM SI_SV_SkipRules A 
				INNER JOIN SI_SV_Question B ON (A.QuestionID = B.QuestionID) ";
		return $sql;
	}
	//@JAPR
	
	static function NewInstance($aRepository, $aQuestionID)
	{
		$strCalledClass = static::class;
		return new $strCalledClass($aRepository, $aQuestionID);
	}
	
	static function NewInstanceWithID($aRepository, $aSkipRuleID)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (((int)  $aSkipRuleID) < 0)
		{
			return $anInstance;
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMSkipRule::getMainQuery().
			" WHERE A.SkipRuleID = ".$aSkipRuleID;
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}

	static function NewInstanceWithName($aRepository, $aRuleName, $aQuestionID)
	{
		$strCalledClass = static::class;
		$anInstance = null;
		
		if (trim($aRuleName) == '') {
			return $anInstance;
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMSkipRule::getMainQuery().
			" WHERE A.QuestionID = {$aQuestionID} AND A.RuleName = ".$aRepository->DataADOConnection->Quote($aRuleName);
		//@JAPR
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die( translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		if (!$aRS->EOF)
		{
			$anInstance = $strCalledClass::NewInstanceFromRS($aRepository, $aRS);
		}
		return $anInstance;
	}
	
	static function NewInstanceFromRS($aRepository, $aRS)
	{
		$aQuestionID = (int)$aRS->fields["questionid"];
		
		$strCalledClass = static::class;
		$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
		$anInstance->SurveyID = (int) @$aRS->fields["surveyid"];
		$anInstance->SkipRuleID = (int) @$aRS->fields["skipruleid"];
		$anInstance->QuestionID = (int) @$aRS->fields["questionid"];
		$anInstance->RuleName = (string) trim(@$aRS->fields["rulename"]);
		$anInstance->SkipCondition = (string) trim(@$aRS->fields["skipcondition"]);
		$anInstance->NextSectionID = (int) @$aRS->fields["nextsectionid"];
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$strCalledClass = static::class;
		
		$aQuestionID = getParamValue('QuestionID', 'both', '(int)');
		if (array_key_exists("SkipRuleID", $aHTTPRequest->POST)) {
			$intSkipRuleID = $aHTTPRequest->POST["SkipRuleID"];
			
			if (is_array($intSkipRuleID)) {
				$aCollection = BITAMSkipRuleCollection::NewInstance($aRepository, $aQuestionID, $intSkipRuleID);
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else {
				$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intSkipRuleID);
				if (is_null($anInstance)) {
					$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
				}
				//@JAPR 2017-02-21: Corregido un bug, al crear nuevas condiciones de salto, como no estaba asignando este valor no realizaba el reemplazo de variables
				$anInstance->SurveyID = getParamValue('SurveyID', 'both', '(int)');
				//@JAPR
				$anInstance->updateFromArray($aHTTPRequest->POST);
				
				$aResult = $anInstance->save();
				
				if (array_key_exists("go", $_POST)) {
					$go = $_POST["go"];
				}
				else {
					$go = "self";
				}
				
				if ($go == "parent") {
					$anInstance = $anInstance->get_Parent();
				}
				else {
					if ($go == "new") {
						$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
					}
				}
				
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		
		$anInstance = null;
		if (array_key_exists("SkipRuleID", $aHTTPRequest->GET)) {
			$intSkipRuleID = $aHTTPRequest->GET["SkipRuleID"];
			$anInstance = $strCalledClass::NewInstanceWithID($aRepository, (int)$intSkipRuleID);

			if (is_null($anInstance))
			{
				$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
			}
		}
		else {
			$anInstance = $strCalledClass::NewInstance($aRepository, $aQuestionID);
		}
		
		return $anInstance;
	}
	
	function updateFromArray($anArray)
	{
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		global $garrUpdatedPropsWithVars;
		if (!isset($garrUpdatedPropsWithVars)) {
			$garrUpdatedPropsWithVars = array();
		}
		if (!isset($garrUpdatedPropsWithVars["SkipRules"])) {
			$garrUpdatedPropsWithVars["SkipRules"] = array();
		}
		//@JAPR
		
		if (array_key_exists("SkipRuleID", $anArray)) {
			$this->SkipRuleID = (int) $anArray["SkipRuleID"];
		}
		
		if (array_key_exists("RuleName", $anArray)) {
			$this->RuleName = trim($anArray["RuleName"]);
		}
		
		if (array_key_exists("SkipCondition", $anArray)) {
			$this->SkipCondition = trim($anArray["SkipCondition"]);
			//Cuando se sobreescribe la propiedad en este método, quiere decir que se está grabando, así que traducimos las variables a IDs
			$this->SkipCondition = $this->TranslateVariableQuestionNumbersByIDs($this->SkipCondition, optyFormula);
			//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
			$garrUpdatedPropsWithVars["SkipRules"]["SkipCondition"] = 1;
			//@JAPR
		}
		
		if (array_key_exists("NextSectionID", $anArray))
		{
			$this->NextSectionID = (int) $anArray["NextSectionID"];
		}
		
		return $this;
	}
	
	/* Función equivalente a ReplaceVariableQuestionNumbersByIDs, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionNumbersByIDs($sText, $iPropertyID = 0) {
		$blnServerVariables = false;
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		return (string) @BITAMSurvey::ReplaceVariableQuestionNumbersByIDs($this->Repository, $this->SurveyID, $sText, (($this->LastModAdminVersion > 0)?$this->LastModAdminVersion:esvCopyForms), otySkipRule, $this->QuestionID, $iPropertyID, $blnServerVariables);
	}
	
	/* Función equivalente a ReplaceVariableQuestionIDsByNumbers, sólo que se invoca a la instancia en lugar de a la clase, con esto se delega la
	asignación específica de los datos del objeto a la instancia misma y se puede crear la función polimorfica
	*/
	function TranslateVariableQuestionIDsByNumbers($sText, $bServerVariables = false) {
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		return (string) @BITAMSurvey::ReplaceVariableQuestionIDsByNumbers($this->Repository, $this->SurveyID, $sText, (($this->LastModAdminVersion > 0)?$this->LastModAdminVersion:esvCopyForms), $bServerVariables);
	}
	
	//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
	/* Esta función actualizará las propiedades que pudieran tener variables en su interior y que no fueron asignadas por la función UpdateFromArray, forzando a traducirlas primero 
	a números de pregunta para que en caso de que el objeto no hubiera sido grabado aún con la versión que ya corregía los IDs se puedan recuperar correctamente, posteriormente las
	volverá a traducir a IDs pero ahora aplicando ya la corrección para no realizar reemplazos parciales. Esta función debe utilizarse siempre antesde un Save del objeto para que 
	durante el primer grabado sea cual fuera la razón para hacerlo, se puedan traducir correctamente las propiedades al momento de asignar el numero de versión de este servicio que ya
	asumirá que graba correctamente este tipo de propiededes, por tanto su uso es:
	1- Esperar a un UpdateFromArray (opcional, si no se invocó, simplemente sobreescribirá todas las propiedades, como por ejemplo en el proceso de copiado)
	2- Ejecutar esta función (si hubo un UpdateFromArray, algunas propiedades podrían ya estar asignadas en el array de reemplazos, así que esas se ignoran)
	3- Ejecutar al Save (esto sobreescribirá al objeto y sus propiedades que utilizan variables
	*/
	function fixAllPropertiesWithVarsNotSet() {
		global $garrUpdatedPropsWithVars;
		
		if (!isset($garrUpdatedPropsWithVars) || !is_array($garrUpdatedPropsWithVars) || !isset($garrUpdatedPropsWithVars["SkipRules"])) {
			$garrUpdatedPropsWithVars = array();
			$garrUpdatedPropsWithVars["SkipRules"] = array();
		}
		
		//Si ya se había aplicado un grabado a este objeto por lo menos con la versión de este fix, entonces ya no tiene sentido continuar con el proceso pues no habra diferencia
		if ($this->LastModAdminVersion >= esvPartialIDsReplaceFix) {
			return;
		}
		
		//Actualiza las propiedades no asignadas en el array de propiedades modificadas
		//Para que el proceso funcione correctamente, es indispensable que se engañe a la instancia sobreescribiendo temporalmente LastModAdminVersion a la versión actual pero sólo
		//cuando ya se va a reemplazar por IDs, cuando se va a reemplazar por números si puede dejar la versión actual para que auto-corrija el error con el que había grabado usando IDs
		//previamente
		$dblLastModAdminVersion = $this->LastModAdminVersion;
		$blnUpdatedProperties = false;
		//Primero traducirá de IDs a números todas las propiedades no grabadas manualmente para corregir los IDs mal grabados
		if (!isset($garrUpdatedPropsWithVars["SkipRules"]["SkipCondition"])) {
			$this->SkipCondition = $this->TranslateVariableQuestionIDsByNumbers($this->SkipCondition);
			$blnUpdatedProperties = true;
		}
		
		if (!$blnUpdatedProperties) {
			return;
		}
		
		//Finalmente actualizará las propiedades no grabadas manualmente a IDs ya aplicando la corrección sobreescribiendo la versión del objeto
		//Como esta función se supone que se usará justo antes de un save, es correcto dejar la propiedad LastModAdminVersion actualizada, ya que de todas formas se modificaría en el save
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		if (!isset($garrUpdatedPropsWithVars["SkipRules"]["SkipCondition"])) {
			$this->SkipCondition = $this->TranslateVariableQuestionNumbersByIDs($this->SkipCondition, optyFormula);
		}
	}
	//@JAPR
	
	function save()
	{
		global $gblShowErrorSurvey;
		
		//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
		$currentDate = date("Y-m-d H:i:s");
		//@JAPR 2017-02-21: Corregido un bug, se estaban haciendo reemplazos parciales cuando no se usaban server variables ya que no buscaba hasta el "." (#7IQZ0Z)
		//Al realizar el grabado del objeto (pero no copiado) se deben actualizar a la par todas las configuraciones que pudieran tener variables para arreglar los IDs mal grabados
		//Se debe realizar antes de la asignación de LastModAdminVersion o de lo contrario ya no tendría efecto pues ya consideraría que está grabado con la versión corregida
		//if (!$bCopy) {
			$this->fixAllPropertiesWithVarsNotSet();
		//}
		
		$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		$intCreationUserID = $_SESSION["PABITAM_UserID"];
		$intLastUserID = $_SESSION["PABITAM_UserID"];
		$strCreationDate = $currentDate;
		$strLastDate = $currentDate;
		$dblCreationVersion = $this->LastModAdminVersion;
		$dblLastVersion = $this->LastModAdminVersion;
		
		if ($this->isNewObject()) {
			$this->CreationAdminVersion = ESURVEY_SERVICE_VERSION;
			
			$sql =  "SELECT ".$this->Repository->DataADOConnection->IfNull("MAX(SkipRuleID)", "0")." + 1 AS SkipRuleID ".
				"FROM SI_SV_SkipRules";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
			
			$this->SkipRuleID = (int) $aRS->fields["skipruleid"];
			//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
			$strAdditionalFields = '';
			$strAdditionalValues = '';
			if (getMDVersion() >= esvPartialIDsReplaceFix) {
				$strAdditionalFields .= ', CreationUserID, LastUserID, CreationDateID, LastDateID, CreationVersion, LastVersion';
				$strAdditionalValues .= ', '.$intCreationUserID.', '.$intLastUserID.
					', '.$this->Repository->DataADOConnection->DBTimeStamp($strCreationDate).
					', '.$this->Repository->DataADOConnection->DBTimeStamp($strLastDate).
					', '.$dblCreationVersion.', '.$dblLastVersion;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_SkipRules (".
						" SkipRuleID".
						", QuestionID".
			            ", RuleName".
						", SkipCondition".
						", NextSectionID".
						$strAdditionalFields.
			            ") VALUES (".
			            $this->SkipRuleID.
						",".$this->QuestionID.
						",".$this->Repository->DataADOConnection->Quote($this->RuleName).
						",".$this->Repository->DataADOConnection->Quote($this->SkipCondition).
						",".$this->NextSectionID.
						$strAdditionalValues.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
	 	else {
			//@JAPR 2017-02-21: Agregados los datos extendidos de modificación (#7IQZ0Z)
			$strAdditionalValues = '';
			if (getMDVersion() >= esvPartialIDsReplaceFix) {
				$strAdditionalValues .= ', LastUserID = '.$_SESSION["PABITAM_UserID"].
					', LastDateID = '.$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR
			$sql = "UPDATE SI_SV_SkipRules SET ".
					"RuleName = ".$this->Repository->DataADOConnection->Quote($this->RuleName).
					", SkipCondition = ".$this->Repository->DataADOConnection->Quote($this->SkipCondition).
					", NextSectionID = ".$this->NextSectionID.
					$strAdditionalValues.
				" WHERE SkipRuleID = ".$this->SkipRuleID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				if($gblShowErrorSurvey) {
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else {
					return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
			}
	 	}
		
		if ($this->SurveyID <= 0 && $this->QuestionID > 0) {
			//Carga la instancia de la pregunta para obtener el ID de la forma
			$mainQuestion = BITAMQuestion::NewInstanceWithID($this->Repository, $this->QuestionID);
			if (!is_null($mainQuestion)) {
				$aSurveyInstance = BITAMSurvey::NewInstanceWithID($this->Repository, $mainQuestion->SurveyID);
				$this->SurveyID = $aSurveyInstance->SurveyID;
			}
		}
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
	}
	
	function remove($bJustMobileTables = false)
	{
		$sql = "DELETE FROM SI_SV_SkipRules WHERE SkipRuleID = ".$this->SkipRuleID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			//die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		BITAMSurvey::updateLastUserAndDateInSurvey($this->Repository, $this->SurveyID);
		return $this;
	}
	
	function isNewObject()
	{
		return ($this->SkipRuleID < 0);
	}
	
	function get_Title()
	{
		return translate("SkipCondition");
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SkipRule";
		}
		else
		{
			return "BITAM_PAGE=SkipRule&SkipRuleID=".$this->SkipRuleID;
		}
	}
	
	function get_Parent()
	{
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($this->Repository);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'SkipRuleID';
	}
	
	function get_FormFieldName()
	{
		return 'RuleName';
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	 
	function canEdit($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		return false;
	}
	
	//EXT
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function getJSonDefinition() {
		global $gbDesignMode;
		
		$arrDef = array();
		$arrDef['id'] = $this->SkipRuleID;
		$arrDef['name'] = $this->RuleName;
		$strData = $this->TranslateVariableQuestionIDsByNumbers($this->SkipCondition);
		$arrDef['skipCondition'] = $strData;
		$arrDef['nextSection'] = $this->NextSectionID;
		if ($gbDesignMode) {
			$arrDef['objectType'] = otySkipRule;
		}
		
		return $arrDef;
	}
}

class BITAMSkipRuleCollection extends BITAMCollection
{	//EXT
	use BITAMCollectionExt;
	public $ObjectType;
	public $QuestionID;				//ID de la pregunta donde existen grabados los datos de esta colección
	
	function __construct($aRepository, $aQuestionID)
	{
		BITAMCollection::__construct($aRepository);
		//EXT
		$this->QuestionID = $aQuestionID;
		$this->ObjectType = otySkipRule;
		$this->ContainerID = "SkipRules";
	}
	
	static function NewInstance($aRepository, $aQuestionID, $anArrayOfSkipRuleIDs = null)
	{
		//@JAPR 2018-10-26: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G1NQEG)
		if( is_null($anArrayOfSkipRuleIDs) || count($anArrayOfSkipRuleIDs)<=0 )
		{
			$anInstance =& BITAMGlobalFormsInstance::GetSkipRulesCollectionByQuestionWithID($aQuestionID);
			if(!is_null($anInstance))
			{
				return $anInstance;
			}
		}
		//@JAPR
		
		$strCalledClass = static::class;

		global $gblShowErrorSurvey;
		
		$anInstance = new $strCalledClass($aRepository, $aQuestionID);
		
		$where = "";
		if (!is_null($anArrayOfSkipRuleIDs))
		{
			switch (count($anArrayOfSkipRuleIDs))
			{
				case 0:
					break;
				case 1:
					$where = "WHERE A.SkipRuleID = ".((int) $anArrayOfSkipRuleIDs[0]);
					break;
				default:
					foreach ($anArrayOfSkipRuleIDs as $intSkipRuleID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$intSkipRuleID; 
					}
					if ($where != "")
					{
						$where = "WHERE A.SkipRuleID IN (".$where.")";
					}
					break;
			}
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMSkipRule::getMainQuery().
			" WHERE A.QuestionID = {$aQuestionID} ".$where." 
			ORDER BY A.RuleName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		while (!$aRS->EOF)
		{
			$anInstance->Collection[] = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$aRS->MoveNext();
		}
		
		return $anInstance;
	}
	
	//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
	/* Carga la colección de reglas de salto filtradas por le ID de la pregunta a la que pertenecen */
	static function NewInstanceByQuestion($aRepository, $aQuestionID)
	{
		global $gblShowErrorSurvey;
		
		$anInstance =& BITAMGlobalFormsInstance::GetSkipRulesCollectionByQuestionWithID($aQuestionID);
		if(!is_null($anInstance))
		{
			return $anInstance;
		}
		
		$strCalledClass = static::class;
		$anInstance = new $strCalledClass($aRepository, $aQuestionID);
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMSkipRule::getMainQuery().
			" WHERE A.QuestionID = {$aQuestionID} 
			ORDER BY A.RuleName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		while (!$aRS->EOF)
		{
			$objSkipRule = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$anInstance->Collection[] = $objSkipRule;
			$aRS->MoveNext();
		}
		
		BITAMGlobalFormsInstance::AddSkipRulesCollectionByQuestionWithID($aQuestionID, $anInstance);
		
		return $anInstance;
	}
	
	/* Obtiene la colección de reglas de salto pertenecientes a las formas indicadas en el parámetro, grabando en el caché de objetos indexado como colección 
	para cada forma y pregunta además de directamente cada objeto para optimizar su carga posterior (esta función se asume que es equivalente a invocar a NewInstance 
	pero con todos los parámetros en null excepto el SurveyID correspondiente) */
	static function NewInstanceFromDBFull($aRepository, $anArrayOfSurveyIDs = null) {
		global $gblShowErrorSurvey;
		
		//Crea la instancia a partir de la clase que realmente ejecuta el método
		$strCalledClass = static::class;
		$strCalledClassColl = $strCalledClass;
		$anInstance = new $strCalledClass($aRepository, -1);
		//Para crear las instancias individuales, tiene que quitar el sufijo Collection a la variable con el nombre de clase
		$strCalledClass = str_ireplace('Collection', '', $strCalledClass);
		
		$arrSkipRulesBySurveyID = array();
		$arrSkipRulesByQuestionID = array();
		
		$filter = "";
		if (!is_null($anArrayOfSurveyIDs))
		{
			switch (count($anArrayOfSurveyIDs))
			{
				case 0:
					break;
				case 1:
					$filter = " AND B.SurveyID = ".((int) $anArrayOfSurveyIDs[0])." ";
					break;
				default:
					foreach ($anArrayOfSurveyIDs as $aSurveyID)
					{
						if ($filter != "")
						{
							$filter .= ", ";
						}
						$filter .= (int)$aSurveyID; 
					}
					if ($filter != "")
					{
						$filter = " AND B.SurveyID IN (".$filter.") ";
					}
					break;
			}
		}
		
		//@JAPR 2018-06-25: Optimizada la carga de formas desde el Admin o URLs externas para remover procesos innecesarios (#G87C4W)
		$sql = BITAMSkipRule::getMainQuery().
			$filter.
			" ORDER BY B.QuestionID, A.RuleName";
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			if($gblShowErrorSurvey) {
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			else {
				return("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SkipRules ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
		}
		while (!$aRS->EOF)
		{
			$objSkipRule = $strCalledClass::NewInstanceFromRS($anInstance->Repository, $aRS);
			$anInstance->Collection[] = $objSkipRule;
			
			//Agrega el objeto a la colección indexada por los padres
			$intSurveyID = $objSkipRule->SurveyID;
			$intQuestionID = $objSkipRule->QuestionID;
			if ( !isset($arrSkipRulesBySurveyID[$intSurveyID]) ) {
				$arrSkipRulesBySurveyID[$intSurveyID] = array();
			}
			$arrSkipRulesBySurveyID[$intSurveyID][] = $objSkipRule;
			if ( !isset($arrSkipRulesByQuestionID[$intQuestionID]) ) {
				$arrSkipRulesByQuestionID[$intQuestionID] = array();
			}
			$arrSkipRulesByQuestionID[$intQuestionID][] = $objSkipRule;
			
			$aRS->MoveNext();
		}
		
		//Finalmente procesa el array de formas y preguntas para indexar por todos los padres incluyendo aquellos que no tenían elementos y agregarlos al caché 
		//(en este punto se asume que este método se ejecutó posterior a la carga masiva de todos los padres, así que se usarán las colecciones que ya deberían 
		//estar cachadas)
		if (isset(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey)) {
			$arrAllSkipRulesCollection = array();
			foreach(BITAMGlobalFormsInstance::$arrAllQuestionCollectionsBySurvey as $objQuestionCollection) {
				foreach ($objQuestionCollection->Collection as $objQuestion) {
					$intQuestionID = $objQuestion->QuestionID;
					//Se debe usar directamente el elemento desde el array en lugar de una variable local, porque BITAMGlobalFormsInstance lo recibe por referencia
					$arrAllSkipRulesCollection[$intQuestionID] = new $strCalledClassColl($aRepository, $intQuestionID);
					//$objSkipRulesCollection = new $strCalledClassColl($aRepository, $intQuestionID);
					if ( isset($arrSkipRulesByQuestionID[$intQuestionID]) ) {
						$arrAllSkipRulesCollection[$intQuestionID]->Collection = $arrSkipRulesByQuestionID[$intQuestionID];
					}
					BITAMGlobalFormsInstance::AddSkipRulesCollectionByQuestionWithID($intQuestionID, $arrAllSkipRulesCollection[$intQuestionID]);
				}
			}
		}
		
		return $anInstance;
	}
	//@JAPR
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		if (array_key_exists("QuestionID", $aHTTPRequest->GET))
		{
			$aQuestionID = (int) $aHTTPRequest->GET["QuestionID"];
		}
		else
		{
			$aQuestionID = 0;
		}
		
		$strCalledClass = static::class;
		return $strCalledClass::NewInstance($aRepository, $aQuestionID);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Skip rules");
	}

	function get_QueryString()
	{
		return "BITAM_PAGE=SkipRule";
	}
	
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=SkipRule";
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'SkipRuleID';
	}

	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function get_FormFieldName()
	{
		return 'RuleName';
	}
}
?>