<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");

$showAllFrames = true;

if(isset($_SESSION["PAdisplayAllFrames"]))
{
	if($_SESSION["PAdisplayAllFrames"]==0)
	{
		$showAllFrames = false;
	}
}

if($showAllFrames)
{
	$strMainFrameset='4%,96%';
}
else 
{
	$strMainFrameset='0%,100%';
}

$strSaveFrameSize = '100%,*';

$_SESSION["PABITAM_UserID"];
require_once('appuser.inc.php');
require_once('survey.inc.php');

$AppUser = BITAMAppUser::NewInstanceWithClaUsuario($theRepository, $theUser->UserID);
//$strUserPassword = BITAMAppUser::getUserPassword($theRepository, $AppUser->CLA_USUARIO);
//@JAPR 2016-06-30: Corregido un bug, el password estaba visible desencriptado (#VRNM0H)
$strUserPassword = BITAMEncode($AppUser->Password);
//@JAPR
$UserMail=$AppUser->Email;

if (array_key_exists("menuType", $_GET))
{
	$menuType = (int) $_GET["menuType"];
}
else 
{
	$menuType = 0;
}
$path_parts = pathinfo($_SERVER["SCRIPT_NAME"]);
$strScriptPath = $path_parts["dirname"];
require_once("eFormsAgentConfig.inc.php");
global $blnIsAgentEnabled;
//21Ene2013: Validar que si no existe la tabla SI_SV_SurveyTasks no se muestre el menú de Tasks
$displayTasks = true;
$sqlTasks = "SELECT SurveyTaskID FROM SI_SV_SurveyTasks";
$aRSTasks = $theRepository->DataADOConnection->Execute($sqlTasks);
if ($aRSTasks === false)
{
	//La tabla no existe, por lo tanto no se despliega la opción Tasks
	$displayTasks = false;
}
$displayTasks = true;
//@JAPR 2014-04-01: Agregada la ventana para permitir enviar mensajes vía Push
$displayPush = false;
/*2015-06-10@JRPP arreglo de encuestas mas usadas*/
$arrSurveryNames = array();
/*2015-06-10@JRPP arreglo de encuestas mas usadas*/
//@JAPR 2015-11-26: Se solicitó por petición de cliente que no se mostraran las formas filtradas por las mas capturadas, sino que se mostrara la lista completa, así que se modifica
//la manera de obtener la lista de formas para que sólo aplique el criterio de Schedulers en caso de invitados, y la lista completa en caso de Administradores
//$arrSurveryIds = array();
/*2015-06-10@JRPP arreglo de encuestas mas usadas*/
$arrCatalogNames = array();
/*2015-06-10@JRPP arreglo de Catalogos mas usadas*/
$arrCatalogIds = array();
/*2015-06-10@JRPP consulto las Catalogos mas usadas*/
//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
$strWhere = '';
//@JAPR 2015-11-26: Se solicitó por petición de cliente que no se mostraran las formas filtradas por las mas capturadas, sino que se mostrara la lista completa, así que se modifica
//la manera de obtener la lista de formas para que sólo aplique el criterio de Schedulers en caso de invitados, y la lista completa en caso de Administradores
$arrSurveryNames = BITAMSurvey::GetSurveyList($theRepository, $theUser->UserID, (int) @$_SESSION["PABITAM_UserRole"]!=1);
/*
if (getMDVersion() >= esvDeleteReports) {
	$strWhere = "AND A.Deleted = 0 ";
}
//@JAPR
//GCRUZ 2015-10-08. Validar que si NO es usuario Admin sólo pueda ver sus formas
if ((int) @$_SESSION["PABITAM_UserRole"]!=1)
{
	$strWhere .= " AND A.UserID = ".$theRepository->DataADOConnection->Quote($UserMail)." ";
}
$sqlTasks = "SELECT A.SurveyID, B.SurveyName, COUNT(A.Repository) 
				FROM SI_SV_SurveyLog A 
					INNER JOIN SI_SV_Survey B ON B.SurveyID = A.SurveyID 
				WHERE B.CreationVersion >= ".esveFormsv6." {$strWhere} 
				GROUP BY A.SurveyID, B.SurveyName 
				ORDER BY 2 DESC LIMIT 5";
$rs = $theRepository->DataADOConnection->Execute($sqlTasks);
while ($rs && !$rs->EOF){
	$arrSurveryNames[]=$rs->fields['surveyname'];
	$arrSurveryIds[]=$rs->fields['surveyid'];
	$rs->MoveNext();
}
*/
/*si son menos de 5 tengo que ir a consultar a la tabla "si_sv_survey" para poder mostrar los 5 registros*/
//GCRUZ 2015-10-08. Validar que si NO es usuario Admin sólo pueda ver sus formas
$strWhereSurveyIDs = '';
if ((int) @$_SESSION["PABITAM_UserRole"]!=1)
{
	$schedulerIDs = array();
	$aSQL = "SELECT SchedulerID FROM si_sv_surveyscheduleruser WHERE UserID = ".$AppUser->CLA_USUARIO;
	$aRS = $AppUser->Repository->DataADOConnection->Execute($aSQL);
	if ($aRS && $aRS->_numOfRows != 0)
	{
		while (!$aRS->EOF)
		{
			$schedulerIDs[] = $aRS->fields['SchedulerID'];
			$aRS->MoveNext();
		}
	}
	else
		$schedulerIDs[] = 0;
	require_once('surveyScheduler.inc.php');
	$objSurveySchedulerCol = BITAMSurveySchedulerCollection::NewInstance($AppUser->Repository, $schedulerIDs);
	$arrSchedulerSurveyIDs = array();
	foreach ($objSurveySchedulerCol as $objSurveyScheduler)
	{
		$arrSchedulerSruveyIDs[] = $objSurveyScheduler->SurveyID;
	}
	if (count($arrSchedulerSurveyIDs) == 0)
		$strWhereSurveyIDs = ' AND 1=0 ';
	else
		$strWhereSurveyIDs = ' AND SurveyID IN ('.implode(',', $arrSchedulerSruveyIDs).') ';
}

//@JAPR 2015-11-26: Se solicitó por petición de cliente que no se mostraran las formas filtradas por las mas capturadas, sino que se mostrara la lista completa, así que se modifica
//la manera de obtener la lista de formas para que sólo aplique el criterio de Schedulers en caso de invitados, y la lista completa en caso de Administradores
/*
if (count($arrSurveryNames) < 5) {
	$intSurveysCount = count($arrSurveryNames);
	//Si no hay suficientes formas con capturas, complemente con formas que no tengan capturas hasta llegar al máximo a mostrar
	$sqlTasks = "SELECT SurveyName, SurveyID 
		FROM SI_SV_Survey 
		WHERE CreationVersion >= ".esveFormsv6.$strWhereSurveyIDs." 
		ORDER BY SurveyName LIMIT 5";
	$rs = $theRepository->DataADOConnection->Execute($sqlTasks);
	while ($rs && !$rs->EOF){
		$intSurveyID = (int) @$rs->fields['surveyid'];
		if ($intSurveyID && !in_array($intSurveyID, $arrSurveryIds)) {
			$arrSurveryNames[] = $rs->fields['surveyname'];
			$arrSurveryIds[] = $intSurveyID;
			$intSurveysCount++;
			
			//Se sale cuando llega al máximo de rows a mostrar
			if ($intSurveysCount >= 5) {
				break;
			}
		}
		$rs->MoveNext();
	}
}
*/
//@JAPR

/*2015-06-10@JRPP consulto los catalogos */
$sqlTasks = "SELECT * FROM SI_SV_Catalog ORDER BY catalogname LIMIT 5";
$rs = $theRepository->DataADOConnection->Execute($sqlTasks);
while ($rs && !$rs->EOF){
	$arrCatalogNames[]=$rs->fields['catalogname'];
	$arrCatalogIds[]=$rs->fields['catalogid'];
	$rs->MoveNext();
}
/*obtengo el total de usuarios*/
$sqlTasks = "SELECT COUNT(*) AS NumUsers FROM SI_USUARIO WHERE CLA_USUARIO > 0";
$rs = $theRepository->ADOConnection->Execute($sqlTasks);
$numUser = 0;
if ($rs && !$rs->EOF){
	$numUser = $rs->fields['numusers']; 
}
/*obtengo el total de grupos de usuario*/
$sqlTasks = "SELECT COUNT(*) AS NumRol FROM SI_ROL WHERE CLA_ROL>=0";
$rs = $theRepository->ADOConnection->Execute($sqlTasks);
$numRol = 0;
if ($rs && !$rs->EOF){
	$numRol = $rs->fields['numrol']; 
}
if (getMDVersion() >= esvPasswordLangAndRedesign) {
	$sql = "SELECT COUNT(*) AS Regs FROM SI_SV_REGID";
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	if ($aRS && !$aRS->EOF) {
		$displayPush = ((int) @$aRS->fields["regs"]) > 0;
	}
}
$numSyncs = 0;

//@JAPR 2016-12-06: Modificado el armado del query para hacer un subquery en lugar de un IN a partir de un String gigante con IDs de escenarios (#OB6JNF)
//$strWhereFilter = '';
//GCRUZ 2015-10-08. Validar que si NO es usuario Admin sólo pueda ver sus escenarios publicados
//@JAPR 2016-08-16: Corregido un bug, no se recuerda por qué razón se decidió con el cambio de GCRUZ sólo validar a los usuarios no Administradores, pero por un comentario de JLC
//y por consistencia con Artus, quien no mostraría los escenarios no publicados ni siquiera a la cuenta Maestra, mucho menos a simples Administradores de eForms, se comentó con LRoux
//y se acordó quitar esta condición para que los escenarios sean filtrados por los publicados de cada usuario logeado sin importar si es o no Administrador de eForms (#OB6JNF)
/*if ((int) @$_SESSION["PABITAM_UserRole"]!=1)
{*/
//@JAPR 2016-12-06: Modificado el armado del query para remover el primer SELECT a SI_ESCENARIO pues es irrelevante si sólo se quiere la CLA_ESCENARIO que se puede obtener directa
//desde SI_ESC_GPO, lo mismo para el caso de SI_ESC_PUB
$strWhereFilter = " AND CLA_ESCENARIO IN (";
$strWhereFilter .= "SELECT t1.CLA_ESCENARIO 
	FROM SI_ESC_GPO t1 
		INNER JOIN SI_ROL_USUARIO t2 ON (t1.CLA_GPO = t2.CLA_ROL) 
	WHERE t2.CLA_USUARIO = {$AppUser->CLA_USUARIO}";
$strWhereFilter .= " UNION DISTINCT ";
$strWhereFilter .= "SELECT CLA_ESCENARIO 
	FROM SI_ESC_PUB 
	WHERE CLA_SUSCRIPTOR = {$AppUser->CLA_USUARIO}";
//@JAPR 2016-09-13: Corregido un bug, no se estaba permitiendo mostrar los escenarios creados por los propios usuarios (#OB6JNF)
$strWhereFilter .= " UNION DISTINCT ";
$strWhereFilter .= "SELECT CLA_ESCENARIO 
	FROM SI_ESCENARIO 
	WHERE CLA_USUARIO = ".$AppUser->CLA_USUARIO;
$strWhereFilter .= ")";
//@JAPR 2016-12-06: Modificado el armado del query para hacer un subquery en lugar de un IN a partir de un String gigante con IDs de escenarios (#OB6JNF)
/*$rsAppUserDash =  $theRepository->ADOConnection->Execute($aSQL);
$arrAppUserDash = array();
if ($rsAppUserDash && $rsAppUserDash->_numOfRows != 0)
{
	while (!$rsAppUserDash->EOF)
	{
		$arrAppUserDash[] = $rsAppUserDash->fields['CLA_ESCENARIO'];
		$rsAppUserDash->MoveNext();
	}
	$strWhereFilter = " AND CLA_ESCENARIO IN (".implode(',', $arrAppUserDash).") ";
}*/
//}

//@JAPR 2015-10-19: Reintegrada la validación de escenarios a partir de SI_CONFIGURA, ya que el cambio previo de Goyo la había removido
//@JAPR 2015-10-06: Agregada la validación de escenarios visibles a partir del Dummy insertado y configurado en SI_CONFIGURA
//@JAPR 2015-10-08: Corregido un bug, había un espacio en el nombre del campo al accesarlo en fields, así que no podía leer el valor de la configuración
$intFirstUserDashboardID = 0;
$sql = "SELECT REF_CONFIGURA 
    FROM SI_CONFIGURA 
    WHERE CLA_CONFIGURA = 703";
$aRS = $theRepository->ADOConnection->Execute($sql);
if ($aRS && !$aRS->EOF) {
    $intFirstUserDashboardID = (int) @$aRS->fields["ref_configura"];
    if ($intFirstUserDashboardID <= 0) {
        $intFirstUserDashboardID = 0;
	}
}

$arrDashboards = array();
//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
if ($AppUser->ArtusWebUserLicense > 0) {
	//@JAPR 2015-08-11: Modificado el orden de los escenarios
	//@JAPR 2015-10-19: Reintegrada la validación de escenarios a partir de SI_CONFIGURA, ya que el cambio previo de Goyo la había removido
	//@JAPR 2016-12-06: Modificado el armado del query para hacer un subquery en lugar de un IN a partir de un String gigante con IDs de escenarios (#OB6JNF)
	$sql = "SELECT CLA_ESCENARIO, NOM_ESCENARIO 
		FROM SI_ESCENARIO 
		WHERE CLA_ESCENARIO > {$intFirstUserDashboardID} AND CLA_USUARIO >= 0 AND (CLA_ROL >= 0 OR CLA_ROL IS NULL) AND CLA_GPO_ESC > 0 ".$strWhereFilter."
		ORDER BY NOM_ESCENARIO";
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS) {
		while (!$aRS->EOF) {
			$intDashboardID = (int) @$aRS->fields["cla_escenario"];
			$strDashboardName = (string) @$aRS->fields["nom_escenario"];
			if ($intDashboardID > 0) {
				$arrDashboards[$intDashboardID] = $strDashboardName;
				if (count($arrDashboards) >= 8) {
					break;
				}
			}
			$aRS->MoveNext();
		}
	}
}
//@JAPR

/*Query de la gráfica de capturas*/
$totalCap=0;
//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
$strWhere = '';
if (getMDVersion() >= esvDeleteReports) {
	$strWhere = "AND A.Deleted = 0 ";
}
//GCRUZ 2015-10-08. Validar que si NO es usuario Admin sólo pueda ver sus capturas
if ((int) @$_SESSION["PABITAM_UserRole"]!=1)
{
	$strWhere .= " AND A.UserID = ".$theRepository->DataADOConnection->Quote($UserMail)." ";
}
//@JAPR
//GCRUZ 2015-08-28. Agregado SessionID al WHERE para ignorar capturas no completadas correctamente
$sql = "SELECT COUNT(*) AS CountCap
	FROM SI_SV_SurveyLog A, SI_SV_Survey B
	WHERE A.SurveyID=B.SurveyID AND DATE_FORMAT(A.SurveyDate,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') AND A.STATUS = 1 AND SessionID IS NOT NULL AND SessionID <> '' {$strWhere}";
$aRS = $theRepository->DataADOConnection->Execute($sql);
if ($aRS && !$aRS->EOF)
{
	$totalCap=(int)$aRS->fields["countcap"];
}

$countCap = array();
//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
//GCRUZ 2015-08-28. Agregado SessionID al WHERE para ignorar capturas no completadas correctamente
$sql = "SELECT B.SurveyName, COUNT(*) AS CountCap
	FROM SI_SV_SurveyLog A, SI_SV_Survey B
	WHERE A.SurveyID=B.SurveyID AND DATE_FORMAT(A.SurveyDate,'%Y-%m-%d')=DATE_FORMAT(NOW(),'%Y-%m-%d') AND A.STATUS = 1 AND SessionID IS NOT NULL AND SessionID <> '' {$strWhere} 
	GROUP BY A.SurveyID
	ORDER BY CountCap DESC
	LIMIT 5";
/*Query temporal para q se vean datos en la gráfica*/
/*
$sql = "SELECT B.SurveyName, COUNT(*) AS CountCap
	FROM SI_SV_SurveyLog A, SI_SV_Survey B
	WHERE A.SurveyID=B.SurveyID AND DATE_FORMAT(A.SurveyDate,'%Y-%m-%d')='2015-07-14' AND A.STATUS = 1
	GROUP BY A.SurveyID
	ORDER BY CountCap DESC
	LIMIT 5";
*/	
	$aRS = $theRepository->DataADOConnection->Execute($sql);
	$maxLim=5;
	if ($aRS && !$aRS->EOF)
	{
		while(!$aRS->EOF)
		{
			$countAux=(int)$aRS->fields["countcap"];
			if($countAux>$maxLim)
			{
				$maxLim=$countAux;
			}
			$countCap[$aRS->fields["surveyname"]]=$countAux;
			$aRS->MoveNext();
		}
	}
	require_once('eSurveyServiceMod.inc.php');
	$aBITAMConnection = connectToKPIRepository();
	//var_dump($aBITAMConnection);
		

	/*Query de la gráfica de sincronizaciones*/	

	$arraySync=array();
	/*
	$sqlSync="SELECT A.SURVEYID, DATE_FORMAT(A.SurveyDate,'%Y-%m-%d') AS SurveyDate, A.UserID
		FROM SI_SV_SurveyTasksLog  A
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($_SESSION["PABITAM_RepositoryName"]).
		" AND DATE_FORMAT(A.SurveyDate,'%Y-%m-%d') BETWEEN ADDDATE(DATE_FORMAT(NOW(),'%Y-%m-%d'),-6) AND DATE_FORMAT(NOW(),'%Y-%m-%d')".
		" GROUP BY A.SurveyID, DATE_FORMAT(A.SurveyDate,'%Y-%m-%d'), A.UserID
		ORDER BY A.SurveyDate";
		$aRSSinc = $aBITAMConnection->ADOConnection->Execute($sqlSync);
		if($aRSSinc===false)
		{
			die( translate("Error accessing")." SI_SV_SurveyTasksLog  A ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSync);
		}
		if ($aRSSinc && !$aRSSinc->EOF)
		{
			while(!$aRSSinc->EOF)
			{
				$svDate=$aRSSinc->fields["surveydate"];
				if(!isset($arraySync[$svDate]))
				{
					$arraySync[$svDate]=0;
				}
				$arraySync[$svDate]++;
				$aRSSinc->MoveNext();
			}
		}
		//var_dump($arraySync);
		//die();
	*/

		//GCRUZ 2015-08-05. Obtener todas las SurveyID existentes
		$aSQL = "SELECT SurveyID,surveyname FROM SI_SV_Survey";
		$allSurveyIDs = array();
		$allSurveyNames=array();
		$aRS = $theRepository->DataADOConnection->Execute($aSQL);
		if ($aRS && $aRS->_numOfRows != 0)
		{
			while (!$aRS->EOF)
			{
				$allSurveyIDs[] = $aRS->fields['SurveyID'];
				//EVEGA 13/08/2015 Todos los names de los surveys para el reporte de sincronizacion
				$allSurveyNames[$aRS->fields['SurveyID']]=$aRS->fields['surveyname'];
				$aRS->MoveNext();
			}
		}
		//@JAPR 2015-08-05: Validado para que no genere un error en caso de no existir alguna tabla (generalmente por ser una BD nueva)
		//Se removió el die para que continue sin formas
		$allSurveyIDs = implode(',', $allSurveyIDs);

	//GCRUZ 2015-10-08. Validar que si NO es usuario Admin sólo pueda ver sus sincronizaciones
	$strWhereUserID = '';
	if ((int) @$_SESSION["PABITAM_UserRole"]!=1)
	{
		$strWhereUserID = ' AND A.UserID = '.$aBITAMConnection->ADOConnection->Quote($UserMail) . ' ';
	}
	$sqlSync="SELECT A.UserID, A.SessionID, DATE_FORMAT(A.CreationDateID,'%Y-%m-%d') AS SurveyDate
			FROM SI_SV_SurveyTasksLog A 
			WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote($_SESSION["PABITAM_RepositoryName"]).$strWhereUserID.
            " AND DATE_FORMAT(A.CreationDateID,'%Y-%m-%d') BETWEEN ADDDATE(DATE_FORMAT(NOW(),'%Y-%m-%d'),-6) AND DATE_FORMAT(NOW(),'%Y-%m-%d') 
			AND SessionID IS NOT NULL AND SessionID <> '' ".(($allSurveyIDs != '')?" AND A.SurveyID IN ({$allSurveyIDs}) ":'')."
			GROUP BY A.UserID, A.SessionID, DATE_FORMAT(A.CreationDateID,'%Y-%m-%d')
			ORDER BY 3 ASC";
		
		$aRSSinc = $aBITAMConnection->ADOConnection->Execute($sqlSync);
		if($aRSSinc===false)
		{
			die( translate("Error accessing")." SI_SV_SurveyTasksLog  A ".translate("table").": ".$aBITAMConnection->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlSync);
		}
		$arrayUserSession=array();
		if ($aRSSinc && !$aRSSinc->EOF)
		{
			while(!$aRSSinc->EOF)
			{
				$svDate=$aRSSinc->fields["surveydate"];
				$userIDSyn=trim(strtolower($aRSSinc->fields["userid"]));
				$sessionIDSync=$aRSSinc->fields["sessionid"];
				if(!isset($arrayUserSession[$userIDSyn.$sessionIDSync]))
				{
					if(!isset($arraySync[$svDate]))
					{
						$arraySync[$svDate]=0;
					}
					$arraySync[$svDate]++;
					$arrayUserSession[$userIDSyn.$sessionIDSync]="";
				}
				$aRSSinc->MoveNext();
			}
		}
	
		if (isset($arraySync[date('Y-m-d')]))
			$numSyncs = $arraySync[date('Y-m-d')];
	
//GCRUZ 2015-09-23 Obtener datos de artus del repositorio
$aSQL = "SELECT KPIServer, ArtusPath FROM saas_databases WHERE repository = ".$aBITAMConnection->ADOConnection->Quote($_SESSION["PABITAM_RepositoryName"]);
$rsArtusPath = $aBITAMConnection->ADOConnection->Execute($aSQL);
$strArtusPath = 'https://';
if ($rsArtusPath && $rsArtusPath->_numOfRows != 0)
{
	$strArtusPath .= $rsArtusPath->fields['KPIServer'].'/'.$rsArtusPath->fields['ArtusPath'].'/';
}

//@JAPR 2015-09-03: Validado que sólo los administradores puedan acceder a ciertas opciones de eForms
//@JAPR 2016-08-23: Corregido el registro de seguridad por usuario, no estaba considerando al repositorio así que mismos usuarios de diferentes BDs habrían tenido conflicto (#KY40NF)
$arrSecurity = (array) @$_SESSION["PABITAM_Security"][$_SESSION["PABITAM_RepositoryName"]][$_SESSION["PABITAM_UserID"]];
//@JAPR
?>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="IE=10" />
<title>KPIOnline Forms -  <?= $UserMail ?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href='favicon.ico' rel='shortcut icon' type='image/x-icon'/>
<link href='favicon.ico' rel='icon' type='image/x-icon'/>
<link rel="stylesheet" type="text/css" href="js/codebase/dhtmlx.css"/>
<link href="css/font/roboto/robotofont.css" rel="stylesheet" type="text/css">
<!--<script src="js/codebase/dhtmlx.js"></script>-->
<script src="js/codebase/dhtmlxFmtd.js"></script>
<script type="text/javascript" src="js/json2.min.js"></script>
<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
<link rel="stylesheet" type="text/css" href="css/customDHTMLX.css"/>
<script language="JavaScript" type="text/javascript" src="js/utils.js"></script>		
<style>
/* it's important to set width/height to 100% for full-screen init */
html, body {
	width: 100%;
	height: 100%;
	margin: 0px;
	overflow: hidden;
}
.dhx_dataview {
	overflow-y:auto !important;
}
.dhxdataview_placeholder{
	  height: 100% !important;
}
table.hdr td {
	cursor:pointer !important;
}
div.gridbox_dhx_terrace.gridbox table.hdr td div.hdrcell {
	  text-transform: none !important;
}
</style>
<script language="JavaScript">
	var objFormsLayoutPortal;
	var objStatsLayOut;
	var objSecurityLayOut;
	var objFormsGrid;
	var objDataSrcsGrid;
	var objFomsDataView
	var objDailyEntriesDataView;
	var objWindows;
	var objDialog;
	var objUsersDataView;
	var objGroupsDataView;
	var objSettingsDataView;
	var intDialogWidth = 380;
	var intDialogHeight = 350;
	var reqAjax = 1;
	var menuType = <?=$menuType?>;
	var UserRole = <?=(int) @$_SESSION["PABITAM_UserRole"]?>;
	var eFormsMDVersion = <?=getMDVersion()?>;
	var esvSurveyMenu = <?=esvSurveyMenu?>;
	var esvAdminWYSIWYG = <?=esvAdminWYSIWYG?>;
	var esvAgendaScheduler = <?=esvAgendaScheduler?>;
	var esveFormsAgent = <?=esveFormsAgent?>;
	var blnIsAgentEnabled = <?=(int) ($blnIsAgentEnabled && $displayTasks)?>;
	var gbIsGeoControl = <?=(int) $gbIsGeoControl?>;
	var displayPush = <?=(int) $displayPush?>;
	//Main Layout
	var dhxSecurityArea = "a";
	var dhxFormsList = "b";
	var dhxDataSrcsList = "c";
	var dhxStatsArea = "d";
	//Security stats layout
	var dhxUsers = "a";
	var dhxGroups = "b";
	var dhxSettings = "c";
	//Stats layout
	var dhxLogChart = "a";
	var dhxLogMap = "b";
	var t = -1;
	dhtmlxEvent(window, 'resize', function(event){
		if (objFormsLayoutPortal) {
			objFormsLayoutPortal.setSizes();
		}
		clearTimeout(t);
	    t = setTimeout(doneResizing, 50);
	});
	function doneResizing(){
		var inHeight = (window.innerHeight||document.body.clientHeight);
		var inWidth = (window.innerWidth||document.body.clientWidth) - 50;
		/*manera de redimencionar los tamaños de ancho*/
		if (objFormsLayoutPortal) {
			objFormsLayoutPortal.cells(dhxFormsList).setWidth(inWidth/2);
			objFormsLayoutPortal.cells(dhxDataSrcsList).setWidth(inWidth/2);
		}
		if (objSecurityLayOut) {
			objSecurityLayOut.cells(dhxUsers).setWidth(inWidth/3);
			objSecurityLayOut.cells(dhxGroups).setWidth(inWidth/3);
			objSecurityLayOut.cells(dhxSettings).setWidth(inWidth/3);
		}
		if (objStatsLayOut) {
			objStatsLayOut.cells(dhxLogChart).setWidth(inWidth/2);
			objStatsLayOut.cells(dhxLogMap).setWidth(inWidth/2);
		}
		/*manera de redimencionar los tamaños de alto*/
		var intSecurityHeight = 118;
		var intListsHeight = 275;
		if (objFormsLayoutPortal) {
			objFormsLayoutPortal.cells(dhxSecurityArea).setHeight(intSecurityHeight/*inHeight/3*/);
			objFormsLayoutPortal.cells(dhxFormsList).setHeight(intListsHeight/*(inHeight-intSecurityHeight)/3*/);
			objFormsLayoutPortal.cells(dhxDataSrcsList).setHeight(intListsHeight/*(inHeight-intSecurityHeight)/3*/);
			objFormsLayoutPortal.cells(dhxStatsArea).setHeight((inHeight-intSecurityHeight-intListsHeight));
		}
	}
	
	//Descarga la instancia de la forma de la memoria
	function doUnloadDialog() {
		if (objDialog) {
			var objForm = objDialog.getAttachedObject();
			if (objForm && objForm.unload) {
				objForm.unload();
				objForm = null;
			}
		}
		
		if (objWindows) {
			if (objWindows.unload) {
				objWindows.unload();
				objWindows = null;
				objWindows = new dhtmlXWindows({
					image_path:"images/"
				});
			}
		}
		
		objDialog = null;
	}
	
	//Manda los datos al server para ser procesados
	function doSendData() {
		if (!objDialog) {
			return;
		}
		
		objDialog.progressOn();
		var objForm = objDialog.getAttachedObject();
		var objData = objForm.getFormData();
		objForm.lock();
		objForm.send("processRequest.php", "post", function(loader, response) {
			if (!loader || !response) {
				objDialog.progressOff();
				objForm.unlock();
				return;
			}
			
			if (loader.xmlDoc && loader.xmlDoc.status != 200) {
				alert("<?=translate("HTTP Error")?> #" + loader.xmlDoc.status + ": " + loader.xmlDoc.statusText);
				objDialog.progressOff();
				objForm.unlock();
				return;
			}
			
			setTimeout(function () {
				try {
					var objResponse = JSON.parse(response);
				} catch(e) {
					alert(e + "\r\n" + response.substr(0, 1000));
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				
				if (objResponse.error) {
					alert(objResponse.error.desc);
					objDialog.progressOff();
					objForm.unlock();
					return;
				}
				else {
					if (objResponse.warning) {
						alert(objResponse.warning.desc);
					}
				}
				
				//Sólo si todo sale Ok cierra el diálogo y continua cargando el diseño de la forma recien creada
				doUnloadDialog();
				var strURL = objResponse.url;
				if (strURL && parent.doExecuteURL) {
					parent.doExecuteURL("", undefined, undefined, strURL);
				}
			}, 100);
		});
	}
	
	//Muestra el diálogo para crear una nueva forma
	function addNewForm() {
		if (!objWindows) {
			//Prepara el controlador de ventanas que permitirá crear diálogos de captura o ventanas flotantes auxiliares
			objWindows = new dhtmlXWindows({
				image_path:"images/"
			});
		}
		
		objDialog = objWindows.createWindow({
			id:"newObject",
			left:0,
			top:0,
			width:intDialogWidth,
			height:intDialogHeight,
			center:true,
			modal:true
		});
		
		objDialog.setText("<?=translate("New survey")?>");
		objDialog.denyPark();
		objDialog.denyResize();
		objDialog.setIconCss("without_icon");
		
		//Al cerrar realizará el cambio de sección de la pregunta
		objDialog.attachEvent("onClose", function() {
			return true;
		});
		
		/* El diálogo para crear las formas simplemente pide el nombre que se le asignará a la nueva forma */
		var objFormData = [
			{type:"settings"/*, offsetLeft:20*/},
			{type:"input", name:"SurveyName", label:"<?=translate("Name")?>", labelAlign:"left", value:"", labelWidth:100, inputWidth:200, required:true, validate:"NotEmpty"},
			{type:"block", blockOffset:0, offsetLeft:100, list:[
				{type:"button", name: "btnOk", value:"<?=translate("OK")?>"},
				{type:"newcolumn"},
				{type:"button", name: "btnCancel", value:"<?=translate("Cancel")?>"}
			]},
			{type:"input", name:"SurveyID", value:-1, hidden:true},
			{type:"input", name:"Design", value:1, hidden:true},
			{type:"input", name:"RequestType", value:reqAjax, hidden:true},
			{type:"input", name:"Process", value:"Add", hidden:true},
			{type:"input", name:"ObjectType", value:<?=otySurvey?>, hidden:true}
		];
		
		var objForm = objDialog.attachForm(objFormData);
		objForm.adjustParentSize();
		objForm.setItemFocus("SurveyName");
		objForm.attachEvent("onBeforeValidate", function (id) {
			console.log('Before validating the form: id == ' + id);
		});
		
		objForm.attachEvent("onAfterValidate", function (status) {
			console.log('After validating the form: status == ' + status);
		});
		
		objForm.attachEvent("onValidateSuccess", function (name, value, result) {
			console.log('Validating rule ok: name == ' + name + ', value == ' + value + ', result == ' + result);
		});
		
		objForm.attachEvent("onValidateError", function (name, value, result) {
			console.log('Error validating the form: name == ' + name + ', value == ' + value + ', result == ' + result);
		});
		
		objForm.attachEvent("onKeyup", function(inp, ev, name, value) {
			if (!name || !inp || !ev || !ev.keyCode) {
				return;
			}
			
			switch (ev.keyCode) {
				case 13:
					//OMMC 2015-11-12: Agregada validación para prevenir la creación de varios objetos derivado de múltiples <enter> 
					if(objDialog.bitProcessing === undefined){
						objDialog.bitProcessing = true;
						setTimeout(function() {
							objForm.callEvent("onButtonClick", ["btnOk"]);
							objDialog.bitProcesing = undefined;
						}, 100);
					}
					break;
				case 27:
					setTimeout(function() {
						objForm.callEvent("onButtonClick", ["btnCancel"])
					}, 100);
					break;
			}
		});
		
		objForm.attachEvent("onButtonClick", function(name) {
			switch (name) {
				case "btnCancel":
					setTimeout(function () {
						doUnloadDialog();
					}, 100);
					break;
					
				case "btnOk":
					setTimeout(function() {
						if (objForm.validate()) {
							doSendData();
						}
					}, 100);
					break;
			}
		});
	}
	
	//JAPR 2015-11-25: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
	function browserApp(iSurveyID) {
		if (!$.isNumeric(iSurveyID)) {
			iSurveyID = 0;
		}
		
		d = new Date();
		dteNow = '' + d.getFullYear() + d.getMonth() + d.getDay() + d.getHours() + d.getMinutes() + d.getSeconds();
		openWindow('GenerateSurvey.php?dte=' + dteNow + ((iSurveyID > 0)?'&DefaultSurveyID='+iSurveyID :'') + '&login=1');
	}
	//JAPR
	
	function onLoad() {
		var rows = [];
		var data = {};
		
		/*2015-06-03@JRPP Añadimos un layout para colocar la pagina de nuestro portal */
		objFormsLayoutPortal = new dhtmlXLayoutObject({
			parent: document.body,
			pattern: "4I" /*4F genera una platilla de 3 renglones con el primer renglon dividido en 2 secciones*/
		});
		/*Al primer layout que es My Surveys le agrego un ancho de 900px*/
		//objFormsLayoutPortal.cells(dhxFormsList).setWidth("50%"); 
		/*Coloco el titulo al layout*/
		objFormsLayoutPortal.cells(dhxFormsList).hideHeader();
		/*Coloco fijas todas las columnas*/
		objFormsLayoutPortal.cells(dhxFormsList).fixSize(true, true);
		objFormsLayoutPortal.cells(dhxDataSrcsList).fixSize(true, true);
		objFormsLayoutPortal.cells(dhxSecurityArea).fixSize(true, true);
		objFormsLayoutPortal.cells(dhxStatsArea).fixSize(true, true);
		$(objFormsLayoutPortal.cells(dhxFormsList).cell).addClass('customGrid')
		$(objFormsLayoutPortal.cells(dhxDataSrcsList).cell).addClass('customGrid');
		
<?
	if (count($arrSurveryNames) == 0) {
?>
<?	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
?>
		objFomsDataView = objFormsLayoutPortal.cells(dhxFormsList).attachDataView({
			container:"formsColl",
			drag:false, 
			select:false,
			type:{
				template:"<div style=' font-size: 30px; float: left; text-align: center; width: 100%; padding-top:6%;'><?=translate("Click here to add your first form")?></div>",
				width:"100%; widthNA:100",
			},
			height:"auto"
		});
		
		objFomsDataView.attachEvent("onItemClick", function (id, ev, html){
			console.log('objFomsDataView.onItemClick ' + id);
			
			addNewForm();
		});
		objFomsDataView.add({id:"-1", description:"Click here to add your first form"});
<?	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		}
		else {
			//@JAPR 2015-11-09: Corregido un bug, si no es administrador no debe poder agregar formas, pero tampoco debe quedar en blanco la celda (#2QM0J2)
			//JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
			//Si es un invitado, al hacer click entra a la captura Web de la forma
?>
		objFormsGrid = objFormsLayoutPortal.cells(dhxFormsList).attachGrid();
		objFormsGrid.setHeader("<?=translate("My Forms")?>", null, ["background-color:cccccc;"]);//La cabecera de las columnas
		objFormsGrid.setInitWidthsP("100");
        objFormsGrid.setColAlign("left");       //la alineacion de las columnas  
        objFormsGrid.setColTypes("ro");                //el tipo "ro"(solo lectura)
        objFormsGrid.init();
		
		objFormsGrid.attachEvent("onHeaderClick", function(ind,obj) {
			browserApp();
		});
<?
			//@JAPR
		}
?>
<?
	}
	else {
?>
		//Inicia el código para generar el Grid de Formas
		/*Agrego un datagrid para posteriormente in incluyendo cada uno de los elementos*/
		objFormsGrid = objFormsLayoutPortal.cells(dhxFormsList).attachGrid();
		objFormsGrid.setHeader("<?=translate("My Forms")?>", null, ["background-color:cccccc;cursor:pointer;"]);//La cabecera de las columnas
        //objFormsGrid.setInitWidths("100%");          //el ancho de las columna s 
		objFormsGrid.setInitWidthsP("100");
        objFormsGrid.setColAlign("left");       //la alineacion de las columnas  
        objFormsGrid.setColTypes("ro");                //el tipo "ro"(solo lectura)
        //objFormsGrid.setColSorting("str");          //el tipo  "str"(cadena)
        objFormsGrid.init();
		
<?		//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
			//JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
			//Si es un Administrador de eForms, al hacer click en la forma entra al diseño de la misma
?>
		objFormsGrid.attachEvent("onRowSelect", function(rId,cInd) {	//onRowDblClicked
<?
			//@JAPR 2016-08-15: Agregada la configuración de permisos en el Administrador (#KY40NF)
			if (!isset($arrSecurity[otySurvey][asecShow]) || $arrSecurity[otySurvey][asecShow]) {
?>
			if (parent.doExecuteURL && rId) {
				strURL = "main.php?BITAM_SECTION=SurveyExtCollection&Design=1&SurveyID=" + rId;
				parent.doExecuteURL("", undefined, undefined, strURL);
			}
<?
			}
?>
		});
		objFormsGrid.attachEvent("onHeaderClick", function(ind,obj) {
			parent.doExecuteURL("Forms");
		});
<?
		}
		else {
			//JAPR 2015-11-24: Habilitada la captura vía Web invocada desde el Administrador (#NGA28W)
			//Si es un invitado, al hacer click entra a la captura Web de la forma
?>
		objFormsGrid.attachEvent("onRowSelect", function(rId,cInd) {	//onRowDblClicked
			browserApp(rId);
		});
		objFormsGrid.attachEvent("onHeaderClick", function(ind,obj) {
			browserApp();
		});
<?
		}

		//@JAPR 2015-11-26: Se solicitó por petición de cliente que no se mostraran las formas filtradas por las mas capturadas, sino que se mostrara la lista completa, así que se modifica
		//la manera de obtener la lista de formas para que sólo aplique el criterio de Schedulers en caso de invitados, y la lista completa en caso de Administradores
		//for($i=0; $i<count($arrSurveryNames); $i++){
		foreach ($arrSurveryNames as $intSurveyID => $strSurveyName) {
			//@JAPR 2015-11-02: Agregado el escape de las comillas en el nombre de la forma
			//@JAPR 2018-11-13: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
		rows.push({id:<?= $intSurveyID ?>, data: ["<?=GetValidJScriptText($strSurveyName)?>"] });
<?
		}
		//@JAPR
?>
		data.rows = rows;
		objFormsGrid.parse(data,"json");
		objFormsGrid.setStyle("font-size: 14px; font-weight: bold;", "cursor:pointer;");
		//Termina el código para generar el Grid de formas
<?
	}
?>
		
		/*Limpio las variables ya que me serviran para colocar los catalogos*/
		rows = [];
		data = {};
<?
	//@JAPR 2016-10-10: Agregada la validación de permiso de Artus Web antes de permitir interactuar con los escenarios (#MX5O2V)
	if ($AppUser->ArtusWebUserLicense <= 0) {
?>
		objFormsLayoutPortal.cells(dhxDataSrcsList).hideArrow();
		objFormsLayoutPortal.cells(dhxDataSrcsList).setText('<div style="font-size:16px;font-weight: bold;"><?=translate("There are no reports")?></div>');
<?
	} else {
?>
		objFormsLayoutPortal.cells(dhxDataSrcsList).hideHeader();
		/*Agrego un dataview para posteriormente in incluyendo cada uno de los elementos*/
		objDataSrcsGrid = objFormsLayoutPortal.cells(dhxDataSrcsList).attachGrid();
		//objDataSrcsGrid.setHeader("<?=translate("Data Source")?>", null, ["background-color:cccccc;"]);//La cabecera de las columnas
		//objDataSrcsGrid.setHeader("<?=translate("Dashboards")?>", null, ["background-color:cccccc;"]);//La cabecera de las columnas
		objDataSrcsGrid.setHeader("<?=translate("My reports")?>", null, ["background-color:cccccc;"]);//La cabecera de las columnas
        objDataSrcsGrid.setInitWidthsP("100");          //el ancho de las columna s 
        objDataSrcsGrid.setColAlign("left");       //la alineacion de las columnas  
        objDataSrcsGrid.setColTypes("ro");                //el tipo "ro"(solo lectura)
        //objDataSrcsGrid.setColSorting("str");          //el tipo  "str"(cadena)
        objDataSrcsGrid.init();
		//GCRUZ 2015-09-23 Poner la URL de artus correcta. $strArtusPath
        objDataSrcsGrid.attachEvent("onHeaderClick", function(ind,obj) {
			//RV Mayo2016: Validar que el click del encabezado sólo esté activo si hay reportes en la lista, es decir,
			//si el número de filas en el grid de reportes es mayor que cero.
			if(this.getRowsNum()>0)
			{
				try {
					<?//@JAPR 2016-06-30: Corregido un bug, el password estaba visible desencriptado (#VRNM0H)?>
					var strURL = "<?=$strArtusPath?>check.php?repository=<?=(string) @$_SESSION["PABITAM_RepositoryName"]?>&username=<?=$AppUser->ArtusUser?>&pwdencripted=1&password=<?=$strUserPassword?>&params=NOSCRIPT";
					d = new Date();
					y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
					var aWindow = window.open(strURL, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
				} catch(e) {
					alert(e);
				}
			}
		});
		objDataSrcsGrid.attachEvent("onRowSelect", function(rId,cInd) {	//onRowDblClicked
			try {
				<?//@JAPR 2016-06-30: Corregido un bug, el password estaba visible desencriptado (#VRNM0H)?>
				var strURL = "<?=$strArtusPath?>check.php?repository=<?=(string) @$_SESSION["PABITAM_RepositoryName"]?>&username=<?=$AppUser->ArtusUser?>&pwdencripted=1&password=<?=$strUserPassword?>&stage=" + rId + "&params=NOSCRIPT";
				d = new Date();
				y  = 'mywindow' + d.getHours() + d.getMinutes() + d.getSeconds();
				var aWindow = window.open(strURL, y, 'width=800,height=600,fullscreen=yes,titlebar=no,location=no,menubar=no,toolbar=no,status=no,resizable=yes,scrollbars=yes',1);
			} catch(e) {
				alert(e);
			}
		});
		
<?
		foreach ($arrDashboards as $intDashboardID => $strDashboardName) {
			//@JAPR 2018-11-13: Corregido el escape de algunos caracteres en strings (#KGW0CU)
?>
		rows.push({id:<?=$intDashboardID?>, data: ["<?=GetValidJScriptText($strDashboardName)?>"] });
<?
		}
?>
		data.rows = rows;
		objDataSrcsGrid.parse(data,"json");
		objDataSrcsGrid.setStyle("font-size: 14px; font-weight: bold;", "cursor:pointer;");
<?
	}
	//@JAPR
?>
		
		objFormsLayoutPortal.cells(dhxSecurityArea).hideHeader();
		objSecurityLayOut = objFormsLayoutPortal.cells(dhxSecurityArea).attachLayout({pattern: "3W"});
		objSecurityLayOut.cells(dhxUsers).hideHeader();
		objSecurityLayOut.cells(dhxGroups).hideHeader();
		objSecurityLayOut.cells(dhxSettings).hideHeader();
		objSecurityLayOut.cells(dhxUsers).fixSize(true, true);
		objSecurityLayOut.cells(dhxGroups).fixSize(true, true);
		objSecurityLayOut.cells(dhxSettings).fixSize(true, true);
		
		objStatsLayOut = objFormsLayoutPortal.cells(dhxStatsArea).attachLayout({pattern: "2U"});
//GCRUZ 2015-08-03. Generar cell de log chart con el puro chart (no usar dataview) (no ocultar el header para poner el título del chart ahí)
//		objStatsLayOut.cells(dhxLogChart).hideHeader();
//		objStatsLayOut.cells(dhxLogMap).hideHeader();
		objStatsLayOut.cells(dhxLogChart).fixSize(true, true);
		objStatsLayOut.cells(dhxLogMap).fixSize(true, true);
		//JAPR 2015-08-17: Removida la flecha para contraer la celda y agregado el click para cargar el detalle correspondiente
		objStatsLayOut.cells(dhxLogChart).hideArrow();
		$(objStatsLayOut.cells(dhxLogChart).cell).on('click', function() {
			parent.doExecuteURL("Reports");
		});
		objStatsLayOut.cells(dhxLogMap).hideArrow();
<?	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
?>
		$(objStatsLayOut.cells(dhxLogMap).cell).on('click', function() {
			parent.doExecuteURL("Tasks");
		});
<?
		}
?>

		setTimeout(function() {
			$(objStatsLayOut.cells(dhxLogChart).cell).css("cursor", "pointer");
			$(objStatsLayOut.cells(dhxLogChart).cell).find("span div").css("cursor", "pointer");
			$(objStatsLayOut.cells(dhxLogMap).cell).css("cursor", "pointer");
			$(objStatsLayOut.cells(dhxLogMap).cell).find("span div").css("cursor", "pointer");
		}, 100);
		//$(objStatsLayOut.cont).find("span div").css("cursor", "pointer");
		/*
		template:"<div style=' font-size:18px;  font-weight:normal; text-align: center; color: white; background-color: rgb(47, 85, 151); width: 96%;'>#description#</div>
		<div style= 'font-size: 16px; padding: 10px;'>#text#</div>
		<div style= 'font-size:30px; float: left; text-align: right; width: 43%; padding-top: 32px;'>#number#</div>
		<img style='padding-left: 13px; padding-top: 19px;' src=#picture#>",
		*/
		//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
		//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
		//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema
		objSecurityLayOut.cells(dhxUsers).attachHTMLString("<div id='usersStatsColl'><div>")
		//objUsersDataView = objSecurityLayOut.cells(dhxUsers).attachDataView({
		objUsersDataView = new dhtmlXDataView({
			container:"usersStatsColl",
			drag:false, 
			select:false,
			type:{
				template:"<div style='width:100%;height:100%;position:relative;background-image:url(#backgroundImage#);background-size:100% 118px;'>\
					<img src='#logo#' style='position:absolute;top:8px;left:5px;'></img>\
					<div style='float:right;width:20px;'>&nbsp;</div>\
					<div style='float:right;text-align:center;'>\
						<div style='font-family:Roboto Regular;font-size:40px;color:white;'>#count#</div>\
						<div style='font-family:Roboto Light;font-size:23px;color:white;'>#description#</div>\
					</div>\
					<div style='position:absolute;top:90px;left:10px;font-family:Roboto Regular;font-size:14px;color:white;'>#text#</div>\
					<div style='position:absolute;top:90px;right:10px;'>\
						<img src='images/admin/whiteRightArrow.png' style='float:right;'></img>\
					</div>\
				</div>",
				width:"100%; widthNA:100",
				height:"300",
				border:0,
				padding:0
			},
			padding:0,
			height:"auto"
		});
		
		//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
		//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
		//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema
		objSecurityLayOut.cells(dhxGroups).attachHTMLString("<div id='groupsStatsColl'><div>")
		//objGroupsDataView = objSecurityLayOut.cells(dhxGroups).attachDataView({
		objGroupsDataView = new dhtmlXDataView({
			container:"groupsStatsColl",
			drag:false, 
			select:false,
			type:{
				template:"<div style='width:100%;height:100%;position:relative;background-image:url(#backgroundImage#);background-size:100% 118px;'>\
					<img src='#logo#' style='position:absolute;top:8px;left:5px;'></img>\
					<div style='float:right;width:20px;'>&nbsp;</div>\
					<div style='float:right;text-align:center;'>\
						<div style='font-family:Roboto Regular;font-size:40px;color:white;'>#count#</div>\
						<div style='font-family:Roboto Light;font-size:23px;color:white;'>#description#</div>\
					</div>\
					<div style='position:absolute;top:90px;left:10px;font-family:Roboto Regular;font-size:14px;color:white;'>#text#</div>\
					<div style='position:absolute;top:90px;right:10px;'>\
						<img src='images/admin/whiteRightArrow.png' style='float:right;'></img>\
					</div>\
				</div>",
				width:"100%; widthNA:100",
				height:"300",
				border:0,
				padding:0
			},
			height:"auto"
		});
		
		//JAPR 2015-11-06: Corregido un bug, cuando se delega a DHTMLX el generar automáticamente el componente, en caso contados y aleatorios, otorgaba el mismo Id a dos o mas elementos
		//y eso causaba que el que tomó el mismo ID de otro se generara incorrecto en uno de los casos, dejándo el componente totalmente en blanco. Ahora se modificó para agregar
		//un div con un Id fijo y sobre él montar el componente DHTMLX, de esa manera el ID ya no es aleatorio basado en marca de tiempo y no se podrá presentar el problema
		objSecurityLayOut.cells(dhxSettings).attachHTMLString("<div id='settingsStatsColl'><div>")
		//objSettingsDataView = objSecurityLayOut.cells(dhxSettings).attachDataView({
		objSettingsDataView = new dhtmlXDataView({
			container:"settingsStatsColl",
			drag:false, 
			select:false,
			type:{
				template:"<div style='width:100%;height:100%;position:relative;background-image:url(#backgroundImage#);background-size:100% 118px;'>\
					<img src='#logo#' style='position:absolute;top:8px;left:5px;'></img>\
					<div style='float:right;width:20px;'>&nbsp;</div>\
					<div style='float:right;text-align:center;'>\
						<div style='font-family:Roboto Regular;font-size:40px;color:white;'>#count#</div>\
						<div style='font-family:Roboto Light;font-size:23px;color:white;'>#description#</div>\
					</div>\
					<div style='position:absolute;top:90px;left:10px;font-family:Roboto Regular;font-size:14px;color:white;'>#text#</div>\
					<div style='position:absolute;top:90px;right:10px;'>\
						<img src='images/admin/whiteRightArrow.png' style='float:right;'></img>\
					</div>\
				</div>",
				width:"100%; widthNA:100",
				height:"300",
				border:0,
				padding:0
			},
			height:"auto"
		});
		objUsersDataView.add({id:"Usuarios", description:"<?=translate("Users")?>", 
			backgroundImage:"images/admin/yellowFrame.png", logo:"images/admin/usersLogo.png",
			count:<?=$numUser?>, text:"<?=translate("Add users and assign them to groups of users")?>"});
		objGroupsDataView.add({id:"Groups", description:"<?=translate("Groups")?>", 
			backgroundImage:"images/admin/blueFrame.png", logo:"images/admin/groupsLogo.png",
			count:<?=$numRol?>, text:"<?=translate("Add and edit user groups")?>"});
		objSettingsDataView.add({id:"Settings", description:"<?=translate("Synchronizations")?>", 
			backgroundImage:"images/admin/yellowFrame.png", logo:"images/admin/syncLogo.png",
			count:<?=$numSyncs?>, text:"<?=translate("")?>"});
		
		objFormsLayoutPortal.cells(dhxStatsArea).hideHeader();
		var strTemplate = "";
<?
		if ($totalCap == 0) {
?>
			var strTemplateCapt = "<div style=' font-size: 30px; float: left; text-align: center; width: 100%; padding-top:6%;'>#numero# #description#</div>";
<?
		}
		else {
?>
			var strTemplateCapt = "<div style=' font-size: 30px; float: left; text-align: center; width: 100%; padding-top:6%;'>#numero# #description#</div><div id='#divGraphID#' style='width:600px;height:150px;margin:20px;border:1px solid #A4BED4'></div>";
<?
		}
?>
//GCRUZ 2015-08-03. Generar cell de log chart con el purto chart (no usar dataview)
/*		//GCRUZ Comentar este código
		objDailyEntriesDataView = objStatsLayOut.cells(dhxLogChart).attachDataView({
			container:"logChartColl",
			drag:false, 
			select:false,
			type:{
				template:strTemplate
			},
			height:"auto"
		});
		
		objDailyEntriesDataView.attachEvent("onItemClick", function (id, ev, html){
			console.log('objDailyEntriesDataView.onItemClick ' + id);
			
			parent.doExecuteURL("Reports");
		});
<?
		if ($totalCap == 0) {
?>
		objDailyEntriesDataView.add({id:"imageSurvery", description:"<?=translate("There are no entries today")?>", numero:""});
<?
		}
		else {
?>
		objDailyEntriesDataView.add({id:"imageSurvery", description:"<?=translate("Catch today")?>", numero:"<?=$totalCap?>", divGraphID:"divGraph01"});
<?
		}
		
?>
*/
		var strTemplate = "";
		
<?
		if (count($arraySync) == 0) {
?>
			var strTemplateSync = "<div style=' font-size: 30px; float: left; text-align: center; width: 100%; padding-top:6%;'>#numero##description#</div>";
<?
		}
		else {
?>
			var strTemplateSync = "<div style=' font-size: 30px; float: left; text-align: center; width: 100%; padding-top:6%;'>#numero##description#</div><div id='#divGraphID#' style='width:600px;height:150px;margin:20px;border:1px solid #A4BED4'></div>";
<?
		}
?>
//GCRUZ 2015-08-03. Generar cell de log chart con el purto chart (no usar dataview)
/*		//GCRUZ Comentar este código
		objDailySyncDataView = objStatsLayOut.cells(dhxLogMap).attachDataView({
			container:"logChartColl",
			drag:false, 
			select:false,
			type:{
				template:strTemplate
			},
			height:"auto"
		});
		objDailySyncDataView.attachEvent("onItemClick", function (id, ev, html){
			console.log('objDailySyncDataView.onItemClick ' + id);
			
			parent.doExecuteURL("Tasks");
		});
<?
		if (count($arraySync) == 0) {
?>
		objDailySyncDataView.add({id:"imageSurvery",image:"images/admin/graph.png", description:"<?=translate("There are no synchronizations today")?>", numero:""});
<?
		}
		else {
?>
		objDailySyncDataView.add({id:"imageSurvery",image:"images/admin/graph.png", description:"<?=translate("Synchronizations history")?>", numero:"", divGraphID:"divGraph02"});
<?
		}
?>
*/		
<?	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
		if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
?>
		/*Se agregan eventos*/
		objUsersDataView.attachEvent("onItemClick", function (id, ev, html){
			parent.doExecuteURL("Users");
		});
		objGroupsDataView.attachEvent("onItemClick", function (id, ev, html){
			parent.doExecuteURL("User Groups");
		});
		objSettingsDataView.attachEvent("onItemClick", function (id, ev, html){
			parent.doExecuteURL("Tasks");
		});
<?
		}
?>
		
		doneResizing();
		
		/*Inicia Código para agregar la gráfica de capturas*/
<?
		$chartSegments = 5;
		if (count($countCap) > 0) {
?>
			var dataCap = [
			<?
			$numCount=count($countCap);
			$iCount=0;
			foreach($countCap as $surveyName => $captures)
			{
				if($iCount==($numCount-1))
				{
				//@JAPR 2015-11-02: Agregado el escape de las comillas en el nombre de la forma
			?>
				[<?=$captures?>,"<?=addslashes($surveyName)?>"]
			<?
				}
				else
				{
			?>
				[<?=$captures?>,"<?=addslashes($surveyName)?>"],
			<?
				}
				$iCount++;
			}
			?>
			];
//GCRUZ 2015-08-03. Generar cell de log chart con el purto chart (no usar dataview)
/*		//GCRUZ Comentar este código	
			var myBarChart;
		<?
			$maxLim=(int)($maxLim*1.20);
			$step=(int)($maxLim/$chartSegments);
		?>
			myBarChart = new dhtmlXChart({
				view:"bar",
				color:"#4682B4",
				container:"divGraph01",
				value:"#data0#",
				radius:0,
				border:true,
				label:"#data0#",
				xAxis:{
					template:"#data1#"
				},
				yAxis:{
					start:0,
					end:<?=$maxLim?>,
					step:<?=$step?>,
					template:function(obj){
			//return (obj%20?"":obj)
			return (obj%<?=$step?>?"":obj)
					}
				}
			});
			myBarChart.parse(dataCap,"jsarray");
*/
			objDailyEntriesChart = objStatsLayOut.cells(dhxLogChart).attachChart({
				view:"bar",
				color:"#4682B4",
				container:"divGraph01",
				value:"#data0#",
				radius:0,
				border:true,
				label:"#data0#",
				xAxis:{
					template:"#data1#"
				},
				yAxis:{
					color:"#A4A4A4",
					start:0,
					end:<?=$maxLim?>,
					step:<?=$step?>,
					template:""
/*
					template:function(obj){
				//return (obj%20?"":obj)
				return (obj%<?=$step?>?"":obj)
						}
*/
					}
			});
			objDailyEntriesChart.attachEvent("onItemClick", function (id, ev, html){
				console.log('objDailyEntriesChart.onItemClick ' + id);
				
				parent.doExecuteURL("Reports");
			});
			objStatsLayOut.cells(dhxLogChart).setText('<div style="font-size:16px;font-weight: bold;"><?=$totalCap?> <?=translate("Catch today")?></div>');
			objDailyEntriesChart.parse(dataCap,"jsarray");
<?
		}
		else
		{
?>
		objStatsLayOut.cells(dhxLogChart).attachHTMLString('<div align="center"><br><img src="images/nodata.jpg"></div>');
/*
		objDailyEntriesDataView = objStatsLayOut.cells(dhxLogChart).attachDataView({
			container:"logChartColl",
			drag:false, 
			select:false,
			type:{
				template:strTemplateCapt
			},
			height:"auto"
		});
		
		objDailyEntriesDataView.attachEvent("onItemClick", function (id, ev, html){
			console.log('objDailyEntriesDataView.onItemClick ' + id);
			
			parent.doExecuteURL("Reports");
		});
		objDailyEntriesDataView.add({id:"imageSurvery", description:"<?=translate("There are no entries today")?>", numero:""});
*/
		objStatsLayOut.cells(dhxLogChart).setText('<div style="font-size:16px;font-weight: bold;"><?=translate("There are no entries today")?></div>');
<?
		}
		/*Termina código para agregar la gráfica de capturas*/
		
		/*Inicia Código para agregar la gráfica de sincronizaciones*/

		if (count($arraySync) > 0) {
?>
			var dataSinc = [
			<?
			$numCount=count($arraySync);
			$iCount=0;
			$maxLimSinc=5;
			foreach($arraySync as $surveyDate => $sincro)
			{
				if($iCount==($numCount-1))
				{
			?>
				[<?=$sincro?>,"<?=$surveyDate?>"]
			<?
				}
				else
				{
			?>
				[<?=$sincro?>,"<?=$surveyDate?>"],
			<?
				}
				$iCount++;
				if($sincro>$maxLimSinc)
				{
					$maxLimSinc=$sincro;
				}
			}
			$maxLimSinc=(int)($maxLimSinc*1.20);
			$stepSinc=(int)($maxLimSinc/$chartSegments);
			?>
			];
//GCRUZ 2015-08-03. Generar cell de log chart con el purto chart (no usar dataview)
/*		//GCRUZ Comentar este código	
			var myBarChartSinc;

			myBarChartSinc = new dhtmlXChart({
				view:"bar",
				color:"#4682B4",
				container:"divGraph02",
				value:"#data0#",
				radius:0,
				border:true,
				label:"#data0#",
				xAxis:{
					template:"#data1#"
				},
				yAxis:{
					start:0,
					end:<?=$maxLimSinc?>,
					step:<?=$stepSinc?>,
					template:function(obj){
			//return (obj%20?"":obj)
			return (obj%<?=$stepSinc?>?"":obj)
					}
				}
			});
			myBarChartSinc.parse(dataSinc,"jsarray");
*/
			objDailySyncChart = objStatsLayOut.cells(dhxLogMap).attachChart({
				view:"bar",
				color:"#4682B4",
				container:"divGraph02",
				value:"#data0#",
				radius:0,
				border:true,
				label:"#data0#",
				xAxis:{
					template:"#data1#"
				},
				yAxis:{
					color:"#A4A4A4",
					start:0,
					end:<?=$maxLimSinc?>,
					step:<?=$stepSinc?>,
					template : ""
/*					template:function(obj){
				//return (obj%20?"":obj)
				return (obj%<?=$stepSinc?>?"":obj)
						}
*/
					}
			});
<?	//@JAPR 2015-09-03: validado que sólo los administradores puedan acceder a ciertas opciones de eForms
			if ((int) @$_SESSION["PABITAM_UserRole"]==1) {
?>
			objDailySyncChart.attachEvent("onItemClick", function (id, ev, html){
				console.log('objDailySyncChart.onItemClick ' + id);
				
				parent.doExecuteURL("Tasks");
			});
<?
			}
?>
			objStatsLayOut.cells(dhxLogMap).setText('<div style="font-size:16px;font-weight: bold;"><?=translate("Synchronizations history")?></div>');
			objDailySyncChart.parse(dataSinc,"jsarray");
<?
		}
		else
		{
?>
			objStatsLayOut.cells(dhxLogMap).attachHTMLString('<div align="center"><br><img src="images/nosync.jpg"></div>');
/*
			objDailySyncDataView = objStatsLayOut.cells(dhxLogMap).attachDataView({
				container:"logChartColl",
				drag:false, 
				select:false,
				type:{
					template:strTemplateSync
				},
				height:"auto"
			});
			objDailySyncDataView.attachEvent("onItemClick", function (id, ev, html){
				console.log('objDailySyncDataView.onItemClick ' + id);
				
				parent.doExecuteURL("Tasks");
			});
			objDailySyncDataView.add({id:"imageSurvery",image:"images/admin/graph.png", description:"<?=translate("There are no synchronizations today")?>", numero:""});
*/
			objStatsLayOut.cells(dhxLogMap).setText('<div style="font-size:16px;font-weight: bold;"><?=translate("There are no synchronizations today")?></div>');
<?
		}
		/*Termina código para agregar la gráfica de capturas*/
?>

//GCRUZ 2015-08-03. Generar cell de log chart con el purto chart (no usar dataview)
			objStatsLayOut.attachEvent("onCollapse", function(name){
				// your code
				objStatsLayOut.cells(name).expand();
			});
	}
</script>
</head>
<body onload="onLoad();" style="width: 100%; height: 100%;">
	<div id="__menu__" style="overflow:auto;"></div>
</body>
</html>