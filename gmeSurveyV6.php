<?php
$bDebug=true;

//Obtener los Datos del Reporte
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("report.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else
{	//English
	InitializeLocale(2);
	LoadLanguageWithName("EN");
}

$wTitle     =(array_key_exists("WinTitle", $_GET) ? $_GET["WinTitle"]   : 'Bitam ' . translate('Survey') . ' - '. translate('Map'));

$reportType = $_REQUEST['reportType'];

if ($reportType == 0)
{
	require_once('eSurveyServiceMod.inc.php');
	$aBITAMConnection = connectToKPIRepository();
	//@JAPR 2017-05-16: Corregido un bug, estaba intentando obtener el repositorio del request siendo que no se envía en la URL, ahora se obtendrá de la sesión (#SM8SGM)
	$aSQL = "SELECT UserID, Latitude, Longitude FROM SI_SV_SurveyTasksLog WHERE repository = ".$aBITAMConnection->ADOConnection->Quote(@$_SESSION['PABITAM_RepositoryName'])." AND surveyid = ".$_REQUEST['surveyID']." AND surveydate BETWEEN ".$aBITAMConnection->ADOConnection->Quote($_REQUEST['startDate'])." AND ".$aBITAMConnection->ADOConnection->Quote($_REQUEST['endDate'])." ORDER BY creationdateid";
	$aRS = $aBITAMConnection->ADOConnection->Execute($aSQL);
	
	if ($aRS === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyTasksLog Table : ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
	}	
}
else
{
	//@JAPR 2015-08-14: Agregado el borrado lógico de capturas en el log
	$strWhere = '';
	if (getMDVersion() >= esvDeleteReports) {
		$strWhere = "AND A.Deleted = 0 ";
	}
	
	//@JAPR 2017-05-16: Corregido un bug, estaba intentando obtener el repositorio del request siendo que no se envía en la URL, ahora se obtendrá de la sesión (#SM8SGM)
	$aSQL = "SELECT A.UserID, A.Latitude, A.Longitude 
		FROM SI_SV_SurveyLog A 
		WHERE A.Repository = ".$theRepository->DataADOConnection->Quote(@$_SESSION['PABITAM_RepositoryName'])." 
			AND A.SurveyID = ".$_REQUEST['surveyID']." {$strWhere} 
			AND A.SurveyDate BETWEEN ".$theRepository->DataADOConnection->Quote($_REQUEST['startDate'])." AND ".$theRepository->DataADOConnection->Quote($_REQUEST['endDate'])." 
		ORDER BY A.CreationDateID";
		
	$aRS = $theRepository->DataADOConnection->Execute($aSQL);
	if ($aRS === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyTasksLog Table : ".$theRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$aSQL);
	}
	
	/*
	$aSQL = "SELECT A.UserID, A.Latitude, A.Longitude 
		FROM SI_SV_SurveyLog A 
		WHERE A.Repository = ".$aBITAMConnection->ADOConnection->Quote(@$_SESSION['PABITAM_RepositoryName'])." 
			AND A.SurveyID = ".$_REQUEST['surveyID']." {$strWhere} 
			AND A.SurveyDate BETWEEN ".$aBITAMConnection->ADOConnection->Quote($_REQUEST['startDate'])." AND ".$aBITAMConnection->ADOConnection->Quote($_REQUEST['endDate'])." 
		ORDER BY A.CreationDateID";
			
	$aRS = $aBITAMConnection->ADOConnection->Execute($aSQL);
	*/
}

$desc = $aRS->fields['UserID'];
$longitud = $aRS->fields['Latitude'];
$latitud = $aRS->fields['Longitude'];


//Parsear los Datos a Markers
class Marker
{
	public $desc='';
	public $long=0;
	public $lat =0;

	public function __construct($desc, $long, $lat)
	{
       $this->desc	= $desc;
       $this->long	= $long;
       $this->lat   = $lat;
	}
}

//Separadores
$sep      ='_AWSep_';
$sepMarker='_AWSepMkr_';
$aMarker = array();

$bFijo=false;
if($bFijo)
{
	//Crear Markers Dummy
	$i=0;
	$aMarker[$i++]=new Marker('Bitam, Tampico, Mex', 22.249183, -97.868183);
	$aMarker[$i++]=new Marker('Monterrey, Mex', 25.648954,-100.302429);
	$aMarker[$i++]=new Marker('Ciudad de Mexico, Mex', 19.139979,-99.255981);
	$aMarker[$i++]=new Marker('Las Vegas, US', 36.155618,-115.172424);
	$aMarker[$i++]=new Marker('Houston, US', 29.847791,-95.369568);
	$aMarker[$i++]=new Marker('Dallas, US', 32.175612,-96.789551);
	$aMarker[$i++]=new Marker('San Antonio, US',29.113775,-98.22876);

	//Valores y Descripciones de los Markers
	for($iMaxDatas=5, $i=1, $sDataVals='', $sDataDesc=''; $i<$iMaxDatas; $i++)
	{
		$sDataDesc .='Data'.$i.(($i+1)<$iMaxDatas?$sep:'');
		$sDataVals .=$i.(($i+1)<$iMaxDatas?',':'');
	}
	//Formar los Markers en Cadena
	for($i=0, $sMarkerInf=''; $i<count($aMarker); $i++)
	{
		$sMarkerInf.=$aMarker[$i]->desc.$sep.$aMarker[$i]->long.$sep.$aMarker[$i]->lat.$sep.$sDataVals.$sepMarker;
	}
}
else
{
	$aMarker[0]=new Marker($desc, $longitud, $latitud);
	$sMarkerInf=$aMarker[0]->desc.$sep.$aMarker[0]->long.$sep.$aMarker[0]->lat.$sep.$aMarker[0]->lat.','.$aMarker[0]->long.$sepMarker;
}
$sDataDesc ='Latitude_AWSep_Longitude';
$sMarkerInf=str_replace('"', '', $sMarkerInf);
$sMarkerInf=str_replace("\n", ' ', $sMarkerInf);
$sMarkerInf=str_replace("\r", ' ', $sMarkerInf);
$sDataDesc =htmlspecialchars($sDataDesc, ENT_QUOTES);
$sMarkerInf=htmlspecialchars($sMarkerInf, ENT_QUOTES);

?>
<!DOCTYPE html>
<html>
<head>
	<title><?=$wTitle?></title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<style type="text/css">
		html { height: 100% }
		body { height: 100%; margin: 0; padding: 0 }
		#map_canvas { height: 100% }
	</style>
<?
	//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
?>
	<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery/ui/jquery-ui-1.8.23.custom.js"></script>
<?php
	//@JAPR
echo "   <script language='JavaScript'>\n";
echo "      var sDataDesc ='".$sDataDesc."';\n";
echo "      var sMarkerInf='".$sMarkerInf."';\n";
echo "      var sep       ='".$sep."';\n";
echo "      var sepMarker ='".$sepMarker."';\n";
echo "   </script>\n";
//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
$intMapType = maptApple;
$objSetting = BITAMSettingsVariable::NewInstanceWithName($theRepository, 'MAPTYPE');
if (!is_null($objSetting) && trim($objSetting->SettingValue != '')) {
	$intMapType = (int) @$objSetting->SettingValue;
	if ( $intMapType < maptGoogle || $intMapType > maptApple ) {
		$intMapType = maptApple;
	}
}

//Carga el API de mapas según la configuración
switch ($intMapType) {
	case maptGoogle:
?>
	<script type="text/javascript" src="https://maps-api-ssl.google.com/maps/api/js?sensor=false<?=googleServerAPIKey?>"></script>
<?
		break;
	case maptApple:
	default:
?>
	<script type="text/javascript" src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.js"></script>
<?
		break;
}
//@JAPR
?>
	<script type="text/javascript" src="js/gmeSurvey.js"></script>
	<!-- <link rel="stylesheet" type="text/css" href="css/gmeSurvey.css"> -->
	<script type="text/javascript">
	</script>
</head>
<?
//@JAPR 2018-10-16: Integrado los Mapas de Apple (#64ISFM)
?>
<body onload="initializeMaps(<?=$intMapType?>)">
	<div id="map_canvas" style="width:100%; height:100%"></div>
</body>
</html>