<?php

// Contiene información referente a un campo real dentro de una tabla (basado en el MetaColumns del objeto adodb)
class BITAMTableField extends BITAMObject
{
	public $Table;
	public $Name;
	public $MetaType;						// http://phplens.com/adodb/reference.functions.metatype.html
	public $MaxLength;
	public $GetSchemaFromRepository;		// Indica si se utilizará ADOConnection (true) o DataADOConnection (false)
	
	function __construct($aRepository, $aTableName, $bFromRepository = false)
	{	
		BITAMObject::__construct($aRepository);
		$this->Table = $aTableName;
		$this->Name = '';
		$this->MetaType = '';
		$this->MaxLength = 0;
		$this->Scale = 0;
		$this->GetSchemaFromRepository = $bFromRepository;
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	/* Se renombró esta función porque de manera atípica dimensionTableField se extienda de esta clase en lugar de object directamente, así que
	definir un método NewInstance aquí provocaba que ya no pudiera ser redefinido en dimensionTableField la cual requiere parámetros mas
	complejos, así que lo mas óptimo es renombrar esta función que es muy específica para campos de una tabla
	*/
	static function NewInstanceField($aRepository, $aTableName, $bFromRepository = false)
	{
		return new BITAMTableField($aRepository, $aTableName, $bFromRepository);
	}
	
	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	/* Se renombró esta función porque de manera atípica dimensionTableField se extienda de esta clase en lugar de object directamente, así que
	definir un método NewInstanceWithName aquí provocaba que ya no pudiera ser redefinido en dimensionTableField la cual requiere parámetros mas
	complejos, así que lo mas óptimo es renombrar esta función que es muy específica para campos de una tabla
	*/
	static function NewInstanceWithFieldName($aRepository, $aTableName, $aFieldName, $bFromRepository = false)
	{
		$anInstance = new BITAMTableField($aRepository, $aTableName, $bFromRepository);
		$anInstance->getFieldData($aFieldName);
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = null;
		$aTableName = '';
		$aFieldName = '';
		$bFromRepository = '0';
		if (array_key_exists("TableName", $aHTTPRequest->GET))
		{
			$aTableName = $aHTTPRequest->GET["TableName"];
		}
		
		if (array_key_exists("FieldName", $aHTTPRequest->GET))
		{
			$aFieldName = $aHTTPRequest->GET["FieldName"];
		}
		
		if (array_key_exists("FromRepository", $aHTTPRequest->GET))
		{
			$bFromRepository = $aHTTPRequest->GET["FromRepository"];
		}
		
		if ($aFieldName != '')
		{
			//@JAPR 2015-02-10: Agregado soporte para php 5.6
			$anInstance = BITAMTableField::NewInstanceWithFieldName($aRepository,$aTableName,$aFieldName,$bFromRepository);
			if (is_null($anInstance))
			{
				//@JAPR 2015-02-10: Agregado soporte para php 5.6
				$anInstance = BITAMTableField::NewInstanceField($aRepository, $aTableName, $bFromRepository);
			}
		}
		else
		{
			//@JAPR 2015-02-10: Agregado soporte para php 5.6
			$anInstance = BITAMTableField::NewInstanceField($aRepository, $aTableName, $bFromRepository);
		}
		return $anInstance;
	}
	
	function getFieldData($aFieldName)
	{
		if ($this->Table == '' || $aFieldName == '')
		{
			return;
		}
		
		if ($this->GetSchemaFromRepository)
		{
			//$metaColumns = $this->Repository->ADOConnection->MetaColumns($this->Table);
			$metaColumns=$this->Repository->GetTableSchema($this->Table,false);
		}
		else 
		{
			//$metaColumns = $this->Repository->DataADOConnection->MetaColumns($this->Table);
			$metaColumns=$this->Repository->GetTableSchema($this->Table,true);
		}
		
		if (array_key_exists(strtoupper($aFieldName), $metaColumns))
		{
			$aFieldSchema = $metaColumns[strtoupper($aFieldName)];
			$this->Name = $aFieldSchema->name;
			$this->MetaType = $aFieldSchema->type;
			$this->MaxLength = $aFieldSchema->max_length;
		}
	}
	
	function get_FormIDFieldName()
	{
		return 'Name';
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=TableField&TableName=".$this->Table.'&FromRepository='.(int)$this->GetSchemaFromRepository;
		}
		else
		{
			return "BITAM_PAGE=TableField&TableName=".$this->Table.'&FieldName='.$this->Name.'&FromRepository='.(int)$this->GetSchemaFromRepository;
		}
	}

	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Field");
		}
		else
		{  
			return translate("Field");
		}
	}

	function isNewObject()
	{
		return ($this->Name == '');
	}

	function get_Parent()
	{
		if ($this->Table != '')
		{
			//@JAPR 2015-02-10: Agregado soporte para php 5.6
			return BITAMTableFieldCollection::NewInstanceFields($this->Repository,$this->Table,null,null,$this->GetSchemaFromRepository);
		}
		else 
		{
			return $this->Repository;
		}
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Table";
		$aField->Title = translate("Table");
		$aField->Type = "String";
		$aField->Size = 64;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Name";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 64;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataTypeName";
		$aField->Title = translate("Data Type");
		$aField->Type = "String";
		$aField->Size = 10;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MaxLength";
		$aField->Title = translate("Length");
		$aField->Type = "Integer";
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	// Regresa una descripción del tipo de dato con nombres comunes
	function DataTypeName()
	{
		$strDataTypeName = $this->MetaType;
		switch (strtoupper($this->MetaType))
		{
			case 'C':
				$strDataTypeName = 'VarChar';
				break;
			case 'X':
				$strDataTypeName = 'Text';
				break;
			case 'D':
				$strDataTypeName = 'DateTime';
				break;
			case 'T':
				$strDataTypeName = 'TimeStamp';
				break;
			case 'L':
				$strDataTypeName = 'Boolean';
				break;
			case 'N':
				$strDataTypeName = 'Float';
				break;
			case 'I':
				$strDataTypeName = 'Integer';
				break;
			case 'R':
				$strDataTypeName = 'Identity';
				break;
			case 'B':
				$strDataTypeName = 'Blob';
				break;
		}
		
		return $strDataTypeName;
	}

	// Regresa una descripción del tipo de dato con nombres que maneja el FrameWork de Ektos en BITAMObject->Type
	function FrameWorkDataTypeName()
	{
		$strDataTypeName = $this->MetaType;
		switch (strtoupper($this->MetaType))
		{
			case 'C':
				$strDataTypeName = 'String';
				break;
			case 'X':
				$strDataTypeName = 'LargeString';
				break;
			case 'D':
				$strDataTypeName = 'DateTime';
				//$strDataTypeName = 'String';	// 'DateTime'
				//$this->MaxLength = 10;
				break;
			case 'T':
				$strDataTypeName = 'DateTime';
				//$strDataTypeName = 'String';	// 'DateTime'
				//$this->MaxLength = 10;
				break;
			case 'L':
				$strDataTypeName = 'Boolean';
				break;
			case 'N':
				$strDataTypeName = 'Float';
				break;
			case 'I':
				$strDataTypeName = 'Integer';
				break;
			case 'R':
				$strDataTypeName = 'Integer';
				break;
			case 'B':
				$strDataTypeName = 'Blob';
				break;
		}
		
		return $strDataTypeName;
	}

	// Regresa una máscara cuando el tipo de dato iArtusDataType es del especificado (basandose en los tipos de dato de Artus:
	// 1|2 = String
	// 6|7 = Integer
	// 8|21|22|23|24 = Float
	// 12 = DateTime
	function FrameWorkFormatMask($iArtusDataType = 1, $sFormatMask = '', $sDefaultMask = '')
	{
		$strFormatMask = $sDefaultMask;
		switch (strtoupper($this->MetaType))
		{
			case 'C':
			case 'X':
				if ($iArtusDataType == 1 || $iArtusDataType == 2)
				{
					$strFormatMask = $sFormatMask;
				}
				break;
			case 'D':
			case 'T':
				if ($iArtusDataType == 12)
				{
					$strFormatMask = $sFormatMask;
				}
				break;
			case 'N':
				if ($iArtusDataType == 8 || $iArtusDataType == 21 || $iArtusDataType == 22 || $iArtusDataType == 23 || $iArtusDataType == 24)
				{
					$strFormatMask = $sFormatMask;
				}
				break;
			case 'L':
			case 'I':
			case 'R':
				if ($iArtusDataType == 6 || $iArtusDataType == 7)
				{
					$strFormatMask = $sFormatMask;
				}
				break;
		}
		
		return $strFormatMask;
	}

	// Indica si el campo permite especificar una longitud (solo los tipo string)
	function hasLength()
	{
		$blnHasLength = false;
		switch (strtoupper($this->MetaType))
		{
			//case 'D':
			//case 'T':
			case 'C':
			case 'X':
				$blnHasLength = true;
				break;
		}
		
		return $blnHasLength;
	}

	// Indica si el campo debe ser escapado
	function mustBeQuoted()
	{
		$blnMustBeQuoted = false;
		switch (strtoupper($this->MetaType))
		{
			case 'C':
			case 'X':
				$blnMustBeQuoted = true;
				break;
		}
		
		return $blnMustBeQuoted;
	}

	// Indica si el campo es una fecha
	function isADate()
	{
		$blnIsADate = false;
		switch (strtoupper($this->MetaType))
		{
			case 'D':
			case 'T':
				$blnIsADate = true;
				break;
		}
		
		return $blnIsADate;
	}
}

// Contiene una colección de campos referentes a una tabla específica
class BITAMTableFieldCollection extends BITAMCollection
{
	public $Table;
	public $GetSchemaFromRepository;		// Indica si se utilizará ADOConnection (true) o DataADOConnection (false)
	
	function __construct($aRepository, $aTableName, $bFromRepository = false)
	{	
		BITAMCollection::__construct($aRepository);
		$this->Table = $aTableName;
	}

	//@JAPR 2015-02-10: Agregado soporte para php 5.6
	/* Se renombró esta función porque de manera atípica dimensionTableField se extienda de esta clase en lugar de object directamente, así que
	definir un método NewInstance aquí provocaba que ya no pudiera ser redefinido en dimensionTableField la cual requiere parámetros mas
	complejos, así que lo mas óptimo es renombrar esta función que es muy específica para campos de una tabla
	*/
	static function NewInstanceFields($aRepository, $aTableName, $aFieldNames = null, $in = true, $bFromRepository = false)
	{
		$anInstance =  new BITAMTableFieldCollection($aRepository, $aTableName, $bFromRepository);
		$anInstance->getFieldCollectionData($aFieldNames, $in);
		return $anInstance;
	}

	static function NewInstanceEmpty($aRepository, $aTableName, $bFromRepository = false)
	{
		return new BITAMTableFieldCollection($aRepository, $aTableName, $bFromRepository);
	}

	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		$anInstance = null;
		$aTableName = '';
		$bFromRepository = '0';
		if (array_key_exists("TableName", $aHTTPRequest->GET))
		{
			$aTableName = $aHTTPRequest->GET["TableName"];
		}
		
		if (array_key_exists("FromRepository", $aHTTPRequest->GET))
		{
			$bFromRepository = $aHTTPRequest->GET["FromRepository"];
		}
		
		if ($aTableName != '')
		{
			//@JAPR 2015-02-10: Agregado soporte para php 5.6
			$anInstance = BITAMTableFieldCollection::NewInstanceFields($aRepository,$aTableName,null,null,$bFromRepository);
			if (is_null($anInstance))
			{
				$anInstance = BITAMTableFieldCollection::NewInstanceEmpty($aRepository, $aTableName, $bFromRepository);
			}
		}
		else
		{
			$anInstance = BITAMTableFieldCollection::NewInstanceEmpty($aRepository, $aTableName, $bFromRepository);
		}
		return $anInstance;
	}

	function get_Parent()
	{
		return $this->Repository;
	}

	function get_Title()
	{	
		$title = translate("Fields");
		
		return $title;
	}

	function getFieldCollectionData($aFieldNames = null, $in = true, $bFillExtraData = false)
	{
		if ($this->Table == '')
		{
			return;
		}
		
		if ($this->GetSchemaFromRepository)
		{
			//$metaColumns = $this->Repository->ADOConnection->MetaColumns($this->Table);
			$metaColumns=$this->Repository->GetTableSchema($this->Table,false);
		}
		else 
		{
			//$metaColumns = $this->Repository->DataADOConnection->MetaColumns($this->Table);
			$metaColumns=$this->Repository->GetTableSchema($this->Table,true);
		}
		
		$blnSortArray = false;
		if (!is_null($aFieldNames))
		{
			$strCode = '$blnIncludeField = '.(($in)?'' : '!').'array_key_exists($aFieldName, $aFieldNames);';
			//Prepara un arreglo para reordenar los elementos de la colección en base al arreglo del $in
			if ($in)
			{
				$blnSortArray = true;
				$arrayFieldsKeys = array_keys($aFieldNames);
				$arrayFieldsByKeys = array_flip($arrayFieldsKeys);
			}
		}
		
		foreach ($metaColumns as $aFieldName => $aFieldSchema)
		{
			$blnIncludeField = true;
			if (!is_null($aFieldNames))
			{
				eval($strCode);
			}
			
			if ($blnIncludeField)
			{
				if (get_class($this) == 'BITAMDimensionTableFieldCollection')
				{
					$aTableFieldInstance = new BITAMDimensionTableField($this->Repository, $this->consecutivo, $this->cla_concepto);
				}
				else
				{
					$aTableFieldInstance = new BITAMTableField($this->Repository, $this->GetSchemaFromRepository);
				}
				$aTableFieldInstance->Table = $this->Table;
				$aTableFieldInstance->Name = $aFieldSchema->name;
				$aTableFieldInstance->MetaType = $aFieldSchema->type;
				$aTableFieldInstance->MaxLength = $aFieldSchema->max_length;
				if ($bFillExtraData && get_class($aTableFieldInstance) == 'BITAMDimensionTableField')
				{
					$aTableFieldInstance->OrderID = (int)@$aFieldNames[$aFieldName][1];
					$aTableFieldInstance->Formula = (string)@$aFieldNames[$aFieldName][2];
					$aTableFieldInstance->FormulaBD = (string)@$aFieldNames[$aFieldName][3];
					$aTableFieldInstance->FormatMask = (string)@$aFieldNames[$aFieldName][4];
				}
				if ($blnSortArray)
				{
					$this->Collection[$arrayFieldsByKeys[$aFieldName]] = $aTableFieldInstance;
				}
				else
				{
					$this->Collection[] = $aTableFieldInstance;
				}
			}
		}
		
		//Si se ordenó según el query (es un $in con campos especificados) entonces reordenamos los índices de la colección
		//manteniendo los valores en la misma posición, ya que pudieron quedar espacios cuando se llenaron según el MetaColumns
		if ($blnSortArray)
		{
			//$this->Collection = array_values($this->Collection);
			ksort($this->Collection);
			reset($this->Collection);
		}
	}
	
	function get_QueryString()
	{
		return "BITAM_SECTION=TableFieldCollection&TableName=".$this->Table;
	}
	
	function get_FormIDFieldName()
	{
		return 'Table';
	}
	
	function get_FormFields($aUser)
	{
		$myFields = array();
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "Name";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 64;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "DataTypeName";
		$aField->Title = translate("Data Type");
		$aField->Type = "String";
		$aField->Size = 10;
		$myFields[$aField->Name] = $aField;
		
		return $myFields;
	}
	
	function canRemove($aUser)
	{
		return false;
	}
	 
	function canAdd($aUser)
	{
		return false;
	}
	
	function canEdit($aUser)
	{
		return false;
	}

	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function getFieldNames()
	{
		$arrFieldNames = array();
		for ($i=0; $i < count($this->Collection); $i++)
		{
			$arrFieldNames[] = $this->Collection[$i]->Name;
		}
		return $arrFieldNames;
	}
	
	// Indica si el campo permite especificar una longitud (solo los tipo string)
	function hasLength($aFieldName)
	{
		$blnHasLength = false;
		$aFieldName = strtoupper($aFieldName);
		foreach ($this->Collection as $aTableField)
		{
			if (strtoupper($aTableField->Name) == $aFieldName)
			{
				$blnHasLength = $aTableField->hasLength();
				break;
			}
		}
		
		return $blnHasLength;
	}
	
	// Retorna el elemento cuya propiedad tenga el valor especificado (desde la posición inicial, el primero que encuentre)
	function getItemByProperty($aPropertyValue, $sPropertyName = 'Name', $bCaseSensitive = false)
	{
		$anInstance = null;
		if (!$bCaseSensitive)
		{
			$aPropertyValue = strtoupper($aPropertyValue);
		}
		foreach ($this->Collection as $aKey => $anItem)
		{
			//if (property_exists($anItem, $sPropertyName))
			if (isset($anItem->$sPropertyName))
			{
				$anItemPropertyValue = $anItem->$sPropertyName;
				if (!$bCaseSensitive)
				{
					$anItemPropertyValue = strtoupper($anItemPropertyValue);
				}
				
				if ($anItemPropertyValue == $aPropertyValue)
				{
					$anInstance = $anItem;
					break;
				}
			}
		}
		
		return $anInstance;
	}
}

?>