<?php

require_once('checkCurrentSession.inc.php');
require_once('survey.inc.php');
//echo "<pre>";
//var_dump($_REQUEST);exit;

//Cargar TODAS las surveyIDs
$loadedSurveys = BITAMSurvey::getSurveys($theRepository, true);
if (!isset($_REQUEST['SurveyID']) || $_REQUEST['SurveyID'] == '')
{
	$surveyID = array();
}
else
	$surveyID = explode(',', $_REQUEST['SurveyID']);

$date_begin = (isset($_REQUEST['startDate']) && trim($_REQUEST['startDate']) != '')? date('Y-m-d', strtotime($_REQUEST['startDate'])).' 00:00:00': null;
$date_end = (isset($_REQUEST['endDate']) && trim($_REQUEST['endDate']) != '')? date('Y-m-d', strtotime($_REQUEST['endDate'])).' 23:59:59': null;
$userIDs = (isset($_REQUEST['userID']) && trim($_REQUEST['userID']) != '')? explode(',', $_REQUEST['userID']): array();

//GCRUZ 2015-08-27 Buscar en los botones two state de Dia/Semana/Mes/A�o
$dateTwoState = (isset($_REQUEST['dateTwoState']) && trim($_REQUEST['dateTwoState']) != '')? $_REQUEST['dateTwoState'] : '';
//GCRUZ 2015-08-27 Filtro de b�squeda de formas
$searchForm = (isset($_REQUEST['searchForm']) && trim($_REQUEST['searchForm']) != '')? $_REQUEST['searchForm'] : '';
switch($dateTwoState)
{
	case 'T':
		$date_begin = date('Y-m-d').' 00:00:00';
		$date_end = date('Y-m-d').' 23:59:59';
		break;
	case 'W':
		$weekDay = intval(date('w'));
		$date_begin = date('Y-m-d', strtotime('-'.$weekDay.' days')).' 00:00:00';
		$date_end = date('Y-m-d', strtotime('+'.(6-$weekDay).' days')).' 23:59:59';
		die('GMDataError: Invalid Parameters!');
		break;
	case 'M':
		$tDay = date('t');
		$date_begin = date('Y-m-').'01 00:00:00';
		$date_end = date('Y-m-').$tDay.' 23:59:59';
		die('GMDataError: Invalid Parameters!');
		break;
	case 'Y':
		$date_begin = date('Y-').'01-01 00:00:00';
		$date_end = date('Y-').'12-31 23:59:59';
		die('GMDataError: Invalid Parameters!');
		break;
}

if (date('Y-m-d', strtotime($date_begin)) != date('Y-m-d', strtotime($date_end)))
	die('GMDataError: Invalid Parameters!');

$groupIDs = (isset($_REQUEST['GroupID']) && trim($_REQUEST['GroupID']) != '')? explode(',', $_REQUEST['GroupID']): array();
if(count($groupIDs) > 0)
{
	$sql= "SELECT CLA_USUARIO FROM SI_ROL_USUARIO WHERE CLA_ROL IN (".implode(',', $groupIDs).")";
	$aRS = $theRepository->ADOConnection->Execute($sql);
	if ($aRS === false)
	{
		die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$theRepository->ADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
	}

	//Contiene las claves de los usuarios pertenecientes a los grupos seleccionados
	while (!$aRS->EOF)
	{
		$userIDs[] = (int) $aRS->fields["cla_usuario"];
		$aRS->MoveNext();
	}
	//GCRUZ 2015-09-30 Validar GroupID=0 (Ninguno)
	if (array_search('0', $groupIDs) !== false)
	{
		$aSQL = "SELECT GROUP_CONCAT(DISTINCT(CLA_USUARIO)) AS 'CLA_USUARIO' FROM SI_ROL_USUARIO";
		$rsUsersWithGroup = $theRepository->ADOConnection->Execute($aSQL);
		$strUsersWithGroup = '';
		if ($rsUsersWithGroup && $rsUsersWithGroup->_numOfRows != 0)
			$strUsersWithGroup = trim($rsUsersWithGroup->fields['CLA_USUARIO']);
		$strWhereUsersWithGroup = '';
		if ($strUsersWithGroup != '')
			$strWhereUsersWithGroup = ' CLA_USUARIO NOT IN ('.$strUsersWithGroup.') AND ';
		$aSQL = "SELECT DISTINCT(Email) AS 'Email' FROM SI_SV_Users WHERE ".$strWhereUsersWithGroup." CLA_USUARIO NOT IN (-1)";
		$rsNoGroup = $theRepository->DataADOConnection->Execute($aSQL);
		if ($rsNoGroup && $rsNoGroup->_numOfRows != 0)
		{
			while(!$rsNoGroup->EOF)
			{
				$userIDs[] = $rsNoGroup->fields['Email'];
				$rsNoGroup->MoveNext();
			}
		}
	}
}

if (strtolower($searchForm) != '')
{
	foreach($loadedSurveys as $skey => $asurvey)
	{
		if ($searchForm != '' && strpos(strtolower($asurvey), strtolower($searchForm)) === false)
			continue;
		$surveyID[] = $skey;
	}
}

if (count($userIDs) != 1)
	die('GMDataError: Invalid Parameters!');

$aSQLWhere = '';
if (count($surveyID) > 0)
	$aSQLWhere .= ' AND A.SurveyID IN ('.implode(',', $surveyID).') ';
if (count($userIDs) > 0)
	$aSQLWhere .= " AND A.UserID IN ('".implode("','", $userIDs)."') ";
$aSQL = "SELECT A.UserID, A.SurveyDate, B.SurveyName, A.Latitude, A.Longitude FROM SI_SV_SurveyLog A JOIN SI_SV_Survey B ON A.SurveyID = B.SurveyID WHERE A.DELETED = 0 AND A.SurveyDate BETWEEN '".$date_begin."' AND '".$date_end."'";
$aSQL .= $aSQLWhere;
$aSQL .= ' ORDER BY A.UserID, A.SurveyDate';
$arrBounds = array();
$arrRoutes = array();
$arrPos = array();
$rsEntries = $theRepository->DataADOConnection->Execute($aSQL);
if($rsEntries && $rsEntries->_numOfRows != 0)
{
	while(!$rsEntries->EOF)
	{
		//Capturas para mostrar marcadores
		$indexPos = array_search($rsEntries->fields['Latitude'].'_'.$rsEntries->fields['Longitude'], $arrPos);
		if ($indexPos === false)
		{
			$entry = array();
			$entry['latitude'] = $rsEntries->fields['Latitude'];
			$entry['longitude'] = $rsEntries->fields['Longitude'];
			$arrBounds[] = $entry;
			$arrPos[] = $rsEntries->fields['Latitude'].'_'.$rsEntries->fields['Longitude'];
		}
		//Capturas para mostrar rutas de usuario
		$userID = $rsEntries->fields['UserID'];
		$dateID = date('Y-m-d', strtotime($rsEntries->fields['SurveyDate']));
		if (!isset($arrRoutes[$userID][$dateID]))
		{
			$arrRoutes[$userID][$dateID] = array();
			$arrRoutes[$userID][$dateID]['color'] = '#'.dechex(mt_rand(0x000000, 0xFFFFFF));
			$arrRoutes[$userID][$dateID]['userid'] = $userID;
			$arrRoutes[$userID][$dateID]['surveydate'] = $dateID;
			$arrRoutes[$userID][$dateID]['bounds'] = array();
			$route = array();
			$route['surveydate'] = $rsEntries->fields['SurveyDate'];
			$route['latitude'] = $rsEntries->fields['Latitude'];
			$route['longitude'] = $rsEntries->fields['Longitude'];
			$route['title'] = $rsEntries->fields['SurveyDate'].PHP_EOL.$userID.PHP_EOL.$rsEntries->fields['SurveyName'];
			$route['info'] = $rsEntries->fields['SurveyDate']."<br>".$userID."<br>".$rsEntries->fields['SurveyName'];
			$route['label'] = count($arrRoutes[$userID][$dateID]['bounds'])+1;
			$arrRoutes[$userID][$dateID]['bounds'][] = $route;
		}
		else
		{
			$route = array();
			$route['surveydate'] = $rsEntries->fields['SurveyDate'];
			$route['latitude'] = $rsEntries->fields['Latitude'];
			$route['longitude'] = $rsEntries->fields['Longitude'];
			$route['title'] = $rsEntries->fields['SurveyDate'].PHP_EOL.$userID.PHP_EOL.$rsEntries->fields['SurveyName'];
			$route['info'] = $rsEntries->fields['SurveyDate']."<br>".$userID."<br>".$rsEntries->fields['SurveyName'];
			$route['label'] = count($arrRoutes[$userID][$dateID]['bounds'])+1;
			$arrRoutes[$userID][$dateID]['bounds'][] = $route;
		}
		$rsEntries->MoveNext();
	}
}

echo json_encode(array('users' => $arrRoutes, 'bounds' => $arrBounds));

?>