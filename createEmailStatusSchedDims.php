<?php
//@JAPR 2015-02-05: Unificado el inicio de todos los archivos que se invocan de forma independiente ya que casi todo era igual
require_once("checkCurrentSession.inc.php");
require_once("survey.inc.php");

//Cargar diccionario del lenguage a utilizar
if (array_key_exists("PAuserLanguage", $_SESSION))
{
	InitializeLocale($_SESSION["PAuserLanguageID"]);
	LoadLanguageWithName($_SESSION["PAuserLanguage"]);
}
else 
{	//English
	InitializeLocale(2);	
	LoadLanguageWithName("EN");
}

//Una referencia del objeto de conexion
$aRepository = $theRepository;

//Recorremos todas las encuestas
$surveyCollection = BITAMSurveyCollection::NewInstance($aRepository, null, false);

$strOriginalWD = getcwd();

//@JAPR 2015-02-09: Agregado soporte para php 5.6
//session_register("BITAM_UserID");
//session_register("BITAM_UserName");
//session_register("BITAM_RepositoryName");
$_SESSION["BITAM_UserID"] = $_SESSION["PABITAM_UserID"];
$_SESSION["BITAM_UserName"] = $_SESSION["PABITAM_UserName"];
$_SESSION["BITAM_RepositoryName"] = $_SESSION["PABITAM_RepositoryName"];

require_once("../model_manager/filescreator.inc.php");
require_once("../model_manager/modeldimension.inc.php");
require_once("../model_manager/modeldata.inc.php");

foreach ($surveyCollection->Collection as $surveyInstance) 
{
	//Crear dimension EMail
	if(is_null($surveyInstance->EmailDimID) || $surveyInstance->EmailDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, aunque no está registrada en la tabla del modelo global
		$dimensionName = "EMail";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_RAPID_WEB;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;

		//Verificamos si hay dimension EMail existente en caso contario
		//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
		
		if($cla_descrip!=-1)
		{
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			$aEMailDimID = $cla_descrip;
		}
		else 
		{
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Insertamos el valor de No Aplica en el catalogo de correo electronico
			BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
			$aEMailDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		
		$sql = "UPDATE SI_SV_Survey SET EMailDimID = ".$aEMailDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//Crear dimension Status
	if(is_null($surveyInstance->StatusDimID) || $surveyInstance->StatusDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, aunque no está registrada en la tabla del modelo global
		$dimensionName = "Status Survey";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_RAPID_WEB;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 0;

		//Verificamos si hay dimension User existente en caso contario
		//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
		
		if($cla_descrip!=-1)
		{
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			$aStatusDimID = $cla_descrip;
		}
		else 
		{
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Insertamos el valor de No Aplica en el catalogo de correo electronico
			BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
			
			//Insertamos los 3 valores de la dimension Status
			BITAMSurvey::insertStatusDimValues($aRepository, $anInstanceModelDim);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
			$aStatusDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		
		$sql = "UPDATE SI_SV_Survey SET StatusDimID = ".$aStatusDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
	
	//Crear dimension Scheduler
	if(is_null($surveyInstance->SchedulerDimID) || $surveyInstance->SchedulerDimID<=0)
	{
		//@JAPR 2015-05-06: Corregida la reutilización de dimensiones para que no provoque errores
		//Esta dimensión es global, aunque no está registrada en la tabla del modelo global
		$dimensionName = "Scheduler";
		$anInstanceModelDim = BITAMModelDimension::NewModelDimension($aRepository, $surveyInstance->ModelID);
		$anInstanceModelDim->Dimension->Estatus = 1;
		$anInstanceModelDim->Dimension->ParentID = -1;
		$anInstanceModelDim->Dimension->IsIncremental = 0;
		$anInstanceModelDim->Dimension->Created_By = CREATED_BY_RAPID_WEB;
		$anInstanceModelDim->Dimension->DimensionName = $dimensionName;
		$anInstanceModelDim->Dimension->UseKey = 1;

		//Verificamos si hay dimension EMail existente en caso contario
		//entonces no se inserta la dimension y se hace referencia al mismo cla_descrip
		$cla_descrip = BITAMSurvey::getDimClaDescrip($aRepository, $dimensionName);
		
		if($cla_descrip!=-1)
		{
			$anInstanceModelDim->Dimension->DimensionClaDescrip = $cla_descrip;
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Survey
			$aSchedulerDimID = $cla_descrip;
		}
		else 
		{
			$anInstanceModelDim->save();
			chdir($strOriginalWD);
			
			//Insertamos el valor de No Aplica en el catalogo de correo electronico
			BITAMSurvey::insertNewNoApplyValue($aRepository, $anInstanceModelDim);
			
			//Obtenemos el Id de la dimension creada y lo almacenamos en la tabla SI_SV_Question
			$aSchedulerDimID = $anInstanceModelDim->Dimension->DimensionClaDescrip;
		}
		
		$sql = "UPDATE SI_SV_Survey SET SchedulerDimID = ".$aSchedulerDimID." WHERE SurveyID = ".$surveyInstance->SurveyID;
		
		if ($aRepository->DataADOConnection->Execute($sql) === false)
		{
			die(translate("Error accessing")." SI_SV_Survey ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
	}
}
?>