<?php
require_once("question.inc.php");
require_once("object.trait.php");

class BITAMQuestionExt extends BITAMQuestion
{
	use BITAMObjectExt;
	function __construct($aRepository, $aSectionID)
	{
		parent::__construct($aRepository, $aSectionID);
		//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
		$this->HasSaveAndGoParent = false;
		$this->HasFormTitle = false;
		$this->HasHr = false;
		$this->HasPath = false;
		$this->HasCancel = false;
		$this->HasEdit = false;
		$this->Mode = parent::MODE_EDIT;
	}
	
	//@JAPR 2015-04-06: Rediseñado el Admin con DHTMLX
	function hideBackButton($aUser) {
		return true;
	}
	
	function hideRemoveButton($aUser)
	{
		return true;
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_QueryString()
	{
		if ($this->isNewObject())
		{
			return "BITAM_PAGE=QuestionExt&SectionID=".$this->SectionID;
		}
		else
		{
			return "BITAM_PAGE=QuestionExt&SectionID=".$this->SectionID."&QuestionID=".$this->QuestionID;
		}
	}
	
	function generateAfterFormCode($aUser)
	{
		//@JAPR 2015-04-30: Corregido un bug, al integrar el nuevo archivo se desactivaron las validaciones previas al grabado
 		parent::generateAfterFormCode($aUser);
		//@JAPR
		
 		$myFormName = get_class($this);
		
		//Si se trata de un objeto nuevo, el grabado se debe hacer hacia un archivo alternativo que procese todos estos requests
		if ($this->isNewObject()) {
?>
	 	<script language="JavaScript">
			<?=$myFormName?>_SaveForm.action = '<?='processRequest.php?Process=Add&ObjectType='.otyQuestion.'&'.$this->get_QueryString()?>&SelSurveyID=<?=$this->SurveyID?>';
		</script>
<?
		}
	}
}

class BITAMQuestionExtCollection extends BITAMQuestionCollection
{
	function get_AddRemoveQueryString()
	{
		return "BITAM_PAGE=QuestionExt&SectionID=".$this->SectionID."&SelSurveyID=".$this->SurveyID;
	}
}
?>