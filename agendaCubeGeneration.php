<?php

require_once('artusmodelsurvey.class.php');

function agendaCubeGeneration($theRepository, $theAppUser)
{
	$bAgendaCubeGeneration = true;
	$processTime =  getParamValue('Time', 'both', "(string)");

	require_once('eSurveyServiceMod.inc.php');
	$aBITAMConnection = connectToKPIRepository();

	$theArtusModelScheduler = ARTUSModelFromScheduler::NewInstance($theRepository);
	if ($theArtusModelScheduler)
		$theArtusModelScheduler->createIfNotExistsSchedulerModel();

	//Validar que exista el campo de SI_SV_GblSurveyModel.GblSurveySchedulerModelID
	$aSQL = "ALTER TABLE SI_SV_GblSurveyModel ADD GblSurveySchedulerModelID INTEGER NULL";
	$theRepository->DataADOConnection->Execute($aSQL);
	if ($theArtusModelScheduler->cla_concepto > 0)
	{
		$aSQL = "UPDATE SI_SV_GblSurveyModel SET GblSurveySchedulerModelID = ".$theArtusModelScheduler->cla_concepto;
		$theRepository->DataADOConnection->Execute($aSQL);

		if ($processTime == 'AM')
			$fecha_ini = date('Y-m-d', strtotime('-1 days'));
		elseif ($processTime == 'PM')
			$fecha_ini = date('Y-m-d');
		else
			$fecha_ini = date('Y-m-d');
		$fecha_fin = date('Y-m-d');
		agendaCubeGenerationLogString("Running runSchedulerProc('{$fecha_ini}','{$fecha_fin}')");
		$error_msg = '';
		$theArtusModelScheduler->runSchedulerProc($fecha_ini, $fecha_fin, $error_msg);
		if ($error_msg != '')
		{
			agendaCubeGenerationLogString("Failed runSchedulerProc...".PHP_EOL.$error_msg);
			$bAgendaCubeGeneration = false;
		}
		agendaCubeGenerationLogString('Finished runSchedulerProc');
	}
	else
	{
		agendaCubeGenerationLogString('Could not find Scheduler Model');
		$bAgendaCubeGeneration = false;
	}


	if ($bAgendaCubeGeneration)
	{
		$aResponse = array();
		$aResponse['error'] = false;
		$aResponse['errmsg'] = "AgendaCubeGeneration:Success";
	}
	else
	{
		$aResponse = array();
		$aResponse['error'] = true;
		$aResponse['errmsg'] = "AgendaCubeGeneration:Failed";
	}
	return $aResponse;

}

function agendaCubeGenerationLogString($msg)
{
	//Si no hay nombre de archivo entonces utiliza el default. Si hay nombre pero no contiene un path entonces usa el path default
	//@JAPR 2019-09-12: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
	//Integradas las funciones comunes para almacenamiento de log de eForms
	$sFilePath = GeteFormsLogPath();
	error_log(date('Y-m-d H:i:s').' :: '.$msg.PHP_EOL, 3, $sFilePath.'agendaCubeGeneration.log');
	//@JAPR
}

?>