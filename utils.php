<?php
function get_select_options ($option_list, $selected) 
{
	return get_select_options_with_id($option_list, $selected);
}


function get_select_options_with_id ($option_list, $selected_key) 
{
	return get_select_options_with_id_separate_key($option_list, $option_list, $selected_key);
}

function get_select_options_with_id_separate_key ($label_list, $key_list, $selected_key) 
{
	$select_options = "";
	//for setting null selection values to human readable --None--
	$pattern        = "/'0?'></";
	$replacement    = "''>-- None --<";

	//create the type dropdown domain and set the selected value if $opp value already exists
	foreach ($key_list as $option_key=>$option_value) 
	{
		$selected_string = '';
		// the system is evaluating $selected_key == 0 || '' to true.  Be very careful when changing this.  Test all cases.
		// The bug was only happening with one of the users in the drop down.  It was being replaced by none.
		if (($option_key != '' && $selected_key == $option_key) || ($selected_key == '' && $option_key == '') || (is_array($selected_key) &&  in_array($option_key, $selected_key)))
		{
			$selected_string = 'selected ';
		}

		$html_value      = $option_key;
		$select_options .= "\n<OPTION ".$selected_string."value='$html_value'>$label_list[$option_key]</OPTION>";
	}
	$select_options = preg_replace($pattern, $replacement, $select_options);

	return $select_options;
}
?>