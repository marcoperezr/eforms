<?php

require_once("repository.inc.php");
require_once("initialize.php");
require_once("appuser.inc.php");
require_once("survey.inc.php");
require_once("artusreporter.inc.php");
require_once("paginator.inc.php");
require_once("catalog.inc.php");
//@JAPR 2015-08-27: Rediseñadas las agendas en v6
require_once("datasource.inc.php");
require_once("eBavelCatalogValue.inc.php");
require_once("agendas.inc.php");

$agStartDate=null;
$agEndDate=null;
$agUserID=null;
$agIpp=null;
$agCatalogID=null;

class BITAMSurveyAgenda extends BITAMObject
{
	public $AgendaID;
	public $AgendaName;					//Opcional, si se captura se utilizará este nombre, de lo contrario el nombre será la concatenación de los nombres de los elementos seleccionados en otras columnas
	public $CatalogID;					//Catálogo del que se obtendrá el valor al filtrar, las encuestas sólo pueden aplicarse si utilizan este catálogo, ya que al día de hoy en las aplicaciones móviles solo puede haber preguntas de catálogo si la encuesta está asociada al mismo catálogo 
	public $CatalogDim;					//No es un campo, se obtiene mediante Join
	public $CatalogName;				//No es un campo, se obtiene mediante Join
	public $CheckPointName;				//No es un campo, se obtiene mediante Join
	public $CheckPointID;				//No es un campo, se obtiene mediante Join
	public $CatalogTable;				//No es un campo, se obtiene mediante Join
	public $AttributeID;				//Atributo del catálogo por el cual se va a filtrar, las encuestas sólo pueden aplicarse si contienen alguna pregunta que utilice a este catálogo (por consiguiente, la propia encuesta está asociada a este catálogo)
	public $MemberName;					//No es un campo, se obtiene mediante Join
	public $FilterText;					//El valor exacto del atributo que se utilizará como filtro al aplicar la encuesta (sólo puede ser uno por ahora)
	public $FilterTextAttr;				//Guarda el orden de los atributos
	//public $SurveyID;					//La encuesta para la cual aplica la agenda (depende del catálogo y del atributo específicos seleccionados)
	//public $SurveyName;					//No es un campo, se obtiene mediante Join
	public $CaptureStartDate;			//Fecha de inicio de captura, las agendas serán mostradas cuando su fecha sea <= que la actual de la aplicación (DateTime)
	public $CaptureStartTime;			//Hora de inicio de captura en formato hh:mm (Varchar)
	public $CaptureStartTime_Hours;		//Requerido por el componente Time para el campo CaptureStartTime
	public $CaptureStartTime_Minutes;	//Requerido por el componente Time para el campo CaptureStartTime
	public $UserID;						//El usuario al que se configuró la agenda. Por ahora son personalizadas, así que no se pueden aplicar a grupos de usuarios
	public $UserName;					//No es un campo, se obtiene mediante un subquery
	public $SurveyIDs;					//Lista de encuestas para la cual aplica la agenda (depende del catálogo y del atributo específicos seleccionados)
	public $surveyIDsStatus;			//Array indexado por ID de encuesta con los Status que tenían al momento de ser cargadas desde la base de datos (no cambian con el POST, pero se debe validar para que respeten su Status previo si aun sigue seleccionada la encuesta)
	public $LastDateID;					//Fecha de última modificación de la Agenda
	//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
	public $Status;						//SI_SV_SurveyAgendaDet. El Status general de la agenda. Si hay una encuesta recalendariza entonces ese es el Status (agdRescheduled), si hay alguna encuesta aun no capturada entonces ese es el Status (agdOpen), sólo si todas las encuestas están capturadas se considera terminada (agdAnswered)
	public $CaptureStartDateOld;		//Fecha de inicio de captura, las agendas serán mostradas cuando su fecha sea <= que la actual de la aplicación (DateTime)
	public $CaptureStartTime_HoursOld;	//Requerido por el componente Time para el campo CaptureStartTime
	public $CaptureStartTime_MinutesOld;//Requerido por el componente Time para el campo CaptureStartTime
	//@JAPR 2012-08-08: Agregada la versión de definición de las agendas
	public $VersionNum;
	//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda (a la fecha de impementación sólo podía haber una encuesta de CheckIn)
	//Esta fecha y hora se podrían extraer directamente de la tabla paralela, sin embargo sería muy ineficiente ya que eso es un join con tablas grandes y que pueden variar por agenda (ya que no siempre es la misma encuesta de CheckIn), así que se dejará directo en la agenda
	public $EntryCheckInStartDate;		//Fecha y Hora de la captura de la encuesta de CheckIn para la agenda (corresponde con SVSurvey_####.DateID de la encuesta de CheckIn donde Agenda ID == a esta Agenda
	public $EntryCheckInStartDate_Hours;	//Requerido por el componente Time para el campo EntryCheckInStartDate
	public $EntryCheckInStartDate_Minutes;	//Requerido por el componente Time para el campo EntryCheckInStartDate
	//@JAPR
	
	public $CheckinSurvey;
	public $SurveysRadioForAgenda;
    
    public $GenerationDate;             //jcem s eagrega para generar agendas del dia siguiente
    public $AgendaSchedID;              //jcem: se agrega para guardar el id del agendasched que lo genero
	//@JAPR 2015-08-27: Rediseñadas las agendas en v6
	public $CreationAdminVersion;		//Versión del código del Administrador de eForms que creó este objeto
	public $LastModAdminVersion;		//Versión del último código del Administrador de eForms que realizó el grabado de este objeto
    
    function __construct($aRepository)
	{
		BITAMObject::__construct($aRepository);
		$this->AgendaID = -1;
		$this->AgendaName = '';
		$this->CatalogID = 0;
		$this->CatalogDim = 0;
		$this->CatalogName = '';
		$this->CatalogTable = '';
		$this->AttributeID = 0;
		$this->MemberName = '';
		$this->FilterText = '';
		$this->FilterTextAttr = '';
		//$this->SurveyID = -1;
		//$this->SurveyName = '';
		$this->CaptureStartDate = "";
		$this->CaptureStartTime = array(0,0);
		$this->CaptureStartTime_Hours = 0;
		$this->CaptureStartTime_Minutes = 0;
		$this->UserID = $_SESSION["PABITAM_UserID"];
		$this->UserName = '';
		$this->SurveyIDs = array();
		$this->surveyIDsStatus = array();
		$this->SurveysRadioForAgenda = array();
		$this->LastDateID = '';
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		$this->Status = agdOpen;
		$this->CaptureStartDateOld = "";
		$this->CaptureStartTime_HoursOld = 0;
		$this->CaptureStartTime_MinutesOld = 0;
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas
		$this->VersionNum = 0;
		//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		$this->EntryCheckInStartDate = '';
		$this->CheckinSurvey = 0;
        $this->GenerationDate = null;
        $this->AgendaSchedID = null;
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$this->CreationAdminVersion = 0;
		$this->LastModAdminVersion = 0;
		
		$this->CheckPointID = 0;
	}

	static function NewInstance($aRepository)
	{
		return new BITAMSurveyAgenda($aRepository);
	}

	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	static function NewInstanceWithID($aRepository, $anAgendaID, $bJustPending = false)
	{
		$anInstance = null;
		
		if (((int)  $anAgendaID) < 0)
		{
			return $anInstance;
		}
		//Conchita 10-nov-2011 Agregado EmailReplyTo
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$strFieldsByVersion = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strFieldsByVersion .= ', t1.VersionNum ';
		}
		if(getMDVersion() >= esvAgendaRedesign) {
			$strFieldsByVersion .= ', t1.CheckinSurvey ';
		}
		//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		if(getMDVersion() >= esvAgendaRedesign) {
			$strFieldsByVersion .= ', t1.EntryCheckInStartDate, t1.Status ';
		}
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		if(getMDVersion() >= esveAgendas) {
			$strFieldsByVersion .= ', t1.CreationVersion, t1.LastVersion';
		}
		//@JAPR
		$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, t2.DataSourceName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, t1.FilterTextAttr, 
					t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID, t1.checkpointID, t5.name as CheckPointName $strFieldsByVersion 
				FROM SI_SV_SurveyAgenda t1 
					LEFT OUTER JOIN si_sv_datasource t2 ON t1.CatalogID = t2.DataSourceID 
					LEFT OUTER JOIN si_sv_datasourcemember t3 ON t1.CatalogID = t3.DataSourceID AND t1.AttributeID = t3.MemberID 
					LEFT OUTER JOIN si_sv_checkpoint t5 ON t1.checkpointID = t5.id
				WHERE t1.AgendaID = ".((int)$anAgendaID);
					//LEFT OUTER JOIN SI_SV_Survey t4 ON t1.SurveyID = t4.SurveyID 		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		if (!$aRS->EOF)
		{
			$anInstance = BITAMSurveyAgenda::NewInstanceFromRS($aRepository, $aRS, $bJustPending);
			//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
			//Ahora el estado de la agenda se captura directo en ella misma, ya no depende del estado de cada encuesta
			if(getMDVersion() < esvAgendaRedesign) {
				$sql = "SELECT A.AgendaID, A.Status, B.EnableReschedule 
					FROM SI_SV_SurveyAgendaDet A, SI_SV_Survey B 
					WHERE A.SurveyID = B.SurveyID AND A.AgendaID = ".((int)$anAgendaID)." 
					GROUP BY A.AgendaID";
				$aRS = $aRepository->DataADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				
				while (!$aRS->EOF)
				{
					$intStatus = (int) $aRS->fields['status'];
					switch ($intStatus)
					{
						case agdRescheduled:
							//Si hay por lo menos una encuesta recalendarizada, se considera toda la agenda como recalendarizada
							$anInstance->Status = $intStatus;
							break 2;
					
						case agdOpen:
							//Si hay por lo menos una encuesta sin contestar y que NO esté configurada para recalendarizar dicha encuesta, se considera 
							//toda la agenda como pendiente de contestar
							if (((int) $aRS->fields['enablereschedule']) == 0)
							{
								$anInstance->Status = $intStatus;
								break 2;
							}
							//En este caso es una encuesta no contestada pero que permite reagendar, así que esa no cuenta
							break;
						
						default:
							break 2;
					}
					$aRS->MoveNext();
				}
				
				//Si llega hasta aquí es porque todas las encuestas de la agenda han sido contestadas, o bien sólo quedan por contestar las que están
				//configuradas como Encuestas de Status, así que en realidad ya no hay nada pendiente por capturar (el Status default ya es agdAnswered)
			}
			//@JAPR
		}
		return $anInstance;
	}

	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	static function NewInstanceFromRS($aRepository, $aRS, $bJustPending = false)
	{
		$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
		$anInstance->AgendaID = (int) $aRS->fields["agendaid"];
		$anInstance->AgendaName = (string) $aRS->fields["agendaname"];
		$anInstance->CatalogID = (int) $aRS->fields["catalogid"];
		$anInstance->CatalogDim = (int) @$aRS->fields["parentid"];
		$anInstance->CatalogTable = (string) $aRS->fields["tablename"];
		$anInstance->CatalogName = (string) $aRS->fields["datasourcename"];
		$anInstance->CheckPointName = (string) $aRS->fields["checkpointname"];
		$anInstance->CheckPointID = (string) $aRS->fields["checkpointid"];
		$anInstance->AttributeID = (int) $aRS->fields["attributeid"];
		$anInstance->MemberName = (string) $aRS->fields["membername"];
		$anInstance->FilterText = (string) $aRS->fields["filtertext"];
		$anInstance->FilterTextAttr = (string) $aRS->fields["filtertextattr"];
		//$anInstance->SurveyID = (int) $aRS->fields["surveyid"];
		//$anInstance->SurveyName = (string) $aRS->fields["surveyname"];
		if(!is_null($aRS->fields["capturestartdate"]) && trim($aRS->fields["capturestartdate"])!="")
		{
			$anInstance->CaptureStartDate = substr($aRS->fields["capturestartdate"], 0, 10);
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$anInstance->CaptureStartDateOld = $anInstance->CaptureStartDate;
		}
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$anInstance->VersionNum = (int) @$aRS->fields["versionnum"];
		//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		//@AAL 15/05/2015 Corregido Issue #0AWCEA: No estaba mostrando la hora en que se entra a capturar
		$anInstance->EntryCheckInStartDate = (string) @$aRS->fields["entrycheckinstartdate"];
		$arrDate = privateUnformatDateTime($anInstance->EntryCheckInStartDate, '');
		$intDateVal = (float) @implode('', $arrDate);
		$anInstance->EntryCheckInStartDate_Hours = 0;
		$anInstance->EntryCheckInStartDate_Minutes = 0;
		if ($intDateVal) {
			$anInstance->EntryCheckInStartDate_Hours = (int) @$arrDate[3];
			$anInstance->EntryCheckInStartDate_Minutes = (int) @$arrDate[4];
		}
		//JAPR
		$strCaptureStartTime = trim($aRS->fields["capturestarttime"]);
		$anInstance->CaptureStartTime_Hours = 0;
		$anInstance->CaptureStartTime_Minutes = 0;
		if ($strCaptureStartTime != '')
		{
			$arrHour = explode(':', $strCaptureStartTime);
			if (count($arrHour) == 2)
			{
				if (is_numeric($arrHour[0]))
				{
					$anInstance->CaptureStartTime[0] = $arrHour[0];
					$anInstance->CaptureStartTime_Hours =(int) $arrHour[0];
				}
				if (is_numeric($arrHour[1]))
				{
					$anInstance->CaptureStartTime[1] = $arrHour[1];
					$anInstance->CaptureStartTime_Minutes =(int) $arrHour[1];
				}
			}
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$anInstance->CaptureStartTime_HoursOld = $anInstance->CaptureStartTime_Hours;
			$anInstance->CaptureStartTime_MinutesOld = $anInstance->CaptureStartTime_Minutes;
		}
		
		$anInstance->UserID = (int) $aRS->fields["userid"];
		if ($anInstance->UserID <= 0)
		{
			$anInstance->UserID = $_SESSION["PABITAM_UserID"];
		}
		$anInstance->LastDateID = (string) $aRS->fields["lastdateid"];
		
		$anInstance->readSurveyIDs($bJustPending);
		
		if (getMDVersion() >= esvAgendaRedesign) {
			$anInstance->CheckinSurvey = (int) @$aRS->fields["checkinsurvey"];
		}
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		$anInstance->Status = (int) @$aRS->fields["status"];
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		$anInstance->CreationAdminVersion = (float) @$aRS->fields["creationversion"];
		$anInstance->LastModAdminVersion = (float) @$aRS->fields["lastversion"];
		//@JAPR
		
		return $anInstance;
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		global $agStartDate;
		if(array_key_exists("startDate", $aHTTPRequest->GET))
		{
			$agStartDate = $aHTTPRequest->GET["startDate"];
		}

		global $agEndDate;
		if(array_key_exists("endDate", $aHTTPRequest->GET))
		{
			$agEndDate = $aHTTPRequest->GET["endDate"];
		}

		global $agUserID;
		if(array_key_exists("userID", $aHTTPRequest->GET))
		{
			$agUserID = $aHTTPRequest->GET["userID"];
		}
		
		global $agIpp;
		if(array_key_exists("ipp", $aHTTPRequest->GET))
		{
			$agIpp = $aHTTPRequest->GET["ipp"];
		}
		
		global $agCatalogID;
		if(array_key_exists("catalogID", $aHTTPRequest->GET))
		{
			$agCatalogID = $aHTTPRequest->GET["catalogID"];
		}
		
		if (array_key_exists("AgendaID", $aHTTPRequest->POST))
		{
			$anAgendaID = $aHTTPRequest->POST["AgendaID"];
			
			if (is_array($anAgendaID))
			{
				if(array_key_exists("startDate", $aHTTPRequest->POST))
				{
					$agStartDate = $aHTTPRequest->POST["startDate"];
				}
		
				if(array_key_exists("endDate", $aHTTPRequest->POST))
				{
					$agEndDate = $aHTTPRequest->POST["endDate"];
				}
		
				if(array_key_exists("userID", $aHTTPRequest->POST))
				{
					$agUserID = $aHTTPRequest->POST["userID"];
				}
				
				if(array_key_exists("ipp", $aHTTPRequest->POST))
				{
					$agIpp = $aHTTPRequest->POST["ipp"];
				}
				
				if(array_key_exists("catalogID", $aHTTPRequest->POST))
				{
					$agCatalogID = $aHTTPRequest->POST["catalogID"];
				}
				
				$aCollection = BITAMSurveyAgendaCollection::NewInstance($aRepository, $anAgendaID, $agUserID, $agEndDate, null, false, $agStartDate, $agCatalogID, $agIpp, false);
				
				foreach ($aCollection as $anInstanceToRemove)
				{
					$anInstanceToRemove->remove();
				}
				$aHTTPRequest->RedirectTo = $aCollection;
			}
			else
			{
				$anInstance = BITAMSurveyAgenda::NewInstanceWithID($aRepository, (int)$anAgendaID);
				if (is_null($anInstance))
				{
					$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
				}
				$anInstance->updateFromArray($aHTTPRequest->GET);
				$anInstance->updateFromArray($aHTTPRequest->POST);
				$anInstance->save();
				if (array_key_exists("go", $_POST))
				{
					$go = $_POST["go"];
				}
				else
				{
					$go = "self";
				}
				if ($go == "parent")
				{
					$anInstance = $anInstance->get_Parent();
				}
				else
				{
					if ($go == "new")
					{
						$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
					}
				}
				$aHTTPRequest->RedirectTo = $anInstance;
			}
			return null;
		}
		$anInstance = null;
		if (array_key_exists("AgendaID", $aHTTPRequest->GET))
		{
			$anAgendaID = $aHTTPRequest->GET["AgendaID"];

			$anInstance = BITAMSurveyAgenda::NewInstanceWithID($aRepository, (int)$anAgendaID);

			if (is_null($anInstance))
			{
				$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
			}
		}
		else
		{
			$anInstance = BITAMSurveyAgenda::NewInstance($aRepository);
		}
		return $anInstance;
	}

	function updateFromArray($anArray)
	{
		if (array_key_exists("AgendaID", $anArray))
		{
			$this->AgendaID = (int)$anArray["AgendaID"];
		}
		
		if (array_key_exists("AgendaName", $anArray))
		{
			$this->AgendaName = (string)$anArray["AgendaName"];
		}
		
	 	if (array_key_exists("CatalogID", $anArray))
		{
			$this->CatalogID = rtrim($anArray["CatalogID"]);
			//$aCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->catalogID);
			$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, $this->CatalogID, true);
			$attributeIDs = $tmpCatalogCollection[$this->CatalogID];
			$hasAttributes = false;
			$arrAttributeValues = array();
			
			foreach ($attributeIDs as $ckey => $attributeID) {
				//if (array_key_exists(("DSC_".$ckey), $anArray)) {
				if (array_key_exists(("DSC_".$attributeID), $anArray)) {
					if (!$hasAttributes) {
						$hasAttributes = true;
					}
					//$arrAttributeValues[$ckey] = $anArray[("DSC_".$ckey)];
					$arrAttributeValues[$ckey] = $anArray[("DSC_".$attributeID)];
				}
			}
			if ($hasAttributes) {
				$strAttributeValues = implode($arrAttributeValues, "|");
				$this->FilterText = $strAttributeValues;
			}
		}

		if (array_key_exists("CheckPointID", $anArray))
		{
			$this->CheckPointID = rtrim($anArray["CheckPointID"]);

			$anCheckpoint = BITAMCheckpoint::NewInstanceWithId( $this->Repository, $this->CheckPointID );

			$this->CatalogID = $anCheckpoint->catalog_id;

			$tmpCatalogCollection = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $this->CatalogID);

			$attributeIDs = $tmpCatalogCollection->Collection;
			$hasAttributes = false;
			$arrAttributeValues = array();
			$arrPosAttributeValues = array();
			
			foreach ($attributeIDs as $eachAttribute) {
				if (array_key_exists(("DSC_".$eachAttribute->MemberID), $anArray)) {
					if (!$hasAttributes) {
						$hasAttributes = true;
					}
					$arrAttributeValues[$eachAttribute->MemberID] = $anArray[("DSC_".$eachAttribute->MemberID)];
					$arrPosAttributeValues[$eachAttribute->MemberID] = $eachAttribute->MemberID;
				}
			}

			if ($hasAttributes) {
				$this->FilterText = implode($arrAttributeValues, "|");
				$this->FilterTextAttr = implode($arrPosAttributeValues, "|");
			}
		}
		
		if (array_key_exists("AttributeID", $anArray))
		{
			$this->AttributeID = (int)$anArray["AttributeID"];
		}
		
		if (array_key_exists("SurveyIDs", $anArray))
		{
			$this->SurveyIDs = $anArray["SurveyIDs"];
		}
		else 
		{
			if (array_key_exists("SurveyIDs_EmptySelection", $anArray))
			{
				if ((bool) $anArray["SurveyIDs_EmptySelection"])
				{
					$this->SurveyIDs = array();
				}
			}
		}
		
		/*if (array_key_exists("FilterText", $anArray))
		{
			$this->FilterText = (string)$anArray["FilterText"];
		}*/
		
		if (array_key_exists("CaptureStartDate", $anArray))
		{
			$this->CaptureStartDate = (string)$anArray["CaptureStartDate"];
		}
		
		if (array_key_exists("CaptureStartTime_Hours", $anArray))
		{
			$this->CaptureStartTime[0] = (int)$anArray["CaptureStartTime_Hours"];
			$this->CaptureStartTime_Hours = $this->CaptureStartTime[0];
		}
		if (array_key_exists("CaptureStartTime_Minutes", $anArray))
		{
			$this->CaptureStartTime[1] = (int)$anArray["CaptureStartTime_Minutes"];
			$this->CaptureStartTime_Minutes = $this->CaptureStartTime[1];
		}
		
		if (array_key_exists("UserID", $anArray))
		{
			$this->UserID = (int)$anArray["UserID"];
		}
		
		if (array_key_exists("CheckinSurvey", $anArray))
		{
			$this->CheckinSurvey = (int)$anArray["CheckinSurvey"];
		}

		/** Actualizamos en la instancia el nombre de usuario */
        $rs = $this->Repository->ADOConnection->Execute( "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO = " . $this->UserID );

        if( $rs && !$rs->EOF )
        {
        	$this->UserName = $rs->fields['NOM_LARGO'];
        }

        /** Actualizamos en la instancia el nombre del punto de visita */
        $rs = $this->Repository->DataADOConnection->Execute( "SELECT `name` FROM si_sv_checkpoint WHERE id = " . $this->CheckPointID );

        if( $rs && !$rs->EOF )
        {
        	$this->CheckPointName = $rs->fields['name'];
        }
		
		return $this;
	}
	
	function save()
	{
        //para compatibilidad con generacion de agendas dia siguiente
        $currentDate = (is_null($this->GenerationDate))?date("Y-m-d H:i:s"):$this->GenerationDate;
                
		$strOriginalWD = getcwd();
        
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		//$this->LastModAdminVersion = ESURVEY_SERVICE_VERSION;
		$this->LastModAdminVersion = esveAgendas;
		//@JAPR
        $isnewobject = $this->isNewObject();

	 	if ($isnewobject) {
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			$this->CreationAdminVersion = $this->LastModAdminVersion;
			//@JAPR
			$sql =  "SELECT ".
						$this->Repository->DataADOConnection->IfNull("MAX(AgendaID)", "0")." + 1 AS AgendaID".
						" FROM SI_SV_SurveyAgenda";
			$aRS = $this->Repository->DataADOConnection->Execute($sql);
			if (!$aRS || $aRS->EOF)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$this->AgendaID = (int)$aRS->fields["AgendaID"];
			//Conchita 10-nov-2011 Agregado EmailReplyTo
			//@JAPR 2012-05-30: Corregido un bug, la inserción de la hora estaba mal, había un parámetro extra en el Quote de ese campo
			//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
			$strFieldsByVersion = "";
			$strValuesByVersion = "";
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strFieldsByVersion .= ',VersionNum';
				$strValuesByVersion .= ',1';
			}
			//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
			if (getMDVersion() >= esvAgendaRedesign) {
				$strFieldsByVersion .= ',CheckinSurvey, Status';
				$strValuesByVersion .= ', '.$this->CheckinSurvey.', '.$this->Status;
			}
            
            //2014.12.18 JCEM al generarse desde agendascheduler se guarda tambien el agendashedid
            if (getMDVersion() >= esvAgendaScheduler) {
                $strFieldsByVersion .= ',AgendaSchedID';
                $strValuesByVersion .= ', '.((is_null($this->AgendaSchedID))?'NULL': $this->AgendaSchedID);
            }
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
 			if (getMDVersion() >= esveAgendas) {
				$strFieldsByVersion .= ', CreationVersion, LastVersion, checkpointID';
				$strValuesByVersion .= ', '.$this->LastModAdminVersion.', '.$this->LastModAdminVersion.', '.$this->CheckPointID;
			}
			//@JAPR
			$sql = "INSERT INTO SI_SV_SurveyAgenda (".
						" AgendaID".
						", AgendaName".
			            ", CreationUserID".
			            ", CreationDateID".
			            ", LastUserID".
			            ", LastDateID".
			            //", SurveyID".
			            ", CatalogID".
			            ", AttributeID".
						", UserID".
						", CaptureStartDate".
						", CaptureStartTime".
						", FilterText".
						", FilterTextAttr".
						$strFieldsByVersion.
			            ") VALUES (".
			            $this->AgendaID.
			            ",".$this->Repository->DataADOConnection->Quote($this->AgendaName).
						",".$_SESSION["PABITAM_UserID"].
						",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
						",".$_SESSION["PABITAM_UserID"].
						",".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
			            //",".$this->SurveyID.
						",".$this->CatalogID.
						",".$this->AttributeID.
						",".$this->UserID.
						",".(($this->CaptureStartDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureStartDate):"NULL").
						",".$this->Repository->DataADOConnection->Quote($this->getAgendaTime()).
			            ",".$this->Repository->DataADOConnection->Quote($this->FilterText).
			            ",".$this->Repository->DataADOConnection->Quote($this->FilterTextAttr).
			            $strValuesByVersion.
					")";
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        else 
        {
			//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
        	$this->VersionNum++;
			$strValuesByVersion = "";
			if (getAppVersion() >= esvMultiDynamicAndStandarization) {
				$strValuesByVersion .= ', VersionNum = '.$this->VersionNum;
			}
			//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
			if (getMDVersion() >= esvAgendaRedesign) {
				$strValuesByVersion .= ', CheckinSurvey = '.$this->CheckinSurvey.', Status = '.$this->Status;
			}
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			if (getMDVersion() >= esveAgendas) {
				$strValuesByVersion .= ', LastVersion = '.$this->LastModAdminVersion;
			}
			//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
            $sql = "UPDATE SI_SV_SurveyAgenda SET ".
            		" AgendaName = ".$this->Repository->DataADOConnection->Quote($this->AgendaName).
					", LastUserID = ".$_SESSION["PABITAM_UserID"].
					", LastDateID = ".$this->Repository->DataADOConnection->DBTimeStamp($currentDate).
					", FilterText = ".$this->Repository->DataADOConnection->Quote($this->FilterText).
					", FilterTextAttr = ".$this->Repository->DataADOConnection->Quote($this->FilterTextAttr).
					", CaptureStartDate = ".(($this->CaptureStartDate!="")?$this->Repository->DataADOConnection->DBTimeStamp($this->CaptureStartDate):"NULL").
					", CaptureStartTime = ".$this->Repository->DataADOConnection->Quote($this->getAgendaTime()).
					$strValuesByVersion.
				" WHERE AgendaID = ".$this->AgendaID;
			if ($this->Repository->DataADOConnection->Execute($sql) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
        }
        
        $this->LastDateID = $currentDate;
        $this->UpdateSurveyIDs();

		//GCRUZ 2016-06-02. Agregados campos nuevos de Latitude, Longitude
		if (getMDVersion() >= esvAgendaDetNewModelFields) {
			$this->updateAgendaLatLng();
		}
        
        chdir($strOriginalWD);
		
		return $this;
	}
    
	function remove()
	{
		$sql = "DELETE FROM SI_SV_SurveyAgendaDet WHERE AgendaID = ".$this->AgendaID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$sql = "DELETE FROM SI_SV_SurveyAgenda WHERE AgendaID = ".$this->AgendaID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		return $this;
	}
	
	//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
	//Regresa el icono de Recalendarizada si la encuesta se encuentra en ese estado (@JAPRWarngin: eventualmente puede regresar un icono de Status
	//para cada agenda)
	function RescheduleStatus()
	{
		$htmlString = '';
		switch ($this->Status)
		{
			case agdRescheduled:
				$htmlString="<img src=images/agendaRescheduled.png title='".translate("Rescheduled")."'>";
				break;
			
			default:
				break;
		}
		
		return $htmlString;
	}
	
	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	function readSurveyIDs($bJustPending = false)
	{
		$this->UserIDs = array();
		$sql = "SELECT SurveyID, Status FROM SI_SV_SurveyAgendaDet WHERE AgendaID = ".$this->AgendaID;
		if ($bJustPending)
		{
			$sql .= " AND (Status IS NULL OR Status = ".agdOpen.")";
		}
		
		$aRS = $this->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while(!$aRS->EOF)
		{
			$aSurveyID = (int)$aRS->fields["surveyid"];
			$this->SurveyIDs[] = $aSurveyID;
			$this->surveyIDsStatus[$aSurveyID] = (int)@$aRS->fields["status"];
			if (getMDVersion() >= esvAgendaRedesign) {
				$tmpSurvey = BITAMSurvey::NewInstanceWithID($this->Repository, $aSurveyID);
				//@JAPR 2015-01-27: Corregido un bug, estaba mal validado el uso de la Forma de las agendas
				if (!is_null($tmpSurvey)) {
					$this->SurveysRadioForAgenda[$aSurveyID] = $tmpSurvey->RadioForAgenda;
				}
				//@JAPR
			}
			$aRS->MoveNext();
		}
	}
	
	function UpdateSurveyIDs()
	{
		//Elimino los registros anteriores y se vuelven a insertar nuevamente:
		$sql = "DELETE FROM SI_SV_SurveyAgendaDet WHERE AgendaID = ".$this->AgendaID;
		if ($this->Repository->DataADOConnection->Execute($sql) === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		foreach ($this->SurveyIDs as $aSurveyID)
		{
			$intStatus = (int) @$this->surveyIDsStatus[$aSurveyID];
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			//Si la agenda tenía un Status de Recalendarizar y en efecto se está cambiando la fecha de la agenda, entonces el Status se cambia
			//a abierto para que vuelva a estar disponible esta agenda
			if ($intStatus == agdRescheduled && ($this->CaptureStartDate != $this->CaptureStartDateOld ||
					$this->CaptureStartTime_Hours != $this->CaptureStartTime_HoursOld || $this->CaptureStartTime_Minutes != $this->CaptureStartTime_MinutesOld))
			{
				$intStatus = agdOpen;
			}
			//@JAPR
			
			$sqlIns = "INSERT INTO SI_SV_SurveyAgendaDet (AgendaID, SurveyID, Status) 
				VALUES (".$this->AgendaID.",".$aSurveyID.",".$intStatus.")";
			if ($this->Repository->DataADOConnection->Execute($sqlIns) === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sqlIns);
			}
		}
	}
	
	function isNewObject()
	{
		return ($this->AgendaID < 0);
	}
	
	function getAgendaName()
	{
		if ($this->AgendaName != '')
		{
			return $this->AgendaName;
		}
		else 
		{
			return $this->CatalogName.' - '.$this->MemberName.' - '.$this->FilterText;
		}
	}
	
	function getAgendaTime()
	{
		return (($this->CaptureStartTime_Hours < 10)?'0':'').$this->CaptureStartTime_Hours.':'.(($this->CaptureStartTime_Minutes < 10)?'0':'').$this->CaptureStartTime_Minutes;
	}
	
	//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
	//Obtiene la hora de captura de la encuesta de CheckIn en formato de 24hrs (para despliegue, no es una hora completa ni se debe usar para fórmulas)
	function getCheckInTime() {
		return (($this->EntryCheckInStartDate_Hours < 10)?'0':'').$this->EntryCheckInStartDate_Hours.':'.(($this->EntryCheckInStartDate_Minutes < 10)?'0':'').$this->EntryCheckInStartDate_Minutes;
	}
	function getAgendaStatus() {
		$status = '';
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		//echo(nl2br("\r\n Agenda data: $this->CaptureStartDate => $this->Status"));
		switch($this->Status) {
			case agdOpen:
				$status = translate("Unanswered");
				break;
			case agdCanceled:
				$status = translate("Cancelled");
				break;
			case agdRescheduled:
				$status = translate("Rescheduled");
				break;
			case agdFinished:
				$status = translate("Finished");
				break;
			case agdAnswered:
			default:
				$status = translate("Incomplete");
				break;
		}
		//@JAPR
		return $status;
	}
	
	//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
	function getUserName()
	{
		return (($this->UserName != '')?$this->UserName:$this->UserID);
	}
	//@JAPR

	//GCRUZ 2016-06-06. Agenda Lat y Lng basado en el filtro
	function updateAgendaLatLng()
	{
		//Obtener el checkpoint
		if ($aCheckPoint = BITAMCheckpoint::NewInstanceWithId($this->Repository, $this->CheckPointID ))
		{
			$arrAttrsKey = array();
			$arrFilters = array();
			$arrAttrsFields = array();
			$aDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $aCheckPoint->catalog_id);
			$bEbavelSource = false;
			if ($aDataSource->eBavelAppID > 0 && $aDataSource->eBavelFormID > 0)
			{
				//GCRUZ 2016-06-09. Filtros como arreglo. Bug en issue: MXVIZA
				$aSQL = "SELECT id_fieldform, id FROM si_forms_fieldsforms WHERE section_id = ".$aDataSource->eBavelFormID;
				$rsEbavelFields = $this->Repository->ADOConnection->Execute($aSQL);
				$arrEbavelFields = array();
				if ($rsEbavelFields && $rsEbavelFields->_numOfRows != 0)
				{
					while(!$rsEbavelFields->EOF)
					{
						$arrEbavelFields[$rsEbavelFields->fields['id_fieldform']] = $rsEbavelFields->fields['id'];
						$rsEbavelFields->MoveNext();
					}
				}
				$bEbavelSource = true;
			}
			foreach ($aCheckPoint->attributes as $anAttribute)
			{
				if (intval($anAttribute['type']) == 1)
				{
					$arrAttrsKey[] = intval($anAttribute['id']);
					$arrFilters[intval($anAttribute['id'])] = explode('|', $this->FilterText);
					$arrAttrsFields['FilterField'] = intval($anAttribute['id']);
				}
				if (intval($anAttribute['type']) == 3)
				{
					$arrAttrsKey[] = intval($anAttribute['id']);
					$arrAttrsFields['Latitude'] = intval($anAttribute['id']);
				}
				if (intval($anAttribute['type']) == 4)
				{
					$arrAttrsKey[] = intval($anAttribute['id']);
					$arrAttrsFields['Longitude'] = intval($anAttribute['id']);
				}
			}
			$arrAttributes = BITAMDataSourceMemberCollection::NewInstance($this->Repository, $aCheckPoint->catalog_id, $arrAttrsKey);
			$arrDataSourceMember = array();
			$arrDataSourceMemberFields = array();
			foreach($arrAttributes->Collection as $aMember)
			{
				$aDataMember = array();
				$aDataMember['DataSourceID'] = $aMember->DataSourceID;
				$aDataMember['sourceMemberID'] = $aMember->MemberID;
				$aDataMember['MemberName'] = $aMember->MemberName;
				$aDataMember['FieldName'] = $aMember->FieldName;
				$arrDataSourceMember[] = $aDataMember;
				if (!$bEbavelSource)
					$arrDataSourceMemberFields[intval($aMember->MemberID)] = $aMember->FieldName;
				else
					$arrDataSourceMemberFields[intval($aMember->MemberID)] = $arrEbavelFields[$aMember->eBavelFieldID];
			}

			$aCollection = $aDataSource->getAttributeValues($arrDataSourceMember, -1, ( count( $arrFilters ) ? $arrFilters : '' ), 1, true, false, false, null, false);
			$arrAttrData = array();
			if ($aCollection && $aCollection->_numOfRows != 0)
			{
				$fieldFilter = $aCollection->fields[strtolower($arrDataSourceMemberFields[$arrAttrsFields['FilterField']])];
				$sourceFieldLat = strtolower($arrDataSourceMemberFields[$arrAttrsFields['Latitude']]);
				$sourceFieldLng = strtolower($arrDataSourceMemberFields[$arrAttrsFields['Longitude']]);
				if ($bEbavelSource)
				{
					if ($sourceFieldLat == $sourceFieldLng)
					{
						$sourceFieldLat = $sourceFieldLat."_l";
						$sourceFieldLng = $sourceFieldLng."_a";
					}
				}
				$fieldLat = $aCollection->fields[$sourceFieldLat];
				$fieldLng = $aCollection->fields[$sourceFieldLng];
				$arrAttrData = array('filter' => $fieldFilter, 'lat' => $fieldLat, 'lng' => $fieldLng);
			}
			if (isset($arrAttrData['lat']) && isset($arrAttrData['lng']))
			{
				//Validar que existen los campos
				$aSQL = "ALTER TABLE si_sv_surveyagenda ADD Latitude DOUBLE NULL";
				$this->Repository->DataADOConnection->Execute($aSQL);
				$aSQL = "ALTER TABLE si_sv_surveyagenda ADD Longitude DOUBLE NULL";
				$this->Repository->DataADOConnection->Execute($aSQL);
				$aSQL = "UPDATE SI_SV_SurveyAgenda SET Latitude = ".floatval($arrAttrData['lat']).", Longitude = ".floatval($arrAttrData['lng'])." WHERE AgendaID = ".$this->AgendaID;
				$this->Repository->DataADOConnection->Execute($aSQL);
			}
		}
	}
	
	function get_Title()
	{
		if ($this->isNewObject())
		{
			return translate("New")." ".translate("Agenda");
		}
		else
		{
			return $this->getAgendaName();
			//return $this->CatalogName.' - '.$this->MemberName.' - '.$this->FilterText; // .' - '.$this->SurveyName;
		}
	}
	
	function get_QueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}

		if ($this->isNewObject())
		{
			return "BITAM_PAGE=SurveyAgenda".$strParameters;
		}
		else
		{
			return "BITAM_PAGE=SurveyAgenda&AgendaID=".$this->AgendaID.$strParameters;
		}
	}
	
	function get_Children($aUser)
	{
		return array();
	}
	
	function get_Parent()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;
		
		return BITAMSurveyAgendaCollection::NewInstance($this->Repository, null, $agUserID, $agEndDate, null, false, $agStartDate, $agCatalogID, $agIpp, false);
	}
	
	function get_PathArray()
	{
		$parent = $this->get_Parent();
		$pathArray = $parent->get_PathArray();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}
	
	function get_FormIDFieldName()
	{
		return 'AgendaID';
	}
		
	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		require_once("user.inc.php");
		require_once("catalog.inc.php");
		
		$myFields = array();
		
		$isReadOnly = false;
		if(!$this->isNewObject())
		{
			$isReadOnly = true;
		}
		$arrayStatusSurvey = array();
		$arrayStatusSurvey[0] = 0;
		$arrayStatusSurvey[1] = 1;
		
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "AgendaName";
		$aField->Title = translate("Agenda Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		*/
		
		//Al cambiar el usuario se deben refrescar los catálogos disponibles
		$userField = BITAMFormField::NewFormField();
		$userField->Name = "UserID";
		$userField->Title = translate("User");
		$userField->Type = "Object";
		$userField->VisualComponent = "Combobox";
		//@JAPR 2015-12-02: Modificado el nombre de la clase para que no cause conflicto con eBavel u otros productos (#GC22SM)
		$userField->Options = BITAMeFormsUser::GetUsers($this->Repository, false);
		//@JAPR 2012-03-26: Corregido un bug, al no tener el método OnChange, no sincronizaba las combos al cargar la página
		//@JAPR 2012-03-30: Corregido un bug, al cambiar el usuario hay que volver a ir al server por los valores del atributo pues cada usuario
		//puede tener un filtro distinto
		//$userField->OnChange = "updateUsers()";
		//$userField->OnChangeBeforeUpdate = true;
		//@JAPR
		$userField->IsDisplayOnly = ($isReadOnly && $this->UserID > 0);
		$myFields[$userField->Name] = $userField;
		
		$usedCatalogs = BITAMCatalog::GetUsedCatalogs($this->Repository, null, true);
		//combo from catalog
		//Al cambiar el catálogo se deben refrescar tanto la combo de atributos como la del filtro de atributo y las encuestas que están
		//disponibles utilizando para el usuario y que utilizan dicho catálogo
		$catalogField = BITAMFormField::NewFormField();
		$catalogField->Name = "CatalogID";
		$catalogField->Title = translate("Catalog");
		$catalogField->Type = "Object";
		$catalogField->VisualComponent = "Combobox";
		//$catalogField->Options = BITAMCatalog::getCatalogs($this->Repository);
		//@JAPR 2012-07-04: Corregido un bug en Agendas, no se permitía crear agendas de encuestas que no estuvieran en Schedulers porque no se
		//listaban sus catálogos
		//$catalogField->Options = BITAMSurveyAgenda::getCatalogsByUserID($this->Repository, $this->UserID);  
        
        //2015.01.08 JCEM #INHWFA Se agrega none cuando no recibe ningun resultado
        if (count($usedCatalogs) == 0){
            $usedCatalogs[0] = translate("None");
        }
		$catalogField->Options = $usedCatalogs;
        
      
		//@JAPR 2012-03-26: Corregido un bug, al no tener el método OnChange, no sincronizaba las combos al cargar la página
		
		if (getMDVersion() >= esvAgendaRedesign) {
			if ($this->isNewObject()) {
				$catalogField->OnChange = "updateAttributeFields()";
			}
		} else {
			$catalogField->OnChange = "updateFields();";
		}
		//@JAPR
		
		$catalogField->IsDisplayOnly = ($isReadOnly && $this->CatalogID > 0);
		$myFields[$catalogField->Name] = $catalogField;
		if (getMDVersion() >= esvAgendaRedesign) {
			$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true);
			$catFilterInstance = null;
			foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) {
				$lastField = null;
				$curCatMember = 0;
				$arrKeys = array_keys($tmpCatalog);
				foreach ($tmpCatalog as $ckey => $tmpCatMember) {
					if (!$this->isNewObject()) {
						if ($this->CatalogID  != $catKey) {
							break;
						}
						$catFilterInstance = BITAMCatalogFilter::NewInstance($this->Repository, $this->CatalogID);
						//$catFilterInstance->AttribFields = array();
						/*foreach ($catFilterInstance->AttribFields as $afkey => $af) {
							$catFilterInstance->AttribFields[$afkey] = null;
						}*/
					}
					$tmpMember = BITAMCatalogMember::NewInstanceWithID($this->Repository, $ckey);
					$attributeField = BITAMFormField::NewFormField();
					$attributeField->Name = "DSC_".$tmpMember->ClaDescrip;
					$attributeField->Title = translate($tmpMember->MemberName);
					$attributeField->Type = "Object";
					$attributeField->VisualComponent = "Combobox";
					$dynamicProperty = $attributeField->Name;
					$this->$dynamicProperty = $dynamicProperty;
					$attribFields = null;
					if (!$this->isNewObject()) {
						if ($this->FilterText != "") {
							$arrFilterText = explode("|", $this->FilterText);
							if (isset($catFilterInstance)) {
								if (!is_null($lastField)) {
									$memberorder = ($lastField->MemberOrder-1);
									$memberorder = ($tmpMember->MemberOrder-1);
									$catFilterInstance->AttribFields[$memberorder] = 'DSC_'.$tmpMember->ClaDescrip;
									$tempFieldName = $catFilterInstance->AttribFields[$memberorder];
									$catFilterInstance->$tempFieldName = null;
									$catFilterInstance->$tempFieldName = $arrFilterText[($curCatMember-1)];
									/*if (!is_null($lastField)) {
										$catFilterInstance->$tempFieldName = $arrFilterText[($curCatMember-1)];
									}*/
								}
							}
						}
						//var_dump(json_encode($catFilterInstance->AttribFields));
						$catalogValues = $catFilterInstance->getCatalogValues(($tmpMember->MemberOrder-1));
						$attributeField->Options = $catalogValues;
						$lastField = $tmpMember;
					}
					if ($this->FilterText != "") {
						$arrFilterText = explode("|", $this->FilterText);
						$this->$dynamicProperty = $arrFilterText[$curCatMember];
					}
					$nextKey = (isset($arrKeys[($curCatMember+1)])) ? $arrKeys[($curCatMember+1)] : null;
					if (!is_null($nextKey)) {
						$attribName = "DSC_".$nextKey;
					}
					$myFields[$attributeField->Name] = $attributeField;
					$curCatMember++;
				}
			}
		}
		
		//@JAPR 2012-07-04: Corregido un bug en Agendas, no se permitía crear agendas de encuestas que no estuvieran en Schedulers porque no se
		//listaban sus catálogos
		//Ya no estarán sincronizados el usuario con la lista de catálogos disponibles
		//$catalogField->Parent = $userField;
		//$userField->Children[] = $catalogField;
		//@JAPR
		
		//@JAPR 2012-05-30: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
		if ($this->isNewObject())
		{
			//@JAPR 2012-07-04: Corregido un bug en Agendas, no se permitía crear agendas de encuestas que no estuvieran en Schedulers porque no se
			//listaban sus catálogos
			/*
			if ($this->UserID > 0 && isset($catalogField->Options[$this->UserID]) && count($catalogField->Options[$this->UserID]) > 0)
			{
				foreach ($catalogField->Options[$this->UserID] as $this->CatalogID => $strCatalogName)
				{
					break;
				}
			}
			*/
			foreach ($catalogField->Options as $this->CatalogID => $strCatalogName)
			{
				break;
			}
			//@JAPR
		}
		//@JAPR
		
		//atributes combo
		//Al cambiar el atributo se debe refrescar sólo la combo con el filtro de atributo
		$catmemberField = BITAMFormField::NewFormField();
		$catmemberField->Name = "AttributeID";
		$catmemberField->Title = translate("Attribute");
		$catmemberField->Type = "Object";
		$catmemberField->VisualComponent = "Combobox";
		$catmemberField->Options = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository);
		//$catmemberField->OnChange = "updateMemberValues()";
		$catmemberField->IsDisplayOnly = ($isReadOnly && $this->AttributeID > 0);
		$myFields[$catmemberField->Name] = $catmemberField;

		//$catmemberField->Parent = $catalogField;
		//$catalogField->Children[] = $catmemberField;
		
		if (getMDVersion() >= esvAgendaRedesign) {
			//$catmemberField->IsVisible = false;
		}
        
		//@JAPR 2012-05-30: Validado que si es un objeto nuevo, se seleccione por default el primer Atributo disponible
		if ($this->isNewObject())
		{
			if ($this->CatalogID > 0 && isset($catmemberField->Options[$this->CatalogID]) && count($catmemberField->Options[$this->CatalogID]) > 0)
			{
				foreach ($catmemberField->Options[$this->CatalogID] as $this->AttributeID => $strAttributeName)
				{
					break;
				}
			}
		}
		//@JAPR
		
		$defaultFilter = array();
		$defaultFilter[$this->AttributeID] = array('' => '');
		if ($this->AttributeID > 0 && ($this->isNewObject() || $this->FilterText != ''))
		{
			$defaultFilter[$this->AttributeID] = BITAMSurveyAgenda::getCatMemberValuesByAttributeID($this->Repository, $this->AttributeID, $this->UserID);
			if ($this->isNewObject())
			{
				foreach ($defaultFilter[$this->AttributeID] as $strKey => $strValue)
				{
					//$this->FilterText = $strValue;
					break;
				}
			}
		}
		$catValuesField = BITAMFormField::NewFormField();
		$catValuesField->Name = "FilterText";
		$catValuesField->Title = translate("Attribute value");
		$catValuesField->Type = "Object";
		$catValuesField->VisualComponent = "Combobox";
		$catValuesField->Options = $defaultFilter;
		//$catValuesField->DynamicLoadFn = "updateMemberValues()";
		
		if (getMDVersion() >= esvAgendaRedesign) {
			//$catValuesField->IsVisible = false;
		}
		
		//$catValuesField->IsDisplayOnly = ($isReadOnly && $this->FilterText != '');
		$myFields[$catValuesField->Name] = $catValuesField;

		//$catValuesField->Parent = $catmemberField;
		//$catmemberField->Children[] = $catValuesField;
		
		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "CheckinSurvey";
		$surveyField->Title = translate("Checkin Survey");
		$surveyField->Type = "Object";
		$surveyField->VisualComponent = "Combobox";
		$arraySurveys = array(0 => translate("None")) + BITAMSurvey::getSurveys($this->Repository);
		$surveyField->Options = $arraySurveys;
		$myFields[$surveyField->Name] = $surveyField;		
		
		//Surveys
		$surveyField = BITAMFormField::NewFormField();
		$surveyField->Name = "SurveyIDs";
		$surveyField->Title = translate("Surveys");
		$surveyField->Type = "Array";
		//$surveyField->VisualComponent = "Combobox";
		//$surveyField->Options = BITAMSurveyAgenda::getSurveysByCatMemberID($this->Repository);
		$surveyField->Options = BITAMSurveyAgenda::getAllSurveys($this->Repository);
		//$surveyField->IsDisplayOnly = ($isReadOnly && $this->SurveyID > 0);
		//$surveyField->Size = 1000;
		$myFields[$surveyField->Name] = $surveyField;
		
		//$surveyField->Parent = $catmemberField;
		//$catmemberField->Children[] = $surveyField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartDate";
		$aField->Title = translate("Capture Start Date");
		$aField->Type = "Date";
		$aField->FormatMask = "yyyy-MM-dd";
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartTime";
		$aField->Title = translate("Capture Start Time");
		$aField->Type = "Time";
		$myFields[$aField->Name] = $aField;
		return $myFields;
	}
	
	//Obtiene la lista de catálogos indexada por usuario aplicando la seguridad de cada uno
	static function getCatalogsByUserID($aRepository, $userID)
	{
		
	    $sql = "SELECT DISTINCT C.UserID, A.CatalogID, D.CatalogName".
			" FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerUser C, SI_SV_Catalog D ".
			" WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID".	// AND C.UserID = ".$userID.
				" AND A.CatalogID = D.CatalogID AND A.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0".
			" ORDER BY 1, 3";
		
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_SurveyScheduler, SI_SV_SurveySchedulerUser, SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["userid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["userid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["userid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["catalogid"]] = $aRS->fields["catalogname"];
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}
		
	    //Obtener el listado de encuestas disponibles para el usuario logueado x rol, en este caso como la tabla de Roles por usuario
	    //no se tiene cargada en el DWH, se tiene que hacer en 2 partes, primero obtener los catálogos por Rol y luego concatenar cada uno
	    //de estos Arrays al Array de catálogos por usuario, obteniendo la lista de usuarios por cada rol como paso intermedio
	    //$roles = BITAMAppUser::getArtusRolesByClaUsuario($aRepository, $userID);
	    $roles = array();
	    if (count($roles) > 0 || true)
	    {
	    	$sql = "SELECT DISTINCT C.RolID, A.CatalogID, D.CatalogName".
				" FROM SI_SV_Survey A, SI_SV_SurveyScheduler B, SI_SV_SurveySchedulerRol C, SI_SV_Catalog D".
				" WHERE A.SurveyID = B.SurveyID AND B.SchedulerID = C.SchedulerID".	// AND C.RolID IN ( ". implode(",", $roles). " )".
					" AND A.CatalogID = D.CatalogID AND A.Status IN (".agdOpen.", ".agdAnswered.") AND A.CatalogID > 0".
				" ORDER BY 1, 3";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_SurveyScheduler, SI_SV_SurveySchedulerRol, SI_SV_Catalog ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			$allValuesxRol = array();
	
			if (!$aRS->EOF)
			{
				$lastID = $aRS->fields["rolid"];
				$aGroup = array();
				do
				{
					if ($lastID != $aRS->fields["rolid"])
					{
						$allValuesxRol[$lastID] = $aGroup;
						$lastID = $aRS->fields["rolid"];
						$aGroup = array();
					}
					
					$aGroup[$aRS->fields["catalogid"]] = $aRS->fields["catalogname"];
	  				$aRS->MoveNext();
	
				}while (!$aRS->EOF);
				
				$allValuesxRol[$lastID] = $aGroup;
			}
			
			//Recorre cada uno de los Roles y concatena los catálogos obtenidos a cada usuario
			if (count($allValuesxRol) > 0)
			{
				$sql = "SELECT A.CLA_ROL, A.CLA_USUARIO ".
					" FROM SI_ROL_USUARIO A".
					//" WHERE A.CLA_ROL IN (". implode(",", $roles). " )".
					" ORDER BY A.CLA_ROL";
				$aRS = $aRepository->ADOConnection->Execute($sql);
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_ROL_USUARIO ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}

				$arrUsersxRol = array();
		
				if (!$aRS->EOF)
				{
					$lastID = $aRS->fields["cla_rol"];
					$aGroup = array();
					do
					{
						if ($lastID != $aRS->fields["cla_rol"])
						{
							$arrUsersxRol[$lastID] = $aGroup;
							$lastID = $aRS->fields["cla_rol"];
							$aGroup = array();
						}
						
						$aGroup[] = $aRS->fields["cla_usuario"];
		  				$aRS->MoveNext();
		
					}while (!$aRS->EOF);
					
					$arrUsersxRol[$lastID] = $aGroup;
				}
				
				foreach ($allValuesxRol as $aRolID => $arrCatalogs)
				{
					if (isset($arrUsersxRol[$aRolID]))
					//Agrega los catálogos por rol a la lista de catálogos de cada usuario
					foreach ($arrUsersxRol[$aRolID] as $anUserId)
					{
						foreach ($arrCatalogs as $aCatalogID => $aCatalogName)
						{
							if (!isset($allValues[$anUserId][$aCatalogID]))
							{
								$allValues[$anUserId][$aCatalogID] = $aCatalogName;
							}
						}
					}
				}
			}
	    }
		
		return $allValues;
	}
	
	//@JAPR 2015-08-29: Rediseñadas las agendas en v6
	/* Obtiene la lista de atributos indexada por su MemberID y agrupada por el CatalogID basada en los filtros de los parámetros
	@JAPRWarning: Esta función realmente debería estar en CatalogMember o DataSourceMember según el caso, no en este elemento ya que no es natural para esta clase
	*/
	static function getCatMembersByCatalogID($aRepository, $justKeyOfAgendas = null, $catalogID = null, $justParentIDs = null, $aCheckPointID = null)
	{		
		//@JAPR 2014-12-04: Corregido un bug, el campo KeyOfAgenda se utilizó a partir de cierta versión
		$strAdditionalFields = '';
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		if (getMDVersion() < esveFormsv6) {
			$strAdditionalFields .= ', A.ParentID';
		}
		//@JAPR
		if (getMDVersion() >= esvAgendaRedesign) {
			$strAdditionalFields .= ', A.KeyOfAgenda';
		}
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//Se cambia la tabla a SI_SV_DataSourceMember ya que ahora las Agendas se asocian a DataSources, además se debe hacer join con el CheckPoint correcto,
		//ya que el DataSource se envuelve en un CheckPoint para definir el tipo de uso de los atributos (al momento de la implementación, sólo podía haber un 
		//CheckPoint por DataSource, sin embargo previniento que se pudieran repetir, se recibirá como parámetro el CheckOpoint específico
		$sql = "SELECT A.DataSourceID AS CatalogID, A.MemberID, A.MemberName, A.MemberOrder {$strAdditionalFields} 
			FROM SI_SV_DataSourceMember A ";
			//FROM SI_SV_CatalogMember ";
		//@JAPR 2014-12-04: Corregido un bug, el campo KeyOfAgenda se utilizó a partir de cierta versión
		$strWhere = ' WHERE ';
		if (!is_null($justKeyOfAgendas) && $justKeyOfAgendas && getMDVersion() >= esveAgendas) {
			$sql .= "
				LEFT OUTER JOIN si_sv_checkpointattr B ON A.MemberID = B.attr_id ";
			$sql .= "
				WHERE B.type = 1";
			$strWhere = " AND ";
		}
		if (!is_null($catalogID)) {
			//@JAPR 2015-08-29: Rediseñadas las agendas en v6
			$sql .= $strWhere."A.DataSourceID = ".$catalogID." ";
		}
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		$sql .= "
			ORDER BY A.DataSourceID, A.MemberOrder";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_DataSourceMember ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();
		
		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catalogid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catalogid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catalogid"];
					$aGroup = array();
				}
				if (is_null($justParentIDs)) {
					$aGroup[$aRS->fields["memberid"]] = (string) @$aRS->fields["membername"];
				} else {
					$aGroup[$aRS->fields["memberid"]] = (int) @$aRS->fields["parentid"];
				}
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	/* Regresa el filtro SQL de la agenda basado en las variables de atributos preparadas para reemplazo en algunas funciones que obtienen datos
	de los catálogos, utilizando como valor directamente el filtro configurado en la agenda. Este método originalmente era una función global
	que no tenía necesidad, ya que se invocaba desde una función propia de la instancia de agenda, así que se convirtió en una función local
	//@JAPR 2015-08-29: Agregado el parámetro $aLastKeyAttribID para indicar cual es el último atributo llave usado, ya que ese es el límite del filtro generalmente
	*/
	function getAgendaFilter(&$aLastKeyAttribID = 0) {
		$strFilter = '';
		if ($this->FilterText != '') {
			$arrFilterText = explode("|", $this->FilterText);
			$catalogID = $this->CatalogID;
			$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, $catalogID);
			foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) {
				$first = true;
				$curCatMember = 0;
				foreach ($tmpCatalog as $ckey => $tmpCatMember) {
					//@JAPR 2015-08-29: Rediseñadas las agendas en v6
					//Se deja de usar la instancia porque sólo se cargaba para obtener el MemberID, el cual ya tiene porque se carga precisamente con él
					//$tmpMember = BITAMCatalogMember::NewInstanceWithID($this->Repository, $ckey);
					//$memberID = $tmpMember->MemberID;
					$memberID = $ckey;
					$aLastKeyAttribID = $memberID;
					//@JAPR 2015-09-04: Validado para que en caso de no venir todos los elementos no marque error
					$memberValue = $this->Repository->DataADOConnection->Quote((string) @$arrFilterText[$curCatMember]);
					if (!$first) {
						$strFilter .= ' && ';
					}
					//@CMID66 = 'The Home Depot' AND @CMID65 = 'Home Depot Tampico'
					//@JAPR 2015-08-29: Rediseñadas las agendas en v6, ahora se utilizan los DataSources y se delimitarán entre {} los atributos
					$strFilter .= ("{@DMID".$memberID."} = ".$memberValue);
					$curCatMember++;
					$first = false;
				}
			}
		}
		
		return $strFilter;
	}
	
	//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
	/* Esta función regresa un array que contendrá un índice por cada Forma donde se hubieran generado valores de catálogo basados en un filtro
	dinámico de dicha forma, por lo que podrían existir valores diferentes del mismo catálogo entre varias formas, además de un índice
	con el Id de Forma == 0 para los casos en que mas se repitiera el mismo filtro dinámico de los mismos catálogos, de tal manera
	que funcione como un tipo de Default por si no viene el catálogo específicamente en una forma determinada, ejemplo:
	array = (
		0 => array(catalogID1 => array(values), catalogID2 => array(values)),			//El índice default, si no viene el catalogID en la forma se usará este
		surveyID1 => array(catalogID3 => array(values), catalogID4 => array(values)),	//Una forma con catalogID que son personalizados para ella
		surveyID3 => array(catalogID1 => array(values), catalogID3 => array(values))	//Una forma con un catalogID personalizado pero otro que no es igual que el default (por tanto personalizado también, pero compartido con el default)
	)
	Aunque no se mandan en el array, la agenda pudo tener a surveyID2 y surveyID4, el hecho de no ir en el array significa que utilizarán
	específicamente los catalogID1 y/o catalogID2 (no tienen que ser necesariamente ambos, pudo ser cualquiera de ellos o ninguno incluso), ya que
	tomará los valores del índice default 0 para dichos catálogos por tener el mismo filtro en ambas formas, siempre y cuando dicho catálogo
	esté filtrado dinámicamente para esas formas dentro del objeto dynamicCatFilters, puesto que si no estuviera filtrado de forma dinámica,
	entonces simplemente significa que no debe tomar los valores para ese catálogo desde la agenda sino que debe utilizar los valores de la propia
	definición del catálogo, lo anterior aplica por igual para el surveyID1 puesto que no contiene a catalogID1 ni catalogID2, pero
	en el caso de surveyID3 sólo podría aplicar para catalogID2, ya que catalogID1 está personalizado directamente en su array de forma que si
	tomará los valores directamente de él
	*/
	function getAllDynamicAttributeValues($anAgendaDef) {
		require_once("processsurveyfunctions.php");
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n entering getAllDynamicAttributeValues for agenda {$this->AgendaID}");
			PrintMultiArray($arrQuestionCatalogValues);
		}
		
		global $gblShowErrorSurvey;
		$gblShowErrorSurvey = false;
		
		$arrDynamicFiltersBySurveyID = array();
		
		//Genera un array local para permitir incluir a la forma de CheckIn en el proceso
		$arrSurveyIDs = $this->SurveyIDs;
		if ($this->CheckinSurvey > 0 && !in_array($this->CheckinSurvey, $arrSurveyIDs)) {
			$arrSurveyIDs[] = $this->CheckinSurvey;
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n getAllDynamicAttributeValues Surveys:");
			PrintMultiArray($arrSurveyIDs);
		}
		//Recorre todas las formas de la agenda para verificar si tiene o no algún filtro dinámico que procesar
		foreach ($arrSurveyIDs as $intSurveyID) {
	        $objSurveyFilterCollection = BITAMSurveyFilterCollection::NewInstance($this->Repository, $intSurveyID);
	        if (is_null($objSurveyFilterCollection) || count($objSurveyFilterCollection->Collection) == 0) {
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n getAllDynamicAttributeValues No filters for Survey: $intSurveyID");
				}
	        	continue;
	        }
	        
	        //En caso de tener por lo menos un filtro dinámico, agrega el filtro a la colección sin ser traducido aún, ya que mas adelante se
	        //cambiarán las variables por el valor según el filtro de la agenda. En este punto aún no puede agrupar los catálogos ya que cada
	        //variable cambia entre formas diferentes aunque el valor al final sea el mismo para ellas, así que hay que esperar a la traducción
	        //de variables para realmente agrupar los filtros. No aplican los filtros dinámicos del catálogo de la Agenda
			if (!isset($arrDynamicFiltersBySurveyID[$intSurveyID])) {
				$arrDynamicFiltersBySurveyID[$intSurveyID] = array();
			}
			
			foreach ($objSurveyFilterCollection->Collection as $objSurveyFilter) {
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//if ($objSurveyFilter->CatalogID == $this->CatalogID) {
				if ($objSurveyFilter->DataSourceID == $this->CatalogID) {
					continue;
				}
				
				//El filtrado específico si se hace por Catálogo en lugar de por DataSource, dada la manera en que funcionan en esta versión los catálogos,
				//ya no se podrían repetir entre formas diferentes los mismos catálogos (los DataSource si)
				$arrDynamicFiltersBySurveyID[$intSurveyID][$objSurveyFilter->CatalogID] = $objSurveyFilter->FilterText;
				//@JAPR
			}
			
			if (count($arrDynamicFiltersBySurveyID[$intSurveyID])== 0) {
				unset($arrDynamicFiltersBySurveyID[$intSurveyID]);
			}
		}
		
		//Al llegar a este punto ya se tiene identificado si existe por lo menos un filtro dinámico para algún catálogo, pero no se sabe si
		//estará o no involucrado el filtro propio de la agenda, así que se tiene que procesar cada filtro traduciendo sus variables
		if (count($arrDynamicFiltersBySurveyID) == 0) {
			return null;
		}
		
		//Se obtiene la combinación completa del catálogo aplicando el filtro de la agenda, de tal forma que se pueda pasar como array de
		//datos para traducir las variables. A la fecha de implementación, sólo se permitía este proceso utilizando variables de preguntas las
		//cuales siempre estaban en el nuevo formato de variables
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
		//Se cambió el proceso para reutilizar la instancia de valores que ya se generaron para cada catálogo asociado al DataSource de la agenda, de tal manera
		//que no se tenga que duplicar la carga de dichos datos, el parámetro $anAgendaDef contiene la definición tal como se mandará al App y de ahí se puede
		//obtener la información requerida
		$arrQuestionCatalogValues = array();
		if (is_array($anAgendaDef) && isset($anAgendaDef['fullCatalogFilter'])) {
			$arrQuestionCatalogValues = $anAgendaDef['fullCatalogFilter'];
		}
		
		/*
	    $objCatalog = @BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
	    if (is_null($objCatalog)) {
	    	return null;
	    }
	    
	    $arrAgendaCatValues = array();
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    $intMaxOrder = 0;
	    $strFilter = $this->getAgendaFilter();
		$strFilter = BITAMCatalog::TranslateAttributesVarsToFieldNames($this->Repository, $this->CatalogID, $strFilter);
		$arrAttributesDef = BITAMCatalog::GetCatalogAttributes($this->Repository, $this->CatalogID, $intMaxOrder);
		$arrAgendaCatValues = @$objCatalog->getAttributeValues(array_values($arrAttributesDef), -1, $strFilter, null, null, false, true);
		//@JAPR
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n getAllDynamicAttributeValues {$this->AgendaID} Agenda Values:");
			PrintMultiArray($arrAgendaCatValues);
		}
		
		if (count($arrAgendaCatValues) == 0) {
			return null;
		}
		
		//Genera el Array de valores tal como se procesaría para una pregunta de catálogo (generalmente contiene hasta el atributo de la pregunta
		//durante el proceso de grabado, pero en este caso como es una información que aún no se manda al App y tenemos el catálogo directo,
		//si se puede obtener toda la jerarquía de atributos así que se utilizarán todos tal cual, asumiendo que esta misma información será
		//la que se mande al App al descargar la definición
		$arrQuestionCatalogValues = array();
		//@JAPR 2015-08-29: Rediseñadas las agendas en v6
		//En esta versión cada pregunta/sección puede tener un orden diferente, así que genera la lista de valores pero indexada específicamente en el orden de
		//los atributos según el catálogo específico mapeado al DataSource. Dado a que el proceso de carga de los valores por catálogo ya hace este proceso, se va
		//a cambiar la secuencia de invocación de esta función para que esté posterior a la carga de valores de los catálogos de la Agenda, así se podrá recibir
		//la colección de valores ya filtrados según la agenda y en el orden del catálogo
		while (count($arrAgendaCatValues) > 0 && isset($arrAgendaCatValues[0])) {
			$arrQuestionCatalogValues[] = (string) @$arrAgendaCatValues[0]['value'];
			if (isset($arrAgendaCatValues[0]['children'])) {
				$arrAgendaCatValues = $arrAgendaCatValues[0]['children'];
			}
			else {
				$arrAgendaCatValues = array();
			}
		}
		*/
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n getAllDynamicAttributeValues {$this->AgendaID} Agenda Cat Values for question:");
			PrintMultiArray($arrQuestionCatalogValues);
		}
		
		//Recorre cada catálogo de cada forma, preparando una array de valores de las preguntas que sean del mismo catálogo de la agenda, para
		//posteriormente aplicar la traducción de las variables de dichas preguntas, y finalmente aplicar la traducción de las variables de
		//filtros de catálogo para pedir los valores que apliquen en el filtro resultante, si éste falla quiere decir que faltó traducir alguna
		//variable de pregunta y por tanto el filtro no es apto para este proceso, sólo en caso de que todo resulte correcto es como se agregaría
		//dicho filtro a una colección junto a los valores obtenidos con él, para porteriormente agruparlos en caso de ser necesario
		$arrGroupedDynamicValuesByCatalogID = array();
		$arrTranslatedDynamicValuesBySurveyID = array();
		$arrTranslatedDynamicFiltersBySurveyID = array();
		$arrGroupedDynamicFiltersCountByCatalogID = array();
		foreach ($arrDynamicFiltersBySurveyID as $intSurveyID => $arrSurveyFilters) {
			//Carga la colección de preguntas de la forma, se hizo de esta manera porque esta colección está cachada y será mas fácil recorrerla
			//cada vez que se necesite, en lugar de volver a ejecutar los queries por cada iteración utilizando una función que no se encuentre 
			//cachada pero obtenga exclusivamente lo que se necesita y sólo se usará en este punto aunque varias veces
			$objQuestionCollection = BITAMQuestionCollection::NewInstance($this->Repository, $intSurveyID);
			if (is_null($objQuestionCollection)) {
				continue;
			}
			
			//Genera el array con los datos de las preguntas que son del mismo catálogo de la agenda
			$arrFieldValues = array();
			$arrQuestionIDs = array();
			foreach ($objQuestionCollection->Collection as $objQuestion) {
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//if ($objQuestion->QTypeID != qtpSingle || $objQuestion->CatalogID != $this->CatalogID) {
				if ($objQuestion->QTypeID != qtpSingle || $objQuestion->DataSourceID != $this->CatalogID) {
					continue;
				}
				
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//Se cambió el proceso para reutilizar la instancia de valores que ya se generaron para cada catálogo asociado al DataSource de la agenda, de tal manera
				//que no se tenga que duplicar la carga de dichos datos, el parámetro $anAgendaDef contiene la definición tal como se mandará al App y de ahí se puede
				//obtener la información requerida
				if (!isset($arrQuestionCatalogValues[$objQuestion->CatalogID])) {
					continue;
				}
				
				//Esta es una pregunta del mismo catálogo de la agenda y es simple choice, así que se agrega el array de valores a traducir
				$arrQuestionIDs[$objQuestion->QuestionID] = $objQuestion->QuestionID;
				//@JAPR 2015-03-30: Corregido un bug, este array se debe indexar con el número de pregunta en lugar del ID
				$strQuestionFieldID = 'qfield'.$objQuestion->QuestionNumber;
				//@JAPR
				$arrFieldValues[$strQuestionFieldID] = $arrQuestionCatalogValues[$objQuestion->CatalogID];
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n getAllDynamicAttributeValues, Questions with the Agenda Catalog in SurveyID == {$intSurveyID}:");
					//@JAPR 2016-04-28: Corregido un bug, no se puede mandar como parámetro de PrintMultiArray un array_keys, debe ser una variable
					$arrFieldValuesKeys = array_keys($arrFieldValues);
					PrintMultiArray($arrFieldValuesKeys);
					//@JAPR
				}
			}
			
			//Recorre todos los filtros dinámicos sin importar si son o no del catálogo de la agenda, a cada uno le traducirá las variables de
			//preguntas que si son del catálogo de la agenda
			foreach ($arrSurveyFilters as $intCatalogID => $strFilterText) {
				$arrInfo = array();
				$strTranslatedFilterText = @replaceQuestionVariables($strFilterText, $arrFieldValues, false, $this->Repository, $intSurveyID, false, false, true, $arrInfo);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n getAllDynamicAttributeValues, replaceVars extended info:");
					PrintMultiArray($arrInfo);
				}
				
				//Verifica si hay alguna otra pregunta además de las relacionadas con el catálogo de la agenda, si la hay entonces no se puede
				//utilizar este filtro
				$blnInvalidVariable = false;
				if (is_array($arrInfo) && isset($arrInfo['elements'])) {
					foreach ($arrInfo['elements'] as $strExpr => $arrVarData) {
						if ($arrVarData['type'] != otyQuestion) {
							$blnInvalidVariable = true;
							break;
						}
						
						$intQuestionID = (int) @$arrVarData['id'];
						$strFunction = strtolower((string) @$arrVarData['fn']);
						//@JAPR 2016-04-28: Corregido un bug, no se habían agregado las nuevas funciones de atributos así que cuando se usaban se excluían y no generaba el filtro (#MKM198)
						if (!isset($arrQuestionIDs[$intQuestionID]) || ($strFunction != 'attr' && $strFunction != 'attrid' && $strFunction != 'attrdid')) {
							$blnInvalidVariable = true;
							break;
						}
					}
				}
				if ($blnInvalidVariable) {
					continue;
				}
				
				//Agrega el filtro a la colección agrupándolo y contabilizando cuantos filtros idénticos hay, para decidir cual será el que
				//se utilizará como el filtro default de este catálogo
				if (!isset($arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID])) {
					$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID] = array();
				}
				
				if (!isset($arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strTranslatedFilterText])) {
					$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strTranslatedFilterText] = 1;
				}
				else {
					$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strTranslatedFilterText] += 1;
				}
				
				//Agrega el filtro traducido a un array de equivalencias respecto al filtro original, ya que el proceso final buscará
				//directamente sobre el traducido en cuanto a variables de preguntas, ya que ese es el que se puede agrupar
				if (!isset($arrTranslatedDynamicFiltersBySurveyID[$intCatalogID])) {
					$arrTranslatedDynamicFiltersBySurveyID[$intCatalogID] = array();
				}
				$arrTranslatedDynamicFiltersBySurveyID[$intCatalogID][$strFilterText] = $strTranslatedFilterText;
			}
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n getAllDynamicAttributeValues, arrGroupedDynamicFiltersCountByCatalogID:");
			PrintMultiArray($arrGroupedDynamicFiltersCountByCatalogID);
		}
		
	    //@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
	    $intMaxOrder = 0;
		//Recorre todos los filtros traducidos en cuanto a variables de preguntas para aplicar la traducción de variables de atributos,
		//además ya puede identificar por catálogo cual de los filtros es el utilizado mas veces, de tal forma que pueda agregarlo 
		//directamente en el índice default del array final en lugar de en una forma específica
		foreach ($arrGroupedDynamicFiltersCountByCatalogID as $intCatalogID => $arrTranslatedFilters) {
		    $objCatalog = @BITAMCatalog::NewInstanceWithID($this->Repository, $intCatalogID);
		    if (is_null($objCatalog)) {
		    	continue;
		    }
			
			$intMaxFormsCount = 0;
			$strDefaultFilterText = '';
			foreach ($arrTranslatedFilters as $strFilterText => $intFormsCount) {
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//En este punto la traducción se debe hacer respecto al DataSource, no al Catálogo
				$strTranslatedFilterText = BITAMDataSource::TranslateAttributesVarsToFieldNames($this->Repository, $objCatalog->DataSourceID, $strFilterText);
			    //$strTranslatedFilterText = BITAMCatalog::TranslateAttributesVarsToFieldNames($this->Repository, $intCatalogID, $strFilterText);
				//@JAPR
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n getAllDynamicAttributeValues, processing catalogID == {$intCatalogID}, filter == \"{$strFilterText}\", translation == \"{$strTranslatedFilterText}\", used by {$intFormsCount} forms");
				}
				
				//Obtiene los valores del filtro, en caso de error significaría probablemente que no se tradujeron todas las variables
				//de preguntas, por lo que debe haber condiciones que no se pueden resolver sólo con la agenda y por tanto la agenda no
				//puede mandar los valores prefiltrados para este catálogo, así que este filtro es descartado
				$blnError = false;
				$arrAttributesDef = BITAMCatalog::GetCatalogAttributes($this->Repository, $intCatalogID, $intMaxOrder);
				$arrFilteredValues = @$objCatalog->getAttributeValues(array_values($arrAttributesDef), -1, $strTranslatedFilterText, null, null, false, true, $blnError);
				if (getParamValue('DebugBreak', 'both', '(int)', true)) {
					echo("<br>\r\n getAllDynamicAttributeValues, (Error?: '{$blnError}') filtered values for \"{$strFilterText}\":");
					PrintMultiArray($arrFilteredValues);
				}
				
				if ($blnError) {
					//En caso de error, no se prefiltrará el catálogo para la Agenda sino que se dejará que el App utilice la definición
					//completa del catálogo, de tal manera que funcione tal como lo había hecho hasta antes de esta implementación, aunque
					//si llega a fallar este query entonces muy probablemente lo haga también al obtener la definición del catálogo completa
					//Además debe eliminar esta iteración de filtros, sólo que en este punto exclusivamente lo marcará con un total de 0 ocurrencias
					//para que sea omitido mas adelante
					$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strFilterText] = 0;
					continue;
				}
				
				//Si llegó a este punto significa que el filtro es válido y ya se tienen los valores, así que los agrega a un array temporal
				//previo a generar el array final de resultados
				if ($intFormsCount > $intMaxFormsCount) {
					$intMaxFormsCount = $intFormsCount;
					$strDefaultFilterText = $strFilterText;
				}
				
				if (!isset($arrGroupedDynamicValuesByCatalogID[$intCatalogID])) {
					$arrGroupedDynamicValuesByCatalogID[$intCatalogID] = array();
				}
				$arrGroupedDynamicValuesByCatalogID[$intCatalogID][$strFilterText] = $arrFilteredValues;
			}
			
			//Agrega el filtro con mas ocurrencias al índice default, excepto si la máxima cantidad de ocurrencias era 1, ya que en ese caso
			//no hay optimización y complicará entender los datos al ser analizados
			if ($intMaxFormsCount > 1 && isset($arrGroupedDynamicValuesByCatalogID[$intCatalogID][$strDefaultFilterText])) {
				if (!isset($arrTranslatedDynamicValuesBySurveyID[0])) {
					$arrTranslatedDynamicValuesBySurveyID[0] = array();
				}
				
				$arrTranslatedDynamicValuesBySurveyID[0][$intCatalogID] = $arrGroupedDynamicValuesByCatalogID[$intCatalogID][$strDefaultFilterText];
				//Debe eliminar el filtro de este array de agrupaciones, ya que el mismo se utilizará para agregar los filtros por forma y
				//no se debe repetir la colección de valores que ya se agregó por default, sin embargo se eliminará simplemente marcando con un
				//total de 0 ocurrencias
				//unset($arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strDefaultFilterText]);
				$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strDefaultFilterText] = 0;
			}
		}
		
		//Por último genera el array que será enviado al App, el cual viene indexado como se ejemplifica en la definición de esta función,
		//personalizado por Forma excepto en los filtros mas utilizados, los cuales se agregarán a un índice especial con ID == 0
		foreach ($arrDynamicFiltersBySurveyID as $intSurveyID => $arrSurveyFilters) {
		//foreach ($arrGroupedDynamicFiltersCountByCatalogID as $intCatalogID => $arrTranslatedFilters) {
			//foreach ($arrTranslatedFilters as $strFilterText => $intFormsCount) {
			foreach ($arrSurveyFilters as $intCatalogID => $strFilterText) {
				//El array que se está recorriendo contiene los filtros en el formato original, sin embargo los arrays ya con datos procesados
				//están traducidos en cuanto a variables de preguntas, por lo tanto se obtiene primero el filtro con traducción parcial
				$strTranslatedFilterText = (string) @$arrTranslatedDynamicFiltersBySurveyID[$intCatalogID][$strFilterText];
				
				//Sólo procesa los filtros no marcados como eliminados, entre los cuales se incluyen ya los default
				$intFormsCount = (int) @$arrGroupedDynamicFiltersCountByCatalogID[$intCatalogID][$strTranslatedFilterText];
				$arrFilteredValues = @$arrGroupedDynamicValuesByCatalogID[$intCatalogID][$strTranslatedFilterText];
				if ($intFormsCount == 0 || is_null($arrFilteredValues)) {
					continue;
				}
				
				//Es un filtro válido que no se había procesado, así que se agrega al array resultante
				if (!isset($arrTranslatedDynamicValuesBySurveyID[$intSurveyID])) {
					$arrTranslatedDynamicValuesBySurveyID[$intSurveyID] = array();
				}
				
				$arrTranslatedDynamicValuesBySurveyID[$intSurveyID][$intCatalogID] = $arrFilteredValues;
			}
		}
		
		if (getParamValue('DebugBreak', 'both', '(int)', true)) {
			echo("<br>\r\n getAllDynamicAttributeValues, arrTranslatedDynamicValuesBySurveyID:");
			PrintMultiArray($arrTranslatedDynamicValuesBySurveyID);
		}
		
		return $arrTranslatedDynamicValuesBySurveyID;
	}
	//@JAPR
	
	static function getAllSurveys ($aRepository) {
		return BITAMSurvey::getSurveys($aRepository, null, true);
	}
	
	static function getSurveysByCatMemberID($aRepository)
	{
		//@JAPR 2012-05-31: Agregado el soporte para preguntas de tipo Open Numericas cuyo valor proviene de un Catálogo
		//Se tiene que validar para que no se consideren las preguntas abiertas numéricas ya que son un caso especial, por lo tanto no se
		//encadenan como Padres/Hijas de otras preguntas Single Choice
		$sql = "SELECT B.CatMemberID, A.SurveyID, A.SurveyName".
			" FROM SI_SV_Survey A, SI_SV_Question B".
			" WHERE A.SurveyID = B.SurveyID AND B.CatMemberID > 0 AND B.QTypeID <>".qtpOpenNumeric.
			" ORDER BY B.CatMemberID, A.SurveyName";
		$aRS = $aRepository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Survey, SI_SV_Question ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$allValues = array();

		if (!$aRS->EOF)
		{
			$lastID = $aRS->fields["catmemberid"];
			$aGroup = array();
			do
			{
				if ($lastID != $aRS->fields["catmemberid"])
				{
					$allValues[$lastID] = $aGroup;
					$lastID = $aRS->fields["catmemberid"];
					$aGroup = array();
				}
				
				$aGroup[$aRS->fields["surveyid"]] = $aRS->fields["surveyname"];
  				$aRS->MoveNext();

			}while (!$aRS->EOF);
			
			$allValues[$lastID] = $aGroup;
		}

		return $allValues;
	}
	
	static function getCatMemberValuesByAttributeID($aRepository, $anAttributeID, $anUserID = null)
	{
		require_once("catalog.inc.php");
		require_once("catalogmember.inc.php");
		require_once("dimension.inc.php");
		
		if (is_null($anUserID))
		{
			$anUserID = $_SESSION["PAtheUserID"];
		}
		$attribValues = array();
		$aCatalogMember = BITAMCatalogMember::NewInstanceWithID($aRepository, $anAttributeID);
		$aDimension = null;
		$strFilter = '';
		if (!is_null($aCatalogMember))
		{
			$aCatalog = BITAMCatalog::NewInstanceWithID($aRepository, $aCatalogMember->CatalogID);
			if (!is_null($aCatalog))
			{
				$strFilter = $aCatalog->generateResultFilterByUser($anUserID);
				//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
				if (trim($strFilter) != '')
				{
					$strFilter = " WHERE ".$strFilter;
				}
				//@JAPR
			}
			$aDimension = BITAMDimension::NewInstanceWithDescripID($aRepository, $aCatalogMember->ClaDescrip, -1, false);
		}
		
		$attribValues = array();
		$strResult="";
		if (!is_null($aDimension))
		{
			//Cambia el nombre de la llave para que apunte a la llave subrrogada del catálogo, ya que como es un DISTINCT para efectos de este caso
			//no importa cual valor de la subrrogada se espera, el que sea que regrese será capaz de obtener la descripción de esta dimensión
			//$aDimension->nom_fisicok = strtolower($aDimension->nom_tabla).'key';
			//$aDimension->nom_fisicok_bd = 'MAX('.$aDimension->nom_tabla.'KEY) AS '.$aDimension->nom_tabla.'KEY';
			//$aDimension->getDimensionValuesExt('', $aDimension->nom_fisico);
			//@JAPR 2012-05-30: Corregido un bug, no estaba enviando el filtro por usuario al método de carga de los valores del catálogo
			$aDimension->getDimensionValuesExt($strFilter);
			//@JAPR
			//Los valores están contenidos en el Array propio de la dimensión, así que eso es lo que se devuelve
			$attribValues = $aDimension->values;
			if (isset($attribValues[$aDimension->NoApplyValue]))
			{
				unset($attribValues[$aDimension->NoApplyValue]);
			}
		}
		return $attribValues;
	}

	function generateBeforeFormCode($aUser)
 	{	
	?>
 		<script language="JavaScript">		
		var initialValuesSet = false;
		
		var filterAttribIDs = new Array;
		var filterAttribPos = new Array;
		var filterAttribMembers = new Array;
		var filterAttribMemberFields = new Array;
		var numFilterAttributes = new Array;
		var lastAttributeID = new Array();
		var isEbavel = new Array();
		<? $tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, null, true);
		foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { ?>
			if (!numFilterAttributes[<?= $catKey ?>]) {
				numFilterAttributes[<?= $catKey ?>] = new Array;
				filterAttribIDs[<?= $catKey ?>] = new Array;
				filterAttribPos[<?= $catKey ?>] = new Array;
				filterAttribMembers[<?= $catKey ?>] = new Array;
				filterAttribMemberFields[<?= $catKey ?>] = new Array;
				numFilterAttributes[<?= $catKey ?>] = new Array;
				lastAttributeID[<?= $catKey ?>] = new Array;
			}
			numFilterAttributes[<?= $catKey ?>] = <?= count($tmpCatalog)?>;
			<? $tmpCurCat = 0;
			foreach ($tmpCatalog as $ckey => $tmpCatMember) {
				$position = $tmpCurCat;
				$attribID = $tmpCatMember;
				//$attribName = $this->AttribFields[$position];
				//$attribName = $tmpCatMember;
				$attribName = 'DSC_'.$tmpCatMember;
				//$attribMemberID = $this->AttribMemberIDs[$attribName]
				$attribMemberID = $ckey;
				?>
				filterAttribIDs[<?= $catKey ?>][<?=$position?>] = <?=$attribID?>;
				filterAttribPos[<?= $catKey ?>]['<?=$attribName?>'] = <?=$position?>;
				filterAttribMembers[<?= $catKey ?>]['<?=$attribName?>'] = <?=$attribMemberID?>;
				filterAttribMemberFields[<?= $catKey ?>][<?=$attribMemberID?>] = '<?=$attribName?>';
				<? 
				$tmpCurCat++; 
				$lastAttributeID = $ckey;
				?>
			<? } ?>
			lastAttributeID[<?= $catKey ?>] = <?=$lastAttributeID?>;
			<? 
			$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $catKey);
			$isEbavel = ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) ? 1 : 0;
			?>
			isEbavel[<?= $catKey ?>] = <?= $isEbavel ?>;
		<? } ?>
		
		<? if (count($tmpCatalogCollection) > 0) { ?>
		
			//var editing = false;
			function refreshAttributes(attribName, event) {
				var event = (window.event) ? window.event : event;
				/*if ((event.target || event.srcElement).name == '') {
					editing = true;
				}*/
				var isCatalogID = false;
				if (!event) {
					isCatalogID = true;
				} else {
					var objElement = document.getElementsByName((event.target || event.srcElement).name)[0];
					if (objElement == undefined || objElement.name == 'CatalogID') {
						isCatalogID = true;
					}
				}
				objAttrib = document.getElementsByName(attribName)[0];
				var objCmbCatalog = BITAMSurveyAgenda_SaveForm.CatalogID;
				var intCatalogID = objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
				if (objAttrib.value == 'sv_ALL_sv') {
					for (i = 0;i < numFilterAttributes[intCatalogID];i++) {
						var thisPosition = filterAttribPos[intCatalogID][attribName];
						//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
						var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
						var position = filterAttribPos[intCatalogID][searchAttrib];
						if(position > thisPosition) {
							var obj = document.getElementsByName(searchAttrib)[0];
							if (obj) {
								obj.length = 0;
								var newOption = new Option('<?=translate("All")?>', 'sv_ALL_sv');
								obj.options[obj.options.length] = newOption;
							}
						}
					}
					return;
				}
				var xmlObj = getXMLObject();
				if (xmlObj == null) {
					alert("XML Object not supported");
					return(new Array);
				}
				strMemberIDs = "";
				strParentIDs = "";
				strValues = "";
				var nextAttrib = undefined;
				for(i = 0; i < numFilterAttributes[intCatalogID]; i++) {
					var thisPosition = filterAttribPos[intCatalogID][attribName];
					//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var position = filterAttribPos[intCatalogID][searchAttrib];
					var obj = document.getElementsByName(searchAttrib)[0];
					if(position <= thisPosition) {
						if(strMemberIDs!="") {
							strMemberIDs+="_SVSEPSV_";
						}
						strMemberIDs+=filterAttribMembers[intCatalogID][searchAttrib];
						if(strParentIDs!="") {
							strParentIDs+="_SVSEPSV_";
						}
						strParentIDs+=filterAttribIDs[intCatalogID][position];
						if(strValues!="") {
							strValues+="_SVSEPSV_";
						}
						strValues+=obj.value;
					} else {
						if (thisPosition == 0 && isCatalogID) {
								nextAttrib = strMemberIDs;
						} else {
							var nextAttribSearch = 'DSC_' + filterAttribIDs[intCatalogID][i];
							nextAttrib = filterAttribMembers[intCatalogID][nextAttribSearch];
						}
						break;
					}
				}
				var catMemberID = (isEbavel[intCatalogID]) ? nextAttrib : lastAttributeID[intCatalogID];
				//var catMemberID = lastAttributeID[intCatalogID];
				var changedMemberID = filterAttribMembers[intCatalogID][attribName];
				if (isCatalogID) {
					changedMemberID = '';
				}
				xmlObj.open("POST", "getAttribValues.php?CatalogID="+intCatalogID+"&CatMemberID="+catMemberID+"&ChangedMemberID="+changedMemberID+"&strMemberIDs="+strMemberIDs+"&strParentIDs="+strParentIDs+"&strValues="+strValues+'&isAgenda=1', false, false);
				xmlObj.send(null);
				strResult = Trim(unescape(xmlObj.responseText));
				arrayAttrib = strResult.split("_SV_AT_SV_");
				var numAttrib = arrayAttrib.length;
				var refreshAttrib = new Array();
				for (var i = 0; i < numAttrib; i++) {
					if (arrayAttrib[i] != '') {
						var atribInfo = arrayAttrib[i].split("_SV_ID_SV_");
						var membID = atribInfo[0];
						var attribValues = atribInfo[1].split("_SV_VAL_SV_");
						var numAtVal = attribValues.length;
						var searchAttrib = filterAttribMemberFields[intCatalogID][membID];
						var obj = document.getElementsByName(searchAttrib)[0];
						if (obj) {
							obj.length = 0;
							var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
							obj.options[obj.options.length] = newOption;
							for (j = 0; j < numAtVal; j++) {
								var newOption = new Option(attribValues[j], attribValues[j]);
								obj.options[obj.options.length] = newOption;
							}
							refreshAttrib[i]=membID;
						}
					}
				}
				//Asignarle sólo la opción All a los combos que tienen orden de jerarquía mayor al orden del combo cambiado
				// y que no sea el combo al que se le acaban de reasignar valores en el ciclo anterior
				for (i = 0; i < numFilterAttributes[intCatalogID]; i++) {
					//var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var searchAttrib = 'DSC_' + filterAttribIDs[intCatalogID][i];
					var obj = document.getElementsByName(searchAttrib)[0];
					if(filterAttribPos[intCatalogID][searchAttrib] > filterAttribPos[intCatalogID][attribName] && !inArray(refreshAttrib, filterAttribMembers[intCatalogID][searchAttrib])) {
						obj.length = 0;
						var newOption = new Option("<?=translate("All")?>", "sv_ALL_sv");
						obj.options[obj.options.length] = newOption;
					}
				}
				/*if (editing && filterText) {
					document.getElementsByName(("DSC_30895"))[0].selectedIndex = "Altamira";
					editing = false;
				}*/
			}
			
		<? } ?>
		
		function updateUsers()
 		{
 			if (cancel==true || true)
 			{
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					BITAMSurveyAgenda_FilterText = new Array();
	 			}
 				
				//BITAMSurveyAgenda_CatalogID_Update();
 				
	 			//updateMemberValues();
 			}
 			//cancel=false;
 		}
		
		var catMemberArray = {};
		var catMemberOrder = {};
		var agendaFilterText = '<?=$this->FilterText?>';
		var firstTime = true;
		
		function setAgendaAttributes () {
			<? $tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, null, true); ?>
			<? foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { ?>
					<? $tmpCurCat = 0; ?>
					catMemberArray[<?=$catKey?>] = {};
					catMemberOrder[<?=$catKey?>] = new Array();
					<? foreach ($tmpCatalog as $ckey => $tmpCatMember) { ?>
						catMemberArray[<?=$catKey?>][<?=$ckey?>] = <?=$tmpCatMember?>;
						catMemberOrder[<?=$catKey?>][<?=$tmpCurCat?>] = <?=$tmpCatMember?>;
						<? $tmpCurCat++; ?>
					<? } ?>
			<? } ?>
		}
		setAgendaAttributes();
		
		
		
		function updateAttributeFields () {
			var objCmbCatalog = BITAMSurveyAgenda_SaveForm.CatalogID;
			//Se inicializa en null para luego comprobar si se trajeron catalogos y mostrar o no las opciones
			var intCatalogID = null;
			if (objCmbCatalog && objCmbCatalog.options && objCmbCatalog.options.length > 0){
				intCatalogID =objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
			}
			
			var firstMember = true;
			for (var ckey in catMemberOrder) {
				var tmpCat = catMemberOrder[ckey];
				for (var mkey in tmpCat) {
					if (!isNaN(mkey)) {
						var catmemberkey = tmpCat[mkey];
						var rowMember = document.getElementById("Row_DSC_"+catmemberkey);
						//se valida si intCatalogId is null se ocultan estos controles
						rowMember.style.display = (intCatalogID != null && ckey == intCatalogID) ? ((bIsIE)?"inline":"table-row") : "none";
						if (firstMember && (ckey == intCatalogID)) {
							var strAttributeID = 'DSC_'+catmemberkey;
							if (agendaFilterText != '' && firstTime) {
								continue;
							} else {
								refreshAttributes(strAttributeID);
							}
							firstMember = false;
						}
						
					}
				}
			} 
			
			firstTime = false;
			cancel=false;
			
			return;
		}
 		
 		function updateFields()
 		{
 			if (cancel==true || true)
 			{
 				var catMember = BITAMSurveyAgenda_SaveForm.AttributeID;
 				
 				if (initialSelectionCatMember!=null)
 				{
 					for(i=0;i<catMember.options.length;i++)
 					{
 						if (catMember.options[i].value==initialSelectionCatMember)
 						{	
 							catMember.selectedIndex=i;
 							break;
 						}
 					}
 				}
 				
	 			//updateMemberValues();
 			}
 			//cancel=false;
 		}
 		
 		function updateMemberValues()
 		{
 			if (cancel==true || true) // || true)
 			{
 				var intUserID = 0;
 				var cmbUserID = BITAMSurveyAgenda_SaveForm.UserID;
 				if (cmbUserID != null && cmbUserID.selectedIndex >= 0)
 				{
 					intUserID = cmbUserID.options[cmbUserID.selectedIndex].value;
 				}
 				//Verifica si ya se habían cargado los valores de este atributo, si no es así hace una petición Ajax para obtenerlos
				var myParent = BITAMSurveyAgenda_SaveForm.AttributeID;
	        	var myParentSelection = null;
	        	if (myParent != null && myParent.selectedIndex >= 0)
	        	{
	        		var myParentSelection = myParent.options[myParent.selectedIndex].value;
	        	}
	        	if (myParentSelection != null)
	        	{
	        		var myNewValues = BITAMSurveyAgenda_FilterText[myParentSelection];
	        	}
	        	if (myNewValues == null)
	        	{
	        		//No se habían cargado los valores de este atributo
   	        		var myNewValues = new Array();
   	        		
					var xmlObj = getXMLObject();
					
					if (xmlObj == null)
					{
						alert("XML Object not supported");
					}
					else
					{
						var objCmbCatalog = BITAMSurveyAgenda_SaveForm.CatalogID;
						var intCatalogID =objCmbCatalog.options[objCmbCatalog.selectedIndex].value;
						
					 	xmlObj.open("POST", "getDistinctAttribValues.php?CatalogID="+intCatalogID+"&CatMemberID="+myParentSelection+"&userID="+intUserID, false);
						
					 	xmlObj.send(null);
					 	
					 	strResult = Trim(unescape(xmlObj.responseText));
					 	if (strResult.indexOf('<?=SV_SRV_ERROR?>') > 0)
					 	{
					 		var errorData = strResult.split('<?=SV_SRV_ERROR?>');
					 		if (errorData.length > 1)
					 		{
								alert("Error retrieving attribute's values: (" + errorData[0] + ') ' + errorData[1]);
					 		}
					 		else
					 		{
								alert("Error retrieving attribute's values: " + strResult);
					 		}
				 			//cancel=false;
							return(new Array);
					 	}
					 	else
					 	{
					 		var attribValues = strResult.split("_SV_VAL_");
					 		var numVal = attribValues.length;
					 		for (i = 0; i < numVal; i++)
					 		{
					 			arrValData = attribValues[i].split("<?=SV_DELIMITER?>");
					 			if (arrValData.length > 1)
					 			{
					 				myNewValues[i] = new Array(arrValData[0], arrValData[1]);
					 			}
					 			else
					 			{
					 				myNewValues[i] = new Array(i, arrValData[0]);
					 			}
					 		}
							//myNewValues = attribValues;
		   	        		BITAMSurveyAgenda_FilterText[myParentSelection] = myNewValues;
					 	}
					}
	        	}
	        	else
	        	{
	        		//Reutiliza los valores previamente cargados
   	        		var myNewValues = BITAMSurveyAgenda_FilterText[myParentSelection];
		        }
				
				//BITAMSurveyAgenda_SurveyIDs_Update();
 				BITAMSurveyAgenda_FilterText_Update();
 				/*
	 			if (initialValuesSet != undefined && initialValuesSet == true)
	 			{
					var surveyIDs = document.getElementsByName('SurveyIDs[]');
					if(initialSelectionSurveyIDs!=null)
					{
						for(i=0;i<initialSelectionSurveyIDs.length;i++)
						{
							for(j=0;j<surveyIDs.length;j++)
							{
								if(surveyIDs[j].value==initialSelectionSurveyIDs[i])
								{
									surveyIDs[j].checked = true;
									break;
								}
							}
						}
					}
	 			}
				*/
 				var filterText = BITAMSurveyAgenda_SaveForm.FilterText;
 				
 				if (initialSelectionFilterText!=null)
 				{
 					for(i=0;i<filterText.options.length;i++)
 					{
 						if (filterText.options[i].value==initialSelectionFilterText)
 						{	
 							filterText.selectedIndex=i;
 							break;
 						}
 					}
 				}
 			}
 			cancel=false;
 		}
 		
		//initialSelectionSurveyIDs=null;
 		initialSelectionCatMember = null;
 		initialSelectionFilterText = null;

 		function setInitialValues()
		{
            var catMember = BITAMSurveyAgenda_SaveForm.AttributeID;
            initialSelectionCatMember = null;
            if (catMember.selectedIndex >= 0)
            {
	        	initialSelectionCatMember = catMember.options[catMember.selectedIndex].value;
            }
                    
            var filterText = BITAMSurveyAgenda_SaveForm.FilterText;
            initialSelectionFilterText = null;
            if (filterText.selectedIndex >= 0)
            {
	        	initialSelectionFilterText = filterText.options[filterText.selectedIndex].value;
            }
			
            initialValuesSet = true;
			/*var surveyIDs = document.getElementsByName('SurveyIDs[]');
	       	initialSelectionSurveyIDs = new Array();
	       	if (surveyIDs != null)	//  && initialSelectionCatMember != null
	       	{
	       		if (surveyIDs != null)	//&& arrSurveyIDs != undefined
	       		{
					for (var i = 0; i < surveyIDs.length; i++)
					{
						if (surveyIDs[i].checked)
						{
		       				initialSelectionSurveyIDs[initialSelectionSurveyIDs.length] = surveyIDs[i].value;
						}
					}
	       		}
	       	}
	       	var strText = '';
	       	for (var idx=0; idx < initialSelectionSurveyIDs.length; idx++)
	       	{
	       		strText += idx + '=>' + initialSelectionSurveyIDs[idx];
	       	}*/
		}
		
 		function verifyFieldsAndSave(target)
 		{
			var strAlert = '';
			
			/*
 			if(Trim(BITAMSurveyAgenda_SaveForm.SchedulerName.value)=='')
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Description"))?>';
 			}
 			*/

  	 		var objCmb = BITAMSurveyAgenda_SaveForm.CatalogID;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			if(valueID <= 0)
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Catalog"))?>';
 			}
 			
  	 		var objCmb = BITAMSurveyAgenda_SaveForm.AttributeID;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			if(valueID <= 0)
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Attribute"))?>';
 			}
 			//@AAL 15/05/2015 Corregido Issue #0AWCEA: No estaba mostrando la hora en que se entra a capturar
 			var ObjComb = BITAMSurveyAgenda_SaveForm.CheckinSurvey;
			var Surveys = document.getElementsByName("SurveyIDs[]");
			var Txt = ObjComb.options[ObjComb.selectedIndex].text;
			for (var i = 0; i < Surveys.length; i++) {
				if(Surveys[i].value == ObjComb.value && Surveys[i].checked == true)
				{
					strAlert += '\n' + " <?=translate('Please select another Checkin Survey or unmark the Survey ')?>" + ObjComb.options[ObjComb.selectedIndex].text;
					break;
				}
			}
 			
 			/*
  	 		var objCmb = BITAMSurveyAgenda_SaveForm.SurveyID;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);
 			if(valueID <= 0)
 			{	
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Survey"))?>';
 			}
 			
			var itemsArray=document.getElementsByName("SurveyIDs[]");
			var numItems = itemsArray.length;
			var itemsSelected = false;
			var i;
			
			for (i=0; i<numItems; i++)
			{
				if(itemsArray[i].checked)
				{
					itemsSelected = true;
					break;
				}
			}
			
			if(!itemsSelected)
  			{
 				strAlert+= '\n'+'<?=sprintf(translate("%s is a required field. Please fill in accordingly"),translate("Surveys"))?>';
  			}
 			*/
 			/*
  	 		var objCmb = BITAMSurveyAgenda_SaveForm.AllowMultipleSurveys;
	 		var valueID = parseInt(objCmb.options[objCmb.selectedIndex].value);

	 		if(valueID==1)
	 		{
	 			var CaptureStartDateTime = new Date();
				
				var strToSplitCaptureStartDate = BITAMSurveyAgenda_SaveForm.CaptureStartDate.value.split(" ", 2);
				strToSplitCaptureStartDate = strToSplitCaptureStartDate[0].split("-",3);
				
				if (strToSplitCaptureStartDate[0]  == '0000' || strToSplitCaptureStartDate[1] == '00' || strToSplitCaptureStartDate[2] == '00')
				{
					strAlert+= '\n'+"<?=translate("Invalid start date of capture")?>" + ": " + strToSplitCaptureStartDate[0] + "-" + strToSplitCaptureStartDate[1] + "-"+ strToSplitCaptureStartDate[2] + "\n";
				}
	 		}
	 		*/
            
 			if(Trim(strAlert)!='')
 			{
 				alert(strAlert);
 			}
 			else
 			{
 				BITAMSurveyAgenda_Ok(target);
 			}
 		}
	</script>
<?
	}
	
	function generateAfterFormCode($aUser)
	{
?>
	<script language="JavaScript">
	 	setInitialValues();
		
		<? if (getMDVersion() >= esvAgendaRedesign) {?>
			var rowAttribute = document.getElementById("Row_AttributeID");
			var rowFilterText = document.getElementById("Row_FilterText");
	 		rowAttribute.style.display = "none";
			rowFilterText.style.display = "none";
		<? } ?>
		
		/********/
		
		<? 
		$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, null, true);
		foreach ($tmpCatalogCollection as $catKey => $tmpCatalog) { 
			foreach ($tmpCatalog as $ckey => $tmpCatMember) {
				if (!$this->isNewObject()) {
					if ($this->CatalogID != $catKey) {
						break;
					}
				}
				$attribName = "DSC_".$tmpCatMember; ?>
				if (document.getElementsByName('<?=$attribName?>')[0].addEventListener) {
					document.getElementsByName('<?=$attribName?>')[0].addEventListener("change", function(evt){ refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name, evt); }, true);
				} else  {
					document.getElementsByName('<?=$attribName?>')[0].attachEvent("onchange", function () { refreshAttributes(document.getElementsByName('<?=$attribName?>')[0].name); })
				}
			<?
			}
		}
		?>
		
		/*********/
					 	
		objOkSelfButton = document.getElementById("BITAMSurveyAgenda_OkSelfButton");
		objOkParentButton = document.getElementById("BITAMSurveyAgenda_OkParentButton");
		
		objOkSelfButton.onclick=new Function("verifyFieldsAndSave('self');");
		objOkParentButton.onclick=new Function("verifyFieldsAndSave('parent');");
		
<?
		if($this->isNewObject())
 		{
?>
 			objOkNewButton = document.getElementById("BITAMSurveyAgenda_OkNewButton");
 			objOkNewButton.onclick=new Function("verifyFieldsAndSave('new');");
<?
		}
?>
	</script>
<?
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canEdit($aUser)
	{
		//@JAPR 2014-12-17: Corregido un bug, la validación sólo consideraba al día actual como bloqueado
		if($_SESSION["PABITAM_UserRole"]==1 && $this->CaptureStartDate > date("Y-m-d"))
		{
			//$userCanEdit = false;
			return true;
		}
		else 
		{
			return false;	
		}
	}

	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		//@JAPR 2014-12-17: Agregada validación para evitar eliminar agendas históricas o en proceso de captura
		if($_SESSION["PABITAM_UserRole"]==1 && $this->CaptureStartDate > date("Y-m-d"))
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
  //Obtiene la definición en JSon de este objeto y sus dependencias (no hay referencias a catálogos u otros objetos globales, sólo son aquellos objetos que dependen
  //directamente de este
	function getJSonDefinition() {
		global $appVersion;
		
		$arrDef = array();
		
		$arrDef['id'] = $this->AgendaID;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['name'] = str_replace("|", ", ", $this->FilterText);
		$arrDef['catalogID'] = $this->CatalogID;
		$arrDef['attributeID'] = $this->AttributeID;
		$arrDef['versionNum'] = $this->VersionNum;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['filter'] = $this->FilterText;
		$arrDef['catalogFilter'] = array();
		$arrDef['userID'] = $this->UserID;
		//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
		$arrDef['captureStartDate'] = $this->CaptureStartDate;
		$arrDef['captureStartTime'] = $this->getAgendaTime();
		$arrDef['lastDateID'] = $this->LastDateID;
		//@JAPR 2014-11-20: Modificado para considerar a todas las agendas incluso si sólo tienen forma de CheckIn
		if (count($this->SurveyIDs)) {
			$arrDef['surveys'] = array_combine(array_values($this->SurveyIDs), $this->surveyIDsStatus);
		}
		else {
			$arrDef['surveys'] = array();
		}
		
		//@JAPR 2012-03-26: Agregado el filtro jerárquico de la agenda
		//Obtiene la colección de atributos del catálogo para usar al armar el query
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		//En esta versión se dejó el campo, pero realmente es un DataSourceID, así que se debe generar el catalogFilter pero indexado por cada Catalog del DataSource al que apunta
		$intDataSourceID = $this->CatalogID;
		$arrCatFilter = array();
		if ($intDataSourceID > 0) {
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			//Este proceso ya no aplica, ya que obtenía sólo un conjunto de valores para el único catálogo de la Agenda, lo cual ahora es un DataSource así que deberá obtener múltiples
			//valores indexados por el CatalogID específico (diferente combinación de atributos) pero filtrados por el DataSource
			if (getMDVersion() < esveAgendas) {
				$arrCatalogMembers = null;
				//@JAPR 2015-08-27: Rediseñadas las agendas en v6
				$arrCatalogMembers = BITAMCatalogMemberCollection::NewInstance($this->Repository, $intCatalogID);
				if (is_null($arrCatalogMembers)) {
					$arrCatalogMembers = array();
				}
				
				//Obtiene el valor de todos los miembros del catálogo hasta el atributo por el que se filtra la agenda, para generar el 
				//Array completo del filtro para ese catálogo
				if (!is_null($arrCatalogMembers) && count($arrCatalogMembers->Collection) > 0) {
					/***/
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
					if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
						//obtenemos los ids de los campos de ebavel
						$fieldsForAgenda = array();
						$countKeyOfAgenda = 0;
						foreach ($arrCatalogMembers as $aCatalogMember) {
							$eBavelFieldID = $aCatalogMember->eBavelFieldID;
							if ($aCatalogMember->KeyOfAgenda > 0) {
								if (!isset($fieldsForAgenda["KeyOfAgenda"])) {
									$fieldsForAgenda["KeyOfAgenda"] = array();
								}
								$fieldsForAgenda["KeyOfAgenda"][$aCatalogMember->ClaDescrip] = $eBavelFieldID;
								$countKeyOfAgenda++;
							}
							if (!isset($fieldsForAgenda["Fields"])) {
								$fieldsForAgenda["Fields"] = array();
							}
							$fieldsForAgenda["Fields"][$eBavelFieldID] = $eBavelFieldID;
						}
					
						//obtenemos los campo de ebavel (FFRMS_)
						$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
						if (getParamValue('DebugBreak', 'both', '(int)', true)) {
							echo("<br>\r\n CatalogFilter #1 anBavelFieldIDsColl");
							PrintMultiArray($anBavelFieldIDsColl);
						}
						$ebavelFieldsForAgenda = array();
						$ebavelTypesForAgenda = array(); 	//array con los tipos para saber si poner quote o no
						$curKeyOfAgenda = 0;
						foreach ($fieldsForAgenda["KeyOfAgenda"] as $eBavelFieldID) {
							foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
								if ($anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
									if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
										$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
										$ebavelTypesForAgenda["KeyOfAgenda"] = array();
									}
									$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
									$ebavelFieldTypeIsNumeric = 1;
									$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
									switch ($intQTypeID) {
										case qtpOpenNumeric:
										case qtpCalc:
											break;
										default:
											$ebavelFieldTypeIsNumeric = 0;
											break;
									}
									$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
									$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
									break;
								}
							}
						}
						
						//Llena el array de fields que se usará mas adelante para generar el catalogFilter
						foreach ($arrCatalogMembers as $aCatalogMember) {
							if ($countKeyOfAgenda == $curKeyOfAgenda) {
								break;
							}
							
							$eBavelFieldID = $aCatalogMember->eBavelFieldID;
							if ($eBavelFieldID <= 0) {
								continue;
							}
							
							foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
								if ($anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
									$strSuffix = '';
									$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
									$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
									$intEBavelFieldID = (int) @$anAttributeInfo['id_fieldform']; 	/* id num de ebavel */
									if ($inteBavelFieldDataID > ebfdValue) {
										switch ($inteBavelFieldDataID) {
											case ebfdLatitude:
												$strEBavelFieldID += '_L';
												break;
											case ebfdLongitude:
												$strEBavelFieldID += '_A';
												break;
										}
									}
									
									if (isset($fieldsForAgenda["KeyOfAgenda"])) {
										if (in_array($intEBavelFieldID, $fieldsForAgenda["KeyOfAgenda"])) {
											$curKeyOfAgenda++;
										}
									}
									
									if (isset($fieldsForAgenda["Fields"])) {
										if (!isset($ebavelFieldsForAgenda["Fields"])) {
											$ebavelFieldsForAgenda["Fields"] = array();
										}
										$ebavelFieldsForAgenda["Fields"][$intEBavelFieldID] = $strEBavelFieldID;
									}
									break;
								}
							}
						}
						
						//instanciamos el collection
						$strFilterArray = array();
						$curFilter = 0;
						if($this->FilterText) {
							//se genera el where
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo("<br>\r\n CatalogFilter #1 Values: {$this->FilterText}");
								PrintMultiArray($ebavelFieldsForAgenda["KeyOfAgenda"]);
							}
							$attributeValue = explode('|', $this->FilterText);
							foreach ($ebavelFieldsForAgenda["KeyOfAgenda"] as $ekey => $ebavelfield) {
								//checar si es numerico el campo o no para ponerle quotes en el where
								$ebavelValue = $attributeValue[$curFilter];
								$ebavelValue = ($ebavelTypesForAgenda["KeyOfAgenda"][$ekey]) ? $ebavelValue : $this->Repository->DataADOConnection->Quote($ebavelValue);
								array_push($strFilterArray, ($ebavelfield . " = " . $ebavelValue));
								$curFilter++;
							}
						}
						
						if (count($strFilterArray)) {
							
							$strFilter = implode(" AND ",$strFilterArray);
							
							$bGetDistinctValues = true;
							$bGetRecordset = false;
							$bApplyFilters = false;
							$bIncludeSystemFields = false;
							
							//$aRS = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
							$ebavelCatalogValuesCollection = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
							
							//instanciamos con el recordset filtrado que obtuvimos
							//$ebavelCatalogValuesInstance = BITAMeBavelCatalogValue::NewInstanceFromRS($this->Repository, $aRS, $aCatalogID); /* esto ya no */
							
							if (!is_null($ebavelCatalogValuesCollection) && isset($ebavelCatalogValuesCollection->Collection[0])) {
								$ebavelCatalogValuesInstance = $ebavelCatalogValuesCollection->Collection[0];
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\n CatalogFilter #1");
									PrintMultiArray($ebavelCatalogValuesInstance);
								}
								
								$arrCatFilter = array();
								//foreach ($arrCatalogMembers as $aCatalogMember) {
								//var_dump($ebavelFieldsForAgenda["Fields"]);
								//exit();
								//$curKeyOfAgendaAgenda = 0;
								foreach ($ebavelFieldsForAgenda["Fields"] as $ebavelFieldID => $aCatalogMember) {
									//$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
									if (isset($fieldsForAgenda["Fields"][$eBavelFieldID]) ) {
										$fieldName = $aCatalogMember;
										if (isset($ebavelCatalogValuesInstance->$fieldName) && $ebavelFieldID != -1) {
											//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
											$arrCatFilter[] = (string) $ebavelCatalogValuesInstance->$fieldName;
											/*if ($aCatalogMember->MemberID == $this->AttributeID)
											{
												break;
											}*/
										}
									}
								}
								if (getParamValue('DebugBreak', 'both', '(int)', true)) {
									echo("<br>\r\n CatalogFilter #1 final");
									PrintMultiArray($arrCatFilter);
								}
								//echo(json_encode($arrCatFilter));
								//exit();						
							}
						}
					}
					else {
						/****/
						$tableName = $this->CatalogTable;
						$fieldKey = "RIDIM_".$this->CatalogDim."KEY";
						$sql = "SELECT ";
						
						if (getMDVersion() >= esvAgendaRedesign) {
							//$agendaAttributes = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, $this->CatalogID);
							//var_dump($this->FilterText);
							$agendaAttributeValues = explode("|", $this->FilterText);
							//var_dump($agendaAttributeValues);
							//exit();
							$agendaAttributesCount = count($agendaAttributeValues);
						}
						$agendaLastAttributeNumber = 0;
						foreach ($arrCatalogMembers as $aCatalogMember)
						{
							if ($aCatalogMember->KeyOfAgenda)
							{
								$agendaLastAttributeNumber = $aCatalogMember->MemberOrder;
							}
						}
						$agendaAttributeNumber = 0;
						
						$sqlWhere = "";
						foreach ($arrCatalogMembers as $aCatalogMember)
						{
							if (getMDVersion() >= esvAgendaRedesign) {
								$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
								//if ($aCatalogMember->MemberID == $this->AttributeID)
								if ($aCatalogMember->KeyOfAgenda) {						
									if ($agendaAttributeNumber+1 == $agendaAttributesCount) {
										if ($agendaAttributeNumber == 0) {
											$sqlWhere .= " WHERE ";
										} else {
											$sqlWhere .= " AND ";
										}
										$sqlWhere .= ($fieldName." = ".$this->Repository->DataADOConnection->Quote($agendaAttributeValues[$agendaAttributeNumber]));
										$sql .= $fieldName.' FROM '.$tableName. $sqlWhere . " ORDER BY ".$fieldKey;
										break;
									} else {
										$sql .= $fieldName.', ';
										if ($agendaAttributeNumber == 0) {
											$sqlWhere .= " WHERE ";
										} else {
											$sqlWhere .= " AND ";
										}
										//var_dump($agendaAttributeNumber);
										$sqlWhere .= ($fieldName." = ".$this->Repository->DataADOConnection->Quote($agendaAttributeValues[$agendaAttributeNumber]));
									}
									$agendaAttributeNumber++;
									//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
									//$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$this->Repository->DataADOConnection->Quote($this->FilterText)." ORDER BY ".$fieldKey;
									//break;
								} else {
									//Es un padre, lo agrega directamente al query
									$sql .= $fieldName.', ';
								}
							} else {
								$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
								if ($aCatalogMember->MemberID == $this->AttributeID)
								{
									//Es el atributo asociado a la agenda, termina de armar el query y sale del ciclo
									$sql .= $fieldName." FROM ".$tableName." WHERE ".$fieldName." = ".$this->Repository->DataADOConnection->Quote($this->FilterText)." ORDER BY ".$fieldKey;
									break;
								}
								else 
								{
									//Es un padre, lo agrega directamente al query
									$sql .= $fieldName.', ';
								}
							}	
						}
						
						$aRSCat = $this->Repository->DataADOConnection->Execute($sql);
						if ($aRSCat)
						{
							if (getParamValue('DebugBreak', 'both', '(int)', true)) {
								echo("<br>\r\n CatalogFilter #2");
								PrintMultiArray($aRSCat->fields);
							}
							if (!$aRSCat->EOF)
							{
								$arrCatFilter = array();
								foreach ($arrCatalogMembers as $aCatalogMember)
								{
									$fieldName = "dsc_".$aCatalogMember->ClaDescrip;
									if (isset($aRSCat->fields[$fieldName])) {
										//@JAPR 2015-02-24: Agregado soporte para php 5.6 (Removida la codificación manual a utf8)
										$arrCatFilter[] = (string) $aRSCat->fields[$fieldName];
										if ($aCatalogMember->MemberID == $this->AttributeID)
										{
											break;
										}
									}
								}
							}
						}
					}
				}
				$arrDef['catalogFilter'] = $arrCatFilter;
				
				if (getMDVersion() >= esvAgendaRedesign) {
					$this->getAgendaRedesignDefinition($arrDef, $arrCatalogMembers);
				}
				
				//JAPR 2014-12-03: Agregado el array de valores del catálogo de la agenda
				if (getMDVersion() >= esvAgendaRedesign && $appVersion >= esvAgendaWithValues) {
					$objCatalog = @BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
					if (!is_null($objCatalog)) {
						//@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
						$intMaxOrder = 0;
						$strFilter = GetAgendaFilter($this->Repository, $this->AgendaID);
						$strFilter = BITAMCatalog::TranslateAttributesVarsToFieldNames($this->Repository, $this->CatalogID, $strFilter);
						$arrAttributesDef = BITAMCatalog::GetCatalogAttributes($this->Repository, $this->CatalogID, $intMaxOrder);
						$arrDef['attributeValues'] = @$objCatalog->getAttributeValues(array_values($arrAttributesDef), -1, $strFilter, null, null, false, true);
						if (is_null($arrDef['attributeValues'])) {
							unset($arrDef['attributeValues']);
						}
					}
				}
			}
			
			//@JAPR 2015-03-09: Agregados a la agenda los datos de todos los catálogos involucrados con el filtrado de la misma
			//La versión de Metadata no cambió para esta funcionalidad, sin embargo debe ser a partir del rediseño de Agendas, lo que si se valida
			//es que la versión del App esté preparada para utilizarla, ya que de lo contrario es inútil enviar estos datos al App
			$arrAgendaCatValues = array();
			//@JAPR 2015-08-29: Rediseñadas las agendas en v6
			//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
			//Se cambió el proceso para reutilizar la instancia de valores que ya se generaron para cada catálogo asociado al DataSource de la agenda, de tal manera
			//que no se tenga que duplicar la carga de dichos datos, el parámetro $anAgendaDef contiene la definición tal como se mandará al App y de ahí se puede
			//obtener la información requerida
			/*
			if (getMDVersion() >= esvAgendaWithCatValues && $appVersion >= esvAgendaWithCatValues) {
				$arrAgendaCatValues = $this->getAllDynamicAttributeValues($arrDef);
				if (!is_null($arrAgendaCatValues) && is_array($arrAgendaCatValues) && count($arrAgendaCatValues) > 0) {
					//Este array contendrá un índice por cada Forma donde se hubieran generado valores de catálogo basados en un filtro dinámico
					//de dicha forma, por lo que podrían existir valores diferentes del mismo catálogo entre varias formas, además de un índice
					//con el Id de Forma == 0 para los casos en que mas se repitiera el mismo filtro dinámico de los mismos catálogos, de tal manera
					//que funcione como un tipo de Default por si no viene el catálogo específicamente en una forma determinada
					$arrDef['catalogValues'] = $arrAgendaCatValues;
				}
				else {
					$arrAgendaCatValues = array();
				}
			}
			*/
			
			//@JAPR 2015-08-27: Rediseñadas las agendas en v6
			//Almacenará la primer colección de valores de un catálogo del DataSource de la agenda, para reutilizarla al obtener los datos de despliegue de la agenda
			$objFirstCatalogValuesColl = null;
			$objFirstCatalogAttributes = null;
			
			//Basado en las formas que son capturadas por esta Agenda, obtiene la lista de Catálogos que se basan en el DataSource de la Agenda, ya que debe generar una instancia de
			//valores por cada combinación de los mismos
			$arrSurveyIDs = $this->SurveyIDs;
			if ($this->CheckinSurvey > 0 && !in_array($this->CheckinSurvey, $arrSurveyIDs)) {
				$arrSurveyIDs[] = $this->CheckinSurvey;
			}
			
			if (count($arrSurveyIDs)) {
				$strSurveyIDs = implode(',', $arrSurveyIDs);
				
				//En este punto obtendrá todos los valores de los catálogos que NO estuvieran filtrados dinámicamente, y los agregará a la colección de valores indexada por catálogo, así
				//que primero identifica los catálogos ya filtrados dinámicamente
				$arrFilteredCatalogIDs = array();
				foreach ($arrAgendaCatValues as $intSurveyID => $arrCatalogIDs) {
					foreach ($arrCatalogIDs as $intCatalogID => $arrFilteredValues) {
						$arrFilteredCatalogIDs[$intCatalogID] = $intCatalogID;
					}
				}
				
				$strExcludeCatalogs = '';
				if (count($arrFilteredCatalogIDs)) {
					$strExcludeCatalogs = " AND A.CatalogID NOT IN (".implode(',', $arrFilteredCatalogIDs).") ";
				}
				
				//Obtiene toda la lista de Catálogos que son usados entre todas las formas de esta agenda, excluyendo aquellos que ya se encontraban filtrados, y que sólo sean derivados
				//del DataSource de la agenda. Adicionalmente obtiene el DataSourceMemberID aplicado, ya que el CatalogFilter de cada catálogo debe incluir los 
				//valores hasta dicho atributo como máximo, para permitir eventualmente que los hijos puedan ser seleccionables (a la fecha de implementación, no
				//se podían repetir CatalogIDs entre preguntas, así que era válido este proceso)
				$arrCatalogIDs = array();
				$sql = "SELECT DISTINCT A.CatalogID, A.DataSourceMemberID 
					FROM SI_SV_Question A 
					WHERE A.DataSourceID = {$intDataSourceID} AND A.SurveyID IN ({$strSurveyIDs}) {$strExcludeCatalogs} 
						UNION 
					SELECT DISTINCT A.CatalogID, A.DataSourceMemberID 
					FROM SI_SV_Section A 
					WHERE A.DataSourceID = {$intDataSourceID} AND A.SurveyID IN ({$strSurveyIDs}) {$strExcludeCatalogs} 
					ORDER BY 1";
				$aRS = $this->Repository->DataADOConnection->Execute($sql);
				if ($aRS) {
					while (!$aRS->EOF) {
						$intCatalogID = (int) @$aRS->fields["catalogid"];
						$intDataSourceMemberID = (int) @$aRS->fields["datasourcememberid"];
						if ($intCatalogID > 0) {
							$arrCatalogIDs[$intCatalogID] = $intDataSourceMemberID;
						}
						$aRS->MoveNext();
					}
				}
				
				//Si hay por lo menos un catálogo, debe cargar la instancia de cada uno y clasificarlo por los atributos que utiliza, de esa manera se pueden agrupar varios catálogos
				//que contienen el mismo orden de atributos, ya que esos cargarán la misma lista de valores y se pueden unificar en una misma consulta al DataSource (el filtro es el
				//mismo, pero el árbol de valores no pues se agrupa por la secuencia de atributos)
				//El índice será la combinación de atributos separada por ","
				$arrCatalogsByHierarchy = array();
				//@JAPR 2015-08-29: Se utilizará la función de instancia en lugar de la global
				$intLastKeyDataSourceMemberID = 0;
				$strDataSourceFilter = $this->getAgendaFilter($intLastKeyDataSourceMemberID);
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//En este punto la traducción se debe hacer respecto al DataSource, no al Catálogo
				$strDataSourceFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($this->Repository, $intDataSourceID, $strDataSourceFilter);
				//@JAPR
				foreach ($arrCatalogIDs as $intCatalogID => $intDataSourceMemberID) {
					if ($intCatalogID <= 0) {
						continue;
					}
					
					$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $intCatalogID);
					$objCatalogMembersColl = BITAMCatalogMemberCollection::NewInstance($this->Repository, $intCatalogID);
					if (is_null($objCatalog) || is_null($objCatalogMembersColl)) {
						continue;
					}
					
					$strAttribIDsStr = '';
					foreach ($objCatalogMembersColl->Collection as $aCatalogMember) {
						$strAttribIDsStr .= ','.$aCatalogMember->DataSourceMemberID;
					}
					
					//Si es la primera vez que ocurre esta combinación, entonces agrega la instancia de este catálogo para usarla específicamente para obtener los valores, aunque agrupará__halt_compiler
					//posiblemente a mas catálogos
					if (!isset($arrCatalogsByHierarchy[$strAttribIDsStr])) {
						$arrCatalogsByHierarchy[$strAttribIDsStr] = array();
						$arrCatalogsByHierarchy[$strAttribIDsStr]['Catalog'] = $objCatalog;
						$arrCatalogsByHierarchy[$strAttribIDsStr]['DataSourceMemberIDs'] = $strAttribIDsStr;
						$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogIDs'] = array();
						$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogMembersColl'] = array();
						$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogFilter'] = array();
						
						//Obtiene la colección de valores de este catálogo
						//@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
						$intMaxOrder = 0;
						$objCatalogAttributes = BITAMCatalog::GetCatalogAttributes($this->Repository, $intCatalogID, $intMaxOrder);
						if (is_null($objCatalogAttributes)) {
							continue;
						}
						
						$objCatalogValuesColl = $objCatalog->getAttributeValues(array_values($objCatalogAttributes), -1, $strDataSourceFilter, null, null, false, true);
						if (!is_null($objCatalogValuesColl)) {
							$arrCatalogsByHierarchy[$strAttribIDsStr]['AttributeValues'] = $objCatalogValuesColl;
							if (is_null($objFirstCatalogValuesColl)) {
								$objFirstCatalogAttributes = $objCatalogAttributes;
								$objFirstCatalogValuesColl = $objCatalogValuesColl;
							}
							
							//Genera el CatalogFilter a partir de los elementos obtenidos como valores, ya que el CatalogFilter al final es sólo la jerarquía
							//de atributos usados en el catálogo hasta X atributo (el usado específicamente en la pregunta/sección) tomando el primer valor de cada
							//atributo padre hasta llega al atributo filtrado en la Agenda, ya que todos los padres teóricamente deberían estar filtrados por ella
							//así que sólo traerían un valor. Dado a que en v6 las preguntas/secciones pueden reutilizar el mismo DataSource con Atributos en orden
							//diferente (de ahí que se generan múltiples catálogos del mismo DataSource), se cortará el filtro por catálogo al encontrar el atributo
							//de la agenda (el último usado como Key) o el usado por la pregunta/sección, esto es así porque luego del usado por la agenda ya se
							//podrían generar múltiples valores en el resto de los atributos, y hasta el usado por la pregunta todos se consideran padres así que sólo
							//habría un único camino a seguir
							$objCatalogValuesCollByLevel = $objCatalogValuesColl;
							$objCatalogFilter = array();
							$objFullCatalogFilter = array();
							$blnAddFilter = true;
							foreach ($objCatalogAttributes as $objCatalogAttribute) {
								//Si no hay elementos en este nivel, ya no puede continuar (eso no debería ser posible)
								if (!is_array($objCatalogValuesColl) || count($objCatalogValuesColl) == 0 || !isset($objCatalogValuesColl[0])) {
									break;
								}
								
								$strValue = (string) @$objCatalogValuesColl[0]["value"];
								$objFullCatalogFilter[] = $strValue;
								
								//Si no hay mas niveles ya no puede continuar (eso no debería ser posible)
								if (!isset($objCatalogValuesColl[0]["children"])) {
									break;
								}
								
								//Cambia de nivel el array
								$objCatalogValuesColl = $objCatalogValuesColl[0]["children"];
								
								//Al encontrar el último DataSourceMember filtrado de la agenda y/o usado por la pregunta, ya no hay necesidad de continuar
								$intAttributeDataSourceMemberID = (int) @$objCatalogAttribute["sourceMemberID"];
								
								//Sólo agrega al CatalogFilter los valores según la condición previa, por eso esta asignación está hasta el final
								if ($blnAddFilter) {
									$objCatalogFilter[] = $strValue;
								}
								
								if ($intAttributeDataSourceMemberID == $intDataSourceMemberID) {
									$blnAddFilter = false;
								}
								
								//El CatalogFilter siempre debe llegar hasta el atributo de la pregunta para que pueda tener una respuesta por default, así que 
								//esta condición no es válida
								//if ($intAttributeDataSourceMemberID == $intLastKeyDataSourceMemberID) {
								//	$blnAddFilter = false;
								//}
							}
							$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogFilter'] = $objCatalogFilter;
							$arrCatalogsByHierarchy[$strAttribIDsStr]['FullCatalogFilter'] = $objFullCatalogFilter;
						}
					}
					
					//Agrega el catálogo a la lista de los que se actualizarán por ser la misma combinación
					$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogIDs'][$intCatalogID] = $intCatalogID;
					$arrCatalogsByHierarchy[$strAttribIDsStr]['CatalogMembersColl'][$intCatalogID] = $objCatalogMembersColl;
				}
				
				//Al terminar procesará todos los catálogos clasificados regresando una instancia de valores por cada uno de ellos
				$arrDef['attributeValues'] = array();
				//Si hay por lo menos un catálogo, se generará una propiedad especial para que el App pueda verificar si este catálogo aplica o no en la agenda, ya que la antigua
				//propiedad de catalogID ahora es realmente un DataSourceID
				$arrDef['catalogIDs'] = array();
				foreach ($arrCatalogsByHierarchy as $strAttribIDsStr => $arrCatData) {
					$intFirstCatalogID = 0;
					if (isset($arrCatData['AttributeValues']) && isset($arrCatData['CatalogIDs'])) {
						$arrGroupedCatalogIDs = $arrCatData['CatalogIDs'];
						foreach ($arrGroupedCatalogIDs as $intCatalogID) {
							if (!$intFirstCatalogID) {
								//Sólo el primer catálogo tendrá la referencia completa de los valores, los demás serán un apuntador al primer catálogo
								$intFirstCatalogID = $intCatalogID;
								$arrDef['attributeValues'][$intCatalogID] = $arrCatData['AttributeValues'];
							}
							else {
								//En este caso se trata de una referencia solamente, esto para no duplicar el mismo array de valores múltiples veces
								$arrDef['attributeValues'][$intCatalogID] = $intFirstCatalogID;
							}
							
							$arrDef['catalogIDs'][] = $intCatalogID;
							//El filtro se manda independientemente de si el catálogo es o no compartido
							if (isset($arrCatData['CatalogFilter'])) {
								$arrDef['catalogFilter'][$intCatalogID] = $arrCatData['CatalogFilter'];
							}
							if (isset($arrCatData['FullCatalogFilter'])) {
								$arrDef['fullCatalogFilter'][$intCatalogID] = $arrCatData['FullCatalogFilter'];
							}
						}
					}
				}
				
				//@JAPR 2015-08-29: Rediseñadas las agendas en v6
				//Ahora las agendas utilizan un DataSource, así que se hace el cambio para ya no usar el Catálogo directamente
				//Se cambió el proceso para reutilizar la instancia de valores que ya se generaron para cada catálogo asociado al DataSource de la agenda, de tal manera
				//que no se tenga que duplicar la carga de dichos datos, el parámetro $anAgendaDef contiene la definición tal como se mandará al App y de ahí se puede
				//obtener la información requerida
				if (getMDVersion() >= esvAgendaWithCatValues && $appVersion >= esvAgendaWithCatValues) {
					$arrAgendaCatValues = $this->getAllDynamicAttributeValues($arrDef);
					if (!is_null($arrAgendaCatValues) && is_array($arrAgendaCatValues) && count($arrAgendaCatValues) > 0) {
						//Este array contendrá un índice por cada Forma donde se hubieran generado valores de catálogo basados en un filtro dinámico
						//de dicha forma, por lo que podrían existir valores diferentes del mismo catálogo entre varias formas, además de un índice
						//con el Id de Forma == 0 para los casos en que mas se repitiera el mismo filtro dinámico de los mismos catálogos, de tal manera
						//que funcione como un tipo de Default por si no viene el catálogo específicamente en una forma determinada
						$arrDef['catalogValues'] = $arrAgendaCatValues;
					}
					else {
						$arrAgendaCatValues = array();
					}
					
					//Al final para ser compatibles con el proceso de la versión anterior, habría que remover todos los valores de catálogos completos si
					//también existían como un catálogo filtrado dinámicamente
				}
				//@JAPR
			}
			
			//@JAPR 2015-08-30: Complementa los datos de la agenda sin necesidad de queries adicionales, asé que la función getAgendaRedesignDefinition
			//ya no es necesaria para dicho proceso
			$arrDef['status'] = $this->Status;
			$arrDef['checkInSurvey'] = $this->CheckinSurvey;
			$arrDef['checkInSurveyStatus'] = $this->CheckinSurvey;
			$arrDef['surveysRadio'] = $this->SurveysRadioForAgenda;
			
			//Los valores de longitud, latitude y los displays, se obtiene a partir de la combinación del filtro de la agenda, la cual es una colección de valores
			//que se debe obtener directo del DataSource (hasta este punto se han cargado según la definición de los catálogos específicos), así que se lanza una
			//consulta para extraer esta información (se podría optimizar el proceso usando cualquiera de las colecciones de valores de los catálogos, ya que en
			//esencia todas se cargan de la misma tabla aunque ordenadas de manera diferente, así que dependiendo de por cual se cargó, se podría presentar un
			//valor u otro lo que llevaría a diferentes displays en determinado momento)
			$objDataSource = BITAMDataSource::NewInstanceWithID($this->Repository, $intDataSourceID);
			if (!is_null($objDataSource)) {
				$objCatalogValuesColl = null;
				//@JAPRWarning: Temporalmente en lo que se corrige bien, se tendrán que regresar todos los atributos
				$intMaxOrder = 0;
				$objCatalogAttributes = BITAMDataSource::GetCatalogAttributes($this->Repository, $intDataSourceID, $intMaxOrder);
				if (is_null($objFirstCatalogValuesColl)) {
					if (!is_null($objCatalogAttributes)) {
						$strDataSourceFilter = $this->getAgendaFilter($intLastKeyDataSourceMemberID);
						$strDataSourceFilter = BITAMDataSource::TranslateAttributesVarsToFieldNames($this->Repository, $intDataSourceID, $strDataSourceFilter);
						//@JAPR 2015-12-22: Se removió el parámetro #5 $attribPosition, ya que no se usaba, fue un mal intento de simular BITAMCatalogFilter
						$objCatalogValuesColl = $objDataSource->getAttributeValues(array_values($objCatalogAttributes), -1, $strDataSourceFilter, null, false, true);
					}
				}
				else {
					//En este caso lo único que hay que hacer es sobreescribir las propiedades del CheckPoint en las instancias de Atributos
					foreach ($objFirstCatalogAttributes as $intMemberID => $objCatMemberDef) {
						$intDataSourceMemberID = (int) @$objCatMemberDef["sourceMemberID"];
						if ($intDataSourceMemberID > 0 && isset($objCatalogAttributes[$intDataSourceMemberID])) {
							$objFirstCatalogAttributes[$intMemberID]["KeyOfAgenda"] = (int) @$objCatalogAttributes[$intDataSourceMemberID]["KeyOfAgenda"];
							$objFirstCatalogAttributes[$intMemberID]["AgendaLatitude"] = (int) @$objCatalogAttributes[$intDataSourceMemberID]["AgendaLatitude"];
							$objFirstCatalogAttributes[$intMemberID]["AgendaLongitude"] = (int) @$objCatalogAttributes[$intDataSourceMemberID]["AgendaLongitude"];
							$objFirstCatalogAttributes[$intMemberID]["AgendaDisplay"] = (int) @$objCatalogAttributes[$intDataSourceMemberID]["AgendaDisplay"];
						}
					}
					$objCatalogAttributes = $objFirstCatalogAttributes;
					$objCatalogValuesColl = $objFirstCatalogValuesColl;
				}
				
				if (!is_null($objCatalogValuesColl)) {
					//Recorre todos los atributos de la colección de valores para obtener los datos que necesita con la primer combinación disponible
					$agendaLatitude = null;
					$agendaLongitude = null;
					$agendaDisplays = array();
					foreach ($objCatalogAttributes as $objCatalogAttribute) {
						//Si no hay elementos en este nivel, ya no puede continuar (eso no debería ser posible)
						if (!is_array($objCatalogValuesColl) || count($objCatalogValuesColl) == 0 || !isset($objCatalogValuesColl[0])) {
							break;
						}
						
						$strValue = (string) @$objCatalogValuesColl[0]["value"];
						
						if ((int) @$objCatalogAttribute["AgendaLatitude"]) {
							$agendaLatitude = (float) $strValue;
						}
						elseif ((int) @$objCatalogAttribute["AgendaLongitude"]) {
							$agendaLongitude = (float) $strValue;
						}
						
						if ((int) @$objCatalogAttribute["AgendaDisplay"]) {
							$agendaDisplays[(string) @$objCatalogAttribute["name"]] = $strValue;
						}
						
						//Si no hay mas niveles ya no puede continuar (eso no debería ser posible)
						if (!isset($objCatalogValuesColl[0]["children"])) {
							break;
						}
						
						//Cambia de nivel el array
						$objCatalogValuesColl = $objCatalogValuesColl[0]["children"];
					}
					
					if (!is_null($agendaLatitude)) {
						$arrDef['latitude'] = $agendaLatitude;
					}
					
					if (!is_null($agendaLongitude)) {
						$arrDef['longitude'] = $agendaLongitude;
					}
					
					if (count($agendaDisplays) > 0) {
						$arrDef['displays'] = $agendaDisplays;
					}
				}
			}
			//@JAPR
		}

		return $arrDef;
	}
	
	//@JAPRDescontinuada en v6
	function getAgendaRedesignDefinition (&$arrDef, $arrCatalogMembers) {
		$arrDef['status'] = $this->Status;
		$arrDef['checkInSurvey'] = $this->CheckinSurvey;
		$arrDef['checkInSurveyStatus'] = $this->CheckinSurvey;

		$objCatalog = BITAMCatalog::NewInstanceWithID($this->Repository, $this->CatalogID);
		if ($objCatalog->eBavelAppID > 0 && $objCatalog->eBavelFormID > 0) {
			//obtenemos los ids de los campos de ebavel
			$fieldsForAgenda = array();
			foreach ($arrCatalogMembers as $aCatalogMember) {
				$eBavelFieldID = $aCatalogMember->eBavelFieldID;
				if ($aCatalogMember->KeyOfAgenda > 0) {
					if (!isset($fieldsForAgenda["KeyOfAgenda"])) {
						$fieldsForAgenda["KeyOfAgenda"] = array();
					}
					$fieldsForAgenda["KeyOfAgenda"][$aCatalogMember->ClaDescrip] = $eBavelFieldID;
				}
				if ($aCatalogMember->AgendaLatitude > 0) {
					$fieldsForAgenda["Latitude"] = $eBavelFieldID . "_L";
				} 
				if ($aCatalogMember->AgendaLongitude > 0) {
					$fieldsForAgenda["Longitude"] = $eBavelFieldID . "_A";
				} 
				if ($aCatalogMember->AgendaDisplay > 0) {
					if (!isset($fieldsForAgenda["Display"])) {
						$fieldsForAgenda["Display"] = array();
					}
					$fieldsForAgenda["Display"][$aCatalogMember->MemberName] = $eBavelFieldID;
				}
			}

			//obtenemos los campo de ebavel (FFRMS_)
			$anBavelFieldIDsColl = BITAMeBavelCatalogValue::GeteBavelCatalogDefinition($this->Repository, $this->CatalogID);
			$ebavelFieldsForAgenda = array();
			$ebavelTypesForAgenda = array(); 	//array con los tipos para saber si poner quote o no

			foreach ($fieldsForAgenda["KeyOfAgenda"] as $eBavelFieldID) {
				foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
					if ($anAttributeInfo['id_fieldform'] == $eBavelFieldID) {
						if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
							$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
							$ebavelTypesForAgenda["KeyOfAgenda"] = array();
						}
						$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
						$ebavelFieldTypeIsNumeric = 1;
						$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
						switch ($intQTypeID) {
							case qtpOpenNumeric:
							case qtpCalc:
								break;
							default:
								$ebavelFieldTypeIsNumeric = 0;
								break;
						}
						$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
						$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
						break;
					}
				}
			}

			foreach ($anBavelFieldIDsColl as $anAttributeInfo) {
				$strSuffix = '';
				$inteBavelFieldDataID = (int) @$anAttributeInfo['dataID'];
				$strEBavelFieldID = (string) @$anAttributeInfo['id'];  			/* FFRMS_ se usa en el where */
				$intEBavelFieldID = (string) @$anAttributeInfo['id_fieldform']; 	/* id num de ebavel */
				if ($inteBavelFieldDataID > ebfdValue) {
					switch ($inteBavelFieldDataID) {
						case ebfdLatitude:
							$strEBavelFieldID .= '_L';
							$intEBavelFieldID .= "_L";
							break;
						case ebfdLongitude:
							$strEBavelFieldID .= '_A';
							$intEBavelFieldID .= "_A";
							break;
					}
				}
				$ebavelFieldTypeIsNumeric = 1;
				$intQTypeID = convertEBavelToEFormsFieldType($anAttributeInfo['tagname']);
				switch ($intQTypeID) {
					case qtpOpenNumeric:
					case qtpCalc:
						break;
					default:
						$ebavelFieldTypeIsNumeric = 0;
						break;
				}
				/*
				if (isset($fieldsForAgenda["KeyOfAgenda"])) {
					if (in_array($intEBavelFieldID, $fieldsForAgenda["KeyOfAgenda"])) {
						if (!isset($ebavelFieldsForAgenda["KeyOfAgenda"])) {
							$ebavelFieldsForAgenda["KeyOfAgenda"] = array();
							$ebavelTypesForAgenda["KeyOfAgenda"] = array();
						}
						$ebavelFieldsForAgenda["KeyOfAgenda"][(count($ebavelFieldsForAgenda["KeyOfAgenda"]))] = $strEBavelFieldID;
						$ebavelTypesForAgenda["KeyOfAgenda"][(count($ebavelTypesForAgenda["KeyOfAgenda"]))] = $ebavelFieldTypeIsNumeric;
					}
				}
				*/
				if (isset($fieldsForAgenda["Latitude"]) && ($fieldsForAgenda["Latitude"] == $intEBavelFieldID)) {
					$ebavelFieldsForAgenda["Latitude"] = $strEBavelFieldID;
				}
				if (isset($fieldsForAgenda["Longitude"]) && ($fieldsForAgenda["Longitude"] == $intEBavelFieldID)) {
					$ebavelFieldsForAgenda["Longitude"] = $strEBavelFieldID;
				}
				if (isset($fieldsForAgenda["Display"])) {
					foreach ($fieldsForAgenda["Display"] as $dkey => $fieldForDisplay) {
						if ($fieldForDisplay == $intEBavelFieldID) {
							if (!isset($ebavelFieldsForAgenda["Display"])) {
								$ebavelFieldsForAgenda["Display"] = array();
							}
							$ebavelFieldsForAgenda["Display"][$dkey] = $strEBavelFieldID;
							break;
						}
					}
				}
			}
			
			//instanciamos el collection
			$strFilterArray = array();
			$curFilter = 0;
			if($this->FilterText) {
				//se genera el where
				$attributeValue = explode('|', $this->FilterText);
				foreach ($ebavelFieldsForAgenda["KeyOfAgenda"] as $ekey => $ebavelfield) {
					//checar si es numerico el campo o no para ponerle quotes en el where
					$ebavelValue = $attributeValue[$curFilter];
					$ebavelValue = ($ebavelTypesForAgenda["KeyOfAgenda"][$ekey]) ? $ebavelValue : $this->Repository->DataADOConnection->Quote($ebavelValue);
					array_push($strFilterArray, ($ebavelfield . " = " . $ebavelValue));
					$curFilter++;
				}
			}
			
			if (count($strFilterArray)) {
				$strFilter = implode(" AND ",$strFilterArray);
				
				$bGetDistinctValues = true;
				$bGetRecordset = false;
				$bApplyFilters = false;
				$bIncludeSystemFields = false;
				
				//$aRS = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
				$ebavelCatalogValuesCollection = BITAMeBavelCatalogValueCollection::NewInstance($this->Repository, $this->CatalogID, $bApplyFilters, $strFilter, -1, $bGetRecordset, $bIncludeSystemFields, $bGetDistinctValues);
				
				//instanciamos con el recordset filtrado que obtuvimos
				//$ebavelCatalogValuesInstance = BITAMeBavelCatalogValue::NewInstanceFromRS($this->Repository, $aRS, $aCatalogID); /* esto ya no */
				
				//var_dump($ebavelCatalogValuesCollection->Collection);
				//exit();
				
				if (!is_null($ebavelCatalogValuesCollection) && isset($ebavelCatalogValuesCollection->Collection[0])) {
					$ebavelCatalogValuesInstance = $ebavelCatalogValuesCollection->Collection[0];
				
					//obtenemos latitud
					$agendaLatitude = '';
					if (isset($ebavelFieldsForAgenda["Latitude"])) {
						$latitudeEBavelField = $ebavelFieldsForAgenda["Latitude"];
						if (property_exists($ebavelCatalogValuesInstance, $latitudeEBavelField)) {
							$agendaLatitude = $ebavelCatalogValuesInstance->$latitudeEBavelField;
						}
					}
					
					//obtenemos longitud
					$agendaLongitude = '';
					if (isset($ebavelFieldsForAgenda["Longitude"])) {
						$longitudeEBavelField = $ebavelFieldsForAgenda["Longitude"];
						if (property_exists($ebavelCatalogValuesInstance, $longitudeEBavelField)) {
							$agendaLongitude = $ebavelCatalogValuesInstance->$longitudeEBavelField;
						}
					}
					
					//obtenemos displays
					$agendaDisplays = array();
					if (isset($ebavelFieldsForAgenda["Display"])) {
						foreach ($ebavelFieldsForAgenda["Display"] as $dkey => $agendaDisplayField) {
							$displaysEBavelField = $agendaDisplayField;
							$agendaDisplays[$dkey] = $ebavelCatalogValuesInstance->$displaysEBavelField;
						}		
					}
					
					//seteamos la definition
					$arrDef['latitude'] = $agendaLatitude;
					$arrDef['longitude'] = $agendaLongitude;
					$arrDef['displays'] = $agendaDisplays;
				}
			}
		} else {
			$fieldsForAgenda = array();
			foreach ($arrCatalogMembers as $aCatalogMember) {
				$fieldName = "DSC_".$aCatalogMember->ClaDescrip;
				if ($aCatalogMember->AgendaLatitude > 0) {
					$fieldsForAgenda["Latitude"] = $fieldName;
				} 
				if ($aCatalogMember->AgendaLongitude > 0) {
					$fieldsForAgenda["Longitude"] = $fieldName;
				} 
				if ($aCatalogMember->AgendaDisplay > 0) {
					if (!isset($fieldsForAgenda["Display"])) {
						$fieldsForAgenda["Display"] = array();
					}
					$fieldsForAgenda["Display"][$aCatalogMember->MemberName] = $fieldName;
				} 
			}
			$fieldAttributeName = "";
			$sqlFields = "";
			
			if($this->FilterText) {
				$arrKeyOfAgendaFields = array();
				foreach ($arrCatalogMembers as $aCatalogMember) {
					if ($aCatalogMember->KeyOfAgenda) {
						$fieldAttributeName = "DSC_".$aCatalogMember->ClaDescrip;
						array_push($arrKeyOfAgendaFields, $fieldAttributeName);
					}
				}
						
				if (count($arrKeyOfAgendaFields)) {
					$sqlFields .= (implode(', ', $arrKeyOfAgendaFields));
				}
			}
					
			if (isset($fieldsForAgenda["Latitude"]) && isset($fieldsForAgenda["Longitude"])) {
				$sqlFields .= (", " . $fieldsForAgenda["Latitude"] . ", " . $fieldsForAgenda["Longitude"]);
			}
			if (isset($fieldsForAgenda["Display"])) {
				foreach ($fieldsForAgenda["Display"] as $dkey => $aDisplay) {
					$sqlFields .= (", " . $fieldsForAgenda["Display"][$dkey]);
				}
			}
					
			$where = " ";
			if ($this->FilterText) {
				$whereArray = array();
				$attributeValue = explode('|', $this->FilterText);
				$countAttributeValue = 0;
				foreach ($arrCatalogMembers as $aCatalogMember) {
					if ($aCatalogMember->KeyOfAgenda) {
						if (isset($attributeValue[$countAttributeValue])) {
							if ($countAttributeValue == 0) {
								$where .= "WHERE ";
							}
							array_push($whereArray, ("DSC_".$aCatalogMember->ClaDescrip . " = " . $this->Repository->DataADOConnection->Quote($attributeValue[$countAttributeValue])));
							$countAttributeValue++;
						}
					}
				}
				$where .= implode($whereArray, ' AND ');
			} else {
				return;
			}
			
			$tableName = $this->CatalogTable;
			$fieldKey = "RIDIM_".$this->CatalogDim."KEY";
			$sql = "SELECT " . $sqlFields." FROM ".$tableName.$where." ORDER BY ".$fieldKey;
			$aRSCat = $this->Repository->DataADOConnection->Execute($sql);
			
			$agendaLatitude = 0;
			$agendaLongitude = 0;
			$agendaDisplays = array();
			if ($aRSCat) {
				if (!$aRSCat->EOF) {
					if (isset($fieldsForAgenda["Latitude"]) && isset($fieldsForAgenda["Longitude"])) {
						if (is_numeric($aRSCat->fields[$fieldsForAgenda["Latitude"]]) && is_numeric($aRSCat->fields[$fieldsForAgenda["Longitude"]])) {
							$agendaLatitude = $aRSCat->fields[$fieldsForAgenda["Latitude"]];
							$agendaLongitude = $aRSCat->fields[$fieldsForAgenda["Longitude"]];
						}
					}
					if (isset($fieldsForAgenda["Display"])) {
						foreach ($fieldsForAgenda["Display"] as $dkey => $aDisplay) {
							$anAgendaDisplay = @$aRSCat->fields[$fieldsForAgenda["Display"][$dkey]];
							$agendaDisplays[$dkey] = $anAgendaDisplay;
						}
					}
				}
			}
			
			$arrDef['latitude'] = $agendaLatitude;
			$arrDef['longitude'] = $agendaLongitude;
			$arrDef['displays'] = $agendaDisplays;
		}	
		$arrDef['surveysRadio'] = $this->SurveysRadioForAgenda;
	}
}

class BITAMSurveyAgendaCollection extends BITAMCollection
{
	public $UserID;
	public $CatalogID;
	public $StartDate;
	public $EndDate;
	public $ipp;				//Items per page
	public $Pages;
	
	function __construct($aRepository)
	{
		BITAMCollection::__construct($aRepository);
		
		$this->StartDate = "";
		$this->EndDate = "";
		$this->UserID = -2;
		$this->CatalogID = 0;
		$this->ipp = 100;
		$this->Pages = null;
	}
	
	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	static function NewInstance($aRepository, $anArrayOfAgendaIDs = null, $anArrayOdUserID = null, $anEndDate = null, $aTime = null, $bJustPending = false, $aStartDate = null, $aCatalogID = null, $ipp = 100, $bUsePaginator = false, $attributes = null, $searchString = null, $statusID = null, $CheckPointID = null)
	{
		$anInstance = new BITAMSurveyAgendaCollection($aRepository);
		//@JAPR 2015-10-26: Corregido un bug, no estaba considerando la posibilidad de que este parámetro fuera un array
		$anInstance->UserID = ((is_null($anArrayOdUserID) || is_array($anArrayOdUserID))?-2:$anArrayOdUserID);
		//@JAPR
		$anInstance->CatalogID = $aCatalogID;
		$anInstance->ipp = $ipp;
		$where = "";
		$strAnd = '';
		
		//28Ene2013		
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$agStartDate = $aStartDate;
		$agEndDate = $anEndDate;
		$agUserID = $anArrayOdUserID;
		$agIpp = $ipp;
		$agCatalogID = $aCatalogID;
		
		if (!is_null($anArrayOfAgendaIDs))
		{
			if (!is_array($anArrayOfAgendaIDs))
			{
				$anArrayOfAgendaIDs = array($anArrayOfAgendaIDs);
			}
			
			switch (count($anArrayOfAgendaIDs))
			{
				case 0:
					break;
				case 1:
					$where = " t1.AgendaID = ".((int) $anArrayOfAgendaIDs[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($anArrayOfAgendaIDs as $anAgendaID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anAgendaID; 
					}
					if ($where != "")
					{
						$where = " t1.AgendaID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($anArrayOdUserID))
		{
			if (!is_array($anArrayOdUserID))
			{
				//@JAPR 2015-10-26: Corregido un bug, no estaba considerando la posibilidad de que este parámetro fuera un array
				$anArrayOdUserID = (int) @$anArrayOdUserID;
				if ($anArrayOdUserID <= 0) {
					//En este caso no es un ID de usuario válido, así que se genera vacío para que no filtre la colección
					$anArrayOdUserID = array();
				}
				else {
					$anArrayOdUserID = array($anArrayOdUserID);
				}
				//@JAPR
			}
			
			switch (count($anArrayOdUserID))
			{
				case 0:
					$anInstance->UserID = -2;
					break;
				case 1:
					
					if($anArrayOdUserID[0]!=-2)
					{
						$where .= $strAnd." t1.UserID = ".((int) $anArrayOdUserID[0]);
						$strAnd = " AND ";
					}

					$anInstance->UserID = (int) $anArrayOdUserID[0];
					break;
				default:
					$in = "";
					foreach ($anArrayOdUserID as $anID)
					{
						if ((int) $anID > 0)
						{
							if ($in != "")
							{
								$in .= ", ";
							}
							else 
							{
								$anInstance->UserID = $anID;
							}
							$in .= (int)$anID; 
						}
					}
					if ($in != "")
					{
						$where .= $strAnd." t1.UserID IN (".$in.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		//28Ene2013: Faltaba filtro de catálogo
		if($anInstance->CatalogID!=null)
		{
			$where .= $strAnd." t1.CatalogID = ".$anInstance->CatalogID;
			$strAnd = " AND ";
		}
		
		//@JAPR 2012-03-16: Agregada la vista tipo Reporte con filtros para la colección
		$anInstance->StartDate = $aStartDate;
		$anInstance->EndDate = $anEndDate;
		
		//28Ene2013: Faltaba filtro por campo "FROM" (fecha inicial)
		if (trim($aStartDate)!="" && substr($aStartDate,0,10)!="0000-00-00")
		{
			$where .= $strAnd." t1.CaptureStartDate >= ".$aRepository->DataADOConnection->DBTimeStamp($aStartDate);
			$strAnd = " AND ";
		}
		
		//@JAPR 2012-03-14: Agregado el filtro por Fecha y Hora
		if (trim($anEndDate)!="" || !is_null($aTime))
		{
			if (trim($anEndDate)!="" && substr($anEndDate,0,10)!="0000-00-00" && !is_null($aTime))
			{
				$where .= $strAnd." (t1.CaptureStartDate < ".$aRepository->DataADOConnection->DBTimeStamp($anEndDate).
					" OR (t1.CaptureStartDate = ".$aRepository->DataADOConnection->DBTimeStamp($anEndDate)." AND ".
						" t1.CaptureStartTime <= ".$aRepository->DataADOConnection->Quote($aTime).") )";
				$strAnd = " AND ";
			}
			elseif (trim($anEndDate)!="" && substr($anEndDate,0,10)!="0000-00-00")
			{
				$where .= $strAnd." t1.CaptureStartDate <= ".$aRepository->DataADOConnection->DBTimeStamp($anEndDate);
				$strAnd = " AND ";
			}
			else 
			{
				$where .= $strAnd." t1.CaptureStartTime <= ".$aRepository->DataADOConnection->Quote($aTime);
				$strAnd = " AND ";
			}
		}
		
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		if ($bJustPending) {
			$where .= $strAnd." t1.AgendaID NOT IN ( 
				SELECT DISTINCT AgendaID 
				FROM SI_SV_SurveyAgendaDet 
				WHERE Status = ".agdRescheduled.")";
			$strAnd = " AND ";
		}
		//@JAPR
		
		if (!is_null($attributes)) {
		
			$strRLIKE = "";
			$countAttributes = 0;
			foreach ($attributes as $attribute) {
				$tmpAttribute = array();
				foreach ($attribute as $attrValue) {
					if ($attrValue == "") {
						array_push($tmpAttribute, '.*');
					} else {
						array_push($tmpAttribute, $attrValue);
					}
				}
				//@AAL 24/04/2015: Modificado por que estaba mal formando el query al armar el filtro
				if (count($tmpAttribute)) {
					$strRLIKE = ("(".implode("|", $tmpAttribute).")[|]?");
				}
				if ($strRLIKE != "") {
					$where .= $strAnd." t1.FilterText RLIKE " . $aRepository->DataADOConnection->Quote($strRLIKE);
					if (count($attributes) < $countAttributes) {
						$where .= " OR ";
					}
				}
				$countAttributes++;
			}
			if ($strRLIKE != "") {
				$strAnd = " AND ";
			}
		}
		
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		//Filtra sólo por agendas de la versión 6
		$where .= $strAnd." t1.CreationVersion >= ".esveAgendas;
		$strAnd = " AND ";
		//@JAPR

		if( !is_null( $CheckPointID ) )
		{
			$where .= $strAnd." t1.checkpointID " . ( is_array( $CheckPointID ) ? ' IN ('. implode(',', $CheckPointID) .') ' : ' = ' . $CheckPointID ) ;
		}
		
		if ($where != '')
		{
			$where = "WHERE ".$where;
		}
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$strFieldsByVersion = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strFieldsByVersion .= ', t1.VersionNum ';
		}
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		//@AAL 15/05/2015 Corregido Issue #0AWCEA: No se estaba considerando la hora en que se capturaba la agenda.
		if (getMDVersion() >= esvAgendaRedesign) {
			$strFieldsByVersion .= ', t1.CheckinSurvey, t1.Status, t1.EntryCheckInStartDate ';
		}
		//@JAPR
		$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, t2.DataSourceName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, t1.FilterTextAttr, 
					t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID, t1.checkpointID, t5.name as CheckPointName $strFieldsByVersion 
				FROM SI_SV_SurveyAgenda t1 
					LEFT OUTER JOIN si_sv_datasource t2 ON t1.CatalogID = t2.DataSourceID 
					LEFT OUTER JOIN si_sv_datasourcemember t3 ON t1.CatalogID = t3.DataSourceID AND t1.AttributeID = t3.MemberID 
					LEFT OUTER JOIN si_sv_checkpoint t5 ON t1.checkpointID = t5.id
					".$where.	//LEFT OUTER JOIN SI_SV_Survey t4 ON t1.SurveyID = t4.SurveyID
				" ORDER BY t1.UserID, t2.DataSourceName, t3.MemberName, t1.FilterText";
		
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		//@JAPR 2012-03-16: Agregada la vista tipo Reporte con filtros para la colección
		if ($bUsePaginator)
		{
			//Total de Registros
			$numRows = $aRS->RecordCount();
			
			$_GET["ipp"] = $ipp;
			
			if(!isset($_GET["page"]))
			{
				$_GET["page"] = 1;
			}
			
			if($numRows>0)
			{
				//Creamos el objeto de paginacion
				$anInstance->Pages = new Paginator();
				$anInstance->Pages->items_total = $numRows;
				$anInstance->Pages->items_per_page = $ipp;
				$anInstance->Pages->mid_range = 9;
				$anInstance->Pages->paginate();
				
				//Agregamos el elemento LIMIT al SELECT para obtener los registros que se requieren
				$sql .= $anInstance->Pages->limit;
				
				$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
				
				if ($aRS === false)
				{
					die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda, SI_SV_Catalog, SI_SV_CatalogMember ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
				}
				else 
				{
					$anInstance->Pages->current_num_items = $aRS->RecordCount();
				}
			}
		}
		//@JAPR
		
		$arrUserIDs = array();
		$arrAgendas = array();
		while (!$aRS->EOF)
		{
			//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
			//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
			$anInstanceAgenda = BITAMSurveyAgenda::NewInstanceFromRS($anInstance->Repository, $aRS, $bJustPending);
			if (!$bJustPending || count($anInstanceAgenda->SurveyIDs) > 0)
			{
				$anInstance->Collection[] = $anInstanceAgenda;
				$arrUserIDs[$anInstanceAgenda->UserID] = $anInstanceAgenda->UserID;
			}
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$arrAgendas[$anInstanceAgenda->AgendaID] = $anInstanceAgenda;
			//@JAPR
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
		if (count($arrUserIDs) > 0)
		{
			$sql = "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO IN (".implode(',', $arrUserIDs).')';
			$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
			if ($aRS)
			{
				while (!$aRS->EOF)
				{
					$intUserID = $aRS->fields["cla_usuario"];
					$arrUserIDs[$intUserID] = (string) $aRS->fields["nom_largo"];
					$aRS->MoveNext();
				}
			}
			
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda)
			{
				$anInstanceAgenda->UserName = (string) @$arrUserIDs[$anInstanceAgenda->UserID];
			}
		}
		
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		//Ahora el estado de la agenda se captura directo en ella misma, ya no depende del estado de cada encuesta
		if(getMDVersion() < esvAgendaRedesign) {
			if ($where != '')
			{
				$where = "AND ".substr($where, 6);
			}
			$sql = "SELECT A.AgendaID, A.Status, B.EnableReschedule 
				FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet A, SI_SV_Survey B 
				WHERE t1.AgendaID = A.AgendaID AND A.SurveyID = B.SurveyID ".$where." 
				ORDER BY A.AgendaID, A.Status DESC";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$intAgendaID = (int) $aRS->fields['agendaid'];
				$intStatus = (int) $aRS->fields['status'];
				switch ($intStatus)
				{
					case agdRescheduled:
						//Si hay por lo menos una encuesta recalendarizada, se considera toda la agenda como recalendarizada
						$anInstanceAgenda = @$arrAgendas[$intAgendaID];
						if (!is_null($anInstanceAgenda))
						{
							$anInstanceAgenda->Status = $intStatus;
							unset($arrAgendas[$intAgendaID]);
						}
						break;
				
					case agdOpen:
						//Si hay por lo menos una encuesta sin contestar y que NO esté configurada para recalendarizar dicha encuesta, se considera 
						//toda la agenda como pendiente de contestar
						if (((int) $aRS->fields['enablereschedule']) == 0)
						{
							$anInstanceAgenda = @$arrAgendas[$intAgendaID];
							if (!is_null($anInstanceAgenda))
							{
								$anInstanceAgenda->Status = $intStatus;
								unset($arrAgendas[$intAgendaID]);
							}
							break;
						}
						//En este caso es una encuesta no contestada pero que permite reagendar, así que esa no cuenta
						break;
					
					default:
						break;
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
		
		if(!is_null($searchString) && $searchString != '') {
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda) {
				$arr = array();
				array_push($arr, $anInstanceAgenda->UserName);
				array_push($arr, $anInstanceAgenda->CatalogName);
				array_push($arr, $anInstanceAgenda->FilterText);
				array_push($arr, $anInstanceAgenda->CaptureStartDate);
				array_push($arr, $anInstanceAgenda->getAgendaTime());
				array_push($arr, $anInstanceAgenda->getAgendaStatus());
				$foundString = false;
				foreach ($arr as $value) {
					if (strpos(strtolower($value), strtolower($searchString)) !== false) {
						$foundString = true;
					}
				}
				if (!$foundString) {
					unset($anInstance->Collection[$aKey]);
				}
			}
		}
		
		if(!is_null($statusID) && $statusID != '') {
			//echo($statusID);
			//exit();
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda) {
				//$arrStatus = explode("|", $statusID);
				$foundStatus = false;
				foreach ($statusID as $statusValue) {
					if ($anInstanceAgenda->Status == $statusValue) {
						$foundStatus = true;
						break;
					}
				}
				if (!$foundStatus) {
					unset($anInstance->Collection[$aKey]);
				}
			}
		
		}
				
		return $anInstance;
	}
	
	static function NewInstanceUniquePerDay($aRepository, $userID, $aDate, $catalogID, $attributeID, $filterText)
	{
		$bJustPending = false;
		$anInstance = new BITAMSurveyAgendaCollection($aRepository);
		
		$where = "";
		$strAnd = '';
		if (!is_null($catalogID))
		{
			if (!is_array($catalogID))
			{
				$catalogID = array($catalogID);
			}
			
			switch (count($catalogID))
			{
				case 0:
					break;
				case 1:
					$where = " t1.CatalogID = ".((int) $catalogID[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($catalogID as $anID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anID; 
					}
					if ($where != "")
					{
						$where = " t1.CatalogID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		//@JAPR 2014-11-03: Modificado el esquema de agendas
		//Ya no se agregará el campo de CatalogAttribute
		if (!is_null($attributeID) && $attributeID > 0)
		{
			if (!is_array($attributeID))
			{
				$attributeID = array($attributeID);
			}
			
			switch (count($attributeID))
			{
				case 0:
					break;
				case 1:
					$where = " t1.AttributeID = ".((int) $attributeID[0]);
					$strAnd = " AND ";
					break;
				default:
					foreach ($attributeID as $anID)
					{
						if ($where != "")
						{
							$where .= ", ";
						}
						$where .= (int)$anID; 
					}
					if ($where != "")
					{
						$where = " t1.AttributeID IN (".$where.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($userID))
		{
			if (!is_array($userID))
			{
				$userID = array($userID);
			}
			
			switch (count($userID))
			{
				case 0:
					break;
				case 1:
					$where .= $strAnd." t1.UserID = ".((int) $userID[0]);
					$strAnd = " AND ";
					$anInstance->UserID = (int) $userID[0];
					break;
				default:
					$in = "";
					foreach ($userID as $anID)
					{
						if ((int) $anID > 0)
						{
							if ($in != "")
							{
								$in .= ", ";
							}
							else 
							{
								$anInstance->UserID = $anID;
							}
							$in .= (int)$anID; 
						}
					}
					if ($in != "")
					{
						$where .= $strAnd." t1.UserID IN (".$in.")";
						$strAnd = " AND ";
					}
					break;
			}
		}
		
		if (!is_null($aDate) && substr($aDate,0,10)!="0000-00-00")
		{
			$where .= $strAnd." t1.CaptureStartDate = ".$aRepository->DataADOConnection->DBTimeStamp($aDate);
			$strAnd = " AND ";
		}
		
		if (!is_null($filterText) && $filterText != '')
		{
			$where .= $strAnd." t1.FilterText = ".$aRepository->DataADOConnection->Quote($filterText);
			$strAnd = " AND ";
		}
		
		//@JAPR 2015-08-27: Rediseñadas las agendas en v6
		//Filtra sólo por agendas de la versión 6
		$where .= $strAnd." t1.CreationVersion >= ".esveAgendas;
		$strAnd = " AND ";
		//@JAPR
		
		if ($where != '')
		{
			$where = "WHERE ".$where;
		}
		//@JAPR 2012-08-08: Agregada la versión de definición de las agendas (campo VersionNum)
		$strFieldsByVersion = "";
		if (getAppVersion() >= esvMultiDynamicAndStandarization) {
			$strFieldsByVersion .= ', t1.VersionNum ';
		}
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		if(getMDVersion() >= esvAgendaRedesign) {
			$strFieldsByVersion .= ', t1.CheckinSurvey, t1.Status ';
		}
		//@JAPR
		//GCRUZ 2015-09-04. Se cambia SI_SV_Catalog y SI_SV_CatalogMember por SI_SV_Datasource y SI_SV_DatasourceMember
		$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, 0 AS ParentID, t2.DataSourceName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, 
					t1.CaptureStartDate, t1.CaptureStartTime, t1.UserID, t1.LastDateID $strFieldsByVersion 
				FROM SI_SV_SurveyAgenda t1 
					LEFT OUTER JOIN SI_SV_Datasource t2 ON t1.CatalogID = t2.DataSourceID 
					LEFT OUTER JOIN SI_SV_DataSourceMember t3 ON t1.CatalogID = t3.DataSourceID AND t1.AttributeID = t3.MemberID 
					".$where.	//LEFT OUTER JOIN SI_SV_Survey t4 ON t1.SurveyID = t4.SurveyID
				" ORDER BY t1.UserID, t2.DataSourceName, t3.MemberName, t1.FilterText";
//		$sql = "SELECT t1.AgendaID, t1.AgendaName, t1.CatalogID, t2.ParentID, t2.CatalogName, t2.TableName, t1.AttributeID, t3.MemberName, t1.FilterText, 
//					LEFT OUTER JOIN SI_SV_Catalog t2 ON t1.CatalogID = t2.CatalogID 
//					LEFT OUTER JOIN SI_SV_CatalogMember t3 ON t1.CatalogID = t3.CatalogID AND t1.AttributeID = t3.MemberID 
//				" ORDER BY t1.UserID, t2.CatalogName, t3.MemberName, t1.FilterText";
				
		//var_dump($sql);
		//exit();
				
		$aRS = $anInstance->Repository->DataADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgenda ".translate("table").": ".$anInstance->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		$arrUserIDs = array();
		$arrAgendas = array();
		while (!$aRS->EOF)
		{
			$anInstanceAgenda = BITAMSurveyAgenda::NewInstanceFromRS($anInstance->Repository, $aRS, $bJustPending);
			if (!$bJustPending || count($anInstanceAgenda->SurveyIDs) > 0)
			{
				$anInstance->Collection[] = $anInstanceAgenda;
				$arrUserIDs[$anInstanceAgenda->UserID] = $anInstanceAgenda->UserID;
			}
			//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
			$arrAgendas[$anInstanceAgenda->AgendaID] = $anInstanceAgenda;
			//@JAPR
			$aRS->MoveNext();
		}
		
		//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
		if (count($arrUserIDs) > 0)
		{
			$sql = "SELECT CLA_USUARIO, NOM_LARGO FROM SI_USUARIO WHERE CLA_USUARIO IN (".implode(',', $arrUserIDs).')';
			$aRS = $anInstance->Repository->ADOConnection->Execute($sql);
			if ($aRS)
			{
				while (!$aRS->EOF)
				{
					$intUserID = $aRS->fields["cla_usuario"];
					$arrUserIDs[$intUserID] = (string) $aRS->fields["nom_largo"];
					$aRS->MoveNext();
				}
			}
			
			foreach ($anInstance->Collection as $aKey => $anInstanceAgenda)
			{
				$anInstanceAgenda->UserName = (string) @$arrUserIDs[$anInstanceAgenda->UserID];
			}
		}
		
		//@JAPR 2012-06-04: Agregada la opción para recalendarizar las Agendas de trabajo en base a las encuestas contestadas
		//@JAPR 2014-11-07: Modificado el esquema de Agendas. Ahora las agendas ya tienen un Status propio en la tabla SI_SV_SurveyAgenda
		//Ahora el estado de la agenda se captura directo en ella misma, ya no depende del estado de cada encuesta
		if(getMDVersion() < esvAgendaRedesign) {
			if ($where != '')
			{
				$where = "AND ".substr($where, 6);
			}
			$sql = "SELECT A.AgendaID, A.Status, B.EnableReschedule 
				FROM SI_SV_SurveyAgenda t1, SI_SV_SurveyAgendaDet A, SI_SV_Survey B 
				WHERE t1.AgendaID = A.AgendaID AND A.SurveyID = B.SurveyID ".$where." 
				ORDER BY A.AgendaID, A.Status DESC";
			$aRS = $aRepository->DataADOConnection->Execute($sql);
			if ($aRS === false)
			{
				die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_SurveyAgendaDet ".translate("table").": ".$aRepository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
			}
			
			while (!$aRS->EOF)
			{
				$intAgendaID = (int) $aRS->fields['agendaid'];
				$intStatus = (int) $aRS->fields['status'];
				switch ($intStatus)
				{
					case agdRescheduled:
						//Si hay por lo menos una encuesta recalendarizada, se considera toda la agenda como recalendarizada
						$anInstanceAgenda = @$arrAgendas[$intAgendaID];
						if (!is_null($anInstanceAgenda))
						{
							$anInstanceAgenda->Status = $intStatus;
							unset($arrAgendas[$intAgendaID]);
						}
						break;
				
					case agdOpen:
						//Si hay por lo menos una encuesta sin contestar y que NO esté configurada para recalendarizar dicha encuesta, se considera 
						//toda la agenda como pendiente de contestar
						if (((int) $aRS->fields['enablereschedule']) == 0)
						{
							$anInstanceAgenda = @$arrAgendas[$intAgendaID];
							if (!is_null($anInstanceAgenda))
							{
								$anInstanceAgenda->Status = $intStatus;
								unset($arrAgendas[$intAgendaID]);
							}
							break;
						}
						//En este caso es una encuesta no contestada pero que permite reagendar, así que esa no cuenta
						break;
					
					default:
						break;
				}
				$aRS->MoveNext();
			}
		}
		//@JAPR
		
		return $anInstance;
	}
	
	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	static function NewInstanceByUser($aRepository, $userID, $bJustPending = false)
	{
		return BITAMSurveyAgendaCollection::NewInstance($aRepository, null, array($userID), $bJustPending);
	}
	
	//@JAPR 2012-03-014: Agregado el parámetro $bJustPending para regresar sólo las encuestas que aun no han sido capturadas para esta
	//agenda, de manera que sólo se regrese lo realmente pendiente a los móviles
	static function NewInstanceByUserAndDateTime($aRepository, $userID, $aDate = null, $aTime = null, $bJustPending = false)
	{
		return BITAMSurveyAgendaCollection::NewInstance($aRepository, null, array($userID), $aDate, $aTime, $bJustPending);
	}
	
	static function PerformHTTPRequest($aRepository, $aHTTPRequest, $aUser)
	{
		//@JAPR 2014-11-07: Agregado el proceso de recalendarización de agendas
		//Como ya se reciben los IDs específicos de las agendas a cambiar, se carga la colección en base a ellos. Al final dejará continuar
		//la carga normal para forzar a un refresh a partir de los filtros recibidos
		$intReschedule = getParamValue('reschedule', 'both', '(int)');
		if ($intReschedule) {
			$strAgendaRescheduleID = @$_POST['agendaRescheduleID'];
			$strNewStartDate = getParamValue('newStartDate', 'both', '(string)');
			$intNewStartHour = getParamValue('newStartHour', 'both', '(int)');
			$intNewStartMinutes = getParamValue('newStartMinutes', 'both', '(int)');
			$strToday = date('Ymd');
			$strRescheduleDate = date('Ymd', strtotime($strNewStartDate));
			if (!is_null($strAgendaRescheduleID) && $strAgendaRescheduleID && $strNewStartDate && $strRescheduleDate > $strToday) {
				$arrAgendaRescheduleID = @explode(',', $strAgendaRescheduleID);
				if (is_array($arrAgendaRescheduleID) && count($arrAgendaRescheduleID) > 0) {
					$objAgendaColl = BITAMSurveyAgendaCollection::NewInstance($aRepository, $arrAgendaRescheduleID);
					if (!is_null($objAgendaColl)) {
						foreach ($objAgendaColl->Collection as $objAgenda) {
							//Valida que no se pueda cambiar una agenda cuya fecha es <= que la actual, ya que podría ser una agenda que
							//se esté capturando o ya es una histórica
							if (is_null($objAgenda->CaptureStartDate) || $objAgenda->CaptureStartDate == '') {
								continue;
							}
							$strAgendaDate = date('Ymd', strtotime($objAgenda->CaptureStartDate));
							if ($strAgendaDate < $strToday) {
								continue;
							}
							
							//Primero tiene que editar la agenda que se va a recalendarizar para cambiar su Status (no importa como quedaron los
							//Status de sus encuestas)
							$objAgenda->Status = agdRescheduled;
							$objAgenda->save();
							
							//Posteriormente se debe forzar a grabarla como nueva agenda pero esta vez limpiando el estado de todas sus encuestas,
							//ya con la nueva fecha de captura
							$objAgenda->AgendaID = -1;
							foreach ($objAgenda->surveyIDsStatus as $intSurveyID => $intStatus) {
								$objAgenda->surveyIDsStatus[$intSurveyID] = agdOpen;
							}
							$objAgenda->Status = agdOpen;
							$objAgenda->CaptureStartDate = $strNewStartDate;
							$objAgenda->CaptureStartTime_Hours = $intNewStartHour;
							$objAgenda->CaptureStartTime_Minutes = $intNewStartMinutes;
							$objAgenda->save();
						}
					}
				}
			}
		}
		//@JAPR
		
		$userID=null;
		if (array_key_exists("userID", $aHTTPRequest->POST))
		{
			if (getMDVersion() >= esvAgendaRedesign) {
				if ($aHTTPRequest->POST["userID"] != "") {
					$arrUsers = explode("|", $aHTTPRequest->POST["userID"]);
					$userID = $arrUsers;
				}
			} else {
				$userID = (int)$aHTTPRequest->POST["userID"];
			}
		}
		
		$statusID=null;
		if (array_key_exists("statusID", $aHTTPRequest->POST))
		{
			if (getMDVersion() >= esvAgendaRedesign) {
				if ($aHTTPRequest->POST["statusID"] != "") {
					$arrUsers = explode("|", $aHTTPRequest->POST["statusID"]);
					$statusID = $arrUsers;
				}
			} else {
				$statusID = (int)$aHTTPRequest->POST["statusID"];
			}
		}
		
		$catalogID=null;
		if (array_key_exists("catalogID", $aHTTPRequest->POST))
		{
			if($aHTTPRequest->POST["catalogID"] != 0)
			{
				//var_dump($aHTTPRequest->POST["catalogID"]);
				//exit();
				$catalogID = (int)$aHTTPRequest->POST["catalogID"];
			}
		}
		
		$attributesID = null;
		if (array_key_exists("attributesID", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["attributesID"] != "") {
				$attributesID = json_decode($aHTTPRequest->POST["attributesID"]);
			}
		}
		
		$searchString = null;
		if (array_key_exists("searchString", $aHTTPRequest->POST)) {
			if ($aHTTPRequest->POST["searchString"] != "") {
				$searchString = @$aHTTPRequest->POST["searchString"];
			}
		}
		
		$startDate=null;
		if (array_key_exists("startDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["startDate"])!="" && substr($aHTTPRequest->POST["startDate"],0,10)!="0000-00-00")
			{
				$startDate = trim($aHTTPRequest->POST["startDate"]);
			}
		}

		$endDate=null;
		if (array_key_exists("endDate", $aHTTPRequest->POST))
		{
			if(trim($aHTTPRequest->POST["endDate"])!="" && substr($aHTTPRequest->POST["endDate"],0,10)!="0000-00-00")
			{
				$endDate = trim($aHTTPRequest->POST["endDate"]);
			}
		}
		
		$ipp=100;
		if (array_key_exists("ipp", $aHTTPRequest->POST))
		{
			$ipp = (int)$aHTTPRequest->POST["ipp"];
		}
		
		//var_dump("(".json_encode(BITAMSurveyAgendaCollection::NewInstance($aRepository, null, $userID, $endDate, null, null,$startDate, $catalogID, $ipp, null)).")");
				
		return BITAMSurveyAgendaCollection::NewInstance($aRepository, null, $userID, $endDate, null, null,$startDate, $catalogID, $ipp, null, $attributesID, $searchString, $statusID);
	}
	
	function get_Parent()
	{
		return $this->Repository;
	}
	
	function get_Title()
	{	
		return translate("Agendas");
	}

	function get_QueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}
		
		return "BITAM_SECTION=SurveyAgendaCollection".$strParameters;
	}
	
	function get_AddRemoveQueryString()
	{
		global $agStartDate;
		global $agEndDate;
		global $agUserID;
		global $agIpp;
		global $agCatalogID;

		$strParameters = "";

		if(!is_null($agStartDate))
		{
			$strParameters.="&startDate=".$agStartDate;
		}

		if(!is_null($agEndDate))
		{
			$strParameters.="&endDate=".$agEndDate;
		}

		if(!is_null($agUserID))
		{
			$strParameters.="&userID=".$agUserID;
		}
		
		if(!is_null($agIpp))
		{
			$strParameters.="&ipp=".$agIpp;
		}
		
		if(!is_null($agCatalogID))
		{
			$strParameters.="&catalogID=".$agCatalogID;
		}
		
		return "BITAM_PAGE=SurveyAgenda".$strParameters;
	}
	
	function get_PathArray()
	{
		$pathArray = array();
		$pathArray[] = array($this->get_Title(), $this->get_QueryString());
		return $pathArray;
	}

	function get_FormIDFieldName()
	{
		return 'AgendaID';
	}

	function get_FormFields($aUser)
	{
		require_once("formfield.inc.php");
		
		$myFields = array();
		
		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getAgendaName";
		$aField->Title = translate("Name");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		*/
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "RescheduleStatus";
		$aField->Title = translate("");
		$aField->Type = "String";
		$myFields[$aField->Name] = $aField;
		
		//@JAPR 2012-03-15: Agregado el nombre del usuario para cada encuesta
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getUserName";
		$aField->Title = translate("User");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CatalogName";
		$aField->Title = translate("Catalog");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		/*
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "MemberName";
		$aField->Title = translate("Attribute");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		*/
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "FilterText";
		$aField->Title = translate("Attribute value");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "CaptureStartDate";
		$aField->Title = translate("Date");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getAgendaTime";
		$aField->Title = translate("Time");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getAgendaStatus";
		$aField->Title = translate("Status");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;

		//@JAPR 2014-11-03: Agregada la hora de captura real de la encuesta de CheckIn de cada Agenda
		$aField = BITAMFormField::NewFormField();
		$aField->Name = "getCheckInTime";
		$aField->Title = translate("CheckIn time");
		$aField->Type = "String";
		$aField->Size = 255;
		$myFields[$aField->Name] = $aField;
		//@JAPR
		
		return $myFields;
	}
	
	function isEmptyEndDate()
	{
		return is_null($this->EndDate) || trim($this->EndDate) == "" || substr($this->EndDate,0,10) == "0000-00-00";
	}
	
	function isEmptyStartDate()
	{
		return is_null($this->StartDate) || trim($this->StartDate) == "" || substr($this->StartDate,0,10) == "0000-00-00";
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canAdd($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;	
		}
	}
	
	//@JAPR 2015-02-09: Agregado soporte para php 5.6
	function canRemove($aUser)
	{
		if($_SESSION["PABITAM_UserRole"]==1)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	
	function afterTitleCode ($aUser)
	{
	
		?>
		<div id="boxFilter" style="float:left; width: 190px; height: 100%; background:white;border-right: 1px solid lightgray"></div>
		<?
	
		require_once("catalog.inc.php");
		/*Este código no aplica para el caso de Agendas, se podría aplicar para filtrar por el estado de la Agenda pero eso hasta que
			se implementen los estados mencionados por DABdo el 2012-03-15 (-1=Nueva definición (no enviada a la App), 0=Enviada a App pero no
		//$surveyCollection = BITAMSurveyCollection::NewInstance($this->Repository);
		//$numSurveys = count($surveyCollection->Collection);
		*/
		
		$maxCell = 4;
		//$numColSpanForm = 2;
		$totalColSpanTable = 1 + ($maxCell*2);
		
		if($this->UserID != -2 || !$this->isEmptyStartDate() || !$this->isEmptyEndDate() || $this->ipp != 100)
		{
			$imgSrcClear = "images/sub_cancelarfiltro.gif";
			$lblApply = translate("Reapply");
		}
		else 
		{
			$imgSrcClear = "images/sub_cancelarfiltro_disabled.gif";
			$lblApply = translate("Apply");
		}
		
		$arrayFilters = array();
		$applyFilter = '<a href="javascript:applyFilterAttrib();"><img src="images/sub_aplicarfiltro.gif" alt="'.translate("Apply filter").'" style="cursor:hand">&nbsp;<span id="lblApply">'.$lblApply.'</span></a>&nbsp;&nbsp;';
		$clearFilter = '<a href="javascript:clearFilter();"><img src="'.$imgSrcClear.'" alt="'.translate("Clear Filters").'" style="cursor:hand">&nbsp;<span id="lblClear">'.translate("Clear").'</span></a>&nbsp;&nbsp;';
		
		$arrayFilters[0]=$applyFilter;
		$arrayFilters[1]=$clearFilter;
		
		$arrayButtons = array();		
		/*
		$btnMapData='<span id="btnMapData" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="googleMap('.$this->SurveyID.');"><img src="images/map.gif">&nbsp;'.translate("Map data").'</span>';
		$btnZipData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="zipReport();"><img src="images/zip.gif">&nbsp;'.translate("Download images").'</span>';
		$btnExportData='<span class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="exportReport();"><img src="images/download.gif">&nbsp;'.translate("Export data").'</span>';
		$btnEditAudit='<span id="btnEditLog" style="display:inline" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:editAudit();"><img src="images/edit.png">&nbsp;'.translate("Edit log").'</span>';
		$btnEditAudit.='<span id="btnAuditTrail" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:return false;"><img src="images/edit.png">&nbsp;'.translate("Audit trail").'</span>';
		
		$arrayButtons[0]=$btnMapData;
		$arrayButtons[1]=$btnZipData;
		$arrayButtons[2]=$btnExportData;
		$arrayButtons[3]=$btnEditAudit;
		$btnDelEntries = '<span id="btnDelEntries" style="display:none" class="alinkleft" onmouseover="aChange(this, \'alinkescfavhoveredleft\')" onmouseout="aChange(this, \'alinkescfavleft\')" onclick="javascript:BITAMReportCollection_Remove();"><img src="images/delete.png">&nbsp;'.translate("Delete entries").'</span>';
		*/
		$btnDelEntries = "";
		
		$arrFilterAttributes = array();
		$arrFilterAttributes['CatalogID'] = array("MemberName" => translate("Catalog"));
		$arrFilterAttributes['CatalogID']["Values"] = BITAMCatalog::getCatalogs($this->Repository);
		
		//Validamos si nos faltaron campos de filtros o botones que no se hayan logrado colocar
		$countFilters = count($arrayFilters);
		$countButtons = count($arrayButtons);
		$countButtons = ($countButtons < 3)?3:0;
		
		$strFrom =
		"<input type=\"text\" id=\"CaptureStartDate\" name=\"CaptureStartDate\" size=\"22\" maxlength=\"19\" value=\"".$this->StartDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0) {year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureStartDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureStartDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureStartDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureStartDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureStartDate = new calendar('calendar_BITAMSurvey_CaptureStartDate', 'calendarCallback_BITAMSurvey_CaptureStartDate');
					</script>";
		
		$strTo =
		"<input type=\"text\" id=\"CaptureEndDate\" name=\"CaptureEndDate\" size=\"22\" maxlength=\"19\" value=\"".$this->EndDate."\" onfocus=\"javascript:var v = formatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; if (this.createTextRange){var r = this.createTextRange(); r.moveEnd('character', v.length); r.select();}\" onkeypress=\"javascript: event.returnValue = validateFormat(this, unformatDateTimeRegExpStr('yyyy-MM-dd hh:mm:ss'), event.keyCode);\" onblur=\"javascript:var v = unformatDateTime(this.value, 'yyyy-MM-dd hh:mm:ss'); this.value = v; var today = new Date(); var year = new Number(this.value.substr(0, 4)); if (year == 0)	{year = today.getFullYear();}var month = new Number(this.value.substr(5, 2)); if (month == 0){month = today.getMonth() + 1;} calendar_BITAMSurvey_CaptureEndDate.setCurrentYear(year); calendar_BITAMSurvey_CaptureEndDate.setCurrentMonth(month - 1);this.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');\">
					<script language=\"JavaScript\">
						function calendarCallback_BITAMSurvey_CaptureEndDate(day, month, year)
						{
							if (String(month).length == 1)
							{
								month = '0' + month;
							}
							if (String(day).length == 1)
							{
								day = '0' + day;
    						}

    						var obj = document.getElementById('CaptureEndDate');
    						var v = obj.value;
							v = v.substr(10);
    						v = year + '-' + month + '-' + day + v;
            				obj.value = v;
							obj.value = formatDateTime(v, 'yyyy-MM-dd hh:mm:ss');
						}
						calendar_BITAMSurvey_CaptureEndDate = new calendar('calendar_BITAMSurvey_CaptureEndDate', 'calendarCallback_BITAMSurvey_CaptureEndDate');
					</script>";

	/*
?>
	<span id="btnBack" style="display:none" class="alinkleft" onmouseover="aChange(this,'alinkescfavhoveredleft')" onmouseout="aChange(this,'alinkescfavleft')" onclick="javascript:editAudit();"><img src="images/home.png">&nbsp;<?="Back"?></span>
<?
	*/
?>
	 <? $displayFilterTable = (getMDVersion() >= esvAgendaRedesign) ? "none" : "block"; ?>
	<table id="filterTable" style="display: <?= $displayFilterTable ?>;" cellpadding="0" cellspacing="0">
<?		
		/*Este código no aplica para el caso de Agendas, se podría aplicar para filtrar por el estado de la Agenda pero eso hasta que
		se implementen los estados mencionados por DABdo el 2012-03-15 (-1=Nueva definición (no enviada a la App), 0=Enviada a App pero no
		capturada, 1=Ya capturada en la App
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
			<?="Form"?>&nbsp;
			</td>
			<td colspan="<?=$numColSpanForm?>">
			
				<select id="SurveyID" name="SurveyID" style="width:100%;font-size:11px" onchange="javascript:applyFilterSurvey();">
<?
			global $idxSelSurveyID;
			$idxSelSurveyID = 0;
			$cmbIndex = 0;

			for ($i=0; $i<$numSurveys; $i++)
			{
				$anSurveyID = $surveyCollection->Collection[$i]->SurveyID;
				$select="";
				if($this->SurveyID == $anSurveyID)
				{
					$idxSelSurveyID = $cmbIndex;
					$select = "selected";
				}
?>
					<option value="<?=$anSurveyID?>" <?=$select?>><?=($surveyCollection->Collection[$i]->SurveyName)?></option>
<?
				$cmbIndex++;
			}
?>
				</select>
			</td>
<?
			$countCell = 1;
			
			while(($countCell%$maxCell)!=0)
			{
?>
			<td>
				&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
				$countCell++;
			}
?>
		</tr>
<?
			*/
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td colspan="<?=$totalColSpanTable?>">
				&nbsp;
			</td>
			
		</tr>
<?

		$arrayUsers = array();
		$arrayUsers[-2] = translate("All");
		//$arrayUsers[-1] = "NA";
		
		$sql = "SELECT A.CLA_USUARIO, B.NOM_LARGO FROM SI_SV_Users A, SI_USUARIO B WHERE A.CLA_USUARIO = B.CLA_USUARIO ";
		if($_SESSION["PAFBM_Mode"]==1)
		{
			$sql.=" AND B.CLA_USUARIO > 0 ";
		}
		if($_SESSION["PABITAM_UserID"] != 1) {
			$sql .= (" AND A.Supervisor = ".$_SESSION["PABITAM_UserID"]);
		}
		$sql.=" ORDER BY NOM_LARGO ";
		$aRS = $this->Repository->ADOConnection->Execute($sql);
		if ($aRS === false)
		{
			die("(".__METHOD__.") ".translate("Error accessing")." SI_SV_Users, SI_USUARIO ".translate("table").": ".$this->Repository->DataADOConnection->ErrorMsg().". ".translate("Executing").": ".$sql);
		}
		
		while (!$aRS->EOF)
		{
			$cla_usuario = (int)$aRS->fields["cla_usuario"];
			$nom_largo = $aRS->fields["nom_largo"]; 
			$arrayUsers[$cla_usuario] = $nom_largo;
			$aRS->MoveNext();
		}
		
		$countCell = 0;
		$numRows = 0;
		$positionDates = 2;
		$positionFilter = 3;
		$positionButtons = 4;
		$position = 1;
		if((count($arrFilterAttributes)+1)>$countButtons)
		{
			$maxRows = count($arrFilterAttributes)+1;
		}
		else 
		{
			$maxRows = $countButtons;
		}
		
		$selected = "";
		//if($this->SurveyID!=0)
		//{
?>
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>

			<td>
			<?=translate("Filters")?>&nbsp;
			</td>
			<td style="text-align:right;" class="styleBorderFilterLeftTop">
				&nbsp;&nbsp;&nbsp;<span><?=translate("User")?></span>
			</td>
			<td style="text-align:right;" class="styleBorderFilterTop">
				<select id="UserID" name="UserID" style="width:150px; font-size:11px">
<?		
		foreach ($arrayUsers as $keyUser => $nameUser)
		{
			$selected="";
			if($this->UserID == $keyUser)
			{
				$selected = "selected";
			}
?>
					<option value="<?=$keyUser?>" <?=$selected?>><?=$nameUser?></option>
<?
		}
?>
				</select>
			</td>
<?
			$countCell = 1;
		//}
		
		if(count($arrFilterAttributes)>0)
		{
			foreach($arrFilterAttributes as $memberID => $memberInfo)
			{
				$position++;
				$select="";

				if($position==$positionDates)
				{
					if($numRows==0)
					{
?>
			<td style="text-align:right;" class="styleBorderFilterTop">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
			</td>
			<td class="styleBorderFilterTop">
				<?=$strFrom?>
			</td>
<?
					}
					elseif($numRows==1)
					{
?>
			<td style="text-align:right;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
			</td>
			<td>
				<?=$strTo?>
			</td>
<?
					}
					elseif($numRows==2)
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}

?>
			<td style="text-align:right;" <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
			<td <?=$classBorderBottom?>>
<?
						if(!is_null($this->Pages))
						{
?>
			
				<?=$this->Pages->display_items_per_page_report()?>
<?
						}
						else 
						{
?>
				&nbsp;
<?
						}
?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = '';

						if($maxRows==($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
			<td <?=$classBorderBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					$classBorderTop = "";
					$classBorderRightTop = "";
					if($numRows==0)
					{
						$classBorderTop = ' class="styleBorderFilterTop"';
						$classBorderRightTop = ' class="styleBorderFilterRightTop"';
					}
					else 
					{
						$classBorderTop = '';
						$classBorderRightTop = ' class="styleBorderFilterRight"';
					}
					
					if(isset($arrayFilters[$numRows]))
					{
?>
			<td <?=$classBorderTop?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightTop?>>
				<?=$arrayFilters[$numRows]?>
			</td>
<?
					}
					else 
					{
						$classBorderBottom = "";
						$classBorderRightBottom = "";
						if($maxRows == ($numRows+1))
						{
							$classBorderBottom = ' class="styleBorderFilterBottom"';
							$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
						}
						else 
						{
							$classBorderBottom = '';
							$classBorderRightBottom = ' class="styleBorderFilterRight"';
						}
?>
			<td <?=$classBorderBottom?>>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td <?=$classBorderRightBottom?>>
				&nbsp;
			</td>
<?
					}
					
					$position++;
					$countCell++;
					
					if(isset($arrayButtons[$numRows]))
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td style="padding-top:5px;padding-bottom:5px;">
				<?=$arrayButtons[$numRows]?>
			</td>
<?
					}
					else 
					{
?>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</td>
			<td>
				&nbsp;
			</td>
<?
					}
					
					$countCell++;
				}
				
				if(($countCell%$maxCell)==0)
				{
					$numRows++;
					$position = 1;
?>
		</tr>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
				else 
				{
					$position++;
				}
				
				$classBorderLeft = ' class="styleBorderFilterLeft"';
				$classBorderBottom = '';
				if($maxRows == ($numRows+1))
				{
					$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
					$classBorderBottom = ' class="styleBorderFilterBottom"';
				}

?>
			<td style="text-align:right;" <?=$classBorderLeft?>>
				&nbsp;&nbsp;&nbsp;<span><?=$memberInfo["MemberName"]?></span>
			</td>
			<td <?=$classBorderBottom?>>
				<select id="<?=$memberID?>" name="<?=$memberID?>" memberID="<?=$memberID?>" style="width:150px; font-size:11px">
<?
				global $idxSelSurveyID;
				$idxSelSurveyID = 0;
				$cmbIndex = 0;

?>
					<option value="0" ><?=translate("All")?></option>
<?
				$attribValues = $memberInfo["Values"];
				$attribSelected = null;
				eval('$attribSelected = $this->'.$memberID.";");
				
				if (is_null($attribSelected))
				{
					$attribSelected = 0;
				}
				foreach ($attribValues as $key=>$desc)
				{
					$select="";

					if($attribSelected == $key)
					{
						$idxSelSurveyID = $cmbIndex;
						$select = "selected";
					}
						
?>
						<option value="<?=$key?>" <?=$select?>><?=$desc?></option>
<?
					$cmbIndex++;
				}
?>
				</select>
			</td>
<?
				$countCell++;
			}
		}
		
		//if($this->SurveyID!=0)
		//{
			if(($countCell%$maxCell)==0)
			{
				$numRows++;
				$position = 1;
?>
		</tr>
<?
				if(($numRows+1)<$countButtons)
				{
?>
		<tr>
			<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
			</td>

			<td>
				&nbsp;
			</td>
<?
				}
			}
			else 
			{
				$position++;
			}
			
			if(($numRows+1)<=$countButtons)
			{
				while(($numRows+1)<=$countButtons)
				{
					if($position==1)
					{
						$classBorderLeft = ' class="styleBorderFilterLeft"';
						$classBorderBottom = '';
						if($maxRows==($numRows+1))
						{
							$classBorderLeft = ' class="styleBorderFilterLeftBottom"';
							$classBorderBottom = ' class="styleBorderFilterBottom"';
						}
	
	?>
				<td <?=$classBorderLeft?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						$position++;
					}
					
					if($position==$positionDates)
					{
						if($numRows==0)
						{
	?>
				<td style="text-align:right;" class="styleBorderFilterTop">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("From")?></span>
				</td>
				<td class="styleBorderFilterTop">
					<?=$strFrom?>
				</td>
	<?
						}
						else if($numRows==1)
						{
	?>
				<td style="text-align:right;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?=translate("To")?></span>
				</td>
				<td>
					<?=$strTo?>
				</td>
	<?
						}
						else if($numRows==2)
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
	?>
				<td style="text-align:right;" <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><?="Max Records"?></span>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
					
							}
	?>
				</td>
				<td <?=$classBorderBottom?>>
	<?
							if(!is_null($this->Pages))
							{
	?>
				
					<?=$this->Pages->display_items_per_page_report()?>
	<?
							}
							else 
							{
	?>
					&nbsp;
	<?
							}
	?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = '';
	
							if($maxRows==($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
							}
				//@JAPR 2012-01-10: Corregido un bug, los nombres previos de estas variables no existían, además, deberían ser $classBorderBottom
				//que que se trata de celdas vacías debajo de los filtros de fecha y Max records, así que mientras no se agreguen mas opciones
				//fijas esto debería no tener border para cada filtro intermedio, y debería ser border inferior para el último filtro
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
				<td <?=$classBorderBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						$classBorderTop = "";
						$classBorderRightTop = "";
						if($numRows==0)
						{
							$classBorderTop = ' class="styleBorderFilterTop"';
							$classBorderRightTop = ' class="styleBorderFilterRightTop"';
						}
						else 
						{
							$classBorderTop = '';
							$classBorderRightTop = ' class="styleBorderFilterRight"';
						}
						
						if(isset($arrayFilters[$numRows]))
						{
	?>
				<td <?=$classBorderTop?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightTop?>>
					<?=$arrayFilters[$numRows]?>
				</td>
	<?
						}
						else 
						{
							$classBorderBottom = "";
							$classBorderRightBottom = "";
							if($maxRows == ($numRows+1))
							{
								$classBorderBottom = ' class="styleBorderFilterBottom"';
								$classBorderRightBottom = ' class="styleBorderFilterRightBottom"';
							}
							else 
							{
								$classBorderBottom = '';
								$classBorderRightBottom = ' class="styleBorderFilterRight"';
							}
	?>
				<td <?=$classBorderBottom?>>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td <?=$classBorderRightBottom?>>
					&nbsp;
				</td>
	<?
						}
						
						$position++;
						$countCell++;
						
						if(isset($arrayButtons[$numRows]))
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td style="padding-top:5px;padding-bottom:5px;">
					<?=$arrayButtons[$numRows]?>
				</td>
	<?
						}
						else 
						{
	?>
				<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
	<?
						}
						
						$countCell++;
					}
					
						$numRows++;
						$position = 1;
	?>
				</tr>
	<?
						if(($numRows+1)<=$countButtons)
						{
	?>
				<tr>
				<td style="padding-top:5px;padding-bottom:5px;">
<?
					if($maxRows!=($numRows+1))
					{
?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<?
					}
					else 
					{
?>
				<?=$btnDelEntries?>
<?
					}
?>
				</td>
	
					<td>
						&nbsp;
					</td>
	<?
						}
				}
			}
			else 
			{
				while(($countCell%$maxCell)!=0)
				{
					//Si es el primero
					if(($countCell+1)%$maxCell==1)
					{
						$classBorderCell01 = ' class="styleBorderFilterLeftBottom"';
						$classBorderCell02 = ' class="styleBorderFilterBottom"';
					}
					else 
					{
						//Si es el penultimo par de tds
						if(($countCell+1)%$maxCell==3)
						{
							$classBorderCell01 = ' class="styleBorderFilterBottom"';
							$classBorderCell02 = ' class="styleBorderFilterRightBottom"';
						}
						else
						{
							//Si es intermedio
							if(($countCell+1)%$maxCell==2)
							{
								$classBorderCell01 = ' class="styleBorderFilterBottom"';
								$classBorderCell02 = ' class="styleBorderFilterBottom"';
							}
							else 
							{	//Si es el ultimo(el cuarto)
								$classBorderCell01 = '';
								$classBorderCell02 = '';
							}
						}
					}
	?>
				<td <?=$classBorderCell01?>>
					&nbsp;
				</td>
				<td <?=$classBorderCell02?>>
					&nbsp;
				</td>
	<?
					$countCell++;
				}
			}
		//}
?>
	</table>
	<br>
<?
	}
	
	function generateBeforeFormCode($aUser)
	{
		$myFormName = get_class($this);
?>		

	<link type="text/css" rel="stylesheet" href="css/eBavel.css"/>

	<script type="text/javascript" src="js/jquery_1.9.1/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery-ui-1.10.1.custom.min.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.jsperanto.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamApp.js"></script>
	<script type="text/javascript" src="js/eBavel/bitamstorage.js"></script>
	<script type="text/javascript" src="js/eBavel/databases.js"></script>
	<script type="text/javascript" src="js/eBavel/section.class.js"></script>
	<script type="text/javascript" src="js/eBavel/jquery.widgets.js"></script>
	<script type="text/javascript" src="js/eBavel/date.js"></script>
	<script type="text/javascript" src="js/jsondefinition/bitamAppFormsJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jsondefinition/agendasJsonDefinition.js"></script>
	<script type="text/javascript" src="js/jquery.widgets.js"></script>
	<script type="text/javascript" language="javascript" src="js/dialogs.js"></script>
	<script language="JavaScript">
	
		function applyFilterAttrib (filter, oldTags, descrTag, allCatalogs)
		{
			if (filter) {
				
				for (var fkey in filter) {
				
					//Usuarios
					if (fkey == 'Usuario') {
						var filterUsers = filter['Usuario'];
						var agendasUsers = new Array();
						for (var ukey in filterUsers) {
							var filterUser = filterUsers[ukey];
							agendasUsers.push(filterUser);
						}
						if (agendasUsers.length) {
							frmAttribFilter.userID.value = agendasUsers.join('|');
						}
						if (descrTag == 'Usuario') {
							frmAttribFilter.oldUserTags.value = oldTags;
						}
					}
					
					//Catalogo
					if (fkey =='Catalogo') {
						frmAttribFilter.catalogID.value = parseInt(filter['Catalogo'][0], 10);
					}
					frmAttribFilter.oldCatalogTags.value = allCatalogs;
					
					//Attributes
					if (fkey == 'Attributes') {
						var filterAttributes = filter['Attributes'];
						frmAttribFilter.attributesID.value = JSON.stringify(filterAttributes);
						if (descrTag.indexOf('AttribID_') != -1){
							frmAttribFilter.oldAttributesTags.value = oldTags;
						}
					}
					
					//SearchString
					if (fkey == 'SearchString') {
						frmAttribFilter.searchString.value = filter['SearchString'];
					}
					
					//Status
					if (fkey == 'Status') {
						var filterStatus = filter['Status'];
						var agendasStatus = new Array();
						for (var ukey in filterStatus) {
							var filterAgendaStatus = filterStatus[ukey];
							agendasStatus.push(filterAgendaStatus);
						}
						if (agendasStatus.length) {
							frmAttribFilter.statusID.value = agendasStatus.join('|');
						}
						if (descrTag == 'Status') {
							frmAttribFilter.oldStatusTags.value = oldTags;
						}
					}
					
					if (fkey == 'Date') {
						if (fkey && filter[fkey] && filter[fkey][0] && filter[fkey][0] != undefined) {
							var dateFilter = filter[fkey].split('@');
							if (dateFilter.length == 2) {
								frmAttribFilter.startDate.value = dateFilter[0];
								frmAttribFilter.endDate.value = dateFilter[1];
							}
						}
					}
				}
				
				
			} else {
				var objCatalogID = document.getElementById("CatalogID");
				frmAttribFilter.catalogID.value = objCatalogID.value;

				var objStartDate = document.getElementById("CaptureStartDate");
				frmAttribFilter.startDate.value = objStartDate.value;

				var objEndDate = document.getElementById("CaptureEndDate");
				frmAttribFilter.endDate.value = objEndDate.value;
				
				var objUserID = document.getElementById("UserID");
				frmAttribFilter.userID.value = objUserID.value;
				
				var objIpp = document.getElementById("ipp");
				frmAttribFilter.ipp.value = objIpp.value;
			}

			frmAttribFilter.submit();
		}
		
		function clearFilter()
		{
<?
			if($this->UserID != -2 || !$this->isEmptyStartDate() || !$this->isEmptyEndDate() || $this->ipp != 100 || $this->CatalogID != 0)
			{
?>
			var objCatalogID = document.getElementById("CatalogID");
			frmAttribFilter.catalogID.value = "";

			var objStartDate = document.getElementById("CaptureStartDate");
			frmAttribFilter.startDate.value = "";

			var objEndDate = document.getElementById("CaptureEndDate");
			frmAttribFilter.endDate.value = "";
			
			var objUserID = document.getElementById("UserID");
			frmAttribFilter.userID.value = -2;
			
			var objIpp = document.getElementById("ipp");
			frmAttribFilter.ipp.value = 100;
			
			frmAttribFilter.submit();
<?
			}
			else 
			{
?>
<?
			}
?>
		}

		function extractNumber(obj)
		{
			//Obtener el valor del input text
			var temp = obj.value;

			//Crear nuestra expresion regular con digitos
			var reg0Str = '[0-9]*';

			//No se manejara signo negativo 
			//reg0Str = '^-?' + reg0Str;
			reg0Str = '^' + reg0Str;

			//Indicamos que nuestra expresion regular solo
			//aceptara digitos al final de la cadena
			reg0Str = reg0Str + '$';

			//Hacemos la instancia RegExp para que se cree
			//un objeto tipo expresion regular
			var reg0 = new RegExp(reg0Str);

			//Evaluamos nuestra cadena o valor que se
			//encuentra en nuestro input text
			//Si regresa true esto quiere decir
			//que hasta el momento si se ha cumplido
			//con la expresión regular
			if (reg0.test(temp)) return true;

			//Creamos una cadena con todos los caracteres permitidos
			//en nuestra entrada de texto, esto es digitos y el signo
			//negativo, el [^.....] indica que se hara una busqueda
			//para todos aquellos caracteres que no esten entre los
			//corchetes
			var reg1Str = '[^0-9' + ']';

			//Creamos nuestra expresión regular, pero se
			//le indica ahora, que es una expresión regular
			//con búsqueda globalpara todas las entradas de
			//texto que no cumplen con lo especificado en reg1Str
			var reg1 = new RegExp(reg1Str, 'g');
			//Se reemplaza dichos caracteres con vacío
			temp = temp.replace(reg1, '');

			//Se asigna nuevamente el valor a obj.value
			obj.value = temp;
		}
		
		function checkLimitNumber(obj)
		{
			// value is present
			var tval=Trim(obj.value);
			if (tval=='')
			{
				return true;
			}
			
			reg=/^0*/;
			tval=tval.replace(reg,'')

			if (tval!='')
			{
				val=parseInt(tval);
			}
			else
			{
				val=0;
			}

			var min=1;
			var max=1000;
			
			var msg="";

			if(min!='' && max!='')
			{
				msg='Max records should be in range of ' + min + ' to ' + max + '.' ;
			}
			else
			{
  				if(min!='')
  				{
  					msg='Input value should be greater than or equal to ' + min +'.';
  				}
				else
				{
    				if(max!='')
    				{
						msg='Input value should be less than or equal to ' + max + '.';
    				}
				}
			}

			if(min!='')
			{
				if (min>val)
				{
					alert(msg);
    				obj.value = 100;
				}
			}

			if (max!='')
			{
				if (val>max) 
				{
					alert(msg);
    				obj.value = 100;
				}
			}
		}

 		function downloadAgendasTemplate(bFill)
 		{
			window.open("downloadAgendasTemplate.php?fill="+bFill, "downloadAgendasTemplate", "menubar=no,toolbar=no,status=no,resizable=no,scrollbars=no", 1)
 		}
 		function uploadAgendasFile()
 		{
 		  document.forms.frmuploadAgendasFile.style.display = "inline"
 		}
 		function appendAgendasFile()
 		{
 		  document.forms.frmappendAgendasFile.style.display = "inline"
 		}
		function agendaChange () {
			openWindowDialog('main.php?BITAM_PAGE=SurveyAgendaReschedule', [window], [], 600, 600, agendaReschedule, 'agendaReschedule');
		}
		function agendaReschedule(sValue, sParams)
		{
			var strInfo = sValue;
			if (strInfo) {
				try {
					var objAgendaReschedule = JSON.parse(strInfo);
					if (objAgendaReschedule.date) {
						var elements = document.getElementsByName("AgendaID[]")
						if (elements && elements.length) {
							var arrAgendaIDs = new Array();
							for (var i = 0; i < elements.length; i++) {
								if (elements[i].checked) {
									var intAgendaID = elements[i].value;
									arrAgendaIDs.push(intAgendaID);
								}
			  }
		
							if (arrAgendaIDs.length) {
								frmAttribFilter.newStartDate.value = objAgendaReschedule.date;
								frmAttribFilter.newStartHour.value = objAgendaReschedule.hour;
								frmAttribFilter.newStartMinutes.value = objAgendaReschedule.minutes;
								frmAttribFilter.agendaRescheduleID.value = arrAgendaIDs.join(',');
								frmAttribFilter.reschedule.value = 1;
								frmAttribFilter.submit();
							}
						}
					}
				} catch (e) {}
			}
		}
		
		</script>
<?
	}

	//@JAPR 2012-06-04: Agregada la importación de agendas desde MS Excel
 	function addButtons($aUser)
	{
		//@JAPR 2014-11-04: Modificado para siempre eliminar las agendas pre-existentes
		//Se ocultó el botón de Append para sólo permitir el Upload completo del template de agengas
?>
    <nobr>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadAgendasTemplate(0);"><img src="images/download_xls_templates.gif"> <?=translate("Download agendas template")."   "?>  </span>-->
		<button id="btnDownloadAgendasTemplate0" class="alinkescfav" onclick="downloadAgendasTemplate(0);"><img src="images/download_xls_templates.gif" alt="<?=translate('Download agendas template')?>" title="<?=translate('Download agendas template')?>" displayMe="1" /> <?=translate("Download agendas template")?></button>
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="downloadAgendasTemplate(1);"><img src="images/download_xls_templates.gif"> <?=translate("Export agendas")."   "?>  </span>-->
		<button id="btnDownloadAgendasTemplate1" class="alinkescfav" onclick="downloadAgendasTemplate(1);"><img src="images/download_xls_templates.gif" alt="<?=translate('Export agendas')?>" title="<?=translate('Export agendas')?>" displayMe="1" /> <?=translate("Export agendas")?></button>
		
		<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="uploadAgendasFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> <?=translate("Upload agendas file")."   "?>  </span></span>-->
		<button id="btnUploadAgendasFile" class="alinkescfav" onclick="uploadAgendasFile();"><img src="images/load_data_XLS.gif" alt="<?=translate('Upload agendas file')?>" title="<?=translate('Upload agendas file')?>" displayMe="1" /> <?=translate("Upload agendas file")?></button>
	    <form name="frmuploadAgendasFile" method="POST" action="uploadAgendas.php" enctype="multipart/form-data" target="frameuploadAgendasFile" style="display:none">
	      <input type="file" name="xlsfile">
	      <input type="submit" name="txtsubmit" value="<?=translate("Upload file")?>">
	      <input type="button" name="txtcancel" value="<?=translate("Cancel")?>" onclick="document.forms.frmuploadAgendasFile.style.display='none';document.forms.frmuploadAgendasFile.reset()">
	    </form>
	    
    	<!--<span class="alink" onmouseover="aChange(this,'alinkescfavhovered')" onmouseout="aChange(this,'alinkescfav')" onclick="appendAgendasFile();"><img src="images/load_data_XLS.gif"> <span id="txtLoadSpan"> <?=translate("Append agendas file")."   "?>  </span></span>-->
    	<!--<button id="btnAppendAgendasFile" class="alinkescfav" onclick="appendAgendasFile();"><img src="images/load_data_XLS.gif" alt="<?=translate('Append agendas file')?>" title="<?=translate('Append agendas file')?>" displayMe="1" /> <?=translate("Append agendas file")?></button>-->
	    <form name="frmappendAgendasFile" method="POST" action="uploadAgendas.php?appendFile=1" enctype="multipart/form-data" target="frameappendAgendasFile" style="display:none">
	      <input type="file" name="xlsfile">
	      <input type="submit" name="txtsubmit" value="<?=translate("Upload file")?>">
	      <input type="button" name="txtcancel" value="<?=translate("Cancel")?>" onclick="document.forms.frmappendAgendasFile.style.display='none';document.forms.frmappendAgendasFile.reset()">
	    </form>
		
		<button id="btnChangeSchedule" class="alinkescfav" onclick="agendaChange();"><img src="images/reschedule.gif" alt="<?=translate('Reschedule')?>" title="<?=translate('Reschedule')?>" displayMe="1" /> <?=translate("Reschedule")?></button>
		
		<div id="dialog-modal" title="Basic modal dialog" style="display: none;">
			<p>Date: <input type="text" id="datepicker"></p>
		</div>
		
    </nobr>
<?
	}
	//@JAPR
		
	function generateAfterFormCode($aUser)
	{
	
?>

	<script>
	
		delete Array.prototype.contains;
		bitamApp.getAppFormsDefinition();
		$.extend($, { t: function (word) {
				return $.jsperanto.translate(word);
			}
		});
				
		function addDescriptionTag (tagName, tagDescription, element) {
			var realTagName = $.t(tagName);
			if (!descriptionTags[realTagName]) {
				descriptionTags[realTagName] = {};
			}
			var theTag = descriptionTags[realTagName];
			descriptionTags[realTagName].description = tagDescription;
			if (!theTag.elements) {
				theTag.elements = {};
			}
			var elementID = element.id;
			var elementName = element.name;
			if (theTag.elements[elementID] == undefined) {
				theTag.elements[elementID] = {};
				theTag.elements[elementID].id = elementName;
				theTag.elements[elementID].num = 1;
			} else {
				theTag.elements[elementID].num = (theTag.elements[elementID].num+1);
			}
		}
		var descriptionTags = {};
		
		var searchString = '';
		<? if (isset($_POST["searchString"]) && $_POST["searchString"] != '') { ?>
			searchString = '<?= $_POST["searchString"] ?>';
		<? } ?>
		
		var filterDate = '';
		<? if ((isset($_POST["startDate"]) && $_POST["startDate"] != '') && (isset($_POST["endDate"]) && $_POST["endDate"] != '')) { ?>
			filterDate = '<?= $_POST["startDate"] ?>@<?= $_POST["endDate"] ?>';
		<? } ?>
				
		<? foreach ($this->Collection as $anAgenda) { ?>
				//addDescriptionTag('Fecha','Fecha' , { });
				addDescriptionTag('Catalogo','Catalogo' , { id: <?= $anAgenda->CatalogID ?>, name: "<?= $anAgenda->CatalogName ?>" });
				addDescriptionTag('Usuario', 'Usuario', { id: <?= $anAgenda->UserID ?>, name: "<?= $anAgenda->UserName ?>" });
				<?
				if (isset($_POST["catalogID"]) && $_POST["catalogID"] != '' && $_POST["catalogID"] != 0) {
					$tmpCatalogCollection = BITAMSurveyAgenda::getCatMembersByCatalogID($this->Repository, true, $_POST["catalogID"]);
					if (!empty($tmpCatalogCollection)) {
						$attributeIDs = $tmpCatalogCollection[$_POST["catalogID"]];
						$values = explode("|", $anAgenda->FilterText);
						$vkey = 0;
						foreach ($attributeIDs as $ckey => $attributeID) {
							if (isset($values[$vkey])) {
								$value = $values[$vkey++];
								?>
									addDescriptionTag('AttribID_'+ <?= $ckey ?>, "<?= $attributeID ?>", { id: "<?= $value ?>", name: "<?= $value ?>" });
								<?
							}
						}
					}
				} ?>
				
				addDescriptionTag('Status', 'Status', { id: <?= $anAgenda->Status ?>, name: "<?= $anAgenda->getAgendaStatus() ?>" });
				
				<?
		} ?>
		var oldUserTags = JSON.parse(<?= json_encode((isset($_POST['oldUserTags']) && $_POST['oldUserTags'] != '') ? $_POST['oldUserTags'] : '{}') ?>);
		var oldStatusTags = JSON.parse(<?= json_encode((isset($_POST['oldStatusTags']) && $_POST['oldStatusTags'] != '') ? $_POST['oldStatusTags'] : '{}') ?>);
		var oldCatalogTags = JSON.parse(<?= json_encode((isset($_POST['oldCatalogTags']) && $_POST['oldCatalogTags'] != '') ? $_POST['oldCatalogTags'] : '{}') ?>);
		var oldAttributesTags = JSON.parse(<?= json_encode((isset($_POST['oldAttributesTags']) && $_POST['oldAttributesTags'] != '') ? $_POST['oldAttributesTags'] : '{}') ?>);
		var checkedElemenTags = {};

		/*@AAL 24/04/2015: agregado para identificar si se trata de una agenda o AgendaScheduler y mostrar el filtro fecha,
		la cual solo debe de mostrarse en agendas, la validación se muestra em el archivo jquery.widgets.js*/
		var isAgenda = true;

		function initBoxFilter () {
			var sectionID = bitamApp.formsSections.Agendas.id;
			var sectionView = bitamApp.formsSections.Agendas.sectionView;
			var aSection = bitamApp.application.payload.sections[sectionID];
			var checkedUserValues = ("<?= (isset($_POST["userID"]) ? $_POST["userID"] : "") ?>");
			if (checkedUserValues) {
				checkedElemenTags['Usuario'] = checkedUserValues.split('|');
				$.each(checkedElemenTags['Usuario'], function (index, value) {
					value = ('' + value);
				});
			}
			
			var checkedStatusValues = ("<?= (isset($_POST["statusID"]) ? $_POST["statusID"] : "") ?>");
			if (checkedStatusValues) {
				checkedElemenTags['Status'] = checkedStatusValues.split('|');
				$.each(checkedElemenTags['Status'], function (index, value) {
					value = ('' + value);
				});
			}
			
			var checkedCatalogValues = ("<?= (isset($_POST["catalogID"]) ? $_POST["catalogID"] : "") ?>");
			if (checkedCatalogValues) {
				checkedElemenTags['Catalogo'] = checkedCatalogValues.split('|');
				$.each(checkedElemenTags['Catalogo'], function (index, value) {
					value = ('' + value);
				});
			}
			
			<?
			
			$atValues = "";
			if (isset($_POST["attributesID"])) {
				if ($_POST["attributesID"] == "") {
					$atValues = "{}";
				} else {
					$atValues = "" . ($_POST['attributesID']);
				}
			} else {
				$atValues = "{}";
			}
			
			?>
			
			var checkedAttributesValues = JSON.parse('<?= $atValues ?>');
			//alert(JSON.stringify(checkedAttributesValues));
			if (checkedAttributesValues) {
				for (var akey in checkedAttributesValues) {
					//alert(akey);
					checkedElemenTags[akey] = checkedAttributesValues[akey];
				}
			}
			if (!$.isEmptyObject(oldUserTags) && descriptionTags['Usuario'] && descriptionTags['Usuario'].elements) {
				$.extend(descriptionTags['Usuario'].elements, oldUserTags);
			}
			if (!$.isEmptyObject(oldStatusTags) && descriptionTags['Status'] && descriptionTags['Status'].elements) {
				$.extend(descriptionTags['Status'].elements, oldStatusTags);
			}
			if (!$.isEmptyObject(oldCatalogTags) && descriptionTags['Catalogo'] && descriptionTags['Catalogo'].elements) {
				$.extend(descriptionTags['Catalogo'].elements, oldCatalogTags);
			}
			if (!$.isEmptyObject(oldAttributesTags)) {
				$.extend(descriptionTags, oldAttributesTags);
			}
			//alert('searchString: '+searchString);
			var options = {
				'descriptionTags': descriptionTags,
				'checked': checkedElemenTags,
				'searchString': searchString,
				'filterDate': filterDate
			};
			
			if ($.isEmptyObject($('#boxFilter').data())) {
				$('#boxFilter').boxfilter(options);
			} else {
				$('#boxFilter').boxfilter('update');
			}
		}
		try {
			initBoxFilter();
		} catch (e) {
			alert('Search filter error: '+e);
		}
		
	</script>

	<form name="frmAttribFilter" action="main.php?BITAM_SECTION=SurveyAgendaCollection" method="POST" target="body" accept-charset="utf-8">
		<input type="hidden" name="BITAM_SECTION" value="SurveyAgendaCollection">
		<input type="hidden" name="catalogID" id="catalogID" value="">
		<input type="hidden" name="startDate" id="startDate" value="">
		<input type="hidden" name="endDate" id="endDate" value="">
		<input type="hidden" name="userID" id="userID" value="">
		<input type="hidden" name="ipp" id="ipp" value="">
		<input type="hidden" name="attributesID" id="attributesID" value="">
		<input type="hidden" name="statusID" id="statusID" value="">
		<input type="hidden" name="searchString" id="searchString" value="">
		
		<input type="hidden" name="oldUserTags" id="oldUserTags" value="">
		<input type="hidden" name="oldCatalogTags" id="oldCatalogTags" value="">
		<input type="hidden" name="oldAttributesTags" id="oldAttributesTags" value="">
		<input type="hidden" name="oldStatusTags" id="oldStatusTags" value="">
		
		<input type="hidden" name="agendaRescheduleID" id="agendaRescheduleID" value="">
		<input type="hidden" name="reschedule" id="reschedule" value="0">
		<input type="hidden" name="newStartDate" id="newStartDate" value="">
		<input type="hidden" name="newStartHour" id="newStartHour" value="00">
		<input type="hidden" name="newStartMinutes" id="newStartMinutes" value="00">
	</form>
	
<?
	//Vamos a desplegar el pie de pagina, es decir, la paginacion
		if(!is_null($this->Pages))
		{
?>
	<style type="text/css">
	.paginate 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
	}

	a.paginate 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		color: #000080;
		text-decoration: none;
	}

	a.paginate:hover 
	{
		background-color: #000080;
		color: #FFF;
		text-decoration: underline;
	}
	
	.currentlink 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: .7em;
		font-weight: bold;
	}

	a.currentlink 
	{
		border: 1px solid #000080;
		padding: 2px 6px 2px 6px;
		cursor: default;
		background:#000080;
		color: #FFF;
		text-decoration: none;
	}
	
	.showingInfo 
	{
		font-family: Arial, Helvetica, sans-serif;
		font-size: 11px;
	}
	</style>
	<br/>
	<?=$this->Pages->display_pages(true)?>
	<br/>
<?
		}
	}


	function generateInsideFormCode($aUser)
	{
?>
		<input type="hidden" id="startDate" name="startDate" value="<?=$this->StartDate?>">
		<input type="hidden" id="endDate" name="endDate" value="<?=$this->EndDate?>">
		<input type="hidden" id="userID" name="userID" value="<?=$this->UserID?>">
		<input type="hidden" id="ipp" name="ipp" value="<?=$this->ipp?>">
		<input type="hidden" id="catalogID" name="catalogID" value="<?=$this->CatalogID?>">
<?
	}
}
?>