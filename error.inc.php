<?php
//@JAPR 2019-09-18: Agregada la subcarpeta agrupada por nombre de agente para agrupar las tareas que corresponden a dicho servicio (#DGXRW8)
//@JAPRDescontinuado: Este archivo realmente no se utiliza, cuando se invoca a llamar lo hace desde una carpeta atrás, es decir, invoca al de Artus, por lo que los errores los controlaría
//como lo hace dicho producto, almacenando los logs en su carpeta y no en la de eForms
//@JAPR 2015-02-10: Agregado soporte para php 5.6
if (str_replace('.', '', PHP_VERSION) > 530) {
	error_reporting(E_ALL ^ E_DEPRECATED);
} else {
	error_reporting(E_ALL);
}

global $AbortOnError;
$AbortOnError = true;
global $DieOnError;
global $BitamNotifyError;
$BitamNotifyError = false;

function NotifyException($anException)
{
	return NotifyError(E_ERROR, $anException->getMessage(), $anException->getFile(), $anException->getLine());
}

function NotifyError($anErrorNo, $anErrorMsg, $aFile, $aLine)
{
	global $DieOnError;
	global $AbortOnError;
  	global $php_errormsg;
  	$php_errormsg = $anErrorMsg;
	$errorReporting = error_reporting();
	if ($errorReporting & $anErrorNo)
	{
		//@JAPR 2015-02-10: Agregado soporte para php 5.6
		if (str_replace('.', '', PHP_VERSION) > 530) {
			error_reporting(E_ALL ^ E_DEPRECATED);
		} else {
			error_reporting(E_ALL);
		}
		$abort = false;
		$aBacktrace = array_reverse(debug_backtrace());
		switch ($anErrorNo)
		{
	   		case E_ERROR:
	  			$anErrorType = 'Error';
				$abort = $AbortOnError;
	  			break;
			case E_WARNING:
	  			$anErrorType = "Warning";
	  			if ((stripos($aFile, 'cTable.php') !== false) &&
	  				(stripos($aFile, 'eval()\'d code') !== false)){
	  				printf("[%s:%d] %s", $aFile, $aLine, $anErrorMsg);
	  				return;
	  			}
	  			break;
	  		case E_PARSE:
	  			$anErrorType = "Parse";
	  			//return;
	  			break;
	  		case E_NOTICE:
	  			$anErrorType = "Notice";
	  			//return;
	  			break;
	  		case E_CORE_ERROR:
				$abort = $AbortOnError;
	  			$anErrorType = "Core Error";
	  			//return;
	  			break;
	  		case E_CORE_WARNING:
	  			$anErrorType = "Core Warning";
	  			//return;
	  			break;
	  		case E_COMPILE_ERROR:
				$abort = $AbortOnError;
	  			$anErrorType = "Compile Error";
	  			//return;
	  			break;
	  		case E_COMPILE_WARNING:
	  			$anErrorType = "Compile Error";
	  			//return;
	  			break;
	  		case E_USER_ERROR:
				$abort = $AbortOnError;
	  			$anErrorType = 'User Error';
	  			break;
			case E_USER_WARNING:
	  			$anErrorType = 'User Warning';
	  			//return;
	  			break;
	  		case E_USER_NOTICE:
	  			$anErrorType = 'User Notice';
	  			//return;
	  			break;
	  		case E_STRICT:
	  			$anErrorType = 'Strict';
	  			//return;
	  			break;
			default:
	  			$anErrorType = 'Unknown Error: '.((int) $anErrorNo);
	  			//return;
	  			break;
	  	}
		ob_start();
?>
<table border="0" cellpadding="3" width="600">
	<tr class="h">
		<th colspan="3">
			PHP <?= $anErrorType ?>: <i><?= $anErrorMsg ?></i> in <i><?= $aFile ?></i> on line <i><?= $aLine ?></i><br>
		</th>
	</tr>
<?
		$blnAddTrace = true;
		if (isset($DieOnError) && $DieOnError)
		{
			$blnAddTrace = false;
		}
		if ($blnAddTrace)
		{
?>
	<tr class="h">
		<th colspan="3">
			Call Stack
		</th>
	</tr>
	<tr class="e">
		<th>#</th>
		<th>Function</th>
		<th>Location</th>
	</tr>
<?
			$i = 1;
			$functionName = null;
			foreach ($aBacktrace as $anArray)
			{
				if (is_null($functionName))
				{
					$functionName = '{main}';
				}
?>
	<tr>
		<td class="e"><?= $i ?></td>
		<td class="v"><?= $functionName ?>()</td>
		<td class="v"><?= array_key_exists('file', $anArray) ? $anArray['file'] : '{php function}' ?><b>:</b><?= array_key_exists('line', $anArray) ? $anArray['line'] : 0 ?></td>
	</tr>
<?
				$functionName = $anArray['function'];
				$i++;
			}
		}
?>
</table>
<br />
<?
		$error = ob_get_contents();
		ob_end_clean();
		ob_start();
		if (ini_get('register_argc_argv') == '1')
		{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html><head>
<style type="text/css">
body {background-color: #ffffff; color: #000000;}
body, td, th, h1, h2 {font-family: sans-serif;}
pre {margin: 0px; font-family: monospace;}
a:link {color: #000099; text-decoration: none; background-color: #ffffff;}
a:hover {text-decoration: underline;}
table {border-collapse: collapse;}
.center {text-align: center;}
.center table { margin-left: auto; margin-right: auto; text-align: left;}
.center th { text-align: center !important; }
td, th { border: 1px solid #000000; font-size: 75%; vertical-align: baseline;}
h1 {font-size: 150%;}
h2 {font-size: 125%;}
.p {text-align: left;}
.e {background-color: #ccccff; font-weight: bold; color: #000000;}
.h {background-color: #9999cc; font-weight: bold; color: #000000;}
.v {background-color: #cccccc; color: #000000;}
.vr {background-color: #cccccc; text-align: right; color: #000000;}
img {float: right; border: 0px;}
hr {width: 600px; background-color: #cccccc; border: 0px; height: 1px; color: #000000;}
</style>
<title>phpinfo()</title>
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW,NOARCHIVE" /></head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body><div class="center">
<table border="0" cellpadding="3" width="600">
	<tr class="h">
		<td colspan="3" class="v">
			<pre>
<?
			phpinfo();
?>
			</pre>
		</td>
	</tr>
</table>
</div></body></html>
<?
		}
		else
		{
			phpinfo();
		}
		$phpinfo = ob_get_contents();
		ob_end_clean();
		$strAdvisorErrorMessage = $phpinfo;

		$message = str_replace('<body><div class="center">', '<body><div class="center">'.$error, $phpinfo);
		if (($result = NotifyByFile($anErrorType, $message)) !== true)
		{
		}

		global $BitamNotifyError;
		if ($BitamNotifyError)
		{
			if (($result = NotifyBySMTP($anErrorType, $message)) !== true)
			{
				if (($result = NotifyByHTTP($anErrorType, $message)) !== true)
				{
				}
			}
		}
		error_reporting($errorReporting);
		if ($abort && !$blnAddTrace)
		{
			$aPos = strpos($strAdvisorErrorMessage, '<body><div class="center">');
			if (!is_null($aPos))
			{
				$strAdvisorErrorMessage = substr($strAdvisorErrorMessage, 0, $aPos + strlen('<body><div class="center">')).'</div></body></html>';
			}
			$strAdvisorErrorMessage = str_replace('<body><div class="center">', '<body><div class="center">'.$error, $strAdvisorErrorMessage);
			$strAdvisorErrorMessage = '<!-- AWERROR:AWERRDESC='.$error.'</AWERRDESC> -->'."\n\r".$strAdvisorErrorMessage;
			echo ($strAdvisorErrorMessage);
			die();
		}
		foreach ($_SERVER as $name => $value) 
		{ 
			if (substr($name, 0, 5) == 'HTTP_') 
			{ 
				$name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))); 
				$headers[$name] = $value; 
			} else if ($name == "CONTENT_TYPE") { 
				$headers["Content-Type"] = $value; 
			} else if ($name == "CONTENT_LENGTH") { 
				$headers["Content-Length"] = $value; 
			} 
		} 
		
		if ($abort)
		{
			//si el request acepta JSON o Javascript mandamos el codigo de error en un objeto de javascript
			$anErrorSumary = '';
			if(strpos($anErrorMsg, 'Error QUERY (duplicate entry)') === 0)
			{
				$anErrorSumary = 'Duplicate entry';
			}
			if(strpos($headers["Accept"], "application/json") !== false || strpos($headers["Accept"], "text/javascript") !== false){
				$jsonResponse = new Ajax_Response();
				$jsonResponse->setError($anErrorNo);
				$jsonResponse->setErrorDescription($anErrorMsg);
				$jsonResponse->setErrorSummary($anErrorSumary);
				$jsonResponse->printJSONP();
				exit();
			}
			return exit($anErrorMsg);
		}
		return true;
	}
	return false;
}

set_error_handler('NotifyError');
//set_exception_handler('NotifyException');


function NotifySQLError($anErrorMsg, $aSql, $aFile, $aLine, $abort=true)
{
	global $DieOnError;

	$aBacktrace = array_reverse(debug_backtrace());
	ob_start();
?>
<table border="0" cellpadding="3" width="600">
	<tr class="h">
		<th colspan="3">
			Fatal SQL error: <i><?= $anErrorMsg ?></i> in <i><?= $aFile ?></i> on line <i><?= $aLine ?></i><br>
		</th>
	</tr>
<?
		$blnAddTrace = true;
		if (isset($DieOnError) && $DieOnError)
		{
			$blnAddTrace = false;
		}
		if ($blnAddTrace)
		{
?>
	<tr class="h">
		<th colspan="3">
			<b>Executing:</b><pre><?= $aSql ?></pre>
		</th>
	</tr>
	<tr class="h">
		<th colspan='3'>
			Call Stack
		</th>
	</tr>
	<tr class="e">
		<th>#</th>
		<th>Function</th>
		<th>Location</th>
	</tr>
<?
			$i = 1;
			$functionName = null;
			foreach ($aBacktrace as $anArray)
			{
				if (is_null($functionName))
				{
					$functionName = '{main}';
				}
?>
	<tr>
		<td class="e"><?= $i ?></td>
		<td class="v"><?= $functionName ?>()</td>
		<td class="v"><?= $anArray['file'] ?><b>:</b><?= $anArray['line'] ?></td>
	</tr>
<?
			$functionName = $anArray['function'];
				$i++;
			}
		}
?>
</table>
<br />
<?
	$error = ob_get_contents();
	ob_end_clean();
	ob_start();
	if (ini_get('register_argc_argv') == '1')
	{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html><head>
<style type="text/css">
body {background-color: #ffffff; color: #000000;}
body, td, th, h1, h2 {font-family: sans-serif;}
pre {margin: 0px; font-family: monospace;}
a:link {color: #000099; text-decoration: none; background-color: #ffffff;}
a:hover {text-decoration: underline;}
table {border-collapse: collapse;}
.center {text-align: center;}
.center table { margin-left: auto; margin-right: auto; text-align: left;}
.center th { text-align: center !important; }
td, th { border: 1px solid #000000; font-size: 75%; vertical-align: baseline;}
h1 {font-size: 150%;}
h2 {font-size: 125%;}
.p {text-align: left;}
.e {background-color: #ccccff; font-weight: bold; color: #000000;}
.h {background-color: #9999cc; font-weight: bold; color: #000000;}
.v {background-color: #cccccc; color: #000000;}
.vr {background-color: #cccccc; text-align: right; color: #000000;}
img {float: right; border: 0px;}
hr {width: 600px; background-color: #cccccc; border: 0px; height: 1px; color: #000000;}
</style>
<title>phpinfo()</title>
<meta name="ROBOTS" content="NOINDEX,NOFOLLOW,NOARCHIVE" /></head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<body><div class="center">
<table border="0" cellpadding="3" width="600">
	<tr class="h">
		<td colspan="3" class="v">
			<pre>
<?
		phpinfo();
?>
			</pre>
		</td>
	</tr>
</table>
</div></body></html>
<?
	}
	else
	{
		phpinfo();
	}
	$phpinfo = ob_get_contents();
	ob_end_clean();
	$strAdvisorErrorMessage = $phpinfo;
	$message = str_replace('<body><div class="center">', '<body><div class="center">'.$error, $phpinfo);
	if (($result = NotifyByFile("SQL Error", $message)) !== true)
	{
	}
	global $BitamNotifyError;
	if ($BitamNotifyError)
	{
		if (($result = NotifyBySMTP("SQL Error", $message)) !== true)
		{
			if (($result = NotifyByHTTP("SQL Error", $message)) !== true)
			{
			}
		}
	}

	if (!$blnAddTrace)
	{
		$aPos = strpos($strAdvisorErrorMessage, '<body><div class="center">');
		if (!is_null($aPos))
		{
			$strAdvisorErrorMessage = substr($strAdvisorErrorMessage, 0, $aPos + strlen('<body><div class="center">')).'</div></body></html>';
		}
		$strAdvisorErrorMessage = str_replace('<body><div class="center">', '<body><div class="center">'.$error, $strAdvisorErrorMessage);
		$strAdvisorErrorMessage = '<!-- AWERROR:AWERRDESC='.$error.'</AWERRDESC> -->'."\n\r".$strAdvisorErrorMessage;
		echo ($strAdvisorErrorMessage);
		die();
	}
	if ($abort) {
		exit($anErrorMsg);
	}
}



function fgetlines($fp)
{
    $lines = "";
    while ($str = @fgets($fp, 515))
    {
        $lines .= $str;
        # if the 4th character is a space then we are done reading
        # so just break the loop
        if (substr($str, 3, 1) == " ") { break; }
    }
    return $lines;
}


function NotifyBySMTP($subject, $message)
{
	$smtpServer = "mail.bitam.com";
    $smtpPort = "25";
    $timeout = "3";
    $username = "artuserror";
    $password = "b1tamae";
    $from = "artuserror@bitam.com";
    $to = "artuserror@bitam.com";

	$fp = @fsockopen($smtpServer, $smtpPort, $errno, $errstr, $timeout);
	if (!$fp) return "Failed to even make a connection";

	$res = fgetlines($fp);
	// echo($res);
	if (substr($res, 0, 3) != "220")
	{
		@fclose($fp);
		return "Failed to connect";
	}

	if ($username)
	{
		$req = "EHLO localhost\r\n";
		// echo($req);
		@fputs($fp, $req);
		$res = fgetlines($fp);
		// echo($res);
		if (substr($res, 0, 3) != "250")
		{
			@fclose($fp);
			return "Failed to Introduce";
		}

		$req = "AUTH LOGIN\r\n";
		// echo($req);
		@fputs($fp, $req);
		$res = fgetlines($fp);
		// echo($res);
		if (substr($res, 0, 3) != "334")
		{
			@fclose($fp);
			return "Failed to Initiate Authentication";
		}

		$req = base64_encode($username)."\r\n";
		// echo($req);
		@fputs($fp, $req);
		$res = fgetlines($fp);
		// echo($res);
		if (substr($res, 0, 3) != "334")
		{
			@fclose($fp);
			return "Failed to Provide Username for Authentication";
		}

		$req = base64_encode($password)."\r\n";
		// echo($req);
		@fputs($fp, $req);
		$res = fgetlines($fp);
		// echo($res);
		if (substr($res, 0, 3) != "235")
		{
			@fclose($fp);
			return "Failed to Authenticate";
		}
	}
	else
	{
		$req = "HELO localhost\r\n";
		// echo($req);
		@fputs($fp, $req);
		$res = fgetlines($fp);
		// echo($res);
		if (substr($res, 0, 3) != "250")
		{
			@fclose($fp);
			return "Failed to Introduce";
		}
	}

	$req = "MAIL FROM: <$from>\r\n";
	// echo($req);
	@fputs($fp, $req);
	$res = fgetlines($fp);
	// echo($res);
	if (substr($res, 0, 3) != "250")
	{
		@fclose($fp);
		return "MAIL FROM failed";
	}

	$req = "RCPT TO: <$to>\r\n";
	// echo($req);
	@fputs($fp, $req);
	$res = fgetlines($fp);
	// echo($res);
	if (substr($res, 0, 3) != "250")
	{
		@fclose($fp);
		return "RCPT TO failed";
	}

	$req = "DATA\r\n";
	// echo($req);
	@fputs($fp, $req);
	$res = fgetlines($fp);
	if (substr($res, 0, 3) != "354")
	{
		@fclose($fp);
		return "DATA failed";
	}

	$headers  = "MIME-Version: 1.0\r\n";
    $headers .= "Content-type: text/html; charset=utf-8\r\n";

    $req = "To: ".$to."\r\n";
    $req .= "From: ".$from."\r\n";
    $req .= "Subject: ".$subject."\r\n";
    $req .= $headers."\r\n\r\n";
    $req .= $message."\r\n.\r\n";
	// echo($req);
	@fputs($fp, $req);
	$res = fgetlines($fp);
	// echo($res);
	if (substr($res, 0, 3) != "250")
	{
		@fclose($fp);
		return "Message Body Failed";
	}

	$req = "QUIT\r\n";
	// echo($req);
	@fputs($fp, $req);
	$res = fgetlines($fp);
	// echo($res);
	if (substr($res, 0, 3) != "221")
	{
		@fclose($fp);
		return "QUIT failed";
	}

	return true;
}

function NotifyByHTTP($subject, $message)
{
	$httpServer = "www.bitam.com";
    $httpPort = "80";
    $timeout = "3";

    $request = "SUBJECT=".urlencode($subject);
    $request .= "&MESSAGE=".urlencode($message);

    //echo("<pre>");
	//echo("REQUEST:\n");
	//echo($request);
	$fp = @fsockopen($httpServer, $httpPort, $errno, $errstr, $timeout);
	if (!$fp) return "Failed to even make a connection";

	// Send the request
	@fputs($fp, "POST /notifyerror/notifyerror.php HTTP/1.0\r\n");
	@fputs($fp, "Host: www.bitam.com\r\n");
	@fputs($fp, "Content-Length: ".strlen($request)."\r\n");
	@fputs($fp, "Content-Type: application/x-www-form-urlencoded\r\n\r\n");
	@fputs($fp, $request);
	@fputs($fp, "\r\n\r\n");

	// Receive the response
	$response = fgetlines($fp);
	$pos = strpos($response, "\r\n\r\n");
	if ($pos !== false)
	{
		$response = substr($response, $pos + strlen("\r\n\r\n"));
	}

	//echo("RESPONSE:\n");
	//echo($response);
    //echo("</pre>");

    // Close the connection
	@fclose($fp);
	if ($response == "true")
	{
		return true;
	}
	return $response;
}

function NotifyByFile($subject, $message)
{
	if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')
	{
		$wildcard = dirname(__FILE__)."\\log\\*.html";
	}
	else
	{
		$wildcard = dirname(__FILE__)."/log/*.html";
	}
	$existingFiles = @glob($wildcard);
	if ($existingFiles !== false)
	{
		$n = count($existingFiles);
		$i = 0;
		while (($n - $i) >= 100)
		{
			@unlink($existingFiles[$i]);
			if (@file_exists($existingFiles[$i]))
			{
				return "Failed to delete previous log file";
			}
			$i++;
		}
	}
	if (strtoupper(substr(PHP_OS, 0, 3)) == 'WIN')
	{
		$filename = dirname(__FILE__)."\\log\\".date("YmdHis").".".((int) (((double) microtime()) * 1000000)).".html";
	}
	else
	{
		$filename = dirname(__FILE__)."/log/".date("YmdHis").".".((int) (((double) microtime()) * 1000000)).".html";
	}
	$fp = @fopen($filename, "wb");
	if (!$fp) return "Failed to even create a log file";

	if (!@fwrite($fp, $message))
	{
		@fclose($fp);
		return "Failed to write to log file";
	}
	@fclose($fp);

	return true;
}

?>